<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>
<script>
$(document).ready(function(){
	
});
window.onresize = function(){
	poGrid.resizeCanvas();
}
function selectRecordCloseWindow(){
	if(selectedRow>=0){
		var i = document.getElementById('rowId').value;
		if(i>=0){
			window.opener.document.getElementById('ticketGroup_'+i).value=poGrid.getDataItem(selectedRow).categorryTicketGroupId;
			window.opener.document.getElementById('section_'+i).value=poGrid.getDataItem(selectedRow).section;
			//window.opener.document.getElementById('section_'+i).readOnly = true;
			window.opener.document.getElementById('row_'+i).value=poGrid.getDataItem(selectedRow).row;
			//window.opener.document.getElementById('row_'+i).readOnly = true;
			window.opener.document.getElementById('seatLow_'+i).value=poGrid.getDataItem(selectedRow).availableSeatLow;
			window.opener.document.getElementById('seatHigh_'+i).value=poGrid.getDataItem(selectedRow).availableSeatHigh;
			window.opener.document.getElementById('qty_'+i).value=poGrid.getDataItem(selectedRow).availableQunatity;
			window.opener.document.getElementById('price_'+i).value=poGrid.getDataItem(selectedRow).price;
			//window.opener.document.getElementById('price_'+i).readOnly = true;
			window.close();
		}else{
			jAlert("Something went wrong, Please try againg by refreshing window.");
		}
		
	}else{
		jAlert("Cannot Map, No Record is selected.");
	}
}
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Map Long Inventory(PO)</li>
		</ol>
	</div>
</div>
<br />

<div style="position: relative" id="po">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Purchase Orders</label> <span id="openOrder_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel" onclick="openOrderGridToggleFilterRow()"></span>
		</div>
		<div id="po_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="po_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br/><br/>
<div class="row">
	<div class="col-lg-12">
		<div class="form-group" align="center">
			<input type="hidden" value="${rowId}" id="rowId"/>
			<button type="button" onclick="selectRecordCloseWindow();" style="width:100px" class="btn btn-primary">Map</button>
		</div>
	</div>
</div>

<div id="event_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
				Show records with PO <input type="text" id="openOrderGridSearch">
</div>
<script>
var selectedRow='';
var id='';
var poDataView;
var poGrid;
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var ticketGroupId="${selectedTicket}";
var poData = [];
var poColumns = [ {
	id : "poId",
	name : "PO ID",
	field : "poId",
	sortable : true
},{
	id : "section",
	name : "Section",
	field : "section",
	sortable : true
},{
	id : "row",
	name : "Row",
	field : "row",
	sortable : true
},{
	id : "seatLow",
	name : "Seat Low",
	field : "seatLow",
	sortable : true
}, {
	id : "seatHigh",
	name : "Seat High",
	field : "seatHigh",
	width:80,
	sortable : true
},{
	id : "availableSeatLow",
	name : "Available SeatLow",
	field : "availableSeatLow",
	sortable : true
},{
	id : "availableSeatHigh",
	name : "Available SeatHigh",
	field : "availableSeatHigh",
	sortable : true
}, {
	id : "quantity",
	name : "Total Quantity",
	field : "quantity",
	sortable : true
}, {
	id : "availableQunatity",
	name : "Available Quantity",
	field : "availableQunatity",
	sortable : true
},{
	id : "price",
	name : "price",
	field : "price",
	sortable : true
},{
	id : "createdBy",
	name : "Created By",
	field : "createdBy",
	sortable : true
}];

var poOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var poSortcol = "id";
var poSortdir = 1;
var poSearchString = "";
var percentCompleteThreshold = 0;

function poFilter(item, args) {
	var x= item["id"];
	if (args.poSearchString  != ""
			&& x.indexOf(args.poSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.poSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function poComparer(a, b) {
	var x = a[poSortcol], y = b[poSortcol];
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function openOrderGridToggleFilterRow() {
		poGrid.setTopPanelVisibility(!poGrid.getOptions().showTopPanel);
	}

//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#openOrder_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function refreshpoGridValues() {
		var i = 0;
		<c:forEach var="po" items="${ticketGroups}">
			var d = (poData[i] = {});
			d["id"] = '${po.id}';
			d["categorryTicketGroupId"] = '${po.id}';
			d["poId"] = '${po.purchaseOrderId}';
			d["section"] = '${po.section}';
			d["row"] = '${po.row}';
			d["seatLow"] = '${po.seatLow}';
			d["seatHigh"] = '${po.seatHigh}';
			d["availableSeatLow"] = '${po.availableSeatLow}';
			d["availableSeatHigh"] = '${po.availableSeatHigh}';
			d["quantity"] = '${po.quantity}';
			d["availableQunatity"] = '${po.availableQty}';
			d["price"] = '${po.priceStr}';
			d["createdBy"] = '${po.createdBy}';
			i++;
		</c:forEach>
	
			
		poDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		poGrid = new Slick.Grid("#po_grid", poDataView, poColumns, poOptions);
		poGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		poGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var poPager = new Slick.Controls.Pager(poDataView, poGrid, $("#po_pager"),pagingInfo);
		var poColumnpicker = new Slick.Controls.ColumnPicker(poColumns, poGrid,
			poOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#event_inlineFilterPanel").appendTo(poGrid.getTopPanel()).show();
		
		poGrid.onSort.subscribe(function(e, args) {
		poSortdir = args.sortAsc ? 1 : -1;
		poSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			poDataView.fastSort(poSortcol, args.sortAsc);
		} else {
			poDataView.sort(poComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	poDataView.onRowCountChanged.subscribe(function(e, args) {
		poGrid.updateRowCount();
		poGrid.render();
	});
	poDataView.onRowsChanged.subscribe(function(e, args) {
		poGrid.invalidateRows(args.rows);
		poGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
		$("#openOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			poSearchString = this.value;
			updatePoGridFilter();
		});
		
	function updatePoGridFilter() {
		poDataView.setFilterArgs({
			poSearchString : poSearchString
		});
		poDataView.refresh();
	}
	var eventrowIndex;
	poGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = poGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				//getCustomerInfoForInvoice(temprEventRowIndex);
			}
			
			selectedRow = poGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = poGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});

	// initialize the model after all the events have been hooked up
	poDataView.beginUpdate();
	poDataView.setItems(poData);
	
	poDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			poSearchString : poSearchString
		});
	poDataView.setFilter(poFilter);
		
	poDataView.endUpdate();
	poDataView.syncGridSelection(poGrid, true);
	$("#gridContainer").resizable();
	poGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
	if(ticketGroupId>0){
		for(var i=0;i<poGrid.getDataLength();i++){
			if(poGrid.getDataItem(i).id==ticketGroupId){
				poGrid.setSelectedRows([i]);
				break;
			}
		}
	}
	
}
window.onload = function() {
	refreshpoGridValues();
	
};
</script>