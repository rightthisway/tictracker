<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Invoice Refund</title>
	<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
	<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
	<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
	<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
	<!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
	<link href="../resources/css/style.css" rel="stylesheet">
	<link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/bootstrap.min.js"></script>
	<script src="../resources/js/jquery.alerts.js"></script>
</head>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Invoice</a>
			</li>
			<li><i class="fa fa-laptop"></i>Refund</li>
		</ol>
	</div>
</div>
<br />
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<div class="col-sm-12" id="msgDiv" style="display:none;">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
			<span id="msgBox"></span>
		</strong>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Payment Details</b></header>
			<div class="panel-body">
			
			<div class="form-group">
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Customer Name : </label><span style="font-size: 12px;">${customer.customerName} ${customer.lastName}</span>
                </div>
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Customer Email : </label><span style="font-size: 12px;">${customer.email}</span>
				</div>
				<%-- <div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Customer phone :</label><span style="font-size: 12px;">${customer.phone}</span>
				</div> --%>
			</div>
			
			<div class="form-group">
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Invoice Number : </label>
					<span style="font-size: 12px;" >${invoice.id} </span>
				</div> 
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Order No : </label><span style="font-size: 12px;">${order.id}</span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Order Created On : </label><span style="font-size: 12px;">${order.createdDateStr}</span>
				</div>
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Order Total : </label><span style="font-size: 12px;">${order.orderTotal}</span>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Primary Payment Method : </label><span style="font-size: 12px;">${order.primaryPaymentMethod}</span>
                </div>
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Secondary Payment Method : </label><span id="secondaryMethod" style="font-size: 12px;"></span>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Primary Payment Amount : </label><span id="primaryAmount" style="font-size: 12px;">${order.primaryAvailableAmt}</span>
                </div>
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Secondary Payment Amount : </label><span id="secondaryAmount" style="font-size: 12px;">${order.secondaryAvailableAmt}</span>
				</div>				
			</div>
			<div class="form-group">			
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Third Payment Method : </label><span id="thirdMethod" style="font-size: 12px;"></span>
				</div>
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Third Payment Amount : </label><span id="thirdAmount" style="font-size: 12px;">${order.thirdAvailableAmt}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>
<c:if test="${isValid==true}">
<div id="row">
	<div class="col-lg-12">
		<form class="form-validate form-horizontal" id="invoiceRefund" method="post" action="${pageContext.request.contextPath}/Accounting/InvoiceRefund">
			<div class="form-group">
				<div class="col-sm-11">
					<label>Note</label>
					<textarea name="refundNote" id="refundNote" rows="8" cols="80"></textarea>
				</div>
			</div>
			<div class="form-group">
				<input type="hidden" id="action" name="action" value="" /> 
				<input type="hidden" id="orderId" name="orderId" value="${order.id}" />
				<input type="hidden" id="primaryPaymentAmt" name="primaryPaymentAmt" value="${order.primaryAvailableAmt}" />
				<input type="hidden" id="secondaryPaymentAmt" name="secondaryPaymentAmt" value="${order.secondaryAvailableAmt}" />
				<input type="hidden" id="thirdPaymentAmt" name="thirdPaymentAmt" value="${order.thirdAvailableAmt}" />
				<div class="col-sm-2" id="refundDiv" style="display: none;">
					<label>Refund Amount 
					</label> <input type="text" id="refundAmount" name="refundAmount"  class='form-control' />
				</div>
				<div class="col-sm-3" id="walletCCDiv" style="display: none;">
					<label>Credit to Customer Wallet From Credit Card
					</label> <input type="text" id="creditWalletAmount" name="creditWalletAmount" class='form-control' />
				</div>
				<div class="col-sm-3" id="creditDiv" style="display: none;">
					<label>Credit to Customer Wallet From Wallet
					</label> <input type="text" id="creditAmount" name="creditAmount" class='form-control' />
				</div>
				<div class="col-sm-2" id="rewarPointDiv" style="display: none;">
					<label>Revert Reward points
					</label> <input type="text" id="rewardPoints" name="rewardPoints" class='form-control' />
				</div>
				<div class="col-sm-2">
					<button type="button" id="refundButton" class="btn btn-primary" style="margin-top: 19px;" onclick="makeRefund();">Make Refund</button>
				</div>
			</div>
		</form>
	</div>
</div>
</c:if>
<script>
var primaryPaymentMethod = '${order.primaryPaymentMethod}';
var secondaryPaymentMethod = '${order.secondaryPaymentMethod}';
var thirdPaymentMethod = '${order.thirdPaymentMethod}';
/*var primaryPaymentAmount = '${order.primaryPayAmt}';
var secondaryPaymentAmount = '${order.secondaryPayAmt}';
var thirdPayAmount = '${order.thirdPayAmt}';*/
var orderTotal = '${order.orderTotal}';
$(document).ready(function() {
	$('#refundNote').val('${invoice.internalNote}');
	
	if(primaryPaymentMethod == 'CREDITCARD' || primaryPaymentMethod == 'PAYPAL'
		|| primaryPaymentMethod == 'GOOGLEPAY' || primaryPaymentMethod == 'IPAY'){			
		$("#refundDiv").show();
		$("#walletCCDiv").show();
		//$("#creditDiv").show();		
	}
	else if(primaryPaymentMethod == 'CUSTOMER_WALLET'){
		if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){
			$("#refundDiv").show();
			$("#walletCCDiv").show();
			$("#creditDiv").show();	
			$('#secondaryMethod').text(secondaryPaymentMethod);
		}else{
			$("#creditDiv").show();
		}
	}
	else if(primaryPaymentMethod == 'FULL_REWARDS'){
		//$("#rewarPointDiv").show();
	}
	else if(primaryPaymentMethod == 'PARTIAL_REWARDS'){
		//$('#rewardPoints').val($('#primaryPaymentAmt').val());
		if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'PAYPAL'
			|| secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){
			$("#refundDiv").show();
			$("#walletCCDiv").show();
			//$("#creditDiv").show();
			//$("#rewarPointDiv").show();
			$('#secondaryMethod').text(secondaryPaymentMethod);
		}
		else if(secondaryPaymentMethod == 'CUSTOMER_WALLET' && (thirdPaymentMethod == 'CREDITCARD'
			|| thirdPaymentMethod == 'GOOGLEPAY' || thirdPaymentMethod == 'IPAY')){
			$("#refundDiv").show();
			$("#walletCCDiv").show();
			$("#creditDiv").show();
			//$("#rewarPointDiv").show();
			$('#secondaryMethod').text(secondaryPaymentMethod);
			$('#thirdMethod').text(thirdPaymentMethod);
		}
		else if(secondaryPaymentMethod == 'CUSTOMER_WALLET'){
			$("#creditDiv").show();
			//$("#rewarPointDiv").show();
			$('#secondaryMethod').text(secondaryPaymentMethod);
		}
		/* else if(secondaryPaymentMethod == 'IPAY' || secondaryPaymentMethod == 'GOOGLEPAY'){
			$('#secondaryMethod').text(secondaryPaymentMethod);
			$("#msgDiv").show();
			$('#msgBox').text('Cannot refund as selected invoice payment is made using '+secondaryPaymentMethod);
		} */
	}
	/*
	if(primaryPaymentMethod=="CREDITCARD"){
		$('#rewarPointDiv').hide();
	}else if(primaryPaymentMethod== "PARTIAL_REWARDS" && secondaryPaymentMethod == 'CREDITCARD'){
		$('#refundAmount').text(secondaryMethod);
	}else if(primaryPaymentMethod== "FULL_REWARDS"){
		$('#refundDiv').hide();
		$('#creditDiv').hide();
		$('#refundAmount').val();
	}
	*/
});

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	window.opener.getInvoiceGridData(0);
    }
};

function makeRefund(){
	var refundAmount = $('#refundAmount').val();
	var walletCCAmt = $('#creditWalletAmount').val();
	var creditAmount = $('#creditAmount').val();
	var rewardPoints = $('#rewardPoints').val();
	var primaryPaymentAmount = $('#primaryPaymentAmt').val();
	var secondaryPaymentAmount = $('#secondaryPaymentAmt').val();
	var thirdPayAmount = $('#thirdPaymentAmt').val();
	refundAmount = parseFloat(refundAmount);
	walletCCAmt = parseFloat(walletCCAmt);
	creditAmount = parseFloat(creditAmount);
	rewardPoints = parseFloat(rewardPoints);
	primaryPaymentAmount = parseFloat(primaryPaymentAmount);
	secondaryPaymentAmount = parseFloat(secondaryPaymentAmount);
	thirdPayAmount = parseFloat(thirdPayAmount);
	var isValid = false;
	
	if(primaryPaymentMethod == 'CREDITCARD' || primaryPaymentMethod == 'PAYPAL'
		|| primaryPaymentMethod == 'GOOGLEPAY' || primaryPaymentMethod == 'IPAY'){
		if(($('#refundAmount').val() != null && $('#refundAmount').val() != '') || ($('#creditWalletAmount').val() != null && $('#creditWalletAmount').val() != '')){
			if($('#refundAmount').val() > 0 || $('#creditWalletAmount').val() > 0){
				if(refundAmount > primaryPaymentAmount){
					jAlert("Refund amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
					return false;
				}
				else if(walletCCAmt > primaryPaymentAmount){
					jAlert("Credit to Wallet from paypal/cc amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
					return false;
				}
				else if((refundAmount+walletCCAmt) > primaryPaymentAmount){
					jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
					return false;
				}else{
					isValid = true;
				}
			}else{
				jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
				return false;
			}
		}
		else{
			jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty");
			return false;
		}
	}
	else if(primaryPaymentMethod == 'CUSTOMER_WALLET'){
		if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){
			if(($('#refundAmount').val() != null && $('#refundAmount').val() != '') || ($('#creditWalletAmount').val() != null && $('#creditWalletAmount').val() != '') || ($('#creditAmount').val() != null && $('#creditAmount').val() != '')){
				if($('#refundAmount').val() > 0 || $('#creditWalletAmount').val() > 0 || $('#creditAmount').val() > 0){
					if(refundAmount > secondaryPaymentAmount){
						jAlert("Refund amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
						return false;
					}
					else if(creditAmount > primaryPaymentAmount){
						jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
						return false;
					}
					else if((refundAmount+walletCCAmt) > secondaryPaymentAmount){
						jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
					return false;
				}
			}
			else{
				jAlert("Refund/Credit to wallet from paypal/cc amount should not be empty.");
				return false;
			}
		}
		else{
			if($('#creditAmount').val() != null && $('#creditAmount').val() != ''){
				if($('#creditAmount').val() > 0){
					if(creditAmount > primaryPaymentAmount){
						jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
						return false;			
					}else{
						isValid = true;
					}
				}else{
					jAlert("Credit to wallet from Wallet amount should be greater than 0");
					return false;
				}
			}
			else{
				jAlert("Credit to wallet from Wallet amount should not be empty.");
				return false;			
			}
		}		
	}
	else if(primaryPaymentMethod == 'FULL_REWARDS'){
		isValid = true;
		/*if($('#rewardPoints').val() != null && $('#rewardPoints').val() != ''){
			if(rewardPoints > primaryPaymentAmount){
				jAlert("Reward points should be less than or equal to "+primaryPaymentAmount);
				return false;
			}else{
				isValid = true;
			}
		}
		else{
			jAlert("Reward points should not be empty.");
			return false;
		}*/
	}
	else if(primaryPaymentMethod == 'PARTIAL_REWARDS'){
		
		if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'PAYPAL'
			|| secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){					
			if(($('#refundAmount').val() != null && $('#refundAmount').val() != '') || ($('#creditWalletAmount').val() != null && $('#creditWalletAmount').val() != '')){
				if($('#refundAmount').val() > 0 || $('#creditWalletAmount').val() > 0){
					if(refundAmount > secondaryPaymentAmount){
						jAlert("Refund amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
						return false;
					}
					else if((refundAmount+walletCCAmt) > secondaryPaymentAmount){
						jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
					return false;
				}
			}
			else{
				//jAlert("Refund/Wallet amount/Reward Points should not be empty.");
				jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty.");
				return false;
			}
		}
		else if(secondaryPaymentMethod == 'CUSTOMER_WALLET' && (thirdPaymentMethod == 'CREDITCARD' 
			|| thirdPaymentMethod == 'GOOGLEPAY' || thirdPaymentMethod == 'IPAY')){
			if(($('#refundAmount').val() != null && $('#refundAmount').val() !='') || ($('#creditWalletAmount').val() != null && $('#creditWalletAmount').val() != '')
					|| ($('#creditAmount').val() != null && $('#creditAmount').val() != '')){
				if($('#refundAmount').val() > 0 || $('#creditWalletAmount').val() > 0 || $('#creditAmount').val() > 0){
					if(creditAmount > secondaryPaymentAmount){
						jAlert("Wallet amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
						return false;
					}
					else if(refundAmount > thirdPayAmount){
						jAlert("Refund amount should not be greater than "+thirdPayAmount+" (Third Payment Amount).");
						return false;
					}
					else if((refundAmount+walletCCAmt) > thirdPayAmount){
						jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+thirdPayAmount+" (Third Payment Amount).");
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
					return false;
				}
			}
			else{
				//jAlert("Refund/Wallet Amount/Reward Points should not be empty.");
				jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should not be empty.");
				return false;
			}
		}
		else if(secondaryPaymentMethod == 'CUSTOMER_WALLET'){
			if(($('#creditAmount').val() != null && $('#creditAmount').val() != '')){
				if($('#creditAmount').val() > 0){
					if(creditAmount > secondaryPaymentAmount){
						jAlert("Credit to wallet from Wallet amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Credit to wallet from Wallet amount should be greater than 0");
					return false;
				}
			}
			else{
				//jAlert("Reward Points/Wallet amount should not be empty.");
				jAlert("Credit to wallet from Wallet amount should not be empty.");
				return false;
			}
		}
	}
	if(isValid){
		//jAlert("Pass value to controller..");
		$('#action').val('PARTIAL_REWARDS');
		$('#invoiceRefund').submit();
	}
}
	/*
	if(primaryPaymentMethod == "CREDITCARD"){
		if((refundAmout+creditAmount) > parseInt(primaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Order total");
			return;
		}
		if((refundAmout+creditAmount) == 0){
			jAlert("Atleast Refund or Credit amount should be greater than 0.");
			return;
		}
		$('#action').val('CREDITCARD');
	}else if(primaryPaymentMethod == "PAYPAL"){
		if((refundAmout+creditAmount) > parseInt(primaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Order total");
			return;
		}
		if((refundAmout+creditAmount) == 0){
			jAlert("Atleast Refund or Credit amount should be greater than 0.");
			return;
		}
		$('#action').val('PAYPAL');
	}else if(primaryPaymentMethod== "PARTIAL_REWARDS" && secondaryPaymentMethod == 'CREDITCARD'){
		if((refundAmout+creditAmount) > parseInt(secondaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Secondary Payment amount.");
			return;
		}
		if((refundAmout+creditAmount+rewardPoints) == 0){
			jAlert("Atleast (Refund + Credit + reward) amount should be greater than 0.");
			return;
		}
		if(rewardPoints > parseInt(primaryPaymentAmount)){
			jAlert("Reward points should be less than Primary payment amount.");
			return;
		}
		$('#action').val('PARTIAL_REWARDS');
	}else if(primaryPaymentMethod== "PARTIAL_REWARDS" && secondaryPaymentMethod == 'PAYPAL'){
		if((refundAmout+creditAmount) > parseInt(secondaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Secondary Payment amount.");
			return;
		}
		if((refundAmout+creditAmount+rewardPoints) == 0){
			jAlert("Atleast (Refund + Credit + reward) amount should be greater than 0.");
			return;
		}
		if(rewardPoints > parseInt(primaryPaymentAmount)){
			jAlert("Reward points should be less than Primary payment amount.");
			return;
		}
		$('#action').val('PARTIAL_REWARDS');
	}else if(primaryPaymentMethod == "FULL_REWARDS"){
		if(rewardPoints == 0){
			jAlert("Please add reward point to revert greater than 0.");
			return;
		}
		if(rewardPoints > parseInt(orderTotal)){
			jAlert("Reward points should not be greater than Order total.");
			return;
		}
		$('#action').val('FULL_REWARDS');
	}
	*/
	/* $.ajax({
		url : "${pageContext.request.contextPath}/Accounting/InvoiceRefund",
		type : "post",
		dataType: "json",
		data : $("#invoiceRefund").serialize(),
		success : function(res){
			
		}, error : function(error){
			jAlert("There is something wrong. Please try again"+error,"Error");
			return false;
		} 
	});*/

</script>
</html>