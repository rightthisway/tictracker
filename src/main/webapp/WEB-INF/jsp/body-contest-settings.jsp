<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script>
var jq2 = $.noConflict(true);
$(document).ready(function() {
	
});
function getContestConfigSettings(action){
	//var requestUrl = "${pageContext.request.contextPath}/UpdateQuizConfigSettings";
	$.ajax({
		url : "${pageContext.request.contextPath}/GetContestSettings",
		type : "post",
		dataType: "json",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				if(action == 'LIVEURL'){
					$('#videoSourceUrl').val(jsonData.configSettings.liveStreamUrl);
					$('#videoSourceUrlModal').modal('show');
				}else if(action == 'LIVESTHRESHOLD'){
					$('#earnLiveThreshhold').val(jsonData.configSettings.liveThreshold);
					$('#earnLiveThreshHoldModal').modal('show');
				}else if(action == 'NOTIFICATION7'){
					$('#notification7').val(jsonData.configSettings.notification7);
					$('#notification7Modal').modal('show');
				}else if(action == 'NOTIFICATION20'){
					$('#notification20').val(jsonData.configSettings.notification20);
					$('#notification20Modal').modal('show');
				}else if(action == 'SHOWNEXTCONTEST'){
					if(jsonData.configSettings.showUpcomingContest == true || jsonData.configSettings.showUpcomingContest == 'true'){
						$('#nextContest').val("YES");
					}else{
						$('#nextContest').val("NO");
					}
					$('#nextContestModal').modal('show');
				}
			} else {
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function updateQuizConfigSettings(action){
	var liveUrl = $('#videoSourceUrl').val();
	var showNextContest = $('#nextContest').val(); 
	var liveThreshHold = $('#earnLiveThreshhold').val();
	var notification7 = $('#notification7').val();
	var notification20 = $('#notification20').val();
	var notificationManual = $('#notificationManual').val();
	var dataString = "action="+action;
	if(action == 'LIVEURL'){
		if(liveUrl == ''){
			jAlert("Video Source Url is Mandatory.");
			return;
		}
		dataString += "&liveUrl="+liveUrl;
	}else if(action == 'LIVESTHRESHOLD'){
		if(liveThreshHold == ''){
			jAlert("Earn Lives Threshold is Mandatory.");
			return;
		}
		dataString += "&liveThreshold="+liveThreshHold;
	}else if(action == 'NOTIFICATION7'){
		if(notification7 == ''){
			jAlert("1 Hour Notification text is Mandatory.");
			return;
		}
		dataString += "&notification7="+notification7;
	}else if(action == 'NOTIFICATION20'){
		if(notification20 == ''){
			jAlert("3 Min. Notification text is Mandatory.");
			return;
		}
		dataString += "&notification20="+notification20;
	}else if(action == 'MANUALNOTIFICATION'){
		if(notificationManual == ''){
			jAlert("Manual notification text is Mandatory.");
			return;
		}
		dataString += "&notificationManual="+notificationManual;
	}else if(action == 'SHOWNEXTCONTEST'){
		if(showNextContest == ''){
			jAlert("Display Next contest in APP is mendatory please select either YES or TBD.");
			return;
		}
		dataString += "&nextContest="+showNextContest;
	}
	
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdateContestSettings",
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				if(action == 'LIVEURL'){
					$('#videoSourceUrlModal').modal('hide');
				}else if(action == 'SHOWNEXTCONTEST'){
					$('#nextContestModal').modal('hide');
				}else if(action == 'LIVESTHRESHOLD'){
					$('#earnLiveThreshHoldModal').modal('hide');
				}else if(action == 'NOTIFICATION7'){
					$('#notification7Modal').modal('hide');
				}else if(action == 'NOTIFICATION20'){
					$('#notification20Modal').modal('hide');
				}else if(action == 'MANUALNOTIFICATION'){
					$('#notificationManualModal').modal('hide');
				}
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function openManualNitification(){
	$('#notificationManualModal').modal('show');
}


function getReferralRewardSettings(){
	 
	$.ajax({
		url : "${pageContext.request.contextPath}/GetReferralRewardSettings",
		type : "post",
		dataType: "json",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#referralReward').val(jsonData.referralRewardSettings.contestCustReferralRewardPerc);
				$('#referralRewardCreditType').val(jsonData.referralRewardSettings.referralRewardCreditType);
				$('#contestReferralRewardModal').modal('show');
			} else {
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function updateChattingSettings(isChatting){
	/* $.ajax({
		url : "${pageContext.request.contextPath}/UpdateChatting",
		type : "post",
		dataType: "json",
		data:"isChatting="+isChatting,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	}); */
}

 
function updateReferralRewardsSettings(){
	
	var referralReward = $('#referralReward').val();
	var referralRewardCreditType = $('#referralRewardCreditType').val(); 
	var action= "UPDATE";
	var dataString = "action="+action;
	 
	if(referralReward == ''){
		jAlert("Referral Reward points is mandatory.");
		return;
	}
	dataString += "&referralReward="+referralReward;
	
	if(referralRewardCreditType == ''){
		jAlert("Choose valid referral reward credit type.");
		return;
	}
	dataString += "&referralRewardCreditType="+referralRewardCreditType;
	
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdateReferralRewardSettings",
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#contestReferralRewardModal').modal('hide');
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateOneSignalNotification(){
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdateOneSignalNotifications",
		type : "post",
		dataType: "json",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Contests Notifications & Settings</li>
		</ol>
	</div>
</div>
<div class="row" style="font-size:14px !important;">
     <div class="col-lg-4">
         <section class="panel">
             <header class="panel-heading">
                <strong>Contest Settings</strong> 
             </header>
             <div class="list-group">
            	   <a class="list-group-item " href="javascript:getContestConfigSettings('LIVEURL')">
                    Change Live Stream URL
                 </a>
                 <a class="list-group-item " href="javascript:getContestConfigSettings('LIVESTHRESHOLD')">
                    Change Earn Life Threshold
                 </a>
                  <a class="list-group-item " href="javascript:getReferralRewardSettings();">
                    Referral Reward Setting
                 </a>
             </div>
         </section>
     </div>
     <div class="col-lg-4">
         <section class="panel">
             <header class="panel-heading">
                <strong>Contest Notifications</strong>
             </header>
             <div class="list-group">
            	  <!--   <a class="list-group-item " href="javascript:getContestConfigSettings('NOTIFICATION20')">
                    Change 3 Minutes Notification Message
                 </a> -->
                <!--  <a class="list-group-item " href="javascript:getContestConfigSettings('NOTIFICATION7')">
                     Change 1 Hour Notification Message
                 </a> -->
                  <a class="list-group-item " href="javascript:updateOneSignalNotification();">
                     Update One Signal Notification Device Ids
                 </a>
                  <a class="list-group-item " href="javascript:openManualNitification();">
                     Send Manual Notification
                 </a>
                 <a class="list-group-item " href="javascript:getContestConfigSettings('SHOWNEXTCONTEST')">
                     Display TBD OR Display Next Contest
                 </a>
                 <!--  <a class="list-group-item " href="javascript:updateChattingSettings('true')">
                     Set Chatting On
                 </a> -->
             </div>
         </section>
     </div>
     
     
     
<!-- Video Source Url Update - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="videoSourceUrlModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Live Video Url</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-12 col-xs-12">
						<label>Live Video Source Url<span class="required">*</span>
						</label> <input class="form-control" type="text" id="videoSourceUrl" name="videoSourceUrl">
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateQuizConfigSettings('LIVEURL')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Video Source Url Update - Ends  -->



<!--Next Contest details config. - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="nextContestModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Display Upcoming contest on APP.</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-6 col-xs-6">
						<label>Display TBD / Next Contest?<span class="required">*</span></label>
						<select name="nextContest" id="nextContest" class="form-control">
							<option value="YES">Display Next Contest</option>
							<option value="NO">Display TBD</option>
						</select> 
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateQuizConfigSettings('SHOWNEXTCONTEST')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Next Contest details config.- Ends  -->


<!-- Earn Live Threshhold - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="earnLiveThreshHoldModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Earn Live Threshold</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-12 col-xs-12">
						<label>Earn Lives ThreshHold<span class="required">*</span>
						</label> <input class="form-control" type="text" id="earnLiveThreshhold" name="earnLiveThreshhold">
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateQuizConfigSettings('LIVESTHRESHOLD')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  Earn Live Threshhold - Ends  -->

<!-- Referral Reward - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="contestReferralRewardModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Referral Reward Setting</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-6 col-xs-6">
						<label><b>Referral Reward Credit Type</b><span class="required" >*</span>
						</label> 
						<select  class="form-control" id="referralRewardCreditType" name="referralRewardCreditType">
							<option value="Fixed">Fixed</option>
							<option value="Percentage">Percentage</option>
						</select>
					</div>	
					
					<div class="form-group col-sm-6 col-xs-6">
						<label><b>Referral Reward Points</b><span class="required">*</span>
						</label> <input class="form-control" type="text" id="referralReward" name="referralReward">
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateReferralRewardsSettings()">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  Referral Reward - Ends  -->


<!-- Notification 7 - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="notification7Modal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest 1 Hour Notification</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-12 col-xs-12">
						<label>1 Hour Notification Text<span class="required">*</span></label> 
						<textarea name="notification7" id="notification7" class="form-control" rows="5"></textarea>
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateQuizConfigSettings('NOTIFICATION7')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  Notification 7 - Ends  -->


<!-- Notification 20 - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="notification20Modal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest 3 Min. Notification</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-12 col-xs-12">
						<label>3 Min. Notification Text<span class="required">*</span></label> 
						<textarea name="notification20" id="notification20" class="form-control" rows="5"></textarea>
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateQuizConfigSettings('NOTIFICATION20')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  Notification 20 - Ends  -->


<!-- Notification Manual - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="notificationManualModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Manual Notification</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-12 col-xs-12">
						<label>Manual Notification Text<span class="required">*</span></label> 
						<textarea name="notificationManual" id="notificationManual" class="form-control" rows="5"></textarea>
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="updateQuizConfigSettings('MANUALNOTIFICATION')">Send Now</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  Notification Manual - Ends  -->
     
     
</div>