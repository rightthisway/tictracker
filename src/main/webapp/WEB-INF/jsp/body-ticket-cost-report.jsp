<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script src="../resources/amcharts/core.js" type="text/javascript"></script>
<script src="../resources/amcharts/maps.js" type="text/javascript"></script>
<script src="../resources/amcharts/charts.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/moonrisekingdom.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/material.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/animated.js" type="text/javascript"></script>

<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-laptop"></i>Ticket Cost Data</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
				
</div>


<div>
	<input type="button"class="btn btn-primary"  value="Download PDF" onclick="savePDF();" />
	<div id="chartdiv" style="height: 400px;" class="col-md-12"></div>
	<div id="chart2div" style="height: 400px;"  class="col-md-6"></div>
	<div id="chart3div" style="height: 400px;"  class="col-md-6"></div>
</div>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});
	
});

function callAPIReport(reportURL){
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function openContestReportModal(reportName){
	$('#contestReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
}

function openCustChainReportModal(){
	$('#custChainReportModal').modal('show'); 
}

function generateReport(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var reportURL = $('#contestReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function generateChainReport(){
	var userId = $('#userId').val(); 
	if(null == userId || userId == "" || userId == " "){
		$('#userIdErrId').html("Please Enter Valid User ID."); 
		return;
	}else{
		$('#userIdErrId').html(""); 
		window.location.href = "${pageContext.request.contextPath}/Reports/DownloadCustomerStatsReport?userId="+userId;
	}
}

function generateBotReport(){ 
	window.location.href = "${pageContext.request.contextPath}/Reports/DownloadBotReport";
}




var chart = am4core.create("chartdiv", am4charts.XYChart);
var chart2 = am4core.create("chart2div", am4charts.XYChart);
var chart3 = am4core.create("chart3div", am4charts.XYChart);		
function getReportChart(invoiceStatus, ticketCost, ticketQnty){
	//var chart = am4core.create("chartdiv", am4charts.XYChart);
				// Add data
				
				
				// Create chart instance
				var chart = am4core.create("chartdiv", am4charts.XYChart);
				chart.maskBullets = false;
				chart.numberFormatter.numberFormat = "#.#";

				chart.data = JSON.parse(invoiceStatus);

				chart.colors.list = [
				  am4core.color("#99cc00"),
				  am4core.color("#FFC75F")
				];
				
				// Create axes
				var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
				categoryAxis.dataFields.category = "viewName";
				categoryAxis.renderer.grid.template.location = 0;
				
				var label = categoryAxis.renderer.labels.template;
					label.wrap = true;
					label.maxWidth = 100;

				var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
				valueAxis.renderer.inside = true;
				valueAxis.renderer.labels.template.disabled = true;
				valueAxis.min = 0;
				valueAxis.extraMax = 0.1;
				valueAxis.calculateTotals = true;

				// Create series
				function createSeries(field, name) {
				  
				  // Set up series
				  var series = chart.series.push(new am4charts.ColumnSeries());
				  series.name = name;
				  series.dataFields.valueY = field;
				  series.dataFields.categoryX = "viewName";
				  series.sequencedInterpolation = true;
				  
				  // Make it stacked
				  series.stacked = true;
				  
				  // Configure columns
				  series.columns.template.width = am4core.percent(60);
				  series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
				  
				  // Add label
				  var labelBullet = series.bullets.push(new am4charts.LabelBullet());
				  labelBullet.label.text = "{valueY}";
				  labelBullet.label.fill = am4core.color("#fff");
				  labelBullet.locationY = 0.5;
				  
				  return series;
				}

				createSeries("viewCount", "Completed");
				createSeries("viewCount1", "Outstanding");

				// Create series for total
				var totalSeries = chart.series.push(new am4charts.ColumnSeries());
				totalSeries.dataFields.valueY = "viewCount2";
				totalSeries.dataFields.categoryX = "viewName";
				totalSeries.stacked = true;
				totalSeries.hiddenInLegend = true;
				totalSeries.columns.template.strokeOpacity = 0;

				var totalBullet = totalSeries.bullets.push(new am4charts.LabelBullet());
				totalBullet.dy = -20;
				totalBullet.label.text = "{valueY.total}";
				totalBullet.label.hideOversized = false;
				totalBullet.label.fontSize = 18;
				totalBullet.label.background.fill = totalSeries.stroke;
				totalBullet.label.background.fillOpacity = 0.2;
				totalBullet.label.padding(5, 10, 5, 10);


				// Legend
				chart.legend = new am4charts.Legend();
				
				// Enable export
				chart.exporting.menu = new am4core.ExportMenu();
				
//var chart2 = am4core.create("chart2div", am4charts.XYChart);

				chart2.data = JSON.parse(ticketCost);
				
				chart2.colors.list = [
				  am4core.color("#00cccc"),
				  am4core.color("#FFC75F")
				];
				var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
					categoryAxis.dataFields.category = "viewName";
					categoryAxis.renderer.grid.template.location = 0;
					categoryAxis.renderer.minGridDistance = 30;
					categoryAxis.renderer.labels.template.rotation = 270
					categoryAxis.title.text = "Month Wise Ticket Cost";
					categoryAxis.title.fontWeight = "bold";
				
					var label = categoryAxis.renderer.labels.template;
					label.wrap = true;
					label.maxWidth = 100;
			

					var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
					valueAxis.title.text = "Ticket Cost";
					valueAxis.title.fontWeight = "bold";
				
					
					// Create series
					var series = chart2.series.push(new am4charts.ColumnSeries());
					series.dataFields.valueY = "viewCountDouble";
					series.dataFields.categoryX = "viewName";
					series.name = "viewCountDouble";
					series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
					series.columns.template.fillOpacity = .8;


					var valueLabel = series.bullets.push(new am4charts.LabelBullet());
					valueLabel.label.text = "{valueY}";
					valueLabel.label.rotation = 270
					valueLabel.label.fontSize = 15;
					valueLabel.label.dx = 10;
					
					var columnTemplate = series.columns.template;
					columnTemplate.strokeWidth = 2;
					columnTemplate.strokeOpacity = 1;

				
				
				// Enable export
				chart2.exporting.menu = new am4core.ExportMenu();
				
//var chart3 = am4core.create("chart2div", am4charts.XYChart);

				chart3.data = JSON.parse(ticketQnty);
				
				chart3.colors.list = [
				  am4core.color("#ff9999"),
				  am4core.color("#FFC75F")
				];
				var categoryAxis = chart3.xAxes.push(new am4charts.CategoryAxis());
					categoryAxis.dataFields.category = "viewName";
					categoryAxis.renderer.grid.template.location = 0;
					categoryAxis.renderer.minGridDistance = 30;
					categoryAxis.renderer.labels.template.rotation = 270
					categoryAxis.title.text = "Month Wise Ticket Quantity";
					categoryAxis.title.fontWeight = "bold";
				
					var label = categoryAxis.renderer.labels.template;
					label.wrap = true;
					label.maxWidth = 100;


					var valueAxis = chart3.yAxes.push(new am4charts.ValueAxis());
					valueAxis.title.text = "Ticket Quantity";
					valueAxis.title.fontWeight = "bold";
				
					// Create series
					var series = chart3.series.push(new am4charts.ColumnSeries());
					series.dataFields.valueY = "viewCount";
					series.dataFields.categoryX = "viewName";
					series.name = "viewCount";
					series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
					series.columns.template.fillOpacity = .8;
					
					var valueLabel = series.bullets.push(new am4charts.LabelBullet());
					valueLabel.label.text = "{valueY}";
					valueLabel.label.rotation = 270
					valueLabel.label.fontSize = 15;
					valueLabel.label.dx = 10;

					var columnTemplate = series.columns.template;
					columnTemplate.strokeWidth = 2;
					columnTemplate.strokeOpacity = 1;

				
				
				// Enable export
				chart3.exporting.menu = new am4core.ExportMenu();
		}





function savePDF() {
  
  Promise.all([
    chart.exporting.pdfmake,
    chart.exporting.getImage("png"),
    chart2.exporting.getImage("png"),
	chart3.exporting.getImage("png")
  ]).then(function(res) { 
    
    var pdfMake = res[0];
    
    // pdfmake is ready
    // Create document template
    var doc = {
      pageSize: "A4",
      pageOrientation: "portrait",
      pageMargins: [30, 30, 30, 30],
      content: []
    };
    
   

    doc.content.push({
      text: "Ticket Cost Report",
      fontSize: 20,
      bold: true,
      margin: [0, 20, 0, 15]
    });
	
    doc.content.push({
      image: res[1],
      width: 530,
	  height:350
    });
   
     doc.content.push({
      image: res[2],
      width: 530,
	  height:350
    });
    
	
	doc.content.push({
      image: res[3],
      width: 530,
	  height:350
    });
    pdfMake.createPdf(doc).download("report.pdf");
    
  });
  
}

//call functions once page loaded
	window.onload = function() {
		getReportChart('${MonthWiseInvoiceStatus}', '${MonthWiseTicketCost}', '${MonthWiseTicketQuantity}');
		$('#pollingContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingContestPreference()'>");
		
		enableMenu();
	};		

</script>

<style>
	input{
		color : black !important;
	}
</style>




