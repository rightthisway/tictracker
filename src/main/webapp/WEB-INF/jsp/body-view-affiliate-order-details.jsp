

<!-- popup View Affiliate Order Details -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-order-details">View Order Details</button> -->
	<div id="view-order-details" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">View Order Details</h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Customer Order
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Customer Order</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Customer Order Summery</li>
							</ol>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div id="vOrder_successDiv" class="alert alert-success fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="vOrder_successMsg"></span></strong>
							</div>					
							<div id="vOrder_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="vOrder_errorMsg"></span></strong>
							</div>
						</div>	
					</div>

					<br/>
					<div class="row">
					<div class="col-xs-12">
							<section class="panel full-width"> 
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">
							<b>Order Info</b> </header>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Order ID :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_id">${affiliateHistory.order.id}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Order Total :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_total">${affiliateHistory.orderTotal}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Ticket Quantity :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_qty">${affiliateHistory.order.qty}</span>
									</div>
								
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Event Name :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_eventName">${affiliateHistory.order.eventName}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Event Date & Time :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_eventDateTime">${affiliateHistory.order.eventDateStr}&nbsp;${affiliateHistory.order.eventTimeStr}</span>
									</div>
								</div>
							</div>
							</section>
						</div>
					</div>

					<div class="row">
					<div class="col-xs-12">
							<section class="panel"> 
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">
							<b>Customer Info</b> </header>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Customer Name :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_customerName">${affiliateHistory.customer.customerName} &nbsp;&nbsp; ${affiliateHistory.customer.lastName}</span>
									</div>
									<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Email :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_email">${affiliateHistory.customer.email}</span>
									</div>
								</div>
							</div>
							</section>
						</div>
					</div>

					<div class="row">
					<div class="col-xs-12">
							<section class="panel"> 
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">
							<b>Affiliate Cash</b> </header>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Earned Cash :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_creditedCash">${affiliateHistory.creditedCash} &nbsp;&nbsp; ${affiliateHistory.customer.lastName}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Status :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_status">${affiliateHistory.rewardStatus}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Active Date :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_activeDate">${activeDate}</span>
									</div>
								</div>
							</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup View Order Details -->

<script>
	
	//Start View Order Summary
	function getAffiliateOrderSummary(userId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/GetAffiliateOrderSummary.json",
			type : "post",
			data : "userId="+userId+"&orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Affiliate Order Detail(s) Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}else {
					setAffiliateOrderSummary(jsonData);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setAffiliateOrderSummary(jsonData){
		$('#view-order-details').modal('show');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#vOrder_successDiv').hide();
			$('#vOrder_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#vOrder_errorDiv').hide();
			$('#vOrder_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#vOrder_successDiv').show();
			$('#vOrder_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#vOrder_errorDiv').show();
			$('#vOrder_errorMsg').text(jsonData.errorMessage);
		}
		var affHistory = jsonData.affiliateHistory;
		$('#vOrder_id').text(affHistory.order.id);
		$('#vOrder_total').text(affHistory.orderTotal);
		$('#vOrder_qty').text(affHistory.order.qty);
		$('#vOrder_eventName').text(affHistory.order.eventName);
		$('#vOrder_eventDateTime').text(affHistory.order.eventDateStr+" "+affHistory.order.eventTimeStr);
		$('#vOrder_customerName').text(affHistory.customer.customerName+" "+affHistory.customer.lastName);
		$('#vOrder_email').text(affHistory.customer.email);
		$('#vOrder_creditedCash').text(affHistory.creditedCash);
		$('#vOrder_status').text(affHistory.rewardStatus);
		$('#vOrder_activeDate').text(jsonData.activeDate);		
	}
	//End View Order Summary
	
</script>