<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<script src="../resources/js/app/invoiceRelatedPO.js"></script>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

input {
	color: black !important;
}
</style>

<script>
var jq2 = $.noConflict(true);
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
	if(($('#artistId').val()=='' || $('#artistId').val()==null)
		&& ($('#venueId').val()=='' || $('#venueId').val()==null)){
			$('#resetLink').hide();
	}
	 $('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$("#openOrdersTab").click(function(){
		callTabOnChange('OpenOrders');		
	});
	$("#shipmentPendingsTab").click(function(){
		callTabOnChange('PendingShipmentOrders');
	});
	$("#closedOrdersTab").click(function(){
		callTabOnChange('ClosedOrders');
	});
	$("#passedOrdersTab").click(function(){
		callTabOnChange('PassedOrders');
	});
	
	jq2('#artistVenue').autocomplete("AutoCompleteArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		if(row[0]=="ARTIST"){
			$('#artistId').val(row[1]);
		}else if(row[0]=='VENUE'){
			$('#venueId').val(row[1]);
		}
		$('#artistVenue').val("");
		$('#selectedItem').text(row[2]);
		$('#artistVenueName').val(row[2]);
		$('#resetLink').show();
		getEventforArtistOrVenue(row[0],row[1]);
	});
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshClosedOrderGrid(JSON.parse(JSON.stringify(${closedOrders})));
		$('#closedOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserClosedOrderPreference()'>");
		
	});
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  closedOrderGrid.resizeCanvas();
	});

	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchClosedOrderBtn').click();
		    return false;  
		  }
	});
	
});
	
	var prodType;
	function getCompanyProduct(obj){	
		prodType = obj.value;
	}
	function callTabOnChange(selectedTab) {		
		var data = selectedTab;
		var frmDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		var profitLoss = $('#profitLoss').val();
		var artistId = $('#artistId').val();
		var venueId = $('#venueId').val();
		var eventSelect = $('#eventSelect').val();
		var artistVenue = $('#artistVenueName').val();
		var productType = "${selectedProduct}";
		if(prodType == null || prodType == "" || prodType == 'undefined'){
			prodType = productType;
		}
		/*var externalOrderId = $('#externalOrderId').val();
		var orderId = $('#orderId').val();		
		var invoiceNo = $('#invoiceNo').val();*/
		
		data += "?action=search";
		if(frmDate == null || frmDate == ""){}
		else{
			data += "&fromDate="+frmDate; 
		}
		if(toDate == null || toDate == ""){}
		else{
			data += "&toDate="+toDate;
		}		
		if(profitLoss == null || profitLoss == ""){}
		else{
			data += "&profitLoss="+profitLoss;
		}
		if(prodType != null || prodType != ''){
			data += "&productType="+prodType;
		}
		if(artistVenue == null || artistVenue == ""){}
		else{
			data += "&artistVenueName="+artistVenue;
		}
		if(artistId == null || artistId == ""){}
		else{
			data += "&artistId="+artistId;
		}
		if(venueId == null || venueId == ""){}
		else{
			data += "&venueId="+venueId;
		}
		if(eventSelect == null || eventSelect == ""){}
		else{
			data += "&eventSelect="+eventSelect;
		}
		/*
		if(externalOrderId == null || externalOrderId == ""){}
		else{
			data += "&externalOrderId="+externalOrderId;
		}
		if(orderId == null || orderId == ""){}
		else{
			data += "&orderId="+orderId;
		}
		if(invoiceNo == null || invoiceNo == ""){}
		else{
			data += "&invoiceNo="+invoiceNo;
		}*/
		
		window.location = "${pageContext.request.contextPath}/Deliveries/"+data;
	}
	
	function saveUserClosedOrderPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = closedOrderGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('closedordergrid',colStr);
	}


function resetItem(){
	$('#resetLink').hide();
	$('#artistId').val("");
	$('#venueId').val("");
	$('#eventSelect').empty();
	$('#selectedItem').text("");
	$('#artistVenueName').val("");
	$('#eventSelect').append("<option value=''>--select--</option>");
}

function getEventforArtistOrVenue(type,id){
	$.ajax({
			url : "${pageContext.request.contextPath}/Deliveries/GetEventsByVenueOrArtist",
			type : "post",
			dataType:"json",
			data:"type="+type+"&id="+id,
			success : function(response){
				events = JSON.parse(JSON.stringify(response.events));
				fillEventCombo(events);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
}

function fillEventCombo(eventData){
	var template = '';
	$('#eventSelect').empty();
	$('#eventSelect').append("<option value=''>--select--</option>");
	$.each(eventData, function(i,value) {
		template += "<option value='"+value.eventId+"'>"+value.eventName+" - "+value.eventDateStr+" "+value.eventTimeStr+" "+value.venue+"</option>";
	});
	$('#eventSelect').append(template);
}
function submitSearchForm(){
	$('#action').val("search");
	closedorderGridSearchString='';
	columnFilters = {};
	getClosedOrderGridData(0);
}

function exportToExcel(){
	//var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&artistId="+$('#artistId').val()+"&venueId="+$('#venueId').val()+"&eventSelect="+$('#eventSelect').val()+"&profitLoss="+$('#profitLoss').val()+"&productType="+$('#productType').val()+"&invoiceNo="+$('#invoiceNo').val()+"&externalOrderId="+$('#externalOrderId').val()+"&orderId="+$('#orderId').val();
	var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&artistId="+$('#artistId').val()+"&venueId="+$('#venueId').val()+"&eventSelect="+$('#eventSelect').val()+"&profitLoss="+$('#profitLoss').val()+"&productType="+$('#productType').val()+"&headerFilter="+closedorderGridSearchString;
	appendData += "&brokerId="+$('#brokerId').val();
    //var url = "${pageContext.request.contextPath}/Deliveries/ClosedOrdersExportToExcel?"+appendData;
    var url = apiServerUrl+"ClosedOrdersExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	$('#action').val("search");
	closedorderGridSearchString='';
	columnFilters = {};
	sortingString ='';
	getClosedOrderGridData(0);
}
</script>

<div class="row">
	<ul id="contextMenu" style="display: none; position: absolute">
		<li data="openInvoice">Open Invoice</li>
		<li data="view invoice po">View Related Invoice/Purchase Orders</li>
		<li data="view modify notes">View/Modify Notes</li>
		<!-- <li data="openPO">Open Purchase Order</li> -->
	</ul>
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Deliveries
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Closed Orders</a>
			</li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Closed Orders</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 filters-div">
		<!--  search form start -->
		<form:form role="form" id="searchClosedOrders" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/Deliveries/ClosedOrders">
			<input type="hidden" id="action" name="action" value="" />
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Invoice From Date</label> 
				<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Invoice To Date</label> 
				<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
			</div>
			<!-- 
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Invoice No</label> 
				<input class="form-control searchcontrol" type="text" id="invoiceNo" placeholder="Invoice" name="invoiceNo">
			</div>
			 -->
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Profilt & Loss</label> <select id="profitLoss" name="profitLoss" class="form-control ">
					<c:forEach items="${profitLossSigns}" var="sign">
						<option <c:if test="${sign==selectedSign}"> Selected </c:if> value="${sign}">${sign}</option>
					</c:forEach>
				</select>
			</div>
			<c:if test="${sessionScope.isAdmin == true}">
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Company Product</label> 
				<select id="productType" name="productType" class="form-control" onchange="getCompanyProduct(this);">
					<%-- <option <c:if test="${selectedProduct=='ALL'}"> Selected </c:if> value="ALL">All</option> --%>
					<option <c:if test="${selectedProduct=='REWARDTHEFAN'}"> Selected </c:if> value="REWARDTHEFAN">Reward The Fan</option>
					<option <c:if test="${selectedProduct=='RTW'}"> Selected </c:if> value="RTW">RTW</option>
					<option <c:if test="${selectedProduct=='RTW2'}"> Selected </c:if> value="RTW2">RTW2</option>
					<option <c:if test="${selectedProduct=='SEATGEEK'}"> Selected </c:if> value="SEATGEEK">SEATGEEK</option>
				</select>
			</div>
			</c:if>
	</div>
	<div class="clearfix"></div>
	<div class="col-lg-12 filters-div">
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
			<label for="name" class="control-label">Select Artist or Venue</label> 
			<input class="form-control searchcontrol" type="text" id="artistVenue" placeholder="Artist/Venue"> 
			<input type="hidden" id="artistId" name="artistId" value="${artistId}" /> 
			<input type="hidden"  id="venueId" name="venueId" value="${venueId}" /> 
			<input type="hidden" id="artistVenueName" name="artistVenueName" value="${artistVenueName}" /> 
			<label for="name" id="selectedItem">${artistVenueName}</label> <a href="javascript:resetItem()" id="resetLink">remove</a>
		</div>

		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
			<label for="name" class="control-label">Events</label> <select id="eventSelect" name="eventSelect" class="form-control">
				<option value="">--Select--</option>
				<c:forEach var="event" items="${events}">
					<option value="${event.eventId}" <c:if test="${event.eventId == eventId}"> selected </c:if>>${event.eventName} - ${event.eventDateStr} ${event.eventTimeStr} ${event.building}</option>
				</c:forEach>
			</select>
		</div>
		<!-- 
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
			<label for="name" class="control-label">External Order Id</label>
			 <input class="form-control searchcontrol" type="text" id="externalOrderId" name="externalOrderId"  placeholder="External order id"> 
		</div>
		
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
			<label for="name" class="control-label">Order No</label>
			 <input class="form-control searchcontrol" type="text" id="orderId" name="orderId"  placeholder="Order"> 
		</div>
		 -->
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
			<label>&nbsp</label><input type="hidden" name="brokerId" id="brokerId" value="${brokerId}">
			<button type="button" id="searchClosedOrderBtn" class="btn btn-primary" onclick="submitSearchForm()">search</button>
		</div>
	</div>
	</form:form>
</div>
<div id="shipmentPendingsDiv">
	<div class="full-width">
		<section class="panel">
		<ul class="nav nav-tabs">
			<!--<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="openOrdersTab" href="#openOrders">Open Orders</a></li>
			<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="shipmentPendingsTab" href="#shipmentPendings">Shipment Pendings</a>-->
			<li class="active"><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="closedOrdersTab" href="#closedOrders">Closed Orders</a></li>
			<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="passedOrdersTab" href="#passedOrders">Past Orders</a></li>
		</ul>
		</section>
	</div>
	<div class="full-width">
		<div class="tab-content">
			<!--<div id="openOrders"></div>
			<div id="shipmentPendings"></div>-->
			<div id="closedOrders" class="tab-pane active">
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Closed Orders</label> 
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="closedOrders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="closedOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
			</div>
			<div id="passedOrders"></div>
		</div>
	</div>
</div>

<!-- popup View Related Invoice/Purchase Orders -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-related-invoices-po">View Related Invoice/Purchase Orders</button> -->
	<div id="view-related-invoices-po" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">View Related Invoice/Purchase Orders - Invoice No : <span id="invoiceId_Hdr_RelatedInvoice" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Accounting
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Purchase Orders</li>
							</ol>
						</div>
					</div>
					
					<div id="relInvPO_successDiv" class="alert alert-success fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="relInvPO_successMsg"></span></strong>
					</div>					
					<div id="relInvPO_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="relInvPO_errorMsg"></span></strong>
					</div>
					<br />

					<div class="full-width mt-10" style="position: relative" id="purchaseOrder">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Purchase Order</label> <span id="openOrder_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"></span>
							</div>
							<div id="po_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="po_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					
					<div class="full-width mt-20 mb-20" style="position: relative" id="invoice">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Invoices</label> <span id="openOrder_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"></span>
							</div>
							<div id="invoice_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="invoice_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup View Related Invoice/Purchase Orders -->

<!-- popup View/Modify Notes -->
	<%@include file="body-order-view-modify-notes.jsp"%>
<!-- End popup View/Modify Notes -->

<script>
var orderCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	var pagingInfo;
	var closedOrderView;
	var closedOrderGrid;
	var closedOrderData = [];
	var closedorderGridSearchString='';
	var columnFilters = {};
	var sortingString = '';
	var userClosedOrderColumnsStr = '<%=session.getAttribute("closedordergrid")%>';
	var userClosedOrderColumns = [];
	var loadClosedOrderColumns = ["invoiceId","orderId", "eventName", "eventDate", "eventTime", "venue", "venueCity", "venueState", "venueCountry", 
	     "quantity","customerName", "section", "row", "soldPrice", "marketPrice", "availableSectionTixCount", "availableEventTixCount",
		 "totalSoldPrice", "totalMarketPrice", "PLValue", "priceUpdatedCount", "trackingNo", "internalNotes", "brokerId","companyName", "platform"];
	var allClosedOrderColumns = [ orderCheckboxSelector.getColumnDefinition(),
			{
				id : "invoiceId",
				name : "Invoice Id",
				field : "invoiceId",
				width : 80,
				sortable : true
			}, {
				id : "orderId",
				name : "Order Id",
				field : "orderId",
				width : 80,
				sortable : true
			}, {
				id : "invoiceDateTime",
				name : "Invoice Date",
				field : "invoiceDateTime",
				width : 80,
				sortable : true
			}, {
				id : "purchaseOrderId",
				name : "PO Id",
				field : "purchaseOrderId",
				width : 80,
				sortable : true
			}, {
				id : "purchaseOrderDateTime",
				name : "PO Date",
				field : "purchaseOrderDateTime",
				width : 80,
				sortable : true
			}, {
				id : "eventName",
				name : "Event Name",
				field : "eventName",
				width : 80,
				sortable : true
			}, {
				id : "eventDate",
				name : "Event Date",
				field : "eventDate",
				width : 80,
				sortable : true
			}, {
				id : "eventTime",
				name : "Event Time",
				field : "eventTime",
				width : 80,
				sortable : true
			}, {
				id : "venue",
				name : "Venue",
				field : "venue",
				width : 80,
				sortable : true
			}, {
				id : "venueCity",
				name : "City",
				field : "venueCity",
				width : 80,
				sortable : true
			}, {
				id : "venueState",
				name : "State",
				field : "venueState",
				width : 80,
				sortable : true
			}, {
				id : "venueCountry",
				name : "Country",
				field : "venueCountry",
				width : 80,
				sortable : true
			}, {
				id : "productType",
				name : "Product Type",
				field : "productType",
				width : 80,
				sortable : true
			}, {
				id : "internalNotes",
				name : "Internal Notes",
				field : "internalNotes",
				width : 80,
				sortable : true/* ,
				editor:Slick.Editors.LongText */
			}, {
				id : "quantity",
				name : "Quantity",
				field : "quantity",
				width : 80,
				sortable : true
			},{
				id : "customerName",
				name : "Customer Name",
				field : "customerName",
				width : 80,
				sortable : true
			}, {
				id : "section",
				name : "Section",
				field : "section",
				width : 80,
				sortable : true
			}, {
				id : "row",
				name : "Row",
				field : "row",
				sortable : true
			}, {
				id : "soldPrice",
				name : "Wholesale Price",
				field : "soldPrice",
				width : 80,
				sortable : true
			}, {
				id : "marketPrice",
				name : "Market Price/Ticket",
				field : "marketPrice",
				width : 80,
				sortable : true
			}, {
				id : "lastUpdatedPrice",
				name : "Last Updated Price",
				field : "lastUpdatedPrice",
				width : 80,
				sortable : true
			}, {
				id : "availableSectionTixCount",
				name : "Available Section Tix Count",
				field : "availableSectionTixCount",
				width : 80,
				sortable : true
			}, {
				id : "availableEventTixCount",
				name : "Available Event Tix Count",
				field : "availableEventTixCount",
				width : 80,
				sortable : true
			}, {
				id : "totalSoldPrice",
				name : "Total Sold Price",
				field : "totalSoldPrice",
				width : 80,
				sortable : true
			}, {
				id : "totalMarketPrice",
				name : "Total Market Price",
				field : "totalMarketPrice",
				width : 80,
				sortable : true
			}, {
				id : "PLValue",
				name : "Section P/L",
				field : "PLValue",
				width : 80,
				sortable : true
			}, {
				id : "priceUpdatedCount",
				name : "Price Updated Count",
				field : "priceUpdatedCount",
				width : 80,
				sortable : true
			}, {
				id : "shippingMethod",
				name : "Shipping Method",
				field : "shippingMethod",
				width : 80,
				sortable : true
			}, {
				id : "trackingNo",
				name : "Tracking No",
				field : "trackingNo",
				width : 80,
				sortable : true
			}, {
				id : "secondaryOrderType",
				name : "Secondary Order Type",
				field : "secondaryOrderType",
				width : 80,
				sortable : true
			},{
				id : "secondaryOrderId",
				name : "Secondary Order Id",
				field : "secondaryOrderId",
				width : 80,
				sortable : true
			},{
				id : "lastUpdated",
				name : "Last Updated",
				field : "lastUpdated",
				width : 80,
				sortable : true
			},{
				id : "brokerId",
				name : "Broker Id",
				field : "brokerId",
				width : 80,
				sortable : true
			}, {
				id : "companyName",
				name : "Company Name",
				field : "companyName",
				width : 80,
				sortable : true
			}, {
				id : "platform",
				name : "Platform",
				field : "platform",
				width : 80,
				sortable : true
			}, {
				id : "orderType",
				name : "Order Type",
				field : "orderType",
				width : 80,
				sortable : true
			}  ];

	if (userClosedOrderColumnsStr != 'null' && userClosedOrderColumnsStr != '') {
		var columnOrder = userClosedOrderColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allClosedOrderColumns.length; j++) {
				if (columnWidth[0] == allClosedOrderColumns[j].id) {
					userClosedOrderColumns[i] = allClosedOrderColumns[j];
					userClosedOrderColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		var columnOrder = loadClosedOrderColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allClosedOrderColumns.length;j++){
				if(columnWidth == allClosedOrderColumns[j].id){
					userClosedOrderColumns[i] = allClosedOrderColumns[j];
					userClosedOrderColumns[i].width=80;
					break;
				}
			}			
		}
		//userClosedOrderColumns = allClosedOrderColumns;
	}

	var closedOrderOptions = {
		editable : true,
		enableCellNavigation : true,
		asyncEditorLoading : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var closedOrderGridSortcol = "invoiceId";
	var closedOrderGridSortdir = 1;
	var percentCompleteThreshold = 0;
	//var closedOrderGridSearchString = "";

	/* //function for edit functionality
	function viewLinkFormatter(row,cell,value,columnDef,dataContext){
		//the id is so that you can identify the row when the particular button is clicked
		var link = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' onclick='viewAction(" +closedOrderGrid.getDataItem(row).id+","+closedOrderGrid.getDataItem(row).eventId+")'/>";
		return link;
	} */

	function deleteRecordFromclosedOrderGrid(id) {
		closedOrderView.deleteItem(id);
		closedOrderGrid.invalidate();
	}
	/*
	function closedOrderGridFilter(item, args) {
		var x = item["invoiceId"];
		if (args.closedOrderGridSearchString != ""
				&& x.indexOf(args.closedOrderGridSearchString) == -1) {

			if (typeof x === 'string' || x instanceof String) {
				if (x.toLowerCase().indexOf(
						args.closedOrderGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function closedOrderGridComparer(a, b) {
		var x = a[closedOrderGridSortcol], y = b[closedOrderGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	/*
	function closedOrderGridToggleFilterRow() {
		closedOrderGrid
				.setTopPanelVisibility(!closedOrderGrid.getOptions().showTopPanel);
	}

	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#closedOrder_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	*/
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getClosedOrderGridData(pageNo);
	}
	
	function getClosedOrderGridData(pageNo) {
		$('#action').val("search");
		$.ajax({
			url : "${pageContext.request.contextPath}/Deliveries/ClosedOrders.json",
			type : "post",
			dataType: "json",
			data : $("#searchClosedOrders").serialize()+"&pageNo="+pageNo+"&headerFilter="+closedorderGridSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshClosedOrderGrid(jsonData.closedOrders);
				clearAllSelections();
				$('#closedOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserClosedOrderPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	function refreshClosedOrderGrid(jsonData) {
		closedOrderData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (closedOrderData[i] = {});
				d["id"] = i;
				d["closedOrderId"] = data.id;
				d["invoiceId"] = data.invoiceNo;
				d["orderId"] = data.ticketRequestId;
				d["orderId"] = data.orderId;
				d["invoiceDateTime"] = data.invoiceDateStr;
				d["purchaseOrderId"] = data.poId;
				d["purchaseOrderDateTime"] = data.poDateStr;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["venue"] = data.venueName;
				d["venueCity"] = data.venueCity;
				d["venueState"] = data.venueState;
				d["venueCountry"] = data.venueCountry;
				d["productType"] = data.productType;
				d["internalNotes"] = data.internalNotes;
				d["quantity"] = data.soldQty;
				d["customerName"] = data.customerName;
				d["section"] = data.section;
				d["row"] = data.row;
				d["soldPrice"] = data.actualSoldPrice;
				d["marketPrice"] = data.marketPrice;
				d["lastUpdatedPrice"] = data.lastUpdatedPrice;
				d["availableSectionTixCount"] = data.sectionTixQty;
				d["availableEventTixCount"] = data.eventTixQty;
				d["totalSoldPrice"] = data.totalActualSoldPrice;
				d["totalMarketPrice"] = data.totalMarketPrice;
				d["PLValue"] = data.profitAndLoss;
				d["priceUpdatedCount"] = data.priceUpdateCount;
				d["shippingMethod"] = data.shippingMethod;
				d["trackingNo"] = data.trackingNo;
				d["secondaryOrderType"] = data.secondaryOrderType;
				d["secondaryOrderId"] = data.secondaryOrderId;
				d["lastUpdated"] = data.lastUpdateStr;
				d["eventId"] = data.tmatEventId;
				d["brokerId"] = data.brokerId;
				d["companyName"] = data.companyName;
				d["platform"] = data.platform;
				d["orderType"] = data.orderType;
			}
		}

		closedOrderView = new Slick.Data.DataView();
		closedOrderGrid = new Slick.Grid("#closedOrders_grid", closedOrderView,
				userClosedOrderColumns, closedOrderOptions);
		closedOrderGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		closedOrderGrid.setSelectionModel(new Slick.RowSelectionModel());
		closedOrderGrid.registerPlugin(orderCheckboxSelector);
		if(pagingInfo!=null){
			var closedOrderPager = new Slick.Controls.Pager(closedOrderView,
					closedOrderGrid, $("#closedOrder_pager"),pagingInfo);
		}
		var closedOrderColumnpicker = new Slick.Controls.ColumnPicker(
				allClosedOrderColumns, closedOrderGrid, closedOrderOptions);

		// move the filter panel defined in a hidden div into closedOrderGrid top panel
		//$("#closedOrder_inlineFilterPanel").appendTo(closedOrderGrid.getTopPanel()).show();

		closedOrderGrid.onSort.subscribe(function(e, args) {
			closedOrderGridSortcol = args.sortCol.field;			
			if(sortingString.indexOf(closedOrderGridSortcol) < 0){
				closedOrderGridSortdir = 'ASC';
			}else{
				if(closedOrderGridSortdir == 'DESC' ){
					closedOrderGridSortdir = 'ASC';
				}else{
					closedOrderGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+closedOrderGridSortcol+',SORTINGORDER:'+closedOrderGridSortdir+',';
			getClosedOrderGridData(0)
		});
		// wire up model closedOrders to drive the closedOrderGrid
		closedOrderView.onRowCountChanged.subscribe(function(e, args) {
			closedOrderGrid.updateRowCount();
			closedOrderGrid.render();
		});
		closedOrderView.onRowsChanged.subscribe(function(e, args) {
			closedOrderGrid.invalidateRows(args.rows);
			closedOrderGrid.render();
		});
		
		$(closedOrderGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	closedorderGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  closedorderGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getClosedOrderGridData(0);
				}
			  }
		 
		});
		closedOrderGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'productType' && args.column.id != 'totalSoldPrice' && args.column.id != 'totalMarketPrice'){
					if(args.column.id == 'eventTime'){
						$("<input type='text' placeholder='hh:mm a'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else if(args.column.id == 'invoiceDateTime' || args.column.id == 'purchaseOrderDateTime' || args.column.id == 'eventDate' || args.column.id == 'lastUpdated'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}
		});
		closedOrderGrid.init();
		
		/* closedOrderGrid.onCellChange.subscribe(function (e,args) { 
			var temprOpenOrderRwIndex = closedOrderGrid.getSelectedRows();
			var openOrderId;
			var orderNotes; 
			$.each(temprOpenOrderRwIndex, function (index, value) {
				openOrderId = closedOrderGrid.getDataItem(value).closedOrderId;
				orderNotes = closedOrderGrid.getDataItem(value).internalNotes;
			});
			saveOrderNote(openOrderId, orderNotes);
     	}); */
		
		/*
		// wire up the search textbox to apply the filter to the model
		$("#closedOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			closedOrderGridSearchString = this.value;
			updateclosedOrderGridFilter();
		});
		function updateclosedOrderGridFilter() {
			closedOrderDataView.setFilterArgs({
				closedOrderSearchString : closedOrderSearchString
			});
			closedOrderView.refresh();
		}
		*/
		// initialize the model after all the closedOrders have been hooked up
		closedOrderView.beginUpdate();
		closedOrderView.setItems(closedOrderData);
		/*closedOrderView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			closedOrderGridSearchString : closedOrderGridSearchString
		});
		closedOrderView.setFilter(closedOrderGridFilter);*/
		closedOrderView.endUpdate();
		closedOrderView.syncGridSelection(closedOrderGrid, true);
		closedOrderGrid.resizeCanvas();

		closedOrderGrid.onContextMenu.subscribe(function(e) {
			e.preventDefault();
			var cell = closedOrderGrid.getCellFromEvent(e);
			closedOrderGrid.setSelectedRows([ cell.row ]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if (height < $("#contextMenu").height()) {
				height = e.pageY - $("#contextMenu").height();
			} else {
				height = e.pageY;
			}
			if (width < $("#contextMenu").width()) {
				width = e.pageX - $("#contextMenu").width();
			} else {
				width = e.pageX;
			}
			$("#contextMenu").data("row", cell.row).css("top", height).css(
					"left", width).show();
			$("body").one("click", function() {
				$("#contextMenu").hide();
			});
		});
		$("div#divLoading").removeClass('show');

		/* var closedOrderrowIndex;
		closedOrderGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprclosedOrderRowIndex = closedOrderGrid.getSelectedRows([0])[0];
			if (temprclosedOrderRowIndex != closedOrderrowIndex) {
				closedOrderrowIndex = temprclosedOrderRowIndex;
				//getCustomerInfoForInvoice(temprclosedOrderRowIndex);
			}
		}); */
	}

	$("#contextMenu")
			.click(
					function(e) {
						if (!$(e.target).is("li")) {
							return;
						}
						var index = closedOrderGrid.getSelectedRows([ 0 ])[0];
						if (index >= 0) {
							var productType = closedOrderGrid.getDataItem(index).productType;
							var invoiceId = closedOrderGrid.getDataItem(index).invoiceId;
							var purchaseOrderId = closedOrderGrid.getDataItem(index).purchaseOrderId;
							if (productType != null && productType != ''){ 	//&& productType == 'REWARDTHEFAN') {									
								if ($(e.target).attr("data") == 'view invoice po') {
									getRelatedInvoicesPO(invoiceId, productType);
									/*var url = "${pageContext.request.contextPath}/Accounting/ViewRelatedPOInvoices?invoiceId="+ invoiceId +"&productType="+ productType;
									popupCenter(url, "View Related Invoice/PO(s)", "1000","700");*/
								} else if ($(e.target).attr("data") == 'openInvoice') {
									window.open("${pageContext.request.contextPath}/Accounting/Invoices?action=search&invoiceNo="
											+ invoiceId +"&productType="+ productType, "_blank");
								} else if ($(e.target).attr("data") == 'openPO') {
									if(purchaseOrderId != null && purchaseOrderId != '' && purchaseOrderId != 'undefined'){
										window.location = "${pageContext.request.contextPath}/Accounting/ManagePO?poNo="+ purchaseOrderId+"&productType="+ productType;
									}else{
										jAlert("Purchase Order Id is not found.");
									}
								} else if($(e.target).attr("data") == 'view modify notes'){
									var orderStatusId = closedOrderGrid.getDataItem(index).closedOrderId;
									var orderId = closedOrderGrid.getDataItem(index).orderId;
									if(productType == 'REWARDTHEFAN'){
										getOpenOrderNote(orderStatusId, orderId, 'Closed Orders');
									}else{
										jAlert("This option only available for REWARDTHEFAN product orders.");
									}
								}
							} else {
								jAlert("Only Reward the fan product invoices are available to view.");
							}
						} else {
							jAlert("Please select Order to view Invoice.");
						}
					});

	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
	
	//Start View Related Invoices
	function getRelatedInvoicesPO(invoiceId, productType){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetRelatedPOInvoices",
			type : "post",
			data : "invoiceId="+invoiceId+"&productType="+productType,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));				
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Related PO/Invoice(s) Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setRelatedInvoicesPO(jsonData);
					$('#invoiceId_Hdr_RelatedInvoice').text(invoiceId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setRelatedInvoicesPO(jsonData){
		$('#view-related-invoices-po').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#relInvPO_successDiv').hide();
			$('#relInvPO_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#relInvPO_errorDiv').hide();
			$('#relInvPO_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#relInvPO_successDiv').show();
			$('#relInvPO_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#relInvPO_errorDiv').show();
			$('#relInvPO_errorMsg').text(jsonData.errorMessage);
		} */
		relatedPOGridValues(jsonData.poList, jsonData.poPagingInfo);
		relatedInvoiceGridValues(jsonData.ticketList, jsonData.pagingInfo);
	}
	//End View Related Invoices
	
	//Save Internal Notes
	/* function saveOrderNote(openOrderId, orderNote){	
		$.ajax({
			url : "${pageContext.request.contextPath}/Deliveries/UpdateOrderNote",
			type : "post",
			data : "modifyNotesOpenOrderId="+openOrderId+"&modifyNotesOpenOrderNote="+orderNote+"&action=update",
			dataType : "json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					//getClosedOrderGridData(0);
				}
				jAlert(jsonData.msg);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});		
	} */
	
</script>