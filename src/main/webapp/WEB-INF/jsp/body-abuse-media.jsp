<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(abuseMediaGrid != null && abuseMediaGrid != undefined){
			abuseMediaGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${status == 'ACTIVE'}">	
			$('#activeMedia').addClass('active');
			$('#activeMediaTab').addClass('active');
		</c:when>
		<c:when test="${status == 'BLOCKED'}">
			$('#inactiveMedia').addClass('active');
			$('#inactiveMediaTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeMedia1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#inactiveMedia1").click(function(){
		callTabOnChange('BLOCKED');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/AbuseReportedMedia?status="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling</a></li>
			<li><i class="fa fa-laptop"></i>Manage Abuse Media</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeMediaTab" class=""><a id="activeMedia1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeMedia">Abuse reported Media</a></li>
				<li id="inactiveMediaTab" class=""><a id="inactiveMedia1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveMedia">Blocked/Removed Media</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeMedia" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="removeMedia('REMOVE');">Block/Remove Media</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="removeMedia('BLOCK')">Block User</button>
					<!-- <button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteAbuseMedia()">Delete Point Conversion</button> -->
				</div>
				<br />
				<br />
				<div style="position: relative" id="abuseMediaGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Abuse Reported Media</label>
							<div class="pull-right">
								<a href="javascript:abuseMediaExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseMediaResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abuseMedia_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abuseMedia_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="inactiveMedia" class="tab-pane">
			<c:if test="${status =='BLOCKED'}">	
				<div class="full-width full-width-btn mb-20">
					<!-- <button class="btn btn-primary" id="editQBBtn" type="button" onclick="editAbuseMedia()">Edit Reward Config</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteAbuseMedia()">Delete Reward Config</button> -->
				</div>
				<br />
				<br />
				<div style="position: relative" id="abuseMediaGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Blocked/Remove Media</label>
							<div class="pull-right">
								<a href="javascript:abuseMediaExportToExcel('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseMediaResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abuseMedia_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abuseMedia_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Reward Config Bank -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="qBModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Block/Remove Media</h4>
			</div>
			<div class="modal-body full-width">
				<form name="abuseMediaForm" id="abuseMediaForm" method="post">
					<input type="hidden" id="abuseId" name="abuseId" />
					<input type="hidden" id="action" name="action" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label>Reason<span class="required">*</span></label> 
							<textarea id="reason" name="reason" row="10" cols="15" class="form-control input-sm m-bot15" ></textarea>
						</div>		
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qBSaveBtn" type="button" onclick="saveMedia()">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Reward Media Bank end here  -->
<script type="text/javascript">
	
		//Add Reward Comment Bank	
		function removeMedia(action){		
			var tempAbuseMediaRowIndex = abuseMediaGrid.getSelectedRows([0])[0];
			if (tempAbuseMediaRowIndex == null) {
				jAlert("Please select abuse Media to remove.", "info");
				return false;
			}else {
				var abuseMediaId = abuseMediaGrid.getDataItem(tempAbuseMediaRowIndex).abuseMediaId;
				$('#abuseId').val(abuseMediaId);
				$('#reason').val('');
				$('#action').val(action);
				$('#qBModal').modal('show');
			}
		}
		
		function saveMedia(){
			var reason = $('#reason').val();
			if(reason == ''){
				jAlert("Block Reason is Mandatory.");
				return;
			}
			
			var requestUrl = "${pageContext.request.contextPath}/UpdateAbuseMedia.json";
			var dataString = $('#abuseMediaForm').serialize();
			$.ajax({
				url : requestUrl,
				type : "post",
				dataType: "json",
				data: dataString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						$('#qBModal').modal('hide');
						pagingInfo = jsonData.pagingInfo;
						abuseMediaColumnFilters = {};
						refreshAbuseMediaGridValues(jsonData.abuseDatas);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	
	function pagingControl(move, id) {
		if(id == 'abuseMedia_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getAbuseMediaGridData(pageNo);
		}
	}
	
	//Reward Media Bank Grid
	
	function getAbuseMediaGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/AbuseReportedMedia.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+abuseMediaSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshAbuseMediaGridValues(jsonData.abuseDatas);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function abuseMediaExportToExcel(status){
		var appendData = "headerFilter="+abuseMediaSearchString+"&status="+status;
	    var url =apiServerUrl+"abuseMediaExport?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function abuseMediaResetFilters(){
		abuseMediaSearchString='';
		abuseMediaColumnFilters = {};
		getAbuseMediaGridData(0);
	}
	
	var abuseMediaCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var abuseMediaDataView;
	var abuseMediaGrid;
	var abuseMediaData = [];
	var abuseMediaGridPager;
	var abuseMediaSearchString='';
	var abuseMediaColumnFilters = {};
	var userAbuseMediaColumnsStr = '<%=session.getAttribute("abuseMediaGrid")%>';

	var userAbuseMediaColumns = [];
	var allAbuseMediaColumns = [
			 {
				id : "reportedByUserId",
				field : "reportedByUserId",
				name : "Reported By User Id",
				width : 80,
				sortable : true
			},{
				id : "reportedByEmail",
				field : "reportedByEmail",
				name : "Reported By Email",
				width : 80,
				sortable : true
			},{
				id : "reportedByPhone",
				field : "reportedByPhone",
				name : "Reported By Phone",
				width : 80,
				sortable : true
			},{
				id : "abuseType",
				field : "abuseType",
				name : "Reported Abuse Type",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Reported Date",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "Media User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Media Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Media Phone",
				width : 80,
				sortable : true
			} ,{
				id : "title",
				field : "title",
				name : "Media Title",
				width : 80,
				sortable : true
			} ,{
				id : "description",
				field : "description",
				name : "Media Description",
				width : 80,
				sortable : true
			} ,{
				id : "mediaUrl",
				field : "mediaUrl",
				name : "Media Preview",
				width : 80,
				formatter: previewMediaFormatter
			} ,{
				id : "blockStatus",
				field : "blockStatus",
				name : "Blocked/Removed?",
				width : 80,
				sortable : true
			} ,{
				id : "blockDate",
				field : "blockDate",
				name : "Block/Removed Date",
				width : 80,
				sortable : true
			} ,{
				id : "blockBy",
				field : "blockBy",
				name : "Blocked/Removed By",
				width : 80,
				sortable : true
			} ];

	if (userAbuseMediaColumnsStr != 'null' && userAbuseMediaColumnsStr != '') {
		columnOrder = userAbuseMediaColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allAbuseMediaColumns.length; j++) {
				if (columnWidth[0] == allAbuseMediaColumns[j].id) {
					userAbuseMediaColumns[i] = allAbuseMediaColumns[j];
					userAbuseMediaColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAbuseMediaColumns = allAbuseMediaColumns;
	}
	
	function previewMediaFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.mediaUrl != null &&  dataContext.mediaUrl != 'null' && dataContext.mediaUrl != ''){
			var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.mediaUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>NO MEDIA</label>";
			return button;
		}
		
	}
	
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var abuseMediaOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var abuseMediaGridSortcol = "abuseMediaId";
	var abuseMediaGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function abuseMediaGridComparer(a, b) {
		var x = a[abuseMediaGridSortcol], y = b[abuseMediaGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshAbuseMediaGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		abuseMediaData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (abuseMediaData[i] = {});
				d["id"] = i;
				d["abuseMediaId"] = data.id;
				d["mediaId"] = data.mediaId;
				d["customerId"] = data.customerId;
				d["createdDate"] = data.createdDateStr;
				d["reportedByUserId"] = data.reportedByUserId;
				d["reportedByEmail"] = data.reportedByEmail;
				d["reportedByPhone"] = data.reportedByPhone;
				d["abuseType"] = data.abuseType;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["title"] = data.title;
				d["mediaUrl"] = data.mediaUrl;
				d["description"] = data.description;
				d["blockStatus"] = data.blockStatus;
				if(data.blockStatus == true || data.blockStatus == 'true'){
					d["blockStatus"] = "BLOCKED";
					d["blockDate"] = data.blockDateStr;
					d["blockBy"] = data.blockBy;
				}else if(data.hideStatus == true || data.hideStatus == 'true'){
					d["blockStatus"] = "REMOVED";
					d["blockDate"] = data.hideDateStr;
					d["blockBy"] = data.hide_by;
				}else{
					d["blockStatus"] = "N/A";
					d["blockDate"] = "N/A";
					d["blockBy"] = "N/A";
				}
				
			}
		}

		abuseMediaDataView = new Slick.Data.DataView();
		abuseMediaGrid = new Slick.Grid("#abuseMedia_grid", abuseMediaDataView,
				userAbuseMediaColumns, abuseMediaOptions);
		abuseMediaGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		abuseMediaGrid.setSelectionModel(new Slick.RowSelectionModel());
		abuseMediaGrid.registerPlugin(abuseMediaCheckboxSelector);
		
		abuseMediaGridPager = new Slick.Controls.Pager(abuseMediaDataView,
					abuseMediaGrid, $("#abuseMedia_pager"),
					pagingInfo);
		/* var abuseMediaGridColumnpicker = new Slick.Controls.ColumnPicker(
				allAbuseMediaColumns, abuseMediaGrid, abuseMediaOptions); */
		
		abuseMediaGrid.onSort.subscribe(function(e, args) {
			abuseMediaGridSortdir = args.sortAsc ? 1 : -1;
			abuseMediaGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				abuseMediaDataView.fastSort(abuseMediaGridSortcol, args.sortAsc);
			} else {
				abuseMediaDataView.sort(abuseMediaGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the abuseMediaGrid
		abuseMediaDataView.onRowCountChanged.subscribe(function(e, args) {
			abuseMediaGrid.updateRowCount();
			abuseMediaGrid.render();
		});
		abuseMediaDataView.onRowsChanged.subscribe(function(e, args) {
			abuseMediaGrid.invalidateRows(args.rows);
			abuseMediaGrid.render();
		});
		$(abuseMediaGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							abuseMediaSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								abuseMediaColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in abuseMediaColumnFilters) {
										if (columnId !== undefined
												&& abuseMediaColumnFilters[columnId] !== "") {
											abuseMediaSearchString += columnId
													+ ":"
													+ abuseMediaColumnFilters[columnId]
													+ ",";
										}
									}
									getAbuseMediaGridData(0);
								}
							}

						});
		abuseMediaGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'mediaUrl' && args.column.id != 'imageUrl' && args.column.id != 'blockStatus'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(abuseMediaColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(abuseMediaColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		abuseMediaGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		abuseMediaDataView.beginUpdate();
		abuseMediaDataView.setItems(abuseMediaData);
		//abuseMediaDataView.setFilter(filter);
		abuseMediaDataView.endUpdate();
		abuseMediaDataView.syncGridSelection(abuseMediaGrid, true);
		abuseMediaGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAbuseMediaPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = abuseMediaGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('abuseMediaGrid', colStr);
	}
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		refreshAbuseMediaGridValues(${abuseDatas});
		$('#abuseMedia_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAbuseMediaPreference()'>");
		
		enableMenu();
	};
		
</script>