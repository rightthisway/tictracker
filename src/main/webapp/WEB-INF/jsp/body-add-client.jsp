<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">
	
</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Clients & Brokers & Customers</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Manage Details</a></li>
						<li><i class="fa fa-laptop"></i>Add Client</li>						  	
					</ol>
					
				</div>
</div>


<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">
				<i class="fa fa-laptop"></i> Add Customer
			</h3>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<section class="panel"> <header class="panel-heading">
				Customer Info </header>
				<form class="form-validate form-horizontal" id="loginForm"
					method="post"
					action="${pageContext.request.contextPath}/Admin/AddUser">
					<div class="panel-body">
						<div class="form">
							<%-- <c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if> --%>

							<!-- <input class="form-control" id="action" value="action" name="action" type="hidden" /> -->

							<div class="form-group ">
								<div style="float: left;">
									<label for="cname" class="control-label col-lg-2">Type
										<span class="required">*</span> </label>
									<div class="col-lg-10">
										<select class="form-control input-sm m-bot15">
											<option>Phone Customer</option>
											<option>Web Customer</option>
										</select>
									</div>
								</div>
								<div>
									<label for="cname" class="control-label col-lg-2">Customer
										status <span class="required">*</span> </label>
									<div class="col-lg-10">
										<select style="width: 133px;"
											class="form-control input-sm m-bot15">
											<option>Gold</option>
											<option>Silver</option>
											<option>Platinum</option>
										</select>
									</div>

								</div>
								<div style="float: left;">
									<label for="cname" class="control-label col-lg-2">
										Signup Type <span class="required">*</span> </label>
									<div class="col-lg-10">
										<input class="form-control" id="signupType" name="signupType"
											type="text" value="" />
									</div>
								</div>
								<div>
									<label for="cname" class="control-label col-lg-2"> Name
										<span class="required">*</span> </label>
									<div class="col-lg-10">
										<input class="form-control" id="name" name="customerName"
											type="text" />
									</div>
								</div>
								<div style="float: left;">
									<label for="cemail" class="control-label col-lg-2">E-Mail
										<span class="required">*</span> </label>
									<div class="col-lg-10">
										<input class="form-control " id="email" type="email"
											name="email" />
									</div>
								</div>
								<div>
									<label for="cname" class="control-label col-lg-2">Phone
										<span class="required">*</span> </label>
									<div class="col-lg-10">
										<input class="form-control" id="phone" name="phone"
											type="text" />
									</div>
								</div>
								<div style="float: left;">
									<label for="cname" class="control-label col-lg-2">Other
										Phone </label>
									<div class="col-lg-10">
										<input class="form-control" id="phone" name="phone"
											type="text" />
									</div>
								</div>
								<div>
									<label for="ccomment" class="control-label col-lg-2">Ext</label>
									<div class="col-lg-10">
										<input class="form-control " id="fedexNo" name="fedexNo" />
									</div>
								</div>
							</div>
						</div>

					</div>
					<header class="panel-heading"> Billing Address </header>
					<div class="panel-body">
						<div class="form">
							<div class="form-group ">
								<div style="float: left;">
									<label for="cname" class="control-label col-lg-2">First
										Name <!-- <input type="checkbox" id="internationalPhone" name="internationalPhone" />&nbsp&nbspMain<span class="required">*</span> -->
									</label>
									<div class="col-lg-10">
										<input class="form-control" id="firstName" name=""
											firstName"" type="text" />
									</div>

								</div>
								<div>
									<label for="cname" class="control-label col-lg-2">Last
										Name </label>
									<div class="col-lg-10">
										<input class="form-control" id="lastName" name="lastName"
											type="text" />
									</div>
								</div>
								<div style="float: left;">
									<label for="cemail" class="control-label col-lg-2">Street1
									</label>
									<div class="col-lg-10">
										<input class="form-control " id="fax" type="text" name="fax" />
									</div>
								</div>
								<div>
									<label for="cemail" class="control-label col-lg-2">Street2
									</label>
									<div class="col-lg-10">
										<input class="form-control " id="fax" type="text" name="fax" />
									</div>
								</div>
								<div style="float: left;">
									<label for="cemail" class="control-label col-lg-2">Zip
										Code </label>
									<div class="col-lg-10">
										<input class="form-control " id="fax" type="text" name="fax" />
									</div>
								</div>
								<div>
									<label for="cemail" class="control-label col-lg-2">City
									</label>
									<div class="col-lg-10">
										<input class="form-control " id="fax" type="text" name="fax" />
									</div>
								</div>
								<div style="float: left;">
									<label for="cemail" class="control-label col-lg-2">State
									</label>
									<div class="col-lg-10">
										<input class="form-control " id="fax" type="text" name="fax" />
									</div>
								</div>
								<div>
									<label for="cemail" class="control-label col-lg-2">Country
									</label>
									<div class="col-lg-10">
										<input class="form-control " id="fax" type="text" name="fax" />
									</div>
								</div>
								
							</div>
										<div class="col-lg-offset-2 col-lg-10">
											<button class="btn btn-primary" type="button">Save</button>
											<button class="btn btn-primary" type="button">Cancel</button>
										</div>
									
						</div>

					</div>
				</form>
				</section>
			</div>
		</div>
	</div>