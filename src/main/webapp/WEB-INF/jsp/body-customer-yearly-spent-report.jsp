<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
});
</script>


<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Reports</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="/Reports/Home">Reports</a></li>
						<li><i class="fa fa-laptop"></i>RTF Customer Spent </li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
</div>

<div class="row">
<form name="customerYearlySpentForm" id="customerYearlySpentForm" method="post" action="${pageContext.request.contextPath}/Reports/DownloadRTFCustomerSpentReport">
		
	<div class="full-width">
		<div class="form-group full-width">
			<label for="imageText" class="col-lg-2 col-md-3 col-xs-7 col-lg-offset-2 control-label" style="text-align: right"> Year </label>
			<div class="col-lg-4 col-md-4 col-xs-7">
				<select multiple class="form-control" name="selectedYear" id="selectedYear">						
					<!-- <option value="ALL">ALL</option> -->
					<jsp:useBean id="currentDate" class="java.util.Date" />
					<fmt:formatDate var="year" value="${currentDate}" pattern="yyyy" />
					<c:forEach var="years" begin="2017" end="${year}" >
						<option value="${years}">${years}</option>
					</c:forEach>					
				</select>
			</div>
		</div>
	</div>
	<div class="full-width">
		<div class="form-group full-width">
			<label for="imageText" class="col-lg-2 col-md-3 col-xs-7 col-lg-offset-2 control-label" style="text-align: right"> Select Orders </label>
			<div class="col-lg-4 col-md-4 col-xs-7">
				<select id="selectedOrders" name="selectedOrders" class="form-control" style="">
					<option value="ALL">ALL</option>
					<option value="PHONE_ORDERS">Phone Orders</option>
					<option value="WEB/APP_ORDERS">Web Orders</option>
				</select>				
			</div>
		</div>
	</div>
    <div class="full-width">
		<div class="form-group full-width">
			<div class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3"></div>
			<div class="col-lg-4 col-md-4 col-xs-7">
				<button type="button" id="searchSeatGeekBtn" class="btn btn-primary" onclick="downloadRTFCustomerSpentReport();" style="margin-top:19px;">Download Report</button>
			</div>
		</div>
	</div>
 </form>
</div>

<script>
function downloadRTFCustomerSpentReport(){
	var selYears = [];
	var years = '';
	
	$('#selectedYear :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selYears[i] = $(selected).val();
		}
	});
	for ( var i = 0; i < selYears.length; i++) {
		if(selYears.length > 1){
			years = years + selYears[i] + ',';
		}else{
			years = years + selYears[i];
		}
	}
	
	/*var selectedOrder = [];
	var orders = '';
	 $('#selectedOrders :selected').each(function(i, selected) {
		if (!$(selected).val() == '') {
			selectedOrder[i] = $(selected).val();
		}
	});
	for ( var i = 0; i < selectedOrder.length; i++) {
		orders = orders + selectedOrder[i] + ',';
	} */
	if(years == '' || years == null){
		jAlert("Please select year(s) to Download Report");		
	}else{
		//$("#customerYearlySpentForm").attr('action','${pageContext.request.contextPath}/Reports/DownloadRTFCustomerSpentReport?selectedYears='+years);
		$("#customerYearlySpentForm").attr('action', apiServerUrl+'Reports/DownloadRTFCustomerSpentReport?selectedYears='+years);
		$("#customerYearlySpentForm").submit();
	}
}
</script>