<!DOCTYPE HTML>
<html> 
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Manage Customer Media</title>
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.columnpicker1.css"
	type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
	src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/app/getCityStateCountry.js"></script>
<style>
.cell-title {
	font-weight: bold;
}

.slick-viewport {
	overflow-x: hidden !important;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

.slick-viewport {
	overflow-x: hidden !important;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$("div#divLoading").addClass('show');
	
	var todaysDate = new Date(); // Gets today's date
		
	// Max date attribute is in "MM/DD/YYYY".
	var year = todaysDate.getFullYear();                        // YYYY
	var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);  // MM
	var day = ("0" + todaysDate.getDate()).slice(-2);           // DD
	
	var maxDate = month + "/" + day + "/" + year; 
	
	 $('#fromDate').datepicker({
	    format: "mm/dd/yyyy",
		endDate: maxDate, //todayDate
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		endDate: maxDate, //todayDate
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  grid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchSharingBtn').click();
		    return false;  
		  }
	});
		
});
	
function saveUserDiscountCodeTrackPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = grid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('grid',colStr);
}

function resetFilters(){
	userSearchString='';
	columnFilters = {};
	getUserGridData(0);
}

function exportToExcel(){
	var appendData = "fromDate="+$('#fromDate').val()+"&fromTime="+$('#fromTime').val()+"&toDate="+$('#toDate').val()+"&toTime="+$('#toTime').val();
	appendData += "&headerFilter="+usersSearchString;
    //var url = "${pageContext.request.contextPath}/Client/TrackSharingDiscountCodeExportToExcel?"+appendData;
	var url = apiServerUrl+"TrackSharingDiscountCodeExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function submitSearchForm(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var date1 = new Date(fromDate);
	var date2 = new Date(toDate);
	userSearchString='';
	columnFilters = {};
	if(fromDate ==''&& toDate ==''){
		getUserGridData(0);
	}
	else{
		if(date1<= date2){
			getUserGridData(0);
		}else{
			jAlert("Please select From Date less than To Date.", "info");
			return false;
		}
	}
		
}
</script>

<style>
input {
	color: black !important;
}
</style>
</head>
<body>

	<div class="row">
		<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
		<div class="col-lg-12">
			<h3 class="page-header">
				<i class="fa fa-laptop"></i> MEDIA
			</h3>
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="#">Customer Media</a></li>
				<li><i class="fa fa-laptop"></i>Manage Customer Media</li>
			</ol>

		</div>
	</div>

	<div class="row">
		<%-- <div class="col-xs-12 filters-div">
			<form:form role="form" id="pollingCustomerForm" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/ManageCustomerMedia">			
				
				<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<label for="name" class="control-label">Customer Name/User ID/Email</label> 
					<input class="form-control searchcontrol" type="text" id="name" name="name" value="${name}">					
				</div>
				<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
					<label for="fromDate" class="control-label">From Date</label> 
					<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">					
				</div>
				<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
					<label for="toDate" class="control-label">To Date</label> 
					<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
				</div>
				
				<!-- <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4" >
					<label for="videoType" class="control-label">Video Type</label> 
					<select class="form-control" name="videoType" id="videoType">		
						<option value="ALL">ALL</option>				
						<option value="WINNER">WINNER</option>
						<option value="GENERAL UPLOAD">GENERAL UPLOAD</option>
					</select>					
				</div> -->
				 
				 <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4" >
					<label for="playType" class="control-label">Play Type</label> 
					<select class="form-control" name="playType" id="playType">	
						<option value=""></option>
						<option value="NOTPLAYED">NOT PLAYED</option>					
						<option value="PLAYED">PLAYED</option>
					</select>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<button type="button" id="searchSharingBtn" class="btn btn-primary" onclick="submitSearchForm();" 
					style="width:100px;margin-top:19px;">Search</button>
				</div>
			</form:form>
		</div> --%>
	</div>
	<div class="full-width mb-20 full-width-btn" id="actionButton">
		<!-- <button type="button" class="btn btn-primary" onclick="openPublishMediaDialog()">Publish to RTF Media</button> -->
		<!-- <button type="button" class="btn btn-primary" onclick="markAsPlayed()">Mark as Played</button>
		<button type="button" class="btn btn-primary" onclick="downloadNow()">Download Selected</button> -->
		<button type="button" class="btn btn-primary" onclick="deleteCustomerVideos()">Delete Selected</button>
	</div>
	<div style="position: relative">
		<div style="width: 100%;">
			<div class="table-responsive grid-table mt-20">
				<div class="grid-header full-width">
					<label>Customer Videos</label>
					<div class="pull-right">
						<a href="javascript:exportToExcel()" name="Export to Excel" style="float: right; margin-right: 10px;">Export to Excel</a> 
						<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
					</div>
				</div>
				<div id="myGrid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
				<div id="pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
	</div>
	
	
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="publishVideoModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Publish Customer Media</h4>
			</div>
			<div class="modal-body full-width">
				<form name="pollingVideoForm" id="pollingVideoForm" method="post" enctype="multipart/form-data">
					<input type="hidden" id="custVideoId" name="custVideoId" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label><strong>Category</strong><span class="required">*</span>
							</label> 
							<select name="category" id="category" class="form-control" >
								<c:forEach items="${category}" var="cat">
									<option value="${cat.id}" >${cat.categoryName}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="publishMediaAjaxCall()">Publish</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
 
var customerVideoCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var pagingInfo;
	var dataView;
	var grid;
	var userData = [];
	var userSearchString='';
	var columnFilters = {};
	var sortingString='';
	var userAdminColumnsStr = '<%=session.getAttribute("brokergrid")%>';
	var userAdminColumns =[];
	var allAdminColumns = [ customerVideoCheckboxSelector.getColumnDefinition(),
	  {
		id : "videoId",
		name : "Video ID",
		field : "videoId",
		sortable : true
	},{
		id : "userId",
		name : "Customer ID",
		field : "userId",
		sortable : true
	}, {
		id : "customerName",
		name : "Customer Name",
		field : "customerName",
		sortable : true
	}, {
		id : "phone",
		name : "Phone",
		field : "phone",
		sortable : true
	}, {
		id : "fileName",
		name : "File Name",
		field : "fileName",
		sortable : true
	}, {
		id : "uploadDate",
		name : "Upload Date",
		field : "uploadDate",
		sortable : true
	}, {
		id : "viewMedia",
		field : "viewMedia",
		name : "View Media",
		sortable : true,
		formatter : viewMediaFormatter
	}/* , {
		id : "published",
		field : "published",
		name : "Published",
		sortable : true,
	} , {
		id : "status",
		field : "status",
		name : "Played",
		sortable : true,
		formatter : markAsPlayedFormatter
	}  */ ]; 

	if(userAdminColumnsStr!='null' && userAdminColumnsStr!=''){
		var columnOrder = userAdminColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allAdminColumns.length;j++){
				if(columnWidth[0] == allAdminColumns[j].id){
					userAdminColumns[i] =  allAdminColumns[j];
					userAdminColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userAdminColumns = allAdminColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "userName";
	var sortdir = 1;
	var percentCompleteThreshold = 0;

	function viewMediaFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
		return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	function deleteRecordFromGrid(id) {
		dataView.deleteItem(id);
		grid.invalidate();
	}
	/*
	function myFilter(item, args) {
		var x= item["firstName"];
		if (args.searchString != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function toggleFilterRow() {
		grid.setTopPanelVisibility(!grid.getOptions().showTopPanel);
	}
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getUserGridData(pageNo);
	}
	
	function getUserGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageCustomerMedia.json",
			type : "post",
			dataType: "json",
			data : $('#pollingCustomerForm').serialize()+"&pageNo="+pageNo+"&headerFilter="+userSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pollingVideosPagingInfo;
				refreshUserGridValues(jsonData.pollingVideos);
				clearAllSelections();
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserDiscountCodeTrackPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshUserGridValues(jsonData){
		 $("div#divLoading").addClass('show');
		 userData = [];
		 if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (userData[i] = {});
				
				d["id"] = data.id;
				d["videoId"] = data.id;
				d["userId"] = data.customerId;
				d["customerName"] = data.customerName;
				d["phone"] = data.phoneNo;
				d["videoUrl"] = data.videoUrl;
				d["fileName"] = data.dispFileName;
				d["uploadDate"] = data.uploadDateStr;
				d["markAsPlayed"] = data.status;
				d["published"] ="NO";
				if(data.isPublished == true || data.isPublished == 'true'){
					d["published"] ="YES";	
				}
				
			}
		}
		
		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#myGrid", dataView, userAdminColumns, options);
		grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		var cols = grid.getColumns();
		var colTest = [];
		for ( var c = 0; c < cols.length; c++) {
			//if(cols[c].name!='Title' && cols[c].name!='Start') {
			colTest.push(cols[c]);
			// }  
			grid.setColumns(colTest);
		}
		grid.invalidate();
		grid.setSelectionModel(new Slick.RowSelectionModel());
		grid.registerPlugin(customerVideoCheckboxSelector);
		if(pagingInfo != null){
			var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"),pagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(allAdminColumns, grid,
				options);

		// move the filter panel defined in a hidden div into grid top panel
		//$("#inlineFilterPanel").appendTo(grid.getTopPanel()).show();

		grid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < dataView.getLength(); i++) {
				rows.push(i);
			}
			grid.setSelectedRows(rows);
			e.preventDefault();
		});
		
		grid.onSort.subscribe(function(e, args) {
			sortCol = args.sortCol.field;
			if(sortingString.indexOf(sortCol) < 0){
				sortdir = 'ASC';
			}else{
				if(sortdir == 'DESC' ){
					sortdir = 'ASC';
				}else{
					sortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+sortCol+',SORTINGORDER:'+sortdir+',';
			getUserGridData(0);
		});
		
		// wire up model events to drive the grid
		dataView.onRowCountChanged.subscribe(function(e, args) {
			grid.updateRowCount();
			grid.render();
		});
		dataView.onRowsChanged.subscribe(function(e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});
		$(grid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	userSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  userSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getUserGridData(0);
				}
			  }
		 
		});
		grid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'delCol' && args.column.id != 'role' && args.column.id != 'editCol' && args.column.id != 'downCol'  && args.column.id != 'audit'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}/* 
				
				if(args.column.id == 'downCol' ){
					$("<input type='checkbox' id='downCol' onclick='checkAllDownloadRows();' >")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				} */
				if(args.column.id == 'status'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				} 
			}
			
		});
		grid.init();
			
		//var h_runfilters = null;
		// wire up the slider to apply the filter to the model
		/*$("#pcSlider,#pcSlider2").slider({
		  "range": "min",
		  "slide": function (event, ui) {
		    Slick.GlobalEditorLock.cancelCurrentEdit();
		    if (percentCompleteThreshold != ui.value) {
		      window.clearTimeout(h_runfilters);
		      h_runfilters = window.setTimeout(updateFilter, 10);
		      percentCompleteThreshold = ui.value;
		    }
		  }
		});*/
		/*
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			dataView.setFilterArgs({
				searchString : searchString
			});
			dataView.refresh();
		}
		*/
		/*$("#btnSelectRows").click(function () {
		  if (!Slick.GlobalEditorLock.commitCurrentEdit()) {
		    return;
		  }
		  var rows = [];
		  for (var i = 0; i < 10 && i < dataView.getLength(); i++) {
		    rows.push(i);
		  }
		  grid.setSelectedRows(rows);
		});*/
		// initialize the model after all the events have been hooked up
		dataView.beginUpdate();
		dataView.setItems(userData);
		/*dataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		dataView.setFilter(myFilter);*/
		dataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		dataView.syncGridSelection(grid, true);
		$("#gridContainer").resizable();
		grid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}

	
	/* function checkAllDownloadRows() {
		//downCol
		var rows = [];
		for (var i = 0; i < grid.getDataLength(); i++) {
		    rows.push(i);
		}
		grid.setSelectedRows(rows);
		console.log(rows);
		var temprartistRowIndex = grid.getSelectedRows();
		$.each(temprartistRowIndex, function (index, value) {
			var id = grid.getDataItem(value).id;
			console.log(grid.getDataItem(value).id);
			$("#downloadNow_"+id).attr("checked","checked");
		});
		 
	} */   
	
	function checkAllEditRows() {
		//editCol
		var rows = [];
		for (var i = 0; i <= grid.getDataLength(); i++) {
		    rows.push(i);
		}
		grid.setSelectedRows(rows);
		var temprartistRowIndex = grid.getSelectedRows();
		$.each(temprartistRowIndex, function (index, value) {
			if(null != grid.getDataItem(value)){
				console.log(grid.getDataItem(value).id);
				$("#markAsPlayed_"+grid.getDataItem(value).id).attr("checked","true");
			}
		});
	}
	
	var exts = ['mp4','m4a','fmp4','webm','amr','wma','wav','flv','avi','mov','wmv','mpeg-ts','mpeg-ps'];
	
	function getSelectedVideoGridId(isPublish) {
		var temprartistRowIndex = grid.getSelectedRows();
		
		var videoIdStr='';
		$.each(temprartistRowIndex, function (index, value) {
			if(null != grid.getDataItem(value)){
				var publish = grid.getDataItem(value).published;
				var vidURL = grid.getDataItem(value).videoUrl;
				if(isPublish == true){
					if(publish=="YES"){
						jAlert("Some media from selected list is already published.")
						return;
					}
					var get_ext = vidURL.split('.');
					get_ext = get_ext.reverse();
					if ($.inArray ( get_ext[0].toLowerCase(), exts ) < 0 ){
						jAlert("Image files are not allowed to publish on RTF Media, some of selected records contains image file.");
						return;
					}
				}
				videoIdStr += ','+grid.getDataItem(value).id;
			}
			
		});
		
		if(videoIdStr != null && videoIdStr!='') {
			videoIdStr = videoIdStr.substring(1, videoIdStr.length);
			 return videoIdStr;
		}
	}
	 
	 function downloadNow() {
		 var videoIds  = getSelectedVideoGridId(false);
			if(videoIds==undefined){
				  jAlert("Please select file to download.","Info");
				  return false;
			}
			var res = videoIds.split(',');
			if(res.length>1){
				  jAlert("Please select only one file to download","Info");
				  return false;
			}	  
		window.location.href = "${pageContext.request.contextPath}/DownloadCustomerVideoMedia/?"+"action=update&videoIds="+ videoIds;
		/* if(videoIds=='' || videoIds.length==0){
			jAlert("Please Select video to download.");
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/DownloadCustomerVideoMedia",
				type : "post",
				data : "action=update&videoIds="+ videoIds,
				success : function(res){
					jAlert("Selected Video's are downloaded.");
					grid.setSelectedRows([]);
					clearAllSelections();
					getUserGridData(0);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		} */
		
	}
	 
	 function deleteCustomerVideos() {
		 var videoIds  = getSelectedVideoGridId(false);
		if(videoIds=='' || videoIds.length==0){
			jAlert("Please Select video to deleted.");
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/ManagePollingCustomerVideo",
				type : "post",
				data : "action=delete&videoIds="+ videoIds,
				success : function(res){
					jAlert("Selected Video's are deleted.");
					grid.setSelectedRows([]);
					clearAllSelections();
					getUserGridData(0);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		} 
		
	}
	function markAsPlayed() {
		var videoIds  = getSelectedVideoGridId(false);
		if(videoIds=='' || videoIds.length==0){
			jAlert("Please Select video to update.");
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/ManagePollingCustomerVideo",
				type : "post",
				data : "action=update&videoIds="+ videoIds,
				success : function(res){
					jAlert("Selected Video's Marked as played.");
					grid.setSelectedRows([]);
					clearAllSelections();
					getUserGridData(0);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
	}
	
	/* function openPublishMediaDialog(){
		var videoIds  = getSelectedVideoGridId(true);
		if(videoIds=='' || videoIds.length==0){
			jAlert("Please Select at least one video to publish.");
		}else{
			$('#custVideoId').val(videoIds);
			$('#title').val('');
			//$('#category').val('6');
			$('#publishVideoModal').modal('show');
		}
	}
	
	function publishMediaAjaxCall(){
		$.ajax({
			url : "${pageContext.request.contextPath}/PublishCustomerMediaToRTFMedia",
			type : "post",
			data : "custVideoId="+$('#custVideoId').val()+"&categoryId="+$('#category').val()+"&title="+$('#title').val(),
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status==0 || jsonData.status == undefined){
					jAlert(jsonData.msg);
					return;
				}else{
					$('#publishVideoModal').modal('hide');
					getUserGridData(0);
					jAlert(jsonData.msg);
					return;
				}
				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	} */
	 
	function resetFilters(){
		userSearchString='';
		columnFilters = {};
		sortingString ='';
		getUserGridData(0);
	}
	
	function exportToExcel(){
		if($('#activeDiv').hasClass('active')){
			$('#status').val('Active');
		}else if($('#inactiveDiv').hasClass('active')){
			$('#status').val('Inactive');
		}
		var appendData = "user=BROKER&status="+$('#status').val()+"&headerFilter="+userSearchString;
	    var url = "${pageContext.request.contextPath}/Admin/UsersExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}

	

	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    //jAlert(re.test(email));
	    return re.test(email);
	}
	
	$(document).ready(function(){
		
	});
	
	/* function callTabOnChange(selectedTab) {		
		var data = "?status="+selectedTab;
		window.location = "${pageContext.request.contextPath}/Client/ManageBrokers"+data;
	} */
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pollingVideosPagingInfo}));
		refreshUserGridValues(JSON.parse(JSON.stringify(${pollingVideos})));
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserDiscountCodeTrackPreference()'>");		
	});
</script>
</body>
</html>
