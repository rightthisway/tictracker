<%@include file="/WEB-INF/jsp/taglibs.jsp"%> 

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.giftCardLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	 
	if(($('#sponsor').val()=='' || $('#sponsor').val()==null)){
		$('#resetSponsorLink').hide();
	}
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(pollingCategoryGrid != null && pollingCategoryGrid != undefined){
			pollingCategoryGrid.resizeCanvas();
			
		}
	});
	
	<c:choose>
		<c:when test="${gcStatus == 'ACTIVE'}">	
			$('#activeGiftCard').addClass('active');
			$('#activeGiftCardTab').addClass('active');
		</c:when>
		<c:when test="${gcStatus == 'EXPIRED'}">
			$('#expiredGiftCard').addClass('active');
			$('#expiredGiftCardTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeGiftCard1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredGiftCard1").click(function(){
		callTabOnChange('EXPIRED');
	});
	
	
	 $('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	
	$('#sponsorSearchAutoComplete').autocomplete("AutoCompletePollingSponsor", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		
		formatItem: function(row, i, max) {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2];
		} 
	}).result(function (event,row,formatted){
			$('#sponsor').val(row[1]); 
			$('#selectedSponsorItem').text(row[2]);
			$('#sponsorSearchAutoComplete').val("");
			$('#resetSponsorLink').show();  
	});
	
	
});

function callChangeImage(){
	$('#imageFileDiv').show();
	$('#cardImageDiv').hide();
	$('#fileRequired').val('Y');
}




function resetSponsorItem(){
	$('#resetSponsorLink').hide();
	$('#sponsor').val(""); 
	$('#selectedSponsorItem').text("");
	 
}

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/PollingCategory?gcStatus="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy giftCard">Copy GiftCard</li>
  <li data="edit giftCard">Edit GiftCard</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling Category</a></li>
			<li><i class="fa fa-laptop"></i>Manage Polling Category</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<!--<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeGiftCardTab" class=""><a id="activeGiftCard1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeGiftCard">Active Category</a></li>
				 <li id="expiredGiftCardTab" class=""><a id="expiredGiftCard1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredGiftCard">InActive Category</a></li> 
			</ul>
		</section>-->
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeGiftCard" class="tab-pane">
			<c:if test="${gcStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetGiftCardModal();">Add Category</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingCategory()">Edit Category</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingCategory()">Delete Category</button> 
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Polling Category</label>
							<div class="pull-right">
								<a href="javascript:pollingCategoryResetFilters('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingCategoryResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pollingCategory_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="pollingCategory_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<!-- <br/><br/>
				<div class="full-width full-width-btn mb-20">
				<br/><br/> -->
					
				</div>
			</c:if>				
			</div>
			<%-- <div id="expiredGiftCard" class="tab-pane">
			<c:if test="${gcStatus =='EXPIRED'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingCategory()">Edit Category</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingCategory()">Delete Category</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Polling Category</label>
							<div class="pull-right">
								<a href="javascript:pollingCategoryResetFilters('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingCategoryResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pollingCategory_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="pollingCategory_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div> --%>
	</div>
</div>

<div style="position: relative" id="questionGridDiv" class="tab-pane active">
		<div class="full-width mb-20 mt-20 full-width-btn">
			<button class="btn btn-primary" id="addQBtn" type="button" data-toggle="modal" onclick="resetQModal();">Add Question</button>
			<button class="btn btn-primary" id="editQBtn" type="button" onclick="editQuestion()">Edit Question</button>
			<button class="btn btn-primary" id="deleteQBtn" type="button" onclick="deleteQuestion()">Delete Question</button> 	
		</div>
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Polling Questions</label>
				<div class="pull-right">
					<a href="javascript:questionExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
					<a href="javascript:questionResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
				</div>
			</div>
			<div id="question_grid" style="width: 100%; height: 430px; border: 1px solid gray"></div>
			<div id="question_pager" style="width: 100%; height: 10px;"></div>
	
		</div>
	</div>


<!-- Add/Edit Polling Category  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pollingCategoryModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Polling Category</h4>
			</div>
			<div class="modal-body full-width">
				<form name="pollingCatForm" id="pollingCatForm" method="post">
					<input type="hidden" id="categoryId" name="categoryId" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="gcStatus" name="gcStatus" value="${gcStatus}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label><strong>Title</strong><span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label><strong>Polling Type</strong><span class="required">*</span>
							</label> 
							<select name="pollingType" id="pollingType" class="form-control" onchange="changePollingType();" >
								<option value="RTF">RTF</option>
								<option value="FEEDBACK">FEEDBACK</option>
								<option value="SPONSOR">SPONSOR</option>
							</select>
						</div> 
						
						<div class="form-group col-sm-6 col-xs-6" id="sponsorDiv" style="display: none; ">
							<label for="sponsor" class="control-label"><strong>Sponsor</strong></label> 
							<input class="form-control searchcontrol" type="text" id="sponsorSearchAutoComplete" name="sponsorSearchAutoComplete" placeholder="Sponsor" > 
							<input type="hidden" id="sponsor" name="sponsor" /> 
							<label for="name" id="selectedSponsorItem"></label> <a href="javascript:resetSponsorItem()" id="resetSponsorLink">remove</a>
						</div> 
						
						<div class="form-group col-sm-6 col-xs-6" id="sponsorEmptyDiv">
						</div>
						
						<div class="form-group col-sm-6 col-xs-6">
							<label><strong>Status</strong><span class="required">*</span>
							</label> 
							<select name="status" id="status" class="form-control" >
								<option value="ACTIVE">ACTIVE</option>
								<option value="EXPIRED">INACTIVE</option> 
							</select>
						</div>
						
						<div class="form-group col-sm-6 col-xs-6">
						</div>
						
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="savePollingCategory('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="savePollingCategory('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Add Questions -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="questionModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Question</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionForm" id="questionForm" method="post">
					<input type="hidden" id="q_category_id" name="qCategoryId" />
					<input type="hidden" id="q_polling_type" name="qPollingType" /> 
					<input type="hidden" id="questionId" name="questionId" />
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Question Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionText" name="questionText">
						</div>						
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option A <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionA" name="optionA">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option B <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionB" name="optionB">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option C <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionC" name="optionC">
						</div>
						 
						<div class="form-group col-sm-4 col-xs-4" id="answerDiv">
							<label>Answer <span class="required">*</span></label>
							<select name="answer" id="answer" class="form-control">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
							</select>
						</div>
						 
						<%-- <div class="form-group col-sm-4 col-xs-4" id="mjpGiftCardDiv">
							<label for="giftCard" class="control-label">Gift Card</label> 
							<input class="form-control searchcontrol" type="text" id="giftCardSearchAutoComplete" name="giftCardSearchAutoComplete" placeholder="GiftCard" > 
							<input type="hidden" id="mjpGiftCardId" name="mjpGiftCardId" />
							<input type="hidden" id="mjpGiftCardQty" name="mjpGiftCardQty" />
							<input type="hidden" id="mjpGiftCardAmt" name="mjpGiftCardAmt" />
							<label for="name" id="selectedGiftCardItem">${artistName}</label> <a href="javascript:resetGiftCardItem()" id="resetGiftCardLink">remove</a>
						</div> --%>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qSaveBtn" type="button" onclick="questionSave('save')">Save</button>
				<button class="btn btn-primary" id="qUpdateBtn" type="button" onclick="questionSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add Question end here  -->
<!-- Add/Edit Polling Category  end here  -->

<script type="text/javascript">

	function setGiftCardQuantityGroups(jsonData){
		var ticketGroups = "";
		$('#ei_ticketGroups').empty();
		if(jsonData != null && jsonData != ""){	
			var j=1;			
			for(var i=0; i<jsonData.length; i++){
				var data = jsonData[i];			
				$('#ei_ticketGroups').append('<tr id="addr_'+j+'"></tr>');
				ticketGroups = "";
				ticketGroups += "<td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input type='hidden' name='rowNumber' id='rowNumber_"+j+"' value='"+j+"' />";
				ticketGroups += "<input type='hidden' name='id_"+j+"' id='id_"+j+"' value="+data.id+" />";
				ticketGroups += "<input name='amount_"+j+"' id='amount_"+j+"' type='text' value="+data.amount+" placeholder='Amount' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='quantity_"+j+"' id='quantity_"+j+"' type='text' value="+data.maxQty+" placeholder='Quantity' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='maxThreshold_"+j+"' id='maxThreshold_"+j+"' type='text' value="+data.maxQtyThreshold+" placeholder='Max. Threshold' class='form-control input-md' />";
				ticketGroups += "</td>";
				$('#addr_'+j).html(ticketGroups);
				j++;
			}
		}else{
			callAddNewRow();
		}		
		$('#gcq_rowCount').val($('#tab_logic tr').length-1);
	}


	function callAddNewRow() {
		iRow = $('#tab_logic tr').length-1;
		$('#gcq_rowCount').val(iRow);
		iRow++;
		$('#tab_logic').append('<tr id="addr_'+iRow+'"></tr>');
		
		$('#addr_'+iRow).html(
		 		"<td><input id='amount_"+iRow+"'name='amount_"+iRow+"' type='text' placeholder='Amount' class='form-control input-md' /> </td> " +
			   "<td><input id='quantity_"+iRow+"' name='quantity_"+iRow+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> "+
			   "<td><input id='maxThreshold_"+iRow+"'  name='maxThreshold_"+iRow+"' type='text' placeholder='Max. Threshold'  class='form-control input-md'></td>");
		$('#gcq_rowCount').val(iRow);
	}
	
	function callDeleteRow() {
		if($('#tab_logic tr').length > 1){
			var delRow = $('#tab_logic tr').length-1;
			$("#addr_"+delRow).remove();
			delRow--;
		 }
		$('#gcq_rowCount').val(delRow);
	}


	function pagingControl(move, id) {
		if(id == 'pollingCategory_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getPollingCategoryGridData(pageNo);
		}
	}
	
	//GiftCard  Grid
	
	function getPollingCategoryGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/PollingCategory.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+pollingCategorySearchString+"&gcStatus=${gcStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pollingCategoryPagingInfo;
				refreshPollingCategoryGridValues(jsonData.pollingCategory);	
				clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function pollingCategoryResetFilters(status){
		var appendData = "headerFilter="+pollingCategorySearchString+"&gcStatus="+gcStatus;
	    var url =apiServerUrl+"PollingCategoryExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function pollingCategoryResetFilters(){
		pollingCategorySearchString='';
		sortingString ='';
		pollingCategoryColumnFilters = {};
		getPollingCategoryGridData(0);
	}
	
	/* var giftCardCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		}); */
	
	var pagingInfo;
	var pollingCatDataView;
	var pollingCategoryGrid;
	var pollingCategoryData = [];
	var pollingCategoryGridPager;
	var pollingCategorySearchString='';
	var sortingString='';
	var pollingCategoryColumnFilters = {};
	var userPollingCategoryColumnsStr = '<%=session.getAttribute("pollingcategorygrid")%>';

	var userPollingCategoryColumns = [];
	var allPollingCategoryColumns = [/* giftCardCheckboxSelector.getColumnDefinition(), */
			 {
				id : "id",
				field : "id",
				name : "Category Id",
				width : 80,
				sortable : true
			},{
				id : "title",
				field : "title",
				name : "Title",
				width : 80,
				sortable : true
			},{
				id : "sponsorName",
				field : "sponsorName",
				name : "Sponsor",
				width : 80,
				sortable : true
			},{
				id : "pollingType",
				field : "pollingType",
				name : "Polling Type",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "editPollingCategory",
				field : "editPollingCategory",
				name : "Edit ",
				width : 80,
				formatter: editQBFormatter
			}  ];
	
	if (userPollingCategoryColumnsStr != 'null' && userPollingCategoryColumnsStr != '') {
		columnOrder = userPollingCategoryColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPollingCategoryColumns.length; j++) {
				if (columnWidth[0] == allPollingCategoryColumns[j].id) {
					userPollingCategoryColumns[i] = allPollingCategoryColumns[j];
					userPollingCategoryColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPollingCategoryColumns = allPollingCategoryColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.id +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditPollingCategory(id);
	});
	
	var pollingCategoryOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var pollingCategoryGridSortcol = "categoryId";
	var pollingCategoryGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function pollingCategoryGridComparer(a, b) {
		var x = a[pollingCategoryGridSortcol], y = b[pollingCategoryGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPollingCategoryGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		pollingCategoryData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (pollingCategoryData[i] = {});
				//d["id"] = i;
				d["id"] = data.id;
				d["title"] = data.title;				
				d["sponsorName"] = data.sponsorName;
				d["pollingType"] = data.pollingType;				
				d["status"] =  data.status;				
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["updatedBy"] = data.updatedBy;
				d["createdBy"] = data.createdBy;
				
			}
		}

		pollingCatDataView = new Slick.Data.DataView();
		pollingCategoryGrid = new Slick.Grid("#pollingCategory_grid", pollingCatDataView,
				userPollingCategoryColumns, pollingCategoryOptions);
		pollingCategoryGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		pollingCategoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		//pollingCategoryGrid.registerPlugin(giftCardCheckboxSelector);
		
		pollingCategoryGridPager = new Slick.Controls.Pager(pollingCatDataView,
					pollingCategoryGrid, $("#pollingCategory_pager"),
					pagingInfo);
		var giftCardGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPollingCategoryColumns, pollingCategoryGrid, pollingCategoryOptions);
		
		
		/* pollingCategoryGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = pollingCategoryGrid.getCellFromEvent(e);
		      pollingCategoryGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy giftCard'){
				var tempGiftCardRowIndex = pollingCategoryGrid.getSelectedRows([0])[0];
				if (tempGiftCardRowIndex == null) {
					jAlert("Plese select GiftCard to Copy", "info");
					return false;
				}else {
					var giftCardText = pollingCategoryGrid.getDataItem(tempGiftCardRowIndex).giftCard;
					var a = pollingCategoryGrid.getDataItem(tempGiftCardRowIndex).optionA;
					var b = pollingCategoryGrid.getDataItem(tempGiftCardRowIndex).optionB;
					var c = pollingCategoryGrid.getDataItem(tempGiftCardRowIndex).optionC;
					var answer = pollingCategoryGrid.getDataItem(tempGiftCardRowIndex).answer;
					var copyText = giftCardText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}else if($(e.target).attr("data") == 'edit giftCard'){
				editPollingCategory();
			}
		}); */
		
		
		pollingCategoryGrid.onSort.subscribe(function(e, args) {
			pollingCategoryGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(pollingCategoryGridSortcol) < 0){
				pollingCategoryGridSortdir = 'ASC';
			}else{
				if(pollingCategoryGridSortdir == 'DESC' ){
					pollingCategoryGridSortdir = 'ASC';
				}else{
					pollingCategoryGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+pollingCategoryGridSortcol+',SORTINGORDER:'+pollingCategoryGridSortdir+',';
			getPollingCategoryGridData(0);
		});
		
		// wire up model discountCodes to drive the pollingCategoryGrid
		pollingCatDataView.onRowCountChanged.subscribe(function(e, args) {
			pollingCategoryGrid.updateRowCount();
			pollingCategoryGrid.render();
		});
		pollingCatDataView.onRowsChanged.subscribe(function(e, args) {
			pollingCategoryGrid.invalidateRows(args.rows);
			pollingCategoryGrid.render();
		});
		$(pollingCategoryGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							pollingCategorySearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								pollingCategoryColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in pollingCategoryColumnFilters) {
										if (columnId !== undefined
												&& pollingCategoryColumnFilters[columnId] !== "") {
											pollingCategorySearchString += columnId
													+ ":"
													+ pollingCategoryColumnFilters[columnId]
													+ ",";
										}
									}
									getPollingCategoryGridData(0);
								}
							}

						});
		pollingCategoryGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'editPollingCategory' && args.column.id != 'delPollingCategory'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(pollingCategoryColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(pollingCategoryColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		pollingCategoryGrid.init();
		
		var categoryRowIndex = -1;
		pollingCategoryGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempContestsRowIndex = pollingCategoryGrid.getSelectedRows([0])[0];
			if (tempContestsRowIndex != categoryRowIndex) {
				var categoryId = pollingCategoryGrid.getDataItem(tempContestsRowIndex).id;
				var pollingType = pollingCategoryGrid.getDataItem(tempContestsRowIndex).pollingType;
				$('#q_category_id').val(categoryId);
				$('#q_polling_type').val(pollingType);
				getQuestionGridData(categoryId,0,pollingType);
			}
		});
		
		// initialize the model after all the discountCodes have been hooked up
		pollingCatDataView.beginUpdate();
		pollingCatDataView.setItems(pollingCategoryData);
		//pollingCatDataView.setFilter(filter);
		pollingCatDataView.endUpdate();
		pollingCatDataView.syncGridSelection(pollingCategoryGrid, true);
		pollingCategoryGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function getQuestionGridData(categoryId, pageNo, pollingType){
		$('#contestQuestionTab').addClass('active');
		$('#questionGridDiv').addClass('active');
		$.ajax({
			url : "${pageContext.request.contextPath}/PollingCategoryQuestion",
			type : "post",
			dataType: "json",
			data: "categoryId="+categoryId+"&pageNo="+pageNo+"&headerFilter="+questionSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				questionPagingInfo = jsonData.questionPagingInfo;
				refreshContestQuestionGridValues(jsonData.questionList);					
				$('#q_category_id').val(categoryId);
				$('#q_polling_type').val(pollingType);
				$('#question_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserQuestionPreference()'>");
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function saveUserGiftCardPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = pollingCategoryGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('pollingcategorygrid', colStr);
	}
	
	// Add GiftCard 	
	function resetGiftCardModal(){		
		$('#pollingCategoryModal').modal('show');
		$('#ei_ticketGroups').empty();
		$('#categoryId').val('');
		$('#title').val('');
		//$('#description').val('');
		//$('#conversionType').val('NORMAL');
		//$('#conversionRate').val('');
		//$('#cardType').val('CUSTOMER');
		//$('#imageFile').val('');
		$('#action').val('');
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		//$('#startDate').val('');
		$('#endDate').val('');
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		$('#cardImageDiv').hide();	
		
	}

	function savePollingCategory(action){
		
		var title = $('#title').val();
		var pollingType = $('#pollingType').val();
		var sponsor = $('#sponsor').val();
		var categoryId = $('#categoryId').val();
		var status = $('#status').val(); 
		
		if(title == ''){
			jAlert("Category title is mandatory.");
			return;
		}
		if(pollingType == ''){
			jAlert("Polling Type is mandatory.");
			return;
		}
		
		if(pollingType == 'SPONSOR'){
			if(sponsor == ''){
				jAlert("Please select sponser.");
				return;
			}
		}
		
		if(status == ''){
			jAlert("Polling status is mandatory.");
			return;
		}
		 
		$('#action').val(action);
		var requestUrl = "${pageContext.request.contextPath}/UpdatePollingCategory";
		var form = $('#pollingCatForm')[0];
		var dataString = new FormData(form);
		
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#pollingCategoryModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.pagingInfo);
					pollingCategoryColumnFilters = {};
					refreshPollingCategoryGridValues(JSON.parse(jsonData.pollingCategoryList));
					clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit GiftCard 
	function editPollingCategory(){
		var tempGiftCardRowIndex = pollingCategoryGrid.getSelectedRows([0])[0];
		if (tempGiftCardRowIndex == null) {
			jAlert("Plese select Polling Category to Edit", "info");
			return false;
		}else {
			var categoryId = pollingCategoryGrid.getDataItem(tempGiftCardRowIndex).id;
			getEditPollingCategory(categoryId);
		}
	}
	
	function getEditPollingCategory(categoryId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePollingCategory",
			type : "post",
			dataType: "json",
			data: "categoryId="+categoryId+"&action=EDIT&pageNo=0&gcStatus=${gcStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#pollingCategoryModal').modal('show');
					setPollingCategory(JSON.parse(jsonData.pollingCategory));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setPollingCategory(pollingCategoryObj){
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = pollingCategoryObj;
		$('#categoryId').val(data.id);
		$('#title').val(data.title);
		$('#pollingType').val(data.pollingType);
		if(data.pollingType == 'SPONSOR'){
			$('#sponsor').val(data.sponsorId); 
			$('#selectedSponsorItem').text(data.sponsorName); 
			$('#resetSponsorLink').show();
		}
		$('#status').val(data.status); 
		changePollingType();
	}
	
	function getSelectedPollingCategoryGridId() {
		var tempGiftCardRowIndex = pollingCategoryGrid.getSelectedRows();
		
		var categoryIdStr='';
		$.each(tempGiftCardRowIndex, function (index, value) {
			categoryIdStr += ','+pollingCategoryGrid.getDataItem(value).id;
		});
		
		if(categoryIdStr != null && categoryIdStr!='') {
			categoryIdStr = categoryIdStr.substring(1, categoryIdStr.length);
			 return categoryIdStr;
		}
	}
	//Delete GiftCard 
	function deletePollingCategory(){
		var categoryIds = getSelectedPollingCategoryGridId();
		if (categoryIds == null || categoryIds == '' || categoryIds == undefined) {
			jAlert("Please select Polling Category to Delete", "info");
			return false;
		}else {
			getDeletePollingCategory(categoryIds);
		}
	}
	
	
	function getDeletePollingCategory(categoryIds){
		jConfirm("Are you sure to delete selected Polling Category ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingCategory",
						type : "post",
						dataType: "json",
						data : "categoryId="+categoryIds+"&action=DELETE&gcStatus=${gcStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.pagingInfo);
								pollingCategoryColumnFilters = {};
								refreshPollingCategoryGridValues(JSON.parse(jsonData.pollingCategoryList));
								clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function resetQModal(){
		var contestId = $('#q_category_id').val();
		if(contestId == ''){
			jAlert("Please select polling category to add questions.");
			return;
		}
		$('#questionModal').modal('show');
		$('#questionId').val('');
		$('#questionText').val('');
		$('#optionA').val('');
		$('#optionB').val('');
		$('#optionC').val('');
		$('#answer').val('');
		$('#qSaveBtn').show();
		$('#qUpdateBtn').hide();	
		var pollingType = $('#q_polling_type').val();
		if(pollingType == 'FEEDBACK'){
			$('#answerDiv').attr("style","display:none");
		}else{
			$('#answerDiv').attr("style","display:block");
		}
	}


	function questionSave(action){
		var pollingType = $('#q_polling_type').val();
		var text = $('#questionText').val();
		var oA = $('#optionA').val();
		var oB = $('#optionB').val();
		var oC = $('#optionC').val();
		var answer = $('#answer').val();
		
		if(text == ''){
			jAlert("Question Text is Mandatory.");
			return;
		}
		if(oA == ''){
			jAlert("Option A is Mandatory.");
			return;
		}
		if(oB == ''){
			jAlert("Option B is Mandatory.");
			return;
		}
		if(oC == ''){
			jAlert("Option C is Mandatory.");
			return;
		}
		 
		if(pollingType == 'FEEDBACK'){
			$('#answer').val("");
		}else{
			if(answer == ''){
				jAlert("Answer is Mandatory.");
				return;
			}
		}
		
		
		if(pollingType == 'SPONSOR'){
			if(sponsor == ''){
				jAlert("Please select sponser.");
				return;
			}
		}
		 
			
		var requestUrl = "${pageContext.request.contextPath}/UpdatePollingCategoryQuestion";
		var dataString = "";
		if(action == 'save'){		
			dataString  = $('#questionForm').serialize()+"&action=SAVE";
		}else if(action == 'update'){
			dataString = $('#questionForm').serialize()+"&action=UPDATE";
		}
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			data: dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#questionModal').modal('hide');
					questionPagingInfo = jsonData.questionPagingInfo;
					questionColumnFilters = {};
					refreshContestQuestionGridValues(jsonData.questionList);
					//refreshQuestionGridValues('');
					clearAllSelections();
				}
				jAlert(jsonData.msg);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	function questionResetFilters(){
		questionSearchString='';
		questionColumnFilters = {};
		var contstId = $('#q_category_id').val();
		var pollingType = $('#q_polling_type').val();
		getQuestionGridData(contstId, 0,pollingType);
	}
	
	var questionPagingInfo;
	var questionDataView;
	var questionGrid;
	var questionData = [];
	var questionGridPager;
	var questionSearchString='';
	var questionColumnFilters = {};
	var userQuestionColumnsStr = '<%=session.getAttribute("questiongrid")%>';

	var userQuestionColumns = [];
	var allQuestionColumns = [
			/* {
				id : "srNo",
				field : "srNo",
				name : "Sr. No",
				width : 80,
				sortable : true
			}, */{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			} , {
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userQuestionColumnsStr != 'null' && userQuestionColumnsStr != '') {
		columnOrder = userQuestionColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allQuestionColumns.length; j++) {
				if (columnWidth[0] == allQuestionColumns[j].id) {
					userQuestionColumns[i] = allQuestionColumns[j];
					userQuestionColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userQuestionColumns = allQuestionColumns;
	}
		
	var questionOptions = {
		editable : true,
		enableAddRow : false,
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		asyncEditorLoading : true,
		explicitInitialization : true
	};
	var questionGridSortcol = "questionId";
	var questionGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function questionGridComparer(a, b) {
		var x = a[questionGridSortcol], y = b[questionGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	
	
	function refreshContestQuestionGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		questionData = [];
		if (jsonData != null) {
			questionArray = jsonData;
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (questionData[i] = {});
				d["id"] = i;
				d["questionId"] = data.id;
				d["categoryId"] = data.categoryId; 
				d["question"] = data.question;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC; 
				d["answer"] = data.answer; 
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy; 
			}
		}

		questionDataView = new Slick.Data.DataView();
		questionGrid = new Slick.Grid("#question_grid", questionDataView,
				userQuestionColumns, questionOptions);
		questionGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		questionGrid.setSelectionModel(new Slick.RowSelectionModel());
		
		questionGridPager = new Slick.Controls.Pager(questionDataView,
					questionGrid, $("#question_pager"),
					questionPagingInfo);
		var questionGridColumnpicker = new Slick.Controls.ColumnPicker(
				allQuestionColumns, questionGrid, questionOptions);
		
		questionGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = questionGrid.getCellFromEvent(e);
		      questionGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy question'){
				var tempQuestionBankRowIndex = questionGrid.getSelectedRows([0])[0];
				if (tempQuestionBankRowIndex == null) {
					jAlert("Plese select Question to Copy", "info");
					return false;
				}else {
					var questionText = questionGrid.getDataItem(tempQuestionBankRowIndex).question;
					var a = questionGrid.getDataItem(tempQuestionBankRowIndex).optionA;
					var b = questionGrid.getDataItem(tempQuestionBankRowIndex).optionB;
					var c = questionGrid.getDataItem(tempQuestionBankRowIndex).optionC;
					var answer = questionGrid.getDataItem(tempQuestionBankRowIndex).answer;
					var copyText = questionText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
				}
			}
		});
	
		questionGrid.onSort.subscribe(function(e, args) {
			questionGridSortdir = args.sortAsc ? 1 : -1;
			questionGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				questionDataView.fastSort(questionGridSortcol, args.sortAsc);
			} else {
				questionDataView.sort(questionGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the questionGrid
		questionDataView.onRowCountChanged.subscribe(function(e, args) {
			questionGrid.updateRowCount();
			questionGrid.render();
		});
		questionDataView.onRowsChanged.subscribe(function(e, args) {
			questionGrid.invalidateRows(args.rows);
			questionGrid.render();
		});
		$(questionGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							questionSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								questionColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in questionColumnFilters) {
										if (columnId !== undefined
												&& questionColumnFilters[columnId] !== "") {
											questionSearchString += columnId
													+ ":"
													+ questionColumnFilters[columnId]
													+ ",";
										}
									}
									var contstsId = $('#q_category_id').val();
									var pollingType = $('#q_polling_type').val();
									getQuestionGridData(contstId, 0,pollingType);
									//getQuestionGridData(contstsId, 0);
								}
							}

						});
		questionGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(questionColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(questionColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		questionGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		questionDataView.beginUpdate();
		questionDataView.setItems(questionData);
		//questionDataView.setFilter(filter);
		questionDataView.endUpdate();
		questionDataView.syncGridSelection(questionGrid, true);
		questionGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	//Edit Question
	function editQuestion(){
		var tempCategoryRowIndex = pollingCategoryGrid.getSelectedRows([0])[0];
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (tempQuestionRowIndex == null) {
			jAlert("Plese select Question to Edit", "info");
			return false;
		}else {
			var questionId = questionGrid.getDataItem(tempQuestionRowIndex).questionId;
			var categoryId = pollingCategoryGrid.getDataItem(tempCategoryRowIndex).id;
			getEditQuestion(questionId,categoryId);
		}
	}
		
	function getEditQuestion(questionId,categoryId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePollingCategoryQuestion",
			type : "post",
			dataType: "json",
			data: "questionId="+questionId+"&action=EDIT&qCategoryId="+categoryId,
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					//$('#contestCategoryDiv').empty();					
					$('#questionModal').modal('show');
					setEditQuestion(jsonData.questionList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditQuestion(question){
		$('#qSaveBtn').hide();			
		$('#qUpdateBtn').show();
		if(question != null && question.length > 0){
			for (var i = 0; i < question.length; i++){					
				var data = question[i];
				$('#questionId').val(data.id);
				$('#q_category_id').val(data.categoryId); 
				$('#questionText').val(data.question);
				$('#optionA').val(data.optionA);
				$('#optionB').val(data.optionB);
				$('#optionC').val(data.optionC); 
				$('#answer').val(data.answer); 
				var pollingType = $('#q_polling_type').val();
				if(pollingType == 'FEEDBACK'){
					$('#answerDiv').attr("style","display:none");
				}else{
					$('#answerDiv').attr("style","display:block");
				}
			}
		}
	}
	
	
	function deleteQuestion(){		
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (tempQuestionRowIndex == null) {
			jAlert("Plese select Question to Delete", "info");
			return false;
		}else {
			var questionId = questionGrid.getDataItem(tempQuestionRowIndex).questionId;
			var categoryId = questionGrid.getDataItem(tempQuestionRowIndex).categoryId;
			getDeleteQuestion(questionId,categoryId);
		}
	}
	
	function getDeleteQuestion(questionId,categoryId){
		if (questionId == '') {
			jAlert("Please select a Question to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete selected Question ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingCategoryQuestion",
						type : "post",
						dataType: "json",
						data : "questionId="+questionId+"&qCategoryId="+categoryId+"&action=DELETE",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								questionPagingInfo = jsonData.questionPagingInfo;
								questionColumnFilters = {};
								refreshContestQuestionGridValues(jsonData.questionList);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	function changePollingType(){
		var pollingType = $('#pollingType').val();
		if(pollingType == 'SPONSOR'){
			$("#sponsorDiv").attr("style","display: block");
			$("#sponsorEmptyDiv").attr("style","display: none");
		}else{
			$("#sponsorDiv").attr("style","display: none");
			$("#sponsorEmptyDiv").attr("style","display: block");
		}
	}
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pollingCategoryPagingInfo};
		refreshPollingCategoryGridValues(${pollingCategory});
		$('#pollingCategory_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGiftCardPreference()'>");
		
		enableMenu();
	};
		
</script>