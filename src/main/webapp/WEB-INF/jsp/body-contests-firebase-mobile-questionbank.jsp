<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#qBCategoryAutoComplete').autocomplete("AutoCompleteQuestionBankCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='CAT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[1] ;
			}
		}
	}).result(function (event,row,formatted){
		if(row[0]=="CAT"){
			$('#qBCategory').val(row[1]);
			$('#selectedQBCategory').text(row[1]);
		}
		$('#qBCategoryAutoComplete').val("");
	});
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(questionBankGrid != null && questionBankGrid != undefined){
			questionBankGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${status == 'ACTIVEQ'}">	
			$('#activeQuestion').addClass('active');
			$('#activeQuestionTab').addClass('active');
		</c:when>
		<c:when test="${status == 'USEDQ'}">
			$('#usedQuestion').addClass('active');
			$('#usedQuestionTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeQuestion1").click(function(){
		callTabOnChange('ACTIVEQ');
	});
	$("#usedQuestion1").click(function(){
		callTabOnChange('USEDQ');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ContestsFirebaseMobile?status="+selectedTab;
}

</script>

<ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Question Bank</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeQuestionTab" class=""><a id="activeQuestion1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeQuestion">Active Questions</a></li>
				<li id="usedQuestionTab" class=""><a id="usedQuestion1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#usedQuestion">Used Questions</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeQuestion" class="tab-pane">
			<c:if test="${status =='ACTIVEQ'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetQuestionBankModal();">Add Question</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editQuestionBank()">Edit Question</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteQuestionBank()">Delete Question</button>
					<button class="btn btn-primary" id="factQBBtn" type="button" onclick="openFactCheckedPOpup()">Fact Check/UnCheck</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="questionBankGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Qustion Bank</label>
							<div class="pull-right">
								<a href="javascript:questionBankExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:questionBankResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="questionBank_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="questionBank_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/><br/><br/>
				<div class="full-width full-width-btn mb-20" style="margin: 20px;">
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('SPORTS')" >SPORTS</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('CONCERTS')"  >CONCERTS</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('THEATER')"  >THEATER</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('Broadway')"  >BROADWAY</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('LAS VEGAS')"  >LAS VEGAS</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('MLB')"  >MLB</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('NBA')"  >NBA</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('NFL')"  >NFL</button>
					<button class="btn btn-primary" type="button" onClick="reLoadQuestions('NHL')"  >NHL</button>
				</div>
			</c:if>				
			</div>
			<div id="usedQuestion" class="tab-pane">
			<c:if test="${status =='USEDQ'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editQuestionBank()">Edit Question</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteQuestionBank()">Delete Question</button>
					<button class="btn btn-primary" id="factQBBtn" type="button" onclick="openFactCheckedPOpup()">Fact Check/UnCheck</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="questionBankGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Qustion Bank</label>
							<div class="pull-right">
								<a href="javascript:questionBankExportToExcel('USED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:questionBankResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="questionBank_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="questionBank_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/><br/><br/>
						<div class="full-width full-width-btn mb-20" style="margin: 20px;">
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('SPORTS')" >SPORTS</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('CONCERTS')"  >CONCERTS</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('THEATER')"  >THEATER</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('Broadway')"  >BROADWAY</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('LAS VEGAS')"  >LAS VEGAS</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('MLB')"  >MLB</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('NBA')"  >NBA</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('NFL')"  >NFL</button>
							<button class="btn btn-primary" type="button" onClick="reLoadQuestions('NHL')"  >NHL</button>
						</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Question Bank -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="qBModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Question Bank</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionBankForm" id="questionBankForm" method="post">
					<input type="hidden" id="qBId" name="qBId" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Question Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="qBText" name="qBText">
						</div>						
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option A <span class="required">*</span>
							</label> <input class="form-control" type="text" id="qBOptionA" name="qBOptionA">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option B <span class="required">*</span>
							</label> <input class="form-control" type="text" id="qBOptionB" name="qBOptionB">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option C <span class="required">*</span>
							</label> <input class="form-control" type="text" id="qBOptionC" name="qBOptionC">
						</div>
						<!-- <div class="form-group col-sm-6 col-xs-6">
							<label>Option D <span class="required">*</span>
							</label> <input class="form-control" type="text" id="qBOptionD" name="qBOptionD">
						</div> -->
						<div class="form-group col-sm-4 col-xs-4">
							<label>Answer <span class="required">*</span>
							<select name="qBAnswer" id="qBAnswer" class="form-control">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<!-- <option value="D">D</option> -->
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Question Reward Points<span class="required">*</span>
							</label> <input class="form-control" type="text" id="qBReward" name="qBReward">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Category<span class="required">*</span>
							</label> <input class="form-control searchcontrol" type="text" id="qBCategoryAutoComplete" name="qBCategoryAutoComplete" placeholder="Category">
							<input class="form-control" type="hidden" id="qBCategory" name="qBCategory">
							<label for="name" id="selectedQBCategory"></label>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Difficulty Level <span class="required">*</span>
							<select name="difficultyLevel" id="difficultyLevel" class="form-control">
								<option value="Super Easy">Super Easy</option>
								<option value="Easy">Easy</option>
								<option value="Moderate">Moderate</option>
								<option value="Hard">Hard</option>
								<option value="Super Hard">Super Hard</option>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Fact Checked <span class="required">*</span>
							<select name="factChecked" id="factChecked" class="form-control">
								<option value="No">No</option>
								<option value="Yes">Yes</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qBSaveBtn" type="button" onclick="saveQuestionBank('save')">Save</button>
				<button class="btn btn-primary" id="qBUpdateBtn" type="button" onclick="saveQuestionBank('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Question Bank end here  -->


<!--  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="factCheckModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Question Fact Checked</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionFactForm" id="questionFactForm" method="post">
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-5 col-xs-5">
							<label>&nbsp;&nbsp;&nbsp;</label> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Fact Checked <span class="required">*</span>
							<select name="factChecked1" id="factChecked1" class="form-control">
								<option value="No">No</option>
								<option value="Yes">Yes</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qFUpdateBtn" type="button" onclick="markFactChecked()">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function openFactCheckedPOpup(){
		var questionIds = getSelectedQuestionGridId();
		if (questionIds == null || questionIds == '' || questionIds == undefined) {
			jAlert("Plese select Question to Mark it Fact Checked", "info");
			return false;
		}else {
			$('#factCheckModal').modal('show');
		}
		
	}
	function markFactChecked(){
		var questionIds = getSelectedQuestionGridId();
		if (questionIds == null || questionIds == '' || questionIds == undefined) {
			jAlert("Plese select Question to Mark it Fact Checked", "info");
			return false;
		}else {
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateQuestionBank",
				type : "post",
				dataType: "json",
				data : "qBId="+questionIds+"&action=FACT&pageNo=0&factChecked="+$('#factChecked1').val(),
				success : function(response) {
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						pagingInfo = jsonData.questionBankPagingInfo;
						questionBankColumnFilters = {};
						refreshQuestionBankGridValues(jsonData.questionBankList);
						clearAllSelections();
						$('#factCheckModal').modal('hide');
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
				});
		}
	}
	
	function pagingControl(move, id) {
		if(id == 'questionBank_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getQuestionBankGridData(pageNo);
		}
	}
	
	//Question Bank Grid
	
	function getQuestionBankGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestsFirebaseMobile.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+questionBankSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.questionBankPagingInfo;
				refreshQuestionBankGridValues(jsonData.questionBankList);	
				clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function questionBankExportToExcel(status){
		var appendData = "headerFilter="+questionBankSearchString+"&status="+status;
	    var url =apiServerUrl+"ContestQuesBankExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function questionBankResetFilters(){
		questionBankSearchString='';
		questionBankColumnFilters = {};
		getQuestionBankGridData(0);
	}
	
	function reLoadQuestions(value){
		questionBankSearchString = 'category:'+value+',';
		getQuestionBankGridData(0);
	}
	
	var questionCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var questionBankDataView;
	var questionBankGrid;
	var questionBankData = [];
	var questionBankGridPager;
	var questionBankSearchString='';
	var questionBankColumnFilters = {};
	var userQuestionBankColumnsStr = '<%=session.getAttribute("questionbankgrid")%>';

	var userQuestionBankColumns = [];
	var allQuestionBankColumns = [questionCheckboxSelector.getColumnDefinition(),
			 {
				id : "questionBankId",
				field : "questionBankId",
				name : "Ques. No",
				width : 80,
				sortable : true
			},{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			},/* {
				id : "optionD",
				field : "optionD",
				name : "option D",
				width : 80,
				sortable : true
			}, */{
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "questionReward",
				field : "questionReward",
				name : "Question/Reward",
				width : 80,
				sortable : true
			},{
				id : "difficultyLevel",
				field : "difficultyLevel",
				name : "Difficulty Level",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "category",
				field : "category",
				name : "Category",
				width : 80,
				sortable : true
			},{
				id : "factChecked",
				field : "factChecked",
				name : "Fact Checked",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "editQuestionBank",
				field : "editQuestionBank",
				name : "Edit ",
				width : 80,
				formatter: editQBFormatter
			}  ];

	if (userQuestionBankColumnsStr != 'null' && userQuestionBankColumnsStr != '') {
		columnOrder = userQuestionBankColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allQuestionBankColumns.length; j++) {
				if (columnWidth[0] == allQuestionBankColumns[j].id) {
					userQuestionBankColumns[i] = allQuestionBankColumns[j];
					userQuestionBankColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userQuestionBankColumns = allQuestionBankColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.questionBankId +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditQuestionBank(id);
	});
	
	function deleteQBFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delQBClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.questionBankId +"'/>";		
		return button;
	}
	
	
	$('.delQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getDeleteQuestionBank(id);
	});	
	
	var questionBankOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var questionBankGridSortcol = "questionBankId";
	var questionBankGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function questionBankGridComparer(a, b) {
		var x = a[questionBankGridSortcol], y = b[questionBankGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshQuestionBankGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		questionBankData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (questionBankData[i] = {});
				d["id"] = i;
				d["questionBankId"] = data.id;
				d["question"] = data.question;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC;
				if(data.factChecked == true || data.factChecked == 'true'){
					d["factChecked"] = 'Yes';
				}else{
					d["factChecked"] = 'No';
				}
				
				d["difficultyLevel"] = data.dificultyLevel;
				d["answer"] = data.answer;
				if(data.answer == 'A'){
					d["answer"] = data.answer+' - '+ data.optionA;
				}else if(data.answer == 'B'){
					d["answer"] = data.answer+' - '+ data.optionB;
				}else if(data.answer == 'C'){
					d["answer"] = data.answer+' - '+ data.optionC;
				}
				
				d["questionReward"] = data.rewardPerQuestionStr;
				d["status"] = data.status;
				d["category"] = data.category;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		questionBankDataView = new Slick.Data.DataView();
		questionBankGrid = new Slick.Grid("#questionBank_grid", questionBankDataView,
				userQuestionBankColumns, questionBankOptions);
		questionBankGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		questionBankGrid.setSelectionModel(new Slick.RowSelectionModel());
		questionBankGrid.registerPlugin(questionCheckboxSelector);
		
		questionBankGridPager = new Slick.Controls.Pager(questionBankDataView,
					questionBankGrid, $("#questionBank_pager"),
					pagingInfo);
		var questionBankGridColumnpicker = new Slick.Controls.ColumnPicker(
				allQuestionBankColumns, questionBankGrid, questionBankOptions);
		
		
		questionBankGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = questionBankGrid.getCellFromEvent(e);
		      questionBankGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy question'){
				var tempQuestionBankRowIndex = questionBankGrid.getSelectedRows([0])[0];
				if (tempQuestionBankRowIndex == null) {
					jAlert("Plese select Question to Copy", "info");
					return false;
				}else {
					var questionText = questionBankGrid.getDataItem(tempQuestionBankRowIndex).question;
					var a = questionBankGrid.getDataItem(tempQuestionBankRowIndex).optionA;
					var b = questionBankGrid.getDataItem(tempQuestionBankRowIndex).optionB;
					var c = questionBankGrid.getDataItem(tempQuestionBankRowIndex).optionC;
					var answer = questionBankGrid.getDataItem(tempQuestionBankRowIndex).answer;
					var copyText = questionText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}else if($(e.target).attr("data") == 'edit question'){
				editQuestionBank();
			}
		});
		
		
		questionBankGrid.onSort.subscribe(function(e, args) {
			questionBankGridSortdir = args.sortAsc ? 1 : -1;
			questionBankGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				questionBankDataView.fastSort(questionBankGridSortcol, args.sortAsc);
			} else {
				questionBankDataView.sort(questionBankGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the questionBankGrid
		questionBankDataView.onRowCountChanged.subscribe(function(e, args) {
			questionBankGrid.updateRowCount();
			questionBankGrid.render();
		});
		questionBankDataView.onRowsChanged.subscribe(function(e, args) {
			questionBankGrid.invalidateRows(args.rows);
			questionBankGrid.render();
		});
		$(questionBankGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							questionBankSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								questionBankColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in questionBankColumnFilters) {
										if (columnId !== undefined
												&& questionBankColumnFilters[columnId] !== "") {
											questionBankSearchString += columnId
													+ ":"
													+ questionBankColumnFilters[columnId]
													+ ",";
										}
									}
									getQuestionBankGridData(0);
								}
							}

						});
		questionBankGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editQuestionBank' && args.column.id != 'delQuestionBank'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(questionBankColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(questionBankColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		questionBankGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		questionBankDataView.beginUpdate();
		questionBankDataView.setItems(questionBankData);
		//questionBankDataView.setFilter(filter);
		questionBankDataView.endUpdate();
		questionBankDataView.syncGridSelection(questionBankGrid, true);
		questionBankGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserQuestionBankPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = questionBankGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('questionbankgrid', colStr);
	}
	
	// Add Question Bank	
	function resetQuestionBankModal(){		
		$('#qBModal').modal('show');
		$('#qBId').val('');
		$('#qBText').val('');
		$('#qBOptionA').val('');
		$('#qBOptionB').val('');
		$('#qBOptionC').val('');
		//$('#qBOptionD').val('');	
		$('#qBAnswer').val('');
		$('#qBReward').val('5');
		$('#qBCategoryAutoComplete').val('');
		$('#qBCategory').val('');
		$('#selectedQBCategory').text('');
		//$('#qBNo').val('');
		
		$('#qBSaveBtn').show();
		$('#qBUpdateBtn').hide();	
	}

	function saveQuestionBank(action){
		
		var qBText = $('#qBText').val();
		var qBOptionA = $('#qBOptionA').val();
		var qBOptionB = $('#qBOptionB').val();
		var qBOptionC = $('#qBOptionC').val();
		//var qBOptionD = $('#qBOptionD').val();	
		var qBAnswer = $('#qBAnswer').val();
		var qBReward = $('#qBReward').val();
		//var qBNo = $('#qBNo').val();
		var qBCategory = $('#qBCategory').val();
		var qBCategoryAutoComplete = $('#qBCategoryAutoComplete').val();
		
		if(qBText == ''){
			jAlert("Question Bank Text is Mandatory.");
			return;
		}
		if(qBOptionA == ''){
			jAlert("Option A is Mandatory.");
			return;
		}
		if(qBOptionB == ''){
			jAlert("Option B is Mandatory.");
			return;
		}
		if(qBOptionC == ''){
			jAlert("Option C is Mandatory.");
			return;
		}
		/* if(qBOptionD == ''){
			jAlert("Option D is Mandatory.");
			return;
		} */
		if(qBAnswer == ''){
			jAlert("Answer is Mandatory.");
			return;
		}
		if(qBReward == ''){
			jAlert("Question Reward points are Mandatory.");
			return;
		}
		if(qBCategory == '' && qBCategoryAutoComplete == ''){
			jAlert("Question Category is Mandatory.");
			return;
		}
		if(qBCategoryAutoComplete != '' && qBCategoryAutoComplete != null){
			$('#qBCategory').val(qBCategoryAutoComplete);
		}	
		
		var requestUrl = "${pageContext.request.contextPath}/UpdateQuestionBank";
		var dataString = "";
		if(action == 'save'){		
			dataString  = $('#questionBankForm').serialize()+"&action=SAVE&pageNo=0";
		}else if(action == 'update'){
			dataString = $('#questionBankForm').serialize()+"&action=UPDATE&pageNo=0";
		}
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			data: dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#qBModal').modal('hide');
					pagingInfo = jsonData.questionBankPagingInfo;
					questionBankColumnFilters = {};
					refreshQuestionBankGridValues(jsonData.questionBankList);
					clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit Question Bank
	function editQuestionBank(){
		var tempQuestionBankRowIndex = questionBankGrid.getSelectedRows([0])[0];
		if (tempQuestionBankRowIndex == null) {
			jAlert("Plese select Question to Edit", "info");
			return false;
		}else {
			var questionBankId = questionBankGrid.getDataItem(tempQuestionBankRowIndex).questionBankId;
			getEditQuestionBank(questionBankId);
		}
	}
	
	function getEditQuestionBank(questionBankId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateQuestionBank",
			type : "post",
			dataType: "json",
			data: "qBId="+questionBankId+"&action=EDIT&pageNo=0",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#qBModal').modal('show');
					setEditQuestionBank(jsonData.questionBankList);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditQuestionBank(questionBank){
		$('#qBSaveBtn').hide();
		$('#qBUpdateBtn').show();
		if(questionBank != null && questionBank.length > 0){
			for (var i = 0; i < questionBank.length; i++){					
				var data = questionBank[i];
				$('#qBId').val(data.id);
				$('#qBText').val(data.question);
				$('#qBOptionA').val(data.optionA);
				$('#qBOptionB').val(data.optionB);
				$('#qBOptionC').val(data.optionC);
				$('#difficultyLevel').val(data.dificultyLevel);	
				$('#qBAnswer').val(data.answer);
				$('#qBReward').val(data.rewardPerQuestionStr);
				$('#qBCategoryAutoComplete').val('');
				$('#qBCategory').val(data.category);
				$('#selectedQBCategory').text(data.category);
				//$('#qBNo').val(data.id);
			}
		}
	}
	
	function getSelectedQuestionGridId() {
		var tempQuestionRowIndex = questionBankGrid.getSelectedRows();
		
		var questionIdStr='';
		$.each(tempQuestionRowIndex, function (index, value) {
			questionIdStr += ','+questionBankGrid.getDataItem(value).questionBankId;
		});
		
		if(questionIdStr != null && questionIdStr!='') {
			questionIdStr = questionIdStr.substring(1, questionIdStr.length);
			 return questionIdStr;
		}
	}
	//Delete Question Bank
	function deleteQuestionBank(){
		var questionIds = getSelectedQuestionGridId();
		if (questionIds == null || questionIds == '' || questionIds == undefined) {
			jAlert("Plese select Question to Delete", "info");
			return false;
		}else {
			//var questionBankId = questionBankGrid.getDataItem(tempQuestionBankRowIndex).questionBankId;
			getDeleteQuestionBank(questionIds);
		}
	}
	
	
	function getDeleteQuestionBank(questionIds){
		/* if (questionIds == '') {
			jAlert("Please select a Question to Delete.","Info");
			return false;
		} */
		jConfirm("Are you sure to delete selected Questions ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateQuestionBank",
						type : "post",
						dataType: "json",
						data : "qBId="+questionIds+"&action=DELETE&pageNo=0",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.questionBankPagingInfo;
								questionBankColumnFilters = {};
								refreshQuestionBankGridValues(jsonData.questionBankList);
								clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	/* function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	} */
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${questionBankPagingInfo};
		refreshQuestionBankGridValues(${questionBankList});
		$('#questionBank_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserQuestionBankPreference()'>");
		
		enableMenu();
	};
		
</script>