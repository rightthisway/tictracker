<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/bootstrap.min.js"></script>
	
    <script src="../resources/js/jquery.alerts.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		var window_height = $( window ).height();
			$('.login-logo-div').css('height', window_height);
			
			$("#companyDiv").hide();
			
			$("#userRole").change(function(event){				
				if ($("#userRole option:selected")){
					if($("#userRole option:selected").text() == 'Broker'){
						$("#companyDiv").show();
						$('#companyName').removeAttr('readonly');
					}else {
						$("#companyDiv").hide();
						$("#companyName").val('');
						//$('#companyName').attr('readonly','readonly');
					}					
				} else {
					$("#companyDiv").hide();
					$("#companyName").val('');
					//$('#companyName').attr('readonly','readonly');
				}
			});
    	});
	
    	function loginValidations(){
    		if($("#username").val() == ''){
    			jAlert("Username can't be blank");
    			return false;
    		}else if($("#password").val() == ''){
    			jAlert("Password can't be blank");
    			return false;
    		}
    	}
		
		function signUpMethod(){
			var serviceFeesFlag = false;
			var roleFlag = false;
			var flag = true;
			
    		if($("#signUpFirstName").val() == ''){				
    			jAlert("Firstname can't be blank");
				flag = false;
    			return false;
    		}else if($("#signUpLastName").val() == ''){
    			jAlert("Lastname can't be blank");
				flag = false;
    			return false;
    		}else if($("#signUpEmail").val() == ''){
    			jAlert("Email can't be blank");
				flag = false;
    			return false;
    		}else if($("#signUpPassword").val() == ''){
    			jAlert("Password can't be blank");
				flag = false;
    			return false;
    		}else if($("#phoneNo").val() == ''){
    			jAlert("Phone No can't be blank");
				flag = false;
    			return false;
    		}else if($("#userRole").val() == ''){
    			jAlert("Please choose at least one role.");
				flag = false;
    			return false;
    		}else if($("#userRole option:selected").text() == 'Broker'){			
				if($("#companyName").val() == ''){
					roleFlag = true;
				}
				if($("#serviceFees").val() == ''){
					serviceFeesFlag = true;
				}
			}
			if(roleFlag){
				jAlert("Invalid Company.","Info");
				flag = false;
				return false;
			}
			if(serviceFeesFlag){
				jAlert("Service Fees can't be blank.","Info");
				flag = false;
				return false;
			}
			if(flag){
				$.ajax({
					url : "${pageContext.request.contextPath}/SignUp",
					type : "post",
					data : $("#signUpForm").serialize(),
					
					success : function(res){
						alert(res);						
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});			
			}
    	}
				
    </script>
    <link rel="shortcut icon" href="../resources/images/favicon.png">

    <title>Ticket Fulfillment - Login</title>

    <!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.css" rel="stylesheet" />
	
    <!-- Custom styles -->
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />

	<style>
		input::-webkit-input-placeholder {
			color: black !important;
		}
		
		input::-moz-placeholder {  /* Firefox 19+ */
			color: black !important;  
		}
 
		input:-ms-input-placeholder {  
			color: black !important;  
		}
	</style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body>

    <div class="container">
	
		
		
		<!-- ================ Sign In ========================= -->
		
		<div class="full-width text-center login-logo-div">
			<div class="login-div-inner">
				<div class="">
					<img style="" src="../resources/images/ticket-fulfillment.png"/>
			
					<div class="clearfix"></div>
					<form class="login-form" onsubmit="return loginValidations()" action="Login" method="post">
					  <c:if test="${error == true }">
						<label style="color:red;display:block;text-align:center;font-size:20px;">Username or Password is incorrect</label>
					  </c:if>      
						<div class="login-wrap full-width">
							<!-- <p class="login-img"><i class="icon_lock_alt"></i></p> -->
							<div class="input-group">
							<label>Username:</label>
							  <!-- <span class="input-group-addon"></span> -->
							  
							  <input name="username" type="text" class="form-control" autofocus>
							</div>
							<div class="input-group">
							<label>Password:</label>
								<!-- <span class="input-group-addon"></span> -->
								 
								<input id="password" name="password" type="password" class="form-control">
							</div>
							<!-- <label class="checkbox">
								<input type="checkbox" value="remember-me"> Remember me
								<span class="pull-right"> <a href="#"> Forgot Password?</a></span>
							</label> -->
							<div class="login-form-btn">
								<!-- <div class="input-group text-right">
									<a href="" class="blue">Forgot Password ?</a>
								</div> -->
								<div class="input-group mt-20">
									<button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
									<!-- <buton class="btn btn-info btn-lg btn-block" type="submit">Signup</button> -->
								</div>
								<!-- <div class="input-group text-center">
									<p>Don't have an account? <a href="#" class="blue" data-toggle="modal" data-target="#signup">Sign Up!</a></p>
								</div> -->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- ================== Ends Sign In =================== -->
		
		<!-- ================== Sign Up Starts =================== -->
		<div id="signup" class="modal fade sign-pop" role="dialog" style="">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
					<div class="modal-body">
						<div class="reward_popup">
							
							<div class="reward_popup_contant">
								<a class="popuplogo" href="#"><img style="" src="../resources/images/ticket-fulfillment.png"/></a>
								<c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if>
							  
								<form class="popup_form" id="signUpForm" method="post" action="${pageContext.request.contextPath}/SignUp">
									<div class="form-group row">
										<div class="col-sm-6"><input autofocus="" id="signUpFirstName" name="firstName" placeholder="First Name" required="required" class="form-control" type="text"></div>
										<div class="col-sm-6"><input id="signUpLastName" name="lastName" placeholder="Last Name" required="required" class="form-control" type="text"></div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6"><input id="signUpEmail" name="email" placeholder="Email" required="required" class="form-control" type="email"></div>
										<div class="col-sm-6"><input id="signUpPassword" name="password" placeholder="Password" required="required" pattern=".{8,20}" title="Allowed character range between 8-20" class="form-control" type="password"></div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6">
											<select id="userRole" name="userRole" class="form-control" required="required">
												<option value="">-- User Type --</option>
												<option value="role_broker">Broker</option>
												<option value="role_affiliates">Affiliates</option>
											</select> 
										</div>
										<div class="col-sm-6">
											<input id="phoneNo" name="phoneNo" placeholder="Phone" required="required" pattern=".{6,15}" title="Allowed character range between 6-15" class="form-control" type="text">
										</div>
									</div>
									<div class="form-group row" id="companyDiv">
										<div class="col-sm-6"><input id="companyName" name="companyName" placeholder="Company Name" required="required" class="form-control" type="text"></div>
										<div class="col-sm-6"><input id="serviceFees" name="serviceFees" placeholder="Service Fees" required="required" class="form-control" type="text"></div>
									</div>
									<div class="sign_up">
										<!-- <p class="forgot">
											<span class="small">8-20 letters, numbers and/or special characters</span>
											<input id="test2" placeholder="Agree" class="form-control hide" type="checkbox">
											<label for="test2">By signing up, you agree to our 
										<a href="">User Agreement</a> &amp; 
										<a href="">Privacy Policy</a></label>

										</p> -->
										<button class="btn btn-primary btn-lg" type="button" onclick="signUpMethod()">Sign Up</button>
										<div class="alert alert-danger" id="signUp-alert-message" style="display:none;text-align:center;"></div>
										<p class="signup-wrap mt-20">Already Registered?<a href="#" data-dismiss="modal" class="blue"> Sign In</a></p>
										<input name="signUpType" id="signUpType" value="REWARDTHEFAN" type="hidden">
									</div>
								   <!--  <input id="socialAccountId" name="socialAccountId" type="hidden">
									<input id="fbAccessToken" name="fbAccessToken" type="hidden"> -->
								</form>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>
		<!-- ================ Ends Sign Up ========================= -->
		
		
		
	</div>

  </body>
</html>
