<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.giftCardLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(couponCodeGrid != null && couponCodeGrid != undefined){
			couponCodeGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${ccStatus == 'ACTIVE'}">	
			$('#activeCouponCode').addClass('active');
			$('#activeCouponCodeTab').addClass('active');
		</c:when>
		<c:when test="${ccStatus == 'EXPIRED'}">
			$('#expiredCouponCode').addClass('active');
			$('#expiredCouponCobeTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeCouponCode1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredCouponCode1").click(function(){
		callTabOnChange('EXPIRED');
	});
	
	
	 $('#exprydate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		exprydate: new Date()
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	
	
});


function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ecomm/CouponCodes?ccStatus="+selectedTab;
}

</script>


<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Coupon Code</a></li>
			<li><i class="fa fa-laptop"></i>Manage Coupon Code</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeCouponCodeTab" class=""><a id="activeCouponCode1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeCouponCode">Active Coupon Code</a></li>
				<li id="expiredCouponCobeTab" class=""><a id="expiredCouponCode1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredCouponCode">Expired Coupon Code</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeCouponCode" class="tab-pane">
			<c:if test="${ccStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetCouponCodeModal();">Add Coupon Code</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editCouponCode()">Edit Coupon Code</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="expireCouponCode()">Expire Coupon Code</button>
					<button class="btn btn-primary" id="mapProdQBBtn" type="button" onclick="getCoupProdMapGrid()">Map Products</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Coupon Code</label>
							<div class="pull-right">
								<a href="javascript:couponCodeExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:couponCodeResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="couponCode_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="cpncode_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="expiredCouponCode" class="tab-pane">
			<c:if test="${ccStatus =='EXPIRED'}">	
				<div class="full-width full-width-btn mb-20">
				
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteCouponCode()">Delete Coupon Code</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:couponCodeExportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:couponCodeResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="couponCode_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="cpncode_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Coupon Code  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="couponCodeModal" class="modal fade">
	<div class="modal-dialog modal-lg">
	
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Coupon Code</h4>
			</div>
			<div class="modal-body full-width">
				<form name="couponCodeForm" id="couponCodeForm" method="post">
					<input type="hidden" id="ccId" name="ccId" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="ccStatus" name="ccStatus" value="${ccStatus}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Coupon Code<span class="required">*</span>
							</label> <input class="form-control" type="text" id="coucde" name="coucde">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Coupon Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="couname" name="couname">
						</div>
						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="coudesc" name="coudesc">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Discount<span class="required">*</span>
							</label> <input class="form-control" type="text" id="coudiscount" name="coudiscount">
						</div>						
						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Discount Type<span class="required">*</span></label>
							<select id="discounttype" name="discounttype" class="form-control" >
								<option value="PERCENTAGE">PERCENTAGE</option>
								<option value="FLAT">FLAT</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Coupon Type<span class="required">*</span></label>
							<select id="coutype" name="coutype" class="form-control" >
								<option value="PUBLIC">PUBLIC</option>
								<option value="CONTEST">CONTEST</option>
							</select>
						</div>						
						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Expiry Date<span class="required">*</span></label>
							</label> <input class="form-control searchcontrol" type="text" id="exprydate" name="exprydate" value="${exprydate}"> 
						</div>
						
						
					</div>
					
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="saveCouponCode('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="saveCouponCode('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Coupon   end here  -->




<!-- Add/Edit Add Product Mapping  -->



<div id="add-category" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Map Coupon Code To Products <span id="pollingName_Hdr_Categoryies" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Coupon Code Mapping
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Coupon Code</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Products Mapped</li>
						</ol>
					</div>
				</div>
				<br />
				
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Map Coupon Codes</label>
							<div class="pull-right">
								<a href="javascript:pollingCategoryExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:coupProdMapResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="couponcdde_grid" style="width: 100%; height: 280px; border:1px solid gray"></div>
						<div id="category_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="pollingId_Category" name="pollingId_Category" />
				<button type="button" class="btn btn-primary" onclick="saveSelectedProductsToCoponCode()">Add Product Mapping</button>				
				<button type="button" class="btn btn-primary" onclick="removeSelectedProductsToCoponCode()">Remove Product Mapping</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>





<!-- Add Product Mapping  end here  -->



<script type="text/javascript">	

	function pagingControl(move, id) {
		if(id == 'cpncode_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getCouponCodeGridData(pageNo);
		}
	}
	
	//Coupon Code  Grid
	
	function getCouponCodeGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/CouponCodes.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+couponCodeSearchString+"&ccStatus=${ccStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				//jAlert(jsonData);
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshCouponCodeGridValues(jsonData.couponcodelst);	
				//clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function couponCodeExportToExcel(status){
		var appendData = "headerFilter="+couponCodeSearchString+"&ccStatus="+ccStatus;
	    var url =apiServerUrl+"GiftCardsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function couponCodeResetFilters(){
		couponCodeSearchString='';
		sortingString ='';
		couponCodeColumnFilters = {};
		getCouponCodeGridData(0);
	}
	
	/* var giftCardCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		}); */
	
	var pagingInfo;
	var couponCodeDataView;
	var couponCodeGrid;
	var couponCodeData = [];
	var couponCodeGridPager;
	var couponCodeSearchString='';
	var sortingString='';
	var couponCodeColumnFilters = {};
	var userGiftCardColumnsStr = '<%=session.getAttribute("couponcodegrid")%>';

	var userGiftCardColumns = [];
	var allGiftCardColumns = [/* giftCardCheckboxSelector.getColumnDefinition(), */
	{
				id : "ccId",
				field : "ccId",
				name : "cc Id",
				width : 80,
				sortable : true
			},{
				id : "coucde",
				field : "coucde",
				name : "Coupon Code",
				width : 80,
				sortable : true
			},{
				id : "couname",
				field : "couname",
				name : "Name",
				width : 80,
				sortable : true
			},{
				id : "coudesc",
				field : "coudesc",
				name : "Description",
				width : 80,
				sortable : false
			},{
				id : "coudiscount",
				field : "coudiscount",
				name : "Discount",
				width : 80,
				sortable : true
			},{
				id : "discounttype",
				field : "discounttype",
				name : "Disc. Type",
				width : 80,
				sortable : true
			},{
				id : "coutype",
				field : "coutype",
				name : "Coupon Type",
				width : 80,
				sortable : true
			},{
				id : "exprydate",
				field : "exprydate",
				name : "Expiry On",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			} ];

	if (userGiftCardColumnsStr != 'null' && userGiftCardColumnsStr != '') {
		columnOrder = userGiftCardColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allGiftCardColumns.length; j++) {
				if (columnWidth[0] == allGiftCardColumns[j].id) {
					userGiftCardColumns[i] = allGiftCardColumns[j];
					userGiftCardColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userGiftCardColumns = allGiftCardColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.giftCardId +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditCouponCode(id);
	});
	
	var couponCodeOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var giftCardGridSortcol = "giftCardId";
	var giftCardGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function giftCardGridComparer(a, b) {
		var x = a[giftCardGridSortcol], y = b[giftCardGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshCouponCodeGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		couponCodeData = [];
		//jAlert(JSON.stringify(jsonData));
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (couponCodeData[i] = {});
				d["id"] = i;
				d["ccId"] = data.ccId;
				d["coucde"] = data.coucde;				
				d["couname"] = data.couname;
				d["coudesc"] = data.coudesc;
				d["coudiscount"] = data.coudiscount;
				d["coustatus"] =  data.coustatus;
				d["discounttype"] = data.discounttype;
				d["coutype"] = data.coutype;
				d["exprydate"] = data.exprydate;
				d["updDate"] = data.updDate;
				d["updBy"] = data.updBy;
				
			}
		}
	

		couponCodeDataView = new Slick.Data.DataView();
		couponCodeGrid = new Slick.Grid("#couponCode_grid", couponCodeDataView,
				userGiftCardColumns, couponCodeOptions);
		couponCodeGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		couponCodeGrid.setSelectionModel(new Slick.RowSelectionModel());
		//couponCodeGrid.registerPlugin(giftCardCheckboxSelector);
		
		couponCodeGridPager = new Slick.Controls.Pager(couponCodeDataView,
					couponCodeGrid, $("#cpncode_pager"),
					pagingInfo);
		var giftCardGridColumnpicker = new Slick.Controls.ColumnPicker(
				allGiftCardColumns, couponCodeGrid, couponCodeOptions);
	
		
		couponCodeGrid.onSort.subscribe(function(e, args) {
			giftCardGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(giftCardGridSortcol) < 0){
				giftCardGridSortdir = 'ASC';
			}else{
				if(giftCardGridSortdir == 'DESC' ){
					giftCardGridSortdir = 'ASC';
				}else{
					giftCardGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+giftCardGridSortcol+',SORTINGORDER:'+giftCardGridSortdir+',';
			getCouponCodeGridData(0);
		});
		
		// wire up model discountCodes to drive the couponCodeGrid
		couponCodeDataView.onRowCountChanged.subscribe(function(e, args) {
			couponCodeGrid.updateRowCount();
			couponCodeGrid.render();
		});
		couponCodeDataView.onRowsChanged.subscribe(function(e, args) {
			couponCodeGrid.invalidateRows(args.rows);
			couponCodeGrid.render();
		});
		$(couponCodeGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							couponCodeSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								couponCodeColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in couponCodeColumnFilters) {
										if (columnId !== undefined
												&& couponCodeColumnFilters[columnId] !== "") {
											couponCodeSearchString += columnId
													+ ":"
													+ couponCodeColumnFilters[columnId]
													+ ",";
										}
									}
									getCouponCodeGridData(0);
								}
							}

						});
		couponCodeGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'editGiftCard' && args.column.id != 'delGiftCard'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(couponCodeColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(couponCodeColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		couponCodeGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		couponCodeDataView.beginUpdate();
		couponCodeDataView.setItems(couponCodeData);		
		couponCodeDataView.endUpdate();
		couponCodeDataView.syncGridSelection(couponCodeGrid, true);
		couponCodeGrid.resizeCanvas();		
		$("div#divLoading").removeClass('show');
	}
	
	function saveCouponCodePreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = couponCodeGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		//saveUserPreference('couponCodeGrid', colStr);
	}
	
	// Add Coupon Code Modal 	
	function resetCouponCodeModal(){		
		$('#couponCodeModal').modal('show');
		$('#ei_ticketGroups').empty();
		//$('#ccId').val('');
		$('#couname').val('');
		$('#coudesc').val('');
		$('#coudiscount').val('');
		$('#exprydate').val('');		
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		//$('#cardImageDiv').hide();	
		
	}

	function saveCouponCode(action){	
	
			
		var coucde = $('#coucde').val();
		var couname = $('#couname').val();
		var coudesc = $('#coudesc').val();		
		var coudiscount = $('#coudiscount').val();
		var discounttype = $('#discounttype').val();
		var coutype = $('#coutype').val();		
		var exprydate = $('#exprydate').val();
		
		
		if(coucde == ''){
			jAlert("Coupon Code is mandatory.");
			return;
		}
		if(couname == ''){
			jAlert("Coupon Name is mandatory.");
			return;
		}
		if(coudesc == ''){
			jAlert("Coupon Description is mandatory.");
			return;
		}
		if(coudiscount == ''){
			jAlert("Coupon Discount is mandatory.");
			return;
		}
		
		if(exprydate == ''){
			jAlert("Expiry Date is mandatory.");
			return;
		}
		
		$('#action').val(action);
		var requestUrl = "${pageContext.request.contextPath}/ecomm/UpdateCouponCode";
		var form = $('#couponCodeForm')[0];
		var dataString = new FormData(form);		
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			//enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#couponCodeModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.pagingInfo);
					couponCodeColumnFilters = {};
					refreshCouponCodeGridValues(JSON.parse(jsonData.couponcodelst));
					//clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	
	//Edit Coupon Code 
	function editCouponCode(){
		var tempGiftCardRowIndex = couponCodeGrid.getSelectedRows([0])[0];
		if (tempGiftCardRowIndex == null) {
			jAlert("Plese select Coupon Code to Edit", "info");
			return false;
		}else {
			var couponCodeId = couponCodeGrid.getDataItem(tempGiftCardRowIndex).ccId;
			getEditCouponCode(couponCodeId);
		}
	}
	
	function getEditCouponCode(couponCodeId){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/UpdateCouponCode",
			type : "post",
			dataType: "json",
			data: "ccId="+couponCodeId+"&action=EDIT&pageNo=0&ccStatus=${ccStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){							
					$('#couponCodeModal').modal('show');
					//$('#cardImageDiv').show();	
					//$('#imageFileDiv').hide();
					//$('#fileRequired').val('N');
					//alert(jsonData);
					setEditCouponCode(JSON.parse(jsonData.couponCodeMstr));
					//setEditCouponCode(jsonData.couponCodeMstr);
					//setGiftCardQuantityGroups(JSON.parse(jsonData.qtyList));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditCouponCode(couponcodedt){	
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = couponcodedt;
		$('#ccId').val(data.ccId);
		$('#coucde').val(data.coucde);
		$('#couname').val(data.couname);
		$('#coudesc').val(data.coudesc);
		$('#coudiscount').val(data.coudiscount);
		$('#discounttype').val(data.discounttype);
		$('#coutype').val(data.coutype);
		$('#exprydate').val(data.exprydateStr);
	
		
		
	}
	
	/**function getSelectedGiftCardGridId() {
		var tempGiftCardRowIndex = couponCodeGrid.getSelectedRows();
		
		var giftCardIdStr='';
		$.each(tempGiftCardRowIndex, function (index, value) {
			giftCardIdStr += ','+couponCodeGrid.getDataItem(value).giftCardId;
		});
		
		if(giftCardIdStr != null && giftCardIdStr!='') {
			giftCardIdStr = giftCardIdStr.substring(1, giftCardIdStr.length);
			 return giftCardIdStr;
		}
	}
	**/
	
	
	//Expire Delete Coupon Code 
	function expireCouponCode(){
	
	var tempGiftCardRowIndex = couponCodeGrid.getSelectedRows([0])[0];
		if (tempGiftCardRowIndex == null) {
			jAlert("Please select Coupon Code to Expire", "info");
			return false;
		}else {
			var couponCodeId = couponCodeGrid.getDataItem(tempGiftCardRowIndex).ccId;
			expireCCode(couponCodeId);
		}
	}
	
	
	function expireCCode(couponCodeId){
		jConfirm("Are you sure you want to Expire selected Coupon Code ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ecomm/UpdateCouponCode",
						type : "post",
						dataType: "json",
						data : "ccId="+couponCodeId+"&action=EXPIRE&ccStatus=${ccStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.pagingInfo);
								couponCodeColumnFilters = {};
								refreshCouponCodeGridValues(JSON.parse(jsonData.couponcodelst));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	function deleteCouponCode(){
	
	var tempGiftCardRowIndex = couponCodeGrid.getSelectedRows([0])[0];
		if (tempGiftCardRowIndex == null) {
			jAlert("Please select Coupon Code to Delete", "info");
			return false;
		}else {
			var couponCodeId = couponCodeGrid.getDataItem(tempGiftCardRowIndex).ccId;
			deleteCoupCode(couponCodeId);
		}
	}
	
	
	function deleteCoupCode(couponCodeId){
		jConfirm("Are you sure you want to Delete selected Coupon Code ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ecomm/UpdateCouponCode",
						type : "post",
						dataType: "json",
						data : "ccId="+couponCodeId+"&action=DELETE&ccStatus=${ccStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.pagingInfo);
								couponCodeColumnFilters = {};
								refreshCouponCodeGridValues(JSON.parse(jsonData.couponcodelst));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		refreshCouponCodeGridValues(${couponcodelst});	
		$('#cpncode_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveCouponCodePreference()'>");
		
		enableMenu();
	};
	
	
		function viewImg(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='viewClickableImage' src='../resources/images/audit-icon.png' id='"+ dataContext.pimg +"'/>";
	    return button;
	}
	
	
	$('.viewClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	
	
// MAPPING PRODUCTS BEGINS HERE .....................................................................


	// Mapping  page begin
	function getCoupProdMapGrid(){
		var tmpCoupMapGridRowIdx = couponCodeGrid.getSelectedRows([0])[0];
		if (tmpCoupMapGridRowIdx == null) {
		jAlert("Plese select Coupon Code to Edit", "info");
			return false;
		}else {
			var coupcdId = couponCodeGrid.getDataItem(tmpCoupMapGridRowIdx).ccId;				
			$('#pollingId_Category').val(coupcdId);
			$('#add-category').modal('show');
			coupCodeProdMapColumnFilters = {};
			getCoupMapGridData(coupcdId,0);
		}		
	}


	function getCoupMapGridData(pollId,pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/CouponCodeProdMapping.json",
			type : "post",
			dataType: "json",
			data: "ccId="+pollId+"&pageNo="+pageNo+"&ccStatus=${pcStatus}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				coupMapPagingInfo = jsonData.couprodmappagingInfo;
				refreshCpnCodeProdMapGridValues(jsonData.prodcoupmaplst);
				clearAllSelections();
				$('#contestQuesBank_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestQuesBankPreference()'>");
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}


	
	function coupProdMapResetFilters(){
		coupProdMapSearchString='';
		coupCodeProdMapColumnFilters = {};
		var contestId = $('#pollingId_Category').val();
		getCoupMapGridData(0, contestId);
	}
	
	var cpnCdeCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var coupMapPagingInfo;
	var coupMapDataView;
	var coupProdMapGrid;
	var coupMapProdData = [];
	var coupMapGridPager;
	var coupProdMapSearchString='';
	var coupCodeProdMapColumnFilters = {};
	var CoupCdeColumnStr = '<%=session.getAttribute("coupProdMapGrid")%>';



	var userCoupMappingColumns = [];
	var coupcdeColumns = [cpnCdeCheckboxSelector.getColumnDefinition(),
				{id:"slrpitmsId", name:"Product Code", field: "slrpitmsId",width:80, sortable: true}, 
				{id:"isMapped", name:"Is Mapped", field: "isMapped",width:80, sortable: true},        
               {id:"pname", name:"Product Name", field: "pname",width:80, sortable: true},
               {id:"pselMinPrc", name:"Selling Price", field: "pselMinPrc",width:80, sortable: true},
               {id:"pregMinPrc", name:"Regular Price", field: "pregMinPrc",width:80, sortable: true},
                {id:"pimg", name:"", name:"Product Image" , field: "pimg",width:80, formatter: viewImg},         
              ];

	if (CoupCdeColumnStr != 'null' && CoupCdeColumnStr != '') {
		columnOrder = CoupCdeColumnStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < coupcdeColumns.length; j++) {
				if (columnWidth[0] == coupcdeColumns[j].id) {
					userCoupMappingColumns[i] = coupcdeColumns[j];
					userCoupMappingColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userCoupMappingColumns = coupcdeColumns;
	}
	
	var coupMapProdOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var coupMapGridSortCol = "categoryd";
	var categoryGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function categoryGridComparer(a, b) {
		var x = a[coupMapGridSortCol], y = b[coupMapGridSortCol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshCpnCodeProdMapGridValues(jsonData) {
	
	    //jAlert('called refresh ');
		$("div#divLoading").addClass('show');
		coupMapProdData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (coupMapProdData[i] = {});
				d["id"] = i;
				d["slrpitmsId"] = data.slrpitmsId;
				d["isMapped"] = data.isMapped;
				d["pname"] = data.pname;
				d["pselMinPrc"] = data.pselMinPrc;
				d["pregMinPrc"] = data.pregMinPrc;
				d["pimg"] = data.pimg;
				
			}
		}		

		coupMapDataView = new Slick.Data.DataView();
		coupProdMapGrid = new Slick.Grid("#couponcdde_grid", coupMapDataView,
				userCoupMappingColumns, coupMapProdOptions);
		coupProdMapGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		coupProdMapGrid.setSelectionModel(new Slick.RowSelectionModel());
		coupProdMapGrid.registerPlugin(cpnCdeCheckboxSelector);
		
		coupMapGridPager = new Slick.Controls.Pager(coupMapDataView,
					coupProdMapGrid, $("#contestQuesBank_pager"),
					coupMapPagingInfo);
		var categoryGridColumnpicker = new Slick.Controls.ColumnPicker(
				coupcdeColumns, coupProdMapGrid, coupMapProdOptions);
		
		coupProdMapGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = coupProdMapGrid.getCellFromEvent(e);
		      coupProdMapGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu1").height()){
					height = e.pageY - $("#contextMenu1").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu1").width()){
					width =  e.pageX- $("#contextMenu1").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu1")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu1").hide();
		      });
		    });
		
		$("#contextMenu1").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
		});
			
		coupProdMapGrid.onSort.subscribe(function(e, args) {
			categoryGridSortdir = args.sortAsc ? 1 : -1;
			coupMapGridSortCol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				coupMapDataView.fastSort(coupMapGridSortCol, args.sortAsc);
			} else {
				coupMapDataView.sort(categoryGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the coupProdMapGrid
		coupMapDataView.onRowCountChanged.subscribe(function(e, args) {
			coupProdMapGrid.updateRowCount();
			coupProdMapGrid.render();
		});
		coupMapDataView.onRowsChanged.subscribe(function(e, args) {
			coupProdMapGrid.invalidateRows(args.rows);
			coupProdMapGrid.render();
		});
		$(coupProdMapGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							coupProdMapSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								coupCodeProdMapColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in coupCodeProdMapColumnFilters) {
										if (columnId !== undefined
												&& coupCodeProdMapColumnFilters[columnId] !== "") {
											coupProdMapSearchString += columnId
													+ ":"
													+ coupCodeProdMapColumnFilters[columnId]
													+ ",";
										}
									}
									getCoupMapGridData(0);
								}
							}

						});
		coupProdMapGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {				
				$("<input type='text'>").data("columnId", args.column.id)
						.val(coupCodeProdMapColumnFilters[args.column.id]).appendTo(
								args.node);				
			}
		});
		coupProdMapGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		coupMapDataView.beginUpdate();
		coupMapDataView.setItems(coupMapProdData);
		//coupMapDataView.setFilter(filter);
		coupMapDataView.endUpdate();
		coupMapDataView.syncGridSelection(coupProdMapGrid, true);
		//coupProdMapGrid.resizeCanvas();
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
	}


 // SAVE Selected Products ......................
 function removeSelectedProductsToCoponCode(){ 
 		var coupId = $('#pollingId_Category').val();		
		if(coupId == null || coupId == '') {
		 jAlert("Please Re select the Coupon Code to Map/Remove Products ");
		 return;
		}		
		var ids = getSelectedProductIdFromGrid();
		console.log(" IDS SELECTED ARE " + ids);	
		if(ids==null || ids=='' || ids == 'undefined'){				
		 	jAlert(" Please Select Products to Remove ");
		  	return;
		}else {
		 	removeProductMapping(coupId,ids);
		  }
 }
 
 
 	
		function removeProductMapping(coupId,ids) {				
			$.ajax({
				url : "${pageContext.request.contextPath}/ecomm/RemoveCouponCodeProdMap.json",
				type : "post",
				dataType : "json",
				data : "ccId="+coupId+"&slrpitmsIds="+ids+"&action=SAVEMAP",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						jAlert(" Product - Coupon Codes UN-Mapped Successfully");							
						coupMapPagingInfo = jsonData.couprodmappagingInfo;									
						refreshCpnCodeProdMapGridValues(jsonData.prodcoupmaplst);									
						clearAllSelections();
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		
		
	}
 
 
 function saveSelectedProductsToCoponCode(){ 				
		var coupId = $('#pollingId_Category').val();		
		if(coupId == null || coupId == '') {
		 jAlert("Please Re select the Coupon Code to Map/Remove Products ");
		 return;
		}		
		var ids = getSelectedProductIdFromGrid();
		console.log(" IDS SELECTED ARE " + ids);	
		if(ids==null || ids=='' || ids == 'undefined'){
			jAlert(" Please Select Products To Map");
			return;
		}else {
			updateProductMapping(coupId,ids);
		}
		
	}	
	
		
		function updateProductMapping(coupId,ids) {				
			$.ajax({
				url : "${pageContext.request.contextPath}/ecomm/UpdateCouponCodeProdMap.json",
				type : "post",
				dataType : "json",
				data : "ccId="+coupId+"&slrpitmsIds="+ids+"&action=SAVEMAP",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));					
					if(jsonData.status == 1){
					jAlert(" Product - Coupon Codes Mapped Successfully");				
						coupMapPagingInfo = jsonData.couprodmappagingInfo;									
						refreshCpnCodeProdMapGridValues(jsonData.prodcoupmaplst);										
						clearAllSelections();
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		
		
	}
	
	function getSelectedProductIdFromGrid() {
		var tempCoupMapGridRowIndex = coupProdMapGrid.getSelectedRows();
		
		var slrpitmsIdStr='';
		$.each(tempCoupMapGridRowIndex, function (index, value) {
			slrpitmsIdStr += ','+coupProdMapGrid.getDataItem(value).slrpitmsId;
		});
		
		if(slrpitmsIdStr != null && slrpitmsIdStr!='') {
			slrpitmsIdStr = slrpitmsIdStr.substring(1, slrpitmsIdStr.length);
			 return slrpitmsIdStr;
		}
	}
 
 // End Save Selected Products     ...

	
	// MAPPING PRODUCTS ENDS HERE   ..
	
	
	
	
	
		
</script>