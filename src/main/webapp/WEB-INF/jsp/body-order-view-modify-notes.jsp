<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script>

//Start - View Modify Notes Script
var orderLabel;

function getOpenOrderNote(openOrderId, orderId, orderHdr){
	$.ajax({		  
		url : "${pageContext.request.contextPath}/Deliveries/GetOrderNote",
		type : "post",
		data : "openOrderId="+openOrderId,
		dataType:"json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.status == 1){
				viewModifyNotesFormUpdate(jsonData);
				$('#openOrderId_Hdr_ModNotes').text(orderId);
				$('#openOrder_Hdr_ModNotes').text(orderHdr);
				//$('#openOrder_Label_ModNotes').text(orderHdr);
				orderLabel = orderHdr;
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function viewModifyNotesFormUpdate(jsonData){
	$('#modifyNotesOpenOrderId').val(jsonData.id);
	$('#modifyNotesOpenOrderNote').val(jsonData.internalNote);
	$('#view-modify-notes').modal('show');
}

function updateOpenOrderNote(){
	if($('#modifyNotesOpenOrderNote').val() == ''){
		jAlert("Please provide Order Note.");
		return false;
	}else{
		$.ajax({
			url : "${pageContext.request.contextPath}/Deliveries/UpdateOrderNote",
			type : "post",
			data : $("#editOpenOrderNote").serialize(),
			dataType : "json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#view-modify-notes').modal('hide');
					
					if(orderLabel == 'Open Order'){
						getOpenOrderGridData(0);
					}
					if(orderLabel == 'Shipment Pending Orders'){
						getShipemtPendingGridData(0);
					}
					if(orderLabel == 'Pending Receipt Orders'){
						getShipemtPendingGridData(0);
					}
					if(orderLabel == 'Closed Orders'){
						getClosedOrderGridData(0);
					}
					if(orderLabel == 'Past Orders'){
						getClosedOrderGridData(0);
					}
				}
				jAlert(jsonData.msg);
				//$('#ModifyNotesMsg').show();
				//$('#ModifyNotesMsg').text(res);
				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
}
//End Modify Notes Script

</script>


<!-- popup View/Modify Notes -->
	<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-modify-notes">View/Modify Notes</button>-->

	<div id="view-modify-notes" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">View/Modify Notes - Order No : <span id="openOrderId_Hdr_ModNotes" class="headerTextClass"></span></h4>
		  </div>
		  <div class="modal-body full-width">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header">
						<i class="fa fa-laptop"></i>Deliveries
					</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#"><span id="openOrder_Hdr_ModNotes"></span></a>
						</li>
						<li><i class="fa fa-laptop"></i>Note</li>
					</ol>
				</div>
			</div>
			<div id="ModifyNotesMsg" style="display: none;" class="alert alert-success fade in"></div>
			<br />

			<div id="row">
				<div class="col-xs-12">
					<form class="form-validate form-horizontal" id="editOpenOrderNote" method="post" action="${pageContext.request.contextPath}/Deliveries/EditOpenOrderNote">
						<div class="form-group">
							<input type="hidden" id="action" name="action" value="update" />
							 <input type="hidden" id="modifyNotesOpenOrderId" name="modifyNotesOpenOrderId" />
							<div class="col-xs-12" align="center">
								<label><span id="openOrder_Label_ModNotes"></span>Internal Note</label>
								<textarea class="form-control mb-20" id="modifyNotesOpenOrderNote" name="modifyNotesOpenOrderNote" cols="50" rows="5"></textarea>
							</div>
						</div>
					</form>
				</div>
				
			</div>

		  </div>
		  <div class="modal-footer full-width">
				<button type="button" onclick="updateOpenOrderNote();" style="" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View/Modify Notes -->
