<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<!-- full calendar css-->
<link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
<!-- easy pie chart-->
<link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
<!-- owl carousel -->
<link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link rel="stylesheet" href="../resources/css/fullcalendar.css">
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<link href="../resources/css/xcharts.min.css" rel=" stylesheet">
<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">

<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

<!-- javascripts -->
<%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
<!-- bootstrap -->
<script src="../resources/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="../resources/js/jquery.scrollTo.min.js"></script>
<script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/jquery.alerts.js"></script>

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>

<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>

<script>
window.onresize = function(){
	shippingGrid.resizeCanvas();
}
var shippingCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});

var pagingInfo;
var shippingGrid;
var shippingDataView;
var shippingData=[];
var shippingColumn = [ shippingCheckboxSelector.getColumnDefinition(), 
              /*  {id:"id", name:"ID", field: "id", sortable: true}, */
			   {id:"shippingId", name:"Shipping ID", field: "shippingId", sortable: true},
               {id:"firstname", name:"First Name", field: "firstname", sortable: true},
               {id:"lastname", name:"Last Name", field: "lastname", sortable: true},
               {id:"street1", name:"Street1", field: "street1", sortable: true},
               {id:"street2", name:"Street2", field: "street2", sortable: true},
               {id:"country", name:"Country", field: "country", sortable: true},
               {id:"state", name:"State", field: "state", sortable: true},
               {id:"city", name:"City", field: "city", sortable: true},
               {id:"zipcode", name:"Zip Code", field: "zipcode", sortable: true}
              ];
              
var shippingOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var sortcol = "shippingId";
var sortdir = 1;
var percentCompleteThreshold = 0;
var searchString = "";

//filter option
function myFilter(item, args) {
	var x= item["shippingId"];
	if (args.searchString  != ""
			&& x.indexOf(args.searchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}


function comparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
function shippingToggleFilterRow() {
	shippingGrid.setTopPanelVisibility(!shippingGrid.getOptions().showTopPanel);
}
	
//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$("#shipping_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});
 
function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(pagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(pagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(pagingInfo.pageNum)-1;
	}	
}

function createShippingGrid(jsonData) {	
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (shippingData[i] = {});
			d["id"] = data.id;
			d["shippingId"] = data.id;
			d["firstname"] = data.firstName;
			d["lastname"] = data.lastName;
			d["street1"] = data.addressLine1;
			d["street2"] = data.addressLine2;
			d["country"] = data.country;
			d["state"] = data.state;
			d["city"] = data.city;
			d["zipcode"] = data.zipCode;
		}
	}
	
	shippingDataView = new Slick.Data.DataView({inlineFilters: true });
	shippingGrid = new Slick.Grid("#custShippingGrid", shippingDataView, shippingColumn, shippingOptions);
	shippingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	shippingGrid.setSelectionModel(new Slick.RowSelectionModel());
	shippingGrid.registerPlugin(shippingCheckboxSelector);
	if(pagingInfo != null){
		var shippingGridPager = new Slick.Controls.Pager(shippingDataView, shippingGrid, $("#shipping_pager"),pagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(shippingColumn, shippingGrid, shippingOptions);

	  
	  // move the filter panel defined in a hidden div into shippingGrid top panel
	  //$("#shipping_inlineFilterPanel").appendTo(shippingGrid.getTopPanel()).show();
	  
	  shippingGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < shippingDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    shippingGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  shippingGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      // using temporary Object.prototype.toString override
	      // more limited and does lexicographic sort only by default, but can be much faster
	      // use numeric sort of % and lexicographic for everything else
	      shippingDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      // using native sort with comparer
	      // preferred method but can be very slow in IE with huge datasets
	      shippingDataView.sort(comparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the shippingGrid
	  shippingDataView.onRowCountChanged.subscribe(function (e, args) {
	    shippingGrid.updateRowCount();
	    shippingGrid.render();
	  });
	  shippingDataView.onRowsChanged.subscribe(function (e, args) {
	    shippingGrid.invalidateRows(args.rows);
	    shippingGrid.render();
	  });
	  
	  // wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    searchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    shippingDataView.setFilterArgs({
	      searchString: searchString
	    });
	    shippingDataView.refresh();
	  }
	  
	  shippingDataView.beginUpdate();
	  shippingDataView.setItems(shippingData);
	  shippingDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    searchString: searchString
	  });
	  shippingDataView.setFilter(myFilter);
	  shippingDataView.endUpdate();
	  // if you don't want the items that are not visible (due to being filtered out
	  // or being on a different page) to stay selected, pass 'false' to the second arg
	  shippingDataView.syncGridSelection(shippingGrid, true);
	  shippingDataView.refresh();
	  $("#gridContainer").resizable();
	  shippingGrid.resizeCanvas();
}	

function createFedexLabel(){
	var serviceType = $('#serviceType').val();
	var signatureType = $('#signatureType').val();
	var count=0; 	
	var shippingId;
	var row = shippingGrid.getSelectedRows([0])[0];
	if(row>=0){
		shippingId = shippingGrid.getDataItem(row).shippingId;
	
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					if(eventCheckbox[i].checked == true){
						count++;
					}
				}
			}
		}	
		if(count==1){
			
			if(serviceType == 'select'){
				jAlert("Please choose service type");
				return false;
			}		
			$.ajax({
				url : "${pageContext.request.contextPath}/Accounting/CreateFedexLabelForPO",
				type : "post",
				dataType : "json",
				data : "poId="+$('#poId').val()+"&shippingId="+shippingId+"&serviceType="+serviceType+"&signatureType="+signatureType,
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					jAlert(jsonData.msg);				
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});	
		}else{
			jAlert('Please select only one shipping address.');
			return false;
		}
	}else{
		jAlert('Please select shipping address.');
		return false;
	}
}

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	window.opener.getPOGridData(0);
    }
};
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Purchase Order</a></li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Shipping Address</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>
</div>
<br />

<div class="row">
	<div class="col-lg-12">

		<div style="position: relative">
			<div style="width: 100%;">
				<br /> <br />
				<div class="grid-header" style="width: 100%">
					<label>Customer Shipping/Other Addresses</label>
				</div>
				<div id="custShippingGrid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
				<div id="shipping_pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
		<br />
		<div style="position: relative">
			<div style="width: 100%;">
				<div class="col-sm-6">
					<label>Service Type <span class="required">*</span> </label> <select id="serviceType" name="serviceType" class="form-control" onchange="">
						<option value='select'>--select--</option>
						<option value="FEDEX_EXPRESS_SAVER">FEDEX EXPRESS SAVER</option>
						<option value="FIRST_OVERNIGHT">FIRST OVERNIGHT</option>
						<option value="PRIORITY_OVERNIGHT">PRIORITY OVERNIGHT</option>
						<option value="STANDARD_OVERNIGHT">STANDARD OVERNIGHT</option>
						<option value="FEDEX_2_DAY">FEDEX 2-DAY</option>
						<option value="GROUND_HOME_DELIVERY">GROUND HOME DELIVERY</option>
						<option value="INTERNATIONAL_PRIORITY">INTERNATIONAL PRIORITY</option>
					</select>
				</div>
				<div class="col-sm-6">
					<label>Signature Type <span class="required">*</span> </label> <select id="signatureType" name="signatureType" class="form-control" onchange="">
						<option value="NO_SIGNATURE_REQUIRED">NO SIGNATURE REQUIRED</option>
						<option value="SERVICE_DEFAULT">SERVICE DEFAULT</option>
						<option value="INDIRECT">INDIRECT</option>
						<option value="DIRECT">DIRECT</option>
						<option value="ADULT">ADULT</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
<br />
<br />

<div class="row">
	<div class="col-lg-12">
		<div class="form-group" align="center">
			<input type="hidden" name="poId" id="poId" value="${purchaseOrderId}" />
			<button type="button" class="btn btn-primary" onclick="createFedexLabel();">Create</button>
		</div>
	</div>
</div>

<c:if test="${empty shippingAddressList}">
	<div class="form-group" align="center">
		<div class="col-sm-12">
			<label style="font-size: 15px; font-weight: bold;">No Shipping address found, Please add shipping Address.</label>
		</div>
	</div>
</c:if>

<!--</div>-->


<script>
window.onload = function() {
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
	createShippingGrid(JSON.parse(JSON.stringify(${shippingAddressList})));
};
</script>