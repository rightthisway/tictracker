<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>	
<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.autoPrice {
	border-color: #079e07
}

.manualPrice {
	border-color: rgba(255, 0, 0, 0.88);
}

.form-horizontal .form-group {
	border-bottom: 1px solid #eff2f7;
	padding-bottom: 5px;
	margin-bottom: 5px;
}

.form-group-top {
	padding-top: 5px;
	margin-top: 5px;
}

.td {
	text-align: center;
	vertical-align: middle;
}

.list-group-item {
	border: 0px;
	background-color: inherit;
}

.existing-row-bg {
	background-color: #caf4ca;
}

.noresize {
	resize: none;
}

.fullWidth {
	width: 100%;
}
</style>

<script type="text/javascript">
	function callParentTypeChange() {
		$("#searchForm").submit();
	}

	$(document).ready(
			function() {

				var copyFromLeagueId = '${copyFromLeagueId}';
				if (copyFromLeagueId != null && copyFromLeagueId != 'null'
						&& copyFromLeagueId != '') {
					$('.existing-row-bg:checkbox:checked');
				}
				$('#selectAll').click(function() {
					if ($('#selectAll').attr('checked')) {
						$('.selectCheck').attr('checked', true);
					} else {
						$('.selectCheck').attr('checked', false);
					}
				});
				$('#copyAllTicketsCount').click(function() {
					if ($('#copyAllTicketsCount').attr('checked')) {
						$('.selectCheck').attr('checked', true);
						copyTextField('ticketsCount');
					} else {
						$('.selectCheck').attr('checked', true);
					}
				});
				$('#copyAllPrice').click(function() {
					if ($('#copyAllPrice').attr('checked')) {
						$('.selectCheck').attr('checked', true);
						copyTextField('price');
					} else {
						$('.selectCheck').attr('checked', true);
					}
				});
				
				$('#copyAllQuantityType').click(function() {
					if ($('#copyAllQuantityType').attr('checked')) {
						$('.selectCheck').attr('checked', true);
						copyTextField('quantityType');
					} else {
						$('.selectCheck').attr('checked', true);
					}
				});

			});
	function copyTextField(fieldName) {
		var isFirst = true;
		var firstFieldValue;
		$(".selectCheck:checked")
				.each(
						function() {
							var id = this.id.replace('checkbox', fieldName);
							if (isFirst) {
								firstFieldValue = $("#" + id).val();
								isFirst = false;
								var flag = callTicketsCountChange(this.id
										.replace('checkbox_', ''));
								if (!flag) {
									return false;
								}
							} else {
								$("#" + id).val(firstFieldValue);
								callTicketsCountChange(this.id.replace(
										'checkbox_', ''));
							}
						});
	}

	function callPriceChange(rowNumber) {
		var price = $('#price_' + rowNumber).val();
		if (price != '' && isNaN(price)) {
			jAlert('Please Enter valid Zone price.');
			return;
		}
		callSelectRow(rowNumber);
	}

	function callTicketsCountChange(rowNumber) {
		var regex = /^\d+$/;
		var ticketsCount = $('#ticketsCount_' + rowNumber).val();
		var soldTicketsCount = $('#soldTicketsCount_' + rowNumber).val();
		if (ticketsCount == '' || !regex.test(ticketsCount)) {
			jAlert('Please Enter valid tickets count.');
			$('#availabletixCountDiv_' + rowNumber).text(0);
			$('#ticketsCount_' + rowNumber).focus();
			return false;
		}
		var availabletixCount = ticketsCount - soldTicketsCount;
		if (availabletixCount < 0) {
			availabletixCount = 0;
		}
		$('#availabletixCountDiv_' + rowNumber).text(availabletixCount);
		callSelectRow(rowNumber);
		return true;
	}

	function callSelectRow(rowNumber) {
		$('#checkbox_' + rowNumber).attr('checked', true);
	}

	function callSaveBtnClick(action) {
		$("#infoMainDiv").hide();
		var zoneCount = 0;
		var flag = true;
		var regex = /^\d+$/;
		var decimalRegex = /^\d+(\.\d{0,2})?$/;
		var leagueNames = $('#leagueName').val();
		var savedZone = $('#savedZone').val();
		var selectedZone = '';
		
		var isMinimamOnerecord = false;
		$('.selectCheck:checkbox:checked').each(function() {

			var id, value;
			id = this.id.replace('checkbox', 'ticketsCount');
			value = $.trim($("#" + id).val());
			if (value == '' || !regex.test(value)) {
				jAlert('Please Enter valid tickets count.');
				$("#" + id).focus();
				flag = false;
				return false;
			}

			id = this.id.replace('checkbox', 'price');
			value = $.trim($("#" + id).val());
			if (value == '' || !decimalRegex.test(value)) {
				jAlert('Please Enter valid price.');
				$("#" + id).focus();
				flag = false;
				return false;
			}
			
			if(value != '' && value != null){
				zoneCount++;
			}
			
			id = this.id.replace('checkbox', 'zone');
			selectedZone = $.trim($("#" + id).val());

			isMinimamOnerecord = true;
		});
				
		if (flag && !isMinimamOnerecord) {
			jAlert('Select minimum one team zone to save.');
			$("#" + id).focus();
			flag = false;
			return false;
		}
		
		if(zoneCount > 1){
			jAlert('You can save only one zone per team, please select only one zone.');
			flag = false;
			return false;
		}
		
		if (flag) {
			if(savedZone != '' && savedZone != null && savedZone == selectedZone){
				var confirmMsgs = "Are you sure you want to update zone '"+selectedZone+"' ?";
				jConfirm(confirmMsgs,"Confirm", function(r) {
				if (r) {
						$("#action").val(action);
						$("#teamZoneForm").submit();
					}
				});
			}else if(savedZone != '' && savedZone != null && savedZone != selectedZone && zoneCount == 1){							
				var confirmMsg = "There is already zones '"+savedZone+"' is saved, ";
					confirmMsg += "Are you sure you want to replace that with currently selected zone '"+selectedZone+"' for event : '"+leagueNames+"' ?";
				jConfirm(confirmMsg,"Confirm", function(r) {
				if (r) {
						$("#action").val(action);
						$("#teamZoneForm").submit();
					}
				});
			}else{
				jConfirm("Are you sure you want to save selected team zone ?.","Confirm", function(r) {
				if (r) {
						$("#action").val(action);
						$("#teamZoneForm").submit();
					}
				});
			}
		}

	}

	function callDeleteBtnOnClick(action) {
		$("#infoMainDiv").hide();

		var flag = true;
		var isMinimamOnerecord = false;
		$('.selectCheck:checkbox:checked').each(function() {

			var id, value;
			id = this.id.replace('checkbox', 'id');
			value = $.trim($("#" + id).val());
			if (value != '') {
				isMinimamOnerecord = true;
			}

		});
		if (!isMinimamOnerecord) {
			jAlert('Select minimum one Existing team zones to delete.');
			$("#" + id).focus();
			flag = false;
			return false;
		}
		if (flag) {
			jConfirm("Are you sure you want to remove selected team zones ?.",
					"Confirm", function(r) {
						if (r) {
							$("#action").val(action);
							$("#teamZoneForm").submit();
						}
					});
		}

	}
	function callParentTypeOnChange(action) {
		$('#gridDiv').hide();
		$('#leagueId').val('');
		$('#copyFromLeagueId').val('');
		$('#copyToLeagueIds').val('');
		$("#action").val(action);
		$("#teamZoneForm").submit();
	}

	function loadChild(action) {
		$('#gridDiv').hide();
		$('#leagueId').val('');
		$('#copyFromLeagueId').val('');
		$('#copyToLeagueIds').val('');
		$("#action").val(action);
		$("#teamZoneForm").submit();
	}

	function loadGrandChild(action) {
		$('#gridDiv').hide();
		$('#leagueId').val('');
		$('#grandChildType').val('');
		$('#copyFromLeagueId').val('');
		$('#copyToLeagueIds').val('');
		$("#action").val(action);
		$("#teamZoneForm").submit();
	}

	function callLeagueOnChange(action) {
		$("#infoMainDiv").hide();
		$('#copyFromLeagueId').val('');
		var flag = true;
		/* var parentType = $('#parentType').val();
		if (parentType == null || parentType == '') {
			jAlert('Please select valid parent type.');
			document.getElementById("parentType").focus();
			flag = false;
			return false;
		} */
		var selectedLeagues = [];
		$('#leagueId :selected').each(function(i, selected) {
			if (!$(selected).val() == '') {
				selectedLeagues[i] = $(selected).val();
			}
		});

		if (selectedLeagues.length > 1) {
			return false;
		}

		if (flag) {
			$('#gridDiv').hide();
			$("#action").val(action);
			$("#teamZoneForm").submit();
		}
	}
	function callTeamOnChange() {
		$("#infoMainDiv").hide();

		var flag = true;
		/* var parentType = $('#parentType').val();
		if (parentType == null || parentType == '') {
			jAlert('Please select valid parent type.');
			document.getElementById("parentType").focus();
			flag = false;
			return false;
		} */
		var leagueId = $('#leagueId').val();
		if (leagueId == null || leagueId == '') {
			$('#gridDiv').hide();
			jAlert('Please select valid Event Name.');
			document.getElementById("leagueId").focus();
			flag = false;
			return false;
		}

		if (flag) {
			$('#gridDiv').hide();
			$("#action").val(action);
			$("#teamZoneForm").submit();
		}
	}
	function callCityOnChange(action) {

		$("#infoMainDiv").hide();

		var flag = true;
		/* var parentType = $('#parentType').val();
		if (parentType == null || parentType == '') {
			jAlert('Please select valid parent type.');
			document.getElementById("parentType").focus();
			flag = false;
			return false;
		} */
		var leagueId = $('#leagueId').val();
		if (leagueId == null || leagueId == '') {
			$('#gridDiv').hide();
			jAlert('Please select valid Event Name.');
			document.getElementById("leagueId").focus();
			flag = false;
			return false;
		}

		if (flag) {
			$('#gridDiv').hide();
			$("#action").val(action);
			$("#teamZoneForm").submit();
		}
	}

	function copyZones() {
		var leagues = '';
		var selectedLeagues = [];
		$('#leagueId :selected').each(function(i, selected) {
			if (!$(selected).val() == '') {
				selectedLeagues[i] = $(selected).val();
			}
		});
		var copyLeagueId = $('#copyFromLeagueId').val();

		if (selectedLeagues.length == 0) {
			jAlert("Please select Event.");
			return false;
		}
		if (copyLeagueId == '' || copyLeagueId <= 0) {
			jAlert("Please select to Source event to copy zones.");
			return false;
		}

		if (selectedLeagues.length == 1) {
			if (copyLeagueId == selectedLeagues[0]) {
				jAlert("Copy souce and destination event must be different.");
				return false;
			}
		}

		for ( var i = 0; i < selectedLeagues.length; i++) {
			leagues = leagues + selectedLeagues[i] + ',';
		}
		$('#copyToLeagueIds').val(leagues);
		$('#gridDiv').hide();
		$("#action").val("copy");
		$("#teamZoneForm").submit();
	}
</script>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Fantasy Sports Tickets Zones</a>
			</li>
			<li><i class="fa fa-laptop"></i>Fantasy Sports Tickets Zones</li>
		</ol>
	</div>
</div>

<div class="full-width">
	<div class="alert alert-success fade in" id="infoMainDiv" <c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="icon-remove"></i>
		</button>
		<span id="infoMsgDiv">${info}</span>
	</div>
	
	<form name="teamZoneForm" id="teamZoneForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelTeamZones">
		<input type="hidden" id="action" name="action" />
		<div class="full-width">
			<div class="form-group full-width">
				<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Parent Type</label>
				<div class="col-lg-4 col-md-4 col-xs-7">
					<label>SPORTS</label>
				</div>
				<div class="col-lg-2"></div>
			</div>
		</div>

			<div class="full-width">
				<div class="form-group full-width">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Child Type</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<select name="childType" id="childType" class="form-control input-sm m-bot15 full-width" onchange="loadGrandChild('search');" style="max-width:200px">
							<option value="">--Select--</option>
							<%-- <option value="0" <c:if test="${childId ne null and '0' eq childId}"> selected </c:if>>ALL</option> --%>
							<c:forEach items="${childCategories}" var="childCat">
								<option value="${childCat.id}" <c:if test="${childId ne null and childCat.id eq childId}"> selected </c:if>>${childCat.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>

			<c:if test="${not empty childId}">
				<div class="full-width">
					<div class="form-group">
						<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Grand Child Type</label>
						<div class="col-lg-4 col-md-4 col-xs-7">
							<select name="grandChildType" id="grandChildType" class="form-control input-sm m-bot15" onchange="callParentTypeOnChange('search');" style="max-width:200px">
								<option value="">--Select--</option>
								<%-- <option value="0" <c:if test="${grandChildId ne null and '0' eq grandChildId}"> selected </c:if>>ALL</option> --%>
								<c:forEach items="${grandChildCategories}" var="grandChildCat">
									<option value="${grandChildCat.id}" <c:if test="${grandChildId ne null and grandChildCat.id eq grandChildId}"> selected </c:if>>${grandChildCat.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
			</c:if>

		<div class="full-width">
			<div class="form-group">

				<c:choose>
					<c:when test="${not empty grandChildId}">
							<label for="imageText" class="col-lg-2 col-lg-offset-3 control-label">Event Name</label>
							<div class="col-lg-5">
								<input type="hidden" id="copyToLeagueIds" name="copyToLeagueIds" value="" /> <select multiple name="leagueId" id="leagueId" style="height: 100px; width:330px;" class="form-control input-sm m-bot15"
									onchange="callLeagueOnChange('search');">
									<!-- <option value="">--- Select ---</option> -->
									<c:forEach var="league" items="${leaguesList}">
										<option value="${league.id}" <c:if test="${leagueId ne null and league.id eq leagueId}">selected</c:if>>${league.name}</option>										
									</c:forEach>
									<c:forEach var="league" items="${leaguesList}">
										<c:if test='${leagueId ne null and league.id eq leagueId}'><input type="hidden" id="leagueName" name="leagueName" value="${league.name}"></c:if>
									</c:forEach>
								</select> <input type="hidden" id="artistId" name="artistId" value="${artistId}" />
							</div>
							<div class="col-lg-2"></div>
					</c:when>
					<c:otherwise>
						<%-- <c:if test="${not empty parentType and parentType ne 'SPORTS'}">
							<label for="imageText" class="col-lg-2 col-lg-offset-3 control-label">Team/Artist</label>
							<div class="col-lg-4 ">
								<select name="leagueId" id="leagueId" class="form-control input-sm m-bot15" onchange="callLeagueOnChange('search');">
									<option value="">--- Select ---</option>
									<c:forEach var="league" items="${leaguesList}">
										<option value="${league.id}" <c:if test="${leagueId ne null and league.id eq leagueId}">selected</c:if>>${league.name}</option>
									</c:forEach>
								</select> <input type="hidden" id="artistId" name="artistId" value="${artistId}" />
							</div>
							<div class="col-lg-2"></div>
						</c:if> --%>
					</c:otherwise>
				</c:choose>

			</div>
		</div>
		<c:if test="${not empty grandChildId}">
				<div class="full-width">
					<div class="form-group">
						<label for="imageText" class="col-lg-2 col-lg-offset-3 control-label">Copy Zones from Event</label>
						<div class="col-lg-4 ">
							<select name="copyFromLeagueId" id="copyFromLeagueId" class="form-control input-sm m-bot15">
								<option value="">--- Select ---</option>
								<c:forEach var="league" items="${leaguesList}">
									<option value="${league.id}" <c:if test="${copyFromLeagueIdStr ne null and league.id eq copyFromLeagueIdStr}">selected</c:if>>${league.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-lg-2">
							<button type="button" style="" onclick="copyZones();" class="btn btn-primary btn-sm">Copy</button>
						</div>
					</div>
				</div>
		</c:if>

			<c:choose>
				<c:when test="${not empty grandChildId}">
						<div class="full-width">
							<div class="form-group">
								<label for="imageText" class="col-lg-2 col-lg-offset-3 control-label">Team</label>
								<div class="col-lg-4 ">
									<select name="teamId" id="teamId" class="form-control input-sm m-bot15" onchange="callTeamOnChange('search');">
										<option value="">--- All ---</option>
										<c:forEach var="team" items="${teamsList}">
											<option value="${team.id}" <c:if test="${teamId ne null and team.id eq teamId}">selected</c:if>>${team.name}</option>
										</c:forEach>
									</select> <input type="hidden" id="artistId" name="artistId" value="${artistId}" />
								</div>
								<div class="col-lg-2"></div>
							</div>
						</div>
				</c:when>
				<c:otherwise>
					<%-- <c:if test="${parentType ne null and parentType ne 'SPORTS'}">
						<div class="full-width">
							<div class="form-group">
								<label for="imageText" class="col-lg-2 col-lg-offset-3 control-label">City</label>
								<div class="col-lg-4 ">
									<select name="city" id="city" class="form-control input-sm m-bot15" onchange="callCityOnChange('search');">
										<option value="">--- All ---</option>
										<c:forEach var="city" items="${cityList}">
											<option value="${city}" <c:if test="${selectedCity == city}">selected</c:if>>${city}</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-lg-2"></div>
							</div>
						</div>
					</c:if> --%>
				</c:otherwise>
			</c:choose>
		<div class="row clearfix">
			<div class="full-width column" id="gridDiv">
				<c:if test="${leagueId ne null and leagueId ne ''}">
					<c:choose>
						<c:when test="${not empty cjTeamZones}">
							<div class="pull-right">
								<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
								<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
							</div>
							<br />
							<br />
							<div class="table-responsive">
							<display:table list="${cjTeamZones}" defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator" id="cjTeamZone" requestURI="#" class="table table-bordered table-hover">
							
								<c:set var="rowBg" value="" />
								<c:if test="${cjTeamZone.id ne null}">
									<c:set var="rowBg" value="existing-row-bg" />
								</c:if>

								<display:column class="col-lg-1 ${rowBg}" title='<br/><input type="checkbox" name="selectAll" id="selectAll">'>
									<input type="checkbox" name="checkbox_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="checkbox_${cjTeamZone.team.id}_${cjTeamZone.zone}" class="selectCheck ${rowBg}" />
									<input type="hidden" name="id_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="id_${cjTeamZone.team.id}_${cjTeamZone.zone}" value="${cjTeamZone.id}" />
									<input type="hidden" name="teamId_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="teamId_${cjTeamZone.team.id}_${cjTeamZone.zone}" value="${cjTeamZone.teamId}" />
									<input type="hidden" name="zone_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="zone_${cjTeamZone.team.id}_${cjTeamZone.zone}" value="${cjTeamZone.zone}" />
									<input type="hidden" name="eventId_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="eventId_${cjTeamZone.team.id}_${cjTeamZone.zone}" value="${cjTeamZone.eventId}" />
									<input type="hidden" name="soldTicketsCount_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="soldTicketsCount_${cjTeamZone.team.id}_${cjTeamZone.zone}" value="${cjTeamZone.soldTicketsCount}" />
								</display:column>
								<%-- <c:choose>
								
									<c:when test="${parentType eq 'SPORTS' }">
										
									</c:when>
									<c:otherwise>
										<display:column class="col-lg-2 ${rowBg}" title="City" sortable="true" group="1">${cjTeamZone.event.city}</display:column>
									</c:otherwise>
								</c:choose> --%>
								<display:column class="col-lg-2 ${rowBg}" title="Team" sortable="true" group="1">${cjTeamZone.team.name}</display:column>
								<%-- <c:choose>
									<c:when test="${parentType eq 'SPORTS' }">
										<display:column class="col-lg-2 ${rowBg}" title="Event" sortable="true" group="1">${cjTeamZone.eventString}</display:column>
									</c:when>
									<c:otherwise>
										<display:column class="col-lg-2 ${rowBg}" title="Event" sortable="true" group="1">${cjTeamZone.event.eventName} ${cjTeamZone.event.eventDateTimeStr}</display:column>
									</c:otherwise>
								</c:choose> --%>
								<display:column class="col-lg-2 ${rowBg}" title="Event" sortable="true" group="1">${cjTeamZone.eventString}</display:column>
								<%-- <c:choose>
									<c:when test="${parentType eq 'SPORTS' }">
										<display:column class="col-lg-2 ${rowBg}" title="Venue" sortable="true" group="1">${cjTeamZone.venueString}</display:column>
									</c:when>
									<c:otherwise>
										<display:column class="col-lg-2 ${rowBg}" title="Venue" sortable="true" group="1">${cjTeamZone.event.formattedVenueDescription}</display:column>
									</c:otherwise>
								</c:choose> --%>
								<display:column class="col-lg-2 ${rowBg}" title="Venue" sortable="true" group="1">${cjTeamZone.venueString}</display:column>
								<display:column class="col-lg-1 ${rowBg}" title="Zone">${cjTeamZone.zone}</display:column>
								<display:column class="col-lg-1 ${rowBg}" title='Price<br/><input type="checkbox" name="copyAllPrice" id="copyAllPrice" >'>
									<c:if test="${cjTeamZone.isAutoPrice eq null or cjTeamZone.isAutoPrice eq true}">
										<div class="form-group">
											<div>
												<input class="form-control input-sm m-bot15 fullWidth autoPrice" style="width:70px;" type="text" name="price_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="price_${cjTeamZone.team.id}_${cjTeamZone.zone}"
													value="${cjTeamZone.price}" onchange="callPriceChange('${cjTeamZone.team.id}_${cjTeamZone.zone}');">
											</div>
										</div>
									</c:if>
									<c:if test="${cjTeamZone.isAutoPrice ne null and cjTeamZone.isAutoPrice eq false}">
										<div class="form-group">
											<div>
												<input class="form-control input-sm m-bot15 fullWidth manualPrice" style="width:70px;" type="text" name="price_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="price_${cjTeamZone.team.id}_${cjTeamZone.zone}"
													value="${cjTeamZone.price}" onchange="callPriceChange('${cjTeamZone.team.id}_${cjTeamZone.zone}');">
											</div>
										</div>
									</c:if>
								</display:column>
								<display:column class="col-lg-1 ${rowBg}" title='Quantity Type<br/><input type="checkbox" name="copyAllQuantityType" id="copyAllQuantityType" >'>
									<div class="form-group">
										<div>
											<select id="quantityType_${cjTeamZone.team.id}_${cjTeamZone.zone}" style="width:80px;" name="quantityType_${cjTeamZone.team.id}_${cjTeamZone.zone}" style="width:85px;" class="form-control fullWidth" onchange="callPriceChange('${cjTeamZone.team.id}_${cjTeamZone.zone}');">
												<c:forEach items="${quantityTypes}" var="quantityType">
													<option <c:if test="${quantityType==cjTeamZone.quantityType}"> Selected </c:if> value="${quantityType}">${quantityType}</option>
												</c:forEach>
											</select>
										</div>
									</div>
								</display:column>


								<display:column class="col-lg-1 ${rowBg}" title='Ticket Count<br/><input type="checkbox" name="copyAllTicketsCount" id="copyAllTicketsCount" >'>
									<%-- <input type="text" value="${cjTeamZone.ticketsCount}" name="ticketsCount_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="ticketsCount_${cjTeamZone.team.id}_${cjTeamZone.zone}" 
					onchange="selectRow('${cjTeamZone.team.id}_${cjTeamZone.zone}');" size="5" class="markupText"/> --%>
									<div class="form-group">
										<div>
											<input class="form-control input-sm m-bot15 fullWidth" type="text" style="width:40px;" name="ticketsCount_${cjTeamZone.team.id}_${cjTeamZone.zone}" id="ticketsCount_${cjTeamZone.team.id}_${cjTeamZone.zone}"
												value="${cjTeamZone.ticketsCount}" onchange="callTicketsCountChange('${cjTeamZone.team.id}_${cjTeamZone.zone}');">
										</div>
									</div>
								</display:column>
								<display:column class="col-lg-1 ${rowBg}" title="Sold Tix Count">${cjTeamZone.soldTicketsCount}</display:column>
								<display:column class="col-lg-1 ${rowBg}" title="Available Tix Count">
									<span id="availabletixCountDiv_${cjTeamZone.team.id}_${cjTeamZone.zone}">${cjTeamZone.availableTixCount}</span>
								</display:column>
								<display:column class="col-lg-1 ${rowBg}" title="Points Redeemed">${cjTeamZone.pointsRedeemed}</display:column>
								<display:column class="col-lg-1 ${rowBg}" title="Last Updated By">${cjTeamZone.lastUpdateddBy}</display:column>
								<display:column class="col-lg-1 ${rowBg}" title="Last Updated">${cjTeamZone.lastUpdatedStr}</display:column>
							</display:table>
							</div>

							<div class="pull-right mt-20">
								<input type="hidden" name="savedZone" id="savedZone" value=${savedZoneStr}>
								<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
								<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
							</div>
						</c:when>
						<c:otherwise>
							<div class="row">
								<div class="alert alert-danger fade in" id="infoMainDiv">
									<button data-dismiss="alert" class="close close-sm" type="button">
										<i class="icon-remove"></i>
									</button>
									<span id="infoMsgDiv">No Records Found.</span>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
				</c:if>
			</div>
		</div>
	</form>
</div>
