<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}
/* .light_box {
	display: none;
	position: absolute;
	top: 10%;
	left: 25%;
	width: 45%;
	height: 100%;
	padding: 20px;
	border: 1px solid #888;
	background-color: #fefefe;
	z-index: 1;
	overflow: auto;
} */

#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}

</style>
<script>
	function goToPurchaseOrder(poId){
		window.opener.location = "${pageContext.request.contextPath}/Accounting/ManagePO?poNo="+poId;
		window.close();
	}
</script>
 
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Purchase Order
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Purchase Order</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Purchase Order Summery</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
				<div class="alert alert-success fade in">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						${successMessage} With ID : <a href="javascript:goToPurchaseOrder(${purchaseOrder.id})" style="font-size:15px;">${purchaseOrder.id}</a>
					</strong>
			</c:if>
			<c:if test="${errorMessage != null}">
				<div class="alert alert-block alert-danger fade in">
					<strong
						style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
				</div>
			</c:if>
		</div>	
</div>
<br/><br/>
<div class="row">
<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 13px;font-family: arial;" class="panel-heading">
		<b>Customer Details</b> </header>
		<div class="panel-body">

			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Customer Name :&nbsp;</label>
					<span style="font-size: 15px;">${customerInfo.firstName} &nbsp;&nbsp; ${customerInfo.lastName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Email :&nbsp;</label><span style="font-size: 15px;">${customerInfo.email}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Phone :&nbsp;</label><span style="font-size: 15px;">${customerInfo.phone1}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line1 :&nbsp;</label><span style="font-size: 15px;">${customerInfo.addressLine1}</span>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Address Line2 :&nbsp;</label><span style="font-size: 15px;">${customerInfo.addressLine2}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">City :&nbsp;</label><span style="font-size: 15px;">${customerInfo.city}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">State :&nbsp;</label><span style="font-size: 15px;">${customerInfo.stateName}</span>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px;font-weight:bold;">Country :&nbsp;</label><span style="font-size: 15px;">${customerInfo.countryName}</span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">ZipCode :&nbsp;</label><span style="font-size: 15px;">${customerInfo.zipCode}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>

<div class="row">
<div class="col-lg-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">
			<b>Ticket Details</b>
			</header>
			<div class="panel-body">
			<c:set var="lastEventId" value=""/>
	<c:forEach var="ticketGroup" items="${ticketGroups}">
		<c:if test="${ticketGroup.eventId ne lastEventId}">
		<div class="form-group" style="magin-top:30px;">
		<div class="col-lg-12 col-sm-12 ">&nbsp;</div>
		 <div class="col-lg-12 col-sm-12 ">
				<label style="font-size: 13px;font-weight:bold;">${ticketGroup.eventName} &nbsp; ${ticketGroup.eventDateStr} &nbsp; ${ticketGroup.eventTimeStr}</label>
			 </div>
		</div>
		</c:if>
		<div class="form-group">
		 	<div class="col-lg-2 col-sm-2">
				<label style="font-size: 13px;font-weight:bold;">Section :&nbsp; </label>
				<span style="font-size: 15px;">${ticketGroup.section}</span> 
			 </div>
			 <div class="col-lg-2 col-sm-2">
				<label style="font-size: 13px;font-weight:bold;">Row :&nbsp; </label>
				<span style="font-size: 15px;">${ticketGroup.row}</span> 
			 </div>
			 <div class="col-lg-2 col-sm-2">
				<label style="font-size: 13px;font-weight:bold;">Seat Low :&nbsp; </label>
				<span style="font-size: 15px;">${ticketGroup.seatLow}</span> 
			 </div>
			 <div class="col-lg-2 col-sm-2">
				<label style="font-size: 13px;font-weight:bold;">Seat High :&nbsp; </label>
				<span style="font-size: 15px;">${ticketGroup.seatHigh}</span> 
			 </div>
			  <div class="col-lg-2 col-sm-2" >
				<label style="font-size: 13px;font-weight:bold;">Quantity :&nbsp; </label>
				<span style="font-size: 15px;">${ticketGroup.quantity}</span> 
			 </div>
			  <div class="col-lg-2 col-sm-2" >
				<label style="font-size: 13px;font-weight:bold;">Price :&nbsp; </label>
				<span style="font-size: 15px;">${ticketGroup.price}</span> 
			 </div>
		</div>
		
		<c:set var="lastEventId" value="${ticketGroup.eventId}"/>	
	</c:forEach>
	<!-- 
	<div class="form-group">
		<div class="col-lg-12 col-sm-12 ">
			<hr/>
		</div>
	</div>
	 -->
	</div>
</section>
</div>
</div>

<div class="row">
<div class="col-lg-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading">
			<b>Total Ticket Details</b>
			</header>
			<div class="panel-body">
					<div class="form-group" style="magin-top: 60px;">
						<div class="col-lg-3 col-sm-3 ">
							<label style="font-size: 13px; font-weight: bold;">Shipping
								Method :&nbsp; </label> <span style="font-size: 15px;">${shippingMethod}</span>
						</div>
						<div class="col-lg-3 col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Tracking
								No :&nbsp; </label> <span style="font-size: 15px;">${purchaseOrder.trackingNo}</span>
						</div>
						<div class="col-lg-3 col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Total
								Price :&nbsp; $</label> <span style="font-size: 15px;">${purchaseOrder.poTotal}</span>
						</div>
						<div class="col-lg-3 col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Total
								Qty :&nbsp; </label> <span style="font-size: 15px;">${purchaseOrder.ticketCount}</span>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-3 col-sm-3 ">
							<label style="font-size: 13px; font-weight: bold;">External
								PO No :&nbsp;</label> <span style="font-size: 15px;">${purchaseOrder.externalPONo}</span>
						</div>
						<div class="col-lg-3 col-sm-3 ">
							<label style="font-size: 13px; font-weight: bold;">Consignment
								PO No :&nbsp;</label> <span style="font-size: 15px;">${purchaseOrder.consignmentPoNo}</span>
						</div>
						<div class="col-lg-3 col-sm-3 ">
							<label style="font-size: 13px; font-weight: bold;">Transaction
								Office :&nbsp;</label> <span style="font-size: 15px;">${purchaseOrder.transactionOffice}</span>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

<div class="row">
	<div class="col-lg-12">		
		 <section class="panel">
			<header style="font-size: 13px;font-family: arial;" class="panel-heading"><b>Payment Details</b></header>
			<div class="panel-body">
			<c:if test="${paymentDetails.paymentType=='card'}">
				<div class="form-group">
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Card Holder Name :&nbsp; </label>
					  	<span style="font-size: 15px;">${paymentDetails.name}</span> 
					</div>
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Card Type :&nbsp; </label>
					  	<span style="font-size: 15px;">${paymentDetails.cardType}</span>
					</div>
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Card Last 4 Digit :&nbsp; </label>
						<span style="font-size: 15px;">${paymentDetails.cardLastFourDigit}</span>
					</div>
					<hr/>
				</div>
			</c:if>
			<c:if test="${paymentDetails.paymentType=='account'}">
				<div class="form-group">
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Account Name :&nbsp; </label>
					  <span  style="font-size: 15px;">${paymentDetails.name}</span> 
					</div>
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Routing no. Last 4 Digit :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.routingLastFourDigit}</span>
					</div>
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Account no. Last 4 Digit :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.accountLastFourDigit}</span> 
					</div>
					<hr/>
				</div>
			</c:if>
			<c:if test="${paymentDetails.paymentType=='cheque'}">
				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px;font-weight:bold;">Name :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.name}</span> 
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px;font-weight:bold;">Routing no. Last 4 Digit :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.routingLastFourDigit}</span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px;font-weight:bold;">Account no. Last 4 Digit :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.accountLastFourDigit}</span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px;font-weight:bold;">Cheque No :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.chequeNo}</span> 
					</div>
					<hr/>
				</div>
			</c:if>
				<div class="form-group">
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Payment Date :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.paymentDateStr}</span>
					</div>
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">payment Status :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.paymentStatus}</span>
					</div>
					<div class="col-sm-4">
						<label style="font-size: 13px;font-weight:bold;">Payment Note :&nbsp; </label>
					  	<span  style="font-size: 15px;">${paymentDetails.paymentNote}</span>
					</div>
					<!-- <div class="col-sm-3"></div>  -->
				</div>
			</div>
		</section>
	</div>

</div>
</div>
