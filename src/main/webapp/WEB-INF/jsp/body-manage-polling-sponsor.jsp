<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.giftCardLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(pollingSponsorGrid != null && pollingSponsorGrid != undefined){
			pollingSponsorGrid.resizeCanvas();
			
		}
	});
	
	<c:choose>
		<c:when test="${pcStatus == 'ACTIVE'}">	
			$('#activePollingSponsor').addClass('active');
			$('#activePollingSponsorTab').addClass('active');
		</c:when>
		<c:when test="${pcStatus == 'EXPIRED'}">
			$('#expiredPollingSponsor').addClass('active');
			$('#expiredPollingSponsorTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activePollingSponsor1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredPollingSponsor1").click(function(){
		callTabOnChange('EXPIRED');
	});
	
	
	 $('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	
	
});

function callChangeImage(){
	$('#imageFileDiv').show();
	$('#sponsorImageDiv').hide();
	$('#fileRequired').val('Y');
}

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/PollingSponsor?pcStatus="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy giftCard">Copy GiftCard</li>
  <li data="edit giftCard">Edit GiftCard</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling Sponsors</a></li>
			<li><i class="fa fa-laptop"></i>Manage Polling Sponsor</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activePollingSponsorTab" class=""><a id="activePollingSponsor1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activePollingSponsor">Active Sponsors</a></li>
				<li id="expiredPollingSponsorTab" class=""><a id="expiredPollingSponsor1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredPollingSponsor">InActive Sponsors</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activePollingSponsor" class="tab-pane">
			<c:if test="${pcStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetpollingSponsorModal();">Add Sponsor</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingSponsor()">Edit Sponsor</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingSponsor()">Delete Sponsor</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Polling Sponsors</label>
							<div class="pull-right">
								<a href="javascript:pollingSponsorExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingSponsorResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="polling_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="polling_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="expiredPollingSponsor" class="tab-pane">
			<c:if test="${pcStatus =='EXPIRED'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingSponsor()">Edit Sponsor</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingSponsor()">Delete Sponsor</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Polling Sponsor</label>
							<div class="pull-right">
								<a href="javascript:pollingSponsorExportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingSponsorResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="polling_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="polling_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit GiftCard  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pollingSponsorModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Polling Sponsor</h4>
			</div>
			<div class="modal-body full-width">
				<form name="giftCardForm" id="giftCardForm" method="post">
					<input type="hidden" id="cardId" name="cardId" />
					<input type="hidden" id="id" name="id" value="${id}"/>
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="pcStatus" name="pcStatus" value="${pcStatus}" />
					<input type="hidden" id="status" name="status" value="${status}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>						
						<div class="form-group col-sm-12 col-xs-12">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="description" name="description">
						</div>
						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Contact Person<span class="required">*</span></label>
							<input class="form-control" type="text" id="contactPerson" name="contactPerson">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>email<span class="required">*</span>
							</label> <input class="form-control" type="text" id="email" name="email">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Phone<span class="required">*</span></label>
							</label> <input class="form-control" type="text" id="phone" name="phone"> 
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Alternate Phone<span class="required">*</span>
							</label>  <input class="form-control" type="text" id="altPhone" name="altPhone"> 
						</div>
						
						<div class="form-group col-sm-12 col-xs-12">
							<label>Address<span class="required">*</span>
							</label> <input class="form-control" type="text" id="address" name="address">
						</div>					
					</div>
					<div id="imageFileDiv" class="form-group col-sm-8 col-xs-8">
						 	<input type="hidden" name="fileRequired" id="fileRequired" />
							<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 			<input type="file" id="imageFile" name="imageFile">
             		</div>
					<div class="form-group form-group-top" id="sponsorImageDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="sponsorImageTag" src="" height="150" width="400" />
               				 <a style="color:blue " href="javascript:callChangeImage();">Change Image</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="saveGiftCard('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="saveGiftCard('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit GiftCard  end here  -->

<script type="text/javascript">

	function setGiftCardQuantityGroups(jsonData){
		var ticketGroups = "";
		$('#ei_ticketGroups').empty();
		if(jsonData != null && jsonData != ""){	
			var j=1;			
			for(var i=0; i<jsonData.length; i++){
				var data = jsonData[i];			
				$('#ei_ticketGroups').append('<tr id="addr_'+j+'"></tr>');
				ticketGroups = "";
				ticketGroups += "<td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input type='hidden' name='rowNumber' id='rowNumber_"+j+"' value='"+j+"' />";
				ticketGroups += "<input type='hidden' name='id_"+j+"' id='id_"+j+"' value="+data.id+" />";
				ticketGroups += "<input name='amount_"+j+"' id='amount_"+j+"' type='text' value="+data.amount+" placeholder='Amount' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='quantity_"+j+"' id='quantity_"+j+"' type='text' value="+data.maxQty+" placeholder='Quantity' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='maxThreshold_"+j+"' id='maxThreshold_"+j+"' type='text' value="+data.maxQtyThreshold+" placeholder='Max. Threshold' class='form-control input-md' />";
				ticketGroups += "</td>";
				$('#addr_'+j).html(ticketGroups);
				j++;
			}
		}else{
			callAddNewRow();
		}		
		$('#gcq_rowCount').val($('#tab_logic tr').length-1);
	}


	function callAddNewRow() {
		iRow = $('#tab_logic tr').length-1;
		$('#gcq_rowCount').val(iRow);
		iRow++;
		$('#tab_logic').append('<tr id="addr_'+iRow+'"></tr>');
		
		$('#addr_'+iRow).html(
		 		"<td><input id='amount_"+iRow+"'name='amount_"+iRow+"' type='text' placeholder='Amount' class='form-control input-md' /> </td> " +
			   "<td><input id='quantity_"+iRow+"' name='quantity_"+iRow+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> "+
			   "<td><input id='maxThreshold_"+iRow+"'  name='maxThreshold_"+iRow+"' type='text' placeholder='Max. Threshold'  class='form-control input-md'></td>");
		$('#gcq_rowCount').val(iRow);
	}
	
	function callDeleteRow() {
		if($('#tab_logic tr').length > 1){
			var delRow = $('#tab_logic tr').length-1;
			$("#addr_"+delRow).remove();
			delRow--;
		 }
		$('#gcq_rowCount').val(delRow);
	}


	function pagingControl(move, id) {
		if(id == 'polling_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getPollingSponsorGridData(pageNo);
		}
	}
	
	//Polling  Grid
	
	function getPollingSponsorGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/PollingSponsor.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+pollingSearchString+"&pcStatus=${pcStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pollingSponsorPagingInfo;
				refreshPollingSponsorGridValues(jsonData.pollingSponsor);	
				//clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function pollingSponsorExportToExcel(status){
		var appendData = "headerFilter="+pollingSearchString+"&pcStatus="+pcStatus;
	    var url =apiServerUrl+"GiftCardsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function pollingSponsorResetFilters(){
		pollingSearchString='';
		sortingString ='';
		pollingColumnFilters = {};
		getPollingSponsorGridData(0);
	}
	
	
	
	var pagingInfo;
	var pollingContestDataView;
	var pollingSponsorGrid;
	var pollingData = [];
	var pollingGridPager;
	var pollingSearchString='';
	var sortingString='';
	var pollingColumnFilters = {};
	var PollingSponsorColumnsStr = '<%=session.getAttribute("pollingsponsorgrid")%>';

	var userPollingSponsorColumns = [];
	var allPollingColumns = [/* giftCardCheckboxSelector.getColumnDefinition(), */
			 {
				id : "id",
				field : "id",
				name : "Sponsor Id",
				width : 80,
				sortable : true
			},{
				id : "name",
				field : "name",
				name : "Name",
				width : 80,
				sortable : true
			},{
				id : "description",
				field : "description",
				name : "Description",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Email",
				width : 80,
				sortable : true
			},{
				id : "contactPerson",
				field : "contactPerson",
				name : "Contact Person",
				width : 80,
				sortable : true
			},{
				id : "address",
				field : "address",
				name : "Address",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "editPollingSponsor",
				field : "editPollingSponsor",
				name : "Edit ",
				width : 80,
				formatter: editQBFormatter
			}  ];
	
	if (PollingSponsorColumnsStr != 'null' && PollingSponsorColumnsStr != '') {
		columnOrder = PollingSponsorColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPollingColumns.length; j++) {
				if (columnWidth[0] == allPollingColumns[j].id) {
					userPollingSponsorColumns[i] = allPollingColumns[j];
					userPollingSponsorColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPollingSponsorColumns = allPollingColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.id +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditPollSponsor(id);
	});
	
	var pollingOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var pollingSponsorGridSortcol = "id";
	var pollingSponsorGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function pollingSponsorGridComparer(a, b) {
		var x = a[pollingSponsorGridSortcol], y = b[pollingSponsorGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPollingSponsorGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		pollingData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (pollingData[i] = {});
				//d["id"] = i;
				d["id"] = data.id;
				d["email"] = data.email;				
				d["description"] = data.description;
				d["name"] = data.name;
				d["address"] = data.address;
				d["status"] =  data.status;
				d["altPhone"] = data.altPhone;
				d["contactPerson"] = data.contactPerson;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["updatedBy"] = data.updatedBy;
				d["createdBy"] = data.createdBy;
				//d["startDate"] = data.startDateStr;
				//d["endDate"] = data.endDateStr;
			}
		}

		pollingContestDataView = new Slick.Data.DataView();
		pollingSponsorGrid = new Slick.Grid("#polling_grid", pollingContestDataView,
				userPollingSponsorColumns, pollingOptions);
		pollingSponsorGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		pollingSponsorGrid.setSelectionModel(new Slick.RowSelectionModel());
		//pollingSponsorGrid.registerPlugin(giftCardCheckboxSelector);
		
		pollingGridPager = new Slick.Controls.Pager(pollingContestDataView,
					pollingSponsorGrid, $("#polling_pager"),
					pagingInfo);
		var pollingGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPollingColumns, pollingSponsorGrid, pollingOptions);
		
		
		
		
		
		pollingSponsorGrid.onSort.subscribe(function(e, args) {
			pollingSponsorGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(pollingSponsorGridSortcol) < 0){
				pollingSponsorGridSortdir = 'ASC';
			}else{
				if(pollingSponsorGridSortdir == 'DESC' ){
					pollingSponsorGridSortdir = 'ASC';
				}else{
					pollingSponsorGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+pollingSponsorGridSortcol+',SORTINGORDER:'+pollingSponsorGridSortdir+',';
			getPollingSponsorGridData(0);
		});
		
		// wire up model discountCodes to drive the pollingSponsorGrid
		pollingContestDataView.onRowCountChanged.subscribe(function(e, args) {
			pollingSponsorGrid.updateRowCount();
			pollingSponsorGrid.render();
		});
		pollingContestDataView.onRowsChanged.subscribe(function(e, args) {
			pollingSponsorGrid.invalidateRows(args.rows);
			pollingSponsorGrid.render();
		});
		$(pollingSponsorGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							pollingSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								pollingColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in pollingColumnFilters) {
										if (columnId !== undefined
												&& pollingColumnFilters[columnId] !== "") {
											pollingSearchString += columnId
													+ ":"
													+ pollingColumnFilters[columnId]
													+ ",";
										}
									}
									getPollingSponsorGridData(0);
								}
							}

						});
		pollingSponsorGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'editPollingSponsor' && args.column.id != 'delGiftCard'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(pollingColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(pollingColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		pollingSponsorGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		pollingContestDataView.beginUpdate();
		pollingContestDataView.setItems(pollingData);
		//pollingContestDataView.setFilter(filter);
		pollingContestDataView.endUpdate();
		pollingContestDataView.syncGridSelection(pollingSponsorGrid, true);
		pollingSponsorGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserPollingPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = pollingSponsorGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('pollingsponsorgrid', colStr);
	}
	
	// Add GiftCard 	
	function resetpollingSponsorModal(){
		$('#pollingSponsorModal').modal('show');
		$('#ei_ticketGroups').empty();
		$('#cardId').val('');
		$('#email').val('');
		$('#description').val('');
		$('#Address').val('');
		$('#cardType').val('CUSTOMER');
		$('#imageFile').val('');
		$('#action').val('');
		$('#imageFileDiv').show();
		$('#sponsorImageDiv').hide();
		$('#fileRequired').val('Y');
		$('#startDate').val('');
		$('#endDate').val('');
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		
	}

	function saveGiftCard(action){
		var title = $('#title').val();
		var id = $('#id').val();
		var status = $('#status').val();
		var description = $('#description').val();
		var contactPerson = $('#contactPerson').val();
		var address = $('#address').val();
		var email = $('#email').val();
		var phone = $('#phone').val();
		var altPhone = $('#altPhone').val();
		var imageFile = $('#imageFile').val();
		var fileRequired = $('#fileRequired').val();
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		
		if(title == ''){
			jAlert("Polling Sponsor title is mendatory.");
			return;
		}
		if(description == ''){
			jAlert("Polling Sponsor description is mendatory.");
			return;
		}
		if(contactPerson == ''){
			jAlert("Polling Sponsor contactPerson is mendatory.");
			return;
		}
		if(email == ''){
			jAlert("Polling Sponsor email is mendatory.");
			return;
		}
		if(phone == ''){
			jAlert("Polling Sponsor phone is mendatory.");
			return;
		}
		if(altPhone == ''){
			jAlert("Polling Sponsor altPhone is mendatory.");
			return;
		}
		if(address == ''){
			jAlert("Address is mendatory.");
			return;
		}
		if(endDate == ''){
			jAlert("End Date is mendatory.");
			return;
		}
		if(validateEmail($('#email').val()) == false){
			jAlert("Invalid Email.","Info");
			return false;
		}
		if($('#phone').val() == '' || isNaN($('#phone').val())){
			jAlert("Please Add valid numeric value for phone","Info");
			return false;
		}
		if($('#altPhone').val() == '' || isNaN($('#altPhone').val())){
			jAlert("Please Add valid numeric value for Alternate Phone","Info");
			return false;
		}
		if(getValidPhoneNumber($('#phone').val()) == false){
			jAlert("Invalid Phone.","Info");
			return false;
		}
		if(getValidPhoneNumber($('#altPhone').val()) == false){
			jAlert("Invalid Alternate Phone.","Info");
			return false;
		}
		if(fileRequired == 'Y' && imageFile == ''){
			jAlert("GiftCard Image is mendatory.");
			return;
		} 	
		
		$('#action').val(action);
		var requestUrl = "${pageContext.request.contextPath}/UpdatePollingSponsor";
		var form = $('#giftCardForm')[0];
		var dataString = new FormData(form);
		
		/* if(action == 'SAVE'){		
			dataString  = dataString+"&action=SAVE&pageNo=0";
		}else if(action == 'UPDATE'){
			dataString = dataString+"&action=UPDATE&pageNo=0";
		} */
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#pollingSponsorModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.pollingSponsorPagingInfo);
					pollingColumnFilters = {};
					refreshPollingSponsorGridValues(JSON.parse(jsonData.pollingSponsor));
					//clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    //alert(re.test(email));
	    return re.test(email);
	}
	function getValidPhoneNumber(value)	{		
		 var filter = /^\d*(?:\.\d{1,2})?$/;
         if (filter.test(value)) {
           if(value.length==10){
                 return true;
           }
         }
         return false;
	
	}
	//Edit GiftCard 
	function editPollingSponsor(){
		var tempPollingSponsorRowIndex = pollingSponsorGrid.getSelectedRows([0])[0];
		if (tempPollingSponsorRowIndex == null) {
			jAlert("Plese select Polling Sponsor to Edit", "info");
			return false;
		}else {
			var id = pollingSponsorGrid.getDataItem(tempPollingSponsorRowIndex).id;
			getEditPollSponsor(id);
		}
	}
	
	function getEditPollSponsor(id){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePollingSponsor",
			type : "post",
			dataType: "json",
			data: "id="+id+"&action=EDIT&pageNo=0&pcStatus=${pcStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#pollingSponsorModal').modal('show');
					$('#sponsorImageDiv').show();	
					$('#imageFileDiv').hide();
					$('#fileRequired').val('N'); 
					setEditPollingSponsor(JSON.parse(jsonData.pollSponsor));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditPollingSponsor(pollSponsor){
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = pollSponsor;
		$('#title').val(data.name);
		$('#id').val(data.id);
		$('#description').val(data.description);
		$('#address').val(data.address);
		$('#contactPerson').val(data.contactPerson);
		$('#email').val(data.email);
		$('#phone').val(data.phone);
		$('#altPhone').val(data.altPhone);
		$("#sponsorImageTag").attr("src", data.imageUrl);
	}
	
	function getSelectedPollSponsorGridId() {
		var tempPollingSponsorIdsRowIndex = pollingSponsorGrid.getSelectedRows();
		
		var pollingSponsorIdStr='';
		$.each(tempPollingSponsorIdsRowIndex, function (index, value) {
			pollingSponsorIdStr += ','+pollingSponsorGrid.getDataItem(value).id;
		});
		
		if(pollingSponsorIdStr != null && pollingSponsorIdStr!='') {
			pollingSponsorIdStr = pollingSponsorIdStr.substring(1, pollingSponsorIdStr.length);
			 return pollingSponsorIdStr;
		}
	}
	//Delete GiftCard 
	function deletePollingSponsor(){
		var pollingSponsorIds = getSelectedPollSponsorGridId();
		if (pollingSponsorIds == null || pollingSponsorIds == '' || pollingSponsorIds == undefined) {
			jAlert("Plese select Polling Sponsors to Delete", "info");
			return false;
		}else {
			getDeletePollingSponsor(pollingSponsorIds);
		}
	}
	
	
	function getDeletePollingSponsor(pollingSponsorIds){
		jConfirm("Are you sure to delete selected Polling Sponsors ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingSponsor",
						type : "post",
						dataType: "json",
						data : "id="+pollingSponsorIds+"&action=DELETE&pcStatus=${pcStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.pollingSponsorPagingInfo);
								pollingColumnFilters = {};
								refreshPollingSponsorGridValues(JSON.parse(jsonData.pollingSponsor));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pollingSponsorPagingInfo};
		refreshPollingSponsorGridValues(${pollingSponsor});
		$('#polling_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingPreference()'>");
		
		enableMenu();
	};
		
</script>