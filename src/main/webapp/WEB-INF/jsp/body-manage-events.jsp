<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page import="com.rtw.tracker.datas.TrackerUser" %>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Events</title>
  
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="../resources/js/app/invoice.js"></script>
<script src="../resources/js/app/editShippingAddress.js"></script>
<script src="../resources/js/app/holdTicket.js"></script>
<script src="../resources/js/app/holdTicketHistory.js"></script>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
    background: #87ceeb;
    text-overflow: clip;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
  .slick-headerrow-column input {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.deleteClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.auditClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

 #contextMenu {
      background:#FFFFFF;
	  color:#000000;
      border: 1px solid gray;
      padding: 2px;
      display: inline-block;
      min-width: 200px;
	  
      -moz-box-shadow: 2px 2px 2px silver;
      -webkit-box-shadow: 2px 2px 2px silver;
      z-index: 99999;
    }
    #contextMenu li {
      padding: 4px 4px 4px 14px;
      list-style: none;
      cursor: pointer;
    }
    #contextMenu li:hover {
		color:#FFFFFF;
      background-color:#4d94ff;
    }
	
	#eventContextMenu {
      background:#FFFFFF;
	  color:#000000;
      border: 1px solid gray;
      padding: 2px;
      display: inline-block;
      min-width: 200px;
	  
      -moz-box-shadow: 2px 2px 2px silver;
      -webkit-box-shadow: 2px 2px 2px silver;
      z-index: 99999;
    }
    #eventContextMenu li {
      padding: 4px 4px 4px 14px;
      list-style: none;
      cursor: pointer;
    }
    #eventContextMenu li:hover {
		color:#FFFFFF;
      background-color:#4d94ff;
    }
    
</style>

<script type="text/javascript">	
$(document).ready(function(){
	
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#availableTab').click(function(){		
		var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];		
		var eventId =eventGrid.getDataItem(temprEventRowIndex).eventMarketId;
		var eventDetailsStr;
		if(eventDetailHeader != null){
			eventDetailsStr = eventDetailHeader;
		}
		setTimeout(function(){
			getCategoryTicketGroupsforEvent(eventId, eventDetailsStr);},10);
	});
	
	$('#holdTab').click(function(){
		var eventId = $('#eventId').val();
		var eventDetailsStr;
		if(eventDetailHeader != null){
			eventDetailsStr = eventDetailHeader;
		}
		setTimeout(function(){
			getHoldTicketsGridData(eventId, eventDetailsStr, 0);},10);
	});
	
	$('#lockedTab').click(function(){
		var eventId = $('#eventId').val();
		var eventDetailsStr;
		if(eventDetailHeader != null){
			eventDetailsStr = eventDetailHeader;
		}
		setTimeout(function(){
			getLockedTicketsGridData(eventId, eventDetailsStr, 0);},10);
	});
	
	$('#soldTab').click(function(){
		var eventId = $('#eventId').val();
		var eventDetailsStr;
		if(eventDetailHeader != null){
			eventDetailsStr = eventDetailHeader;
		}
		setTimeout(function(){
			getSoldTicketsGridData(eventId, eventDetailsStr, 0);},10);
	});
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		ticketPagingInfo = JSON.parse(JSON.stringify(${ticketPagingInfo}));
		refreshEventGridValues(JSON.parse(JSON.stringify(${events})));
		$('#event_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserEventPreference()'>");
		$('#ticket_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserTicketPreference()'>");
		
	});
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  eventGrid.resizeCanvas();	  
	  if(ticketGrid!=null && ticketGrid != undefined){
		  ticketGrid.resizeCanvas();
	  }
	  if(holdTicketsGrid != null && holdTicketsGrid != undefined){
		  holdTicketsGrid.resizeCanvas();
	  }
	  if(lockedTicketsGrid != null && lockedTicketsGrid != undefined){
		  lockedTicketsGrid.resizeCanvas();
	  }
	  if(soldTicketGrid != null && soldTicketGrid != undefined){
		  soldTicketGrid.resizeCanvas();
	  }
	});
	
	$('.searchcontrol').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {  
		getEventsGridData(0);
	    return false;  
	  }
	});
	
	$('.searchcontrol1').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {  
		 searchTicketData();
	     return false;  
	  }
	});
});


function searchData(){
	eventGridSearchString='';
	columnFilters = {};
	getEventsGridData(0);
}



function exportToExcel(){	
	var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&searchType="+$('#searchType').val()+"&searchValue="+$('#searchValue').val()+"&headerFilter="+eventGridSearchString;
    var url = apiServerUrl+"EventsExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	eventGridSearchString='';
	sortingString ='';
	columnFilters = {};
	getEventsGridData(0);
}
</script>

</head>
<body>
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="Edit Ticket">Edit Ticket</li>
  <li data="Create Invoice">Create Invoice</li>
</ul>
<ul id="eventContextMenu" style="display:none;position:absolute">
  <!-- <li data="View Sold Tickets">View Sold Tickets</li> -->
  <li data="View Zones Map/Tickets">View Zones Map/Tickets</li>
  <li data="BroadCast Tickets">BroadCast All Tickets</li>
  <li data="UnBroadCast Tickets">UnBroadCast All Tickets</li>
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Events</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Events</li>
		</ol>

	</div>
	
	<div class="col-xs-12 filters-div">
                <!--  search form start -->
			<form:form role="form" id="eventSearch" class="full-width" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/Events">
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">From Date</label>
					<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">To Date</label>
					<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
				</div>				
							
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">Search By</label>
					<select id="searchType" name="searchType" class="form-control ">
					  <option value="event_name">Event</option>
 					  <option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
					  <!-- <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >Grand Child Category</option>
					  <option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
					  <option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
					  <option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option> -->					  
					</select>					
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">Search Value</label>
					<input class="form-control searchcontrol" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label>&nbsp</label><input type="hidden" name="brokerId" id="brokerId" value="${brokerId}">
					<button type="button" id="searchEventBtn" class="btn btn-primary" onclick="searchData()">search</button>
				</div>
				 
               </form:form>
                <!--  search form end -->                
           </div>
           
</div>

<!-- event Grid Code Starts -->
<div style="position: relative" id="eventGridDiv" class="ticket-window">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Events Details</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="event_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="event_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>

<!-- slickgrid related scripts -->

<script>
	var eventDetailHeader;
	var pagingInfo;
	var eventDataView;
	var eventGrid;
	var eventData = [];
	var eventGridSearchString='';
	var sortingString='';
	var columnFilters = {};
	var userEventColumnsStr = '<%=session.getAttribute("eventsgrid")%>';
	var userEventColumns =[];	
	var loadEventColumns = ["eventName", "eventDate", "eventTime", "dayOfTheWeek", "venue", "noOfTix", "city", "state", "country", "notes"];
	var allEventColumns = [ {id : "eventMarketId",
		name : "Event Market Id",
		field : "eventMarketId",
		width:80,
		sortable : true
	},{id : "eventName",
		name : "Event Name",
		field : "eventName",
		width:80,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width:80,
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width:80,
		sortable : true
	}, {
		id : "dayOfTheWeek",
		name : "Day of Week",
		field : "dayOfTheWeek",
		width:80,
		sortable : true
	},{
		id : "venue",
		name : "Venue",
		field : "venue",
		width:80,
		sortable : true
	}, {
		id : "venueId",
		name : "Venue Id",
		field : "venueId",
		width:80,
		sortable : true
	},{
		id : "noOfTix",
		name : "No of Tix",
		field : "noOfTix",
		width:80,
		sortable : true
	},{
		id : "noOfTixSold",
		name : "No of Tix Sold",
		field : "noOfTixSold",
		width:80,
		sortable : true
	},{
		id : "city",
		name : "City",
		field : "city",
		width:80,
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		width:80,
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country",
		width:80,	
		sortable: true
	},/*{
		id: "artist", 
		name: "Artist", 
		field: "artist", 
		width:80,
		sortable: true
	},*/{
		id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		width:80,
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	},{
		id : "notes",
		field : "notes",
		name : "Notes",
		width:80,
		sortable: true,
		editor:Slick.Editors.LongText
	},{
		id : "eventCreated",
		field : "eventCreated",
		name : "Event Created",
		width:80,
		sortable: true
	} ,{
		id : "eventLastUpdated",
		field : "eventLastUpdated",
		name : "Event Last Updated",
		width:80,
		sortable: true
	} ];

	
	if(userEventColumnsStr!='null' && userEventColumnsStr!=''){
		columnOrder = userEventColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allEventColumns.length;j++){
				if(columnWidth[0] == allEventColumns[j].id){
					userEventColumns[i] = allEventColumns[j];
					userEventColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		columnOrder = loadEventColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allEventColumns.length;j++){
				if(columnWidth == allEventColumns[j].id){
					userEventColumns[i] = allEventColumns[j];
					userEventColumns[i].width=80;
					break;
				}
			}			
		}
		//userEventColumns = allEventColumns;
	}
	
	var eventOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var eventGridSortcol = "eventName";
	var eventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromEventGrid(id) {
		eventDataView.deleteItem(id);
		eventGrid.invalidate();
	}
	/*
	function eventGridFilter(item, args) {
		var x = item["eventName"];
		if (args.eventGridSearchString != ""
				&& x.indexOf(args.eventGridSearchString) == -1) {
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function eventGridComparer(a, b) {
		var x = a[eventGridSortcol], y = b[eventGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function eventGridToggleFilterRow() {
		eventGrid.setTopPanelVisibility(!eventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#event_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){
		if(id=='event_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getEventsGridData(pageNo);
		}else if(id=='ticket_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(ticketPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(ticketPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(ticketPagingInfo.pageNum)-1;
			}
			getEventsGridData(pageNo);
		}else if(id=='hold_ticket_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(holdTicketsPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(holdTicketsPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(holdTicketsPagingInfo.pageNum)-1;
			}
			var eventId = $('#eventId').val();
			var eventDetailsStr;
			if(eventDetailHeader != null){
				eventDetailsStr = eventDetailHeader;
			}
			getHoldTicketsGridData(eventId, eventDetailsStr, pageNo);
		}else if(id=='locked_ticket_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(lockedTicketsPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(lockedTicketsPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(lockedTicketsPagingInfo.pageNum)-1;
			}
			var eventId = $('#eventId').val();
			var eventDetailsStr;
			if(eventDetailHeader != null){
				eventDetailsStr = eventDetailHeader;
			}
			getLockedTicketsGridData(eventId, eventDetailsStr, pageNo);
		}else if(id=='sold_ticket_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(soldTicketsPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(soldTicketsPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(soldTicketsPagingInfo.pageNum)-1;
			}
			var eventId = $('#eventId').val();
			var eventDetailsStr;
			if(eventDetailHeader != null){
				eventDetailsStr = eventDetailHeader;
			}
			getSoldTicketsGridData(eventId, eventDetailsStr, pageNo);
		}else if(id=='editHold_customer_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(holdTicketPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(holdTicketPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(holdTicketPagingInfo.pageNum)-1;
			}
			getEditHoldCustomerGridData(pageNo);
		}else if(id=='holdTicketsHistoyPager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(holdHistoryPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(holdHistoryPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(holdHistoryPagingInfo.pageNum)-1;
			}	
			getHoldTicketsHistoryGridData(pageNo);
		}else if(id=='custArtist_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custArtistPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custArtistPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custArtistPagingInfo.pageNum)-1;
			}
			getArtistGridData(pageNo);
		}else if(id=='custEvent_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custEventPagingInfo.pageNum)-1;
			}
			getCustEventsGridData(pageNo);
		}else if(id=='custRewardPoints_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custRewardPointsPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custRewardPointsPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custRewardPointsPagingInfo.pageNum)-1;
			}	
			getRewardPointsGridData(pageNo);
		}else if(id == "custInvoiceTicket_pager"){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum)-1;
			}
		}
	}
	
	function getEventsGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/GetEvents.json",
			type : "post",
			dataType: "json",
			data : $("#eventSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+eventGridSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} 
				pagingInfo = jsonData.pagingInfo;
				refreshEventGridValues(jsonData.events);
				refreshTicketGridValues('', '');
				$('#eventId').val('');
				createHoldTicketsGrid('', '');
				createLockedTicketsGrid('', '');
				refreshSoldTicketDetils('', '');
				clearAllSelections();
				$('#event_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserEventPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function refreshEventGridValues(jsonData) {
		eventData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (eventData[i] = {});
				d["id"] = i;
				d["eventMarketId"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["dayOfTheWeek"] = data.dayOfWeek;
				d["venue"] = data.building;
				d["venueId"] = data.venueId;
				d["noOfTix"] = data.noOfTixCount;
				d["noOfTixSold"] = data.noOfTixSoldCount;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["grandChildCategory"] = data.grandChildCategoryName;
				d["childCategory"] = data.childCategoryName;
				d["parentCategory"] = data.parentCategoryName;
				d["eventCreated"] = data.eventCreationStr;
				d["eventLastUpdated"] = data.eventUpdatedStr;
				d["notes"] = data.notes;
			}
		}

		eventDataView = new Slick.Data.DataView();
		eventGrid = new Slick.Grid("#event_grid", eventDataView, userEventColumns, eventOptions);
		eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		eventGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"),pagingInfo);
		}
		var eventGridColumnpicker = new Slick.Controls.ColumnPicker(allEventColumns, eventGrid,
				eventOptions);

		// move the filter panel defined in a hidden div into eventGrid top panel
		//$("#event_inlineFilterPanel").appendTo(eventGrid.getTopPanel()).show();

		eventGrid.onSort.subscribe(function(e, args) {
			eventGridSortcol = args.sortCol.field;			
			if(sortingString.indexOf(eventGridSortcol) < 0){
				eventGridSortdir = 'ASC';
			}else{
				if(eventGridSortdir == 'DESC' ){
					eventGridSortdir = 'ASC';
				}else{
					eventGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+eventGridSortcol+',SORTINGORDER:'+eventGridSortdir+',';
			getEventsGridData(0)
		});
		
		// wire up model events to drive the eventGrid
		eventDataView.onRowCountChanged.subscribe(function(e, args) {
			eventGrid.updateRowCount();
			eventGrid.render();
		});
		eventDataView.onRowsChanged.subscribe(function(e, args) {
			eventGrid.invalidateRows(args.rows);
			eventGrid.render();
		});
		
		$(eventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			eventGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  eventGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getEventsGridData(0);
				}
			  }
		 
		});
		eventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'dayOfTheWeek'){
					if(args.column.id == 'eventTime'){
						$("<input type='text' placeholder='hh:mm a'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else if(args.column.id == 'eventDate' || args.column.id == 'eventCreated' || args.column.id == 'eventLastUpdated'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}
		});
		eventGrid.init();
			
		eventGrid.onCellChange.subscribe(function (e,args) { 
				var temprEventRowIndex = eventGrid.getSelectedRows();
				var eventId;
				var eventNotes; 
				$.each(temprEventRowIndex, function (index, value) {
					eventId = eventGrid.getDataItem(value).eventMarketId;
					eventNotes = eventGrid.getDataItem(value).notes;
				});
				saveNotes(eventId,eventNotes);
         });
		/*
		// wire up the search textbox to apply the filter to the model
		$("#eventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			eventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			eventDataView.setFilterArgs({
				eventGridSearchString : eventGridSearchString
			});
			eventDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		eventDataView.beginUpdate();
		eventDataView.setItems(eventData);
		/*eventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			eventGridSearchString : eventGridSearchString
		});
		eventDataView.setFilter(eventGridFilter);*/
		eventDataView.endUpdate();
		eventDataView.syncGridSelection(eventGrid, true);
		$("#gridContainer").resizable();
		eventGrid.resizeCanvas();
		
		eventGrid.onContextMenu.subscribe(function (e) {
	      e.preventDefault();
	      var cell = eventGrid.getCellFromEvent(e);
		  eventGrid.setSelectedRows([cell.row]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if(height < $("#eventContextMenu").height()){
				height = e.pageY - $("#eventContextMenu").height();
			}else{
				height = e.pageY;
			}
			if(width < $("#eventContextMenu").width()){
				width =  e.pageX- $("#eventContextMenu").width();
			}else{
				width =  e.pageX;
			}
		 $("#eventContextMenu")
	          .data("row", cell.row)
	          .css("top", height)
	          .css("left", width)
	          .show();
	      $("body").one("click", function () {
	        $("#eventContextMenu").hide();
	      });
	    });
		
		var eventrowIndex;
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				 var eventId =eventGrid.getDataItem(temprEventRowIndex).eventMarketId;
				 var eventdetailStr = eventGrid.getDataItem(temprEventRowIndex).eventName+', '+eventGrid.getDataItem(temprEventRowIndex).eventDate+', '+
				eventGrid.getDataItem(temprEventRowIndex).eventTime+', '+eventGrid.getDataItem(temprEventRowIndex).venue;
				 
				 $('#AddTicketGroupSuccessMsg').hide();
				 eventDetailHeader = eventdetailStr;
				getCategoryTicketGroupsforEvent(eventId,eventdetailStr);
			}
		});
	}
	
	
		
	$("#eventContextMenu").click(function (e) {
		if (!$(e.target).is("li")) {
		  return;
		}
		
		/* if($(e.target).attr("data") == 'View Sold Tickets'){
			var eventId = getSelectedEventGridId();
			if(eventId>0){
				popupCenter("${pageContext.request.contextPath}/GetSoldTickets?eventId="+eventId,"Sold Tickets",'1000','450');
			}else{
				jAlert("Please select event to see sold tickets.");
			}
		}else */
		if($(e.target).attr("data") == 'View Zones Map/Tickets'){
			var eventId = getSelectedEventGridId();
			if(eventId>0){
				popupCenter("${pageContext.request.contextPath}/GetZoneMapTickets?eventId="+eventId,"Zones And Tickets",'1200','900');
			}else{
				jAlert("Please select event to see Tickets.");
			}
		}
		if($(e.target).attr("data") == 'BroadCast Tickets'){
			broadCastTickets("true");
		}
		if($(e.target).attr("data") == 'UnBroadCast Tickets'){
			broadCastTickets("false");
		}
	});
	
	function getSelectedEventGridId() {
		var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
		if(temprEventRowIndex > -1) {
			 return eventGrid.getDataItem(temprEventRowIndex).eventMarketId;
		}
	}
	
	
	function getCategoryTicketGroupsforEvent(eventId,eventdetailStr){		
		$('#availableTab').addClass('active');
		$('#available').addClass('active');
		$('#holdTab').removeClass("active");
		$('#hold').removeClass("active");
		$('#lockedTab').removeClass("active");
		$('#locked').removeClass("active");
		$('#soldTab').removeClass("active");
		$('#sold').removeClass("active");
		$.ajax({
			url : "${pageContext.request.contextPath}/getCategoryTicketGroups.json",
			type : "get",
			data : $("#ticketSearch").serialize()+"&eventId="+ eventId+"&headerFilter="+ticketGridSearchString,
			dataType:"json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				//jAlert(jsonData);
				if(jsonData==null || jsonData=="") {
					jAlert("No Tickets Found.");
				}
				ticketPagingInfo = jsonData.pagingInfo;
				refreshTicketGridValues(jsonData.tickets,eventdetailStr);
				clearAllSelections();
				$('#eventId').val(eventId);
				$('#ticket_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserTicketPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
    }
	
	function saveUserEventPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = eventGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('eventsgrid',colStr);
	}
	
	function saveUserTicketPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = ticketGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('ticketsgrid',colStr);
	}
	function saveNotes(eventId,notes){
		$.ajax({
			url : "${pageContext.request.contextPath}/saveEventNotes",
			type : "post",
			data : "eventId="+eventId+"&notes="+notes,
			success : function(res){
				//jAlert(res);
				if(res.msg != null && res.msg != "" && res.msg != undefined){
					jAlert(res.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function searchTicketData(){
		var eventId = $('#eventId').val();
		var eventDetailsStr = $('#ticketGridHeaderLabel').text();
		getCategoryTicketGroupsforEvent(eventId,eventDetailsStr);
	}
	
	function ticketExportToExcel(){	
		var eventId = $('#eventId').val();
		//var appendData = "sectionSearch="+$('#sectionSearch').val()+"&rowSearch="+$('#rowSearch').val()+"&seatSearch="+$('#seatSearch').val()+"&eventId="+eventId;
		var appendData = "eventId="+eventId+"&headerFilter="+ticketGridSearchString;
		appendData += "&brokerId="+$('#brokerId').val();
	    var url = apiServerUrl + "TicketsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function ticketResetFilters(){
		if($('#available').hasClass('active')){
			$('#status').val('Available');
		}
		ticketGridSearchString='';
		ticketGridcolumnFilters = {};
		var eventId = $('#eventId').val();
		var eventDetailsStr = $('#ticketGridHeaderLabel').text();
		getCategoryTicketGroupsforEvent(eventId,eventDetailsStr);
	}
	
	function holdTicketExportToExcel(){
		var eventId = $('#eventId').val();
		//var appendData = "sectionSearch="+$('#sectionSearch').val()+"&rowSearch="+$('#rowSearch').val()+"&seatSearch="+$('#seatSearch').val()+"&eventId="+eventId;
		var appendData = "eventId="+eventId+"&headerFilter="+holdTicketsSearchString;
		appendData += "&brokerId="+$('#brokerId').val();
	    var url = apiServerUrl + "HoldTicketsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	/* Hold Ticket Grid */

	var holdTicketsPagingInfo;
	var holdTicketsGrid;
	var holdTicketsDataView;
	var holdTicketsData=[];
	var holdTicketsSearchString = "";
	var holdTicketsColumnFilters = {};
	var holdTicketsColumn = [ /*{id:"customerId", name:"Customer Id", field: "customerId", sortable: true}, */
	               {id:"customerName", name:"Customer Name", field: "customerName", sortable: true},
	               {id:"salePrice", name:"Sale Price", field: "salePrice", sortable: true},
	               {id:"createdBy", name:"Created By", field: "createdBy", sortable: true},
	               {id:"expirationDate", name:"Expiration Date", field: "expirationDate", sortable: true},
	               {id:"expirationMinutes", name:"Expiration Minutes", field: "expirationMinutes", sortable: true},
	               /* {id:"shippingMethodId", name:"Shipping Method ID", field: "shippingMethodId", sortable: true},    */            
	               {id:"shippingMethod", name:"Shipping Method", field: "shippingMethod", sortable: true}, 
	               {id:"holdDate", name:"Hold Date", field: "holdDate", sortable: true},
	               {id:"internalNote", name:"Internal Notes", field: "internalNote", sortable: true},
	               {id:"externalNote", name:"External Notes", field: "externalNote", sortable: true},
	               {id:"ticketIds", name:"Ticket ID's", field: "ticketIds", sortable: true},
	               {id:"ticketGroupId", name:"Ticket Group ID", field: "ticketGroupId", sortable: true},
	               {id:"section", name:"Section", field: "section", sortable: true},
	               {id:"row", name:"Row", field: "row", sortable: true},
	               {id:"seatLow", name:"Seat Low", field: "seatLow", sortable: true},
	               {id:"seatHigh", name:"Seat High", field: "seatHigh", sortable: true},
	               {id:"externalPo", name:"External PO", field: "externalPo", sortable: true},
	               {id:"status", name:"Status", field: "status", sortable: true},
	               {id:"brokerId", name:"Broker ID", field:"brokerId", sortable:true}
	              ];
		
	var holdTicketsOptions = {	
	    enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};

	var sortcol = "customerName";
	var sortdir = 1;
	var percentCompleteThreshold = 0;

	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
			return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)){
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	function getHoldTicketsGridData(eventId, eventDetailsStr, pageNo) {			
			if(eventId != null && eventId > 0){
				$.ajax({
					url : "${pageContext.request.contextPath}/GetHoldTicketsForEvent",
					type : "post",
					data : "eventId="+eventId+"&pageNo="+pageNo+"&headerFilter="+holdTicketsSearchString,
					success : function(response){
						var jsonData = JSON.parse(JSON.stringify(response));					
						if(jsonData!='' && jsonData.status==0) {
							jAlert(jsonData.msg);
						}
						holdTicketsPagingInfo = jsonData.holdTicketsPagingInfo;					
						createHoldTicketsGrid(jsonData.holdTicketsList, eventDetailsStr);
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}else{
				jAlert("Select a event to view ticket details.");
			}
		}
	
	function createHoldTicketsGrid(jsonData, eventDetailsStr) {		
		if(eventDetailsStr != '0'){
			$('#holdGridHeaderLabel').text(eventDetailsStr);
		}
		holdTicketsData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (holdTicketsData[i] = {});
				d["id"] = i;
				d["customerId"] = data.customerId;
				d["customerName"] = data.customerName;
				d["salePrice"] = data.salePrice;
				d["createdBy"] = data.cSR;
				d["expirationDate"] = data.expirationDateStr;
				d["expirationMinutes"] = data.expirationMinutes;
				/* d["shippingMethodId"] = data.shippingMethodId; */
				d["shippingMethod"] = data.shippingMethod;
				d["holdDate"] = data.holdDateTimeStr;
				d["internalNote"] = data.internalNote;
				d["externalNote"] = data.externalNote;
				d["ticketIds"] = data.ticketIds;
				d["ticketGroupId"] = data.ticketGroupId;
				d["section"] = data.section;
				d["row"] = data.row;
				d["seatLow"] = data.seatLow;
				d["seatHigh"] = data.seatHigh;
				d["externalPo"] = data.externalPo;
				d["status"] = data.status;
				d["brokerId"] = data.brokerId;
			}
		}
		
		holdTicketsDataView = new Slick.Data.DataView();
		holdTicketsGrid = new Slick.Grid("#hold_ticket_grid", holdTicketsDataView, holdTicketsColumn, holdTicketsOptions);
		holdTicketsGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		holdTicketsGrid.setSelectionModel(new Slick.RowSelectionModel());				
		if(holdTicketsPagingInfo != null){
			var holdTicketGridPager = new Slick.Controls.Pager(holdTicketsDataView, holdTicketsGrid, $("#hold_ticket_pager"), holdTicketsPagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(holdTicketsColumn, holdTicketsGrid, holdTicketsOptions);
				
		  holdTicketsGrid.onSort.subscribe(function (e, args) {
		    sortdir = args.sortAsc ? 1 : -1;
		    sortcol = args.sortCol.field;
		    if ($.browser.msie && $.browser.version <= 8) {
		      holdTicketsDataView.fastSort(sortcol, args.sortAsc);
		    } else {
		      holdTicketsDataView.sort(comparer, args.sortAsc);
		    }
		  });
		  
		  // wire up model events to drive the holdTicketsGrid
		  holdTicketsDataView.onRowCountChanged.subscribe(function (e, args) {
		    holdTicketsGrid.updateRowCount();
		    holdTicketsGrid.render();
		  });
		  holdTicketsDataView.onRowsChanged.subscribe(function (e, args) {
		    holdTicketsGrid.invalidateRows(args.rows);
		    holdTicketsGrid.render();
		  });

			$(holdTicketsGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
			 	holdTicketsSearchString='';
				 var columnId = $(this).data("columnId");
				  if (columnId != null) {
					holdTicketsColumnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in holdTicketsColumnFilters) {
						  if (columnId !== undefined && holdTicketsColumnFilters[columnId] !== "") {
							 holdTicketsSearchString += columnId + ":" +holdTicketsColumnFilters[columnId]+",";
						  }
						}
						var eventId = $('#eventId').val();
						var eventDetailsStr = $('#ticketGridHeaderLabel').text();
						getHoldTicketsGridData(eventId, eventDetailsStr, 0);
					}
				  }		 
			});
			holdTicketsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id == 'expirationDate' || args.column.id == 'holdDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(holdTicketsColumnFilters[args.column.id])
					   .appendTo(args.node);
					}else{	
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(holdTicketsColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}			
			});
			holdTicketsGrid.init();	  	 
			 
		  holdTicketsDataView.beginUpdate();
		  holdTicketsDataView.setItems(holdTicketsData);
		  
		  holdTicketsDataView.endUpdate();
		  holdTicketsDataView.syncGridSelection(holdTicketsGrid, true);
		  holdTicketsDataView.refresh();
		  $("#gridContainer").resizable();
		  holdTicketsGrid.resizeCanvas();
	}	

	function holdResetFilters(){
		holdTicketsSearchString='';
		holdTicketsColumnFilters = {};
		var eventId = $('#eventId').val();
		var eventDetailsStr = $('#ticketGridHeaderLabel').text();
		getHoldTicketsGridData(eventId, eventDetailsStr, 0);
	}
</script>

<!-- event Grid Code Starts -->

<!-- <header class="panel-heading"> 
	<a data-toggle="modal" class="btn btn-primary" id="addCategoryTickets" onclick="addModal();">Add</a>
</header> -->

<!-- ticket Grid Code Starts -->
<br />
<div class="row">
	<div id="AddTicketGroupSuccessMsg" style="display: none;" class="alert alert-success fade in">
		Tickets Added/Updated Successfully.
	</div>
	<!--
	<div class="col-xs-12">		
			
		<form:form role="form" id="ticketSearch" method="post" class="full-width" onsubmit="return false" action="${pageContext.request.contextPath}/getCategoryTicketGroups">
			
		<div class="full-width mb-20 mt-20 full-width-btn">				
				<button type="button" class="btn btn-primary" id="addCategoryTickets" onclick="addModal();">Add Manual Ticket</button>
				<button type="button" class="btn btn-primary" id="createInvoice" onclick="createInvoicePopup();">Create Invoice</button>
				<button type="button" class="btn btn-primary" id="editCatTicket" onclick="editTicket();">Edit Ticket</button>
				<button type="button" class="btn btn-primary" id="holdTicketBtn" onclick="holdTicket();">Hold Ticket</button>
				<button type="button" class="btn btn-primary" id="holdedTicket" onclick="holdedTickets();">Hold Ticket History</button>				
		</div>			
		 	
		<div class="form-group col-xs-2 col-md-2">
			<label for="name" class="control-label">Section</label>
			<input class="form-control searchcontrol1" placeholder="Section" type="text" id="sectionSearch" name="sectionSearch" value="${sectionSearch}">
		</div>
		
		<div class="form-group col-xs-2 col-md-2">
			<label for="name" class="control-label">Row</label>
			<input class="form-control searchcontrol1" placeholder="Row" type="text" id="rowSearch" name="rowSearch" value="${rowSearch}">
		</div>
		
		<div class="form-group col-xs-2 col-md-2">
			<label for="name" class="control-label">Seat</label>
			<input class="form-control searchcontrol1" placeholder="Seat" type="text" id="seatSearch" name="seatSearch" value="${seatSearch}">
		</div>
		
		<div class="form-group col-xs-1 col-md-1">
			<button type="button" id="searchTicketBtn" class="btn btn-primary" style="margin-top:18px;" onclick="searchTicketData()">search</button>
		</div>
			 
		</form:form>			                
	</div> 
	 -->
 </div>
 
 <div id="ticketDiv" class="mt-20 full-width">
	<div class="row">
		<section class="col-xs-12 invoice-panel panel">
		<ul class="nav nav-tabs" style="">
			<li id="availableTab" class="active"><a id="availableTicket" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#available">Available Tickets</a></li>
			<li id="holdTab" class=""><a id="holdTicket" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#hold">Hold Tickets</a>
			<li id="lockedTab" class=""><a id="lockedTicket" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#locked">Locked Tickets</a></li>
			<li id="soldTab" class=""><a id="soldTicket" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#sold">Sold Tickets</a></li>
		</ul>
		</section>
	</div>
	<div class="full-width">
		<div class="tab-content">
			<div id="available" class="tab-pane active">							
				<div class="full-width mb-20 full-width-btn" id="actionButton">
					<button type="button" class="btn btn-primary" id="uploadTickets" onclick="showUploadTicketModal();">Upload Tickets</button>
					<button type="button" class="btn btn-primary" id="addCategoryTickets" onclick="addTicketModal();">Add Manual Ticket</button>
					<button type="button" class="btn btn-primary" id="createInvoice" onclick="createInvoicePopup();">Create Invoice</button>
					<button type="button" class="btn btn-primary" id="editCatTicket" onclick="editTicket();">Edit Ticket</button>
					<button type="button" class="btn btn-primary" id="holdTicketBtn" onclick="holdTicket();">Hold Ticket</button>
					<button type="button" class="btn btn-primary" id="broadcastTicketBtn" onclick="broadCastTicket();">BroadCast/UnBroadCast</button>						
				</div>
				<br />
				<br />
				<br />
				<div style="position: relative" id="ticketGridDiv" class="ticket-window">
					<div class="table-responsive grid-table long-width grid-table-2">
						<div class="grid-header full-width">
							<label id="ticketGridHeaderLabel">Ticket Details</label>
							<div class="pull-right">
								<a href="javascript:ticketExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:ticketResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>	
							</div>
						</div>
						<div id="ticket_grid"></div>
						<div id="ticket_pager"></div>
					</div>
				</div>
			</div>
			
			<div id="hold" class="tab-pane">								
				<div class="full-width mb-20 full-width-btn" id="actionButton">
					<button type="button" class="btn btn-primary" id="holdedTicket" onclick="holdedTickets();">Hold Ticket History</button>	
					<button type="button" class="btn btn-primary" id="createInvoice" onclick="createInvoicePopupForHold();">Create Invoice</button>								
					<button type="button" class="btn btn-primary" id="unHoldTicket" onclick="updateTicketHold('UNHOLD');">UnHold</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="ticketGridDiv" class="ticket-window">
					<div class="table-responsive grid-table">																	
						<div class="grid-header full-width">
							<label id="holdGridHeaderLabel">Hold Tickets</label>
							<div class="pull-right">
								<a href="javascript:holdTicketExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:holdResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
							</div>
						</div>
						<div id="hold_ticket_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="hold_ticket_pager" style="width: 100%; height: 20px;"></div>							
					</div>
				</div>
			</div>
			
			<div id="locked" class="tab-pane">
				<br />
				<br />
				<div style="position: relative" id="ticketGridDiv" class="ticket-window">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label id="lockedGridHeaderLabel">Locked Tickets</label>
							<div class="pull-right">
								<a href="javascript:lockedTicketExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:lockedResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>	
							</div>
						</div>
						<div id="locked_ticket_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="locked_ticket_pager" style="width: 100%; height: 20px;"></div>
					</div>
				</div>
			</div>
			
			<div id="sold" class="tab-pane">
				<br />
				<br />
				<div style="position: relative" id="ticketGridDiv" class="ticket-window">
					<div class="table-responsive grid-table">																	
						<div class="grid-header full-width">
							<label id="soldGridHeaderLabel">Sold Tickets</label>
							<div class="pull-right">
								<a href="javascript:soldTicketExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:soldResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
							</div>
						</div>
						<div id="sold_ticket_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="sold_ticket_pager" style="width: 100%; height: 20px;"></div>							
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- slickgrid related scripts -->

<script>
	var ticketCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	var ticketPagingInfo;
	var ticketDataView;
	var ticketGrid;
	var ticketData = [];
	var ticketGridSearchString='';
	var ticketGridcolumnFilters = {};
	var ticketColumnStr = '<%=session.getAttribute("ticketsgrid")%>';
	var userTicketColumns = [];
	var allTicketColumns = [ ticketCheckboxSelector.getColumnDefinition(),
	{	id : "section",
		name : "Section",
		field : "section",
		width:100,
		sortable : true
	}, {
		id : "row",
		name : "Row",
		field : "row",
		width:100,
		sortable : true
	}, {
		id : "seatLow",
		name : "Seat Low",
		field : "seatLow",
		width:100,
		sortable : true
	}, {
		id : "seatHigh",
		name : "Seat High",
		field : "seatHigh",
		width:100,
		sortable : true
	}, {
		id : "quantity",
		name : "Quantity",
		field : "quantity",
		width:100,
		sortable : true
	}, {
		id : "retailPrice",
		name : "Retail Price",
		field : "retailPrice",
		width:100,
		sortable : true
	}, {
		id : "wholeSalePrice",
		name : "WholeSale Price",
		width:100,
		field : "wholeSalePrice",
		sortable : true
	}, {
		id : "price",
		name : "Price",
		width:100,
		field : "price",
		sortable : true
	}, {
		id : "taxAmount",
		name : "Fees Amount",
		width:100,
		field : "taxAmount",
		sortable : true
	}, {
		id : "facePrice",
		name : "Face Price",
		width:100,
		field : "facePrice",
		sortable : true
	},{
		id : "cost",
		name : "cost",
		width:100,
		field : "cost",
		sortable : true
	},{
		id : "ticketType",
		name : "Ticket Type",
		width:100,
		field : "ticketType",
		sortable : true
	},{
		id : "shippingMethod",
		name : "Shipping Method",
		width:100,
		field : "shippingMethod",
		sortable : true
	},{id: "nearTermDisplayOption", 
		name: "NearTerm Display Option",
		width:80,		
		field: "nearTermDisplayOption", 
		sortable: true
	},{id: "broadcast", 
		name: "BroadCast", 
		field: "broadcast",
		width:80,		
		sortable: true
	}, {
		id : "sectionRange",
		name : "Section Range",
		field : "sectionRange",
		width:80,
		sortable : true
	}, {id: "rowRange", 
		name: "Row Range", 
		width:80,
		field: "rowRange", 
		sortable: true
	}, {id: "internalNotes", 
		name: "Internal Notes", 
		field: "internalNotes",
		width:80,
		editor:Slick.Editors.LongText,
		sortable: true
	},{id: "externalNotes", 
		name: "External Notes", 
		field: "externalNotes",
		width:100,
		editor:Slick.Editors.LongText,
		sortable: true
	},{id: "marketPlaceNotes", 
		name: "Market Place Notes", 
		field: "marketPlaceNotes", 
		width:100,
		editor:Slick.Editors.LongText,
		sortable: true
	},{id: "productType", 
		name: "Product Type",
		width:80,		
		field: "productType", 
		sortable: true
	},{ id:"brokerId", 
		name:"Broker ID", 
		field:"brokerId",
		sortable:true
	}, {id: "eventName", 
		name: "Event Name", 
		field: "eventName",
		width:100,
		sortable: true
	},{id: "eventDate", 
		name: "Event Date", 
		field: "eventDate",
		width:80,
		sortable: true
	},{id: "eventTime", 
		name: "Event Time", 
		field: "eventTime",
		width:80,
		sortable: true
	},{id: "venue", 
		name: "Venue", 
		field: "venue", 
		width:100,
		sortable: true
	}
	];

	if(ticketColumnStr!='null' && ticketColumnStr!=''){
		var columnOrder = ticketColumnStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allTicketColumns.length;j++){
				if(columnWidth[0] == allTicketColumns[j].id){
					userTicketColumns[i] =  allTicketColumns[j];
					userTicketColumns[i].width=(columnWidth[1]-5);
					console.log(userTicketColumns[i].id+"--"+userTicketColumns[i].width)
					break;
				}
			}
			
		}
	}else{
		userTicketColumns = allTicketColumns;
	}
	
	var ticketOptions = {
		editable: true,
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var ticketGridSortcol = "section";
	var ticketGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	//Now define your buttonFormatter function
	function buttonFormatter(row,cell,value,columnDef,dataContext){  
	     var button = "<input class='invoice' value='Invoice' type='button' id='"+ dataContext.id +"' />"; 
	    //var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.id +"'/>";
	    return button;
	}
	
	//function to hoo up the create invoice button
	$('.invoice').live('click', function(){
		var me = $(this), id = me.attr('id');
		popupCreateInvoice(id);
	});
	
	function deleteRecordFromEventGrid(id) {
		ticketDataView.deleteItem(id);
		ticketGrid.invalidate();
	}
	/*
	function ticketGridFilter(item, args) {
		var x= item["section"];
		if (args.ticketGridSearchString != ""
				&& x.indexOf(args.ticketGridSearchString) == -1) {
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function ticketGridComparer(a, b) {
		var x = a[ticketGridSortcol], y = b[ticketGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function ticketGridToggleFilterRow() {
		ticketGrid.setTopPanelVisibility(!ticketGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#ticket_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	
	*/
	function refreshTicketGridValues(jsonData,eventdetailStr) {
		if(eventdetailStr != '0') {
			$('#ticketGridDiv').show();
			$('#ticketGridHeaderLabel').text(eventdetailStr);
		}
		ticketData = [];
		if(jsonData!=null){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (ticketData[i] = {});
				d["id"] = data.id;
				d["section"] = data.section;
				d["row"] = data.row;
				d["seatLow"] = data.seatLow;
				d["seatHigh"] = data.seatHigh;
				d["quantity"] = data.quantity;
				d["retailPrice"] = data.retailPrice;
				d["wholeSalePrice"] = data.wholeSalePrice;
				d["price"] = data.price;
				d["taxAmount"] = data.taxAmount;
				d["facePrice"] = data.facePrice;
				d["cost"] = data.cost;
				d["ticketType"] = data.ticketType;
				d["shippingMethod"] = data.shippingMethod;
				d["nearTermDisplayOption"] = data.nearTermDisplayOption;
				if(data.broadcast != null && data.broadcast==1) {
					d["broadcast"] = 'Yes';
				} else {
					d["broadcast"] = 'No';
				}
				d["sectionRange"] = data.sectionRange;
				d["rowRange"] = data.rowRange;
				
				d["internalNotes"] = data.internalNotes;
				d["externalNotes"] = data.externalNotes;
				d["marketPlaceNotes"] = data.marketPlaceNotes;
				d["productType"] = data.productType;
				d["brokerId"] = data.brokerId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDate;
				d["eventTime"] = data.eventTime;
				d["venue"] = data.venue;
			
			}
		}
		
		ticketDataView = new Slick.Data.DataView();
		ticketGrid = new Slick.Grid("#ticket_grid", ticketDataView, userTicketColumns, ticketOptions);
		ticketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		ticketGrid.setSelectionModel(new Slick.RowSelectionModel());
		ticketGrid.registerPlugin(ticketCheckboxSelector);
		if(ticketPagingInfo!=null){
			var ticketGridPager = new Slick.Controls.Pager(ticketDataView, ticketGrid, $("#ticket_pager"),ticketPagingInfo);
		}
		
		var ticketGridColumnpicker = new Slick.Controls.ColumnPicker(allTicketColumns, ticketGrid,
				ticketOptions);

		// move the filter panel defined in a hidden div into ticketGrid top panel
		//$("#ticket_inlineFilterPanel").appendTo(ticketGrid.getTopPanel()).show();

		/* ticketGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < ticketDataView.getLength(); i++) {
				rows.push(i);
			}
			ticketGrid.setSelectedRows(rows);
			e.preventDefault();
		}); */
		
		ticketGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = ticketGrid.getCellFromEvent(e);
			  ticketGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		ticketGrid.onSort.subscribe(function(e, args) {
			ticketGridSortdir = args.sortAsc ? 1 : -1;
			ticketGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				ticketDataView.fastSort(ticketGridSortcol, args.sortAsc);
			} else {
				ticketDataView.sort(ticketGridComparer, args.sortAsc);
			}
		});
		// wire up model tickets to drive the ticketGrid
		ticketDataView.onRowCountChanged.subscribe(function(e, args) {
			ticketGrid.updateRowCount();
			ticketGrid.render();
		});
		ticketDataView.onRowsChanged.subscribe(function(e, args) {
			ticketGrid.invalidateRows(args.rows);
			ticketGrid.render();
		});
		$(ticketGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			ticketGridSearchString = '';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				ticketGridcolumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in ticketGridcolumnFilters) {
					  if (columnId !== undefined && ticketGridcolumnFilters[columnId] !== "") {
						  ticketGridSearchString += columnId + ":" +ticketGridcolumnFilters[columnId]+",";
					  }
					}
					var eventId = $('#eventId').val();
					var eventDetailsStr = $('#ticketGridHeaderLabel').text();
					getCategoryTicketGroupsforEvent(eventId,eventDetailsStr);
				}
			  }
		 
		});
		ticketGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'eventName' && args.column.id != 'eventDate' && args.column.id != 'eventTime' && args.column.id != 'venue'){
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(ticketGridcolumnFilters[args.column.id])
			   .appendTo(args.node);
			}
			}
		});
		ticketGrid.init();
			
		ticketGrid.onCellChange.subscribe(function (e,args) { 
			var item = args.item;
			var editedColumn =''; 
			var value='';
			var ticketId=item.id;
			if (args.cell == ticketGrid.getColumnIndex('internalNotes')) {
				editedColumn='internalNotes';
				value = item.internalNotes;
			} else if (args.cell == ticketGrid.getColumnIndex('externalNotes')) {
				editedColumn='externalNotes';
				value = item.externalNotes;
			} else if(args.cell == ticketGrid.getColumnIndex('marketPlaceNotes')) {
				editedColumn='marketPlaceNotes';
				value = item.marketPlaceNotes;
			}
			saveTicketNotes(ticketId,editedColumn,value);
	 });

		/*
		// wire up the search textbox to apply the filter to the model
		$("#ticketGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			ticketGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			ticketDataView.setFilterArgs({
				ticketGridSearchString : ticketGridSearchString
			});
			ticketDataView.refresh();
		}
		*/
		// initialize the model after all the tickets have been hooked up
		ticketDataView.beginUpdate();
		ticketDataView.setItems(ticketData);
		/*ticketDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			ticketGridSearchString : ticketGridSearchString
		});
		ticketDataView.setFilter(ticketGridFilter);*/
		ticketDataView.endUpdate();
		ticketDataView.syncGridSelection(ticketGrid, true);
		$("#gridContainer").resizable();

		 $("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'Edit Ticket'){
				editTicket();
			}else if($(e.target).attr("data") == 'Create Invoice'){
				createInvoicePopup();
			}
		});
		ticketGrid.resizeCanvas();
		
	}
	
	function getSelectedTicketGridId() {
		var tempTicketRowIndex = ticketGrid.getSelectedRows([0])[0];
		if(tempTicketRowIndex >= 0) {
			 return ticketGrid.getDataItem(tempTicketRowIndex).id;
		}
	}
	
	function saveTicketNotes(ticketId,editedColumn,value){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateTicketGroupNotes",
			type : "post",
			data : "ticketId="+ticketId+"&editColumn="+editedColumn+"&value="+value,
			success : function(res){
				//jAlert(res);
				if(res.msg != null && res.msg != "" && res.msg != undefined){
					jAlert(res.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var ticketBroadCast = new Array();
	function getSelectedTicketGridIds() {
		var tempTicketRowIndex = ticketGrid.getSelectedRows();
		ticketBroadCast = new Array();
		
		var ticketIdStr=''; var i=0;
		$.each(tempTicketRowIndex, function (index, value) {
			ticketIdStr += ','+ticketGrid.getDataItem(value).id;
			ticketBroadCast[i] = ticketGrid.getDataItem(value).broadcast; i++;
		});
		
		if(ticketIdStr != null && ticketIdStr!='') {
			ticketIdStr = ticketIdStr.substring(1, ticketIdStr.length);
			 return ticketIdStr;
		}
	}
	
	function find_duplicate_in_array(arra1) {
		  var i,
		  len=arra1.length,
		  result = [],
		  obj = {}; 
		  for (i=0; i<len; i++){
		  	obj[arra1[i]]=0;
		  }
		  for (i in obj) {
		  	result.push(i);
		  }
		  return result;
	}
	
	function broadCastTicket(){
		var ticketIds  = getSelectedTicketGridIds();
		var ticketBroadCastCheck = find_duplicate_in_array(ticketBroadCast);
		var broadcast;
		if(ticketBroadCastCheck.length > 1){
			jAlert("Please Select Tickets for only one action either Broadcast/Unbroadcast.");
			return;
		}else{
			if(ticketBroadCastCheck[0] == 'Yes'){
				broadcast = "false";
			}else{
				broadcast = "true";
			}
		}
		if(ticketIds=='' || ticketIds.length==0){
			jAlert("Please Select Ticket to add Broadcast/Unbroadcast.");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateTicketDetails.json",
				type : "post",
				dataType:"json",
				data : "ticketIds="+ticketIds+"&broadcast="+broadcast,
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));			
					if(jsonData.ticketMessage != undefined){
						jAlert(jsonData.ticketMessage);
						var eventId = $('#eventId').val();
						var eventDetailsStr = $('#ticketGridHeaderLabel').text();
						getCategoryTicketGroupsforEvent(eventId,eventDetailsStr);						
						clearAllSelections();
					}
					
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function broadCastTickets(broadcast){
		var eventId = getSelectedEventGridId();
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateTicketDetails.json",
			type : "post",
			dataType:"json",
			data : "eventId="+eventId+"&broadcast="+broadcast,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));			
				if(jsonData.eventMessage != undefined){
					jAlert(jsonData.eventMessage);
					getEventsGridData(0);
					clearAllSelections();
				}
				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	<!-- ticket Grid Code Ends -->
	
	//toggle the add manual tickets creation modal to add
	function addTicketModal(){
		$('#AddTicketGroupSuccessMsg').hide();
		var eventId = getSelectedEventGridId();
		if(eventId != null && eventId!='') {
			$('#eventId').val(eventId);
			categoryTicketFormUpdate(false,null);
			//$('#myModal').modal('show');	
		} else {
			jAlert("Select an Event to create tickets.");
		}
			
	}
	
//Sold Tickets Grid

	var soldTicketDataView;
	var soldTicketsPagingInfo;
	var soldTicketGrid;
	var soldTicketData = [];
	var soldTicketsSearchString='';
	var soldTicketsColumnFilters = {};
	var soldTicketColumnStr = '<%=session.getAttribute("ticketsgrid")%>';
	var soldTicketColumns = [ {id: "invoiceId", 
		name: "Invoice No", 
		field: "invoiceId", 
		sortable: true
	},{
		id : "section",
		name : "Section",
		field : "section",
		sortable : true
	}, {
		id : "row",
		name : "Row",
		field : "row",
		sortable : true
	}, {
		id : "seatLow",
		name : "Seat Low",
		field : "seatLow",
		sortable : true
	}, {
		id : "seatHigh",
		name : "Seat High",
		field : "seatHigh",
		sortable : true
	}, {
		id : "quantity",
		name : "Quantity",
		field : "quantity",
		sortable : true
	}, {
		id : "retailPrice",
		name : "Retail Price",
		field : "retailPrice",
		sortable : true
	}, {
		id : "wholeSalePrice",
		name : "WholeSale Price",
		field : "wholeSalePrice",
		sortable : true
	}, {
		id : "price",
		name : "Price",
		field : "price",
		sortable : true
	},{
		id : "facePrice",
		name : "Face Price",
		field : "facePrice",
		sortable : true
	},{
		id : "cost",
		name : "cost",
		field : "cost",
		sortable : true
	},{
		id : "shippingMethod",
		name : "Shipping Method",
		field : "shippingMethod",
		sortable : true
	},{id: "nearTermDisplayOption", 
		name: "NearTerm Display Option", 
		field: "nearTermDisplayOption", 
		sortable: true
	},{id: "broadcast", 
		name: "BroadCast", 
		field: "broadcast", 
		sortable: true
	}, {
		id : "sectionRange",
		name : "Section Range",
		field : "sectionRange",
		sortable : true
	}, {id: "rowRange", 
		name: "Row Range", 
		field: "rowRange", 
		sortable: true
	}, {id: "internalNotes", 
		name: "Internal Notes", 
		field: "internalNotes",
		editor:Slick.Editors.LongText,
		sortable: true
	},{id: "externalNotes", 
		name: "External Notes", 
		field: "externalNotes",
		sortable: true
	}, {id: "marketPlaceNotes", 
		name: "Market Place Notes", 
		field: "marketPlaceNotes", 
		sortable: true
	}];
	
	
	var soldTicketOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var soldTicketGridSortcol = "invoiceId";
	var soldTicketGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	
	function soldTicketGridComparer(a, b) {
		var x = a[soldTicketGridSortcol], y = b[soldTicketGridSortcol];
		if(!isNaN(x)){
		  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	function getSoldTicketsGridData(eventId, eventDetailsStr, pageNo) {			
			if(eventId != null && eventId > 0){
				$.ajax({
					url : "${pageContext.request.contextPath}/GetSoldTicketsForEvent.json",
					type : "post",
					data : "eventId="+eventId+"&pageNo="+pageNo+"&headerFilter="+soldTicketsSearchString,
					success : function(response){
						var jsonData = JSON.parse(JSON.stringify(response));					
						if(jsonData==null || jsonData=='') {
							jAlert("No Data Found.");
						}
						soldTicketsPagingInfo = jsonData.soldTicketsPagingInfo;					
						refreshSoldTicketDetils(jsonData.soldTicketsList, eventDetailsStr);
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}else{
				jAlert("Select a event to view ticket details");
			}
		}
		
	function refreshSoldTicketDetils(jsonData, eventDetailsStr){
		if(eventDetailsStr != '0'){
			$('#soldGridHeaderLabel').text(eventDetailsStr);
		}
		soldTicketData = [];
		if(jsonData!=null){			
			for (var i = 0; i < jsonData.length; i++) {				
				var data = jsonData[i];
				var d = (soldTicketData[i] = {});
				d["id"] = i;
				d["invoiceId"] = data.invoiceId;
				d["section"] = data.section;
				d["row"] = data.row;
				d["seatLow"] = data.seatLow;
				d["seatHigh"] = data.seatHigh;
				d["quantity"] = data.quantity;
				d["retailPrice"] = data.retailPrice;
				d["wholeSalePrice"] = data.wholeSalePrice;
				d["price"] = data.price;
				d["facePrice"] = data.facePrice;
				d["cost"] = data.cost;
				d["shippingMethod"] = data.shippingMethod;
				d["nearTermDisplayOption"] = data.nearTermDisplayOption;
				if(data.broadcast != null && data.broadcast==1) {
					d["broadcast"] = 'Yes';
				} else {
					d["broadcast"] = 'No';
				}
				d["sectionRange"] = data.sectionRange;
				d["rowRange"] = data.rowRange;
				d["internalNotes"] = data.internalNotes;
				d["externalNotes"] = data.externalNotes;
				d["marketPlaceNotes"] = data.marketPlaceNotes;
			}
		}				
	
	soldTicketDataView = new Slick.Data.DataView();
	soldTicketGrid = new Slick.Grid("#sold_ticket_grid", soldTicketDataView, soldTicketColumns, soldTicketOptions);
	soldTicketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	soldTicketGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(soldTicketsPagingInfo!=null){
		var soldTicketGridPager = new Slick.Controls.Pager(soldTicketDataView, soldTicketGrid, $("#sold_ticket_pager"),soldTicketsPagingInfo);
	}
	
	var soldTicketGridColumnPicker = new Slick.Controls.ColumnPicker(soldTicketColumns, soldTicketGrid,
			soldTicketOptions);
	
	 soldTicketGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < soldTicketDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    soldTicketGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	 
	soldTicketGrid.onSort.subscribe(function(e, args) {
		soldTicketGridSortdir = args.sortAsc ? 1 : -1;
		soldTicketGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			soldTicketDataView.fastSort(soldTicketGridSortcol, args.sortAsc);
		} else {
			soldTicketDataView.sort(soldTicketGridComparer, args.sortAsc);
		}
	});
	// wire up model tickets to drive the soldTicketGrid
	soldTicketDataView.onRowCountChanged.subscribe(function(e, args) {
		soldTicketGrid.updateRowCount();
		soldTicketGrid.render();
	});
	soldTicketDataView.onRowsChanged.subscribe(function(e, args) {
		soldTicketGrid.invalidateRows(args.rows);
		soldTicketGrid.render();
	});
	
	$(soldTicketGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	soldTicketsSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				soldTicketsColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in soldTicketsColumnFilters) {
					  if (columnId !== undefined && soldTicketsColumnFilters[columnId] !== "") {
						 soldTicketsSearchString += columnId + ":" +soldTicketsColumnFilters[columnId]+",";
					  }
					}
					var eventId = $('#eventId').val();
					var eventDetailsStr = $('#ticketGridHeaderLabel').text();
					getSoldTicketsGridData(eventId, eventDetailsStr, 0);
				}
			  }		 
		});
	soldTicketGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){			
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(soldTicketsColumnFilters[args.column.id])
			   .appendTo(args.node);
			}			
		});
	soldTicketGrid.init();	 
	// initialize the model after all the tickets have been hooked up
	soldTicketDataView.beginUpdate();
	soldTicketDataView.setItems(soldTicketData);
	
	soldTicketDataView.endUpdate();
	soldTicketDataView.syncGridSelection(soldTicketGrid, true);
	soldTicketDataView.refresh();
	$("#gridContainer").resizable();
	soldTicketGrid.resizeCanvas();
}

function soldResetFilters(){
	soldTicketsSearchString='';
	soldTicketsColumnFilters = {};
	var eventId = $('#eventId').val();
	var eventDetailsStr = $('#ticketGridHeaderLabel').text();
	getSoldTicketsGridData(eventId, eventDetailsStr, 0);
}

function soldTicketExportToExcel(){
	var eventId = $('#eventId').val();
	//var appendData = "sectionSearch="+$('#sectionSearch').val()+"&rowSearch="+$('#rowSearch').val()+"&seatSearch="+$('#seatSearch').val()+"&eventId="+eventId;
	var appendData = "eventId="+eventId+"&headerFilter="+soldTicketsSearchString;
	appendData += "&brokerId="+$('#brokerId').val();
    var url = apiServerUrl + "SoldTicketsExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}	

//Locked Tickets Grid	
	
var lockedTicketsPagingInfo;
var lockedTicketsGrid;
var lockedTicketsDataView;
var lockedTicketsData=[];
var lockedTicketsSearchString = "";
var lockedTicketsColumnFilters = {};
var lockedTicketsColumn = [ {
	id : "section",
	name : "Section",
	field : "section",
	sortable : true
}, {
	id : "row",
	name : "Row",
	field : "row",
	sortable : true
}, {
	id : "seatLow",
	name : "Seat Low",
	field : "seatLow",
	sortable : true
}, {
	id : "seatHigh",
	name : "Seat High",
	field : "seatHigh",
	sortable : true
}, {
	id : "quantity",
	name : "Quantity",
	field : "quantity",
	sortable : true
}, {
	id : "retailPrice",
	name : "Retail Price",
	field : "retailPrice",
	width:100,
	sortable : true
}, {
	id : "wholeSalePrice",
	name : "WholeSale Price",
	field : "wholeSalePrice",
	sortable : true
}, {
	id : "price",
	name : "Price",
	field : "price",
	sortable : true
},{
	id : "taxAmount",
	name : "Tax Amount",
	field : "taxAmount",
	sortable : true
},{
	id : "facePrice",
	name : "Face Price",
	field : "facePrice",
	sortable : true
},{
	id : "cost",
	name : "cost",
	field : "cost",
	sortable : true
},{
	id : "ticketType",
	name : "Ticket Type",
	field : "ticketType",
	sortable : true
},{
	id : "shippingMethod",
	name : "Shipping Method",
	field : "shippingMethod",
	sortable : true
},{id: "nearTermDisplayOption", 
	name: "NearTerm Display Option",
	field: "nearTermDisplayOption", 
	sortable: true
},{id: "broadcast", 
	name: "BroadCast", 
	field: "broadcast",
	sortable: true
}, {
	id : "sectionRange",
	name : "Section Range",
	field : "sectionRange",
	sortable : true
}, {id: "rowRange", 
	name: "Row Range",
	field: "rowRange", 
	sortable: true
}, {id: "internalNotes", 
	name: "Internal Notes", 
	field: "internalNotes",
	editor:Slick.Editors.LongText,
	sortable: true
},{id: "externalNotes", 
	name: "External Notes", 
	field: "externalNotes",
	editor:Slick.Editors.LongText,
	sortable: true
},{id: "marketPlaceNotes", 
	name: "Market Place Notes", 
	field: "marketPlaceNotes", 
	editor:Slick.Editors.LongText,
	sortable: true
},	{id: "productType", 
	name: "Product Type",
	field: "productType", 
	sortable: true
},{id: "eventName", 
	name: "Event Name", 
	field: "eventName",
	sortable: true
},{id: "eventDate", 
	name: "Event Date", 
	field: "eventDate",
	sortable: true
},{id: "eventTime", 
	name: "Event Time", 
	field: "eventTime",
	sortable: true
},{id: "venue", 
	name: "Venue", 
	field: "venue",
	sortable: true
}, 
               /* {id:"categoryTicketGroupId", name:"Category Ticket Group ID", field: "categoryTicketGroupId", sortable: true}, */
               {id:"ipAddress", name:"Customer IP", field: "ipAddress", sortable: true},
               {id:"platform", name:"Platform", field: "platform", sortable: true},
               {id:"creationDate", name:"Created Date", field: "creationDate", sortable: true}
               /*{id:"lockStatus", name:"Status", field: "lockStatus", sortable: true}*/
              ];
              
var lockedTicketsOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "categoryTicketGroupId";
var sortdir = 1;
var percentCompleteThreshold = 0;

function lockedTicketComparer(a, b) {
	var x = a[sortcol], y = b[sortcol];
	if(!isNaN(x)){
		return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
    if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
 

function getLockedTicketsGridData(eventId, eventDetailsStr, pageNo) {	
	if(eventId != null && eventId > 0){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetLockedTicketsForEvent.json",
			type : "post",
			data : "eventId="+eventId+"&pageNo="+pageNo+"&headerFilter="+lockedTicketsSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				}
				lockedTicketsPagingInfo = jsonData.lockedTicketsPagingInfo;
				createLockedTicketsGrid(jsonData.lockedTicketsList, eventDetailsStr);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}else{
		jAlert("Select a event to view ticket details");
	}
}
	
function createLockedTicketsGrid(jsonData, eventDetailsStr) {	
	if(eventDetailsStr != '0'){
		$('#lockedGridHeaderLabel').text(eventDetailsStr);
	}
	lockedTicketsData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (lockedTicketsData[i] = {});
			d["id"] = i;
			d["section"] = data.section;
			d["row"] = data.row;
			d["seatLow"] = data.seatLow;
			d["seatHigh"] = data.seatHigh;
			d["quantity"] = data.quantity;
			d["retailPrice"] = data.retailPrice;
			d["wholeSalePrice"] = data.wholeSalePrice;
			d["price"] = data.price;
			d["taxAmount"] = data.taxAmount;
			d["facePrice"] = data.facePrice;
			d["cost"] = data.cost;
			d["ticketType"] = data.ticketType;
			d["shippingMethod"] = data.shippingMethod;
			d["nearTermDisplayOption"] = data.nearTermDisplayOption;
			if(data.broadcast != null && data.broadcast==1) {
				d["broadcast"] = 'Yes';
			} else {
				d["broadcast"] = 'No';
			}
			d["sectionRange"] = data.sectionRange;
			d["rowRange"] = data.rowRange;
			
			d["internalNotes"] = data.internalNotes;
			d["externalNotes"] = data.externalNotes;
			d["marketPlaceNotes"] = data.marketPlaceNotes;
			d["productType"] = data.productType;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
			d["venue"] = data.venue;
			/* d["categoryTicketGroupId"] = data.categoryTicketGroupId; */
			d["ipAddress"] = data.ipAddress;
			d["platform"] = data.platform;
			d["creationDate"] = data.creationDate;
			/*d["lockStatus"] = data.lockStatus;*/
		}
	}
					
	lockedTicketsDataView = new Slick.Data.DataView();
	lockedTicketsGrid = new Slick.Grid("#locked_ticket_grid", lockedTicketsDataView, lockedTicketsColumn, lockedTicketsOptions);
	lockedTicketsGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	lockedTicketsGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(lockedTicketsPagingInfo != null){
		var lockedTicketGridPager = new Slick.Controls.Pager(lockedTicketsDataView, lockedTicketsGrid, $("#locked_ticket_pager"),lockedTicketsPagingInfo);
	}
	var lockedTicketGridColumnpicker = new Slick.Controls.ColumnPicker(lockedTicketsColumn, lockedTicketsGrid, lockedTicketsOptions);
	 
	  lockedTicketsGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      lockedTicketsDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      lockedTicketsDataView.sort(lockedTicketComparer, args.sortAsc);
	    }
	  });
	  
	  // wire up model events to drive the lockedTicketsGrid
	  lockedTicketsDataView.onRowCountChanged.subscribe(function (e, args) {
	    lockedTicketsGrid.updateRowCount();
	    lockedTicketsGrid.render();
	  });
	  lockedTicketsDataView.onRowsChanged.subscribe(function (e, args) {
	    lockedTicketsGrid.invalidateRows(args.rows);
	    lockedTicketsGrid.render();
	  });

		$(lockedTicketsGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	lockedTicketsSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				lockedTicketsColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in lockedTicketsColumnFilters) {
					  if (columnId !== undefined && lockedTicketsColumnFilters[columnId] !== "") {
						 lockedTicketsSearchString += columnId + ":" +lockedTicketsColumnFilters[columnId]+",";
					  }
					}
					var eventId = $('#eventId').val();
					var eventDetailsStr = $('#ticketGridHeaderLabel').text();
					getLockedTicketsGridData(eventId, eventDetailsStr, 0);
				}
			  }		 
		});
		lockedTicketsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'creationDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(lockedTicketsColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id != 'eventName' && args.column.id != 'eventDate' && args.column.id != 'eventTime' && args.column.id != 'venue'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(lockedTicketsColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}			
		});
		lockedTicketsGrid.init();	  	 
		 
	  lockedTicketsDataView.beginUpdate();
	  lockedTicketsDataView.setItems(lockedTicketsData);
	  
	  lockedTicketsDataView.endUpdate();
	  lockedTicketsDataView.syncGridSelection(lockedTicketsGrid, true);
	  lockedTicketsDataView.refresh();
	  $("#gridContainer").resizable();
	  lockedTicketsGrid.resizeCanvas();
}	

function lockedResetFilters(){	
	lockedTicketsSearchString='';
	lockedTicketsColumnFilters = {};	
	var eventId = $('#eventId').val();
	var eventDetailsStr = $('#ticketGridHeaderLabel').text();
	getLockedTicketsGridData(eventId, eventDetailsStr, 0);
}

function lockedTicketExportToExcel(){
	var eventId = $('#eventId').val();
	//var appendData = "sectionSearch="+$('#sectionSearch').val()+"&rowSearch="+$('#rowSearch').val()+"&seatSearch="+$('#seatSearch').val()+"&eventId="+eventId;
	var appendData = "eventId="+eventId+"&headerFilter="+lockedTicketsSearchString;
    var url = apiServerUrl + "LockedTicketsExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function showUploadTicketModal(){
	$('#uploadTicketModal').modal('show');
}

function uploadTicketFile(){
	var validFilesTypes = ["xls", "xlsx","csv"];
	var file = $('#ticketFile').val();
	if(file==null || file== ''){
		jAlert("Please Select file to upload tickets");
		return;
	}
    var ext = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
	var isValidFile = false;
	for (var i = 0; i < validFilesTypes.length; i++) {
        if (ext == validFilesTypes[i]) {
            isValidFile = true;
            break;
        }
    }
	if(!isValidFile){
		jAlert("Please select valid Excel file.");
		return;
	}
	//$('#uploadTicketForm').submit();
	
	 var form = $('#uploadTicketForm')[0];
     var data = new FormData(form);
	
	 $.ajax({
		url : apiServerUrl + "UploadTickets",
		type : "post",
		enctype:"multipart/form-data",
		 processData : false,
         contentType : false,
         cache : false,
		data : data,
		success : function(res){
			$('#uploadTicketModal').modal('hide');
			jAlert(res);
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	}); 
}

</script>
<form class="form-horizontal" id="uploadTicketForm" enctype="multipart/form-data" action ="${pageContext.request.contextPath}/UploadTickets" method="post" >
	<div class="modal fade" id="uploadTicketModal" tabindex="-1" role="dialog" aria-labelledby="uploadTicketModal" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content full-width">
               <div class="modal-header full-width">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Upload Tickets</h4>
               </div>
                
	               <div class="modal-body full-width">
						<div class="full-width tab-fields">
							<div class="form-group col-md-4 col-sm-6 col-xs-12">
								<label>Upload file</label> 
								<input class="form-control" id="ticketFile" name="ticketFile" type="file" />
							</div>
						</div>
						<div class="full-width text-center">
							<%TrackerUser trackerUser = (TrackerUser)request.getSession().getAttribute("trackerUser"); %>
							<input type="hidden" id="trackerSessionUser" name="trackerSessionUser" value="<%=trackerUser.getFirstName() %>" />
						   <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
						   <button class="btn btn-success" type="button" onclick="uploadTicketFile();" >Upload</button>
					   </div>
	               </div>
	               
          	</div>
        </div>
     </div>
</form>
	<!-- Toggle add shipping address form-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                      <div class="modal-content full-width">
                                          <div class="modal-header full-width">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title">Add/Edit Category Tickets - Event Id : <span id="eventId_Hdr_CatTicket" class="headerTextClass"></span></h4>
                                          </div>
                                          <div class="modal-body full-width">
					
                               <form role="form" class="full-width" id="addCategoryTicketForm" method="post" action="${pageContext.request.contextPath}/AddCategoryTickets">
							 	<input id="action" value="create" name="action" type="hidden" />
							 	<input type="hidden" name="eventId" id="eventId"/>
							 	<input type="hidden" name="id"  id="id" value=""/>
							 	<%-- <input class="form-control" id="custId" value="${customerId}" name="custId" type="hidden" /> --%>
								<div class="full-width tab-fields">
									<div class="form-group col-sm-4 col-xs-6">
										<label>Section <span class="required">*</span>
										</label> <input class="form-control" id="section" name="section"
											type="text" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Row <span class="required">*</span>
										</label> <input class="form-control" id="row" name="row"
											type="text" value="" />
									</div>

									<div class="form-group col-sm-4 col-xs-6">
										<label>Quantity <span class="required">*</span>
										</label> <input class="form-control" id="quantity" type="text"
											name="quantity" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Section Range
										</label> <input class="form-control" id="sectionRange" name="sectionRange"
											type="text" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Row Range
										</label> <input class="form-control" id="rowRange" name="rowRange"
											type="text" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Seat Low
										</label> <input class="form-control" id="seatLow" name="seatLow"
											type="text" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Seat High
										</label> <input class="form-control" id="seatHigh" name="seatHigh"
											type="text" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Price</label> <input class="form-control"
											id="price" type="text" name="price"
											value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Fees Amount</label> <input class="form-control"
											id="taxAmount" type="text" name="taxAmount"
											value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Shipping Method</label> <!-- <input class="form-control"
											id="shippingMethod" type="text" name="shippingMethod"
											value="" /> -->
											<select id="shippingMethod" name="shippingMethod" class="form-control ">
											  <option value="-1">--select--</option>
											  <c:forEach items="${shippingMethods}" var="shippingMethod">
												<option value="${shippingMethod.id}">
													 ${shippingMethod.name}
												</option>
											</c:forEach>
											</select>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>NearTerm Display Option</label> <input class="form-control"
											id="nearTermDisplayOption" type="text" name="nearTermDisplayOption"
											value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Internal Notes</label> <input class="form-control"
											id="internalNotes" type="text" name="internalNotes"
											value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>External Notes</label> <input class="form-control"
											id="externalNotes" type="text" name="externalNotes"
											value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>MarketPlace Notes</label> <input class="form-control"
											id="marketPlaceNotes" type="text" name="marketPlaceNotes"
											value="" />
									</div>
							            <div class="form-group col-sm-4 col-xs-6">
							            	<label>Broadcast</label>
							            	<select id="broadcast" name="broadcast" class="form-control input-sm m-bot15">
                                              <option value="1">Yes</option>
                                              <option value="0">No</option>
                                          </select>
								         </div>
								      </div>
	                                         <!--  </div> -->
                                          
                                          </form>
                                      </div>
									  <div class="modal-footer full-width text-center">
											<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                            <button class="btn btn-success" type="button" onclick="addCategoryTicekt();" >Save</button>
									  </div>
                                  </div>
                              </div>
                              </div>
							  <!-- popup myZoneTicket -->
								<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myZoneTicket">View Zones Map/Tickets</button> -->

								<div id="myZoneTicket" class="modal fade" role="dialog">
								  <div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content full-width">
									  <div class="modal-header full-width">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">View Zones Map/Tickets</h4>
									  </div>
									  <div class="modal-body full-width">
										
											<div class="full-width">
												<div class="col-xs-12">
													<div class="svgContainer" style="z-index: 10">
														<div id="svgMap" class="col-xs-12 col-sm-6 col-md-8" style="">
															<svg id="svg1" height="710" version="1.1" width="900" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"
																viewBox="0 0 2650 2048" preserveAspectRatio="xMinYMin" align="left"> <desc style='-webkit-tap-highlight-color: rgba(0, 0, 0, 0);'>Created with Rapha�l 2.1.4</desc> <defs
																style='-webkit-tap-highlight-color: rgba(0, 0, 0, 0);'></defs> <image id='mapImg' x='0' y='0' width='2560' height='2048' preserveAspectRatio='none' xmlns:xlink='http://www.w3.org/1999/xlink'
																xlink:href='${ticketList.event.svgMapPath}' style='-webkit-tap-highlight-color: rgba(0, 0, 0, 0);' stroke-width='1'></image> ${ticketList.event.svgText} <img id="svgZoomIn"
																style="cursor: pointer; float: right; bottom: 90%; position: absolute; right: 95%; width: 30px !important; height: 30px !important;" src="../resources/images/zoomIn.jpg" /> &nbsp <img
																style="cursor: pointer; float: right; bottom: 83%; position: absolute; right: 95%; width: 30px !important; height: 30px !important;" id="svgZoomOut" src="../resources/images/zoomOut.jpg" /> </svg>
														</div>
													</div>
												</div>
												<div class="ticket-price" style="display: none">
													<ul id="tickets">
														<c:forEach var="ticket1" items="${ticketList.ticketGroupQtyList}">
															<c:if test="${ticket1.qty == 0 }">
																<c:forEach var="ticket" items="${ticket1.categoryTicketGroups}" varStatus="loopStatus">
																	<li data-zone-qty="${ticket.quantity}" data-zone="${ticket.svgKey}" data-color="${ticket.colorCode}" id="tr_${ticket.svgKey}-:-${ticket.colorCode}"></li>
																</c:forEach>
															</c:if>
														</c:forEach>
													</ul>
												</div>
											</div>
											<div class="full-width mb-20">
												<div class="table-responsive grid-table mt-20">
													<div class="grid-header full-width">
														<label>Tickets</label>
													</div>
													<div id="ticket_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
													<div id="ticket_pager" style="width: 100%; height: 10px;"></div>
												</div>
											</div>
									  </div>
									  <div class="modal-footer full-width text-center">
										<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
									  </div>
									</div>

								  </div>
								</div>
								<!-- End popup myZoneTicket -->
								
								<!-- popup create invoice -->
								<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#create-invoice">Create Invoice</button> -->

								<div id="create-invoice" class="modal fade" role="dialog">
								  <div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content full-width">
									  <div class="modal-header full-width">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Create Invoice - Event Id : <span id="eventId_Hdr_CreateInvoice" class="headerTextClass"></span></h4>
									  </div>
									  <div class="modal-body full-width">
										
										<div class="row">
											<div class="col-xs-12">
												 
												<ol class="breadcrumb">
													<li><i class="fa fa-laptop"></i>Create Invoice</li>
												</ol>
												
											</div>
										</div>

										<div class="row">
										<div class="col-xs-12">
										<h3 style="font-size: 13px;font-family: arial;" class="page-header">Search Customer</h3>		
											 <div class="form-group">
												<button type="button" class="btn btn-primary" style="float:right;"  onclick="addNewCustomer();">Add New Customer</button>
											 </div>
										  </div>
										</div>
										<br/>
										<div class="full-width" style="position: relative;" id="customerGridDiv">
											<div class="table-responsive grid-table">
												<div class="grid-header full-width">
													<label style="font-size: 10px;">Customers Details</label>
													<div class="pull-right">
													<a href="javascript:resetFiltersCreateInvoice()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
													</div>
												</div>
												<div id="ci_customer_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
												<div id="ci_customer_pager" style="width: 100%; height: 10px;"></div>
											</div>
										</div>

										<br />
										<br />
										<form class="form-horizontal full-width" onsubmit="return false;" id="invoiceForm" method="post" action="SaveInvoice">
										<div class="row">
											<div class="col-xs-12">
												<section class="panel full-width"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
												Customer Billing Details
												</header>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Customer Name:</label>
															<span style="font-size: 12px;" id="customerNameDiv"></span>
															<input type="hidden" name="shFirstName" id="shFirstName" value="" />
															<input type="hidden" name="shLastName" id="shLastName" value="" />
															<input type="hidden" name="blFirstName" id="blFirstName" value="" />
															<input type="hidden" name="blLastName" id="blLastName" value="" />
															<input type="hidden" name="isLongSale" id="isLongSale"/>
															<input type="hidden" name="promoTrackingId" id="promoTrackingId"/>
															<input type="hidden" name="affPromoTrackingId" id="affPromoTrackingId"/>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Email:</label>
															<input type="hidden" name="blEmail" id="blEmail" value="" />
															<span id=emailDiv></span>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Phone:</label><span id=phoneDiv></span>
															<input type="hidden" name="phone" id="phone" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Address Line:</label><span id=addressLineDvi></span>
															<input type="hidden" name="addressLine1" id="addressLine1" value="" />
															<input type="hidden" name="addressLine2" id="addressLine2" value="" />
														</div>
													</div>
													
													<div class="form-group">
														<div class="col-sm-3"> 
															<label style="font-size: 13px;font-weight:bold;">Country:</label><span id=countryDiv></span>
															<input type="hidden" name="country" id="country" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">State:</label><span id=stateDiv></span>
															<input type="hidden" name="state" id="state" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">City:</label><span id=cityDiv></span>
															<input type="hidden" name="city" id="city" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">ZipCode:</label><span id=zipCodeDiv></span>
															<input type="hidden" name="zipCode" id="zipCode" value="" />
														</div>
													</div>
													
												</div>
												</section>
											</div>
										</div>

										<!-- Shipping Address section -->
										<div class="row">
											<div class="col-xs-12">
												<section class="panel full-width">
												 <header style="font-size: 13px;font-family: arial;" class="panel-heading">Shipping Address
												  &nbsp;&nbsp;&nbsp;&nbsp; 
												  <input name="isBilling" id="isBilling" value="" type="checkbox" onclick = "copyInvoiceBillingAddress();"/> 
															Same as billing address
												  &nbsp;&nbsp;&nbsp;&nbsp;
												 <a href="javascript:showShippingAddress();" id="showShAddr" style="font-size: 13px;font-family: arial;"/>View/Change Shipping Address</a>
												 
												 </header>
													<div class="panel-body">
													<div class="form-group">
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">Address Line 1:</label><span id=shAddressLine1Div></span>
															<input type="hidden" name="shAddressLine1" id="shAddressLine1" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">Address Line 2:</label><span id=shAddressLine2Div></span>
															<input type="hidden" name="shAddressLine2" id="shAddressLine2" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">Phone:</label><span id=shPhoneDiv></span>
															<input type="hidden" name="shPhone" id="shPhone" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">Country:</label><span id=shCountryDiv></span>
															<input type="hidden" name="shCountry" id="shCountry" value="" />
														</div>
													</div>
													<div class="form-group">
														
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">State:</label><span id=shStateDiv></span>
															<input type="hidden" name="shState" id="shState" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">City:</label><span id=shCityDiv></span>
															<input type="hidden" name="shCity" id="shCity" value="" />
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px; font-weight: bold;">ZipCode:</label><span id=shZipCodeDiv></span>
															<input type="hidden" name="shZipCode" id="shZipCode" value="" />
														</div>
													</div>
												</div>
												</section>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<section class="panel full-width"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="eventDetailDiv">
												 </header>
												<div class="panel-body">
												<input type="hidden" name="action" id="createInvoiceAction" />
												<input type="hidden" name="eventId" id="createInvoiceEventId" />
												<input type="hidden" name="eventName" id="eventName" />
												<input type="hidden" name="eventDate" id="eventDate" />
												<input type="hidden" name="eventTime" id="eventTime" />
												<input type="hidden" name="venueId" id="venueId" />
												<input type="hidden" name="createInvoiceType" id="createInvoiceType" />
												<input type="hidden" name="bAddressId" id="bAddressId" value="" />
												<input type="hidden" name="sAddressId" id="sAddressId" value="" />
												<input type="hidden" name="shippingIsSameAsBilling" id="shippingIsSameAsBilling" value="No" />
												<input type="hidden" name="holdTicketCustomerId" id="holdTicketCustomerId" />
												<input type="hidden" name="customerId" id="customerIdDiv" value=""/>
												<input type="hidden" name="ticketIdStr" id="ticketIdStr" value="" />
												<input type="hidden" name="holdTicketIds" id="holdTicketIds" />
												<div id="ticketInfoDiv">
													<!-- <input type="hidden" name="section" id="section" />
													<input type="hidden" name="row" id="row" />
													<input type="hidden" name="ticketPrice" id="ticketPrice" />
													<input type="hidden" name="loyalFanPrice" id="loyalFanPrice" />
													<input type="hidden" name="productType" id="productType" />
													<input type="hidden" name="shippingMethodId" id="shippingMethodId" />
													<input type="hidden" name="ticketId" id="ticketId" />
													<input type="hidden" name="ticketIdStr" id="ticketIdStr" />
													<div class="form-group">
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Section:</label>
															<span style="font-size: 12px;" id="ciSection"></span>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Row:</label>
															<span id="ciRow"></span>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Product Type :</label>
															<span id="ciProducType"></span>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Shipping Method : </label><span id="ciShippingMethod"></span>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Quantity :</label>
															<span id="ciQuantity"></span>
															<input type="text" class="form-control" id="ticketCount1" name="ticketCount1" onblur="onQuantityChange();">
															<input type="hidden" class="form-control" id="ticketCount" name="ticketCount" value="">
															
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Price:</label><span id="ciTicketPrice"></span>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Fees Amount :</label>
															<span id="ciTaxAmount"></span>
															<input type="text" class="form-control" id="createInvoiceTaxAmount" name="taxAmount" onchange="soldPriceChange(false)"> 
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Fees Amount</label>
															<input type="text" class="form-control" id="createInvoiceTaxAmount" name="taxAmount" onblur="soldPriceChange(false);">
														</div>
													</div> -->
												</div>
												</div>
												</section>
												</div>
												</div>	
										<div class="row">
											<div class="col-xs-12">
												<section class="panel full-width"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="paymentDetailsDiv">
													Payment Details
												 </header>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Active Reward Points : </label>
															<span id=activeRewardPoints></span>
															<input type="hidden" id="customerRewardPoints" name = "customerRewardPoints"/>
														</div>
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Wallet Credit : </label>
															<span id=walletCreditText></span>
															<input type="hidden" id="walletCredit" name ="walletCredit"/>
														</div>
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Earned Reward Points : </label>
															<span id=earnedRewardPoints></span>
															<input type="hidden" id="earnRewardPoint" name ="earnRewardPoint"/>
														</div>
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Loyal Fan Price Applicable : </label>
															<span id=loyalFanPriceText></span>
															<input type="hidden" id="isLoyalFanPrice" name ="isLoyalFanPrice"/>
														</div>
													</div>
													
													<div class="form-group">
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Payment Method : </label>
															<select id="paymentMethod" name="paymentMethod" class="form-control input-sm m-bot15" onchange="changePaymentMethod()">
															  <option value="CREDITCARD">Credit Card</option>
																<option value="REWARDPOINTS">Reward Points</option>
																<option value="WALLET">Wallet</option>
																<option value="WALLET_CREDITCARD">Wallet + Credit Card</option>
																<option value="PARTIAL_REWARD_CC">Partial Reward + Credit Card</option>
																<option value="PARTIAL_REWARD_WALLET">Partial Reward + Wallet</option>
																<option value="PARTIAL_REWARD_WALLET_CC">Partial Reward + Wallet + Credit Card</option>
																<option value="ACCOUNT_RECIVABLE">Account Receivable</option>
															</select>
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Promotional/Referral Code :</label>
															<input type="text" class="form-control" id="referralCode" name="referralCode" onblur="doValidateReferralCode();">
														</div>
														<div class="col-sm-3">
															<label style="font-size: 13px;font-weight:bold;">Order Type : </label>
															<select id="orderType" name="orderType" class="form-control input-sm m-bot15" >
															  <option value="REGULAR">REGULAR ORDER</option>
																<option value="CONTEST">CONTEST ORDER</option>
															</select>
														</div>
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Total Payable Amount : </label>
															<span id=totalPayableAmount></span>
															<input type="hidden" id="payableAmount" name ="payableAmount"/>
														</div>
														<!-- <div class="col-sm-3">		
															<button type="button" onclick="doValidateReferralCode();"  style="margin-top:23px;" class="btn btn-primary">Validate</button>
														</div>	 
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Active Reward Points : </label>
															<span id=activeRewardPoints></span>
															<input type="hidden" id="customerRewardPoints" name = "customerRewardPoints"/>
														</div>
														<div class="col-sm-3">		
															<label style="font-size: 13px;font-weight:bold;">Total Payable Amount : </label>
															<span id=totalPayableAmount></span>
															<input type="hidden" id="payableAmount" name ="payableAmount"/>
														</div>	-->			
													</div>
													<div class="form-group" id="creditCardDiv">
														<div class="col-sm-3" class="cardInfo">
															<label style="font-size: 13px;font-weight:bold;">Available Cards : </label>
															<select id="custCards" name="custCards" class="form-control input-sm m-bot15">
																<option value="0">Get New Card</option>
															</select>
															<input type="hidden" id="sTempToken" name = "sTempToken"/>
															<input type="hidden" id="sBySavedCard" name = "sBySavedCard"/>
															<input type="hidden" id="sSavedCardId" name = "sSavedCardId"/>
														</div>
														
														<div class="col-sm-3" class="cardInfo">
															<label style="font-size: 13px;font-weight:bold;">Card Number :</label>
															<input type="text" class="form-control" id="cardNumber" name="cardNumber" data-stripe="number">
														</div>
														
														<div class="col-sm-3" class="cardInfo">
															<label style="font-size: 13px;font-weight:bold;">Expiry Month :</label>
															<select id="expMonth" name="expMonth" class="form-control input-sm m-bot15">
																<c:forEach begin="01" end="12" var="expirtMonth">
																	<option value="${expirtMonth}">${expirtMonth}</option>
																</c:forEach> 
															</select>
														</div>
														
														<div class="col-sm-3" class="cardInfo">
															<label style="font-size: 13px;font-weight:bold;">Expiry Year :</label>
															<c:set value="true" var="yearFlag"></c:set>
															<c:set value="${currentYear + 1 }" var="currentYearInc"></c:set>
															<select id="expYear" name="expYear" class="form-control input-sm m-bot15">
															  <c:forEach begin="1" end="20">
															  
																<c:choose>
																	<c:when test="${yearFlag == true}">
																		<option value="${currentYear}">${currentYear}</option>
																		<c:set value="false" var="yearFlag"></c:set>
																	</c:when>
																	<c:otherwise>
																		<option value="${currentYearInc}">
																			${currentYearInc }
																			<c:set value="${currentYearInc + 1 }" var="currentYearInc"></c:set>
																		</option>
																	</c:otherwise>
																</c:choose>
															  </c:forEach>
															</select>
														</div>
														<div class="col-sm-3" class="cardInfo">
															<label style="font-size: 13px;font-weight:bold;">Name in the card :</label>
															<input type="text" class="form-control" id="name" name="name" data-stripe="name">
														</div>
														<div class="col-sm-3" class="cardInfo">
															<label style="font-size: 13px;font-weight:bold;">Cvv :</label>
															<input type="text" class="form-control" id="cvv" name="cvv" data-stripe="cvc">					
														</div>
														</div>
														<div class="form-group">
														<div class="col-sm-3" id="cardAmountDiv" style="display:none">
															<label style="font-size: 13px;font-weight:bold;">Card Amount :</label>
															<input type="text" class="form-control" id="cardAmount" name="cardAmount" onchange="calRewardPoints()">
														</div>
														<div class="col-sm-3" id="rewardPointsDiv" style="display:none">
															<label style="font-size: 13px;font-weight:bold;">Reward Points :</label>
															<input type="text" class="form-control" id="rewardPoints" name="rewardPoints" >					
														</div>
														<div class="col-sm-3" id="walletAmountDiv" style="display:none">
															<label style="font-size: 13px;font-weight:bold;">Wallet Credit Amount :</label>
															<input type="text" class="form-control" id="walletAmount" name="walletAmount" onchange="calRewardPoints()">					
														</div>
														<input type="hidden" size="12" id="stripeExp"  data-stripe="exp">
														<input type="hidden"  id="cvvMaxDigit" name="cvvMaxDigit" /> 				
													</div>
													
														</form>
												</div>
												</section>
												</div>
											</div>

									  </div>
									  <div class="modal-footer full-width text-center">
											<div id="POAction" class="col-xs-12 text-center">
												<button type="button" id="createInvoiceBtn" style="display: none;" onclick="doSaveInvoice();" class="btn btn-primary">Save</button>
												<button data-dismiss="modal" class="btn btn-cancel" onclick="cancelAction();"
													type="button">Close</button>
											</div>
										</div>
									</div>

								  </div>
								</div>
								<!-- End popup create invoice -->
								
								<!-- popup Add New Customer -->
									<%@include file="body-add-new-customer.jsp" %>
								<!-- End popup Add New Customer -->
																
								<!-- popup View Customer Details -->
									<%@include file="body-view-customer-details.jsp"%>
								<!-- End popup View Customer Details -->

								<!-- popup Hold Ticket -->
								<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#hold-ticket">Hold Ticket</button> -->

								<div id="hold-ticket" class="modal fade" role="dialog">
								  <div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content full-width">
									  <div class="modal-header full-width">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Hold Ticket - Event Id : <span id="eventId_Hdr_HoldTicket" class="headerTextClass"></span></h4>
									  </div>
									  <div class="modal-body full-width">
										<style>
											.slick-headerrow-column {
											  background: #87ceeb;
											  text-overflow: clip;
											  -moz-box-sizing: border-box;
											  box-sizing: border-box;
											}
											.slick-headerrow-column input {
											  margin: 0;
											  padding: 0;
											  width: 100%;
											  height: 100%;
											  -moz-box-sizing: border-box;
											  box-sizing: border-box;
											}
										</style>
										<div class="row">
											<div class="col-xs-12">
												<h3 class="page-header">
													<i class="fa fa-laptop"></i> Events
												</h3>
												<ol class="breadcrumb">
													<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Tickets</a></li>
													<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Hold/Unhold Tickets</li>
												</ol>
											</div>
										</div>

										<br />
										<div class="full-width hold-ticket-table" style="position: relative" id="ticket">
											<div class="table-responsive grid-table mb-20">
												<div class="grid-header full-width">
													<label>Tickets</label>
												</div>
												<div id="editHold_ticket_grid" style="width: 100%; height: 200px; border: 1px solid gray;"></div>
												<div id="editHold_ticket_pager" style="width: 100%; height: 20px;"></div>
												
											</div>
										</div>	
										<br/><br/>
										<div class="full-width manage-event-filter">
											<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
												<label for="name" class="control-label">Expiry Date</label>
												<input class="form-control searchcontrol" type="text" id="expDate" name="expDate">
												<input type="hidden" name="holdTicketId" id="holdTicketId">
											</div>
											<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
												<label for="name" class="control-label">Expiry Minutes</label>
												<input class="form-control searchcontrol" type="text" id="expMinutes" name="expMinutes">
											</div>
											<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
												<label for="name" class="control-label">Sales Price</label>
												<input class="form-control searchcontrol" type="text" id="holdSalesPrice" name="holdSalesPrice">
											</div>
										</div>
										<br/><br/>
										<br/><br/>
										<div class="full-width" style="position: relative;">
											<div class="table-responsive grid-table mb-20">
												<div class="grid-header full-width">
													<label style="font-size: 10px;">Customers Details</label>
													<div class="pull-right">
													<a href="javascript:resetEditHoldTicketFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
													</div>
												</div>
												<div id="editHold_customer_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
												<div id="editHold_customer_pager" style="width: 100%; height: 20px;"></div>
											</div>
										</div>
										<br/><br/>
										<div class="full-width mb-20 text-center">
											
											<!-- <button type="button" class="btn btn-primary" onclick="updateTicketHold('UNHOLD')">Unhold</button> -->
										</div>
									  </div>
										<div class="modal-footer full-width">
											<button type="button" class="btn btn-primary" onclick="updateTicketHold('HOLD')">Hold</button>
											<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
										</div>
									</div>

								  </div>
								</div>
								<!-- End popup Hold Ticket -->
								
								
								<!-- popup Edit Hold Ticket -->
								<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-hold-ticket">Edit Hold Ticket</button> -->

								<div id="edit-hold-ticket" class="modal fade" role="dialog">
								  <div class="modal-dialog modal-lg">

									<!-- Modal content-->
									<div class="modal-content full-width">
									  <div class="modal-header full-width">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Hold Ticket History - Event Id : <span id="eventId_Hdr_HoldTicketHistory" class="headerTextClass"></span></h4>
									  </div>
									  <div class="modal-body full-width">
										<style>
											.cell-title {
												  font-weight: bold;
												}
												.cell-effort-driven {
												  text-align: center;
												}
												.slick-headerrow-column {
												 background: #87ceeb;
												 text-overflow: clip;
												 -moz-box-sizing: border-box;
												 box-sizing: border-box;
												}
												.slick-headerrow-column input {
													 margin: 0;
													 padding: 0;
													 width: 100%;
													 height: 100%;
													 -moz-box-sizing: border-box;
													 box-sizing: border-box;
												}
												.cell-selection {
												  border-right-color: silver;
												  border-right-style: solid;
												  background: silver;
												  color: gray;
												  text-align: right;
												  font-size: 10px;
												}
												.slick-row.selected .cell-selection {
												  background-color: transparent; /* show default selected row background */
												}
												
											</style>
											<div class="row">
												<div class="col-xs-12">
													<h3 class="page-header">
														<i class="fa fa-laptop"></i> Events
													</h3>
													<ol class="breadcrumb">
														<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Events</a>
														</li>
														<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Holded Tickets</li>
													</ol>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12">
													<div style="position: relative">
														<div class="table-responsive grid-table">
															<div class="grid-header full-width">
																<label>Hold/UnHold Tickets</label>
																<div class="pull-right">
																<a href="javascript:resetHoldHistoryFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
																</div>
															</div>
															<div id="holdTicketsHistoryGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
															<div id="holdTicketsHistoyPager" style="width: 100%; height: 20px;"></div>
														</div>
													</div>
												</div>
											</div>
											<br/><br/>
									  </div>
										<div class="modal-footer full-width">
											<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
										</div>
									</div>

								  </div>
								</div>
								<!-- End Edit Hold Ticket -->
								
								<!-- popup View/Change Shipping Address -->
								<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#change-address">View/Change Shipping Address</button> -->

								<div id="change-address" class="modal fade" role="dialog">
								  <div class="modal-dialog modal-lg">
									<!-- Modal content-->
									<div class="modal-content full-width">
										<div class="modal-header full-width">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">View/Change Shipping Address</h4>
										</div>
										<div class="modal-body full-width">
											<div class="row">
												<div class="col-xs-12">
													<h3 class="page-header">
														<i class="fa fa-laptop"></i> Accounting
													</h3>
													<ol class="breadcrumb">
														<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
														</li>
														<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Shipping Address</li>
													</ol>
												</div>
											</div>
											


											<div class="full-width mt-20" style="position: relative">
												<div class="table-responsive grid-table">
													<div class="grid-header full-width">
														<label>Customer Shipping/Other Addresses</label> <!-- <span id="shipping_grid_toogle_search" style="float: right"
															class="ui-icon ui-icon-search" title="Toggle search panel"
															onclick="shippingToggleFilterRow()"></span> -->
													</div>
													<div id="custShippingGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
													<div id="shipping_pager" style="width: 100%; height: 20px;"></div>
												</div>
											</div>

											
											<!-- <div class="col-xs-12">
												<div class="form-group full-width mt-20" align="center">
													<button type="button" class="btn btn-primary" onclick="updateShippingAddress();">Save</button>
												</div>
											</div> -->
										
											<div class="form-group full-width" align="center" id="shippingAddressMsg" style="dislpay:none;">
												<!-- <div class="col-xs-12">
													<label style="font-size: 15px;font-weight:bold;">No Shipping address found, Please add shipping Address.</label>
												</div> -->
											</div>
										</div>
										<div class="modal-footer full-width">
											
												<input type="hidden" name="productType" id="productType" value="${productType}" />
												<button type="button" class="btn btn-primary" onclick="updateShippingAddress();">Save</button>
												<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
											
										</div>
									</div>

								  </div>
								</div>
							<!-- End popup View/Change Shipping Address -->
<script type="text/javascript">



function editTicket() {
	$('#AddTicketGroupSuccessMsg').hide();
	var tempTicketRowIndex = ticketGrid.getSelectedRows([0])[0];
	if(tempTicketRowIndex >= 0) {
		var type = ticketGrid.getDataItem(tempTicketRowIndex).ticketType;
		var ticketId = ticketGrid.getDataItem(tempTicketRowIndex).id;
		if(type=='Real'){
			jAlert("You can update only Category Tickets, Not Allowed to Update Real ticket.");
		}else{			
			//var eventId = getSelectedEventGridId();
			//$('#eventId').val(eventId);
				$.ajax({
					url : "${pageContext.request.contextPath}/CheckLockedTickets.json",
					type : "get",
					data : "ticketId="+ticketId,
					/* async : false, */
					success : function(response){
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.msg == "true"){
							editCategoryTicket(ticketId);
						}else{
							jAlert(jsonData.msg,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
	} else {
		jAlert("Select a Ticket to update.");
	}
}

function holdTicket(){
	var tempTicketRowIndex = ticketGrid.getSelectedRows([0])[0];
	if(tempTicketRowIndex >= 0) {
		var type = ticketGrid.getDataItem(tempTicketRowIndex).ticketType;
		var ticketId = ticketGrid.getDataItem(tempTicketRowIndex).id;
		if(type=='Real'){
			$.ajax({
				url : "${pageContext.request.contextPath}/Invoice/HoldTicket.json",
				type : "post",
				data :"ticketGroupId="+ticketId,
				dataType:"json",
				success : function(res){
					var jsonData = res;
					if(jsonData.status == 1){
						$('#hold-ticket').modal('show');
						$('#holdTicketId').val(jsonData.ticketGroupId);
						editHoldPagingInfo = JSON.parse(jsonData.holdPagingInfo);
						refreshEditHoldTicketGridValues(JSON.parse(jsonData.tickets));
						resetEditHoldTicketFilters();
						clearAllSelections();
						$('#expDate').val('');
						$('#expMinutes').val('');
						$('#holdSalesPrice').val('');
						//getEditHoldCustomerGridData(0);
						var eventId = getSelectedEventGridId();
						$('#eventId_Hdr_HoldTicket').text(eventId);
					}else{
						jAlert("There is something wrong. Please try again");
					}
				}, error : function(error){
					jAlert("There is something wrong. Please try again"+error,"Error");
					return false;
				}
			});
			//var holdTicketUrl = "${pageContext.request.contextPath}/Invoice/HoldTicket?ticketGroupId="+ticketId;
			//popupCenter(holdTicketUrl, 'Hold Ticket', '800', '700');
		}else{
			jAlert("Please Select Real Ticket, Cannot hold category tickets.");
			return;
		}
	}else{
		jAlert("Please select a Real Ticket.");
		return;
	}
		
}

function holdedTickets(){
	resetHoldHistoryFilters();
	var eventId = getSelectedEventGridId();
	$('#eventId_Hdr_HoldTicketHistory').text(eventId);
	//getHoldTicketsHistoryGridData(0);
	//var holdedTicketUrl = "${pageContext.request.contextPath}/EditHoldTickets";
	//popupCenter(holdedTicketUrl, 'Holded Ticket', '800', '500');
}

function editCategoryTicket(ticketId) {
	$.ajax({
		  
		url : "${pageContext.request.contextPath}/getCategoryTicketGroupById",
		type : "post",
		data : "action=create&ticketId="+ticketId,
		dataType:"json",
		success : function(res){
			//var jsonData = JSON.parse(JSON.stringify(res));
			var jsonDatas = res;
			var jsonData = jsonDatas.categoryTicket;
			if(jsonData==null || jsonData=="") {
				jAlert("No Tickets Found.");
			} else {
				categoryTicketFormUpdate(true,jsonData);
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
function categoryTicketFormUpdate(isUpdateForm,jsonData) {
	if(isUpdateForm) {
		$('#id').val(jsonData.id);
		$('#section').val(jsonData.section);
		$('#row').val(jsonData.row);
		$('#quantity').val(jsonData.quantity);
		$('#sectionRange').val(jsonData.sectionRange);
		$('#rowRange').val(jsonData.rowRange);
		$('#seatLow').val(jsonData.seatLow);
		$('#seatHigh').val(jsonData.seatHigh);
		$('#price').val(jsonData.price);
		$('#taxAmount').val(jsonData.taxAmount);
		if(jsonData.shippingMethod!=null && jsonData.shippingMethod!='null' && jsonData.shippingMethod >= 0){
			$('#shippingMethod').val(jsonData.shippingMethod);
		}
		$('#nearTermDisplayOption').val(jsonData.nearTermDisplayOption);
		$('#internalNotes').val(jsonData.internalNotes);
		$('#externalNotes').val(jsonData.externalNotes);
		$('#marketPlaceNotes').val(jsonData.marketPlaceNotes);
		
		if(jsonData.broadcast=='0') {
			$('#broadcast').val('0');	
		} else {
			$('#broadcast').val('1');
		}
		
	} else {
		$('#id').val('');
		$('#section').val('');
		$('#row').val('');
		$('#quantity').val('');
		$('#seatLow').val('');
		$('#seatHigh').val('');
		$('#price').val('');
		$('#taxAmount').val('');
		$('#sectionRange').val('');
		$('#rowRange').val('');
		$('#shippingMethod').val(-1);
		$('#nearTermDisplayOption').val('');
		$('#internalNotes').val('');
		$('#externalNotes').val('');
		$('#marketPlaceNotes').val('');
		$('#broadcast').val('');
	}	
	$('#myModal').modal('show');
	var eventId = getSelectedEventGridId();
	$('#eventId_Hdr_CatTicket').text(eventId);
}
function addCategoryTicekt() {

	var flag =true;
	var intRegex = /^\d+$/;
	var decimalRegex = /^\d+(\.\d{0,2})?$/;
	
	var section = $('#section').val();
	if(section == null || section =='') {
		jAlert('Please enter valid section.');
		$('#section').focus();
		flag = false;
		return false;
	}
	var qty = $('#quantity').val();
	if(qty == null || qty =='' || !intRegex.test(qty)) {
		jAlert('Please enter valid quantity.');
		$('#quantity').focus();
		flag = false;
		return false;
	}
	/* var sectionRange = $('#sectionRange').val();
	if(sectionRange == null || sectionRange =='') {
		jAlert('Please enter valid section range.');
		$('#sectionRange').focus();
		flag = false;
		return false;
	}
	var rowRange = $('#rowRange').val();
	if(rowRange == null || rowRange =='') {
		jAlert('Please enter valid row range.');
		$('#rowRange').focus();
		flag = false;
		return false;
	} */
	var price = $('#price').val();
	if(price == null || price =='' || !decimalRegex.test(price)) {
		jAlert('Please enter valid price.');
		$('#price').focus();
		flag = false;
		return false;
	}
	
	var taxAmount = $('#taxAmount').val();
	if(taxAmount != '' && !decimalRegex.test(taxAmount)) {
		jAlert('Please enter valid Tax Amount.');
		$('#taxAmount').focus();
		flag = false;
		return false;
	}
	
	var eventId=$('#eventId').val();
	if(flag) {
		$.ajax({
			url : "${pageContext.request.contextPath}/AddCategoryTickets.json",
			type : "post",
			data : $("#addCategoryTicketForm").serialize(),
			//dataType:"application/json",
			/* async : false, */
			success : function(res){
				$('#myModal').modal('hide');	
				$('#AddTicketGroupSuccessMsg').show();
				//$('#AddTicketGroupSuccessMsg').text(res);
				getCategoryTicketGroupsforEvent(eventId,'0');
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

}

//poupup create Invoice
function createInvoicePopup() {
	$('#AddTicketGroupSuccessMsg').hide();
	var tempTicketRowIndexs = ticketGrid.getSelectedRows([0]);
	var count = 0;
	var ticketIds='';
	var isValid=true;
	var type;
	if(tempTicketRowIndexs.length == 1) {
		ticketIds = ticketGrid.getDataItem(tempTicketRowIndexs[0]).id;
		type = ticketGrid.getDataItem(tempTicketRowIndexs[0]).ticketType;
	}else if(tempTicketRowIndexs.length > 1){
		for(var i =0;i<tempTicketRowIndexs.length;i++){
			type = ticketGrid.getDataItem(tempTicketRowIndexs[i]).ticketType;
			ticketIds += ticketGrid.getDataItem(tempTicketRowIndexs[i]).id+',';
			if(type == 'Category'){
				isValid = false;
				jAlert('Please select only one category ticket to create invoice. Invoice with multiple ticket allowed for only real tickets.');
				return;
			}
		}
	}
		
		
		
		/* var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					if(eventCheckbox[i].checked == true){
						count++;
					}
				}
			}
		} */	
		if(isValid){
			//if(broadcast == 'Yes' || broadcast == 'YES'){
				if(type=='Real'){
					$.ajax({
						url : "${pageContext.request.contextPath}/Invoice/CreateInvoiceRealTicket",
						type : "post",
						dataType:"json",
						data : "ticketGroupId="+ticketIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							if(jsonData!='' && jsonData.status == 1){
								$('#create-invoice').modal('show');
								resetFiltersCreateInvoice();
								resetCreateInvoiceData();
								createTicketDiv(jsonData);
								fillCreateInvoiceData(jsonData, '');
								changePaymentMethod();
								var eventId = getSelectedEventGridId();
								$('#eventId_Hdr_CreateInvoice').text(eventId);
							}else{
								jAlert(jsonData.msg);
								return;
							}
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
					//var createPOUrl = "${pageContext.request.contextPath}/Invoice/CreateInvoiceRealTicket?ticketGroupId="+ticketId;
					//popupCenter(createPOUrl, 'Create Invoice Real Ticket', '1200', '800');
				}else{
					$.ajax({
						url : "${pageContext.request.contextPath}/Invoice/CreateInvoice",
						type : "post",
						dataType:"json",
						data : "ticketId="+ticketIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							if(jsonData!='' && jsonData.status == 1){
								$('#create-invoice').modal('show');
								resetFiltersCreateInvoice();
								resetCreateInvoiceData();
								createTicketDiv(jsonData);
								fillCreateInvoiceData(jsonData, '');
								changePaymentMethod();
								var eventId = getSelectedEventGridId();
								$('#eventId_Hdr_CreateInvoice').text(eventId);
							}else{
								jAlert(jsonData.msg);
								return;
							}
							
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
					//var createPOUrl = "${pageContext.request.contextPath}/Invoice/CreateInvoice?ticketId="+ticketId;
					//popupCenter(createPOUrl, 'Create Invoice', '1200', '800');
				}
			}
		}

function createInvoicePopupForHold() {
	$('#AddTicketGroupSuccessMsg').hide();
	var tempTicketRowIndex = holdTicketsGrid.getSelectedRows([0])[0];
	var count = 0;
	if(tempTicketRowIndex >= 0) {
		var ticketId = holdTicketsGrid.getDataItem(tempTicketRowIndex).ticketGroupId;
		var custId = holdTicketsGrid.getDataItem(tempTicketRowIndex).customerId;
		var ticketIds = holdTicketsGrid.getDataItem(tempTicketRowIndex).ticketIds;
		var salePrice = holdTicketsGrid.getDataItem(tempTicketRowIndex).salePrice;
		$.ajax({
			url : "${pageContext.request.contextPath}/Invoice/CreateInvoiceHoldTicket",
			type : "post",
			dataType:"json",
			data : "ticketGroupId="+ticketId+"&ticketIds="+ticketIds+"&salePrice="+salePrice,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData!='' && jsonData.status == 1){
					$('#create-invoice').modal('show');
					resetCreateInvoiceData();
					createTicketDiv(jsonData);
					fillCreateInvoiceData(jsonData, custId);
					changePaymentMethod();
					var eventId = getSelectedEventGridId();
					$('#eventId_Hdr_CreateInvoice').text(eventId);								
				}else{
					jAlert(jsonData.msg);
					return;
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
		
	} else {
		jAlert("Select a Ticket to create Invoice.");
	}
}


	//show the pop window center
	function popupCenter(url, title, w, h) {  
	    // Fixes dual-screen position                         Most browsers      Firefox  
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
	              
	    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
	    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
	              
	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
	  
	    // Puts focus on the newWindow  
	    if (window.focus) {  
	        newWindow.focus();  
	    }  
	}
	
	jQuery(document).ready(function () {
		var winSize = jQuery(window).width();
		if (winSize <= 991) {
			setTimeout(function () {
				var grid_top2 = jQuery('.grid-table-2').offset().top + 90;
				jQuery('ul#contextMenu').css('top',grid_top2);
			}, 4000);
		}
	});

	function clearAllSelections(){
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
	}
	
</script>
	
</body>
</html>