
<script src="../resources/js/app/customerCreation.js"></script>
<script src="../resources/js/app/getCityStateCountry.js"></script>

<!-- popup Add New Customer -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#add-new-customer2">Add New Customer</button> -->
<div id="add-new-customer2" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New Customer</h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Customers
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="${pageContext.request.contextPath}/Client/ManageDetails">Manage Details</a>
							</li>
							<li><i class="fa fa-laptop"></i>Add Customer</li>
						</ol>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<section class="panel full-width mb-10"> <header class="panel-heading full-width">
						Fill Customer Info </header>
						<div class="panel-body">
								<div id="addCust_successDiv" class="alert alert-success fade in" style="display:none;">
									<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
									<span id="addCust_successMsg"></span></strong>
								</div>					
								<div id="addCust_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
									<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
									<span id="addCust_errorMsg"></span></strong>
								</div>
							<form role="form" class="row" id="addCustomerForm" method="post" action="${pageContext.request.contextPath}/Client/AddCustomer">
								<input id="addCust_action" value="addCustomer" name="action" type="hidden" />
								<input id="addCust_isPopup" value="${isPopup}" name="isPopup" type="hidden" />
								<div class="tab-fields full-width mt-10">
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Type <span class="required">*</span> </label>
										<select path="customerType" id="addCust_customerType" name="customerType"
											class="form-control input-sm m-bot15">
											<option value="PHONE_CUSTOMER">Phone Customer</option>
											<option value="WEB_CUSTOMER">Web Customer</option>
											<option value="RETAIL_CUSTOMER">Retail Customer</option>
											<option value="BROKER">Broker</option>
											<option value="VENDOR">Vendor</option>
											<option value="CORPORATE">Corporate</option>
											<option value="CONSUMER">Consumer</option>
											<option value="EMPLOYEE">Employee</option>
											<option value="INTERNAL_USE_ONLY">Internal Use Only</option>
											<option value="EXCHANGES">Exchanges</option>
											<option value="EBAY_CUSTOMER">Ebay Customer</option>
											<option value="DIVISIONS">Divisions</option>
											<option value="EBAY_ESL">Ebay ESL</option>
											<option value="EBAY_CAT">Ebay CAT</option>
											<option value="CORPORATION">Corporation</option>
											<option value="AFFILIATE">Affiliate</option>
											<option value="EBAY_ZONES">Ebay Zones</option>
											<option value="AO_WEB_CUSTOMER">AO Web Customer</option>
											<option value="EBAY_RATA">Ebay RATA</option>
											<option value="TND_ZONES">TND Zones</option>
										</select>
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Client/Broker <span class="required">*</span> </label>
										<select name="clientBrokerType"
											class="form-control input-sm m-bot15" id="addCust_clientBrokerType">
											<option value="client">Client</option>
											<option value="broker">Broker</option>
											<option value="both">Both</option>
										</select>
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Customer status <span class="required">*</span> </label>
										<select path="customerLevel" name="customerLevel" id="addCust_customerLevel"
											class="form-control input-sm m-bot15">
											<option value="GOLD">Gold</option>
											<option value="SILVER">Silver</option>
											<option value="PLATINUM">Platinum</option>
										</select>

									</div>

									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label> Signup Type <span class="required">*</span> </label>
										<select path="signupType" id="addCust_signupType" name="signupType" class="form-control input-sm m-bot15">
											<option value="REWARDTHEFAN">REWARD THE FAN</option>
											<option value="FACEBOOK">Facebook</option>
											<option value="GOOGLE">Google</option>
											<!--<option value="phone">Phone</option>-->
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="tab-fields full-width">
									<div class="form-group col-sm-4 col-xs-6">
										<label>First Name<span class="required">*</span> </label>
										<input class="form-control" path="customerName" id="addCust_name" name="customerName" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Last Name <span class="required">*</span></label>
										<input class="form-control" path="lastName" id="addCust_lastName" name="lastName" />
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="tab-fields full-width">
									<div class="form-group col-sm-4 col-xs-6">
										<label>E-Mail <span class="required">*</span>
										</label><input class="form-control" path="email" id="addCust_email" type="email" name="email" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Phone <span class="required">*</span> </label> <input
											class="form-control" path="phone" id="addCust_phone" name="phone" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Ext</label> <input class="form-control" path="extension" id="addCust_extension"
											name="extension" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Other Phone </label> <input class="form-control" path="otherPhone" id="addCust_otherPhone"
											name="otherPhone" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Representative </label> <input class="form-control" path="representativeName" id="addCust_representativeName"
											name="representativeName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Company Name </label> <input class="form-control" path="companyName" id="addCust_companyName"
											name="companyName" type="text" />
									</div>
								</div>

								<header class="panel-heading full-width mt-20"> Billing Address </header>
								<div class="tab-fields full-width mt-10">
									<div class="form-group col-sm-4 col-xs-6">
										<label>First Name <span class="required">*</span></label> <input class="form-control"
											id="addCust_blFirstName" name="blFirstName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Last Name <span class="required">*</span></label>
										<input class="form-control" id="addCust_blLastName" name="blLastName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>E-Mail</label>
										<input class="form-control" path="email" id="addCust_blEmail" type="email" name="blEmail" />
										<input type="checkbox" id="addCust_sameBillingEmail">Same as Primary Email
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street1 <span class="required">*</span></label>
										<input class="form-control" id="addCust_blStreet1" type="text" name="addressLine1" value="" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street2</label>
										<input class="form-control" id="addCust_blStreet2" type="text" name="addressLine2" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Zip Code <span class="required">*</span></label>		
										<input class="form-control" id="addCust_blZipCode" type="text" name="zipCode" onblur="getCityStateCountry(this.value, 'body-add-new-customer-billing')"/>
									</div>	
									<div class="form-group col-sm-4 col-xs-6">
										<label>City <span class="required">*</span></label>
										<input class="form-control" id="addCust_blCity" type="text" name="city" />
									</div> 
									<div class="form-group col-sm-4 col-xs-6">
										<label>Country <span class="required">*</span></label>		
										<select id="addCust_blCountryName" name="countryName" class="form-control input-sm m-bot15" onchange="loadStateAddCustomer(addCust_blCountryName,addCust_blStateName,'')">
											<%--<option value="-1">--select--</option>
											<c:forEach var="country" items="${countries}">
												<option value="${country.id}"><c:out value="${country.name}"/></option>
											 </c:forEach> --%>
										</select>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>State <span class="required">*</span></label>
										<select id="addCust_blStateName" name="stateName" class="form-control input-sm m-bot15" onchange="">
											<%-- <option value="-1">--select--</option>
											  <c:forEach var="state" items="${states}">
												<option value="${state.id}"><c:out value="${state.name}"/></option>
											 </c:forEach> --%><!---->
										</select>
									</div>
								</div>
								<header class="panel-heading full-width"> 
									<span style="display:inline-block; vertical-align: top;">Shipping Address</span>
									<input name="isBilling" id="addCust_isBilling" value="" type="checkbox" onclick = "copyBillingAddress();" style="display: inline-block; margin: 12px 3px 0 14px;"/> 
									<span style="display:inline-block; vertical-align: top;">Same as billing address</span>
								</header>
								<div class="tab-fields full-width mt-10">
									<div class="form-group col-sm-4 col-xs-6">
										<label>First Name <span class="required">*</span></label> <input class="form-control"
											id="addCust_shFirstName" name="shFirstName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Last Name <span class="required">*</span></label>
										<input class="form-control" id="addCust_shLastName" name="shLastName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>E-Mail</label>
										<input class="form-control" path="email" id="addCust_shEmail" type="email" name="shEmail" />
										<input type="checkbox" id="addCust_sameShippingEmail">Same as Primary Email
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street1<span class="required">*</span></label>
										<input class="form-control" id="addCust_shStreet1" type="text" name="shStreet1" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street2</label>
										<input class="form-control" id="addCust_shStreet2" type="text" name="shStreet2" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Zip Code <span class="required">*</span></label>		
										<input class="form-control " id="addCust_shZipCode" type="text" name="shZipCode" onblur="getCityStateCountry(this.value, 'body-add-new-customer-shipping')"/>
									</div>	
									<div class="form-group col-sm-4 col-xs-6">
										<label>City <span class="required">*</span></label>
										<input class="form-control " id="addCust_shCity" type="text" name="shCity" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Country <span class="required">*</span></label>		
										<select id="addCust_shCountryName" name="shCountryName" class="form-control input-sm m-bot15" onchange="loadStateAddCustomer(addCust_shCountryName,addCust_shStateName,'')">
											<%--<option value="-1">--select--</option>
											<c:forEach var="country" items="${countries}">
												<option value="${country.id}"><c:out value="${country.name}"/></option>
											 </c:forEach>--%>
										</select>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>State <span class="required">*</span></label>
										<select id="addCust_shStateName" name="shStateName" class="form-control input-sm m-bot15" onchange="">
											<%-- <option value="-1">--select--</option>
											  <c:forEach var="state" items="${states}">
												<option value="${state.id}"><c:out value="${state.name}"/></option>
											 </c:forEach> --%>
										</select>
										<!--<input class="form-control " id="state" type="text" name="state" /> -->
									</div>
								</div>
								<!-- <div class="form-group">
									<div class="col-sm-6"> -->
										<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
										
										<!-- <button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
									</div>
								</div> -->
							</form>
						</div>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="doCustomerValidations();">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End popup Add New Customer -->

	<script>
	
	$(document).ready(function(){
		
		$("#addCust_sameBillingEmail").change(function(){
			 if($("#addCust_sameBillingEmail").is(':checked')){
			 	var email = $('#addCust_email').val();
			 	if(email=='' || email==undefined){
			 		jAlert("Please add Primary Email address from top level section.");
			 		$("#addCust_sameBillingEmail").attr('checked',false);
			 		return false;
			 	}
			 	$("#addCust_blEmail").val(email);
		 	 }else{
		 		$("#addCust_blEmail").val('');
		 		
		 	 }
		});
		$("#addCust_sameShippingEmail").change(function(){
			 if($("#addCust_sameShippingEmail").is(':checked')){
			 	var email = $('#addCust_email').val();
			 	if(email=='' || email==undefined){
			 		jAlert("Please add Primary Email address from top level section.");
			 		$("#addCust_sameShippingEmail").attr('checked',false);
			 		return false;
			 	}
			 	$("#addCust_shEmail").val(email);
		 	 }else{
		 		$("#addCust_shEmail").val('');
		 		
		 	 }
		});
		
	});
	
	//Start Add New Customer
	function addNewCustomer(){
		addCustomer();
		//popupCenter("${pageContext.request.contextPath}/Client/AddCustomerPopup","Add Customer","1000","1000");		
	}
	
	function addCustomer(){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Client/AddCustomer.json",
			type : "post",
			//data : "poId="+poId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData!=null && jsonData!="") {
					setCustomerValues(jsonData);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End Add New Customer
	</script>