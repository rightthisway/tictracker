<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

  
</style>

<script type="text/javascript">

var jq2 = $.noConflict(true);
$(document).ready(function(){
	
	var allChilds = '${childsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allChilds=='true'){
			$('#childsCheckAll').attr("checked","checked");
		}
		allChilds='false';
	}
	selectCheckBox();
	
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
});

function callHideGridDiv() {
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
}

function getChildCategories(){
	var parentId =$('#parentCategory').val();
	var url = "GetChildCategoriesByParentCategory?parentId="+parentId;
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#childs').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i];
                if(data.name.toUpperCase()!='NONE' && data.name.toUpperCase()!='OTHER') {
					var rowText = "<option value="+data.id+ ">"+ data.name+"</option>";
					$('#childs').append(rowText);
                }
            }	
		}
	}); 
} 
function callSearchBtnClick(action){
	/*
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
	 var flag = true;
	 var childCategory=$("#childs").val();
	  if(childCategory==null || childCategory==''){
		  jAlert('Please select the child category.');
		  flag= false;
		  return false;
	  }
		
	 if(flag) {
	 	$("#action").val(action);
	 	$("#childImageForm").submit();
	 }
	 */
	 $("#action").val(action);
	 $("#childImageForm").submit();
}
function selectAllChilds(){
	if((document.getElementById("childsCheckAll").checked)){
		$("#childs").each(function(){
			$("#childs option").attr("selected","selected"); 
		});
	}
	else{
		$("#childs").each(function(){
			$("#childs option").removeAttr("selected"); 
		});
	}
}

function callChangeImage(childCategoryId){
	$('#imageUploadDiv_'+childCategoryId).show();
	$('#imageDisplayDiv_'+childCategoryId).hide();
	$('#fileExisting_'+childCategoryId).val('N');
	$('#fileDeleted_'+childCategoryId).val('Y');
	selectRow(childCategoryId);
}
function callChangeCircleImage(childCategoryId){
	$('#circleImageUploadDiv_'+childCategoryId).show();
	$('#circleImageDisplayDiv_'+childCategoryId).hide();
	$('#circleFileExisting_'+childCategoryId).val('N');
	$('#circleFileDeleted_'+childCategoryId).val('Y');
	selectRow(childCategoryId);
}

function selectRow(childCategoryId) {
	$('#checkbox_'+childCategoryId).attr('checked', true);
}
var validFilesTypes = ["jpg", "jpeg","png","gif"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
        	file.focus();
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
            file.value = null;
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
        	file.focus();
            jAlert("Image Size Should be Greater than 0 and less than 1 MB.");
            file.value = null;
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null || file.value == '') {
    		jAlert("Please select valid image to upload.");
    		file.focus();
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }


function callSaveBtnOnClick(action) {
	
	var flag = true;
	
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		var isMinimamOneImage=false;
		id = this.id.replace('checkbox','file');
		var file = $('#'+id).val();
		if(file != null && file != '') {
			id = this.id.replace('checkbox','file');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
			isMinimamOneImage = true;
		} else {
			id = this.id.replace('checkbox','fileExisting');
			var isFileExisting = $('#'+id).val();
			if(isFileExisting != null && isFileExisting=='Y') {
				isMinimamOneImage = true;
			}
		}
		if(!flag) {
			return false;
		}
		id = this.id.replace('checkbox','circleFile');
		var circleFile = $('#'+id).val();
		if(circleFile != null && circleFile != '') {
			id = this.id.replace('checkbox','circleFile');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
			isMinimamOneImage = true;
		} else {
			id = this.id.replace('checkbox','circleFileExisting');
			var isCircleFileExisting = $('#'+id).val();
			if(isCircleFileExisting != null && isCircleFileExisting=='Y') {
				isMinimamOneImage = true;
			}
		}
		
		if(!isMinimamOneImage) {
			jAlert('Select image or circle image to save child category.');
			id = this.id.replace('checkbox','file');
			$("#"+id).focus();
			flag= false;
			return false;
		}
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one child category to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to save selected child category images ?.","Confirm",function(r){
		  if(r) {
				$("#action").val(action);
			 	$("#childImageForm").submit();
			}
	  });
	}
	
	
}
function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing child category image to delete.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to remove selected child category images ?","Confirm",function(r){
		  if(r) { 
				$("#action").val(action);
				$("#childImageForm").submit();
			 }
	  });
	}
	
	
}

function callImageOnChange(childCategoryId) {
	$('#fileDeleted_'+childCategoryId).val('N');
	selectRow(childCategoryId);
}
function callCircleImageOnChange(childCategoryId) {
	$('#circleFileDeleted_'+childCategoryId).val('N');
	selectRow(childCategoryId);
}
</script>

<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Images</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Images</a></li>
						<li><i class="fa fa-laptop"></i>Child Category</li>						  	
					</ol>
				</div>
</div>

<div class="container">
<div class="row">

	<div class="alert alert-success fade in" id="infoMainDiv"
	<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="childImageForm" id="childImageForm" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/ChildCategoryImages">
	<input type="hidden" id="action" name="action" />
	<div class="row filters-div">
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
          <label for="imageText" class=control-label"><b>Child :</b></label>
		</div>
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">  
			<select class="form-control input-sm m-bot15 fullWidth" type="text" name="childSearch" id="childSearch" onchange="callSearchBtnClick('search')">
			 <option value="">--All--</option>
			<c:forEach items="${childsList}" var="child">
				<option value="${child.id}" <c:if test="${child.id == childSearch}">selected</c:if>>${child.name}</option>
			</c:forEach>
			</select> 
        </div>        
          <!-- 
          <div class="col-lg-4">
          		<select name="parentCategory" id="parentCategory" class="form-control input-sm m-bot15" onchange="getChildCategories();">
                	<option value="">--- Select ---</option>
					<c:forEach var="parent" items="${parentList}">
					    <option value="${parent.id}" <c:if test="${selectedParent ne null and parent.id eq selectedParent}">selected</c:if>>${parent.name}</option>
					  </c:forEach>
               </select>
          </div>         
           -->
          <!-- <div class="col-lg-2">
          	<button type="button" onclick="callSearchBtnClick('search');" class="btn btn-primary btn-sm">Search</button>
          </div> -->
          
        </div>
           <!-- 
          <div class="form-group" >
          <label for="imageText" class="col-lg-3 col-lg-offset-2 control-label"><b>Child Category : </b></label>
          <div class="col-lg-4">
          		<input type="hidden" id="childStr" name="childStr" value = ${childStr}/> 
				<input type="checkbox" id="childsCheckAll" name="childsCheckAll" onclick ="selectAllChilds()" 
					<c:if test ="${childsCheckAll}"> checked="checked" </c:if>/> 
				<label class="control-label" for="childsCheckAll">Select All</label>
				<div class="form-group" >
                    <div >
                    <select multiple class="form-control fullWidth" name="childs" id="childs" onchange="callHideGridDiv();">
                      <c:forEach items="${childs}" var="child">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${child.id}${temp}"/>
						<option 
							<c:if test="${fn:contains(childStr,temp2)}"> Selected </c:if>
							value="${child.id}"> ${child.name}</option>
						</c:forEach> 
                     </select>
                    	
                    </div>
                </div>
          </div>
          <div class="col-lg-2">
          </div>
           </div>
           
          <div class="form-group" >
	          <div class="col-lg-2 col-lg-offset-4">
	          <button type="button" onclick="callSearchBtnClick('search');" class="btn btn-primary btn-sm">Search</button>
	          </div>
          </div>
          -->
	</div>
<div class="full-width" id="gridDiv">
	<c:if test="${not empty childImageList}">
	<div class="full-width column">
			<div class="pull-right">
					<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
				<br />
				<br />
		<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-3">
							Child Category
						</th>
						<th class="col-lg-4">
							Image
						</th>
						<!-- <th class="col-lg-4">
							Circle Image
						</th> -->
						
						
					</tr>
				</thead>
				<tbody>
				 <c:forEach var="ccImage" varStatus="vStatus" items="${childImageList}">
                    <tr <c:if test="${ccImage.id ne null}">style="background-color: #caf4ca;"</c:if> >
						<td>
	                      	<input type="checkbox" class="selectCheck" id="checkbox_${ccImage.childCategory.id}" name="checkbox_${ccImage.childCategory.id}" />
							<input type="hidden" name="id_${ccImage.childCategory.id}" id="id_${ccImage.childCategory.id}" value="${ccImage.id}" />
							<input type="hidden" name="fileExisting_${ccImage.childCategory.id}" id="fileExisting_${ccImage.childCategory.id}" 
							<c:if test="${ccImage.imageFileUrl ne null}"> value="Y" </c:if>
							<c:if test="${ccImage.imageFileUrl eq null}"> value="N" </c:if> />
							<input type="hidden" name="circleFileExisting_${ccImage.childCategory.id}" id="circleFileExisting_${ccImage.childCategory.id}" 
							<c:if test="${ccImage.circleImageFileUrl ne null}"> value="Y" </c:if>
							<c:if test="${ccImage.circleImageFileUrl eq null}"> value="N" </c:if> />
							<input type="hidden" name="circleFileDeleted_${ccImage.childCategory.id}" id="circleFileDeleted_${ccImage.childCategory.id}" value="N" />
							<input type="hidden" name="fileDeleted_${ccImage.childCategory.id}" id="fileDeleted_${ccImage.childCategory.id}" value="N" />
                      </td>
                      <td style="font-size: 13px;" align="center">
                     		<b><label for="childCategory" class="list-group-item" id="childCategory_${ccImage.childCategory.id}">${ccImage.childCategory.name}</label></b>
                     		
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="form-group form-group-top" id="imageUploadDiv_${ccImage.childCategory.id}" 
                                  <c:if test="${ccImage.imageFileUrl ne null }">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-lg-3 control-label">Image File</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="file_${ccImage.childCategory.id}" name="file_${ccImage.childCategory.id}" onchange="callImageOnChange('${ccImage.childCategory.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="imageDisplayDiv_${ccImage.childCategory.id}" 
                                   <c:if test="${ccImage.imageFileUrl eq null}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${ccImage.imageFileUrl ne null}">
                                          <img src='${api.server.url}GetImageFile?type=childCategoryImage&filePath=${ccImage.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage('${ccImage.childCategory.id}');">
											Change Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td>
						<%-- <td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="form-group form-group-top" id="circleImageUploadDiv_${ccImage.childCategory.id}" 
                                  <c:if test="${ccImage.circleImageFileUrl ne null }">style="display: none;" </c:if>>
                                      <label for="circleImageFile" class="col-lg-3 control-label">Circle Image File</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="circleFile_${ccImage.childCategory.id}" name="circleFile_${ccImage.childCategory.id}" onchange="callCircleImageOnChange('${ccImage.childCategory.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="circleImageDisplayDiv_${ccImage.childCategory.id}" 
                                   <c:if test="${ccImage.circleImageFileUrl eq null}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${ccImage.circleImageFileUrl ne null}">
                                          <img src='${pageContext.request.contextPath}/RewardTheFan/GetImageFile?type=childCategoryImage&filePath=${ccImage.circleImageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeCircleImage('${ccImage.childCategory.id}');">
											Change Circle Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td> --%>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
			<div class="full-width">
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${empty childImageList and not empty childStr}">
	<div class="alert alert-block alert-danger fade in" id="infoMainDiv">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span>No records Found.</span>
    </div>
	</c:if>
	</div>
	</form>
</div>
