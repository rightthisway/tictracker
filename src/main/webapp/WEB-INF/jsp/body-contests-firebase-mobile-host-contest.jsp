<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-firestore.js"></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
#winnerDivStyle{
    width:100%;
    height:190px;
    white-space: nowrap;
}
#winnerDivStyle a {
    display: inline-block;
    vertical-align: middle;
}

#winnerDivStyle img {border: 0;}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var jq2 = $.noConflict(true);
//sandbox
/* var firebaseConfig = {
	    apiKey: "AIzaSyBt49nnBppWx77BLEeA1A5dWxGHgd3fQks",
	    authDomain: "firestoresandbox-c5064.firebaseapp.com",
	    databaseURL: "https://firestoresandbox-c5064.firebaseio.com",
	    projectId: "firestoresandbox-c5064",
	    storageBucket: "firestoresandbox-c5064.appspot.com",
	    messagingSenderId: "23325625262",
	    appId: "1:23325625262:web:3d9c58fa812bede38f563f"
	  }; */
	  
	  
//Production
var firebaseConfig = {
    apiKey: "AIzaSyDoZ79ezKCN8Ns-xIRH4qCfgoiYRJ2tJSE",
    authDomain: "rewardthefan-1239.firebaseapp.com",
    databaseURL: "https://rewardthefan-1239.firebaseio.com",
    projectId: "rewardthefan-1239",
    storageBucket: "rewardthefan-1239.appspot.com",
    messagingSenderId: "65473242204",
    appId: "1:65473242204:web:dc312ed5861016646bf299"
  };
//Initialize Firebase
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore();
var hostColl = db.collection("hostNode");
var tracker = hostColl.doc("tracker");
var que = hostColl.doc("que");
var answerCount = hostColl.doc("answerCount");;
var totalUserCount = hostColl.doc("totalUserCount");
var jackpot = hostColl.doc("jackpot");
var summary = hostColl.doc("summary");
var summaryCount = hostColl.doc("summaryCount");
var grand = hostColl.doc("grand");



tracker.onSnapshot(function(doc){
	var data = doc.data();
	$('#nextQuestion1').text('');
	setFirestoreDate(data.state);
});

var ANS = '';
var QSIZE = parseInt('${contest.questionSize}');
var queNo = '';


que.get().then(function(doc){
	var data = doc.data();
	$('#contestMsg').hide();
	$('#optionDiv').hide();
	
	queNo = data.qNo;
	ANS = data.ans;
});



function setFirestoreDate(state){
	$('#runningQuestionDiv').show();
	if(state == 'QUESTION'){
		que.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			
			queNo = data.qNo;
			ANS = data.ans;
			$('#nextQuestion1').text('');
			if(queNo == 1 || queNo == '1'){
				$('#nextQuestion1').text('CONTEST STARTED READY FOR Q1');
			}else if(queNo == null || queNo == '' || queNo == 'null'){
				$('#nextQuestion1').text('CONTEST ENDED');
			}else{
				$('#nextQuestion1').text('ANSWER DISPLAYED');
				nextQuestionCounter("Q"+queNo+" IN",queNo);
			}
		});
	}else if(state == 'QUEFIRED'){
		$('#contestMsg').hide();
		$('#optionDiv').hide();
		$('#nextQuestion1').text('Q'+queNo+" DISPLAYED");
	}else if(state == 'COUNT'){
		answerCount.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').show();
			$('#optionDiv').css('background','#00a0df');
			
			if(ANS == 'A'){
				$('#correctOption').text('A');
				$('#correctOptionCount').text(data.countA);
			}else if(ANS == 'B'){
				$('#correctOption').text('B');
				$('#correctOptionCount').text(data.countB);
			}else if(ANS == 'C'){
				$('#correctOption').text('C');
				$('#correctOptionCount').text(data.countC);
			}
			if(queNo == null || queNo == '' || queNo == 'null'){
				$('#nextQuestion1').text('CONTEST ENDED');
			}else{
				$('#nextQuestion1').text('READY FOR A'+queNo);
			}
			
		});
	}else if(state == 'MINIJACKPOT'){
		jackpot.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			$('#nextQuestion1').text('READY FOR MINI JACKPOT WINNER');
			
			renderMiniJackpotWinners(data.winner);
		});
	}else if(state == 'SUMMARY'){
		summary.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			
			renderSummaryWinners(data.winner);
			$('#nextQuestion1').text('DISPLAYED SUMMARY WINNERS');
			
		});
	}else if(state == 'LOTTERY'){
		summary.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			
			renderSummaryWinners(data.winner);
			$('#nextQuestion1').text('RUNNING LOTTERY');
			
		});
	}else if(state == 'WINNER'){
		grand.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			
			renderWinners(data.winner);
			$('#nextQuestion1').text('READY FOR GRAND WINNERS');
		});
	}else if(state == 'WINFIRED'){
		grand.onSnapshot(function(doc){
			var data = doc.data();
			$('#contestMsg').hide();
			$('#optionDiv').hide();
			
			renderWinners(data.winner);
			$('#nextQuestion1').text('DISPLAYED GRAND WINNER');
		});
	}else if(state == 'END'){
		$('#contestMsg').hide();
		$('#winnerRewardLbl').text('');
		$('#optionDiv').hide();
		$('#totalUserCountlbl').text('0');
		$('#nextQuestion').text('');
		$('#nextQuestion1').text('CONTEST ENDED');
	}else{
		$('#optionDiv').hide();
	}
	
}






totalUserCount.onSnapshot(function(doc){
	var data = doc.data();
	$('#totalUserCountlbl').text(data.count);
});



	  
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}		
	});
	
});


function nextQuestionCounter(buttonText,queNo){
	var c = 15;
	$('#nextQuestion').text(buttonText+' - '+c);
	setInterval(function(){
	  c--;
	  if(c>=0){
		  $('#nextQuestion').text(buttonText+' - '+c);
	  }
      if(c==0){
    	  $('#nextQuestion1').text('READY FOR Q'+queNo);
		  $('#nextQuestion').text('');
		  
      }
	},1000);
}


function renderWinners(winner){
	var winnerDiv = '<table cellspacing=6 cellpadding=2>';
	$('#winnerDiv').empty();
	if(winner.length> 0){
		$('#winnerRewardLbl').text(winner[0].tix+' Tickets');
	}
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<tr><td style="margin:15px;text-align:center;"><label style="font-size: 15px;">'+winner[i].uId+'</td></tr>';
	}
	winnerDiv +='</table>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv); 
}

function fillLiveUserList(data){
	if(data.userList!=null && data.userList != '' && data.userList.length > 0){
		var userString = '';
		$('#liveUsersDiv').empty();
		for(var i=0;i<data.userList.length;i++){
			userString += '<div class="form-group col-sm-2 col-xs-2">'+
			'<label id="totalUserCountlbl" style="font-size: 15px;">'+data.userList[i].uId+'</label></div>';
		}
		$('#liveUsersDiv').append(userString);
	}
}

function renderSummaryWinners(winner){
	var winnerDiv = '<table cellspacing=6 cellpadding=2>';
	$('#winnerDiv').empty();
	if(winner.length> 0){
		$('#winnerRewardLbl').text('$'+winner[0].prize);
	}
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<tr><td style="margin:15px;text-align:center;"><label style="font-size: 15px;">'+winner[i].uId+'</td></tr>';
	}
	winnerDiv +='</table>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv); 
	
	/* winnerDiv = '<marquee scrollamount="15"><table><tr="row1"></tr><tr="row2"></tr><tr="row3"></tr></table></marquee>';
	$('#winnerDiv').append(winnerDiv);
	if(winner.length> 0){
		$('#winnerRewardLbl').text('$'+winner[0].prize);
	}
	for(var i=0;i<winner.length;i=i+3){
		var row1  = document.getElementById('row1');
		var cell = row1.insertCell(-1);
		cell.innerHTML = winner[i].uId;
		if((i+1) < winner.length){
			var row2  = document.getElementById('row2');
			var cell = row2.insertCell(-1);
			cell.innerHTML = winner[i+1].uId;
		}
		if((i+2) < winner.length){
			var row3  = document.getElementById('row3');
			var cell = row3.insertCell(-1);
			cell.innerHTML = winner[i+2].uId;
		}
	} */
	
	
}

function renderMegaJackpotWinners(winner){
	var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
	$('#winnerDiv').empty();
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
		'<label style="font-size: 30px;">'+winner[i].rPointsSt+'</label><br/><label style="font-size: 30px;">'+winner[i].jCreditSt+'</label>'+
		'<br/><label style="font-size: 12px;">'+winner[i].uId+'</label></div>';
			
	}
	winnerDiv +='</div></marquee>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv);
	
}

function renderMiniJackpotWinners(winner,type,prize){
	if(winner.length <=3){
		var winnerDiv = '<div align="center" id="grandWinnerDiv"><table align="center"> <tr>';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(type+' - '+prize);
		}
		for(var i=0;i<winner.length;i++){
			winnerDiv +=  '<td align="center">'+
				'<label style="font-size: 12px;margin:15px;text-align:center;width:350px;">'+winner[i].uId+'</label></td>';
		}
		winnerDiv +='</tr></table></div>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}else{
		var winnerDiv = '<marquee scrollamount="15"><div id="winnerDivStyle">';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(type+' - '+prize);
		}
		for(var i=0;i<winner.length;i++){
				winnerDiv += '<div style="margin:15px;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
			'<label style="font-size: 15px;">'+winner[i].uId+'</label>'+
			'</div>';
				
		}
		winnerDiv +='</div></marquee>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}
	
}


</script>

<div id="contestDiv">	
	<div class="panel-body1 full-width" >
			<div class="form-group tab-fields">
				<div class="form-group col-sm-3 col-xs-3">
					<label style="font-size: 15px;">Users :&nbsp;&nbsp;&nbsp;</label>
					<label id="totalUserCountlbl" style="font-size: 15px;margin-left:-12px;">0</label>
				</div>
				 <div class="form-group col-sm-8 col-xs-8" id="lifelineCountDiv" style="margin-left:-45px;">
					<label id="nextQuestion1" style="font-size: 15px;;color:red;">Contest Not Started</label><br/>
					<label id="nextQuestion" style="font-size: 15px;;color:red;"></label>
					<!-- <button class="btn btn-primary" id="nextQuestion" type="button" disabled>Contest Not Started</button> -->
				</div>
				<div id="optionDiv" class="form-group col-sm-2 col-xs-2" style="margin-top:-24px;margin-left:8px;">
					<label id="correctOption" style="font-size: 15px;"></label>
					<label id="correctOptionCount" style="font-size: 15px;float:right;"></label>
				</div>
			</div>
			<div class="form-group tab-fields">
				
			</div>
			<div align="left" id="winnerRewardDollarDiv">
				<label id= "winnerRewardLbl" style="font-size: 15px;"></label>
				<br/><br/>
			</div>
			<div id="winnerDiv" style="display:none;float:left">
				
			</div>
		</form>
		<div id="contestMsg" class="form-group tab-fields" style="text-align: center;">
				<div class="form-group col-sm-12 col-xs-12">
				<c:if test="${not empty msg}">
					<label style="font-size: 25px;;color:red;">${msg}</label>	
				</c:if>
				</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	//call functions once page loaded
	window.onload = function() {
		enableMenu();
	};
		
</script>