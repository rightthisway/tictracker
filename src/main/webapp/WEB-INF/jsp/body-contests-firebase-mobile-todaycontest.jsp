<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
#winnerDivStyle{
    width:100%;
    height:190px;
    white-space: nowrap;
}
#winnerDivStyle a {
    display: inline-block;
    vertical-align: middle;
}

#winnerDivStyle img {border: 0;}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var QSIZE = '${contest.questionSize}';
var questionArray = [];
var j = 0;
var promoAutoGenerate;
var promoFlatDiscount;
var varSelectedList = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}		
	});
	
});


function contDownTimer(buttonText,qNo){
	var c = 20;
	if(qNo == 1 || qNo==2){
		c = 20;
	}
	$('#nextQuestion').text(c);
	$('#nextQuestion').attr("disabled",true);
	setInterval(function(){
	  c--;
	  if(c>=0){
		  $('#nextQuestion').text(c);
	  }
      if(c==0){
    	  $('#nextQuestion').attr("disabled",false);
    	  $('#nextQuestion').text(buttonText);
      }
	},1000);
}

function contDownTimer1(buttonText,qNo){
	var c = 15;
	$('#nextQuestion').text(c);
	$('#nextQuestion').attr("disabled",true);
	setInterval(function(){
	  c--;
	  if(c>=0){
		  $('#nextQuestion').text(c);
	  }
      if(c==0){
    	  $('#nextQuestion').attr("disabled",false);
    	  $('#nextQuestion').text(buttonText);
      }
	},1000);
}

var isTotalCount= true;
function nextQuestion(){
	var buttonType = $('#nextQuestion').text();
	var type = '';
	
	var userCountUpdater;
	if(buttonType!='' && buttonType != undefined){
		if(buttonType.indexOf('Resume') >= 0){
			type='RESUME';
		}else if(buttonType.indexOf('Question') >= 0 || buttonType.indexOf('Summary') >= 0
				|| buttonType.indexOf('End') >= 0){
			type='QUESTION';
			if(buttonType.indexOf('Summary') >= 0){
				type = 'SUMMARY';
			}
		}else if(buttonType.indexOf('Answer') >= 0){
			type='ANSWER';
		}else if(buttonType.indexOf('Start') >= 0){
			type='START';
		}else if(buttonType.indexOf('Lottery') >= 0){
			type = 'LOTTERY';
		}else if(buttonType.indexOf('Declare Winners') >= 0){
			type = 'WINNERS';
		}else if(buttonType.indexOf('Display Winners') >= 0){
			type = 'WINNER';
		}else if(buttonType.indexOf('Count') >= 0){
			type = 'COUNT';
		}else if(buttonType.indexOf('Declare Mini Jackpot') >= 0){
			type = 'MINIJACKPOTS';
		}else if(buttonType.indexOf('Display Mini Jackpot') >= 0){
			type = 'MINIJACKPOT';
		}else if(buttonType.indexOf('Declare Mega Jackpot') >= 0){
			type = 'MEGAJACKPOTS';
		}else if(buttonType.indexOf('Display Mega Jackpot') >= 0){
			type = 'MEGAJACKPOT';
		}
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/NextContestQuestionFireBase",
		type : "post",
		dataType: "json",
		data: "runningContestId="+$('#runningContestId').val()+"&runningQuestionNo="+$('#runningQuestionNo').val()+'&type='+type,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				var data = jsonData.question;
				$('#runningContestId').val(data.contestId);
				$('#runningQuestionNo').val(data.serialNo);
				//$('#queNo').text(data.serialNo+".  ");
				$('#runningQuestionText').text(data.serialNo+".  "+data.question);
				$('#runningQuestionOptionA').text(data.optionA);
				$('#runningQuestionOptionB').text(data.optionB);
				$('#runningQuestionOptionC').text(data.optionC);
				$('#questionRewardDollar').text('');
				$('#questionOptionACount').text('');
				$('#questionOptionBCount').text('');
				$('#questionOptionCCount').text('');
				//$('#runningQuestionOptionD').text(data.optionD);
				$('#optionADiv').css('background','#ccc');
				$('#optionBDiv').css('background','#ccc');
				$('#optionCDiv').css('background','#ccc');
				if(isTotalCount == true){
					userCountUpdater = setInterval(function(){updateTotalUserCount()},5000);
				}
				isTotalCount = false;
				$('#contestInfoDiv1').hide();
				$('#contestInfoDiv2').hide();
				$('#questionSizeDiv').hide();
				$('#lifelineCountLbl').text(' N/A ');
				if(type=='START'){
					var qNo = parseInt(data.serialNo);
					$('#runningQuestionText').text(jsonData.contestMsg);
					$('#nextQuestion').text('Show Question-'+(qNo+1));
					$('#runningQuestionNo').val(0);
				}else if(type=='QUESTION'  || (type=='RESUME' && jsonData.contest.lastAction == 'QUESTION')){
					$('#optionDiv').show();
					$('#winnerRewardLbl').text('');
					$('#winnerDiv').empty();
					$('#runningQuestionDiv').addClass('col-xs-6');
					$('#runningQuestionDiv').addClass('col-sm-6');
					$('#runningQuestionDiv').removeClass('col-xs-12');
					$('#runningQuestionDiv').removeClass('col-sm-12');
					$('#optionADiv').show();
					$('#optionBDiv').show();
					$('#optionCDiv').show();
					$('#lifelineCountLbl').text(jsonData.lifeCount);
					contDownTimer('Show Count-'+data.serialNo,data.serialNo);
					$('#runningQuestionAnswer').text('');
					$('#nextADiv').hide();
					$('#nextBDiv').hide();
					$('#nextCDiv').hide();
					$('#nextQuestionText').text('');
					$('#liveUsersDiv').empty();
				}else if(type=='COUNT' || (type=='RESUME' && jsonData.contest.lastAction == 'COUNT')){
					$('#nextQuestion').text('Show Answer-'+data.serialNo);
					$('#optionDiv').show();
					if(data.answer == 'A'){
						$('#optionADiv').css('background','#00a0df');
					}else if(data.answer == 'B'){
						$('#optionBDiv').css('background','#00a0df');
					}else if(data.answer == 'C'){
						$('#optionCDiv').css('background','#00a0df');
					}
					
					$('#nextADiv').hide();
					$('#nextBDiv').hide();
					$('#nextCDiv').hide();
					$('#nextQuestionText').text('');
					$('#questionRewardDollar').text("Reward $"+jsonData.userCount.quRwds);
					$('#questionOptionACount').text(jsonData.userCount.aCount);
					$('#questionOptionBCount').text(jsonData.userCount.bCount);
					$('#questionOptionCCount').text(jsonData.userCount.cCount);
					fillLiveUserList(jsonData);
				}else if(type=='ANSWER' || (type=='RESUME' && jsonData.contest.lastAction == 'ANSWER')){
					$('#optionDiv').show();
					$('#nextADiv').show();
					$('#nextBDiv').show();
					$('#nextCDiv').show();
					var qNo = parseInt(data.serialNo);
					
					$('#nextQuestion').text('Show Question-'+(qNo+1));
					if(data.answer == 'A'){
						$('#optionADiv').css('background','#00a0df');
					}else if(data.answer == 'B'){
						$('#optionBDiv').css('background','#00a0df');
					}else if(data.answer == 'C'){
						$('#optionCDiv').css('background','#00a0df');
					}
					$('#questionRewardDollar').text("Reward $"+jsonData.userCount.quRwds);
					$('#questionOptionACount').text(jsonData.userCount.aCount);
					$('#questionOptionBCount').text(jsonData.userCount.bCount);
					$('#questionOptionCCount').text(jsonData.userCount.cCount);
					if(qNo == QSIZE){
						$('#nextQuestion').text('Show Summary');
						$('#nextADiv').hide();
						$('#nextBDiv').hide();
						$('#nextCDiv').hide();
						$('#nextQuestionText').text('');
					}else if(qNo == (QSIZE-1) && jsonData.contest.contestJackpotType == 'MEGA'){
						$('#nextQuestion').text('Declare Mega Jackpot Winners');
						$('#nextADiv').hide();
						$('#nextBDiv').hide();
						$('#nextCDiv').hide();
						$('#nextQuestionText').text('');
					}else{
						if(jsonData.contest.contestJackpotType == 'MINI' &&
								data.miniJackpotType != '' && data.miniJackpotType != null &&  data.miniJackpotType !='NONE'){
							$('#nextQuestion').text('Declare Mini Jackpot Winners');
							$('#nextADiv').hide();
							$('#nextBDiv').hide();
							$('#nextCDiv').hide();
							$('#nextQuestionText').text('');
						}else{
							contDownTimer1('Show Question-'+(qNo+1),qNo);
							$('#nextADiv').css('background','#ccc');
							$('#nextBDiv').css('background','#ccc');
							$('#nextCDiv').css('background','#ccc');
							$('#nextQuestionText').text(jsonData.nextQuestion.serialNo+".  "+jsonData.nextQuestion.question);
							$('#nextQuestionOptionA').text(jsonData.nextQuestion.optionA);
							$('#nextQuestionOptionB').text(jsonData.nextQuestion.optionB);
							$('#nextQuestionOptionC').text(jsonData.nextQuestion.optionC);
							if(jsonData.nextQuestion.answer == 'A'){
								$('#nextADiv').css('background','#00a0df');
							}else if(jsonData.nextQuestion.answer == 'B'){
								$('#nextBDiv').css('background','#00a0df');
							}else if(jsonData.nextQuestion.answer == 'C'){
								$('#nextCDiv').css('background','#00a0df');
							}
						}
					}
				}else if(type=='MINIJACKPOTS' || (type=='RESUME' && jsonData.contest.lastAction == 'MINIJACKPOTS')){
					$('#runningQuestionDiv').removeClass('col-xs-6');
					$('#runningQuestionDiv').removeClass('col-sm-6');
					$('#runningQuestionDiv').addClass('col-xs-12');
					$('#runningQuestionDiv').addClass('col-sm-12');
					$('#runningQuestionText').text('Mini Jackpot Winners Are...');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					$('#nextQuestion').text('Display Mini Jackpot Winners');
					if(jsonData.jackpotStatus == 1){
						renderMiniJackpotWinners(jsonData.winners,data.miniJackpotType,data.mjpQtyPerWinner);
					}else{
						jAlert(jsonData.jackpotMsg);
						return;
					}
					if(jsonData.jackpotMsg != undefined && jsonData.jackpotMsg!= ''){
						jAlert(jsonData.jackpotMsg);
						return;
					}
					$('#liveUsersDiv').empty();
				}else if(type=='MINIJACKPOT' || (type=='RESUME' && jsonData.contest.lastAction == 'MINIJACKPOT')){
					var qNo = parseInt(data.serialNo);
					$('#runningQuestionText').text('Mini Jackpot Winners Are...');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					$('#nextQuestion').text('Show Question-'+(qNo+1));
					if(jsonData.jackpotStatus == 1){
						renderMiniJackpotWinners(jsonData.winners,data.miniJackpotType,data.mjpQtyPerWinner);
					}else{
						jAlert(jsonData.jackpotMsg);
						return;
					}
					if(jsonData.jackpotMsg != undefined && jsonData.jackpotMsg!= ''){
						jAlert(jsonData.jackpotMsg);
						return;
					}
					$('#liveUsersDiv').empty();
				}else if(type=='MEGAJACKPOTS' || (type=='RESUME' && jsonData.contest.lastAction == 'MEGAJACKPOTS')){
					$('#runningQuestionDiv').removeClass('col-xs-6');
					$('#runningQuestionDiv').removeClass('col-sm-6');
					$('#runningQuestionDiv').addClass('col-xs-12');
					$('#runningQuestionDiv').addClass('col-sm-12');
					$('#runningQuestionText').text('Mega Jackpot Winners Are...');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					$('#nextQuestion').text('Display Mega Jackpot Winners');
					if(jsonData.jackpotStatus == 1){
						renderMegaJackpotWinners(jsonData.winners,jsonData.winnerCount);
					}else{
						jAlert(jsonData.jackpotMsg);
						return;
					}
					if(jsonData.jackpotMsg != undefined && jsonData.jackpotMsg!= ''){
						jAlert(jsonData.jackpotMsg);
						return;
					}
					$('#liveUsersDiv').empty();
				}else if(type=='MEGAJACKPOT' || (type=='RESUME' && jsonData.contest.lastAction == 'MEGAJACKPOT')){
					var qNo = parseInt(data.serialNo);
					$('#runningQuestionText').text('Mega Jackpot Winners Are...');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					contDownTimer('Show Question-'+(qNo+1));
					//$('#nextQuestion').text('Show Question-'+(qNo+1));
					if(jsonData.jackpotStatus == 1){
						renderMegaJackpotWinners(jsonData.winners,jsonData.winnerCount);
					}else{
						jAlert(jsonData.jackpotMsg);
						return;
					}
					if(jsonData.jackpotMsg != undefined && jsonData.jackpotMsg!= ''){
						jAlert(jsonData.jackpotMsg);
						return;
					}
					$('#liveUsersDiv').empty();
				}else if(type=='SUMMARY' || (type=='RESUME' && jsonData.contest.lastAction == 'SUMMARY')){
					if(jsonData.summaryStatus == 1){
						renderSummaryWinners(jsonData.summaryWinner);
					}else{
						jAlert(jsonData.msg);
						return;
					}
					$('#runningQuestionDiv').removeClass('col-xs-6');
					$('#runningQuestionDiv').removeClass('col-sm-6');
					$('#runningQuestionDiv').addClass('col-xs-12');
					$('#runningQuestionDiv').addClass('col-sm-12');
					$('#runningQuestionText').text('Press the button below to enter all the winners into lottery.');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					clearInterval(userCountUpdater);
					contDownTimer('Enter to Lottery',data.serialNo);
					//$('#nextQuestion').text('Enter to Lottery');
					$('#liveUsersDiv').empty();
				}else if(type=='LOTTERY' || (type=='RESUME' && jsonData.contest.lastAction == 'LOTTERY')){
					$('#runningQuestionDiv').removeClass('col-xs-6');
					$('#runningQuestionDiv').removeClass('col-sm-6');
					$('#runningQuestionDiv').addClass('col-xs-12');
					$('#runningQuestionDiv').addClass('col-sm-12');
					$('#runningQuestionText').text('Press the button below to randomnly choose Tickets Winner.');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					contDownTimer('Declare Winners',data.serialNo);
					//$('#nextQuestion').text('Declare Winners');
					$('#liveUsersDiv').empty();
				}else if(type=='WINNER' || (type=='RESUME' && jsonData.contest.lastAction == 'WINNER')){
					if(jsonData.winnerStatus == 1){
						
					}else{
						jAlert(jsonData.winnerMsg);
						return;
					}
					$('#runningQuestionText').text('Press End Contest to End contest.');
					$('#nextQuestion').text('End Contest');
					$('#liveUsersDiv').empty();
				}else if(type=='WINNERS' || (type=='RESUME' && jsonData.contest.lastAction == 'WINNERS')){
					$('#runningQuestionText').text('Contest Grand Winners are....');
					$('#optionADiv').hide();
					$('#optionBDiv').hide();
					$('#optionCDiv').hide();
					$('#nextQuestion').text('Display Winners');
					if(jsonData.winnerStatus == 1){
						renderWinners(jsonData.winners,jsonData.winnerCount);
					}else{
						jAlert(jsonData.winnerMsg);
						return;
					}
					$('#liveUsersDiv').empty();
				}
			}else{
				jAlert(jsonData.msg);
				return;
			}
			if((jsonData.msg != '' && jsonData.msg != null) || (type=='RESUME' && jsonData.contest.lastAction == 'END')){
				 if(buttonType.indexOf('End') >= 0){
					    $('#nextQuestion').hide();
						$('#runningQuestionText').text('');
						$('#optionADiv').hide();
						$('#optionBDiv').hide();
						$('#optionCDiv').hide();
						jAlert(jsonData.msg);
						window.location="${pageContext.request.contextPath}/ContestsFirebaseMobile?status=EXPIRED";
				 }else{
					 jAlert(jsonData.msg);
				 }
			}
			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateTotalUserCount(){
	$.ajax({
		url : "${pageContext.request.contextPath}/GetTotalUserCount",
		type : "post",
		dataType: "json",
		data: "runningContestId="+$('#runningContestId').val(),
		global:false,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#totalUserCountlbl').text(jsonData.totalUserCount);
			}
		},
		error : function(error){
			console.log('Error occured while getting total user count.');
		}
	});
}

function renderWinners(winner,count){
	if(winner.length <=3){
		var winnerDiv = '<div align="center" id="grandWinnerDiv"><table align="center"> <tr>';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(winner[0].rTix+' Tickets');
		}
		for(var i=0;i<winner.length;i++){
			winnerDiv +=  '<td align="center">'+
				'<label style="font-size: 60px;margin:15px;border:1px solid;text-align:center;width:350px;">'+winner[i].uId+'</label></td>';
		}
		winnerDiv +='</tr></table></div>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}else{
		var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(winner[0].rTix+' Tickets');
		}
		for(var i=0;i<winner.length;i++){
				winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
			'<label style="font-size: 55px;">'+winner[i].uId+'</label>'+
			'</div>';
				
		}
		winnerDiv +='</div></marquee>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}
}

function fillLiveUserList(data){
	if(data.userList!=null && data.userList != '' && data.userList.length > 0){
		var userString = '';
		$('#liveUsersDiv').empty();
		for(var i=0;i<data.userList.length;i++){
			userString += '<div class="form-group col-sm-2 col-xs-2">'+
			'<label id="totalUserCountlbl" style="font-size: 20px;">'+data.userList[i].uId+'</label></div>';
		}
		$('#liveUsersDiv').append(userString);
	}
}

function renderSummaryWinners(winner){
	var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
	$('#winnerDiv').empty();
	if(winner.length> 0){
		$('#winnerRewardLbl').text('$'+winner[0].rPointsSt);
	}
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
		'<label style="font-size: 55px;">'+winner[i].uId+'</label>'+
		'</div>';
			
	}
	winnerDiv +='</div></marquee>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv);
}

function renderMegaJackpotWinners(winner){
	var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
	$('#winnerDiv').empty();
	for(var i=0;i<winner.length;i++){
			winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
		'<label style="font-size: 30px;">'+winner[i].rPointsSt+'</label><br/><label style="font-size: 30px;">'+winner[i].jCreditSt+'</label>'+
		'<br/><label style="font-size: 50px;">'+winner[i].uId+'</label></div>';
			
	}
	winnerDiv +='</div></marquee>';
	$('#winnerDiv').show();
	$('#winnerDiv').append(winnerDiv);
	
}

function renderMiniJackpotWinners(winner,type,prize){
	if(winner.length <=3){
		var winnerDiv = '<div align="center" id="grandWinnerDiv"><table align="center"> <tr>';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(type+' - '+prize);
		}
		for(var i=0;i<winner.length;i++){
			winnerDiv +=  '<td align="center">'+
				'<label style="font-size: 60px;margin:15px;border:1px solid;text-align:center;width:350px;">'+winner[i].uId+'</label></td>';
		}
		winnerDiv +='</tr></table></div>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}else{
		var winnerDiv = '<marquee scrollamount="10"><div id="winnerDivStyle">';
		$('#winnerDiv').empty();
		if(winner.length> 0){
			$('#winnerRewardLbl').text(type+' - '+prize);
		}
		for(var i=0;i<winner.length;i++){
				winnerDiv += '<div style="margin:15px;border:1px solid;text-align:center;" class="form-group col-sm-4 col-xs-4">'+
			'<label style="font-size: 55px;">'+winner[i].uId+'</label>'+
			'</div>';
				
		}
		winnerDiv +='</div></marquee>';
		$('#winnerDiv').show();
		$('#winnerDiv').append(winnerDiv);
	}
	
}


function resetContest(action){
	var confirmText;
	if(action == 'RESET'){
		confirmText = "Are you sure to reset Contest data ?";
	}else if(action == 'END'){
		confirmText = "Are you sure to end contest ?";
	}else if(action == 'ACTIVE'){
		confirmText = "Are you sure to move contest to active?";
	}
	
	jConfirm(confirmText,"Confirm",function(r){
		if (r) {
			$.ajax({
					url : "${pageContext.request.contextPath}/ResetContest",
					type : "post",
					dataType: "json",
					data : "contestId="+$('#runningContestId').val()+"&action="+action,
					success : function(response) {
						var jsonData = JSON.parse(JSON.stringify(response));
						jAlert(jsonData.msg);
						if(action == 'END' && jsonData.status==1){
							window.location="${pageContext.request.contextPath}/ContestsFirebaseMobile?status=EXPIRED";
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
					});
		} else {
			return false;
		}
	});
	return false;
}

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Today's Contest</li>
			<li style="float:right;" ><button type="button" class="btn btn-primary" onclick="resetContest('END');">End Contest</button></li>
		</ol>
		
	</div>
</div>
<div id="contestDiv">	
	<div class="panel-body1 full-width">
		
		<form name="runningContestForm" id="runningContestForm" method="post">
			<input type="hidden" id="runningContestId" name="runningContestId" value="${contest.id}" />
			<input type="hidden" id="runningQuestionNo" name="runningQuestionNo"  value="${lastQuestionNo}"/>
			<div class="form-group tab-fields">
				<div class="form-group col-sm-4 col-xs-4" id="contestInfoDiv1">
					<label style="font-size: 25px;">${contest.contestName}</label><br/>
					<label style="font-size: 20px;">${contest.startDateTimeStr}</label><br/>
				</div>
				<div class="form-group col-sm-4 col-xs-4" id="contestInfoDiv2">
					<label style="font-size: 20px;">Grand Prize : ${contest.freeTicketPerWinner} Free Tickets of ${contest.promoRefName}</label><br/>
					<label style="font-size: 20px;">Reward Dollars : ${contest.rewardPoints}</label><br/>
					
				</div>
				<div class="form-group col-sm-4 col-xs-4" id="questionSizeDiv">
					<label style="font-size: 20px;">Question Size : ${contest.questionSize}</label>
				</div>
				<div class="form-group col-sm-4 col-xs-4">
					<label style="font-size: 20px;">Total User Count :&nbsp;&nbsp;&nbsp;</label>
					<label id="totalUserCountlbl" style="font-size: 20px;">0</label>
				</div>
				<div class="form-group col-sm-4 col-xs-4" id="lifelineCountDiv">
					<label style="font-size: 20px;">Last Q Used Lifeline Count :&nbsp;&nbsp;&nbsp;</label>
					<label id="lifelineCountLbl" style="font-size: 20px;"></label>
				</div>
				<div id="liveUsersDiv"></div>
				
				</div>
				<%-- <div class="form-group col-sm-8 col-xs-8">
					<c:if test="${empty msg}">
						<label style="font-size: 15px;">Note : Once contest is started please do not refresh or leave page or contest data will be lost.</label>
					</c:if>
				</div> --%>
			</div>
			<div class="form-group tab-fields">
			<hr style="width: 100%; color: black; height: 1px; background-color:black;" />
				<div id="runningQuestionDiv" class="form-group col-sm-6 col-xs-6">
					<label id="runningQuestionText" style="font-size: 35px;"></label>
					
				</div>
				<div class="form-group col-sm-6 col-xs-6">
					<label id="nextQuestionText" style="font-size: 35px;"></label>
				</div>
			</div>
			<br/><br/>
			<div align="center" id="winnerRewardDollarDiv">
				<label id= "winnerRewardLbl" style="font-size: 80px;"></label>
			</div>
			<div id="winnerDiv" class="form-group tab-fields" style="display:none">
				
			</div>
			<div class="form-group tab-fields" style="display:none;" id="optionDiv">
				<div id="optionADiv" class="form-group col-sm-6 col-xs-6" style="margin-left: 20px;">
					<label style="font-size: 20px;">A.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionA" style="font-size: 30px;"></label>
					<label id="questionOptionACount" style="font-size: 30px;float:right;"></label>
				</div>
				<div id="nextADiv" class="form-group col-sm-5 col-xs-5" style="display:none;margin-left: 20px;">
					<label style="font-size: 20px;">A.&nbsp;&nbsp;&nbsp;</label>
					<label id="nextQuestionOptionA" style="font-size: 30px;"></label>
				</div>
				<!-- <div class="form-group col-sm-4 col-xs-4" style="margin-left: 60px;">
					<label id="questionRewardDollar" style="font-size: 30px;"></label>
				</div> -->
				<div id="optionBDiv" class="form-group col-sm-6 col-xs-6" style="margin-left: 20px;">
					<label style="font-size: 20px;">B.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionB" style="font-size: 30px;"></label>
					<label id="questionOptionBCount" style="font-size: 30px;float:right;"></label>
				</div>
				<div id="nextBDiv" class="form-group col-sm-5 col-xs-5" style="display:none;margin-left: 20px;">
					<label style="font-size: 20px;">B.&nbsp;&nbsp;&nbsp;</label>
					<label id="nextQuestionOptionB" style="font-size: 30px;"></label>
				</div>
				<div id="optionCDiv" class="form-group col-sm-6 col-xs-6" style="margin-left: 20px;">
					<label style="font-size: 20px;">C.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionC" style="font-size: 30px;"></label>
					<label id="questionOptionCCount" style="font-size: 30px;float:right;"></label>
				</div>
				<div id="nextCDiv" class="form-group col-sm-65 col-xs-5" style="display:none;margin-left: 20px;">
					<label style="font-size: 20px;">C.&nbsp;&nbsp;&nbsp;</label>
					<label id="nextQuestionOptionC" style="font-size: 30px;"></label>
				</div>
				<!-- <div class="form-group col-sm-6 col-xs-6">
					<label>D.&nbsp;&nbsp;&nbsp;</label>
					<label id="runningQuestionOptionD"></label>
				</div> -->
			</div>
		</form>
		<div class="form-group tab-fields" style="text-align: center;">
				<div class="form-group col-sm-12 col-xs-12">
				<c:if test="${empty msg}">
					<button class="btn btn-primary" id="nextQuestion" type="button" onclick="nextQuestion()">${buttonText}</button>
				</c:if>
				<c:if test="${not empty msg}">
					<label style="font-size: 18px;;color:red;">${msg}</label>	
				</c:if>
				</div>
		</div>
		<!-- <div class="form-group tab-fields">
			<hr style="width: 100%; color: black; height: 1px; background-color:black;" />
				<div class="form-group col-sm-12 col-xs-12">
					<label id="queNo" style="font-size: 15px;"><b>&nbsp;&nbsp;&nbsp;</b></label>
					<label id="nextQuestionText" style="font-size: 35px;"></label>
					
				</div>
		</div>
		<div class="form-group tab-fields" style="display:none;" id="optionDiv">
			<div id="optionADiv" class="form-group col-sm-7 col-xs-7" style="margin-left: 20px;">
				<label style="font-size: 20px;">A.&nbsp;&nbsp;&nbsp;</label>
				<label id="nextQuestionOptionA" style="font-size: 30px;"></label>
			</div>
			<div id="optionBDiv" class="form-group col-sm-7 col-xs-7" style="margin-left: 20px;">
				<label style="font-size: 20px;">B.&nbsp;&nbsp;&nbsp;</label>
				<label id="nextQuestionOptionB" style="font-size: 30px;"></label>
			</div>
			<div id="optionCDiv" class="form-group col-sm-7 col-xs-7" style="margin-left: 20px;">
				<label style="font-size: 20px;">C.&nbsp;&nbsp;&nbsp;</label>
				<label id="nextQuestionOptionC" style="font-size: 30px;"></label>
			</div>
		</div> -->
					
	</div>
</div>

<!-- Start contest modal -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="contestStartModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Started</h4>
			</div>
			<div class="modal-body full-width">
				<form name="runningContestForm" id="runningContestForm" method="post">
					<input type="hidden" id="runningContestId" name="runningContestId" />
					<input type="hidden" id="runningQuestionNo" name="runningQuestionNo" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label id="queNo">&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionText"></label>
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>A.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionA"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>B.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionB"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>C.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionC"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>D.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionD"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Answer:&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionAnswer"></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="nextQuestion" type="button" onclick="nextQuestion()">Question-1</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div> -->
<!-- Start contest modal  -->

<script type="text/javascript">

	//call functions once page loaded
	window.onload = function() {
		enableMenu();
	};
		
</script>