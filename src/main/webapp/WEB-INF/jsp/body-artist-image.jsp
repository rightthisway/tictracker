<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--
<link href="../resources/css/datepicker.css" rel="stylesheet">

<script src="../resources/js/bootstrap-datepicker.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>-->

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<script src="../resources/js/bootstrap-datepicker.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

 .cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
    .slick-headerrow-column {
     background: #87ceeb;
     text-overflow: clip;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
	}
	.slick-headerrow-column input {
	     margin: 0;
	     padding: 0;
	     width: 100%;
	     height: 100%;
	     -moz-box-sizing: border-box;
	     box-sizing: border-box;
	}
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
  
</style>

<script type="text/javascript">
/*
var jq2 = $.noConflict(true);
$(document).ready(function(){
	
	var allArtists = '${artistsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allArtists=='true'){
			$('#artistsCheckAll').attr("checked","checked");
		}
		allArtists='false';
	}
	selectCheckBox();
	
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	 jq2('#autoChildGrandChild').autocomplete("AutoCompleteChildAndGrandChild", {
			width: 650,
			max: 1000,
			minChars: 2,		
			dataType: "text",
			
			formatItem: function(row, i, max) {
				if(row[0]=='CHILD'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} else {
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				}
			}
		}).result(function (event,row,formatted){
			
				$('#autoChildGrandChild').val('');
				$('#selectedValue').text(row[2]);
				if(row[0]=='CHILD'){
					$('#selectedOption').text('Child');
					$("#child").val(row[1]);
					$("#grandChild").val('');
					getArtists('childId',row[1]);
					
				} else {
					$('#selectedOption').text('GrandChild');
					$("#grandChild").val(row[1]);
					$("#child").val('');
					getArtists('grandChildId',row[1]);
					
				} 
		}); 
});

function callHideGridDiv() {
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
}

function getArtists(isArtist,id){
	
	var url = "";
	if(isArtist == 'childId'){
		url = "GetArtistsByChildAndGrandChild?childId="+id;
	} else{
		url = "GetArtistsByChildAndGrandChild?grandChildId="+id;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#artists').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"
				$('#artists').append(rowText);
            }	
		}
	}); 
} 
function callSearchBtnClick(action){
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
	 var flag = true;
	 var artist=$("#artists").val();
	  if(artist==null || artist==''){
		  jAlert('Please select the artists.');
		  flag= false;
		  return false;
	  }
		
	 if(flag) {
	 	$("#action").val(action);
	 	$("#artistImageForm").submit();
	 }
}
function selectAllArtists(){
	if((document.getElementById("artistsCheckAll").checked)){
		$("#artists").each(function(){
			$("#artists option").attr("selected","selected"); 
		});
	}
	else{
		$("#artists").each(function(){
			$("#artists option").removeAttr("selected"); 
		});
	}
}

function callChangeImage(artistId){
	$('#imageUploadDiv_'+artistId).show();
	$('#imageDisplayDiv_'+artistId).hide();
	$('#fileRequired_'+artistId).val('Y');
	selectRow(artistId);
}

function selectRow(artistId) {
	$('#checkbox_'+artistId).attr('checked', true);
}
var validFilesTypes = ["jpg", "jpeg","png","gif"];;

    function CheckExtension(file) {
        //global document: false 
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
        	file.focus();
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
            file.value = null;
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        //global document: false 
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
        	file.focus();
            jAlert("Image Size Should be Greater than 0 and less than 1 MB.");
            file.value = null;
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null || file.value == '') {
    		jAlert("Please select valid image to upload.");
    		file.focus();
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callCardTypeChange(obj,rowId) {
	if(obj.value=='PROMOTION') {
		$('#cardDetailDiv_'+rowId).show();
		$('#tmatArtistDiv_'+rowId).show();
	} else if(obj.value=='YESNO') {
		$('#cardDetailDiv_'+rowId).show();
		$('#tmatArtistDiv_'+rowId).hide();
	} else {
		$('#cardDetailDiv_'+rowId).hide();
		$('#tmatArtistDiv_'+rowId).hide();
	}
}


function callSaveBtnOnClick(action) {
	
	var flag = true;
	
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		id = this.id.replace('checkbox','fileRequired');
		var fileRequired = $('#'+id).val();
		if(fileRequired == '' || fileRequired == 'Y') {
			id = this.id.replace('checkbox','file');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
		}
		if(!flag) {
			return false;
		}
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one artist/team to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to save selected artist/team images ?.","Confirm",function(r){
		  if(r) {
				$("#action").val(action);
			 	$("#artistImageForm").submit();
			} 
	  });
	}
	
	
}
function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing artist/team image to delete.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to remove selected artist/team images ?.","Confirm",function(r){
		  if(r) { 
			$("#action").val(action);
			$("#artistImageForm").submit();
			} 
	  });
	}
	
	
}
*/
</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Images</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Images</a></li>
						<li><i class="fa fa-laptop"></i>Artist/Team</li>						  	
					</ol>
				</div>
</div>
<!--
<div class="container">
<div class="row">

	<div class="alert alert-success fade in" id="infoMainDiv"
	<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="artistImageForm" id="artistImageForm" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/ArtistImages">
	<input type="hidden" id="action" name="action" />
	<div class="row">
		<div class="form-group" >
          <label for="imageText" class="col-lg-3 col-lg-offset-2 control-label"><b>Select Child or GrandChild :</b></label>
          <div class="col-lg-4">
          		<input type="hidden" id="child" name="child" value ="${child}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<div class="form-group" >
                    <div >
                    	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="autoChildGrandChild" id="autoChildGrandChild" onchange="callHideGridDiv();">
                    </div>
                </div>
          </div>
          <div class="col-lg-2">
          </div>
           </div>
           <div class="form-group" >
	          <label for="imageText" class="col-lg-3 col-lg-offset-2 control-label">
	          	<b><span id="selectedOption" >${selectedOption}</span></b>
	          </label>
          <div class="col-lg-4 ">
          	<label for="imageText" class="control-label">
	          	<b><span id="selectedValue" >${selectedValue}</span></b>
          </label>
          </div>
			<div class="col-lg-2"></div>           
          </div>
          <div class="form-group" >
          <label for="imageText" class="col-lg-3 col-lg-offset-2 control-label"><b>Artists : </b></label>
          <div class="col-lg-4">
          		<input type="hidden" id="artistStr" name="artistStr" value = ${artistStr}/> 
				<input type="checkbox" id="artistsCheckAll" name="artistsCheckAll" onclick ="selectAllArtists()" 
					<c:if test ="${artistsCheckAll}"> checked="checked" </c:if>/> 
				<label class="control-label" for="artistsCheckAll">Select All</label>
				<div class="form-group" >
                    <div >
                    <select multiple class="form-control fullWidth" name="artists" id="artists" onchange="callHideGridDiv();">
                      <c:forEach items="${artists}" var="artist">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${artist.id}${temp}"/>
						<option 
							<c:if test="${fn:contains(artistStr,temp2)}"> Selected </c:if>
							value="${artist.id}"> ${artist.name}</option>
						</c:forEach> 
                     </select>
                    </div>
                </div>
          </div>
          <div class="col-lg-2">
          </div>
           </div>
          <div class="form-group" >
	          <div class="col-lg-2 col-lg-offset-4">
	          <button type="button" onclick="callSearchBtnClick('search');" class="btn btn-primary btn-sm">Search</button>
	          </div>
          </div>
	</div>
<div class="row clearfix" id="gridDiv">
	<c:if test="${not empty artistImageList}">
	<div class="col-md-12 column">
			<div class="pull-right">
					<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
				<br />
				<br />
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-4">
							Artist
						</th>
						<th class="col-lg-7">
							Image
						</th>
						
						
					</tr>
				</thead>
				<tbody>
				 <c:forEach var="aImage" varStatus="vStatus" items="${artistImageList}">
                    <tr <c:if test="${aImage.id ne null}">style="background-color: #caf4ca;"</c:if> >
						<td>
	                      	<input type="checkbox" class="selectCheck" id="checkbox_${aImage.artist.id}" name="checkbox_${aImage.artist.id}" />
							<input type="hidden" name="id_${aImage.artist.id}" id="id_${aImage.artist.id}" value="${aImage.id}" />
							<input type="hidden" name="fileRequired_${aImage.artist.id}" id="fileRequired_${aImage.artist.id}" 
							<c:if test="${aImage.id ne null}"> value="N" </c:if>
							<c:if test="${aImage.id eq null}"> value="Y" </c:if> />
                      </td>
                      <td style="font-size: 13px;" align="center">
                     		<b><label for="artist" class="list-group-item" id="artist_${aImage.artist.id}">${aImage.artist.name}</label></b>
                     		
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="form-group form-group-top" id="imageUploadDiv_${aImage.artist.id}" 
                                  <c:if test="${aImage.id ne null }">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-lg-3 control-label">Image File</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="file_${aImage.artist.id}" name="file_${aImage.artist.id}" onchange="selectRow('${aImage.artist.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="imageDisplayDiv_${aImage.artist.id}" 
                                   <c:if test="${aImage.id eq null}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${aImage.id ne null}">
                                          <img src='${pageContext.request.contextPath}/RewardTheFan/GetImageFile?type=artistImage&filePath=${aImage.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage('${aImage.artist.id}');">
											Change Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>
		</div>
	</c:if>
	<c:if test="${empty artistImageList and not empty artistStr}">
	<div class="alert alert-block alert-danger fade in" id="infoMainDiv">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span>No records Found.</span>
    </div>
	</c:if>
	</div>
	</form>
</div>
-->


<c:if test="${successMessage != null}">
	<div id="successMessage" class="alert alert-success fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong
			style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<br/>

<div style="position: relative">
	<div style="width: 100%;">	
		<div class="grid-header" style="width: 100%">
			<label>Manage Artist</label>
				<div class="pull-right">
					<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
					<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'> Reset Filters &nbsp; |</a>
				</div>
		</div>
		<div id="artistGrid"style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="pager" style="width: 100%; height: 20px;"></div>
		<br />
	</div>
</div>
<br/>

<div id="artistImageDiv" style="display: none">
	<form role="form" name="artistImageForm" enctype="multipart/form-data" id="artistImageForm" method="post" modelAttribute="artistImage" action="${pageContext.request.contextPath}/GetArtistImages">
	<input type="hidden" id="action" name="action" />
	<input type="hidden" id="artistIdStr" name="artistIdStr" />	
	<input type="hidden" name="fileRequired" id="fileRequired" />
	<input type="hidden" name="id" id="id" />
	<div class="form-group form-group-top mt-20" id="imageFileDiv" style="width: 100%">
			 
             <div class="col-sm-6 col-xs-12 mb-20">
				<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 <input type="file" id="file" name="file">
             </div>
			 <div class="col-sm-6 col-xs-12 mb-20">
				<button type="button" class="btn btn-primary" onclick="callSubmitForm('update');">Save</button>
			 </div>
    </div>
    <div class="form-group form-group-top" id="cardImageDiv" style="width: 100%">
            <div class="col-lg-3"> &nbsp; </div>
			<div class="col-lg-6">
                <img id="artistImgTag" src="${api.server.url}GetImageFile?type=artistImage&filePath="+artistImageInf height="150" width="400" />
                <a style="color:blue " href="javascript:callChangeImage();">Change Image</a>
            </div>
			<div class="col-lg-3">&nbsp;</div>
    </div>
     </form>
</div>

<script>
	var artistImageInf;
	var artistId;
	var dataView;
	var grid;
	var data = [];
	var columns=[];
	var artistGridSearchString='';
	var columnFilters = {};
	var userArtistColumnsStr = '<%=session.getAttribute("artistgrid")%>';
	var userArtistColumns =[];
	var allArtistColumns = [ {	id: "artist", 
		name: "Artist", 
		field: "artist",
		width:80,		
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,			
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	}, {
		id : "imgUploaded",
		name : "Image Uploaded",
		field : "imgUploaded",
		width:80,
		sortable : true		
	}, {
		id : "eventCount",
		name : "Event Count",
		field : "eventCount",
		width:80,
		sortable : true		
	}, /*{id: "editCol", field:"editCol", name:"Edit", width:20, formatter:editFormatter},*/
       {id:"delCol", name:"Delete", field: "delCol", width:10, formatter:deleteFormatter}
	];

	if(userArtistColumnsStr!='null' && userArtistColumnsStr!=''){
		var columnOrder = userArtistColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allArtistColumns.length;j++){
				if(columnWidth[0] == allArtistColumns[j].id){
					userArtistColumns[i] =  allArtistColumns[j];
					userArtistColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}			
		}
	}else{
		userArtistColumns = allArtistColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "artistName";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
		
	function deleteRecordFromGrid(id) {
		dataView.deleteItem(id);
		grid.invalidate();
	}
		
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getArtistGridData(pageNo);
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+artistGridSearchString;
	    //var url = "${pageContext.request.contextPath}/ArtistImagesExportToExcel?"+appendData;
	    var url = apiServerUrl+"ArtistImagesExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		artistGridSearchString='';
		columnFilters = {};
		getArtistGridData(0);
	}
	
	function getArtistGridData(pageNo) {
		var temprEventRowIndex = grid.getSelectedRows([0])[0];
		if (temprEventRowIndex > -1) {
			artistId = grid.getDataItem(temprEventRowIndex).artistId;
			$("#action").val('update');
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/ArtistImages.json",
			type : "post",
			dataType: "json",
			data : $("#artistImageForm").serialize()+"&artistId="+artistId+"&pageNo="+pageNo+"&headerFilter="+artistGridSearchString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.artistPagingInfo;
				refreshArtistGridValues(jsonData.artistList);
				clearAllSelections();
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshArtistGridValues(jsonData) {
		data = [];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  artist = jsonData[i];
				var d = (data[i] = {});
				d["artistId"] = artist.artistId;
				d["id"] =  i;
				d["grandChildCategory"] = artist.grandChildCategory;
				d["childCategory"] = artist.childCategory;
				d["parentCategory"] = artist.parentCategory;
				d["artist"] = artist.artistName;
				d["imageFileUrl"] = artist.imageFileUrl;
				d["imgUploaded"] = artist.imgUploaded;
				d["eventCount"] = artist.eventCount;
			}
		}

		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#artistGrid", dataView, userArtistColumns, options);
		grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		grid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"),pagingInfo);
		}
		
		var columnpicker = new Slick.Controls.ColumnPicker(allArtistColumns, grid,
				options);
		
		grid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = grid.getSelectedRows([0])[0];
			if (temprEventRowIndex != artistId) {
				artistId = grid.getDataItem(temprEventRowIndex).artistId;
				
				$('#successMessage').hide();
				getArtistImageForEdit(artistId);
			}
		});
		
		grid.onSort.subscribe(function(e, args) {
			sortdir = args.sortAsc ? 1 : -1;
			sortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				dataView.fastSort(sortcol, args.sortAsc);
			} else {
				dataView.sort(comparer, args.sortAsc);
			}
		});
		// wire up model events to drive the grid
		dataView.onRowCountChanged.subscribe(function(e, args) {
			grid.updateRowCount();
			grid.render();
		});
		dataView.onRowsChanged.subscribe(function(e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});

		$(grid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	artistGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						 artistGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getArtistGridData(0);
				}
			  }
		 
		});
		grid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'imgUploaded' && args.column.id != 'editCol' && args.column.id != 'delCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}			
		});
		grid.init();
		
		// initialize the model after all the events have been hooked up
		dataView.beginUpdate();
		dataView.setItems(data);
		
		dataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		dataView.syncGridSelection(grid, true);
		$("#gridContainer").resizable();
		grid.resizeCanvas();
	}
	
	function getArtistImageForEdit(artistId){		
		$.ajax({
				url : "${pageContext.request.contextPath}/GetArtistImagesForEdit",
				type : "post",
				data:"artistId="+artistId,
				success : function(response){
					var data = JSON.parse(JSON.stringify(response));					
					showArtistImageInfo(data.artistImage);
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
		});
	}
	
	function showArtistImageInfo(artistImageInfo){
		artistImageInf = artistImageInfo.imageFileUrl;		
		if(artistImageInfo.id != null && artistImageInfo.id > 0) {
			$('#id').val(artistImageInfo.id);
			$('#artistImageDiv').show();
			$('#imageFileDiv').hide();
			//$("#artistImgTag").attr("src","${pageContext.request.contextPath}/RewardTheFan/GetImageFile?type=artistImage&filePath="+artistImageInf);
			$("#artistImgTag").attr("src", apiServerUrl+"GetImageFile?type=artistImage&filePath="+artistImageInf);
			$('#cardImageDiv').show();
		}else{
			$('#id').val('');
			$('#artistImageDiv').show();
			$('#imageFileDiv').show();
			$('#cardImageDiv').hide();
		}
	}
	
	//function for deleting shipping address functionality
	function deleteFormatter(row,cell,value,columnDef,dataContext){  
	   /*  var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />"; */
		var button = "<img class='deleteClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.artistId +"'/>";
		//the id is so that you can identify the row when the particular button is clicked
		return button;
		//Now the row will display your button
	}

	//function for edit functionality
	function editFormatter(row,cell,value,columnDef,dataContext){
		//the id is so that you can identify the row when the particular button is clicked
		var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.artistId +"'/>";
		return button;
	}

	//Function to hook up the edit button event for Shipping Address
	$('.editClickableImage').live('click', function(){
		var me = $(this), id = me.attr('id');
		getArtistImageForEdit(id);
	});

	//Now you can use jquery to hook up your delete button event for Shipping Address
	$('.deleteClickableImage').live('click', function(){
		var me = $(this), id = me.attr('id');
		var delFlag = callDeleteBtnOnClick();//confirm("Are you sure,Do you want to Delete it?");
	});

	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshArtistGridValues(JSON.parse(JSON.stringify(${artistList})));		
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
	});
	
	function saveUserArtistPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('artistgrid',colStr);
	}
	
	function callChangeImage(){
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		$('#successMessage').hide();
	}
	
	function callDeleteBtnOnClick() {
		var temprEventRowIndex = grid.getSelectedRows([0])[0];
		var imgUploadedStatus = grid.getDataItem(temprEventRowIndex).imgUploaded;
		if(imgUploadedStatus == 'Yes'){
			jConfirm("Are you sure you want to delete this Artist Image ?","Confirm",function(r){
			if(r){
				callSubmitForm('delete');
			}else{}
			});
		}else{
			jAlert("There is no file uploaded.");
			return;
		}
	}
	
	function callSubmitForm(action) {
		var temprEventRowIndex = grid.getSelectedRows([0])[0];
		if (temprEventRowIndex > -1) {
			artistId = grid.getDataItem(temprEventRowIndex).artistId;
		}
		$('#artistIdStr').val(artistId);
		$("#action").val(action);
		$('#fileRequired').val('Y');
		$("#artistImageForm").attr('action', '${pageContext.request.contextPath}/updateArtistImages');
		$("#artistImageForm").submit();		
	}
	</script>