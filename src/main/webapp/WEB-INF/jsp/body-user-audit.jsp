<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
  
 <link href="../resources/css/datepicker.css" rel="stylesheet">  
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script src="../resources/js/app/userAudit.js"></script>

<script>

$(document).ready(function(){
	
	$('#userAudit_fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation:"bottom",
		todayHighlight: true
    });
	
	$('#userAudit_toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation:"bottom",
		todayHighlight: true
    });
	
});

//Start - Audit User
function getUserAudit(userId) {
	$.ajax({			  
		url : "${pageContext.request.contextPath}/Admin/AuditUser",
		type : "post",
		data : "userId="+userId,
		dataType:"json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.msg != null && jsonData.msg != "") {
				jAlert(jsonData.msg);
				userAuditColumnFilters = {};
				userAuditGridValues('', '');
			} 
			if(jsonData.status == 1){
				$('#audit-user').modal('show');
				$('#userAudit_userId').val(userId);
				$('#userEmail_Hdr_AuditUser').text(jsonData.userEmail);
				userAuditColumnFilters = {};					
				userAuditGridValues(jsonData.userActionList, jsonData.userAuditPagingInfo);
				$('#userAudit_fromDate').val(jsonData.fromDate);
				$('#userAudit_toDate').val(jsonData.toDate);
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
//End Audit User

</script>

<!-- popup Audit User -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#audit-user">Audit User</button> -->

	<div id="audit-user" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Audit User - Email : <span id="userEmail_Hdr_AuditUser" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Admin
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Admin</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Audit User</li>
						</ol>
					</div>
				</div>

				<br />
				
				<div class="form-group full-width">
					<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
						<label for="name" class="control-label">From Date</label>
						<input type="text" name="fromDate" id="userAudit_fromDate" class="form-control searchcontrol"/>
						<input type="hidden" id="userAudit_userId" name="userId" /> 
					</div>
					<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
						<label for="name" class="control-label">To Date</label>
						<input type="text" name="toDate" id="userAudit_toDate" class="form-control"/> 
					</div>
					<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">						
						<button type="button" class="btn btn-primary" style="margin-top : 18px;" onclick="getUserAuditGridData(0)">Search</button>
					</div>
				</div>
				
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>User Actions</label>
							 <div class="pull-right">
								<!-- <a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:userAuditResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="userAudit_grid" style="width: 100%; height: 200px;border:1px solid gray"></div>
						<div id="userAudit_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			
			<div class="modal-footer full-width">				
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Audit User -->