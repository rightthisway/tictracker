<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>
<script>
var stateName;
	$(document).ready(function() {
		stateName = "${order.blStateName}";
		loadState();
	});
	
	 window.onunload = function () {
        var win = window.opener;
        if (!win.closed) {
        	window.opener.getInvoiceGridData(0);
        }
    };

	function updateAddress() {
		if ($('#firstName').val() == '') {
			jAlert("Billing Address - FirstName is Mendatory.");
			return false;
		} else if ($('#lastName').val() == '') {
			jAlert("Billing Address - LastName is Mendatory.");
			return false;
		} else if ($('#street1').val() == '') {
			jAlert("Billing Address - Street-1 is Mendatory.");
			return false;
		} else if ($('#city').val() == '') {
			jAlert("Billing Address - City is Mendatory.");
			return false;
		} else if ($('#state').val() <= 0) {
			jAlert("Billing Address - State is Mendatory.");
			return false;
		} else if ($('#country').val() <= 0) {
			jAlert("Billing Address - Country is Mendatory.");
			return false;
		} else if ($('#zipcode').val() == '') {
			jAlert("Billing Address - Zipcode is Mendatory.");
			return false;
		} else if ($('#phone').val() == '') {
			jAlert("Billing Address - Phone is Mendatory.");
			return false;
		} else if ($('#email').val() == '') {
			jAlert("Billing Address - Email is Mendatory.");
			return false;
		}
		$("#action").val('update');
		$("#updateBillingAddress").submit();
	}
	function loadState(){
		var countryId = $('#country').val();
		if(countryId>0){
			$.ajax({
				url : "${pageContext.request.contextPath}/GetStates",
				type : "post",
				dataType:"json",
				data : "countryId="+countryId,
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					updateStateCombo(jsonData);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$('#state').empty();
			$('#state').append("<option value='-1'>--select--</option>");
		}
	}

	/* function createFedexLabel(){
		var serviceType = $('#serviceType').val();
		if(serviceType == 'select'){
			jAlert("Please choose service type");
			return false;
		}		
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/CreateFedexLabel",
			type : "post",
			dataType : "json",
			data : "invoiceId=" + $('#invoiceId').val()+"&serviceType="+serviceType,
			success : function(res) {
				var jsonData = JSON.parse(JSON.stringify(res));
				jAlert(jsonData.msg);
				
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
			});	
	} */
	
	function updateStateCombo(jsonData){
		$('#state').empty();
		$('#state').append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				$('#state').append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
			}
			if(stateName!=''){
			$("#state option:contains(" + stateName + ")").attr('selected', 'selected');
		}
		}
	}
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Invoice</a>
			</li>
			<li><i class="fa fa-laptop"></i>Order Billing Address</li>
		</ol>
	</div>
</div>
<br />
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<div id="row">
	<div class="col-lg-12">
		<form class="form-validate form-horizontal" id="updateBillingAddress" method="post" action="${pageContext.request.contextPath}/Accounting/EditBillingAddress">
			<div class="form-group">
				<input type="hidden" id="action" name="action" value="" /> <input type="hidden" id="orderId" name="orderId" value="${order.orderId}" />
				<div class="col-sm-6">
					<label>First Name <span class="required">*</span>
					</label> <input type="text" id="firstName" name="firstName" value="${order.blFirstName}" class='form-control' />
				</div>
				<div class="col-sm-6">
					<label>Last Name <span class="required">*</span>
					</label> <input type="text" id="lastName" name="lastName" value="${order.blLastName}" class='form-control' />
				</div>

			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label>Street-1 </label> <input type="text" id="street1" name="street1" value="${order.blAddress1}" class='form-control' />
				</div>
				<div class="col-sm-6">
					<label>Street-2 </label> <input type="text" id="street2" name="street2" value="${order.blAddress2}" class='form-control' />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label>City <span class="required">*</span>
					</label> <input type="text" id="city" name="city" value="${order.blCity}" class='form-control' />
				</div>
				<div class="col-sm-6">
					<label>Country <span class="required">*</span>
					</label> <select id="country" name="country" class="form-control" onchange="loadState()">
						<option value='-1'>--select--</option>
						<c:forEach var="country" items="${countryList}">
							<option value="${country.id}" <c:if test="${order.blCountryName == country.name}"> selected </c:if>>
								<c:out value="${country.name}" />
							</option>
						<!--
							<option value="${country.id}" <c:if test="${order.shCountry == country.id}"> selected </c:if>>
								<c:out value="${country.name}" />
							</option>
						-->
						</c:forEach>
					</select>
				</div>

			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label>State <span class="required">*</span>
					</label> <select id="state" name="state" class="form-control" onchange="">
						<option value='-1'>--select--</option>
						 <c:forEach var="state" items="${stateList}">
							<option value="${state.id}" <c:if test="${order.blStateName == state.name}"> selected </c:if>>
								<c:out value="${state.name}" />
							</option>
							<!--
							<option value="${state.id}" <c:if test="${order.shState == state.id}"> selected </c:if>>
								<c:out value="${state.name}" />
							</option>
							-->
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-6">
					<label>Zipcode <span class="required">*</span>
					</label> <input type="text" id="zipcode" name="zipcode" value="${order.blZipCode}" class='form-control' />
				</div>

			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<label>Email <span class="required">*</span>
					</label> <input type="text" id="email" name="email" value="${order.blEmail}" class='form-control' />
				</div>
				<div class="col-sm-6">
					<label>Phone <span class="required">*</span>
					</label> <input type="text" id="phone" name="phone" value="${order.blPhone1}" class='form-control' />
				</div>
			</div>
			<%-- <c:if test="${not empty invoiceId}">
				<div class="form-group">
					<div class="col-sm-6">
						<label>Service Type <span class="required">*</span>
						</label>  <select id="serviceType" name="serviceType" class="form-control" onchange="">
							<option value='select'>--select--</option>
							<option value="FEDEX_EXPRESS_SAVER"> FEDEX EXPRESS SAVER </option>						
							<option value="FIRST_OVERNIGHT"> FIRST OVERNIGHT </option>
							<option value="PRIORITY_OVERNIGHT"> PRIORITY OVERNIGHT </option>
							<option value="STANDARD_OVERNIGHT"> STANDARD OVERNIGHT </option>
							<option value="FEDEX_2_DAY"> FEDEX 2-DAY </option>
							<option value="GROUND_HOME_DELIVERY"> GROUND HOME DELIVERY </option>
							<option value="INTERNATIONAL_PRIORITY"> INTERNATIONAL PRIORITY </option>
						</select>
					</div>
					<div class="col-sm-6">
						&nbsp;
					</div>
				</div>
			</c:if> --%>
			<input type="hidden" id="invoiceId" name="invoiceId" value="${invoiceId}" />
		</form>
	</div>
</div>
	<br />
<div id="row">
	<div class="col-lg-8">
		<div class="form-group">
			<div class="col-sm-3">
				<button type="button" onclick="updateAddress();"  class="btn btn-primary btn-sm">Update Address</button>
			</div>
		<%-- <c:if test="${not empty invoiceId}">
			<div class="col-sm-3">
				<button type="button" onclick="createFedexLabel();"  class="btn btn-primary btn-sm">Create Fedex Label</button>
			</div>
		</c:if> --%>
	</div>
</div>
</div>
<div id="row"><br/><br/></div>