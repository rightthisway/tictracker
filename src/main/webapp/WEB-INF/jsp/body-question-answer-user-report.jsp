<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script src="../resources/amcharts/core.js" type="text/javascript"></script>
<script src="../resources/amcharts/maps.js" type="text/javascript"></script>
<script src="../resources/amcharts/charts.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/moonrisekingdom.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/material.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/animated.js" type="text/javascript"></script>

<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-laptop"></i>Contest Question Wise User Answer Count Report Data</li>
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
				
</div>


<div>
	<!--<input type="button"class="btn btn-primary"  value="Download PDF" onclick="savePDF();" />-->
	<div id="chartdiv" style="height:500px;" class="col-md-12"></div>
</div>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});
	
});

function callAPIReport(reportURL){
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function openContestReportModal(reportName){
	$('#contestReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
}

function openCustChainReportModal(){
	$('#custChainReportModal').modal('show'); 
}

function generateReport(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var reportURL = $('#contestReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function generateChainReport(){
	var userId = $('#userId').val(); 
	if(null == userId || userId == "" || userId == " "){
		$('#userIdErrId').html("Please Enter Valid User ID."); 
		return;
	}else{
		$('#userIdErrId').html(""); 
		window.location.href = "${pageContext.request.contextPath}/Reports/DownloadCustomerStatsReport?userId="+userId;
	}
}

function generateBotReport(){ 
	window.location.href = "${pageContext.request.contextPath}/Reports/DownloadBotReport";
}


		

		// Create chart instance
		var chart = am4core.create("chartdiv", am4charts.XYChart);
		//var chart1 = am4core.create("chart1div", am4charts.XYChart);
		function getReportChart(answeredForEachGame){
		// Themes begin
		am4core.useTheme(am4themes_animated);	
		
		var chart = am4core.create("chartdiv", am4charts.XYChart);
				chart.maskBullets = false;
				chart.numberFormatter.numberFormat = "#.#";

				chart.data = JSON.parse(answeredForEachGame);

				chart.colors.list = [
				  am4core.color("#99cc00"),
				  am4core.color("#FFC75F")
				];
				
				var title = chart.titles.create();
				title.text = "Questions Attempted / Answered  Contest Wise Report";
				title.fontSize = 25;
				title.marginBottom = 30;

				// Create axes
				var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
				categoryAxis.dataFields.category = "viewName";
				categoryAxis.renderer.grid.template.location = 0;
				categoryAxis.renderer.labels.template.rotation = 270
				
				var label = categoryAxis.renderer.labels.template;
					label.wrap = true;
					label.maxWidth = 100;

				var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
				valueAxis.renderer.inside = true;
				valueAxis.renderer.labels.template.disabled = true;
				valueAxis.min = 0;
				valueAxis.extraMax = 0.1;
				valueAxis.calculateTotals = true;

				// Create series
				function createSeries(field, name) {
				  
				  // Set up series
				  var series = chart.series.push(new am4charts.ColumnSeries());
				  series.name = name;
				  series.dataFields.valueY = field;
				  series.dataFields.categoryX = "viewName";
				  series.sequencedInterpolation = true;
				  
				  // Make it stacked
				  series.stacked = true;
				  
				  // Configure columns
				  series.columns.template.width = am4core.percent(60);
				  series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
				  
				  // Add label
				  var labelBullet = series.bullets.push(new am4charts.LabelBullet());
				  labelBullet.label.text = "{valueY}";
				  labelBullet.label.fill = am4core.color("#fff");
				  labelBullet.locationY = 0.5;
				  
				  
				  return series;
				}

				createSeries("viewCountDouble", "Attempted");
				createSeries("viewCountDouble1", "Answered");

				chart.scrollbarX = new am4charts.XYChartScrollbar();
				chart.scrollbarX.series.push(series);		


				// Legend
				chart.legend = new am4charts.Legend();
				
				// Enable export
				chart.exporting.menu = new am4core.ExportMenu();
}



//call functions once page loaded
	window.onload = function() {
		 $("div#divLoading").addClass('show');
		getReportChart('${answeredForEachGame}');
		$('#pollingContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingContestPreference()'>");
		
		enableMenu();
	};		

</script>

<style>
	input{
		color : black !important;
	}
</style>




