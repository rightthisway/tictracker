<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
    <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker.css" type="text/css"/>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}

#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}
	
#totalPrice{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}

#totalQty{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}
</style>
<script>
function goToOrder(orderId){
	window.opener.location = "${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelOrders?status=All&orderId="+orderId;
	window.close();
}
</script>

    
<div class="row">
	<div class="col-lg-12">
		 
		<ol class="breadcrumb">
			<li><i class="fa fa-laptop"></i>Fantasy Order</li>
		</ol>
		
	</div>
</div>

<div class="row">
<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong
					style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					${successMessage} <a href="javascript:goToOrder(${cjeOrder.id})" style="font-size:15px;">${cjeOrder.id}</a>
				</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong
					style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>	
</div>
<br/><br/>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Order Details </b></header>
			<div class="panel-body">
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Order Number : </label>
					<span style="font-size: 12px;" >${cjeOrder.id} </span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Created Date : </label><span style="font-size: 12px;">${cjeOrder.createdDateStr}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Status : </label><span style="font-size: 12px;">${cjeOrder.status}</span>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Used Reward points : </label><span style="font-size: 12px;">${customerLoyalty.pointsSpent}</span>
                </div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Regular Order Id : </label><span style="font-size: 12px;">${cjeOrder.regularOrderId}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Is Package : </label>
					<c:if test="${cjeOrder.isPackageSelected ne null and cjeOrder.isPackageSelected == true}"><span style="font-size: 12px;">Yes</span></c:if>
					<c:if test="${cjeOrder.isPackageSelected eq null or cjeOrder.isPackageSelected== false}"><span style="font-size: 12px;">No</span></c:if>
				</div>
			</div>
			
		</div>
		</section>
	</div>
</div>

<br/><br/>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 25px;font-family: arial;" class="panel-heading"><b>Customer Details </b></header>
			<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Customer Name : </label>
					<span style="font-size: 12px;" id="customerNameDiv">${customer.customerName}&nbsp;${customer.lastName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Phone : </label><span style="font-size: 12px;">${customer.phone}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Email : </label><span style="font-size: 12px;">${customer.email}</span>
                </div>
			</div>
			
			<%-- <div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Product Type : </label><span style="font-size: 12px;">${customer.productType}</span>
                </div>
				<div class="col-sm-4">
					&nbsp;
				</div>
				<div class="col-sm-4">
					&nbsp;
				</div>
				
			</div> --%>
			
		</div>
		</section>
	</div>
</div>

<!-- Shipping Address section -->
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
		 <header style="font-size: 25px;font-family: arial;" class="panel-heading"><b>Event Details</b>
		 </header>
			<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Event Name :</label><span style="font-size: 12px;">${event.eventName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Event Date :</label><span style="font-size: 12px;">${event.eventDateStr} ${event.eventTimeStr}</span>
				</div>
				
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">League Name :</label><span style="font-size: 12px;">${cjeOrder.leagueName}</span>
				</div>
				
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Team Name :</label><span style="font-size: 12px;">${cjeOrder.teamName}</span>
				</div>
				<div class="col-sm-4">
					
				</div>
				<div class="col-sm-4">
					<%-- <label style="font-size: 13px; font-weight: bold;">Venue Name :</label><span style="font-size: 12px;">${eventInfo.building}</span> --%>
				</div>				
			</div>
			
		</div>
		</section>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 25px;font-family: arial;" class="panel-heading">
			<b>Ticket Details</b>
		 </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Section :</label>
					<span style="font-size: 12px;">${ticket.section}</span>
				</div>
				<c:choose>
				<c:when test="${cjeOrder.regularOrderId != null}">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Row :</label>
					<span style="font-size: 12px;">${ticket.row}</span>
				</div>
				</c:when>
				</c:choose>				
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Quantity :</label>
					<span style="font-size: 12px;">${ticket.quantity}</span>
				</div>				
			</div>
			
			<div class="form-group">
			<c:choose>
				<c:when test="${cjeOrder.regularOrderId != null}">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Price :</label>					
					<span style="font-size: 12px;">${ticket.price}</span>
				</div>
				</c:when>
				<c:otherwise>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Price :</label>					
					<span style="font-size: 12px;">${ticket.zonePrice}</span>
				</div>
				</c:otherwise>
			</c:choose>
			</div>
			
		</div>
		</section>
	</div>
</div>