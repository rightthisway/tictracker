<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Popular Events</title>
  
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>
	
	
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}
.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.deleteClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.auditClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

</style>


<script type="text/javascript">
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  eventGrid.resizeCanvas();
	  popEventGrid.resizeCanvas();
	});
	
});
setTimeout(function(){ 
	$('#event_pager> div').append("<span class='slick-pager-status'> -Total <b>${totalCount}</b> Events</span>"); 
	$('#event_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserEventPreference()'>");
	$('#popEvent_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPopEventPreference()'>");
		
}, 2000);
	function saveUserEventPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = eventGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('eventgrid',colStr);
	}
	function saveUserPopEventPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = popEventGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('popeventgrid',colStr);
	}
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchPopularEventsBtn').click();
		    return false;  
		  }
		});
	
</script>

</head>

<body>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Events</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Popular Events</li>
		</ol>

	</div>
	
	<div class="col-lg-12">
                <!--  search form start -->
			<form:form role="form" id="eventSearch" method="post" action="${pageContext.request.contextPath}/RewardTheFan/PopularEvents">
				<input type="hidden" id="productId" name="productId" value="${product.id}" />
				<input type="hidden" id="eventID" name="eventID">
				<div class="form-group col-xs-2 col-md-2">
					<label for="name" class="control-label">From Date</label>
					<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
				</div>
				<div class="form-group col-xs-2 col-md-2">
					<label for="name" class="control-label">To Date</label>
					<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
				</div>
				
							
				<div class="form-group col-xs-3 col-md-3">
					<label for="name" class="control-label">Search By</label>
					<select id="searchType" name="searchType" class="form-control ">
					  <option value="event_name">Event</option>
 					  <option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
					  <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >Grand Child Category</option>
					  <option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
					  <option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
					  <option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option>
					  
					</select>
					
				</div>
				
				<div class="form-group col-xs-3 col-md-3">
				<label for="name" class="control-label">Search Value</label>
					<input class="form-control searchcontrol" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
				</div>
				<div class="form-group col-xs-1 col-md-1">
				<label for="name" class="control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				 <button type="submit" id="searchPopularEventsBtn" class="btn btn-primary">search</button>
				 </div>
				 
               </form:form>
                <!--  search form end -->                
           </div>
           
</div>

<!-- event Grid Code Starts -->
<div style="position: relative" id="eventGridDiv">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Events Details</label> <span id="event_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"
				onclick="eventGridToggleFilterRow()"></span>
		</div>
		<div id="event_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="event_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="event_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with EventName including <input type="text" id="eventGridSearch">
</div>


<script>

var eventCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});

	var eventDataView;
	var eventGrid;
	var eventData = [];
	var userEventColumnsStr = '<%=session.getAttribute("eventgrid")%>';
	var userEventColumns =[];
	var allEventColumns = [ eventCheckboxSelector.getColumnDefinition(),
		{id : "eventMarketId",
		name : "Event Market Id",
		field : "eventMarketId",
		width:80,
		sortable : true
	},{id : "eventName",
		name : "Event Name",
		field : "eventName",
		width:80,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width:80,
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width:80,
		sortable : true
	}, {
		id : "dayOfTheWeek",
		name : "Day of Week",
		field : "dayOfTheWeek",
		width:80,
		sortable : true
	},{
		id : "venue",
		name : "Venue",
		field : "venue",
		width:80,
		sortable : true
	}, {
		id : "venueId",
		name : "Venue Id",
		width:80,
		field : "venueId",
		sortable : true
	},{
		id : "noOfTix",
		name : "No of Tix",
		width:80,
		field : "noOfTix",
		sortable : true
	},{
		id : "noOfTixSold",
		name : "No of Tix Sold",
		field : "noOfTixSold",
		width:80,
		sortable : true
	},{
		id : "city",
		name : "City",
		field : "city",
		width:80,
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		width:80,
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country",
		width:80,		
		sortable: true
	}, 
	//{id: "artist",name: "Artist",field: "artist",width:80,sortable: true},
		{id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		width:80,
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	},{
		id : "notes",
		field : "notes",
		name : "Notes",
		width:80,
		sortable: true,
		editor:Slick.Editors.LongText
		
	},{
		id : "eventCreated",
		field : "eventCreated",
		width:80,
		name : "Event Created",
		sortable: true
	} ,{
		id : "eventLastUpdated",
		field : "eventLastUpdated",
		name : "Event Last Updated",
		width:80,
		sortable: true
	} ];

	
	if(userEventColumnsStr!='null' && userEventColumnsStr!=''){
		columnOrder = userEventColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allEventColumns.length;j++){
				if(columnWidth[0] == allEventColumns[j].id){
					userEventColumns[i] =  allEventColumns[j];
					userEventColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userEventColumns = allEventColumns;
	}
	
	var eventOptions = {
		editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	
	

	var eventGridSortcol = "eventName";
	var eventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var eventGridSearchString = "";

	
	function deleteRecordFromEventGrid(id) {
		eventDataView.deleteItem(id);
		eventGrid.invalidate();
	}
	function eventGridFilter(item, args) {
		var x= item["eventName"];
		if (args.eventGridSearchString  != ""
				&& x.indexOf(args.eventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function eventGridComparer(a, b) {
		var x = a[eventGridSortcol], y = b[eventGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function eventGridToggleFilterRow() {
		eventGrid.setTopPanelVisibility(!eventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#event_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
			$(e.target).addClass("ui-state-hover")
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover")
		});

	
	function getEventGridData() {
		var searchValue = $("#searchValue").val();
		if(searchValue== null || searchValue == '') {
			jAlert('Enter valid key to searh events');
			return false;
		}
		
		$.ajax({
			  
			url : "${pageContext.request.contextPath}/GetEventDetails",
			type : "post",
			data : $("#eventSearch").serialize(),
			//dataType:"application/json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(res,true);
				//jAlert(jsonData);
				if(jsonData==null || jsonData=='') {
					jAlert("No Event found.");
				} 
				refreshEventGridValues(jsonData,true);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
function refreshEventGridValues(jsonData,isAjaxCall) {
		
		eventData = [];
		
		if(isAjaxCall) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (eventData[i] = {});
				d["id"] = i;
				//d["id"] = data.eventId;
				d["eventID"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDate;
				d["eventTime"] = data.eventTime;
				d["venue"] = data.venue;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				//d["artist"] = data.artist;
			}
		} else {
			var i=0;
			<c:forEach var="event" items="${eventsList}">
			var d = (eventData[i] = {});
			d["id"] = i;
			//d["id"] = "${event.eventId}";
			//d["id"] = "id_" + i;
			d["eventMarketId"] = "${event.eventId}";
			d["eventName"] = "${event.eventName}";
			d["eventDate"] = "${event.eventDateStr}";
			d["eventTime"] = "${event.eventTimeStr}";
			d["dayOfTheWeek"] = "${event.dayOfWeek}";
			d["venue"] = "${event.building}";
			d["venueId"] = "${event.venueId}";
			d["noOfTix"] = "${event.noOfTixCount}";
			d["noOfTixSold"] = "${event.noOfTixSoldCount}";
			d["city"] = "${event.city}";
			d["state"] = "${event.state}";
			d["country"] = "${event.country}";
			d["grandChildCategory"] = "${event.grandChildCategoryName}";
			d["childCategory"] = "${event.childCategoryName}";
			d["parentCategory"] = "${event.parentCategoryName}";
			//d["artist"] = "${event.artistName}";
			d["eventCreated"] = "${event.eventCreationStr}";
			d["eventLastUpdated"] = "${event.eventUpdatedStr}";
			d["notes"] = "${event.notes}";
			i++;
			</c:forEach>
		}

		eventDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		eventGrid = new Slick.Grid("#event_grid", eventDataView, userEventColumns, eventOptions);
		eventGrid.setSelectionModel(new Slick.RowSelectionModel());
		eventGrid.registerPlugin(eventCheckboxSelector);
		eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"));
		var eventGridColumnpicker = new Slick.Controls.ColumnPicker(allEventColumns, eventGrid,
				eventOptions);

		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#event_inlineFilterPanel").appendTo(eventGrid.getTopPanel()).show();

		/*eventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < eventDataView.getLength(); i++) {
				rows.push(i);
			}
			eventGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		eventGrid.onSort.subscribe(function(e, args) {
			eventGridSortdir = args.sortAsc ? 1 : -1;
			eventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				eventDataView.fastSort(eventGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				eventDataView.sort(eventGridComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the eventGrid
		eventDataView.onRowCountChanged.subscribe(function(e, args) {
			eventGrid.updateRowCount();
			eventGrid.render();
		});
		eventDataView.onRowsChanged.subscribe(function(e, args) {
			eventGrid.invalidateRows(args.rows);
			eventGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#eventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			eventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			eventDataView.setFilterArgs({
				eventGridSearchString : eventGridSearchString
			});
			eventDataView.refresh();
		}
		 eventGrid.onCellChange.subscribe(function (e,args) { 
				var temprEventRowIndex = eventGrid.getSelectedRows();
				var eventId;
				var eventNotes; 
				$.each(temprEventRowIndex, function (index, value) {
					eventId = eventGrid.getDataItem(value).eventMarketId;
					eventNotes = eventGrid.getDataItem(value).notes;
				});
				saveNotes(eventId,eventNotes);
         });
		
		
		// initialize the model after all the events have been hooked up
		eventDataView.beginUpdate();
		eventDataView.setItems(eventData);
		eventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			eventGridSearchString : eventGridSearchString
		});
		eventDataView.setFilter(eventGridFilter);
		eventDataView.endUpdate();
		eventDataView.syncGridSelection(eventGrid, true);
		$("#gridContainer").resizable();
		eventGrid.resizeCanvas();
		 $("div#divLoading").removeClass('show');
		/* var eventrowIndex;
		eventGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				 var eventId =eventGrid.getDataItem(temprEventRowIndex).id;
				//jAlert(eventId);
				//getCategoryTicketGroupsforEvent(eventId);
			}
		}); */
	}
	
	function getSelectedEventGridId() {
		var temprEventRowIndex = eventGrid.getSelectedRows();
		
		var eventIdStr='';
		$.each(temprEventRowIndex, function (index, value) {
			eventIdStr += ','+eventGrid.getDataItem(value).eventMarketId;
		});
		
		if(eventIdStr != null && eventIdStr!='') {
			eventIdStr = eventIdStr.substring(1, eventIdStr.length);
			 return eventIdStr;
		}
	}
	function getSelectedEventGridRowCount() {
		var temprEventRowIndex = eventGrid.getSelectedRows();
		
		var rowCount=0;
		$.each(temprEventRowIndex, function (index, value) {
			rowCount++;
		});
		
		return rowCount;
	}
	
	function updatePopularEvents(eventIds,productId) {
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePopularEvents",
			type : "post",
			data : "action=create&productId="+productId+"&eventIds="+ eventIds,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.length > 0){
					jAlert("Selected event added to popular events.");
				}					
				refreshpopEventGridValues(jsonData,true);
				eventGrid.setSelectedRows([]);
				clearAllSelections();
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function addPopularEvents() {
		var eventIds = getSelectedEventGridId();
		var productId=$("#productId").val();
		var flag = true;
		if(eventIds == null || eventIds=='') {
			flag= false;
			jAlert("Select an Events to add as Popular Events.");
			return false;
		}
		
		var selectedEventsCount = getSelectedEventGridRowCount();
		if(flag) {
			$.ajax({
				url : "${pageContext.request.contextPath}/GetPopularEventsCount",
				type : "post",
				data : "productId="+productId,
				success : function(res){
					//var jsonData = JSON.parse(JSON.stringify(res));
					if((selectedEventsCount+ parseInt(res)) > 20){
						jAlert("Maximum Eligible popluar events are 20.\n"
								+"Remove some existing poluar events(Count : "+parseInt(res)+") to add selected events as popular events.");
						return false;
					} else {
						updatePopularEvents(eventIds,productId);
					}				
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		return false;	
		} 
	}
	
	function saveNotes(eventId,notes){
		$.ajax({
			url : "${pageContext.request.contextPath}/saveEventNotes",
			type : "post",
			data : "eventId="+eventId+"&notes="+notes,
			success : function(res){
				jAlert(res);				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

</script>

<!-- event Grid Code Starts -->
<br />
<!-- <header class="panel-heading"> 
	<a data-toggle="modal" class="btn btn-primary" id="addCategoryTickets" onclick="addModal();">Add</a>
</header> -->

<!-- popEvent Grid Code Starts -->
<br />
<div style="position: relative;" id="popEventGridDiv">
<a data-toggle="modal" class="btn btn-primary" id="addPopularEvents" onclick="addPopularEvents();">Add Popular Events</a>
<div id="AddpopEventGroupSuccessMsg" style="display: none;" class="alert alert-success fade in">
 	Popular Events Added Successfully.
  </div>
  
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label id="popEventGridHeaderLabel">Popular Events</label> <span id="popEvent_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"
				onclick="popEventGridToggleFilterRow()"></span>
		</div>
		<div id="popEvent_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="popEvent_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="popEvent_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with EventName including <input type="text" id="popEventGridSearch">
</div>
<br />
<br />
<a data-toggle="modal" class="btn btn-primary" id="deletePopularEvents" onclick="deletePopularEvents();">Delete Popular Events</a>
<script>
var popEventCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var popEventDataView;
	var popEventGrid;
	var popEventData = [];
	var userPopEventColumnsStr = '<%=session.getAttribute("popeventgrid")%>';
	var userPopEventColumns =[];
	var allPopEventColumns =  [ popEventCheckboxSelector.getColumnDefinition(),
		{id : "eventName",
		name : "Event Name",
		field : "eventName",
		width:80,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width:80,
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width:80,
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		width:80,
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		width:80,
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		width:80,
		sortable : true
	},  {id: "country", 
		name: "Country", 
		field: "country",
		width:80,		
		sortable: true
	}, 
	//{id: "artist",name: "Artist",field: "artist",width:80,sortable: true}, 
		{id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	},{id: "createdBy", 
		name: "Created By", 
		field: "createdBy",
		width:80,
		sortable: true
	}, {
		id : "createdDate",
		field : "createdDate",
		name : "Created Date",
		width:80,
		sortable: true
	} ];

	
	if(userPopEventColumnsStr!='null' && userPopEventColumnsStr!=''){
		columnOrder = userPopEventColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allPopEventColumns.length;j++){
				if(columnWidth[0] == allPopEventColumns[j].id){
					userPopEventColumns[i] =  allPopEventColumns[j];
					userPopEventColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userPopEventColumns = allPopEventColumns;
	}
	
	var popEventOptions = {
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var popEventGridSortcol = "eventName";
	var popEventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var popEventGridSearchString = "";

	function deleteRecordFromEventGrid(id) {
		popEventDataView.deleteItem(id);
		popEventGrid.invalidate();
	}
	function popEventGridFilter(item, args) {
		var x= item["eventName"];
		if (args.popEventGridSearchString  != ""
				&& x.indexOf(args.popEventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.popEventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function popEventGridComparer(a, b) {
		var x = a[popEventGridSortcol], y = b[popEventGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function popEventGridToggleFilterRow() {
		popEventGrid.setTopPanelVisibility(!popEventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#popEvent_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

	function refreshpopEventGridValues(jsonData,isAjaxCall) {
		
		/*if(eventdetailStr != '0') {
			$('#popEventGridDiv').show();
			//$('#popEventGridHeaderLabel').text(eventdetailStr);
		}*/
		popEventData = [];
		if(isAjaxCall) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (popEventData[i] = {});
				d["id"] = i;
				//d["id"] = data.eventId;
				d["eventID"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDate;
				d["eventTime"] = data.eventTime;
				d["venue"] = data.venue;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				//d["artist"] = data.artist;
				d["createdBy"] = data.createdBy;
				d["createdDate"] = data.createdDate;
			}
		} else {
			var i=0;
			<c:forEach var="event" items="${popEvents}">
			var d = (popEventData[i] = {});
			d["id"] = i;
			//d["id"] = "${event.eventDetails.eventId}";
			//d["id"] = "id_" + i;
			d["eventID"] = "${event.eventDetails.eventId}";
			d["eventName"] = "${event.eventDetails.eventName}";
			d["eventDate"] = "${event.eventDetails.eventDateStr}";
			d["eventTime"] = "${event.eventDetails.eventTimeStr}";
			d["venue"] = "${event.eventDetails.building}";
			d["city"] = "${event.eventDetails.city}";
			d["state"] = "${event.eventDetails.state}";
			d["country"] = "${event.eventDetails.country}";
			d["grandChildCategory"] = "${event.eventDetails.grandChildCategoryName}";
			d["childCategory"] = "${event.eventDetails.childCategoryName}";
			d["parentCategory"] = "${event.eventDetails.parentCategoryName}";
			//d["artist"] = "${event.eventDetails.artistName}";
			d["createdBy"] = "${event.createdBy}";
			d["createdDate"] = "${event.createdDateStr}";
			i++;
			</c:forEach>
		}
		
		popEventDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		popEventGrid = new Slick.Grid("#popEvent_grid", popEventDataView, userPopEventColumns, popEventOptions);
		popEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		popEventGrid.registerPlugin(popEventCheckboxSelector);
		popEventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		var popEventGridPager = new Slick.Controls.Pager(popEventDataView, popEventGrid, $("#popEvent_pager"));
		var popEventGridColumnpicker = new Slick.Controls.ColumnPicker(allPopEventColumns, popEventGrid,
				popEventOptions);

		// move the filter panel defined in a hidden div into popEventGrid top panel
		$("#popEvent_inlineFilterPanel").appendTo(popEventGrid.getTopPanel()).show();

		/* popEventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < popEventDataView.getLength(); i++) {
				rows.push(i);
			}
			popEventGrid.setSelectedRows(rows);
			e.preventDefault();
		}); */
		
		popEventGrid.onSort.subscribe(function(e, args) {
			popEventGridSortdir = args.sortAsc ? 1 : -1;
			popEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				popEventDataView.fastSort(popEventGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				popEventDataView.sort(popEventGridComparer, args.sortAsc);
			}
		});
		// wire up model popEvents to drive the popEventGrid
		popEventDataView.onRowCountChanged.subscribe(function(e, args) {
			popEventGrid.updateRowCount();
			popEventGrid.render();
		});
		popEventDataView.onRowsChanged.subscribe(function(e, args) {
			popEventGrid.invalidateRows(args.rows);
			popEventGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#popEventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			popEventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			popEventDataView.setFilterArgs({
				popEventGridSearchString : popEventGridSearchString
			});
			popEventDataView.refresh();
		}
		
		// initialize the model after all the popEvents have been hooked up
		popEventDataView.beginUpdate();
		popEventDataView.setItems(popEventData);
		popEventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			popEventGridSearchString : popEventGridSearchString
		});
		popEventDataView.setFilter(popEventGridFilter);
		popEventDataView.endUpdate();
		popEventDataView.syncGridSelection(popEventGrid, true);
		popEventGrid.setSelectedRows([]);
		popEventGrid.resizeCanvas();
	}
	
	<!-- popEvent Grid Code Ends -->
	
	function getSelectedPopEventGridId() {
		var temprEventRowIndex = popEventGrid.getSelectedRows();
		
		var eventIdStr='';
		$.each(temprEventRowIndex, function (index, value) {
			eventIdStr += ','+popEventGrid.getDataItem(value).eventID;
		});
		
		if(eventIdStr != null && eventIdStr!='') {
			eventIdStr = eventIdStr.substring(1, eventIdStr.length);
			 return eventIdStr;
		}
	}
	
	function deletePopularEvents() {
		var eventIds = getSelectedPopEventGridId();
		var productId = $("#productId").val();
		if(eventIds != null && eventIds!='') {
			jConfirm("Are you sure you want to remove selected events from popular events?","Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePopularEvents",
						type : "post",
						data : "action=delete&productId="+productId+"&eventIds="+ eventIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							refreshpopEventGridValues(jsonData,true);
							clearAllSelections();
							jAlert("Selected event removed from popular events.");
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		}else{
			jAlert("Select an Events to remove from Popular Events.");
		}
	}
	
	
	function clearAllSelections(){
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
		
	}
	
	//call functions once page loaded
	window.onload = function() {
		refreshEventGridValues(null,false);
		refreshpopEventGridValues(null,false) ;
		enableMenu();
		
	};
</script>
	
</body>
</html>