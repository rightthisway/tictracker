<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
	border-bottom: 1px solid #eff2f7;
	padding-bottom: 5px;
	margin-bottom: 5px;
}

.form-group-top {
	padding-top: 5px;
	margin-top: 5px;
}

.td {
	text-align: center;
	vertical-align: middle;
}

.list-group-item {
	border: 0px;
	background-color: inherit;
}

.noresize {
	resize: none;
}

.fullWidth {
	width: 100%;
}
</style>

<script type="text/javascript">
	function callParentTypeChange() {
		$("#searchForm").submit();
	}

	$(document).ready(function() {
		$('.datepicker1').datepicker({
			format : "mm/dd/yyyy",
			autoclose : true,
			orientation : "top",
			todayHighlight : true
		});

		$('.selectCheck').attr('checked', false);
		$('#selectAll').click(function() {
			if ($('#selectAll').attr('checked')) {
				$('.selectCheck').attr('checked', true);
			} else {
				$('.selectCheck').attr('checked', false);
			}
		});
		$('#copyAllOdds').click(function() {
			if ($('#copyAllOdds').attr('checked')) {
				$('.selectCheck').attr('checked', true);
				copyTextField('odds');

				copyTextField('fractionalOdd');
			} else {
				//$('.selectCheck').attr('checked', true);
			}
		});
		$('#copyAllFractionalOdds').click(function() {
			if ($('#copyAllFractionalOdds').attr('checked')) {
				$('.selectCheck').attr('checked', true);
				copyTextField('fractionalOdd');
				if ($('#fractionalOdd_1').val() != '') {
					copyTextField('odds');
				}
			} else {
				//$('.selectCheck').attr('checked', true);
			}
		});

		$('#copyAllMarkup').click(function() {
			if ($('#copyAllMarkup').attr('checked')) {
				$('.selectCheck').attr('checked', true);
				copyTextField('markup');
			} else {
				//$('.selectCheck').attr('checked', true);
			}
		});
		$('#copyAllname').click(function() {
			if ($('#copyAllname').attr('checked')) {
				$('.selectCheck').attr('checked', true);
				copyTextField('name');
			} else {
				//$('.selectCheck').attr('checked', true);
			}
		});
		

		$('#copyAllCutoffDates').click(function() {
			if ($('#copyAllCutoffDates').attr('checked')) {
				$('.selectCheck').attr('checked', true);
				copyTextField('cutoffDate');
			} else {
				//$('.selectCheck').attr('checked', true);
			}
		});

	});

	function copyTextField(fieldName) {
		var isFirst = true;
		var firstFieldValue;
		$(".selectCheck:checked").each(function() {
			var id = this.id.replace('checkbox', fieldName);
			if (isFirst) {
				firstFieldValue = $("#" + id).val();
				isFirst = false;
			} else {
				$("#" + id).val(firstFieldValue);
			}
		});
	}

	function countFractionalOdd(rowNumber) {
		var fractionalValue = $('#fractionalOdd_' + rowNumber).val();
		if (fractionalValue != null && fractionalValue != '') {
			if (fractionalValue.indexOf("/") > 0) {
				var array = fractionalValue.split("/");
				var N = parseFloat(array[0]);
				var D = parseFloat(array[1]);
				if(N==0 && D==0){
					$('#fractionalOdd_' + rowNumber).val('');
					jAlert("Please enter valid fractional odd value.");
					return;
				}else{
					var odd = parseFloat((D / (D + N)) * 100);
					$('#odds_' + rowNumber).val(odd.toFixed(2));
					callSelectRow(rowNumber);
				}
				
			} else {
				$('#fractionalOdd_' + rowNumber).val('');
				jAlert('Please enter valid fraction value.');
				return;
			}
		}
	}
	function onOddChange(rowNumber) {
		var odd = $('#odds_' + rowNumber).val();
		var fractionalValue = $('#fractionalOdd_' + rowNumber).val();
		if(isNaN(odd) || odd < 0){
			$('#odds_' + rowNumber).val('');
			jAlert("Please enter valid odd value.");
		}
		if (fractionalValue != null && fractionalValue != '' && odd != null
				&& odd != '') {
			jAlert("Please remove Fractional Odds.");
			$('#odds_' + rowNumber).val('');
			return;
		}
		callSelectRow(rowNumber);
	}

	function callSelectRow(rowNumber) {
		$('#checkbox_' + rowNumber).attr('checked', true);
	}

	function callSaveBtnClick(action) {
		$("#infoMainDiv").hide();

		var flag = true;
		var regex = /^[0-9]{0,10}(\.[0-9]{0,3})?$/;

		var isMinimamOnerecord = false;
		
		if($('#grandChildType').val()=='' || $('#grandChildType').val()==null
				|| $('#grandChildType').val() ==undefined){
			jAlert("Please Select valid Grandchild to add teams.");
			return false;
		}
		
		$('.selectCheck:checkbox:checked').each(function() {

			var id, value;
			id = this.id.replace('checkbox', 'name');
			value = $.trim($("#" + id).val());
			if (value == '') {
				jAlert('Please enter valid team name.');
				$("#" + id).focus();
				flag = false;
				return false;
			}
			id = this.id.replace('checkbox', 'odds');
			value = $.trim($("#" + id).val());
			if (value == '' || !regex.test(value) || value < 0) {
				jAlert('Please Enter valid odds, should be greater than 0');
				$("#" + id).focus();
				flag = false;
				return false;
			}
			
			id = this.id.replace('checkbox', 'markup');
			value = $.trim($("#" + id).val());
			if (value == '' || !regex.test(value)) {
				jAlert('Please Enter valid markup.');
				$("#" + id).focus();
				flag = false;
				return false;
			}
			isMinimamOnerecord = true;
		});
		if (flag && !isMinimamOnerecord) {
			jAlert('Select minimum one team to save.');
			$("#" + id).focus();
			flag = false;
			return false;
		}
		if (flag) {
			jConfirm("Are you sure you want to save selected teams ?.",
					"Confirm", function(r) {
						if (r) {
							$("#action").val(action);
							$("#teamForm").submit();
						}
					});
		}
	}

	function callDeleteBtnOnClick(action) {
		$("#infoMainDiv").hide();

		var flag = true;
		var parentType = $('#parentType').val();
		var isMinimamOnerecord = false;
		$('.selectCheck:checkbox:checked').each(function() {

			var id, value;
			id = this.id.replace('checkbox', 'id');
			value = $.trim($("#" + id).val());
			if (value != '') {
				isMinimamOnerecord = true;
			}

		});
		if (!isMinimamOnerecord) {
			jAlert('Select minimum one Existing team to delete.');
			$("#" + id).focus();
			flag = false;
			return false;
		}
		if (flag) {
			jConfirm(
					"Are you sure you want to remove selected teams from selected Grand Child.?",
					"Confirm", function(r) {
						if (r) {
							$("#action").val(action);
							$("#teamForm").submit();
						}
					});
		}
	}

	function callParentTypeOnChange(action) {
		$('#gridDiv').hide();
		$('#leagueId').val('');
		$("#action").val(action);
		$("#teamForm").submit();
	}

	function loadChild(action) {
		$('#gridDiv').hide();
		$('#leagueId').val('');
		$("#action").val(action);
		$("#teamForm").submit();
	}

	function loadGrandChild(action) {
		$('#gridDiv').hide();
		$('#leagueId').val('');
		$('#grandChildType').val('');
		$("#action").val(action);
		$("#teamForm").submit();
	}


	function exportToExcel() {
		var childType = $('#childType').val();
		var grandChildType = $('#grandChildType').val();
		
		var appendData = "parentType=" + $('#parentType').val();
		if(childType != null || childType != '' || childType != undefined){
			appendData += "&childType="+$('#childType').val();
		}
		if(grandChildType != null || grandChildType != '' || grandChildType != undefined){
			appendData += "&grandChildType="+$('#grandChildType').val();
		}
		//var url = "${pageContext.request.contextPath}/CrownJewelEvents/CategoryTeamsExportToExcel?"+ appendData;
		var url = apiServerUrl+"CategoryTeamsExportToExcel?"+ appendData;
		$('#download-frame').attr('src', url);
	}
</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Fantasy Category</a></li>
			<li><i class="fa fa-laptop"></i>Category Teams</li>
		</ol>
	</div>
</div>

<div class="container">
	<div class="row">

		<div class="alert alert-success fade in" id="infoMainDiv" <c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
			<button data-dismiss="alert" class="close close-sm" type="button">
				<i class="icon-remove"></i>
			</button>
			<span id="infoMsgDiv">${info}</span>
		</div>
	</div>
	
	<form name="teamForm" id="teamForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelCategoryTeams">
		<input type="hidden" id="action" name="action" />
		<div class="row">
			<div class="form-group full-width">
				<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Parent Type</label>
				<div class="col-lg-4 col-md-4 col-xs-7">
				<input type="hidden" id="parentType" name="parentType" value="SPORTS" />
					<label>SPORTS</label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group full-width">
				<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Child Type</label>
				<div class="col-lg-4 col-md-4 col-xs-7">
					<select name="childType" id="childType" class="form-control input-sm m-bot15" onchange="loadGrandChild('search');">
						<option value="-1">--Select--</option>
						<c:forEach items="${childCategories}" var="childCat">
							<option value="${childCat.id}" <c:if test="${childId ne null and childCat.id eq childId}"> selected </c:if>>${childCat.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>

		<c:if test="${not empty grandChildCategories}">
			<div class="row">
				<div class="form-group full-width">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Grand Child Type</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<select name="grandChildType" id="grandChildType" class="form-control input-sm m-bot15" onchange="callParentTypeOnChange('search');">
							<option value="">--Select--</option>
							<c:forEach items="${grandChildCategories}" var="grandChildCat">
								<option value="${grandChildCat.id}" <c:if test="${grandChildId ne null and grandChildCat.id eq grandChildId}"> selected </c:if>>${grandChildCat.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
		</c:if>

		<%-- <div class="row">
			<div class="form-group full-width">
				<c:if test="${childId eq '0' or not empty grandChildId}">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Events</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<select name="leagueId" id="leagueId" class="form-control input-sm m-bot15" onchange="callLeagueOnChange('search');">
							<option value="">--- Select ---</option>
							<c:forEach var="league" items="${leaguesList}">
								<option value="${league.id}" <c:if test="${leagueId ne null and league.id eq leagueId}">selected</c:if>>${league.name}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-lg-2"></div>
				</c:if>
			</div>
		</div> --%>
		<div class="row">
		<span style="color:red">Anything updated on this page will updated to Fantasy Ticket Teams.</span>
	</div>
	<c:if test="${not empty grandChildId}">
		<div class="row clearfix">
			<div class="full-width column" id="gridDiv">
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
					<a href="javascript:exportToExcel()" style="float: right; margin-left: 10px;">Export to Excel</a>
				</div>
				<br /> <br />
				<div class="table-responsive mb-20">
				<table class="table table-bordered table-hover" id="tab_logic" align="center">
					<thead>
						<tr>
							<th class="col-lg-1 "><input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All</th>
							<th class="col-lg-3 ">Team Name<br /> <input type="checkbox" name="copyAllname" id="copyAllname"></th>
							<th class="col-lg-1 ">Markup %<br /> <input type="checkbox" name="copyAllMarkup" id="copyAllMarkup">
							</th>
							<th class="col-lg-1 ">Fractional Odds<br /> D/(D+N)*100 <input type="checkbox" name="copyAllFractionalOdds" id="copyAllFractionalOdds">
							</th>
							<th class="col-lg-1 ">Odds %<br /> <input type="checkbox" name="copyAllOdds" id="copyAllOdds">
							</th>
							<th class="col-lg-2 ">Cutoff Date <input type="checkbox" name="copyAllCutoffDates" id="copyAllCutoffDates">
							</th>
							<!-- <th class="col-lg-1 " >
							Package<br />
							<input type="checkbox" name="copyAllPackageApplicable" id="copyAllPackageApplicable" >
						</th>
						<th class="col-lg-1 ">
							Package Cost $<br />
							<input type="checkbox" name="copyAllPackageCost" id="copyAllPackageCost" >
						</th>
						<th class="col-lg-3 ">
							Package Notes<br />
							<input type="checkbox" name="copyAllPackageNotes" id="copyAllPackageNotes" >
						</th> -->
							<th class="col-lg-2 ">Last Updated By</th>
							<th class="col-lg-2 ">Last Updated</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${not empty teamsList}">
							<c:forEach var="cjTeam" varStatus="vStatus" items="${teamsList}">
								<tr id="teamRow_${vStatus.count}">
									<td><input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" /> 
										<input type="hidden" name="rowNumber" id="rowNumber_${vStatus.count}" value="${vStatus.count}" /> 
										<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjTeam.id}" /> 
									</td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<input class="form-control input-sm m-bot15 fullWidth" type="text" name="name_${vStatus.count}" id="name_${vStatus.count}" value="${cjTeam.name}"
													onchange="callSelectRow('${vStatus.count}');">
											</div>
										</div>
									</td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<input class="form-control input-sm m-bot15 fullWidth" type="text" name="markup_${vStatus.count}" id="markup_${vStatus.count}" value="${cjTeam.markup}"
													onchange="callSelectRow('${vStatus.count}');">
											</div>
										</div>
									</td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<input class="form-control input-sm m-bot15 fullWidth" type="text" name="fractionalOdd_${vStatus.count}" id="fractionalOdd_${vStatus.count}" value="${cjTeam.fractionalOdd}"
													onchange="countFractionalOdd('${vStatus.count}');">
											</div>
										</div>
									</td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<input class="form-control input-sm m-bot15 fullWidth" type="text" name="odds_${vStatus.count}" id="odds_${vStatus.count}" value="${cjTeam.odds}"
													onchange="onOddChange('${vStatus.count}');">
											</div>
										</div>
									</td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<input class="datepicker1 form-control input-sm m-bot15 fullWidth" type="text" name="cutoffDate_${vStatus.count}" id="cutoffDate_${vStatus.count}" value="${cjTeam.cutOffDateStr}"
													onchange="callSelectRow('${vStatus.count}');">
											</div>
										</div></td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<label>${cjTeam.lastUpdatedBy}</label>
											</div>
										</div></td>
									<td style="font-size: 13px;" align="center">
										<div class="form-group">
											<div>
												<label>${cjTeam.lastUpdatedStr}</label>
											</div>
										</div></td>
									<%-- <td style="vertical-align: middle;"><input type="checkbox" class="selectPackage" id="packageApplicable_${vStatus.count}" name="packageApplicable_${vStatus.count}"
										onclick="callSelectRow('${vStatus.count}');" <c:if test="${cjTeam.packageApplicable}">checked="checked" </c:if> /></td>
									<td style="vertical-align: middle;"><input class="form-control input-sm m-bot15 fullWidth" type="text" name="packageCost_${vStatus.count}" size="8" id="packageCost_${vStatus.count}"
										value="<fmt:formatNumber pattern="0.00" value="${cjTeam.packageCost}"/>" onchange="callSelectRow('${vStatus.count}');"></td>
									<td style="vertical-align: middle;"><input class="form-control input-sm m-bot15 fullWidth" type="text" name="packageNotes_${vStatus.count}" id="packageNotes_${vStatus.count}"
										value="${cjTeam.packageNotes}" onchange="callSelectRow('${vStatus.count}');"></td> --%>

								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</div>
				<a id="add_row" onclick="addNewTeamRow();" class="btn btn-default btn-sm pull-left">Add Row</a>
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
			</div>
		</div>
		</c:if>
	</form>
</div>

<script type="text/javascript">
	var rCount = $('#tab_logic tr').length;

	function addNewTeamRow() {
		$("#infoMainDiv").hide();
		rCount++;
		$('#tab_logic').append('<tr id="teamRow_' + (rCount) + '"></tr>');
		var rowHtml = '';

		rowHtml = rowHtml
				+ "<td>"
				+ "<input type='checkbox' class='selectCheck' id='checkbox_"+rCount+"' name='checkbox_"+rCount+"' />"
				+ "<input type='hidden' name='rowNumber_"+rCount+"' id='rowNumber_"+rCount+"' value="+rCount+"/>"
				+ "<input type='hidden' name='id_"+rCount+"' id='id_"+rCount+"' />"
				+ "<input type='hidden' name='leagueId_"+rCount+"' id='leagueId_"+rCount+"' />"
				+ "</td>";
		rowHtml = rowHtml
				+ "<td style='font-size: 13px;' align='center'> "
				+ "<div class='form-group' >"
				+ "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='name_"
				+ rCount + "' id='name_" + rCount + "'onchange='callSelectRow("
				+ rCount + ");'>" + "</div></div></td>";

		rowHtml = rowHtml
				+ "<td style='font-size: 13px;' align='center'> "
				+ "<div class='form-group' >"
				+ "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='markup_"
				+ rCount + "' id='markup_" + rCount
				+ "'onchange='callSelectRow(" + rCount + ");'>"
				+ "</div></div></td>";

		rowHtml = rowHtml
				+ "<td style='font-size: 13px;' align='center'> "
				+ "<div class='form-group' >"
				+ "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='fractionalOdd_"
				+ rCount + "' id='fractionalOdd_" + rCount
				+ "'onchange='countFractionalOdd(" + rCount + ");'>"
				+ "</div></div></td>";

		rowHtml = rowHtml
				+ "<td style='font-size: 13px;' align='center'> "
				+ "<div class='form-group' >"
				+ "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='odds_"
				+ rCount + "' id='odds_" + rCount
				+ "' onchange='callSelectRow(" + rCount + ");'>"
				+ "</div></div></td>";

		rowHtml = rowHtml
				+ "<td style='font-size: 13px;' align='center'> "
				+ "<div class='form-group' >"
				+ "<div ><input class='datepicker1 form-control input-sm m-bot15 fullWidth' type='text' name='cutoffDate_"
				+ rCount + "' id='cutoffDate_" + rCount
				+ "' onchange='callSelectRow(" + rCount + ");'>"
				+ "</div></div></td>";
		 rowHtml = rowHtml + "<td style='vertical-align: middle;'> <label></label></td>";
	     rowHtml = rowHtml + "<td style='vertical-align: middle;'> <label></label></td>";

		/*  rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
		 		"<input type='checkbox' class='selectPackage' id='packageApplicable_"+rCount+"' name='packageApplicable_"+rCount+"' onclick='callSelectRow("+rCount+");'>"+
		 	    "</td>";
		    rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
		           "<input class='form-control input-sm m-bot15 fullWidth' type='text' name='packageCost_"+rCount+"' id='packageCost_"+rCount+"'  onchange='callSelectRow("+rCount+");'>"+
		           "</td>";
		           
		 rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
				"<input class='form-control input-sm m-bot15 fullWidth' type='text' name='packageNotes_"+rCount+"' id='packageNotes_"+rCount+"' onchange='callSelectRow("+rCount+");'>"+
		           
		           "</td>"; */
		var row = $(rowHtml);
		$('#teamRow_' + rCount).html(row);
		$('.datepicker1').datepicker({
			format : "mm/dd/yyyy",
			autoclose : true,
			orientation : "top",
			todayHighlight : true
		});

	}
	var teamTable = document.getElementById("tab_logic");
	if (teamTable != null && teamTable != '' && $('#tab_logic tr').length <= 1) {
		addNewTeamRow();
	}
</script>