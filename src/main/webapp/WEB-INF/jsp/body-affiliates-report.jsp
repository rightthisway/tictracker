<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page import="com.rtw.tracker.datas.TrackerUser" %>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<style>
input {
	color: black !important;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
  background:#FFFFFF;
  color:#000000;
  border: 1px solid gray;
  padding: 2px;
  display: inline-block;
  min-width: 200px;
  
  -moz-box-shadow: 2px 2px 2px silver;
  -webkit-box-shadow: 2px 2px 2px silver;
  z-index: 99999;
}
#contextMenu li {
  padding: 4px 4px 4px 14px;
  list-style: none;
  cursor: pointer;
}
#contextMenu li:hover {
	color:#FFFFFF;
  background-color:#4d94ff;
}

#addCustomer {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

#contextMenuInvoice {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenuInvoice li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenuInvoice li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenuPO {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenuPO li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenuPO li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
$(document).ready(function(){
	
	$('#searchValue').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {
		 getAffiliateGridData(0);
	    return false;  
	  }
	});	
	
});


function exportToExcel(){	
    //var url = "${pageContext.request.contextPath}/ExportAffiliateReport?headerFilter="+affiliateGridSearchString;
    var userId = $('#userId').val();
    var url = apiServerUrl + "ExportAffiliateReport?userId="+userId+"&headerFilter="+affiliateGridSearchString;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	affiliateGridSearchString='';
	columnFilters = {};
	getAffiliateGridData(0);
}

function saveUserCustomerPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = affiliateGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('affiliategrid',colStr);
}
</script>
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="view order">View Order Details</li>  
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Affiliates
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Affiliates</a></li>
			<li><i class="fa fa-laptop"></i>Report</li>
		</ol>
	</div>
</div>

<div class="full-width">
	<div class="form-group col-xs-4 col-md-4">
		<label><b>Promotional Code &nbsp; &nbsp; &nbsp; &nbsp; : <span id="activeCpromoCode">${promoCodeHistory.promoCode}</span></b></label><br/>
		<label><b>Active Cash &nbsp; &nbsp; &nbsp; &nbsp; : <span id="activeCash">${affiliateCashReward.activeCash}</span></b></label><br/>
		<label><b>Total Credited Cash &nbsp; &nbsp; &nbsp; &nbsp; : <span id="totalCreditedCash">${affiliateCashReward.totalCreditedCash}</span></b></label><br/>
	</div>
	<div class="form-group col-xs-4 col-md-4">
		<label><b>Valid from Date &nbsp; &nbsp; &nbsp; &nbsp; : <span id="pendingCash">${promoCodeHistory.fromDateStr}</span></b></label><br/>
		<label><b>Pending Cash &nbsp; &nbsp; &nbsp; &nbsp; : <span id="pendingCash">${affiliateCashReward.pendingCash}</span></b></label><br/>
		<label><b>Total Paid Cash &nbsp; &nbsp; &nbsp; &nbsp; : <span id="pendingCash">${affiliateCashReward.totalPaidCash}</span></b></label><br/>
	</div>
	<div class="form-group col-xs-4 col-md-4">
		<label><b>Valid to Date &nbsp; &nbsp; &nbsp; &nbsp; : <span id="pendingCash">${promoCodeHistory.toDateStr}</span></b></label><br/>
		<label><b>Total Debited Cash &nbsp; &nbsp; &nbsp; &nbsp; : <span id="totalDebitedCash">${affiliateCashReward.totalDebitedCash}</span></b></label>
	</div>
	<%TrackerUser trackerUser = (TrackerUser)request.getSession().getAttribute("trackerUser"); %>
	<input type="hidden" id="userId" name="userId" value="<%=trackerUser.getId() %>" />
</div>

<!-- Grid view for affiliates -->
<div style="position: relative">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Customer Orders</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float: right; margin-right: 10px;'>Export to Excel</a> 
				<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="affiliateGrid" style="width: 100%; height: 200px; overflow: auto; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="affiliate_pager" style="width: 100%; height: 20px;"></div>
	</div>
</div>
<br />

</div>
<!-- <div id="inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
	Show records with customer including <input type="text" id="txtSearch2">
	
  <div style="width:100px;display:inline-block;" id="pcSlider2"></div>

</div>-->

<!-- popup View Order Details -->
	<%@include file="body-view-affiliate-order-details.jsp"%>
<!-- End popup View Order Details -->

<script>
var pagingInfo;
var orderId='';
var affiliateGrid;
var eventrowIndex;
var affiliateDataView;
var affiliateData=[];
var affiliateGridSearchString='';
var columnFilters = {};
var userAffiliateColumnsStr = '<%=session.getAttribute("affiliategrid")%>';
var userAffiliateColumns =[];
var loadAffiliateColumns = ["orderId", "firstName", "lastName", "eventName", "eventDate", "eventTime", 
			"ticketQty", "orderTotal", "cashCredit", "status", "promoCode"];
var allAffiliateColumns = [
				{id:"orderId", name:"Order Id", field: "orderId",width:80, sortable: true},         
               {id:"firstName", name:"First Name", field: "firstName",width:80, sortable: true},
               {id:"lastName", name:"Last Name", field: "lastName",width:80, sortable: true},
               {id:"eventName", name:"Event Name", field: "eventName",width:80, sortable: true},
               {id:"eventDate", name:"Event Date", field: "eventDate",width:80, sortable: true},
               {id:"eventTime", name:"Event Time", field: "eventTime",width:80, sortable: true},
               {id:"ticketQty", name:"Quantity", field: "ticketQty",width:80, sortable: true},
               {id:"orderTotal", name:"Order Total", field: "orderTotal",width:80, sortable: true},
               {id:"cashCredit", name:"Affiliate Fee", field: "cashCredit",width:80, sortable: true},
               {id:"status", name:"Status", field: "status",width:80, sortable: true},
               {id:"promoCode", name:"Affiliate Code", field:"promoCode", width:80, sortable: true}
              ];
  
	if(userAffiliateColumnsStr!='null' && userAffiliateColumnsStr!=''){
		var columnOrder = userAffiliateColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allAffiliateColumns.length;j++){
				if(columnWidth[0] == allAffiliateColumns[j].id){
					userAffiliateColumns[i] =  allAffiliateColumns[j];
					userAffiliateColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadAffiliateColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allAffiliateColumns.length;j++){
				if(columnWidth == allAffiliateColumns[j].id){
					userAffiliateColumns[i] = allAffiliateColumns[j];
					userAffiliateColumns[i].width=80;
					break;
				}
			}			
		}
		//userAffiliateColumns = allAffiliateColumns;
	}
  
var affiliateOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var affiliateSortCol = "orderId";
var affiliateSortdir = 1;
var percentCompleteThreshold = 0;

function affiliateComparer(a, b) {
	  var x = a[affiliateSortCol], y = b[affiliateSortCol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	  if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

function pagingControl(move,id){
	var pageNo = 0;
	if(id=='affiliate_pager'){
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getAffiliateGridData(pageNo);
	}
}

function getAffiliateGridData(pageNo) {
	eventrowIndex=-1;
	$.ajax({
		url : "${pageContext.request.contextPath}/AffiliateReport.json",
		type : "post",
		dataType: "json",
		data : "pageNo="+pageNo+"&headerFilter="+affiliateGridSearchString,
		success : function(res){
			var jsonData = res;
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);				
			}
			/* if(jsonData==null || jsonData=='') {
				jAlert("No Data Found.");
			} */
			pagingInfo = jsonData.pagingInfo;
			refreshAffiliateGridValues(jsonData.affiliates);
			clearAllSelections();
			$('#affiliate_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
function refreshAffiliateGridValues(jsonData) {
	affiliateData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (affiliateData[i] = {});			
			d["id"] = i;
			d["orderId"] = data.orderId;
			d["firstName"] = data.firstName;
			d["lastName"] = data.lastName;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
			d["ticketQty"] = data.ticketQty;
			d["orderTotal"] = data.orderTotal;
			d["cashCredit"] = data.cashCredit;
			d["status"] = data.status;
			d["promoCode"] = data.promoCode;
		}
	}
	affiliateDataView = new Slick.Data.DataView();
	affiliateGrid = new Slick.Grid("#affiliateGrid", affiliateDataView, userAffiliateColumns, affiliateOptions);
	affiliateGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = affiliateGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  affiliateGrid.setColumns(colTest);
	  }
	  affiliateGrid.invalidate();
	  affiliateGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(pagingInfo!=null){
		  var pager = new Slick.Controls.Pager(affiliateDataView, affiliateGrid, $("#affiliate_pager"),pagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(allAffiliateColumns,affiliateGrid, affiliateOptions);
	 
	  affiliateGrid.onSort.subscribe(function (e, args) {
	    affiliateSortdir = args.sortAsc ? 1 : -1;
	    affiliateSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      affiliateDataView.fastSort(affiliateSortCol, args.sortAsc);
	    } else {
	      affiliateDataView.sort(affiliateComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the affiliateGrid
	  affiliateDataView.onRowCountChanged.subscribe(function (e, args) {
	    affiliateGrid.updateRowCount();
	    affiliateGrid.render();
	  });
	  affiliateDataView.onRowsChanged.subscribe(function (e, args) {
	    affiliateGrid.invalidateRows(args.rows);
	    affiliateGrid.render();
	  });
	  
	  $(affiliateGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	affiliateGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  affiliateGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getAffiliateGridData(0);
				}
			  }
		 
		});
	  	affiliateGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'delCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		affiliateGrid.init();
		
		affiliateGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = affiliateGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				orderId = affiliateGrid.getDataItem(temprEventRowIndex).orderId;
				var productType = affiliateGrid.getDataItem(temprEventRowIndex).productType;
			}
		});
	  	
		affiliateGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = affiliateGrid.getCellFromEvent(e);
		      affiliateGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY;
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
	  affiliateDataView.beginUpdate();
	  affiliateDataView.setItems(affiliateData);
	  
	  affiliateDataView.endUpdate();
	  affiliateDataView.syncGridSelection(affiliateGrid, true);
	  $("#gridContainer").resizable();
	  $("div#divLoading").removeClass('show');
	  affiliateGrid.resizeCanvas();
}	

$("#contextMenu").click(function (e) {
	if (!$(e.target).is("li")) {
	  return;
	}
	var index = affiliateGrid.getSelectedRows([0])[0];
	if(index>=0){
		var orderId = affiliateGrid.getDataItem(index).orderId;
		if ($(e.target).attr("data") == 'view order') {
			if(orderId>0){
				getAffiliateOrderSummary('',orderId);
			/*var url = "${pageContext.request.contextPath}/AffiliateOrderDetails?orderId="+ orderId;
			popupCenter(url, "View Order", "1000","600");*/
			}else{
				jAlert("Invoice is not Created yet for selected Order.");
			}
		}
	}else{
		jAlert("Please select Order to view details.");
	}
});

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    }  
} 

//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshAffiliateGridValues(JSON.parse(JSON.stringify(${affiliates})));
		$('#affiliate_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");
	};
</script>