<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage PO</title>
 <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>

<script src="../resources/js/bootstrap-datepicker.js"></script>
<script src="../resources/js/app/invoiceRelatedPO.js"></script>
<script src="../resources/js/app/poPaymentHistory.js"></script>
<script src="../resources/js/app/poTicketGroupOnHand.js"></script>
<script src="../resources/js/app/poCreation.js"></script>
<script src="../resources/js/app/poUpdate.js"></script>
<script src="../resources/js/app/poFedexLabel.js"></script>

  <style>
    .cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
    .slick-headerrow-column {
     background: #87ceeb;
     text-overflow: clip;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
	}
	.slick-headerrow-column input {
	     margin: 0;
	     padding: 0;
	     width: 100%;
	     height: 100%;
	     -moz-box-sizing: border-box;
	     box-sizing: border-box;
	}
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }

#addUser {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}
#contextMenu {
    background:#FFFFFF;
 color:#000000;
    border: 1px solid gray;
    padding: 2px;
    display: inline-block;
    min-width: 200px;
 
    -moz-box-shadow: 2px 2px 2px silver;
    -webkit-box-shadow: 2px 2px 2px silver;
    z-index: 99999;
  }
  #contextMenu li {
    padding: 4px 4px 4px 14px;
    list-style: none;
    cursor: pointer;
  }
  #contextMenu li:hover {
	color:#FFFFFF;
    background-color:#4d94ff;
  }
  
  #contextMenuOther {
    background:#FFFFFF;
 	color:#000000;
    border: 1px solid gray;
    padding: 2px;
    display: inline-block;
    min-width: 200px;
 
    -moz-box-shadow: 2px 2px 2px silver;
    -webkit-box-shadow: 2px 2px 2px silver;
    z-index: 99999;
  }
  #contextMenuOther li {
    padding: 4px 4px 4px 14px;
    list-style: none;
    cursor: pointer;
  }
  #contextMenuOther li:hover {
	color:#FFFFFF;
    background-color:#4d94ff;
  }
</style>
</head>
<body>
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="pdf">Download Pdf</li>
  <li data="print po">View/Print PO</li>
  <li data="view payment history">View Payment History</li>
  <li data="view related invoices">View Related Invoices</li>
  <li data="view modify notes">View/Modify notes</li>
  <li data="view customer details">View Customer Details</li>
  <li data="modify ticket onHand">Modify Ticket ON-HAND/NOT-ON-HAND</li>
  <li data="edit po csr">Edit PO CSR</li>
  <li data="upload eticket">Upload E-Ticket</li>
</ul>
<ul id="contextMenuOther" style="display:none;position:absolute">
  <li data="pdf">Download Pdf</li>
  <li data="print po">View/Print PO</li>
  <li data="view payment history">View Payment History</li>
  <li data="view related invoices">View Related Invoices</li>
  <li data="view customer details">View Customer Details</li>
</ul>
<div class="row">
<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> ACCOUNTING
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Accounting</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage PO</li>
		</ol>

	</div>
	<div class="col-xs-12 filters-div">
                <!--  search form start -->
			<form role="form" id="poSearch" onsubmit="return false" action="${pageContext.request.contextPath}/Accounting/ManagePO">
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<input type="hidden" id="action" name="action" value=""/> <input type="hidden" id="poNo" name="poNo" value="${poNo}">
					<label for="name" class="control-label">From Date</label>
					<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
				</div>
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">To Date</label>
					<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
				</div>				
				<!-- 			
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">Po No</label>
					<input class="form-control searchcontrol" placeholder="PO Id" type="text" id="poNo" name="poNo" value="${poNo}">
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">CSR</label>
					<input class="form-control searchcontrol" placeholder="CSR" type="text" id="csr" name="csr" value="${csr}">
				</div>
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">Customer</label>
					<input class="form-control searchcontrol" placeholder="Customer" type="text" id="customer" name="customer" value="${customer}">
				</div>
				 -->
				<c:if test="${sessionScope.isAdmin == true}">
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Company Product</label> 
						<select id="productType" name="productType" class="form-control ">
							<option <c:if test="${selectedProduct=='REWARDTHEFAN'}"> Selected </c:if> value="REWARDTHEFAN">Reward The Fan</option>
							<option <c:if test="${selectedProduct=='RTW'}"> Selected </c:if> value="RTW">RTW</option>
							<option <c:if test="${selectedProduct=='RTW2'}"> Selected </c:if> value="RTW2">RTW2</option>
						</select>
					</div>
				</c:if>
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<input type="hidden" name="brokerId" id="brokerId" value="${brokerId}">
					<button type="button" id="searchPOBtn" class="btn btn-primary" onclick="searchPo();">search</button>
				 </div>
				 
               </form>
                <!--  search form end -->                
           </div>
    
</div>

<div style="position: relative">	
	<div class="full-width">
		<span id="editActionButton" class="full-width mb-20 full-width-btn">
			<button type="submit" class="btn btn-primary" onclick="createPO()">Create PO</button>
			<a data-toggle="modal" class="btn btn-primary" id="editCatTicket" onclick="editPO();">Edit PO</a>
			<button type="button" class="btn btn-primary" onclick="deleteFedexLabel()">Remove Fedex Label</button>
			<button type="button" class="btn btn-primary" onclick="createFedexLabel()">Create Fedex Label</button> 
		</span>		
	</div>
	<br />
	<div class="table-responsive grid-table">
		<!-- Add user for light box-->
		<!-- <a style="margin-top: -0.8%; margin-left: 0%;" class="btn btn-primary"
			id="addUser" href="javascript:void(0)"
			onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"
			title="Add User">Add User</a>
			<br/><br/> -->
		<div class="grid-header full-width">
			<label>Manage Purchase Order</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>		
			</div>
		</div>
		<div id="poGrid"style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="pager" style="width: 100%; height: 20px;"></div>
		<br />
	</div>
</div>

<!-- =================================== Pop Windows Starts here ============================= -->

<!-- popup View Payment History -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-payment-history">View Payment History</button>  -->

	<div id="view-payment-history" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Payment History - PO No : <span id="poId_Hdr_PayHistory" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Payment History</li>
						</ol>
					</div>
				</div>
				<br />

				<div id="poPayHistory_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="poPayHistory_successMsg"></span></strong>
				</div>					
				<div id="poPayHistory_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="poPayHistory_errorMsg"></span></strong>
				</div>
				<br />
				
				<div class="full-width" style="position: relative" id="poPaymentHistory">
					<div class="table-responsive grid-table">						
						<div class="grid-header full-width">
							<label>Payment History</label> <!--<span id="openOrder_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel"></span>-->
						</div>
						<div id="poPaymentGrid" style="display:none;">
							<div id="poPaymentHistory_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="poPaymentHistory_pager" style="width: 100%; height: 10px;"></div>
						</div>
						<div id="rtwPOPaymentGrid" style="display:none;">	
							<div id="rtwPOPaymentHistory_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="rtwPOPaymentHistory_pager" style="width: 100%; height: 10px;"></div>			
						</div>						
					</div>
				</div>
				<!--
				<div id="inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
									Show records with payment history including <input type="text" id="txtSearch2">
				</div>
				-->
			</div>
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View Payment History -->

<!-- popup View Related Invoices -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-related-invoices">View Related Invoices</button> -->

	<div id="view-related-invoices" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Related Invoices - PO No : <span id="poId_Hdr_RelatedInvoice" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Purchase Orders</li>
						</ol>
					</div>
				</div>
				<br />
				
				<div id="relatedInv_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="relatedInv_successMsg"></span></strong>
				</div>					
				<div id="relatedInv_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="relatedInv_errorMsg"></span></strong>
				</div>
				<br />

				<div class="full-width" style="position: relative" id="purchaseOrder">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Purchase Order</label> <span id="openOrder_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel"></span>
						</div>
						<div id="po_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="po_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				
				<div class="full-width mt-20" style="position: relative" id="invoice">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Invoices</label> <span id="openOrder_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel"></span>
						</div>
						<div id="invoice_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="invoice_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	  </div>
	</div>
<!-- End popup View Related Invoices -->


<!-- popup View/Modify notes -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-modify-notes">View/Modify notes</button> -->

	<div id="view-modify-notes" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View/Modify notes - PO No : <span id="poId_Hdr_ModNotes" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Purchase Order</a>
							</li>
							<li><i class="fa fa-laptop"></i>Note</li>
						</ol>
					</div>
				</div>
				
				<div id="modNotes_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="modNotes_successMsg"></span></strong>
				</div>					
				<div id="modNotes_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="modNotes_errorMsg"></span></strong>
				</div>
				<br />

				<div id="row">
					<div class="col-xs-12">
						<form class="form-validate form-horizontal" id="editPONote" method="post" action="${pageContext.request.contextPath}/Accounting/EditPONote">
							<div class="form-group">
								<input type="hidden" id="modNotes_action" name="action" value="" />
								 <input type="hidden" id="modNotes_poId" name="poId" value="${poId}" />
								<div class="col-xs-12" align="center">
									<label>Internal Note</label>
									<textarea class="form-control" id="modNotes_internalNote" name="internalNote"  cols="50" rows="5">${internalNote}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12" align="center">
									<label>External Note</label>
									<textarea class="form-control" id="modNotes_externalNote" name="externalNote" cols="50" rows="5">${externalNote}</textarea>
								</div>
							</div>
						</form>
					</div>
					
				</div>

			</div>
			<div class="modal-footer full-width">
				<button type="button" onclick="updatePONote();" style="width:100px" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View/Modify notes -->


<!-- popup Create PO -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#create-po">Create PO</button> -->
	<div id="create-po" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Create PO</h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Purchase Order
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Purchase Order</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Create PO</li>
						</ol>
					</div>
				</div>

				<div class="row">
				<div class="col-xs-12">
				<h3 style="font-size: 13px;font-family: arial;" class="page-header">
							Search Customer
						</h3>		
						<div id="cPO_successDiv" class="alert alert-success fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="cPO_successMsg"></span></strong>
						</div>					
						<div id="cPO_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="cPO_errorMsg"></span></strong>
						</div>
						<br />
						<!--  search form start -->
						<form:form class="form-inline" role="form" onsubmit="return false" id="customerSearch" method="post" >
							<!-- 
							<div class="form-group col-xs-6 col-sm-4">
							<label for="name" class="control-label">Search By</label> 
								<select id="searchType" name="searchBy" class="form-control input-sm m-bot15">
								 <option value="customerName" <c:if test="${searchType eq 'customerName'}">selected</c:if> >Customer Name</option>
								  <option value="email">Email</option>
								  <option value="customerType" <c:if test="${searchType eq 'customerType'}">selected</c:if> >CustomerType</option>
								</select>
							</div>
							
							<div class="form-group col-xs-6 col-sm-4">
								<label for="name" class="control-label">Search Value</label>
								<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
							</div>
							
							<div class="form-group col-xs-6 col-sm-4">
								<label for="name" class="control-label">Product Type</label> <select id="productType" name="productType" class="form-control ">
									<option <c:if test="${productType=='ALL'}"> Selected </c:if> value="ALL">All</option>
									<option <c:if test="${productType=='REWARDTHEFAN'}"> Selected </c:if> value="REWARDTHEFAN">Reward The Fan</option>
									<option <c:if test="${productType=='RTW'}"> Selected </c:if> value="RTW">RTW</option>
									<option <c:if test="${productType=='RTW2'}"> Selected </c:if> value="RTW2">RTW2</option>
								</select>
							</div>
							
							<div class="form-group col-xs-6 col-sm-4">
								<label>Client/Broker</label>
								<select name="customerTypeSearchValues"
									class="form-control input-sm m-bot15" id="customerTypeSearch">
									<option value="both" <c:if test="${customerType=='both'}"> Selected </c:if>>Both</option>
									<option value="client" <c:if test="${customerType=='client'}"> Selected </c:if>>Client</option>
									<option value="broker" <c:if test="${customerType=='broker'}"> Selected </c:if>>Broker</option>
								</select>
							</div>
							
							<div class="form-group col-xs-6 col-sm-4">
								<button type="button" id="searchCustomerBtn" class="btn btn-primary" style="margin-top:18px;"  onclick="getCustomerGridData(0);">Search</button>
							 </div>
							 -->
							<div class="form-group full-width">
								<button type="button" class="btn btn-primary" style=""  onclick="addNewCustomer();">Add New Customer</button>
							</div>
							 
							</form:form>
						  </div>
				</div>
				
				<div class="full-width mt-20" id="customerGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label style="font-size: 10px;">Customers Details</label>
							<div class="pull-right">
							<a href="javascript:cPOCustomerResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="createPOCustomer_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="createPOCustomer_pager" style="width: 100%; height: 20px;"></div>
					</div>
				</div>

				<div class="full-width mt-20">
					<div class="full-width">
						<section class="panel full-width"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
						Customer Details </header>
						<div class="panel-body full-width">
							<div class="form-group full-width">
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer Name:</label>
									<span style="font-size: 12px;" id="cPO_customerNameDiv"></span>
									
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Email:</label>
									<span id="cPO_emailDiv"></span>
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Phone:</label>
									<span id="cPO_phoneDiv"></span>
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Address Line:</label>
									<span id="cPO_addressLineDiv"></span>
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">City:</label>
									<span id="cPO_cityDiv"></span>
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">State:</label>
									<span id="cPO_stateDiv"></span>
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Country:</label>
									<span id="cPO_countryDiv"></span>
								</div>
								<div class="form-group col-md-4 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">ZipCode:</label>
									<span id="cPO_zipCodeDiv"></span>
								</div>
							</div>
							
						</div>
						</section>
					</div>
				</div>
				

				<div class="full-width mt-10" id="cPO_eventInfo" style="display:none">
				<div class="full-width tab-fields">
					<header style="font-size: 13px;font-family: arial;" class="panel-heading">Select an event to add ticket </header>
					<div class="row">
						<form:form class="form-inline" onsubmit="return false" role="form" id="eventSearch" method="post" action="${pageContext.request.contextPath}/Events">
							<input type="hidden" id="eventID" name="eventID">
							<%-- <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
								<label for="name" class="control-label">From Date </label> 
								<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
							</div>
							<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-4">
								<label for="name" class="control-label">To Date&nbsp;&nbsp;&nbsp;&nbsp; </label> 
								<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
							</div> --%>
							<div class="form-group col-md-3 col-sm-4 col-xs-6">
								<select id="searchType" name="searchType" class="form-control" style="margin-top:18px;">
									<option value="event_name">Event</option>
									<option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
									<!-- <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >GrandChildCategory</option>
									<option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
									<option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
									<option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option> -->
								</select>
							</div>
							<div class="form-group col-md-3 col-sm-4 col-xs-6">
								<input class="form-control searchcontrol" placeholder="Search" type="text" style="margin-top:18px;" id="cPO_eventSearchValue" name="eventSearchValue" value="${eventSearchValue}">								
							</div>            
							<div class="form-group col-md-3 col-sm-4 col-xs-6">
								<button type="button" id="searchEventBtn" class="btn btn-primary" style="margin-top:18px;" onclick="searchEventData();">Search</button>
							</div>
							<div class="form-group col-md-3 col-sm-4 col-xs-6"> 
								&nbsp;
							</div>			
						</form:form>
						<!--  search form end -->                
					</div>
				</div>
				
				<!-- event Grid Code Starts -->
				<div class="full-width mt-20 mb-20" style="position: relative; display: none;" id="eventGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Events Details</label>
							<div class="pull-right">
							<a href="javascript:cPOEventResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="createPOEvent_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="createPOEvent_pager" style="width: 100%; height: 20px;"></div>
					</div>
				</div>

				<!-- Show the selected event info -->
				
				<div class="full-width form-group" id="cPO_addTicketDiv" style="display: none">
					<button class="btn btn-primary" type="submit" id="cPO_addTicket" onclick="addTicketInfo();">Add Ticket</button>
				</div>
				
				</div>
				<div id="cPO_poInfo" class="full-width mt-20">
				<div class="full-width">		
						 <section class="panel">
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">Ticket Details</header>
							<div class="panel-body">
				<div class="form-group full-width">
					<form name="purchaseOrderForm" id="cPO_purchaseOrderForm" action="${pageContext.request.contextPath}/PurchaseOrder/SavePO" method="post">
					<input type="hidden" name="action" value="action" id="cPO_action"/>
					<input type="hidden" name="poType" id="cPO_poType">
					<input type="hidden" name="customerId" value="" id="cPO_customerId"/>
						<div class="full-width">
							<div class="table-responsive">
							<table id="cPO_myTable"
								class="table table-bordered table-striped table-advance table-hover">
								<thead>
									<tr>
										<th></th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Event Name</th>
										<!-- <th><i class=""></i> Event Date</th>
									<th><i class=""></i> Venue</th> -->
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Section</th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Row</th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Seat Low</th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Seat High</th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Qty</th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Price</th>
										<th style="font-size: 13px;font-family: arial;"><i class=""></i> Action</th>
									</tr> 
									<!-- dynamic rows -->
									<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
								</thead>
								<tbody id="cPO_myTableBody">
								</tbody>
							</table>
							</div>
						</div>
				</div>

				<div class="row tab-fields">
					
						 <div class="form-group col-md-3 col-sm-4 col-xs-6">
							<label >Shipping Method</label>
							<select id="cPO_shippingMethod" name="shippingMethod" class="form-control" onchange="cPOShowHideFedexInfo();">
							  <!--<option value="-1">--select--</option>
							  <c:forEach items="${shippingMethods}" var="shippingMethod">
								<option value="${shippingMethod.id}"> ${shippingMethod.name}
								</option>
							  </c:forEach>-->
							</select>
						 </div>
						 <div class="form-group col-md-3 col-sm-4 col-xs-6" id="cPO_trackingInfo">
							<label >Tracking No </label>
							<input type="text" name="trackingNo" id="cPO_trackingNo" class="form-control"/>
						 </div>
						 <div class="form-group col-md-3 col-sm-4 col-xs-6"  style="">
							Total Price : $<input type="text" id="cPO_totalPrice" name="totalPrice" readOnly value="" class="form-control" style="border: none; background:none;">
						 </div>
						 <div class="form-group col-md-3 col-sm-4 col-xs-6"  style="">
							Total Qty :
							<input type="text" id="cPO_totalQty" name="totalQty" readOnly value="" class="form-control" style="border: none; background:none;">
						 </div>
						 
					
				</div>
				<div class="row">
					<div class="full-width tab-fields">
						<div class="form-group col-md-3 col-sm-4 col-xs-6">
							<label>External PO No :</label>
							<input type="text" name="externalPONo" value=""  class="form-control"/>
						 </div>
						 <div class="form-group col-md-3 col-sm-4 col-xs-6">
							<label class="full-width">&nbsp</label>
							<label>Consignment PO :</label>
							 <input type="checkbox" id="cPO_poTypeCheck" onclick="cPOTypeCheck();"/>
						 </div>
						
						 <div class="form-group col-md-3 col-sm-4 col-xs-6" id="cPO_consignmentPoDiv">
							<label>Consignment PO No :</label>
							<input type="text" name="consignmentPo" id="cPO_consignmentPO" value=""  class="form-control"/>
						 </div>
						 <div class="form-group col-md-3 col-sm-4 col-xs-6">
							<label>Transaction Office :</label>
							<select name="transactionOffice" class="form-control">
								<option value="Main">Main</option>
								<option value="Spec">Spec</option>
							</select> 
						 </div>
						</div>
					</div>
					</div>
				</section>
				</div>
				<div class="row">
					<div class="col-xs-12">		
						 <section class="panel full-width">
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">Payment Details</header>
							<div class="panel-body">
								<div class="row tab-fields">
								
								<div class="form-group full-width">
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										  <input type="radio" name="paymentType" value="card" onclick="cPOPaymentType(this);">Credit Card
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										  <input type="radio" name="paymentType" value="account" onclick="cPOPaymentType(this);">In Account
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										  <input type="radio" name="paymentType" value="cheque" onclick="cPOPaymentType(this);">Cheque
									</div>
									<hr/>
								</div>
								<div class="form-group full-width" id="cPO_cardInfo">
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Card Holder Name</label>
										<input type="text" name="name" id="cPO_name" class="form-control"/> 
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Card Type</label>
										<select name="cardType" class="form-control">
											<option value="American Express">American Express</option>
											<option value="Visa">Visa</option>
											<option value="MasterCard">MasterCard</option>
											<option value="JCB">JCB</option>
											<option value="Discover">Discover</option>
											<option value="Diners Club">Diners Club</option>
										</select> 
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Card Last 4 Digit</label>
										<input type="text" name="cardNo" id="cardNo" class="form-control"/> 
									</div>
									<hr/>
								</div>
								<div class="form-group full-width" id="cPO_accountInfo">
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Account Name</label>
										<input type="text" name="accountName" id="cPO_accountName" class="form-control"/> 
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Routing no. Last 4 Digit</label>
										<input type="text" name="routingNo" id="cPO_routingNo" class="form-control"/>
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Account no. Last 4 Digit</label>
										<input type="text" name="accountNo" id="cPO_accountNo" class="form-control"/> 
									</div>
									<hr/>
								</div>
								<div class="form-group full-width" id="cPO_chequeInfo">
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Name</label>
										<input type="text" name="chequeName" id="cPO_chequeName" class="form-control"/> 
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Routing no. Last 4 Digit</label>
										<input type="text" name="routingChequeNo" id="cPO_routingChequeNo" class="form-control"/>
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Account no. Last 4 Digit</label>
										<input type="text" name="accountChequeNo" id="cPO_accountChequeNo" class="form-control"/>
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Cheque No.</label>
										<input type="text" name="checkNo" id="cPO_checkNo" class="form-control"/> 
									</div>
									<hr/>
								</div>
								
								</div>
								<div class="row tab-fields">
								
								<div class="form-group full-width">
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>Payment Date</label>
										<input type="text" name="paymentDate" id="cPO_paymentDate" class="form-control"/> 
									</div>
									<div class="form-group col-md-3 col-sm-4 col-xs-6">
										<label>payment Status</label>
										<select name="paymentStatus" class="form-control">
											<option value="Paid">Paid</option>
											<option value="Pending">Pending</option>
										</select> 
									</div>
									<div class="form-group col-md-6 col-sm-12 col-xs-12">
										<label>Payment Note</label>
										<textarea name="paymentNote" id="cPO_paymentNote" rows="4" cols="50" class="form-control"> </textarea>
									</div>
									
								</div>
								
								</div>
							</div>
						</section>
					</div>

				</div>

				
				</form>
				</div>
			</div>
			<div class="modal-footer full-width" id="cPO_btn_div">
				<button class="btn btn-primary" type="button" onclick="savePurchaseOrder();">Create PO</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Create PO -->

<!-- popup View Customer Details -->
	<%@include file="body-view-customer-details.jsp"%>
<!-- End popup View Customer Details -->

<!-- popup Modify Ticket onHand -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modify-ticket-onhand">Modify Ticket onHand</button> -->

	<div id="modify-ticket-onhand" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modify Ticket onHand - PO No : <span id="poId_Hdr_ModTicket" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Purchase Order</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Edit Ticket ON-HAND/NOT ON-HAND</li>
						</ol>
					</div>
				</div>

				<br />
				
				<div id="poTicketGroup_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="poTicketGroup_successMsg"></span></strong>
				</div>					
				<div id="poTicketGroup_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="poTicketGroup_errorMsg"></span></strong>
				</div>
				<br />
				
				<div class="full-width" style="position: relative" id="poTicketGroup">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Ticket Groups</label> <!--<span id="poTicketGroup_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel"></span>-->
						</div>
						<div id="poTicketGroup_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="poTicketGroup_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="poTicketGroup_poId" name="poId" value="${poId}" />
				<button type="button" class="btn btn-primary" onclick="updatePOTicketOnHand('Yes')">Mark as ON-HAND</button>
				<button type="button" class="btn btn-primary" onclick="updatePOTicketOnHand('No')">Mark as NOT ON-HAND</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Modify Ticket onHand -->

<!-- popup Edit PO CSR -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-po-csr">Edit PO CSR</button> -->

	<div id="edit-po-csr" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit PO CSR - PO No : <span id="poId_Hdr_EdCSR" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Purchase Order</a>
							</li>
							<li><i class="fa fa-laptop"></i>PO CSR</li>
						</ol>
					</div>
				</div>
				
				<div id="poCsr_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="poCsr_successMsg"></span></strong>
				</div>					
				<div id="poCsr_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="poCsr_errorMsg"></span></strong>
				</div>
				<br />

				<div id="row">
					<div class="col-xs-12 full-width mb-20">
						<form class="form-validate form-horizontal" id="poCsr_changeCsr" method="post" action="${pageContext.request.contextPath}/Accounting/EditPOCsr">
							<div class="form-group">
								<input type="hidden" id="poCsr_action" name="action" value="" />
								<input type="hidden" id="poCsr_poId" name="poId" value="${poId}" />
								<div class="col-xs-12" align="center">
									<label>CSR</label> 
									<select name="csrUser" id="poCsr_csrUser" style="font-size: 13px; max-width:300px;" class='form-control input-md'>
										<%--<option value="auto">Auto</option>
										<c:forEach items="${users}" var="user">
											<option value="${user.userName}" <c:if test="${poCsr ==user.userName}">selected</c:if>>${user.userName}</option>
										</c:forEach>--%>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="modal-footer full-width">
				<button type="button" onclick="changePOCsr();" style="width:100px" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Edit PO CSR -->


<!-- popup Upload Eticket -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#upload-eticket">Upload Eticket</button> -->

	<div id="upload-eticket" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Upload Eticket - PO No : <span id="poId_Hdr_UploadETicket" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
				<div class="row">
					<div class="col-xs-12">
						<h3 style="font-size: 13px;font-family: arial;" class="page-header">
							<i class="fa fa-laptop"></i> ACCOUNTING
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#"
								onclick="window.close(); window.opener.location.reload(true);">Accounting</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Edit Purchase Order</li>
						</ol>

					</div>
				</div>

				<div id="poInfo">
				<div class="row">
					<div class="col-xs-12">
						<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading mb-10">
						Customer Info </header>
						<div class="full-width">
							
							<div id="upEticket_successDiv" class="alert alert-success fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="upEticket_successMsg"></span></strong>
							</div>					
							<div id="upEticket_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="upEticket_errorMsg"></span></strong>
							</div>
							<br />
							
								<div class="form-group">
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">PO ID:&nbsp;</label>
									<span style="font-size: 15px;" id="upEticket_poId_span">${purchaseOrder.id}</span>
								</div>
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">PO Total:&nbsp;</label>
									<span style="font-size: 15px;" id="upEticket_poTotal">${purchaseOrder.poTotal}</span>
								</div>
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer Name:&nbsp;</label>
									<span style="font-size: 15px;" id="upEticket_customerName">${customerInfo.customerName}</span>
								</div>
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Created Date:&nbsp; </label>
									<span style="font-size: 15px;" id="upEticket_createTime">${purchaseOrder.createTimeStr}</span>
								</div>				
							</div>
							<div class="form-group">
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">CSR:&nbsp;</label>
									<span style="font-size: 15px;" id="upEticket_csr">${purchaseOrder.csr}</span>
								</div>
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Quantity:&nbsp;</label>
									<span style="font-size: 15px;" id="upEticket_ticketCount">${purchaseOrder.ticketCount}</span>
								</div>
								<div class="form-group col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Shipping Method:&nbsp;</label>
									<span style="font-size: 15px;" id="upEticket_shippingMethod">
									<%--<c:forEach items="${shippingMethods}" var="shippingMethod">
										 <c:if test="${shippingMethod.id==purchaseOrder.shippingMethodId}">  ${shippingMethod.name} </c:if>					
									</c:forEach>--%><!---->
									</span>
								</div>
							</div>
						</div>
					</section>
				</div>
				</div>

				<div class="row">
					<div class="full-width col-xs-12">
						<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
						Ticket Info </header>
						<div class="full-width">				
							<div class="form-group full-width">
								<form id="upEticket_purchaseOrderForm" name="purchaseOrderForm"
								method="post" enctype="multipart/form-data"
								action="${pageContext.request.contextPath}/Accounting/UploadEticket">
								<input type="hidden" class="form-control" id="upEticket_action" value="action" name="action" /> 
								<input type="hidden" class="form-control" id="upEticket_poId" value="${purchaseOrder.id}" name="poId" />
								<input type="hidden" class="form-control" id="upEticket_customerId" value="${purchaseOrder.customerId}" name="customerId" />
										<div class="table-responsive">
											<table id="upEticket_TG" class="table table-striped table-advance table-hover">
												<tbody>
													<tr>
														<th>
															<!-- S.No -->
														</th>
														 <th style="font-size: 13px;font-family: arial;">Event Name</th>
														<th style="font-size: 13px;font-family: arial;">Section</th>
														<th style="font-size: 13px;font-family: arial;">Row</th>
														<th style="font-size: 13px;font-family: arial;">Seat Low</th>
														<th style="font-size: 13px;font-family: arial;">Seat High</th>
														<th style="font-size: 13px;font-family: arial;">Qty</th>
														<th style="font-size: 13px;font-family: arial;">Price</th>
														<th style="font-size: 13px;font-family: arial;">Instant Download</th>
														<th style="font-size: 13px;font-family: arial;">Upload</th>
														<!--<th style="font-size: 13px;font-family: arial;">Action</th>-->
													</tr>
													<!-- dynamic rows -->
													<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
													<c:forEach var="ticketGroup" items="${ticketGroup}" varStatus="theCount">
														<input type="hidden" id="rowId_${theCount.count}" name="rowId_${theCount.count}" value="${ticketGroup.eventId}">
														
														<%-- <input type="text" name="eventId_${ticketGroup.eventId}" value=""/> --%>
														<tr>
															<td>
																<%-- ${theCount.count} --%>
															</td>
															<td><label name="event_${ticketGroup.id}">
																${ticketGroup.events.eventName} ${ticketGroup.events.eventDateStr} ${ticketGroup.events.eventTimeStr}</label>
															</td>
															<td><input type="text" disabled="true" class="section" name="section_${theCount.count}_${ticketGroup.id}"
																value="${ticketGroup.section}" />
															</td>
															<td><input type="text" disabled="true" class="rowValue" name="row_${theCount.count}_${ticketGroup.id}"
																size="10" value="${ticketGroup.row}" />
															</td>
															<td><input type="text" disabled="true" class="seatLow" name="seatLow_${theCount.count}_${ticketGroup.id}"
																value="${ticketGroup.seatLow}" />
															</td>
															<td><input type="text" disabled="true" class="seatHigh" name="seatHigh_${theCount.count}_${ticketGroup.id}"
																value="${ticketGroup.seatHigh}" />
															</td>
															<td><input type="text" disabled="true" name="qty_${theCount.count}_${ticketGroup.id}"
																id="qty_${theCount.count}_${ticketGroup.id}" class="qty"
																onkeyup="sumQty(this);" value="${ticketGroup.quantity}" />
															</td>
															<td><input type="text" disabled="true" name="price_${theCount.count}_${ticketGroup.id}"
																id="price_${theCount.count}_${ticketGroup.id}" class="price"
																onkeyup="sumPrice(this);" value="${ticketGroup.price}" />
															</td>
															<td><input type="checkbox" name="instantdownload_${theCount.count}_${ticketGroup.id}"
																id="instantdownload_${theCount.count}_${ticketGroup.id}" class="instantdownload"
																value="True" />True
															</td>
															<td>
															
															<c:choose>
																<c:when test="${null ne ticketGroup.ticketGroupTicketAttachment }">
																	<c:set value="${ticketGroup.ticketGroupTicketAttachment}" var="ticketAttachment" ></c:set>
																
																	<input type="file" name="file_${theCount.count}_${ticketGroup.id}"
																					id="file_${theCount.count}_${ticketGroup.id}" />
																	<p class="textWrap">
																		<a href="javascript:downloadTicketFile('${ticketAttachment.ticketGroupId}','${ticketAttachment.type}','${ticketAttachment.position}')">${ticketAttachment.filePath}</a>
																	</p>
																</c:when>
																<c:otherwise>
																	<input type="file" name="file_${theCount.count}_${ticketGroup.id}"
																			id="file_${theCount.count}_${ticketGroup.id}" />
																</c:otherwise>
															
															
															</c:choose>											
															</td>
															
														</tr>
														<input type="hidden" name="ticketGroup_${theCount.count}_${ticketGroup.id}" />
													</c:forEach>
												</tbody>
											</table>
										</div>
								</form>
							</div>
						</div>	
					</section>
					</div>
				</div>

				</div>
				
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="uploadEticketPOValidation();">Update PO</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Upload Eticket -->

<!-- popup Add New Customer -->
	<%@include file="body-add-new-customer.jsp" %>
<!-- End popup Add New Customer -->

<!-- popup Edit PO -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-po">Edit PO</button> -->
<div id="edit-po" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit PO - PO No : <span id="poId_Hdr_EdPO" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 style="font-size: 13px;font-family: arial;" class="page-header">
								<i class="fa fa-laptop"></i> ACCOUNTING
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#"
									onclick="window.close(); window.opener.location.reload(true);">Accounting</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Edit Purchase Order</li>
							</ol>

						</div>
					</div>

					<div id="poInfo" class="full-width">
					<div class="row">
						<div class="col-xs-12">
							<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
							Customer Info </header>
							<div class="panel-body">
								<div id="ePO_successDiv" class="alert alert-success fade in" style="display:none;">
									<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
									<span id="ePO_successMsg"></span></strong>
								</div>					
								<div id="ePO_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
									<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
									<span id="ePO_errorMsg"></span></strong>
								</div>
								<br />
								
									<div class="full-width">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Customer Name:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_customerName">${customerInfo.customerName}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Email:&nbsp; </label>
										<span style="font-size: 15px;" id="ePO_userName">${customerInfo.userName}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Phone:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_phone">${customerInfo.phone}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Address Line1:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_addressLine1">${customerAddress.addressLine1}</span>
									</div>
								
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Address Line2:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_addressLine2">${customerAddress.addressLine2}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">City:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_city">${customerAddress.city}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">State:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_state">${customerAddress.state.name}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Country:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_country">${customerAddress.country.name}</span>
									</div>
								
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">ZipCode:&nbsp;</label>
										<span style="font-size: 15px;" id="ePO_zipCode">${customerAddress.zipCode}</span>
									</div>
								</div>
							</div>
						</section>
					</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<section class="panel full-width"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
							Ticket Info </header>
							<div class="panel-body full-width">				
								<div class="form-group">
									<form id="ePO_purchaseOrderForm" name="purchaseOrderForm"
									method="post"
									action="${pageContext.request.contextPath}/Accounting/EditPurchaseOrder">
									<input class="form-control" id="ePO_action" value="action" name="action" type="hidden" /> 
									<input class="form-control" id="ePO_purchaseOrderId" value="${purchaseOrder.id}" name="poId" type="hidden" /> 
									<input class="form-control" id="ePO_customerId" value="${purchaseOrder.customerId}" name="customerId" type="hidden" />
										<div class="table-responsive">
											<table id="ePO_myTable"
												class="table table-bordered table-striped table-advance table-hover">
												<thead>
													<tr>
														<th>
															<!-- S.No -->
														</th> 
														 <th style="font-size: 13px;font-family: arial;">Event Name</th>
														<th style="font-size: 13px;font-family: arial;">Section</th>
														<th style="font-size: 13px;font-family: arial;">Row</th>
														<th style="font-size: 13px;font-family: arial;">Seat Low</th>
														<th style="font-size: 13px;font-family: arial;">Seat High</th>
														<th style="font-size: 13px;font-family: arial;">Qty</th>
														<th style="font-size: 13px;font-family: arial;">Price</th>
														<th style="font-size: 13px;font-family: arial;">Action</th>
													</tr>
												</thead>
												<tbody id="ePO_myTableBody">
													<!-- dynamic rows -->
													<!--<h4 id="totalQty" style="float:right;">Total qty</h4>-->
													<c:forEach var="ticketGroup" items="${ticketGroup}" varStatus="theCount">
														<input type="hidden" id="rowId_${theCount.count}" name="rowId_${theCount.count}" value="${ticketGroup.eventId}">
														<input type="hidden" name="ticketGroup_${theCount.count}_${ticketGroup.id}" />
														<%-- <input type="text" name="eventId_${ticketGroup.eventId}" value=""/> --%>
														<tr>
															<<td>
																<%-- ${theCount.count} --%>
															</td> 
															<td><label name="event_${ticketGroup.id}">
																${ticketGroup.events.eventName} ${ticketGroup.events.eventDateStr} ${ticketGroup.events.eventTimeStr}</label>
															</td>
															<td><input type="text" class="section" name="section_${theCount.count}_${ticketGroup.id}"
																value="${ticketGroup.section}" />
															</td>
															<td><input type="text" class="rowValue" name="row_${theCount.count}_${ticketGroup.id}"
																size="10" value="${ticketGroup.row}" />
															</td>
															<td><input type="text" class="seatLow" name="seatLow_${theCount.count}_${ticketGroup.id}"
																value="${ticketGroup.seatLow}" />
															</td>
															<td><input type="text" class="seatHigh" name="seatHigh_${theCount.count}_${ticketGroup.id}"
																value="${ticketGroup.seatHigh}" />
															</td>
															<td><input type="text" name="qty_${theCount.count}_${ticketGroup.id}"
																id="qty_${theCount.count}_${ticketGroup.id}" class="ePO_qty"
																onkeyup="sumQty(this);" value="${ticketGroup.quantity}" />
															</td>
															<td><input type="text" name="price_${theCount.count}_${ticketGroup.id}"
																id="price_${theCount.count}_${ticketGroup.id}" class="ePO_price"
																onkeyup="sumPrice(this);" value="${ticketGroup.price}" />
															</td>
															<td><input type="button" value="Remove"
																onclick="removeField(this); sumQty(this); sumPrice(this);" class="button" />
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
											</div>
										</div>
									
					<div class="full-width mt-20">					
						<div class="row mt-10">
							
								<div class="tab-fields">
								 <div class="form-group col-md-3 col-sm-4 col-xs-6">
									<label>Shipping Method : </label>
									<select id="ePO_shippingMethod" name="shippingMethod" class="form-control" onchange="editPOShowHideFedexInfo();">
									 <%-- <option value="-1">--select--</option>
									  <c:forEach items="${shippingMethods}" var="shippingMethod">
										<option <c:if test="${shippingMethod.id==purchaseOrder.shippingMethodId}"> Selected </c:if>
											value="${shippingMethod.id}"> ${shippingMethod.name}
										</option>
									</c:forEach>--%><!---->
									</select>
								 </div>
								 
								 <div class="form-group col-md-3 col-sm-4 col-xs-6" id="ePO_trackingInfo">
									Tracking No : <input type="text" name="trackingNo" id="ePO_trackingNo" value="${purchaseOrder.trackingNo}" class="form-control"/>
								 </div>
								 
								 <div class="form-group col-md-3 col-sm-4 col-xs-6" style="float:right">
									Total Price : $<input type="text" id="ePO_totalPrice" name="totalPrice" readOnly value="${purchaseOrder.poTotal}" style="border: none; background:none;" class="form-control"">
								 </div>
								 
								 <div class="form-group col-md-3 col-sm-4 col-xs-6" style="float:right">
									Total Qty : <input type="text" id="ePO_totalQty" name="totalQty" readOnly value="${purchaseOrder.ticketCount}" style="border: none; background:none;" class="form-control">
								 </div>
								 
								<div class="form-group col-md-3 col-sm-4 col-xs-6">
									<label>External PO No :</label>
									<input type="text" name="externalPONo" id="ePO_externalPONo" value="${purchaseOrder.externalPONo}" class="form-control"/>
								 </div>
								 <div class="form-group col-md-3 col-sm-4 col-xs-6">
									<label class="full-width">&nbsp</label>
									<label>Consignment PO :</label>
									<input type="checkbox" id="ePO_poTypeCheck" onclick="ePOTypeCheck();" />
									<input type="hidden" id="ePO_poType" name="poType" />
								 </div>
								 <div class="form-group col-md-3 col-sm-4 col-xs-6" id="ePO_consignmentPoDiv">
									
									<label>Consignment PO No :</label>
									<input type="text" name="consignmentPo" id="ePO_consignmentPo" value="${purchaseOrder.consignmentPoNo}" class="form-control"/>
								 </div>
								 <div class="form-group col-md-3 col-sm-4 col-xs-6">
									<label>Transaction Office :</label>
									<select name="transactionOffice" id="ePO_transactionOffice" class="form-control">
										<%--<option value="Main" <c:if test="${purchaseOrder.transactionOffice == 'Main'}" >selected</c:if>>Main</option>
										<option value="Spec" <c:if test="${purchaseOrder.transactionOffice == 'Spec'}" >selected</c:if>>Spec</option>--%><!---->
									</select>  
								 </div>
								</div>
							
						</div>
					</div>	
					</section>
					</div>
					</div>
					<div class="row">
						<div class="col-xs-12">		
							 <section class="panel">
								<header style="font-size: 13px;font-family: arial;" class="panel-heading">Payment Details</header>
								<div class="panel-body">
									<div class="row">
									
									<div class="tab-fields full-width">
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											  <input type="radio" name="paymentType" id="radioCard" value="card" onclick="ePOPaymentType(this);">Credit Card
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											  <input type="radio" name="paymentType" id="radioAccount" value="account" onclick="ePOPaymentType(this);">In Account
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											  <input type="radio" name="paymentType" id="radioCheque" value="cheque" onclick="ePOPaymentType(this);">Cheque
										</div>
									</div>
									<hr/>
									<div class="full-width tab-fields" id="ePO_cardInfo">
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Card Holder Name</label>
											<input type="text" name="name" id="ePO_name" <c:if test="${payment.paymentType=='card'}">value="${payment.name}"</c:if> class="form-control"/> 
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Card Type</label>
											<select name="cardType" id="ePO_cardType" class="form-control">
												<option value="American Express" <c:if test="${payment.cardType=='American Express'}">selected</c:if> >American Express</option>
												<option value="Visa" <c:if test="${payment.cardType=='Visa'}">selected</c:if> >Visa</option>
												<option value="MasterCard" <c:if test="${payment.cardType=='MasterCard'}">selected</c:if> >MasterCard</option>
												<option value="JCB" <c:if test="${payment.cardType=='JCB'}">selected</c:if> >JCB</option>
												<option value="Discover" <c:if test="${payment.cardType=='Discover'}">selected</c:if> >Discover</option>
												<option value="Diners Club" <c:if test="${payment.cardType=='Diners Club'}">selected</c:if> >Diners Club</option>
											</select> 
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Card Last 4 Digit</label>
											<input type="text" name="cardNo" id="ePO_cardNo" value="${payment.cardLastFourDigit}" class="form-control"/> 
										</div>
										<hr/>
									</div>
									<div class="tab-fields full-width" id="ePO_accountInfo">
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Account Name</label>
											<input type="text" name="accountName" id="ePO_accountName" <c:if test="${payment.paymentType=='account'}">value="${payment.name}"</c:if> class="form-control"/> 
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Routing no. Last 4 Digit</label>
											<input type="text" name="routingNo" id="ePO_routingNo" <c:if test="${payment.paymentType=='account'}">value="${payment.routingLastFourDigit}"</c:if> class="form-control"/>
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Account no. Last 4 Digit</label>
											<input type="text" name="accountNo" id="ePO_accountNo"  <c:if test="${payment.paymentType=='account'}">value="${payment.accountLastFourDigit}"</c:if>value="${payment.accountLastFourDigit}" class="form-control"/> 
										</div>
										<hr/>
									</div>
									<div class="full-width tab-fields" id="ePO_chequeInfo">
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Name</label>
											<input type="text" name="chequeName" id="ePO_chequeName" <c:if test="${payment.paymentType=='cheque'}">value="${payment.name}"</c:if> class="form-control"/> 
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Routing no. Last 4 Digit</label>
											
											<input type="text" name="routingChequeNo" id="ePO_routingChequeNo" <c:if test="${payment.paymentType=='cheque'}">value="${payment.routingLastFourDigit}"</c:if> class="form-control"/>
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Account no. Last 4 Digit</label>
											<input type="text" name="accountChequeNo" id="ePO_accountChequeNo" <c:if test="${payment.paymentType=='cheque'}">value="${payment.accountLastFourDigit}"</c:if>  class="form-control"/>
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Cheque No.</label>
											<input type="text" name="checkNo" id="ePO_checkNo" value="${payment.chequeNo}" class="form-control"/> 
										</div>
									</div>
									
									</div>
									<div class="row">
									
									<div class="tab-fields full-width">
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>Payment Date</label>
											<input type="text" name="paymentDate" id="ePO_paymentDate" value="${payment.paymentDateStr}" class="form-control"/> 
										</div>
										<div class="form-group col-md-3 col-sm-4 col-xs-6">
											<label>payment Status</label>
											<select name="paymentStatus" id="ePO_paymentStatus" class="form-control">
												<%--<option value="Paid" <c:if test="${payment.paymentStatus=='Paid'}">selected</c:if>>Paid</option>
												<option value="Pending"  <c:if test="${payment.paymentStatus=='Pending'}">selected</c:if>>Pending</option>--%><!---->
											</select> 
										</div>
										<div class="form-group col-md-6 col-sm-4 col-xs-6">
											<label>Payment Note</label>
											<textarea name="paymentNote" id="ePO_paymentNote" rows="4" cols="50" class="form-control">${payment.paymentNote}</textarea>
										</div>
										
									</div>
									
									</div>
								</div>
							</section>
						</div>

					</div>

					
					</form>
					</div>
					

				<div id="eventInfo" class="full-width mt-20">
					<div class="full-width">
						<header style="font-size: 13px;font-family: arial;" class="panel-heading mb-20">Select an event to add ticket </header>
						<div  class="tab-fields full-width">
									<!--  search form start -->
							<form:form class="form-inline" role="form" onsubmit="return false" id="ePO_eventSearch" method="post" action="${pageContext.request.contextPath}/Events">
							<input type="hidden" id="eventID" name="eventID">
								<%-- <div class="form-group col-md-3 col-sm-4 col-xs-6">
									<label for="name" class="control-label">From Date</label> 
									<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
								</div>
								<div class="form-group col-md-3 col-sm-4 col-xs-6">
									<label for="name" class="control-label">To Date</label> 
									<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
								</div> --%>			
								<div class="form-group col-md-3 col-sm-4 col-xs-6">
									<!--<select id="searchType" name="searchType" class="form-control input-sm m-bot15">-->
									<select id="searchType" name="searchType" class="form-control">
										<option value="event_name">Event</option>
										<option value="artist_name" <c:if test="${searchType eq 'artist_name'}">selected</c:if> >Artist</option>
										<%-- <option value="grand_child_category_name" <c:if test="${searchType eq 'grand_child_category_name'}">selected</c:if> >GrandChildCategory</option>
										<option value="building" <c:if test="${searchType eq 'building'}">selected</c:if> >Venue</option>
										<option value="city" <c:if test="${searchType eq 'city'}">selected</c:if> >City</option>
										<option value="state" <c:if test="${searchType eq 'state'}">selected</c:if> >State</option>	 --%>			  
									</select>
								</div>
								<div class="form-group col-md-3 col-sm-4 col-xs-6">			
									<div class="navbar-form">
										<input class="form-control searchcontrol" placeholder="Search" type="text" id="ePO_eventSearchValue" name="eventSearchValue" value="${eventSearchValue}">
									</div>
								</div>
								<div class="form-group col-md-3 col-sm-4 col-xs-6">		
									<button type="button" id="ePO_searchEventBtn" class="btn btn-primary" onclick="ePOSearchEventData();">Search</button>
								</div>
								
								  </form:form>
								   <!--  search form end -->                
							 
					</div>
					
					<!-- event Grid Code Starts -->
					<div  class="full-width mt-10" style="position: relative; display: none;" id="ePO_eventGridDiv">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Events Details</label>
								<div class="pull-right">
									<a href="javascript:ePOEventResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
								</div>
							</div>
							<div id="editPOEvent_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="editPOEvent_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					
					<div class="form-group full-width mt-20 text-center" id="ePO_addTicketDiv" style="display: none">
						
							<button class="btn btn-primary" type="submit" id="ePO_addTicket" onclick="ePOAddTicketInfo();">Add Ticket</button>
						
					</div>
					<br/>
					</div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width" id="editBtnDiv">
				<button class="btn btn-primary" type="button" onclick="doEditPOValidation();">Update PO</button>
				<button class="btn btn-primary" type="button" onclick="doVoidPO();">Void PO</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
			<div class="modal-footer full-width" id="closeBtnDiv" style="display:none">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		  </div>
		  
		</div>
	</div>
<!-- End popup Edit PO -->

<!-- popup Create Fedex Label -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#create-fedex-label">Create Fedex Label</button> -->
	<div id="create-fedex-label" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Create Fedex Label - PO No : <span id="poId_Hdr_CreateFedex" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Shipping Address</li>
						</ol>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div id="poFedex_successDiv" class="alert alert-success fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="poFedex_successMsg"></span></strong>
						</div>					
						<div id="poFedex_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="poFedex_errorMsg"></span></strong>
						</div>
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class="col-lg-12">
						<div class="full-width mt-10 mb-20" style="position: relative">
							<div class="table-responsive grid-table">
								<div class="grid-header full-width">
									<label>Customer Shipping/Other Addresses</label> <!-- <span id="shipping_grid_toogle_search" style="float: right"
										class="ui-icon ui-icon-search" title="Toggle search panel"
										onclick="shippingToggleFilterRow()"></span> -->
								</div>
								<div id="poFedexShipping_Grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
								<div id="poFedexShipping_pager" style="width: 100%; height: 20px;"></div>
							</div>
						</div>				
						<br />
						
						<div style="position: relative">
							<div class="full-width">
								<div class="col-sm-4">
									<label>Company Name <span class="required">*</span></label> 
									<input type="text" id="poFedex_companyName" name="companyName" class='form-control' />
								</div>
								<div class="col-sm-4">
									<label>Service Type <span class="required">*</span> </label> 
									<select id="poFedex_serviceType" name="serviceType" class="form-control" onchange="">
										<option value='select'>--select--</option>
										<option value="FEDEX_EXPRESS_SAVER">FEDEX EXPRESS SAVER</option>
										<option value="FIRST_OVERNIGHT">FIRST OVERNIGHT</option>
										<option value="PRIORITY_OVERNIGHT">PRIORITY OVERNIGHT</option>
										<option value="STANDARD_OVERNIGHT">STANDARD OVERNIGHT</option>
										<option value="FEDEX_2_DAY">FEDEX 2-DAY</option>
										<option value="GROUND_HOME_DELIVERY">GROUND HOME DELIVERY</option>
										<option value="INTERNATIONAL_PRIORITY">INTERNATIONAL PRIORITY</option>
									</select>
								</div>
								<div class="col-sm-4">
									<label>Signature Type <span class="required">*</span> </label> 
									<select id="poFedex_signatureType" name="signatureType" class="form-control" onchange="">
										<option value="NO_SIGNATURE_REQUIRED">NO SIGNATURE REQUIRED</option>
										<option value="SERVICE_DEFAULT">SERVICE DEFAULT</option>
										<option value="INDIRECT">INDIRECT</option>
										<option value="DIRECT">DIRECT</option>
										<option value="ADULT">ADULT</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--</div>-->
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" name="poId" id="poFedex_poId" value="${purchaseOrderId}" />
				<button type="button" class="btn btn-primary" onclick="createFedexLabelForPO();">Create</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Create Fedex Label -->


<script>
	var poId; 
	var dataView;
	var grid;
	var data = [];
	var columns=[];
	var poGridSearchString='';
	var columnFilters = {};
	var sortingString='';
	var userPOColumnsStr = '<%=session.getAttribute("pogrid")%>';
	var userPOColumns =[];
	var loadPOColumns = ["purchaseOrderId", "customerName", "poTotal", "ticketQty", "created", "createdBy", "productType",
		"csr", "poAged", "trackingNo", "view", "eventName", "eventDate", "eventTime","RTWPO","brokerId"];
	var allPOColumns = [ {
		id : "purchaseOrderId",
		name : "PO Id",
		width:80,
		field : "purchaseOrderId",
		sortable : true
	},{
		id : "customerName",
		name : "Customer",
		width:80,
		field : "customerName",
		sortable : true
	}, {
		id : "customerType",
		name : "Customer Type",
		field : "customerType",
		width:80,
		sortable : true
	}, {
		id : "purchaseOrderType",
		name : "Purchase Order Type",
		field : "purchaseOrderType",
		width:80,
		sortable : true
	}, {
		id : "poTotal",
		name : "Grand Total",
		field : "poTotal",
		width:80,
		sortable : true
	},{
		id : "ticketQty",
		name : "Ticket Qty",
		field : "ticketQty",
		width:80,
		sortable : true
	},{
		id : "eventTicketQty",
		name : "Event Ticket Qty",
		field : "eventTicketQty",
		width:80,
		sortable : true
	},{
		id : "eventTicketCost",
		name : "Event Ticket Cost",
		field : "eventTicketCost",
		width:80,
		sortable : true
	},{
		id : "created",
		name : "Created",
		field : "created",
		width:80,
		sortable : true
	},{
		id : "createdBy",
		name : "Created By",
		field : "createdBy",
		width:80,
		sortable : true
	},{
		id : "shippingType",
		name : "Shipping Type",
		field : "shippingType",
		width:80,
		sortable : true
	},  {id: "status", 
		name: "Status", 
		field: "status",
		width:80,		
		sortable: true
	},
	{id: "productType",name: "Product Type",field: "productType",width:80,sortable: true},
	{id: "csr",name: "CSR",field: "csr",width:80,sortable: true},
	{id: "poAged",name: "PO Aged",field: "poAged",width:80,sortable: true},
	{id: "lastUpdated",name: "Last Updated",field: "lastUpdated",width:80,sortable: true},
	{id: "isEmailed",name: "IS Emailed",field: "isEmailed",width:80,sortable: true},
	{id: "consignmentPoNo",name: "Consignment Po No",field: "consignmentPoNo",width:80,sortable: true},
	{id: "transactionOffice",name: "Transaction Office",field: "transactionOffice",width:80,sortable: true},
	{id: "trackingNo",name: "Tracking No",field: "trackingNo",width:80,sortable: true, formatter : trackOrderFormatter},
	{
		id : "view",
		name : "Fedex Label",
		field : "view",
		width : 100,
		sortable : true,
		formatter : viewLinkFormatter
	},
	{id: "shippingNotes",name: "Shipping Notes",field: "shippingNotes",width:80,sortable: true},
	{id: "externalNotes",name: "External Notes",field: "externalNotes",width:80,sortable: true},
	{id: "internalNotes",name: "Internal Notes",field: "internalNotes",width:80,sortable: true},
	{
		id : "eventId",
		name : "Event Id",
		field : "eventId",
		width : 100,
		sortable : true
	}, {
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		width : 100,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width : 100,
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width : 100,
		sortable : true
	},{
		id : "RTWPO",
		name : "RTW PO",
		field : "RTWPO",
		width : 100,
		sortable : true
	}, {
		id : "brokerId",
		name : "Broker Id",
		field : "brokerId",
		width : 100,
		sortable : true
	}
	];

	if(userPOColumnsStr!='null' && userPOColumnsStr!=''){
		var columnOrder = userPOColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allPOColumns.length;j++){
				if(columnWidth[0] == allPOColumns[j].id){
					userPOColumns[i] =  allPOColumns[j];
					userPOColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadPOColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allPOColumns.length;j++){
				if(columnWidth == allPOColumns[j].id){
					userPOColumns[i] = allPOColumns[j];
					userPOColumns[i].width=80;
					break;
				}
			}			
		}
		//userPOColumns = allPOColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "customerName";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
	var searchString = "";
	
	//Now define your buttonFormatter function
	function viewLinkFormatter(row, cell, value, columnDef, dataContext) {		
		var fedexLabelCreated = grid.getDataItem(row).view;
		var link = '';
		if(fedexLabelCreated == 'Yes'){
			link = "<img class='editClickableImag' style='height:17px;' src='../resources/images/viewPdf.png' onclick='getFedexLabel("
					+ grid.getDataItem(row).purchaseOrderId + ")'/>";
		}
		return link;
	}
	
	function trackOrderFormatter(row, cell, value, columnDef, dataContext) {
		var trackingNo = grid.getDataItem(row).trackingNo;
		var link = '';
		if (trackingNo != null && trackingNo != '') {
			link = "<a href='javascript:trackMyOrder(" + trackingNo + ")'><u>"
					+ trackingNo + "</u></a>";
		}
		return link;
	}
	
	function editPOFormatter(row, cell, value, columnDef, dataContext) {
		/*  var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />"; */
		var editPObutton = "<input type='button' class='editPO' value='Edit PO' id='"+ dataContext.id +"'/>";
		//the id is so that you can identify the row when the particular button is clicked
		return editPObutton;
		//Now the row will display your button
	}
	function deletePOFormatter(row, cell, value, columnDef, dataContext) {
		/* var button = "<input class='audit' value='Audit' type='button' id='"+ dataContext.id +"' />"; */
		var deletePObutton = "<input type='button' class='deletePO' value='Delete PO' id='"+ dataContext.id +"'/>";
		//the id is so that you can identify the row when the particular button is clicked
		return deletePObutton;
		//Now the row will display your button
	}

	//Function to hook up the edit button event
	$('.editPO').live('click', function() {
		var me = $(this), id = me.attr('id');
		popupPOEdit(id);
	});
	
	//Now you can use jquery to hook up your delete button event
	$('.deletePO').live('click', function() {
		var me = $(this), id = me.attr('id');
		var delFlag = deletePO(id);
	});

	function deleteRecordFromGrid(id) {
		dataView.deleteItem(id);
		grid.invalidate();
	}
	
	function trackMyOrder(trackingNo) {
		var url = "https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber="
				+ trackingNo;
		popupCenter(url, "Fedex Tracking", "1100", "900");
	}
	
	function getFedexLabel(poId){
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/CheckFedexLabelGenerated",
			type : "get",
			data : "poId="+ poId,
			dataType:"json",
			success : function(res){
				if(res.msg=='OK'){
					 //var url = "${pageContext.request.contextPath}/Accounting/GetFedexLabelPdf?poId="+poId;
					 var url = apiServerUrl+"GetFedexLabelPdf?poId="+poId;
					 $('#download-frame').attr('src', url);
				}else{
					jAlert("Fedex Label not found for selected invoice, Please Generate fedex Label.");
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
		
	}
	/*
	function myFilter(item, args) {
		var x= item["customerName"];
		if (args.searchString != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function toggleFilterRow() {
		grid.setTopPanelVisibility(!grid.getOptions().showTopPanel);
	}
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){
		if(id=='pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getPOGridData(pageNo);
		}
		if(id=='custArtist_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custArtistPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custArtistPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custArtistPagingInfo.pageNum)-1;
			}
			getArtistGridData(pageNo);
		}
		if(id=='custEvent_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custEventPagingInfo.pageNum)-1;
			}
			getCustEventsGridData(pageNo);
		}
		if(id=='custRewardPoints_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custRewardPointsPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custRewardPointsPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custRewardPointsPagingInfo.pageNum)-1;
			}	
			getRewardPointsGridData(pageNo);
		}
		if(id == "custInvoiceTicket_pager"){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum)-1;
			}
		}
		if(id=='createPOCustomer_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(createPOCustomerPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(createPOCustomerPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(createPOCustomerPagingInfo.pageNum)-1;
			}
			getCreatePOCustomerGridData(pageNo);
		}
		if(id=='createPOEvent_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(createPOEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(createPOEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(createPOEventPagingInfo.pageNum)-1;
			}
			getCreatePOEventGridData(pageNo);
		}
		if(id=='editPOEvent_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(editPOEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(editPOEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(editPOEventPagingInfo.pageNum)-1;
			}
			getEditPOEventGridData(pageNo);
		}
	}
	
	function getPOGridData(pageNo) {
		var poNo = $('#poNo').val(); 
		if(poNo != null && poNo != ""){
			$('#poNo').val('');
			$("#action").val('search');
		}	
			
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/ManagePO.json",
			type : "post",
			dataType: "json",
			data : $("#poSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+poGridSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshPOGridValues(jsonData.purchaseOrders);
				clearAllSelections();
				$('#pager> div').append("<span class='slick-pager-status'> <b>Purchase Order Grand Total : <span id='purchaseOrderTotal'>"+jsonData.purchaseOrderTotal+"</span></b></span>");				
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPOPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshPOGridValues(jsonData) {
		data = [];
		var prodType;
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  po= jsonData[i];
				var d = (data[i] = {});
				prodType = po.productType;
				d["id"] = i;
				d["purchaseOrderId"] = po.id;
				d["customerName"] = po.customerName;
				d["customerId"] = po.customerId;
				d["customerType"] = po.customerType;
				d["poTotal"] = po.poTotal;
				d["ticketQty"] = po.ticketQty;
				d["eventTicketQty"] = po.eventTicketQty;
				d["eventTicketCost"] = po.eventTicketCost;
				d["created"] = po.createdDateStr;
				d["createdBy"] = po.createdBy;
				d["shippingType"] = po.shippingType;
				d["status"] = po.status;
				d["productType"] = po.productType;
				d["lastUpdated"] = po.lastUpdatedStr;
				d["poAged"] = po.poAged;
				d["csr"] = po.csr;
				d["purchaseOrderType"] = po.purchaseOrderType;
				d["consignmentPoNo"] = po.consignmentPoNo;
				d["transactionOffice"] = po.transactionOffice;
				d["trackingNo"] = po.trackingNo;
				d["shippingNotes"] = po.shippingNotes;
				d["externalNotes"] = po.externalNotes;
				d["internalNotes"] = po.internalNotes;
				d["eventId"] = po.eventId;
				d["eventName"] = po.eventName;
				d["eventDate"] = po.eventDateStr;
				d["eventTime"] = po.eventTimeStr;	
				d["isEmailed"] = po.isEmailed;
				d["RTWPO"] = 'No';
				if(po.posPOId!=null && po.posPOId!=''){
					d["RTWPO"] = 'Yes';
				}
				d["view"] = po.fedexLabelCreated;
				d["brokerId"] = po.brokerId;
			}
		}
		
		if(prodType == "RTW" || prodType == "RTW2")
			$("#editActionButton").hide();

		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#poGrid", dataView, userPOColumns, options);
		grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		grid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"),pagingInfo);
		}
		
		var columnpicker = new Slick.Controls.ColumnPicker(allPOColumns, grid,
				options);

		// move the filter panel defined in a hidden div into grid top panel
		/*$("#inlineFilterPanel").appendTo(grid.getTopPanel()).show();

		grid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < dataView.getLength(); i++) {
				rows.push(i);
			}
			grid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		grid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = grid.getSelectedRows([0])[0];
			if (temprEventRowIndex != poId) {
				poId = grid.getDataItem(temprEventRowIndex).purchaseOrderId;
			}
		});
		grid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = grid.getCellFromEvent(e);
		      grid.setSelectedRows([cell.row]);
		      var productType = grid.getDataItem(cell.row).productType;
		      poId = grid.getDataItem(cell.row).purchaseOrderId;
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < ($("#contextMenu").height()+100)){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY;
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			if(productType == "REWARDTHEFAN"){
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
			}else if(productType == "RTW" || productType == "RTW2"){
				 $("#contextMenuOther")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenuOther").hide();
		      });
			}
		    });
		grid.onSort.subscribe(function(e, args) {
			sortcol = args.sortCol.field;			
			if(sortingString.indexOf(sortcol) < 0){
				sortdir = 'ASC';
			}else{
				if(sortdir == 'DESC' ){
					sortdir = 'ASC';
				}else{
					sortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+sortcol+',SORTINGORDER:'+sortdir+',';
			getPOGridData(0)
		});
		// wire up model events to drive the grid
		dataView.onRowCountChanged.subscribe(function(e, args) {
			grid.updateRowCount();
			grid.render();
		});
		dataView.onRowsChanged.subscribe(function(e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});

		$(grid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	poGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						 poGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getPOGridData(0);
				}
			  }
		 
		});
		grid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'productType'){
					if(args.column.id == 'eventTime'){
						$("<input type='text' placeholder='hh:mm a'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else if(args.column.id == 'created' || args.column.id == 'lastUpdated' || args.column.id == 'eventDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}			
		});
		grid.init();
		//var h_runfilters = null;
		// wire up the slider to apply the filter to the model
		/*$("#pcSlider,#pcSlider2").slider({
		  "range": "min",
		  "slide": function (event, ui) {
		    Slick.GlobalEditorLock.cancelCurrentEdit();
		    if (percentCompleteThreshold != ui.value) {
		      window.clearTimeout(h_runfilters);
		      h_runfilters = window.setTimeout(updateFilter, 10);
		      percentCompleteThreshold = ui.value;
		    }
		  }
		});*/
		
		/*$("#btnSelectRows").click(function () {
		  if (!Slick.GlobalEditorLock.commitCurrentEdit()) {
		    return;
		  }
		  var rows = [];
		  for (var i = 0; i < 10 && i < dataView.getLength(); i++) {
		    rows.push(i);
		  }
		  grid.setSelectedRows(rows);
		});*/
		// initialize the model after all the events have been hooked up
		dataView.beginUpdate();
		dataView.setItems(data);
		
		dataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		dataView.syncGridSelection(grid, true);
		$("#gridContainer").resizable();
		grid.resizeCanvas();
	}

	
	$("#contextMenuOther").click(function (e) {
		if (!$(e.target).is("li")) {
		  return;
		}
		var row = grid.getSelectedRows([0])[0];
		var productType = grid.getDataItem(row).productType;
		if(row < 0){
			jAlert("Please select Invoice.");
			return;
		}
		var productType = grid.getDataItem(row).productType;
		if($(e.target).attr("data") == 'view payment history'){
			getPOPaymentHistory(poId, productType);
			 /*var url = "${pageContext.request.contextPath}/Accounting/ViewPOPaymentHistory?poId="+poId+"&productType="+productType;
			 popupCenter(url,"PO Payment History","800","400");*/
		}else if($(e.target).attr("data") == 'view related invoices'){
			getRelatedInvoicesPO(poId, productType);
			 /*var url = "${pageContext.request.contextPath}/Accounting/ViewRelatedPOInvoices?poId="+poId+"&productType="+productType;
			 popupCenter(url,"View Related invoice(s)","1000","700");*/
		}else if($(e.target).attr("data") == 'view customer details'){
			 var customerId = grid.getDataItem(row).customerId;
			 $('#idHdr_CustomerDetails').text("PO No : "+poId);
			 getCustomerInfoForEdit(customerId);
			 /*var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="+customerId;
			 popupCenter(url,"View Customer","800","800");*/
		}else if($(e.target).attr("data") == 'pdf'){
			generateRTWRTW2POPdf(poId,productType, 'attachment');
		}else if($(e.target).attr("data") == 'print po'){
			generateRTWRTW2POPdf(poId,productType, 'inline');
		}
	});
	 
	$("#contextMenu").click(function (e) {
		if (!$(e.target).is("li")) {
		  return;
		}
		var row = grid.getSelectedRows([0])[0];
		if(row < 0){
			jAlert("Please select Invoice.");
			return;
		}
		var productType = grid.getDataItem(row).productType;
		if($(e.target).attr("data") == 'pdf'){
			generatePOPdf('attachment');
		}else if($(e.target).attr("data") == 'print po'){
			generatePOPdf('inline');
		}else if($(e.target).attr("data") == 'view payment history'){
			 getPOPaymentHistory(poId, productType);
			 /*var url = "${pageContext.request.contextPath}/Accounting/ViewPOPaymentHistory?poId="+poId+"&productType="+productType;
			 popupCenter(url,"PO Payment History","800","400");*/
		}else if($(e.target).attr("data") == 'view related invoices'){
			getRelatedInvoicesPO(poId, productType);
			 /*var url = "${pageContext.request.contextPath}/Accounting/ViewRelatedPOInvoices?poId="+poId+"&productType="+productType;
			 popupCenter(url,"View Related invoice(s)","1000","700");*/
		}else if($(e.target).attr("data") == 'view modify notes'){
			getPONote(poId);
			 /*var url = "${pageContext.request.contextPath}/Accounting/EditPONote?poId="+poId;
			 popupCenter(url,"View/Modify PO Note","500","600");*/
		}else if($(e.target).attr("data") == 'view customer details'){
			 var customerId = grid.getDataItem(row).customerId;
			 $('#idHdr_CustomerDetails').text("PO No : "+poId);
			 getCustomerInfoForEdit(customerId);			 
			 /*var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="+customerId;
			 popupCenter(url,"View Customer","800","800");*/
		}else if($(e.target).attr("data") == 'modify ticket onHand'){
			getPOTicketGroupOnHandStatus(poId);
			 /*var url = "${pageContext.request.contextPath}/Accounting/UpdatePOTicketGroupOnhandStatus?poId="+poId;
			 popupCenter(url,"Edit Ticket ON-HAND Status","900","500");*/
		}else if($(e.target).attr("data") == 'edit po csr'){
			getPOCSR(poId);
			/*var url = "${pageContext.request.contextPath}/Accounting/EditPOCsr?poId="+poId;
			popupCenter(url,"Edit PO CSR","500","400");*/
		}else if($(e.target).attr("data") == 'upload eticket'){
			getUploadETicket(poId);
			/*var url = "${pageContext.request.contextPath}/Accounting/UploadEticket?poId="+poId;
			popupCenter(url,"Upload E-Ticket","1200","600");*/
		}
	});
	
	function generatePOPdf(inline){
		 //var url = "${pageContext.request.contextPath}/Accounting/GetPOPdf?poId="+poId+"&inline="+inline+"&brokerId="+$('#brokerId').val();
		 var url = apiServerUrl+"GetPOPdf?poId="+poId+"&inline="+inline+"&brokerId="+$('#brokerId').val();
		 if(inline=='inline'){
 			 popupCenter(url,"PO PDF","1000","900");
		 }else{
			 $('#download-frame').attr('src', url);
		 }
	}
	 
	function generateRTWRTW2POPdf(poId,productType, inline) {
		/* var url = "${pageContext.request.contextPath}/Accounting/GetRTWRTW2POPdf?poId="
				+ poId +"&productType=" +productType+"&inline=" + inline; */
		var url = apiServerUrl+"GetRTWRTW2POPdf?poId="+ poId +"&productType=" +productType+"&inline=" + inline;
		if (inline == 'inline') {
			popupCenter(url, "RTW/RTW2 PO PDF", "1000", "900");
		} else {
			$('#download-frame').attr('src', url);
		}
	}
	
	function editPO() {
		if(poId != null && poId!='') {
			//popupPOEdit(poId);
			editPOCall(poId);
		} else {
			jAlert("Select a Purchase order to edit.","Info");
		}
	}
	
	//popup purchase order edit
	function popupPOEdit(poId) {
		var editPOUrl = "EditPurchaseOrder?poId=" + poId;
		popupCenter(editPOUrl, 'Edit Purchase Order', '1200', '1200');
	}

	//Delete selected purchase order 
	function deletePO(poId){
		if(poId == ''){
			jAlert("Please try again.","Info");
			return false;
		}
		jConfirm("Are you sure to delete an PO ?","Confirm",function(r){
			if(r){
				$.ajax({
					url : "${pageContext.request.contextPath}/Accounting/DeletePO",
					type : "post",
					data : "userName="+ $("#userName").val() + "&poId=" + poId,
					/* async : false, */
					success : function(response){
						if(response == "true"){
							jAlert("Purchase order deleted successfully.","Info");
							deleteRecordFromGrid(poId);
							return true;
						}else{
							jAlert(response,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
			}else {
				return false;
			}
		});
		return false;
	}
	function searchPo(){
		$("#action").val('search');
		poGridSearchString='';
		columnFilters = {};
		getPOGridData(0);
	}
	
	function oncChildClosed(){
		jAlert("Record updated succesfully.","Info");
	}
	
	function exportToExcel(){
		//var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&poNo="+$('#poNo').val()+"&csr="+$('#csr').val()+"&customer="+$('#customer').val()+"&productType="+$('#productType').val();
		var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&productType="+$('#productType').val()+"&headerFilter="+poGridSearchString;
		appendData += "&brokerId="+$('#brokerId').val();
	    //var url = "${pageContext.request.contextPath}/Accounting/ExportToExcel?"+appendData;
	    var url = apiServerUrl+"PurchaseOrdersExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		$("#action").val('search');
		poGridSearchString='';
		sortingString ='';
		columnFilters = {};
		getPOGridData(0);
	}
	
	function createFedexLabel() {
		var row = grid.getSelectedRows([0])[0];
		if (row == null) {
			jAlert("Please select a PO to create fedex label.");
		} else {
			var shippingMethod = grid.getDataItem(row).shippingType;
			var poId = grid.getDataItem(row).purchaseOrderId;
			var fedexLabelCreated = grid.getDataItem(row).view;
			if (shippingMethod != "FedEx") {
				jAlert("Could not create Fedex label, Selected PO shipping type is not FedEx.");
				return;
			}			
			if(fedexLabelCreated != null && fedexLabelCreated == 'Yes'){
				jAlert("Label is already created for selected PO, to recreate again. Please remove label first.");
				return;
			}	
			getShippingAddressForFedex(poId);
			/*var url = "${pageContext.request.contextPath}/Accounting/getShippingAddresses?poId="+ poId;
			popupCenter(url, "Create Fedex Label - PO", "800",
					"600");*/
		}
	}
	
	function deleteFedexLabel() {
		var row = grid.getSelectedRows([0])[0];
		if (row == null) {
			jAlert("Please select a PO to remove fedex label.");
		} else {
			var poId = grid.getDataItem(row).purchaseOrderId;
			var trackingNo = grid.getDataItem(row).trackingNo;
			var fedexLabelCreated = grid.getDataItem(row).view;
			if(fedexLabelCreated != null && fedexLabelCreated == "Yes"){
			jConfirm("Are you sure you want to delete fedex label for selected PO ?","Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/Accounting/RemoveFedexLabel",
						type : "post",
						dataType : "json",
						data : "poId=" + poId,
						success : function(res) {
							var jsonData = JSON.parse(JSON.stringify(res));
							if(jsonData.status == 1){
								getPOGridData(pagingInfo.pageNum);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
							//jAlert(jsonData.msg);							
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}else{
					return false;
				}
			});
			}else{
				jAlert("No FedEx label is created for selected PO.");
				return;
			}
		}
	}

	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
	
	function saveUserPOPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('pogrid',colStr);
	}
	
$(document).ready(function(){
	 $('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation:"bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation:"bottom",
		todayHighlight: true
    });
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshPOGridValues(JSON.parse(JSON.stringify(${purchaseOrders})));
		$('#pager> div').append("<span class='slick-pager-status'> <b>Purchase Order Grand Total : <span id='purchaseOrderTotal'>${purchaseOrderTotal}</span></b></span>");
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPOPreference()'>");
		
	});
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  grid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getPOGridData(0);
		    return false;  
		  }
		});

});
	
//poupup create po
/*function createPO(){	
	var createPOUrl = "${pageContext.request.contextPath}/PurchaseOrder/CreatePurchaseOrder";
	popupCenter(createPOUrl, 'create po for customer', '1200', '1200');
}*/	
	
	
	//Start - View Payment History	
	function getPOPaymentHistory(poId, productType){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetPOPaymentHistory",
			type : "post",
			data : "poId="+poId+"&productType="+productType,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setPOPaymentHistory(jsonData);
					$('#poId_Hdr_PayHistory').text(poId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setPOPaymentHistory(jsonData){
		$('#view-payment-history').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#poPayHistory_successDiv').hide();
			$('#poPayHistory_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#poPayHistory_errorDiv').hide();
			$('#poPayHistory_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#poPayHistory_successDiv').show();
			$('#poPayHistory_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#poPayHistory_errorDiv').show();
			$('#poPayHistory_errorMsg').text(jsonData.errorMessage);
		}*/
		if(jsonData.paymentDetails != null){
			$('#poPaymentGrid').show();
			$('#rtwPOPaymentGrid').hide();
			poPaymentHistoryGridValues(jsonData.paymentDetails, jsonData.pagingInfo);
		}
		if(jsonData.rtwPaymentDetails != null){
			$('#poPaymentGrid').hide();
			$('#rtwPOPaymentGrid').show();
			rtwPOPaymentHistoryGridValues(jsonData.rtwPaymentDetails, jsonData.pagingInfo);
		}
	}
	//End Payment History
	
	//Start View Related Invoices
	function getRelatedInvoicesPO(poId, productType){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetRelatedPOInvoices",
			type : "post",
			data : "poId="+poId+"&productType="+productType,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Related PO/Invoice(s) Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setRelatedInvoicesPO(jsonData);
					$('#poId_Hdr_RelatedInvoice').text(poId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setRelatedInvoicesPO(jsonData){
		$('#view-related-invoices').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#relatedInv_successDiv').hide();
			$('#relatedInv_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#relatedInv_errorDiv').hide();
			$('#relatedInv_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#relatedInv_successDiv').show();
			$('#relatedInv_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#relatedInv_errorDiv').show();
			$('#relatedInv_errorMsg').text(jsonData.errorMessage);
		} */
		relatedPOGridValues(jsonData.poList, jsonData.poPagingInfo);
		relatedInvoiceGridValues(jsonData.ticketList, jsonData.pagingInfo);
	}
	//End View Related Invocies
	
	//Start - View Modify Notes Script
	function getPONote(poId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetPONote",
			type : "post",
			data : "poId="+poId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setPONote(jsonData);
					$('#poId_Hdr_ModNotes').text(poId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setPONote(jsonData){
		$('#modNotes_poId').val(jsonData.poId);
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#modNotes_successDiv').hide();
			$('#modNotes_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#modNotes_errorDiv').hide();
			$('#modNotes_errorMsg').text('');
		}*/
		$('#modNotes_internalNote').val(jsonData.internalNote);
		$('#modNotes_externalNote').val(jsonData.externalNote);
		$('#view-modify-notes').modal('show');
	}
	
	function updatePONote(){
		$('#modNotes_action').val('update');
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/GetPONote",
			type : "post",
			dataType : "json",
			data : $("#editPONote").serialize(),
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#view-modify-notes').modal('hide');
					getPOGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				/*if(jsonData.successMessage != null && jsonData.successMessage != ""){
					//$('#modNotes_successDiv').show();
					//$('#modNotes_successMsg').text(jsonData.successMessage);
					jAlert(jsonData.successMessage);
				}
				if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
					//$('#modNotes_errorDiv').show();
					//$('#modNotes_errorMsg').text(jsonData.errorMessage);
					jAlert(jsonData.errorMessage);
				}*/
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	//End Modify Notes Script
	
	//Start Modify Ticket ON-HAND/NOT-ON-HAND
	function getPOTicketGroupOnHandStatus(poId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/UpdatePOTicketGroupOnHandStatus",
			type : "post",
			data : "poId="+poId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setPOTicketGroupOnHandStatus(jsonData);
					$('#poId_Hdr_ModTicket').text(poId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setPOTicketGroupOnHandStatus(jsonData){
		$('#modify-ticket-onhand').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#poTicketGroup_successDiv').hide();
			$('#poTicketGroup_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#poTicketGroup_errorDiv').hide();
			$('#poTicketGroup_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#poTicketGroup_successDiv').show();
			$('#poTicketGroup_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#poTicketGroup_errorDiv').show();
			$('#poTicketGroup_errorMsg').text(jsonData.errorMessage);
		}*/
		$('#poTicketGroup_poId').val(jsonData.poId);
		clearAllSelections();
		poTicketGroupGridValues(jsonData.ticketGroups, jsonData.pagingInfo);
	}
	//End Modify Ticket ON-HAND/NOT-ON-HAND
	
	//Start Edit PO CSR
	function getPOCSR(poId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetPOCsr",
			type : "post",
			data : "poId="+poId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setPOCSR(jsonData);
					$('#poId_Hdr_EdCSR').text(poId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	
	function setPOCSR(jsonData){
		$('#edit-po-csr').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#poCsr_successDiv').hide();
			$('#poCsr_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#poCsr_errorDiv').hide();
			$('#poCsr_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#poCsr_successDiv').show();
			$('#poCsr_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#poCsr_errorDiv').show();
			$('#poCsr_errorMsg').text(jsonData.errorMessage);
		}*/
		updateComboBox(jsonData.users,poCsr_csrUser,jsonData.poCsr);
		$('#poCsr_poId').val(jsonData.poId);
	}
	
	function updateComboBox(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='auto'>Auto</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].name == listName){
					$(id).append("<option value="+jsonData[i].name+" selected>"+jsonData[i].name+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].name+">"+jsonData[i].name+"</option>");
				}
			}
		}
	}
	
	function changePOCsr(){
		$('#poCsr_action').val('changeCSR');
		$.ajax({
				url : "${pageContext.request.contextPath}/Accounting/GetPOCsr",
				type : "post",
				dataType : "json",
				data :$("#poCsr_changeCsr").serialize(),
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#edit-po-csr').modal('hide');
						getPOGridData(0);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					/*if(jsonData.successMessage != null && jsonData.successMessage != ""){
						//$('#poCsr_successDiv').show();
						//$('#poCsr_successMsg').text(jsonData.successMessage);
						jAlert(jsonData.successMessage);
					}
					if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
						//$('#poCsr_errorDiv').show();
						//$('#poCsr_errorMsg').text(jsonData.errorMessage);
						jAlert(jsonData.errorMessage);
					}*/
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
	}
	//End Edit PO CSR
	
	//Start Upload Eticket
	function getUploadETicket(poId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/UploadEticket",
			type : "post",
			data : "poId="+poId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData!=null || jsonData!="") {
					if(jsonData.status == 1){
						$('#upload-eticket').modal('show');
						setUploadETicket(jsonData);
						$('#poId_Hdr_UploadETicket').text(poId);
					}/* else{
						jAlert(jsonData.msg);
					} */
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	
	function setUploadETicket(jsonData){		
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#upEticket_successDiv').hide();
			$('#upEticket_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#upEticket_errorDiv').hide();
			$('#upEticket_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			//$('#upEticket_successDiv').show();
			//$('#upEticket_successMsg').text(jsonData.successMessage);
			jAlert(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			//$('#upEticket_errorDiv').show();
			//$('#upEticket_errorMsg').text(jsonData.errorMessage);
			jAlert(jsonData.errorMessage);
		}*/
		var purchaseOrderInfo = jsonData.purchaseOrder;
		$('#upEticket_poId_span').text(purchaseOrderInfo.poId);
		$('#upEticket_poTotal').text(purchaseOrderInfo.poTotal);
		$('#upEticket_createTime').text(purchaseOrderInfo.poCreateTime);
		$('#upEticket_csr').text(purchaseOrderInfo.poCsr);
		$('#upEticket_ticketCount').text(purchaseOrderInfo.poTicketCount);
		$('#upEticket_shippingMethod').text(purchaseOrderInfo.poShippingMethod);
		$('#upEticket_poId').val(purchaseOrderInfo.poId);
		$('#upEticket_customerId').val(purchaseOrderInfo.poCustomerId);
		
		var customerPOInfo = jsonData.customerInfo;
		$('#upEticket_customerName').text(customerPOInfo.customerName);
		setPOTicketGroups(jsonData.ticketGroup);
	}
	
	function setPOTicketGroups(jsonData){
		var ticketGroups = "";		
		if(jsonData != null && jsonData != ""){	
			var j=1;			
			for(var i=0; i<jsonData.length; i++){
				//j=1;
				var data = jsonData[i];
				$('#upEticket_TG').append("<tr id='upEt_"+j+"'></tr>");
				ticketGroups = "";
				ticketGroups += "<td>";
				ticketGroups += "<input type='hidden' id='rowId_"+j+"' name='rowId_"+j+"' value="+data.tgEventId+" >";
				ticketGroups += "<input type='hidden' name='ticketGroup_"+j+"_"+data.tgId+"' />";
				ticketGroups += "</td><td>";
				ticketGroups += "<label name='event_"+data.tgId+"'>";
				ticketGroups += data.tgEventName+" "+data.tgEventDate+" "+data.tgEventTime+"</label>";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' disabled='true' class='section' name='section_"+j+"_"+data.tgId+"' value="+data.tgSection+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' disabled='true' class='rowValue' name='row_"+j+"_"+data.tgId+"'	size='10' value="+data.tgRow+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' disabled='true' class='seatLow' name='seatLow_"+j+"_"+data.tgId+"' value="+data.tgSeatLow+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' disabled='true' class='seatHigh' name='seatHigh_"+j+"_"+data.tgId+"' value="+data.tgSeatHigh+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' disabled='true' name='qty_"+j+"_"+data.tgId+"' id='qty_"+j+"_"+data.tgId+"' class='up_qty' value="+data.tgQuantity+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' disabled='true' name='price_"+j+"_"+data.tgId+"' id='price_"+j+"_"+data.tgId+"' class='up_price' value="+data.tgPrice+" />";
				ticketGroups += "</td><td>";
				if(data.tgInstantDownload == 'Yes'){ 
					ticketGroups += "<input type='checkbox' name='instantdownload_"+j+"_"+data.tgId+"' id='instantdownload_"+j+"_"+data.tgId+"' class='instantdownload'	value='True' Checked/>True";
				}else{
					ticketGroups += "<input type='checkbox' name='instantdownload_"+j+"_"+data.tgId+"' id='instantdownload_"+j+"_"+data.tgId+"' class='instantdownload'	value='True' />True";
				}
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='file' name='file_"+j+"_"+data.tgId+"'	id='file_"+j+"_"+data.tgId+"' />";
				if(data.tgrpFilePath != null && data.tgrpFilePath != ''){
					ticketGroups += "<p class='textWrap'>";
					ticketGroups += "<a href=javascript:downloadTicketFile("+data.tgrpId+",'"+data.tgrpType+"',"+data.tgrpPosition+")>"+data.tgrpFilePath+"</a></p>";
				}
				ticketGroups += "</td>";			
				$('#upEt_'+j).html(ticketGroups);
				j++;
			}
		}
	}
	
	function uploadEticketPOValidation(){
		$("#upEticket_action").val('uploadPOInfo');
		var form = $('#upEticket_purchaseOrderForm')[0];
		var data = new FormData(form);
	
		$.ajax({
			url : "/Accounting/UploadEticket",
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			processData : false,
			contentType : false,
			cache : false,
			data : data,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#upload-eticket').modal('hide');
					getPOGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				//setUploadETicket(jsonData);
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			} 
		});		
	}
	
	function downloadTicketFile(ticketGroupId,fileType,position){
		 //var url = "${pageContext.request.contextPath}/Accounting/DownloadETicket?ticketGroupId="+ticketGroupId+"&fileType="+fileType+"&position="+position;
		 var url = apiServerUrl+"DownloadETicket?ticketGroupId="+ticketGroupId+"&fileType="+fileType+"&position="+position;
		 $('#download-frame').attr('src', url);
	}
	//End Upload Eticket
	
	//Start Create PO
	function createPO(){
		var jsonData = "";
		$.ajax({		  
			url : "${pageContext.request.contextPath}/PurchaseOrder/CreatePurchaseOrder",
			type : "post",
			//data : "poId="+poId,
			dataType:"json",
			success : function(res){
				jsonData = JSON.parse(JSON.stringify(res));
				//if(jsonData != null && jsonData != "") {
				if(jsonData.status == 1) {
					$('#create-po').modal('show');
					setValuesForCreatePO(jsonData);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End Create PO
	
	//Start Edit PO
	function editPOCall(poId){
		var jsonData = "";
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/EditPurchaseOrder",
			type : "post",
			data : "poId="+poId,
			dataType:"json",
			success : function(res){
				jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData != null && jsonData != "") {
					if(jsonData.status == 1){
						$('#edit-po').modal('show');
						setValuesForEditPO(jsonData);
						$('#poId_Hdr_EdPO').text(poId);
					}else{
						jAlert(jsonData.msg);
					}
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End Edit PO
	
	//Start Create Fedex Label
	function getShippingAddressForFedex(poId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/getShippingAddressForFedex",
			type : "post",
			data : "poId="+poId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Shipping Address Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setFedexLabelForPO(jsonData);
					$('#poId_Hdr_CreateFedex').text(poId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	
	function setFedexLabelForPO(jsonData){
		$('#create-fedex-label').modal('show');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#poFedex_successDiv').hide();
			$('#poFedex_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#poFedex_errorDiv').hide();
			$('#poFedex_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#poFedex_successDiv').show();
			$('#poFedex_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#poFedex_errorDiv').show();
			$('#poFedex_errorMsg').text(jsonData.errorMessage);
		}
		$('#poFedex_poId').val(jsonData.purchaseOrderId);
		$('#poFedex_companyName').val(jsonData.companyName);
		poFedexShippingGridValues(jsonData.shippingAddressList, jsonData.pagingInfo);
	}
	//End Create Fedex Label
	
	function clearAllSelections(){
	var custGridTable = document.getElementsByClassName('slick-cell-checkboxsel');	
	for(var j=0;j<custGridTable.length;j++){
			var eventCheckbox = custGridTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
	}
</script>
</body>
</html>
