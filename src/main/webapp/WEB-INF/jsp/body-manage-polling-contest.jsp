<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.pollingContestLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(pollingContestGrid != null && pollingContestGrid != undefined){
			pollingContestGrid.resizeCanvas();
			
		}
	});
	
	<c:choose>
		<c:when test="${pcStatus == 'ACTIVE'}">	
			$('#activePollingContest').addClass('active');
			$('#activePollingContestTab').addClass('active');
		</c:when>
		<c:when test="${pcStatus == 'PASSIVE'}">
			$('#expiredPollingContest').addClass('active');
			$('#expiredPollingContestTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activePollingContest1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredPollingContest1").click(function(){
		callTabOnChange('PASSIVE');
	});
	
	
	 $('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	
	
});

function callChangeImage(){
	$('#imageFileDiv').show();
	$('#cardImageDiv').hide();
	$('#fileRequired').val('Y');
}

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/PollingContest?pcStatus="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy pollingContest">Copy PollingContest</li>
  <li data="edit pollingContest">Edit PollingContest</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling</a></li>
			<li><i class="fa fa-laptop"></i>Manage Polling</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activePollingContestTab" class=""><a id="activePollingContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activePollingContest">Active Polling</a></li>
				<li id="expiredPollingContestTab" class=""><a id="expiredPollingContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredPollingContest">InActive Polling</a></li>
			</ul>
		</section>
	</div>	

	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activePollingContest" class="tab-pane">
			<c:if test="${pcStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="stopPollingContest()">Stop Polling</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingPrizes()">View Prizes</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="pollingContestDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Polling</label>
							<div class="pull-right">
								<a href="javascript:pollingContestExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingContestResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pollingContest_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="pollingContest_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
				
				<div class="full-width full-width-btn mb-20">
				</div>
				<div id="pollingCategory" style="position: relative;">
	
				<div class="table-responsive grid-table grid-table-2">
					<div class="grid-header full-width">
						<label>Polling Categories</label>
						<div class="pull-right">
							<a href="javascript:affiliateExportToExcel()" name='Export to Excel' style='float: right; margin-right: 10px;'>Export to Excel</a> 
							<a href="javascript:pollingCategoryResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="pollingCategoryGrid" style="width: 100%; height: 200px; overflow: auto; border: 1px solid gray;"></div>
					<div id="pollingCategory_pager" style="width: 100%; height: 20px;"></div>
				</div>
			</div>
			</c:if>				
			</div>
			<div id="expiredPollingContest" class="tab-pane">
			<c:if test="${pcStatus =='PASSIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetPollingContestModal();">Add Polling</button>					
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingContest()">Edit Polling</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPollingPrizes()">Manage Prizes</button>
					<button class="btn btn-primary" id="addQBBtn" type="button" onclick="startPollingContest();">Start Polling</button>	
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingContest()">Delete Polling</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="pollingContestDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Polling</label>
							<div class="pull-right">
								<a href="javascript:pollingContestExportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingContestResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pollingContest_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="pollingContest_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
				
				<div class="full-width full-width-btn mb-20">
				</div>
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="getCategoryGrid()">Add Category</button>				
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteCategory()">Delete Category</button>
				</div>
				<div class="table-responsive grid-table grid-table-2">
					<div class="grid-header full-width">
						<label>Polling Categories</label>
						<div class="pull-right">
							<a href="javascript:pollingCategoryExportToExcel()" name='Export to Excel' style='float: right; margin-right: 10px;'>Export to Excel</a> 
							<a href="javascript:affiliateResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="pollingCategoryGrid" style="width: 100%; height: 200px; overflow: auto; border: 1px solid gray;"></div>
					<div id="pollingCategory_pager" style="width: 100%; height: 20px;"></div>
				</div>
			</div>
			</c:if>				
			</div>
	</div>
</div>

<!--  Contest Category Grid -->


<!-- Add/Edit Polling contest  -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pollingContestModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Polling</h4>
			</div>
			<div class="modal-body full-width" >
				<form name="pollingContestForm" id="pollingContestForm" method="post">
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="pollingId" name="pollingId" />
					<input type="hidden" id="id" name="id" />
					<input type="hidden" id="pcStatus" name="pcStatus" value="${pcStatus}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>						
						<div class="form-group col-sm-12 col-xs-12">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="description" name="description">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Polling Interval<span class="required">*</span>
							</label> <input class="form-control" type="text" id="pollingInterval" name="pollingInterval">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Max Questions<span class="required">*</span></label>
							</label> <input class="form-control" type="text" id="maxQuePerCust" name="maxQuePerCust">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Start Date<span class="required">*</span></label>
							</label> <input class="form-control searchcontrol" type="text" id="startDate" name="startDate" value="${startDateStr}"> 
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>End Date<span class="required">*</span>
							</label>  <input class="form-control searchcontrol" type="text" id="endDate" name="endDate" value="${endDateStr}"> 
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="savePollingContest('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="savePollingContest('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Polling Contest end here  -->

<!-- Add/Edit Polling Prize begin here  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pollingPrizeModal" class="modal fade">
	<div class="modal-dialog modal-2g">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Polling Rewards</h4>
			</div>
			<div class="modal-body full-width" >
				<form name="pollingContestForm" id="pollingContestForm" method="post">
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="pollPrizeId" name="pollPrizeId" />
					<input type="hidden" id="pcStatus" name="pcStatus" value="${pcStatus}" />
					<div class="form-group tab-fields">
						<c:if test="${pcStatus =='PASSIVE'}">
							<div class="form-group col-sm-12 col-xs-12">
								<label>Magic Wand<span class="required">*</span>
								</label> <input class="form-control" type="text" id="eraserPerQue" name="eraserPerQue">
							</div>						
							<div class="form-group col-sm-12 col-xs-12">
								<label>Lives<span class="required">*</span>
								</label> <input class="form-control" type="text" id="lifePerQue" name="lifePerQue">
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>Superfan Stars<span class="required">*</span>
								</label> <input class="form-control" type="text" id="starsPerQue" name="starsPerQue">
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>RTF Points<span class="required">*</span>
								</label> <input class="form-control" type="text" id="rtfPointsPerQue" name="rtfPointsPerQue">
							</div>
						</c:if>
						<c:if test="${pcStatus =='ACTIVE'}">
							<div class="form-group col-sm-12 col-xs-12">
								<label>Magic Wand<span class="required"></span>
								</label> <input class="form-control" type="text" id="eraserPerQue" name="eraserPerQue" readonly>
							</div>						
							<div class="form-group col-sm-12 col-xs-12">
								<label>Lives<span class="required"></span>
								</label> <input class="form-control" type="text" id="lifePerQue" name="lifePerQue" readonly>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>Superfan Stars<span class="required"></span>
								</label> <input class="form-control" type="text" id="starsPerQue" name="starsPerQue" readonly>
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>RTF Points<span class="required"></span>
								</label> <input class="form-control" type="text" id="rtfPointsPerQue" name="rtfPointsPerQue" readonly>
							</div>
						</c:if>
					</div>
					
				</form>
			</div>
			<div class="modal-footer full-width">
				<c:if test="${pcStatus =='ACTIVE'}">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</c:if>
				<c:if test="${pcStatus =='PASSIVE'}">
					<button class="btn btn-primary" id="updateBtn" type="button" onclick="updatePollingPrize()">Update</button>
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</c:if>
			</div>
		</div>
	</div>
</div>


<!-- Add/Edit Polling contest Prize End -->

<!--  Category page begin -->
<div id="add-category" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Category(s) - Polling : <span id="pollingName_Hdr_Categoryies" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Polling Category
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Polling</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Category List</li>
						</ol>
					</div>
				</div>
				<br />
				
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Catagories</label>
							<div class="pull-right">
								<a href="javascript:pollingCategoryExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingCategoryResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="category_grid" style="width: 100%; height: 280px; border:1px solid gray"></div>
						<div id="category_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="pollingId_Category" name="pollingId_Category" />
				<button type="button" class="btn btn-primary" onclick="addCategoryForPolling()">Add Polling Category</button>				
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	<!-- category page end -->




<script type="text/javascript">

	function setPollingContestQuantityGroups(jsonData){
		var ticketGroups = "";
		$('#ei_ticketGroups').empty();
		if(jsonData != null && jsonData != ""){	
			var j=1;			
			for(var i=0; i<jsonData.length; i++){
				var data = jsonData[i];			
				$('#ei_ticketGroups').append('<tr id="addr_'+j+'"></tr>');
				ticketGroups = "";
				ticketGroups += "<td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input type='hidden' name='rowNumber' id='rowNumber_"+j+"' value='"+j+"' />";
				ticketGroups += "<input type='hidden' name='id_"+j+"' id='id_"+j+"' value="+data.id+" />";
				ticketGroups += "<input name='amount_"+j+"' id='amount_"+j+"' type='text' value="+data.amount+" placeholder='Amount' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='quantity_"+j+"' id='quantity_"+j+"' type='text' value="+data.maxQty+" placeholder='Quantity' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='maxThreshold_"+j+"' id='maxThreshold_"+j+"' type='text' value="+data.maxQtyThreshold+" placeholder='Max. Threshold' class='form-control input-md' />";
				ticketGroups += "</td>";
				$('#addr_'+j).html(ticketGroups);
				j++;
			}
		}else{
			callAddNewRow();
		}		
		$('#gcq_rowCount').val($('#tab_logic tr').length-1);
	}


	function callAddNewRow() {
		iRow = $('#tab_logic tr').length-1;
		$('#gcq_rowCount').val(iRow);
		iRow++;
		$('#tab_logic').append('<tr id="addr_'+iRow+'"></tr>');
		
		$('#addr_'+iRow).html(
		 		"<td><input id='amount_"+iRow+"'name='amount_"+iRow+"' type='text' placeholder='Amount' class='form-control input-md' /> </td> " +
			   "<td><input id='quantity_"+iRow+"' name='quantity_"+iRow+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> "+
			   "<td><input id='maxThreshold_"+iRow+"'  name='maxThreshold_"+iRow+"' type='text' placeholder='Max. Threshold'  class='form-control input-md'></td>");
		$('#gcq_rowCount').val(iRow);
	}
	
	function callDeleteRow() {
		if($('#tab_logic tr').length > 1){
			var delRow = $('#tab_logic tr').length-1;
			$("#addr_"+delRow).remove();
			delRow--;
		 }
		$('#gcq_rowCount').val(delRow);
	}


	function pagingControl(move, id) {
		if(id == 'pollingContest_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getPollingContestGridData(pageNo);
		}else if(id=='pollingCategory_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(affiliatePagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(affiliatePagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(affiliatePagingInfo.pageNum)-1;
			}		
			var id = $('#id').val();
			getPollingCategoryGridData(id,0);
		}else if(id=='category_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(affiliatePagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(affiliatePagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(affiliatePagingInfo.pageNum)-1;
			}		
			var id = $('#id').val();
			getCategoryGridData(id,0);
		}
	}
	
	
	
	//PollingContest  Grid
	
	function getPollingContestGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/PollingContest.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+pollingContestSearchString+"&pcStatus=${pcStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pollingContestPagingInfo;
				pollingContestColumnFilters = {};
				refreshPollingContestGridValues(jsonData.pollingContest);	
				//clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function pollingContestExportToExcel(status){
		var appendData = "headerFilter="+pollingContestSearchString+"&pcStatus="+pcStatus;
	    var url =apiServerUrl+"PollingContestsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function pollingContestResetFilters(){
		pollingContestSearchString='';
		sortingString ='';
		pollingContestColumnFilters = {};
		getPollingContestGridData(0);
	}
	
	/* var pollingContestCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		}); */
	
	var pagingInfo;
	var pollingContestRowIndex=-1;
	var pollingContestDataView;
	var pollingContestGrid;
	var pollingContestData = [];
	var pollingContestGridPager;
	var pollingContestSearchString='';
	var pollingCategoryPagingInfo;
	var sortingString='';
	var pollingContestColumnFilters = {};
	var userPollingContestColumnsStr = '<%=session.getAttribute("pollingcontestgrid")%>';
	var userPollingContestColumns = [];
	var allPollingContestColumns = [/* pollingContestCheckboxSelector.getColumnDefinition(), */	
			 {
				id : "pollingId",
				field : "pollingId",
				name : "Polling Id",
				width : 80,
				sortable : true
			},{
				id : "title",
				field : "title",
				name : "Title",
				width : 80,
				sortable : true
			},{
				id : "description",
				field : "description",
				name : "Description",
				width : 80,
				sortable : true
			},{
				id : "startDate",
				field : "startDate",
				name : "Start Date",
				width : 80,
				sortable : true
			},{
				id : "endDate",
				field : "endDate",
				name : "End Date",
				width : 80,
				sortable : true
			},{
				id : "pollingInterval",
				field : "pollingInterval",
				name : "Polling Interval",
				width : 80,
				sortable : true
			},{
				id : "maxQuePerCust",
				field : "maxQuePerCust",
				name : "Max Questions",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "editPollingContest",
				field : "editPollingContest",
				name : "Edit ",
				width : 80,
				formatter: editQBFormatter
			}  ];
	
	if (userPollingContestColumnsStr != 'null' && userPollingContestColumnsStr != '') {
		columnOrder = userPollingContestColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPollingContestColumns.length; j++) {
				if (columnWidth[0] == allPollingContestColumns[j].id) {
					userPollingContestColumns[i] = allPollingContestColumns[j];
					userPollingContestColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPollingContestColumns = allPollingContestColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.pollingId +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditPollingContest(id);
	});
	
	var pollingContestOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var pollingContestGridSortcol = "pollingContestId";
	var pollingContestGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function pollingContestGridComparer(a, b) {
		var x = a[pollingContestGridSortcol], y = b[pollingContestGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPollingContestGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		pollingContestData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (pollingContestData[i] = {});
				d["id"] = i;
				d["pollingId"] = data.id;
				d["title"] = data.title;				
				d["description"] = data.description;
				d["startDate"] = data.startDateStr;
				d["endDate"] = data.endDateStr;
				d["pollingInterval"] = data.pollingInterval;
				d["maxQuePerCust"] = data.maxQuePerCust;							
				d["status"] =  data.status;				
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["updatedBy"] = data.updatedBy;
				d["createdBy"] = data.createdBy;
				
			}
		}

		pollingContestDataView = new Slick.Data.DataView();
		pollingContestGrid = new Slick.Grid("#pollingContest_grid", pollingContestDataView,
				userPollingContestColumns, pollingContestOptions);
		pollingContestGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		pollingContestGrid.setSelectionModel(new Slick.RowSelectionModel());
		//pollingContestGrid.registerPlugin(pollingContestCheckboxSelector);
		
		pollingContestGridPager = new Slick.Controls.Pager(pollingContestDataView,
					pollingContestGrid, $("#pollingContest_pager"),
					pagingInfo);
		var pollingContestGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPollingContestColumns, pollingContestGrid, pollingContestOptions);
		
		
		pollingContestGrid.onSort.subscribe(function(e, args) {
			pollingContestGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(pollingContestGridSortcol) < 0){
				pollingContestGridSortdir = 'ASC';
			}else{
				if(pollingContestGridSortdir == 'DESC' ){
					pollingContestGridSortdir = 'ASC';
				}else{
					pollingContestGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+pollingContestGridSortcol+',SORTINGORDER:'+pollingContestGridSortdir+',';
			getPollingContestGridData(0);
		});
		
		// wire up model discountCodes to drive the pollingContestGrid
		pollingContestDataView.onRowCountChanged.subscribe(function(e, args) {
			pollingContestGrid.updateRowCount();
			pollingContestGrid.render();
		});
		pollingContestDataView.onRowsChanged.subscribe(function(e, args) {
			pollingContestGrid.invalidateRows(args.rows);
			pollingContestGrid.render();
		});
		$(pollingContestGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							pollingContestSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								pollingContestColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in pollingContestColumnFilters) {
										if (columnId !== undefined
												&& pollingContestColumnFilters[columnId] !== "") {
											pollingContestSearchString += columnId
													+ ":"
													+ pollingContestColumnFilters[columnId]
													+ ",";
										}
									}
									getPollingContestGridData(0);
								}
							}

						});
		pollingContestGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'editPollingContest' && args.column.id != 'delPollingContest'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(pollingContestColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(pollingContestColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		
		pollingContestGrid.onSelectedRowsChanged.subscribe(function() { 
				var temprEventRowIndex = pollingContestGrid.getSelectedRows([0])[0];
				if (temprEventRowIndex != pollingContestRowIndex) {
					pollingContestRowIndex = temprEventRowIndex;
					id = pollingContestGrid.getDataItem(temprEventRowIndex).pollingId;
					getPollingCategoryGridData(id,0);
				}
			});
		pollingContestGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		pollingContestDataView.beginUpdate();
		pollingContestDataView.setItems(pollingContestData);
		//pollingContestDataView.setFilter(filter);
		pollingContestDataView.endUpdate();
		pollingContestDataView.syncGridSelection(pollingContestGrid, true);
		pollingContestGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	
	
	
	
	function saveUserPollingContestPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = pollingContestGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('pollingcontestgrid', colStr);
	}
	
	// Add Polling Contest 	
	function resetPollingContestModal(){		
		$('#pollingContestModal').modal('show');
		$('#ei_ticketGroups').empty();
		$('#id').val('');
		$('#title').val('');
		$('#description').val('');
		$('#pollingInterval').val('');
		$('#maxQuePerCust').val('');
		//$('#cardType').val('CUSTOMER');
		//$('#imageFile').val('');
		$('#action').val('');
		$('#imageFileDiv').show();
		//$('#cardImageDiv').hide();
		//$('#fileRequired').val('Y');
		$('#startDate').val('');
		$('#endDate').val('');
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		$('#cardImageDiv').hide();	
		
	}

	function savePollingContest(action){
		var pollingId = $('#id').val();
		var title = $('#title').val();
		var description = $('#description').val();
		var pollingInterval = $('#pollingInterval').val();
		var maxQuePerCust = $('#maxQuePerCust').val();
		var imageFile = $('#imageFile').val();
		var fileRequired = $('#fileRequired').val();
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		
		if(title == ''){
			jAlert("Polling title is mandatory.");
			return;
		}
		if(description == ''){
			jAlert("Polling description is mandatory.");
			return;
		}
		if(pollingInterval == ''){
			jAlert("Polling PollingInterval is mandatory.");
			return;
		}
		if(maxQuePerCust == ''){
			jAlert("Polling MaxQustions is mandatory.");
			return;
		}
		if(startDate == ''){
			jAlert("Start Date is mandatory.");
			return;
		}
		if(endDate == ''){
			jAlert("End Date is mandatory.");
			return;
		}
		if($('#maxQuePerCust').val() == '' || isNaN($('#maxQuePerCust').val())){
			jAlert("Please Add valid numeric value for MaxQustions","Info");
			return false;
		}
		if($('#pollingInterval').val() == '' || isNaN($('#pollingInterval').val())){
			jAlert("Please Add valid numeric value for Polling Interval","Info");
			return false;
		}
		$('#action').val(action);
		var requestUrl = "${pageContext.request.contextPath}/UpdatePollingContest";
		var form = $('#pollingContestForm')[0];
		var dataString = new FormData(form);
		
		/* if(action == 'SAVE'){		
			dataString  = dataString+"&action=SAVE&pageNo=0";
		}else if(action == 'UPDATE'){
			dataString = dataString+"&action=UPDATE&pageNo=0";
		} */
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#pollingContestModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.pollingContestPagingInfo);
					pollingContestColumnFilters = {};
					refreshPollingContestGridValues(JSON.parse(jsonData.pollingContest));
					//clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	
	function editPollingPrizes(){
		
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempPollingContestRowIndex == null) {
			jAlert("Please select polling to Edit / View", "info");
			return false;
		}else {			
			var pollingPrizeId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
						
			//getEditPollingContest(pollingContestId);
			getEditPollingPrize(pollingPrizeId);
		}
	}
	
	
	
	
	
	
	//Edit PollingContest 
	function editPollingContest(){
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempPollingContestRowIndex == null) {
			jAlert("Please select Polling to Edit", "info");
			return false;
		}else {
			var pollContestId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
			getEditPollingContest(pollContestId);
		}
	}
	
	function getEditPollingPrize(pollPrizeId){
		$('#polPrizeId').val(pollPrizeId);
		$.ajax({
			url : "${pageContext.request.contextPath}/EditPollingPrizes",
			type : "post",
			dataType: "json",
			data: "contestId="+pollPrizeId+"&action=EDIT&pageNo=0&pcStatus=${pcStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				//alert(JSON.stringify(response))
				if(jsonData.status == 1){					
					$('#pollingContestModal').modal('hide');
					$('#pollingPrizeModal').modal('show');
					
					//$('#pollingContestModal').modal('show');
					//$('#cardImageDiv').show();	
					//$('#imageFileDiv').hide();
					//$('#fileRequired').val('N');
					//setEditPollingContest(JSON.parse(jsonData.card));
					//setEditPollingPrize(JSON.parse(jsonData.rewards));
					setEditPollingPrize(jsonData.rewards);
					//setPollingContestQuantityGroups(JSON.parse(jsonData.qtyList));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function updatePollingPrize(){
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempPollingContestRowIndex == null) {
			jAlert("Please select Polling to Edit", "info");
			return false;
		}else {
			var pollId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
			doUpdatePollingPrize(pollId);
		}
	}
	
	function doUpdatePollingPrize(pollId){
		var lifePerQue = $('#lifePerQue').val();
		var starsPerQue = $('#starsPerQue').val();
		var eraserPerQue = $('#eraserPerQue').val(); 
		var rtfPointsPerQue = $('#rtfPointsPerQue').val();
		var pollPrizeId = $('#pollPrizeId').val();
		if($('#eraserPerQue').val() == '' || isNaN($('#eraserPerQue').val())){
			jAlert("Please Add valid numeric value for Magic Wand","Info");
			return false;
		}
		if($('#starsPerQue').val() == '' || isNaN($('#starsPerQue').val())){
			jAlert("Please Add valid numeric value for Superfan Stars","Info");
			return false;
		}
		if($('#lifePerQue').val() == '' || isNaN($('#lifePerQue').val())){
			jAlert("Please Add valid numeric value for Lives","Info");
			return false;
		}
		if(rtfPointsPerQue == '' || isNaN(rtfPointsPerQue)){
			jAlert("Please Add valid numeric value for Rtf Points","Info");
			return false;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePollingPrizes",
			type : "post",
			dataType: "json",
			data:  "pollId="+pollId+"&eraserPerQue="+eraserPerQue+"&lifePerQue="+lifePerQue+"&starsPerQue="+starsPerQue+"&pollPrizeId="+pollPrizeId+"&action=UPDATE&rtfPointsPerQue="+rtfPointsPerQue,
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				//alert(JSON.stringify(response))
				if(jsonData.status == 1){					
					//$('#pollingContestModal').modal('hide');
					$('#pollingPrizeModal').modal('hide');
					//$('#pollingContestModal').modal('show');
					//$('#cardImageDiv').show();	
					//$('#imageFileDiv').hide();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function getEditPollingContest(pollingContestId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePollingContest",
			type : "post",
			dataType: "json",
			data: "id="+pollingContestId+"&action=EDIT&pageNo=0&gcStatus=${pcStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#pollingContestModal').modal('show');
					setEditPollingContest(JSON.parse(jsonData.pollContest));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditPollingContest(pollingContest){
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = pollingContest;
		$('#pollingId').val(data.id);
		$('#title').val(data.title);
		$('#description').val(data.description);
		$('#pollingInterval').val(data.pollingInterval);
		$('#maxQuePerCust').val(data.maxQuePerCust);
		$('#startDate').val(data.startDateStr);
		$('#endDate').val(data.endDateStr);
	}
	
	function setEditPollingPrize(rewards){
		//$('#saveBtn').hide();
		//$('#updateBtn').show();
		var data = rewards;
		
		//alert(data.id   + data.contestId);
		$('#pollPrizeId').val(data.id);
		//$('#contestId').val(data.contestId);
		$('#lifePerQue').val(data.lifePerQue);
		$('#starsPerQue').val(data.starsPerQue);
		/* $('#rdPerQue').val(data.rdPerQue);
		$('#ticketsPerQue').val(data.ticketsPerQue);
		$('#gcPerQue').val(data.gcPerQue); */
		$('#eraserPerQue').val(data.eraserPerQue);
		$('#rtfPointsPerQue').val(data.rtfPointsPerQue);
		/* $('#gbricksPerQue').val(data.gbricksPerQue);
		$('#hifivePerQue').val(data.hifivePerQue);
		$('#crstballPerQue').val(data.crstballPerQue); */
		
		/* $('#maxlifePerContest').val(data.maxlifePerContest);
		$('#maxstarsPerContest').val(data.maxstarsPerContest);
		$('#maxrdPerContest').val(data.maxrdPmaxgcPerContesterContest);
		$('#maxticketsPerContest').val(data.maxticketsPerContest);
		$('#maxgcPerContest').val(data.maxgcPerContest);
		$('#maxeraserPerContest').val(data.maxeraserPerContest);
		$('#maxgbricksPerContest').val(data.maxgbricksPerContest);
		$('#maxhifivePerContest').val(data.maxhifivePerContest);
		$('#maxcrstballPerContest').val(data.maxcrstballPerContest);


		$('#maxlifePerCustPerDay').val(data.maxlifePerCustPerDay);
		$('#maxstarsPerCustPerDay').val(data.maxstarsPerCustPerDay);
		$('#maxrdPerCustPerDay').val(data.maxrdPerCustPerDay);
		$('#maxticketsPerCustPerDay').val(data.maxticketsPerCustPerDay);
		$('#maxgcPerCustPerDay').val(data.maxgcPerCustPerDay);
		$('#maxeraserPerCustPerDay').val(data.maxeraserPerCustPerDay);
		$('#maxgbricksPerCustPerDay').val(data.maxgbricksPerCustPerDay);
		$('#maxhifivePerCustPerDay').val(data.maxhifivePerCustPerDay);
		$('#maxcrstballPerCustPerDay').val(data.maxcrstballPerCustPerDay); */
	}
	
	
	//Delete PollingContest 
	function deletePollingContest(){
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempPollingContestRowIndex == null) {
			jAlert("Please select PollingContest to Delete", "info");
			return false;
		}else {
			var pollContestId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
			getDeletePollingContest(pollContestId);
		}
	}
	
	
	function getDeletePollingContest(pollingContestIds){
		jConfirm("Are you sure to delete selected polling ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingContest",
						type : "post",
						dataType: "json",
						data : "id="+pollingContestIds+"&action=DELETE&pcStatus=${pcStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.pollingContestPagingInfo);
								pollingContestColumnFilters = {};
								refreshPollingContestGridValues(JSON.parse(jsonData.pollingContest));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	// Start Stop Polling Contest
	
	function startPollingContest(){
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempPollingContestRowIndex == null) {
			jAlert("Please select Polling to Start", "info");
			return false;
		}else {
			var pollContestId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
			var maxQuePerCust = pollingContestGrid.getDataItem(tempPollingContestRowIndex).maxQuePerCust;
			var pollingInterval = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingInterval;
			getStartStopPollingContest(pollContestId,"START",maxQuePerCust,pollingInterval);
		}
	}
	function stopPollingContest(){
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempPollingContestRowIndex == null) {
			jAlert("Please select Polling to Stop", "info");
			return false;
		}else {
			var pollContestId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
			getStartStopPollingContest(pollContestId,"STOP", null,null);
		}
	}
	
	function getStartStopPollingContest(pollContestId,action,maxQuePerCust,pollingInterval){		
		jConfirm("Please Click OK to " + action + " Polling ","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingContest",
						type : "post",
						dataType: "json",
						data : "id="+pollContestId+"&action="+action+"&maxQuePerCust="+maxQuePerCust+"&pcStatus=${pcStatus}"+"&pollingInterval="+pollingInterval,
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.pollingContestPagingInfo);
								pollingContestColumnFilters = {};
								refreshPollingContestGridValues(JSON.parse(jsonData.pollingContest));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	// polling category  begine
	
	//Polling Category Order Grid
var pollingCategoryPagingInfo;
var categoryId='';
var pollingCategoryGrid;
var eventrowIndex;
var pollingCategoryDataView;
var pollingCategoryData=[];
var pollingCategoryGridSearchString='';
var pollingCategoryColumnFilters = {};
var pollingCategoryColumnsStr = '<%=session.getAttribute("pollingCategoryGrid")%>';
var pollingCategoryColumns =[];
var loadpollingCategoryColumns = ["categoryId", "categoryName", "pollingType", "sponsorName"];
var pollingCategoryColumns = [
				{id:"categoryId", name:"Category Id", field: "categoryId",width:80, sortable: true},         
               {id:"categoryName", name:"Category Name", field: "categoryName",width:80, sortable: true},
               {id:"pollingType", name:"Polling Type", field: "pollingType",width:80, sortable: true},
               {id:"sponsorName", name:"Sponsor Name", field: "sponsorName",width:80, sortable: true}
              /*  {id:"eventDate", name:"Event Date", field: "eventDate",width:80, sortable: true},
               {id:"eventTime", name:"Event Time", field: "eventTime",width:80, sortable: true},
               {id:"ticketQty", name:"Quantity", field: "ticketQty",width:80, sortable: true},
               {id:"orderTotal", name:"Order Total", field: "orderTotal",width:80, sortable: true},
               {id:"cashCredit", name:"Affiliate Fee", field: "cashCredit",width:80, sortable: true},
               {id:"status", name:"Status", field: "status",width:80, sortable: true} */
              ];
  
	if(pollingCategoryColumnsStr!='null' && pollingCategoryColumnsStr!=''){
		var columnOrder = pollingCategoryColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<pollingCategoryColumns.length;j++){
				if(columnWidth[0] == pollingCategoryColumns[j].id){
					pollingCategoryColumns[i] =  pollingCategoryColumns[j];
					pollingCategoryColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadpollingCategoryColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<pollingCategoryColumns.length;j++){
				if(columnWidth == pollingCategoryColumns[j].id){
					pollingCategoryColumns[i] = pollingCategoryColumns[j];
					pollingCategoryColumns[i].width=80;
					break;
				}
			}			
		}
	}
  
var pollingCategoryOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var pollingCategorySortCol = "categoryId";
var pollingCategorySortdir = 1;
var percentCompleteThreshold = 0;

function pollingCategoryComparer(a, b) {
	  var x = a[pollingCategorySortCol], y = b[pollingCategorySortCol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	  	if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}


	
function getPollingCategoryGridData(pollId, pageNo){
		eventrowIndex=-1;
		$.ajax({
			url : "${pageContext.request.contextPath}/GetPollingCategoryByContId",
			type : "post",
			dataType: "json",
			data: "pollId="+pollId+"&pageNo="+pageNo+"&pcStatus=${pcStatus}"+"&headerFilter="+pollingCategoryGridSearchString,
		success : function(res){
			var jsonData = res;
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				var msg =  jsonData.msg.replace(/"/g, '');
				if(msg != null && msg != ""){
					jAlert(msg);
				}
			}
			/* if(jsonData==null || jsonData=='' || jsonData.pollingCategory.length==0) {
				jAlert("No Data Found.");
			} */
			pollingCategoryPagingInfo = jsonData.pollingCategoryPagingInfo;
			refreshPollingCategoryGridValues(jsonData.pollingCategory);
			$('#id').val(id);
			//$('#pollingCategory').show();
			clearAllSelections();
			$('#pollingCategory_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
function refreshPollingCategoryGridValues(jsonData) {
	pollingCategoryData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (pollingCategoryData[i] = {});			
			d["id"] = i;
			d["categoryId"] = data.categoryId;
			d["categoryName"] = data.categoryName;
			d["pollingType"] = data.pollingType;
			d["sponsorName"] = data.sponsorName;
			/*d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
			 d["paymentNote"] = data.paymentNote; */
		}
	}
	pollingCategoryDataView = new Slick.Data.DataView();
	pollingCategoryGrid = new Slick.Grid("#pollingCategoryGrid", pollingCategoryDataView, pollingCategoryColumns, pollingCategoryOptions);
	pollingCategoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = pollingCategoryGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  pollingCategoryGrid.setColumns(colTest);
	  }
	  pollingCategoryGrid.invalidate();
	  pollingCategoryGrid.setSelectionModel(new Slick.RowSelectionModel());
	  /* if(pollingCategoryPagingInfo!=null){
		  var affiliatePager = new Slick.Controls.Pager(pollingCategoryDataView, pollingCategoryGrid, $("#pollingCategory_pager"),pollingCategoryPagingInfo);
	  } */
	  var columnpicker = new Slick.Controls.ColumnPicker(pollingCategoryColumns,pollingCategoryGrid, pollingCategoryOptions);
	 
	  pollingCategoryGrid.onSort.subscribe(function (e, args) {
	    pollingCategorySortdir = args.sortAsc ? 1 : -1;
	    pollingCategorySortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      pollingCategoryDataView.fastSort(pollingCategorySortCol, args.sortAsc);
	    } else {
	      pollingCategoryDataView.sort(pollingCategoryComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the pollingCategoryGrid
	  pollingCategoryDataView.onRowCountChanged.subscribe(function (e, args) {
	    pollingCategoryGrid.updateRowCount();
	    pollingCategoryGrid.render();
	  });
	  pollingCategoryDataView.onRowsChanged.subscribe(function (e, args) {
	    pollingCategoryGrid.invalidateRows(args.rows);
	    pollingCategoryGrid.render();
	  });
	  
	  $(pollingCategoryGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	pollingCategoryGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				pollingCategoryColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in pollingCategoryColumnFilters) {
					  if (columnId !== undefined && pollingCategoryColumnFilters[columnId] !== "") {
						  pollingCategoryGridSearchString += columnId + ":" +pollingCategoryColumnFilters[columnId]+",";
					  }
					}
					var id = $('#id').val();
					getPollingCategoryGridData(id,0);
				}
			  }
		 
		});
	  	pollingCategoryGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(pollingCategoryColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(pollingCategoryColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id != 'delCol' && args.column.id != 'payOrder'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(pollingCategoryColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		pollingCategoryGrid.init();
		
		pollingCategoryGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = pollingCategoryGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				categoryId = pollingCategoryGrid.getDataItem(temprEventRowIndex).categoryId;
			}
		});
	  	
		pollingCategoryGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = pollingCategoryGrid.getCellFromEvent(e);
		      pollingCategoryGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY;
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
	  pollingCategoryDataView.beginUpdate();
	  pollingCategoryDataView.setItems(pollingCategoryData);
	  
	  pollingCategoryDataView.endUpdate();
	  pollingCategoryDataView.syncGridSelection(pollingCategoryGrid, true);
	  pollingCategoryDataView.refresh();
	  $("#gridContainer").resizable();
	  pollingCategoryGrid.resizeCanvas();
}	

$("#contextMenu").click(function (e) {
	if (!$(e.target).is("li")) {
	  return;
	}
	var index = pollingCategoryGrid.getSelectedRows([0])[0];
	if(index>=0){
		var id = $('#id').val();
		var categoryId = pollingCategoryGrid.getDataItem(index).categoryId;
		if ($(e.target).attr("data") == 'view order') {
			if(categoryId>0){				
				getAffiliateOrderSummary(id, categoryId);
				/*var url = "${pageContext.request.contextPath}/AffiliateOrderDetails?id="+id+"&categoryId="+ categoryId;
				popupCenter(url, "View Order", "1000","600");*/
			}else{
				jAlert("Invoice is not Created yet for selected Order.");
			}
		}else if($(e.target).attr("data") == 'view modify notes'){
			getAffiliatePaymentNote(id, categoryId);
		}
	}else{
		jAlert("Please select Order to view details.");
	}
});

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    } 
} 

	// Category page begin
	function getCategoryGrid(){
		var tempContestGridRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		if (tempContestGridRowIndex == null) {
			jAlert("Please select polling to Add Category", "info");
			return false;
		}else {
			var pollId = pollingContestGrid.getDataItem(tempContestGridRowIndex).pollingId;
			var title = pollingContestGrid.getDataItem(tempContestGridRowIndex).title;
			$('#pollingName_Hdr_Categoryies').text(title);
			$('#pollingId_Category').val(id);
			$('#add-category').modal('show');
			categoryColumnFilters = {};
			getCategoryGridData(pollId,0);
		}		
	}
	
	function getCategoryGridData(pollId,pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetPollingCategorySelection.json",
			type : "post",
			dataType: "json",
			data: "pollId="+pollId+"&pageNo="+pageNo+"&pcStatus=${pcStatus}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				categoryPagingInfo = jsonData.categoryPagingInfo;
				refreshCategoryGridValues(jsonData.category);clearAllSelections();
				$('#contestQuesBank_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestQuesBankPreference()'>");
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function pollingCategoryResetFilters(){
		categorySearchString='';
		categoryColumnFilters = {};
		var contestId = $('#pollingId_Category').val();
		getCategoryGridData(0, contestId);
	}
	
	var categoryCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var categoryPagingInfo;
	var categoryDataView;
	var categoryGrid;
	var categoryData = [];
	var categoryGridPager;
	var categorySearchString='';
	var categoryColumnFilters = {};
	var categoryColumnsStr = '<%=session.getAttribute("categoryGrid")%>';

	var userContestQuesBankColumns = [];
	var categoryColumns = [categoryCheckboxSelector.getColumnDefinition(),
				{id:"categoryId", name:"Category Id", field: "categoryId",width:80, sortable: true},         
               {id:"categoryName", name:"Category Name", field: "categoryName",width:80, sortable: true},
               {id:"pollingType", name:"Polling Type", field: "pollingType",width:80, sortable: true},
               {id:"sponsorName", name:"Sponsor Name", field: "sponsorName",width:80, sortable: true},
               /*{id:"selected", name:"Sponsor Name", field: "selected",width:80, sortable: true}
               {id:"eventDate", name:"Event Date", field: "eventDate",width:80, sortable: true} */
              ];

	if (categoryColumnsStr != 'null' && categoryColumnsStr != '') {
		columnOrder = categoryColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < categoryColumns.length; j++) {
				if (columnWidth[0] == categoryColumns[j].id) {
					userContestQuesBankColumns[i] = categoryColumns[j];
					userContestQuesBankColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestQuesBankColumns = categoryColumns;
	}
	
	var contestQuesBankOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var categoryGridSortcol = "categoryd";
	var categoryGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function categoryGridComparer(a, b) {
		var x = a[categoryGridSortcol], y = b[categoryGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshCategoryGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		categoryData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (categoryData[i] = {});
				d["id"] = i;
				d["categoryId"] = data.categoryId;
				d["categoryName"] = data.categoryName;
				d["pollingType"] = data.pollingType;
				d["sponsorName"] = data.sponsorName;
				d["selected"] = data.selected;
				//d["status"] = data.status;
				//d["category"] = data.category;
			}
		}

		categoryDataView = new Slick.Data.DataView();
		categoryGrid = new Slick.Grid("#category_grid", categoryDataView,
				userContestQuesBankColumns, contestQuesBankOptions);
		categoryGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		categoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		categoryGrid.registerPlugin(categoryCheckboxSelector);
		
		categoryGridPager = new Slick.Controls.Pager(categoryDataView,
					categoryGrid, $("#contestQuesBank_pager"),
					categoryPagingInfo);
		var categoryGridColumnpicker = new Slick.Controls.ColumnPicker(
				categoryColumns, categoryGrid, contestQuesBankOptions);
		
		categoryGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = categoryGrid.getCellFromEvent(e);
		      categoryGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu1").height()){
					height = e.pageY - $("#contextMenu1").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu1").width()){
					width =  e.pageX- $("#contextMenu1").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu1")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu1").hide();
		      });
		    });
		
		$("#contextMenu1").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
		});
			
		categoryGrid.onSort.subscribe(function(e, args) {
			categoryGridSortdir = args.sortAsc ? 1 : -1;
			categoryGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				categoryDataView.fastSort(categoryGridSortcol, args.sortAsc);
			} else {
				categoryDataView.sort(categoryGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the categoryGrid
		categoryDataView.onRowCountChanged.subscribe(function(e, args) {
			categoryGrid.updateRowCount();
			categoryGrid.render();
		});
		categoryDataView.onRowsChanged.subscribe(function(e, args) {
			categoryGrid.invalidateRows(args.rows);
			categoryGrid.render();
		});
		$(categoryGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							categorySearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								categoryColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in categoryColumnFilters) {
										if (columnId !== undefined
												&& categoryColumnFilters[columnId] !== "") {
											categorySearchString += columnId
													+ ":"
													+ categoryColumnFilters[columnId]
													+ ",";
										}
									}
									getCategoryGridData(0);
								}
							}

						});
		categoryGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {				
				$("<input type='text'>").data("columnId", args.column.id)
						.val(categoryColumnFilters[args.column.id]).appendTo(
								args.node);				
			}
		});
		categoryGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		categoryDataView.beginUpdate();
		categoryDataView.setItems(categoryData);
		//categoryDataView.setFilter(filter);
		categoryDataView.endUpdate();
		categoryDataView.syncGridSelection(categoryGrid, true);
		//categoryGrid.resizeCanvas();
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
	}
	
	function addCategoryForPolling(){
		var pollingId = $('#pollingId_Category').val();
		var ids = getSelectedCategoryGridId();
		if(ids==null || ids==''){
			jAlert("Please select atleast one Category(s) to add to Polling");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdatePollingCategoryMapper",
				type : "post",
				dataType : "json",
				data : "pollId="+pollingId+"&categoryIds="+ids+"&action=ADDNEW",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#add-category').modal('hide');
						pollingCategoryPagingInfo = jsonData.pollingCategoryPagingInfo;
						refreshPollingCategoryGridValues(jsonData.pollingCategory);
						//questionColumnFilters = {};
						clearAllSelections();
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function getSelectedCategoryGridId() {
		var tempPollingCategoryRowIndex = categoryGrid.getSelectedRows();
		
		var categoryIdStr='';
		$.each(tempPollingCategoryRowIndex, function (index, value) {
			categoryIdStr += ','+categoryGrid.getDataItem(value).categoryId;
		});
		
		if(categoryIdStr != null && categoryIdStr!='') {
			categoryIdStr = categoryIdStr.substring(1, categoryIdStr.length);
			 return categoryIdStr;
		}
	}
	
	function deleteCategoryFromMapper(pollingId, categoryId){
		var pollId = pollingId;
		var ids = categoryId;
		if(ids==null || ids==''){
			jAlert("Please select atleast one Category(s) to delete");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdatePollingCategoryMapper",
				type : "post",
				dataType : "json",
				data : "pollId="+pollId+"&categoryIds="+ids+"&action=DELETE",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#add-category').modal('hide');
						pollingCategoryPagingInfo = jsonData.pollingCategoryPagingInfo;
						refreshPollingCategoryGridValues(jsonData.pollingCategory);
						//questionColumnFilters = {};
						clearAllSelections();
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	function deleteCategory(){
		if(pollingCategoryGrid == null){
			jAlert("Select polling to List Category ", "info");
			return false;
		}
		var tempPollingContestRowIndex = pollingContestGrid.getSelectedRows([0])[0];
		var tempPollingCategoryRowIndex = pollingCategoryGrid.getSelectedRows([0])[0];
		if (tempPollingCategoryRowIndex == null) {
			jAlert("Plese select polling Category to Delete", "info");
			return false;
		}else {			
			var categoryId = pollingCategoryGrid.getDataItem(tempPollingCategoryRowIndex).categoryId;
			var pollingId = pollingContestGrid.getDataItem(tempPollingContestRowIndex).pollingId;
			deleteCategoryFromMapper(pollingId, categoryId);
		}
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pollingContestPagingInfo};
		refreshPollingContestGridValues(${pollingContest});
		$('#pollingContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingContestPreference()'>");
		
		enableMenu();
	};
		
</script>