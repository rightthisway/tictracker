<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<!-- full calendar css-->
<link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
<!-- easy pie chart-->
<link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
<!-- owl carousel -->
<link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link rel="stylesheet" href="../resources/css/fullcalendar.css">
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<link href="../resources/css/xcharts.min.css" rel=" stylesheet">
<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

<!-- javascripts -->
<%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
<!-- bootstrap -->
<script src="../resources/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="../resources/js/jquery.scrollTo.min.js"></script>
<script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/jquery.alerts.js"></script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
	Stripe.setPublishableKey('${sPubKey}');
</script>


<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker.css" type="text/css" />

<!-- slickgrid related scripts -->
<script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

.slick-viewport {
	overflow-x: hidden !important;
}
/* .light_box {
	display: none;
	position: absolute;
	top: 10%;
	left: 25%;
	width: 45%;
	height: 100%;
	padding: 20px;
	border: 1px solid #888;
	background-color: #fefefe;
	z-index: 1;
	overflow: auto;
} */
#addCustomer {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

#totalPrice {
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
}

#totalQty {
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
}
</style>

<script>
function createOrder() {
	var customerId = getSelectedCustomerId();	
	var grandChild = $("#grandChildType").val();
	
	if (customerId <= 0) {
		jAlert("Please select Customer.");
		return false;
	}
	/*
	var parentType = $("#parentType").val();
	var leagueId = $("#fantasyEvent").val();
	var teamId = $('#fantasyTeam').val();	
	var childType = $("#childType").val();
	
	if (parentType == '' || parentType == null) {
		jAlert("Please select Parent Type.");
		return false;
	}
	if (childType < 0) {
		jAlert("Please select Child Category.");
		return false;
	}
	if (leagueId < 0) {
		jAlert("Please select Event.");
		return false;
	}
	if (teamId < 0) {
		jAlert("Please select Team.");
		return false;
	}*/
	if (grandChild < 0) {
		jAlert("Please select Grand Child.");
		return false;
	}
	var index = getSelectedTicketRowId();
	if (index < 0) {
		jAlert("Please select Ticket.");
		return false;
	}
	var ticketId = ticketGrid.getDataItem(index).id;
	var fantasyEventId = ticketGrid.getDataItem(index).fantasyEventId;
	var fantasyTeamId = ticketGrid.getDataItem(index).teamId;
	var fantasyTeamZoneId = ticketGrid.getDataItem(index).teamZoneId;
	var isRealTicket = ticketGrid.getDataItem(index).isRealTicket;
	var zone = ticketGrid.getDataItem(index).section;
	var quantity = ticketGrid.getDataItem(index).quantity;
	var requiredPoints = ticketGrid.getDataItem(index).requiredPointsAsDouble;
		
	$.ajax({
		url : "${pageContext.request.contextPath}/GetFantasyTicketValidation",
		type : "post",
		data : "customerId=" + customerId + "&grandChildCategoryId=" + grandChild
				+ "&fantasyEventId=" + fantasyEventId +"&fantasyTeamId="+fantasyTeamId
				+ "&fantasyTicketId="+ticketId+"&fantasyTeamZoneId="+fantasyTeamZoneId
				+ "&isRealTicket="+isRealTicket+"&zone="+zone
				+ "&quantity="+quantity+"&requiredPoints="+requiredPoints,
		dataType : "json",
		success : function(res) {
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.proceedPurchase == 'Yes'){
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){									
					jConfirm(jsonData.msg,"Confirm", function(r) {
					if (r) {
							createOrderAPICall();
						}
					});
				}
			}else{
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}
		},
		error : function(error) {
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
	
	/*var packageAllowed  = ticketGrid.getDataItem(index).packageAllowed;
	var customerPoints = parseFloat($('#customerRewardPoints').val());
	if(packageAllowed == 'Yes'){
		requiredPoints = parseFloat(ticketGrid.getDataItem(index).packageCostAsDouble);
	}else{
		requiredPoints = parseFloat(ticketGrid.getDataItem(index).requiredPointsAsDouble);
	}
	 
	if(requiredPoints > customerPoints){
		jAlert("Selected Customer have only "+customerPoints+" active reward points, Require Points are "+requiredPoints);
		return false;
	}
	

	$("#ticketId").val(ticketId);
	$("#qty").val(quantity);
	$("#isRealTicket").val(isRealTicket);
	$("#packageOption").val(packageAllowed);
	$("#customerId").val(customerId);
	$("#action").val('createOrder');
	$("#createOrderForm").submit();*/
}

	function createOrderAPICall(){
		var customerId = getSelectedCustomerId();
		var grandChild = $("#grandChildType").val();
		var index = getSelectedTicketRowId();
		if (index < 0) {
			jAlert("Please select Ticket.");
			return false;
		}
		var ticketId = ticketGrid.getDataItem(index).id;
		var fantasyEventId = ticketGrid.getDataItem(index).fantasyEventId;
		var fantasyTeamId = ticketGrid.getDataItem(index).teamId;
		var fantasyTeamZoneId = ticketGrid.getDataItem(index).teamZoneId;
		var isRealTicket = ticketGrid.getDataItem(index).isRealTicket;
		var zone = ticketGrid.getDataItem(index).section;
		var quantity = ticketGrid.getDataItem(index).quantity;
		var requiredPoints = ticketGrid.getDataItem(index).requiredPointsAsDouble;
		
		$.ajax({
			url : "${pageContext.request.contextPath}/CrownJewelEvents/FantasyOrderCreation",
			type : "post",
			data : "customerId=" + customerId + "&grandChildCategoryId=" + grandChild
					+ "&fantasyEventId=" + fantasyEventId +"&fantasyTeamId="+fantasyTeamId
					+ "&fantasyTicketId="+ticketId+"&fantasyTeamZoneId="+fantasyTeamZoneId
					+ "&isRealTicket="+isRealTicket+"&zone="+zone
					+ "&quantity="+quantity+"&requiredPoints="+requiredPoints,
			dataType : "json",
			success : function(res) {
				var jsonData = JSON.parse(JSON.stringify(res));				
				if(jsonData.orderNo != '' && jsonData.orderNo != null){
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg+". Order No: "+jsonData.orderNo);
					}					
				}else{
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Invoice cancel action
	function cancelAction() {
		window.close();
	}

	function editAddr(id) {
		jAlert('edit action' + id);
		var currentTD = $(this).parents('tr').find('td');
		if ($(this).html() == 'Edit') {
			currentTD = $(this).parents('tr').find('td');
			$.each(currentTD, function() {
				$(this).prop('contenteditable', true);
			});
		} else {
			$.each(currentTD, function() {
				$(this).prop('contenteditable', false);
			});
		}

		$(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit');

	}

	$(document).ready(function() {
		$('#childDiv').hide();
		$('#grandChildDiv').hide();
		$('#eventDiv').hide();
		$('#teamDiv').hide();
		getCustomerGridData(0);
		//loadChild();
		$('#searchValue').keypress(function(event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			if (keyCode == 13) // the enter key code
			{
				getCustomerGridData(0);
				return false;
			}
		});

		$(window).resize(function() {
			customerGrid.resizeCanvas();
		});
		
		window.onunload = function () {
			var win = window.opener;
			if (!win.closed) {			
				window.opener.getCrownJewelOrderGridData(0);			
			}
		};
		
	});

	/* window.onunload = function () {
	    var win = window.opener;
	    if (!win.closed) {
	    	window.opener.searchTicketData();
	    }
	}; */

	/* function addNewCustomer() {
		popupCenter(
				"${pageContext.request.contextPath}/Client/AddCustomerPopup",
				"Add Customer", "1000", "1000");
	} */

	function resetFilters() {
		customerGridSearchString = '';
		columnFilters = {};
		getCustomerGridData(0);
	}
</script>


<div class="row">
	<div class="col-lg-12">

		<ol class="breadcrumb">
			<li><i class="fa fa-laptop"></i>Create Fantasy Order</li>
		</ol>

	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>

		<!-- <div class="form-group">
			<div class="col-sm-3" style="float: right;">
				<button type="button" class="btn btn-primary" style="float: right; margin-top: -25px;" onclick="addNewCustomer();">Add New Customer</button>
			</div>
		</div> -->
		<br />
	<div style="position: relative;" id="customerGridDiv">
		<div style="width: 100%;">
			<div class="grid-header" style="width: 100%">
				<label style="font-size: 10px;">Customers Details</label>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
			</div>
			<div id="customer_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
			<div id="customer_pager" style="width: 100%; height: 10px;"></div>
		</div>
	</div>
	</div>
</div>

	<br /> <br />
	<form class="form-horizontal" id="createOrderForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/CreateFantasyOrder">
		<!-- <div class="row">
			<div class="col-lg-12">
				<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading"> Customer Billing Details </header>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Customer Name:</label> <span style="font-size: 12px;" id="customerNameDiv"></span> <input type="hidden" name="shFirstName" id="shFirstName"
								value="" /> <input type="hidden" name="shLastName" id="shLastName" value="" /> <input type="hidden" name="blFirstName" id="blFirstName" value="" /> <input type="hidden" name="blLastName"
								id="blLastName" value="" />


						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Email:</label> <input type="hidden" name="blEmail" id="blEmail" value="" /> <span id=emailDiv></span>
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Phone:</label><span id=phoneDiv></span> <input type="hidden" name="phone" id="phone" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Address Line:</label><span id=addressLineDvi></span> <input type="hidden" name="addressLine1" id="addressLine1" value="" /> <input
								type="hidden" name="addressLine2" id="addressLine2" value="" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Country:</label><span id=countryDiv></span> <input type="hidden" name="country" id="country" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">State:</label><span id=stateDiv></span> <input type="hidden" name="state" id="state" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">City:</label><span id=cityDiv></span> <input type="hidden" name="city" id="city" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">ZipCode:</label><span id=zipCodeDiv></span> <input type="hidden" name="zipCode" id="zipCode" value="" />
						</div>
					</div>

				</div>
				</section>
			</div>
		</div>

		Shipping Address section
		<div class="row">
			<div class="col-lg-12">
				<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">Shipping Address &nbsp;&nbsp;&nbsp;&nbsp; <input name="isBilling"
					id="isBilling" value="" type="checkbox" onclick="copyBillingAddress();" /> Same as billing address &nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:showShippingAddress();" id="showShAddr"
					style="font-size: 13px; font-family: arial;" />View/Change Shipping Address</a> </header>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Address Line 1:</label><span id=shAddressLine1Div></span> <input type="hidden" name="shAddressLine1" id="shAddressLine1" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Address Line 2:</label><span id=shAddressLine2Div></span> <input type="hidden" name="shAddressLine2" id="shAddressLine2" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Phone:</label><span id=shPhoneDiv></span> <input type="hidden" name="shPhone" id="shPhone" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Country:</label><span id=shCountryDiv></span> <input type="hidden" name="shCountry" id="shCountry" value="" />
						</div>
					</div>
					<div class="form-group">

						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">State:</label><span id=shStateDiv></span> <input type="hidden" name="shState" id="shState" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">City:</label><span id=shCityDiv></span> <input type="hidden" name="shCity" id="shCity" value="" />
						</div>
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">ZipCode:</label><span id=shZipCodeDiv></span> <input type="hidden" name="shZipCode" id="shZipCode" value="" />
						</div>
					</div>
				</div>
				</section>
			</div>
		</div> -->
		<div class="row">
			<div class="col-lg-12">
				<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> Fantasy Ticket </header>
				<div class="panel-body">
					<input type="hidden" name="shippingIsSameAsBilling" id="shippingIsSameAsBilling" value="No" />
					<div class="form-group">
						<div class="col-sm-3">
							<label style="font-size: 13px; font-weight: bold;">Active Reward Points : </label> 
							<span id=activeRewardPoints></span> 
							<input type="hidden" id="customerRewardPoints" name="customerRewardPoints" />
						</div>
						<div class="col-sm-3">
							<label>Parent Type : SPORTS</label> 
						</div>
					</div>
					<div class="form-group">						
						<%-- <select name="parentType" id="parentType" class="form-control" onchange="loadChild();">
							<option value="">--- Select ---</option>
							<c:forEach items="${parentCategoryList}" var="parentCat">
								<option value="${parentCat.name}" <c:if test="${parentType eq parentCat.name}"> selected </c:if>>${parentCat.name}</option>
							</c:forEach>
						</select> --%>
						<div class="col-sm-3" id="childDiv">
							<label>Child Type</label> 
							<select name="childType" id="childType" class="form-control" onchange="loadGrandChild();">
							</select>
						</div>
						<div class="col-sm-3" id="grandChildDiv">
							<label>Grand Child Type</label> 
							<select name="grandChildType" id="grandChildType" class="form-control" onchange="loadEvents();">
							</select>
						</div>
						<div class="col-sm-3" id="eventDiv">
							<label>Fantasy Event</label> 
							<select name="fantasyEvent" id="fantasyEvent" class="form-control" onchange="loadTeams();">
							</select>
						</div>
						<div class="col-sm-3" id="teamDiv">
							<label>Fantasy Team</label> 
							<select name="fantasyTeam" id="fantasyTeam" class="form-control" onchange="loadTickets();">
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12 full-width">
							<div id="packageInfoDiv"></div>
						</div>
					</div>
					<input type="hidden" id="qty" name="qty" /> 
					<input type="hidden" id="ticketId" name="ticketId" /> 
					<input type="hidden" id="isRealTicket" name="isRealTicket" /> 
					<input type="hidden" id="packageOption" name="packageOption" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="customerId" name="customerId" />

					<div style="position: relative;" id="ticketGrid">
						<div style="width: 100%;">
							<div class="grid-header" style="width: 100%">
								<label style="font-size: 10px;">Fantasy Sports Tickets</label>
								<!-- <a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a> -->
							</div>
							<div id="ticket_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
							<div id="ticket_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					<br />
					<br />
					<div class="form-group">
						<div id="POAction" class="col-sm-6">
							<button type="button" id="createFantasyOrder" onclick="createOrder();" class="btn btn-primary btn-sm">Save</button>
							<button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
						</div>
					</div>
				</div>
				</section>
			</div>
		</div>
	</form>

	<!-- Show the selected customer info -->
	<br /> <br /> <br />
	
	<!-- popup View Customer Details -->
		<%@include file="body-view-customer-details.jsp"%>
	<!-- End popup View Customer Details -->

	<script>
		
		/*function loadChild() {
			var searchValue = 'SPORTS';
			if (searchValue != '') {
				$
						.ajax({
							url : "${pageContext.request.contextPath}/GetFantasyChildCategories",
							type : "post",
							data : "parentCategoryName=" + searchValue,
							dataType : "json",
							success : function(res) {
								$('#childDiv').show();
								var jsonData = JSON.parse(JSON.stringify(res));
								var select = document
										.getElementById('childType');
								$('#childType').empty();
								 $("#grandChildType").empty();
								 $("#fantasyEvent").empty();
								 $('#fantasyTeam').empty();
								var opt = document.createElement('option');
								opt.value = '-1';
								opt.innerHTML = '--select--';
								select.appendChild(opt);
								if (jsonData != '' && jsonData.length > 0) {
									for ( var i = 0; i < jsonData.length; i++) {
										var opt = document
												.createElement('option');
										opt.value = jsonData[i].id;
										opt.innerHTML = jsonData[i].name;
										select.appendChild(opt);
									}
								}
								refreshTicketGridValues(null);

							},
							error : function(error) {
								jAlert(
										"Your login session is expired please refresh page and login again.",
										"Error");
								return false;
							}
						});
			} else {
				$('#childDiv').hide();
			}

		}*/

		function loadGrandChild() {
			//var searchValue = $("#childType").val();
			var customerId = getSelectedCustomerId();
			if (customerId != '') {
				$.ajax({
					url : "${pageContext.request.contextPath}/GetFantasyGrandChildCategories",
					type : "post",
					//data : "childCategoryId=" + searchValue,
					data : "customerId="+customerId,
					dataType : "json",
					success : function(res) {
						$('#grandChildDiv').show();
						var respData = JSON.parse(JSON.stringify(res));
						var jsonData = respData.fantasyCategoryList;
						var select = document
								.getElementById('grandChildType');
						$('#grandChildType').empty();
						 $("#fantasyEvent").empty();
						 $('#fantasyTeam').empty();
						var opt = document.createElement('option');
						opt.value = '-1';
						opt.innerHTML = '--select--';
						select.appendChild(opt);
						if (jsonData != '' && jsonData.length > 0) {
							for ( var i = 0; i < jsonData.length; i++) {
								var opt = document
										.createElement('option');
								opt.value = jsonData[i].id;
								opt.innerHTML = jsonData[i].name;
								select.appendChild(opt);
							}
						}
						refreshTicketGridValues(null);
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			} else {
				jAlert("Please Select Customer.");
				$('#grandChildDiv').hide();
			}
		}

		function loadEvents() {
			var searchValue = $("#grandChildType").val();
			var customerId = getSelectedCustomerId();
			if (searchValue != '') {
				$
						.ajax({
							url : "${pageContext.request.contextPath}/GetFantasyEventsByGrandChild",
							type : "post",
							data : "grandChildCategoryId=" + searchValue+"&customerId="+customerId,
							dataType : "json",
							success : function(res) {
								/*$('#eventDiv').show();
								var jsonData = JSON.parse(JSON.stringify(res));
								var select = document
										.getElementById('fantasyEvent');
								$('#fantasyEvent').empty();
								 $('#fantasyTeam').empty();
								var opt = document.createElement('option');
								opt.value = '-1';
								opt.innerHTML = '--select--';
								select.appendChild(opt);
								if (jsonData != '' && jsonData.length > 0) {
									for ( var i = 0; i < jsonData.length; i++) {
										var opt = document
												.createElement('option');
										opt.value = jsonData[i].id;
										opt.innerHTML = jsonData[i].name;
										select.appendChild(opt);
									}
								}
								refreshTicketGridValues(null);*/
								var jsonData = JSON.parse(JSON.stringify(res));
								var jsonArray = jsonData.eventTeamGrid;
								ticketPagingInfo = res.ticketPagingInfo;
								$('#packageInfoDiv').html(res.packageInfo);
								refreshTicketGridValues(jsonArray);
							},
							error : function(error) {
								jAlert(
										"Your login session is expired please refresh page and login again.",
										"Error");
								return false;
							}
						});
			} else {
				$('#fantasyEvent').hide();
			}
		}

		/*function loadTeams() {
			var searchValue = $("#fantasyEvent").val();
			if (searchValue != '') {
				$
						.ajax({
							url : "${pageContext.request.contextPath}/GetFantasyTeamsbyEventId",
							type : "post",
							data : "leagueId=" + searchValue,
							dataType : "json",
							success : function(res) {
								$('#teamDiv').show();
								var jsonData = JSON.parse(JSON.stringify(res));
								var select = document
										.getElementById('fantasyTeam');
								$('#fantasyTeam').empty();
								var opt = document.createElement('option');
								opt.value = '-1';
								opt.innerHTML = '--select--';
								select.appendChild(opt);
								if (jsonData != '' && jsonData.length > 0) {
									for ( var i = 0; i < jsonData.length; i++) {
										var opt = document
												.createElement('option');
										opt.value = jsonData[i].id;
										opt.innerHTML = jsonData[i].name;
										select.appendChild(opt);
									}
								}
								refreshTicketGridValues(null);
							},
							error : function(error) {
								jAlert(
										"Your login session is expired please refresh page and login again.",
										"Error");
								return false;
							}
						});
			} else {
				$('#fantasyTeam').hide();
			}
		}

		function loadTickets() {
			var customerId = getSelectedCustomerId();
			var leagueId = $("#fantasyEvent").val();
			var teamId = $('#fantasyTeam').val();
			if (customerId <= 0) {
				jAlert("Please Select Customer.");
			} else if (leagueId <= 0) {
				jAlert("Please Select Event.");
			} else if (teamId <= 0) {
				jAlert("Please Select Team.");
				refreshTicketGridValues(null);
			} else {
				$
						.ajax({
							url : "${pageContext.request.contextPath}/GetFantasyTickets",
							type : "post",
							data : "leagueId=" + leagueId + "&teamId=" + teamId
									+ "&customerId=" + customerId,
							dataType : "json",
							success : function(res) {
								var jsonData = JSON.parse(res.data);
								if (jsonData.crownJewelTicketList != undefined
										&& jsonData.crownJewelTicketList.sectionTicketsMap != undefined) {
									var jsonArray = jsonData.crownJewelTicketList.sectionTicketsMap;
									ticketPagingInfo = res.ticketPagingInfo;
									refreshTicketGridValues(jsonArray);
								} else {
									jAlert("No Tickets are found for selected Event and Teams.");
									refreshTicketGridValues(null);
								}
							},
							error : function(error) {
								jAlert(
										"Your login session is expired please refresh page and login again.",
										"Error");
								return false;
							}
						});
			}
		}*/

		var pagingInfo;
		var customerGrid;
		var customerDataView;
		var customerData = [];
		var customerGridSearchString = '';
		var columnFilters = {};
		var customerColumns = [ {
			id : "customerType",
			name : "Customer Type",
			field : "customerType",
			sortable : true,
			width : 50
		}, {
			id : "customerName",
			name : "First Name",
			field : "customerName",
			sortable : true,
			width : 100
		}, {
			id : "lastName",
			name : "Last Name",
			field : "lastName",
			sortable : true,
			width : 100
		}, {
			id : "email",
			name : "Email",
			field : "email",
			sortable : true,
			width : 140
		}, {
			id : "productType",
			name : "Product Type",
			field : "productType",
			sortable : true,
			width : 120
		}, {
			id : "client",
			name : "Client",
			field : "client",
			sortable : true
		}, {
			id : "broker",
			name : "Broker",
			field : "broker",
			sortable : true
		}, {
			id : "street1",
			name : "Street1",
			field : "street1",
			sortable : true
		}, {
			id : "street2",
			name : "Street2",
			field : "street2",
			sortable : true
		}, {
			id : "city",
			name : "City",
			field : "city",
			sortable : true
		}, {
			id : "state",
			name : "State",
			field : "state",
			sortable : true
		}, {
			id : "country",
			name : "Country",
			field : "country",
			sortable : true
		}, {
			id : "zip",
			name : "Zip",
			field : "zip",
			sortable : true
		}, {
			id : "phone",
			name : "Phone",
			field : "phone",
			width : 100,
			sortable : true
		}, {
			id : "editCol",
			field : "editCol",
			name : "Edit Customer",
			width : 10,
			formatter : editFormatter
		} ];

		var customerOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect : false,
			topPanelHeight : 25,
			showHeaderRow : true,
			headerRowHeight : 30,
			explicitInitialization : true
		};
		var customerGridSortcol = "customerName";
		var customerGridSortdir = 1;
		var percentCompleteThreshold = 0;

		function editFormatter(row, cell, value, columnDef, dataContext) {
			var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.customerId +"'/>";
			return button;
		}
		$('.editClickableImage').live('click', function() {
			var me = $(this), id = me.attr('id');
			var delFlag = editCustomer(id);
		});

		function customerGridComparer(a, b) {
			var x = a[customerGridSortcol], y = b[customerGridSortcol];
			if (!isNaN(x)) {
				return (parseFloat(x) == parseFloat(y) ? 0
						: (parseFloat(x) > parseFloat(y) ? 1 : -1));
			}
			if (x == '' || x == null) {
				return 1;
			} else if (y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String)
					&& (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));
			}
		}

		function pagingControl(move, id) {
			if(id=='customer_pager'){
				var pageNo = 0;
				if (move == 'FIRST') {
					pageNo = 0;
				} else if (move == 'LAST') {
					pageNo = parseInt(pagingInfo.totalPages) - 1;
				} else if (move == 'NEXT') {
					pageNo = parseInt(pagingInfo.pageNum) + 1;
				} else if (move == 'PREV') {
					pageNo = parseInt(pagingInfo.pageNum) - 1;
				}
				getCustomerGridData(pageNo);
			}
			if(id=='custArtist_pager'){
				var pageNo = 0;
				if(move == 'FIRST'){
					pageNo = 0;
				}else if(move == 'LAST'){
					pageNo = parseInt(custArtistPagingInfo.totalPages)-1;
				}else if(move == 'NEXT'){
					pageNo = parseInt(custArtistPagingInfo.pageNum) +1;
				}else if(move == 'PREV'){
					pageNo = parseInt(custArtistPagingInfo.pageNum)-1;
				}
				getArtistGridData(pageNo);
			}
			if(id=='custEvent_pager'){
				var pageNo = 0;
				if(move == 'FIRST'){
					pageNo = 0;
				}else if(move == 'LAST'){
					pageNo = parseInt(custEventPagingInfo.totalPages)-1;
				}else if(move == 'NEXT'){
					pageNo = parseInt(custEventPagingInfo.pageNum) +1;
				}else if(move == 'PREV'){
					pageNo = parseInt(custEventPagingInfo.pageNum)-1;
				}
				getCustEventsGridData(pageNo);
			}
			if(id=='custRewardPoints_pager'){
				var pageNo = 0;
				if(move == 'FIRST'){
					pageNo = 0;
				}else if(move == 'LAST'){
					pageNo = parseInt(custRewardPointsPagingInfo.totalPages)-1;
				}else if(move == 'NEXT'){
					pageNo = parseInt(custRewardPointsPagingInfo.pageNum) +1;
				}else if(move == 'PREV'){
					pageNo = parseInt(custRewardPointsPagingInfo.pageNum)-1;
				}	
				getRewardPointsGridData(pageNo);
			}
			if(id == "custInvoiceTicket_pager"){
				var pageNo = 0;
				if(move == 'FIRST'){
					pageNo = 0;
				}else if(move == 'LAST'){
					pageNo = parseInt(custInvoiceTicketPagingInfo.totalPages)-1;
				}else if(move == 'NEXT'){
					pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum) +1;
				}else if(move == 'PREV'){
					pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum)-1;
				}
			}
		}

		function getCustomerGridData(pageNo) {
			$('#addTicketDiv').show();
			var searchValue = $("#searchValue").val();
			$
					.ajax({
						url : "${pageContext.request.contextPath}/Client/ManageDetails.json",
						type : "post",
						data : $("#customerSearch").serialize() + "&pageNo="
								+ pageNo + "&headerFilter="
								+ customerGridSearchString,
						dataType : "json",
						success : function(res) {
							var jsonData = JSON.parse(JSON.stringify(res));
							pagingInfo = jsonData.pagingInfo;
							refreshcustomerGridValues(jsonData.customers);
						},
						error : function(error) {
							jAlert(
									"Your login session is expired please refresh page and login again.",
									"Error");
							return false;
						}
					});
		}
		function refreshcustomerGridValues(jsonData) {
			$("div#divLoading").addClass('show');
			customerData = [];
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (customerData[i] = {});
				d["id"] = i;
				d["customerId"] = data.customerId;
				d["bAddressId"] = data.billingAddressId;
				d["firstName"] = data.customerName;
				d["lastName"] = data.lastName;
				d["customerType"] = data.customerType;
				d["customerName"] = data.customerName;
				d["client"] = data.client;
				d["broker"] = data.broker;
				d["email"] = data.customerEmail;
				d["productType"] = data.productType;
				d["street1"] = data.addressLine1;
				d["street2"] = data.addressLine2;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["zip"] = data.zipCode;
				d["phone"] = data.phone;
			}

			customerDataView = new Slick.Data.DataView();
			customerGrid = new Slick.Grid("#customer_grid", customerDataView,
					customerColumns, customerOptions);
			customerGrid.registerPlugin(new Slick.AutoTooltips({
				enableForHeaderCells : true
			}));
			customerGrid.setSelectionModel(new Slick.RowSelectionModel());
			if (pagingInfo != null) {
				var customerGridPager = new Slick.Controls.Pager(
						customerDataView, customerGrid, $("#customer_pager"),
						pagingInfo);
			}
			var customerGridColumnpicker = new Slick.Controls.ColumnPicker(
					customerColumns, customerGrid, customerOptions);

			// move the filter panel defined in a hidden div into customerGrid top panel
			//$("#customer_inlineFilterPanel").appendTo(customerGrid.getTopPanel()).show();

			customerGrid.onSort.subscribe(function(e, args) {
				customerGridSortdir = args.sortAsc ? 1 : -1;
				customerGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					customerDataView
							.fastSort(customerGridSortcol, args.sortAsc);
				} else {
					customerDataView.sort(customerGridComparer, args.sortAsc);
				}
			});
			// wire up model customers to drive the customerGrid
			customerDataView.onRowCountChanged.subscribe(function(e, args) {
				customerGrid.updateRowCount();
				customerGrid.render();
			});
			customerDataView.onRowsChanged.subscribe(function(e, args) {
				customerGrid.invalidateRows(args.rows);
				customerGrid.render();
			});
			$(customerGrid.getHeaderRow()).delegate(
					":input",
					"keyup",
					function(e) {
						var keyCode = (e.keyCode ? e.keyCode : e.which);
						customerGridSearchString = '';
						var columnId = $(this).data("columnId");
						if (columnId != null) {
							columnFilters[columnId] = $.trim($(this).val());
							if (keyCode == 13) {
								for ( var columnId in columnFilters) {
									if (columnId !== undefined
											&& columnFilters[columnId] !== "") {
										customerGridSearchString += columnId
												+ ":" + columnFilters[columnId]
												+ ",";
									}
								}
								getCustomerGridData(0);
							}
						}

					});
			customerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if (args.column.id.indexOf('checkbox') == -1) {
					if (args.column.id != 'editCol') {
						$("<input type='text'>").data("columnId",
								args.column.id).val(
								columnFilters[args.column.id]).appendTo(
								args.node);
					}
				}
			});
			customerGrid.init();
			// initialize the model after all the customers have been hooked up
			customerDataView.beginUpdate();
			customerDataView.setItems(customerData);
			customerDataView.endUpdate();
			customerDataView.syncGridSelection(customerGrid, true);
			$("#gridContainer").resizable();

			var customerRowIndex;
			customerGrid.onSelectedRowsChanged
					.subscribe(function() {
						var tempcustomerRowIndex = customerGrid
								.getSelectedRows([ 0 ])[0];
						if (tempcustomerRowIndex != customerRowIndex) {
							customerRowIndex = tempcustomerRowIndex;
							resetCustomerInfo();
							var customerEmail = customerGrid
									.getDataItem(customerRowIndex).email;
							if (customerEmail == null || customerEmail == '') {
								jAlert("Invoice cannot be created, Selected customer does not have email address");
								$('#createFantasyOrder').hide();
								return;
							}
							getCustomerSavedCards(tempcustomerRowIndex);
							/* if (!getCustomerInfoForInvoice(tempcustomerRowIndex)) {
								jAlert("Invoice can not be created for selected Customer, Please add customer address.");
								$('#createFantasyOrder').hide();
							} else {
								getCustomerShippingAddressDetails(
										tempcustomerRowIndex, '');
								getCustomerSavedCards(tempcustomerRowIndex);
								$('#createFantasyOrder').show();
							} */
						}
					});
			$("div#divLoading").removeClass('show');
		}

		function editCustomer(custId) {
			if (custId == null) {
				jAlert("There is something wrong.Please try again.");
			} else {
				getCustomerInfoForEdit(custId);
				/* var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="
						+ custId;
				popupCenter(url, "Edit Customer Details", "1000", "800"); */
			}
		}

		function getSelectedCustomerId() {
			var index = customerGrid.getSelectedRows([ 0 ])[0];
			if (index >= 0) {
				return customerGrid.getDataItem(index).customerId;
			}
			return -1;

		}

		function resetCustomerInfo() {
			$("#customerNameDiv").empty();
			$("#emailDiv").empty();
			$("#phoneDiv").empty();
			$("#addressLineDvi").empty();
			$("#countryDiv").empty();
			$("#stateDiv").empty();
			$("#cityDiv").empty();
			$("#zipCodeDiv").empty();

			$('#shAddressLine1Div').empty();
			$('#shAddressLine2Div').empty();
			$('#shPhoneDiv').empty();
			$('#shCountryDiv').empty();
			$('#shStateDiv').empty();
			$('#shCityDiv').empty();
			$('#shZipCodeDiv').empty();
		}

		//Get selected event on button click
		/* function getCustomerInfoForInvoice(customerGridIndex) {
			var address = customerGrid.getDataItem(customerGridIndex).street1;
			var strret2 = customerGrid.getDataItem(customerGridIndex).street2;
			if (address == undefined) {
				address = '';
			}
			if (strret2 == undefined) {
				strret2 = '';
			}
			$("#customerIdDiv").val(
					customerGrid.getDataItem(customerGridIndex).customerId);
			$("#customerNameDiv").text(
					customerGrid.getDataItem(customerGridIndex).customerName);
			$("#emailDiv").text(
					customerGrid.getDataItem(customerGridIndex).email);
			$("#phoneDiv").text(
					customerGrid.getDataItem(customerGridIndex).phone);
			$("#bAddressId").val(
					customerGrid.getDataItem(customerGridIndex).bAddressId);
			$("#blFirstName").val(
					customerGrid.getDataItem(customerGridIndex).firstName);
			$("#blLastName").val(
					customerGrid.getDataItem(customerGridIndex).lastName);
			$("#blEmail")
					.val(customerGrid.getDataItem(customerGridIndex).email);
			$("#phone").val(customerGrid.getDataItem(customerGridIndex).phone);
			if (strret2 != null && strret2 != '') {
				address = address + ',' + strret2;
			}
			$("#addressLineDvi").text(address);
			$("#addressLine1").val(address);
			$("#addressLine2").val(strret2);
			$("#countryDiv").text(
					customerGrid.getDataItem(customerGridIndex).country);
			$("#country").val(
					customerGrid.getDataItem(customerGridIndex).country);
			$("#stateDiv").text(
					customerGrid.getDataItem(customerGridIndex).state);
			$("#state").val(customerGrid.getDataItem(customerGridIndex).state);
			$("#city").val(customerGrid.getDataItem(customerGridIndex).city);
			$("#cityDiv")
					.text(customerGrid.getDataItem(customerGridIndex).city);
			$("#zipCodeDiv").text(
					customerGrid.getDataItem(customerGridIndex).zip);
			$("#zipCode").val(customerGrid.getDataItem(customerGridIndex).zip);
			if ((address == null || address == '')
					&& (strret2 == null || strret2 == '')) {
				return false;
			} else {
				return true;
			}

		}

		var shippingAddressData = "";
		//Get customer shipping address details
		function getCustomerShippingAddressDetails(selectedCustRowId,
				shippingAddressId) {
			var custId = customerGrid.getDataItem(selectedCustRowId).customerId;
			$
					.ajax({
						url : "${pageContext.request.contextPath}/getShippingAddressDetails",
						type : "post",
						dataType : "json",
						data : {
							customerId : custId,
							shippingId : shippingAddressId
						},
						success : function(res) {
							shippingAddressData = JSON.parse(JSON
									.stringify(res));
							if (shippingAddressData != ''
									&& shippingAddressData.length > 0) {
								$.each(shippingAddressData, function(index,
										element) {
									$("#sAddressId").val(element.shId);
									$('#shAddressLine1Div').append(
											element.street1);
									$('#shAddressLine1').val(element.street1);
									$('#shAddressLine2').val(element.street2);
									$('#shFirstName').val(element.firstName);
									$('#shLastName').val(element.lastName);
									$('#shCity').val(element.city);
									$('#shState').val(element.state);
									$('#shCountry').val(element.country);
									$('#shZipCode').val(element.zipCode);
									$('#shPhone').val(element.phone);
									$('#shAddressLine2Div').append(
											element.street2);
									$('#shPhoneDiv').append(element.phone);
									$('#shCountryDiv').append(element.country);
									$('#shStateDiv').append(element.state);
									$('#shCityDiv').append(element.city);
									$('#shZipCodeDiv').append(element.zipCode);
									$('#createInvoice').show();
								});
							} else {
								jAlert("Invoice can not be created for selected Customer, please add shipping address.");
								$('#createInvoice').hide();
							}

						},
						error : function(error) {
							jAlert(
									"Your login session is expired please refresh page and login again.",
									"Error");
							$('#createInvoice').hide();
							return false;
						}
					});
		} */

		//Get customer saved card details
		function getCustomerSavedCards(selectedCustRowId) {
			var custId = customerGrid.getDataItem(selectedCustRowId).customerId;
			$
					.ajax({
						url : "${pageContext.request.contextPath}/GetSavedCardsRewardPointsAndLoyaFan",
						type : "post",
						dataType : "json",
						data : {
							customerId : custId,
							eventId : $('#eventId').val()
						},

						success : function(res) {
							var respData = JSON.parse(JSON.stringify(res));
							$('#activeRewardPoints').text(
									parseFloat(respData.rewardPoints)
											.toFixed(2));
							$('#customerRewardPoints').val(
									parseFloat(respData.rewardPoints)
											.toFixed(2));
							loadGrandChild();
							$('#packageInfoDiv').html('');
							refreshTicketGridValues(null);
						},
						error : function(error) {
						}
					});
		}

		getCustomerGridData(0);

		function addCustomerMsg() {
			jAlert("Customer Added successfully.");
		}

		/* function changeShippingAddress(addressId) {
			selectedCustRowId = customerGrid.getSelectedRows([ 0 ])[0];
			$('#shAddressLine1Div').empty();
			$('#shAddressLine2Div').empty();
			$('#shPhoneDiv').empty();
			$('#shCountryDiv').empty();
			$('#shStateDiv').empty();
			$('#shCityDiv').empty();
			$('#shZipCodeDiv').empty();
			getCustomerShippingAddressDetails(selectedCustRowId, addressId);
		}

		function showShippingAddress() {
			var custId = document.getElementById('customerIdDiv').value;
			if (custId == null) {
				jAlert("There is something wrong.Please try again.");
			} else {
				var createPOUrl = "${pageContext.request.contextPath}/Invoice/getShippingAddress?custId="
						+ custId;
				popupCenter(createPOUrl, 'Shipping Addresses', '800', '500');
			}
		} */

		//show the pop window center
		function popupCenter(url, title, w, h) {
			// Fixes dual-screen position                         Most browsers      Firefox  
			var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
					: screen.left;
			var dualScreenTop = window.screenTop != undefined ? window.screenTop
					: screen.top;

			width = window.innerWidth ? window.innerWidth
					: document.documentElement.clientWidth ? document.documentElement.clientWidth
							: screen.width;
			height = window.innerHeight ? window.innerHeight
					: document.documentElement.clientHeight ? document.documentElement.clientHeight
							: screen.height;

			var left = ((width / 2) - (w / 2)) + dualScreenLeft;
			var top = ((height / 2) - (h / 2)) + dualScreenTop;
			var newWindow = window.open(url, title, 'scrollbars=yes, width='
					+ w + ', height=' + h + ', top=' + top + ', left=' + left);

			// Puts focus on the newWindow  
			if (window.focus) {
				newWindow.focus();
			}
		}

		//Copy billing address
		/* function copyBillingAddress() {
			if ($("#isBilling").prop('checked') == true) {
				var bAddressId = $("#bAddressId").val();
				$("#sAddressId").val(bAddressId);
				$("#shippingIsSameAsBilling").val('Yes');
				$('#createInvoice').show();

				// $("#sAddressId").val($("#bAddressId").val());
				$('#shAddressLine1Div').text($("#addressLine1").val());
				$('#shAddressLine1').val($("#addressLine1").val());
				$('#shAddressLine2').val($("#addressLine2").val());
				$('#shFirstName').val($("#blFirstName").val());
				$('#shLastName').val($("#blLastName").val());
				$('#shCity').val($("#city").val());
				$('#shState').val($("#state").val());
				$('#shCountry').val($("#country").val());
				$('#shZipCode').val($("#zipCode").val());
				$('#shPhone').val($("#phone").val());
				$('#shAddressLine2Div').text($("#addressLine2").val());
				$('#shPhoneDiv').text($("#phone").val());
				$('#shCountryDiv').text($("#country").val());
				$('#shStateDiv').text($("#state").val());
				$('#shCityDiv').text($("#city").val());
				$('#shZipCodeDiv').text($("#zipCode").val());
				$('#createInvoice').show();

			} else {

				$("#sAddressId").val('');
				$("#shippingIsSameAsBilling").val('No');

				$('#shAddressLine1Div').text('');
				$('#shAddressLine1').val('');
				$('#shAddressLine2').val('');
				$('#shFirstName').val('');
				$('#shLastName').val('');
				$('#shCity').val('');
				$('#shState').val('');
				$('#shCountry').val('');
				$('#shZipCode').val('');
				$('#shPhone').val('');
				$('#shAddressLine2Div').text('');
				$('#shPhoneDiv').text('');
				$('#shCountryDiv').text('');
				$('#shStateDiv').text('');
				$('#shCityDiv').text('');
				$('#shZipCodeDiv').text('');

				if (shippingAddressData != '' && shippingAddressData.length > 0) {
					$.each(shippingAddressData, function(index, element) {
						$("#sAddressId").val(element.shId);
						$('#shAddressLine1Div').append(element.street1);
						$('#shAddressLine1').val(element.street1);
						$('#shAddressLine2').val(element.street2);
						$('#shFirstName').val(element.firstName);
						$('#shLastName').val(element.lastName);
						$('#shCity').val(element.city);
						$('#shState').val(element.state);
						$('#shCountry').val(element.country);
						$('#shZipCode').val(element.zipCode);
						$('#shPhone').val(element.phone);
						$('#shAddressLine2Div').append(element.street2);
						$('#shPhoneDiv').append(element.phone);
						$('#shCountryDiv').append(element.country);
						$('#shStateDiv').append(element.state);
						$('#shCityDiv').append(element.city);
						$('#shZipCodeDiv').append(element.zipCode);
						$('#createInvoice').show();
					});
				}
			}
		} */

		var ticketPagingInfo;
		var ticketDataView;
		var ticketGrid;
		var ticketData = [];
		var userTicketColumns = [ 
		 /*{
			id : "teamId",
			name : "Team Id",
			field : "teamId",
			width : 100,
			sortable : true
		}, */{
			id : "teamName",
			name : "Team Name",
			field : "teamName",
			//width : 100,
			sortable : true
		}, /*{
			id : "fantasyEventId",
			name : "Fantasy Event Id",
			field : "fantasyEventId",
			width : 100,
			sortable : true
		}, {
			id : "teamZoneId",
			name : "Team Zone Id",
			field : "teamZoneId",
			width : 100,
			sortable : true
		}, */{
			id : "section",
			name : "Zone",
			field : "section",
			//width : 100,
			sortable : true
		}, {
			id : "quantity",
			name : "Quantity",
			field : "quantity",
			//width : 100,
			sortable : true
		}, {
			id : "requiredPointsAsDouble",
			name : "Required Points",
			field : "requiredPointsAsDouble",
			//width : 100,
			sortable : true
		}, /*{
			id : "ticketId",
			name : "Ticket Id",
			field : "ticketId",
			width : 100,
			sortable : true
		}, */{
			id : "tmatEventId",
			name : "TMAT Event Id",
			field : "tmatEventId",
			//width : 100,
			sortable : true
		}, {
			id : "eventDate",
			name : "Event Date",
			field : "eventDate",
			//width : 100,
			sortable : true
		}, {
			id : "eventTime",
			name : "Event Time",
			field : "eventTime",
			//width : 100,
			sortable : true
		}, {
			id : "venueName",
			name : "Venue",
			field : "venueName",
			//width : 100,
			sortable : true
		}, {
			id : "city",
			name : "City",
			field : "city",
			//width : 100,
			sortable : true
		}, {
			id : "state",
			name : "State",
			field : "state",
			//width : 100,
			sortable : true
		}, {
			id : "country",
			name : "Country",
			field : "country",
			//width : 100,
			sortable : true
		}/*, {
			id : "isReedemable",
			name : "Reedemable",
			width : 100,
			field : "isReedemable",
			sortable : true
		}, {
			id : "packageAllowed",
			name : "Is Package",
			field : "packageAllowed",
			width : 100,
			sortable : true
		}, {
			id : "packageCostAsDouble",
			name : "Package Cost",
			field : "packageCostAsDouble",
			width : 100,
			sortable : true
		}, {
			id : "packageDetails",
			name : "Package Details",
			field : "packageDetails",
			width : 100,
			sortable : true
		},*/ ];

		var ticketOptions = {
			editable : true,
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect : false,
			topPanelHeight : 25,
			//showHeaderRow: true,
			headerRowHeight : 30,
			explicitInitialization : true
		};
		var ticketGridSortcol = "section";
		var ticketGridSortdir = 1;
		var percentCompleteThreshold = 0;

		function ticketGridComparer(a, b) {
			var x = a[ticketGridSortcol], y = b[ticketGridSortcol];
			if (!isNaN(x)) {
				return (parseFloat(x) == parseFloat(y) ? 0
						: (parseFloat(x) > parseFloat(y) ? 1 : -1));
			}
			if (x == '' || x == null) {
				return 1;
			} else if (y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String)
					&& (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));
			}
		}
		function refreshTicketGridValues(jsonData) {
			ticketData = [];
			if (jsonData != null) {
				for ( var i = 0; i < jsonData.length; i++) {
					var data = jsonData[i];
					var d = (ticketData[i] = {});
					d["id"] = data.ticketId;
					d["ticketId"] = data.ticketId;
					d["teamId"] = data.teamId;
					d["teamName"] = data.teamName;
					d["fantasyEventId"] = data.eventId;
					d["teamZoneId"] = data.teamZoneId;
					d["section"] = data.zone;
					d["quantity"] = data.quantity;
					d["isRealTicket"] = data.isRealTicket;
					d["requiredPointsAsDouble"] = data.requiredPoints.toFixed(2);
					d["tmatEventId"] = data.tmatEventId;
					d["eventDate"] = data.eventDateStr;
					d["eventTime"] = data.eventTimeStr;
					d["venueName"] = data.venueName;
					d["city"] = data.city;
					d["state"] = data.state;
					d["country"] = data.country;
					
					/*d["id"] = data.ticketsMap[0].ticketId;
					d["section"] = data.section;
					d["quantity"] = data.ticketsMap[0].quantity;
					d["isRealTicket"] = data.ticketsMap[0].isRealTicket;
					d["requiredPointsAsDouble"] = data.ticketsMap[0].requiredPointsAsDouble.toFixed(2);					
					if (data.peackageAllowed) {
						d["packageAllowed"] = 'Yes';
					} else {
						d["packageAllowed"] = 'No';
					}
					d["packageCostAsDouble"] = data.packageCostAsDouble
							.toFixed(2);
					;
					d["packageDetails"] = data.packageDetails;
					if (data.ticketsMap[0].isReedemable) {
						d["isReedemable"] = 'Yes';
					} else {
						d["isReedemable"] = 'No';
					}*/
				}
			} else {
				if(ticketGrid!=undefined){
					ticketGrid.invalidateAllRows();
					ticketDataView.setItems([]);
				}
				
			}

			ticketDataView = new Slick.Data.DataView();
			ticketGrid = new Slick.Grid("#ticket_grid", ticketDataView,
					userTicketColumns, ticketOptions);
			ticketGrid.registerPlugin(new Slick.AutoTooltips({
				enableForHeaderCells : true
			}));
			ticketGrid.setSelectionModel(new Slick.RowSelectionModel());
			if (ticketPagingInfo != null) {
				var ticketGridPager = new Slick.Controls.Pager(ticketDataView,
						ticketGrid, $("#ticket_pager"), ticketPagingInfo);
			}

			var ticketGridColumnpicker = new Slick.Controls.ColumnPicker(
					userTicketColumns, ticketGrid, ticketOptions);

			ticketGrid.onSort.subscribe(function(e, args) {
				ticketGridSortdir = args.sortAsc ? 1 : -1;
				ticketGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					ticketDataView.fastSort(ticketGridSortcol, args.sortAsc);
				} else {
					ticketDataView.sort(ticketGridComparer, args.sortAsc);
				}
			});
			// wire up model tickets to drive the ticketGrid
			ticketDataView.onRowCountChanged.subscribe(function(e, args) {
				ticketGrid.updateRowCount();
				ticketGrid.render();
			});
			ticketDataView.onRowsChanged.subscribe(function(e, args) {
				ticketGrid.invalidateRows(args.rows);
				ticketGrid.render();
			});
			ticketGrid.init();

			// initialize the model after all the tickets have been hooked up
			ticketDataView.beginUpdate();
			ticketDataView.setItems(ticketData);
			ticketDataView.endUpdate();
			ticketDataView.syncGridSelection(ticketGrid, true);
			$("#gridContainer").resizable();
			ticketGrid.resizeCanvas();

		}
		function getSelectedTicketRowId() {
			var index = ticketGrid.getSelectedRows([ 0 ])[0];
			return index;

		}
	</script>