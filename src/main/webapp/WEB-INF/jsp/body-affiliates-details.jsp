<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script src="../resources/js/app/affiliatePromoCodeHistory.js"></script>

<style>
input {
	color: black !important;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
  background:#FFFFFF;
  color:#000000;
  border: 1px solid gray;
  padding: 2px;
  display: inline-block;
  min-width: 200px;
  
  -moz-box-shadow: 2px 2px 2px silver;
  -webkit-box-shadow: 2px 2px 2px silver;
  z-index: 99999;
}
#contextMenu li {
  padding: 4px 4px 4px 14px;
  list-style: none;
  cursor: pointer;
}
#contextMenu li:hover {
	color:#FFFFFF;
  background-color:#4d94ff;
}

#addCustomer, #addUser, #active, #inactive {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

#contextMenuOther {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenuOther li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenuOther li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenuPO {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenuPO li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenuPO li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

</style>
<script>
$(document).ready(function(){
	
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#regFromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#regToDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#searchValue').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {
		var userId = $('#userId').val();
		getAffiliateGridData(userId,0);
	    return false;  
	  }
	});	
	
	$('#menuContainer').click(function(){
		if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
		}else{
		   $('#menuContainer').addClass('ABCD');
		}
		if(affiliatesAllGrid != null && affiliatesAllGrid != undefined){
			affiliatesAllGrid.resizeCanvas();
		}
		if(affiliateGrid != null && affiliateGrid != undefined){
			affiliateGrid.resizeCanvas();
		}
	});
	
	$("#activeAffiliate").click(function(){
			callTabOnChange('Active');
	});
	$("#inactiveAffiliate").click(function(){
		callTabOnChange('Inactive');
	});
	
	<c:choose>
		<c:when test="${status == 'Active' or status =='ACTIVE'}">
			$('#activeDiv').addClass('active');
			$('#activeTab').addClass('active');
		</c:when>
		<c:when test="${status == 'Inactive' or status =='INACTIVE'}">
			$('#inactiveDiv').addClass('active');
			$('#inactiveTab').addClass('active');
		</c:when>
	</c:choose>
});

function callTabOnChange(selectedTab) {		
	var data = "?status="+selectedTab;
	window.location = "${pageContext.request.contextPath}/Affiliates"+data;
}
	
function exportToExcel(){
	if($('#activeDiv').hasClass('active')){
		$('#status').val('Active');
	}else if($('#inactiveDiv').hasClass('active')){
		$('#status').val('Inactive');
	}
	var appendData = "user=AFFILIATE&status="+$('#status').val()+"&headerFilter="+affiliatesAllGridSearchString;
    var url = apiServerUrl + "UsersExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function affiliateExportToExcel(){	
	var userId = $('#userId').val();
    var url = apiServerUrl + "ExportAffiliateReport?userId="+userId+"&headerFilter="+affiliateGridSearchString;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	affiliatesAllGridSearchString='';
	columnFilters = {};
	getAffiliatesAllGridData(0);
}

function affiliateResetFilters(){
	affiliateGridSearchString='';
	affiliatesColumnFilters = {};
	var userId = $('#userId').val();
	getAffiliateGridData(userId,0);
}

function saveUserCustomerPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = customerGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('affiliategrid',colStr);
}

//call functions once page loaded
$(window).load(function() {
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
	affiliatePagingInfo = JSON.parse(JSON.stringify(${affiliatePagingInfo}));
	var affiliatesList = ${affiliates};
	if(affiliatesList != null && affiliatesList.length > 0){
		refreshAffiliatesAllGridValues(JSON.parse(JSON.stringify(${affiliates})));
	}
	$('#affiliates_allpager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveAffiliateUserPreference()'>");
	$('#affiliate_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");	
});

</script>
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="view order">View Order Details</li>
  <li data="view modify notes">View/Modify Notes</li>  
</ul>
<ul id="contextMenuOther" style="display:none;position:absolute">
  <li data="view affiliate codes">View Affiliate Code</li>
  <c:if test="${status =='Active'}"><li data="generate new affiliate code">Generate New Affiliate Code</li></c:if> 
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Admin
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Affiliates</a></li>
			<li><i class="fa fa-laptop"></i>Manage Affiliates</li>
		</ol>
	</div>
</div>

<!-- Grid view for affiliates -->
<div style="position: relative">
	<div class="full-width">
		<a style="margin-top: -0.8%; margin-left: 0%;" class="btn btn-primary"
		id="addUser" href="${pageContext.request.contextPath}/Client/AddUsers?role=role_affiliates"
		title="Add User">Add Affiliates</a>
		<!--
		<a style="margin-top: -0.8%; margin-left: 2%;" class="btn btn-primary"
		id="active" href="#"
		title="Active" onclick="getDateForPromoCode();">Active</a>
		
		<a style="margin-top: -0.8%; margin-left: 2%;" class="btn btn-primary"
		id="inactive" href="#"
		title="In Active" onclick="userStatus('INACTIVE');">In Active</a>
				
		<br/><br/>

		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Manage Affiliates</label>
				<div class="pull-right">
					<!-- <a href="javascript:exportToExcel()" name='Export to Excel' style='float: right; margin-right: 10px;'>Export to Excel</a>--> 
					<!--<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
				</div>
			</div>
			<div id="affiliatesAllGrid" style="width: 100%; height: 200px; overflow: auto; border-right: 1px solid gray; border-left: 1px solid gray"></div>
			<div id="affiliates_allpager" style="width: 100%; height: 20px;"></div>
		</div>-->
	</div>
	
	<div class="full-width">
		<section class="invoice-panel panel">
		<ul class="nav nav-tabs" style="">
			<li id="activeTab" class=""><a id="activeAffiliate" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeDiv">Active</a></li>
			<li id="inactiveTab" class=""><a id="inactiveAffiliate" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveDiv">Inactive</a>
		</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content"> <input type="hidden" value="" id="status" name="status" />
			<div id="activeDiv" class="tab-pane">
				<c:if test="${status =='Active'}">
				<div class="full-width mb-20 full-width-btn">
					<button type="button" class="btn btn-primary" onclick="userStatus('INACTIVE');">In Active</button>
					<button type="button" class="btn btn-primary" onclick="viewHistory();">View Affiliate Code</button>
					<button type="button" class="btn btn-primary" onclick="getDateForRegeneratePromoCode();">Generate New Affiliate Code</button>
				</div>
				<br />	
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Manage Affiliates</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="affiliatesAllGrid" style="width: 100%; height: 200px; border: 1px solid gray;"></div>
					<div id="affiliates_allpager" style="width: 100%; height: 20px;"></div>
				</div>
				</c:if>
			</div>
			
			<div id="inactiveDiv" class="tab-pane">
				<c:if test="${successMessage != null}">
					<div class="alert alert-success fade in">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
					</div>
				</c:if>
				<c:if test="${status =='INACTIVE' or status =='Inactive'}">	
				<div class="form-group col-xs-12 col-md-12">
					<button type="button" class="btn btn-primary" onclick="getDateForPromoCode();">Active</button>
					<button type="button" class="btn btn-primary" onclick="viewHistory();">View Affiliate Code</button>
					<!--<button type="button" class="btn btn-primary" onclick="getDateForRegeneratePromoCode();">Generate New Affiliate Code</button>-->
				</div>
				<br />
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Manage Affiliates</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="affiliatesAllGrid" style="width: 100%; height: 200px; border: 1px solid gray;"></div>
					<div id="affiliates_allpager" style="width: 100%; height: 20px;"></div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
	
</div>

<div class="row">
<input type="hidden" name="userId" id="userId"/>
</div>
<br/>

<!-- Grid view for affiliates -->
<div id="affiliateOrder" style="position: relative;">
	<div class="table-responsive grid-table grid-table-2">
		<div class="grid-header full-width">
			<label>Affiliate Customer Orders</label>
			<div class="pull-right">
				<a href="javascript:affiliateExportToExcel()" name='Export to Excel' style='float: right; margin-right: 10px;'>Export to Excel</a> 
				<a href="javascript:affiliateResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="affiliateGrid" style="width: 100%; height: 200px; overflow: auto; border: 1px solid gray;"></div>
		<div id="affiliate_pager" style="width: 100%; height: 20px;"></div>
	</div>
</div>
<br />

<!-- Toggle add Modal-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		  <div class="modal-content full-width">
			  <div class="modal-header full-width">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">Add Dates</h4>
			  </div>
			  <div class="modal-body full-width">					
				   <form role="form" class="tab-fields full-width" id="" method="post" action="">
						<div class="full-width">
							<div class="form-group col-sm-4 col-xs-6">
								<label for="name" class="control-label">From Date</label>
								<input class="form-control" type="text" id="fromDate" name="fromDate">
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label for="name" class="control-label">To Date</label>
								<input class="form-control" type="text" id="toDate" name="toDate">
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label for="name" class="control-label">Promotional Code</label>
								<input class="form-control" id="promoCode" name="promoCode" onblur="checkActivePromoCode();" >
							</div>
						</div>
						
				   </form>
			  </div>
			  <div class="modal-footer full-width">
					<button class="btn btn-success" type="button" onclick="userStatus('ACTIVE');">Save</button>
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
		  </div>
	  </div>
	</div>
	
</div>
<!-- <div id="inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
	Show records with customer including <input type="text" id="txtSearch2">
	
  <div style="width:100px;display:inline-block;" id="pcSlider2"></div>

</div>-->

<!-- popup View Order Details -->
	<%@include file="body-view-affiliate-order-details.jsp"%>
<!-- End popup View Order Details -->

<!-- popup Edit Affiliates -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-affiliates">Edit Affiliates</button> -->
	<div id="edit-affiliates" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Affiliates - Email : <span id="affiliateEmail_Hdr" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<style>
						#status_div{
							display: inline-block;
							margin-top: 15px;
							width: auto;
						}
					</style>
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header"><i class="fa fa-laptop"></i> Admin</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a href="#">Affiliates</a></li>
								<li><i class="fa fa-laptop"></i>Edit Affiliates</li>
							</ol>							
						</div>
					</div>

					<div class="row">
						  <div class="col-xs-12">
							  <section class="panel">
								  <header class="panel-heading">
										Fill Affiliates Details          
								  </header>
								  <div class="panel-body">
									  <div class="form">
									  <div id="editAff_successDiv" class="alert alert-success fade in" style="display:none;">
										<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
										<span id="editAff_successMsg"></span></strong>
									  </div>					
									  <div id="editAff_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
										<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
										<span id="editAff_errorMsg"></span></strong>
									  </div>
									  <form class="form-validate form-horizontal" id="editUserForm" method="post" action="" >
										  <input class="form-control" id="editAff_action" value="action" name="action" type="hidden" />
										  <input class="form-control" id="editAff_userId" value="${userId}" name="userId" type="hidden" />
										  <input class="form-control" id="editAff_status" value="${status}" name="status" type="hidden" />
										   <div class="form-group col-sm-6 col-xs-6">
												<div class="col-xs-12 full-width">
													  <label>User Name</label>
													  <input type="text" id="userName" name="userName" readonly="readonly" size="100" style="font-weight: bold; border:none; background: transparent;" />
												</div>
										  </div>
										  
										  <div class="form-group col-sm-6 col-xs-6">
											  <label>First Name <span class="required">*</span></label>											  
											  <input class="form-control" id="firstName" name="firstName" type="text"/>
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
											  <label>Last Name <span class="required">*</span></label>
											  <input class="form-control" id="lastName" name="lastName" type="text"/>
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
											  <label>E-Mail <span class="required">*</span></label>											  
											  <input class="form-control" id="email" type="email" name="email"/>											  
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
											  <label>Phone</label>
											  <input class="form-control" id="phone" name="phone" maxlength="10" />											  
										  </div>										  
										  <div class="form-group col-sm-6 col-xs-6">
											  <label>Status </label>
											  <select class="form-control" name="affiliateStatus" id="affiliateStatus">
												<option value="GOLD">Gold</option>
												<option value="PLATINIUM">Platinium</option>
											  </select>											  
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
											  <label> Affiliates Cash(RTF Orders) % <span class="required">*</span></label>
											  <input class="form-control" id="affiliateDiscount" name="affiliateDiscount" type="text" />
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
											  <label>Customer Discount(RTF Orders) % <span class="required">*</span></label>
											  <input class="form-control" id="customerOrderDiscount" name="customerOrderDiscount" type="text" />
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
										  	<label>Affiliates cash(Phone Orders) % <span class="required">*</span></label>
										  	<input class="form-control"  id="phoneAffiliateDiscount" name="phoneAffiliateDiscount" />
										  </div>
										 <div class="form-group col-sm-6 col-xs-6">
										 	<label>Customer Discount(Phone Orders) % <span class="required">*</span></label>
										 	<input class="form-control"  id="phoneCustomerOrderDiscount" name="phoneCustomerOrderDiscount" />
										 </div>
										 <div class="form-group col-sm-6 col-xs-6">
											  <label>Repeat Business </label>
											  <select class="form-control" name="affiliateBusiness" id="affiliateBusiness">
												<option value="NO">No</option>
												<option value="YES">Yes</option>
											  </select>											  
										  </div>
										 <div class="form-group col-sm-6 col-xs-6">
											  <label>Can Earn Reward Points</label>
											  <select class="form-control" name="canEarnRewardPoints" id="canEarnRewardPoints">
												<option value="YES">Yes</option>
												<option value="NO">No</option>
											</select>										  
										  </div>
										  <div class="form-group col-sm-6 col-xs-6">
										      <label>Roles</label>										  
											  <input type="hidden" id="affiliateRole" name="role" value="role_affiliates">
											  <label>Affiliates</label>												                                    	
										  </div>										  
										  
										  <div class="form-group full-width" align="center">
											  <div class="col-sm-12 col-xs-12">
												  <button class="btn btn-primary" type="button" onclick="doEditAffiliateValidations()">Update</button>
											  </div>
										  </div>
									  </form>
									</div>

								  </div>
								  
								  <header class="panel-heading mt-20">
									  Change Password
								  </header>
								  <div class="panel-body">
									  <div class="form">
									  
										  <form class="form-validate form-horizontal" id="changePasswordForm" method="post" action="" >
											  <input class="form-control" id="actionChangepassword" value="action" name="action" type="hidden" />
											  <input class="form-control" id="editAff_Pwd_userId" value="${userId}" name="userId" type="hidden" />
											  <div class="form-group col-sm-6 col-xs-6">
												  <label for="curl" class="control-label col-sm-3 col-xs-4">Password <span class="required">*</span></label>
												  <input class="form-control" id="password" name="password" type="password" />												  
											  </div>
											  <div class="form-group col-sm-6 col-xs-6">
												  <label for="cname" class="control-label col-sm-3 col-xs-4">Re-Password <span class="required">*</span></label>
												  <input class="form-control" id="repassword" name="repassword" type="password" />
											  </div>                                      
											  
											  <div class="form-group full-width" align="center">
												  <div class="col-sm-12 col-xs-12">
													  <button class="btn btn-primary" type="button" onclick="doEdAffChangePasswordValidations()">Update Password</button>
												  </div>
											  </div>
										  </form>
									  </div>

								  </div>
									
								<%-- <header class="panel-heading">
									  Actions
								  </header>
								  <div class="form-group">
									 <div class="form">
										<br/><br/>
									<form class="form-validate form-horizontal" id="Form" method="post" action="${pageContext.request.contextPath}/Admin/EditUser" >
										<input class="form-control" id="" value="action" name="action" type="hidden" />
										<input class="form-control" id="userId" value="${userId}" name="userId" type="hidden" />
										<div class="col-lg-offset-2 col-sm-10 col-xs-8">
											<button class="btn btn-primary" type="button" onclick="logOutAction()">Logout User</button>
										</div>
									</form>
									</div>
								  </div> --%>
							  </section>
						  </div>
					</div>

					
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Toggle add Modal-->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#date-modal-3">Add Dates</button> -->
	<div class="modal fade" id="date-modal-3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
			  <div class="modal-content full-width">
				  <div class="modal-header full-width">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h4 class="modal-title">Add Dates</h4>
				  </div>
				  <div class="modal-body full-width">					
					   <form role="form" class="full-width" id="" method="post" action="">
							<div class="tab-fields full-width">
								<div class="form-group col-sm-4 col-xs-6">
									<label for="name" class="control-label">From Date</label>
									<input class="form-control" type="text" id="regFromDate" name="fromDate">
								</div>
								<div class="form-group col-sm-4 col-xs-6">
									<label for="name" class="control-label">To Date</label>
									<input class="form-control" type="text" id="regToDate" name="toDate">
								</div>
								<div class="form-group col-sm-4 col-xs-6">
									<label>Promotional Code</label>
									<input class="form-control" id="regPromoCode" name="regPromoCode" onblur="checkPromoCode();" >
								</div>
							</div>
					   </form>
				  </div>
				  <div class="modal-footer full-width">
						<button class="btn btn-success" type="button" onclick="regeneratePromoCode();">Save</button>
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					</div>
			  </div>
		  </div>
	</div>
<!-- End popup Edit Affiliates -->

<!-- popup Affiliates View -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view1">Affiliates</button> -->
<div id="view1" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Affiliates</h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Affiliates
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Affiliates</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Promotional Code</li>
						</ol>
					</div>
				</div>
				<br />

				<div id="promoHistory_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="promoHistory_successMsg"></span></strong>
				</div>					
				<div id="promoHistory_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="promoHistory_errorMsg"></span></strong>
				</div>
				<br/>
				
				<div style="position: relative" id="ctd">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Promotional Code History</label> <!--<span id="promoCode_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel" onclick="promoCodeGridToggleFilterRow()"></span>-->
						</div>
						<div id="promocode_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="promocode_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				
				<!--
				<div id="promocode_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
								Show records with ID <input type="text" id="promoCodeGridSearch">-->
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
			
		</div>
	</div>
</div>
<!-- End Affiliates View -->


<!-- popup View/Modify Notes -->
	<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-modify-notes">View/Modify Notes</button>-->

	<div id="view-modify-notes" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">View/Modify Notes - Order No : <span id="affOrderId_Hdr_ModNotes" class="headerTextClass"></span></h4>
		  </div>
		  <div class="modal-body full-width">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header">
						<i class="fa fa-laptop"></i>Affiliates
					</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Affiliates</a>
						</li>
						<li><i class="fa fa-laptop"></i>Note</li>
					</ol>
				</div>
			</div>
			<div id="ModifyNotesMsg" style="display: none;" class="alert alert-success fade in"></div>
			<br />

			<div id="row">
				<div class="col-xs-12">
					<form class="form-validate form-horizontal" id="editAffPaymentNote" method="post" action="${pageContext.request.contextPath}/EditAffCusOrderNote">
						<div class="form-group">
							<input type="hidden" id="action" name="action" value="update" />
							 <input type="hidden" id="affOrderId_ModNotes" name="affOrderIdModNotes" />
							<div class="col-xs-12" align="center">
								<label>Note</label>
								<textarea class="form-control mb-20" id="affPaymentNote_ModNotes" name="affPaymentNoteModNotes" cols="50" rows="5"></textarea>
							</div>
						</div>
					</form>
				</div>
				
			</div>

		  </div>
		  <div class="modal-footer full-width">
				<button type="button" onclick="saveAffiliatePaymentNote();" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View/Modify Notes -->

<!-- popup Audit User -->
	<%@include file="body-user-audit.jsp" %>
<!-- End popup Audit User -->

<script>

var fromDate;
var toDate;
var affActivePromoCodeFlag = true;
function getDateForPromoCode(){
	$('#myModal').modal('show');
	fromDate = $('#fromDate').val();
	toDate = $('#toDate').val();
}

function userStatus(status) {	
	fromDate = $('#fromDate').val();
	toDate = $('#toDate').val();
	var paramString;
	
	var tempUserRowIndex = affiliatesAllGrid.getSelectedRows([0])[0];
	if(tempUserRowIndex >= 0) {
		var userId = affiliatesAllGrid.getDataItem(tempUserRowIndex).userId;
		if(userId==null || userId == 0){
			jAlert("Please select a user to update status.");
		}else{
			if((fromDate == '' || toDate == '') && status == 'ACTIVE'){
				jAlert("Please Select Effective From Date and To Date.");					
			}
			if(fromDate != '' && toDate != '' && status == 'ACTIVE'){
				var promoCode = $('#promoCode').val();
				if(affActivePromoCodeFlag){
					$('#myModal').modal('hide');
					paramString = "status="+status+"&fromDate="+fromDate+"&toDate="+toDate+"&promoCode="+promoCode; 
					
					$.ajax({				  
						url : "${pageContext.request.contextPath}/Client/UserStatusChange",
						type : "post",
						data : paramString+"&userId="+userId,
						success : function(response){
							if (response.msg == "true") {
								jAlert("User Updated Successfully.","Info");
								getAffiliatesAllGridData(0);
								return true;
							} else {
								jAlert(response.msg,"Info","Info");
								return false;
							}
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}else{
					jAlert("Please Change Promotional Code.", "info");					
				}
			}
			if(status == 'INACTIVE'){
				paramString = "status="+status;
				
				jConfirm("Are you sure to delete all Promotional Code ?","Confirm",function(r){
				if (r){
					$.ajax({				  
						url : "${pageContext.request.contextPath}/Client/UserStatusChange",
						type : "post",
						data : paramString+"&userId="+userId,
						success : function(response){
							if (response.msg == "true") {
								jAlert("User Updated Successfully.","Info");
								getAffiliatesAllGridData(0);
								return true;
							} else {
								jAlert(response.msg,"Info","Info");
								return false;
							}
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}else{
					return false;
				}
				});
			}
		}
	} else {
		jAlert("Select a user to update status.");
	}	
}

//When Active - Check Promo Code If already exist or Not
function checkActivePromoCode(){
	var promoCode = $('#promoCode').val();
	if(promoCode != null && promoCode != ""){
		$.ajax({
			url : "${pageContext.request.contextPath}/CheckAffiliatePromoCode",
			type : "get",
			data : "promoCode=" + promoCode,
			success : function(response){
				if(response.msg == "true"){
					affActivePromoCodeFlag = true;
					return true;
				}else{
					jAlert(response.msg, "Info");
					affActivePromoCodeFlag = false;
					return false;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				affActivePromoCodeFlag = false;
				return false;
			}
		});
	}else{
		affActivePromoCodeFlag = true;
		return true;
	}
}


//Affiliates User Grid
var pagingInfo;
var userId='';
var affiliatesAllGrid;
var affiliatesRowIndex;
var affiliatesAllDataView;
var affiliatesAllData=[];
var affiliatesAllGridSearchString='';
var columnFilters = {};
var affiliatesAllColumnsStr = '<%=session.getAttribute("affiliatesAllGrid")%>';
var affiliatesAllColumns =[];
/* var loadAffiliatesAllColumns = ["firstName", "lastName", "activeCash", "pendingCash", "lastCreditedCash", "lastDebitedCash", "totalCreditedCash", 
			"totalDebitedCash", "lastUpdate"]; */
var loadAffiliatesAllColumns = ["userName", "firstName", "lastName", "email", "phone", "status", "promotionalCode", "activeCash", 
			"pendingCash", "editCol", "audit"];
var affiliateGridColumns = [
				{id:"userId", name:"User ID", field: "userId",width:80, sortable: true},
				{id:"userName", name:"User Name", field: "userName",width:80, sortable: true},
				{id:"firstName", name:"First Name", field: "firstName",width:80, sortable: true},
				{id:"lastName", name:"Last Name", field: "lastName",width:80, sortable: true},
				{id:"email", name:"Email", field: "email",width:80, sortable: true},
				{id:"phone", name:"Phone", field: "phone",width:80, sortable: true},
				{id:"status", name:"Status", field: "status",width:80, sortable: true},
				{id:"promotionalCode", name:"Promotional Code", field: "promotionalCode",width:80, sortable: true},
               {id:"activeCash", name:"Active Cash", field: "activeCash",width:80, sortable: true},
               {id:"pendingCash", name:"Pending Cash", field: "pendingCash",width:80, sortable: true},
			   {id : "editCol", field : "editCol", name : "Edit", width:80, formatter : editFormatter},
			   {id : "audit", field : "delCol", name : "Audit", width:80, formatter : auditFormatter}
               /* {id : "delCol", field : "delCol", name : "Delete", width:80, formatter : buttonFormatter},
               {id:"lastCreditedCash", name:"Last Credited Cash", field: "lastCreditedCash",width:80, sortable: true},
               {id:"lastDebitedCash", name:"Last Debited Cash", field: "lastDebitedCash",width:80, sortable: true},
               {id:"totalCreditedCash", name:"Total Credited Cash", field: "totalCreditedCash",width:80, sortable: true},
               {id:"totalDebitedCash", name:"Total Debited Cash", field: "totalDebitedCash",width:80, sortable: true},
               {id:"lastUpdate", name:"Last Updated", field: "lastUpdate",width:80, sortable: true} */               
              ];
  
	if(affiliatesAllColumnsStr!='null' && affiliatesAllColumnsStr!=''){
		var columnOrder = affiliatesAllColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<affiliateGridColumns.length;j++){
				if(columnWidth[0] == affiliateGridColumns[j].id){
					affiliatesAllColumns[i] =  affiliateGridColumns[j];
					affiliatesAllColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadAffiliatesAllColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<affiliateGridColumns.length;j++){
				if(columnWidth == affiliateGridColumns[j].id){
					affiliatesAllColumns[i] = affiliateGridColumns[j];
					affiliatesAllColumns[i].width=80;
					break;
				}
			}			
		}
		//affiliatesAllColumns = affiliateGridColumns;
	}
  
var affiliateGridOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var affiliateSortCol = "userId";
var affiliateSortdir = 1;
var percentCompleteThreshold = 0;

//Now define your buttonFormatter function
/* function buttonFormatter(row, cell, value, columnDef, dataContext) {
	//the id is so that you can identify the row when the particular button is clicked
	var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.userId +"'/>";	
	return button;
} */

//function for audit functionality
function auditFormatter(row, cell, value, columnDef, dataContext) {
	//the id is so that you can identify the row when the particular button is clicked
	var button = "<img class='auditClickableImage' src='../resources/images/audit-icon.png' id='"+ dataContext.userId +"'/>";	
	return button;
}

//function for edit functionality
function editFormatter(row, cell, value, columnDef, dataContext) {
	//the id is so that you can identify the row when the particular button is clicked	
	var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.userId +"'/>";
	return button;
}

//Now you can use jquery to hook up your delete button event
$('.delClickableImage').live('click', function() {
	var me = $(this), id = me.attr('id');
	var delFlag = deleteUser(id);//confirm("Are you sure,Do you want to Delete it?");
});

//Now you can use jquery to hook up your delete button event
$('.auditClickableImage').live('click', function() {
	var me = $(this), id = me.attr('id');
	popupUserAudit(id);
	//dataView.deleteItem(id);
	//grid.invalidate();        
});

//Function to hook up the edit button event
$('.editClickableImage').live('click', function() {
	var me = $(this), id = me.attr('id');
	popupUserEdit(id);
});

function affiliateComparer(a, b) {
	  var x = a[affiliateSortCol], y = b[affiliateSortCol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	  	if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

function pagingControl(move,id){
	if(id=='affiliates_allpager'){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getAffiliatesAllGridData(pageNo);
	}
	if(id=='affiliate_pager'){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(affiliatePagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(affiliatePagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(affiliatePagingInfo.pageNum)-1;
		}		
		var userId = $('#userId').val();
		getAffiliateGridData(userId,0);
	}
	if(id == 'userAudit_pager'){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(userAuditPagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(userAuditPagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(userAuditPagingInfo.pageNum)-1;
		}	
		getUserAuditGridData(pageNo);
	}
}

function getAffiliatesAllGridData(pageNo) {
	if($('#activeDiv').hasClass('active')){
		$('#status').val('Active');
	}else if($('#inactiveDiv').hasClass('active')){
		$('#status').val('Inactive');
	}
	affiliatesRowIndex=-1;
	$.ajax({
		url : "${pageContext.request.contextPath}/Affiliates.json",
		type : "post",
		dataType: "json",
		data : "pageNo="+pageNo+"&headerFilter="+affiliatesAllGridSearchString+"&status="+$('#status').val(),
		success : function(res){
			var jsonData = res;
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				var msg =  jsonData.msg.replace(/"/g, '');
				if(msg != null && msg != ""){
					jAlert(msg);
				}
			}
			/*if(jsonData==null || jsonData=='') {
				jAlert("No Data Found.");
			}*/			
			pagingInfo = jsonData.pagingInfo;
			refreshAffiliatesAllGridValues(jsonData.affiliates);
			$('#userId').val('');
			refreshAffiliateGridValues('');
			clearAllSelections();
			$('#affiliates_allpager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveAffiliateUserPreference()'>");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function refreshAffiliatesAllGridValues(jsonData) {
	affiliatesAllData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (affiliatesAllData[i] = {});			
			d["id"] = i;
			d["userId"] = data.userId;
			d["userName"] = data.userName;
			d["firstName"] = data.firstName;
			d["lastName"] = data.lastName;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["status"] = data.status;
			d["promotionalCode"] = data.promotionalCode;
			d["activeCash"] = data.activeCash;
			d["pendingCash"] = data.pendingCash;
			/* d["lastCreditedCash"] = data.lastCreditedCash;
			d["lastDebitedCash"] = data.lastDebitedCash;
			d["totalCreditedCash"] = data.totalCreditedCash;
			d["totalDebitedCash"] = data.totalDebitedCash;
			d["lastUpdate"] = data.lastUpdate; */
		}
	}
	affiliatesAllDataView = new Slick.Data.DataView();
	affiliatesAllGrid = new Slick.Grid("#affiliatesAllGrid", affiliatesAllDataView, affiliatesAllColumns, affiliateGridOptions);
	affiliatesAllGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = affiliatesAllGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  affiliatesAllGrid.setColumns(colTest);
	  }
	  affiliatesAllGrid.invalidate();
	  affiliatesAllGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(pagingInfo!=null){
		  var pager = new Slick.Controls.Pager(affiliatesAllDataView, affiliatesAllGrid, $("#affiliates_allpager"),pagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(affiliateGridColumns,affiliatesAllGrid, affiliateGridOptions);
	 
	  affiliatesAllGrid.onSort.subscribe(function (e, args) {
	    affiliateSortdir = args.sortAsc ? 1 : -1;
	    affiliateSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      affiliatesAllDataView.fastSort(affiliateSortCol, args.sortAsc);
	    } else {
	      affiliatesAllDataView.sort(affiliateComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the affiliatesAllGrid
	  affiliatesAllDataView.onRowCountChanged.subscribe(function (e, args) {
	    affiliatesAllGrid.updateRowCount();
	    affiliatesAllGrid.render();
	  });
	  affiliatesAllDataView.onRowsChanged.subscribe(function (e, args) {
	    affiliatesAllGrid.invalidateRows(args.rows);
	    affiliatesAllGrid.render();
	  });
	  
	  $(affiliatesAllGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	affiliatesAllGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  affiliatesAllGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getAffiliatesAllGridData(0);
				}
			  }
		 
		});
	  	affiliatesAllGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'delCol' && args.column.id != 'editCol' && args.column.id != 'audit' && args.column.id != 'activeCash' && args.column.id != 'pendingCash'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		affiliatesAllGrid.init();
		
		affiliatesAllGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = affiliatesAllGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != affiliatesRowIndex) {
				affiliatesRowIndex = temprEventRowIndex;
				userId = affiliatesAllGrid.getDataItem(temprEventRowIndex).userId;
				getAffiliateGridData(userId,0);
			}
		});
	  	
		affiliatesAllGrid.onContextMenu.subscribe(function (e) {
	      e.preventDefault();
	      var cell = affiliatesAllGrid.getCellFromEvent(e);
	      affiliatesAllGrid.setSelectedRows([cell.row]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if(height < $("#contextMenuOther").height()){
				height = e.pageY - $("#contextMenuOther").height();
			}else{
				height = e.pageY;
			}
			if(width < $("#contextMenuOther").width()){
				width =  e.pageX- $("#contextMenuOther").width();
			}else{
				width =  e.pageX;
			}
		 $("#contextMenuOther")
	          .data("row", cell.row)
	          .css("top", height)
	          .css("left", width)
	          .show();
	      $("body").one("click", function () {
	        $("#contextMenuOther").hide();
	      });
	    });
		
	  affiliatesAllDataView.beginUpdate();
	  affiliatesAllDataView.setItems(affiliatesAllData);
	  
	  affiliatesAllDataView.endUpdate();
	  affiliatesAllDataView.syncGridSelection(affiliatesAllGrid, true);
	  $("#gridContainer").resizable();
	  $("div#divLoading").removeClass('show');
	  affiliatesAllGrid.resizeCanvas();
}	

$("#contextMenuOther").click(function (e) {
	if (!$(e.target).is("li")) {
	  return;
	}
	var index = affiliatesAllGrid.getSelectedRows([0])[0];
	if(index>=0){		
		if ($(e.target).attr("data") == 'view affiliate codes') {			
			viewHistory();
		}
		if ($(e.target).attr("data") == 'generate new affiliate code') {
			getDateForRegeneratePromoCode();
		}
	}else{
		jAlert("Please select affiliate to view/generate affiliate code.");
	}
});

function saveAffiliateUserPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = affiliatesAllGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('affiliatesAllGrid',colStr);
}

function deleteUser(userId) {
	if (userId == '') {
		jAlert("Userid found empty please refresh and try again.","Info");
		return false;
	}
	jConfirm("Are you sure to delete an User ?","Confirm",function(r){
		if (r) {
			$.ajax({
					url : "${pageContext.request.contextPath}/DeleteUser",
					type : "post",
					data : "userId="+ userId,
					success : function(response) {
						if(response.status == 1){
							getAffiliatesAllGridData(0);
							//jAlert("User Deleted successfully.","Info");
							//deleteRecordFromGrid(userId);							
							//return true;
						} /* else {
							jAlert(response.msg,"Info","Info");
							return false;
						} */
						if(response.msg != null && response.msg != "" && response.msg != undefined){
							jAlert(response.msg);
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
					});
		} else {
			return false;
		}
	});
	return false;
}

//popup user audit
function popupUserAudit(userId) {
	getUserAudit(userId);
	/* var url = "${pageContext.request.contextPath}/Client/AuditUsers?userId=" + userId;
	popupCenter(url, 'edit user', '600', '500'); */
}

//popup user edit
function popupUserEdit(userId) {
	getAffiliateDetailsForEdit(userId);
	/*var editUserUrl = "${pageContext.request.contextPath}/Client/EditUsers?userId=" + userId;
	popupCenter(editUserUrl, 'edit user', '800', '500');*/
}

//Affiliate Customer Order Grid
var affiliatePagingInfo;
var orderId='';
var affiliateGrid;
var eventrowIndex;
var affiliateDataView;
var affiliateData=[];
var affiliateGridSearchString='';
var affiliatesColumnFilters = {};
var userAffiliateColumnsStr = '<%=session.getAttribute("affiliategrid")%>';
var userAffiliateColumns =[];
var loadAffiliateColumns = ["orderId", "firstName", "lastName", "eventName", "eventDate", "eventTime", 
			"ticketQty", "orderTotal", "cashCredit", "status", "promoCode", "payOrder"];
var allAffiliateColumns = [
				{id:"orderId", name:"Order Id", field: "orderId",width:80, sortable: true},         
               {id:"firstName", name:"First Name", field: "firstName",width:80, sortable: true},
               {id:"lastName", name:"Last Name", field: "lastName",width:80, sortable: true},
               {id:"eventName", name:"Event Name", field: "eventName",width:80, sortable: true},
               {id:"eventDate", name:"Event Date", field: "eventDate",width:80, sortable: true},
               {id:"eventTime", name:"Event Time", field: "eventTime",width:80, sortable: true},
               {id:"ticketQty", name:"Quantity", field: "ticketQty",width:80, sortable: true},
               {id:"orderTotal", name:"Order Total", field: "orderTotal",width:80, sortable: true},
               {id:"cashCredit", name:"Affiliate Fee", field: "cashCredit",width:80, sortable: true},
               {id:"status", name:"Status", field: "status",width:80, sortable: true},
               {id:"promoCode", name:"Affiliate Code", field:"promoCode", width:80, sortable: true},
               /* {id:"paymentNote", name:"Payment Note", field:"paymentNote", width:80, sortable: true}, */
               {id:"payOrder", name:"Pay", field:"payOrder", width:80, sortable: true, formatter: payOrderFormatter}
              ];
  
	if(userAffiliateColumnsStr!='null' && userAffiliateColumnsStr!=''){
		var columnOrder = userAffiliateColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allAffiliateColumns.length;j++){
				if(columnWidth[0] == allAffiliateColumns[j].id){
					userAffiliateColumns[i] =  allAffiliateColumns[j];
					userAffiliateColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadAffiliateColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allAffiliateColumns.length;j++){
				if(columnWidth == allAffiliateColumns[j].id){
					userAffiliateColumns[i] = allAffiliateColumns[j];
					userAffiliateColumns[i].width=80;
					break;
				}
			}			
		}
		//userAffiliateColumns = allAffiliateColumns;
	}
  
var affiliateOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var affiliateSortCol = "orderId";
var affiliateSortdir = 1;
var percentCompleteThreshold = 0;

function affiliateComparer(a, b) {
	  var x = a[affiliateSortCol], y = b[affiliateSortCol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	  	if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

function payOrderFormatter(row, cell, value, columnDef, dataContext) {
	var orderStatus = affiliateGrid.getDataItem(row).status;
	var link = '';
	var userId = $('#userId').val();
	if(orderStatus == 'ACTIVE' || orderStatus == 'Active'){
		link = "<span class='label btn-primary' style='font-size:100%;' onclick='getAffiliatePaymentNote(userId, "+ affiliateGrid.getDataItem(row).orderId + ")'>Pay</span>";		
	}
	return link;
}
	
function getAffiliateGridData(userId, pageNo) {
	eventrowIndex=-1;
	$.ajax({
		url : "${pageContext.request.contextPath}/GetAffiliatesOrder",
		type : "post",
		dataType: "json",
		data : "userId="+userId+"&pageNo="+pageNo+"&headerFilter="+affiliateGridSearchString,
		success : function(res){
			var jsonData = res;
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				var msg =  jsonData.msg.replace(/"/g, '');
				if(msg != null && msg != ""){
					jAlert(msg);
				}
			}
			/* if(jsonData==null || jsonData=='' || jsonData.affiliatesOrder.length==0) {
				jAlert("No Data Found.");
			} */
			affiliatePagingInfo = jsonData.affiliatePagingInfo;
			refreshAffiliateGridValues(jsonData.affiliatesOrder);
			$('#userId').val(userId);
			//$('#affiliateOrder').show();
			clearAllSelections();
			$('#affiliate_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustomerPreference()'>");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
function refreshAffiliateGridValues(jsonData) {
	affiliateData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (affiliateData[i] = {});			
			d["id"] = i;
			d["orderId"] = data.orderId;
			d["firstName"] = data.firstName;
			d["lastName"] = data.lastName;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
			d["ticketQty"] = data.ticketQty;
			d["orderTotal"] = data.orderTotal;
			d["cashCredit"] = data.cashCredit;
			d["status"] = data.status;
			d["promoCode"] = data.promoCode;
			/* d["paymentNote"] = data.paymentNote; */
		}
	}
	affiliateDataView = new Slick.Data.DataView();
	affiliateGrid = new Slick.Grid("#affiliateGrid", affiliateDataView, userAffiliateColumns, affiliateOptions);
	affiliateGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = affiliateGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  affiliateGrid.setColumns(colTest);
	  }
	  affiliateGrid.invalidate();
	  affiliateGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(affiliatePagingInfo!=null){
		  var affiliatePager = new Slick.Controls.Pager(affiliateDataView, affiliateGrid, $("#affiliate_pager"),affiliatePagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(allAffiliateColumns,affiliateGrid, affiliateOptions);
	 
	  affiliateGrid.onSort.subscribe(function (e, args) {
	    affiliateSortdir = args.sortAsc ? 1 : -1;
	    affiliateSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      affiliateDataView.fastSort(affiliateSortCol, args.sortAsc);
	    } else {
	      affiliateDataView.sort(affiliateComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the affiliateGrid
	  affiliateDataView.onRowCountChanged.subscribe(function (e, args) {
	    affiliateGrid.updateRowCount();
	    affiliateGrid.render();
	  });
	  affiliateDataView.onRowsChanged.subscribe(function (e, args) {
	    affiliateGrid.invalidateRows(args.rows);
	    affiliateGrid.render();
	  });
	  
	  $(affiliateGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	affiliateGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				affiliatesColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in affiliatesColumnFilters) {
					  if (columnId !== undefined && affiliatesColumnFilters[columnId] !== "") {
						  affiliateGridSearchString += columnId + ":" +affiliatesColumnFilters[columnId]+",";
					  }
					}
					var userId = $('#userId').val();
					getAffiliateGridData(userId,0);
				}
			  }
		 
		});
	  	affiliateGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(affiliatesColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(affiliatesColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id != 'delCol' && args.column.id != 'payOrder'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(affiliatesColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		affiliateGrid.init();
		
		affiliateGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = affiliateGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				orderId = affiliateGrid.getDataItem(temprEventRowIndex).orderId;
			}
		});
	  	
		affiliateGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = affiliateGrid.getCellFromEvent(e);
		      affiliateGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY;
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
	  affiliateDataView.beginUpdate();
	  affiliateDataView.setItems(affiliateData);
	  
	  affiliateDataView.endUpdate();
	  affiliateDataView.syncGridSelection(affiliateGrid, true);
	  affiliateDataView.refresh();
	  $("#gridContainer").resizable();
	  affiliateGrid.resizeCanvas();
}	

$("#contextMenu").click(function (e) {
	if (!$(e.target).is("li")) {
	  return;
	}
	var index = affiliateGrid.getSelectedRows([0])[0];
	if(index>=0){
		var userId = $('#userId').val();
		var orderId = affiliateGrid.getDataItem(index).orderId;
		if ($(e.target).attr("data") == 'view order') {
			if(orderId>0){				
				getAffiliateOrderSummary(userId, orderId);
				/*var url = "${pageContext.request.contextPath}/AffiliateOrderDetails?userId="+userId+"&orderId="+ orderId;
				popupCenter(url, "View Order", "1000","600");*/
			}else{
				jAlert("Invoice is not Created yet for selected Order.");
			}
		}else if($(e.target).attr("data") == 'view modify notes'){
			getAffiliatePaymentNote(userId, orderId);
		}
	}else{
		jAlert("Please select Order to view details.");
	}
});

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    } 
} 

	//Start Edit Affiliates
	function getAffiliateDetailsForEdit(userId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Client/EditUsers",
			type : "post",
			data : "userId="+userId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData != null && jsonData != "") {
					if(jsonData.status == 1){
						setAffiliateDetailsForEdit(jsonData);
					}
					else{
						jAlert(jsonData.msg);
					}
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setAffiliateDetailsForEdit(jsonData){
		$('#edit-affiliates').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#editAff_successDiv').hide();
			$('#editAff_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#editAff_errorDiv').hide();
			$('#editAff_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#editAff_successDiv').show();
			$('#editAff_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#editAff_errorDiv').show();
			$('#editAff_errorMsg').text(jsonData.errorMessage);
		} */
		
		$('#editAff_userId').val(jsonData.userId);
		$('#editAff_status').val(jsonData.userStatus);
		$('#editAff_Pwd_userId').val(jsonData.userId);
		var trackerUser = jsonData.trackerUser;
		var trackerBroker = jsonData.trackerBroker;
		
		$('#userName').val(trackerUser.userName);
		$('#firstName').val(trackerUser.firstName);
		$('#lastName').val(trackerUser.lastName);
		$('#email').val(trackerUser.email);
		$('#phone').val(trackerUser.phone);
		$("#promotionalCode").val(trackerUser.promoCode);
		if($("#promotionalCode").val() != ''){
			$('#promoCode').show();
		}else{
			$('#promoCode').hide();
		}
		if(jsonData.status == true || jsonData.status == 'true'){
			$('#status_div').show();
		}else{
			$('#status_div').hide();
		}
		
		var affiliateSetting = jsonData.affiliateSetting;
		if(affiliateSetting != null){
			if(affiliateSetting.status != null){
				//$("#affiliateStatus option:contains(" + affiliateSetting.status + ")").attr('selected', 'selected');
				$("#affiliateStatus").val(affiliateSetting.status);
			}
			if(affiliateSetting.isRepeatBusiness != null && affiliateSetting.isRepeatBusiness == true){
				//$("#affiliateBusiness option:contains('YES')").attr('selected', 'selected');
				$("#affiliateBusiness").val('YES');
			}else{
				//$("#affiliateBusiness option:contains('NO')").attr('selected', 'selected');
				$("#affiliateBusiness").val('NO');
			}
			if(affiliateSetting.isEarnRewardPoints != null && affiliateSetting.isEarnRewardPoints == true){
				//$("#affiliateBusiness option:contains('YES')").attr('selected', 'selected');
				$("#canEarnRewardPoints").val('YES');
			}else{
				//$("#affiliateBusiness option:contains('NO')").attr('selected', 'selected');
				$("#canEarnRewardPoints").val('NO');
			}
			$('#affiliateDiscount').val(affiliateSetting.cashDiscount);
			$('#customerOrderDiscount').val(affiliateSetting.customerDiscount);
			$('#phoneAffiliateDiscount').val(affiliateSetting.phoneCashDiscount);
			$('#phoneCustomerOrderDiscount').val(affiliateSetting.phoneCustomerDiscount);
		}else{
			$("#affiliateStatus").val('GOLD');
			$("#affiliateBusiness").val('NO');
			$("#canEarnRewardPoints").val('NO');
			$('#affiliateDiscount').val('');
			$('#customerOrderDiscount').val('');
			$('#phoneAffiliateDiscount').val('');
			$('#phoneCustomerOrderDiscount').val('');
		}	
		
		$("#password").val('');
		$("#repassword").val('');
		$('#affiliateEmail_Hdr').text(trackerUser.email);
	}
	
	function doEditAffiliateValidations(){
		if($("#firstName").val() == ''){
			jAlert("Firstrname can't be blank","Info");
			return false;
		}else if($("#lastName").val() == ''){
			jAlert("Lastname can't be blank","Info");
			return false;
		}else if($("#email").val() == ''){
			jAlert("Email can't be blank","Info");
			return false;
		}else if(validateEmail($('#email').val()) == false){
			jAlert("Invalid Email.","Info");
			return false;
		}else if($('#affiliateDiscount').val() == '' || isNaN($('#affiliateDiscount').val())){
			jAlert("Please Add valid numeric value for Affiliates Discount(RTF Orders) %.","Info");
			return false;
		}else if($('#customerOrderDiscount').val() == '' || isNaN($('#customerOrderDiscount').val())){
			jAlert("Please Add valid numeric value for Customer discount(RTF Orders).","Info");
			return false;
		}else if($('#phoneAffiliateDiscount').val() == '' || isNaN($('#phoneAffiliateDiscount').val())){
			jAlert("Please Add valid numeric value for Affiliates Discount(Phone Orders) %.","Info");
			return false;
		}else if($('#phoneCustomerOrderDiscount').val() == '' || isNaN($('#phoneCustomerOrderDiscount').val())){
			jAlert("Please Add valid numeric value for Customer discount(Phone Orders).","Info");
			return false;
		}else{
			$.ajax({
					url : "${pageContext.request.contextPath}/CheckUserForEdit.json",
					type : "get",
					data : "userName="+ $("#userName").val() + "&email=" + $("#email").val() + "&userId=" + $('#editAff_userId').val(),
					/* async : false, */
					
					success : function(response){
						if(response.msg == "true"){
							$("#editAff_action").val('editUserInfo');
							/*$('#editUserForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
							$("#editUserForm").submit();*/
							$.ajax({
								url : "/Client/EditUsers",
								type : "post",
								dataType : "json",
								data : $("#editUserForm").serialize(),
								success : function(res) {
									var jsonData = JSON.parse(JSON.stringify(res));
									if(jsonData != null && jsonData != ""){
										if(jsonData.status == 1){											
											getAffiliatesAllGridData(0);
										}
										jAlert(jsonData.msg);
									}
									/* if(jsonData.successMessage != null && jsonData.successMessage != ""){
										//$('#editAff_successDiv').show();
										//$('#editAff_successMsg').text(jsonData.successMessage);
										jAlert(jsonData.successMessage);
										getAffiliatesAllGridData(0);
									}
									if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
										//$('#editAff_errorDiv').show();
										//$('#editAff_errorMsg').text(jsonData.errorMessage);
										jAlert(jsonData.errorMessage);
									} */
								},
								error : function(error) {
									jAlert("Your login session is expired please refresh page and login again.", "Error");
									return false;
								}
							});
						}else{
							jAlert(response.msg,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		}		
	}
	
	function doEdAffChangePasswordValidations(){
		if($("#password").val() == ''){
			jAlert("Password can't be blank","Info");
			return false;
		}else if($("#repassword").val() == ''){
			jAlert("Re-Password can't be blank","Info");
			return false;
		}else if($("#password").val() != $("#repassword").val()){
			jAlert("Password and Re-Password must match","Info");
			return false;
		}else{
			$("#actionChangepassword").val('changePassword');
			/*$('#changePasswordForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
			$("#changePasswordForm").submit();*/
			$.ajax({
				url : "/Client/EditUsers",
				type : "post",
				dataType : "json",
				data : $("#changePasswordForm").serialize(),
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData != null && jsonData != ""){
						if(jsonData.status == 1){}
						jAlert(jsonData.msg);
					}
					/* if(jsonData.successMessage != null && jsonData.successMessage != ""){
						//$('#editAff_successDiv').show();
						//$('#editAff_successMsg').text(jsonData.successMessage);
						jAlert(jsonData.successMessage);
					}
					if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
						//$('#editAff_errorDiv').show();
						//$('#editAff_errorMsg').text(jsonData.errorMessage);
						jAlert(jsonData.errorMessage);
					} */
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	}
	
	var regFromDate;
	var regToDate;
	var regPromoCode;
	function getDateForRegeneratePromoCode(){
		var userId = $('#userId').val();
		if(userId == '' || userId == null){
			jAlert("Please select User to Generate New Affiliate Code.","Info");
			return false;
		}else{
			$('#date-modal-3').modal('show');
			regFromDate = $('#regFromDate').val();
			regToDate = $('#regToDate').val();
			promoCode = $('#regPromoCode').val();
		}
	}
	
	var affiliateFlag = true;
	function regeneratePromoCode(){		
		regFromDate = $('#regFromDate').val();
		regToDate = $('#regToDate').val();
		regPromoCode = $('#regPromoCode').val();
		var userId = $('#userId').val();		
		if(affiliateFlag){
			if(regToDate != '' || regToDate != null){
				$('#date-modal-3').modal('hide');
				$.ajax({
					url : "${pageContext.request.contextPath}/GenerateAffiliatePromoCode",
					type : "post",
					data : "userId="+userId+"&fromDate="+regFromDate+"&toDate="+regToDate+"&promoCode="+regPromoCode,
					success : function(response){
						if(response.status == 1){
							jAlert("Promotional Code Generated.", "Info");
							$('#promotionalCode').val(response.msg);
							getAffiliatesAllGridData(0);
						}else{
							jAlert(response.msg, "Info");
						}
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		}else{
			jAlert('Please change Promotional Code.', "Info");
			return false;
		}
	}
	
	function checkPromoCode(){
		var promoCode = $('#regPromoCode').val();
		if(promoCode != null && promoCode != ""){
			$.ajax({
				url : "${pageContext.request.contextPath}/CheckAffiliatePromoCode",
				type : "get",
				data : "promoCode=" + promoCode,
				success : function(response){
					if(response.msg == "true"){
						affiliateFlag = true;
						return true;
					}else{
						jAlert(response.msg, "Info");
						affiliateFlag = false;
						return false;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					affiliateFlag = false;
					return false;
				}
			});
		}else{
			affiliateFlag = true;
			return true;
		}
	}
	//End Edit Affiliates
	
	//Start Promo Code History
	function viewHistory(){
		var userId = $('#userId').val();
		if(userId == '' || userId == null){
			jAlert("Please select User to View Affiliate Codes.","Info");
			return false;
		}else{
			getAffiliatePromoCodeHistory(userId);
		}
		/*var viewHistoryUrl = "${pageContext.request.contextPath}/GetAffiliatePromoHistory?userId="+userId;
		popupCenter(viewHistoryUrl, 'Promotional Code History', '700', '500');*/
	}
	
	function getAffiliatePromoCodeHistory(userId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/GetAffiliatePromoHistory",
			type : "post",
			data : "userId="+userId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData==null || jsonData=="") {
					jAlert("No Promotional Code History Found.");
				} else {
					setAffiliatePromoCodeHistory(jsonData);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setAffiliatePromoCodeHistory(jsonData){
		$('#view1').modal('show');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#promoHistory_successDiv').hide();
			$('#promoHistory_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#promoHistory_errorDiv').hide();
			$('#promoHistory_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#promoHistory_successDiv').show();
			$('#promoHistory_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#promoHistory_errorDiv').show();
			$('#promoHistory_errorMsg').text(jsonData.errorMessage);
		}
		promoCodeHistoryGridValues(jsonData.promoCodeHistory, jsonData.pagingInfo);
	}
	//End Promo Code History
	
	//Start - View/Pay Note (Modify Notes)
	function getAffiliatePaymentNote(userId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/getAffiliatePaymentNote",
			type : "post",
			data : "userId="+userId+"&orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					viewAffCusModifyNotes(jsonData);
					$('#affOrderId_Hdr_ModNotes').text(orderId);
				}else{
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	function viewAffCusModifyNotes(jsonData){
		$('#affOrderId_ModNotes').val(jsonData.orderId);
		$('#affPaymentNote_ModNotes').val(jsonData.paymentNote);
		$('#view-modify-notes').modal('show');
	}

	function saveAffiliatePaymentNote(){
		if($('#affPaymentNote_ModNotes').val() == ''){
			jAlert("Please provide Payment Note.");
			return false;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/saveAffiliatePaymentNote",
				type : "post",
				data : $("#editAffPaymentNote").serialize()+"&userId="+$('#userId').val(),
				dataType : "json",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#view-modify-notes').modal('hide');
						affiliateResetFilters();
					}
					jAlert(jsonData.msg);
					//$('#ModifyNotesMsg').show();
					//$('#ModifyNotesMsg').text(res);
					
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	//End Modify Notes Script

	jQuery(document).ready(function () {
		var winSize = jQuery(window).width();
		if (winSize <= 991) {
			setTimeout(function () {
				var grid_top2 = jQuery('.grid-table-2').offset().top + 90;
				jQuery('ul#contextMenu').css('top',grid_top2);
			}, 4000);
		}
	});
</script>