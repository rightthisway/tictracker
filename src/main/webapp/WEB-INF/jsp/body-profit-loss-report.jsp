<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>


<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
});
</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Reports</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="/Reports/Home">Reports</a></li>
						<li><i class="fa fa-laptop"></i>PO/Invoice Report</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
</div>

<div class="row">
           <div class="col-xs-12 filters-div">
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="fromDate" class="control-label">Start Date</label> 
					<input class="form-control searchcontrol" type="text" id="startDate" name="startDate" value="${startDate}">					
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">End Date</label> 
					<input class="form-control searchcontrol" type="text" id="endDate" name="endDate" value="${endDate}">
				</div>
           </div>  
           <div class="col-xs-12 filters-div">
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<button type="button" id="searchSeatGeekBtn" class="btn btn-primary" onclick="downloadRTFInvoiceReport();" style="margin-top:19px;">RTF Invoice Report</button>
				</div>
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<button type="button" id="searchSeatGeekBtn" class="btn btn-primary" onclick="downloadRTFPurchaseOrderReport();" style="margin-top:19px;">RTF Purchase Order Report</button>
				</div>
           </div>
</div>

<script>
function downloadRTFInvoiceReport(){
	window.location.href = "/Reports/DownloadRTFInvoiceReport?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val();
}

function downloadRTFPurchaseOrderReport(){
	window.location.href = "/Reports/DownloadRTFPurchaseOrderReport?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val();
}
</script>