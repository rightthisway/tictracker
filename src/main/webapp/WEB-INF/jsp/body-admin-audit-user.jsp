<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
        startDate : "01-01-2016",
        endDate: "${endDate}"
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
        startDate : "01-01-2016",
        endDate: "${endDate}"
    });
});

function doAuditUserValidations(){
	if($("#fromDate").val() == ''){
		jAlert("From date can't be blank.","Info");
		return false;
	}else if($("#toDate").val() == ''){
		jAlert("To date can't be blank.","Info");
		return false;
	}else{
		$("#auditUserForm").attr('action','${pageContext.request.contextPath}/Admin/AuditUser?userId='+$("#userId").val());
		$("#auditUserForm").submit();
	}
}
</script>

<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Admin</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
						<li><i class="fa fa-laptop"></i>Audit User</li>
					</ol>
					
				</div>
				
				<c:if test="${msg != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
				</c:if>
				
				<form class="form-validate form-horizontal" id="auditUserForm" method="post">
                       <input class="form-control" id="action" value="action" name="action" type="hidden" />
                       
                       		<table class="table table-striped table-advance table-hover">
                       		<tbody>
                       		<tr>
                       		<td>
                            	Date *
                            </td>
                            <td>
                                	<input style="width:180px;" id="fromDate" name="fromDate" value="${fromDate}" type="text"/>
									<input style="width:180px;" id="toDate" name="toDate" value="${toDate}" type="text"/>
								
                             </td>
                             </tr>
                             <tr>
                             	<td>
                             		User *
                             	</td>
                             	<td>
                             		<select style="width:18%;" id="userId">
                                       <c:forEach items="${allTrackerUserList}" var="trackerUser">
                                       		<option name="userId" value="${trackerUser.id}"
                                       		<c:if test="${userId == trackerUser.id}">selected</c:if>>
                                       		${trackerUser.firstName} ${trackerUser.lastName}</option>
                                       </c:forEach>
                                    </select>
                             	</td>
                             </tr>
                             <tr>
                             <td></td>
                             	<td>
                             		<button class="btn btn-primary" onclick="doAuditUserValidations()" type="button">Submit</button>
                             	</td>
                             </tr>
                             </tbody>
                             </table>
				</form>
</div>
<c:choose>
	<c:when test="${not empty userActionList}">
		<display:table id="userAction" name="${userActionList}" excludedParams="*" pagesize="10" requestURI="AuditUser?userId=${userId}" class="table table-striped table-advance table-hover">
			<display:column title='<i class="icon_pin_alt"></i> IP Address' property="ipAddress" sortable="true"></display:column>
			<display:column title='<i class="icon_profile"></i> First Name' property="trackerUser.firstName" sortable="true"></display:column>
			<display:column title='<i class="icon_profile"></i> Last Name' property="trackerUser.lastName" sortable="true"></display:column>
			<display:column title='<i class="icon_cogs"></i> Action' property="action" sortable="true"></display:column>
			<display:column title='<i class="icon_calendar"></i> Time' property="timeStampStr" sortable="true"></display:column>
		</display:table>
	</c:when>
	<c:otherwise>
		<center><label>Nothing Found To Display.</label></center>
	</c:otherwise>
</c:choose>
