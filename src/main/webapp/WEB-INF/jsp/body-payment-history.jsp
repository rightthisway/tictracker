<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<script>
var prodType;
$(document).ready(function(){
	$("div#divLoading").addClass('show');
});
window.onresize = function(){
	paymentHistoryGrid.resizeCanvas();
	rtwpaymentHistoryGrid.resizeCanvas();
}
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Payment History</li>
		</ol>
	</div>
</div>
<br />

<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
	<br />
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
	<br />
</c:if>
<div style="position: relative" id="paymentHistory">
	<div style="width: 100%;">
		<c:if test="${productType == 'REWARDTHEFAN'}">
			<div class="grid-header" style="width: 100%">
				<label>Payment History</label> <span id="openOrder_grid_toogle_search" style="float: right"
					class="ui-icon ui-icon-search" title="Toggle search panel"></span>
			</div>
			<div id="paymentHistory_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
			<div id="paymentHistory_pager" style="width: 100%; height: 10px;"></div>
		</c:if>
		
		<c:if test="${productType != 'REWARDTHEFAN'}">
			<div class="grid-header" style="width: 100%">
				<label>Payment History</label> <span id="rtwopenOrder_grid_toogle_search" style="float: right"
					class="ui-icon ui-icon-search" title="Toggle search panel"></span>
			</div>
			<div id="rtwpaymentHistory_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
			<div id="rtwpaymentHistory_pager" style="width: 100%; height: 10px;"></div>			
		</c:if>
		
	</div>
</div>
<!--
<div id="inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  					Show records with payment history including <input type="text" id="txtSearch2">
</div>
-->
<script>
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var paymentHistoryDataView;
var paymentHistoryGrid;
var paymentHistoryData = [];
var paymentHistoryColumns = [ {
	id : "primaryPaymentMethod",
	name : "Primary Payment Method",
	field : "primaryPaymentMethod",
	sortable : true
},{
	id : "secondaryPaymentMethod",
	name : "Secondary Payment Method",
	field : "secondaryPaymentMethod",
	sortable : true
},{
	id : "thirdPaymentMethod",
	name : "Third Payment Method",
	field : "thirdPaymentMethod",
	sortable : true
},{
	id : "primaryAmount",
	name : "Primary Amount",
	field : "primaryAmount",
	sortable : true
},{
	id : "secondaryAmount",
	name : "Secondary Amount",
	field : "secondaryAmount",
	sortable : true
},{
	id : "thirdAmount",
	name : "Third Amount",
	field : "thirdAmount",
	sortable : true
}, {
	id : "paidOn",
	name : "Paid On",
	field : "paidOn",
	width:80,
	sortable : true
}, {
	id : "primaryTransaction",
	name : "Primary Transaction Id",
	field : "primaryTransaction",
	sortable : true
}, {
	id : "secondaryTransaction",
	name : "Secondary Transaction Id",
	field : "secondaryTransaction",
	sortable : true
},{
	id : "thirdTransaction",
	name : "Third Transaction Id",
	field : "thirdTransaction",
	sortable : true
},{id:"csr", name:"CSR", field: "csr", sortable: true}];

var paymentHistoryOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
};
var paymentHistorySortcol = "id";
var paymentHistorySortdir = 1;
var paymentHistorySearchString = "";

function paymentHistoryFilter(item, args) {
	var x= item["invoiceId"];
	if (args.eventGridSearchString  != ""
			&& x.indexOf(args.eventGridSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function paymentHistoryComparer(a, b) {
	var x = a[paymentHistorySortcol], y = b[paymentHistorySortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function refreshPaymentHistoryGridValues() {
		var i = 0;
		<c:forEach var="payment" items="${paymentHistory}">
			var d = (paymentHistoryData[i] = {});
			d["id"] = i;
			d["primaryPaymentMethod"] = '${payment.primaryPaymentMethod}';
			d["secondaryPaymentMethod"] = '${payment.secondaryPaymentMethod}';
			d["thirdPaymentMethod"] = '${payment.thirdPaymentMethod}';
			d["primaryAmount"] = '${payment.primaryAmount}';
			d["secondaryAmount"] = '${payment.secondaryAmount}';
			d["thirdAmount"] = '${payment.thirdPayAmt}';
			d["paidOn"] = '${payment.paidOnStr}';
			d["secondaryTransaction"] = '${payment.secondaryTransaction}';
			d["primaryTransaction"] = '${payment.primaryTransaction}';
			d["thirdTransaction"] = '${payment.thirdTransactionId}';
			d["csr"] = '${payment.csr}';
			i++;
		</c:forEach>
	
			
		paymentHistoryDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		paymentHistoryGrid = new Slick.Grid("#paymentHistory_grid", paymentHistoryDataView, paymentHistoryColumns, paymentHistoryOptions);
		paymentHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		paymentHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		var paymentHistoryPager = new Slick.Controls.Pager(paymentHistoryDataView, paymentHistoryGrid, $("#paymentHistory_pager"),pagingInfo);
		var paymentHistoryColumnpicker = new Slick.Controls.ColumnPicker(paymentHistoryColumns, paymentHistoryGrid,
			paymentHistoryOptions);

	paymentHistoryGrid.onSort.subscribe(function(e, args) {
		paymentHistorySortdir = args.sortAsc ? 1 : -1;
		paymentHistorySortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			paymentHistoryDataView.fastSort(paymentHistorySortcol, args.sortAsc);
		} else {
			paymentHistoryDataView.sort(paymentHistoryComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	paymentHistoryDataView.onRowCountChanged.subscribe(function(e, args) {
		paymentHistoryGrid.updateRowCount();
		paymentHistoryGrid.render();
	});
	paymentHistoryDataView.onRowsChanged.subscribe(function(e, args) {
		paymentHistoryGrid.invalidateRows(args.rows);
		paymentHistoryGrid.render();
	});

	// initialize the model after all the events have been hooked up
	paymentHistoryDataView.beginUpdate();
	paymentHistoryDataView.setItems(paymentHistoryData);
	paymentHistoryDataView.endUpdate();
	paymentHistoryDataView.syncGridSelection(paymentHistoryGrid, true);
	$("#gridContainer").resizable();
	paymentHistoryGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
}

var rtwpaymentHistoryDataView;
var rtwpaymentHistoryGrid;
var rtwpaymentHistoryData = [];
var rtwpaymentHistoryColumns = [ {
	id : "creditCardNumber",
	name : "Credit Card Number",
	field : "creditCardNumber",
	sortable : true
},{
	id : "checkNumber",
	name : "Check Number",
	field : "checkNumber",
	sortable : true
},{
	id : "amount",
	name : "Amount",
	field : "amount",
	sortable : true
},{
	id : "note",
	name : "Note",
	field : "note",
	sortable : true
}, {
	id : "paidOn",
	name : "Paid On",
	field : "paidOn",
	width:80,
	sortable : true
}, {
	id : "systemUser",
	name : "System User",
	field : "systemUser",
	sortable : true
}, {
	id : "paymentType",
	name : "Payment Type",
	field : "paymentType",
	sortable : true
},{
	id : "transOffice",
	name : "Transaction Office",
	field : "transOffice",
	sortable : true
}];

var rtwpaymentHistoryOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
};
var rtwpaymentHistorySortcol = "id";
var rtwpaymentHistorySortdir = 1;
var percentCompleteThreshold = 0;
var rtwpaymentHistorySearchString = "";

function rtwpaymentHistoryFilter(item, args) {
	var x= item["invoiceId"];
	if (args.rtwpaymentHistorySearchString  != ""
			&& x.indexOf(args.rtwpaymentHistorySearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.rtwpaymentHistorySearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function rtwpaymentHistoryComparer(a, b) {
	var x = a[rtwpaymentHistorySortcol], y = b[rtwpaymentHistorySortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function paymentToggleFilterRow() {
	rtwpaymentHistoryGrid.setTopPanelVisibility(!rtwpaymentHistoryGrid.getOptions().showTopPanel);
}
	
$("#rtwopenOrder_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});

function refreshrtwPaymentHistoryGridValues() {
		var i = 0;
		<c:forEach var="rtwpayment" items="${paymentHistory}">
			var d = (rtwpaymentHistoryData[i] = {});
			d["id"] = i;
			d["creditCardNumber"] = '${rtwpayment.creditCardNumber}';
			d["checkNumber"] = '${rtwpayment.checkNumber}';
			d["amount"] = '${rtwpayment.amount}';
			d["note"] = '${rtwpayment.note}';
			d["paidOn"] = '${rtwpayment.paidOnStr}';
			d["systemUser"] = '${rtwpayment.systemUser}';
			d["paymentType"] = '${rtwpayment.paymentType}';
			d["transOffice"] = '${rtwpayment.transOffice}';
			i++;
		</c:forEach>
	
			
		rtwpaymentHistoryDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		rtwpaymentHistoryGrid = new Slick.Grid("#rtwpaymentHistory_grid", rtwpaymentHistoryDataView, rtwpaymentHistoryColumns, rtwpaymentHistoryOptions);
		rtwpaymentHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		rtwpaymentHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		var rtwpaymentHistoryPager = new Slick.Controls.Pager(rtwpaymentHistoryDataView, rtwpaymentHistoryGrid, $("#rtwpaymentHistory_pager"),pagingInfo);
		var rtwpaymentHistoryColumnpicker = new Slick.Controls.ColumnPicker(rtwpaymentHistoryColumns, rtwpaymentHistoryGrid,
			rtwpaymentHistoryOptions);

		// move the filter panel defined in a hidden div into rtwpaymentHistoryGrid top panel
	  //$("#inlineFilterPanel").appendTo(rtwpaymentHistoryGrid.getTopPanel()).show();
	  
	  rtwpaymentHistoryGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < rtwpaymentHistoryDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    rtwpaymentHistoryGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  
	rtwpaymentHistoryGrid.onSort.subscribe(function(e, args) {
		rtwpaymentHistorySortdir = args.sortAsc ? 1 : -1;
		rtwpaymentHistorySortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			rtwpaymentHistoryDataView.fastSort(rtwpaymentHistorySortcol, args.sortAsc);
		} else {
			rtwpaymentHistoryDataView.sort(rtwpaymentHistoryComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	rtwpaymentHistoryDataView.onRowCountChanged.subscribe(function(e, args) {
		rtwpaymentHistoryGrid.updateRowCount();
		rtwpaymentHistoryGrid.render();
	});
	rtwpaymentHistoryDataView.onRowsChanged.subscribe(function(e, args) {
		rtwpaymentHistoryGrid.invalidateRows(args.rows);
		rtwpaymentHistoryGrid.render();
	});

	// wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    rtwpaymentHistorySearchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    rtwpaymentHistoryDataView.setFilterArgs({
	      rtwpaymentHistorySearchString: rtwpaymentHistorySearchString
	    });
	    rtwpaymentHistoryDataView.refresh();
	}
	  
	// initialize the model after all the events have been hooked up
	rtwpaymentHistoryDataView.beginUpdate();
	rtwpaymentHistoryDataView.setItems(rtwpaymentHistoryData);
	
	rtwpaymentHistoryDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    rtwpaymentHistorySearchString: rtwpaymentHistorySearchString
	  });
	rtwpaymentHistoryDataView.setFilter(rtwpaymentHistoryFilter);
	  
	rtwpaymentHistoryDataView.endUpdate();
	rtwpaymentHistoryDataView.syncGridSelection(rtwpaymentHistoryGrid, true);
	$("#gridContainer").resizable();
	rtwpaymentHistoryGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
}

window.onload = function() {
	prodType = "${productType}";
	if(prodType == 'REWARDTHEFAN'){
		refreshPaymentHistoryGridValues();
	}else{
		refreshrtwPaymentHistoryGridValues();
	}
};

</script>