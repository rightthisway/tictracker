<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">
	
</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Clients & Brokers & Customers</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Manage Details</a></li>
						<li><i class="fa fa-laptop"></i>Add Client Broker</li>						  	
					</ol>
				</div>
</div>

<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Customer Info
                          </header>
                          <form class="form-validate form-horizontal" id="loginForm" method="post" action="${pageContext.request.contextPath}/Admin/AddUser" >
                          <div class="panel-body">
                              <div class="form">
                              <%-- <c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if> --%>
                                  
                                      <!-- <input class="form-control" id="action" value="action" name="action" type="hidden" /> -->
                                      
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Type <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <select class="form-control input-sm m-bot15">
                                              <option>Broker</option>
                                          </select>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Company Name <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="companyName" name="companyName" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">E-Mail </label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="email" type="email" name="email"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Website URL </label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id=""website"" name="website" type="text"/>
                                          </div>
                                      </div>                                      
                                      <div class="form-group ">
                                          <label for="ccomment" class="control-label col-lg-2">Fed Ex Account # </label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="fedexNo" name="fedexNo" />
                                          </div>
                                      </div>
                              </div>

                          </div>
                          <header class="panel-heading">
                              Phone
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">International <input type="checkbox" id="internationalPhone" name="internationalPhone" />&nbsp&nbspMain<span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="mainPhone" name="mainPhone" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Main Extension </label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="mainExtension" name="mainExtension" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">Fax </label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="fax" type="text" name="fax"/>
                                          </div>
                                      </div>                                      
                                      <div class="form-group ">
                                          <label for="ccomment" class="control-label col-lg-2">Other Phone </label>
                                          <div class="col-lg-10">
                                              <select class="form-control input-sm m-bot15">
                                              <option>Billing</option>
                                          </select>
                                          	  <input class="form-control " id="company" name="company" />
                                          </div>
                                      </div>
                                  
                              </div>
							
                          </div>
                          
                          <header class="panel-heading">
                              Address
                          </header>
                          <div class="panel-body">
                              <div class="form">
                              		  <div class="form-group ">
                                          <label for="ccomment" class="control-label col-lg-2">Type <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                          <select class="form-control input-sm m-bot15" name="addressType">
                                              <option>Main</option>
                                          </select>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Street1 <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="Street1" name="Street1" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Street2 </label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="Street2" name="Street2" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">Zip </label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="zip" type="text" name="zip"/>
                                          </div>
                                      </div>                                      
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">City <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="city" type="text" name="city"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">State </label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="state" type="text" name="state"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">Country <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="country" type="text" name="country"/>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="button">Save</button>
                                          </div>
                                      </div>
                                  
                              </div>
							
                          </div>
                          
                          </form>
                          
                      </section>
                  </div>
              </div>