<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

input {
	color: black !important;
}
</style>

<script>
var jq2 = $.noConflict(true);
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
		
	$("#childCategoryTab").click(function(){
		callTabOnChange('CrownJewelChild');
	});
	
	$("#grandChildCategoryTab").click(function(){
		callTabOnChange('CrownJewelGrandChild');
	});
	
		
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshChildCategoryGridValues(JSON.parse(JSON.stringify(${childCategory})));
		$('#childCategory_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserChildCategoryPreference()'>");
	});
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  childCategoryGrid.resizeCanvas();
	});
	
		
});

	function callTabOnChange(selectedTab) {		
		var data = selectedTab;			
		window.location = "${pageContext.request.contextPath}/CrownJewelEvents/"+data;
	}


	function saveUserChildCategoryPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = childCategoryGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('childcategorygrid',colStr);
	}

	function resetFilters(){
		$('#action').val('search');
		childCategoryGridSearchString='';
		columnFilters = {};
		getChildCategoryGridData(0);
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+childCategoryGridSearchString;
	    //var url = "${pageContext.request.contextPath}/CrownJewelEvents/ChildCategoryExportToExcel?"+appendData;
	    var url = apiServerUrl+"ChildCategoryExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
</script>

<div class="row">	
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Fantasy Sports Tickets
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Fantasy Sports Tickets</a></li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Child/Grand Child Category</li>
		</ol>
	</div>
</div>

<div id="childCategoryDiv">
	<div class="full-width">
		<section class="panel">
		<ul class="nav nav-tabs" style="">
			<li class="active"><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="childCategoryTab" href="#childCategory">Child Category</a></li>
			<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="grandChildCategoryTab" href="#grandChildCategory">Grand Child Category</a></li>
		</ul>
		</section>
	</div>
	<div class="full-width">
		<div class="tab-content">
		
		<div id="AddChildCategorySuccessMsg" style="display: none;" class="alert alert-success fade in">
			Child Category Created/Updated successfully.
		</div>
		<div id="">
			<b> APPLICABLE ONLY FOR SPORTS CATEGORY </b> 
		</div>
		<div>
				<button type="button" style="margin-top:18px;" class="btn btn-primary" id="addChildCategory" onclick="addModal();">Add Child Category</button>
				<button type="button" style="margin-top:18px;" class="btn btn-primary" id="removeChildCategory" onclick="removeChildCategory()">Remove Child Category</button>
		</div>
		<br/>
			<div id="childCategory" class="tab-pane active">
				<div class="table-responsive grid-table">
					<div class="grid-header" style="width: 100%">
						<label>Child Category</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'> Reset Filters &nbsp; | </a>
						</div>
					</div>
					<div id="childCategory_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="childCategory_pager" style="width: 100%; height: 10px;"></div>
				</div>
			</div>
			<div id="grandChildCategory"></div>
		</div>
	</div>
</div>

<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Child Category</button> -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content full-width">
               <div class="modal-header full-width">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Add Child Category</h4>
               </div>
               <div class="modal-body full-width">
					
				   <form role="form" class="form-horizontal" id="addChildCategoryForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/updateChildCategory">
					<input id="action" value="create" name="action" type="hidden" />
					<input type="hidden" id="id" name="id">
					<div class="tab-fields full-width">									
						<div class="form-group col-sm-4 col-xs-6">
							<label>Parent Category</label>
								<select id="parentId" name="parentId" class="form-control ">
								  <option value="-1">--select--</option>
								  <c:forEach items="${parentCategoryList}" var="parentCat">
									<option value="${parentCat.id}">
										 ${parentCat.name}
									</option>
								</c:forEach>
								</select>
						</div>
						<div class="form-group col-sm-4 col-xs-6">
							<label>Child Category</label> 
							<input class="form-control"	id="name" type="text" name="name"/>
						</div>
					   </div>
						</form>
					</div>
					<div class="modal-footer full-width">
						<button class="btn btn-primary" type="button" onclick="addChildCategory();" >Save</button>
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					</div>
                 </div>
             </div>
        </div>
<script>
var childCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	var pagingInfo;
	var childCategoryView;
	var childCategoryGrid;
	var childCategoryData = [];
	var childCategoryGridSearchString='';
	var columnFilters = {};
	var userChildCategoryColumnsStr = '<%=session.getAttribute("childcategorygrid")%>';
	var userChildCategoryColumns = [];
	
	var allChildCategoryColumns = [ childCheckboxSelector.getColumnDefinition(), 
		{id : "id", name : "ID", field : "id", width : 20, sortable : true}, 
		{id : "parentCategory", name : "Parent Category", field : "parentCategory", width : 80, sortable : true}, 
		{id : "childCategory", name : "Child Category", field : "childCategory", width : 80, sortable : true},
		{id : "lastUpdatedBy", name : "Last Updated By", field : "lastUpdatedBy", width : 80, sortable : true},
		{id : "lastUpdated", name : "Last Updated", field : "lastUpdated", width : 80, sortable : true},
		{id : "editCol", name : "Edit",	field : "editCol", width : 20, sortable : true, formatter : editFormatter} ];

	if (userChildCategoryColumnsStr != 'null' && userChildCategoryColumnsStr != '') {
		var columnOrder = userChildCategoryColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allChildCategoryColumns.length; j++) {
				if (columnWidth[0] == allChildCategoryColumns[j].id) {
					userChildCategoryColumns[i] = allChildCategoryColumns[j];
					userChildCategoryColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userChildCategoryColumns = allChildCategoryColumns;
	}

	var childCategoryOptions = {
		editable : true,
		enableCellNavigation : true,
		asyncEditorLoading : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var childCategoryGridSortcol = "id";
	var openOrderGridSortdir = 1;
	var percentCompleteThreshold = 0;

	//function for edit functionality
	function editFormatter(row, cell, value, columnDef, dataContext) {		
		var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.id +"'/>";
		return button;
	}
	
	//Function to hook up the edit button event
	$('.editClickableImage').live('click', function() {
		var me = $(this), id = me.attr('id');
		editChildCategory(id);
	});
	
	function childCategoryComparer(a, b) {
		var x = a[childCategoryGridSortcol], y = b[childCategoryGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getChildCategoryGridData(pageNo);
	}
	
	function getChildCategoryGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelChild.json",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+childCategoryGridSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */				
				pagingInfo = jsonData.pagingInfo;
				refreshChildCategoryGridValues(jsonData.childCategory);
				clearAllSelections();
				$('#childCategory_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserChildCategoryPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshChildCategoryGridValues(jsonData) {
		childCategoryData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (childCategoryData[i] = {});
				d["id"] = data.id;
				d["parentCategory"] = data.parentCategoryName;
				d["childCategory"] = data.name;
				d["lastUpdatedBy"] = data.lastUpdatedBy;
				d["lastUpdated"] = data.lastUpdatedStr;
			}
		}

		childCategoryView = new Slick.Data.DataView();
		childCategoryGrid = new Slick.Grid("#childCategory_grid", childCategoryView,
				userChildCategoryColumns, childCategoryOptions);
		childCategoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true}));
		childCategoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		childCategoryGrid.registerPlugin(childCheckboxSelector);
		if(pagingInfo != null){
			var childCategoryPager = new Slick.Controls.Pager(childCategoryView,
					childCategoryGrid, $("#childCategory_pager"),pagingInfo);
		}
		
		var childCategoryColumnPicker = new Slick.Controls.ColumnPicker(
				allChildCategoryColumns, childCategoryGrid, childCategoryOptions);

		childCategoryGrid.onSort.subscribe(function(e, args) {
			childCategorySortdir = args.sortAsc ? 1 : -1;
			childCategoryGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				childCategoryView.fastSort(childCategoryGridSortcol, args.sortAsc);
			} else {
				childCategoryView.sort(childCategoryComparer, args.sortAsc);
			}
		});
		
		// wire up model childCategory to drive the childCategoryGrid
		childCategoryView.onRowCountChanged.subscribe(function(e, args) {
			childCategoryGrid.updateRowCount();
			childCategoryGrid.render();
		});
		childCategoryView.onRowsChanged.subscribe(function(e, args) {
			childCategoryGrid.invalidateRows(args.rows);
			childCategoryGrid.render();
		});
		
		$(childCategoryGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			childCategoryGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  childCategoryGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getChildCategoryGridData(0);
				}
			  }		 
		});
		childCategoryGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'lastUpdated'){
					$("<input type='text' placeholder='mm/dd/yyyy hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id != 'editCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		childCategoryGrid.init();
		
		// initialize the model after all the childCategory have been hooked up
		childCategoryView.beginUpdate();
		childCategoryView.setItems(childCategoryData);
		
		childCategoryView.endUpdate();
		childCategoryView.syncGridSelection(childCategoryGrid, true);
		childCategoryGrid.resizeCanvas();

		var openOrderrowIndex;
		childCategoryGrid.onSelectedRowsChanged
				.subscribe(function() {
					var temprOpenOrderRowIndex = childCategoryGrid
							.getSelectedRows([ 0 ])[0];
					if (temprOpenOrderRowIndex != openOrderrowIndex) {
						openOrderrowIndex = temprOpenOrderRowIndex;
						$('#AddChildCategorySuccessMsg').hide();
					}
				});

		
		$("div#divLoading").removeClass('show');
	}
	
	//toggle the add manual tickets creation modal to add
	function addModal(){
		$('#AddChildCategorySuccessMsg').hide();
		childCategoryFormUpdate(false,null,0);			
	}
	
	function editChildCategory(childId) {
		$('#AddChildCategorySuccessMsg').hide();
		$.ajax({			  
			url : "${pageContext.request.contextPath}/CrownJewelEvents/getChildCategoryValue",
			type : "post",
			dataType:"json",
			data : "childId="+childId,
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Child Category Found.");
				} */ 
				childCategoryFormUpdate(true,jsonData.childCat, childId);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function childCategoryFormUpdate(isUpdateForm,jsonData, childId) {
		if(isUpdateForm) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (childCategoryData[i] = {});
				if(childId == data.id){		
					$('#id').val(data.id);
					$('#name').val(data.name);
					$('#parentId').val(data.parentCatId);			
				}
			}
		} else {
			$('#id').val('');
			$('#name').val('');
			$('#parentId').val('');
		}
		$('#myModal').modal('show');
	}
	
	function addChildCategory(){
		$('#AddChildCategorySuccessMsg').hide();
		var childId = $('#id').val();
		var parentId = $('#parentId').val();
		var name = $('#name').val();
		if(parentId < 0){
			jAlert("Please select valid Parent Category.");
			return;
		}
		if(name == ''){
			jAlert("Please add valid name for Child Category.");
			return;
		}
		if(childId != null && childId != ""){
			$('#action').val('update');
		}else{
			$('#action').val('create');
		}
		$.ajax({			  
			url : "${pageContext.request.contextPath}/CrownJewelEvents/updateChildCategory",
			type : "post",
			data : $("#addChildCategoryForm").serialize(),
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				$('#myModal').modal('hide');
				$('#AddChildCategorySuccessMsg').show();
				getChildCategoryGridData(0);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function removeChildCategory() {
		var childIds = getSelectedChildCategoryGridId();
		var flag = true;
		if(childIds == null || childIds=='') {
			flag= false;
			jAlert("Select a Child to remove from Child Category.");
			return false;
		}
		
		if(flag) {
			jConfirm("Are you sure you want to remove selected child category?","confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/CrownJewelEvents/updateChildCategory",
						type : "post",
						data : "action=delete&&childIds="+ childIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							$('#AddChildCategorySuccessMsg').text(jsonData.msg);
							$('#AddChildCategorySuccessMsg').show();
							getChildCategoryGridData(0);
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		} else{
			jAlert("Select a Child to remove from Child Category.");
		} 
	}
	
	function getSelectedChildCategoryGridId() {
		var tempChildRowIndex = childCategoryGrid.getSelectedRows();
		
		var childIdStr='';
		$.each(tempChildRowIndex, function (index, value) {
			childIdStr += ','+childCategoryGrid.getDataItem(value).id;
		});
		
		if(childIdStr != null && childIdStr!='') {
			childIdStr = childIdStr.substring(1, childIdStr.length);
			 return childIdStr;
		}
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
</script>