<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	
});
</script>

<style>
	input{
		color : black !important;
	}
</style>
	<form name="inventoryuploadform" id="inventoryuploadform" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/ecomm/UploadInventory?action=UPLOAD">

<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Upload Inventory </h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i>Product Management</li>
						<li><i class="fa fa-laptop"></i>Upload Inventory</li>						  	
					</ol>
				
				</div>
</div>

<div class="row">
           <div class="col-xs-12 filters-div">
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<input type="hidden" name="fileRequired" id="fileRequired" />
					<label for="imageFile" class="col-lg-3 control-label"></label>
                 	<input type="file" id="imageFile" name="imageFile">
				</div>
           </div>  
           <div class="col-xs-12 filters-div">
				<div class="form-group col-lg-3 col-md-3 col-sm-4 col-xs-5">
					<button type="submit" id="searchSeatGeekBtn" class="btn btn-primary" style="margin-top:19px;">Upload Inventory </button> 
				<!--<button type="button" id="searchSeatGeekBtn" class="btn btn-primary" onclick="uploadInventoryXls();" style="margin-top:19px;">Upload Inventory </button> -->
				</div>
				<div class="form-group col-lg-3 col-md-3 col-sm-4 col-xs-5">
				
				</div>
           </div>
</div>

<script>


$(window).load(function() {
	var respstatus= '${status}';
	var respmsg= '${msg}';   
	if(typeof respmsg !== 'undefined' && respmsg != ''){
		jAlert(respmsg);		
	}else {
		//jAlert(respmsg);
	}



	});
	

	function uploadInventoryXls() {

		var imageFile = $('#imageFile').val();
		
		if(imageFile == ''){
			jAlert("Please Select the Excel File to Upload.");
			return;
		}	
		
		var form = $('#inventoryuploadform')[0];
		var dataString = new FormData(form);		
		$.ajax({
			url :"${pageContext.request.contextPath}/ecomm/UploadInventory.json?action=UPLOAD",
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			processData : false,
			contentType : false,
			cache : false,
			data : dataString,
			success : function(response){
			//var jsonData = JSON.parse(JSON.stringify(response));

				jAlert("File Upload has been successfull ");
			//	if(jsonData.status == 1){
					//$('#pollingSponsorModal').modal('hide');
					//pagingInfo = JSON.parse(jsonData.pollingSponsorPagingInfo);
					//pollingColumnFilters = {};
					//refreshPollingSponsorGridValues(JSON.parse(jsonData.pollingSponsor));
					//clearAllSelections();
					//jAlert("Inventory File/Data Has Been Uploaded Sucessfully.");
			//	}
			//	if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
			//		jAlert(jsonData.msg);
			//	}
			},
			error : function(error){
			//	jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
		
	}
</script>