<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var j = 0;
var promoAutoGenerate;
var promoFlatDiscount;
var varSelectedList = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}		
		if(contestRequestGrid != null && contestRequestGrid != undefined){
			contestRequestGrid.resizeCanvas();
		}
	});
	
});

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Contest Event Request</li>
		</ol>
	</div>
</div>
<div id="contestDiv">	
	<div class="panel-body1 full-width">
		
		<div class="full-width full-width-btn mb-20">
			<button type="button" class="btn btn-primary" onclick="updateContestEventRequest('APPROVE');">Approve</button>
			<button type="button" class="btn btn-primary" onclick="updateContestEventRequest('REJECT');">Reject</button>
		</div>
		
		<input type="hidden" name="contestEvReqIdStr" id="contestEvReqIdStr"/>
		<br />
		<br />
		<div style="position: relative" id="contestRequestGridDiv">
			<div class="table-responsive grid-table">
				<div class="grid-header full-width">
					<label>Contests Request</label>
					<div class="pull-right">
						<!--<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
						<a href="javascript:contestRequestResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
					</div>
				</div>
				<div id="contestRequest_grid" style="width: 100%; height: 200px; border: 1px solid gray;"></div>
				<div id="contestRequest_pager" style="width: 100%; height: 10px;"></div>
		
			</div>
		</div>			
		<br />
			
	</div>
</div>


<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'contestRequest_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getContestRequestGridData(pageNo);
		}
	}
	
	//Contest Event Request Grid
	
	function getContestRequestGridData(pageNo){		
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestsFirebaseMobile.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+contestRequestSearchString+"&status=REQUEST",
			success : function(response){				
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.contestRequestPagingInfo;
				refreshContestRequestGridValues(jsonData.contestRequestList);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});		
	}
	
	function contestRequestExportToExcel(){
		var appendData = "headerFilter="+contestRequestSearchString;
	    var url = "${pageContext.request.contextPath}/contestRequestExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function contestRequestResetFilters(){
		contestRequestSearchString='';
		contestRequestColumnFilters = {};
		getContestRequestGridData(0);
	}
	
	/*var contestRequestCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});*/

	var pagingInfo;
	var contestRequestDataView;
	var contestRequestGrid;
	var contestRequestData = [];
	var contestRequestPager;
	var contestRequestSearchString='';
	var contestRequestColumnFilters = {};
	var userContestRequestColumnsStr = '<%=session.getAttribute("contestrequestgrid")%>';

	var userContestRequestColumns = [];
	var allContestRequestColumns = [ /*contestRequestCheckboxSelector.getColumnDefinition(),*/
			/*{
				id : "contestRequestId",
				field : "contestRequestId",
				name : "Contest Req. Id",
				width : 80,
				sortable : true
			},*/{
				id : "eventName",
				field : "eventName",
				name : "Event Name",
				width : 80,
				sortable : true
			},{
				id : "eventDate",
				field : "eventDate",
				name : "Event Date",
				width : 80,
				sortable : true
			},{
				id : "venue",
				field : "venue",
				name : "Venue",
				width : 80,
				sortable : true
			},{
				id : "eventDescription",
				field : "eventDescription",
				name : "Description",
				width : 80,
				sortable : true
			},{
				id : "socialMediaLink",
				field : "socialMediaLink",
				name : "Social Media",
				width : 80,
				sortable : true
			},{
				id : "contactName",
				field : "contactName",
				name : "Name",
				width : 80,
				sortable : true
			},{
				id : "contactEmail",
				field : "contactEmail",
				name : "Email",
				width : 80,
				sortable : true
			},{
				id : "contactPhone",
				field : "contactPhone",
				name : "Phone",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "noOfTickets",
				field : "noOfTickets",
				name : "No. Of Tickets",
				width : 80,
				sortable : true
			},/*{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			}*/];

	if (userContestRequestColumnsStr != 'null' && userContestRequestColumnsStr != '') {
		columnOrder = userContestRequestColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestRequestColumns.length; j++) {
				if (columnWidth[0] == allContestRequestColumns[j].id) {
					userContestRequestColumns[i] = allContestRequestColumns[j];
					userContestRequestColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestRequestColumns = allContestRequestColumns;
	}
		
	var contestRequestOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var contestRequestGridSortcol = "contestRequestId";
	var contestRequestGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestRequestGridComparer(a, b) {
		var x = a[contestRequestGridSortcol], y = b[contestRequestGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshContestRequestGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestRequestData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestRequestData[i] = {});
				d["id"] = i;
				d["contestRequestId"] = data.id;
				d["eventName"] = data.eventTitle;
				d["eventDate"] = data.eventDateStr;
				d["venue"] = data.eventLocation;
				d["eventDescription"] = data.eventDescription;
				d["socialMediaLink"] = data.socialMediaLink;
				d["contactName"] = data.contactName;
				d["contactEmail"] = data.contactEmail;
				d["contactPhone"] = data.contactPhone;
				d["createdDate"] = data.createdDateStr;
				d["createdBy"] = data.createdBy;
				d["noOfTickets"] = data.noOfTickets;
				//d["status"] = data.status;
			}
		}

		contestRequestDataView = new Slick.Data.DataView();
		contestRequestGrid = new Slick.Grid("#contestRequest_grid", contestRequestDataView,
				userContestRequestColumns, contestRequestOptions);
		contestRequestGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestRequestGrid.setSelectionModel(new Slick.RowSelectionModel());
		/*contestRequestGrid.registerPlugin(contestRequestCheckboxSelector);*/
		
		contestRequestPager = new Slick.Controls.Pager(contestRequestDataView,
					contestRequestGrid, $("#contestRequest_pager"),
					pagingInfo);
		var contestRequestGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestRequestColumns, contestRequestGrid, contestRequestOptions);
		
	
		contestRequestGrid.onSort.subscribe(function(e, args) {
			contestRequestGridSortdir = args.sortAsc ? 1 : -1;
			contestRequestGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestRequestDataView.fastSort(contestRequestGridSortcol, args.sortAsc);
			} else {
				contestRequestDataView.sort(contestRequestGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the Contest Request Grid
		contestRequestDataView.onRowCountChanged.subscribe(function(e, args) {
			contestRequestGrid.updateRowCount();
			contestRequestGrid.render();
		});
		contestRequestDataView.onRowsChanged.subscribe(function(e, args) {
			contestRequestGrid.invalidateRows(args.rows);
			contestRequestGrid.render();
		});
		$(contestRequestGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestRequestSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								contestRequestColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in contestRequestColumnFilters) {
										if (columnId !== undefined
												&& contestRequestColumnFilters[columnId] !== "") {
											contestRequestSearchString += columnId
													+ ":"
													+ contestRequestColumnFilters[columnId]
													+ ",";
										}
									}									
									getContestRequestGridData(0);
								}
							}

						});
		contestRequestGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'eventDate' || args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(contestRequestColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(contestRequestColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		contestRequestGrid.init();
		
		var contestEvReqRowIndex = -1;
		contestRequestGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempContestEvReqRowIndex = contestRequestGrid.getSelectedRows([0])[0];
			if (tempContestEvReqRowIndex != contestEvReqRowIndex) {
				var contestRequestId = contestRequestGrid.getDataItem(tempContestEvReqRowIndex).contestRequestId;				
				$('#contestEvReqIdStr').val(contestRequestId);
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		contestRequestDataView.beginUpdate();
		contestRequestDataView.setItems(contestRequestData);
		//contestRequestDataView.setFilter(filter);
		contestRequestDataView.endUpdate();
		contestRequestDataView.syncGridSelection(contestRequestGrid, true);
		contestRequestGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserContestRequestPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestRequestGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('contestrequestgrid', colStr);
	}
	
	function updateContestEventRequest(status){
		var contEvReqId = $('#contestEvReqIdStr').val();
		if (contEvReqId == '') {
			jAlert("Please select a Contest Event Request to Update Status.","Info");
			return false;
		}		
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateContestEventRequest",
			type : "post",
			dataType: "json",
			data : "contestRequestId="+contEvReqId+"&action="+status,
			success : function(response) {
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					contestRequestColumnFilters = {};
					pagingInfo = jsonData.contestRequestPagingInfo;
					refreshContestRequestGridValues(jsonData.contestRequestList);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}					
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});			
	}	

	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${contestRequestPagingInfo};
		refreshContestRequestGridValues(${contestRequestList});
		$('#contestRequest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestRequestPreference()'>");
		
		enableMenu();
	};
		
</script>