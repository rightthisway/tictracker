<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

  
</style>

<script type="text/javascript">
var jq2 = $.noConflict(true);
$(document).ready(function(){
	
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
});

function callChangeImage(parentCategoryId){
	$('#imageUploadDiv_'+parentCategoryId).show();
	$('#imageDisplayDiv_'+parentCategoryId).hide();
	$('#fileRequired_'+parentCategoryId).val('Y');
	selectRow(parentCategoryId);
}

function selectRow(parentCategoryId) {
	$('#checkbox_'+parentCategoryId).attr('checked', true);
}
var validFilesTypes = ["jpg","jpeg","png","gif"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
        	file.focus();
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
            file.value = null;
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
        	file.focus();
            jAlert("Image Size Should be Greater than 0 and less than 1 MB.");
            file.value = null;
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null || file.value == '') {
    		jAlert("Please select valid image to upload.");
    		file.focus();
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callSaveBtnOnClick(action) {
	
	var flag = true;
	
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		id = this.id.replace('checkbox','fileRequired');
		var fileRequired = $('#'+id).val();
		if(fileRequired == '' || fileRequired == 'Y') {
			id = this.id.replace('checkbox','file');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
		}
		if(!flag) {
			return false;
		}
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one parent category to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to save selected parent category images ?.","Confirm",function(r){
		  if(r) {
				$("#action").val(action);
			 	$("#parentImageForm").submit();
			} 
	  });
	}
	
	
}
function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing parent category image to delete.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	 jConfirm("Are you sure you want to remove selected parent category images ?.","Confirm",function(r){
		 if(r) { 
				$("#action").val(action);
				$("#parentImageForm").submit();
			 }
	 });
	}
	
	
}
</script>

<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Images</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Images</a></li>
						<li><i class="fa fa-laptop"></i>Parent Category</li>						  	
					</ol>
				</div>
</div>

<div class="container">
<div class="row">

	<div class="alert alert-success fade in" id="infoMainDiv"
	<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="parentImageForm" id="parentImageForm" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/ParentCategoryImages">
	<input type="hidden" id="action" name="action" />
	<div class="row clearfix" id="gridDiv">
	<c:if test="${not empty parentImageList}">
	<div class="col-md-12 column">
			<div class="pull-right">
					<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
				<br />
				<br />
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-4">
							Parent Category
						</th>
						<th class="col-lg-7">
							Image
						</th>
						
						
					</tr>
				</thead>
				<tbody>
				 <c:forEach var="pcImage" varStatus="vStatus" items="${parentImageList}">
                    <tr <c:if test="${pcImage.id ne null}">style="background-color: #caf4ca;"</c:if> >
						<td>
	                      	<input type="checkbox" class="selectCheck" id="checkbox_${pcImage.parentCategory.id}" name="checkbox_${pcImage.parentCategory.id}" />
							<input type="hidden" name="id_${pcImage.parentCategory.id}" id="id_${pcImage.parentCategory.id}" value="${pcImage.id}" />
							<input type="hidden" name="fileRequired_${pcImage.parentCategory.id}" id="fileRequired_${pcImage.parentCategory.id}" 
							<c:if test="${pcImage.id ne null}"> value="N" </c:if>
							<c:if test="${pcImage.id eq null}"> value="Y" </c:if> />
                      </td>
                      <td style="font-size: 13px;" align="center">
                     		<b><label for="parentCategory" class="list-group-item" id="parentCategory_${pcImage.parentCategory.id}">${pcImage.parentCategory.name}</label></b>
                     		
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="form-group form-group-top" id="imageUploadDiv_${pcImage.parentCategory.id}" 
                                  <c:if test="${pcImage.id ne null }">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-lg-3 control-label">Image File</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="file_${pcImage.parentCategory.id}" name="file_${pcImage.parentCategory.id}" onchange="selectRow('${pcImage.parentCategory.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="imageDisplayDiv_${pcImage.parentCategory.id}" 
                                   <c:if test="${pcImage.id eq null}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${pcImage.id ne null}">
                                          <img src='${api.server.url}GetImageFile?type=parentCategoryImage&filePath=${pcImage.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage('${pcImage.parentCategory.id}');">
											Change Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>
		</div>
	</c:if>
	<c:if test="${empty parentImageList and not empty parentStr}">
	<div class="alert alert-block alert-danger fade in" id="infoMainDiv">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span>No records Found.</span>
    </div>
	</c:if>
	</div>
	</form>
</div>



