<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
.promoAll{
	margin-right:5px;
	background: #87ceeb;
	font-size: 10pt;
	height:20px;
}
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var j = 0;
var promoFlatDiscount;
var isReset = false;
var varSelectedList = '';
var varSelectedCustomer = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#expiryDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$("#freeTicket1").click(function(){
		callTabOnChange('FREETICKET');
	});
	$("#freeLives1").click(function(){
		callTabOnChange('FREELIVES');
	});
	
	
	$('#artistSearchAutoComplete').autocomplete("AutoCompleteArtistEventAndCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='PARENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='EVENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[3] ;
			}
		}
	}).result(function (event,row,formatted){
		if(row[0]=="ARTIST"){
			$('#artistId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=="EVENT"){
			$('#eventId').val(row[1]);
			$('#selectedItem').text(row[3]);
		}else if(row[0]=='PARENT'){
			$('#parentId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=='CHILD'){
			$('#childId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=='GRAND'){
			$('#grandChildId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}
		$('#artistSearchAutoComplete').val("");
		$('#artistEventCategoryType').val(row[0]);
		$('#artistEventCategoryName').val(row[2]);		
		$('#resetLink').show();
	});
	
	
	jq2('#promo_customer').autocomplete("AutoCompleteCustomer", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
		}
	}).result(function (event,row,formatted){
		varSelectedCustomer = varSelectedCustomer +","+ row[0] + "_" + row[1];		
		$('#customerId').val(row[1]);
		$('#promo_customer').val("");
		$('#promoCustomers').empty();
		$('#promoCustomers').append('<div class="block promoAll">'+row[2]+'</div>');
		$('#customerName').val(row[2]+"-"+row[3]);
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ContestPromocodes?status="+selectedTab;
}

function resetFreeTicketModal(){
	$('#saveBtn').show();			
	$('#updateBtn').hide();
	$('#promoId').val('');
	$('#promoDisciption').val('');
	$('#freeTickets').val('');
	$('#artistId').val('');
	$('#eventId').val('');
	$('#parentId').val('');
	$('#childId').val('');
	$('#grandChildId').val('');
	$('#selectedItem').text('');
	$('#customerId').val('');
	$('#expiryDate').val('');
	$('#promoCustomers').text('');
	$('#customerName').text('');
	$('#artistEventCategoryName').val('');
}



function updateOffer(status,statusType){
	var temprDiscountRowIndex = discountCodeGrid.getSelectedRows([0])[0];
	if (temprDiscountRowIndex < 0 || temprDiscountRowIndex == undefined) {
		jAlert("Please select Records to update.");
		return;
	}
	var promoOffrId = discountCodeGrid.getDataItem(temprDiscountRowIndex).promoId;
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdateContestPromocodeStatus",
		type : "post",
		dataType:"json",
		data:"status="+status+"&promocodeId="+promoOffrId+"&statusType="+statusType,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				pagingInfo = JSON.parse(jsonData.discountCodePagingInfo);
				refreshDiscountCodeGridValues(JSON.parse(jsonData.discountCodeList));
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function getDiscountCodeGridData(pageNo){
	$.ajax({
		url : "${pageContext.request.contextPath}/ContestPromocodes",
		type : "post",
		dataType : "json",
		data : "pageNo="+pageNo+"&headerFilter="+discountCodeSearchString+"&isReset="+isReset+"&status=${statusType}",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			isReset =false;
			if(jsonData.status == 1){
				pagingInfo = JSON.parse(jsonData.discountCodePagingInfo);
				refreshDiscountCodeGridValues(JSON.parse(jsonData.discountCodeList));
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function referSaveDiscountCode(action){
	
	var promoName = $('#promoDisciption').val();
	var freeTickets = $('#freeTickets').val();
	var customerId = $('#customerId').val();
	var expiryDate = $('#expiryDate').val();
	
	var artistId = $('#artistId').val();
	var eventId = $('#eventId').val();
	var parentId = $('#parentId').val();
	var childId = $('#childId').val();
	var grandChildId = $('#grandChildId').val();
	
	if(promoName == '' || promoName == undefined){
		jAlert("Promo Offer Descrption is Mandatory.");
		return;
	}
	if(freeTickets == '' || freeTickets == undefined){
		jAlert("No Of Free Tickets is Mandatory.");
		return;
	}
	if(customerId == '' || customerId == undefined){
		jAlert("Promo Offer Customer selection is Mandatory.");
		return;
	}
	if(expiryDate == '' || expiryDate == undefined){
		jAlert("Promo Offer Expiry Date is Mandatory.");
		return;
	}
	
	if(artistId == '' && eventId == '' && parentId == '' && childId == '' && grandChildId == ''){
		jAlert("Promo Offer Artist/Event/Category is Mendatory.");
		return;
	}
	
	$.ajax({
		url : "${pageContext.request.contextPath}/GenerateContestPromocode",
		type : "post",
		dataType: "json",
		data: $('#discountCodeForm').serialize()+"&action="+action,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#myModal-2').modal('hide');
				pagingInfo = JSON.parse(jsonData.discountCodePagingInfo);
				columnFilters = {};
				refreshDiscountCodeGridValues(JSON.parse(jsonData.discountCodeList));
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest Promotional Offers</a>
			</li>
			<li><i class="fa fa-laptop"></i>GContest Promotional codes</li>
		</ol>
	</div>
</div>


<div class="row">
	<div class="alert alert-success fade in" id="infoMainDiv" <c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="icon-remove"></i>
		</button>
		<span id="infoMsgDiv">${info}</span>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="freeTicketTab" class="active"><a id="freeTicket1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#freeTicket">Tickets Promo Offers</a></li>
				<li id="FreeLivesTab" class=""><a id="freeLives1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#freeLives">Earn Lives Promo Offers</a></li>
			</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="freeTicket" class="tab-pane active">
					<div class="full-width full-width-btn mb-20">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-2" onclick="resetFreeTicketModal();">Add Free Tickets Promo</button>
						<button type="button" id="enableOffer" class="btn btn-primary" onclick="updateOffer('ACTIVE','FREETICKET');">Enable</button>
						<button type="button" id="disableOffer" class="btn btn-primary" onclick="updateOffer('DISABLED','FREETICKET');">Disable</button>
					</div>
					<br/><br/>
					<div style="position: relative" id="artistGridDiv">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Free Ticket Offers</label>
								<div class="pull-right">
									<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
									<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
								</div>
							</div>
							<div id="discountCode_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
							<div id="discountCode_pager" style="width: 100%; height: 10px;"></div>
				
						</div>
					</div>
			</div>
			<div id="freeLives" class="tab-pane">
				
			</div>
		</div>
	</div>	
	
	<!-- Add Promotional Code -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal-2">Add Promotional Code</button> -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Free Tickets Code</h4>
				</div>
					<div class="modal-body full-width">
						<form name="discountCodeForm" id="discountCodeForm" method="post">
						<input id="promoId" name="promoId" type="hidden" />
						<div class="form-group tab-fields">
							 <!-- <div class="form-group col-sm-6 col-xs-6">
								<label>Auto Generate <span class="required">*</span></label> 
								<span id="promo_ag_yes"><input class="form-control" id="promo_autoGenerate_yes" name="promoAutoGenerate" type="radio" value="Yes"/>Yes</span>
								<input class="form-control" id="promo_autoGenerate_no" name="promoAutoGenerate" type="radio" value="No"/>No
							</div>
							<div id="promoCodeDiv" class="form-group col-sm-6 col-xs-6">
								<label>Promotional Code <span class="required">*</span></label> 
								<input class="form-control" id="promoCode" name="promoCode" type="text" />
							</div> -->
							<div class="form-group col-sm-6 col-xs-6">
								<label>Promo Description <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promoDisciption" name="promoDisciption">
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>No of Free Tickets <span class="required">*</span></label> 
								<input class="form-control" type="text" id="freeTickets" name="freeTickets"> 
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label for="name" class="control-label">Artist/Event/Category</label> 
								<input class="form-control searchcontrol" type="text" id="artistSearchAutoComplete" name="artistSearchAutoComplete" placeholder="Artist"> 
								<input type="hidden" value="${artistId}" id="artistId" name="artistId" /> 
								<input type="hidden" value="${eventId}" id="eventId" name="eventId" /> 
								<input type="hidden" value="${parentId}" id="parentId" name="parentId" /> 
								<input type="hidden" value="${childId}" id="childId" name="childId" /> 
								<input type="hidden" value="${grandChildId}" id="grandChildId" name="grandChildId" />
								<input type="hidden" id="artistEventCategoryName" name="artistEventCategoryName" />
								<input type="hidden" id="artistEventCategoryType" name="artistEventCategoryType" />
								<label for="name" id="selectedItem">${artistName}</label> <a href="javascript:resetItem()" id="resetLink">remove</a>
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Customer <span class="required">*</span></label> 
								<input class="form-control searchcontrol" type="text" id="promo_customer" name="promoCustomer">
								<input type="hidden" value="${customerId}" id="customerId" name="customerId" />
								<input type="hidden" id="customerName" name="customerName" />
								<label for="name" id="promoCustomers"></label>
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Expiry Date <span class="required">*</span></label> 
								<input class="form-control" type="text" id="expiryDate" name="expiryDate">
							</div>
						</div>
						</form>			
					</div>
					<div class="modal-footer full-width">
					<button class="btn btn-primary" id="updateBtn" type="button" onclick="referSaveDiscountCode('UPDATE')">Update</button>
						<button class="btn btn-primary" id="saveBtn" type="button" onclick="referSaveDiscountCode('SAVE')">Save</button>
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					</div>			
			</div>
		</div>
	</div>
	<!-- Ends of Add Shipping/Other Address popup-->
	
	
	<script type="text/javascript">
	var discountCodeCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	function exportToExcel(){
		var appendData = "headerFilter="+discountCodeSearchString;
	    //var url = "${pageContext.request.contextPath}/Client/DiscountCodeExportToExcel?"+appendData;
		var url = apiServerUrl+"DiscountCodeExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		discountCodeSearchString='';
		columnFilters = {};
		isReset =true;
		getDiscountCodeGridData(0);
		//refreshPromoOfferDtlGridValues('');
	}

	var pagingInfo;
	var discountCodeDataView;
	var discountCodeGrid;
	var discountCodeData = [];
	var discountCodeGridPager;
	var discountCodeSearchString='';
	var columnFilters = {};
	var userDiscountCodeColumnsStr = '<%=session.getAttribute("freeTicketOfferGrid")%>';

		var userDiscountCodeColumns = [];
		var allDiscountCodeColumns = [discountCodeCheckboxSelector.getColumnDefinition(),
				/* {
					id : "promotionalCode",
					name : "Promo Code",
					field : "promotionalCode",
					width : 80,
					sortable : true
				},  */{
					id : "promoName",
					name : "Promo Discription",
					field : "promoName",
					width : 80,
					sortable : true
				},/*  {
					id : "customerId",
					name : "Customer Id",
					field : "customerId",
					width : 80,
					sortable : true
				}, */  {
					id : "customerName",
					name : "Customer Name",
					field : "customerName",
					width : 80,
					sortable : true
				}, {
					id : "noOfFreeTickets",
					name : "Free Tickets",
					field : "noOfFreeTickets",
					width : 80,
					sortable : true
				},{
					id : "promoType",
					name : "Promo Type",
					field : "promoType",
					width : 80,
					sortable : true
				},{
					id : "promoTypeName",
					name : "Promo Name",
					field : "promoTypeName",
					width : 80,
					sortable : true
				},{
					id : "expiryDate",
					name : "Expiry Date",
					field : "expiryDate",
					width : 80,
					sortable : true
				},{
					id : "status",
					field : "status",
					name : "Status",
					width : 80,
					sortable : true
				}, {
					id : "createdBy",
					field : "createdBy",
					name : "Created By",
					width : 80,
					sortable : true
				}, {
					id : "createdDate",
					field : "createdDate",
					name : "Created Date",
					width : 80,
					sortable : true
				}, {
					id : "modifiedBy",
					field : "modifiedBy",
					name : "ModifiedBy ",
					width : 80,
					sortable : true
				},{
					id : "Action",
					field : "Action",
					name : "Action ",
					width : 80,
					formatter:buttonFormatter
				}];

		if (userDiscountCodeColumnsStr != 'null' && userDiscountCodeColumnsStr != '') {
			columnOrder = userDiscountCodeColumnsStr.split(',');
			var columnWidth = [];
			for ( var i = 0; i < columnOrder.length; i++) {
				columnWidth = columnOrder[i].split(":");
				for ( var j = 0; j < allDiscountCodeColumns.length; j++) {
					if (columnWidth[0] == allDiscountCodeColumns[j].id) {
						userDiscountCodeColumns[i] = allDiscountCodeColumns[j];
						userDiscountCodeColumns[i].width = (columnWidth[1] - 5);
						break;
					}
				}

			}
		} else {
			userDiscountCodeColumns = allDiscountCodeColumns;
		}

		function buttonFormatter(row,cell,value,columnDef,dataContext){  
			    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.promoId +"'/>";
			    return button;
		}
		$('.editClickableImage').live('click', function(){
		    var me = $(this), id = me.attr('id');
		    getEditPromotionalCode(id);//confirm("Are you sure,Do you want to Delete it?");
		});
		
		var discountCodeOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect : false,
			topPanelHeight : 25,
			showHeaderRow : true,
			headerRowHeight : 30,
			explicitInitialization : true
		};
		var discountCodeGridSortcol = "promoCode";
		var discountCodeGridSortdir = 1;
		var percentCompleteThreshold = 0;
		var discountCodeGridSearchString = "";

		function discountCodeGridComparer(a, b) {
			var x = a[discountCodeGridSortcol], y = b[discountCodeGridSortcol];
			if (!isNaN(x)) {
				return (parseFloat(x) == parseFloat(y) ? 0
						: (parseFloat(x) > parseFloat(y) ? 1 : -1));
			}
			if (x == '' || x == null) {
				return 1;
			} else if (y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String)
					&& (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));
			}
		}
				
		function refreshDiscountCodeGridValues(jsonData) {
			$("div#divLoading").addClass('show');
			discountCodeData = [];
			if (jsonData != null && jsonData.length > 0) {
				for ( var i = 0; i < jsonData.length; i++) {
					var data = jsonData[i];
					var d = (discountCodeData[i] = {});
					d["id"] = i;
					d["promoId"] = data.id;
					d["customerName"] = data.userId;
					d["promoName"] = data.promoName;
					d["customerId"] = data.customerId;
					d["noOfFreeTickets"] = data.freeTixCount;
					d["expiryDate"] = data.expiryDateStr;
					d["promoType"] = data.promoRefType;
					d["promoTypeName"] = data.promoRefName;
					/* d["promoType"] = data.promoType;
					d["artist"] = data.artist;
					d["venue"] = data.venue;
					d["parent"] = data.parent;
					d["child"] = data.child;
					d["grandChild"] = data.grandChild;
					d["artistId"] = data.artistId;
					d["venueId"] = data.venueId;
					d["parentId"] = data.parentId;
					d["childId"] = data.childId;
					d["grandChildId"] = data.grandChildId; */
					d["status"] = data.status;
					d["createdBy"] = data.createdBy;
					d["createdDate"] = data.createdDateStr;
					d["modifiedBy"] = data.updatedDateStr;
				}
			}

			discountCodeDataView = new Slick.Data.DataView();
			discountCodeGrid = new Slick.Grid("#discountCode_grid", discountCodeDataView,
					userDiscountCodeColumns, discountCodeOptions);
			discountCodeGrid.registerPlugin(new Slick.AutoTooltips({
				enableForHeaderCells : true
			}));
			discountCodeGrid.setSelectionModel(new Slick.RowSelectionModel());
			discountCodeGrid.registerPlugin(discountCodeCheckboxSelector);
			
				discountCodeGridPager = new Slick.Controls.Pager(discountCodeDataView,
						discountCodeGrid, $("#discountCode_pager"),
						pagingInfo);
			var discountCodeGridColumnpicker = new Slick.Controls.ColumnPicker(
					allDiscountCodeColumns, discountCodeGrid, discountCodeOptions);
			
			/* discountCodeGrid.onContextMenu.subscribe(function(e) {
				e.preventDefault();
				var cell = discountCodeGrid.getCellFromEvent(e);
				discountCodeGrid.setSelectedRows([ cell.row ]);
				var row = discountCodeGrid.getSelectedRows([ 0 ])[0];			
				var promoId = discountCodeGrid.getDataItem(row).promoOfferId;
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				
				if (height < ($("#contextMenu").height() + 100)) {
					height = e.pageY - $("#contextMenu").height();
				} else {
					height = e.pageY;
				}
				if (width < $("#contextMenu").width()) {
					width = e.pageX - $("#contextMenu").width();
				} else {
					width = e.pageX;
				}
				$("#contextMenu").data("row", cell.row).css("top", height).css(
						"left", width).show();
				$("body").one("click", function() {
					$("#contextMenu").hide();
				});
				
			}); */
		
			discountCodeGrid.onSort.subscribe(function(e, args) {
				discountCodeGridSortdir = args.sortAsc ? 1 : -1;
				discountCodeGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					discountCodeDataView.fastSort(discountCodeGridSortcol, args.sortAsc);
				} else {
					discountCodeDataView.sort(discountCodeGridComparer, args.sortAsc);
				}
			});
			// wire up model discountCodes to drive the discountCodeGrid
			discountCodeDataView.onRowCountChanged.subscribe(function(e, args) {
				discountCodeGrid.updateRowCount();
				discountCodeGrid.render();
			});
			discountCodeDataView.onRowsChanged.subscribe(function(e, args) {
				discountCodeGrid.invalidateRows(args.rows);
				discountCodeGrid.render();
			});
			$(discountCodeGrid.getHeaderRow())
					.delegate(
							":input",
							"keyup",
							function(e) {
								var keyCode = (e.keyCode ? e.keyCode : e.which);
								discountCodeSearchString = '';
								var columnId = $(this).data("columnId");
								if (columnId != null) {
									columnFilters[columnId] = $.trim($(this)
											.val());
									if (keyCode == 13) {
										for ( var columnId in columnFilters) {
											if (columnId !== undefined
													&& columnFilters[columnId] !== "") {
												discountCodeSearchString += columnId
														+ ":"
														+ columnFilters[columnId]
														+ ",";
											}
										}
										getDiscountCodeGridData(0);
									}
								}

							});
			discountCodeGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if (args.column.id.indexOf('checkbox') == -1) {
					if(args.column.id == 'startDate' || args.column.id == 'endDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}

			});
			discountCodeGrid.init();
			
			var discountCodeRowIndex = -1;
			discountCodeGrid.onSelectedRowsChanged.subscribe(function() { 
				var temprDiscountRowIndex = discountCodeGrid.getSelectedRows([0])[0];
				if (temprDiscountRowIndex != discountCodeRowIndex) {
					/* var promoOffrId = discountCodeGrid.getDataItem(temprDiscountRowIndex).promoOfferId;
					getPromoOfferDtlGridData(promoOffrId,0); */
				}
			});
			// initialize the model after all the discountCodes have been hooked up
			discountCodeDataView.beginUpdate();
			discountCodeDataView.setItems(discountCodeData);
			//discountCodeDataView.setFilter(filter);
			discountCodeDataView.endUpdate();
			discountCodeDataView.syncGridSelection(discountCodeGrid, true);
			discountCodeGrid.resizeCanvas();
			/* $("#gridContainer").resizable(); */
			$("div#divLoading").removeClass('show');
		}

		function getSelectedDiscountCodeGridId() {
			var temprDiscountCodeRowIndex = discountCodeGrid.getSelectedRows();

			var discountCodeIdStr = '';
			$.each(temprDiscountCodeRowIndex, function(index, value) {
				discountCodeIdStr += ',' + discountCodeGrid.getDataItem(value).promoOfferId;
			});

			if (discountCodeIdStr != null && discountCodeIdStr != '') {
				discountCodeIdStr = discountCodeIdStr.substring(1, discountCodeIdStr.length);
				return discountCodeIdStr;
			}
		}

		$('#menuContainer').click(function() {
			if ($('.ABCD').length > 0) {
				$('#menuContainer').removeClass('ABCD');
			} else {
				$('#menuContainer').addClass('ABCD');
			}
			if(discountCodeGrid != null && discountCodeGrid != undefined){
				discountCodeGrid.resizeCanvas();
			}
						
		});

		function pagingControl(move, id) {
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getDiscountCodeGridData(pageNo);
		}
	
		function getEditPromotionalCode(promoOfferId){
			$.ajax({
				url : "${pageContext.request.contextPath}/ContestPromocodes",
				type : "post",
				dataType: "json",
				data: "promocodeId="+promoOfferId+"&status=${statusType}",
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						setEditPromotionalCode(JSON.parse(jsonData.discountCode));
					}else{
						jAlert(jsonData.msg);
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	
		function setEditPromotionalCode(data){
			$('#promoCategory').empty();
			$('#myModal-2').modal('show');
			$('#saveBtn').hide();			
			$('#updateBtn').show();
			if(data != null){
				$('#promoId').val(data.id);
				//$('#promoCode').val(data.promocode);
				$('#promoDisciption').val(data.promoName);
				$('#freeTickets').val(data.freeTixCount);
				if(data.promoRefType == 'ARTIST'){
					$('#artistId').val(data.promoRefId);
				}else if(data.promoRefType == 'EVENT'){
					$('#eventId').val(data.promoRefId);
				}else if(data.promoRefType == 'PARENT'){
					$('#parentId').val(data.promoRefId);
				}else if(data.promoRefType == 'CHILD'){
					$('#childId').val(data.promoRefId);
				}else if(data.promoRefType == 'GRAND'){
					$('#grandChildId').val(data.promoRefId);
				}
				$('#selectedItem').text(data.promoRefName);
				$('#customerId').val(data.customerId);
				$('#expiryDate').val(data.expiryDateEditStr);
				$('#promoCustomers').text(data.userId);
				$('#customerName').text(data.userId);
				$('#artistEventCategoryName').val(data.promoRefName);
			}
		}
		
		function saveUserDiscountCodePreference() {
			var cols = visibleColumns;
			if (cols == null || cols == '' || cols.length == 0) {
				cols = discountCodeGrid.getColumns();
			}
			var colStr = '';
			for ( var i = 0; i < cols.length; i++) {
				colStr += cols[i].id + ":" + cols[i].width + ",";
			}
			saveUserPreference('freeTicketOfferGrid', colStr);
		}

		//call functions once page loaded
		window.onload = function() {
			pagingInfo = JSON.parse(JSON.stringify(${discountCodePagingInfo}));
			refreshDiscountCodeGridValues(JSON.parse(JSON.stringify(${discountCodeList})));
			$('#discountCode_pager> div')
					.append(
							"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserDiscountCodePreference()'>");
			enableMenu();
		};
		
	</script>