<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

  
</style>

<script type="text/javascript">

var jq2 = $.noConflict(true);
$(document).ready(function(){
	
	var allParent = '${parentCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allParent=='true'){
			$('#parentCheckAll').attr("checked","checked");
		}
		allParent='false';
	}
	selectCheckBox();
	
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
});

function callHideGridDiv() {
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
}

function callSearchBtnClick(action){
	 $("#action").val(action);
	 $("#parentImageForm").submit();
}
function selectAllGrandChilds(){
	if((document.getElementById("parentCheckAll").checked)){
		$("#parents").each(function(){
			$("#parents option").attr("selected","selected"); 
		});
	}
	else{
		$("#parents").each(function(){
			$("#parents option").removeAttr("selected"); 
		});
	}
}

function callChangeImage(parentId){
	$('#imageUploadDiv_'+parentId).show();
	$('#imageDisplayDiv_'+parentId).hide();
	$('#fileExisting_'+parentId).val('N');
	$('#fileDeleted_'+parentId).val('Y');
	selectRow(parentId);
}

function selectRow(parentId) {
	$('#checkbox_'+parentId).attr('checked', true);
}
var validFilesTypes = ["jpg", "jpeg","png","gif"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
        	file.focus();
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
            file.value = null;
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
        	file.focus();
            jAlert("Image Size Should be Greater than 0 and less than 1 MB.");
            file.value = null;
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null || file.value == '') {
    		jAlert("Please select valid image to upload.");
    		file.focus();
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callSaveBtnOnClick(action) {
	
	var flag = true;
	
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		var isMinimamOneImage=false;
		id = this.id.replace('checkbox','file');
		var file = $('#'+id).val();
		if(file != null && file != '') {
			id = this.id.replace('checkbox','file');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
			isMinimamOneImage = true;
		} else {
			id = this.id.replace('checkbox','fileExisting');
			var isFileExisting = $('#'+id).val();
			if(isFileExisting != null && isFileExisting=='Y') {
				isMinimamOneImage = true;
			}
		}
		if(!flag) {
			return false;
		}
		id = this.id.replace('checkbox','circleFile');
		var circleFile = $('#'+id).val();
		if(circleFile != null && circleFile != '') {
			id = this.id.replace('checkbox','circleFile');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
			isMinimamOneImage = true;
		} else {
			id = this.id.replace('checkbox','circleFileExisting');
			var isCircleFileExisting = $('#'+id).val();
			if(isCircleFileExisting != null && isCircleFileExisting=='Y') {
				isMinimamOneImage = true;
			}
		}
		
		if(!isMinimamOneImage) {
			jAlert('Select atleast one image to save parent.');
			id = this.id.replace('checkbox','file');
			$("#"+id).focus();
			flag= false;
			return false;
		}
		if(!flag) {
			return false;
		}
		
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one Loyalfan Parent to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to save selected LoyalFan Parent images ?.","Confirm",function(r){
		  if(r) {
				$("#action").val(action);
			 	$("#parentImageForm").submit();
			}
	  });
	}
	
	
}
function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing Loyal image to delete.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	 jConfirm("Are you sure you want to remove selected LoyalFan images ?.","Confirm",function(r){
		 if(r) { 
				$("#action").val(action);
				$("#parentImageForm").submit();
			 }
	 });
	}
	
	
}
function callImageOnChange(parentId) {
	$('#fileDeleted_'+parentId).val('N');
	selectRow(parentId);
}
function callCircleImageOnChange(parentId) {
	$('#circleFileDeleted_'+parentId).val('N');
	selectRow(parentId);
}
</script>

<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i>Images</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Images</a></li>
						<li><i class="fa fa-laptop"></i>Loyal Fan Images</li>						  	
					</ol>
				</div>
</div>

<div class="container">
<div class="row">

	<div class="alert alert-success fade in" id="infoMainDiv"
	<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="parentImageForm" id="parentImageForm" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/LoyalFanParentCategoryImages">
	<input type="hidden" id="action" name="action" />
	<div class="row filters-div">
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">			
			<label for="imageText" class="control-label"><b>Loyal Fan Parent :</b></label>
        </div> 
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">		
			<input type="hidden" id="parent" name="parent" value ="${parent}">
			<select class="form-control input-sm fullWidth" type="text" name="parentSearch" id="parentSearch" onchange="callSearchBtnClick('search')"> 	
			  <option value="">--All--</option>
			  <c:forEach items="${parentList}" var="parent">
				<option value="${parent.id}" <c:if test="${parent.id == parentSearch}">selected</c:if>>${parent.name}</option>
			 </c:forEach> 
			</select>	
        </div>
	</div>
<div class="row clearfix" id="gridDiv">
	<c:if test="${not empty parentImageList}">
	<div class="full-width column">
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>
			<br />
			<br />
			<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-3">
							Loyal Fan Category
						</th>
						<th class="col-lg-4">
							Image
						</th>
					</tr>
				</thead>
				<tbody>
				 <c:forEach var="pImage" varStatus="vStatus" items="${parentImageList}">
                    <tr <c:if test="${pImage.id ne null}">style="background-color: #caf4ca;"</c:if> >
						<td>
	                      	<input type="checkbox" class="selectCheck" id="checkbox_${pImage.category.id}" name="checkbox_${pImage.category.id}" />
							<input type="hidden" name="id_${pImage.category.id}" id="id_${pImage.category.id}" value="${pImage.id}" />
							<input type="hidden" name="fileExisting_${pImage.category.id}" id="fileExisting_${pImage.category.id}" 
							<c:if test="${pImage.imageFileUrl ne null}"> value="Y" </c:if>
							<c:if test="${pImage.imageFileUrl eq null}"> value="N" </c:if> />
							<input type="hidden" name="fileDeleted_${pImage.category.id}" id="fileDeleted_${pImage.category.id}" value="N" />
                      </td>
                    <td style="font-size: 13px;" align="center">
                     		<b><label for="parent" class="list-group-item" id="parent_${pImage.category.id}">${pImage.category.name}</label></b>
                     		
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="orm-group-top" id="imageUploadDiv_${pImage.category.id}" 
                                  <c:if test="${pImage.imageFileUrl ne null }">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-md-3 col-sm-12 co-xs-12">Image File</label>
                                      <div class="col-lg-9 col-sm-12 co-xs-12 text-center">
                                          <input type="file" id="file_${pImage.category.id}" name="file_${pImage.category.id}" onchange="callImageOnChange('${pImage.category.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="imageDisplayDiv_${pImage.category.id}" 
                                   <c:if test="${pImage.imageFileUrl eq null}">style="display: none" </c:if>>
                                      <div class="col-xs-12 ">
                                       <c:if test="${pImage.imageFileUrl ne null}">
                                          <img src='${api.server.url}GetImageFile?type=loyalFanParentCategoryImage&filePath=${pImage.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage('${pImage.category.id}');">
											Change Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>
		</div>
	</c:if>
	<c:if test="${empty parentImageList and not empty parentStr}">
	<div class="alert alert-block alert-danger fade in" id="infoMainDiv">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span>No records Found.</span>
    </div>
	</c:if>
	</div>
	</form>
</div>
