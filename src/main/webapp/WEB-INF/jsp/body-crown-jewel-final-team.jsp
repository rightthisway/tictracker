<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

  
</style>

<script type="text/javascript">

function callParentTypeChange() {
	$('#statusId').val('');
	$("#searchForm").submit();
}

$(document).ready(function(){
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	<c:if test ="${statusId == 'DELETED'}">
		$('#bottomSaveFinalBtn').hide();
		$('#topSaveFinalBtn').hide();
		$('#topExcludeBtn').text('Revert to Active');;
		$('#bottomExcludeBtn').text('Revert to Active');;
	</c:if>
	
	
});

function callSelectRow(rowNumber) {
	$('#checkbox_'+rowNumber).attr('checked', true);
}
function callExcludeBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var arr=[];
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
			
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		arr.push(value);
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing team to '+action);
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag){
		if(action == "Save Final"){
			if(arr.length != 2){
				jAlert("Please select exactly 2 teams to mark as final teams");
				flag=false;
				return false;
			}
		}
	}
	if(flag) {
		var msg="";
		if(action=="Exclude"){
			msg = "Are you sure you want to Exclude selected teams? all associated orders will be mark as void";
			if($('#statusId').val()=='DELETED'){
				action = "Revert";
				msg = "Are you sure you want to revert selected Teams from Deleted to Active, All Associated Orders are also Activated?";
			}
		}else{
			msg = "Are you sure you want to set teams("+$('#teamName_'+arr[0]).val()+" and "+$('#teamName_'+arr[1]).val()+")as final team? all other teams are marked as excluded and related order marked as void";
		}
	   jConfirm(msg,"Confirm",function(r){
		 if(r){
			 $("#action").val(action);
				$("#teamForm").submit();
		 }
	  });
	}
	
 }
 function callParentTypeOnChange(action) {	 
	 $('#gridDiv').hide();
	 $('#leagueId').val('');
	 $("#action").val(action);
	 $('#statusId').val('');
	 $("#teamForm").submit();
 }

 function loadGrandChild(action){
 	 $('#gridDiv').hide();
 	$('#grandChildType').val('');
 	 $('#leagueId').val('');
 	 $('#statusId').val('');
 	 $("#action").val(action);
 	 $("#teamForm").submit();
 }
 
 function callLeagueOnChange(action) {
	 $("#infoMainDiv").hide();
	 $('#statusId').val('');
	 var flag = true;
	 /*var parentType = $('#parentType').val();
	 if(parentType == null || parentType == ''){
		 jAlert('Please select valid parent type.');
		 document.getElementById("parentType").focus();
		 flag = false;
		 return false;
	 }
	 var leagueId = $('#leagueId').val();
	 if(leagueId == null || leagueId == ''){
		 $('#gridDiv').hide();
		 jAlert('Please select valid League.');
		 document.getElementById("leagueId").focus();
		 flag = false;
		 return false;
	 }*/
		
	 if(flag) {
	 	$('#gridDiv').hide();
	 	$("#action").val(action);
	 	$("#teamForm").submit();
	 }
 }
 
 function callStatusOnChange(){
	 $("#infoMainDiv").hide();
	 $('#gridDiv').hide();
	 var teamStatus = $('#teamStatus').val();
	 $('#statusId').val(teamStatus);
	 if(teamStatus == 'ACTIVE'){
		 $("#action").val("search");
		 $("#teamForm").submit();
	 }else if(teamStatus == 'DELETED'){
		 $("#action").val("searchDeleted");
		 $("#teamForm").submit();
	 }
 }
/* function callCityOnChange(action) {
	 
	$("#infoMainDiv").hide();
	 
	 var flag = true;
	 var parentType = $('#parentType').val();
	 if(parentType == null || parentType == ''){
		 jAlert('Please select valid parent type.');
		 document.getElementById("parentType").focus();
		 flag = false;
		 return false;
	 }
	 var leagueId = $('#leagueId').val();
	 if(leagueId == null || leagueId == ''){
		 $('#gridDiv').hide();
		 jAlert('Please select valid Event.');
		 document.getElementById("leagueId").focus();
		 flag = false;
		 return false;
	 }
		
	 if(flag) {
	 	$('#gridDiv').hide();
	 	$("#action").val(action);
	 	$("#teamForm").submit();
	 }
 } */
</script>

<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Fantasy Sports Tickets</a></li>
						<li><i class="fa fa-laptop"></i>Final Team</li>						  	
					</ol>
				</div>
</div>

<div class="full-width">
<div class="row">

<div class="alert alert-success fade in" id="infoMainDiv"
<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="teamForm" id="teamForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelFinalTeam">
	<input type="hidden" id="action" name="action" />
	<div class="row">
	<input type="hidden" name="parentType" id="parentType" value="SPORTS" />
		 <div class="form-group full-width">
          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Parent Type</label>
          <div class="col-lg-4 col-md-4 col-xs-7">
               <label>SPORTS</label>
          </div>
          <div class="col-lg-2">
          </div>
           </div>
      </div>
          
	<div class="row">   
		<div class="form-group full-width">
		  <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Child Type</label>
		  <div class="col-lg-4 col-md-4 col-xs-7">
			<select name="childType" id="childType" class="form-control input-sm m-bot15" onchange="loadGrandChild('search');" style="max-width:200px">
				<option value="">--Select--</option>				
				<%-- <option value="0" <c:if test="${childId ne null and '0' eq childId}"> selected </c:if>>ALL</option> --%>
				<c:forEach items="${childCategories}" var="childCat">
					<option value="${childCat.id}" <c:if test="${childId ne null and childCat.id eq childId}"> selected </c:if>>${childCat.name}</option>
				</c:forEach>
			</select>
		  </div>
		</div>
	</div>
	
	<c:if test="${childId ne null and childId ne '0' and childId ne '-1'}">
	<div class="row">     
	      <div class="form-group full-width">
	          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Grand Child Type</label>
	          <div class="col-lg-4 col-md-4 col-xs-7">
	          	<select name="grandChildType" id="grandChildType" class="form-control input-sm m-bot15" onchange="callParentTypeOnChange('search');" style="max-width:200px">
	          		<option value="">--Select--</option>
					<%-- <option value="0" <c:if test="${grandChildId ne null and '0' eq grandChildId}"> selected </c:if>>ALL</option> --%>
					<c:forEach items="${grandChildCategories}" var="grandChildCat">							
						<option value="${grandChildCat.id}" <c:if test="${grandChildId ne null and grandChildCat.id eq grandChildId}"> selected </c:if>>${grandChildCat.name}</option>
					</c:forEach>
				</select>				
	          </div>
	      </div>
      </div>
	 </c:if>	 
	 
	 <div class="row">
        <div class="form-group full-width">
          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">
	          <%-- <c:choose>
	          	<c:when test="${parentType eq 'SPORTS' and not empty childId}">
	          		<c:if test="${childId eq '0' or not empty grandChildId}">Event</c:if>
	          	</c:when>
	          	<c:otherwise>
	          		<c:if test="${not empty parentType and not empty childId}">
						<c:if test="${childId eq '0' or not empty grandChildId}">Team/Artist</c:if></c:if>
				</c:otherwise>
          	</c:choose> --%>
          	<c:if test="${not empty grandChildId}">Event</c:if>
          </label>
          <c:if test="${not empty grandChildId}">
		         <div class="col-lg-4 col-md-4 col-xs-7">
		          	<select name="leagueId" id="leagueId" class="form-control input-sm m-bot15" onchange="callLeagueOnChange('search');" style="max-width:200px">
		                	<option value="">--- Select ---</option>
							<c:forEach var="league" items="${leaguesList}">
							    <option value="${league.id}" <c:if test="${leagueId ne null and league.id eq leagueId}">selected</c:if>>${league.name}</option>
							  </c:forEach>
		               </select>
		               <input type="hidden" id="artistId" name="artistId" value="${artistId}" />
		          </div>
				  <div class="col-lg-2"></div>
				 </c:if>
          </div>
     </div>
     
          <c:if test="${leagueId ne null and leagueId ne ''}">
          <div class="row">
             <div class="form-group full-width">
              <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Status</label>
	          <div class="col-lg-4 col-md-4 col-xs-7">
	          	<select name="teamStatus" id="teamStatus" class="form-control input-sm m-bot15" onchange="callStatusOnChange();" style="max-width:200px">
						<option value="ACTIVE" <c:if test="${statusId == 'ACTIVE'}">selected</c:if>>ACTIVE</option>
						<option value="DELETED" <c:if test="${statusId == 'DELETED'}">selected</c:if>>DELETED</option>
	               </select>
	               <input type="hidden" id="statusId" name="statusId" value="${statusId}" />
	         </div>
	        </div>
	        </div>
         </c:if>
	

<div class="row clearfix">
		<div class="col-md-12 column" id="gridDiv">
		<c:if test="${leagueId ne null and leagueId ne ''}">
			<%--  <c:choose>
            <c:when test="${parentType eq 'SPORTS' }"> --%>
            <div class="pull-right">
				<button type="button" onclick="callExcludeBtnOnClick('Save Final');" id="topSaveFinalBtn" class="btn btn-primary btn-sm">Save Final</button>
				<button type="button" onclick="callExcludeBtnOnClick('Exclude');" id="topExcludeBtn" class="btn btn-danger btn-sm">Exclude</button>
			</div>
			<br />
			<br />
			<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic" align="center">
				<thead>
					<tr >
						<th class="col-lg-1 ">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-3 ">
							Team Name
						</th>
						<th class="col-lg-3 ">
							Markup %
						</th>
						<th class="col-lg-1 ">
							Odds %
						</th>
						<th class="col-lg-1 ">
							Cutoff Date
						</th>
						 <th class="col-lg-1 ">
							Order Count
						</th > 
						<!-- <th class="col-lg-1 " >
							Package
						</th>
						<th class="col-lg-1 ">
							Package Cost $
						</th>
						<th class="col-lg-3 ">
							Package Notes
						</th> -->
						<th class="col-lg-1">
							Last Updated By
						</th>
						<th class="col-lg-1">
							Last Updated
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty teamsList}">
				 <c:forEach var="cjTeam" varStatus="vStatus" items="${teamsList}">
                      <tr id="teamRow_${vStatus.count}">
                      <td>
                      	<input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" />
						<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjTeam.id}" />
						<input type="hidden" name="leagueId_${vStatus.count}" id="leagueId_${vStatus.count}" value="${cjTeam.leagueId}" />
						<input type="hidden" name="teamName_${cjTeam.id}" id="teamName_${cjTeam.id}" value="${cjTeam.name}" /> 
                      </td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<label id="name_${vStatus.count}">${cjTeam.name}</label>
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<label id="name_${vStatus.count}">${cjTeam.markup}</label>
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<label id="odds_${vStatus.count}">${cjTeam.odds}</label>
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<label id="cutoffDate_${vStatus.count}">${cjTeam.cutOffDateStr}</label>
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<label id="orderCount_${vStatus.count}">${cjTeam.ordersCount}</label>
                                  </div>
                              </div>
						</td>
						
						<%-- <td style="vertical-align: middle;">
                            <label id="packageApplicable_${vStatus.count}">
                            <c:if test="${cjTeam.packageApplicable == true}">Yes</c:if>
                            <c:if test="${cjTeam.packageApplicable == false}">No</c:if>
                            </label> 
						</td> --%>
						<%-- <td style="vertical-align: middle;">
							<label id="packageCost_${vStatus.count}"><fmt:formatNumber pattern="0.00" value="${cjTeam.packageCost}"/></label>
						</td>
						<td style="vertical-align: middle;">
							<label id="packageNotes_${vStatus.count}">${cjTeam.packageNotes}</label>
						</td> --%>
						<td style="vertical-align: middle;">
							<label id="lastUpdatedBy_${vStatus.count}">${cjTeam.lastUpdateddBy}</label>
						</td>
						<td style="vertical-align: middle;">
							<label id="lastUpdated_${vStatus.count}">${cjTeam.lastUpdatedStr}</label>
						</td>
						</tr>
					</c:forEach>
					</c:if>
					

				</tbody>
			</table>
			</div>
				<div class="pull-right mt-20">
					<button type="button" onclick="callExcludeBtnOnClick('Save Final');" id="bottomSaveFinalBtn" class="btn btn-primary btn-sm">Save Final</button>
					<button type="button" onclick="callExcludeBtnOnClick('Exclude');" id="bottomExcludeBtn" class="btn btn-danger btn-sm">Exclude</button>
				</div>
			<%-- </c:when>
			<c:otherwise>
				<c:choose>
				<c:when test="${not empty teamsList}">
				<div class="pull-right">
					<button type="button" onclick="callExcludeBtnOnClick('Save Final');" class="btn btn-primary btn-sm">Save Final</button>
					<button type="button" onclick="callExcludeBtnOnClick('Exclude');" class="btn btn-danger btn-sm">Exclude</button>
				</div>
				<br />
				<br />
				<div class="table-responsive">
				<table class="table table-bordered table-hover" id="tab_logic" align="center">
				<thead>
					<tr >
						<th class="col-lg-1 ">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-1 ">
							City
						</th>
						<th class="col-lg-2 ">
							Event
						</th>
						<th class="col-lg-1 ">
							Event Date
						</th>
						<th class="col-lg-2 ">
							Venue
						</th>
						<!-- <th class="col-lg-1 ">
							Tickets Count
						</th >  -->
						<th class="col-lg-1 " >
							Package
						</th>
						<th class="col-lg-1 ">
							Package Cost $
						</th>
						<th class="col-lg-3 ">
							Package Notes
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty teamsList}">
				 <c:forEach var="cjTeam" varStatus="vStatus" items="${teamsList}">
                      <tr id="teamRow_${vStatus.count}"
                      <c:if test="${cjTeam.id ne null}">style="background-color: #caf4ca;"</c:if> >
                      <td>
                      	<input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" />
						<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjTeam.id}" />
						<input type="hidden" name="leagueId_${vStatus.count}" id="leagueId_${vStatus.count}" value="${cjTeam.leagueId}" /> 
                      </td>
						<td style="font-size: 13px;" align="center">
                     		<b><label for="CityText" class="list-group-item" id="cityText_${vStatus.count}">${cjTeam.event.city}</label></b>
						</td>
						<td style="font-size: 13px;" align="center">
                     		<b><label for="EventText" class="list-group-item" id="eventNameText_${vStatus.count}">${cjTeam.event.eventName}</label></b>
						</td>
						<td style="font-size: 13px;" align="center">
                     		<b><label for="EventText" class="list-group-item" id="eventDateText_${vStatus.count}">${cjTeam.event.eventDateTimeStr}</label></b>
						</td>
						
						<td style="font-size: 13px;" align="center">
                     		<b><label for="EventText" class="list-group-item" id="venueText_${vStatus.count}">${cjTeam.event.formattedVenueDescription}</label></b>
						</td>
						<td style="vertical-align: middle;">
                            <label id="packageApplicable_${vStatus.count}">
	                            <c:if test="${cjTeam.packageApplicable == true}">Yes</c:if>
	                            <c:if test="${cjTeam.packageApplicable == false}">No</c:if>
                            </label>
						</td>
						<td style="vertical-align: middle;">
							<label id="packageCost_${vStatus.count}"><fmt:formatNumber pattern="#0" value="${cjTeam.packageCost}"/></label>
						</td>
						<td style="vertical-align: middle;">
							<label id="packageNotes_${vStatus.count}">${cjTeam.packageNotes}</label>
						</td>
						</tr>
					</c:forEach>
					</c:if>
					

				</tbody>
			</table>
			</div>
				<div class="pull-right">
					<button type="button" onclick="callExcludeBtnOnClick('Save Final');" class="btn btn-primary btn-sm">Save Final</button>
					<button type="button" onclick="callExcludeBtnOnClick('Exclude');" class="btn btn-danger btn-sm">Exclude</button>
				</div>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="alert alert-danger fade in" id="infoMainDiv">
					       	 <button data-dismiss="alert" class="close close-sm" type="button">
					            <i class="icon-remove"></i>
					        </button>
					        <span id="infoMsgDiv">No Records Found.</span>
					    </div>
					</div>
				</c:otherwise>
				</c:choose>
				
			</c:otherwise>
			</c:choose> --%>
		</c:if>
		<c:if test="${leagueId eq null and leagueId eq ''}">
			<div class="row">
				<div class="alert alert-danger fade in" id="infoMainDiv">
			       	 <button data-dismiss="alert" class="close close-sm" type="button">
			            <i class="icon-remove"></i>
			        </button>
			        <span id="infoMsgDiv">No Records Found.</span>
			    </div>
			</div>
		</c:if>
		</div>
	</div>
</form>
</div>
