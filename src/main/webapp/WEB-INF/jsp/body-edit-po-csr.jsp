<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
	
});

window.onunload = function () {
    var win = window.opener;
    if (!win.closed) {
    	window.opener.getPOGridData(0);
    }
};

function changePOCsr(){
	$('#action').val('changeCSR');
	$("#changeCsr").submit();
}
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Purchase Order</a>
			</li>
			<li><i class="fa fa-laptop"></i>PO CSR</li>
		</ol>
	</div>
</div>
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<br />

<div id="row">
	<div class="col-lg-12">
		<form class="form-validate form-horizontal" id="changeCsr" method="post" action="${pageContext.request.contextPath}/Accounting/EditPOCsr">
			<div class="form-group">
				<input type="hidden" id="action" name="action" value="" /> <input type="hidden" id="poId" name="poId" value="${poId}" />
				<div class="col-sm-5" align="center">
					<label>CSR</label> <select name="csrUser" id="csrUser" style="font-size: 13px;" class='form-control input-md'>
						<option value="auto">Auto</option>
						<c:forEach items="${users}" var="user">
							<option value="${user.userName}" <c:if test="${poCsr ==user.userName}">selected</c:if>>${user.userName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</form>
	</div>
	<br />
	<div class="col-lg-12">
		<div class="form-group" align="center">
			<button type="button" onclick="changePOCsr();" style="width:100px" class="btn btn-primary">Save</button>
		</div>
	</div>
</div>
