<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Tracking By Date</title>
 <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  
  <script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

  <style>
    .cell-title {
      font-weight: bold;
    }
	.slick-viewport {
    overflow-x: hidden !important;
}
    .cell-effort-driven {
      text-align: center;
    }
     .slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
    .slick-viewport {
    overflow-x: hidden !important;
	}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$("div#divLoading").addClass('show');
	$('#fromDate').datepicker({
		format: "mm/dd/yyyy",
        endDate: "${endDate}",
        autoclose : true,
		orientation: "bottom"
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
        endDate: "${endDate}",
        autoclose : true,
		orientation: "bottom"
    });
	setTimeout(function(){ 
		//$('#pager> div').append("<span class='slick-pager-status'> - Total <b>${totalCount}</b> Website Tracking</span>");
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserTrackingPreference()'>");
		
	}, 500);
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  grid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchTrackingBtn').click();
		    return false;  
		  }
	});
	
});
	function saveUserTrackingPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('trackinggrid',colStr);
	}

function doTrackingDateValidations(){		
		//$("#webServiceTracking").attr('action','${pageContext.request.contextPath}/Tracking/Home/');
		//$("#webServiceTracking").submit();
		websiteTrackingSearchString='';
		columnFilters = {};
		getWebServiceTrackingGridData(0);
}

function migrateTrackingData(){
	$.ajax({
		url : "${pageContext.request.contextPath}/Tracking/MigrateCassandraTrackingToSQL",
		type : "post",
		dataType: "json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData != undefined){
				jAlert(jsonData.msg);
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function resetFilters(){
	websiteTrackingSearchString='';
	columnFilters = {};
	sortingString='';
	getWebServiceTrackingGridData(0);
}

function exportToExcel(){
	var appendData = $("#webServiceTracking").serialize()+"&headerFilter="+websiteTrackingSearchString;
	var url = apiServerUrl + "WebTrackingExportToExcel/?"+appendData;
    $('#download-frame').attr('src', url);
}
</script>

<style>
	input{
		color : black !important;
	}
</style>
</head>
<body>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header"><i class="fa fa-laptop"></i> Tracking</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Tracking</a></li>
			<li><i class="fa fa-laptop"></i>Tracking Home</li>						  	
		</ol>
		
	</div>
</div>


<div style="position: relative">
	<div style="width: 100%;">
	<div class="full-width filters-div">
	
		<c:if test="${recordNotFound != null}">
		    <div class="alert alert-success fade in">
		    	<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${recordNotFound}</strong>
		    </div>
	    </c:if>
	    <c:if test="${errorMessage != null}">
		    <div class="alert alert-block alert-danger fade in">
		    	<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
		    </div>
    	</c:if>
	
	
	
		<form class="form-validate" 									
									id="webServiceTracking" method="post" onsubmit="return false"
									action="${pageContext.request.contextPath}/Tracking/Home/">
				    <input class="form-control" id="action" value="action" name="action" type="hidden" />
																
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">From date</label>
						<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Hour</label>
						<select id="startHour" name="startHour" class="form-control">
							<option value="00">00</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
						</select>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Minute</label>
						<select id="startMinute" name="startMinute" class="form-control">
							<option value="00">00</option>
							<option value="05">05</option>
							<option value="10">10</option>
							<option value="15">15</option>
							<option value="20">20</option>
							<option value="25">25</option>
							<option value="30">30</option>
							<option value="35">35</option>
							<option value="40">40</option>
							<option value="45">45</option>
							<option value="50">50</option>
							<option value="55">55</option>
							<option value="59">59</option>
						</select>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">To date</label>
						<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Hour</label>
						<select id="endHour" name="endHour" class="form-control">
							<option value="00">00</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
						</select>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Minute</label>
						<select id="endMinute" name="endMinute" class="form-control">
							<option value="00">00</option>
							<option value="05">05</option>
							<option value="10">10</option>
							<option value="15">15</option>
							<option value="20">20</option>
							<option value="25">25</option>
							<option value="30">30</option>
							<option value="35">35</option>
							<option value="40">40</option>
							<option value="45">45</option>
							<option value="50">50</option>
							<option value="55">55</option>
							<option value="59">59</option>
						</select>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Log Type</label>
						<select id="logType" name="logType" class="form-control">
							<option value="OTHER">OTHER</option>
							<option value="CONTEST">CONTEST</option>
						</select>
					</div>
					
					<!-- <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label for="name" class="control-label">Action Type</label>
						<select id="actionType" name="actionType" class="form-control">
							<option value="">--select--</option>
							<option value="CUSTOMERSIGNUP">SIGN UP</option>
							<option  value="CUSTOMERLOGIN">LOGIN</option>
							<option  value="CONFIRMORDER">CREATE ORDER</option>
							<option  value="VIEWORDER">VIEW ORDER</option>
							<option  value="GETTICKETDOWNLOAD">TICKET DOWNLOAD</option>
							<option  value="DASHBOARDINFO">DASHBOARD INFO</option>
							<option  value="JOINCONTEST">JOIN CONTEST</option>
							<option  value="EXITCONTEST">EXIT CONTEST</option>
						</select>
					</div>	 -->				
					
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label>&nbsp</label>
						<button class="btn btn-primary" type="button" id="searchTrackingBtn" style="" onclick="doTrackingDateValidations()">Search</button>
					</div>
					<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
						<label>&nbsp</label>
						<button class="btn btn-primary" type="button" id="migrateBtn" style="" onclick="migrateTrackingData()">Migrate Tracking Data</button>
					</div>
						
			</form>
		</div>
		<br/><br/><br/>

		<div class="table-responsive grid-table mt-20">
		<div class="grid-header full-width">
			<label>Tracking By Date & IP Address</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name="Export to Excel" style="float:right; margin-right:10px;">Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>			 
			</div>
		</div>
		<div id="myGrid"  style="width: 100%; height: 250px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="pager" style="width: 100%; height: 20px;"></div>
		</div>
	</div>
</div>

<script>
	var pagingInfo;
	var dataView;
	var grid;
	var trackingData = [];
	var websiteTrackingSearchString='';
	var sortingString='';
	var columnFilters = {};
	var userTrackingColumnsStr = '<%=session.getAttribute("trackinggrid")%>';
	var userTrackingColumns =[];
	var loadTrackingColumns = ["customerIpAddress", "hittingDate", "platform", "apiName","custId", "contestId","description","appVersion"];
	var allTrackingColumns = [ {
		id : "customerIpAddress",
		name : "Customer IP Address",
		width:80,
		field : "customerIpAddress",
		sortable : true
	},  {
		id : "hittingDate",
		name : "Hitting Date",
		field : "hittingDate",
		width:80,
		sortable : true
	}, {
		id : "platform",
		name : "Platform",
		field : "platform",
		width:80,
		sortable : true
	}, {
		id : "apiName",
		name : "Api Name",
		field : "apiName",
		width:80,
		sortable : true
	}, {
		id : "custId",
		name : "Customer Id",
		field : "custId",
		width:80,
		sortable : true
	},{
		id : "contestId",
		name : "Contest Id",
		field : "contestId",
		width:80,
		sortable : true
	},{
		id : "description",
		name : "Description",
		field : "description",
		width:80,
		sortable : true
	},{
		id : "appVersion",
		name : "App. Version",
		field : "appVersion",
		width:80,
		sortable : true
	}
	
 ];
	
	if(userTrackingColumnsStr!='null' && userTrackingColumnsStr!=''){
		columnOrder = userTrackingColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allTrackingColumns.length;j++){
				if(columnWidth[0] == allTrackingColumns[j].id){
					userTrackingColumns[i] =  allTrackingColumns[j];
					userTrackingColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		columnOrder = loadTrackingColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allTrackingColumns.length;j++){
				if(columnWidth == allTrackingColumns[j].id){
					userTrackingColumns[i] = allTrackingColumns[j];
					userTrackingColumns[i].width=80;
					break;
				}
			}			
		}
		//userTrackingColumns = allTrackingColumns;
	}
	
	var options = {
			enableCellNavigation : true,
			forceFitColumns : true,
			topPanelHeight : 25,
			multiSelect: false,
			showHeaderRow: true,
			headerRowHeight: 30,
			explicitInitialization: true
		};
		var sortcol = "customerIpAddress";
		var sortdir = 1;
		var percentCompleteThreshold = 0;
		var searchString = "";
		

		function deleteRecordFromGrid(id) {
			dataView.deleteItem(id);
			grid.invalidate();
		}
		/*
		function myFilter(item, args) {
			var x= item["customerIpAddress"];
			if (args.searchString  != ""
					&& x.indexOf(args.searchString) == -1) {
				
				if (typeof x === 'string' || x instanceof String) {
					if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}
		*/
		function comparer(a, b) {
			var x = a[sortcol], y = b[sortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
		
		/*		
		function toggleFilterRow() {
			grid.setTopPanelVisibility(!grid.getOptions().showTopPanel);
		}
		$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
				.mouseover(function(e) {
					$(e.target).addClass("ui-state-hover")
				}).mouseout(function(e) {
					$(e.target).removeClass("ui-state-hover")
				});
		*/
		function pagingControl(move,id){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getWebServiceTrackingGridData(pageNo);
		}
		
		function getWebServiceTrackingGridData(pageNo){
			$.ajax({
				url : "${pageContext.request.contextPath}/Tracking/Home.json",
				type : "post",
				dataType: "json",
				data : $("#webServiceTracking").serialize()+"&pageNo="+pageNo+"&headerFilter="+websiteTrackingSearchString+"&sortingString="+sortingString,
				success : function(res){
					var jsonData = res;
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						var msg =  jsonData.msg.replace(/"/g, '');
						if(msg != null && msg != ""){
							jAlert(msg);
						}
					}
					/* if(jsonData==null || jsonData=='') {
						jAlert("No Data Found.");
					} */				
					pagingInfo = jsonData.pagingInfo;
					refreshWebServiceTrackingGridValues(jsonData.webServiceTrackingList);
					clearAllSelections();
					$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserTrackingPreference()'>");
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
		
		function refreshWebServiceTrackingGridValues(jsonData){
			trackingData=[];
			if(jsonData!=null && jsonData.length > 0){
				for (var i = 0; i < jsonData.length; i++) {
					var  data= jsonData[i];
					var d = (trackingData[i] = {});
					d["id"] = data.id;
					d["customerIpAddress"] = data.customerIpAddress;
					d["hittingDate"] = data.hitDateStr;
					d["platform"] = data.platform;
					d["apiName"] = data.apiName;
					d["custId"] = data.custId;
					d["contestId"] = data.contestId;
					d["description"] = data.description;
					d["appVersion"] = data.appVersion;
				}
			}

			dataView = new Slick.Data.DataView();
			grid = new Slick.Grid("#myGrid", dataView, userTrackingColumns, options);
			grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			var cols = grid.getColumns();
			var colTest = [];
			for ( var c = 0; c < cols.length; c++) {
				//if(cols[c].name!='Title' && cols[c].name!='Start') {
				colTest.push(cols[c]);
				// }  
				grid.setColumns(colTest);
			}
			grid.invalidate();
			grid.setSelectionModel(new Slick.RowSelectionModel());
			if(pagingInfo != null){
				var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"),pagingInfo);
			}
			var columnpicker = new Slick.Controls.ColumnPicker(allTrackingColumns, grid,options);
			// move the filter panel defined in a hidden div into grid top panel
			//$("#inlineFilterPanel").appendTo(grid.getTopPanel()).show();

			grid.onKeyDown.subscribe(function(e) {
				// select all rows on ctrl-a
				if (e.which != 65 || !e.ctrlKey) {
					return false;
				}
				var rows = [];
				for ( var i = 0; i < dataView.getLength(); i++) {
					rows.push(i);
				}
				grid.setSelectedRows(rows);
				e.preventDefault();
			});
			grid.onSort.subscribe(function(e, args) {
				sortcol = args.sortCol.field;
				if(sortingString.indexOf(sortcol) < 0){
					sortdi = 'ASC';
				}else{
					if(sortdi == 'DESC' ){
						sortdi = 'ASC';
					}else{
						sortdi = 'DESC';
					}
				}
				sortingString = '';
				sortingString +=',SORTINGCOLUMN:'+sortcol+',SORTINGORDER:'+sortdi+',';
				getWebServiceTrackingGridData(0);
			});
			// wire up model events to drive the grid
			dataView.onRowCountChanged.subscribe(function(e, args) {
				grid.updateRowCount();
				grid.render();
			});
			dataView.onRowsChanged.subscribe(function(e, args) {
				grid.invalidateRows(args.rows);
				grid.render();
			});
			$(grid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
			 	websiteTrackingSearchString='';
				 var columnId = $(this).data("columnId");
				  if (columnId != null) {
					columnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in columnFilters) {
						  if (columnId !== undefined && columnFilters[columnId] !== "") {
							  websiteTrackingSearchString += columnId + ":" +columnFilters[columnId]+",";
						  }
						}
						getWebServiceTrackingGridData(0);
					}
				  }
				  
			});
			grid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id == 'hittingDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
				
			});
			grid.init();
			dataView.beginUpdate();
			dataView.setItems(trackingData);
			dataView.endUpdate();
			// if you don't want the items that are not visible (due to being filtered out
			// or being on a different page) to stay selected, pass 'false' to the second arg
			dataView.syncGridSelection(grid, true);
			$("#gridContainer").resizable();
			grid.resizeCanvas();
			 $("div#divLoading").removeClass('show');
		}
		
		//show the pop window center
		function popupCenter(url, title, w, h) {
			// Fixes dual-screen position                         Most browsers      Firefox  
			var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
					: screen.left;
			var dualScreenTop = window.screenTop != undefined ? window.screenTop
					: screen.top;

			width = window.innerWidth ? window.innerWidth
					: document.documentElement.clientWidth ? document.documentElement.clientWidth
							: screen.width;
			height = window.innerHeight ? window.innerHeight
					: document.documentElement.clientHeight ? document.documentElement.clientHeight
							: screen.height;

			var left = ((width / 2) - (w / 2)) + dualScreenLeft;
			var top = ((height / 2) - (h / 2)) + dualScreenTop;
			var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
					+ ', height=' + h + ', top=' + top + ', left=' + left);

			// Puts focus on the newWindow  
			if (window.focus) {
				newWindow.focus();
			}
		}
		
		$(window).load(function() {
			pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
			refreshWebServiceTrackingGridValues(JSON.parse(JSON.stringify(${webServiceTrackingList})));
		});
</script>
</body>
</html>
