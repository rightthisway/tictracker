<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
        startDate : "01-01-2016",
        endDate: "${endDate}"
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
        startDate : "01-01-2016",
        endDate: "${endDate}"
    });
});
</script>

<style>
input {
	color: black !important;
}
</style>


<div class="row">
	<div class="col-lg-12">
		<!-- <h3 class="page-header">
			<i class="fa fa-laptop"></i>Accounting
		</h3> -->
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Accounting</a></li>
			<li><i class="fa fa-laptop"></i>Purchase Order</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<form class="form-validate form-horizontal" id="auditUserForm"
				method="post" style="width: 60%;">
				<!-- <input class="form-control" id="action" value="action" name="action" type="hidden" /> -->

				<table class="table table-striped table-advance table-hover">
					<tbody>
						<tr>
							<td>Search By</td>
							<td><select name="searchBy">
									<option value="invoiceNo">PO No</option>
									<option value="customer">Customer</option>
							</select></td>
							<td><input style="width: 180px;" id="searchTxt"
								name="searchTxt" type="text" /></td>
							<td>
								<button class="btn btn-primary" title="Search" type="button">Search</button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</section>

		<section class="panel">
			<header class="panel-heading tab-bg-primary ">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#home">All (400)</a>
					</li>
					<li class=""><a data-toggle="tab"  href="#about">Outstanding (20)</a></li>
					<li class=""><a data-toggle="tab" href="#profile">Paid (370)</a>
					</li>
					<li class=""><a data-toggle="tab" href="#contact">Voided (10)</a>
					</li>
				<div style="float:right;">
					<label style="font-size:16px;color:blue;">Purchase Order Date Range : </label>
					<input style="width:180px;" id="fromDate" name="fromDate" value="${fromDate}" type="text"/>
					<input style="width:180px;" id="toDate" name="toDate" value="${toDate}" type="text"/>
					<button class="btn btn-primary" type="button">Search</button>
				</div>
				</ul>
				
			</header>
			<div class="panel-body">
				<div class="tab-content">
					<div id="home" class="tab-pane active">
						<table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class="icon_profile"></i> Customer</th>
                                 <th> Mercury Buyer</th>
                                 <th> Invoice#</th>
                                 <th><i class="icon_calendar"></i> Created</th>
                                 <th> Grand Total</th>
                                 <th> CSR</th>
                                 <th> Emailed ?</th>
                                 <th> Mercury Trans ID</th>
                                 <th> E-Ticket Attached ?</th>
                              </tr>
                              <tr>
                                 <td>Angeline Mcclain</td>
                                 <td>TicPick</td>
                                 <td>1234</td>
                                 <td>March 11 2016 09:00 AM</td>
                                 <td>$560</td>
                                 <td>Request Auto-Process</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                                 <td>1515456</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                              </tr>
                               <tr>
                                 <td>John Man</td>
                                 <td></td>
                                 <td>5858</td>
                                 <td>March 11 2016 11:30 PM</td>
                                 <td>$120</td>
                                 <td>Leaor Zahvi</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                                 <td>2014578</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                              </tr>                           
                           </tbody>
                        </table>
					</div>
					<div id="about" class="tab-pane">
						<table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class="icon_profile"></i> Customer</th>
                                 <th> Mercury Buyer</th>
                                 <th> Invoice#</th>
                                 <th><i class="icon_calendar"></i> Created</th>
                                 <th> Grand Total</th>
                                 <th> CSR</th>
                                 <th> Emailed ?</th>
                                 <th> Mercury Trans ID</th>
                                 <th> E-Ticket Attached ?</th>
                              </tr>
                              <tr>
                                 <td>Mark John</td>
                                 <td>TicPick</td>
                                 <td>7810</td>
                                 <td>March 01 2016 6:00 AM</td>
                                 <td>$560</td>
                                 <td>Request Auto-Process</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                                 <td>1515456</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                              </tr>
                               <tr>
                                 <td>David Peter</td>
                                 <td></td>
                                 <td>4691</td>
                                 <td>March 10 2016 07:30 PM</td>
                                 <td>$230</td>
                                 <td>Mike Zippo</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                                 <td>1245785</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                              </tr>                           
                           </tbody>
                        </table>
					</div>
					<div id="profile" class="tab-pane">
						<table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class="icon_profile"></i> Customer</th>
                                 <th> Mercury Buyer</th>
                                 <th> Invoice#</th>
                                 <th><i class="icon_calendar"></i> Created</th>
                                 <th> Grand Total</th>
                                 <th> CSR</th>
                                 <th> Emailed ?</th>
                                 <th> Mercury Trans ID</th>
                                 <th> E-Ticket Attached ?</th>
                              </tr>
                              <tr>
                                 <td>Miller Mack</td>
                                 <td>TicPick</td>
                                 <td>2746</td>
                                 <td>March 05 2016 05:00 PM</td>
                                 <td>$560</td>
                                 <td>Request Auto-Process</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                                 <td>4896315</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                              </tr>
                               <tr>
                                 <td>John Man</td>
                                 <td></td>
                                 <td>5858</td>
                                 <td>March 11 2016 11:30 PM</td>
                                 <td>$120</td>
                                 <td>Leaor Zahvi</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                                 <td>2014578</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                              </tr>                           
                           </tbody>
                        </table>
					</div>
					<div id="contact" class="tab-pane">
						<table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class="icon_profile"></i> Customer</th>
                                 <th> Mercury Buyer</th>
                                 <th> Invoice#</th>
                                 <th><i class="icon_calendar"></i> Created</th>
                                 <th> Grand Total</th>
                                 <th> CSR</th>
                                 <th> Emailed ?</th>
                                 <th> Mercury Trans ID</th>
                                 <th> E-Ticket Attached ?</th>
                              </tr>
                              <tr>
                                 <td>Henry Harris</td>
                                 <td>TicPick</td>
                                 <td>5853</td>
                                 <td>Feb 29 2016 09:00 AM</td>
                                 <td>$158</td>
                                 <td>Request Auto-Process</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                                 <td>1515456</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                              </tr>
                               <tr>
                                 <td>John Man</td>
                                 <td></td>
                                 <td>8967</td>
                                 <td>Feb 11 2016 11:30 PM</td>
                                 <td>$589</td>
                                 <td>Leaor Zahvi</td>
                                 <td><img width="12px" height="12px" src="../resources/images/crossMarkIcon.png"/></td>
                                 <td>2014578</td>
                                 <td><img width="15px" height="15px" src="../resources/images/rightTickIcon.png"/></td>
                              </tr>                           
                           </tbody>
                        </table>
					</div>
				</div>
			</div>
		</section>

	</div>
</div>