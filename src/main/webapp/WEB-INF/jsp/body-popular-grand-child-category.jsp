<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Popular Grand Child Category</title>
  
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.deleteClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.auditClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

</style>
</head>
<body>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Grand Child Category</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Popular Grand Child Category</li>
		</ol>

	</div>
	
	<div class="col-lg-12">
                <!--  search form start -->
			<form:form class="form-inline" role="form" id="grandChildSearch" onsubmit="return false" method="post" action="${pageContext.request.contextPath}/RewardTheFan/PopularGrandChildCategory">
			<input type="hidden" id="productId" name="productId" value="${product.id}" />
				<div class="col-sm-1"> </div>
				<!--<label class="control-label col-sm-2" >Search Type</label>-->
				<div class="col-sm-2">
					<select id="searchType" name="searchType" class="form-control input-sm m-bot15" style = "width:100%">
					  <option value="grandChildCategoryName" >Grand Child Category</option>
					  <option value="childCategoryName" <c:if test="${searchType eq 'childCategoryName'}">selected</c:if> >Child Category</option>
					  <option value="parentCategoryName" <c:if test="${searchType eq 'parentCategoryName'}">selected</c:if> >Parent Category</option>
					  
					</select>
				</div>
               <div class="col-sm-3">
				<div class="navbar-form">
					<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
				</div>
				</div>
				</form:form>
				
				<div class="col-sm-3">
				 <!-- <button type="submit" class="btn btn-primary">search</button> -->
				 <button type="button" id="searchPopularGrandChildBtn" class="btn btn-primary" onclick="getGrandChildGridData();">search</button>
				 </div>
				 <div class="col-sm-1"> </div>
               
                <!--  search form end -->                
           </div>
           
</div>

<!-- grandChild Grid Code Starts -->
<div style="position: relative" id="grandChildGridDiv">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Grand Child Category Details</label> <span id="grandChild_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"
				onclick="grandChildGridToggleFilterRow()"></span>
		</div>
		<div id="grandChild_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="grandChild_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="grandChild_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with Grand Child Category Name including <input type="text" id="grandChildGridSearch">
</div>


<script>

$(document).ready(function() {
	$('#grandChildSearch').submit(function(){
		e.preventDefault();
	});
	
	$('#searchValue').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {
		 getGrandChildGridData();
	    //$('#searchPopularGrandChildBtn').click();
	    //return false;  
	  }
	});
});

var grandChildCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});

	var grandChildDataView;
	var grandChildGrid;
	var grandChildData = [];
	var userGrandChildColumnsStr = '<%=session.getAttribute("grandchildgrid")%>';
	var userGrandChildColumns =[];
	var allGrandChildColumns = [ grandChildCheckboxSelector.getColumnDefinition(),
	 {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory",
		width:80,		
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		width:80,
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	} ];

	if(userGrandChildColumnsStr!='null' && userGrandChildColumnsStr!=''){
		columnOrder = userGrandChildColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allGrandChildColumns.length;j++){
				if(columnWidth[0] == allGrandChildColumns[j].id){
					userGrandChildColumns[i] =  allGrandChildColumns[j];
					userGrandChildColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userGrandChildColumns = allGrandChildColumns;
	}
	
	var grandChildOptions = {
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var grandChildGridSortcol = "grandChildCategory";
	var grandChildGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var grandChildGridSearchString = "";

	
	function deleteRecordFromGrandChildGrid(id) {
		grandChildDataView.deleteItem(id);
		grandChildGrid.invalidate();
	}
	function grandChildGridFilter(item, args) {
		var x= item["grandChildCategory"];
		if (args.grandChildGridSearchString  != ""
				&& x.indexOf(args.grandChildGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.grandChildGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function grandChildGridComparer(a, b) {
		var x = a[grandChildGridSortcol], y = b[grandChildGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function grandChildGridToggleFilterRow() {
		grandChildGrid.setTopPanelVisibility(!grandChildGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#grandChild_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
			$(e.target).addClass("ui-state-hover")
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover")
		});

	
	function getGrandChildGridData() {
		var searchValue = $("#searchValue").val();
		if(searchValue== null || searchValue == '') {
			jAlert('Enter valid key to searh grandChilds');
			return false;
		}
		
		$.ajax({
			  
			url : "${pageContext.request.contextPath}/GetGrandChildCategory",
			type : "post",
			data : $("#grandChildSearch").serialize(),
			dataType:"json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				//jAlert(jsonData);
				if(jsonData==null || jsonData=='') {
					jAlert("No Grand Child Category found.");
				} 
				refreshGrandChildGridValues(jsonData,true);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
function refreshGrandChildGridValues(jsonData,isAjaxCall) {
	 $("div#divLoading").addClass('show');
		grandChildData = [];
		
		if(isAjaxCall) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (grandChildData[i] = {});
				d["id"] = data.grandChildCategoryId;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
			}
		} else {
			var i=0;
			<c:forEach var="grandChild" items="${grandChildCategoryList}">
			var d = (grandChildData[i] = {});
			d["id"] = "${grandChild.grandChildCategoryId}";
			//d["id"] = "id_" + i;
			d["grandChildCategory"] = "${grandChild.grandChildCategoryName}";
			d["childCategory"] = "${grandChild.childCategoryName}";
			d["parentCategory"] = "${grandChild.parentCategoryName}";
			
			i++;
			</c:forEach>
		}

		grandChildDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		grandChildGrid = new Slick.Grid("#grandChild_grid", grandChildDataView, userGrandChildColumns, grandChildOptions);
		grandChildGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		grandChildGrid.setSelectionModel(new Slick.RowSelectionModel());
		grandChildGrid.registerPlugin(grandChildCheckboxSelector);
		var grandChildGridPager = new Slick.Controls.Pager(grandChildDataView, grandChildGrid, $("#grandChild_pager"));
		var grandChildGridColumnpicker = new Slick.Controls.ColumnPicker(allGrandChildColumns, grandChildGrid,
				grandChildOptions);

		// move the filter panel defined in a hidden div into grandChildGrid top panel
		$("#grandChild_inlineFilterPanel").appendTo(grandChildGrid.getTopPanel()).show();

		/*grandChildGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < grandChildDataView.getLength(); i++) {
				rows.push(i);
			}
			grandChildGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		grandChildGrid.onSort.subscribe(function(e, args) {
			grandChildGridSortdir = args.sortAsc ? 1 : -1;
			grandChildGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				grandChildDataView.fastSort(grandChildGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				grandChildDataView.sort(grandChildGridComparer, args.sortAsc);
			}
		});
		// wire up model grandChilds to drive the grandChildGrid
		grandChildDataView.onRowCountChanged.subscribe(function(e, args) {
			grandChildGrid.updateRowCount();
			grandChildGrid.render();
		});
		grandChildDataView.onRowsChanged.subscribe(function(e, args) {
			grandChildGrid.invalidateRows(args.rows);
			grandChildGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#grandChildGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			grandChildGridSearchString = this.value;
			updateGrandChildGridFilter();
		});
		function updateGrandChildGridFilter() {
			grandChildDataView.setFilterArgs({
				grandChildGridSearchString : grandChildGridSearchString
			});
			grandChildDataView.refresh();
		}
		
		// initialize the model after all the grandChilds have been hooked up
		grandChildDataView.beginUpdate();
		grandChildDataView.setItems(grandChildData);
		grandChildDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			grandChildGridSearchString : grandChildGridSearchString
		});
		grandChildDataView.setFilter(grandChildGridFilter);
		grandChildDataView.endUpdate();
		grandChildDataView.syncGridSelection(grandChildGrid, true);
		$("#gridContainer").resizable();
		grandChildGrid.resizeCanvas();
		 $("div#divLoading").removeClass('show');
		/* var grandChildrowIndex;
		grandChildGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprGrandChildRowIndex = grandChildGrid.getSelectedRows([0])[0];
			if (temprGrandChildRowIndex != grandChildrowIndex) {
				grandChildrowIndex = temprGrandChildRowIndex;
				 var grandChildId =grandChildGrid.getDataItem(temprGrandChildRowIndex).id;
				//jAlert(grandChildId);
				//getCategoryTicketGroupsforGrandChild(grandChildId);
			}
		}); */
	}
	
	function getSelectedGrandChildGridId() {
		var temprGrandChildRowIndex = grandChildGrid.getSelectedRows();
		
		var grandChildIdStr='';
		$.each(temprGrandChildRowIndex, function (index, value) {
			grandChildIdStr += ','+grandChildGrid.getDataItem(value).id;
		});
		
		if(grandChildIdStr != null && grandChildIdStr!='') {
			grandChildIdStr = grandChildIdStr.substring(1, grandChildIdStr.length);
			 return grandChildIdStr;
		}
	}
	function getSelectedGrandChildGridRowCount() {
		var temprGrandChildRowIndex = grandChildGrid.getSelectedRows();
		
		var rowCount=0;
		$.each(temprGrandChildRowIndex, function (index, value) {
			rowCount++;
		});
		
		return rowCount;
	}
	
	function updatePopularGrandChilds(grandChildIds,productId) {
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePopularGrandChildCategory",
			type : "post",
			data : "action=create&productId="+productId+"&grandChildIds="+ grandChildIds,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.length >0){
					jAlert("Selected Grandchild added to popular Grand Childs.");
				}
				refreshpopGrandChildGridValues(jsonData,true);
				grandChildGrid.setSelectedRows([]);
				clearAllSelections();
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function addPopularGrandChilds() {
		var grandChildIds = getSelectedGrandChildGridId();
		var productId=$("#productId").val();
		var flag = true;
		if(grandChildIds == null || grandChildIds=='') {
			flag= false;
			jAlert("Select a Grand Child Category to add as Popular Grand Child Categories.");
			return false;
		}
		
		var selectedGrandChildCount = getSelectedGrandChildGridRowCount();
		if(flag) {
			$.ajax({
				url : "${pageContext.request.contextPath}/GetPopularGrandChildsCount",
				type : "post",
				data : "productId="+productId,
				success : function(res){
					//var jsonData = JSON.parse(JSON.stringify(res));
					if((selectedGrandChildCount+ parseInt(res)) > 20){
						jAlert("Maximum Eligible popluar grand childs are 20.\n"
								+"Remove some existing poluar grand childs(Count : "+parseInt(res)+") to add selected grand childs as popular grand childs.");
						return false;
					} else {
						updatePopularGrandChilds(grandChildIds,productId);
					}				
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		return false;	
		} 
	}
	
</script>

<!-- grandChild Grid Code Starts -->
<br />
<!-- <header class="panel-heading"> 
	<a data-toggle="modal" class="btn btn-primary" id="addCategoryTickets" onclick="addModal();">Add</a>
</header> -->

<!-- popGrandChild Grid Code Starts -->
<br />
<div style="position: relative;" id="popGrandChildGridDiv">
<a data-toggle="modal" class="btn btn-primary" id="addPopularGrandChilds" onclick="addPopularGrandChilds();">Add Popular Grand Child Category</a>
<div id="AddpopGrandChildGroupSuccessMsg" style="display: none;" class="alert alert-success fade in">
 	Popular Grand Child Category Added Successfully.
  </div>
  
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label id="popGrandChildGridHeaderLabel">Popular Grand Child Category</label> <span id="popGrandChild_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"
				onclick="popGrandChildGridToggleFilterRow()"></span>
		</div>
		<div id="popGrandChild_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="popGrandChild_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>


<div id="popGrandChild_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
  Show records with GrandChildCategory Name including <input type="text" id="popGrandChildGridSearch">
</div>
<br />
<br />
<a data-toggle="modal" class="btn btn-primary" id="deletePopularGrandChilds" onclick="deletePopularGrandChilds();">Delete Popular Grand Child Category</a>
<script>
var popGrandChildCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var popGrandChildDataView;
	var popGrandChildGrid;
	var popGrandChildData = [];
	var userPopGrandChildColumnsStr = '<%=session.getAttribute("popgrandchildgrid")%>';
	var userPopGrandChildColumns =[];
	var allPopGrandChildColumns =  [ popGrandChildCheckboxSelector.getColumnDefinition(),
	{id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory",
		width:80,		
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	},{id: "createdBy", 
		name: "Created By", 
		field: "createdBy", 
		width:80,
		sortable: true
	}, {
		id : "createdDate",
		field : "createdDate",
		name : "Created Date",
		width:80,
		sortable: true
	} ];

	if(userPopGrandChildColumnsStr!='null' && userPopGrandChildColumnsStr!=''){
		var columnOrder = userPopGrandChildColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			for(var j=0;j<allPopGrandChildColumns.length;j++){
				columnWidth = columnOrder[i].split(":");
				if(columnWidth[0] == allPopGrandChildColumns[j].id){
					userPopGrandChildColumns[i] =  allPopGrandChildColumns[j];
					userPopGrandChildColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userPopGrandChildColumns = allPopGrandChildColumns;
	}
	
	var popGrandChildOptions = {
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var popGrandChildGridSortcol = "grandChildCategory";
	var popGrandChildGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var popGrandChildGridSearchString = "";

	function deleteRecordFromGrandChildGrid(id) {
		popGrandChildDataView.deleteItem(id);
		popGrandChildGrid.invalidate();
	}
	function popGrandChildGridFilter(item, args) {
		var x= item["grandChildCategory"];
		if (args.popGrandChildGridSearchString  != ""
				&& x.indexOf(args.popGrandChildGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.popGrandChildGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function popGrandChildGridComparer(a, b) {
		var x = a[popGrandChildGridSortcol], y = b[popGrandChildGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function popGrandChildGridToggleFilterRow() {
		popGrandChildGrid.setTopPanelVisibility(!popGrandChildGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#popGrandChild_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

	function refreshpopGrandChildGridValues(jsonData,isAjaxCall) {
		
		/*if(eventdetailStr != '0') {
			$('#popGrandChildGridDiv').show();
			//$('#popGrandChildGridHeaderLabel').text(eventdetailStr);
		}*/
		popGrandChildData = [];
		if(isAjaxCall) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (popGrandChildData[i] = {});
				d["id"] = data.grandChildCategoryId;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				d["createdBy"] = data.createdBy;
				d["createdDate"] = data.createdDate;
			}
		} else {
			var i=0;
			<c:forEach var="popGrandChild" items="${popGrandChilds}">
			var d = (popGrandChildData[i] = {});
			d["id"] = "${popGrandChild.grandChildCategoryId}";
			//d["id"] = "id_" + i;
			d["grandChildCategory"] = "${popGrandChild.grandChildCategory.name}";
			d["childCategory"] = "${popGrandChild.grandChildCategory.childCategory.name}";
			d["parentCategory"] = "${popGrandChild.grandChildCategory.childCategory.parentCategory.name}";
			d["createdBy"] = "${popGrandChild.createdBy}";
			d["createdDate"] = "${popGrandChild.createdDateStr}";
			i++;
			</c:forEach>
		}
		
		popGrandChildDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		popGrandChildGrid = new Slick.Grid("#popGrandChild_grid", popGrandChildDataView, userPopGrandChildColumns, popGrandChildOptions);
		popGrandChildGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		popGrandChildGrid.setSelectionModel(new Slick.RowSelectionModel());
		popGrandChildGrid.registerPlugin(popGrandChildCheckboxSelector);
		var popGrandChildGridPager = new Slick.Controls.Pager(popGrandChildDataView, popGrandChildGrid, $("#popGrandChild_pager"));
		var popGrandChildGridColumnpicker = new Slick.Controls.ColumnPicker(allPopGrandChildColumns, popGrandChildGrid,
				popGrandChildOptions);

		// move the filter panel defined in a hidden div into popGrandChildGrid top panel
		$("#popGrandChild_inlineFilterPanel").appendTo(popGrandChildGrid.getTopPanel()).show();

		/* popGrandChildGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < popGrandChildDataView.getLength(); i++) {
				rows.push(i);
			}
			popGrandChildGrid.setSelectedRows(rows);
			e.preventDefault();
		}); */
		
		popGrandChildGrid.onSort.subscribe(function(e, args) {
			popGrandChildGridSortdir = args.sortAsc ? 1 : -1;
			popGrandChildGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				// using temporary Object.prototype.toString override
				// more limited and does lexicographic sort only by default, but can be much faster
				// use numeric sort of % and lexicographic for everything else
				popGrandChildDataView.fastSort(popGrandChildGridSortcol, args.sortAsc);
			} else {
				// using native sort with comparer
				// preferred method but can be very slow in IE with huge datasets
				popGrandChildDataView.sort(popGrandChildGridComparer, args.sortAsc);
			}
		});
		// wire up model popGrandChilds to drive the popGrandChildGrid
		popGrandChildDataView.onRowCountChanged.subscribe(function(e, args) {
			popGrandChildGrid.updateRowCount();
			popGrandChildGrid.render();
		});
		popGrandChildDataView.onRowsChanged.subscribe(function(e, args) {
			popGrandChildGrid.invalidateRows(args.rows);
			popGrandChildGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#popGrandChildGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			popGrandChildGridSearchString = this.value;
			updateGrandChildGridFilter();
		});
		function updateGrandChildGridFilter() {
			popGrandChildDataView.setFilterArgs({
				popGrandChildGridSearchString : popGrandChildGridSearchString
			});
			popGrandChildDataView.refresh();
		}
		
		// initialize the model after all the popGrandChilds have been hooked up
		popGrandChildDataView.beginUpdate();
		popGrandChildDataView.setItems(popGrandChildData);
		popGrandChildDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			popGrandChildGridSearchString : popGrandChildGridSearchString
		});
		popGrandChildDataView.setFilter(popGrandChildGridFilter);
		popGrandChildDataView.endUpdate();
		popGrandChildDataView.syncGridSelection(popGrandChildGrid, true);
		popGrandChildGrid.setSelectedRows([]);
		popGrandChildGrid.resizeCanvas();
	}
	
	<!-- popGrandChild Grid Code Ends -->
	
	function getSelectedPopGrandChildGridId() {
		var temprGrandChildRowIndex = popGrandChildGrid.getSelectedRows();
		
		var grandChildIdStr='';
		$.each(temprGrandChildRowIndex, function (index, value) {
			grandChildIdStr += ','+popGrandChildGrid.getDataItem(value).id;
		});
		
		if(grandChildIdStr != null && grandChildIdStr!='') {
			grandChildIdStr = grandChildIdStr.substring(1, grandChildIdStr.length);
			 return grandChildIdStr;
		}
	}
	
	function deletePopularGrandChilds() {
		var grandChildIds = getSelectedPopGrandChildGridId();
		var productId=$("#productId").val();
		if(grandChildIds != null && grandChildIds!='') {
			jConfirm("Are you sure you want to remove selected grand childs from popular grand childs?","Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePopularGrandChildCategory",
						type : "post",
						data : "action=delete&productId="+productId+"&grandChildIds="+ grandChildIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							jAlert("Selected Grand Child removed from popular Grand Childs.");
							refreshpopGrandChildGridValues(jsonData,true);
							clearAllSelections();
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		}else{
			jAlert("Select a Grand Child Category to remove from Popular Grand Child Categories.");
		}
	}
	
	$('#menuContainer').click(function(){
		  if($('.ABCD').length >0){
			   $('#menuContainer').removeClass('ABCD');
		  }else{
			   $('#menuContainer').addClass('ABCD');
		  }
		  grandChildGrid.resizeCanvas();
		  popGrandChildGrid.resizeCanvas();
	});
	
	function clearAllSelections(){
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
		
	}
	
	function saveUserGrandChildPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grandChildGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('grandchildgrid',colStr);
	}
	function saveUserPopGrandChildPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = popGrandChildGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('popgrandchildgrid',colStr);
	}
	
	
	//call functions once page loaded
	window.onload = function() {
		refreshGrandChildGridValues(null,false);
		refreshpopGrandChildGridValues(null,false) ;
		$('#grandChild_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGrandChildPreference()'>");
		$('#popGrandChild_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPopGrandChildPreference()'>");
		enableMenu();
	};
</script>
	
</body>
</html>