<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage PO</title>
 <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
	
  <style>
    .cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
     .slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }

#addUser {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

</style>
</head>
<body>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> ADMIN
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Admin</a>
			</li>
			<li><i class="fa fa-laptop"></i>Paypal Transactions</li>
		</ol>

	</div>
		<!-- 
			<div class="col-lg-12">
                
				<form role="form" id="paypalSearch" onsubmit="return false" action="${pageContext.request.contextPath}/Admin/getPayPalTracking">
											
				<div class="form-group col-xs-2 col-md-2">
					<label for="name" class="control-label">Order Id</label>
					<input class="form-control searchcontrol" placeholder="Order Id" type="text" id="orderId" name="orderId" value="${orderId}">
				</div>
				
				<div class="form-group col-xs-2 col-md-2">
					<label for="name" class="control-label">Customer</label>
					<input class="form-control searchcontrol" placeholder="Customer" type="text" id="customerName" name="customerName" value="${customerName}">
				</div>
				
				<div class="form-group col-xs-3 col-md-3">
					<label for="name" class="control-label">Transaction Status</label> 
					<select id="transactionStatus" name="transactionStatus" class="form-control ">						
						<option <c:if test="${transactionStatus=='COMPLETED'}"> Selected </c:if> value="COMPLETED">Completed</option>
						<option <c:if test="${transactionStatus=='INITIATED'}"> Selected </c:if> value="INITIATED">Initiated</option>
					</select>
				</div>
				<div class="form-group col-xs-1 col-md-1">
				<label for="name" class="control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				 <button type="button" id="searchPaypalBtn" class="btn btn-primary" onclick="searchPaypal();">search</button>
				 </div>
				 
               </form>
                                
           </div>
    	 -->
</div>
 <br/>
<br/>
<div style="position: relative">
	<div class="table-responsive grid-table">	
		<div class="grid-header full-width">
			<label>PayPal Transactions</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="poGrid"style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="pager" style="width: 100%; height: 20px;"></div>
		<br />
	</div>
</div>

<script>
	var pagingInfo;
	var paypalId;
	var dataView;
	var grid;
	var data = [];
	var columns=[];
	var sortingString ='';
	var paypalTrackingSearchString='';
	var columnFilters = {};
	var userPOColumnsStr = '<%=session.getAttribute("paypalgrid")%>';
	var userPOColumns =[];
	var allPOColumns = [ {
		id : "customerId",
		name : "Customer Id",
		width:80,
		field : "customerId",
		sortable : true
	},{
		id : "firstName",
		name : "Customer",
		width:80,
		field : "firstName",
		sortable : true
	}, {
		id : "lastName",
		name : "Last Name",
		field : "lastName",
		width:80,
		sortable : true
	}, {
		id : "email",
		name : "Email",
		field : "email",
		width:80,
		sortable : true
	},{
		id : "phone",
		name : "Phone",
		field : "phone",
		width:80,
		sortable : true
	},{
		id : "orderId",
		name : "Order Id",
		field : "orderId",
		width:80,
		sortable : true
	},{
		id : "orderTotal",
		name : "Order Total",
		field : "orderTotal",
		width:80,
		sortable : true
	},{
		id : "orderType",
		name : "Order Type",
		field : "orderType",
		width:80,
		sortable : true
	},{
		id : "eventId",
		name : "Event Id",
		field : "eventId",
		width : 100,
		sortable : true
	},  {
		id: "quantity", 
		name: "Quantity", 
		field: "quantity",
		width:80,		
		sortable: true
	},{
		id: "zone",
		name: "Zone",
		field: "zone",
		width:80,
		sortable: true
	},{
		id : "paypalTransactionId",
		name : "Paypal Transaction Id",
		field : "paypalTransactionId",
		width : 80,
		sortable : true
	}, {
		id : "transactionId",
		name : "Transaction Id",
		field : "transactionId",
		width : 80,
		sortable : true
	}, {
		id : "paymentId",
		name : "Payment Id",
		field : "paymentId",
		width : 80,
		sortable : true
	},{
		id: "platform",
		name: "Platform",
		field: "platform",
		width:80,
		sortable: true
	},{
		id: "status",
		name: "Status",
		field: "status",
		width:80,
		sortable: true
	},{
		id: "createdDate",
		name: "Created Date",
		field: "createdDate",
		width:80,
		sortable: true
	},{
		id: "lastUpdated",
		name: "Last Updated",
		field: "lastUpdated",
		width:80,
		sortable: true
	},{
		id: "transactionDate",
		name: "Transaction Date",
		field: "transactionDate",
		width:80,
		sortable: true
	}];

	if(userPOColumnsStr!='null' && userPOColumnsStr!=''){
		var columnOrder = userPOColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allPOColumns.length;j++){
				if(columnWidth[0] == allPOColumns[j].id){
					userPOColumns[i] =  allPOColumns[j];
					userPOColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userPOColumns = allPOColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "customerName";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromGrid(id) {
		dataView.deleteItem(id);
		grid.invalidate();
	}
	/*
	function myFilter(item, args) {
		var x= item["customerName"];
		if (args.searchString != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function toggleFilterRow() {
		grid.setTopPanelVisibility(!grid.getOptions().showTopPanel);
	}
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getPaypalGridData(pageNo);
	}
	
	function getPaypalGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/Admin/PaypalTracking.json",
			type : "post",
			dataType: "json",
			data : $("#paypalSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+paypalTrackingSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */ 
				pagingInfo = jsonData.pagingInfo;
				refreshPaypalGridValues(jsonData.trackingList);
				clearAllSelections();
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPOPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshPaypalGridValues(jsonData) {
		data = [];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  paypal = jsonData[i];
				var d = (data[i] = {});
				d["id"] = i;
				d["customerId"] = paypal.customerId;
				d["firstName"] = paypal.payerFirstName;
				d["lastName"] = paypal.payerLastName;
				d["email"] = paypal.payerEmail;
				d["phone"] = paypal.payerPhone;
				d["orderId"] = paypal.orderId;
				d["orderTotal"] = paypal.orderTotal;
				d["orderType"] = paypal.orderType;
				d["eventId"] = paypal.eventId;
				d["quantity"] = paypal.quantity;
				d["zone"] = paypal.zone;
				d["paypalTransactionId"] = paypal.paypalTransactionId;
				d["transactionId"] = paypal.transactionId;
				d["paymentId"] = paypal.paymentId;
				d["platform"] = paypal.platform;
				d["status"] = paypal.status;
				d["createdDate"] = paypal.createdDateStr;
				d["lastUpdated"] = paypal.lastUpdatedStr;
				d["transactionDate"] = paypal.transactionDateStr;
			}
		}
		
		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#poGrid", dataView, userPOColumns, options);
		grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		grid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"),pagingInfo);
		}
		
		var columnpicker = new Slick.Controls.ColumnPicker(allPOColumns, grid,
				options);

		// move the filter panel defined in a hidden div into grid top panel
		//$("#inlineFilterPanel").appendTo(grid.getTopPanel()).show();

		grid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < dataView.getLength(); i++) {
				rows.push(i);
			}
			grid.setSelectedRows(rows);
			e.preventDefault();
		});
		grid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = grid.getSelectedRows([0])[0];
			if (temprEventRowIndex != paypalId) {
				paypalId = grid.getDataItem(temprEventRowIndex).purchaseOrderId;
			}
		});
		
		grid.onSort.subscribe(function(e, args) {
			sortcol = args.sortCol.field;
			if(sortingString.indexOf(sortcol) < 0){
				sortdir = 'ASC';
			}else{
				if(sortdir == 'DESC' ){
					sortdir = 'ASC';
				}else{
					sortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+sortcol+',SORTINGORDER:'+sortdir+',';
			getPaypalGridData(0);
		});
		// wire up model events to drive the grid
		dataView.onRowCountChanged.subscribe(function(e, args) {
			grid.updateRowCount();
			grid.render();
		});
		dataView.onRowsChanged.subscribe(function(e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});
		$(grid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	paypalTrackingSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  paypalTrackingSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getPaypalGridData(0);
				}
			  }
		 
		});
		grid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'createdDate' || args.column.id == 'lastUpdated' || args.column.id == 'transactionDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
			
		});
		grid.init();
			
		/*
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			dataView.setFilterArgs({
				searchString : searchString
			});
			dataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		dataView.beginUpdate();
		dataView.setItems(data);
		/*dataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		dataView.setFilter(myFilter);*/
		dataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		dataView.syncGridSelection(grid, true);
		$("#gridContainer").resizable();
		grid.resizeCanvas();
	}
		
	function searchPaypal(){
		$("#action").val('search');
		getPaypalGridData(0);
	}
	
	function resetFilters(){
		$("#action").val('search');
		paypalTrackingSearchString='';
		sortingString ='';
		columnFilters = {};
		getPaypalGridData(0);
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+paypalTrackingSearchString;
	    var url = apiServerUrl + "PayPalTrackingExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
	
	function saveUserPOPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('paypalgrid',colStr);
	}
	
$(document).ready(function(){
	 $('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation:"bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation:"bottom",
		todayHighlight: true
    });
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshPaypalGridValues(JSON.parse(JSON.stringify(${trackingList})));
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPOPreference()'>");
		
	});
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  grid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	 if(keyCode == 13)  // the enter key code
	  {
		 getPaypalGridData(0);
	    return false;  
	  }
	});

});

	
</script>
</body>
</html>