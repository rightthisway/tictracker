<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.giftCardOrderLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(giftCardOrderGrid != null && giftCardOrderGrid != undefined){
			giftCardOrderGrid.resizeCanvas();
		}
	});
	
	$('.modal').on('show.bs.modal', function (event) {
        var idx = $('.modal:visible').length;
        $(this).css('z-index', 1040 + (10 * idx));
    });
    $('.modal').on('shown.bs.modal', function (event) {
        var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
        $('.modal-backdrop').not('.stacked').css('z-index', 1039 + (10 * idx));
        $('.modal-backdrop').not('.stacked').addClass('stacked');
    });
	
	<c:choose>
		<c:when test="${gcStatus == 'OUTSTANDING'}">	
			$('#outstandingOrder').addClass('active');
			$('#outstandingOrderTab').addClass('active');
		</c:when>
		<c:when test="${gcStatus == 'COMPLETED'}">
			$('#completedOrder').addClass('active');
			$('#completedOrderTab').addClass('active');
		</c:when>
		<c:when test="${gcStatus == 'VOIDED'}">
			$('#voidedOrder').addClass('active');
			$('#voidedOrderTab').addClass('active');
		</c:when>
		<c:when test="${gcStatus == 'ALL'}">
			$('#allOrder').addClass('active');
			$('#allOrderTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#outstandingOrder1").click(function(){
		callTabOnChange('OUTSTANDING');
	});
	$("#completedOrder1").click(function(){
		callTabOnChange('COMPLETED');
	});
	$("#voidedOrder1").click(function(){
		callTabOnChange('VOIDED');
	});
	$("#allOrder1").click(function(){
		callTabOnChange('ALL');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/GiftCardOrders?gcStatus="+selectedTab;
}

function showUploadBarcodeModal(){
	$('#uploadBarcodeModal').modal('show');
}

function uploadBarcodeFile(){
	var validFilesTypes = ["xls", "xlsx","csv"];
	var file = $('#orderFile').val();
	if(file==null || file== ''){
		jAlert("Please Select file to upload order barcode.");
		return;
	}
    var ext = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
	var isValidFile = false;
	for (var i = 0; i < validFilesTypes.length; i++) {
        if (ext == validFilesTypes[i]) {
            isValidFile = true;
            break;
        }
    }
	if(!isValidFile){
		jAlert("Please select valid Excel file.");
		return;
	}
	//$('#uploadTicketForm').submit();
	
	 var form = $('#uploadBarcodeForm')[0];
     var data = new FormData(form);
	
	 $.ajax({
		url : apiServerUrl + "UploadGiftCardOrderBarcode",
		type : "post",
		enctype:"multipart/form-data",
		 processData : false,
         contentType : false,
         cache : false,
		data : data,
		success : function(res){
			var data = JSON.parse(JSON.stringify(res));
			$('#uploadBarcodeModal').modal('hide');
			if(data.genericResponseDTO.status == 1){
				jAlert(data.genericResponseDTO.message);
			}else{
				jAlert(data.error.description);
			}
			return; 
		}, error : function(response){
			$('#uploadBarcodeModal').modal('hide');
			jAlert("Error occured while uploading barcode data.");
			return false;
		}
	}); 
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy giftCardOrder">Copy GiftCard</li>
  <li data="edit giftCardOrder">Edit GiftCard</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Gift Cards</a></li>
			<li><i class="fa fa-laptop"></i>Manage Gift Card Orders</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="outstandingOrderTab" class=""><a id="outstandingOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#outstandingOrder">Outstanding</a></li>
				<li id="completedOrderTab" class=""><a id="completedOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#completedOrder">Completed</a></li>
				<li id="voidedOrderTab" class=""><a id="voidedOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#voidedOrder">Void</a></li>
				<li id="allOrderTab" class=""><a id="allOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#allOrder">All</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="outstandingOrder" class="tab-pane">
			<c:if test="${gcStatus =='OUTSTANDING'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" type="button" data-toggle="modal" onclick="editGiftCardOrder();">Add E-GiftCard</button>
					<button class="btn btn-primary" type="button" onclick="voidGiftCardOrder('VOID')">Void GiftCard Order</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardOrderGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:giftCardOrderExportToExcel('OUTSTANDING')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardOrderResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCard_order_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCard_order_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="completedOrder" class="tab-pane">
			<c:if test="${gcStatus =='COMPLETED'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" type="button" data-toggle="modal" onclick="editGiftCardOrder();">Add E-GiftCard</button>
					<button class="btn btn-primary" type="button" onclick="voidGiftCardOrder('VOID')">Void GiftCard Order</button>
					<button class="btn btn-primary" type="button" onclick="voidGiftCardOrder('OUTSTANDING')">Mark as Outstanding</button>
					<button class="btn btn-primary" type="button" onclick="giftCardOrderExportToExcel('BARCODE')">Export Orders Without Barcode</button>
					<button class="btn btn-primary" type="button" onclick="showUploadBarcodeModal()">Upload Orders Barcodes</button>
					
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardOrderGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:giftCardOrderExportToExcel('COMPLETED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardOrderResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCard_order_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCard_order_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
			<div id="voidedOrder" class="tab-pane">
			<c:if test="${gcStatus =='VOIDED'}">	
				<div style="position: relative" id="giftCardOrderGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:giftCardOrderExportToExcel('VOIDED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardOrderResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCard_order_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCard_order_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
			<div id="allOrder" class="tab-pane">
			<c:if test="${gcStatus =='ALL'}">	
				<div style="position: relative" id="giftCardOrderGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:giftCardOrderExportToExcel('ALL')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardOrderResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCard_order_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCard_order_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
	</div>
</div>
<div id="preview-giftcard" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><span id="previewFileName" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<iframe id="viewer" frameborder="0" scrolling="no" width="100%" height="600"></iframe>
			</div>
			
		</div>

	  </div>
	</div>
	
	
	
<form class="form-horizontal" id="uploadBarcodeForm" enctype="multipart/form-data" method="post" >
	<div class="modal fade" id="uploadBarcodeModal" tabindex="-1" role="dialog" aria-labelledby="uploadBarcodeModal" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content full-width">
               <div class="modal-header full-width">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Upload Order Barcode</h4>
               </div>
                
	               <div class="modal-body full-width">
						<div class="full-width tab-fields">
							<div class="form-group col-md-4 col-sm-6 col-xs-12">
								<label>Upload file</label> 
								<input class="form-control" id="orderFile" name="orderFile" type="file" />
							</div>
						</div>
						<div class="full-width text-center">
						   <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
						   <button class="btn btn-success" type="button" onclick="uploadBarcodeFile();" >Upload</button>
					   </div>
	               </div>
	               
          	</div>
        </div>
     </div>
</form>

<!-- Add/Edit GiftCardOrder  -->
<div id="giftCardOrderModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<div class="modal-content full-width">
	  <div class="modal-header full-width">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Edit Giftcard Order - Order No : <span id="orderNo_hdr" class="headerTextClass"></span></h4>
	  </div>
	  <div class="modal-body full-width">
			<!-- <iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe> -->
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>GiftCard Order</li>
					</ol>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<section class="full-width mb-20"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
					Customer Details </header>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-3">
								<label style="font-size: 13px;font-weight:bold;">Customer Name:</label>
								<span style="font-size: 13px;" id="customerName"></span> 
							</div>
							<div class="col-sm-3">
								<label style="font-size: 13px;font-weight:bold;">User Id</label>
								<span style="font-size: 15px;" id="userId"></span>
							</div>
							<div class="col-sm-3">
								<label style="font-size: 13px;font-weight:bold;">Email:</label>
								<span style="font-size: 15px;" id="email"></span>
							</div>
							<div class="col-sm-3">
								<label style="font-size: 13px;font-weight:bold;">Phone:</label>
								<span style="font-size: 15px;" id="phone"></span>
							</div>S
						</div>
					</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<section class="panel">
					 <header style="font-size: 13px;font-family: arial;" class="panel-heading">Shipping Address &nbsp;&nbsp;&nbsp;&nbsp;
					 </header>
						<div class="panel-body">
						<div class="form-group full-width">
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">Address Line 1:</label>
								<span style="font-size: 13px;" id="addessLine1"></span>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">Address Line 2:</label>
								<span style="font-size: 15px;" id="addressLine2"></span>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">Phone:</label>
								<span style="font-size: 15px;" id="phone1"></span>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">Country:</label>
								<span style="font-size: 15px;" id="country"></span>
							</div>
						</div>
						<div class="form-group full-width">
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">State:</label>
								<span style="font-size: 15px;" id="state"></span>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">City:</label>
								<span style="font-size: 15px;" id="city"></span>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label style="font-size: 13px; font-weight: bold;">ZipCode:</label>
								<span style="font-size: 15px;" id="zipCode"></span>
							</div>
						</div>
					</div>
					</section>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="giftCardDetailDiv">
					 </header>
					<div class="panel-body">
						<div class="row">
							<div >
								<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Order Id</label>
								<span class="col-sm-2" id="giftCardOrderId"></span>
							</div>
							<div>
								<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Gift Card Title:</label>
								<span style="font-size: 12px;" class="col-sm-2" id="giftCardTitle"></span>
							</div>
							<div>
								<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Quantity:</label>
								<span class="col-sm-2" id="giftCardQuantity"></span>
							</div>
						</div>
						<div class="row">
							<div>
								<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Gift Card Description : </label>
								<span class="col-sm-2" id="giftCardDescription"></span>
							</div>
							<div>
								<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Order Type :</label>
								<span class="col-sm-2" id="giftCardOrderType"></span>
							</div>
							<div >
								<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Redeemed Rewards</label>
								<span class="col-sm-2" id="giftCardRedeemedRewards"></span>
							</div>
						</div>
					</div>
					</section>
				</div>
			</div>
			 <form id="giftCardOrderForm" method="post">
				 <input type="hidden" name="status" value="${gcStatus}" id="status" />
				 <input type="hidden" name="orderId" id="orderId" />
				<%-- <div class="form-group tab-fields">
					 <div id="imageFileDiv" class="form-group col-sm-4 col-xs-4">
						<input type="hidden" name="fileRequired" id="fileRequired" />
						<input type="hidden" name="status" value="${gcStatus}" id="status" />
						<input type="hidden" name="orderId" id="orderId" />
						<label for="giftCardFile" class="col-lg-3 control-label">E-GiftCard File</label>
						<input type="file" id="giftCardFile" name="giftCardFile">
					</div>
					<div id="changeFileDiv" class="form-group col-sm-4 col-xs-4">
						<a href="javascript:downloadFile1('DOWNLOAD')" id="giftCardFileName"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="javascript:downloadFile1('CHANGE')" id="changeFile">Change File</a>
					</div>
					<div id="giftCardRemarkDiv" class="form-group col-sm-10 col-xs-10">
						<label for="giftCardFile" class="control-label">Remarks</label>
						<input type="text" id="giftCardRemark"  name="giftCardRemark" />
					</div>
					
				</div> --%>
				<div id="uploadGiftcard" class="tab-pane" style="">
				<div class="form-group full-width">
				<div class="col-sm-4 col-xs-12">
					<div class="form-group full-width">
						<a id="egiftcard_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addGiftCardUploadRow('egiftcardTable');">Add Row</a>
						<a id='egiftcard_delete_row' class='btn btn-cancel btn-sm pull-left' style="margin-right:10px;" onclick="deleteGiftCardUploadRow('egiftcardTable');">Delete Row</a>
					</div>
					<table class="table table-bordered table-hover" id="egiftcardTable">
					<input type="hidden" name="egiftcardTable_count" id="egiftcardTable_count" >
						<thead>
							<tr >
								<th class="text-center">
									#
								</th>
								<th style="font-size: 13px;font-family: arial;" class="text-center">
									E-Giftcard
								</th>
							</tr>
						</thead>
						<tbody id="egiftcardTable_body">
						</tbody>
					</table>
					</div>
					<div class="col-sm-4 col-xs-12">
						<div class="form-group full-width">
							<a id="barcode_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addGiftCardUploadRow('barcodeTable');">Add Row</a>
							<a id='barcode_delete_row' class="btn btn-cancel btn-sm pull-left" style="margin-right:10px;" onclick="deleteGiftCardUploadRow('barcodeTable');">Delete Row</a>
						</div>
						<table class="table table-bordered table-hover" id="barcodeTable">
						<input type="hidden" name="barcodeTable_count" id="barcodeTable_count" >
						<thead>
							<tr >
								<th class="text-center">
									#
								</th>
								<th style="font-size: 13px;font-family: arial;" class="text-center">
									Barcode
								</th>
								
							</tr>
						</thead>
						<tbody id="barcodeTable_body">
						</tbody>
					</table>
					</div>
				</div>
			</div>
			</form>
	  </div>
	  <div class="modal-footer full-width">				
			<div class="full-width" align="center">
				<button type="button" onclick="saveGiftCardOrder();" class="btn btn-primary">Save</button>
				<button type="button" data-dismiss="modal" class="btn btn-cancel">Close</button>
			</div>
	  </div>
	</div>
  </div>
</div>


<!-- Add/Edit GiftCardOrder end here  -->

<script type="text/javascript">
	
	/* function downloadFile1(type){
		if(type=='DOWNLOAD'){
			var orderId = $('#orderId').val();
			var url = apiServerUrl+"DownloadEGiftCard?orderId="+orderId;
			 $('#download-frame').attr('src', url);
		}else if(type='CHANGE'){
			$('#imageFileDiv').show();
			$('#changeFileDiv').hide();
			$('#fileRequired').val('Y');
		}
		
	} */
	
	function addGiftCardUploadRow(tableName) {	
		var j = $('#'+tableName+' tr').length-1;
		j++;
		$('#'+tableName).append('<tr id="'+tableName+'_'+j+'"></tr>');
		var rowString = '';
		if(tableName=='egiftcardTable'){
			rowString = '<td>'+ (j) +'</td>';			
			rowString += '<td> <input type="file" id="egiftcard_'+j+'" name="egiftcard_'+j+'" onchange="PreviewImage(\'egiftcard_'+j+'\');" />';
			rowString += '<a href="#" onclick="javascript:previewFile(egiftcard_'+j+')" id="prvAnchor_egiftcard_'+j+'"></a></td>';			
		}else if(tableName=='barcodeTable'){
			rowString = '<td>'+ (j) +'</td>';			
			rowString += '<td> <input type="file" id="barcode_'+j+'" name="barcode_'+j+'" onchange="PreviewImage(\'barcode_'+j+'\');" />';
			rowString += '<a href="#" onclick="javascript:previewFile(barcode_'+j+')" id="prvAnchor_barcode_'+j+'"></a></td>';
		}
		$('#'+tableName+'_count').val(j);
		$('#'+tableName+'_'+j).html(rowString);
	}


	function deleteGiftCardUploadRow(tableName) {
		var count = $('#'+tableName+' tr').length-1;
		if($('#'+tableName+'_'+count).hasClass("savedClass")){
			var fileType = '';
			if(tableName=='egiftcardTable'){
				fileType = 'EGIFTCARD';
			}else if(tableName=='barcodeTable'){
				fileType = 'BARCODE';
			} 
			deleteFile(tableName,fileType,count);
		}else{
			$('#'+tableName+'_'+count).remove();
			$('#'+tableName+'_count').val(count-1);
		}
	}

	function PreviewImage(obj) {	
		$("#prvAnchor_"+obj).text('Preview');
	};

	function previewFile(obj){
		
		var filename = obj.value.split('\\').pop().split('/').pop();
		filename = filename.substring(0, filename.length);
		$('#previewFileName').text(filename);
		
		pdffile=obj.files[0];
		pdffile_url=URL.createObjectURL(pdffile);
		$('#viewer').attr('src',pdffile_url);
		
		$('#preview-giftcard').modal('show');	
	}
	


	function deleteFile(tableName,fileType,position){
		jConfirm("Are you Sure you want delete attach file?","Confirm",function(r) {
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/DeleteGiftCardOrderAttachment",
					type : "post",
					dataType : "json",
					data : "orderId="+$('#orderId').val()+"&fileType="+fileType+"&position="+position,
					success : function(res){
						var jsonData = JSON.parse(JSON.stringify(res));
						if(jsonData.status == 1){
							$('#'+tableName+'_'+position).remove();
							$('#'+tableName+'_count').val(position-1);
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
						}
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});	 
	}

	function downloadGiftCardFile(orderId,fileType,position){
		 var url = apiServerUrl+"DownloadEGiftCard?orderId="+orderId+"&fileType="+fileType+"&position="+position;
		 $('#download-frame').attr('src', url);
	}
	
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(pagingInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(pagingInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(pagingInfo.pageNum) - 1;
		}
		getGiftCardOrderGridData(pageNo);
	}
	
	//GiftCardOrder  Grid
	
	function getGiftCardOrderGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/GiftCardOrders.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+giftCardOrderSearchString+"&gcStatus=${gcStatus}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.orderPagingInfo;
				refreshGiftCardOrderGridValues(jsonData.orders);	
				//clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function giftCardOrderExportToExcel(status){
		var appendData = "headerFilter="+giftCardOrderSearchString+"&type="+status+"&gcStatus=${gcStatus}";
	    var url =apiServerUrl+"GiftCardOrdersExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function giftCardOrderResetFilters(){
		giftCardOrderSearchString='';
		giftCardOrderColumnFilters = {};
		getGiftCardOrderGridData(0);
	}
	
	var pagingInfo;
	var giftCardOrderDataView;
	var giftCardOrderGrid;
	var giftCardOrderData = [];
	var giftCardOrderGridPager;
	var giftCardOrderSearchString='';
	var giftCardOrderColumnFilters = {};
	var userGiftCardOrderColumnsStr = '<%=session.getAttribute("giftcardordergrid")%>';

	var userGiftCardOrderColumns = [];
	var allGiftCardOrderColumns = [
			 {
				id : "giftCardOrderId",
				field : "giftCardOrderId",
				name : "Giftcard Order Id",
				width : 80,
				sortable : true
			},{
				id : "cardTitle",
				field : "cardTitle",
				name : "Title",
				width : 80,
				sortable : true
			},{
				id : "cardDescription",
				field : "cardDescription",
				name : "Description",
				width : 80,
				sortable : true
			},{
				id : "quantity",
				field : "quantity",
				name : "Quantity",
				width : 80,
				sortable : true
			},{
				id : "redeemedRewards",
				field : "redeemedRewards",
				name : "Order Total",
				width : 80,
				sortable : true
			},{
				id : "orderType",
				field : "orderType",
				name : "Order Type",
				width : 80,
				sortable : true
			},{
				id : "isEmailSent",
				field : "isEmailSent",
				name : "Order Emailed? ",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "remarks",
				field : "remarks",
				name : "Remarks",
				width : 80,
				sortable : true
			},{
				id : "customerName",
				field : "customerName",
				name : "Customer Name",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "User Id",
				width : 80,
				sortable : true
			} ,{
				id : "barcode",
				field : "barcode",
				name : "Barcode",
				width : 80,
				sortable : true
			} ,{
				id : "isRedeemed",
				field : "isRedeemed",
				name : "Redeemed?",
				width : 80,
				sortable : true
			}];

	if (userGiftCardOrderColumnsStr != 'null' && userGiftCardOrderColumnsStr != '') {
		columnOrder = userGiftCardOrderColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allGiftCardOrderColumns.length; j++) {
				if (columnWidth[0] == allGiftCardOrderColumns[j].id) {
					userGiftCardOrderColumns[i] = allGiftCardOrderColumns[j];
					userGiftCardOrderColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userGiftCardOrderColumns = allGiftCardOrderColumns;
	}
	
	
	var giftCardOrderOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var giftCardOrderGridSortcol = "giftCardOrderId";
	var giftCardOrderGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function giftCardOrderGridComparer(a, b) {
		var x = a[giftCardOrderGridSortcol], y = b[giftCardOrderGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshGiftCardOrderGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		giftCardOrderData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (giftCardOrderData[i] = {});
				d["id"] = i;
				d["giftCardOrderId"] = data.id;
				d["cardTitle"] = data.cardTitle;
				d["cardDescription"] = data.cardDescription;
				d["quantity"] = data.quantity;
				d["redeemedRewards"] = data.redeemedRewards;
				d["orderType"] =  data.orderType;
				d["isEmailSent"] = 'NO';
				if(data.isEmailSent == true || data.isEmailSent == 'true'){
					d["isEmailSent"] = 'YES';
				}
				d["isRedeemed"] = 'NO';
				if(data.isRedeemed == true || data.isRedeemed == 'true'){
					d["isRedeemed"] = 'YES';
				}
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["status"] = data.status;
				d["updatedBy"] = data.updatedBy;
				d["createdBy"] = data.createdBy;
				d["remarks"] = data.remarks;
				d["customerName"] = data.firstName +' '+data.lastName;
				d["userId"] = data.userId;
				d["barcode"] = data.barcode;
			}
		}

		giftCardOrderDataView = new Slick.Data.DataView();
		giftCardOrderGrid = new Slick.Grid("#giftCard_order_grid", giftCardOrderDataView,
				userGiftCardOrderColumns, giftCardOrderOptions);
		giftCardOrderGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		giftCardOrderGrid.setSelectionModel(new Slick.RowSelectionModel());
		//giftCardOrderGrid.registerPlugin(giftCardOrderCheckboxSelector);
		
		giftCardOrderGridPager = new Slick.Controls.Pager(giftCardOrderDataView,
					giftCardOrderGrid, $("#giftCard_order_pager"),
					pagingInfo);
		var giftCardOrderGridColumnpicker = new Slick.Controls.ColumnPicker(
				allGiftCardOrderColumns, giftCardOrderGrid, giftCardOrderOptions);
		
		giftCardOrderGrid.onSort.subscribe(function(e, args) {
			giftCardOrderGridSortdir = args.sortAsc ? 1 : -1;
			giftCardOrderGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				giftCardOrderDataView.fastSort(giftCardOrderGridSortcol, args.sortAsc);
			} else {
				giftCardOrderDataView.sort(giftCardOrderGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the giftCardOrderGrid
		giftCardOrderDataView.onRowCountChanged.subscribe(function(e, args) {
			giftCardOrderGrid.updateRowCount();
			giftCardOrderGrid.render();
		});
		giftCardOrderDataView.onRowsChanged.subscribe(function(e, args) {
			giftCardOrderGrid.invalidateRows(args.rows);
			giftCardOrderGrid.render();
		});
		$(giftCardOrderGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							giftCardOrderSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								giftCardOrderColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in giftCardOrderColumnFilters) {
										if (columnId !== undefined
												&& giftCardOrderColumnFilters[columnId] !== "") {
											giftCardOrderSearchString += columnId
													+ ":"
													+ giftCardOrderColumnFilters[columnId]
													+ ",";
										}
									}
									getGiftCardOrderGridData(0);
								}
							}

						});
		giftCardOrderGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
				$("<input type='text' placeholder='mm/dd/yyyy'>")
			   .data("columnId", args.column.id)
			   .val(giftCardOrderColumnFilters[args.column.id])
			   .appendTo(args.node);
			}
			else{
				$("<input type='text'>").data("columnId", args.column.id)
						.val(giftCardOrderColumnFilters[args.column.id]).appendTo(
								args.node);
			}
		});
		giftCardOrderGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		giftCardOrderDataView.beginUpdate();
		giftCardOrderDataView.setItems(giftCardOrderData);
		//giftCardOrderDataView.setFilter(filter);
		giftCardOrderDataView.endUpdate();
		giftCardOrderDataView.syncGridSelection(giftCardOrderGrid, true);
		giftCardOrderGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserGiftCardOrderPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = giftCardOrderGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('giftcardordergrid', colStr);
	}
	
	

	function saveGiftCardOrder(action){
		var imageFile = $('#giftCardFile').val();
		if(imageFile!=null && imageFile!=''){
			$('#fileRequired').val('Y');
		}
		var fileRequired = $('#fileRequired').val();
		if(fileRequired == 'Y' && imageFile == ''){
			jAlert("GiftCard file is mendatory.");
			return;
		} 	
		var requestUrl = "${pageContext.request.contextPath}/SaveGiftCardOrder";
		var form = $('#giftCardOrderForm')[0];
		var dataString = new FormData(form);
		
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#giftCardOrderModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.orderPagingInfo);
					giftCardOrderColumnFilters = {};
					refreshGiftCardOrderGridValues(JSON.parse(jsonData.orders));
					//clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	
	function voidGiftCardOrder(type){
		var tempGiftCardOrderRowIndex = giftCardOrderGrid.getSelectedRows([0])[0];
		if (tempGiftCardOrderRowIndex == null) {
			jAlert("Plese select GiftCardOrder to Void", "info");
			return false;
		}else {
			var giftCardOrderId = giftCardOrderGrid.getDataItem(tempGiftCardOrderRowIndex).giftCardOrderId;
			var confirmMsg = '';
			if(type=='VOID'){
				confirmMsg = 'Are you sure you want to void selected giftCard order?, all uploaded file will be removed?';
			}else if(type=='OUTSTANDING'){
				confirmMsg = 'Are you sure you want to mark order as outstanding?, all uploaded file will be removed?';
			}
			jConfirm(confirmMsg,"Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/VoidGiftCardOrders",
						type : "post",
						dataType: "json",
						data: "orderId="+giftCardOrderId+"&type="+type+"&status=${gcStatus}",
						success : function(response){				
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){					
								pagingInfo = JSON.parse(jsonData.orderPagingInfo);
								giftCardOrderColumnFilters = {};
								refreshGiftCardOrderGridValues(JSON.parse(jsonData.orders));
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
			
			
			
			
		}
	}
	
	//Edit GiftCardOrder 
	function editGiftCardOrder(){
		var tempGiftCardOrderRowIndex = giftCardOrderGrid.getSelectedRows([0])[0];
		if (tempGiftCardOrderRowIndex == null) {
			jAlert("Plese select GiftCardOrder to Edit", "info");
			return false;
		}else {
			var giftCardOrderId = giftCardOrderGrid.getDataItem(tempGiftCardOrderRowIndex).giftCardOrderId;
			getEditGiftCardOrder(giftCardOrderId);
		}
	}
	
	function getEditGiftCardOrder(giftCardOrderId){
		$.ajax({
			url : "${pageContext.request.contextPath}/EditGiftCardOrder",
			type : "post",
			dataType: "json",
			data: "orderId="+giftCardOrderId,
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#giftCardOrderModal').modal('show');
					setEditGiftCardOrder(jsonData);
					setGiftCardAttachments(jsonData);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditGiftCardOrder(jData){
		giftCardOrder = jData.orders;
		if(giftCardOrder != null && giftCardOrder != ''){
			var data = giftCardOrder;
			$('#orderNo_hdr').text(data.id);
			$('#orderId').val(data.id);
			$('#customerName').text(jData.customer.customerName+' '+jData.customer.lastName);
			$('#email').text(jData.customer.email);
			$('#phone').text(jData.customer.phone);
			$('#userId').text(jData.customer.userId);
			
			$('#addessLine1').text(jData.shipping.addressLine1);
			$('#addressLine2').text(jData.shipping.addressLine2);
			$('#phone1').text(jData.shipping.phone1);
			$('#country').text(jData.shipping.countryName);
			$('#state').text(jData.shipping.stateName);
			$('#city').text(jData.shipping.city);
			$('#zipCode').text(jData.shipping.zipCode);
			
			
			$('#giftCardOrderId').text(data.id);
			$('#giftCardTitle').text(data.cardTitle);
			$('#giftCardQuantity').text(data.quantity);
			$('#giftCardDescription').text(data.cardDescription);
			$('#giftCardOrderType').text(data.orderType);
			$('#giftCardRedeemedRewards').text(data.redeemedRewards);
			if(data.fileName!=null && data.fileName!=''){
				$('#fileRequired').val('N');
				$('#imageFileDiv').hide();
				$('#changeFileDiv').show();
			}else{
				$('#fileRequired').val('Y');
				$('#imageFileDiv').show();
				$('#changeFileDiv').hide();
				
			}
			$('#giftCardFileName').text(data.fileName);
			
			$('#giftCardRemark').val(data.remarks);
			//$("#cardImageTag").attr("src", apiServerUrl+"GetImageFile?type=giftCardOrderImage&filePath="+data.fileName);
		}
	}
	
	function setGiftCardAttachments(jsonData){
		var egiftcard='';
		var barcode=''; 
		if(jsonData.eGiftAttachments != null && jsonData.eGiftAttachments != ''){
			egiftcard = jsonData.eGiftAttachments;
		}
		if(jsonData.barcodeAttachments != null && jsonData.barcodeAttachments != ''){
			barcode = jsonData.barcodeAttachments;
		}
		var eGiftCardRows = "";
		var barcodeRows = "";
		$('#egiftcardTable_body').empty();
		$('#barcodeTable_body').empty();
		$('#egiftcardTable_count').val(jsonData.egiftcardCounts);
		$('#barcodeTable_count').val(jsonData.barcodeCounts);
		
		if(egiftcard != ''){
			var j=1;
			for(var i=0; i<egiftcard.length; i++){
				var data = egiftcard[i];
				eGiftCardRows += "<tr id='egiftcardTable_"+data.position+"' class='savedClass'>";
				eGiftCardRows += "<td>"+j+"</td>";
				eGiftCardRows += "<td>";
				eGiftCardRows += "<input type='file' id='egiftcard_"+data.position+"' name='egiftcard_"+data.position+"' /><br/>";
				eGiftCardRows += "<a href=javascript:downloadGiftCardFile("+data.orderId+",'"+data.fileType+"',"+data.position+")>"+data.fileName+"</a>";					
				eGiftCardRows += "</td></tr>";
				j++;
			}
		}
		if(barcode != ''){
			var j=1;
			for(var i=0; i<barcode.length; i++){
				var data = barcode[i];
				barcodeRows += "<tr id='barcodeTable_"+data.position+"' class='savedClass'>";
				barcodeRows += "<td>"+j+"</td>";
				barcodeRows += "<td>";
				barcodeRows += "<input type='file' id='barcode_"+data.position+"' name='barcode_"+data.position+"' /><br/>";
				barcodeRows += "<a href=javascript:downloadGiftCardFile("+data.orderId+",'"+data.fileType+"',"+data.position+")>"+data.fileName+"</a>";					
				barcodeRows += "</td></tr>";
				j++;
			}
		}
		$('#egiftcardTable_body').append(eGiftCardRows);
		$('#barcodeTable_body').append(barcodeRows);
	}
	
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${orderPagingInfo};
		refreshGiftCardOrderGridValues(${orders});
		$('#giftCard_order_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGiftCardOrderPreference()'>");
		
		enableMenu();
	};
		
</script>