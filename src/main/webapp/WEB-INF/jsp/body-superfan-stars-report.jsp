<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script src="../resources/amcharts/core.js" type="text/javascript"></script>
<script src="../resources/amcharts/maps.js" type="text/javascript"></script>
<script src="../resources/amcharts/charts.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/moonrisekingdom.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/material.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/animated.js" type="text/javascript"></script>

<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-laptop"></i>SuperFan Stars Data</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
				
</div>


<div>
	<input type="button"class="btn btn-primary"  value="Download PDF" onclick="savePDF();" />
	<div id="chartdiv" style="height: 400px;" class="col-md-12"></div>
	<div id="chart2div" style="height: 400px;"  class="col-md-12"></div>
</div>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});
	
});

function callAPIReport(reportURL){
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function openContestReportModal(reportName){
	$('#contestReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
}

function openCustChainReportModal(){
	$('#custChainReportModal').modal('show'); 
}

function generateReport(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var reportURL = $('#contestReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function generateChainReport(){
	var userId = $('#userId').val(); 
	if(null == userId || userId == "" || userId == " "){
		$('#userIdErrId').html("Please Enter Valid User ID."); 
		return;
	}else{
		$('#userIdErrId').html(""); 
		window.location.href = "${pageContext.request.contextPath}/Reports/DownloadCustomerStatsReport?userId="+userId;
	}
}

function generateBotReport(){ 
	window.location.href = "${pageContext.request.contextPath}/Reports/DownloadBotReport";
}




var chart = am4core.create("chartdiv", am4charts.XYChart);
var chart2 = am4core.create("chart2div", am4charts.XYChart);				
function getReportChart(report){
	//var chart = am4core.create("chartdiv", am4charts.XYChart);
				// Add data
				chart.data =  JSON.parse(report);
				
				
				chart.colors.list = [
				  am4core.color("#FF9671"),
				  am4core.color("#FFC75F")
				];

				// Create axes
				var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());	
				categoryAxis.dataFields.category = "viewName";
				categoryAxis.numberFormatter.numberFormat = "#";
				categoryAxis.renderer.inversed = true;
				categoryAxis.renderer.grid.template.location = 0;
				categoryAxis.renderer.cellStartLocation = 0.1;
				categoryAxis.renderer.cellEndLocation = 0.9;
				categoryAxis.title.text = "SuperFan Stars Range";
				categoryAxis.title.fontWeight = "bold";

				var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
				valueAxis.renderer.opposite = true;
				valueAxis.title.text = "Customers Count & SuperFan Stars Availability";
				valueAxis.title.fontWeight = "bold";

				// Create series
				function createSeries(field, name) {
				  var series = chart.series.push(new am4charts.ColumnSeries());
				  series.dataFields.valueX = field;
				  series.dataFields.categoryY = "viewName";
				  series.name = name;
				  series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
				  series.columns.template.height = am4core.percent(100);
				  series.sequencedInterpolation = true;

				
				  var valueLabel = series.bullets.push(new am4charts.LabelBullet());
				  valueLabel.label.text = "{valueX}";
				  valueLabel.label.horizontalCenter = "left";
				  valueLabel.label.dx = 10;
				  valueLabel.label.hideOversized = false;
				  valueLabel.label.truncate = false;

				  var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
				  categoryLabel.label.text = "{name}";
				  categoryLabel.label.horizontalCenter = "right";
				  categoryLabel.label.dx = -10;
				  categoryLabel.label.fill = am4core.color("#fff");
				  categoryLabel.label.hideOversized = false;
				  categoryLabel.label.truncate = false;
				}

				createSeries("viewCount", "Customer Count");
				createSeries("viewCount1", "Super Fan Stars");

				// Enable export
				chart.exporting.menu = new am4core.ExportMenu();
				
//var chart2 = am4core.create("chart2div", am4charts.XYChart);

				chart2.colors.list = [
				  am4core.color("#FF9671"),
				  am4core.color("#FFC75F")
				];
				var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
					categoryAxis.dataFields.category = "viewName";
					categoryAxis.renderer.grid.template.location = 0;
					categoryAxis.renderer.minGridDistance = 30;
					categoryAxis.title.text = "SuperFan Stars Range";
					categoryAxis.title.fontWeight = "bold";
				
					var label = categoryAxis.renderer.labels.template;
					label.wrap = true;
					label.maxWidth = 120;
					
					var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
					valueAxis.title.text = "Customers Count";
					valueAxis.title.fontWeight = "bold";
				
					// Create series
					var series = chart2.series.push(new am4charts.ColumnSeries());
					series.dataFields.valueY = "viewCount";
					series.dataFields.categoryX = "viewName";
					series.name = "viewCount";
					series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
					series.columns.template.fillOpacity = .8;
					
					
					var valueLabel = series.bullets.push(new am4charts.LabelBullet());
				    valueLabel.label.text = "{valueY}";
				    valueLabel.label.horizontalCenter = "left";
				    valueLabel.label.dx = 10;
				    valueLabel.label.hideOversized = false;
				    valueLabel.label.truncate = false;

					var columnTemplate = series.columns.template;
					columnTemplate.strokeWidth = 2;
					columnTemplate.strokeOpacity = 1;

				// Add data
				chart2.data =  JSON.parse(report);
				
				// Enable export
				chart2.exporting.menu = new am4core.ExportMenu();
		}





function savePDF() {
  
  Promise.all([
    chart.exporting.pdfmake,
    chart.exporting.getImage("png"),
    chart2.exporting.getImage("png")
  ]).then(function(res) { 
    
    var pdfMake = res[0];
    
    // pdfmake is ready
    // Create document template
    var doc = {
      pageSize: "A4",
      pageOrientation: "portrait",
      pageMargins: [30, 30, 30, 30],
      content: []
    };
    
   

    doc.content.push({
      text: "SuperFan Stars Availability",
      fontSize: 20,
      bold: true,
      margin: [0, 20, 0, 15]
    });
	
    doc.content.push({
      image: res[1],
      width: 530,
	  height:350
    });
   
     doc.content.push({
      image: res[2],
      width: 530,
	  height:350
    });
    
    pdfMake.createPdf(doc).download("report.pdf");
    
  });
  
}

//call functions once page loaded
	window.onload = function() {
		getReportChart('${report}');
		$('#pollingContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingContestPreference()'>");
		
		enableMenu();
	};		

</script>

<style>
	input{
		color : black !important;
	}
</style>




