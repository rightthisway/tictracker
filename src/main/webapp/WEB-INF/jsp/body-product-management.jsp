<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}
.slick-editableCellCss{
	background: #F8E0F7;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenu1 {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu1 li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu1 li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var questionArray = [];
var j = 0;
var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#pexpdate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$("#pendingProd1").click(function(){
		callTabOnChange('PENDING');
	})
	$("#activeProd1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredProd1").click(function(){
		callTabOnChange('EXPIRED');
	});
	
	$('#isRwdPointCheck').change(function () {
		if($('#isRwdPointCheck').is(':checked')){
			$('#regPriceDiv').hide();
			$('#sellPriceDiv').hide();
			$('#rwdPointDiv').show();
			$('#isRwdPointProd').val('true');
			$('#pregMinPrc').val(0);
			$('#pselMinPrc').val(0);
		}else{
			$('#isRwdPointProd').val('false');
			$('#rwdPointDiv').hide();
			$('#regPriceDiv').show();
			$('#sellPriceDiv').show();
			$('#reqRwdPoint').val(0);
		}
		
	});
	
		
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(prodGrid != null && prodGrid != undefined){
			prodGrid.resizeCanvas();
		}
		if(invGrid!=null && invGrid != undefined){
			 invGrid.resizeCanvas();
		}
	});
	
	
	<c:choose>
		<c:when test="${status == 'PENDING'}">	
			$('#pendingProd').addClass('active');
			$('#pendingProdTab').addClass('active');
		</c:when>
		<c:when test="${status == 'ACTIVE'}">	
			$('#activeProd').addClass('active');
			$('#activeProdTab').addClass('active');
		</c:when>
		<c:when test="${status == 'EXPIRED'}">
			$('#expiredProd').addClass('active');
			$('#expiredProdTab').addClass('active');
		</c:when>
	</c:choose>
});



function resetModal(){
	$('#prodModal').modal('show');
	$('#prodImageDiv').show();
	$('#prodImageDispDiv').hide();
	$('#prodId').val('');
	$('#pname').val('');
	$('#pbrand').val('');
	$('#pdesc').val('');
	$('#plongdesc').val('');	
	$('#pregMinPrc').val('');	
	$('#pselMinPrc').val('');
	$('#isRwdPointProd').val('');
	$('#reqRwdPoint').val('');
	$('#maxcustqty').val('');
	$('#dispOrder').val('');
	$('#isVariant').val('true');
	$('#dispRegPrice').val('true');
	$('#dispPerOff').val('true');
	$('#pexpdate').val('');
	$('#isimgupd').val('TRUE');
	
	$('#isRwdPointCheck').prop('checked', false);
	$('#rwdPointDiv').hide();
	$('#saveBtn').show();
	$('#updateBtn').hide();
}

function prodSave(action){
	var pname = $('#pname').val();
	var pbrand = $('#pbrand').val();
	var pdesc = $('#pdesc').val();	
	var plongdesc = $('#plongdesc').val();
	var pregMinPrc = $('#pregMinPrc').val();
	var pselMinPrc = $('#pselMinPrc').val();
	var maxcustqty = $('#maxcustqty').val();
	var dispOrder = $('#dispOrder').val();
	var pexpdate = $('#pexpdate').val();
	var prodSeller = $('#prodSeller').val();
	var reqRwdPoint = $('#reqRwdPoint').val();
	
	if(prodSeller <= 0){
		jAlert("Please select seller to add/update product.");
		return;
	}
	if(pname == ''){
		jAlert("Product name is mendatory");
		return;
	}
	if(pbrand == ''){
		jAlert("Product brand is mendatory");
		return;
	}
	if(pdesc == ''){
		jAlert("Product short description is mendatory");
		return;
	}
	if(plongdesc == ''){
		jAlert("Product long description is mendatory");
		return;
	}
	
	if($('#isRwdPointCheck').is(':checked')){
		if(reqRwdPoint == '' || !isValidDecimal(reqRwdPoint)){
			jAlert("Product required reward points is mendatory and should be valid numeric value");
			return;
		}
	}else{
		if(pregMinPrc == '' || !isValidDecimal(pregMinPrc)){
			jAlert("Product regular price is mendatory and should be valid numeric value");
			return;
		}
		if(pselMinPrc == ''  || !isValidDecimal(pselMinPrc)){
			jAlert("Product sale price is mendatory and should be valid numeric value");
			return;
		}
	}
	
	if(maxcustqty == '' || !isValidNumber(maxcustqty)){
		jAlert("Product max order qty is mendatory and should be valid number value");
		return;
	}
	if(pexpdate == ''){
		jAlert("Product expiry date is mendatory");
		return;
	}
	if(dispOrder == ''){
		jAlert("Product display order is mendatory");
		return;
	}
	
	var requestUrl = "${pageContext.request.contextPath}/ecomm/UpdateProduct";
	if(action == 'SAVE'){		
		$('#action').val('SAVE');
		//$('#status').val('${status}');
		//dataString  = $('#prodForm').serialize()+"&status=${status}&action=SAVE";
	}else if(action == 'UPDATE'){
		$('#action').val('UPDATE');
		//$('#status').val('${status}');
		//dataString = $('#prodForm').serialize()+"&status=${status}&action=UPDATE";
	}
	var form = $('#prodForm')[0];
	var dataString = new FormData(form);	
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		enctype:"multipart/form-data",
		processData : false,
		contentType : false,
		cache : false,
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#prodModal').modal('hide');
				pagingInfo = jsonData.pagingInfo;
				columnFilters = {};
				refreshProdGridValues(jsonData.prodList);
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


var OPTION = 0;
var OPTVAL = 0;
var optCnt = 0
function addRow(value) {
	optCnt++;
    var prodVarTable = document.getElementById('prodVarTable');
    var rowCnt = prodVarTable.rows.length;    // get the number of rows.
    var tr = prodVarTable.insertRow(rowCnt); // table row.
    tr = prodVarTable.insertRow(rowCnt);
	var addRow = document.getElementById('addRow');
	addRow.setAttribute('disabled', true);
	OPTION = OPTION + 100;
	OPTVAL = OPTVAL + 100;
	OPTVAL = (OPTVAL - (OPTVAL%100));
	
	if(value == null || value == 'null' || value == undefined){
		value = '';
	}
	
    for (var c = 0; c < 5; c++) {
        var td = document.createElement('td');          // TABLE DEFINITION.
        td = tr.insertCell(c);
		if(c == 0){
			var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('value', value);
			ele.setAttribute('id', 'opt_'+OPTION);
			td.appendChild(ele);
		}else if(c==1){
			var x = document.createElement("IMG");
			  x.setAttribute("src", "../resources/images/icons/plus.png");
			  x.setAttribute("width", "20");
			  x.setAttribute("height", "20");
			  x.setAttribute("alt", "Add");
			  x.setAttribute('onclick', 'addOptionValue('+(rowCnt+1)+')');
			td.appendChild(x);
		}else if(c==2){
			var ele = document.createElement('input');
            ele.setAttribute('type', 'hidden');
            ele.setAttribute('value', '');
			ele.setAttribute('disabled', true);
			td.appendChild(ele);
		}else if (c == 3) {   // if its the first column of the table.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'hidden');
            ele.setAttribute('value', '');
			ele.setAttribute('disabled', true);
			td.appendChild(ele);
        }else if (c == 4) {   // if its the first column of the table.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'hidden');
            ele.setAttribute('value', '');
			ele.setAttribute('disabled', true);
			td.appendChild(ele);
        }
		
    }
}



 // function to add new row.
function addOptionValue(rowCnt,value) {
    var prodVarTable = document.getElementById('prodVarTable');

    //var rowCnt = prodVarTable.rows.length;    // get the number of rows.
    var tr = prodVarTable.insertRow(rowCnt); // table row.
    tr = prodVarTable.insertRow(rowCnt);
	var addRow = document.getElementById('addRow');
	if(optCnt < 4){
		addRow.removeAttribute('disabled');
	}
	
	if(value == null || value == undefined){
		value = '';
	}

    for (var c = 0; c < 5; c++) {
        var td = document.createElement('td');          // TABLE DEFINITION.
        td = tr.insertCell(c);
		if(c == 0){
			var ele = document.createElement('input');
            ele.setAttribute('type', 'hidden');
            ele.setAttribute('value', '');
			ele.setAttribute('disabled', true);
			td.appendChild(ele);
		}else if(c==1){
			var ele = document.createElement('input');
            ele.setAttribute('type', 'hidden');
            ele.setAttribute('value', '');
			ele.setAttribute('disabled', true);
			td.appendChild(ele);
		}else if(c==2){
			var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('value', value);
			ele.setAttribute('id', 'val_'+OPTVAL);
			ele.setAttribute('data', 'opt_'+OPTION);
			td.appendChild(ele);
		}else if (c == 3) {   // if its the first column of the table.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'file');
            ele.setAttribute('value', '');
			ele.setAttribute('id', 'valFile_'+OPTVAL);
			ele.setAttribute('data', 'opt_'+OPTION);
			td.appendChild(ele);
        }else if (c == 4) {   // if its the first column of the table.
            var x = document.createElement("IMG");
			  x.setAttribute("src", "../resources/images/icons/minus.png");
			  x.setAttribute("width", "20");
			  x.setAttribute("height", "20");
			  x.setAttribute("alt", "Remove");
            x.setAttribute('onclick', 'removeRow(this)');
            td.appendChild(x);
        }
	}
	OPTVAL = OPTVAL + 1;
}



// function to delete a row.
function removeRow(oButton) {
    var empTab = document.getElementById('prodVarTable');
    empTab.deleteRow(oButton.parentNode.parentNode.rowIndex); // buttton -> td -> tr
}

// function to extract and submit table data.
var cnt = 1;
function saveProdVariants() {
    var myTab = document.getElementById('prodVarTable');
    var arrValues = new Array();
	var optValues = '';
	var lstOpt = '';
	var formData = new FormData();
	console.log(myTab.rows.length);
    // loop through each row of the table.
    for (row = 1; row < myTab.rows.length - 1; row++) {
        // loop through each cell in a row.
        for (c = 0; c < myTab.rows[row].cells.length; c++) {
            var element = myTab.rows.item(row).cells[c];
			if(c==0){
				if (element.childNodes[0].getAttribute('type') == 'text'
					&& element.childNodes[0].value !='' && element.childNodes[0].value != undefined) {
					if(optValues != ''){
						arrValues.push(optValues);
						formData.append('optVal'+cnt,optValues);
						optValues = '';
						cnt++;
					}
					optValues = optValues +  element.childNodes[0].value + ':';
					lstOpt = element.childNodes[0].value;
					
				}
			}else{
				if (element.childNodes[0].getAttribute('type') == 'text'
					&& element.childNodes[0].value !='' && element.childNodes[0].value != undefined) {
					lastVal = element.childNodes[0].value;
					optValues = optValues +  element.childNodes[0].value + ',';
				}else if(element.childNodes[0].getAttribute('type') == 'file'
					&& element.childNodes[0].value !='' && element.childNodes[0].value != undefined){
					var file = element.childNodes[0].files[0];
					formData.append('file_'+lstOpt+'_'+lastVal,file,'file_'+lstOpt+'_'+lastVal+'.'+getFileExtension(file));
							lastVal = '';			

				}
			}
        }
    }
	
	if(optValues != ''){
		formData.append('optVal'+cnt,optValues)
		arrValues.push(optValues);
		optValues = '';
	}
    
	
	
	var tempProdRowIndex = prodGrid.getSelectedRows([0])[0];
	if (tempProdRowIndex == null) {
		jAlert("Plese select product to manage variants", "info");
		return false;
	}else {
		var prodId = prodGrid.getDataItem(tempProdRowIndex).prodId;
		var sellerId = prodGrid.getDataItem(tempProdRowIndex).sellerId;
		formData.append('slrpitmsId',prodId)
		formData.append('slerId',sellerId)
	}
    // finally, show the result in the console.
    //console.log(arrValues);
	 for(var pair of formData.entries()) {
	   console.log(pair[0]+ ', '+ pair[1]); 
	} 
	
	$.ajax({
		url : "${pageContext.request.contextPath}/ecomm/addproditmvaroptns.json",
		type : "post",
		dataType: "json",
		enctype:"multipart/form-data",
		processData : false,
		contentType : false,
		cache : false,
		data : formData,
		
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#prodVarModal').modal('hide');
			}
			jAlert(jsonData.msg);
			return;			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function getFileExtension(file)
{
  var ext = /^.+\.([^.]+)$/.exec(file.name);
  return ext == null ? "" : ext[1];
}




function loadProductOptionValue(jsonData){
	var hasInventory = jsonData.hasInventory;
	var data = JSON.parse(jsonData.variantsList);
	var cnt = 1;
	if(data.length <= 0){
		$('#saveVarBtn').show();
		$('#updateVarBtn').hide();
		return;
	} else {
		/* if(hasInventory) {
			$('#saveVarBtn').hide();
			$('#updateVarBtn').hide();
		} else { */
			$('#saveVarBtn').hide();
			$('#updateVarBtn').show();
		//}
	}
	for(var i=0;i<data.length;i++){
		var op =  data[i];
		cnt++;
		addRow(op.vOptName);
		for(var j=0;j<op.values.length;j++){
			var val =  op.values[j];
			addOptionValue(cnt,val.varVal);
			cnt++;
		}
		
	}
}

function openProdVariantModel(){
	var prodId = $('#selectedProdId').val();
	var sellerId = $('#sellerId').val();
	if (prodId == null || prodId == undefined || prodId == '') {
		jAlert("Plese select product to manage variants", "info");
		return false;
	}
	var requestUrl = "${pageContext.request.contextPath}/ecomm/getproditmvaroptns";
	 $.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: "slrpitmsId="+prodId+"&slerId="+sellerId,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#prodVarModal').modal('show');
				$("#prodVarTable").find("tr:not(:first)").remove();
				loadProductOptionValue(jsonData);
				$('#prodVarModal').modal('show');
			} else {
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	}); 
	
}

function generateInventory(){
	var prodId = $('#selectedProdId').val();
	var sellerId = $('#sellerId').val();
	if (prodId == null || prodId == undefined) {
		jAlert("Plese select product to manage variants", "info");
		return false;
	}
	jConfirm("Are you sure to Generate Inventory. ? Once Inventory Generated Then You can't Edit Variant options.","Confirm",function(r){
		if (r) {
				var requestUrl = "${pageContext.request.contextPath}/ecomm/genProdInventory";
				 $.ajax({
					url : requestUrl,
					type : "post",
					dataType: "json",
					data: "slrpitmsId="+prodId+"&slerId="+sellerId,
					success : function(response){
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.sts == 1){
							//$('#prodVarModal').modal('show');
							//$('#varTableBody').empty();
							//loadProductOptionValue(JSON.parse(jsonData.variantsList));
							getProductInventories(true);
						}
						jAlert(jsonData.msg);
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				}); 
		} else {
			return false;
		}
	});
}

function isValidNumber(value) {
	var intRegex = /^\d+$/;
	if(intRegex.test(value)) {
		return true;
	}
	return false;
}
function isValidDecimal(value) {
	var decimalRegex = /^\d+(\.\d{0,2})?$/;
	if(decimalRegex.test(value)) {
		return true;
	}
	return false;
}


var invUpdFormData = new FormData();
function updateInvDataToForm(invId,editedColumn,value,item) {
	
	if (editedColumn == 'avlQty') {
		if(!isValidNumber(item.avlQty)) {
			jAlert('Enter Valid Available Quantity.');
			return;
		}
	} else if (editedColumn == 'regPrice') {
		if(!isValidDecimal(item.regPrice)) {
			jAlert('Enter Valid Regular Price.');
			return;
		}
	} else if (editedColumn == 'sellPrice') {
		if(!isValidDecimal(item.sellPrice)) {
			jAlert('Enter Valid Sell Price.');
			return;
		}
	}else if (editedColumn == 'reqLPnts') {
		if(!isValidNumber(item.reqLPnts)) {
			jAlert('Enter Valid Required Loyalty Points.');
			return;
		}
	}else if (editedColumn == 'priceAftLPnts') {
		if(!isValidDecimal(item.priceAftLPnts)) {
			jAlert('Enter Valid Price After Loyalty Points.');
			return;
		}
	}
	var key = 'inv_'+invId;
	//invUpdFormData.delete(key);
	var tempVal = item.avlQty+'_'+item.regPrice+'_'+item.sellPrice+'_'+item.reqLPnts+'_'+item.priceAftLPnts;
	invUpdFormData.set(key,tempVal);
	
	for(let [name, value] of invUpdFormData) {
		console.log(name+'_'+value); 
	}
	/* var invUpdVal = $('#inv_'+invId).val();
	if(invUpdVal != null && invUpdVal != '') {
		 $('#inv_'+invId).val(tempVal);
	} else {
		var ele = document.createElement('input');
	    ele.setAttribute('type', 'hidden');
	    ele.setAttribute('value', tempVal);
		ele.setAttribute('id', 'inv_'+invId);
		ele.setAttribute('name', 'inv_'+invId);
		//ele.appendChild(ele);
	} */
	
}

function updateInventoryValues(){
	
	var hasValFlag = false;
	for(let [name, value] of invUpdFormData) {
		hasValFlag = true;  
		var array = value.split("_");  
		if(array.length != 5) {
			console.log(name+'_'+value);
			jAlert("please provide valid inventory values to update.", "info");
			return false;
		}
	}
	if(!hasValFlag) {
		jAlert("Plese change atleast one Inventory values to Update.", "info");
		return false;
	}
	
	var prodId = $('#selectedProdId').val();
	var sellerId = $('#sellerId').val();
	if (prodId == null || prodId == undefined) {
		jAlert("Plese select product to manage variants", "info");
		return false;
	}
	invUpdFormData.append('slrpitmsId',prodId);
	invUpdFormData.append('slerId',sellerId);
	
	var requestUrl = "${pageContext.request.contextPath}/ecomm/updateProdInventories";
	 $.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		enctype:"multipart/form-data",
		processData : false,
		contentType : false,
		cache : false,
		data: invUpdFormData,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				//invUpdFormData = new FormData();
				
				//$('#prodVarModal').modal('show');
				//$("#prodVarTable").find("tr:not(:first)").remove();
				//loadProductOptionValue(JSON.parse(jsonData.variantsList));
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	}); 
	
}

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ecomm/ManageProducts?status="+selectedTab;
}


</script>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Seller</a></li>
			<li><i class="fa fa-laptop"></i>Manage Products</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="pendingProdTab" class=""><a id="pendingProd1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#pendingProd">Pending Products</a></li>
				<li id="activeProdTab" class=""><a id="activeProd1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeProd">Active Products</a></li>
				<li id="expiredProdTab" class=""><a id="expiredProd1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredProd">Expired Products</a></li>
			</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="pendingProd" class="tab-pane">
			<c:if test="${status =='PENDING'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary"  onclick="resetModal();">Add Product</button>
					<button type="button" class="btn btn-primary" onclick="editProd();">Edit Product</button>
					<button type="button" class="btn btn-primary" onclick="changeStatus('ACTIVE');">Activate Product</button>
					<button type="button" class="btn btn-primary" onclick="openProdVariantModel();">Manage Product Variants</button>
					<button type="button" class="btn btn-primary" onclick="changeStatus('EXPIRED');">Remove Product</button>
					<button type="button" class="btn btn-primary" onclick="generateInventory();">Generate Inventory</button>
					<input type="hidden" id="selectedProdId">
					<input type="hidden" id="sellerId">
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Pending Products</label>
							<div class="pull-right">
								<a href="javascript:exportSaaSGameProductsToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="prods_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="prods_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
				</c:if>
			</div>
			<div id="activeProd" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary" onclick="changeStatus('EXPIRED');">Remove Product</button>
					<input type="hidden" id="selectedProdId">
					<input type="hidden" id="sellerId">
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Active Products</label>
							<div class="pull-right">
								<a href="javascript:exportSaaSGameProductsToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="prods_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="prods_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
				</c:if>
			</div>
			<div id="expiredProd" class="tab-pane">
			<c:if test="${status =='EXPIRED'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary" onclick="changeStatus('ACTIVE');">Move to Active</button>
					<input type="hidden" id="selectedProdId">
					<input type="hidden" id="sellerId">
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Expired Products</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="prods_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="prods_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
			</c:if>
			</div>																				
		</div>
	</div>	
</div>


<div class="full-width full-width-btn mb-20">
	<button type="button" class="btn btn-primary"  onclick="updateInventoryValues();">Update Inventory</button>
	</div>
<div style="position: relative;" id="inventoryGrid">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label id="invGridHeaderLabel">Product Inventories</label>
			<div class="pull-right">
				<a href="javascript:invExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:invResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="inv_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="inv_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<form name="invUpdForm" id="invUpdForm" method="post">
</form>



<!-- Add Product -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="prodModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Product</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<form name="prodForm" id="prodForm" method="post">
					<input type="hidden" id="prodId" name="slrpitmsId" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="status" name="status" value="${status}" />
					<input type="hidden" id="isimgupd" name="isimgupd" value="FALSE"/>
					
					<input type="hidden" id="status" name="${status}" />
						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Seller<span class="required">*</span>
							</label>
							<select id="prodSeller" name="slerId" class="form-control input-sm m-bot15">
						  		<option value="-1">-- select --</option>
							  	<c:forEach items="${sellerList}" var="seller">
									<option value="${seller.sellerId}"> ${seller.compName} -- ${seller.email}
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Product Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="pname" name="pname" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Brand Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="pbrand" name="pbrand" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Product Type<span class="required">*</span></label>
							<select id="prodType" name="prodType" class="form-control input-sm m-bot15">
						  		<option value="PRODUCT">PRODUCT</option>
						  		<option value="GIFTCARD">GIFTCARD</option>
							</select>
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Product Short Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="pdesc" name="pdesc" >
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Product Long Description<span class="required">*</span>
							</label> <textarea row="100" col="100" class="form-control" id="plongdesc" name="plongdesc" ></textarea> >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Max. Order Quantity limit
							</label> <input class="form-control" type="text" id="maxcustqty" name="maxcustqty" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Product Display Order
							</label> <input class="form-control" type="text" id="dispOrder" name="dispOrder" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Product With Multiple Variants<span class="required">*</span></label>
							<select id="isVariant" name="isVariant" class="form-control input-sm m-bot15">
						  		<option value="true">YES</option>
						  		<option value="false">NO</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Display Regular price?<span class="required">*</span></label>
							<select id="dispRegPrice" name="dispRegPrice" class="form-control input-sm m-bot15">
						  		<option value="true">YES</option>
						  		<option value="false">NO</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Display Percentage Off?<span class="required">*</span></label>
							<select id="dispPerOff" name="dispPerOff" class="form-control input-sm m-bot15">
						  		<option value="true">YES</option>
						  		<option value="false">NO</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Expiry Date
							</label> <input class="form-control" type="text" id="pexpdate" name="pexpdate" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							 <label class="checkbox-inline">Reward Point Product?
							</label><input type="checkbox" name="isRwdPointCheck" id="isRwdPointCheck">
							<input type="hidden" name="isRwdPointProd" id="isRwdPointProd">
						</div>
						<div class="form-group col-sm-6 col-xs-6" id="regPriceDiv">
							<label>Regular Price<span class="required">*</span>
							</label> <input class="form-control" type="text" id="pregMinPrc" name="pregMinPrc" >
						</div>
						<div class="form-group col-sm-6 col-xs-6" id="sellPriceDiv">
							<label>Sale Price
							</label> <input class="form-control" type="text" id="pselMinPrc" name="pselMinPrc" >
						</div>
						<div class="form-group col-sm-6 col-xs-6" id="rwdPointDiv">
							<label>Required Reward Points
							</label> <input class="form-control" type="text" id="reqRwdPoint" name="reqRwdPoint" >
						</div>
						<div id="prodImageDiv" class="form-group col-sm-4 col-xs-4">
							<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 			<input type="file" id="prodImage" name="prodImage">
             			</div>
             			<div class="form-group form-group-top" id="prodImageDispDiv" style="width: 100%">
            				<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
	               				 <img id="prodImageDisp" src="" height="150" width="400" />
	               				 <a style="color:blue " href="javascript:callChangeProdImage();">Change Image</a>
           					</div>
							<div class="col-lg-3">&nbsp;</div>
    					</div>
					</form>
				</div>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="prodSave('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="prodSave('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>







<!-- Add Product Variant -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="prodVarModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Prod</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-6 col-xs-6">
						<button class="btn btn-primary" id="addRow" type="button" onclick="addRow('null')">Add New Option</button>
					</div>
					<form name="prodVarForm" id="prodVarForm" method="post">
						<table class="table table-bordered table-hover" id="prodVarTable">
							<thead>
								<tr>
									<th class="text-center">
										Product Option
									</th>
									<th class="text-center">
										Add Value
									</th>
									<th class="text-center">
										Option value
									</th>
									<th class="text-center">
										Image
									</th>
									<th class="text-center">
										Remove Value
									</th>
								</tr>
							</thead>
							<tbody id="varTableBody">
							</tbody>
						</table>
					</form>
				</div>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="saveVarBtn" type="button" onclick="saveProdVariants('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateVarBtn" type="button" onclick="saveProdVariants('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	
	//Contest Grid
	function getProdGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/ManageProducts.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+prodSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshProdGridValues(jsonData.prodList);
				clearAllSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcel(type){
		var appendData = "headerFilter="+prodSearchString+"&status=";
	    var url = apiServerUrl+"ContestExportToExcel?"+appendData;
	    //$('#download-frame').attr('src', url);
	}
	
	
	function resetFilters(){
		prodSearchString='';
		columnFilters = {};
		getProdGridData(0);
		//refreshQuestionGridValues('');
	}

	 var prodCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); 
	
	var pagingInfo;
	var prodDataView;
	var prodGrid;
	var prodData = [];
	var prodGridPager;
	var prodSearchString='';
	var columnFilters = {};
	var userProdColumnsStr = '<%=session.getAttribute("prodgrid")%>';

	var userProdColumns = [];
	var loadProdColumns = ["prodId","pBrand", "pName","pDesc","prodType","pRegMinPrice", "psMinPrice","isRwdPointProd","reqRwdPoint","maxCustQty","dispOrder","pExpDate",
	                           "status","updatedDate", "updatedBy","viewImg","editProd"];
	var allProdColumns = [ 
			prodCheckboxSelector.getColumnDefinition(),
			{
				id : "prodId",
				name : "Prod ID",
				field : "prodId",
				width : 80,
				sortable : true
			},{
				id : "pBrand",
				name : "Product Brand",
				field : "pBrand",
				width : 80,
				sortable : true
			}, {
				id : "pName",
				name : "Product Name",
				field : "pName",
				width : 80,
				sortable : true
			},{
				id : "pDesc",
				name : "Product Desc.",
				field : "pDesc",
				width : 80,
				sortable : true
			},{
				id : "prodType",
				name : "Product Type.",
				field : "prodType",
				width : 80,
				sortable : true
			},{
				id : "pRegMinPrice",
				name : "Regular Price",
				field : "pRegMinPrice",
				width : 80,
				sortable : true
			},{
				id : "psMinPrice",
				name : "Sale Price",
				field : "psMinPrice",
				width : 80,
				sortable : true
			},{
				id : "isRwdPointProd",
				name : "Reward Point Product",
				field : "isRwdPointProd",
				width : 80,
				sortable : true
			},{
				id : "reqRwdPoint",
				name : "Req. Reward Points",
				field : "reqRwdPoint",
				width : 80,
				sortable : true
			},{
				id : "maxCustQty",
				name : "Max Qty Limit",
				field : "maxCustQty",
				width : 80,
				sortable : true
			},{
				id : "dispOrder",
				name : "Display Order",
				field : "dispOrder",
				width : 80,
				sortable : true
			},{
				id : "isVariant",
				name : "Product with Variants?",
				field : "isVariant",
				width : 80,
				sortable : true
			},{
				id : "dispRegPrice",
				name : "Display Reg. price?",
				field : "dispRegPrice",
				width : 80,
				sortable : true
			},{
				id : "dispPerOff",
				name : "Display Perc. Off?",
				field : "dispPerOff",
				width : 80,
				sortable : true
			},{
				id : "pExpDate",
				name : "Expiry Date",
				field : "pExpDate",
				width : 80,
				sortable : true
			}, {
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			}, {
				id : "updatedDate",
				name : "Updated Date",
				field : "updatedDate",
				width : 80,
				sortable : true
			}, {
				id : "updatedBy",
				name : "Updated By",
				field : "updatedBy",
				width : 80,
				sortable : true
			},  {
				id : "viewImg",
				field : "viewImg",
				name : "Product Image",
				width : 80,
				formatter: viewImg
			}, {
				id : "delProd",
				field : "delProd",
				name : "Delete ",
				width : 80,
				formatter:deleteFormatter
			}];

	if (userProdColumnsStr != 'null' && userProdColumnsStr != '') {
		columnOrder = userProdColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allProdColumns.length; j++) {
				if (columnWidth[0] == allProdColumns[j].id) {
					userProdColumns[i] = allProdColumns[j];
					userProdColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//userProdColumns = allProdColumns;
		var columnOrder = loadProdColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allProdColumns.length;j++){
				if(columnWidth == allProdColumns[j].id){
					userProdColumns[i] = allProdColumns[j];
					userProdColumns[i].width=80;
					break;
				}
			}			
		}
	}

	function viewImg(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='viewClickableImage' src='../resources/images/audit-icon.png' id='"+ dataContext.pImgUrl +"'/>";
	    return button;
	}
	
	
	$('.viewClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	function deleteFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.prodId +"'/>";		
		return button;
	}
	$('.delClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getDeleteProd(id);
	});
	
	var prodOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var prodGridSortcol = "prodId";
	var prodGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function prodGridComparer(a, b) {
		var x = a[prodGridSortcol], y = b[prodGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshProdGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		prodData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (prodData[i] = {});
				d["id"] = i;
				d["prodId"] = data.spiId;
				d["sellerId"] = data.sellerId;
				d["pBrand"] = data.pBrand;
				d["prodType"] = data.prodType;
				d["pName"] = data.pName;
				d["pDesc"] = data.pDesc;
				if(data.isRwdPointProd == true  || isRwdPointProd.isRwdPointProd == 'true'){
					d["isRwdPointProd"] = "YES";
				}else{
					d["isRwdPointProd"] = "NO"
				}
				d["pRegMinPrice"] = data.pRegMinPrice;
				d["psMinPrice"] = data.psMinPrice;
				d["reqRwdPoint"] = data.reqRwdPoint;
				d["maxCustQty"] = data.maxCustQty;
				d["dispOrder"] = data.dispOrder;
				if(data.isVarient == true  || isRwdPointProd.isVarient == 'true'){
					d["isVariant"] = "YES";
				}else{
					d["isVariant"] = "NO"
				}
				
				if(data.isDisplayRegularPrice == true  || isRwdPointProd.isDisplayRegularPrice == 'true'){
					d["dispRegPrice"] = "YES";
				}else{
					d["dispRegPrice"] = "NO"
				}
				if(data.dispPerctageOff == true  || isRwdPointProd.dispPerctageOff == 'true'){
					d["dispPerOff"] = "YES";
				}else{
					d["dispPerOff"] = "NO"
				}
				
				
				d["pExpDate"] = data.pExpDateStr;
				d["status"] = data.status;
				d["pImgUrl"] = data.pImgUrl;
				d["updatedDate"] = data.updDateStr;
				d["updatedBy"] = data.updBy;
			}
		}

		prodDataView = new Slick.Data.DataView();
		prodGrid = new Slick.Grid("#prods_grid", prodDataView,
				userProdColumns, prodOptions);
		prodGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		prodGrid.setSelectionModel(new Slick.RowSelectionModel());
		prodGrid.registerPlugin(prodCheckboxSelector);
		
			prodGridPager = new Slick.Controls.Pager(prodDataView,
					prodGrid, $("#prods_pager"),
					pagingInfo);
		var prodGridColumnpicker = new Slick.Controls.ColumnPicker(
				allProdColumns, prodGrid, prodOptions);
					
		prodGrid.onSort.subscribe(function(e, args) {
			prodGridSortdir = args.sortAsc ? 1 : -1;
			prodGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				prodDataView.fastSort(prodGridSortcol, args.sortAsc);
			} else {
				prodDataView.sort(prodGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the prodGrid
		prodDataView.onRowCountChanged.subscribe(function(e, args) {
			prodGrid.updateRowCount();
			prodGrid.render();
		});
		prodDataView.onRowsChanged.subscribe(function(e, args) {
			prodGrid.invalidateRows(args.rows);
			prodGrid.render();
		});
		$(prodGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							prodSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											prodSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getProdGridData(0);
								}
							}

						});
		prodGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editProd' && args.column.id != 'delProd'){
					if(args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		prodGrid.init();
		
		var prodRowIndex = -1;
		prodGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempProdRowIndex = prodGrid.getSelectedRows([0])[0];
			if (tempProdRowIndex != prodRowIndex) {
				var prodId = prodGrid.getDataItem(tempProdRowIndex).prodId;
				var sellerId = prodGrid.getDataItem(tempProdRowIndex).sellerId;
				$('#selectedProdId').val(prodId);
				$('#sellerId').val(sellerId);
				getProductInventories(true);
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		prodDataView.beginUpdate();
		prodDataView.setItems(prodData);
		//prodDataView.setFilter(filter);
		prodDataView.endUpdate();
		prodDataView.syncGridSelection(prodGrid, true);
		prodGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveProdPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = prodGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('prodgrid', colStr);
	}
	
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(pagingInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(pagingInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(pagingInfo.pageNum) - 1;
		}
		getProdGridData(pageNo);
	}
	
			
	//Edit Contest
	function editProd(){
		var prodId = $('#selectedProdId').val();
		if (prodId == null || prodId == undefined) {
			jAlert("Plese select product to Edit", "info");
			return false;
		}
		getEditProd(prodId);
	}
	
	
	
	function getEditProd(prodId){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/UpdateProduct",
			type : "post",
			dataType: "json",
			data: "slrpitmsId="+prodId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					$('#prodModal').modal('show');
					setEditProd(jsonData.prod);
				}else{
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function setEditProd(data){
		$('#saveBtn').hide();			
		$('#updateBtn').show();
		$('#prodImageDispDiv').show();
		$('#prodImageDiv').hide();
		$('#prodId').val(data.spiId);
		$('#prodSeller').val(data.sellerId);
		$('#pname').val(data.pName);
		$('#pbrand').val(data.pBrand);
		$('#pdesc').val(data.pDesc);
		$('#plongdesc').val(data.pLongDesc);	
		if(data.isRwdPointProd == true || data.isRwdPointProd == 'true'){
			$('#isRwdPointCheck').prop('checked', true);
			$('#regPriceDiv').hide();
			$('#sellPriceDiv').hide();
			$('#rwdPointDiv').show();
			$('#isRwdPointProd').val('true');
		}else{
			$('#isRwdPointCheck').prop('checked', false);
			$('#isRwdPointProd').val('false');
			$('#rwdPointDiv').hide();
			$('#regPriceDiv').show();
			$('#sellPriceDiv').show();
		}
		$('#pregMinPrc').val(data.pRegMinPrice);	
		$('#pselMinPrc').val(data.psMinPrice);
		$('#reqRwdPoint').val(data.reqRwdPoint);
		$('#prodType').val(data.prodType);
		$('#maxcustqty').val(data.maxCustQty);
		$('#dispOrder').val(data.dispOrder);
		$('#isVariant').val(data.isVarient);
		$('#dispRegPrice').val(data.isDisplayRegularPrice);
		$('#dispPerOff').val(data.dispPerctageOff);
		$('#pexpdate').val(data.pExpDateStr);
		$('#prodImageDisp').attr("src",data.pImgUrl);
	}
	
	function callChangeProdImage(){
		$('#prodImageDispDiv').hide();
		$('#prodImageDiv').show();
		$('#isimgupd').val('TRUE');
	}
	
	//Delete Contest
	function deleteProd(){		
		var prodId = $('#selectedProdId').val();
		if (prodId == null || prodId == undefined) {
			jAlert("Plese select product to Delete", "info");
			return false;
		}
		getDeleteProd(prodId);
	}
	
	function getDeleteProd(prodId){
		if(prodId == '' || prodId == undefined) {
			jAlert("Please select a Product to Delete.","Info");
			return false;S
		}
		jConfirm("Are you sure to delete a product ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ecomm/UpdateProduct",
						type : "post",
						dataType: "json",
						data : "slrpitmsId="+prodId+"&action=DELETE&status=${status}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.sts == 1){
								pagingInfo = jsonData.pagingInfo;
								columnFilters = {};
								refreshProdGridValues(jsonData.prodList);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	
	function changeStatus(action){
		var prodId = $('#selectedProdId').val();
		if (prodId == null || prodId == undefined) {
			jAlert("Plese select product to update", "info");
			return false;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/UpdateProduct",
			type : "post",
			dataType: "json",
			data : "slrpitmsId="+prodId+"&action="+action+"&status=${status}",
			success : function(response) {
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					pagingInfo = jsonData.pagingInfo;
					columnFilters = {};
					refreshProdGridValues(jsonData.prodList);
				}
				jAlert(jsonData.msg);
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	
	
	
	function getProductInventories(clearFilterFlag){
		var prodId = $('#selectedProdId').val();
		var sellerId = $('#sellerId').val();
		if(prodId == null || prodId == undefined){
			jAlert("Please select product from top grid to see product inventories", "Info");
			return false;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/getProdInventories",
			type : "post",
			dataType: "json",
			data : "slrpitmsId="+prodId+"&slerId="+sellerId+"&status=${status}"+"&headerFilter="+invSearchString,
			success : function(response) {
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					invPagingInfo = JSON.parse(jsonData.pagingInfo);
					if(clearFilterFlag) {
						invColumnFilters = {};	
					}
					
					refreshInvGridValues(JSON.parse(jsonData.inventoryList));
					invUpdFormData = new FormData();
				}else{
					jAlert(jsonData.msg);
					return false;
				}
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
		
		
	}
	
	
	
//Inventory Grid.
	
function invExportToExcel(){
	var appendData = "headerFilter="+invSearchString;
    var url = apiServerUrl + "PopularArtistExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function invResetFilters(){
	invSearchString='';
	invColumnFilters = {};
	var prodId = $('#selectedProdId').val();
	if(prodId == null || prodId == undefined){
		jAlert("Please select product from top grid to see product inventories", "Info");
		return false;
	}
	
}

var invCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var invPagingInfo;
	var invDataView;
	var invGrid;
	var invData = [];
	var invSearchString='';
	var invColumnFilters = {};
	var userInvColumnsStr = '<%=session.getAttribute("invgrid")%>';
	var userInvColumns =[];
	var allInvColumns =  [ //invCheckboxSelector.getColumnDefinition(),
	{
		id: "sellerId", 
		name: "Seller ID", 
		field: "sellerId", 
		width:80,
		sortable: true
	},{
		id: "spiId", 
		name: "Prod ID", 
		field: "spiId", 
		width:80,
		sortable: true
	}, {
		id: "vOptNameValCom", 
		name: "Product Variant", 
		field: "vOptNameValCom", 
		width:80,
		sortable: true
	}, {
		id : "soldQty",
		field : "soldQty",
		name : "Sold Qty",
		width:80,
		sortable: true
	},{
		id: "avlQty", 
		name: "Avail. Qty", 
		field: "avlQty", 
		width:80,
		editor: Slick.Editors.Text,
		cssClass:'slick-editableCellCss',
		sortable: true
	}, {
		id : "regPrice",
		field : "regPrice",
		name : "Regular Price",
		width:80,
		editor: Slick.Editors.Text,
		cssClass:'slick-editableCellCss',
		sortable: true
	},{
		id: "sellPrice", 
		name: "Sale Price", 
		field: "sellPrice", 
		width:80,
		editor: Slick.Editors.Text,
		cssClass:'slick-editableCellCss',
		sortable: true
	},{
		id: "reqLPnts", 
		name: "Req. Reward Points", 
		field: "reqLPnts", 
		width:80,
		editor: Slick.Editors.Text,
		cssClass:'slick-editableCellCss',
		sortable: true
	},{
		id: "priceAftLPnts", 
		name: "Price with Reward Points", 
		field: "priceAftLPnts", 
		width:80,
		editor: Slick.Editors.Text,
		cssClass:'slick-editableCellCss',
		sortable: true
	},{
		id: "invImg", 
		name: "View Image", 
		field: "invImg", 
		width:80,
		sortable: true
	},{
		id: "status", 
		name: "Status", 
		field: "status", 
		width:80,
		sortable: true
	},{
		id: "updDate", 
		name: "Last Updated", 
		field: "updDate", 
		width:80,
		sortable: true
	},{
		id: "updBy", 
		name: "Updated By", 
		field: "updBy", 
		width:80,
		sortable: true
	} ];

	if(userInvColumnsStr!='null' && userInvColumnsStr!=''){
		var columnOrder = userInvColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allInvColumns.length;j++){
				if(columnWidth[0] == allInvColumns[j].id){
					userInvColumns[i] =  allInvColumns[j];
					userInvColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userInvColumns = allInvColumns;
	}
	
	var invOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true,
		editable: true,
	    enableAddRow: true
	};
	var invGridSortcol = "spiId";
	var invGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var invGridSearchString = "";

	function deleteRecordFromArtistGrid(id) {
		invDataView.deleteItem(id);
		invGrid.invalidate();
	}
	/*
	function invGridFilter(item, args) {
		var x= item["artistName"];
		if (args.invGridSearchString  != ""
				&& x.indexOf(args.invGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.invGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function invGridComparer(a, b) {
		var x = a[invGridSortcol], y = b[invGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function invGridToggleFilterRow() {
		invGrid.setTopPanelVisibility(!invGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#inv_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

	*/	
	function refreshInvGridValues(jsonData){		
		invData = [];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (invData[i] = {});
				d["spivInvId"] = data.spivInvId;
				d["sellerId"] = data.sellerId;
				d["id"] = i;
				d["spiId"] = data.spiId;
				d["vOptNameValCom"] = data.vOptNameValCom;
				d["soldQty"] = data.soldQty;
				d["avlQty"] = data.avlQty;
				d["regPrice"] = data.regPrice;
				d["sellPrice"] = data.sellPrice;
				d["reqLPnts"] = data.reqLPnts;
				d["priceAftLPnts"] = data.priceAftLPnts;
				d["invImg"] = data.pImg;
				d["status"] = data.status;
				d["updDate"] = data.updDateStr;
				d["updBy"] = data.updBy;
			}
		}		
		
		invDataView = new Slick.Data.DataView();
		invGrid = new Slick.Grid("#inv_grid", invDataView, userInvColumns, invOptions);
		invGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		invGrid.setSelectionModel(new Slick.RowSelectionModel());
		invGrid.registerPlugin(invCheckboxSelector);
		if(invPagingInfo != null){
			var invGridPager = new Slick.Controls.Pager(invDataView, invGrid, $("#inv_pager"),invPagingInfo);
		}
		var invGridColumnpicker = new Slick.Controls.ColumnPicker(allInvColumns, invGrid,
				invOptions);

		// move the filter panel defined in a hidden div into invGrid top panel
		//$("#inv_inlineFilterPanel").appendTo(invGrid.getTopPanel()).show();

		/* invGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < invDataView.getLength(); i++) {
				rows.push(i);
			}
			invGrid.setSelectedRows(rows);
			e.preventDefault();
		}); */
		
		invGrid.onSort.subscribe(function(e, args) {
			invGridSortdir = args.sortAsc ? 1 : -1;
			invGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				invDataView.fastSort(invGridSortcol, args.sortAsc);
			} else {
				invDataView.sort(invGridComparer, args.sortAsc);
			}
		});
		// wire up model invs to drive the invGrid
		invDataView.onRowCountChanged.subscribe(function(e, args) {
			invGrid.updateRowCount();
			invGrid.render();
		});
		invDataView.onRowsChanged.subscribe(function(e, args) {
			invGrid.invalidateRows(args.rows);
			invGrid.render();
		});
		$(invGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	invSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  invColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in invColumnFilters) {
					  if (columnId !== undefined && invColumnFilters[columnId] !== "") {
						  invSearchString += columnId + ":" +invColumnFilters[columnId]+",";
					  }
					}
					//var artistIds = getSelectedartistGridId();
					//var productId=$("#selectedProdId").val();
					getProductInventories(false);
				}
			  }
		 
		});
		invGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(invColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(invColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
			
		});
		invGrid.init();
		
		invGrid.onCellChange.subscribe(function (e,args) { 
			
			var item = args.item;
			var editedColumn =''; 
			var value='';
			var invId = item.spivInvId;
			if (args.cell == invGrid.getColumnIndex('avlQty')) {
				editedColumn='avlQty';
				value = item.avlQty;
			} else if (args.cell == invGrid.getColumnIndex('regPrice')) {
				editedColumn='regPrice';
				value = item.regPrice;
			} else if(args.cell == invGrid.getColumnIndex('sellPrice')) {
				editedColumn='sellPrice';
				value = item.sellPrice;
			}else if(args.cell == invGrid.getColumnIndex('reqLPnts')) {
				editedColumn='reqLPnts';
				value = item.reqLPnts;
			}else if(args.cell == invGrid.getColumnIndex('priceAftLPnts')) {
				editedColumn='priceAftLPnts';
				value = item.priceAftLPnts;
			}
			updateInvDataToForm(invId,editedColumn,value,item);
			
			
			/* var temprOpenOrderRwIndex = invGrid.getSelectedRows();
			var invId;
			var regPrice; 
			$.each(temprOpenOrderRwIndex, function (index, value) {
				invId = invGrid.getDataItem(value).spiId;
				regPrice = invGrid.getDataItem(value).regPrice;
				updateInvDataToForm(invGrid.getDataItem(value));
			});
			alert(invId+' :1:'+regPrice); */
			//saveOrderNote(openOrderId, orderNotes);
     	});
		
		/*
		// wire up the search textbox to apply the filter to the model
		$("#invGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			invGridSearchString = this.value;
			updateInvGridFilter();
		});
		function updateInvGridFilter() {
			invDataView.setFilterArgs({
				invGridSearchString : invGridSearchString
			});
			invDataView.refresh();
		}
		*/
		// initialize the model after all the invs have been hooked up
		invDataView.beginUpdate();
		invDataView.setItems(invData);
		/*invDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			invGridSearchString : invGridSearchString
		});
		invDataView.setFilter(invGridFilter);*/
		invDataView.endUpdate();
		invDataView.syncGridSelection(invGrid, true);
		invGrid.setSelectedRows([]);
		invGrid.resizeCanvas();
	}
	
	<!-- inv Grid Code Ends -->
	
	function saveUserInvPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = invGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('invgrid',colStr);
	}
	
	
	
	
	
	
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshProdGridValues(JSON.parse(JSON.stringify(${prodList})));
		$('#prods_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveProdPreference()'>");
		$('#inv_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserInvPreference()'>");
		enableMenu();
	};
	
	
function getSelectedProductIdForExcelExportFromGrid() {
		var tempprodExpGridRowIndex = prodGrid.getSelectedRows();
		var slrpitmsIdStr='';
		$.each(tempprodExpGridRowIndex, function (index, value) {		
			slrpitmsIdStr += ','+prodGrid.getDataItem(value).prodId;
		});
		
		if(slrpitmsIdStr != null && slrpitmsIdStr!='') {
			slrpitmsIdStr = slrpitmsIdStr.substring(1, slrpitmsIdStr.length);
			 return slrpitmsIdStr;
		}
	}		
function exportSaaSGameProductsToExcel(){
	var prodIdStr = getSelectedProductIdForExcelExportFromGrid();	
	var appendData = "slrpitmsId="+prodIdStr;
    var url = apiServerUrl + "ExportRTFProductsForSaaS?"+appendData;
    $('#download-frame').attr('src', url);
}
	
	
	
		
</script>