<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<!-- full calendar css-->
<link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
<!-- easy pie chart-->
<link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
<!-- owl carousel -->
<link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link rel="stylesheet" href="../resources/css/fullcalendar.css">
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<link href="../resources/css/xcharts.min.css" rel=" stylesheet">
<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

<!-- javascripts -->
<%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
<!-- bootstrap -->
<script src="../resources/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="../resources/js/jquery.scrollTo.min.js"></script>
<script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/jquery.alerts.js"></script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
	Stripe.setPublishableKey('${sPubKey}');
</script>


<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker.css" type="text/css" />


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

.slick-viewport {
	overflow-x: hidden !important;
}
/* .light_box {
	display: none;
	position: absolute;
	top: 10%;
	left: 25%;
	width: 45%;
	height: 100%;
	padding: 20px;
	border: 1px solid #888;
	background-color: #fefefe;
	z-index: 1;
	overflow: auto;
} */
#addCustomer {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

#totalPrice {
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
}

#totalQty {
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
}
</style>

<script>
	var flag;
	function doValidateReferralCode() {
		var custRowId = customerGrid.getSelectedRows([ 0 ])[0];
		var referralCode = $('#referralCode').val();
		var customerId = customerGrid.getDataItem(custRowId).customerId;
		flag = false;
		if ($('#referralCode').val() != null && $('#referralCode').val() != '') {
			$
					.ajax({
						url : "${pageContext.request.contextPath}/Accounting/ValidateReferralCode",
						type : "get",
						data : "customerId=" + customerId + "&referralCode="+ referralCode
						+"&eventId="+$('#eventId').val()+"&ticketGroupId="+$('#ticketId').val()+"&isLongSale=true",
						dataType : "text",
						success : function(res) {
							if (res.indexOf("REFERRALCODEVALIDATED") != -1) {
								jAlert("Referral Code was applied");
								flag = true;
							}else{
								jAlert(res);
								flag = false; 
							}
						},
						error : function(error) {
							jAlert(
									"Your login session is expired please refresh page and login again.",
									"Error");
							flag = false;
						}
					});
		}
	}
	function validateCreditCard() {
		if ($('#cardNumber').val() == null || $('#cardNumber').val() == "") {
			jAlert("Please enter valid card number.");
			return false;
		}

		if ($('#expMonth').val() == null || $('#expMonth').val() == "") {
			jAlert("Please enter expiry month.");
			return false;
		}

		if ($('#expYear').val() == null || $('#expYear').val() == "") {
			jAlert("Please enter expiry year.");
			return false;
		}

		/* if ($('#cvv').val() == null || $('#cvv').val() == "") {
			jAlert("Please enter valid cvv.");
			return false;
		} */
		return true;
	}
	var flags;
	function doSaveInvoice() {

		/* if($('#eventId').val() == null || $('#eventId').val() == ''){
			jAlert("There is something wrong.Please choose valid event");
			return false;
		}
		if($('#ticketId').val() == null || $('#ticketId').val() == ''){
			jAlert("There is something wrong.Please choose valid ticket");
			return false;
		}
		if($('#customerId').val() == null || $('#customerId').val() == ''){
			jAlert("There is something wrong.Please choose valid customer");
			return false;
		} */
		flags = "";
		var referralCode = $('#referralCode').val();
		if ($('#referralCode').val() == null || $('#referralCode').val() == '') {
			flags = true;
		} else {
			flags = flag;
		}
		if (!flags) {
			jAlert("Please validate your Referral Code, if it is not validated. <br> If it's not known Referral Code - kindly clear it.");
			return false;
		}

		if ($('#bAddressId').val() == null || $('#bAddressId').val() == '') {
			jAlert("Billing address is mandatory.");
			return false;
		}

		if ($('#sAddressId').val() == null || $('#sAddressId').val() == '') {
			jAlert("Selected customer does not have Shipping address, Please select Shipping same As Billing Address.");
			return false;
		}
		var paymentMethod = $('#paymentMethod').val();
		var savedCardId = $('#custCards').val();
		var requiredAmount = parseFloat($('#payableAmount').val());

		if (paymentMethod == null) {
			jAlert("Please choose payment method.");
			return false;
		} else if (paymentMethod == 'CREDITCARD') {
			if (null == savedCardId || savedCardId == 0) {
				if (validateCreditCard()) {
					$("#sBySavedCard").val("NO");
					var expMonth = $('#expMonth').val();
					var expYear = $('#expYear').val();
					expYear = expYear.substring(2, 4);
					var stripExp = expMonth + "/" + expYear;
					$('#stripeExp').val(stripExp);
					var cvv = $('#cvv').val();

					if (Stripe.card.validateCardNumber($('#cardNumber').val())
							&& Stripe.card
									.validateExpiry($('#stripeExp').val())
							&& Stripe.card.validateCVC($('#cvv').val())) {
						Stripe.createToken({
							name : $('#name').val(),
							number : $('#cardNumber').val(),
							exp : stripExp,
							cvc : $('#cvv').val()
						}, stripeResponseHandler);

					} else {
						jAlert("Please enter valid card information.");
						return false;
					}
				} else {
					return false;
				}
			} else {
				/* if ($('#cvv').val() == null || $('#cvv').val() == "") {
					jAlert("Please enter valid cvv.");
					return false;
				} */
				$("#sBySavedCard").val("YES");
				$("#sSavedCardId").val(savedCardId);
				$("#action").val('saveInvoice');
				$("#invoiceForm").submit();
			}
			/* $("#action").val('saveInvoice');
			$("#invoiceForm").submit(); */
		} else if (paymentMethod == 'REWARDPOINTS') {
			var total = parseFloat($('#customerRewardPoints').val());
			if (total < requiredAmount) {
				jAlert("Selected customer have only " + total
						+ " active reward points.");
				return false;
			}
			$("#rewardPoints").val(requiredAmount);
			$("#action").val('saveInvoice');
			$("#invoiceForm").submit();
		} else if (paymentMethod == 'WALLET') {
			var walletCredit = parseFloat($('#walletCredit').val());
			if (walletCredit < requiredAmount) {
				jAlert("Selected customer have only " + walletCredit
						+ " Wallet Credit.");
				return false;
			}
			$("#action").val('saveInvoice');
			$("#invoiceForm").submit();
		} else if (paymentMethod == 'WALLET_CREDITCARD') {
			var walletCredit = parseFloat($('#walletCredit').val());
			var cardAmount = parseFloat($('#cardAmount').val());
			var walletAmount = parseFloat($('#walletAmount').val());
			if (cardAmount <= 0 || isNaN(cardAmount)) {
				jAlert("Please Enter Valid Card Amount, should be greater than 0.");
				return false;
			}
			if (walletAmount <= 0 || isNaN(walletAmount)) {
				jAlert("Please Enter valid Wallet Amount, Should be greater than 0.");
				return false;
			}
			if (walletAmount > walletCredit) {
				jAlert("Customer have only " + walletCredit + " Wallet Amount.");
				return false;
			}
			var total = (cardAmount + walletAmount).toFixed(2);

			if (requiredAmount != total) {
				jAlert("Wallet Amount and Card Amount should be equal to "
						+ requiredAmount);
				return false;
			}
			if (null == savedCardId || savedCardId == 0) {
				if (validateCreditCard()) {
					$("#sBySavedCard").val("NO");
					var expMonth = $('#expMonth').val();
					var expYear = $('#expYear').val();
					expYear = expYear.substring(2, 4);
					var stripExp = expMonth + "/" + expYear;
					$('#stripeExp').val(stripExp);
					var cvv = $('#cvv').val();

					if (Stripe.card.validateCardNumber($('#cardNumber').val())
							&& Stripe.card
									.validateExpiry($('#stripeExp').val())
							&& Stripe.card.validateCVC($('#cvv').val())) {
						Stripe.createToken({
							name : $('#name').val(),
							number : $('#cardNumber').val(),
							exp : stripExp,
							cvc : $('#cvv').val()
						}, stripeResponseHandler);

					} else {
						jAlert("Please enter valid card information.");
						return false;
					}
				} else {
					return false;
				}
			} else {
				/* if ($('#cvv').val() == null || $('#cvv').val() == "") {
					jAlert("Please enter valid cvv.");
					return false;
				} */
				$("#sBySavedCard").val("YES");
				$("#sSavedCardId").val(savedCardId);
				$("#action").val('saveInvoice');
				$("#invoiceForm").submit();
			}
			
			/* $("#action").val('saveInvoice');
			$("#invoiceForm").submit(); */
		} else if (paymentMethod == 'PARTIAL_REWARD_CC') {
			var cardAmount = parseFloat($('#cardAmount').val());
			var rewardPoints = parseFloat($('#rewardPoints').val());
			var customerRewardPoints = parseFloat($('#customerRewardPoints')
					.val());
			if (cardAmount <= 0 || isNaN(cardAmount)) {
				jAlert("Please enter vaid Card amount, Should be greater than 0.");
				return false;
			}
			if (rewardPoints <= 0 || isNaN(rewardPoints)) {
				jAlert("Please enter valid reward point, Should be greater than 0.");
				return false;
			}
			if (rewardPoints > customerRewardPoints) {
				jAlert("Selected customer have only " + customerRewardPoints
						+ " active reward points.");
				return false;
			}
			var total = (rewardPoints + cardAmount).toFixed(2);

			if (total != requiredAmount) {
				jAlert("Reward points and Card amount total should be equals to "
						+ requiredAmount);
				return false;
			}
			if (null == savedCardId || savedCardId == 0) {
				if (validateCreditCard()) {
					$("#sBySavedCard").val("NO");
					var expMonth = $('#expMonth').val();
					var expYear = $('#expYear').val();
					expYear = expYear.substring(2, 4);
					var stripExp = expMonth + "/" + expYear;
					$('#stripeExp').val(stripExp);
					var cvv = $('#cvv').val();

					if (Stripe.card.validateCardNumber($('#cardNumber').val())
							&& Stripe.card
									.validateExpiry($('#stripeExp').val())
							&& Stripe.card.validateCVC($('#cvv').val())) {
						Stripe.createToken({
							name : $('#name').val(),
							number : $('#cardNumber').val(),
							exp : stripExp,
							cvc : $('#cvv').val()
						}, stripeResponseHandler);

					} else {
						jAlert("Please enter valid card information.");
						return false;
					}
				} else {
					return false;
				}
			} else {
				$("#sBySavedCard").val("YES");
				$("#sSavedCardId").val(savedCardId);
				$("#action").val('saveInvoice');
				$("#invoiceForm").submit();
			}
			/* if ($('#cvv').val() == null || $('#cvv').val() == "") {
				jAlert("Please enter valid cvv.");
				return false;
			} 
			
			$("#action").val('saveInvoice');
			$("#invoiceForm").submit();*/
		} else if (paymentMethod == 'PARTIAL_REWARD_WALLET') {
			var rewardPoints = parseFloat($('#rewardPoints').val());
			var walletCredit = parseFloat($('#walletCredit').val());
			var walletAmount = parseFloat($('#walletAmount').val());
			var customerRewardPoints = parseFloat($('#customerRewardPoints')
					.val());
			var cardAmount = parseFloat($('#cardAmount').val());
			if (rewardPoints <= 0 || isNaN(rewardPoints)) {
				jAlert("Please enter valid reward points, Should be greater than 0.");
				return false;
			}
			if (walletAmount <= 0 || isNaN(walletAmount)) {
				jAlert("Please enter valid Wallet amount, Should be greater than 0.");
				return false;
			}
			if (rewardPoints > customerRewardPoints) {
				jAlert("Selected customer have only " + customerRewardPoints
						+ " active reward points.");
				return false;
			}
			if (walletAmount > walletCredit) {
				jAlert("Selected customer have only " + customerRewardPoints
						+ " Wallet Amount.");
				return false;
			}
			var total = (walletAmount + rewardPoints).toFixed(2);
			if (total != requiredAmount) {
				jAlert("Reward points and Wallet amount total should be equals to "
						+ requiredAmount);
				return false;
			}
			$("#action").val('saveInvoice');
			$("#invoiceForm").submit();
		} else if (paymentMethod == 'PARTIAL_REWARD_WALLET_CC') {
			var cardAmount = parseFloat($('#cardAmount').val());
			var rewardPoints = parseFloat($('#rewardPoints').val());
			var walletCredit = parseFloat($('#walletCredit').val());
			var walletAmount = parseFloat($('#walletAmount').val());
			var customerRewardPoints = parseFloat($('#customerRewardPoints')
					.val());
			if (cardAmount <= 0 || isNaN(cardAmount)) {
				jAlert("Please enter valid card amount, Should be greater than 0.");
				return false;
			}
			if (rewardPoints <= 0 || isNaN(rewardPoints)) {
				jAlert("Please enter valid reward points, Should be greater than 0.");
				return false;
			}
			if (walletAmount <= 0 || isNaN(walletAmount)) {
				jAlert("Please enter valid wallet amount, Should be greater than 0.");
				return false;
			}
			if (rewardPoints > customerRewardPoints) {
				jAlert("Selected customer have only " + customerRewardPoints
						+ " active reward points.");
				return false;
			}
			if (walletAmount > walletCredit) {
				jAlert("Selected customer have only " + customerRewardPoints
						+ " Wallet Amount.");
				return false;
			}
			var total = (walletAmount + rewardPoints + cardAmount).toFixed(2);
			if (total != requiredAmount) {
				jAlert("Reward points + Wallet amount + Card amount total should be equals to "
						+ requiredAmount);
				return false;
			}
			 if(null == savedCardId || savedCardId == 0){
				 if(validateCreditCard()){
					 $("#sBySavedCard").val("NO");
					  var expMonth = $('#expMonth').val();
				      var expYear = $('#expYear').val();
				      expYear = expYear.substring(2,4);
				      var stripExp = expMonth+"/"+expYear;
				      $('#stripeExp').val(stripExp);
				      var cvv = $('#cvv').val();
				      
					  if (Stripe.card.validateCardNumber($('#cardNumber').val())
					      && Stripe.card.validateExpiry($('#stripeExp').val())
					      && Stripe.card.validateCVC($('#cvv').val())) {
					      Stripe.createToken({
					             name: $('#name').val(),
					             number: $('#cardNumber').val(),
					             exp: stripExp,
					             cvc: $('#cvv').val()
					         }, stripeResponseHandler);
					      
				      } else {
				    	  jAlert("Please enter valid card information.");
				  	  	  return false;
				      }
			 	 }else{
			 		 return false; 
			 	 }
			 }else{
				 $("#sBySavedCard").val("YES");
				 $("#sSavedCardId").val(savedCardId);
				 $("#action").val('saveInvoice');
				 $("#invoiceForm").submit();
			 }
			/* $("#action").val('saveInvoice');
			$("#invoiceForm").submit(); */
		} else {
			return false;
		}
	}

	function stripeResponseHandler(status, response) {
		if (response.error) {
			jAlert(response.error.message);
			return false;
		} else {
			$("#sBySavedCard").val("NO");
			$("#sTempToken").val(response.id);
			$("#action").val('saveInvoice');
			$("#invoiceForm").submit();
			return true;
		}
	};

	//Invoice cancel action
	function cancelAction() {
		window.close();
	}

	function editAddr(id) {
		jAlert('edit action' + id);
		var currentTD = $(this).parents('tr').find('td');
		if ($(this).html() == 'Edit') {
			currentTD = $(this).parents('tr').find('td');
			$.each(currentTD, function() {
				$(this).prop('contenteditable', true);
			});
		} else {
			$.each(currentTD, function() {
				$(this).prop('contenteditable', false);
			});
		}

		$(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit');

	}

	function changePaymentMethod() {
		var paymentMethod = $('#paymentMethod').val();
		if (paymentMethod == 'CREDITCARD') {
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').show();
		} else if (paymentMethod == 'WALLET' || paymentMethod == 'REWARDPOINTS') {
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#walletAmountDiv').hide();
		} else if (paymentMethod == 'WALLET_CREDITCARD') {
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').show();
			$('#creditCardDiv').show();
			$('#walletAmountDiv').show();
		} else if (paymentMethod == 'PARTIAL_REWARD_CC') {
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').show();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').show();
		} else if (paymentMethod == 'PARTIAL_REWARD_WALLET') {
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#rewardPointsDiv').show();
			$('#walletAmountDiv').show();
			$('.cardInfo').hide();
		} else if (paymentMethod == 'PARTIAL_REWARD_WALLET_CC') {
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').show();
			$('#creditCardDiv').show();
			$('#walletAmountDiv').show();
		}
	}

	$(document).ready(function() {
		soldPriceChange();
		getCustomerGridData(0);
		$('#searchValue').keypress(function(event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			if (keyCode == 13) // the enter key code
			{
				getCustomerGridData(0);
				return false;
			}
		});

		$(window).resize(function() {
			customerGrid.resizeCanvas();
		});

		$("#custCards").change(function() {
			var creditCardValue = $("#custCards").val();
			if (creditCardValue != 0) {
				$('#cardNumber').attr('disabled', 'disabled'); // mark it as read only
				$('#expMonth').attr('disabled', 'disabled');
				$('#expYear').attr('disabled', 'disabled');
				$('#name').attr('disabled', 'disabled');
				$('#cvv').attr('disabled', 'disabled');
			}
			if (creditCardValue == 0) {
				$('#cardNumber').removeAttr('disabled');
				$('#expMonth').removeAttr('disabled');
				$('#expYear').removeAttr('disabled');
				$('#name').removeAttr('disabled');
				$('#cvv').removeAttr('disabled');
			}
		});
	});

	window.onunload = function() {
		var win = window.opener;
		if (!win.closed) {
			window.opener.searchTicketData();
		}
	};

	function soldPriceChange() {
		if ($('#soldPrice').val() == '' || $('#soldPrice').val() == null
				|| $('#soldPrice').val() <= 0) {
			jAlert("Sold price cannot be less than 0.");
		}
		var soldPrice = parseFloat($('#soldPrice').val());
		var ticketCount = parseInt($('#ticketCount').val());
		var taxAmt = parseFloat($('#taxAmount').val());
		var payableAmount = parseFloat((soldPrice * ticketCount) + (taxAmt*ticketCount))
				.toFixed(2);

		$('#totalPayableAmount').text(payableAmount);
		$('#payableAmount').val(payableAmount);
		var payAmount = parseFloat(payableAmount);
		var earnPoint = (payAmount * 0.10).toFixed(2);
		$('#earnedRewardPoints').text(earnPoint);
		$('#earnRewardPoint').val(earnPoint);
	}

	function addNewCustomer() {
		popupCenter(
				"${pageContext.request.contextPath}/Client/AddCustomerPopup",
				"Add Customer", "1000", "1000");
	}

	function resetFilters() {
		customerGridSearchString = '';
		columnFilters = {};
		getCustomerGridData(0);
	}
</script>


<div class="row">
	<div class="col-lg-12">

		<ol class="breadcrumb">
			<li><i class="fa fa-laptop"></i>Create Invoice</li>
		</ol>

	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<h3 style="font-size: 13px; font-family: arial;" class="page-header">Search Customer</h3>
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
		<!--  search form start -->
		<form:form class="form-inline" role="form" id="customerSearch" onsubmit="return false" method="post">
			<!-- 
			<div class="form-group col-xs-2 col-md-2">
			<label for="name" class="control-label">Search By</label>
				<select id="searchType" name="searchType" class="form-control input-sm m-bot15">
				  <option value="customerName" <c:if test="${searchType eq 'customerName'}">selected</c:if> >Customer Name</option>
				  <option value="email">Email</option>
				  <option value="customerType" <c:if test="${searchType eq 'customerType'}">selected</c:if> >CustomerType</option>
				</select>
			</div>
            <div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Search Value</label>
				<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
			</div>
			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Product Type</label> <select id="productType" name="productType" class="form-control ">
					<option <c:if test="${productType=='ALL'}"> Selected </c:if> value="ALL">All</option>
					<option <c:if test="${productType=='REWARDTHEFAN'}"> Selected </c:if> value="REWARDTHEFAN">Reward The Fan</option>
					<option <c:if test="${productType=='RTW'}"> Selected </c:if> value="RTW">RTW</option>
					<option <c:if test="${productType=='RTW2'}"> Selected </c:if> value="RTW2">RTW2</option>
				</select>
				</div>
				<div class="form-group col-xs-2 col-md-2">
					<label>Client/Broker</label>
					<select name="customerTypeSearchValues"
						class="form-control input-sm m-bot15" id="customerTypeSearch">
						<option value="both" <c:if test="${customerType=='both'}"> Selected </c:if>>Both</option>
						<option value="client" <c:if test="${customerType=='client'}"> Selected </c:if>>Client</option>
						<option value="broker" <c:if test="${customerType=='broker'}"> Selected </c:if>>Broker</option>
					</select>
				</div>
			<div class="form-group col-xs-2 col-md-2">
			 	<button type="button" id="searchCustomerBtn" class="btn btn-primary" style="margin-top:18px;" onclick="getCustomerGridData(0);">Search</button>
			 </div>
			 -->
			<div class="form-group">
				<button type="button" class="btn btn-primary" style="float: right; margin-top: 18px;" onclick="addNewCustomer();">Add New Customer</button>
			</div>
		</form:form>
	</div>
</div>
<br />
<div style="position: relative;" id="customerGridDiv">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label style="font-size: 10px;">Customers Details</label> <a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
		</div>
		<div id="customer_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="customer_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br />
<br />
<form class="form-horizontal" id="invoiceForm" method="post" action="SaveInvoiceRealTicket">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading"> Customer Billing Details </header>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Customer Name:</label> <span style="font-size: 12px;" id="customerNameDiv"></span> <input type="hidden" name="shFirstName" id="shFirstName"
							value="" /> <input type="hidden" name="shLastName" id="shLastName" value="" /> <input type="hidden" name="blFirstName" id="blFirstName" value="" /> <input type="hidden" name="blLastName"
							id="blLastName" value="" />


					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Email:</label> <input type="hidden" name="blEmail" id="blEmail" value="" /> <span id=emailDiv></span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Phone:</label><span id=phoneDiv></span> <input type="hidden" name="phone" id="phone" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Address Line:</label><span id=addressLineDvi></span> <input type="hidden" name="addressLine1" id="addressLine1" value="" /> <input
							type="hidden" name="addressLine2" id="addressLine2" value="" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Country:</label><span id=countryDiv></span> <input type="hidden" name="country" id="country" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">State:</label><span id=stateDiv></span> <input type="hidden" name="state" id="state" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">City:</label><span id=cityDiv></span> <input type="hidden" name="city" id="city" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">ZipCode:</label><span id=zipCodeDiv></span> <input type="hidden" name="zipCode" id="zipCode" value="" />
					</div>
				</div>

			</div>
			</section>
		</div>
	</div>

	<!-- Shipping Address section -->
	<div class="row">
		<div class="col-lg-12">
			<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">Shipping Address &nbsp;&nbsp;&nbsp;&nbsp; <input name="isBilling" id="isBilling"
				value="" type="checkbox" onclick="copyBillingAddress();" /> Same as billing address &nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:showShippingAddress();" id="showShAddr"
				style="font-size: 13px; font-family: arial;" />View/Change Shipping Address</a> </header>
			<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Address Line 1:</label><span id=shAddressLine1Div></span> <input type="hidden" name="shAddressLine1" id="shAddressLine1" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Address Line 2:</label><span id=shAddressLine2Div></span> <input type="hidden" name="shAddressLine2" id="shAddressLine2" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Phone:</label><span id=shPhoneDiv></span> <input type="hidden" name="shPhone" id="shPhone" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Country:</label><span id=shCountryDiv></span> <input type="hidden" name="shCountry" id="shCountry" value="" />
					</div>
				</div>
				<div class="form-group">

					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">State:</label><span id=shStateDiv></span> <input type="hidden" name="shState" id="shState" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">City:</label><span id=shCityDiv></span> <input type="hidden" name="shCity" id="shCity" value="" />
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">ZipCode:</label><span id=shZipCodeDiv></span> <input type="hidden" name="shZipCode" id="shZipCode" value="" />
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> ${eventDtl.eventName},
			${eventDtl.eventDateStr},${eventDtl.eventTimeStr},${eventDtl.building} </header>
			<div class="panel-body">

				<input type="hidden" name="action" id="action" value="action" /> <input type="hidden" name="eventId" id="eventId" value="${eventDtl.eventId}" /> <input type="hidden" name="eventName"
					id="eventName" value="${eventDtl.eventName}" /> <input type="hidden" name="eventDate" id="eventDate" value="${eventDtl.eventDateStr}" /> <input type="hidden" name="eventTime" id="eventTime"
					value="${eventDtl.eventTimeStr}" /> <input type="hidden" name="venueId" id="venueId" value="${eventDtl.venueId}" /> <input type="hidden" name="section" id="section" value="${catTix.section}" />
				<input type="hidden" name="row" id="row" value="${catTix.row}" /> <input type="hidden" name="ticketPrice" id="ticketPrice" value="${catTix.price}" /> <input type="hidden" name="loyalFanPrice"
					id="loyalFanPrice" value="${catTix.loyalFanPrice}" /> <input type="hidden" name="productType" id="productType" value="${producttype}" /> <input type="hidden" name="shippingMethodId"
					id="shippingMethodId" value="${shippingMethodId}" /> <input type="hidden" name="customerId" id="customerIdDiv" value="" /> 
					<input type="hidden" name="ticketId" id="ticketId"
					value="${ticketGroupId}" /> <input type="hidden" name="bAddressId" id="bAddressId" value="" /> <input type="hidden" name="sAddressId" id="sAddressId" value="" /> <input type="hidden"
					name="shippingIsSameAsBilling" id="shippingIsSameAsBilling" value="No" /> <input type="hidden" name="ticketId" id="ticketId" value="" />

				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Section:</label> <span style="font-size: 12px;">${catTix.section}</span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Row:</label> <span>${catTix.row}</span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Available Seat Low :</label> <span>${catTix.seatLow}</span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Available Seat High : </label><span>${catTix.seatHigh}</span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Quantity :</label> <span>${catTix.quantity}</span> <input type="text" class="form-control" id="ticketCount" name="ticketCount"
							value="${catTix.quantity}" onblur="soldPriceChange();">
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Price:</label><span id="ticketPriceText">${catTix.price}</span>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Tax Amount :</label> <span>${taxAmount}</span> <input type="hidden" id="taxAmount" name="taxAmount" value="${taxAmount}">
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Sold Price Per Ticket :</label> <input type="text" class="form-control" id="soldPrice" name="soldPrice" value="${catTix.price}"
							onblur="soldPriceChange();">
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> Payment Details </header>
			<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Active Reward Points : </label> <span id=activeRewardPoints></span> <input type="hidden" id="customerRewardPoints" name="customerRewardPoints" />
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Wallet Credit : </label> <span id=walletCreditText></span> <input type="hidden" id="walletCredit" name="walletCredit" />
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Earned Reward Points : </label> <span id=earnedRewardPoints></span> <input type="hidden" id="earnRewardPoint" name="earnRewardPoint" />
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Loyal Fan Price Applicable : </label> <span id=loyalFanPriceText></span> <input type="hidden" id="isLoyalFanPrice" name="isLoyalFanPrice" />
				</div>
		</div>
				<div class="form-group">
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Payment Method : </label> <select id="paymentMethod" name="paymentMethod" class="form-control input-sm m-bot15"
							onchange="changePaymentMethod()">
							<option value="CREDITCARD">Credit Card</option>
							<option value="REWARDPOINTS" <c:if test="${paymentType eq 'REWARDPOINTS'}">selected</c:if>>Reward Points</option>
							<option value="WALLET" <c:if test="${paymentType eq 'WALLET'}">selected</c:if>>Wallet</option>
							<option value="WALLET_CREDITCARD" <c:if test="${paymentType eq 'WALLET_CREDITCARD'}">selected</c:if>>Wallet + Credit Card</option>
							<option value="PARTIAL_REWARD_CC" <c:if test="${paymentType eq 'PARTIAL_REWARD_CC'}">selected</c:if>>Partial Reward + Credit Card</option>
							<option value="PARTIAL_REWARD_WALLET" <c:if test="${paymentType eq 'PARTIAL_REWARD_WALLET'}">selected</c:if>>Partial Reward + Wallet</option>
							<option value="PARTIAL_REWARD_WALLET_CC" <c:if test="${paymentType eq 'PARTIAL_REWARD_WALLET_CC'}">selected</c:if>>Partial Reward + Wallet + Credit Card</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Referral Code :</label> <input type="text" class="form-control" id="referralCode" name="referralCode" onblur="doValidateReferralCode();">
					</div>
					<div class="col-sm-3">
						<label style="font-size: 13px; font-weight: bold;">Total Payable Amount : </label> <span id=totalPayableAmount></span> <input type="hidden" id="payableAmount" name="payableAmount" />
					</div>
				</div>

				<div class="form-group" id="creditCardDiv">
					<div class="col-sm-3" class="cardInfo">
						<label style="font-size: 13px; font-weight: bold;">Available Cards : </label> <select id="custCards" name="custCards" class="form-control input-sm m-bot15">
							<option value="0">Get New Card</option>
						</select> <input type="hidden" id="sTempToken" name="sTempToken" /> <input type="hidden" id="sBySavedCard" name="sBySavedCard" /> <input type="hidden" id="sSavedCardId" name="sSavedCardId" />
					</div>

					<div class="col-sm-3" class="cardInfo">
						<label style="font-size: 13px; font-weight: bold;">Card Number :</label> <input type="text" class="form-control" id="cardNumber" name="cardNumber" data-stripe="number">
					</div>

					<div class="col-sm-3" class="cardInfo">
						<label style="font-size: 13px; font-weight: bold;">Expiry Month :</label> <select id="expMonth" name="expMonth" class="form-control input-sm m-bot15">
							<c:forEach begin="01" end="12" var="expirtMonth">
								<option value="${expirtMonth}">${expirtMonth}</option>
							</c:forEach>
						</select>
					</div>

					<div class="col-sm-3" class="cardInfo">
						<label style="font-size: 13px; font-weight: bold;">Expiry Year :</label>
						<c:set value="true" var="yearFlag"></c:set>
						<c:set value="${currentYear + 1 }" var="currentYearInc"></c:set>
						<select id="expYear" name="expYear" class="form-control input-sm m-bot15">
							<c:forEach begin="1" end="20">

								<c:choose>
									<c:when test="${yearFlag == true}">
										<option value="${currentYear}">${currentYear}</option>
										<c:set value="false" var="yearFlag"></c:set>
									</c:when>
									<c:otherwise>
										<option value="${currentYearInc}">
											${currentYearInc }
											<c:set value="${currentYearInc + 1 }" var="currentYearInc"></c:set>
										</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-sm-3" class="cardInfo">
						<label style="font-size: 13px; font-weight: bold;">Name in the card :</label> <input type="text" class="form-control" id="name" name="name" data-stripe="name">
					</div>
					<div class="col-sm-3" class="cardInfo">
						<label style="font-size: 13px; font-weight: bold;">Cvv :</label> <input type="text" class="form-control" id="cvv" name="cvv" data-stripe="cvc">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3" id="cardAmountDiv" style="display: none">
						<label style="font-size: 13px; font-weight: bold;">Card Amount :</label> <input type="text" class="form-control" id="cardAmount" name="cardAmount">
					</div>
					<div class="col-sm-3" id="rewardPointsDiv" style="display: none">
						<label style="font-size: 13px; font-weight: bold;">Reward Points :</label> <input type="text" class="form-control" id="rewardPoints" name="rewardPoints">
					</div>
					<div class="col-sm-3" id="walletAmountDiv" style="display: none">
						<label style="font-size: 13px; font-weight: bold;">Wallet Credit Amount :</label> <input type="text" class="form-control" id="walletAmount" name="walletAmount">
					</div>
					<input type="hidden" size="12" id="stripeExp" data-stripe="exp"> <input type="hidden" id="cvvMaxDigit" name="cvvMaxDigit" />
					<input type="hidden"  id="cvvMaxDigit" name="cvvMaxDigit" />
				</div>
				<div class="form-group">

					<div id="POAction" class="col-sm-6">
						<button type="button" id="createInvoice" style="display: none;" onclick="doSaveInvoice();" class="btn btn-primary btn-sm">Save</button>
						<button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
					</div>
				</div>
</form>
</div>
</section>
</div>
</div>





<!-- Show the selected customer info -->
<br />
<br />
<br />

<!-- slickgrid related scripts -->
<script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<script>
	var pagingInfo;
	var customerGrid;
	var customerDataView;
	var customerData = [];
	var customerGridSearchString = '';
	var columnFilters = {};
	var customerColumns = [ {
		id : "customerType",
		name : "Customer Type",
		field : "customerType",
		sortable : true,
		width : 50
	}, {
		id : "customerName",
		name : "First Name",
		field : "customerName",
		sortable : true,
		width : 100
	}, {
		id : "lastName",
		name : "Last Name",
		field : "lastName",
		sortable : true,
		width : 100
	}, {
		id : "email",
		name : "Email",
		field : "email",
		sortable : true,
		width : 140
	}, {
		id : "productType",
		name : "Product Type",
		field : "productType",
		sortable : true,
		width : 120
	}, {
		id : "client",
		name : "Client",
		field : "client",
		sortable : true
	}, {
		id : "broker",
		name : "Broker",
		field : "broker",
		sortable : true
	}, {
		id : "street1",
		name : "Street1",
		field : "street1",
		sortable : true
	}, {
		id : "street2",
		name : "Street2",
		field : "street2",
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		sortable : true
	}, {
		id : "country",
		name : "Country",
		field : "country",
		sortable : true
	}, {
		id : "zip",
		name : "Zip",
		field : "zip",
		sortable : true
	}, {
		id : "phone",
		name : "Phone",
		field : "phone",
		width : 100,
		sortable : true
	}, {
		id : "editCol",
		field : "editCol",
		name : "Edit Customer",
		width : 10,
		formatter : editFormatter
	} ];

	var customerOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var customerGridSortcol = "customerName";
	var customerGridSortdir = 1;
	var percentCompleteThreshold = 0;

	/*
	function customerGridFilter(item, args) {
		var x= item["customerName"];
		if (args.customerGridSearchString  != ""
				&& x.indexOf(args.customerGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.customerGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	 */
	function editFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.customerId +"'/>";
		return button;
	}
	$('.editClickableImage').live('click', function() {
		var me = $(this), id = me.attr('id');
		var delFlag = editCustomer(id);
	});

	function customerGridComparer(a, b) {
		var x = a[customerGridSortcol], y = b[customerGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	/*
	function customerGridToggleFilterRow() {
		customerGrid.setTopPanelVisibility(!customerGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#customer_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	 */
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(pagingInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(pagingInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(pagingInfo.pageNum) - 1;
		}
		getCustomerGridData(pageNo);
	}

	function getCustomerGridData(pageNo) {
		$('#addTicketDiv').show();
		var searchValue = $("#searchValue").val();
		/*if(searchValue== null || searchValue == '') {
			jAlert('Enter valid key to searh Customers');
			return false;
		}*/

		$
				.ajax({
					url : "${pageContext.request.contextPath}/Client/GetCustomers",
					type : "post",
					data : $("#customerSearch").serialize() + "&pageNo="
							+ pageNo + "&headerFilter="
							+ customerGridSearchString,
					dataType : "json",
					success : function(res) {
						var jsonData = JSON.parse(JSON.stringify(res));
						pagingInfo = jsonData.pagingInfo;
						refreshcustomerGridValues(jsonData.customers);
					},
					error : function(error) {
						jAlert(
								"Your login session is expired please refresh page and login again.",
								"Error");
						return false;
					}
				});
	}
	function refreshcustomerGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		customerData = [];
		for ( var i = 0; i < jsonData.length; i++) {
			var data = jsonData[i];
			var d = (customerData[i] = {});
			d["id"] = i;
			d["customerId"] = data.customerId;
			d["bAddressId"] = data.billingAddressId;
			d["firstName"] = data.customerName;
			d["lastName"] = data.lastName;
			d["customerType"] = data.customerType;
			d["customerName"] = data.customerName;
			d["client"] = data.client;
			d["broker"] = data.broker;
			d["email"] = data.customerEmail;
			d["productType"] = data.productType;
			d["street1"] = data.addressLine1;
			d["street2"] = data.addressLine2;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["zip"] = data.zipCode;
			d["phone"] = data.phone;
		}

		customerDataView = new Slick.Data.DataView();
		customerGrid = new Slick.Grid("#customer_grid", customerDataView,
				customerColumns, customerOptions);
		customerGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		customerGrid.setSelectionModel(new Slick.RowSelectionModel());
		if (pagingInfo != null) {
			var customerGridPager = new Slick.Controls.Pager(customerDataView,
					customerGrid, $("#customer_pager"), pagingInfo);
		}
		var customerGridColumnpicker = new Slick.Controls.ColumnPicker(
				customerColumns, customerGrid, customerOptions);

		// move the filter panel defined in a hidden div into customerGrid top panel
		//$("#customer_inlineFilterPanel").appendTo(customerGrid.getTopPanel()).show();

		customerGrid.onSort.subscribe(function(e, args) {
			customerGridSortdir = args.sortAsc ? 1 : -1;
			customerGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				customerDataView.fastSort(customerGridSortcol, args.sortAsc);
			} else {
				customerDataView.sort(customerGridComparer, args.sortAsc);
			}
		});
		// wire up model customers to drive the customerGrid
		customerDataView.onRowCountChanged.subscribe(function(e, args) {
			customerGrid.updateRowCount();
			customerGrid.render();
		});
		customerDataView.onRowsChanged.subscribe(function(e, args) {
			customerGrid.invalidateRows(args.rows);
			customerGrid.render();
		});
		$(customerGrid.getHeaderRow()).delegate(
				":input",
				"keyup",
				function(e) {
					var keyCode = (e.keyCode ? e.keyCode : e.which);
					customerGridSearchString = '';
					var columnId = $(this).data("columnId");
					if (columnId != null) {
						columnFilters[columnId] = $.trim($(this).val());
						if (keyCode == 13) {
							for ( var columnId in columnFilters) {
								if (columnId !== undefined
										&& columnFilters[columnId] !== "") {
									customerGridSearchString += columnId + ":"
											+ columnFilters[columnId] + ",";
								}
							}
							getCustomerGridData(0);
						}
					}

				});
		customerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if (args.column.id != 'editCol') {
					$("<input type='text'>").data("columnId", args.column.id)
							.val(columnFilters[args.column.id]).appendTo(
									args.node);
				}
			}
		});
		customerGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#customerGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			customerGridSearchString = this.value;
			updatecustomerGridFilter();
		});
		function updatecustomerGridFilter() {
			customerDataView.setFilterArgs({
				customerGridSearchString : customerGridSearchString
			});
			customerDataView.refresh();
		}
		 */
		// initialize the model after all the customers have been hooked up
		customerDataView.beginUpdate();
		customerDataView.setItems(customerData);
		/*customerDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			customerGridSearchString : customerGridSearchString
		});
		customerDataView.setFilter(customerGridFilter);*/
		customerDataView.endUpdate();
		customerDataView.syncGridSelection(customerGrid, true);
		$("#gridContainer").resizable();

		var customerRowIndex;
		customerGrid.onSelectedRowsChanged
				.subscribe(function() {
					var tempcustomerRowIndex = customerGrid
							.getSelectedRows([ 0 ])[0];
					if (tempcustomerRowIndex != customerRowIndex) {
						customerRowIndex = tempcustomerRowIndex;
						resetCustomerInfo();
						var customerEmail = customerGrid
								.getDataItem(customerRowIndex).email;
						if (customerEmail == null || customerEmail == '') {
							jAlert("Invoice cannot be created, Selected customer does not have email address");
							$('#createInvoice').hide();
							return;
						}
						if (!getCustomerInfoForInvoice(tempcustomerRowIndex)) {
							jAlert("Invoice can not be created for selected Customer, Please add customer address.");
							$('#createInvoice').hide();
						} else {
							getCustomerShippingAddressDetails(
									tempcustomerRowIndex, '');
							getCustomerSavedCards(tempcustomerRowIndex);
							$('#createInvoice').show();
						}
						if($("#isBilling").prop('checked') == true){
					$("#isBilling").prop('checked', false);
				}
					}
				});
		$("div#divLoading").removeClass('show');
	}

	function editCustomer(custId) {
		if (custId == null) {
			jAlert("There is something wrong.Please try again.");
		} else {
			var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="
					+ custId;
			popupCenter(url, "Edit Customer Details", "1000", "800");
		}
	}

	function resetCustomerInfo() {
		$("#customerNameDiv").empty();
		$("#emailDiv").empty();
		$("#phoneDiv").empty();
		$("#addressLineDvi").empty();
		$("#countryDiv").empty();
		$("#stateDiv").empty();
		$("#cityDiv").empty();
		$("#zipCodeDiv").empty();

		$('#shAddressLine1Div').empty();
		$('#shAddressLine2Div').empty();
		$('#shPhoneDiv').empty();
		$('#shCountryDiv').empty();
		$('#shStateDiv').empty();
		$('#shCityDiv').empty();
		$('#shZipCodeDiv').empty();
	}

	//Get selected event on button click
	function getCustomerInfoForInvoice(customerGridIndex) {
		var address = customerGrid.getDataItem(customerGridIndex).street1;
		var strret2 = customerGrid.getDataItem(customerGridIndex).street2;
		if (address == undefined) {
			address = '';
		}
		if (strret2 == undefined) {
			strret2 = '';
		}
		$("#customerIdDiv").val(
				customerGrid.getDataItem(customerGridIndex).customerId);
		$("#customerNameDiv").text(
				customerGrid.getDataItem(customerGridIndex).customerName);
		$("#emailDiv").text(customerGrid.getDataItem(customerGridIndex).email);
		$("#phoneDiv").text(customerGrid.getDataItem(customerGridIndex).phone);
		$("#bAddressId").val(
				customerGrid.getDataItem(customerGridIndex).bAddressId);
		$("#blFirstName").val(
				customerGrid.getDataItem(customerGridIndex).firstName);
		$("#blLastName").val(
				customerGrid.getDataItem(customerGridIndex).lastName);
		$("#blEmail").val(customerGrid.getDataItem(customerGridIndex).email);
		$("#phone").val(customerGrid.getDataItem(customerGridIndex).phone);
		if (strret2 != null && strret2 != '') {
			address = address + ',' + strret2;
		}
		$("#addressLineDvi").text(address);
		$("#addressLine1").val(address);
		$("#addressLine2").val(strret2);
		$("#countryDiv").text(
				customerGrid.getDataItem(customerGridIndex).country);
		$("#country").val(customerGrid.getDataItem(customerGridIndex).country);
		$("#stateDiv").text(customerGrid.getDataItem(customerGridIndex).state);
		$("#state").val(customerGrid.getDataItem(customerGridIndex).state);
		$("#city").val(customerGrid.getDataItem(customerGridIndex).city);
		$("#cityDiv").text(customerGrid.getDataItem(customerGridIndex).city);
		$("#zipCodeDiv").text(customerGrid.getDataItem(customerGridIndex).zip);
		$("#zipCode").val(customerGrid.getDataItem(customerGridIndex).zip);
		if ((address == null || address == '')
				&& (strret2 == null || strret2 == '')) {
			return false;
		} else {
			return true;
		}

	}

	var shippingAddressData = "";
	//Get customer shipping address details
	function getCustomerShippingAddressDetails(selectedCustRowId,
			shippingAddressId) {
		var custId = customerGrid.getDataItem(selectedCustRowId).customerId;
		$
				.ajax({
					url : "${pageContext.request.contextPath}/getShippingAddressDetails",
					type : "post",
					dataType : "json",
					data : {
						customerId : custId,
						shippingId : shippingAddressId
					},
					success : function(res) {
						shippingAddressData = JSON.parse(JSON.stringify(res));
						if (shippingAddressData != ''
								&& shippingAddressData.length > 0) {
							$.each(shippingAddressData,
									function(index, element) {
										$("#sAddressId").val(element.shId);
										$('#shAddressLine1Div').append(
												element.street1);
										$('#shAddressLine1').val(
												element.street1);
										$('#shAddressLine2').val(
												element.street2);
										$('#shFirstName')
												.val(element.firstName);
										$('#shLastName').val(element.lastName);
										$('#shCity').val(element.city);
										$('#shState').val(element.state);
										$('#shCountry').val(element.country);
										$('#shZipCode').val(element.zipCode);
										$('#shPhone').val(element.phone);
										$('#shAddressLine2Div').append(
												element.street2);
										$('#shPhoneDiv').append(element.phone);
										$('#shCountryDiv').append(
												element.country);
										$('#shStateDiv').append(element.state);
										$('#shCityDiv').append(element.city);
										$('#shZipCodeDiv').append(
												element.zipCode);
										$('#createInvoice').show();
									});
						} else {
							$('#sAddressId').val('');
							/* jAlert("Invoice can not be created for selected Customer, please add shipping address.");
							$('#createInvoice').hide(); */
						}

					},
					error : function(error) {
						jAlert(
								"Your login session is expired please refresh page and login again.",
								"Error");
						/* $('#createInvoice').hide();
						return false; */
					}
				});
	}

	//Get customer saved card details
	function getCustomerSavedCards(selectedCustRowId) {
		var custId = customerGrid.getDataItem(selectedCustRowId).customerId;
		$
				.ajax({
					url : "${pageContext.request.contextPath}/GetSavedCardsRewardPointsAndLoyaFan",
					type : "post",
					dataType : "json",
					data : {
						customerId : custId,
						eventId : $('#eventId').val()
					},

					success : function(res) {
						var respData = JSON.parse(JSON.stringify(res));
						var savedCardData = respData.savedCards;
						if(savedCardData!='' && savedCardData.length >0){
							 var select=document.getElementById("custCards");
							 document.getElementById("custCards").innerHTML='';
							 var opt=document.createElement('option');
							 opt.value=0;
							 opt.innerHTML="Get New Card";
							 select.appendChild(opt);
							 $.each(savedCardData, function(index, element) {
								 var cardId = element.cardId;
								 var cardType = element.cardType;
								 var cardNo = element.cardNo;
								 opt=document.createElement('option');
								 opt.value=cardId;
								 opt.innerHTML=cardType+"-"+cardNo;
								 select.appendChild(opt);	
							});
						}else{
							 var select=document.getElementById("custCards");
							 document.getElementById("custCards").innerHTML='';
							 var opt=document.createElement('option');
							 opt.value=0;
							 opt.innerHTML="Get New Card";
							 select.appendChild(opt);
						}
						$('#activeRewardPoints').text(parseFloat(respData.rewardPoints).toFixed(2));
						$('#customerRewardPoints').val(parseFloat(respData.rewardPoints).toFixed(2));
						$('#walletCreditText').text(parseFloat(respData.customerCredit).toFixed(2));
						$('#walletCredit').val(parseFloat(respData.customerCredit).toFixed(2));
						if(respData.superFanArtistId ==true || respData.superFanArtistId =='true'){
							$('#loyalFanPriceText').text("Yes");
							$('#isLoyalFanPrice').val("Yes");
							
							$('#soldPrice').val('${catTix.loyalFanPrice}');
							$('#ticketPriceText').text('${catTix.loyalFanPrice}');
							
							var soldPrice = parseFloat($('#soldPrice').val());
							var ticketCount = parseInt($('#ticketCount').val());
							var taxAmt = parseFloat($('#taxAmount').val());
							var payableAmount = parseFloat((soldPrice * ticketCount) + (taxAmt*ticketCount)).toFixed(2);
							
							$('#totalPayableAmount').text(payableAmount);
							$('#payableAmount').val(payableAmount);
						}else{
							$('#loyalFanPriceText').text("No");
							$('#isLoyalFanPrice').val("No");
							
							$('#soldPrice').val('${catTix.price}');
							$('#ticketPriceText').text('${catTix.price}');
							
							var soldPrice = parseFloat($('#soldPrice').val());
							var ticketCount = parseInt($('#ticketCount').val());
							var taxAmt = parseFloat($('#taxAmount').val());
							
							var payableAmount = parseFloat((soldPrice * ticketCount) +(taxAmt*ticketCount)).toFixed(2);
							
							$('#totalPayableAmount').text(payableAmount);
							$('#payableAmount').val(payableAmount);
						}
					},
					error : function(error) {

						var select = document.getElementById("custCards");
						document.getElementById("custCards").innerHTML = '';

						var opt = document.createElement('option');
						opt.value = 0;
						opt.innerHTML = "Get New Card";
						select.appendChild(opt);
					}
				});
	}

	getCustomerGridData(0);

	function addCustomerMsg() {
		jAlert("Customer Added successfully.");
	}

	function changeShippingAddress(addressId) {
		selectedCustRowId = customerGrid.getSelectedRows([ 0 ])[0];
		$('#shAddressLine1Div').empty();
		$('#shAddressLine2Div').empty();
		$('#shPhoneDiv').empty();
		$('#shCountryDiv').empty();
		$('#shStateDiv').empty();
		$('#shCityDiv').empty();
		$('#shZipCodeDiv').empty();
		if($("#isBilling").prop('checked') == true){
			$("#isBilling").prop('checked', false);
		}
		getCustomerShippingAddressDetails(selectedCustRowId, addressId);

	}

	function showShippingAddress() {
		var custId = document.getElementById('customerIdDiv').value;
		if (custId == null) {
			jAlert("There is something wrong.Please try again.");
		} else {
			var createPOUrl = "${pageContext.request.contextPath}/Invoice/getShippingAddress?custId="
					+ custId;
			popupCenter(createPOUrl, 'Shipping Addresses', '800', '500');
		}
	}

	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}

	//Copy billing address
	function copyBillingAddress() {
		if ($("#isBilling").prop('checked') == true) {
			var custRowIndex = customerGrid.getSelectedRows([0])[0];
			  if(custRowIndex == null){
				  jAlert("Please select Customer first.");
				  $("#isBilling").prop('checked', false);
			  }
			var bAddressId = $("#bAddressId").val();
			$("#sAddressId").val(bAddressId);
			$("#shippingIsSameAsBilling").val('Yes');
			$('#createInvoice').show();

			// $("#sAddressId").val($("#bAddressId").val());
			$('#shAddressLine1Div').text($("#addressLine1").val());
			$('#shAddressLine1').val($("#addressLine1").val());
			$('#shAddressLine2').val($("#addressLine2").val());
			$('#shFirstName').val($("#blFirstName").val());
			$('#shLastName').val($("#blLastName").val());
			$('#shCity').val($("#city").val());
			$('#shState').val($("#state").val());
			$('#shCountry').val($("#country").val());
			$('#shZipCode').val($("#zipCode").val());
			$('#shPhone').val($("#phone").val());
			$('#shAddressLine2Div').text($("#addressLine2").val());
			$('#shPhoneDiv').text($("#phone").val());
			$('#shCountryDiv').text($("#country").val());
			$('#shStateDiv').text($("#state").val());
			$('#shCityDiv').text($("#city").val());
			$('#shZipCodeDiv').text($("#zipCode").val());
			$('#createInvoice').show();

		} else {
			$("#sAddressId").val('');
			$("#shippingIsSameAsBilling").val('No');

			$('#shAddressLine1Div').text('');
			$('#shAddressLine1').val('');
			$('#shAddressLine2').val('');
			$('#shFirstName').val('');
			$('#shLastName').val('');
			$('#shCity').val('');
			$('#shState').val('');
			$('#shCountry').val('');
			$('#shZipCode').val('');
			$('#shPhone').val('');
			$('#shAddressLine2Div').text('');
			$('#shPhoneDiv').text('');
			$('#shCountryDiv').text('');
			$('#shStateDiv').text('');
			$('#shCityDiv').text('');
			$('#shZipCodeDiv').text('');

			if (shippingAddressData != '' && shippingAddressData.length > 0) {
				$.each(shippingAddressData, function(index, element) {
					$("#sAddressId").val(element.shId);
					$('#shAddressLine1Div').append(element.street1);
					$('#shAddressLine1').val(element.street1);
					$('#shAddressLine2').val(element.street2);
					$('#shFirstName').val(element.firstName);
					$('#shLastName').val(element.lastName);
					$('#shCity').val(element.city);
					$('#shState').val(element.state);
					$('#shCountry').val(element.country);
					$('#shZipCode').val(element.zipCode);
					$('#shPhone').val(element.phone);
					$('#shAddressLine2Div').append(element.street2);
					$('#shPhoneDiv').append(element.phone);
					$('#shCountryDiv').append(element.country);
					$('#shStateDiv').append(element.state);
					$('#shCityDiv').append(element.city);
					$('#shZipCodeDiv').append(element.zipCode);
					$('#createInvoice').show();
				});
			}
		}
	}
</script>