<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Tracking By Date</title>
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.columnpicker1.css"
	type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
	src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/app/getCityStateCountry.js"></script>
<style>
.cell-title {
	font-weight: bold;
}

.slick-viewport {
	overflow-x: hidden !important;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

.slick-viewport {
	overflow-x: hidden !important;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$("div#divLoading").addClass('show');
	$('#fromDate').datepicker({
		format: "mm/dd/yyyy",
        endDate: "${endDate}",
        autoclose : true,
		orientation: "bottom"
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
        endDate: "${endDate}",
        autoclose : true,
		orientation: "bottom"
    });
	setTimeout(function(){ 
		//$('#pager> div').append("<span class='slick-pager-status'> - Total <b>${totalCount}</b> Website Tracking</span>");
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserManualFedexPreference()'>");
		
	}, 500);
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  manualFedexGrid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchTrackingBtn').click();
		    return false;  
		  }
	});
	
});
	
function saveUserManualFedexPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = manualFedexGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('manualfedexgrid',colStr);
}

function doTrackingDateValidations(){		
		$("#webServiceTracking").attr('action','${pageContext.request.contextPath}/Tracking/Home/');
		//$("#webServiceTracking").submit();
		websiteTrackingSearchString='';
		columnFilters = {};
		getWebServiceTrackingGridData(0);
}

function resetFilters(){
	manualFedexSearchString='';
	columnFilters = {};
	getManualFedexGridData(0);
}

function exportToExcel(){
	var appendData = "headerFilter="+manualFedexSearchString;
    //var url = "${pageContext.request.contextPath}/Admin/ManualFedexExportToExcel?"+appendData;
    var url = apiServerUrl+"ManualFedexExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}
</script>

<style>
input {
	color: black !important;
}
</style>
</head>
<body>

	<div class="row">
		<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
		<div class="col-lg-12">
			<h3 class="page-header">
				<i class="fa fa-laptop"></i> ADMIN
			</h3>
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
				<li><i class="fa fa-laptop"></i>Manual Fedex</li>
			</ol>

		</div>
	</div>


	<div style="position: relative">
		<div style="width: 100%;">
			<div class="full-width filters-div">

				
				<button type="button" class="btn btn-primary" onclick="createManualFedex()">Create Manual Fedex Label</button>				
				<button type="button" class="btn btn-primary" onclick="deleteFedexLabel()">Remove Manual Fedex Label</button>
			</div>
			<br />
			<br />

			<div class="table-responsive grid-table mt-20">
				<div class="grid-header full-width">
					<label>Manual Fedex Label</label>
					<div class="pull-right">
						<a href="javascript:exportToExcel()" name="Export to Excel" style="float: right; margin-right: 10px;">Export to Excel</a> 
						<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
					</div>
				</div>
				<div id="myGrid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
				<div id="pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
	</div>


<!-- popup Create Fedex Label -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#create-manual-fedex">Add New Customer</button> -->
<div id="create-manual-fedex" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Create Manual Fedex</h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Admin
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Fedex</a>
							</li>
							<li><i class="fa fa-laptop"></i>Create Manual Fedex</li>
						</ol>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						
						
						<div class="panel-body">
								<div id="manualFedex_successDiv" class="alert alert-success fade in" style="display:none;">
									<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
									<span id="manualFedex_successMsg"></span></strong>
								</div>					
								<div id="manualFedex_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
									<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
									<span id="manualFedex_errorMsg"></span></strong>
								</div>
							<section class="panel full-width mb-10">
							<form role="form" class="row" id="manualFedexForm" method="post" action="${pageContext.request.contextPath}/Admin/CreateManualFedex">
								<input type="hidden" id="manualFedex_action" value="action" name="action"  />
								
								<header class="panel-heading full-width mt-20"> From Address </header>
								<div class="tab-fields full-width mt-10">
									<div class="form-group col-sm-4 col-xs-6">
										<label>First Name <span class="required">*</span></label> 
										<input class="form-control" id="manualFedex_blFirstName" name="manualFedex_blFirstName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Last Name <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_blLastName" name="manualFedex_blLastName" type="text" />
									</div>
									
									<div class="form-group col-sm-4 col-xs-6">
										<label>Company Name <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_blCompanyName" name="manualFedex_blCompanyName" type="text" value="Reward The Fan LLC"/>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street1 <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_blStreet1" type="text" name="manualFedex_blStreet1" value="418 Clifton Ave"/>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street2</label>
										<input class="form-control" id="manualFedex_blStreet2" type="text" name="manualFedex_blStreet2" value="Suite 303"/>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Zip Code <span class="required">*</span></label>		
										<input class="form-control" id="manualFedex_blZipCode" type="text" name="manualFedex_blZipCode" value="10018" onblur="getCityStateCountry(this.value, 'body-admin-fedex-label-billing')"/>
									</div>	
									<div class="form-group col-sm-4 col-xs-6">
										<label>City <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_blCity" type="text" name="manualFedex_blCity" />
									</div> 
									<div class="form-group col-sm-4 col-xs-6">
										<label>Country <span class="required">*</span></label>		
										<select id="manualFedex_blCountryName" name="manualFedex_blCountryName" class="form-control input-sm m-bot15" onchange="loadStateCountryManualFedex(manualFedex_blCountryName,manualFedex_blStateName,'')">
											<%--<option value="-1">--select--</option>
											<c:forEach var="country" items="${countries}">
												<option value="${country.id}"><c:out value="${country.name}"/></option>
											 </c:forEach> --%><!---->
										</select>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>State <span class="required">*</span></label>
										<select id="manualFedex_blStateName" name="manualFedex_blStateName" class="form-control input-sm m-bot15" onchange="">
											<%-- <option value="-1">--select--</option>
											  <c:forEach var="state" items="${states}">
												<option value="${state.id}"><c:out value="${state.name}"/></option>
											 </c:forEach> --%><!---->
										</select>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Phone <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_blPhone" type="text" name="manualFedex_blPhone" value="800-601-6100"/>
									</div>
								</div>
								<header class="panel-heading full-width"> 
									<span style="display:inline-block; vertical-align: top;">Shipping Address</span>
									<!-- <input name="isBilling" id="manualFedex_isBilling" value="" type="checkbox" onclick = "copyBillingAddress();" style="display: inline-block; margin: 12px 3px 0 14px;"/> 
									<span style="display:inline-block; vertical-align: top;">Same as billing address</span> -->
								</header>
								<div class="tab-fields full-width mt-10">
									<div class="form-group col-sm-4 col-xs-6">
										<label>Full Name <span class="required">*</span></label> <input class="form-control"
											id="manualFedex_shCustomerName" name="manualFedex_shCustomerName" type="text" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Company Name <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_shCompanyName" name="manualFedex_shCompanyName" type="text" />
									</div>

									<div class="form-group col-sm-4 col-xs-6">
										<label>Street1<span class="required">*</span></label>
										<input class="form-control" id="manualFedex_shStreet1" type="text" name="manualFedex_shStreet1" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street2</label>
										<input class="form-control" id="manualFedex_shStreet2" type="text" name="manualFedex_shStreet2" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Zip Code <span class="required">*</span></label>		
										<input class="form-control " id="manualFedex_shZipCode" type="text" name="manualFedex_shZipCode" onblur="getCityStateCountry(this.value, 'body-admin-fedex-label-shipping')"/>
									</div>	
									<div class="form-group col-sm-4 col-xs-6">
										<label>City <span class="required">*</span></label>
										<input class="form-control " id="manualFedex_shCity" type="text" name="manualFedex_shCity" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Country <span class="required">*</span></label>		
										<select id="manualFedex_shCountryName" name="manualFedex_shCountryName" class="form-control input-sm m-bot15" onchange="loadStateCountryManualFedex(manualFedex_shCountryName,manualFedex_shStateName,'')">
											<%--<option value="-1">--select--</option>
											<c:forEach var="country" items="${countries}">
												<option value="${country.id}"><c:out value="${country.name}"/></option>
											 </c:forEach>--%><!---->
										</select>
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>State <span class="required">*</span></label>
										<select id="manualFedex_shStateName" name="manualFedex_shStateName" class="form-control input-sm m-bot15" onchange="">
											<%-- <option value="-1">--select--</option>
											  <c:forEach var="state" items="${states}">
												<option value="${state.id}"><c:out value="${state.name}"/></option>
											 </c:forEach> --%>
										</select>
										<!--<input class="form-control " id="state" type="text" name="state" /> -->
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Phone <span class="required">*</span></label>
										<input class="form-control" id="manualFedex_shPhone" type="text" name="manualFedex_shPhone" />
									</div>
								</div>								
								
								<div class="form-group col-sm-4 col-xs-6">
									<label>Service Type <span class="required">*</span></label>  
									<select id="manualFedex_serviceType" name="manualFedex_serviceType" class="form-control">
										<option value='select'>--select--</option>
										<option value="FEDEX_EXPRESS_SAVER"> FEDEX EXPRESS SAVER </option>						
										<option value="FIRST_OVERNIGHT"> FIRST OVERNIGHT </option>
										<option value="PRIORITY_OVERNIGHT"> PRIORITY OVERNIGHT </option>
										<option value="STANDARD_OVERNIGHT"> STANDARD OVERNIGHT </option>
										<option value="FEDEX_2_DAY"> FEDEX 2-DAY </option>
										<option value="GROUND_HOME_DELIVERY"> GROUND HOME DELIVERY </option>
										<option value="INTERNATIONAL_PRIORITY"> INTERNATIONAL PRIORITY </option>
									</select>
								</div>
								<div class="form-group col-sm-4 col-xs-6">
									<label>Signature Type <span class="required">*</span></label>  
									<select id="manualFedex_signatureType" name="manualFedex_signatureType" class="form-control">
										<option value="NO_SIGNATURE_REQUIRED">NO SIGNATURE REQUIRED</option>
										<option value="SERVICE_DEFAULT">SERVICE DEFAULT</option>
										<option value="INDIRECT">INDIRECT</option>
										<option value="DIRECT">DIRECT</option>
										<option value="ADULT">ADULT</option>
									</select>
								</div>									
															
							</form>
							</section>
						</div>
						
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" type="button" onclick="createManualFedexLabel();">Create Manual Fedex</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End popup Add New Customer -->

	<script>
	
	var pagingInfo;
	var manualFedexDataView;
	var manualFedexGrid;
	var manualFedexData = [];
	var manualFedexSearchString='';
	var columnFilters = {};
	var manualFedexColumnsStr = '<%=session.getAttribute("manualfedexgrid")%>';
	var manualFedexColumns =[];
	var loadManualFedexColumns = ["shCustomerName", "shCompanyName", "shAddress1", "shCity", "shStateName" ,"shCountryName", 
			"blFirstName", "blLastName", "blCompanyName", "blAddress1", "blCity", "blStateName", "blCountryName", "serviceType", 
			"trackingNo", "view", "createdDate", "createdBy"];
	var allManualFedexColumns = [ {
		id : "shCustomerName",
		name : "Customer Name",
		field : "shCustomerName",
		sortable : true
	}, {
		id : "shCompanyName",
		name : "Company Name",
		field : "shCompanyName",
		sortable : true
	}, {
		id : "shAddress1",
		name : "Street1",
		field : "shAddress1",
		sortable : true
	}, {
		id : "shAddress2",
		name : "Street2",
		field : "shAddress2",
		sortable : true
	}, {
		id : "shCity",
		name : "City",
		field : "shCity",
		sortable : true
	}, {
		id : "shStateName",
		name : "State",
		field : "shStateName",
		sortable : true
	}, {
		id : "shCountryName",
		name : "Country",
		field : "shCountryName",
		sortable : true
	}, {
		id : "shZipCode",
		name : "Zip Code",
		field : "shZipCode",
		sortable : true
	}, {
		id : "shPhone",
		name : "Phone",
		field : "shPhone",
		sortable : true
	}, {
		id : "blFirstName",
		name : "From Address First Name",
		field : "blFirstName",
		sortable : true
	}, {
		id : "blLastName",
		name : "From Address Last Name",
		field : "blLastName",
		sortable : true
	}, {
		id : "blCompanyName",
		name : "From Address Company Name",
		field : "blCompanyName",
		sortable : true
	}, {
		id : "blAddress1",
		name : "From Address Street1",
		field : "blAddress1",
		sortable : true
	}, {
		id : "blAddress2",
		name : "From Address Street2",
		field : "blAddress2",
		sortable : true
	}, {
		id : "blCity",
		name : "From Address City",
		field : "blCity",
		sortable : true
	}, {
		id : "blStateName",
		name : "From Address State",
		field : "blStateName",
		sortable : true
	}, {
		id : "blCountryName",
		name : "From Address Country",
		field : "blCountryName",
		sortable : true
	}, {
		id : "blZipCode",
		name : "From Address Zip Code",
		field : "blZipCode",
		sortable : true
	}, {
		id : "blPhone",
		name : "From Address Phone",
		field : "blPhone",
		sortable : true
	}, {
		id : "serviceType",
		name : "Service Type",
		field : "serviceType",
		sortable : true
	}, {
		id : "trackingNo",
		name : "Tracking No",
		field : "trackingNo",
		sortable : true,
		formatter : trackOrderFormatter
	}, {
		id : "view",
		name : "Fedex Label",
		field : "view",
		sortable : true,
		formatter : viewLinkFormatter
	}, {
		id : "createdDate",
		name : "Created Date",
		field : "createdDate",
		sortable : true
	}, {
		id : "createdBy",
		name : "Created By",
		field : "createdBy",
		sortable : true
	}	
    ];
	
	if(manualFedexColumnsStr!='null' && manualFedexColumnsStr!=''){
		columnOrder = manualFedexColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allManualFedexColumns.length;j++){
				if(columnWidth[0] == allManualFedexColumns[j].id){
					manualFedexColumns[i] =  allManualFedexColumns[j];
					manualFedexColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		columnOrder = loadManualFedexColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allManualFedexColumns.length;j++){
				if(columnWidth == allManualFedexColumns[j].id){
					manualFedexColumns[i] = allManualFedexColumns[j];
					manualFedexColumns[i].width=80;
					break;
				}
			}			
		}
		//manualFedexColumns = allManualFedexColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
		multiSelect: false,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "customerIpAddress";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
	var searchString = "";
	
	function viewLinkFormatter(row, cell, value, columnDef, dataContext) {
		//var trackingNo = manualFedexGrid.getDataItem(row).trackingNo;
		var fedexLabelCreated = manualFedexGrid.getDataItem(row).view;
		var link = '';
		//if (trackingNo != null && trackingNo != '') {
		if(fedexLabelCreated == 'Yes'){
			link = "<img class='editClickableImage' style='height:17px;' src='../resources/images/viewPdf.png' onclick='getFedexLabel("
					+ manualFedexGrid.getDataItem(row).id + ")'/>";
		}
		return link;
	}

	function trackOrderFormatter(row, cell, value, columnDef, dataContext) {
		var trackingNo = manualFedexGrid.getDataItem(row).trackingNo;
		var link = '';
		if (trackingNo != null && trackingNo != '') {
			link = "<a href='javascript:trackMyOrder(" + trackingNo + ")'><u>"
					+ trackingNo + "</u></a>";
		}
		return link;
	}
	/*
	function myFilter(item, args) {
		var x= item["customerIpAddress"];
		if (args.searchString  != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*		
	function toggleFilterRow() {
		manualFedexGrid.setTopPanelVisibility(!manualFedexGrid.getOptions().showTopPanel);
	}
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getManualFedexGridData(pageNo);
	}
	
	function getManualFedexGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/Admin/ManualFedex.json",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+manualFedexSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} 				
				pagingInfo = jsonData.pagingInfo;
				refreshManualFedexGridValues(jsonData.manualFedexList);
				clearAllSelections();
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserManualFedexPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	function refreshManualFedexGridValues(jsonData){
		manualFedexData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (manualFedexData[i] = {});
		
				d["id"] = data.id;
				/* d["blFirstName"] = data.blFirstName;
				d["blLastName"] = data.blLastName; */
				
				d["shCustomerName"] = data.shCustomerName;
				d["shCompanyName"] = data.shCompanyName;
				d["shAddress1"] = data.shAddress1;
				d["shAddress2"] = data.shAddress2;
				d["shCity"] = data.shCity;
				d["shStateName"] = data.shStateName;
				d["shCountryName"] = data.shCountryName;
				d["shZipCode"] = data.shZipCode;
				d["shPhone"] = data.shPhone;
				d["blFirstName"] = data.blFirstName;
				d["blLastName"] = data.blLastName;
				d["blCompanyName"] = data.blCompanyName;
				d["blAddress1"] = data.blAddress1;
				d["blAddress2"] = data.blAddress2;
				d["blCity"] = data.blCity;
				d["blStateName"] = data.blStateName;
				d["blCountryName"] = data.blCountryName;
				d["blZipCode"] = data.blZipCode;
				d["blPhone"] = data.blPhone;
				d["serviceType"] = data.serviceType;
				d["trackingNo"] = data.trackingNo;
				d["view"] = data.fedexLabelCreated;
				d["createdDate"] = data.createdDate;
				d["createdBy"] = data.createdBy;
			}
		}

		manualFedexDataView = new Slick.Data.DataView();
		manualFedexGrid = new Slick.Grid("#myGrid", manualFedexDataView, manualFedexColumns, options);
		manualFedexGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		var cols = manualFedexGrid.getColumns();
		var colTest = [];
		for ( var c = 0; c < cols.length; c++) {
			//if(cols[c].name!='Title' && cols[c].name!='Start') {
			colTest.push(cols[c]);
			// }  
			manualFedexGrid.setColumns(colTest);
		}
		manualFedexGrid.invalidate();
		manualFedexGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo != null){
			var pager = new Slick.Controls.Pager(manualFedexDataView, manualFedexGrid, $("#pager"),pagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(allManualFedexColumns, manualFedexGrid,options);
		// move the filter panel defined in a hidden div into manualFedexGrid top panel
		//$("#inlineFilterPanel").appendTo(manualFedexGrid.getTopPanel()).show();

		manualFedexGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < manualFedexDataView.getLength(); i++) {
				rows.push(i);
			}
			manualFedexGrid.setSelectedRows(rows);
			e.preventDefault();
		});
		manualFedexGrid.onSort.subscribe(function(e, args) {
			sortdir = args.sortAsc ? 1 : -1;
			sortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				manualFedexDataView.fastSort(sortcol, args.sortAsc);
			} else {
				manualFedexDataView.sort(comparer, args.sortAsc);
			}
		});
		// wire up model events to drive the manualFedexGrid
		manualFedexDataView.onRowCountChanged.subscribe(function(e, args) {
			manualFedexGrid.updateRowCount();
			manualFedexGrid.render();
		});
		manualFedexDataView.onRowsChanged.subscribe(function(e, args) {
			manualFedexGrid.invalidateRows(args.rows);
			manualFedexGrid.render();
		});
		$(manualFedexGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			manualFedexSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  manualFedexSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getManualFedexGridData(0);
				}
			  }
			  
		});
		manualFedexGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
			
		});
		manualFedexGrid.init();
			
		//var h_runfilters = null;
		// wire up the slider to apply the filter to the model
		/*$("#pcSlider,#pcSlider2").slider({
		  "range": "min",
		  "slide": function (event, ui) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			if (percentCompleteThreshold != ui.value) {
			  window.clearTimeout(h_runfilters);
			  h_runfilters = window.setTimeout(updateFilter, 10);
			  percentCompleteThreshold = ui.value;
			}
		  }
		});*/
		/*
		// wire up the search textbox to apply the filter to the model
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			manualFedexDataView.setFilterArgs({
				searchString : searchString
			});
			manualFedexDataView.refresh();
		}*/
		manualFedexDataView.beginUpdate();
		manualFedexDataView.setItems(manualFedexData);
		/*manualFedexDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		manualFedexDataView.setFilter(myFilter);*/
		manualFedexDataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		manualFedexDataView.syncGridSelection(manualFedexGrid, true);
		$("#gridContainer").resizable();
		manualFedexGrid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}
	
	var countryList = "";
	function getFedexLabel(id){
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/CheckFedexLabelGenerated",
			type : "get",
			data : "id="+ id,
			dataType: "json",
			success : function(res){
				if(res.msg=='OK'){
					 //var url = "${pageContext.request.contextPath}/Accounting/GetFedexLabelPdf?id="+id;
					 var url = apiServerUrl+"GetFedexLabelPdf?id="+id;
					 $('#download-frame').attr('src', url);
				}else{
					jAlert("Fedex Label not found, Please Generate fedex Label.");
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});		
	}
	
	function trackMyOrder(trackingNo) {
		var url = "https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber="
				+ trackingNo;
		popupCenter(url, "Fedex Tracking", "1100", "900");		
	}
		
	function createManualFedex(){
		$("#manualFedex_blFirstName").val('');
		$("#manualFedex_blLastName").val('');
		$("#manualFedex_blCompanyName").val('Reward The Fan LLC');
		$("#manualFedex_blStreet1").val('10 Times Square');
		$("#manualFedex_blStreet2").val('3rd Floor');		
		$("#manualFedex_blCity").val('New York');			
		$("#manualFedex_blZipCode").val('10018');
		$("#manualFedex_blPhone").val('800-601-6100');
		getCityStateCountry('10018', 'body-admin-fedex-label-billing');
		//updateStateCountryCombo(countryList, manualFedex_blCountryName, '');
		//loadStateCountryManualFedex(manualFedex_blCountryName,manualFedex_blStateName,'');
		$("#manualFedex_shCustomerName").val('');
		$("#manualFedex_shCompanyName").val('');
		$("#manualFedex_shStreet1").val('');
		$("#manualFedex_shStreet2").val('');		
		$("#manualFedex_shCity").val('');			
		$("#manualFedex_shZipCode").val('');
		$("#manualFedex_shPhone").val('');		
		updateStateCountryCombo(countryList, manualFedex_shCountryName, '');
		loadStateCountryManualFedex(manualFedex_shCountryName,manualFedex_shStateName,'');
		$("#manualFedex_serviceType").val('select');
		$("#manualFedex_signatureType").val('NO_SIGNATURE_REQUIRED');
		if($("#manualFedex_isBilling").prop('checked') == true){
			$("#manualFedex_isBilling").removeProp('checked');
		}
		$('#create-manual-fedex').modal('show');
	}	
	
	function loadStateCountryManualFedex(countryId, stateId, findName){
		var countryId = $(countryId).val();
		if(countryId>0){
			$.ajax({
				url : "/GetStates",
				type : "post",
				dataType:"json",
				data : "countryId="+countryId,
				success : function(res){
					var jsonData = res.states; //JSON.parse(JSON.stringify(res));
					updateStateCountryCombo(jsonData,stateId,findName);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$(stateId).empty();
			$(stateId).append("<option value='-1'>--select--</option>");
		}
	}
	
	function updateStateCountryCombo(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].id == listName){
					$(id).append("<option value="+jsonData[i].id+" selected>"+jsonData[i].name+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
				}
			}
		}
	}
	
	/* //Copy billing address
	function copyBillingAddress(){
		var flag=false;
		if($("#manualFedex_isBilling").prop('checked') == true){			
			$("#manualFedex_shCustomerName").val($("#manualFedex_blFirstName").val());
			$("#manualFedex_shCompanyName").val($("#manualFedex_blLastName").val());
			$("#manualFedex_shStreet1").val($("#manualFedex_blStreet1").val());
			$("#manualFedex_shStreet2").val($("#manualFedex_blStreet2").val());
			$("#manualFedex_shCountryName").val($("#manualFedex_blCountryName").val());
			loadStateCountryManualFedex(manualFedex_shCountryName,manualFedex_shStateName,$("#manualFedex_blStateName").val());
			$("#manualFedex_shCity").val($("#manualFedex_blCity").val());			
			$("#manualFedex_shZipCode").val($("#manualFedex_blZipCode").val());	
			$("#manualFedex_shPhone").val($("#manualFedex_blPhone").val());	
			
		}else{
			$("#manualFedex_shCustomerName").val('');
			$("#manualFedex_shCompanyName").val('');
			$("#manualFedex_shStreet1").val('');
			$("#manualFedex_shStreet2").val('');
			updateStateCountryCombo(countryList,manualFedex_shCountryName,'');
			loadStateCountryManualFedex(manualFedex_shCountryName,manualFedex_shStateName,'');
			$("#manualFedex_shCity").val('');			
			$("#manualFedex_shZipCode").val('');
			$("#manualFedex_shPhone").val('');				
		}		
	} */
	
	
	function createManualFedexLabel(){
		var serviceType = $('#manualFedex_serviceType').val();
		if($('#manualFedex_blFirstName').val()==''){
			jAlert("Billing - First Name is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blLastName').val()==''){
			jAlert("Billing - Last Name is Mandatory.","Info");
			return false;
		}
		if($('#manualFedex_blCompanyName').val()==''){
			jAlert("From Address - Company Name is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blStreet1').val()==''){
			jAlert("From Address - Street1 is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blZipCode').val()==''){
			jAlert("From Address - ZipCode is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blCity').val()==''){
			jAlert("From Address - City is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blCountryName').val()==''){
			jAlert("From Address - Country is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blStateName').val()==''){
			jAlert("From Address - State is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_blPhone').val()==''){
			jAlert("From Address - Phone is Mandatory.","Info");
			return false;
		}else if(($('#manualFedex_shCustomerName').val()=='') && ($('#manualFedex_shCompanyName').val()=='')){
			jAlert("Shipping - Full Name/Company Name is Mandatory.","Info");
			return false;
		}/*else if($('#manualFedex_shCompanyName').val()==''){
			jAlert("Shipping - Last Name is Mandatory.","Info");
			return false;
		}*/else if($('#manualFedex_shStreet1').val()==''){
			jAlert("Shipping - Street1 is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_shZipCode').val()==''){
			jAlert("Shipping Address - ZipCode is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_shCity').val()==''){
			jAlert("Shipping - City is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_shCountryName').val() < 0){
			jAlert("Shipping - Country is Mandatory.","Info");
			return false;
		}else if($('#manualFedex_shStateName').val() < 0){
			jAlert("Shipping address - State is Mandatory.","Info");
			return false;
		}else if(serviceType == 'select'){
			jAlert("Please choose service type.","Info");
			return false;
		}		
		$.ajax({
			url : "/Accounting/CreateManualFedexLabel",
			type : "post",
			dataType : "json",
			data : $("#manualFedexForm").serialize(),
			success : function(res) {
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#create-manual-fedex').modal('hide');
					getManualFedexGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				//jAlert(jsonData.msg);								
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
			});	
	}
	
	function deleteFedexLabel() {
		var fedexRowIndex = manualFedexGrid.getSelectedRows([ 0 ])[0];
		if (fedexRowIndex == null) {
			jAlert("Please select any value from grid to remove fedex label.");
		} else {
			var fedexId = manualFedexGrid.getDataItem(fedexRowIndex).id;
			var trackingNo = manualFedexGrid.getDataItem(fedexRowIndex).trackingNo;
			var fedexLabelCreated = manualFedexGrid.getDataItem(fedexRowIndex).view;
			if(fedexLabelCreated != null && fedexLabelCreated == "Yes"){
			jConfirm("Are you sure you want to delete fedex label ?","Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/Accounting/RemoveFedexLabel",
						type : "post",
						dataType : "json",
						data : "fedexId=" + fedexId,
						success : function(res) {
							var jsonData = JSON.parse(JSON.stringify(res));
							if(jsonData.status == 1){
								getManualFedexGridData(pagingInfo.pageNum);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
							//jAlert(jsonData.msg);							
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}else{
					return false;
				}
			});
			}else{
				jAlert("No FedEx label is created.");
				return;
			}
		}
	}
		
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshManualFedexGridValues(JSON.parse(JSON.stringify(${manualFedexList})));
		countryList = JSON.parse(JSON.stringify(${countries}));
		updateStateCountryCombo(countryList, manualFedex_blCountryName, '');
		updateStateCountryCombo(countryList, manualFedex_shCountryName, '');		
	});
</script>
</body>
</html>
