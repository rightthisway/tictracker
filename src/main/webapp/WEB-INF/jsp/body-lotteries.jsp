<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var j = 0;
var promoAutoGenerate;
var promoFlatDiscount;
var varSelectedList = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#contest_fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});

	$('#contest_toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	

	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(contestsGrid != null && contestsGrid != undefined){
			contestsGrid.resizeCanvas();
		}
		if(participantsGrid != null && participantsGrid != undefined){
			participantsGrid.resizeCanvas();
		}			
	});
	
	
	jq2('#artistAutocomplete').autocomplete("AutoCompleteArtistAndChildAndGrandChildCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			if(varSelectedList.indexOf(row[0]+"_"+row[1]) > 0){
				return "";
			}
			/* if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else */ if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		varSelectedList = varSelectedList +","+ row[0] + "_" + row[1];		
		if(row[0]=="ARTIST"){
			$('#artistId').val(row[1]);
			$('#artistDiv').empty();
			$('#artistDiv').append('<span class="remove">ARTIST : '+row[2]+'</span>');
			$('#artistAutocomplete').val('');
		}/* else if(row[0]=='CHILD'){
			$('#childId').val(row[1]);
			contestCategory = "<input type='hidden' name='childId_"+j+"' id='childId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=='GRAND'){
			$('#grandChildId').val(row[1]);
			contestCategory = "<input type='hidden' name='grandChildId_"+j+"' id='grandChildId_"+j+"' value="+row[1]+" />";
		} */
		
		j++;
	});
	
	/* $('body').on('click', '.remove', function () {
		$(this).parent('.block').remove();
	}); */
});

/* function removeItem(valu){
	var splitSeletedList = new Array();
	splitSeletedList = varSelectedList.split(",");
	for(var n=0; n<j; n++){
		if(valu == n){
			varSelectedList = varSelectedList.replace(splitSeletedList[n+1], " ");
		}
	}
	
	$('#artistVenue').val("");
	$('#artistCategoryName').val("");
	$('#artistId_'+valu).val("");
	$('#venueId_'+valu).val("");
	$('#parentId_'+valu).val("");
	$('#childId_'+valu).val("");
	$('#grandChildId_'+valu).val("");
} */

function resetModal(){
	$('#contest_id').val('');
	$('#contest_fromDate').val('');
	$('#contest_toDate').val('');
	$('#contest_name').val('');
	$('#artistId').val('');
	$('#artistName').val('');
	$('#artistNameStr').val('');
	$('#contest_minPurchaseAmt').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();
	$('#artistDiv').empty();
	/*$('#contestCategoryDiv').empty();	
	
	j=0;
	varSelectedList = '';*/
}


function contestSave(action){
	//var count = 0;	
		
	var fromDate = $('#contest_fromDate').val();
	var toDate = $('#contest_toDate').val();
	var contestName = $('#contest_name').val();	
	var artistId = $('#artistId').val();	
	var artistName = $('#artistName').val();	
	var minPurchaseAmount = $('#contest_minPurchaseAmt').val();
	
	if(contestName == ''){
		jAlert("Lottery Name is Mandatory.");
		return;
	}
	if(minPurchaseAmount == ''){
		jAlert("Minimum Purchase Amount is Mandatory.");
		return;
	}
	if(artistId == ''){
		jAlert("Please select artist from autocomplete dropdown");
		return;
	}
	if(artistName == ''){
		jAlert("Please enter artist display name.");
		return;
	}
	if(fromDate == '' || toDate == ''){
		jAlert("Start Date and End Date is Mandatory.");
		return;
	}
	
	var requestUrl = "${pageContext.request.contextPath}/UpdateLottery";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#contestForm').serialize()+"&action=SAVE";
	}else if(action == 'update'){
		dataString = $('#contestForm').serialize()+"&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#myModal-2').modal('hide');
				pagingInfo = jsonData.contestsPagingInfo;
				columnFilters = {};
				refreshContestsGridValues(jsonData.contestsList);
				refreshParticipantsGridValues('');
				clearAllSelections();
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Lottery</a></li>
			<li><i class="fa fa-laptop"></i>Lottery</li>
		</ol>
	</div>
</div>


<div class="full-width full-width-btn mb-20">
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-2" onclick="resetModal();">Add Lottery</button>
	<button type="button" class="btn btn-primary" onclick="editLottery();">Edit Lottery</button>
	<button type="button" class="btn btn-primary" onclick="deleteLottery();">Delete Lottery</button>
</div>
<br />
<br />
<div style="position: relative" id="artistGridDiv">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Lotteries</label>
			<div class="pull-right">
				<!--<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
				<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
			</div>
		</div>
		<div id="contests_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="contests_pager" style="width: 100%; height: 10px;"></div>

	</div>
</div>

<div class="row">
	<input type="hidden" name="contestIdStr" id="contestIdStr" />
	
</div>
<br />

<div style="position: relative" id="participantsGridDiv">
	<div class="full-width mb-20 mt-20 full-width-btn">
		<button class="btn btn-primary" id="generateBtn" type="button" onclick="generateWinner('GENERATE')">Choose Winner</button>
		<button class="btn btn-primary" id="viewBtn" type="button" onclick="generateWinner('')">View Winner</button>
	</div>
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Participants</label>
			<div class="pull-right">
				<!--<a href="javascript:participantsExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
				<a href="javascript:participantsResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
			</div>
		</div>
		<div id="participants_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
		<div id="participants_pager" style="width: 100%; height: 10px;"></div>

	</div>
</div>

<!-- View winner details  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="winnerDialog" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Lottery Winner</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Lottery Name  : </label><span id="winnerContestName"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Start Date  : </label><span id="winnerStartDate"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">End Date  : </label><span id="winnerEndDate"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Winner Name  : </label><span id="winnerCustomerName"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Winner Email  : </label><span id="winnerCustomerEmail"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Winning Email Sent : </label><span id="winnerMailSent"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Winner City Preference : </label><span id="winnerCity"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Winner Declared on  : </label><span id="winnerCustomerDate"></span>
					</div>
					<div class="form-group col-sm-12 col-xs-12">
						<label style="font-size: 13px;font-weight:bold;">Winner Declared by  : </label><span id="winnerCustomerBy"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Add/Edit Lottery -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal-2">Add Promotional Code</button> -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Lottery</h4>
			</div>
			<div class="modal-body full-width">
				<form name="contestForm" id="contestForm" method="post">
					<input type="hidden" id="contest_id" name="contestId" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Lottery Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_name" name="contestName">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Artist Display Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="artistName" name="artistName">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Artist<span class="required">*</span></label> 
							<input type="hidden" value="${artistId}" id="artistId" name="artistId" />
							<input type="text"  id="artistAutocomplete" name="artistAutocomplete" />
															 
							<input type="hidden" id="grandChildId" name="grandChildId" />
							<div id="artistDiv"><div class="block"></div></div> 
						</div>
						<%-- <div class="form-group col-sm-6 col-xs-6">
								<label>Artist/Child/GrandChild <span class="required">*</span></label> 
								<input class="form-control searchcontrol" type="text" id="contest_artistCategory" name="contestArtistCategory">								 
								<input type="hidden" value="${artistId}" id="artistId" name="artistId" />								 
								<input type="hidden" id="artistCategoryName" name="artistCategoryName" />								
								<input type="hidden" id="childId" name="childId" />
								<input type="hidden" id="grandChildId" name="grandChildId" />
								<div id="contestCategoryDiv"><div class="block"></div></div> 
							</div> --%>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Minimum Purchase Amount <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_minPurchaseAmt" name="contestMinPurchaseAmt">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Start Date <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_fromDate" name="contestFromDate">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>End Date <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_toDate" name="contestToDate">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="contestSave('save')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="contestSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Ends of Add Shipping/Other Address popup-->

<script type="text/javascript">
		
	//Contest Grid
	
	function getContestsGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/Lotteries.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+contestsSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					pagingInfo = jsonData.contestsPagingInfo;
					refreshContestsGridValues(jsonData.contestsList);
					clearAllSelections();				
				}
				refreshParticipantsGridValues('');
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+contestsSearchString;
	    var url = "${pageContext.request.contextPath}/ContestExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		contestsSearchString='';
		columnFilters = {};
		getContestsGridData(0);
		refreshParticipantsGridValues('');
	}

	/* var contestsCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var pagingInfo;
	var contestsDataView;
	var contestsGrid;
	var contestsData = [];
	var contestsGridPager;
	var contestsSearchString='';
	var columnFilters = {};
	var userContestsColumnsStr = '<%=session.getAttribute("lotterygrid")%>';

	var userContestsColumns = [];
	var allContestsColumns = [ /*contestsCheckboxSelector.getColumnDefinition(),*/
			/*{
				id : "contestId",
				name : "Contest ID",
				field : "contestId",
				width : 80,
				sortable : true
			},*/ {
				id : "contestName",
				name : "Lottery Name",
				field : "contestName",
				width : 80,
				sortable : true
			},{
				id : "artistName",
				name : "Artist Name",
				field : "artistName",
				width : 80,
				sortable : true
			}, {
				id : "minPurchaseAmount",
				name : "Min. Purchase Amount",
				field : "minPurchaseAmount",
				width : 80,
				sortable : true
			}, {
				id : "startDate",
				name : "Start Date",
				field : "startDate",
				width : 80,
				sortable : true
			}, {
				id : "endDate",
				name : "End Date",
				field : "endDate",
				width : 80,
				sortable : true
			}, {
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			}, {
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			}, {
				id : "updatedBy",
				field : "updatedBy",
				name : "ModifiedBy ",
				width : 80,
				sortable : true
			},  {
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},  {
				id : "editContest",
				field : "editContest",
				name : "Edit ",
				width : 80,
				formatter: editFormatter
			}, {
				id : "delContest",
				field : "delContest",
				name : "Delete ",
				width : 80,
				formatter:buttonFormatter
			}];

	if (userContestsColumnsStr != 'null' && userContestsColumnsStr != '') {
		columnOrder = userContestsColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestsColumns.length; j++) {
				if (columnWidth[0] == allContestsColumns[j].id) {
					userContestsColumns[i] = allContestsColumns[j];
					userContestsColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestsColumns = allContestsColumns;
	}

	function editFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.contestId +"'/>";
	    return button;
	}
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditContest(id);
	});
	
	function buttonFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.contestId +"'/>";		
		return button;
	}
	$('.delClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getDeleteContest(id);
	});
	
	var contestsOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var contestsGridSortcol = "contestId";
	var contestsGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestsGridComparer(a, b) {
		var x = a[contestsGridSortcol], y = b[contestsGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshContestsGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestsData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestsData[i] = {});
				d["id"] = i;
				d["contestId"] = data.id;
				d["contestName"] = data.name;
				d["minPurchaseAmount"] = data.minPurchaseAmt;
				d["startDate"] = data.startDate;
				d["endDate"] = data.endDate;
				d["artistName"] = data.artistName;
				d["status"] = data.status;
				d["createdBy"] = data.createdBy;
				d["createdDate"] = data.createdDate;
				d["updatedBy"] = data.updatedBy;
				//d["status"] = data.status;
			}
		}

		contestsDataView = new Slick.Data.DataView();
		contestsGrid = new Slick.Grid("#contests_grid", contestsDataView,
				userContestsColumns, contestsOptions);
		contestsGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestsGrid.setSelectionModel(new Slick.RowSelectionModel());
		//contestsGrid.registerPlugin(contestsCheckboxSelector);
		
			contestsGridPager = new Slick.Controls.Pager(contestsDataView,
					contestsGrid, $("#contests_pager"),
					pagingInfo);
		var contestsGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestsColumns, contestsGrid, contestsOptions);
					
		contestsGrid.onSort.subscribe(function(e, args) {
			contestsGridSortdir = args.sortAsc ? 1 : -1;
			contestsGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestsDataView.fastSort(contestsGridSortcol, args.sortAsc);
			} else {
				contestsDataView.sort(contestsGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestsGrid
		contestsDataView.onRowCountChanged.subscribe(function(e, args) {
			contestsGrid.updateRowCount();
			contestsGrid.render();
		});
		contestsDataView.onRowsChanged.subscribe(function(e, args) {
			contestsGrid.invalidateRows(args.rows);
			contestsGrid.render();
		});
		$(contestsGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestsSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											contestsSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getContestsGridData(0);
								}
							}

						});
		contestsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editContest' && args.column.id != 'delContest'){
					if(args.column.id == 'startDate' || args.column.id == 'endDate' || args.column.id == 'createdDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		contestsGrid.init();
		
		var contestsRowIndex = -1;
		contestsGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
			if (tempContestsRowIndex != contestsRowIndex) {
				var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
				getParticipantsGridData(contestId,0);
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		contestsDataView.beginUpdate();
		contestsDataView.setItems(contestsData);
		//contestsDataView.setFilter(filter);
		contestsDataView.endUpdate();
		contestsDataView.syncGridSelection(contestsGrid, true);
		contestsGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserContestsPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestsGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('lotterygrid', colStr);
	}
	
	function pagingControl(move, id) {
		if(id == 'contests_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getContestsGridData(pageNo);
		}else if(id == 'participants_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(participantsPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(participantsPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(participantsPagingInfo.pageNum) - 1;
			}
			var contestsId = $('#contestIdStr').val();
			getParticipantsGridData(contestsId, pageNo);
		}
	}
	
		
	//Participants Grid		
	
	function getParticipantsGridData(contestId, pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetParticipants",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&pageNo="+pageNo+"&headerFilter="+participantsSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				participantsPagingInfo = jsonData.participantsPagingInfo;
				refreshParticipantsGridValues(jsonData.participantsList);					
				$('#contestIdStr').val(contestId);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function participantsExportToExcel(){
		var contId = $('#contestIdStr').val();
		var appendData = "contestId="+contId+"&headerFilter="+participantsSearchString;
	    var url = "${pageContext.request.contextPath}/ParticipantsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function participantsResetFilters(){
		participantsSearchString='';
		participantsColumnFilters = {};
		var contstId = $('#contestIdStr').val();
		getParticipantsGridData(contstId, 0);
	}
	
	var participantsPagingInfo;
	var participantsDataView;
	var participantsGrid;
	var participantsData = [];
	var participantsGridPager;
	var participantsSearchString='';
	var participantsColumnFilters = {};
	var userParticipantsColumnsStr = '<%=session.getAttribute("participantsgrid")%>';

	var userParticipantsColumns = [];
	var allParticipantsColumns = [
			/*{
				id : "participantId",
				name : "Participant ID",
				field : "participantId",
				width : 80,
				sortable : true
			},{
				id : "participantContId",
				field : "participantContId",
				name : "Participant Contest Id",
				width : 80,
				sortable : true
			},*/{
				id : "customerId",
				field : "customerId",
				name : "Customer Id",
				width : 80,
				sortable : true
			},{
				id : "customerName",
				field : "customerName",
				name : "Customer Name",
				width : 80,
				sortable : true
			},{
				id : "customerEmail",
				field : "customerEmail",
				name : "Customer Email",
				width : 80,
				sortable : true
			},{
				id : "purchaseCustomerId",
				field : "purchaseCustomerId",
				name : "Purchase Customer Id",
				width : 80,
				sortable : true
			},{
				id : "purchaseCustomerName",
				field : "purchaseCustomerName",
				name : "Purchase Customer Name",
				width : 80,
				sortable : true
			},{
				id : "purchaseCustomerEmail",
				field : "purchaseCustomerEmail",
				name : "Purchase Customer Email",
				width : 80,
				sortable : true
			},{
				id : "referralCode",
				field : "referralCode",
				name : "Referral Code",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			}];

	if (userParticipantsColumnsStr != 'null' && userParticipantsColumnsStr != '') {
		columnOrder = userParticipantsColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allParticipantsColumns.length; j++) {
				if (columnWidth[0] == allParticipantsColumns[j].id) {
					userParticipantsColumns[i] = allParticipantsColumns[j];
					userParticipantsColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userParticipantsColumns = allParticipantsColumns;
	}
		
	var participantsOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var participantsGridSortcol = "contestId";
	var participantsGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function participantsGridComparer(a, b) {
		var x = a[participantsGridSortcol], y = b[participantsGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshParticipantsGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		participantsData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (participantsData[i] = {});
				d["id"] = i;
				d["participantId"] = data.id;
				/* d["participantContId"] = data.contestId; */
				d["customerId"] = data.customerId;
				d["customerName"] = data.customerName;
				d["customerEmail"] = data.customerEmail;
				d["purchaseCustomerId"] = data.purchaseCustomerId;
				d["purchaseCustomerName"] = data.purchaseCustomerName;
				d["purchaseCustomerEmail"] = data.purchaseCustomerEmail;
				d["referralCode"] = data.referralCode;
				d["createdDate"] = data.createdDate;
			}
		}

		participantsDataView = new Slick.Data.DataView();
		participantsGrid = new Slick.Grid("#participants_grid", participantsDataView,
				userParticipantsColumns, participantsOptions);
		participantsGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		participantsGrid.setSelectionModel(new Slick.RowSelectionModel());
		//participantsGrid.registerPlugin(contestsCheckboxSelector);
		
		participantsGridPager = new Slick.Controls.Pager(participantsDataView,
					participantsGrid, $("#participants_pager"),
					participantsPagingInfo);
		var participantsGridColumnpicker = new Slick.Controls.ColumnPicker(
				allParticipantsColumns, participantsGrid, participantsOptions);
		
	
		participantsGrid.onSort.subscribe(function(e, args) {
			participantsGridSortdir = args.sortAsc ? 1 : -1;
			participantsGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				participantsDataView.fastSort(participantsGridSortcol, args.sortAsc);
			} else {
				participantsDataView.sort(participantsGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the participantsGrid
		participantsDataView.onRowCountChanged.subscribe(function(e, args) {
			participantsGrid.updateRowCount();
			participantsGrid.render();
		});
		participantsDataView.onRowsChanged.subscribe(function(e, args) {
			participantsGrid.invalidateRows(args.rows);
			participantsGrid.render();
		});
		$(participantsGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							participantsSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								participantsColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in participantsColumnFilters) {
										if (columnId !== undefined
												&& participantsColumnFilters[columnId] !== "") {
											participantsSearchString += columnId
													+ ":"
													+ participantsColumnFilters[columnId]
													+ ",";
										}
									}
									var contstsId = $('#contestIdStr').val();
									getParticipantsGridData(contstsId, 0);
								}
							}

						});
		participantsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(participantsColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(participantsColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		participantsGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		participantsDataView.beginUpdate();
		participantsDataView.setItems(participantsData);
		//participantsDataView.setFilter(filter);
		participantsDataView.endUpdate();
		participantsDataView.syncGridSelection(participantsGrid, true);
		participantsGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserParticipantsPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = participantsGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('participantsgrid', colStr);
	}
	
	
	//Edit Lottery
	function editLottery(){
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select Lottery to Edit", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getEditContest(contestId);
		}
	}
	
	function getEditContest(contestId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateLottery",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					//$('#contestCategoryDiv').empty();					
					$('#myModal-2').modal('show');
					setEditContest(jsonData.contestsList);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function generateWinner(action){
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select lottery to choose winner", "info");
			return false;
		}
		var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
		
		$.ajax({
			url : "${pageContext.request.contextPath}/GetLotteryWinner",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&action="+action,
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					var winner = jsonData.winner;
					console.log(winner);
					$('#winnerContestName').text(winner.contestName);
					$('#winnerStartDate').text(winner.startDate);
					$('#winnerEndDate').text(winner.endDate);
					$('#winnerCustomerName').text(winner.winnerName);
					$('#winnerCustomerEmail').text(winner.winnerEmail);
					$('#winnerCustomerDate').text(winner.createdDate);
					$('#winnerMailSent').text(winner.isEmailed);
					$('#winnerCustomerBy').text(winner.createdBy);
					$('#winnerCity').text(winner.winnerCity);
					$('#winnerDialog').modal('show');
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditContest(contestsEd){
		$('#saveBtn').hide();			
		$('#updateBtn').show();
		
		if(contestsEd != null && contestsEd.length > 0){
			$('#artistDiv').empty();
			for (var i = 0; i < contestsEd.length; i++){					
				var data = contestsEd[i];
				$('#contest_id').val(data.id);
				$('#contest_name').val(data.name);
				$('#contest_minPurchaseAmt').val(data.minPurchaseAmt);
				$('#contest_fromDate').val(data.fromDate);
				$('#contest_toDate').val(data.toDate);
				$('#artistName').val(data.artistName);
				$('#artistId').val(data.artistId);
				$('#artistDiv').append('<span class="remove">ARTIST : '+data.artistNameStr+'</span>');
			}
		}
	}
	
	//Delete Lottery
	function deleteLottery(){		
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select Lottery to Delete", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getDeleteContest(contestId);
		}
	}
	
	function getDeleteContest(contestId){
		if (contestId == '') {
			jAlert("Please select a Lottery to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete a Lottery and its Participants ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateLottery",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=DELETE",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.contestsPagingInfo;
								columnFilters = {};
								refreshContestsGridValues(jsonData.contestsList);
								refreshParticipantsGridValues('');
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	

	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${contestsPagingInfo}));
		participantsPagingInfo = JSON.parse(JSON.stringify(${participantsPagingInfo}));
		refreshContestsGridValues(JSON.parse(JSON.stringify(${contestsList})));
		$('#contests_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestsPreference()'>");
		enableMenu();
	};
		
	</script>