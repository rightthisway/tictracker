<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
.promoAll{
	margin-right:5px;
	background: #87ceeb;
	font-size: 10pt;
	height:20px;
}
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var isReset = false;
$(document).ready(function() {
	$('#startDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	$('#endDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$("#freeTicket1").click(function(){
		callTabOnChange('FREETICKET');
	});
	$("#freeLives1").click(function(){
		callTabOnChange('FREELIVES');
	});
	
});

function resetEarnLivesModal(){
	$('#saveButton').show();			
	$('#updateButton').hide();
	$("#promoCode").removeAttr("disabled");
	$('#promoCode').val('');
	$('#lifePerCustomer').val('');
	$('#maxThreshold').val('');
	$('#startDate').val('');
	$('#endDate').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();
}

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ContestPromocodes?status="+selectedTab
}


function updateOffer(status){
	var temprDiscountRowIndex = promoCodeGrid.getSelectedRows([0])[0];
	if (temprDiscountRowIndex < 0 || temprDiscountRowIndex == undefined) {
		jAlert("Please select Records to update.");
		return;
	}
	var promoOffrId = promoCodeGrid.getDataItem(temprDiscountRowIndex).promoId;
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdateContestEarnLivesOfferStatus",
		type : "post",
		dataType:"json",
		data:"status="+status+"&promocodeId="+promoOffrId,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				pagingInfo = JSON.parse(jsonData.promoPagingInfo);
				refreshPromoCodeGridValues(JSON.parse(jsonData.promoCodeList));
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function getPromoCodeGridData(pageNo){
	$.ajax({
		url : "${pageContext.request.contextPath}/ContestPromocodes",
		type : "post",
		dataType : "json",
		data : "pageNo="+pageNo+"&headerFilter="+promoCodeSearchString+"&isReset="+isReset+"&status=${statusType}",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			isReset = false;
			if(jsonData.status == 1){
				pagingInfo = JSON.parse(jsonData.promoPagingInfo);
				refreshPromoCodeGridValues(JSON.parse(jsonData.promoCodeList));
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function saveEarnLivePromo(action){
	var promoCode = $('#promoCode').val();
	var lifePerCustomer = $('#lifePerCustomer').val();
	var maxThreshold = $('#maxThreshold').val();
	var startDate = $('#startDate').val();
	var endDate = $('#endDate').val();
	
	if(lifePerCustomer == '' || isNaN(lifePerCustomer)){
		jAlert("Max Life per customers is mendatory and it should be valid Number.");
		return;
	}
	if(maxThreshold == '' || isNaN(maxThreshold)){
		jAlert("Max total threshold is mendatory and it should be valid Number.");
		return;
	}
	if(startDate == '' ){
		jAlert("Start Date is mendatory.");
		return;
	}
	if(endDate == ''){
		jAlert("End Date is Mendatory.");
		return;
	}
	
	$.ajax({
		url : "${pageContext.request.contextPath}/GenerateContestEarnLivePromo",
		type : "post",
		dataType: "json",
		data: $('#promoCodeForm').serialize()+"&action="+action,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#myModal-3').modal('hide');
				pagingInfo = JSON.parse(jsonData.promoPagingInfo);
				promoColumnFilters = {};
				refreshPromoCodeGridValues(JSON.parse(jsonData.promoCodeList));
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
	
}
</script>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest Promotional Offers</a>
			</li>
			<li><i class="fa fa-laptop"></i>Contest Promocodes</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="freeTicketTab" class=""><a id="freeTicket1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#freeTicket">Tickets Promo Offers</a></li>
				<li id="FreeLivesTab" class="active"><a id="freeLives1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#freeLives">Earn Lives Promo Offers</a></li>
			</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="allContest" class="tab-pane">
			</div>
			<div id="freeLives" class="tab-pane active">
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-3" onclick="resetEarnLivesModal();">Add Earn Lives Promo</button>
					<button type="button" id="enableLivesOffer" class="btn btn-primary" onclick="updateOffer('ACTIVE');">Enable</button>
					<button type="button" id="disableLivesOffer" class="btn btn-primary" onclick="updateOffer('DISABLED');">Disable</button>
				</div>
				<br/><br/>
				<div style="position: relative" id="artistGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Earn Lives Offers</label>
							<div class="pull-right">
								<a href="javascript:exportPromoToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetPromoFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
							</div>
						</div>
						<div id="promoCode_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="promoCode_pager" style="width: 100%; height: 10px;"></div>
			
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-3" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Earn Lives Promocode</h4>
				</div>
					<div class="modal-body full-width">
						<form name="promoCodeForm" id="promoCodeForm" method="post">
						<input id="promoId" name="promoId" type="hidden" />
						<div class="form-group tab-fields">
							<div id="promoCodeDiv" class="form-group col-sm-6 col-xs-6">
								<label>Promotional Code <span class="required">*</span></label> 
								<input class="form-control" id="promoCode" name="promoCode" type="text" />
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Max Life per Customer<span class="required">*</span></label> 
								<input class="form-control" type="text" id="lifePerCustomer" name="lifePerCustomer">
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Total Max Threshold <span class="required">*</span></label> 
								<input class="form-control" type="text" id="maxThreshold" name="maxThreshold"> 
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Start Date <span class="required">*</span></label> 
								<input class="form-control" type="text" id="startDate" name="startDate">
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>End Date <span class="required">*</span></label> 
								<input class="form-control" type="text" id="endDate" name="endDate">
							</div>
						</div>
						</form>			
					</div>
					<div class="modal-footer full-width">
					<button class="btn btn-primary" id="updateButton" type="button" onclick="saveEarnLivePromo('UPDATE')">Update</button>
						<button class="btn btn-primary" id="saveButton" type="button" onclick="saveEarnLivePromo('SAVE')">Save</button>
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					</div>			
			</div>
		</div>
	</div>
</div>
<script>
function saveUserPromoCodePreference(){
	var cols = visibleColumns;
	if (cols == null || cols == '' || cols.length == 0) {
		cols = promoCodeGrid.getColumns();
	}
	var colStr = '';
	for ( var i = 0; i < cols.length; i++) {
		colStr += cols[i].id + ":" + cols[i].width + ",";
	}
	saveUserPreference('earnLivesOfferGrid', colStr);
}


//Earn Lives Offer grid code start
var promoCodeCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});

function exportPromoToExcel(){
	var appendData = "headerFilter="+promoCodeSearchString;
    //var url = "${pageContext.request.contextPath}/Client/PromoCodeExportToExcel?"+appendData;
	var url = apiServerUrl+"PromoCodeExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetPromoFilters(){
	promoCodeSearchString='';
	columnFilters = {};
	isReset = true;
	getPromoCodeGridData(0);
	//refreshPromoOfferDtlGridValues('');
}

var pagingInfo;
var promoCodeDataView;
var promoCodeGrid;
var promoCodeData = [];
var promoCodeGridPager;
var promoCodeSearchString='';
var columnFilters = {};
var userPromoCodeColumnsStr = '<%=session.getAttribute("earnLivesOfferGrid")%>';

	var userPromoCodeColumns = [];
	var allPromoCodeColumns = [promoCodeCheckboxSelector.getColumnDefinition(),
			/* {
				id : "promotionalCode",
				name : "Promo Code",
				field : "promotionalCode",
				width : 80,
				sortable : true
			},  */{
				id : "promoCode",
				name : "promo code",
				field : "promoCode",
				width : 80,
				sortable : true
			},/*  {
				id : "customerId",
				name : "Customer Id",
				field : "customerId",
				width : 80,
				sortable : true
			}, */  {
				id : "startDate",
				name : "Start Date",
				field : "startDate",
				width : 80,
				sortable : true
			}, {
				id : "endDate",
				name : "End Date",
				field : "endDate",
				width : 80,
				sortable : true
			},{
				id : "maxLifePerCustomer",
				name : "Max. Life per Cust",
				field : "maxLifePerCustomer",
				width : 80,
				sortable : true
			},{
				id : "maxAccumulateThreshold",
				name : "Max. Threshold",
				field : "maxAccumulateThreshold",
				width : 80,
				sortable : true
			},{
				id : "curAccumulatedCount",
				name : "Use Count",
				field : "curAccumulatedCount",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			}, {
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			}, {
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			}, {
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date ",
				width : 80,
				sortable : true
			},{
				id : "Action",
				field : "Action",
				name : "Action ",
				width : 80,
				formatter:buttonFormatter
			}];

	if (userPromoCodeColumnsStr != 'null' && userPromoCodeColumnsStr != '') {
		columnOrder = userPromoCodeColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPromoCodeColumns.length; j++) {
				if (columnWidth[0] == allPromoCodeColumns[j].id) {
					userPromoCodeColumns[i] = allPromoCodeColumns[j];
					userPromoCodeColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPromoCodeColumns = allPromoCodeColumns;
	}

	function buttonFormatter(row,cell,value,columnDef,dataContext){  
		    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.promoId +"'/>";
		    return button;
	}
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditPromotionalCode(id);//confirm("Are you sure,Do you want to Delete it?");
	});
	
	var promoCodeOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var promoCodeGridSortcol = "promoCode";
	var promoCodeGridSortdir = 1;
	var promoPercentCompleteThreshold = 0;
	var promoCodeGridSearchString = "";

	function promoCodeGridComparer(a, b) {
		var x = a[promoCodeGridSortcol], y = b[promoCodeGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshPromoCodeGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		promoCodeData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (promoCodeData[i] = {});
				d["id"] = i;
				d["promoId"] = data.id;
				d["promoCode"] = data.promoCode;
				d["startDate"] = data.startDateStr;
				d["endDate"] = data.endDateStr;
				d["maxLifePerCustomer"] = data.maxLifePerCustomer;
				d["maxAccumulateThreshold"] = data.maxAccumulateThreshold;
				d["curAccumulatedCount"] = data.curAccumulatedCount;
				d["status"] = data.status;
				d["createdBy"] = data.createdBy;
				d["createdDate"] = data.createdDateStr;
				d["updatedBy"] = data.updatedBy;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		promoCodeDataView = new Slick.Data.DataView();
		promoCodeGrid = new Slick.Grid("#promoCode_grid", promoCodeDataView,
				userPromoCodeColumns, promoCodeOptions);
		promoCodeGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		promoCodeGrid.setSelectionModel(new Slick.RowSelectionModel());
		promoCodeGrid.registerPlugin(promoCodeCheckboxSelector);
		
			promoCodeGridPager = new Slick.Controls.Pager(promoCodeDataView,
					promoCodeGrid, $("#promoCode_pager"),
					pagingInfo);
		var promoCodeGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPromoCodeColumns, promoCodeGrid, promoCodeOptions);
		
		/* promoCodeGrid.onContextMenu.subscribe(function(e) {
			e.preventDefault();
			var cell = promoCodeGrid.getCellFromEvent(e);
			promoCodeGrid.setSelectedRows([ cell.row ]);
			var row = promoCodeGrid.getSelectedRows([ 0 ])[0];			
			var promoId = promoCodeGrid.getDataItem(row).promoOfferId;
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			
			if (height < ($("#contextMenu").height() + 100)) {
				height = e.pageY - $("#contextMenu").height();
			} else {
				height = e.pageY;
			}
			if (width < $("#contextMenu").width()) {
				width = e.pageX - $("#contextMenu").width();
			} else {
				width = e.pageX;
			}
			$("#contextMenu").data("row", cell.row).css("top", height).css(
					"left", width).show();
			$("body").one("click", function() {
				$("#contextMenu").hide();
			});
			
		}); */
	
		promoCodeGrid.onSort.subscribe(function(e, args) {
			promoCodeGridSortdir = args.sortAsc ? 1 : -1;
			promoCodeGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				promoCodeDataView.fastSort(promoCodeGridSortcol, args.sortAsc);
			} else {
				promoCodeDataView.sort(promoCodeGridComparer, args.sortAsc);
			}
		});
		// wire up model promoCodes to drive the promoCodeGrid
		promoCodeDataView.onRowCountChanged.subscribe(function(e, args) {
			promoCodeGrid.updateRowCount();
			promoCodeGrid.render();
		});
		promoCodeDataView.onRowsChanged.subscribe(function(e, args) {
			promoCodeGrid.invalidateRows(args.rows);
			promoCodeGrid.render();
		});
		$(promoCodeGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							promoCodeSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											promoCodeSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getPromoCodeGridData(0);
								}
							}

						});
		promoCodeGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'startDate' || args.column.id == 'endDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(columnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		promoCodeGrid.init();
		
		var promoCodeRowIndex = -1;
		promoCodeGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprPromoRowIndex = promoCodeGrid.getSelectedRows([0])[0];
			if (temprPromoRowIndex != promoCodeRowIndex) {
				/* var promoOffrId = promoCodeGrid.getDataItem(temprPromoRowIndex).promoOfferId;
				getPromoOfferDtlGridData(promoOffrId,0); */
			}
		});
		// initialize the model after all the promoCodes have been hooked up
		promoCodeDataView.beginUpdate();
		promoCodeDataView.setItems(promoCodeData);
		//promoCodeDataView.setFilter(filter);
		promoCodeDataView.endUpdate();
		promoCodeDataView.syncGridSelection(promoCodeGrid, true);
		promoCodeGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(promoCodeGrid != null && promoCodeGrid != undefined){
			promoCodeGrid.resizeCanvas();
		}
					
	});
	
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(pagingInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(pagingInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(pagingInfo.pageNum) - 1;
		}
		getPromoCodeGridData(pageNo);
	}
	
	
	function buttonFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.promoId +"'/>";
	    return button;
	}
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditPromotionalCode(id);//confirm("Are you sure,Do you want to Delete it?");
	});
	function getEditPromotionalCode(promoOfferId){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestPromocodes",
			type : "post",
			dataType: "json",
			data: "promocodeId="+promoOfferId+"&status=${statusType}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					setEditPromotionalOffer(JSON.parse(jsonData.promoCode));
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	function setEditPromotionalOffer(data){
		$('#myModal-3').modal('show');
		$('#saveButton').hide();			
		$('#updateButton').show();
		if(data!=null){
			$('#promoId').val(data.id);
			$('#promoCode').val(data.promoCode);
			$("#promoCode").attr("disabled", "disabled");
			$('#lifePerCustomer').val(data.maxLifePerCustomer);
			$('#maxThreshold').val(data.maxAccumulateThreshold);
			$('#startDate').val(data.startDateEditStr);
			$('#endDate').val(data.endDateEditStr);
		}
	}
	
	function saveUserPromoCodePreference(){
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = promoCodeGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('earnLivesOfferGrid', colStr);
	}
	
	window.onload = function() {
		
		pagingInfo = JSON.parse(JSON.stringify(${promoPagingInfo}));
		refreshPromoCodeGridValues(JSON.parse(JSON.stringify(${promoCodeList})));
		$('#promoCode_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPromoCodePreference()'>");
		enableMenu();
	};
</script>
	