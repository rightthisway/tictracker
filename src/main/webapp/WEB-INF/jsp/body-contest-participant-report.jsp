<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script src="../resources/amcharts/core.js" type="text/javascript"></script>
<script src="../resources/amcharts/maps.js" type="text/javascript"></script>
<script src="../resources/amcharts/charts.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/moonrisekingdom.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/material.js" type="text/javascript"></script>
<script src="../resources/amcharts/themes/animated.js" type="text/javascript"></script>

<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-laptop"></i>Contest Participant Data</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
				
</div>


<div>
	<!--<input type="button"class="btn btn-primary"  value="Download PDF" onclick="savePDF();" />-->
	<div id="chart1div" style="height:600px;" class="col-md-12"></div>
	<div id="chartdiv" style="height:3000px;" class="col-md-12"></div>
</div>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});
	
});

function callAPIReport(reportURL){
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function openContestReportModal(reportName){
	$('#contestReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
}

function openCustChainReportModal(){
	$('#custChainReportModal').modal('show'); 
}

function generateReport(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var reportURL = $('#contestReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function generateChainReport(){
	var userId = $('#userId').val(); 
	if(null == userId || userId == "" || userId == " "){
		$('#userIdErrId').html("Please Enter Valid User ID."); 
		return;
	}else{
		$('#userIdErrId').html(""); 
		window.location.href = "${pageContext.request.contextPath}/Reports/DownloadCustomerStatsReport?userId="+userId;
	}
}

function generateBotReport(){ 
	window.location.href = "${pageContext.request.contextPath}/Reports/DownloadBotReport";
}
////////===========================

// // Themes begin
// am4core.useTheme(am4themes_animated);
// // Themes end
 // function getReportChart(contestParticipant){
// // Create chart instance
// var chart = am4core.create("chartdiv", am4charts.XYChart3D);
// chart.paddingBottom = 30;
// chart.angle = 35;

// // Add data
// chart.data =  JSON.parse(contestParticipant);	
// console.log(JSON.parse(contestParticipant));
// // Create axes
// var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
// categoryAxis.dataFields.category = "viewName";
// categoryAxis.renderer.grid.template.location = 0;
// categoryAxis.renderer.minGridDistance = 20;
// categoryAxis.renderer.inside = true;
// categoryAxis.renderer.grid.template.disabled = true;

// let labelTemplate = categoryAxis.renderer.labels.template;
// labelTemplate.rotation = -90;
// labelTemplate.horizontalCenter = "left";
// labelTemplate.verticalCenter = "middle";
// labelTemplate.dy = 10; // moves it a bit down;
// labelTemplate.inside = false; // this is done to avoid settings which are not suitable when label is rotated

// var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
// valueAxis.renderer.grid.template.disabled = true;

// // Create series
// var series = chart.series.push(new am4charts.ConeSeries());
// series.dataFields.valueY = "viewCount";
// series.dataFields.categoryX = "viewName";

// var columnTemplate = series.columns.template;
// columnTemplate.adapter.add("fill", function(fill, target) {
  // return chart.colors.getIndex(target.dataItem.index);
// })

// columnTemplate.adapter.add("stroke", function(stroke, target) {
  // return chart.colors.getIndex(target.dataItem.index);
// })
 // };
/////=================================

		

		// Create chart instance
		var chart = am4core.create("chartdiv", am4charts.XYChart);
		var chart1 = am4core.create("chart1div", am4charts.XYChart);
		function getReportChart(contestParticipant){
		// Themes begin
		am4core.useTheme(am4themes_animated);	
		
		// Add data
		chart.data =  JSON.parse(contestParticipant);

		// Create axes
		var yAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		yAxis.dataFields.category = "viewName";
		yAxis.renderer.grid.template.location = 0;
		yAxis.renderer.labels.template.fontSize = 10;
		yAxis.renderer.minGridDistance = 10;

		var xAxis = chart.xAxes.push(new am4charts.ValueAxis());

		// Create series
		var series = chart.series.push(new am4charts.ColumnSeries());
		series.dataFields.valueX = "viewCount";
		series.dataFields.categoryY = "viewName";
		series.columns.template.tooltipText = "{categoryY}: [bold]{valueX}[/]";
		series.columns.template.strokeWidth = 0;
		series.columns.template.adapter.add("fill", function(fill, target) {
		  if (target.dataItem) {
			switch(target.dataItem.dataContext.region) {
			  case "Central":
				return chart.colors.getIndex(0);
				break;
			  case "East":
				return chart.colors.getIndex(1);
				break;
			  case "South":
				return chart.colors.getIndex(2);
				break;
			  case "West":
				return chart.colors.getIndex(3);
				break;
			}
		  }
		  return fill;
		});
		var valueLabel = series.bullets.push(new am4charts.LabelBullet());
			valueLabel.label.text = "{valueX}";
			valueLabel.label.rotation = 0
			valueLabel.label.fontSize = 10;
			valueLabel.label.dx = 10;
		// Add ranges
		function addRange(label, start, end, color) {
		  var range = yAxis.axisRanges.create();
		  range.category = start;
		  range.endCategory = end;
		  range.label.text = label;
		  range.label.disabled = false;
		  range.label.fill = color;
		  range.label.location = 0;
		  range.label.dx = -130;
		  range.label.dy = 12;
		  range.label.fontWeight = "bold";
		  range.label.fontSize = 12;
		  range.label.horizontalCenter = "left"
		  range.label.inside = true;
		  
		  range.grid.stroke = am4core.color("#396478");
		  range.grid.strokeOpacity = 1;
		  range.tick.length = 200;
		  range.tick.disabled = false;
		  range.tick.strokeOpacity = 0.6;
		  range.tick.stroke = am4core.color("#396478");
		  range.tick.location = 0;
		  
		  range.locations.category = 1;
		}


		chart.cursor = new am4charts.XYCursor();	


//////////////////////////////////
		am4core.useTheme(am4themes_animated);
		// Themes end

		// Create chart instance
		//var chart1 = am4core.create("chart1div", am4charts.XYChart);
		chart1.paddingRight = 20;

		// Add data
		chart1.data = JSON.parse(contestParticipant);

		// Create axes
		var categoryAxis = chart1.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "viewName";
		categoryAxis.renderer.minGridDistance = 50;
		categoryAxis.renderer.grid.template.location = 0.5;
		categoryAxis.renderer.labels.template.rotation = 270
		categoryAxis.startLocation = 0.5;
		categoryAxis.endLocation = 0.5;

		// Pre zoom
		chart1.events.on("datavalidated", function () {
		  categoryAxis.zoomToIndexes(Math.round(chart1.data.length * 0.4), Math.round(chart1.data.length * 0.55));
		});

		// Create value axis
		var valueAxis = chart1.yAxes.push(new am4charts.ValueAxis());
		valueAxis.baseValue = 0;

		// Create series
		var series = chart1.series.push(new am4charts.LineSeries());
		series.dataFields.valueY = "viewCount";
		series.dataFields.categoryX = "viewName";
		series.strokeWidth = 2;
		series.tensionX = 0.77;

		var range = valueAxis.createSeriesRange(series);
		range.value = 0;
		range.endValue = 1000;
		range.contents.stroke = am4core.color("#FF0000");
		range.contents.fill = range.contents.stroke;

		// Add scrollbar
		var scrollbarX = new am4charts.XYChartScrollbar();
		scrollbarX.series.push(series);
		chart1.scrollbarX = scrollbarX;

		chart1.cursor = new am4charts.XYCursor();
}



//call functions once page loaded
	window.onload = function() {
		 $("div#divLoading").addClass('show');
		getReportChart('${contestParticipant}');
		$('#pollingContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingContestPreference()'>");
		
		enableMenu();
	};		

</script>

<style>
	input{
		color : black !important;
	}
</style>




