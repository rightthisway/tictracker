<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.giftCardLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(giftCardGrid != null && giftCardGrid != undefined){
			giftCardGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${gcStatus == 'ACTIVE'}">	
			$('#activeGiftCard').addClass('active');
			$('#activeGiftCardTab').addClass('active');
		</c:when>
		<c:when test="${gcStatus == 'EXPIRED'}">
			$('#expiredGiftCard').addClass('active');
			$('#expiredGiftCardTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeGiftCard1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredGiftCard1").click(function(){
		callTabOnChange('EXPIRED');
	});
	
	
	 $('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	
	
});

/* function callChangeImage(){
	$('#imageFileDiv').show();
	$('#cardImageDiv').hide();
	$('#fileRequired').val('Y');
} */

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/GiftCards?gcStatus="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy giftCard">Copy GiftCard</li>
  <li data="edit giftCard">Edit GiftCard</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Gift Cards</a></li>
			<li><i class="fa fa-laptop"></i>Manage Gift Card</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeGiftCardTab" class=""><a id="activeGiftCard1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeGiftCard">Active GiftCards</a></li>
				<li id="expiredGiftCardTab" class=""><a id="expiredGiftCard1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredGiftCard">Expired GiftCards</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeGiftCard" class="tab-pane">
			<c:if test="${gcStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetGiftCardModal();">Add GiftCard</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editGiftCard()">Edit GiftCard</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteGiftCard()">Delete GiftCard</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:giftCardExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCard_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCard_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="expiredGiftCard" class="tab-pane">
			<c:if test="${gcStatus =='EXPIRED'}">	
				<div class="full-width full-width-btn mb-20">
					<!-- <button class="btn btn-primary" id="editQBBtn" type="button" onclick="editGiftCard()">Edit GiftCard</button> -->
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteGiftCard()">Delete GiftCard</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Cards</label>
							<div class="pull-right">
								<a href="javascript:giftCardExportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCard_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCard_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit GiftCard  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="giftCardModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Gift Card</h4>
			</div>
			<div class="modal-body full-width">
				<form name="giftCardForm" id="giftCardForm" method="post">
					<input type="hidden" id="cardId" name="cardId" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="gcStatus" name="gcStatus" value="${gcStatus}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Gift Card Brand<span class="required">*</span></label>
							<select id="gcBrand" name="gcBrand" class="form-control" >
								<option value="0">-- Select --</option>
							</select>
						</div>						
						<div class="form-group col-sm-12 col-xs-12">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="description" name="description">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Conversion Type<span class="required">*</span></label>
							<select id="conversionType" name="conversionType" class="form-control">
								<option value="NORMAL">NORMAL</option>
								<option value="PERCECNTAGE">PERCENTAGE</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Rewards Conversion Rate<span class="required">*</span>
							</label> <input class="form-control" type="text" id="conversionRate" name="conversionRate">
						</div>
							<div class="form-group col-sm-6 col-xs-6">
							<label>Points Conversion Rate<span class="required">*</span>
							</label> <input class="form-control" type="text" id="pointConversionRate" name="pointConversionRate">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Start Date<span class="required">*</span></label>
							</label> <input class="form-control searchcontrol" type="text" id="startDate" name="startDate" value="${startDate}"> 
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>End Date<span class="required">*</span>
							</label>  <input class="form-control searchcontrol" type="text" id="endDate" name="endDate" value="${endDate}"> 
						</div>
						<div class="full-width mb-10 mt-10">
								<a id="ei_add_row" onclick="callAddNewRow();" class="btn btn-primary" style="margin-right:15px;"  onclick="callAddNewRow();">Add Row</a>
								<a id='delete_row' class="btn btn-cancel" onclick="callDeleteRow();">Delete Row</a>
						</div>
						<div class="table-responsive">
							<input type="hidden" name="rowCount" id="gcq_rowCount"/>
							<table class="table table-bordered table-hover" id="tab_logic">
								<thead>
									<tr>
										<th style="font-size: 13px;font-family: arial;" class="text-center">
											Amount($)
										</th>
										<th style="font-size: 13px;font-family: arial;" class="text-center">
											Quantity
										</th>
										<th style="font-size: 13px;font-family: arial;" class="text-center">
											Max. Qty. Threshold
										</th>
										<th style="font-size: 13px;font-family: arial;" class="text-center">
											Remaining Quantity
										</th>
										<th style="font-size: 13px;font-family: arial;" class="text-center">
											Used Quantity
										</th>
									</tr>
								</thead>
								<tbody id="ei_ticketGroups">
								</tbody>
							</table>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Gift Card Type<span class="required">*</span></label>
							<select id="cardType" name="cardType" class="form-control">
								<option value="CUSTOMER">CUSTOMER</option>
								<option value="CONTEST">CONTEST</option>
							</select>
						</div>
						<!--  <div id="imageFileDiv" class="form-group col-sm-8 col-xs-8">
						 	<input type="hidden" name="fileRequired" id="fileRequired" />
							<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 			<input type="file" id="imageFile" name="imageFile">
             			</div> -->
					</div>
					<div class="form-group form-group-top" id="cardImageDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="cardImageTag" src="" height="150" width="400" />
               				<!--  <a style="color:blue " href="javascript:callChangeImage();">Change Image</a> -->
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="saveGiftCard('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="saveGiftCard('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit GiftCard  end here  -->

<script type="text/javascript">

	function setGiftCardQuantityGroups(jsonData){
		var ticketGroups = "";
		$('#ei_ticketGroups').empty();
		if(jsonData != null && jsonData != ""){	
			var j=1;			
			for(var i=0; i<jsonData.length; i++){
				var data = jsonData[i];			
				$('#ei_ticketGroups').append('<tr id="addr_'+j+'"></tr>');
				ticketGroups = "";
				ticketGroups += "<td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input type='hidden' name='rowNumber' id='rowNumber_"+j+"' value='"+j+"' />";
				ticketGroups += "<input type='hidden' name='id_"+j+"' id='id_"+j+"' value="+data.id+" />";
				ticketGroups += "<input name='amount_"+j+"' id='amount_"+j+"' type='text' value="+data.amount+" placeholder='Amount' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='quantity_"+j+"' id='quantity_"+j+"' type='text' value="+data.maxQty+" placeholder='Quantity' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='maxThreshold_"+j+"' id='maxThreshold_"+j+"' type='text' value="+data.maxQtyThreshold+" placeholder='Max. Threshold' class='form-control input-md' />";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='remainingQty_"+j+"' id='remainingQty_"+j+"' type='text' value="+data.freeQty+" class='form-control input-md' readOnly/>";
				ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
				ticketGroups += "<input name='usedQty_"+j+"' id='usedQty_"+j+"' type='text' value="+data.usedQty+" class='form-control input-md' readOnly/>";
				ticketGroups += "</td>";
				$('#addr_'+j).html(ticketGroups);
				j++;
			}
		}else{
			callAddNewRow();
		}		
		$('#gcq_rowCount').val($('#tab_logic tr').length-1);
	}


	function callAddNewRow() {
		iRow = $('#tab_logic tr').length-1;
		$('#gcq_rowCount').val(iRow);
		iRow++;
		$('#tab_logic').append('<tr id="addr_'+iRow+'"></tr>');
		
		$('#addr_'+iRow).html(
		 		"<td><input id='amount_"+iRow+"'name='amount_"+iRow+"' type='text' placeholder='Amount' class='form-control input-md' /> </td> " +
			   "<td><input id='quantity_"+iRow+"' name='quantity_"+iRow+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> "+
			   "<td><input id='maxThreshold_"+iRow+"'  name='maxThreshold_"+iRow+"' type='text' placeholder='Max. Threshold'  class='form-control input-md'></td>"+
			   "<td><input id='remainigQty_"+iRow+"'  name='remainigQty_"+iRow+"' type='text' placeholder='0'  class='form-control input-md' readOnly></td>"+
			  	"<td><input id='usedQty_"+iRow+"'  name='usedQty_"+iRow+"' type='text' placeholder='0'  class='form-control input-md' readOnly></td>");
		$('#gcq_rowCount').val(iRow);
	}
	
	function callDeleteRow() {
		if($('#tab_logic tr').length > 1){
			var delRow = $('#tab_logic tr').length-1;
			$("#addr_"+delRow).remove();
			delRow--;
		 }
		$('#gcq_rowCount').val(delRow);
	}


	function pagingControl(move, id) {
		if(id == 'giftCard_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getGiftCardGridData(pageNo);
		}
	}
	
	//GiftCard  Grid
	
	function getGiftCardGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/GiftCards.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+giftCardSearchString+"&gcStatus=${gcStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.cardPagingInfo;
				refreshGiftCardGridValues(jsonData.cards);	
				//clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function giftCardExportToExcel(status){
		var appendData = "headerFilter="+giftCardSearchString+"&gcStatus="+gcStatus;
	    var url =apiServerUrl+"GiftCardsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function giftCardResetFilters(){
		giftCardSearchString='';
		sortingString ='';
		giftCardColumnFilters = {};
		getGiftCardGridData(0);
	}
	
	/* var giftCardCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		}); */
	
	var pagingInfo;
	var giftCardDataView;
	var giftCardGrid;
	var giftCardData = [];
	var giftCardGridPager;
	var giftCardSearchString='';
	var sortingString='';
	var giftCardColumnFilters = {};
	var userGiftCardColumnsStr = '<%=session.getAttribute("giftcardgrid")%>';

	var userGiftCardColumns = [];
	var allGiftCardColumns = [/* giftCardCheckboxSelector.getColumnDefinition(), */
			 {
				id : "giftCardId",
				field : "giftCardId",
				name : "Gift Card Id",
				width : 80,
				sortable : true
			},{
				id : "title",
				field : "title",
				name : "Title",
				width : 80,
				sortable : true
			},{
				id : "description",
				field : "description",
				name : "Description",
				width : 80,
				sortable : true
			},{
				id : "imageUrl",
				field : "imageUrl",
				name : "Image Url",
				width : 80,
				sortable : true
			},{
				id : "conversionType",
				field : "conversionType",
				name : "Conversion Type",
				width : 80,
				sortable : true
			},{
				id : "conversionRate",
				field : "conversionRate",
				name : "Rewards Conversion Rate",
				width : 80,
				sortable : true
			},{
				id : "pointConversionRate",
				field : "pointConversionRate",
				name : "Points Conversion Rate",
				width : 80,
				sortable : true
			},{
				id : "cardType",
				field : "cardType",
				name : "Gift Card Type",
				width : 80,
				sortable : true
			},{
				id : "startDate",
				field : "startDate",
				name : "Start Date",
				width : 80,
				sortable : true
			},{
				id : "endDate",
				field : "endDate",
				name : "End Date",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "amount",
				field : "amount",
				name : "Price",
				width : 80,
				sortable : true
			},{
				id : "maxQuantity",
				field : "maxQuantity",
				name : "Max Qty.",
				width : 80,
				sortable : true
			},{
				id : "usedQuantity",
				field : "usedQuantity",
				name : "Used Qty.",
				width : 80,
				sortable : true
			},{
				id : "remainingQty",
				field : "remainingQty",
				name : "Free Qty.",
				width : 80,
				sortable : true
			},{
				id : "maxQtyThreshold",
				field : "maxQtyThreshold",
				name : "Max Qty. Threshold.",
				width : 80,
				sortable : true
			},{
				id : "editGiftCard",
				field : "editGiftCard",
				name : "Edit ",
				width : 80,
				formatter: editQBFormatter
			}  ];

	if (userGiftCardColumnsStr != 'null' && userGiftCardColumnsStr != '') {
		columnOrder = userGiftCardColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allGiftCardColumns.length; j++) {
				if (columnWidth[0] == allGiftCardColumns[j].id) {
					userGiftCardColumns[i] = allGiftCardColumns[j];
					userGiftCardColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userGiftCardColumns = allGiftCardColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.giftCardId +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditGiftCard(id);
	});
	
	var giftCardOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var giftCardGridSortcol = "giftCardId";
	var giftCardGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function giftCardGridComparer(a, b) {
		var x = a[giftCardGridSortcol], y = b[giftCardGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshGiftCardGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		giftCardData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (giftCardData[i] = {});
				d["id"] = i;
				d["giftCardId"] = data.id;
				d["title"] = data.title;
				d["description"] = data.description;
				d["imageUrl"] = data.imageUrl;
				d["conversionType"] = data.conversionType;
				d["status"] =  data.cardStatus;
				d["conversionRate"] = data.conversionRate;
				d["pointConversionRate"] = data.pointsConversionRate;
				d["cardType"] = data.cardType;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["updatedBy"] = data.updatedBy;
				d["createdBy"] = data.createdBy;
				d["amount"] = data.amount;
				d["maxQuantity"] = data.maxQuantity;
				d["usedQuantity"] = data.usedQuantity;
				d["remainingQty"] = data.remainingQty;
				d["maxQtyThreshold"] = data.maxQtyThreshold;
				d["startDate"] = data.startDateStr;
				d["endDate"] = data.endDateStr;
			}
		}

		giftCardDataView = new Slick.Data.DataView();
		giftCardGrid = new Slick.Grid("#giftCard_grid", giftCardDataView,
				userGiftCardColumns, giftCardOptions);
		giftCardGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		giftCardGrid.setSelectionModel(new Slick.RowSelectionModel());
		//giftCardGrid.registerPlugin(giftCardCheckboxSelector);
		
		giftCardGridPager = new Slick.Controls.Pager(giftCardDataView,
					giftCardGrid, $("#giftCard_pager"),
					pagingInfo);
		var giftCardGridColumnpicker = new Slick.Controls.ColumnPicker(
				allGiftCardColumns, giftCardGrid, giftCardOptions);
		
		
		/* giftCardGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = giftCardGrid.getCellFromEvent(e);
		      giftCardGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy giftCard'){
				var tempGiftCardRowIndex = giftCardGrid.getSelectedRows([0])[0];
				if (tempGiftCardRowIndex == null) {
					jAlert("Plese select GiftCard to Copy", "info");
					return false;
				}else {
					var giftCardText = giftCardGrid.getDataItem(tempGiftCardRowIndex).giftCard;
					var a = giftCardGrid.getDataItem(tempGiftCardRowIndex).optionA;
					var b = giftCardGrid.getDataItem(tempGiftCardRowIndex).optionB;
					var c = giftCardGrid.getDataItem(tempGiftCardRowIndex).optionC;
					var answer = giftCardGrid.getDataItem(tempGiftCardRowIndex).answer;
					var copyText = giftCardText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}else if($(e.target).attr("data") == 'edit giftCard'){
				editGiftCard();
			}
		}); */
		
		
		giftCardGrid.onSort.subscribe(function(e, args) {
			giftCardGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(giftCardGridSortcol) < 0){
				giftCardGridSortdir = 'ASC';
			}else{
				if(giftCardGridSortdir == 'DESC' ){
					giftCardGridSortdir = 'ASC';
				}else{
					giftCardGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+giftCardGridSortcol+',SORTINGORDER:'+giftCardGridSortdir+',';
			getGiftCardGridData(0);
		});
		
		// wire up model discountCodes to drive the giftCardGrid
		giftCardDataView.onRowCountChanged.subscribe(function(e, args) {
			giftCardGrid.updateRowCount();
			giftCardGrid.render();
		});
		giftCardDataView.onRowsChanged.subscribe(function(e, args) {
			giftCardGrid.invalidateRows(args.rows);
			giftCardGrid.render();
		});
		$(giftCardGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							giftCardSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								giftCardColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in giftCardColumnFilters) {
										if (columnId !== undefined
												&& giftCardColumnFilters[columnId] !== "") {
											giftCardSearchString += columnId
													+ ":"
													+ giftCardColumnFilters[columnId]
													+ ",";
										}
									}
									getGiftCardGridData(0);
								}
							}

						});
		giftCardGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'editGiftCard' && args.column.id != 'delGiftCard'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(giftCardColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(giftCardColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		giftCardGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		giftCardDataView.beginUpdate();
		giftCardDataView.setItems(giftCardData);
		//giftCardDataView.setFilter(filter);
		giftCardDataView.endUpdate();
		giftCardDataView.syncGridSelection(giftCardGrid, true);
		giftCardGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserGiftCardPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = giftCardGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('giftcardgrid', colStr);
	}
	
	// Add GiftCard 	
	function resetGiftCardModal(){		
		$('#giftCardModal').modal('show');
		$('#ei_ticketGroups').empty();
		$('#cardId').val('');
		$('#title').val('');
		$('#description').val('');
		$('#conversionType').val('NORMAL');
		$('#conversionRate').val('');
		$('#pointConversionRate').val('');
		$('#cardType').val('CUSTOMER');
		$('#imageFile').val('');
		$('#gcBrand').val(0);
		$('#action').val('');
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		$('#startDate').val('');
		$('#endDate').val('');
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		$('#cardImageDiv').hide();	
		
	}

	function saveGiftCard(action){
		
		var title = $('#title').val();
		var description = $('#description').val();
		var conversionType = $('#conversionType').val();
		var conversionRate = $('#conversionRate').val();
		var pointConversionRate = $('#pointConversionRate').val();
		var amount = $('#amount_0').val();
		var cardType = $('#cardType').val();
		//var imageFile = $('#imageFile').val();
		//var fileRequired = $('#fileRequired').val();
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		var gcBrand = $('#gcBrand').val();
		
		if(title == ''){
			jAlert("GiftCard title is mendatory.");
			return;
		}
		if(gcBrand == 0 || gcBrand == '0'){
			jAlert("GiftCard Brand is mendatory.");
			return;
		}
		if(description == ''){
			jAlert("GiftCard description is mendatory.");
			return;
		}
		if(conversionType == ''){
			jAlert("GiftCard conversion type is mendatory.");
			return;
		}
		if(cardType == ''){
			jAlert("GiftCard type is mendatory.");
			return;
		}
		if(conversionRate == ''){
			jAlert("GiftCard rewards conversion rate is mendatory.");
			return;
		}
		if(pointConversionRate == ''){
			jAlert("GiftCard poins conversion rate is mendatory.");
			return;
		}
		if(amount == ''){
			jAlert("GiftCard Price is mendatory.");
			return;
		}
		/* if(fileRequired == 'Y' && imageFile == ''){
			jAlert("GiftCard Image is mendatory.");
			return;
		}  */	
		if(startDate == ''){
			jAlert("Start Date is mendatory.");
			return;
		}
		if(endDate == ''){
			jAlert("End Date is mendatory.");
			return;
		}
		$('#action').val(action);
		var requestUrl = "${pageContext.request.contextPath}/UpdateGiftCard";
		var form = $('#giftCardForm')[0];
		var dataString = new FormData(form);
		
		/* if(action == 'SAVE'){		
			dataString  = dataString+"&action=SAVE&pageNo=0";
		}else if(action == 'UPDATE'){
			dataString = dataString+"&action=UPDATE&pageNo=0";
		} */
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			//enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#giftCardModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.cardPagingInfo);
					giftCardColumnFilters = {};
					refreshGiftCardGridValues(JSON.parse(jsonData.cards));
					//clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit GiftCard 
	function editGiftCard(){
		var tempGiftCardRowIndex = giftCardGrid.getSelectedRows([0])[0];
		if (tempGiftCardRowIndex == null) {
			jAlert("Plese select GiftCard to Edit", "info");
			return false;
		}else {
			var giftCardId = giftCardGrid.getDataItem(tempGiftCardRowIndex).giftCardId;
			getEditGiftCard(giftCardId);
		}
	}
	
	function getEditGiftCard(giftCardId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateGiftCard",
			type : "post",
			dataType: "json",
			data: "cardId="+giftCardId+"&action=EDIT&pageNo=0&gcStatus=${gcStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#giftCardModal').modal('show');
					$('#cardImageDiv').show();	
					$('#imageFileDiv').hide();
					$('#fileRequired').val('N');
					setEditGiftCard(JSON.parse(jsonData.card));
					setGiftCardQuantityGroups(JSON.parse(jsonData.qtyList));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditGiftCard(giftCard){
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = giftCard;
		$('#cardId').val(data.id);
		$('#title').val(data.title);
		$('#description').val(data.description);
		$('#conversionType').val(data.conversionType);
		$('#conversionRate').val(data.conversionRate);
		$('#pointConversionRate').val(data.pointsConversionRate);
		$('#cardType').val(data.cardType);
		$('#gcBrand').val(data.brandId);
		$('#startDate').val(data.startDateStr);
		$('#endDate').val(data.endDateStr);
		$("#cardImageTag").attr("src", apiServerUrl+"GetImageFile?type=giftCardImage&filePath="+data.fileName);
	}
	
	function getSelectedGiftCardGridId() {
		var tempGiftCardRowIndex = giftCardGrid.getSelectedRows();
		
		var giftCardIdStr='';
		$.each(tempGiftCardRowIndex, function (index, value) {
			giftCardIdStr += ','+giftCardGrid.getDataItem(value).giftCardId;
		});
		
		if(giftCardIdStr != null && giftCardIdStr!='') {
			giftCardIdStr = giftCardIdStr.substring(1, giftCardIdStr.length);
			 return giftCardIdStr;
		}
	}
	//Delete GiftCard 
	function deleteGiftCard(){
		var giftCardIds = getSelectedGiftCardGridId();
		if (giftCardIds == null || giftCardIds == '' || giftCardIds == undefined) {
			jAlert("Plese select GiftCard to Delete", "info");
			return false;
		}else {
			//var giftCardId = giftCardGrid.getDataItem(tempGiftCardRowIndex).giftCardId;
			getDeleteGiftCard(giftCardIds);
		}
	}
	
	
	function getDeleteGiftCard(giftCardIds){
		jConfirm("Are you sure to delete selected GiftCards ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateGiftCard",
						type : "post",
						dataType: "json",
						data : "cardId="+giftCardIds+"&action=DELETE&gcStatus=${gcStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.cardPagingInfo);
								giftCardColumnFilters = {};
								refreshGiftCardGridValues(JSON.parse(jsonData.cards));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${cardPagingInfo};
		refreshGiftCardGridValues(${cards});
		
		$.each( ${brands}, function (i, item) {
		    $('#gcBrand').append($('<option>', { 
		        value: item.id,
		        text : item.name 
		    }));
		});
		
		
		$('#giftCard_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGiftCardPreference()'>");
		
		enableMenu();
	};
		
</script>