<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">


var roleAffiliateBroker = "${roleAffiliateBroker}";

$(document).ready(function(){	
	
	<c:if test="${roleSeller ne null and roleSeller eq 'ROLE_SELLER'}">
		$('#sellerDiv').show();
		$('#sellerId').val(${trackerUser.sellerId});
	</c:if>
	
	$('#sellerCheck').change(function() {
	  var id = $('#sellerCheck').is(":checked")
	  if (id == true) {
		  $('#sellerDiv').show();
	  }else{
		  $('#sellerDiv').hide();
	  }
	});
	
	if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
		$('#companyName').removeAttr('readonly');
	}else{
		$('#companyName').attr('readonly','readonly');
	}
	if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_AFFILIATES'){
		$('#fromDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
		
		$('#toDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
		
		
	}
});

	var affiliateFlag = true;
	function doAddUserValidations(){
		var roles = $("input[name=role]");		
		var flag = false;
		
		if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
			if($("#companyName").val() == ''){
				jAlert("Invalid Company.","Info");
				return false;
			}
			if($("#serviceFees").val() == ''){
				jAlert("Service Fees can't be blank","Info");
				return false;
			}
		}
		else if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_AFFILIATES'){
			if($('#affiliateDiscount').val() == '' || isNaN($('#affiliateDiscount').val())){
				jAlert("Please Add valid numeric value for Affiliates Discount(RTF Orders) %.","Info");
				return false;
			}
			if($('#customerOrderDiscount').val() == '' || isNaN($('#customerOrderDiscount').val())){
				jAlert("Please Add valid numeric value for Customer discount(RTF Orders) %.","Info");
				return false;
			}
			if($('#phoneAffiliateDiscount').val() == '' || isNaN($('#phoneAffiliateDiscount').val())){
				jAlert("Please Add valid numeric value for Affiliates Discount(Phone Orders) %.","Info");
				return false;
			}
			if($('#phoneCustomerOrderDiscount').val() == '' || isNaN($('#phoneCustomerOrderDiscount').val())){
				jAlert("Please Add valid numeric value for Customer discount(Phone Orders) %.","Info");
				return false;
			}
			if($('#fromDate').val() == ''){
				jAlert("Please add valid date for Effective From Date.","Info");
				return false;
			}
			if($('#toDate').val() == ''){
				jAlert("Please add valid date for Effective To Date.","Info");
				return false;
			}			
			if(!affiliateFlag){
				jAlert('Please change Promotional Code.', "Info");
				return false;
			}
		}
		else{			
			$.each(roles, function(index, obj){
				if($(obj).attr('checked') == 'checked'){				
					flag = true;
				}
			});
			if(!flag){
				jAlert('Please choose at least one Role.',"Info");
				return false;
			}
		}
		
		var isSeller = $('#sellerCheck').is(":checked");
		if(isSeller == true){
			var sellerId = $('#sellerId').val();
			if(sellerId <= 0){
				jAlert("Please select seller from dropdown","Info");
				return false;
			}
		}else{
			$('#seller').val(-1);
		}
		
		if($("#userName").val() == ''){
			jAlert("Username can't be blank","Info");
			return false;
		}else if($("#firstName").val() == ''){
			jAlert("Firstrname can't be blank","Info");
			return false;
		}else if($("#lastName").val() == ''){
			jAlert("Lastname can't be blank","Info");
			return false;
		}else if($("#email").val() == ''){
			jAlert("Email can't be blank","Info");
			return false;
		}else if($("#password").val() == ''){
			jAlert("Password can't be blank","Info");
			return false;
		}else if($("#repassword").val() == ''){
			jAlert("Re-Password can't be blank","Info");
			return false;
		}else if($("#password").val() != $("#repassword").val()){
			jAlert("Password and Re-Password must match","Info");
			return false;
		}else if(validateEmail($('#email').val()) == false){
			jAlert("Invalid Email.","Info");
			return false;
		}
		else{
			$.ajax({
					url : "${pageContext.request.contextPath}/CheckUser",
					type : "get",
					data : "userName="+ $("#userName").val() + "&email=" + $("#email").val(),
					
					
					success : function(response){
						if(response.msg == "true"){
							if(roleAffiliateBroker != null && (roleAffiliateBroker == 'ROLE_BROKER' || roleAffiliateBroker == 'ROLE_AFFILIATES')){
								$('#loginForm').attr('action','${pageContext.request.contextPath}/Client/AddUsers');
								$("#loginForm").submit();
							}else{
								$('#loginForm').attr('action','${pageContext.request.contextPath}/Admin/AddUser');
								$("#loginForm").submit();
							}
						}else{
							jAlert(response.msg,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		}
		
	}
	
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    //alert(re.test(email));
	    return re.test(email);
	}
	
	function cancelAction(){
		if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
			window.location.href = "${pageContext.request.contextPath}/Client/ManageBrokers?status=Active";
		}
		else if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_AFFILIATES'){
			window.location.href = "${pageContext.request.contextPath}/Affiliates?status=Active";
		}
		else{
			window.location.href = "${pageContext.request.contextPath}/Admin/ManageUsers?status=Active";
		}
	}
	
	function checkPromoCode(){		
		var promoCode = $("#promoCode").val();
		if(promoCode != null && promoCode != ""){
			$.ajax({
				url : "${pageContext.request.contextPath}/CheckAffiliatePromoCode",
				type : "get",
				data : "promoCode=" + promoCode,
				success : function(response){
					if(response.msg == "true"){
						affiliateFlag = true;
						return true;
					}else{
						jAlert(response.msg, "Info");
						affiliateFlag = false;
						return false;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					affiliateFlag = false;
					return false;
				}
			});
		}else{
			affiliateFlag = true;
			return true;
		}
	}
</script>

<style>
	input{
		color : black !important;
	}
	.required{
		color: red !important;
    }
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Admin</h3>
					<ol class="breadcrumb">
						<c:choose>
							<c:when test="${empty roleAffiliateBroker or roleAffiliateBroker eq 'ROLE_SUPER_ADMIN' or roleAffiliateBroker eq 'ROLE_USER'}">
								<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
								<li><i class="fa fa-laptop"></i>Add User</li>
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
								<li><i class="fa fa-home"></i><a href="#">Broker</a></li>
								<li><i class="fa fa-laptop"></i>Add Broker</li>
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
								<li><i class="fa fa-home"></i><a href="#">Affiliates</a></li>
								<li><i class="fa fa-laptop"></i>Add Affiliates</li>
							</c:when>
						</c:choose>						  	
					</ol>
				</div>
</div>

<div class="row">
<div class="col-lg-12">
<section class="panel">
  <header class="panel-heading">
						<c:choose>
							<c:when test="${empty roleAffiliateBroker or roleAffiliateBroker eq 'ROLE_SUPER_ADMIN' or roleAffiliateBroker eq 'ROLE_USER'}">
								Fill User Details
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
								Fill Broker Details
							</c:when>
							<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
								Fill Affiliates Details
							</c:when>
						</c:choose>	
						
                    </header>
 <div class="panel-body">
							<c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if>
  <form:form role="form" class="form-horizontal" commandName="trackerUser" modelAttribute="trackerUser" id="loginForm" method="post" action="">
<input class="form-control" id="action" value="action" name="action" type="hidden" />
    <div class="form-group">
      <div class="col-sm-5"><label>Username <span class="required">*</span></label><form:input class="form-control" path="userName" id="userName" name="userName" type="text" /></div>
      <div class="col-sm-5"><label>Email <span class="required">*</span></label><form:input class="form-control " path="email" id="email" type="email" name="email"/></div>
    </div>

	<div class="form-group">
      <div class="col-sm-5"><label>First Name <span class="required">*</span></label><form:input class="form-control" path="firstName" id="firstName" name="firstName" type="text"/></div>
      <div class="col-sm-3"><label>Last Name <span class="required">*</span></label><form:input class="form-control" path="lastName" id="lastName" name="lastName" type="text"/></div>
    </div>
    
	<div class="form-group">
      <div class="col-sm-5"><label>Password <span class="required">*</span></label><form:input class="form-control " path="password" id="password" name="password" type="password" /></div>
      <div class="col-sm-5"><label>Re-Password<span class="required">*</span></label><input class="form-control" id="repassword" name="repassword" type="password" /></div>
    </div>

	<div class="form-group">
      <div class="col-sm-5">
      	<label>Phone</label>
      	<form:input class="form-control " path="phone" id="phone" name="phone" maxlength="10" />
      </div>	  
	  <c:if test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
		<div class="col-sm-5">
			<label>Company<span class="required">*</span></label>
			<input class="form-control" id="companyName" name="companyName" type="text" readonly="readonly" value="${brokerInfo.companyName}" />
		</div>
	  </c:if>
	</div>
	<div class="form-group">
		<c:if test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
			<!-- <div class="col-sm-5"><label>Auto Generate <span class="required">*</span></label> 
				<span id="promo_ag_yes"><input class="form-control" id="promo_autoGenerate_yes" name="promoAutoGenerate" type="radio" value="Yes"/>Yes</span>
				<input class="form-control" id="promo_autoGenerate_no" name="promoAutoGenerate" type="radio" value="No"/>No
			</div>
			<div class="col-sm-5"><label>Promotional Code <span class="required">*</span></label> 
				<input class="form-control" id="promocode" name="promocode" type="text" />
			</div> -->
			<div class="col-sm-5"><label>Affiliates Status</label>
				<select class="form-control" name="affiliateStatus" id="affiliateStatus">					
					<option value="GOLD" <c:if test="${affiliateInfo ne null and affiliateInfo.status eq 'GOLD'}">selected </c:if> >Gold</option>
					<option value="PLATINIUM" <c:if test="${affiliateInfo ne null and affiliateInfo.status eq 'PLATINIUM'}">selected </c:if> >Platinium</option>
				</select>
			</div>
			<div class="col-sm-5"><label>Affiliates Repeat Business</label>
				<select class="form-control" name="affiliateBusiness" id="affiliateBusiness">
					<option value="NO" <c:if test="${affiliateInfo ne null and affiliateInfo.repeatBusiness eq 'NO'}">selected </c:if> >No</option>
					<option value="YES" <c:if test="${affiliateInfo ne null and affiliateInfo.repeatBusiness eq 'YES'}">selected </c:if> >Yes</option>
				</select>
			</div>
			<div class="col-sm-5">
				<label>Affiliates cash(RTF Orders) %</label>
				<input class="form-control"  id="affiliateDiscount" name="affiliateDiscount" value="${affiliateInfo.cashDiscount}"/>
			</div>
			<div class="col-sm-5">
				<label>Customer Discount(RTF Orders) %</label>
				<input class="form-control"  id="customerOrderDiscount" name="customerOrderDiscount" value="${affiliateInfo.customerDiscount}" />
			</div>
			<div class="col-sm-5">
				<label>Affiliates cash(Phone Orders) %</label>
				<input class="form-control"  id="phoneAffiliateDiscount" name="phoneAffiliateDiscount" value="${affiliateInfo.phoneCashDiscount}" />
			</div>
			<div class="col-sm-5">
				<label>Customer Discount(Phone Orders) %</label>
				<input class="form-control"  id="phoneCustomerOrderDiscount" name="phoneCustomerOrderDiscount" value="${affiliateInfo.phoneCustomerDiscount}" />
			</div>
			<div class="col-sm-5">
				<label>Effective From Date</label>
				<input class="form-control"  id="fromDate" name="fromDate" value="${affiliateInfo.fromDate}" />
			</div>
			<div class="col-sm-5">
				<label>Effective To Date</label>
				<input class="form-control"  id="toDate" name="toDate" value="${affiliateInfo.toDate}" />
			</div>
			<div class="col-sm-5">
				<label>Promotional Code</label>
				<input class="form-control" id="promoCode" name="promoCode" onblur="checkPromoCode()" value="${affiliateInfo.promoCode}" />
			</div>
			<div class="col-sm-5">
				<label>Can Earn Reward Points</label>
				<select class="form-control" name="canEarnRewardPoints" id="canEarnRewardPoints">
					<option value="YES" <c:if test="${affiliateInfo ne null and affiliateInfo.earnRewardPoints eq 'YES'}">selected </c:if> >Yes</option>
					<option value="NO" <c:if test="${affiliateInfo ne null and affiliateInfo.earnRewardPoints eq 'NO'}">selected </c:if> >No</option>
				</select>
			</div>
		</c:if>
	</div>
	
	<c:if test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
	<div class="form-group">	  
		<div class="col-sm-5">
			<label>Service Fees<span class="required">*</span></label>
			<input class="form-control" id="serviceFees" name="serviceFees" value="${brokerInfo.serviceFees}" />
		</div>
    </div>
	</c:if>
	
	<div class="form-group">
		<label class="control-label col-lg-2" for="inputSuccess">Roles <span class="required">*</span></label>
        <div class="col-lg-10">
		<c:choose>
			<c:when test="${empty roleAffiliateBroker or roleAffiliateBroker eq 'ROLE_SUPER_ADMIN' or roleAffiliateBroker eq 'ROLE_USER' or roleAffiliateBroker eq 'ROLE_CONTEST' or roleAffiliateBroker eq 'ROLE_SELLER'}">
			    <div id="roleDiv">
				 	<label class="">				 		
						<input type="checkbox" name="role" value="role_super_admin"
						<c:if test="${roleAdmin ne null and roleAdmin eq 'ROLE_SUPER_ADMIN'}">checked </c:if> > Super Admin
					</label>
					<label class="">
						<input type="checkbox" name="role" value="role_user" 
						<c:if test="${roleUser ne null and roleUser eq 'ROLE_USER'}">checked </c:if> > User
					</label>
					<label class="">
						<input type="checkbox" name="role" value="role_contest" 
						<c:if test="${roleContest ne null and roleContest eq 'ROLE_CONTEST'}">checked </c:if> >Contest
					</label>
					<label class="">
						<input type="checkbox" name="role" value="role_seller"  id="sellerCheck"
						<c:if test="${roleSeller ne null and roleSeller eq 'ROLE_SELLER'}">checked </c:if> >Seller
					</label><br/><br/>
					<div class="col-sm-5" id="sellerDiv"  style="display:none">
						<label>Seller</label>
						<select class="form-control" name="seller" id="sellerId">
							<option value="-1">-- Select --</option>
							<c:forEach items="${sellerList}" var="seller">
								<option value="${seller.sellerId}"> ${seller.compName} -- ${seller.email}
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</c:when>
			<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_BROKER'}">
				<div id="roleBrokerDiv">
				 	<label>
						Broker <input type="hidden" name="role" value="role_broker" id="brokerRole">
					</label>
				</div>
			</c:when>
			<c:when test="${roleAffiliateBroker ne null and roleAffiliateBroker eq 'ROLE_AFFILIATES'}">
				<div id="roleAffiliateDiv">
				 	<label>
						Affiliates <input type="hidden" name="role" value="role_affiliates">
					</label>
				</div>
			</c:when>
		</c:choose>
		</div>
    </div>

    <div class="form-group">
      <div class="col-sm-6">
        <!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
		<c:if test="${roleUser eq null and roleAdmin eq null and brokerInfo eq null and affiliateInfo eq null and roleSeller eq null}">
			<button class="btn btn-primary" type="button" onclick="doAddUserValidations()">Save</button>
		</c:if>
		<button class="btn btn-default" onclick="cancelAction()" type="button">Cancel</button>
      </div>
    </div>
  </form:form>
</section>
</div>
</div>