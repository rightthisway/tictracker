<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Artists Search</title>
  
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
 .slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.deleteClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.auditClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

</style>
</head>
<body>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Artist</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Artist Search</li>
		</ol>
	</div>
	
	<!-- 
	<div class="col-lg-12">
               
			<form:form class="form-inline" role="form" id="artistSearch" onsubmit="return false" method="post" action="${pageContext.request.contextPath}/RewardTheFan/PopularArtist">
			
				<div class="col-sm-1"> </div>
				
				<div class="col-sm-2">
					<select id="searchType" name="searchType" class="form-control input-sm m-bot15" style = "width:100%">
 					  <option value="artistName">Artist</option>
					  <option value="grandChildCategoryName" <c:if test="${grandChildCategoryName eq 'artistName'}">selected</c:if> >GrandChildCategory</option>
					  <option value="childCategoryName" <c:if test="${searchType eq 'childCategoryName'}">selected</c:if> >ChildCategory</option>
					  <option value="parentCategoryName" <c:if test="${searchType eq 'parentCategoryName'}">selected</c:if> >ParentCategory</option>
					</select>
				</div>
               <div class="col-sm-3">
				<div class="navbar-form">
					<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
				</div>
				</div>
				</form:form>
				<div class="col-sm-3">				 
				 	<button type="button" class="btn btn-primary" onclick="getArtistGridData(0);">search</button>
				 </div>
				 <div class="col-sm-1"> </div>
               
                               
           </div>
            -->
            <input type="hidden" id="productId" name="productId" value="${product.id}" />
</div>


<!-- artist Grid Code Starts -->
<div style="position: relative" id="artistGridDiv">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Artist Details</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="artist_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="artist_pager" style="width: 100%; height: 10px;"></div>
		
	</div>
</div>
	

		<div class="full-width mb-20 full-width-btn" id="actionButton">
			<button type="button" class="btn btn-primary" onclick="updateArtist('true')">Make Visible</button>
			<button type="button" class="btn btn-primary" onclick="updateArtist('false')">Make Invisible</button>
		</div>
<script>

$(document).ready(function() {
	getArtistGridData(0);
	$('#artistSearch').submit(function(){
		e.preventDefault();
	});
	
	$('#searchValue').keypress(function (event) {
		
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getArtistGridData(0);
		  }
	});
});

function exportToExcel(){
	var appendData = "headerFilter="+artistSearchString;
    var url = apiServerUrl + "ManageArtistExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	artistSearchString='';
	columnFilters = {};
	getArtistGridData(0);
}

var artistCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	var pagingInfo;
	var artistDataView;
	var artistGrid;
	var artistData = [];
	var artistGridPager;
	var artistSearchString='';
	var columnFilters = {};
	var userArtistColumnsStr = '<%=session.getAttribute("artistsearchgrid")%>';
	var userArtistColumns =[];
	var allArtistColumns = [ artistCheckboxSelector.getColumnDefinition(),
	{id: "artist", 
		name: "Artist", 
		field: "artist",
		width:80,		
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,			
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	}, {
		id : "visibleInSearch",
		field : "visibleInSearch",
		name : "Visible / In Visible",
		width:80,
		sortable: true
	} ];

	if(userArtistColumnsStr!='null' && userArtistColumnsStr!=''){
		columnOrder = userArtistColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allArtistColumns.length;j++){
				if(columnWidth[0] == allArtistColumns[j].id){
					userArtistColumns[i] =  allArtistColumns[j];
					userArtistColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userArtistColumns = allArtistColumns;
	}
	
	var artistOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var artistGridSortcol = "artistName";
	var artistGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var artistGridSearchString = "";
	
	function artistGridComparer(a, b) {
		var x = a[artistGridSortcol], y = b[artistGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*function filter(item) {
	var artistSearchString='';
    for (var columnId in columnFilters) {
      if (columnId !== undefined && columnFilters[columnId] !== "") {
		  artistSearchString += columnId + ":" +columnFilters[columnId]+",";
      }
    }
	getArtistGridData(0,artistSearchString);
	return true;
   
  }*/
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getArtistGridData(pageNo);
	}
	
	function getArtistGridData(pageNo) {
		var searchValue = $("#searchValue").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/GetArtist",
			type : "post",
			dataType: "json",
			data : "searchValue="+searchValue+"&searchType="+$("#searchType").val()+"&pageNo="+pageNo+"&headerFilter="+artistSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Artist found.");
				} 
				pagingInfo = JSON.parse(jsonData.artistPagingInfo);
				refreshartistGridValues(jsonData);
				clearAllSelections();
				$('#artist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
function refreshartistGridValues(jsonData) {
	 $("div#divLoading").addClass('show');
		artistData = [];
		if(jsonData!=null && jsonData.artistList !=null ){
			for (var i = 0; i < jsonData.artistList.length; i++) {
				var  data= jsonData.artistList[i]; 
				var d = (artistData[i] = {});
				d["artistId"] = data.artistId;
				d["id"] =  i;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				d["artist"] = data.artistName;
				d["visibleInSearch"] = data.displayOnSearch;
			}
		}

		artistDataView = new Slick.Data.DataView();
		artistGrid = new Slick.Grid("#artist_grid", artistDataView, userArtistColumns, artistOptions);
		artistGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		artistGrid.setSelectionModel(new Slick.RowSelectionModel());
		artistGrid.registerPlugin(artistCheckboxSelector);
		if(jsonData!=null){
			artistGridPager = new Slick.Controls.Pager(artistDataView, artistGrid, $("#artist_pager"), JSON.parse(jsonData.artistPagingInfo));
		}
		var artistGridColumnpicker = new Slick.Controls.ColumnPicker(allArtistColumns, artistGrid,
				artistOptions);

		
		artistGrid.onSort.subscribe(function(e, args) {
			artistGridSortdir = args.sortAsc ? 1 : -1;
			artistGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				artistDataView.fastSort(artistGridSortcol, args.sortAsc);
			} else {
				artistDataView.sort(artistGridComparer, args.sortAsc);
			}
		});
		// wire up model artists to drive the artistGrid
		artistDataView.onRowCountChanged.subscribe(function(e, args) {
			artistGrid.updateRowCount();
			artistGrid.render();
		});
		artistDataView.onRowsChanged.subscribe(function(e, args) {
			artistGrid.invalidateRows(args.rows);
			artistGrid.render();
		});
		$(artistGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			artistSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  artistSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getArtistGridData(0);
				}
			  }
		 
		});
		artistGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(columnFilters[args.column.id])
			   .appendTo(args.node);
			}
			
		});
		artistGrid.init();
		// initialize the model after all the artists have been hooked up
		artistDataView.beginUpdate();
		artistDataView.setItems(artistData);
		//artistDataView.setFilter(filter);
		artistDataView.endUpdate();
		artistDataView.syncGridSelection(artistGrid, true);
		artistGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		 $("div#divLoading").removeClass('show');
	}
	
	function getSelectedartistGridId() {
		var temprartistRowIndex = artistGrid.getSelectedRows();
		
		var artistIdStr='';
		$.each(temprartistRowIndex, function (index, value) {
			artistIdStr += ','+artistGrid.getDataItem(value).artistId;
		});
		
		if(artistIdStr != null && artistIdStr!='') {
			artistIdStr = artistIdStr.substring(1, artistIdStr.length);
			 return artistIdStr;
		}
	}
	
	function updateArtist(display) {
		var artistIds  = getSelectedartistGridId();
		if(artistIds=='' || artistIds.length==0){
			jAlert("Please Select artist to update.");
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/RewardTheFan/ManageArtistForSearch",
				type : "post",
				data : "action=update&display="+display+"&artistIds="+ artistIds,
				success : function(res){
					jAlert("Selected Artist Updated successfully.");
					artistGrid.setSelectedRows([]);
					clearAllSelections();
					getArtistGridData(0);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
	}
	

	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  artistGrid.resizeCanvas();
	});
	
	function saveUserArtistPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = artistGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('artistsearchgrid',colStr);
	}
	
	//call functions once page loaded
	window.onload = function() {		
		//refreshartistGridValues(null);
		$('#artist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
		enableMenu();
	};
	
</script>
	
</body>
</html>