<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
	
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">
		
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../resources/js/jquery.alerts.js"></script>

	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>	

	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	
<script type="text/javascript">

var roleAffiliateBroker = "${roleAffiliateBroker}";

$(document).ready(function(){	
	
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	if($("#promotionalCode").val() != ''){
		$('#promoCode').show();		
	}else{
		$('#promoCode').hide();
	}
	
	if(${rolesList.contains('ROLE_BROKER') == true}){
	//if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
		$('#companyName').removeAttr('readonly');
	}else{
		$('#companyName').attr('readonly','readonly');
	}
		
});

	window.onunload = function () {
        var win = window.opener;
        if (!win.closed) {
			if(${rolesList.contains('ROLE_AFFILIATES') == true}){
				window.opener.getAffiliatesAllGridData(0);
			}else{
				window.opener.getUserGridData(0);
			}
        }
    };
	
	function doEditUserValidations(){
		var roles = $("input[name=role]");
		var flag = false;
		
		if(${rolesList.contains('ROLE_BROKER') == true}){
		//if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_BROKER'){
			if($("#companyName").val() == ''){
				jAlert("Invalid Company.","Info");
				return false;
			}
			if($("#serviceFees").val() == ''){
				jAlert("Service Fees can't be blank","Info");
				return false;
			}
		}
		else if(${rolesList.contains('ROLE_AFFILIATES') == true}){
		//else if(roleAffiliateBroker != null && roleAffiliateBroker == 'ROLE_AFFILIATES'){
		}
		else{
			$.each(roles, function(index, obj){
				if($(obj).attr('checked') == 'checked'){
					flag = true;
				}
			});
			if(!flag){
				jAlert('Please choose at least one Role.',"Info");
				return false;
			}
		}
		
		if($("#firstName").val() == ''){
			jAlert("Firstrname can't be blank","Info");
			return false;
		}else if($("#lastName").val() == ''){
			jAlert("Lastname can't be blank","Info");
			return false;
		}else if($("#email").val() == ''){
			jAlert("Email can't be blank","Info");
			return false;
		}else if(validateEmail($('#email').val()) == false){
			jAlert("Invalid Email.","Info");
			return false;
		}else{
			$.ajax({
					url : "${pageContext.request.contextPath}/CheckUserForEdit",
					type : "get",
					data : "userName="+ $("#userName").val() + "&email=" + $("#email").val() + "&userId=" + '${userId}',
					/* async : false, */
					
					success : function(response){
						if(response.msg == "true"){
							$("#action").val('editUserInfo');
							if(${rolesList.contains('ROLE_BROKER') == true}){
								$('#editUserForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
								$("#editUserForm").submit();
							}else if(${rolesList.contains('ROLE_AFFILIATES') == true}){
								$('#editUserForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
								$("#editUserForm").submit();
							}else{
								$('#editUserForm').attr('action','${pageContext.request.contextPath}/Admin/EditUser');
								$("#editUserForm").submit();
							}
							//async:false,
							 //url = "${pageContext.request.contextPath}/Admin/ManageUsers";
							 //window.close();
						}else{
							jAlert(response.msg,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		}
		
	}
	
	function doChangePasswordValidations(){
		if($("#password").val() == ''){
			jAlert("Password can't be blank","Info");
			return false;
		}else if($("#repassword").val() == ''){
			jAlert("Re-Password can't be blank","Info");
			return false;
		}else if($("#password").val() != $("#repassword").val()){
			jAlert("Password and Re-Password must match","Info");
			return false;
		}else{
			$("#actionChnagepassword").val('changePassword');
			if(${rolesList.contains('ROLE_BROKER') == true}){
				$('#changePasswordForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
				$("#changePasswordForm").submit();
			}else if(${rolesList.contains('ROLE_AFFILIATES') == true}){
				$('#changePasswordForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
				$("#changePasswordForm").submit();
			}else{
				$('#changePasswordForm').attr('action','${pageContext.request.contextPath}/Admin/EditUser');
				$("#changePasswordForm").submit();
			}			
		}
	}
	
	function logOutAction(){
		$("#").val('logoutUser');
		$("#Form").submit();
	}
	
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    //jAlert(re.test(email));
	    return re.test(email);
	}
	
</script>

<style>
label,header,input {
  font-family: arial;
  font-size: 14px;
}
	input{
		color : black !important;
	}

#regenerate, #viewPromoCode{
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Admin</h3>
					<ol class="breadcrumb">
						<c:choose>
							<c:when test="${rolesList.contains('ROLE_BROKER') == true}">
								<li><i class="fa fa-home"></i><a href="#">Broker</a></li>
								<li><i class="fa fa-laptop"></i>Edit Broker</li>
							</c:when>
							<c:when test="${rolesList.contains('ROLE_AFFILIATES') == true}">
								<li><i class="fa fa-home"></i><a href="#">Affiliates</a></li>
								<li><i class="fa fa-laptop"></i>Edit Affiliates</li>
							</c:when>
							<c:when test="${rolesList.contains('ROLE_BROKER') != true and rolesList.contains('ROLE_AFFILIATES') != true}">
								<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
								<li><i class="fa fa-laptop"></i>Edit User</li>
							</c:when>
						</c:choose> 
												  	
					</ol>
					
				</div>
</div>

<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
							<c:choose>
								<c:when test="${rolesList.contains('ROLE_BROKER') == true}">
									Fill Broker Details
								</c:when>
								<c:when test="${rolesList.contains('ROLE_AFFILIATES') == true}">
									Fill Affiliates Details
								</c:when>
								<c:when test="${rolesList.contains('ROLE_BROKER') != true and rolesList.contains('ROLE_AFFILIATES') != true}">
									Fill User Details
								</c:when>
							</c:choose>                              
                          </header>
                          <div class="panel-body">
                              <div class="form">
                              <c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if>
                                  <form:form class="form-validate form-horizontal" commandName="trackerUser" modelAttribute="trackerUser" id="editUserForm" method="post" action="" >
                                      <input class="form-control" id="action" value="action" name="action" type="hidden" />
                                      <input class="form-control" id="userId" value="${userId}" name="userId" type="hidden" />
									  <input class="form-control" id="status" value="${status}" name="status" type="hidden" />
                                       <div class="form-group">
											<div class="full-width">
												  <label for="cname" class="control-label col-lg-1">User Name</label>
												  <form:input type="text" path="userName" id="userName" name="userName" readonly="readonly" style="width: 160px; font-weight: bold; border:none; background: transparent;" />
											</div>
											<div class="full-width">
											  <div id="promoCode" style="display:none;">
												  <label for="cname" class="control-label col-lg-1">Promotional Code</label>
												  <form:input type="hidden" path="promotionalCode" id="promotionalCode" name="promotionalCode" readonly="readonly" style="width: 160px; font-weight: bold; border:none; background: transparent;" />
												  <a style="margin-top: -0.8%; margin-left: 1%;" class="btn btn-primary" id="viewPromoCode" href="#"	title="View History" onclick="javascript:viewHistory();">View</a>
												  <c:if test="${status eq 'true'}">
														<a style="margin-top: -0.8%; margin-left: 1%;" class="btn btn-primary" id="regenerate" href="#"	title="Regenerate" onclick="javascript:getDateForPromoCode();">Regenerate</a>												  
												  </c:if>
											  </div>
											 </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">First Name <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <form:input class="form-control" path="firstName" id="firstName" name="firstName" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Last Name <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <form:input class="form-control" path="lastName" id="lastName" name="lastName" type="text"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cemail" class="control-label col-lg-2">E-Mail <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <form:input class="form-control " path="email" id="email" type="email" name="email"/>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="ccomment" class="control-label col-lg-2">Phone </label>
                                          <div class="col-lg-10">
                                              <form:input class="form-control " path="phone" id="phone" name="phone" maxlength="10" />
                                          </div>
                                      </div>                                      
									
									  <c:if test="${rolesList.contains('ROLE_BROKER') == true}">
                                      <div class="form-group ">
                                          <label for="ccomment" class="control-label col-lg-2">Company </label>
                                          <div class="col-lg-10">
											  <label for="ccomment" class="control-label col-lg-2"><c:if test="${trackerBroker ne null}"> ${trackerBroker.companyName}</c:if></label>
                                              <input class="form-control" type="hidden" id="companyName" value='<c:if test="${trackerBroker ne null}"> ${trackerBroker.companyName}</c:if>' name="companyName" />
                                          </div>
                                      </div>
									  
									  <div class="form-group">
											<label for="ccomment" class="control-label col-lg-2">Service Fees</label>
											<div class="col-lg-10">
												<input class="form-control" id="serviceFees" name="serviceFees" type="text" value='<c:if test="${trackerBroker ne null}"> ${trackerBroker.serviceFees}</c:if>'/>
											</div>
									  </div>
									  </c:if>
									  
                                      <div class="form-group">
                                      <label class="control-label col-lg-2" for="inputSuccess">Roles</label>
                                      <div class="col-lg-10">
                                      	<c:choose>
                                      		<c:when test="${rolesList.contains('ROLE_BROKER') == true}">
                                      			<input type="hidden" id="brokerRole" name="role" value="role_broker">
												 <label for="ccomment" class="control-label col-lg-2">Broker</label>
                                      		</c:when>
                                      		<c:when test="${rolesList.contains('ROLE_AFFILIATES') == true}">
                                      			<input type="hidden" id="affiliateRole" name="role" value="role_affiliates">
												 <label for="ccomment" class="control-label col-lg-2">Affiliates</label>
                                      		</c:when>
                                      		<c:when test="${rolesList.contains('ROLE_BROKER') != true and rolesList.contains('ROLE_AFFILIATES') != true}">
                                      			  <label class="checkbox-inline">
		                                              <input type="checkbox" <c:if test="${rolesList.contains('ROLE_SUPER_ADMIN') == true}">checked</c:if> name="role" value="role_super_admin"> Super Admin
		                                          </label>
		                                          <label class="checkbox-inline">
		                                              <input type="checkbox" <c:if test="${rolesList.contains('ROLE_USER') == true}">checked</c:if> name="role" value="role_user"> User
		                                          </label>
                                      		</c:when>
                                      	</c:choose>                                      	
                                      </div>
                                  	  </div>
                                      
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="button" onclick="doEditUserValidations()">Update</button>
                                          </div>
                                      </div>
                                  </form:form>
                              </div>

                          </div>
                          
                          <header class="panel-heading">
                              Change Password
                          </header>
                          <div class="panel-body">
                              <div class="form">
                              
                                  <form class="form-validate form-horizontal" id="changePasswordForm" method="post" action="" >
                                      <input class="form-control" id="actionChnagepassword" value="action" name="action" type="hidden" />
                                      <input class="form-control" id="userId" value="${userId}" name="userId" type="hidden" />
                                      <div class="form-group ">
                                          <label for="curl" class="control-label col-lg-2">Password <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="password" name="password" type="password" />
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Re-Password <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="repassword" name="repassword" type="password" />
                                          </div>
                                      </div>                                      
                                      
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="button" onclick="doChangePasswordValidations()">Update Password</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>

                          </div>
							
						<%-- <header class="panel-heading">
                              Actions
                          </header>
						  <div class="form-group">
						  	 <div class="form">
								<br/><br/>
							<form class="form-validate form-horizontal" id="Form" method="post" action="${pageContext.request.contextPath}/Admin/EditUser" >
								<input class="form-control" id="" value="action" name="action" type="hidden" />
								<input class="form-control" id="userId" value="${userId}" name="userId" type="hidden" />
								<div class="col-lg-offset-2 col-lg-10">
									<button class="btn btn-primary" type="button" onclick="logOutAction()">Logout User</button>
								</div>
							</form>
							</div>
                          </div> --%>
                      </section>
                  </div>
              </div>

			  <!-- Toggle add Modal-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		  <div class="modal-content">
			  <div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">Add Dates</h4>
			  </div>
			  <div class="modal-body">					
				   <form role="form" class="form-horizontal" id="" method="post" action="">
						<div class="form-group">
							<div class="col-sm-4">
								<label for="name" class="control-label">From Date</label>
								<input class="form-control" type="text" id="fromDate" name="fromDate">
							</div>
							<div class="col-sm-4">
								<label for="name" class="control-label">To Date</label>
								<input class="form-control" type="text" id="toDate" name="toDate">
							</div>
						</div>
								 <!--  </div> -->
						<div class="modal-footer">
						  <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
						  <button class="btn btn-success" type="button" onclick="regeneratePromoCode();">Save</button>
						</div>
				   </form>
			  </div>
		  </div>
	  </div>
	</div>
	
<script type="text/javascript">

var fromDate;
var toDate;
	function getDateForPromoCode(){
		$('#myModal').modal('show');
		fromDate = $('#fromDate').val();
		toDate = $('#toDate').val();
	}
	
	function regeneratePromoCode(){
		$('#myModal').modal('hide');
		fromDate = $('#fromDate').val();
		toDate = $('#toDate').val();
		var userId = $('#userId').val();
		if(toDate != '' || toDate != null){
			$.ajax({
				url : "${pageContext.request.contextPath}/GenerateAffiliatePromoCode",
				type : "post",
				data : "userId="+userId+"&fromDate="+fromDate+"&toDate="+toDate,
				success : function(response){
					jAlert("Promotional Code Generated.");
					$('#promotionalCode').val(response);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function viewHistory(){
		var userId = $('#userId').val();
		var viewHistoryUrl = "${pageContext.request.contextPath}/GetAffiliatePromoHistory?userId="+userId;
		popupCenter(viewHistoryUrl, 'Promotional Code History', '700', '500');
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {  
	    // Fixes dual-screen position                         Most browsers      Firefox  
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
	              
	    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
	    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
	              
	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
	  
	    // Puts focus on the newWindow  
	    if (window.focus) {  
	        newWindow.focus();  
	    }  
	}
	
</script>