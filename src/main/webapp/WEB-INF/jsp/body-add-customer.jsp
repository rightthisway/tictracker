<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript">
	function doCustomerValidations(){
		if($('#name').val()==''){
			jAlert("First Name is Mendatory.","Info");
			return false;
		}else if($('#lastName').val()==''){
			jAlert("Last Name is Mendatory.","Info");
			return false;
		}else if($('#email').val()==''){
			jAlert("Email is Mendatory.","Info");
			return false;
		}else if($('#phone').val()==''){
			jAlert("Phone is Mendatory.","Info");
			return false;
		}else if($('#blFirstName').val()==''){
			jAlert("Billing address - First Name is Mendatory.","Info");
			return false;
		}else if($('#blLastName').val()==''){
			jAlert("Billing address - Last Name is Mendatory.","Info");
			return false;
		}else if($('#street1').val()==''){
			jAlert("Billing address - Street1 is Mendatory.","Info");
			return false;
		}else if($('#city').val()==''){
			jAlert("Billing address - City is Mendatory.","Info");
			return false;
		}else if($('#countryName').val() < 0){
			jAlert("Billing address - Country is Mendatory.","Info");
			return false;
		}else if($('#stateName').val() < 0){
			jAlert("Billing address - State is Mendatory.","Info");
			return false;
		}else if($('#zipCode').val()==''){
			jAlert("Billing address - ZipCode is Mendatory.","Info");
			return false;
		}else if($('#shFirstName').val()==''){
			jAlert("Shipping Address - First Name is Mendatory.","Info");
			return false;
		}else if($('#shLastName').val()==''){
			jAlert("Shipping Address - Last Name is Mendatory.","Info");
			return false;
		}else if($('#shStreet1').val()==''){
			jAlert("Shipping Address - Street1 is Mendatory.","Info");
			return false;
		}else if($('#shCity').val()==''){
			jAlert("Shipping Address - City is Mendatory.","Info");
			return false;
		}else if($('#shCountryName').val() < 0){
			jAlert("Shipping address - Country is Mendatory.","Info");
			return false;
		}else if($('#shStateName').val() < 0){
			jAlert("Shipping address - State is Mendatory.","Info");
			return false;
		}else if($('#shZipCode').val()==''){
			jAlert("Shipping Address - ZipCode is Mendatory.","Info");
			return false;
		}
		
		$.ajax({
					url : "${pageContext.request.contextPath}/CheckCustomer",
					type : "get",
					data : "userName="+ $("#email").val(),
					/* async : false, */
					success : function(response){
						if(response == "true"){
							$("#addCustomer").submit();
						}else{
							jAlert(response,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		
	}

	function loadState(id,country){
		var countryId =0;
		if(country!=''){
			countryId=country;
		}else if(id=='shStateName'){
			countryId = $('#shCountryName').val();
		}else if(id=='stateName'){
			countryId = $('#countryName').val();
		}
		
		if(countryId>0){
			$.ajax({
				url : "${pageContext.request.contextPath}/GetStates",
				type : "post",
				dataType:"json",
				data : "countryId="+countryId,
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					updateStateCombo(jsonData,id,country);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$('#'+id).empty();
			$('#'+id).append("<option value='-1'>--select--</option>");
		}
	}

	function updateStateCombo(jsonData,id,country){
		$('#'+id).empty();
		$('#'+id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				$('#'+id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
			}
			if(country!=''){
				$("#shStateName").val($("#stateName").val());
			}
		}
	}
	
	function getStateForCountry(){
		var country = $("#countryId").val();
		jAlert(country,"Info");
		$.ajax({
					  
					url : "${pageContext.request.contextPath}/getStates",
					type : "get",
					data : "country="+ $("#countryId").val(),
					dataType:"application/json",
					/* async : false, */
					success : function(res){
						var jsonData = JSON.parse(res);
						jAlert(jsonData,"Info");
					}, error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
    }

	
	//Copy billing address
	function copyBillingAddress(){
		var flag=false;
		if($("#isBilling").prop('checked') == true){
			loadState('shStateName',$("#countryName").val());
			$("#shFirstName").val($("#blFirstName").val());
			$("#shLastName").val($("#blLastName").val());
			$("#shStreet1").val($("#street1").val());
			$("#shStreet2").val($("#street2").val());
			$("#shCountryName").val($("#countryName").val());
			$("#shCity").val($("#city").val());
			
			$("#shZipCode").val($("#zipCode").val());	
			
		}else{
			//"Not checked
		}
		
	}

	//Customer cancel action
	function cancelAction(){
		window.location.href = "${pageContext.request.contextPath}/Client/ManageDetails";
		closeWindow();
	}
</script>

<style>
input {
	color: black !important;
}
.required{
		color: red !important;
    }
</style>


<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Customers
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="${pageContext.request.contextPath}/Client/ManageDetails">Manage Details</a>
			</li>
			<li><i class="fa fa-laptop"></i>Add Customer</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header class="panel-heading">
		Fill Customer Info </header>
		<div class="panel-body">
							  <c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if>
			<form:form role="form" class="form-horizontal" commandName="customer" modelAttribute="customer" id="addCustomer" method="post" action="${pageContext.request.contextPath}/Client/AddCustomer">
				<input id="action" value="action" name="action" type="hidden" />
				<input id="isPopup" value="${isPopup}" name="isPopup" type="hidden" />
				<div class="form-group">
					<div class="col-sm-3">
						<label>Type <span class="required">*</span> </label>
						<form:select path="customerType" name="customerType"
							class="form-control input-sm m-bot15">
							<form:option value="PHONE_CUSTOMER">Phone Customer</form:option>
							<form:option value="WEB_CUSTOMER">Web Customer</form:option>
							<form:option value="RETAIL_CUSTOMER">Retail Customer</form:option>
							<form:option value="BROKER">Broker</form:option>
							<form:option value="VENDOR">Vendor</form:option>
							<form:option value="CORPORATE">Corporate</form:option>
							<form:option value="CONSUMER">Consumer</form:option>
							<form:option value="EMPLOYEE">Employee</form:option>
							<form:option value="INTERNAL_USE_ONLY">Internal Use Only</form:option>
							<form:option value="EXCHANGES">Exchanges</form:option>
							<form:option value="EBAY_CUSTOMER">Ebay Customer</form:option>
							<form:option value="DIVISIONS">Divisions</form:option>
							<form:option value="EBAY_ESL">Ebay ESL</form:option>
							<form:option value="EBAY_CAT">Ebay CAT</form:option>
							<form:option value="CORPORATION">Corporation</form:option>
							<form:option value="AFFILIATE">Affiliate</form:option>
							<form:option value="EBAY_ZONES">Ebay Zones</form:option>
							<form:option value="AO_WEB_CUSTOMER">AO Web Customer</form:option>
							<form:option value="EBAY_RATA">Ebay RATA</form:option>
							<form:option value="TND_ZONES">TND Zones</form:option>
						</form:select>
					</div>
					<div class="col-sm-3">
						<label>Client/Broker <span class="required">*</span> </label>
						<select name="clientBrokerType"
							class="form-control input-sm m-bot15" id="clientBrokerType">
							<option value="client">Client</option>
							<option value="broker">Broker</option>
							<option value="both">Both</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label>Customer status <span class="required">*</span> </label>
						<form:select path="customerLevel" name="customerLevel"
							class="form-control input-sm m-bot15">
							<form:option value="GOLD">Gold</form:option>
							<form:option value="SILVER">Silver</form:option>
							<form:option value="PLATINUM">Platinum</form:option>
						</form:select>

					</div>

					<div class="col-sm-3">
						<label> Signup Type <span class="required">*</span> </label>
						<form:select path="signupType" name="signupType" class="form-control input-sm m-bot15">
							<form:option value="REWARDTHEFAN">REWARD THE FAN</form:option>
							<form:option value="FACEBOOK">Facebook</form:option>
							<form:option value="GOOGLE">Google</form:option>
							<!--<form:option value="phone">Phone</form:option>-->
						</form:select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
					<div class="col-sm-5">
						<label>First Name<span class="required">*</span> </label>
						<form:input class="form-control" path="customerName" id="name" name="customerName" />
					</div>
					<div class="col-sm-5">
						<label>Last Name <span class="required">*</span></label>
						<form:input class="form-control" path="lastName" id="lastName" name="lastName" />
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
					<div class="col-sm-5">
						<label>E-Mail <span class="required">*</span>
						</label><form:input class="form-control" path="email" id="email" type="email" name="email" />
					</div>
					<div class="col-sm-5">
						<label>Phone <span class="required">*</span> </label> <form:input
							class="form-control" path="phone" id="phone" name="phone" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Ext</label> <form:input class="form-control" path="extension" id="fedexNo"
							name="extension" />
					</div>
					<div class="col-sm-5">
						<label>Other Phone </label> <form:input class="form-control" path="otherPhone" id="phone"
							name="phone" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Representative </label> <form:input class="form-control" path="representativeName" id="representativeName"
							name="representativeName" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Company Name </label> <form:input class="form-control" path="companyName" id="companyName"
							name="companyName" type="text" />
					</div>
				</div>

				<header class="panel-heading"> Billing Address </header>
				<div class="form-group">
					<div class="col-sm-5">
						<label>First Name <span class="required">*</span></label> <input class="form-control"
							id="blFirstName" name="blFirstName" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Last Name <span class="required">*</span></label>
					    <input class="form-control" id="blLastName" name="blLastName" type="text" />
					</div>

					<div class="col-sm-5">
						<label>Street1 <span class="required">*</span></label>
					    <input class="form-control" id="street1" type="text" name="addressLine1" value="" />
					</div>
					<div class="col-sm-5">
						<label>Street2</label>
						<input class="form-control" id="street2" type="text" name="addressLine2" />
					</div> 
					<div class="col-sm-5">
						<label>Country <span class="required">*</span></label>		
						<select id="countryName" name="countryName" class="form-control input-sm m-bot15" onchange="loadState('stateName','')">
							<option value="-1">--select--</option>
							<c:forEach var="country" items="${countries}">
         						<option value="${country.id}"><c:out value="${country.name}"/></option>
     						 </c:forEach>
						</select>
					</div>
					<div class="col-sm-5">
						<label>State <span class="required">*</span></label>
						<select id="stateName" name="stateName" class="form-control input-sm m-bot15" onchange="">
							 <option value="-1">--select--</option>
							<%--  <c:forEach var="state" items="${states}">
         						<option value="${state.id}"><c:out value="${state.name}"/></option>
     						 </c:forEach> --%>
						</select>
					</div>	
					<div class="col-sm-5">
						<label>City <span class="required">*</span></label>
						<input class="form-control" id="city" type="text" name="city" />
					</div>
					<div class="col-sm-5">
						<label>Zip Code <span class="required">*</span></label>		
						<input class="form-control" id="zipCode" type="text" name="zipCode" />
					</div>
				</div>
				<header class="panel-heading"> 
					Shipping Address
					<input name="isBilling" id="isBilling" value="" type="checkbox" onclick = "copyBillingAddress();"/> 
					Same as billing address
				</header>
				<div class="form-group">
					<div class="col-sm-5">
						<label>First Name <span class="required">*</span></label> <input class="form-control"
							id="shFirstName" name="shFirstName" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Last Name <span class="required">*</span></label>
					    <input class="form-control" id="shLastName" name="shLastName" type="text" />
					</div>

					<div class="col-sm-5">
						<label>Street1<span class="required">*</span></label>
					    <input class="form-control" id="shStreet1" type="text" name="shStreet1" />
					</div>
					<div class="col-sm-5">
						<label>Street2</label>
						<input class="form-control" id="shStreet2" type="text" name="shStreet2" />
					</div>
					<div class="col-sm-5">
						<label>Country <span class="required">*</span></label>		
						<select id="shCountryName" name="shCountryName" class="form-control input-sm m-bot15" onchange="loadState('shStateName','')">
							<option value="-1">--select--</option>
							<c:forEach var="country" items="${countries}">
         						<option value="${country.id}"><c:out value="${country.name}"/></option>
     						 </c:forEach>
						</select>
					</div>
					<div class="col-sm-5">
						<label>State <span class="required">*</span></label>
						<select id="shStateName" name="shStateName" class="form-control input-sm m-bot15" onchange="">
							 <option value="-1">--select--</option>
							 <%-- <c:forEach var="state" items="${states}">
         						<option value="${state.id}"><c:out value="${state.name}"/></option>
     						 </c:forEach> --%>
						</select>
						<!--<input class="form-control " id="state" type="text" name="state" /> -->
					</div>	
					<div class="col-sm-5">
						<label>City <span class="required">*</span></label>
						<input class="form-control " id="shCity" type="text" name="shCity" />
					</div>
					<div class="col-sm-5">
						<label>Zip Code <span class="required">*</span></label>		
						<input class="form-control " id="shZipCode" type="text" name="shZipCode" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
						<button class="btn btn-primary" type="button" onclick="doCustomerValidations();">Save</button>
						<button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
					</div>
				</div>
			</form:form>
		</div>
		</section>
	</div>
</div>