<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Users</title>
 <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
  <script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
  
  <style>
    .cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
     .slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }
    
#addUser, #active, #inactive {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}
</style>
<script>
$(window).load(function() {
	var trackerBrokerList = ${trackerBrokerList};
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));	var trackerBrokerList = ${trackerBrokerList};
	if(trackerBrokerList != null && trackerBrokerList.length > 0){
		refreshUserGridValues(JSON.parse(JSON.stringify(${trackerBrokerList})));
	}
	$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAdminPreference()'>");		
});
function saveUserAdminPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('brokergrid',colStr);
	}
$('#menuContainer').click(function(){
  if($('.ABCD').length >0){
	   $('#menuContainer').removeClass('ABCD');
  }else{
	   $('#menuContainer').addClass('ABCD');
  }
  grid.resizeCanvas();
});

function userStatus(status) {
	var tempUserRowIndex = grid.getSelectedRows([0])[0];
	if(tempUserRowIndex >= 0) {
		var userId = grid.getDataItem(tempUserRowIndex).id;
		if(userId==null || userId == 0){
			jAlert("Please select a user to update status.");
		}else{			
			$.ajax({				  
				url : "${pageContext.request.contextPath}/Client/UserStatusChange",
				type : "post",
				data : "status="+status+"&userId="+userId,
				success : function(response){
					if (response.msg == "true") {
						jAlert("User Updated Successfully.","Info");
						getUserGridData(0);
						return true;
					} else {
						jAlert(response.msg,"Info","Info");
						return false;
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
			}
	} else {
		jAlert("Select a user to update status.");
	}
}
</script>
</head>
<body>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Admin
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Broker</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Brokers</li>
		</ol>

	</div>
</div>

<div style="position: relative">
	<div class="full-width">
		<!-- Add user for light box-->
		<!-- <a style="margin-top: -0.8%; margin-left: 0%;" class="btn btn-primary"
			id="addUser" href="javascript:void(0)"
			onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"
			title="Add User">Add User</a>
			<br/><br/> -->
		<a style="margin-top: -0.8%; margin-left: 0%;" class="btn btn-primary"
			id="addUser" href="${pageContext.request.contextPath}/Client/AddUsers?role=role_broker"
			title="Add User">Add Broker</a>
			<!--
			<a style="margin-top: -0.8%; margin-left: 2%;" class="btn btn-primary"
			id="active" href="#"
			title="Active" onclick="userStatus('ACTIVE');">Active</a>
			
			<a style="margin-top: -0.8%; margin-left: 2%;" class="btn btn-primary"
			id="inactive" href="#"
			title="In Active" onclick="userStatus('INACTIVE');">In Active</a>
			
			<br/><br/>
			
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Manage Brokers</label>
				<div class="pull-right">
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
				</div>
			</div>
			<div id="myGrid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
			<div id="pager" style="width: 100%; height: 20px;"></div>
		</div>-->
	</div>
	
	<div class="full-width">
		<section class="invoice-panel panel">
		<ul class="nav nav-tabs" style="">
			<li id="activeTab" class=""><a id="activeBroker" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeDiv">Active</a></li>
			<li id="inactiveTab" class=""><a id="inactiveBroker" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveDiv">Inactive</a>
		</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content"> <input type="hidden" value="" id="status" name="status" />
			<div id="activeDiv" class="tab-pane">
				<c:if test="${status =='Active'}">
				<div class="full-width mb-20 full-width-btn">
					<button type="button" class="btn btn-primary" onclick="userStatus('INACTIVE');">In Active</button>
				</div>
				<br />	
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Manage Brokers</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="myGrid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="pager" style="width: 100%; height: 20px;"></div>
				</div>
				</c:if>
			</div>
			
			<div id="inactiveDiv" class="tab-pane">
				<c:if test="${successMessage != null}">
					<div class="alert alert-success fade in">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
					</div>
				</c:if>
				<c:if test="${status =='INACTIVE' or status =='Inactive'}">	
				<div class="form-group col-xs-12 col-md-12">
					<button type="button" class="btn btn-primary" onclick="userStatus('ACTIVE');">Active</button>
				</div>
				<br />
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Manage Brokers</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="myGrid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="pager" style="width: 100%; height: 20px;"></div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
	
</div>

</div>

<!-- for light box -->
<!-- </div> -->

<!-- popup Edit Broker -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-broker">Edit Broker</button> -->
	<div id="edit-broker" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Broker - Email : <span id="brokerEmail_Hdr" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-lg-12">
							<h3 class="page-header"><i class="fa fa-laptop"></i> Admin</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a href="#">Broker</a></li>
								<li><i class="fa fa-laptop"></i>Edit Broker</li>															
							</ol>
							
						</div>
					</div>

					<div class="row">
					  <div class="col-lg-12">
						  <section class="panel">
							  <header class="panel-heading">								
									Fill Broker Details             
							  </header>
							  <div class="panel-body">
								  <div class="form">
										<div id="editBr_successDiv" class="alert alert-success fade in" style="display:none;">
											<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
											<span id="editBr_successMsg"></span></strong>
										</div>					
										<div id="editBr_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
											<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
											<span id="editBr_errorMsg"></span></strong>
										</div>									  
										<form class="form-validate form-horizontal" id="editUserForm" name="editUserForm" method="post" action="" >
										  <input class="form-control" id="editBr_action" value="action" name="action" type="hidden" />
										  <input class="form-control" id="editBr_userId" value="${userId}" name="userId" type="hidden" />
										  <input class="form-control" id="editBr_status" value="${status}" name="status" type="hidden" />
										   <div class="form-group">
												<div class="col-xs-12">
													  <label for="cname" class="">User Name</label>&nbsp; &nbsp;
													  <input type="text" id="userName" name="userName" readonly="readonly" size="100" style="font-weight: bold; border:none; background: transparent;" />
												</div>
										  </div>
										  <div class="form-group ">
											  <label for="cname" class="control-label col-sm-2 col-xs-4">First Name <span class="required">*</span></label>
											  <div class="col-sm-10 col-xs-8">
												  <input class="form-control" id="firstName" name="firstName" type="text"/>
											  </div>
										  </div>
										  <div class="form-group ">
											  <label for="cname" class="control-label col-sm-2 col-xs-4">Last Name <span class="required">*</span></label>
											  <div class="col-sm-10 col-xs-8">
												  <input class="form-control" id="lastName" name="lastName" type="text"/>
											  </div>
										  </div>
										  <div class="form-group ">
											  <label for="cemail" class="control-label col-sm-2 col-xs-4">E-Mail <span class="required">*</span></label>
											  <div class="col-sm-10 col-xs-8">
												  <input class="form-control " path="email" id="email" type="email" name="email"/>
											  </div>
										  </div>
										  <div class="form-group ">
											  <label for="ccomment" class="control-label col-sm-2 col-xs-4">Phone </label>
											  <div class="col-sm-10 col-xs-8">
												  <input class="form-control " id="phone" name="phone" maxlength="10" />
											  </div>
										  </div>
										  <div class="form-group ">
											  <label for="ccomment" class="control-label col-sm-2 col-xs-4">Company </label>
											  <div class="col-sm-10 col-xs-8">
												  <label for="ccomment" class="control-label col-sm-2 col-xs-4" id="companyName_label"></label>
												  <input class="form-control" type="hidden" id="companyName" name="companyName" />
											  </div>
										  </div>
										  <div class="form-group">
												<label for="ccomment" class="control-label col-sm-2 col-xs-4">Service Fees</label>
												<div class="col-sm-10 col-xs-8">
													<input class="form-control" id="serviceFees" name="serviceFees" type="text" />
												</div>
										  </div>
										  <div class="form-group">
											  <label class="control-label col-sm-2 col-xs-4" for="inputSuccess">Roles</label>
											  <div class="col-sm-10 col-xs-8">
													<input type="hidden" id="brokerRole" name="role" value="role_broker">
													<label for="ccomment" class="control-label col-sm-2 col-xs-4">Broker</label>												
											  </div>
										  </div>										  
										  <div class="form-group">
											  <div class="col-lg-offset-2 col-lg-10">
												  <button class="btn btn-primary" type="button" onclick="doEditBrokerValidations()">Update</button>
											  </div>
										  </div>
									  </form>
								  </div>

							  </div>
							  
							  <header class="panel-heading mt-20">
								  Change Password
							  </header>
							  <div class="panel-body">
								  <div class="form">
								  
									  <form class="form-validate form-horizontal" id="changePasswordForm" method="post" action="" >
										  <input class="form-control" id="actionChangepassword" value="action" name="action" type="hidden" />
										  <input class="form-control" id="editBr_Pwd_userId" value="${userId}" name="userId" type="hidden" />
										  <div class="form-group ">
											  <label for="curl" class="control-label col-sm-2 col-xs-4">Password <span class="required">*</span></label>
											  <div class="col-sm-10 col-xs-8">
												  <input class="form-control " id="password" name="password" type="password" />
											  </div>
										  </div>
										  <div class="form-group ">
											  <label for="cname" class="control-label col-sm-2 col-xs-4">Re-Password <span class="required">*</span></label>
											  <div class="col-sm-10 col-xs-8">
												  <input class="form-control" id="repassword" name="repassword" type="password" />
											  </div>
										  </div>                                      
										  
										  <div class="form-group">
											  <div class="col-lg-offset-2 col-lg-10">
												  <button class="btn btn-primary" type="button" onclick="doEdBrChangePasswordValidations()">Update Password</button>
											  </div>
										  </div>
									  </form>
								  </div>

							  </div>
								
							<%-- <header class="panel-heading">
								  Actions
							  </header>
							  <div class="form-group">
								 <div class="form">
									<br/><br/>
								<form class="form-validate form-horizontal" id="Form" method="post" action="${pageContext.request.contextPath}/Admin/EditUser" >
									<input class="form-control" id="" value="action" name="action" type="hidden" />
									<input class="form-control" id="userId" value="${userId}" name="userId" type="hidden" />
									<div class="col-lg-offset-2 col-lg-10">
										<button class="btn btn-primary" type="button" onclick="logOutAction()">Logout User</button>
									</div>
								</form>
								</div>
							  </div> --%><!---->
						  </section>
					  </div>
					</div>

					
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup Edit Broker -->

<!-- popup Audit User -->
	<%@include file="body-user-audit.jsp" %>
<!-- End popup Audit User -->

<script src="../resources/js/slick/lib/firebugx.js"></script>

<%-- <script src="../resources/js/slick/lib/jquery-1.11.2.min.js"></script> --%>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>

<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>

<script>
	var pagingInfo;
	var dataView;
	var grid;
	var userData = [];
	var userSearchString='';
	var columnFilters = {};
	var userAdminColumnsStr = '<%=session.getAttribute("brokergrid")%>';
	var userAdminColumns =[];
	var allAdminColumns = [ {
		id : "userName",
		name : "User Name",
		width:80,
		field : "userName",
		sortable : true
	}, {
		id : "firstName",
		name : "First Name",
		field : "firstName",
		width:80,
		sortable : true
	}, {
		id : "lastName",
		name : "Last Name",
		field : "lastName",
		width:80,
		sortable : true
	}, {
		id : "email",
		name : "Email",
		field : "email",
		width:80,
		sortable : true
	}, {
		id : "phone",
		name : "Phone",
		field : "phone",
		width:80,
		sortable : true
	}, /* {
		id: "role", 
		name: "Role", 
		field: "role", 
		sortable: true,
		width:50, 
		formatter:roleFormatter
	}, */ {
		id : "status",
		name : "Status",
		field : "status",
		width:80,
		sortable : true
	},{
		id : "brokerId",
		name : "Broker ID",
		field : "brokerId",
		width:80,
		sortable : true
	},{
		id : "companyName",
		name : "Company",
		field : "companyName",
		width:80,
		sortable : true
	}/* , {
		id : "promotionalCode",
		name : "Promotional Code",
		field : "promotionalCode",
		width:80,
		sortable : true
	}  */,{
		id : "editCol",
		field : "editCol",
		name : "Edit",
		width:80,
		formatter : editFormatter
	}, /* {
		id : "delCol",
		field : "delCol",
		name : "Delete",
		width:80,
		formatter : buttonFormatter
	}, */ {
		id : "audit",
		field : "delCol",
		name : "Audit",
		width:80,
		formatter : auditFormatter
	} ];

	if(userAdminColumnsStr!='null' && userAdminColumnsStr!=''){
		var columnOrder = userAdminColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allAdminColumns.length;j++){
				if(columnWidth[0] == allAdminColumns[j].id){
					userAdminColumns[i] =  allAdminColumns[j];
					userAdminColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userAdminColumns = allAdminColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "userName";
	var sortdir = 1;
	var percentCompleteThreshold = 0;

	//Now define your buttonFormatter function
	/* function buttonFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.id +"'/>";
		//the id is so that you can identify the row when the particular button is clicked
		return button;
	} */
	
	function auditFormatter(row, cell, value, columnDef, dataContext) {
		/* var button = "<input class='audit' value='Audit' type='button' id='"+ dataContext.id +"' />"; */
		var button = "<img class='auditClickableImage' src='../resources/images/audit-icon.png' id='"+ dataContext.id +"'/>";
		//the id is so that you can identify the row when the particular button is clicked
		return button;
		//Now the row will display your button
	}

	//function for edit functionality
	function editFormatter(row, cell, value, columnDef, dataContext) {
		//the id is so that you can identify the row when the particular button is clicked
		/* var button = "<input class='edit' value='Edit' type='button' id='"+ dataContext.id +"' />"; */
		var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.id +"'/>";
		return button;
	}
	function roleFormatter(row,cell,value,columnDef,dataContext){  
		var userRole = "";
		if(dataContext.role == 'ROLE_SUPER_ADMIN') {
			userRole = "<span class='label label-danger'>Super Admin</span>";
		}else if(dataContext.role == 'ROLE_USER') {
			userRole = "<span class='label label-primary'>User</span>";
		}else if(dataContext.role == 'ROLE_BROKER') {
			userRole = "<span class='label label-success'>Broker</span>";
		}else if(dataContext.role == 'ROLE_AFFILIATES') {
			userRole = "<span class='label btn-primary'>Affiliates</span>";
		}
	    return userRole;
	}

	//Now you can use jquery to hook up your delete button event
	$('.delClickableImage').live('click', function() {
		var me = $(this), id = me.attr('id');
		var delFlag = deleteUser(id);//confirm("Are you sure,Do you want to Delete it?");
	});

	//Now you can use jquery to hook up your delete button event
	$('.auditClickableImage').live('click', function() {
		var me = $(this), id = me.attr('id');
		popupUserAudit(id);
		//dataView.deleteItem(id);
		//grid.invalidate();        
	});

	//Function to hook up the edit button event
	$('.editClickableImage').live('click', function() {
		var me = $(this), id = me.attr('id');
		popupUserEdit(id);
	});

	function deleteRecordFromGrid(id) {
		dataView.deleteItem(id);
		grid.invalidate();
	}
	/*
	function myFilter(item, args) {
		var x= item["firstName"];
		if (args.searchString != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function toggleFilterRow() {
		grid.setTopPanelVisibility(!grid.getOptions().showTopPanel);
	}
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){
		if(id == 'pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getUserGridData(pageNo);
		}
		if(id == 'userAudit_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(userAuditPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(userAuditPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(userAuditPagingInfo.pageNum)-1;
			}	
			getUserAuditGridData(pageNo);
		}
	}
	
	function getUserGridData(pageNo){
		if($('#activeDiv').hasClass('active')){
			$('#status').val('Active');
		}else if($('#inactiveDiv').hasClass('active')){
			$('#status').val('Inactive');
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/Client/ManageBrokers.json",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+userSearchString+"&status="+$('#status').val(),
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				/*if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				}*/
				pagingInfo = jsonData.pagingInfo;
				refreshUserGridValues(jsonData.trackerBrokerList);
				clearAllSelections();
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAdminPreference()'>");
				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshUserGridValues(jsonData){
		 $("div#divLoading").addClass('show');
		 userData = [];
		 if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (userData[i] = {});
		
				d["id"] = data.id;
				//d["id"] = "id_" + i;
				d["userName"] = data.userName;
				d["firstName"] = data.firstName;
				d["lastName"] = data.lastName;
				d["email"] = data.email;
				d["phone"] = data.phone;
				//d["delCol"] = data.id;
				//d["role"] = data.role;
				d["status"] = data.status;
				d["brokerId"] = data.brokerId;
				d["companyName"] = data.companyName;
				//d["promotionalCode"] = data.promotionalCode;
			}
		}
		
		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#myGrid", dataView, userAdminColumns, options);
		grid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		var cols = grid.getColumns();
		var colTest = [];
		for ( var c = 0; c < cols.length; c++) {
			//if(cols[c].name!='Title' && cols[c].name!='Start') {
			colTest.push(cols[c]);
			// }  
			grid.setColumns(colTest);
		}
		grid.invalidate();
		grid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo != null){
			var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"),pagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(allAdminColumns, grid,
				options);

		// move the filter panel defined in a hidden div into grid top panel
		//$("#inlineFilterPanel").appendTo(grid.getTopPanel()).show();

		grid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < dataView.getLength(); i++) {
				rows.push(i);
			}
			grid.setSelectedRows(rows);
			e.preventDefault();
		});
		grid.onSort.subscribe(function(e, args) {
			sortdir = args.sortAsc ? 1 : -1;
			sortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				dataView.fastSort(sortcol, args.sortAsc);
			} else {
				dataView.sort(comparer, args.sortAsc);
			}
		});
		// wire up model events to drive the grid
		dataView.onRowCountChanged.subscribe(function(e, args) {
			grid.updateRowCount();
			grid.render();
		});
		dataView.onRowsChanged.subscribe(function(e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});
		$(grid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	userSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  userSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getUserGridData(0);
				}
			  }
		 
		});
		grid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'delCol' && args.column.id != 'role' && args.column.id != 'editCol' && args.column.id != 'audit'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
			
		});
		grid.init();
			
		//var h_runfilters = null;
		// wire up the slider to apply the filter to the model
		/*$("#pcSlider,#pcSlider2").slider({
		  "range": "min",
		  "slide": function (event, ui) {
		    Slick.GlobalEditorLock.cancelCurrentEdit();
		    if (percentCompleteThreshold != ui.value) {
		      window.clearTimeout(h_runfilters);
		      h_runfilters = window.setTimeout(updateFilter, 10);
		      percentCompleteThreshold = ui.value;
		    }
		  }
		});*/
		/*
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			dataView.setFilterArgs({
				searchString : searchString
			});
			dataView.refresh();
		}
		*/
		/*$("#btnSelectRows").click(function () {
		  if (!Slick.GlobalEditorLock.commitCurrentEdit()) {
		    return;
		  }
		  var rows = [];
		  for (var i = 0; i < 10 && i < dataView.getLength(); i++) {
		    rows.push(i);
		  }
		  grid.setSelectedRows(rows);
		});*/
		// initialize the model after all the events have been hooked up
		dataView.beginUpdate();
		dataView.setItems(userData);
		/*dataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		dataView.setFilter(myFilter);*/
		dataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		dataView.syncGridSelection(grid, true);
		$("#gridContainer").resizable();
		grid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}

	function deleteUser(userId) {
		if (userId == '') {
			jAlert("Userid found empty please refresh and try again.","Info");
			return false;
		}
		jConfirm("Are you sure to delete an User ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/DeleteUser",
						type : "post",
						data : "userName=" + $("#userName").val() + "&userId="
								+ userId,
						success : function(response) {
							if(response.status == 1){
								getUserGridData(0);
								//jAlert("User Deleted successfully.","Info");
								//window.location.href = "${pageContext.request.contextPath}/Admin/ManageUsers";
								//deleteRecordFromGrid(userId);								
								//return true;
							} /* else {
								jAlert(response.msg,"Info","Info");
								return false;
							} */
							if(response.msg != null && response.msg != "" && response.msg != undefined){
								jAlert(response.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}

	function popupUserAudit(userId) {
		getUserAudit(userId);
		/* var url = "${pageContext.request.contextPath}/Client/AuditUsers?userId=" + userId;
		popupCenter(url, 'edit user', '600', '500'); */
		/* var newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
		if (window.focus) {newwindow.focus()}
		return false; */
	}

	//popup user edit
	function popupUserEdit(userId) {
		getBrokerDetailsForEdit(userId);
		/*var editUserUrl = "${pageContext.request.contextPath}/Client/EditUsers?userId=" + userId;
		popupCenter(editUserUrl, 'edit user', '800', '500');*/
	}

	function resetFilters(){
		userSearchString='';
		columnFilters = {};
		getUserGridData(0);
	}
	
	function exportToExcel(){
		if($('#activeDiv').hasClass('active')){
			$('#status').val('Active');
		}else if($('#inactiveDiv').hasClass('active')){
			$('#status').val('Inactive');
		}
		var appendData = "user=BROKER&status="+$('#status').val()+"&headerFilter="+userSearchString;
	    var url = apiServerUrl + "UsersExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}

	//Validation for Add new user
	function doAddUserValidations() {
		var roles = $("input[name=role]");
		var flag = false;
		$.each(roles, function(index, obj) {
			if ($(obj).attr('checked') == 'checked') {
				flag = true;
			}
		});
		if (!flag) {
			jAlert('Please choose at least one Role.',"Info");
			return false;
		}

		if ($("#userName").val() == '') {
			jAlert("Username can't be blank","Info");
			return false;
		} else if ($("#firstName").val() == '') {
			jAlert("Firstrname can't be blank","Info");
			return false;
		} else if ($("#lastName").val() == '') {
			jAlert("Lastname can't be blank","Info");
			return false;
		} else if ($("#email").val() == '') {
			jAlert("Email can't be blank","Info");
			return false;
		} else if ($("#password").val() == '') {
			jAlert("Password can't be blank","Info");
			return false;
		} else if ($("#repassword").val() == '') {
			jAlert("Re-Password can't be blank","Info");
			return false;
		} else if ($("#password").val() != $("#repassword").val()) {
			jAlert("Password and Re-Password must match","Info");
			return false;
		} else if (validateEmail($('#email').val()) == false) {
			jAlert("Invalid Email.","Info");
			return false;
		} else {
			$.ajax({
						url : "${pageContext.request.contextPath}/CheckUser",
						type : "get",
						data : "userName=" + $("#userName").val() + "&email="
								+ $("#email").val(),
						/* async : false, */

						success : function(response) {
							if (response == "true") {
								$("#loginForm").submit();
							} else {
								jAlert(response,"Info");
								return false;
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
		}

	}

	//Start Edit Broker
	function getBrokerDetailsForEdit(userId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Client/EditUsers",
			type : "post",
			data : "userId="+userId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData != null && jsonData != "") {
					if(jsonData.status == 1){
						setBrokerDetailsForEdit(jsonData);
					}
					else{
						jAlert(jsonData.msg);
					}
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setBrokerDetailsForEdit(jsonData){
		$('#edit-broker').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#editBr_successDiv').hide();
			$('#editBr_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#editBr_errorDiv').hide();
			$('#editBr_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#editBr_successDiv').show();
			$('#editBr_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#editBr_errorDiv').show();
			$('#editBr_errorMsg').text(jsonData.errorMessage);
		} */
		$('#editBr_userId').val(jsonData.userId);
		$('#editBr_status').val(jsonData.userStatus);
		$('#editBr_Pwd_userId').val(jsonData.userId);
		var trackerUser = jsonData.trackerUser;
		var trackerBroker = jsonData.trackerBroker;
		
		$('#userName').val(trackerUser.userName);
		$('#firstName').val(trackerUser.firstName);
		$('#lastName').val(trackerUser.lastName);
		$('#email').val(trackerUser.email);
		$('#phone').val(trackerUser.phone);
		$('#companyName_label').text(trackerBroker.brokerCompany);
		$('#companyName').val(trackerBroker.brokerCompany);
		$('#serviceFees').val(trackerBroker.brokerServiceFees);
		$("#password").val('');
		$("#repassword").val('');
		$('#brokerEmail_Hdr').text(trackerUser.email);
	}
	
	function doEditBrokerValidations(){
		if($("#firstName").val() == ''){
			jAlert("Firstrname can't be blank","Info");
			return false;
		}else if($("#lastName").val() == ''){
			jAlert("Lastname can't be blank","Info");
			return false;
		}else if($("#email").val() == ''){
			jAlert("Email can't be blank","Info");
			return false;
		}else if(validateEmail($('#email').val()) == false){
			jAlert("Invalid Email.","Info");
			return false;
		}else if($("#companyName").val() == ''){
			jAlert("Invalid Company.","Info");
			return false;
		}else if($("#serviceFees").val() == ''){
			jAlert("Service Fees can't be blank","Info");
			return false;
		}else{
			$.ajax({
					url : "${pageContext.request.contextPath}/CheckUserForEdit",
					type : "get",
					data : "userName="+ $("#userName").val() + "&email=" + $("#email").val() + "&userId=" + $('#editBr_userId').val(),
					/* async : false, */
					
					success : function(response){
						if(response.msg == "true"){
							$("#editBr_action").val('editUserInfo');
							/*$('#editUserForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
							$("#editUserForm").submit();*/
							$.ajax({
								url : "/Client/EditUsers",
								type : "post",
								dataType : "json",
								data : $("#editUserForm").serialize(),
								success : function(res) {
									var jsonData = JSON.parse(JSON.stringify(res));
									if(jsonData != null && jsonData != ""){
										if(jsonData.status == 1){											
											getUserGridData(0);
										}
										jAlert(jsonData.msg);
									}
									/* if(jsonData.successMessage != null && jsonData.successMessage != ""){
										//$('#editBr_successDiv').show();
										//$('#editBr_successMsg').text(jsonData.successMessage);
										jAlert(jsonData.successMessage);
										getUserGridData(0);
									}
									if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
										//$('#editBr_errorDiv').show();
										//$('#editBr_errorMsg').text(jsonData.errorMessage);
										jAlert(jsonData.errorMessage);
									} */
								},
								error : function(error) {
									jAlert("Your login session is expired please refresh page and login again.", "Error");
									return false;
								}
							});
						}else{
							jAlert(response.msg,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		}		
	}
	
	function doEdBrChangePasswordValidations(){
		if($("#password").val() == ''){
			jAlert("Password can't be blank","Info");
			return false;
		}else if($("#repassword").val() == ''){
			jAlert("Re-Password can't be blank","Info");
			return false;
		}else if($("#password").val() != $("#repassword").val()){
			jAlert("Password and Re-Password must match","Info");
			return false;
		}else{
			$("#actionChangepassword").val('changePassword');
			/*$('#changePasswordForm').attr('action','${pageContext.request.contextPath}/Client/EditUsers');
			$("#changePasswordForm").submit();*/
			$.ajax({
				url : "/Client/EditUsers",
				type : "post",
				dataType : "json",
				data : $("#changePasswordForm").serialize(),
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData != null && jsonData != ""){
						if(jsonData.status == 1){}
						jAlert(jsonData.msg);
					}
					/* if(jsonData.successMessage != null && jsonData.successMessage != ""){
						//$('#editBr_successDiv').show();
						//$('#editBr_successMsg').text(jsonData.successMessage);
						jAlert(jsonData.successMessage);
					}
					if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
						//$('#editBr_errorDiv').show();
						//$('#editBr_errorMsg').text(jsonData.errorMessage);
						jAlert(jsonData.errorMessage);
					} */
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	}	
	//End Edit Broker
	
	$(document).ready(function(){
		$("#activeBroker").click(function(){
			callTabOnChange('Active');
		});
		$("#inactiveBroker").click(function(){
			callTabOnChange('Inactive');
		});
		
		<c:choose>
			<c:when test="${status == 'Active' or status =='ACTIVE'}">
				$('#activeDiv').addClass('active');
				$('#activeTab').addClass('active');
			</c:when>
			<c:when test="${status == 'Inactive' or status =='INACTIVE'}">
				$('#inactiveDiv').addClass('active');
				$('#inactiveTab').addClass('active');
			</c:when>
		</c:choose>
	});
	
	function callTabOnChange(selectedTab) {		
		var data = "?status="+selectedTab;
		window.location = "${pageContext.request.contextPath}/Client/ManageBrokers"+data;
	}
</script>
</body>
</html>