<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
<link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
<link href="../resources/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link href="../resources/css/widgets.css" rel="stylesheet">
<link href="../resources/css/style.css" rel="stylesheet">
<link href="../resources/css/style-responsive.css" rel="stylesheet" />
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script src="../resources/js/jquery.scrollTo.min.js"></script>
<script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>
<style>
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
</style>
<script>
$(document).ready(function(){
	 getCustomerGridData(0);
	
	 $('#expDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
     });
});


function updateTicketHold(onHand){
	var temprTicketRowIndex = ticketGrid.getSelectedRows();
	var ticketIdStr='';
	var validTickets = true;
	$.each(temprTicketRowIndex, function (index, value) {
		if(onHand=='HOLD' && ticketGrid.getDataItem(value).status != 'ACTIVE'){
			validTickets =false;
		}else if(onHand=='UNHOLD' && ticketGrid.getDataItem(value).status != 'ONHOLD'){
			validTickets =false;
			
		}
		ticketIdStr += ','+ticketGrid.getDataItem(value).id;
	});
	
	if(onHand=='UNHOLD' && !validTickets){
		jAlert("Only ONHOLD tickets can be added back to ACTIVE.");
		return;
	}
	if(onHand=='HOLD' && !validTickets){
		jAlert("Only ACTIVE tickets can be added to on Hold.");
		return;
	}
	
	if(ticketIdStr != null && ticketIdStr!='') {
		ticketIdStr = ticketIdStr.substring(1, ticketIdStr.length);
	}
	
	var custRowId = customerGrid.getSelectedRows([0])[0];
	var expDate = $('#expDate').val();
	var expMinutes = $('#expMinutes').val();
	var salesPrice = $('#salesPrice').val();
	
	if(ticketIdStr==null || ticketIdStr==''){
		jAlert("Please select atleast one record to update.");
		return;
	}
	
	if(onHand == 'HOLD'){
		if(custRowId < 0){
			jAlert("Please select customer.");
			return;
		}
		if(expDate == '' && expMinutes==''){
			jAlert("Please add expiry date or expiry minutes.");
			return;
		}
		if(expDate!=null && expDate != ''){
			if(new Date().getTime() <=  new Date(expDate).getTime()){
				jAlert("expiry date must be future date.");
				return;
			}
		}
		if(salesPrice <= 0){
			jAlert("Please add valid sold price.");
			return;
		}
		var customerId = customerGrid.getDataItem(custRowId).customerId;
		window.location="${pageContext.request.contextPath}/Invoice/HoldTicket?ticketGroupId=${ticketGroupId}&action="
		+onHand+"&ticketIds="+ticketIdStr+"&customerId="+customerId+"&salesPrice="+salesPrice+"&expiryDate="+expDate+"&expiryMinutes="+expMinutes;
	}else if(onHand == 'UNHOLD'){
		window.location="${pageContext.request.contextPath}/Invoice/HoldTicket?ticketGroupId=${ticketGroupId}&action="+onHand+"&ticketIds="+ticketIdStr;
	}
	
}
window.onresize = function(){
	ticketGrid.resizeCanvas();
};
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Events
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Tickets</a></li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Hold/Unhold Tickets</li>
		</ol>
	</div>
</div>

<br />
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
</c:if>
<div style="position: relative" id="ticket">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Tickets</label>
		</div>
		<div id="ticket_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="ticket_pager" style="width: 100%; height: 10px;"></div>
		<br />
		<br />
	</div>
</div>	
<br/><br/>
<div class="col-xs-12 manage-event-filter">
	<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
		<label for="name" class="control-label">Expiry Date</label>
		<input class="form-control searchcontrol" type="text" id="expDate" name="expDate">
	</div>
	<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
		<label for="name" class="control-label">Expiry Minutes</label>
		<input class="form-control searchcontrol" type="text" id="expMinutes" name="expMinutes">
	</div>
	<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
		<label for="name" class="control-label">Sales Price</label>
		<input class="form-control searchcontrol" type="text" id="salesPrice" name="salesPrice">
	</div>
</div>
<br/><br/>
<br/><br/>
<div style="position: relative;" id="customerGridDiv">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label style="font-size: 10px;">Customers Details</label>
			<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
		</div>
		<div id="customer_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="customer_pager" style="width: 100%; height: 20px;"></div>
	</div>
</div>



<br/><br/>
<div class="col-lg-12">
	<button type="button" class="btn btn-primary" onclick="updateTicketHold('HOLD')">Hold</button>
	<!-- <button type="button" class="btn btn-primary" onclick="updateTicketHold('UNHOLD')">Unhold</button> -->
</div>
<script>
$("div#divLoading").addClass('show');
var ticketCheckboxSelector = new Slick.CheckboxSelectColumn({
	 cssClass: "slick-cell-checkboxsel"
});
		var pagingInfo;
		var ticketDataView;
		var ticketGrid;
		var ticketData = [];
		var ticketColumns = [ ticketCheckboxSelector.getColumnDefinition(),
		{
			id : "id",
			name : "Ticket Id",
			field : "id",
			width:80,
			sortable : true
		},{
			id : "section",
			name : "Section",
			field : "section",
			width:80,
			sortable : true
		},{
			id : "row",
			name : "Row",
			field : "row",
			width:80,
			sortable : true
		},{
			id : "seatNo",
			name : "Seat No",
			field : "seatNo",
			width:80,
			sortable : true
		},{
			id: "onHandStatus", 
			name: "on Hand", 
			field: "onHandStatus",
			width:80,		
			sortable: true
		},{
			id: "cost", 
			name: "Cost", 
			field: "cost", 
			width:80,
			sortable: true,
		},{
			id: "actualSoldPrice", 
			name: "Actual Sold Price", 
			field: "actualSoldPrice", 
			width:80,
			sortable: true
		},{
			id: "facePrice", 
			name: "Face Price", 
			field: "facePrice",
			width:80,
			sortable: true
		},{
			id: "wholeSalePrice", 
			name: "WholeSale Price", 
			field: "wholeSalePrice",
			width:80,
			sortable: true
		} , {
			id: "retailPrice", 
			name: "Retail Price", 
			field: "retailPrice",
			width:80,
			sortable: true
		} , {
			id: "status", 
			name: "Status", 
			field: "status",
			width:80,
			sortable: true
		}];
		
		var ticketOptions = {
			editable: true,
			//enableAddRow: true,
			enableCellNavigation : true,
			//asyncEditorLoading: true,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25
		};
		var ticketGridSortcol = "id";
		var ticketGridSortdir = 1;
		var percentCompleteThreshold = 0;
		var ticketGridSearchString = "";

		
		function deleteRecordFromTicketGrid(id) {
			ticketDataView.deleteItem(id);
			ticketGrid.invalidate();
		}
		function ticketGridFilter(item, args) {
			var x= item["id"];
			if (args.ticketGridSearchString  != ""
					&& x.indexOf(args.ticketGridSearchString) == -1) {
				
				if (typeof x === 'string' || x instanceof String) {
					if(x.toLowerCase().indexOf(args.ticketGridSearchString.toLowerCase()) == -1) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}

		function ticketGridComparer(a, b) {
			var x = a[ticketGridSortcol], y = b[ticketGridSortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
		function ticketGridToggleFilterRow() {
			ticketGrid.setTopPanelVisibility(!ticketGrid.getOptions().showTopPanel);
		}
		
		function refreshTicketGridValues(jsonData) {
			ticketData = [];
			for(var i=0;i<jsonData.length;i++){
				var d = (ticketData[i] = {});
				d["id"] = jsonData[i].id;
				d["cost"] = jsonData[i].cost;
				d["actualSoldPrice"] = jsonData[i].actualSoldPrice;
				d["facePrice"] = jsonData[i].facePrice;
				d["wholeSalePrice"] = jsonData[i].wholeSalePrice;
				d["retailPrice"] = jsonData[i].retailPrice;
				d["section"] = jsonData[i].section;
				d["row"] = jsonData[i].row;
				d["seatNo"] = jsonData[i].seatNo;
				d["onHandStatus"] = jsonData[i].onHandStatus;
				d["status"] = jsonData[i].status;
			}

			ticketDataView = new Slick.Data.DataView({
				inlineFilters : true
			});
			ticketGrid = new Slick.Grid("#ticket_grid", ticketDataView, ticketColumns, ticketOptions);
			ticketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			ticketGrid.setSelectionModel(new Slick.RowSelectionModel());
			ticketGrid.registerPlugin(ticketCheckboxSelector);
			if(pagingInfo != null){
				var ticketGridPager = new Slick.Controls.Pager(ticketDataView, ticketGrid, $("#ticket_pager"),pagingInfo);
			}
			var ticketGridColumnpicker = new Slick.Controls.ColumnPicker(ticketColumns, ticketGrid,
					ticketOptions);

			// move the filter panel defined in a hidden div into ticketGrid top panel
			$("#ticket_inlineFilterPanel").appendTo(ticketGrid.getTopPanel()).show();
			ticketGrid.onSort.subscribe(function(e, args) {
				ticketGridSortdir = args.sortAsc ? 1 : -1;
				ticketGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					ticketDataView.fastSort(ticketGridSortcol, args.sortAsc);
				} else {
					ticketDataView.sort(ticketGridComparer, args.sortAsc);
				}
			});

			ticketGrid.onCellChange.subscribe(function (e,args) { 
					var temprEventRowIndex = ticketGrid.getSelectedRows();
					var ticketId;
					var eventNotes; 
					$.each(temprEventRowIndex, function (index, value) {
						ticketId = ticketGrid.getDataItem(value).id;
						ticketNotes = ticketGrid.getDataItem(value).notes;
					});
					saveNotes(ticketId,ticketNotes);
	         });
			// wire up model tickets to drive the ticketGrid
			ticketDataView.onRowCountChanged.subscribe(function(e, args) {
				ticketGrid.updateRowCount();
				ticketGrid.render();
			});
			ticketDataView.onRowsChanged.subscribe(function(e, args) {
				ticketGrid.invalidateRows(args.rows);
				ticketGrid.render();
			});

			// wire up the search textbox to apply the filter to the model
			$("#ticketGridSearch").keyup(function(e) {
				Slick.GlobalEditorLock.cancelCurrentEdit();
				// clear on Esc
				if (e.which == 27) {
					this.value = "";
				}
				ticketGridSearchString = this.value;
				updateTicketGridFilter();
			});
			function updateTicketGridFilter() {
				ticketDataView.setFilterArgs({
					ticketGridSearchString : ticketGridSearchString
				});
				ticketDataView.refresh();
			}
			
			// initialize the model after all the tickets have been hooked up
			ticketDataView.beginUpdate();
			ticketDataView.setItems(ticketData);
			ticketDataView.setFilterArgs({
				percentCompleteThreshold : percentCompleteThreshold,
				ticketGridSearchString : ticketGridSearchString
			});
			ticketDataView.setFilter(ticketGridFilter);
			ticketDataView.endUpdate();
			ticketDataView.syncGridSelection(ticketGrid, true);
			$("#gridContainer").resizable();
			ticketGrid.resizeCanvas();
			 $("div#divLoading").removeClass('show');
		}
		
		
		
		var pagingInfo;
		var customerGrid;
		var customerDataView;
		var customerData=[];
		var customerGridSearchString='';
		var columnFilters = {};
		var customerColumns = [
		               {id:"customerType", name:"Customer Type", field: "customerType", sortable: true, width: 50},
		               {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
		               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
		               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
		               {id:"productType", name:"Product Type", field:"productType", sortable: true, width: 120},
		               {id:"client", name:"Client", field: "client", sortable: true},
		               {id:"broker", name:"Broker", field: "broker", sortable: true},
		               {id:"street1", name:"Street1", field: "street1", sortable: true},
		               {id:"street2", name:"Street2", field: "street2", sortable: true},
		               {id:"city", name:"City", field: "city", sortable: true},
		               {id:"state", name:"State", field: "state", sortable: true},
					   {id:"country", name:"Country", field: "country", sortable: true},
		               {id:"zip", name:"Zip", field: "zip", sortable: true},
		               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
		              ];
		              
		var customerOptions = {
				enableCellNavigation : true,
				forceFitColumns : true,
				multiSelect: false,
				topPanelHeight : 25,
				showHeaderRow: true,
				headerRowHeight: 30,
				explicitInitialization: true
			};
			var customerGridSortcol = "customerName";
			var customerGridSortdir = 1;
			var percentCompleteThreshold = 0;

			
			function customerGridComparer(a, b) {
				var x = a[customerGridSortcol], y = b[customerGridSortcol];
				if(!isNaN(x)){
				   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
				}
				if(x == '' || x == null) {
					return 1;
				} else if(y == '' || y == null) {
					return -1;
				}
				if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
					return x.toLowerCase().localeCompare(y.toLowerCase());	
				} else {
					return (x == y ? 0 : (x > y ? 1 : -1));	
				}
			}
			
			function refreshcustomerGridValues(jsonData) {
				 $("div#divLoading").addClass('show');
				customerData = [];
				for (var i = 0; i < jsonData.length; i++) {
					var  data= jsonData[i]; 
					var d = (customerData[i] = {});
					d["id"] = i;
					d["customerId"] = data.customerId;
					d["bAddressId"] = data.billingAddressId;
					d["firstName"] = data.customerName;
					d["lastName"] = data.lastName;
					d["customerType"] = data.customerType;
					d["client"] = data.client;
					d["broker"] = data.broker;
					d["email"] = data.customerEmail;
					d["productType"] = data.productType;
					d["street1"] = data.addressLine1;	
					d["street2"] = data.addressLine2;
					d["city"] = data.city;
					d["state"] = data.state;
					d["country"] = data.country;
					d["zip"] = data.zipCode;
					d["phone"] = data.phone;
				}

				customerDataView = new Slick.Data.DataView();
				customerGrid = new Slick.Grid("#customer_grid", customerDataView, customerColumns, customerOptions);
				customerGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
				customerGrid.setSelectionModel(new Slick.RowSelectionModel());
				if(pagingInfo!=null){
					var customerGridPager = new Slick.Controls.Pager(customerDataView, customerGrid, $("#customer_pager"),pagingInfo);
				}
				
				var customerGridColumnpicker = new Slick.Controls.ColumnPicker(customerColumns, customerGrid,
						customerOptions);

				// move the filter panel defined in a hidden div into customerGrid top panel
				//$("#customer_inlineFilterPanel").appendTo(customerGrid.getTopPanel()).show();

				customerGrid.onSort.subscribe(function(e, args) {
					customerGridSortdir = args.sortAsc ? 1 : -1;
					customerGridSortcol = args.sortCol.field;
					if ($.browser.msie && $.browser.version <= 8) {
						customerDataView.fastSort(customerGridSortcol, args.sortAsc);
					} else {
						customerDataView.sort(customerGridComparer, args.sortAsc);
					}
				});
				// wire up model customers to drive the customerGrid
				customerDataView.onRowCountChanged.subscribe(function(e, args) {
					customerGrid.updateRowCount();
					customerGrid.render();
				});
				customerDataView.onRowsChanged.subscribe(function(e, args) {
					customerGrid.invalidateRows(args.rows);
					customerGrid.render();
				});
				$(customerGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
				 var keyCode = (e.keyCode ? e.keyCode : e.which);
					customerGridSearchString='';
					 var columnId = $(this).data("columnId");
					  if (columnId != null) {
						columnFilters[columnId] = $.trim($(this).val());
						if(keyCode == 13) {
							for (var columnId in columnFilters) {
							  if (columnId !== undefined && columnFilters[columnId] !== "") {
								  customerGridSearchString += columnId + ":" +columnFilters[columnId]+",";
							  }
							}
							getCustomerGridData(0);
						}
					  }
				 
				});
				customerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
					$(args.node).empty();
					if(args.column.id.indexOf('checkbox') == -1){
						if(args.column.id != 'editCol'){
							$("<input type='text'>")
						   .data("columnId", args.column.id)
						   .val(columnFilters[args.column.id])
						   .appendTo(args.node);
						}
					}
				});
				customerGrid.init();
					
				// initialize the model after all the customers have been hooked up
				customerDataView.beginUpdate();
				customerDataView.setItems(customerData);
				customerDataView.endUpdate();
				customerDataView.syncGridSelection(customerGrid, true);
				$("#gridContainer").resizable();
				$("div#divLoading").removeClass('show');
			}


			function pagingControl(move,id){
				var pageNo = 0;
				if(move == 'FIRST'){
					pageNo = 0;
				}else if(move == 'LAST'){
					pageNo = parseInt(pagingInfo.totalPages)-1;
				}else if(move == 'NEXT'){
					pageNo = parseInt(pagingInfo.pageNum) +1;
				}else if(move == 'PREV'){
					pageNo = parseInt(pagingInfo.pageNum)-1;
				}
				getCustomerGridData(pageNo);
			}
			function getCustomerGridData(pageNo) {
				var searchValue = $("#searchValue").val();
				$.ajax({
					url : "${pageContext.request.contextPath}/Client/GetCustomers",
					type : "post",
					data : $("#customerSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+customerGridSearchString,
					dataType:"json",
					success : function(res){
						var jsonData = JSON.parse(JSON.stringify(res));
						pagingInfo = jsonData.pagingInfo;
						refreshcustomerGridValues(jsonData.customers);
					}, error : function(error){
						jAlert("There is something wrong. Please try again"+error,"Error");
						return false;
					}
				});
			}
			
			function resetFilters(){
				customerGridSearchString='';
				columnFilters = {};
				getCustomerGridData(0);
			}
			
window.onload = function() {
	pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
	refreshTicketGridValues(JSON.parse(JSON.stringify(${tickets})));	
}
</script>


