<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var j = 0;
var promoAutoGenerate;
var promoFlatDiscount;
var varSelectedList = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}		
	});
	
	$("#allContest1").click(function(){
		callTabOnChange('ACTIVECONTEST');
	});
	$("#expiredContest1").click(function(){
		callTabOnChange('EXPIREDCONTEST');
	});
	
	<c:choose>
	<c:when test="${status == 'ACTIVECONTEST'}">	
		$('#allContest').addClass('active');
		$('#allContestTab').addClass('active');
	</c:when>
	<c:when test="${status == 'EXPIREDCONTEST'}">
		$('#expiredContest').addClass('active');
		$('#expiredContestTab').addClass('active');
	</c:when>
	</c:choose>
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ContestsFirebaseMobile?status="+selectedTab;
}

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Contest Events</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="allContestTab" class=""><a id="allContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#allContest">Active Contests</a></li>
				<li id="expiredContestTab" class=""><a id="expiredContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredContest">Expired Contests</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="allContest" class="tab-pane">
				<c:if test="${status =='ACTIVECONTEST'}">
				<div class="full-width full-width-btn mb-20">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Active Contests</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<select name="contests" id="contests" class="form-control input-sm m-bot15"
						onchange="getAllContestEventGridData(0);">
							<option value="-1">--Select--</option>					
							<c:forEach items="${contestsList}" var="contest">
								<option value="${contest.id}"> ${contest.contestName} ${contest.startDateTimeStr}
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<br/><br/>
				<div style="position: relative" id="allContestEventsDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Contest Events</label>
								<div class="pull-right">
									<a href="javascript:allContestEventExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
									<a href="javascript:allContestEventResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
								</div>
						</div>
						<div id="allContestEvent_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="allContestEvent_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				<br />
		
				<div class="full-width mb-20 mt-20 full-width-btn">									
					<button type="button" class="btn btn-primary" onclick="updateContestEvents('EXCLUDE');"> Exclude </button>									
				</div>
				<br />
		
				<div style="position: relative" id="excludeContestEventsDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Excluded Events</label>
							<div class="pull-right">
								<a href="javascript:excludeContestEventExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:excludeContestEventResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="excludeContestEvent_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="excludeContestEvent_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<div class="full-width mb-20 mt-20 full-width-btn">									
					<button type="button" class="btn btn-primary" onclick="updateContestEvents('REMOVEEXCLUDE');"> Remove Exclude </button>									
				</div>
				</c:if>
			</div>
			<div id="expiredContest" class="tab-pane">
				<c:if test="${status =='EXPIREDCONTEST'}">
				<div class="full-width full-width-btn mb-20">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Expired Contests</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<select name="contests" id="contests" class="form-control input-sm m-bot15"
						onchange="getAllContestEventGridData(0);">
							<option value="-1">--Select--</option>					
							<c:forEach items="${contestsList}" var="contest">
								<option value="${contest.id}"> ${contest.contestName} ${contest.startDateTimeStr}
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<br/><br/>
				<div style="position: relative" id="allContestEventsDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Contest Events</label>
								<div class="pull-right">
									<a href="javascript:allContestEventExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
									<a href="javascript:allContestEventResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
								</div>
						</div>
						<div id="allContestEvent_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="allContestEvent_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				<br />
		
				<div class="full-width mb-20 mt-20 full-width-btn">									
					<button type="button" class="btn btn-primary" onclick="updateContestEvents('EXCLUDE');"> Exclude </button>									
				</div>
				<br />
		
				<div style="position: relative" id="excludeContestEventsDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Excluded Events</label>
							<div class="pull-right">
								<a href="javascript:excludeContestEventExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:excludeContestEventResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="excludeContestEvent_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="excludeContestEvent_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<div class="full-width mb-20 mt-20 full-width-btn">									
					<button type="button" class="btn btn-primary" onclick="updateContestEvents('REMOVEEXCLUDE');"> Remove Exclude </button>									
				</div>
				</c:if>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'allContestEvent_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}			
			getAllContestEventGridData(pageNo);
		} else if(id == 'excludeContestEvent_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(excludeContestEventPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(excludeContestEventPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(excludeContestEventPagingInfo.pageNum) - 1;
			}			
			getExcludeContestEventGridData(pageNo);
		}
	}
	
	// All Event Grid
	
	function getAllContestEventGridData(pageNo){
		var contestId = $('#contests').val();
		if(contestId != null && contestId != ""){
			$.ajax({
				url : "${pageContext.request.contextPath}/ContestEvents",
				type : "post",
				dataType: "json",
				data: "contestId="+contestId+"&pageNo="+pageNo+"&headerFilter="+allContestEventSearchString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					pagingInfo = jsonData.contestEventPagingInfo;
					refreshAllContestEventGridValues(jsonData.contestEventList);
					excludeContestEventPagingInfo = jsonData.contestExcludeEventPagingInfo;
					refreshExcludeContestEventGridValues(jsonData.contestExcludeEventList);	
					$('#allContestEvent_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAllContestEventPreference()'>");
					$('#excludeContestEvent_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserExcludeContestEventPreference()'>");
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			jAlert("Please select Contest.");
			return;
		}
	}
	
	function allContestEventExportToExcel(){
		var contId = $('#contests').val();
		var appendData = "contestId="+contId+"&headerFilter="+allContestEventSearchString;
	    var url = "${pageContext.request.contextPath}/AllContestEventExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function excludeContestEventExportToExcel(){
		var contId = $('#contests').val();
		var appendData = "contestId="+contId+"&headerFilter="+excludeContestEventSearchString;
	    var url = "${pageContext.request.contextPath}/ExcludeContestEventExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function allContestEventResetFilters(){
		allContestEventSearchString='';
		allContestEventColumnFilters = {};
		getAllContestEventGridData(0);
	}
	
	var allContestEventCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});

	var pagingInfo;
	var allContestEventDataView;
	var allContestEventGrid;
	var allContestEventData = [];
	var allContestEventPager;
	var allContestEventSearchString='';
	var allContestEventColumnFilters = {};
	var userAllContestEventColumnsStr = '<%=session.getAttribute("allcontesteventgrid")%>';

	var userAllContestEventColumns = [];
	var allContestEventColumns = [allContestEventCheckboxSelector.getColumnDefinition(),
			/*{
				id : "contestId",
				field : "contestId",
				name : "Contest ID",
				width : 80,
				sortable : true
			},{
				id : "eventId",
				field : "eventId",
				name : "Event ID",
				width : 80,
				sortable : true
			},*/{
				id : "eventName",
				field : "eventName",
				name : "Event Name",
				width : 80,
				sortable : true
			},{
				id : "eventDate",
				field : "eventDate",
				name : "Event Date",
				width : 80,
				sortable : true
			},{
				id : "eventTime",
				field : "eventTime",
				name : "Event Time",
				width : 80,
				sortable : true
			},{
				id : "venue",
				field : "venue",
				name : "Venue",
				width : 80,
				sortable : true
			}];

	if (userAllContestEventColumnsStr != 'null' && userAllContestEventColumnsStr != '') {
		columnOrder = userAllContestEventColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestEventColumns.length; j++) {
				if (columnWidth[0] == allContestEventColumns[j].id) {
					userAllContestEventColumns[i] = allContestEventColumns[j];
					userAllContestEventColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAllContestEventColumns = allContestEventColumns;
	}
		
	var allContestEventOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var allContestEventGridSortcol = "eventId";
	var allContestEventGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function allContestEventGridComparer(a, b) {
		var x = a[allContestEventGridSortcol], y = b[allContestEventGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshAllContestEventGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		allContestEventData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (allContestEventData[i] = {});
				d["id"] = i;
				//d["contestId"] = data.contestId;				
				d["eventId"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["venue"] = data.building;
			}
		}

		allContestEventDataView = new Slick.Data.DataView();
		allContestEventGrid = new Slick.Grid("#allContestEvent_grid", allContestEventDataView,
				userAllContestEventColumns, allContestEventOptions);
		allContestEventGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		allContestEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		allContestEventGrid.registerPlugin(allContestEventCheckboxSelector);
							
		allContestEventPager = new Slick.Controls.Pager(allContestEventDataView,
					allContestEventGrid, $("#allContestEvent_pager"),
					pagingInfo);
		
		var allContestEventGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestEventColumns, allContestEventGrid, allContestEventOptions);
		
	
		allContestEventGrid.onSort.subscribe(function(e, args) {
			allContestEventGridSortdir = args.sortAsc ? 1 : -1;
			allContestEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				allContestEventDataView.fastSort(allContestEventGridSortcol, args.sortAsc);
			} else {
				allContestEventDataView.sort(allContestEventGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the allContestEventGrid
		allContestEventDataView.onRowCountChanged.subscribe(function(e, args) {
			allContestEventGrid.updateRowCount();
			allContestEventGrid.render();
		});
		allContestEventDataView.onRowsChanged.subscribe(function(e, args) {
			allContestEventGrid.invalidateRows(args.rows);
			allContestEventGrid.render();
		});
		$(allContestEventGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							allContestEventSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								allContestEventColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in allContestEventColumnFilters) {
										if (columnId !== undefined
												&& allContestEventColumnFilters[columnId] !== "") {
											allContestEventSearchString += columnId
													+ ":"
													+ allContestEventColumnFilters[columnId]
													+ ",";
										}
									}
									getAllContestEventGridData(0);
								}
							}

						});
		allContestEventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(allContestEventColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(allContestEventColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(allContestEventColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		allContestEventGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		allContestEventDataView.beginUpdate();
		allContestEventDataView.setItems(allContestEventData);
		//allContestEventDataView.setFilter(filter);
		allContestEventDataView.endUpdate();
		allContestEventDataView.syncGridSelection(allContestEventGrid, true);
		allContestEventGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAllContestEventPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = allContestEventGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('allcontesteventgrid', colStr);
	}
	
	function getSelectedAllContestEventGridId() {
		var tempAllContestEventRowIndex = allContestEventGrid.getSelectedRows();
		
		var eventIdStr='';
		$.each(tempAllContestEventRowIndex, function (index, value) {
			eventIdStr += ','+allContestEventGrid.getDataItem(value).eventId;
		});
		
		if(eventIdStr != null && eventIdStr!='') {
			eventIdStr = eventIdStr.substring(1, eventIdStr.length);
			 return eventIdStr;
		}
	}
	
	// Exclude - Contest Event Grid
	
	function getExcludeContestEventGridData(pageNo){
		var contestId = $('#contests').val();
		if(contestId != null && contestId != ""){
			$.ajax({
				url : "${pageContext.request.contextPath}/ExcludeContestEvents",
				type : "post",
				dataType: "json",
				data: "contestId="+contestId+"&pageNo="+pageNo+"&headerFilter="+excludeContestEventSearchString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					excludeContestEventPagingInfo = jsonData.contestExcludeEventPagingInfo;
					refreshExcludeContestEventGridValues(jsonData.contestExcludeEventList);				
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			jAlert("Please select Contest.");
			return;
		}
	}
	
	function excludeContestEventResetFilters(){
		excludeContestEventSearchString='';
		excludeContestEventColumnFilters = {};
		getExcludeContestEventGridData(0);
	}
	
	var excludeContestEventCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});

	var excludeContestEventPagingInfo;
	var excludeContestEventDataView;
	var excludeContestEventGrid;
	var excludeContestEventData = [];
	var excludeContestEventPager;
	var excludeContestEventSearchString='';
	var excludeContestEventColumnFilters = {};
	var userExcludeContestEventColumnsStr = '<%=session.getAttribute("excludecontesteventgrid")%>';

	var userExcludeContestEventColumns = [];
	var allExcludeContestEventColumns = [excludeContestEventCheckboxSelector.getColumnDefinition(),
			/*{
				id : "contestId",
				field : "contestId",
				name : "Contest ID",
				width : 80,
				sortable : true
			},{
				id : "eventId",
				field : "eventId",
				name : "Event ID",
				width : 80,
				sortable : true
			},*/{
				id : "eventName",
				field : "eventName",
				name : "Event Name",
				width : 80,
				sortable : true
			},{
				id : "eventDate",
				field : "eventDate",
				name : "Event Date",
				width : 80,
				sortable : true
			},{
				id : "eventTime",
				field : "eventTime",
				name : "Event Time",
				width : 80,
				sortable : true
			},{
				id : "venue",
				field : "venue",
				name : "Venue",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			}];

	if (userExcludeContestEventColumnsStr != 'null' && userExcludeContestEventColumnsStr != '') {
		columnOrder = userExcludeContestEventColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allExcludeContestEventColumns.length; j++) {
				if (columnWidth[0] == allExcludeContestEventColumns[j].id) {
					userExcludeContestEventColumns[i] = allExcludeContestEventColumns[j];
					userExcludeContestEventColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userExcludeContestEventColumns = allExcludeContestEventColumns;
	}
		
	var excludeContestEventOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var excludeContestEventGridSortcol = "eventId";
	var excludeContestEventGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function excludeContestEventGridComparer(a, b) {
		var x = a[excludeContestEventGridSortcol], y = b[excludeContestEventGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshExcludeContestEventGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		excludeContestEventData = [];
		if (jsonData != null && jsonData.length > 0) {
			
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (excludeContestEventData[i] = {});
				d["id"] = i;
				//d["contestId"] = data.contestId;				
				d["eventId"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["venue"] = data.building;
				d["updatedBy"] = data.updatedBy;
				d["updatedDate"] = data.updatedDateTimeStr;
			}
		}

		excludeContestEventDataView = new Slick.Data.DataView();
		excludeContestEventGrid = new Slick.Grid("#excludeContestEvent_grid", excludeContestEventDataView,
				userExcludeContestEventColumns, excludeContestEventOptions);
		excludeContestEventGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		excludeContestEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		excludeContestEventGrid.registerPlugin(excludeContestEventCheckboxSelector);
							
		excludeContestEventPager = new Slick.Controls.Pager(excludeContestEventDataView,
					excludeContestEventGrid, $("#excludeContestEvent_pager"),
					excludeContestEventPagingInfo);
		
		var excludeContestEventGridColumnpicker = new Slick.Controls.ColumnPicker(
				allExcludeContestEventColumns, excludeContestEventGrid, excludeContestEventOptions);
		
	
		excludeContestEventGrid.onSort.subscribe(function(e, args) {
			excludeContestEventGridSortdir = args.sortAsc ? 1 : -1;
			excludeContestEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				excludeContestEventDataView.fastSort(excludeContestEventGridSortcol, args.sortAsc);
			} else {
				excludeContestEventDataView.sort(excludeContestEventGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the excludeContestEventGrid
		excludeContestEventDataView.onRowCountChanged.subscribe(function(e, args) {
			excludeContestEventGrid.updateRowCount();
			excludeContestEventGrid.render();
		});
		excludeContestEventDataView.onRowsChanged.subscribe(function(e, args) {
			excludeContestEventGrid.invalidateRows(args.rows);
			excludeContestEventGrid.render();
		});
		$(excludeContestEventGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							excludeContestEventSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								excludeContestEventColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in excludeContestEventColumnFilters) {
										if (columnId !== undefined
												&& excludeContestEventColumnFilters[columnId] !== "") {
											excludeContestEventSearchString += columnId
													+ ":"
													+ excludeContestEventColumnFilters[columnId]
													+ ",";
										}
									}
									getExcludeContestEventGridData(0);
								}
							}

						});
		excludeContestEventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'eventDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(excludeContestEventColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(excludeContestEventColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(excludeContestEventColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		excludeContestEventGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		excludeContestEventDataView.beginUpdate();
		excludeContestEventDataView.setItems(excludeContestEventData);
		//excludeContestEventDataView.setFilter(filter);
		excludeContestEventDataView.endUpdate();
		excludeContestEventDataView.syncGridSelection(excludeContestEventGrid, true);
		excludeContestEventGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserExcludeContestEventPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = excludeContestEventGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('excludecontesteventgrid', colStr);
	}
	
	function getSelectedExcludeContestEventGridId() {
		var tempExcludeContestEventRowIndex = excludeContestEventGrid.getSelectedRows();
		
		var eventIdStr='';
		$.each(tempExcludeContestEventRowIndex, function (index, value) {
			eventIdStr += ','+excludeContestEventGrid.getDataItem(value).eventId;
		});
		
		if(eventIdStr != null && eventIdStr!='') {
			eventIdStr = eventIdStr.substring(1, eventIdStr.length);
			 return eventIdStr;
		}
	}
	
	//Exclude Contest Event
	function updateContestEvents(action){
		var contestId = $('#contests').val();
		var eventIds = '';
		if(action != null && action == 'EXCLUDE'){
			eventIds = getSelectedAllContestEventGridId();		
		}else if(action != null && action == 'REMOVEEXCLUDE'){
			eventIds = getSelectedExcludeContestEventGridId();	
		}
		if(eventIds == null || eventIds == ''){
			jAlert("Please select atleast one Event(s) to add/remove Exclude");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateExcludeContestEvents",
				type : "post",
				dataType: "json",
				data: "contestId="+contestId+"&eventIdStr="+eventIds+"&action="+action,
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					if(jsonData.status == 1){
						getAllContestEventGridData(0);
						clearAllSelections();
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	//call functions once page loaded
	window.onload = function() {
		enableMenu();
	};
		
</script>