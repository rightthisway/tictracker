<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenu1 {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu1 li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu1 li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var questionArray = [];
var j = 0;
var jq2 = $.noConflict(true);
$(document).ready(function() {
	if(($('#artistId').val()=='' || $('#artistId').val()==null) && 
		($('#eventId').val()=='' || $('#eventId').val()==null) && 
		($('#parentId').val()=='' || $('#parentId').val()==null) && 
		($('#childId').val()=='' || $('#childId').val()==null) && 
		($('#grandChildId').val()=='' || $('#grandChildId').val()==null)){
		$('#resetLink').hide();
	}
	
	$('#contest_fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});

	$('#contest_toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$('#expiryDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	
	$("#allContest1").click(function(){
		callTabOnChange('ALL');
	});
	$("#expiredContest1").click(function(){
		callTabOnChange('EXPIRED');
	});
		
	$('#artistSearchAutoComplete').autocomplete("AutoCompleteArtistEventAndCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0] == 'ALL'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='PARENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='EVENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[3] ;
			}
		}
	}).result(function (event,row,formatted){
		if(row[0] == "ALL"){
			$('#selectedItem').text(row[2]);
		}else if(row[0]=="ARTIST"){
			$('#artistId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=="EVENT"){
			$('#eventId').val(row[1]);
			$('#selectedItem').text(row[3]);
		}else if(row[0]=='PARENT'){
			$('#parentId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=='CHILD'){
			$('#childId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=='GRAND'){
			$('#grandChildId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}
		$('#artistSearchAutoComplete').val("");
		$('#artistEventCategoryType').val(row[0]);
		$('#artistEventCategoryName').val(row[2]);		
		$('#resetLink').show();
	});
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(contestsGrid != null && contestsGrid != undefined){
			contestsGrid.resizeCanvas();
		}
		if(questionGrid != null && questionGrid != undefined){
			questionGrid.resizeCanvas();
		}
		if(contestQuesBankGrid != null && contestQuesBankGrid != undefined){
			contestQuesBankGrid.resizeCanvas();
		}
	});
	
	$('#contestQuestionTab').click(function(){				
		var contestsId = $('#contestIdStr').val();
		
		setTimeout(function(){
			getQuestionGridData(contestsId, 0);},10);
	});
	<c:choose>
	<c:when test="${status == 'ALL'}">	
		$('#allContest').addClass('active');
		$('#allContestTab').addClass('active');
	</c:when>
	<c:when test="${status == 'EXPIRED'}">
		$('#expiredContest').addClass('active');
		$('#expiredContestTab').addClass('active');
	</c:when>
	</c:choose>
	
	$('#coGiftCardSearchAutoComplete').autocomplete("AutoCompleteGiftCard", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		
		formatItem: function(row, i, max) {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] +" | "+ row[3] +" | "+ row[4] ;
		}
	}).result(function (event,row,formatted){
			$('#giftCardId').val(row[1]);
			$('#giftCardQty').val(row[4]);
			$('#giftCardAmt').val(row[3]);
			$('#selectedCoGiftCardItem').text(row[2] +" | "+ row[3] +" | "+ row[4]);
			$('#coGiftCardSearchAutoComplete').val("");
			$('#resetCoGiftCardLink').show();
			
			$('#giftCardPerWinner').val("");
			$('#giftCardPerWinnerDiv').show();
	});
	
	$('#giftCardSearchAutoComplete').autocomplete("AutoCompleteGiftCard", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		
		formatItem: function(row, i, max) {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] +" | "+ row[3] +" | "+ row[4] ;
		}
	}).result(function (event,row,formatted){
			$('#mjpGiftCardId').val(row[1]);
			$('#mjpGiftCardQty').val(row[4]);
			$('#mjpGiftCardAmt').val(row[3]);
			$('#selectedGiftCardItem').text(row[2] +" | "+ row[3] +" | "+ row[4]);
			$('#giftCardSearchAutoComplete').val("");
		$('#resetGiftCardLink').show();
	});
	
});

function resetCoGiftCardItem(){
		
	$('#resetCoGiftCardLink').hide();
	$('#giftCardId').val("");
	$('#giftCardQty').val("");
	$('#giftCardAmt').val("");
	$('#selectedCoGiftCardItem').text("");
	
	$('#giftCardPerWinner').val("");
	$('#giftCardPerWinnerDiv').hide();
}

function openAddContestModal(){
	resetModal();
	$.ajax({
		url : "${pageContext.request.contextPath}/GetPreContestChecklist",
		type : "post",
		dataType: "json",
		data: "status=ACTIVE&action=ADD&type=MOBILE",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status==1){
				 $('#preContestId').empty();
				 var selectOption = $("<option />");
				 selectOption.html("--Select--");
				 selectOption.val("-1");
                 $('#preContestId').append(selectOption);
				 for(var i=0;i<jsonData.preContestList.length;i++) {
	                var option1 = $("<option />");
	                option1.html(jsonData.preContestList[i].contestName+' - '+jsonData.preContestList[i].contestDateTimeStr);
	                option1.val(jsonData.preContestList[i].id);
	                $('#preContestId').append(option1);
				}
				$('#myModal-2').modal('show');
			}else{
				jAlert(jsonData.msg);
				return;
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
	
}


function fillContestData(){
	var preContestId =  $('#preContestId').val();
	if(preContestId > 0){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePreContestChecklist",
			type : "post",
			dataType: "json",
			data: "preContestId="+preContestId+"&action=EDIT&status=ACTIVE",
			success : function(response){
				var jsonData1 = JSON.parse(JSON.stringify(response));
				var jsonData = JSON.parse(jsonData1.preContest);
				if(jsonData1.status == 1){
					//$('#contest_id').val(data.id);
					$('#contestForm :input').attr('disabled',true);
					$('#contest_name').val(jsonData.contestName);
					$('#extContestName').val(jsonData.extendedName);
					$('#contest_fromDate').val(jsonData.contestDateStr);
					$('#startDateHour').val(jsonData.hours);
					$('#startDateMinute').val(jsonData.minutes);
					$('#maxTickets').val(jsonData.grandPrizeWinner);	
					//$('#pointsPerWinner').val(data.pointsPerWinner);
					$('#rewardPoints').val(jsonData.rewardDollarsPrize);
					$('#contestMode').val("NORMAL");
					$('#contestCategory').val(jsonData.contestCategory);
					//$('#contestCategory').val(jsonData.categoryName);
					$('#questionSize').val(jsonData.questionSize);
					$('#participantStar').val(data.participantStars);
					$('#referralStar').val(data.referralStars);
					$('#ticketsPerWinner').val(jsonData.grandWinnerTicketPrize);
					
					if(jsonData.categoryType != null && jsonData.categoryType == "ARTIST"){
						$('#artistId').val(jsonData.categoryId);
						$('#eventId').val("");
						$('#parentId').val("");
						$('#childId').val("");
						$('#grandChildId').val("");
					}
					if(jsonData.categoryType != null && jsonData.categoryType == "EVENT"){
						$('#eventId').val(jsonData.categoryId);
						$('#artistId').val("");					
						$('#parentId').val("");
						$('#childId').val("");
						$('#grandChildId').val("");
					}
					if(jsonData.categoryType != null && jsonData.categoryType == "PARENT"){
						$('#parentId').val(jsonData.categoryId);
						$('#artistId').val("");
						$('#eventId').val("");
						$('#childId').val("");
						$('#grandChildId').val("");
					}
					if(jsonData.categoryType != null && jsonData.categoryType == "CHILD"){
						$('#childId').val(jsonData.categoryId);					
						$('#artistId').val("");
						$('#eventId').val("");
						$('#parentId').val("");
						$('#grandChildId').val("");
					}
					if(jsonData.categoryType != null && jsonData.categoryType == "GRAND"){
						$('#grandChildId').val(jsonData.categoryId);
						$('#artistId').val("");
						$('#eventId').val("");
						$('#parentId').val("");
						$('#childId').val("");
					}
					$('#artistEventCategoryName').val(jsonData.categoryName);
					$('#artistEventCategoryType').val(jsonData.categoryType);
					$('#selectedItem').text(jsonData.categoryName);
					$('#resetLink').show();
					$('#promotionalCode').val(jsonData.discountCode);
					$('#discountPercentage').val(jsonData.discountPercentage);
					if(jsonData.zone != null && jsonData.zone != undefined){
						$('#zone').val(jsonData.zone);
					} 
					if(jsonData.pricePerTicket != null && jsonData.pricePerTicket != undefined){
						$('#tixPrice').val(jsonData.pricePerTicket);
					}
					//$('#promoOfferId').val(data.rtfPromoOfferId);
				}
			}
		});
	}else{
		resetModal();
	}
	
}

function resetModal(){
	$('#contestForm :input').attr('disabled',false);
	$('#contest_id').val('');
	$('#contest_name').val('');
	$('#extContestName').val('');
	$('#contest_fromDate').val('');
	$('#startDateHour').val('00');
	$('#startDateMinute').val('00');
	$('#maxTickets').val('');	
	//$('#pointsPerWinner').val('');
	$('#contestMode').val('NORMAL');
	$('#contestCategory').val('NONE');
	$('#questionSize').val('');	
	$('#participantStar').val('1');
	$('#referralStar').val('1');
	$('#referralReward').val('0');
	$('#participantLives').val('0');
	$('#participantRewards').val('0');
	$('#participantPoints').val('0');
	$('#referralPoints').val('0');
	$('#rewardPoints').val('');
	$('#ticketsPerWinner').val('');
	$('#artistId').val('');
	$('#parentId').val('');
	$('#childId').val('');
	$('#grandChildId').val('');
	$('#selectedItem').text('');
	$('#artistEventCategoryName').val('');
	$('#artistEventCategoryType').val('');
	$('#zone').val('');
	$('#promotionalCode').val('');
	$('#discountPercentage').val('');
	$('#tixPrice').val('');
	$('#contestJackpotType').val('NORMAL');
	$('#giftCardId').val('');
	$('#giftCardQty').val('');
	$('#giftCardAmt').val('');
	$('#gamePassword').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();
	$('#gamePassDiv').hide();
	resetCoGiftCardItem();
}

/* function startContest(){
	var index = contestsGrid.getSelectedRows([0])[0];
	if (index < 0) {
		jAlert("Please select Contest to start it.");
		return;
	}
	var contestId = contestsGrid.getDataItem(index).contestId;
	if(contestId < 0){
		jAlert("Please select Contest to start it.");
		return;
	}
	//$('#runningContestId').val(contestId);
	$('#runningQuestionNo').val(0);
	//$('#queNo').text("0.  ");
	$('#runningQuestionText').text('');
	$('#runningQuestionOptionA').text('');
	$('#runningQuestionOptionB').text('');
	$('#runningQuestionOptionC').text('');
	$('#runningQuestionAnswer').text('');
	//$('#contestStartModal').modal('show');
	//$('#startContestDiv').hide();
	$('#nextQuestion').text('Show Question-1');
	//nextQuestion();
} */

function resetQPositionModal(){
	var contestId = $('#q_contest_id').val();
	if(contestId == ''){
		jAlert("Please select Contest to update questions position.");
		return;
	}
	
	var tempContestRowIndex = contestsGrid.getSelectedRows([0])[0];
	if (tempContestRowIndex == null) {
		jAlert("Plese select Contest/Question to update position.", "info");
		return false;
	}
	
	var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
	if (tempQuestionRowIndex == null) {
		jAlert("Plese select Question to update position.", "info");
		return false;
	}	
	
	var questionText = questionGrid.getDataItem(tempQuestionRowIndex).question;
	var questionSize = contestsGrid.getDataItem(tempContestRowIndex).questionSize;
	$('#changeQPositionModal').modal('show');
	$('#changePQText').text(questionText);
	$('#changePositionSelect').empty();
	for(var i =1;i<=questionSize;i++){
		$('#changePositionSelect').append($('<option>', { 
	        value: i,
	        text : i 
	    }));
	}
	
}

function resetQModal(){
	var contestId = $('#q_contest_id').val();
	if(contestId == ''){
		jAlert("Please select Contest to add questions.");
		return;
	}
	$('#questionModal').modal('show');
	$('#questionId').val('');
	$('#difficultyLevel').val('Moderate');
	$('#questionText').val('');
	$('#optionA').val('');
	$('#optionB').val('');
	$('#optionC').val('');
	//$('#optionD').val('');	
	$('#answer').val('');
	$('#questionReward').val('10');
	//$('#questionNo').val('');
	$("#miniJackpotType" ).val('NONE');
	$("#mjpNoOfWinner" ).val('');
	$( "#mjpQtyPerWinner" ).val('');
	$('#mjpGiftCardId').val('');
	$('#mjpGiftCardQty').val('');
	$('#mjpGiftCardAmt').val('');
	$('#selectedGiftCardItem').text('');
	callGiftCardTypeChange('NONE');
	
	$('#qSaveBtn').show();
	$('#qUpdateBtn').hide();	
}


function questionSave(action){
	
	var text = $('#questionText').val();
	var oA = $('#optionA').val();
	var oB = $('#optionB').val();
	var oC = $('#optionC').val();
	var difficultyLevel = $('#difficultyLevel').val();	
	var answer = $('#answer').val();
	var qReward = $('#questionReward').val();
	//var qNo = $('#questionNo').val();
	
	if(text == ''){
		jAlert("Question Text is Mandatory.");
		return;
	}
	if(oA == ''){
		jAlert("Option A is Mandatory.");
		return;
	}
	if(oB == ''){
		jAlert("Option B is Mandatory.");
		return;
	}
	if(oC == ''){
		jAlert("Option C is Mandatory.");
		return;
	}
	if(difficultyLevel == ''){
		jAlert("Question Difficulty Level is Mandatory.");
		return;
	} 
	if(answer == ''){
		jAlert("Answer is Mandatory.");
		return;
	}
	 if(qReward == ''){
		jAlert("Question Reward Points are mendatory.");
		return;
	} 
	 var contestJackpotType =$( "#q_contest_jackpot_type" ).val();
	 
	 var gcType =$( "#miniJackpotType" ).val();
	 if(gcType != null && gcType != '' && gcType != 'NONE') {
		 
		 var mjpNoofWinner =$( "#mjpNoOfWinner" ).val();
		 if(mjpNoofWinner == null || mjpNoofWinner == '') {
			 jAlert("Enter Valid Jackpot No of winner.");
			 return; 
		 }
		 var mjpQtyPerWinner =$( "#mjpQtyPerWinner" ).val();
		 if(mjpQtyPerWinner == null || mjpQtyPerWinner == '') {
			
			 jAlert("Enter Valid Jackpot "+gcType+" per winner.");
			 return; 
		 }
		 
		 if(gcType == 'GIFTCARD') {
			 var mjpQtyPerWinner =$( "#mjpGiftCardId" ).val();
			 if(mjpQtyPerWinner == null || mjpQtyPerWinner == '') {
				
				 jAlert("Select Valid Jackpot Gift Card.");
				 return; 
			 }
		 }
	 } 
		
	var requestUrl = "${pageContext.request.contextPath}/UpdateQuestion";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#questionForm').serialize()+"&action=SAVE";
	}else if(action == 'update'){
		dataString = $('#questionForm').serialize()+"&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#questionModal').modal('hide');
				questionPagingInfo = jsonData.questionPagingInfo;
				questionColumnFilters = {};
				refreshContestQuestionGridValues(jsonData.questionList);
				//refreshQuestionGridValues('');
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function changeContestType(){
	var contestMode = $('#contestMode').val();
	if(contestMode == 'PASSWORD'){
		$('#gamePassDiv').show();
	}else{
		$('#gamePassDiv').hide();
	}
}

function contestSave(action){
	
	var contestName = $('#contest_name').val();
	var fromDate = $('#contest_fromDate').val();
	var maxTickes = $('#maxTickets').val();	
	var artistEventCategoryType = $('#artistEventCategoryType').val();
	var rewardPoints = $('#rewardPoints').val();
	var ticketsPerWinner = $('#ticketsPerWinner').val();
	var contestMode = $('#contestMode').val();
	var contestCategory = $('#contestCategory').val();
	var participantStar = $('#participantStar').val();
	var referralStar = $('#referralStar').val();
	var referralReward = $('#referralReward').val();
	var participantRewards = $('#participantRewards').val();
	var participantLives = $('#participantLives').val();
	var questionSize = $('#questionSize').val();
	var artistId = $('#artistId').val();
	var eventId = $('#eventId').val();
	var parentId = $('#parentId').val();
	var childId = $('#childId').val();
	var grandChildId = $('#grandChildId').val();
	var discountPerc = $('#discountPercentage').val();
	var promoCode = $('#promotionalCode').val();
	var preContestId = $('#preContestId').val();
	var contestJackpotType = $('#contestJackpotType').val();
	var giftCardId = $('#giftCardId').val();
	var giftCardPerWinner = $('#giftCardPerWinner').val();
	var gamePassword = $('#gamePassword').val();

	if(contestMode == 'PASSWORD'){
		if(gamePassword == '' || gamePassword  == null){
			jAlert('Please add password for the contest.');
			return;
		}
	}
	if(contestCategory == 'NONE'){
		jAlert('Please select Contest Category.');
		return;
	}
	
	if(contestName == ''){
		jAlert("Contest Name is Mandatory.");
		return;
	}
	if(maxTickes == ''){
		jAlert("Please enter Max. Ticket winners(Max. No. of users can we tickets.)");
		return;
	}
	if(participantStar == ''){
		jAlert("Please enter participant's superfan game star, It should be Greater than or equals to 0");
		return;
	}
	if(referralStar == ''){
		jAlert("Please enter referral's superfan game star, It should be Greater than or equals to 0");
		return;
	}
	if(referralReward == ''){
		jAlert("Please enter special referral reward, It should be Greater than or equals to 0");
		return;
	}
	if(participantLives == ''){
		jAlert("Please enter participant lives, It should be Greater than or equals to 0");
		return;
	}
	if(participantRewards == ''){
		jAlert("Please enter participant rewards, It should be Greater than or equals to 0");
		return;
	}
	if(rewardPoints == ''){
		jAlert("Please enter Reward Point Prize, It should be Greater than or equals to 0");
		return;
	}
	if(ticketsPerWinner == ''){
		jAlert("Please enter Tickets per winner.");
		return;
	}
	if(fromDate == ''){
		jAlert("Start Date is Mandatory.");
		return;
	}
	if(artistEventCategoryType != 'ALL'){
		if(artistId == '' && eventId == '' && parentId == '' && childId == '' && grandChildId == ''){
			jAlert("Please Select Any one Artist/Event/Categories or All.");
			return;
		}
	}
	if(contestJackpotType == '') {
		jAlert("Please Select Contest Jackpot Type.");
		return;
	}
	
	if(giftCardId != null && giftCardId!='') {
		if(giftCardPerWinner == null || giftCardPerWinner == '') {
			jAlert("Please Provide Valid Gift Card Per Winner.");
			return;
		}
	}
	
	/* if(zone == ''){
		jAlert("Please Enter Zone.");
		return;
	} */
	
	/* if(tixPrice == ''){
		jAlert("Please Enter Single Ticket Price.");
		return;
	} */
	
	if((discountPerc == '' && promoCode != '') || discountPerc != '' && promoCode == ''){
		jAlert("Please Enter Promotional code and Discount Percentage Both or Leave both empty.");
		return;
	}
	$('#contestForm :input').attr('disabled',false);
	var requestUrl = "${pageContext.request.contextPath}/UpdateContest";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#contestForm').serialize()+"&preContestId="+preContestId+"&action=SAVE&type="+$('#type').val()+"&status=${status}";
	}else if(action == 'update'){
		dataString = $('#contestForm').serialize()+"&preContestId="+preContestId+"&action=UPDATE&type="+$('#type').val()+"&status=${status}";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#myModal-2').modal('hide');
				pagingInfo = jsonData.contestsPagingInfo;
				columnFilters = {};
				refreshContestsGridValues(jsonData.contestsList);
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function resetItem(){
	$('#resetLink').hide();
	$('#artistId').val("");
	$('#eventId').val("");
	$('#parentId').val("");
	$('#childId').val("");
	$('#grandChildId').val("");
	$('#selectedItem').text("");
	$('#artistEventCategoryName').val("");
	$('#artistEventCategoryType').val("");
}


function resetGiftCardItem(){
	$('#resetGiftCardLink').hide();
	$('#mjpGiftCardId').val("");
	$('#mjpGiftCardQty').val("");
	$('#mjpGiftCardAmt').val("");
	$('#selectedGiftCardItem').text("");
}

function callGiftCardTypeChange(gcType){
	
	var contestJackpotType = $('#q_contest_jackpot_type').val();
	if(contestJackpotType == null || contestJackpotType =='NORMAL') {
		$('#miniJackpotTypeDiv').hide();
		$('#mjpNoofWinnerDiv').hide();
		$('#mjpQtyPerWinnerDiv').hide();
		$('#mjpGiftCardDiv').hide();
	} else {
		$('#miniJackpotTypeDiv').show();
		if(gcType == null || gcType == '' || gcType =='NONE') {
			$('#mjpNoofWinnerDiv').hide();
			$('#mjpQtyPerWinnerDiv').hide();
			$('#mjpGiftCardDiv').hide();
			
			resetGiftCardItem();
		} else if (gcType == 'GIFTCARD') {
			$('#mjpNoofWinnerDiv').show();
			$('#mjpQtyPerWinnerDiv').show();
			$('#mjpGiftCardDiv').show();
			$('#mjpQtyLabelSpan').text("Gift Cards Per Winner");
			
			//resetGiftCardItem();
		} else if(gcType == 'TICKET') {
			$('#mjpNoofWinnerDiv').show();
			$('#mjpQtyPerWinnerDiv').show();
			$('#mjpGiftCardDiv').hide();
			$('#mjpQtyLabelSpan').text("Ticets Per Winner");
			resetGiftCardItem();
		} else if(gcType == 'LIFELINE') {
			$('#mjpNoofWinnerDiv').show();
			$('#mjpQtyPerWinnerDiv').show();
			$('#mjpGiftCardDiv').hide();
			$('#mjpQtyLabelSpan').text("LifeLine Per Winner");
			resetGiftCardItem();
		} else if(gcType == 'REWARD') {
			$('#mjpNoofWinnerDiv').show();
			$('#mjpQtyPerWinnerDiv').show();
			$('#mjpGiftCardDiv').hide();
			$('#mjpQtyLabelSpan').text("Rewards Per Winner");
			resetGiftCardItem();
		} else if(gcType == 'SUPERFANSTAR') {
			$('#mjpNoofWinnerDiv').show();
			$('#mjpQtyPerWinnerDiv').show();
			$('#mjpGiftCardDiv').hide();
			$('#mjpQtyLabelSpan').text("Superfan Stars Per Winner");
			resetGiftCardItem();
		}else if(gcType == 'POINTS') {
			$('#mjpNoofWinnerDiv').show();
			$('#mjpQtyPerWinnerDiv').show();
			$('#mjpGiftCardDiv').hide();
			$('#mjpQtyLabelSpan').text("RTF Points Per Winner");
			resetGiftCardItem();
		}
		
	}
}

/* function resumeContest(){
	var index = contestsGrid.getSelectedRows([0])[0];
	if (index < 0) {
		jAlert("Please select Contest to resume it.");
		return;
	}
	var contestId = contestsGrid.getDataItem(index).contestId;
	if(contestId < 0){
		jAlert("Please select Contest to resume it.");
		return;
	}
	var status = contestsGrid.getDataItem(index).status;
	if(status != 'STARTED'){
		jAlert("Selected contest is not started, you can resume only started contest.");
		return;
	}
	window.location = "${pageContext.request.contextPath}/ContinueContest?type=MOBILE";
} */


function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ContestsFirebaseMobile?status="+selectedTab;
}


</script>
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
</ul>
<ul id="contextMenu1" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>All Contests</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="allContestTab" class=""><a id="allContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#allContest">Active Contests</a></li>
				<li id="expiredContestTab" class=""><a id="expiredContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredContest">Expired Contests</a></li>
			</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="allContest" class="tab-pane">
			<c:if test="${status =='ALL'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary"  onclick="openAddContestModal();">Add Contest</button>
					<button type="button" class="btn btn-primary" onclick="editContest();">Edit Contest</button>
					<button type="button" class="btn btn-primary" onclick="deleteContest();">Delete Contest</button>
					<button type="button" class="btn btn-primary" onclick="resetContest('RESET');">Reset Contest</button>
					<button type="button" class="btn btn-primary" onclick="resetContest('END');">End Contest</button>
					<button type="button" class="btn btn-primary" onclick="resetContest('REFRESH');">Refresh API Cache</button>
					<button type="button" class="btn btn-primary" onclick="copyContestToSandbox();">Copy to Host APP</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Active Contests</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contests_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="contests_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<input type="hidden" name="contestIdStr" id="contestIdStr"/>
				<br />
				</c:if>
			</div>
			<div id="expiredContest" class="tab-pane">
			<c:if test="${status =='EXPIRED'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary" onclick="editContest();">Edit Contest</button>
					<button type="button" class="btn btn-primary" onclick="deleteContest();">Delete Contest</button>
					<button type="button" class="btn btn-primary" onclick="resetContest('ACTIVE');">Move to Active</button>
					<button type="button" class="btn btn-primary" onclick="viewWinners('');">View Winners</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Expired Contests</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contests_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="contests_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<input type="hidden" name="contestIdStr" id="contestIdStr"/>
				<br />
			</c:if>
			</div>																				
		</div>
	</div>	
		
	<div style="position: relative" id="questionGridDiv" class="tab-pane active">
		<div class="full-width mb-20 mt-20 full-width-btn">
			<button class="btn btn-primary" id="addQBtn" type="button" data-toggle="modal" onclick="resetQModal();">Add Question</button>
			<button class="btn btn-primary" id="editQBtn" type="button" onclick="editQuestion()">Edit Question</button>
			<button class="btn btn-primary" id="deleteQBtn" type="button" onclick="deleteQuestion()">Delete Question</button>
			<button class="btn btn-primary" id="uqrBtn" type="button" onclick="createQuestionRewardUI()"> Update Question Rewards </button>
			<button class="btn btn-primary" id="addQBBtn" type="button" onclick="getContestQuesBankGrid()"> Add From Question Bank </button>	
			<button class="btn btn-primary" id="updateQPBtn" type="button" onclick="updateQuestionPosition()">Update Question Position</button>								
		</div>
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Contests Questions</label>
				<div class="pull-right">
					<a href="javascript:questionExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
					<a href="javascript:questionResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
				</div>
			</div>
			<div id="question_grid" style="width: 100%; height: 430px; border: 1px solid gray"></div>
			<div id="question_pager" style="width: 100%; height: 10px;"></div>
	
		</div>
	</div>
					
</div>


<!-- Add Contest -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest</h4>
			</div>
			<div class="modal-body full-width">
					<div class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Pre Contest CheckList</label>
							<select name="preContestId" id="preContestId" class="form-control"  onChange="fillContestData()">
							</select>
						</div>
					<form name="contestForm" id="contestForm" method="post">
					<input type="hidden" id="contest_id" name="contestId" />
					<input type="hidden" id="questionRewardType" name="questionRewardType" value="POINTS" />
					<!-- <input type="hidden" id="questionRewardType" name="questionRewardType" value="DOLLARS" /> -->
						<div class="form-group col-sm-6 col-xs-6">
							<label>Contest Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_name" name="contestName" >
						</div>	
						<div class="form-group col-sm-6 col-xs-6">
							<label>Extended Contest Name 
							</label> <input class="form-control" type="text" id="extContestName" name="extContestName" >
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Start Date <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_fromDate" name="contestFromDate" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Start Time <span class="required">*</span>
							<select name="startDateHour" id="startDateHour" class="form-control" style="width:120px;" >
								<option value="0">00</option>
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
								<option value="4">04</option>
								<option value="5">05</option>
								<option value="6">06</option>
								<option value="7">07</option>
								<option value="8">08</option>
								<option value="9">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Minutes <span class="required">*</span>
							<select name="startDateMinute" id="startDateMinute" class="form-control" style="width:120px;" >
								<option value="00">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
							<input type="hidden" id="type" name="type" value="MOBILE" />
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Max. Tickets Winners<span class="required">*</span>
							</label> <input class="form-control" type="text" id="maxTickets" name="maxTickets" >
						</div>
						<!-- <div class="form-group col-sm-3 col-xs-3">
							<label>Points/Winner<span class="required">*</span>
							</label> <input class="form-control" type="text" id="pointsPerWinner" name="pointsPerWinner">
						</div> -->
						<div class="form-group col-sm-3 col-xs-3">
							<label>Reward Dollars Prize <span class="required">*</span>
							</label> <input class="form-control" type="text" id="rewardPoints" name="rewardPoints" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Tickets per Winner<span class="required">*</span>
							</label> <input class="form-control" type="text" id="ticketsPerWinner" name="ticketsPerWinner" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Contest Type<span class="required">*</span></label> 
							<select onchange="changeContestType()" name="contestMode" id="contestMode" class="form-control" >
								<option value="NORMAL">Normal Game</option>
								<option value="PASSWORD">Game with Password</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3" id="gamePassDiv">
							<label>Game Password<span class="required">*</span>
							</label> <input class="form-control" type="text" id="gamePassword" name="gamePassword" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Contest Category<span class="required">*</span></label> 
							<select name="contestCategory" id="contestCategory" class="form-control" >
								<option value="NONE">-- Select Category --</option>
								<option value="ALL">ALL</option>
								<option value="SUPERFAN">SUPERFAN</option>
								<option value="SPORTS">SPORTS</option>
								<option value="CONCERTS">CONCERTS</option>
								<option value="THEATER">THEATER</option>
								<option value="MOVIE">MOVIE</option>
								<option value="BASKETBALL">BASKETBALL</option>
								<option value="BASEBALL">BASEBALL</option>
								<option value="FOOTBALL">FOOTBALL</option>
								<option value="HOCKEY">HOCKEY</option>
								<option value="SOCCER">SOCCER</option>
								<option value="CUSTOM">CUSTOM</option>
								<option value="FIGHT">FIGHT</option>
								<option value="MARKET">MARKETING</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3" id="contestQSizeDiv">
							<label>Number of Questions<span class="required">*</span></label> 
							<input class="form-control" type="text" id="questionSize" name="questionSize" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label for="name" class="control-label">Artist</label> 
							<input class="form-control searchcontrol" type="text" id="artistSearchAutoComplete" name="artistSearchAutoComplete" placeholder="Artist" > 
							<input type="hidden" value="${artistId}" id="artistId" name="artistId" /> 
							<input type="hidden" value="${eventId}" id="eventId" name="eventId" /> 
							<input type="hidden" value="${parentId}" id="parentId" name="parentId" /> 
							<input type="hidden" value="${childId}" id="childId" name="childId" /> 
							<input type="hidden" value="${grandChildId}" id="grandChildId" name="grandChildId" />
							<input type="hidden" id="artistEventCategoryName" name="artistEventCategoryName" />
							<input type="hidden" id="artistEventCategoryType" name="artistEventCategoryType" />
							<label for="name" id="selectedItem">${artistName}</label> <a href="javascript:resetItem()" id="resetLink">remove</a>
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Zone
							</label> <input class="form-control" type="text" id="zone" name="zone" >
						</div>
						
						<div class="form-group col-sm-3 col-xs-3">
							<label>Single Ticket Price 
							</label> <input class="form-control" type="text" id="tixPrice" name="tixPrice" >
						</div>
						
						<div class="form-group col-sm-3 col-xs-3">
							<label>Promotional Code
							</label> <input class="form-control" type="text" id="promotionalCode" name="promotionalCode" >
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Discount
							</label> <input class="form-control" type="text" id="discountPercentage" name="discountPercentage" >% <input type="hidden" id="promoOfferId" name="promoOfferId" />
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Participant's SuperFan Game Star<span class="required">*</span></label> 
							<input class="form-control" type="text" id="participantStar" name="participantStar" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Referral's SuperFan Game Star<span class="required">*</span></label> 
							<input class="form-control" type="text" id="referralStar" name="referralStar" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Participant Reward Dollar Prize<span class="required">*</span></label> 
							<input class="form-control" type="text" id="participantRewards" name="participantRewards" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Referral Reward Dollars Prize<span class="required">*</span></label> 
							<input class="form-control" type="text" id="referralReward" name="referralReward" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Participant Points Prize<span class="required">*</span></label> 
							<input class="form-control" type="text" id="participantPoints" name="participantPoints" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Referral Points Prize<span class="required">*</span></label> 
							<input class="form-control" type="text" id="referralPoints" name="referralPoints" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Participant Lives Prize<span class="required">*</span></label> 
							<input class="form-control" type="text" id="participantLives" name="participantLives" >
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="coGiftCardDiv">
							<label for="coGiftCard" class="control-label">Gift Card</label> 
							<input class="form-control searchcontrol" type="text" id="coGiftCardSearchAutoComplete" name="coGiftCardSearchAutoComplete" placeholder="GiftCard" > 
							<input type="hidden" id="giftCardId" name="giftCardId" /> 
							<input type="hidden" id="giftCardQty" name="giftCardQty" />
							<input type="hidden" id="giftCardAmt" name="giftCardAmt" />
							<label for="name" id="selectedCoGiftCardItem"></label> <a href="javascript:resetCoGiftCardItem()" id="resetCoGiftCardLink">remove</a>
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="giftCardPerWinnerDiv">
							<label><span>Gift Cards Per Winner</span><span class="required">*</span>
							</label> <input class="form-control" type="text" id="giftCardPerWinner" name="giftCardPerWinner">
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Contest Jackpot Type<span class="required">*</span></label> 
							<select name="contestJackpotType" id="contestJackpotType" class="form-control" >
								<option value="NORMAL">NORMAL</option>
								<option value="MEGA" >MEGA</option>
								<option value="MINI" >MINI</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="contestSave('save')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="contestSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Ends of Add Shipping/Other Address popup-->

<!-- Video Source Url Update - start  -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="videoSourceUrlModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Video Url</h4>
			</div>
			<div class="modal-body full-width">
				<form name="videoSourceUrlForm" id="videoSourceUrlForm" method="post">
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Video Source Url<span class="required">*</span>
							</label> <input class="form-control" type="text" id="videoSourceUrl" name="videoSourceUrl">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>App Sync Url<span class="required">*</span>
							</label> <input class="form-control" type="text" id="appSyncUrl" name="appSyncUrl">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>App Sync Token<span class="required">*</span>
							</label> <input class="form-control" type="text" id="appSyncToken" name="appSyncToken">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>Android JW Player Licence Key<span class="required">*</span>
							</label> <input class="form-control" type="text" id="jwPlayerLicenceKeyAndroid" name="jwPlayerLicenceKeyAndroid">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>IOS JW Player Licence Key<span class="required">*</span>
							</label> <input class="form-control" type="text" id="jwPlayerLicenceKeyIOS" name="jwPlayerLicenceKeyIOS">
						</div>
					<div class="form-group tab-fields">
							<div class="form-group col-sm-12 col-xs-12">
								<label>Partner Id<span class="required">*</span>
								</label> <input class="form-control" type="text" id="partnerId" name="partnerId">
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>Source Id<span class="required">*</span>
								</label> <input class="form-control" type="text" id="sourceId" name="sourceId">
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>Entry Id<span class="required">*</span>
								</label> <input class="form-control" type="text" id="entryId" name="entryId">
							</div>
							<div class="form-group col-sm-12 col-xs-12">
								<label>Live Stream URL<span class="required">*</span>
								</label> <input class="form-control" type="text" id="liveStreamUrl" name="liveStreamUrl">
							</div>						
						</div>					
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qUpdateConfigBtn" type="button" onclick="updateQuizConfigSettings('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div> -->
<!-- Video Source Url Update - Ends  -->

<!-- Add Questions -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="questionModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Question</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionForm" id="questionForm" method="post">
					<input type="hidden" id="q_contest_id" name="qContestId" />
					<input type="hidden" id="q_contest_jackpot_type" name="qContestJackpotType" />
					<input type="hidden" id="questionId" name="questionId" />
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Question Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionText" name="questionText">
						</div>						
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option A <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionA" name="optionA">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option B <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionB" name="optionB">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Option C <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionC" name="optionC">
						</div>
						<!-- <div class="form-group col-sm-6 col-xs-6">
							<label>Option D <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionD" name="optionD">
						</div> -->
						<div class="form-group col-sm-4 col-xs-4">
							<label>Answer <span class="required">*</span>
							<select name="answer" id="answer" class="form-control">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<!-- <option value="D">D</option> -->
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Question Reward Points<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionReward" name="questionReward">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Answer <span class="required">*</span>
							<select name="difficultyLevel" id="difficultyLevel" class="form-control">
								<option value="Super Easy">Super Easy</option>
								<option value="Easy">Easy</option>
								<option value="Moderate">Moderate</option>
								<option value="Hard">Hard</option>
								<option value="Super Hard">Super Hard</option>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="miniJackpotTypeDiv">
							<label>Jackpot Type<span class="required">*</span></label>
							<select name="miniJackpotType" id="miniJackpotType" class="form-control" onchange="callGiftCardTypeChange(this.value);">
								<option value="NONE">NONE</option>
								<option value="GIFTCARD" >GIFTCARD</option>
								<option value="TICKET"  >TICKET</option>
								<option value="LIFELINE"  >LIFELINE</option>
								<option value="REWARD"  >REWARD</option>
								<option value="SUPERFANSTAR" >SUPERFANSTAR</option>
								<option value="POINTS">RTF POINTS</option>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="mjpNoofWinnerDiv">
							<label>No of winners<span class="required">*</span>
							</label> <input class="form-control" type="text" id="mjpNoOfWinner" name="mjpNoOfWinner">
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="mjpQtyPerWinnerDiv">
							<label><span id="mjpQtyLabelSpan">Qty per winner</span><span class="required">*</span>
							</label> <input class="form-control" type="text" id="mjpQtyPerWinner" name="mjpQtyPerWinner">
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="mjpGiftCardDiv">
							<label for="giftCard" class="control-label">Gift Card</label> 
							<input class="form-control searchcontrol" type="text" id="giftCardSearchAutoComplete" name="giftCardSearchAutoComplete" placeholder="GiftCard" > 
							<input type="hidden" id="mjpGiftCardId" name="mjpGiftCardId" />
							<input type="hidden" id="mjpGiftCardQty" name="mjpGiftCardQty" />
							<input type="hidden" id="mjpGiftCardAmt" name="mjpGiftCardAmt" />
							<label for="name" id="selectedGiftCardItem">${artistName}</label> <a href="javascript:resetGiftCardItem()" id="resetGiftCardLink">remove</a>
							
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qSaveBtn" type="button" onclick="questionSave('save')">Save</button>
				<button class="btn btn-primary" id="qUpdateBtn" type="button" onclick="questionSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add Question end here  -->


<!--Change Question position Start here -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="changeQPositionModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Change Question Position</h4>
			</div>
			<div class="modal-body full-width">
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label id="changePQText">
							</label> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							&nbsp;&nbsp;
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Select Position <span class="required">*</span>
							<select name="changePositionSelect" id="changePositionSelect" class="form-control">
							</select>
						</div>
					</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="chagneQPUpdateBtn" type="button" onclick="updateQuestionPosition('','MANUAL')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Change Question position end here  -->



<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="questionRewardModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Question Reward Points</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionRewardForm" id="questionRewardForm" method="post">
					<div id="questionRewardDiv" class="form-group tab-fields">
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qrUpdateBtn" type="button" onclick="updateQuestionReward()">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Start contest modal -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="contestStartModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Started</h4>
			</div>
			<div class="modal-body full-width">
				<form name="runningContestForm" id="runningContestForm" method="post">
					<input type="hidden" id="runningContestId" name="runningContestId" />
					<input type="hidden" id="runningQuestionNo" name="runningQuestionNo" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label id="queNo">&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionText"></label>
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>A.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionA"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>B.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionB"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>C.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionC"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>D.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionD"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Answer:&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionAnswer"></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="nextQuestion" type="button" onclick="nextQuestion()">Question-1</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div> -->
<!-- Start contest modal  -->

<!-- Add Question(s) from  Question Bank Modal -->
	<div id="add-question-bank" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Question(s) - Contest : <span id="contestName_Hdr_QuesBank" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Contest
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Contest Mobile</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Question Bank</li>
						</ol>
					</div>
				</div>
				<br />
				
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Questions</label>
							<div class="pull-right">
								<a href="javascript:contestQuesBankExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:contestQuesBankResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contestQuesBank_grid" style="width: 100%; height: 280px; border:1px solid gray"></div>
						<div id="contestQuesBank_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="contestId_QuesBank" name="contestId_QuesBank" />
				<button type="button" class="btn btn-primary" onclick="addQuestionsFromQuesBank()">Add Question(s)</button>				
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	<!-- End - Add Question(s) from  Question Bank Modal -->
	
	<!-- Winners Modal start  -->
	<div id="view-contest-winners" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Expired Contest : <span id="contestName_Hdr_QuesBank" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Contest
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Contest Mobile</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Contest Winners</li>
						</ol>
					</div>
					<div class="col-xs-12 filters-div">
						<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
							<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="openExpiryPopup();">Extend Expiry Date</button>
						</div>
					</div>
				</div>
				<br />
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Contest Grand/Jackpot Winners</label>
							<div class="pull-right">
								<!--<a href="javascript:winnerExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
								<a href="javascript:contestWinnerResetFilters('GRAND')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contestGrandWinner_grid" style="width: 100%; height: 200px; border:1px solid gray"></div>
						<div id="contestGrandWinner_pager" style="width: 100%; height: 10px;"></div>
					</div>
					
				</div>
				<br />
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Contest Summary Winners</label>
							<div class="pull-right">
								<!--<a href="javascript:winnerExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
								<a href="javascript:contestWinnerResetFilters('SUMMARY')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contestWinner_grid" style="width: 100%; height: 200px; border:1px solid gray"></div>
						<div id="contestWinner_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="contestId_winner" name="contestId_winner" />
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	<!-- Winners Modal End  -->
	
	
	<div id="extend-winner-expiry-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Extend Winner Expiry Date</h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Contest
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Contest Mobile</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Contest Winners</li>
						</ol>
					</div>
					<div class="col-xs-12 filters-div">
						<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
							<input type="hidden" id="action" name="action" value="" /> <label for="name" class="control-label">Expiry Date</label> 
							<input class="form-control" type="text" id="expiryDate" name="expiryDate" >
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="winnerId" name="winnerId" />
				<button type="button" class="btn btn-primary" onclick="updateExpiryDate()">Update</button>				
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	

<script type="text/javascript">
	
	function openExpiryPopup(){
		var tempRowIndex = contestGrandWinnerGrid.getSelectedRows([0])[0];
		if (tempRowIndex == null) {
			jAlert("Plese Grand winner to change expiry date.", "info");
			return false;
		}else{
			var winnerId = contestGrandWinnerGrid.getDataItem(tempRowIndex).winnerId;
			$('#winnerId').val(winnerId);
			$('#extend-winner-expiry-modal').modal('show');
		}
		
	}
	
	function updateExpiryDate(){
		var winnerId = $('#winnerId').val();
		if(winnerId == null || winnerId =='' || winnerId==undefined){
			jAlert("Winner id is not found", "info");
			return false;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateExpiryDate.json",
			type : "post",
			dataType: "json",
			data: "winnerId="+winnerId,
			success : function(response){
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				if(jsonData.status == 1){
					contestWinnerResetFilters("GRAND");
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	//Contest Grid
	function getContestsGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestsFirebaseMobile.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+contestsSearchString+"&status=${status}",
			success : function(response){
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.contestsPagingInfo;
				refreshContestsGridValues(jsonData.contestsList);
				clearAllSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcel(type){
		var appendData = "headerFilter="+contestsSearchString+"&status="+type+"&type=MOBILE";
	    var url = apiServerUrl+"ContestExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function questionExportToExcel(){
		var contId = $('#contestIdStr').val();
		var appendData = "contestId="+contId+"&headerFilter="+questionSearchString;
	    var url = apiServerUrl+"QuestionExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function contestQuesBankExportToExcel(){
		var appendData = "headerFilter="+contestQuesBankSearchString+"&status=ACTIVE";
	    var url =apiServerUrl+"ContestQuesBankExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	
	function resetFilters(){
		contestsSearchString='';
		columnFilters = {};
		getContestsGridData(0);
		//refreshQuestionGridValues('');
	}

	/* var contestsCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var pagingInfo;
	var contestsDataView;
	var contestsGrid;
	var contestsData = [];
	var contestsGridPager;
	var contestsSearchString='';
	var columnFilters = {};
	var userContestsColumnsStr = '<%=session.getAttribute("contestsgrid")%>';

	var userContestsColumns = [];
	var loadContestsColumns = ["contestId","contestName", "startDate","freeTicketPerWinner", "ticketWinnerThreshhold", "rewardPoints","participantStar","referralStar",
	                           "contestMode", "contestCategory","questionSize", "artistEventCategoryName", "promotionalCode", "discountPercentage", "contestPassword",
	                           "referralReward","participantRewards","participantPoints","referralPoints","participantLives","giftCardName","giftCardPerWinner","status","contestJackpotType", "createdDate", "updatedDate", "createdBy", "updatedBy","getContestUser", "editContest", 
	                           "delContest"];
	var allContestsColumns = [ {
		id : "contestId",
		name : "Contest ID",
		field : "contestId",
		width : 80,
		sortable : true
	},{
				id : "contestName",
				name : "Contest Name",
				field : "contestName",
				width : 80,
				sortable : true
			}, {
				id : "startDate",
				name : "Start Date Time",
				field : "startDate",
				width : 80,
				sortable : true
			},{
				id : "freeTicketPerWinner",
				name : "Free Ticket/Winner",
				field : "freeTicketPerWinner",
				width : 80,
				sortable : true
			},{
				id : "ticketWinnerThreshhold",
				name : "Max. Ticket Winner",
				field : "ticketWinnerThreshhold",
				width : 80,
				sortable : true
			},{
				id : "rewardPoints",
				name : "Reward Points",
				field : "rewardPoints",
				width : 80,
				sortable : true
			},{
				id : "participantStar",
				name : "Participant Star",
				field : "participantStar",
				width : 80,
				sortable : true
			},{
				id : "referralStar",
				name : "Referral Star",
				field : "referralStar",
				width : 80,
				sortable : true
			},{
				id : "participantRewards",
				name : "Participant Reward Dollars",
				field : "participantRewards",
				width : 80,
				sortable : true
			},{
				id : "referralReward",
				name : "Referral Reward Dollars",
				field : "referralReward",
				width : 80,
				sortable : true
			},{
				id : "participantPoints",
				name : "Participant Points",
				field : "participantPoints",
				width : 80,
				sortable : true
			},{
				id : "referralPoints",
				name : "Referral Points",
				field : "referralPoints",
				width : 80,
				sortable : true
			},{
				id : "participantLives",
				name : "Participant Lives",
				field : "participantLives",
				width : 80,
				sortable : true
			},{
				id : "contestMode",
				name : "Contest Mode",
				field : "contestMode",
				width : 80,
				sortable : true
			},{
				id : "contestPassword",
				name : "Contest Password",
				field : "contestPassword",
				width : 80,
				sortable : true
			}, {
				id : "contestCategory",
				name : "Contest Category",
				field : "contestCategory",
				width : 80,
				sortable : true
			},{
				id : "questionSize",
				name : "Number of Que.",
				field : "questionSize",
				width : 80,
				sortable : true
			},{
				id : "artistEventCategoryName",
				name : "Name",
				field : "artistEventCategoryName",
				width : 80,
				sortable : true
			},{
				id : "artistEventCategoryType",
				name : "Type",
				field : "artistEventCategoryType",
				width : 80,
				sortable : true
			},{
				id : "promotionalCode",
				name : "Promotional Code",
				field : "promotionalCode",
				width : 80,
				sortable : true
			},{
				id : "discountPercentage",
				name : "Discount Percentage",
				field : "discountPercentage",
				width : 80,
				sortable : true
			},{
				id : "zone",
				name : "Zone",
				field : "zone",
				width : 80,
				sortable : true
			},{
				id : "singleTixPrice",
				name : "Single Ticket Price",
				field : "singleTixPrice",
				width : 80,
				sortable : true
			},{
				id : "promoOfferId",
				name : "PromoOfferId",
				field : "promoOfferId",
				width : 80,
				sortable : true
			},{
				id : "giftCardName",
				name : "Gift Card",
				field : "giftCardName",
				width : 80,
				sortable : true
			}, 
			{
				id : "giftCardPerWinner",
				name : "Gift Cards Per Winner",
				field : "giftCardPerWinner",
				width : 80,
				sortable : true
			}, 
			{
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			}, {
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			}, {
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			}, {
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			}, {
				id : "updatedBy",
				field : "updatedBy",
				name : "ModifiedBy ",
				width : 80,
				sortable : true
			},{
				id : "contestJackpotType",
				field : "contestJackpotType",
				name : "Jackpot Type",
				width : 80,
				sortable : true
			},{
				id : "getContestUser",
				field : "getContestUser",
				name : "Export Contest Users",
				width : 100,
				formatter: exportContestUserFormatter
			},  {
				id : "editContest",
				field : "editContest",
				name : "Edit ",
				width : 80,
				formatter: editFormatter
			}, {
				id : "delContest",
				field : "delContest",
				name : "Delete ",
				width : 80,
				formatter:buttonFormatter
			}];

	if (userContestsColumnsStr != 'null' && userContestsColumnsStr != '') {
		columnOrder = userContestsColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestsColumns.length; j++) {
				if (columnWidth[0] == allContestsColumns[j].id) {
					userContestsColumns[i] = allContestsColumns[j];
					userContestsColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//userContestsColumns = allContestsColumns;
		var columnOrder = loadContestsColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allContestsColumns.length;j++){
				if(columnWidth == allContestsColumns[j].id){
					userContestsColumns[i] = allContestsColumns[j];
					userContestsColumns[i].width=80;
					break;
				}
			}			
		}
	}

	function editFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.contestId +"'/>";
	    return button;
	}
	function exportContestUserFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<span class='label btn-primary' style='font-size:100%;' onclick='getContestUserExport("+ dataContext.contestId + ")'>Export Customer</span>";
	    return button;
	}
	
	function getContestUserExport(contestId){
	    var url = apiServerUrl + "ExportContestUsers?contestId="+contestId;
	    $('#download-frame').attr('src', url);
	}
	
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditContest(id);
	});
	
	function buttonFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.contestId +"' data-id='"+ dataContext.promoOfferId +"'/>";		
		return button;
	}
	$('.delClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    var promoId = me.attr('data-id');
	    getDeleteContest(id,promoId);
	});
	
	var contestsOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var contestsGridSortcol = "contestId";
	var contestsGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestsGridComparer(a, b) {
		var x = a[contestsGridSortcol], y = b[contestsGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshContestsGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestsData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestsData[i] = {});
				d["id"] = i;
				d["contestId"] = data.id;
				d["contestName"] = data.contestName;
				d["startDate"] = data.startDateTimeStr;
				d["contestCategory"] = data.contestCategory;
				d["ticketWinnerThreshhold"] = data.ticketWinnerThreshhold;
				d["freeTicketPerWinner"] = data.freeTicketPerWinner;
				d["participantStar"] = data.participantStars;
				d["referralStar"] = data.referralStars;
				d["referralReward"] = data.referralRewards;
				d["participantRewards"] = data.participantRewards;
				d["participantPoints"] = data.participantPoints;
				d["referralPoints"] = data.referralPoints;
				d["participantLives"] = data.participantLives;
				d["rewardPoints"] = data.rewardPoints;
				d["contestMode"] = data.contestMode;
				d["contestPassword"] = data.contestPassword;
				d["questionSize"] = data.questionSize;
				d["artistEventCategoryName"] = data.promoRefName;
				d["artistEventCategoryType"] = data.promoRefType;
				d["promotionalCode"] = data.promotionalCode;
				d["discountPercentage"] = data.discountPercentage;
				d["zone"] = data.zone;
				d["singleTixPrice"] = data.singleTicketPrice;
				d["promoOfferId"] = data.rtfPromoOfferId;
				d["status"] = data.status;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
				
				d["giftCardId"] = data.giftCardId;
				d["giftCardQty"] = data.giftCardQty;
				d["giftCardAmt"] = data.giftCardAmt;
				d["giftCardName"] = data.giftCardName;
				d["giftCardPerWinner"] = data.giftCardPerWinner;
				d["contestJackpotType"] = data.contestJackpotType;
			}
		}

		contestsDataView = new Slick.Data.DataView();
		contestsGrid = new Slick.Grid("#contests_grid", contestsDataView,
				userContestsColumns, contestsOptions);
		contestsGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestsGrid.setSelectionModel(new Slick.RowSelectionModel());
		//contestsGrid.registerPlugin(contestsCheckboxSelector);
		
			contestsGridPager = new Slick.Controls.Pager(contestsDataView,
					contestsGrid, $("#contests_pager"),
					pagingInfo);
		var contestsGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestsColumns, contestsGrid, contestsOptions);
					
		contestsGrid.onSort.subscribe(function(e, args) {
			contestsGridSortdir = args.sortAsc ? 1 : -1;
			contestsGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestsDataView.fastSort(contestsGridSortcol, args.sortAsc);
			} else {
				contestsDataView.sort(contestsGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestsGrid
		contestsDataView.onRowCountChanged.subscribe(function(e, args) {
			contestsGrid.updateRowCount();
			contestsGrid.render();
		});
		contestsDataView.onRowsChanged.subscribe(function(e, args) {
			contestsGrid.invalidateRows(args.rows);
			contestsGrid.render();
		});
		$(contestsGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestsSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											contestsSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getContestsGridData(0);
								}
							}

						});
		contestsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editContest' && args.column.id != 'delContest'){
					if(args.column.id == 'startDate' || args.column.id == 'updatedDate' || args.column.id == 'createdDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		contestsGrid.init();
		
		var contestsRowIndex = -1;
		contestsGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
			if (tempContestsRowIndex != contestsRowIndex) {
				var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
				var contestJackpotType = contestsGrid.getDataItem(tempContestsRowIndex).contestJackpotType;
				$('#q_contest_id').val(contestId);
				$('#q_contest_jackpot_type').val(contestJackpotType);
				$('#contestIdStr').val(contestId);
				getQuestionGridData(contestId,0);
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		contestsDataView.beginUpdate();
		contestsDataView.setItems(contestsData);
		//contestsDataView.setFilter(filter);
		contestsDataView.endUpdate();
		contestsDataView.syncGridSelection(contestsGrid, true);
		contestsGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserContestsPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestsGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('contestsgrid', colStr);
	}
	
	function pagingControl(move, id) {
		if(id == 'contests_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getContestsGridData(pageNo);
		} else if(id == 'question_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(questionPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(questionPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(questionPagingInfo.pageNum) - 1;
			}
			var contestsId = $('#contestIdStr').val();
			getQuestionGridData(contestsId, pageNo);
		} else if(id == 'contestQuesBank_pager') {
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(contestQuesBankPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(contestQuesBankPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(contestQuesBankPagingInfo.pageNum) - 1;
			}
			getContestQuesBankGridData(pageNo);
		}
	}
	
		
	//Question Grid		
	
	function getQuestionGridData(contestId, pageNo){
		$('#contestQuestionTab').addClass('active');
		$('#questionGridDiv').addClass('active');
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestQuestions",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&pageNo="+pageNo+"&headerFilter="+questionSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				questionPagingInfo = jsonData.questionPagingInfo;
				refreshContestQuestionGridValues(jsonData.questionList);					
				$('#q_contest_id').val(contestId);
				$('#question_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserQuestionPreference()'>");
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	/* $('.downClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    updateQuestionPosition(id,"DOWN");
	});
	$('.upClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    updateQuestionPosition(id,"UP");
	});
	
	function downQFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img width='22px' height='22px' style='margin-left: 20px;' class='downClickableImage' src='../resources/images/down.png' id='"+ dataContext.questionId +"'/>";		
		return button;
	}
	function upQFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img width='22px' height='22px' style='margin-left: 20px;' class='upClickableImage' src='../resources/images/up.png' id='"+ dataContext.questionId +"'/>";		
		return button;
	} 
	
	function qFormatter(row, cell, value, columnDef, dataContext){
		var button = "<input type='text' value='"+dataContext.serialNo+"' id='qNo_"+ dataContext.questionId +"'/>";		
		return button;
	}*/
	
	
	
	function updateQuestionPosition(){
		var contestId = $('#q_contest_id').val();
		if(contestId == '' || contestId == undefined){
			jAlert("Please select contest first to update question position.");
			return;
		}
		if(questionData == '' || questionData.length <= 0){
			jAlert("question records are not found, please refersh page and try again.");
			return;
		}
		var questionString = '';
		for(var i=0;i<questionData.length;i++){
			var position = questionData[i].serialNo;
			if(position=='' || position == undefined){
				jAlert("Position is not entered for some of the question, please add unique and sequencial position to all question.");
				return;
			}
			questionString += questionData[i].questionId+':'+position+',';
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateQuestionPosition",
			type : "post",
			dataType: "json",
			data : "contestId="+contestId+"&questionString="+questionString,
			success : function(response) {
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
				/* if(action=='MANUAL'){
					$('#changeQPositionModal').modal('hide');
				} */
				questionPagingInfo = jsonData.questionPagingInfo;
				refreshContestQuestionGridValues(jsonData.questionList);
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
			});
	}
	
	
	
	function questionResetFilters(){
		questionSearchString='';
		questionColumnFilters = {};
		var contstId = $('#q_contest_id').val();
		getQuestionGridData(contstId, 0);
	}
	
	var questionPagingInfo;
	var questionDataView;
	var questionGrid;
	var questionData = [];
	var questionGridPager;
	var questionSearchString='';
	var questionColumnFilters = {};
	var userQuestionColumnsStr = '<%=session.getAttribute("questiongrid")%>';

	var userQuestionColumns = [];
	var allQuestionColumns = [
			/* {
				id : "srNo",
				field : "srNo",
				name : "Sr. No",
				width : 80,
				sortable : true
			}, */{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			}, {
				id : "difficultyLevel",
				field : "difficultyLevel",
				name : "Difficulty Level",
				width : 80,
				sortable : true
			}, {
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "questionReward",
				field : "questionReward",
				name : "Question/Reward",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "serialNo",
				field : "serialNo",
				name : "Serial No",
				width : 80,
				editor:Slick.Editors.Integer,
				sortable : true
			},{
				id : "miniJackpotType",
				field : "miniJackpotType",
				name : "Jackpot Type",
				width : 80,
				sortable : true
			},{
				id : "mjpNoOfWinner",
				field : "mjpNoOfWinner",
				name : "No of Winners",
				width : 80,
				sortable : true
			},{
				id : "mjpQtyPerWinner",
				field : "mjpQtyPerWinner",
				name : "Qty per winner",
				width : 80,
				sortable : true
			},{
				id : "mjpGiftCardName",
				field : "mjpGiftCardName",
				name : "Gift Card Name",
				width : 80,
				sortable : true
			}/* ,{
				id : "UP",
				field : "UP",
				name : "UP ",
				width : 80,
				formatter:upQFormatter
			} ,{
				id : "DOWN",
				field : "DOWN",
				name : "DOWN ",
				width : 80,
				formatter:downQFormatter
			} */];

	if (userQuestionColumnsStr != 'null' && userQuestionColumnsStr != '') {
		columnOrder = userQuestionColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allQuestionColumns.length; j++) {
				if (columnWidth[0] == allQuestionColumns[j].id) {
					userQuestionColumns[i] = allQuestionColumns[j];
					userQuestionColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userQuestionColumns = allQuestionColumns;
	}
		
	var questionOptions = {
		editable : true,
		enableAddRow : false,
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		asyncEditorLoading : true,
		explicitInitialization : true
	};
	var questionGridSortcol = "questionId";
	var questionGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function questionGridComparer(a, b) {
		var x = a[questionGridSortcol], y = b[questionGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshContestQuestionGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		questionData = [];
		if (jsonData != null) {
			questionArray = jsonData;
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (questionData[i] = {});
				d["id"] = i;
				d["questionId"] = data.id;
				d["contestId"] = data.contestId;
				d["difficultyLevel"] = data.difficultyLevel;
				d["question"] = data.question;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC;
				d["serialNo"] = data.serialNo;
				d["answer"] = data.answer;
				d["questionReward"] = data.rewardPerQuestionStr;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
				
				d["miniJackpotType"] = data.miniJackpotType;
				d["mjpNoOfWinner"] = data.mjpNoOfWinner;
				d["mjpQtyPerWinner"] = data.mjpQtyPerWinner;
				d["mjpGiftCardName"] = data.mjpGiftCardName;
				d["mjpGiftCardId"] = data.mjpGiftCardId;
				d["mjpGiftCardQty"] = data.mjpGiftCardQty;
				d["mjpGiftCardAmt"] = data.mjpGiftCardAmt;
			}
		}

		questionDataView = new Slick.Data.DataView();
		questionGrid = new Slick.Grid("#question_grid", questionDataView,
				userQuestionColumns, questionOptions);
		questionGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		questionGrid.setSelectionModel(new Slick.RowSelectionModel());
		//questionGrid.registerPlugin(contestsCheckboxSelector);
		
		questionGridPager = new Slick.Controls.Pager(questionDataView,
					questionGrid, $("#question_pager"),
					questionPagingInfo);
		var questionGridColumnpicker = new Slick.Controls.ColumnPicker(
				allQuestionColumns, questionGrid, questionOptions);
		
		
		questionGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = questionGrid.getCellFromEvent(e);
		      questionGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy question'){
				var tempQuestionBankRowIndex = questionGrid.getSelectedRows([0])[0];
				if (tempQuestionBankRowIndex == null) {
					jAlert("Plese select Question to Copy", "info");
					return false;
				}else {
					var questionText = questionGrid.getDataItem(tempQuestionBankRowIndex).question;
					var a = questionGrid.getDataItem(tempQuestionBankRowIndex).optionA;
					var b = questionGrid.getDataItem(tempQuestionBankRowIndex).optionB;
					var c = questionGrid.getDataItem(tempQuestionBankRowIndex).optionC;
					var answer = questionGrid.getDataItem(tempQuestionBankRowIndex).answer;
					var copyText = questionText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}
		});
		
	
		questionGrid.onSort.subscribe(function(e, args) {
			questionGridSortdir = args.sortAsc ? 1 : -1;
			questionGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				questionDataView.fastSort(questionGridSortcol, args.sortAsc);
			} else {
				questionDataView.sort(questionGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the questionGrid
		questionDataView.onRowCountChanged.subscribe(function(e, args) {
			questionGrid.updateRowCount();
			questionGrid.render();
		});
		questionDataView.onRowsChanged.subscribe(function(e, args) {
			questionGrid.invalidateRows(args.rows);
			questionGrid.render();
		});
		$(questionGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							questionSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								questionColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in questionColumnFilters) {
										if (columnId !== undefined
												&& questionColumnFilters[columnId] !== "") {
											questionSearchString += columnId
													+ ":"
													+ questionColumnFilters[columnId]
													+ ",";
										}
									}
									var contstsId = $('#q_contest_id').val();
									getQuestionGridData(contstsId, 0);
								}
							}

						});
		questionGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(questionColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(questionColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		questionGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		questionDataView.beginUpdate();
		questionDataView.setItems(questionData);
		//questionDataView.setFilter(filter);
		questionDataView.endUpdate();
		questionDataView.syncGridSelection(questionGrid, true);
		questionGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserQuestionPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = questionGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('questiongrid', colStr);
	}
			
	//Edit Contest
	function editContest(){
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select contest to Edit", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getEditContest(contestId);
		}
	}
	
	
	
	function getEditContest(contestId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateContest",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&action=EDIT&type="+$('#type').val(),
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#preContestId').empty();
					 var selectOption = $("<option />");
					 selectOption.html("--Select--");
					 selectOption.val("-1");
	                 $('#preContestId').append(selectOption);
					 for(var i=0;i<jsonData.preContestList.length;i++) {
		                var option1 = $("<option />");
		                option1.html(jsonData.preContestList[i].contestName+' - '+jsonData.preContestList[i].contestDateTimeStr);
		                option1.val(jsonData.preContestList[i].id);
		                $('#preContestId').append(option1);
					}					
					$('#myModal-2').modal('show');
					setEditContest(jsonData.contestsList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function setEditContest(contestsEd){
		$('#saveBtn').hide();			
		$('#updateBtn').show();
		if(contestsEd != null && contestsEd.length > 0){
			for (var i = 0; i < contestsEd.length; i++){					
				var data = contestsEd[i];
				if(data.preContestId > 0){
					 $("#preContestId option[value='-1']").remove();
					$('#contestForm :input').attr('disabled',true);
				}
				$('#contest_id').val(data.id);
				$('#preContestId').val(data.preContestId);
				$('#contest_name').val(data.contestName);
				$('#extContestName').val(data.extendedName);
				$('#contest_fromDate').val(data.startDateStr);
				$('#startDateHour').val(data.hours);
				$('#startDateMinute').val(data.minutes);
				$('#maxTickets').val(data.ticketWinnerThreshhold);	
				//$('#pointsPerWinner').val(data.pointsPerWinner);
				$('#rewardPoints').val(data.rewardPoints);
				$('#contestMode').val(data.contestMode);
				$('#gamePassword').val(data.contestPassword);
				changeContestType();
				$('#contestCategory').val(data.contestCategory);
				$('#questionSize').val(data.questionSize);
				$('#participantStar').val(data.participantStars);
				$('#referralStar').val(data.referralStars);
				$('#referralReward').val(data.referralRewards);
				$('#participantRewards').val(data.participantRewards);
				$('#referralPoints').val(data.referralPoints);
				$('#participantPoints').val(data.participantPoints);
				$('#participantLives').val(data.participantLives);
				$('#ticketsPerWinner').val(data.freeTicketPerWinner);
				
				if(data.promoRefType != null && data.promoRefType == "ARTIST"){
					$('#artistId').val(data.promoRefId);
					$('#eventId').val("");
					$('#parentId').val("");
					$('#childId').val("");
					$('#grandChildId').val("");
				}
				if(data.promoRefType != null && data.promoRefType == "EVENT"){
					$('#eventId').val(data.promoRefId);
					$('#artistId').val("");					
					$('#parentId').val("");
					$('#childId').val("");
					$('#grandChildId').val("");
				}
				if(data.promoRefType != null && data.promoRefType == "PARENT"){
					$('#parentId').val(data.promoRefId);
					$('#artistId').val("");
					$('#eventId').val("");
					$('#childId').val("");
					$('#grandChildId').val("");
				}
				if(data.promoRefType != null && data.promoRefType == "CHILD"){
					$('#childId').val(data.promoRefId);					
					$('#artistId').val("");
					$('#eventId').val("");
					$('#parentId').val("");
					$('#grandChildId').val("");
				}
				if(data.promoRefType != null && data.promoRefType == "GRAND"){
					$('#grandChildId').val(data.promoRefId);
					$('#artistId').val("");
					$('#eventId').val("");
					$('#parentId').val("");
					$('#childId').val("");
				}
				$('#artistEventCategoryName').val(data.promoRefName);
				$('#artistEventCategoryType').val(data.promoRefType);
				$('#selectedItem').text(data.promoRefName);
				$('#resetLink').show();
				$('#promotionalCode').val(data.promotionalCode);
				$('#discountPercentage').val(data.discountPercentage);
				if(data.zone != null && data.zone != undefined){
					$('#zone').val(data.zone);
				}
				if(data.singleTicketPrice != null && data.singleTicketPrice != undefined){
					$('#tixPrice').val(data.singleTicketPrice);
				}
				$('#promoOfferId').val(data.rtfPromoOfferId);
				
				$('#giftCardId').val(data.giftCardId);
				$('#contestJackpotType').val(data.contestJackpotType);
				
				if(data.giftCardId != null ) {
					$('#giftCardQty').val(data.giftCardQty);
					$('#giftCardAmt').val(data.giftCardAmt);
					$('#selectedCoGiftCardItem').text(data.giftCardName+" | "+ data.giftCardAmt +" | "+ data.giftCardQty);
					$('#giftCardPerWinner').val(data.giftCardPerWinner);
					
					$('#resetCoGiftCardLink').show();
					$('#giftCardPerWinnerDiv').show();
				} else {
					
					resetCoGiftCardItem();
				}
			}
		}
	}
	
	//Edit Question
	function editQuestion(){
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (tempQuestionRowIndex == null) {
			jAlert("Plese select Question to Edit", "info");
			return false;
		}else {
			var questionId = questionGrid.getDataItem(tempQuestionRowIndex).questionId;
			getEditQuestion(questionId);
		}
	}
		
	function getEditQuestion(questionId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateQuestion",
			type : "post",
			dataType: "json",
			data: "questionId="+questionId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					//$('#contestCategoryDiv').empty();					
					$('#questionModal').modal('show');
					setEditQuestion(jsonData.questionList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditQuestion(question){
		$('#qSaveBtn').hide();			
		$('#qUpdateBtn').show();
		if(question != null && question.length > 0){
			for (var i = 0; i < question.length; i++){					
				var data = question[i];
				$('#questionId').val(data.id);
				$('#q_contest_id').val(data.contestId);
				//$('#q_contest_jackpot_type').val(data.contestJackpotType);
				$('#questionText').val(data.question);
				$('#optionA').val(data.optionA);
				$('#optionB').val(data.optionB);
				$('#optionC').val(data.optionC);
				$('#difficultyLevel').val(data.difficultyLevel);	
				$('#answer').val(data.answer);
				$('#questionReward').val(data.rewardPerQuestionStr);
				//$('#questionNo').val(data.serialNo);
			
				$('#miniJackpotType').val(data.miniJackpotType);
				$('#mjpNoOfWinner').val(data.mjpNoOfWinner);
				$('#mjpQtyPerWinner').val(data.mjpQtyPerWinner);
				$('#mjpGiftCardId').val(data.mjpGiftCardId);
				$('#mjpGiftCardQty').val(data.mjpGiftCardQty);
				$('#mjpGiftCardAmt').val(data.mjpGiftCardAmt);
				$('#selectedGiftCardItem').text(data.mjpGiftCardName+" | "+data.mjpGiftCardAmt+" | "+data.mjpGiftCardQty);
				
				callGiftCardTypeChange(data.miniJackpotType);
			}
		}
	}
	
	//Delete Contest
	function deleteContest(){		
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select contest to Delete", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			var promoOfferId = contestsGrid.getDataItem(tempContestsRowIndex).promoOfferId;
			getDeleteContest(contestId, promoOfferId);
		}
	}
	
	function getDeleteContest(contestId, promoOfferId){
		if (contestId == '') {
			jAlert("Please select a Contest to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete a Contest ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=DELETE&status=${status}&type="+$('#type').val()+"&promoOfferId="+promoOfferId,
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.contestsPagingInfo;
								columnFilters = {};
								refreshContestsGridValues(jsonData.contestsList);
								refreshContestQuestionGridValues([]);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function deleteQuestion(){		
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (tempQuestionRowIndex == null) {
			jAlert("Plese select Question to Delete", "info");
			return false;
		}else {
			var questionId = questionGrid.getDataItem(tempQuestionRowIndex).questionId;
			var contestId = questionGrid.getDataItem(tempQuestionRowIndex).contestId;
			getDeleteQuestion(questionId,contestId);
		}
	}
	
	function getDeleteQuestion(questionId,contestId){
		if (questionId == '') {
			jAlert("Please select a Question to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete selected Question ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateQuestion",
						type : "post",
						dataType: "json",
						data : "questionId="+questionId+"&qContestId="+contestId+"&action=DELETE",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								questionPagingInfo = jsonData.questionPagingInfo;
								questionColumnFilters = {};
								refreshContestQuestionGridValues(jsonData.questionList);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function copyContestToSandbox(){
		var tempIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempIndex == undefined || tempIndex < 0) {
			jAlert("Plese select Contest to copy.", "info");
			return false;
		}
		var contestId = contestsGrid.getDataItem(tempIndex).contestId;
		$.ajax({
			url : "${pageContext.request.contextPath}/CopyContestSetup",
			type : "post",
			dataType: "json",
			data : "contestId="+contestId,
			success : function(response) {
				var jsonData = JSON.parse(JSON.stringify(response));
				jAlert(jsonData.msg);
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	function resetContest(action){
		var tempIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempIndex == undefined || tempIndex < 0) {
			jAlert("Plese select Contest.", "info");
			return false;
		}
		var confirmText;
		if(action == 'RESET'){
			confirmText = "Are you sure to reset Contest data ?";
		}else if(action == 'END'){
			confirmText = "Are you sure to end contest ?";
		}else if(action == 'ACTIVE'){
			confirmText = "Are you sure to move contest to active?";
		}else if(action == 'REFRESH'){
			confirmText = "Are you sure to refresh API cache data for selected contest.";
			var status = contestsGrid.getDataItem(tempIndex).status;
			if(status != 'STARTED'){
				jAlert("You can refresh only started contest API cache data.");
				return false;
			}
		}
		
		var contestId = contestsGrid.getDataItem(tempIndex).contestId;
		jConfirm(confirmText,"Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ResetContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action="+action,
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							getContestsGridData(0);
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function createQuestionRewardUI(){
		var inputStr = '';
		var index = contestsGrid.getSelectedRows([0])[0];
		if (index == undefined) {
			jAlert('Please select contest first to update question rewards.');
			return;
		}
		var contestId = contestsGrid.getDataItem(index).contestId;
		
		$('#questionRewardDiv').empty();
		for(var i=0;i<questionArray.length;i++){
			var d = questionArray[i];
			inputStr += '<div class="form-group col-sm-3 col-xs-3">'+
			'<label>Reward Dollars for Question-'+(i+1)+'<span class="required">*</span></label>'+ 
			'<input class="form-control" type="text" id="questionReward_'+i+'" value="'+d.rewardPerQuestionStr+'" name="questionReward_'+i+'"></div>';
		}
		$('#questionRewardModal').modal('show');
		$('#questionRewardDiv').append(inputStr);
		
	}
	
	function updateQuestionReward(){
		var index = contestsGrid.getSelectedRows([0])[0];
		if (index == undefined) {
			jAlert('Please select contest first to update question rewards.');
			return;
		}
		var contestId = contestsGrid.getDataItem(index).contestId;
		$.ajax({
				url : "${pageContext.request.contextPath}/UpdateQuestionRewards",
				type : "post",
				dataType: "json",
				data :$('#questionRewardForm').serialize()+"&contestId="+contestId,
				success : function(response) {
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						questionPagingInfo = jsonData.questionPagingInfo;
						questionColumnFilters = {};
						refreshContestQuestionGridValues(jsonData.questionList);
						$('#questionRewardModal').modal('hide');
					}
					jAlert(jsonData.msg);
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
				});
			
	}
	
	//Add New Question(s) from Question Bank Grid
	function getContestQuesBankGrid(){
		var tempContestGridRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestGridRowIndex == null) {
			jAlert("Plese select contest to Add Question(s)", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestGridRowIndex).contestId;
			var contestName = contestsGrid.getDataItem(tempContestGridRowIndex).contestName;
			$('#contestName_Hdr_QuesBank').text(contestName);
			$('#contestId_QuesBank').val(contestId);
			$('#add-question-bank').modal('show');
			contestQuesBankColumnFilters = {};
			getContestQuesBankGridData(0);
		}		
	}
	
	function getContestQuesBankGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestQuestionBank.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+contestQuesBankSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				contestQuesBankPagingInfo = jsonData.contestQuesBankPagingInfo;
				refreshContestQuesBankGridValues(jsonData.contestQuesBankList);clearAllSelections();
				$('#contestQuesBank_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestQuesBankPreference()'>");
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function contestQuesBankResetFilters(){
		contestQuesBankSearchString='';
		contestQuesBankColumnFilters = {};
		var contestId = $('#contestId_QuesBank').val();
		getContestQuesBankGridData(0, contestId);
	}
	
	var contestQuesBankCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var contestQuesBankPagingInfo;
	var contestQuesBankDataView;
	var contestQuesBankGrid;
	var contestQuesBankData = [];
	var contestQuesBankGridPager;
	var contestQuesBankSearchString='';
	var contestQuesBankColumnFilters = {};
	var userContestQuesBankColumnsStr = '<%=session.getAttribute("contestquesbankgrid")%>';

	var userContestQuesBankColumns = [];
	var allContestQuesBankColumns = [contestQuesBankCheckboxSelector.getColumnDefinition(),
			/* {
				id : "questionBankId",
				field : "questionBankId",
				name : "Ques. No",
				width : 80,
				sortable : true
			}, */{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			},{
				id : "difficultyLevel",
				field : "difficultyLevel",
				name : "Difficulty Level",
				width : 80,
				sortable : true
			},{
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "questionReward",
				field : "questionReward",
				name : "Question/Reward",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "category",
				field : "category",
				name : "Category",
				width : 80,
				sortable : true
			}];

	if (userContestQuesBankColumnsStr != 'null' && userContestQuesBankColumnsStr != '') {
		columnOrder = userContestQuesBankColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestQuesBankColumns.length; j++) {
				if (columnWidth[0] == allContestQuesBankColumns[j].id) {
					userContestQuesBankColumns[i] = allContestQuesBankColumns[j];
					userContestQuesBankColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestQuesBankColumns = allContestQuesBankColumns;
	}
	
	var contestQuesBankOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var contestQuesBankGridSortcol = "questionBankId";
	var contestQuesBankGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestQuesBankGridComparer(a, b) {
		var x = a[contestQuesBankGridSortcol], y = b[contestQuesBankGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshContestQuesBankGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestQuesBankData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestQuesBankData[i] = {});
				d["id"] = i;
				d["questionBankId"] = data.id;
				d["question"] = data.question;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC;
				d["difficultyLevel"] = data.dificultyLevel;
				d["answer"] = data.answer;
				d["questionReward"] = data.rewardPerQuestionStr;
				d["status"] = data.status;
				d["category"] = data.category;
			}
		}

		contestQuesBankDataView = new Slick.Data.DataView();
		contestQuesBankGrid = new Slick.Grid("#contestQuesBank_grid", contestQuesBankDataView,
				userContestQuesBankColumns, contestQuesBankOptions);
		contestQuesBankGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestQuesBankGrid.setSelectionModel(new Slick.RowSelectionModel());
		contestQuesBankGrid.registerPlugin(contestQuesBankCheckboxSelector);
		
		contestQuesBankGridPager = new Slick.Controls.Pager(contestQuesBankDataView,
					contestQuesBankGrid, $("#contestQuesBank_pager"),
					contestQuesBankPagingInfo);
		var contestQuesBankGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestQuesBankColumns, contestQuesBankGrid, contestQuesBankOptions);
		
		contestQuesBankGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = contestQuesBankGrid.getCellFromEvent(e);
		      contestQuesBankGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu1").height()){
					height = e.pageY - $("#contextMenu1").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu1").width()){
					width =  e.pageX- $("#contextMenu1").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu1")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu1").hide();
		      });
		    });
		
		$("#contextMenu1").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy question'){
				var tempQuestionBankRowIndex = contestQuesBankGrid.getSelectedRows([0])[0];
				if (tempQuestionBankRowIndex == null) {
					jAlert("Plese select Question to Copy", "info");
					return false;
				}else {
					var questionText = contestQuesBankGrid.getDataItem(tempQuestionBankRowIndex).question;
					var a = contestQuesBankGrid.getDataItem(tempQuestionBankRowIndex).optionA;
					var b = contestQuesBankGrid.getDataItem(tempQuestionBankRowIndex).optionB;
					var c = contestQuesBankGrid.getDataItem(tempQuestionBankRowIndex).optionC;
					var answer = contestQuesBankGrid.getDataItem(tempQuestionBankRowIndex).answer;
					var copyText = questionText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}
		});
			
		contestQuesBankGrid.onSort.subscribe(function(e, args) {
			contestQuesBankGridSortdir = args.sortAsc ? 1 : -1;
			contestQuesBankGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestQuesBankDataView.fastSort(contestQuesBankGridSortcol, args.sortAsc);
			} else {
				contestQuesBankDataView.sort(contestQuesBankGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestQuesBankGrid
		contestQuesBankDataView.onRowCountChanged.subscribe(function(e, args) {
			contestQuesBankGrid.updateRowCount();
			contestQuesBankGrid.render();
		});
		contestQuesBankDataView.onRowsChanged.subscribe(function(e, args) {
			contestQuesBankGrid.invalidateRows(args.rows);
			contestQuesBankGrid.render();
		});
		$(contestQuesBankGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestQuesBankSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								contestQuesBankColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in contestQuesBankColumnFilters) {
										if (columnId !== undefined
												&& contestQuesBankColumnFilters[columnId] !== "") {
											contestQuesBankSearchString += columnId
													+ ":"
													+ contestQuesBankColumnFilters[columnId]
													+ ",";
										}
									}
									getContestQuesBankGridData(0);
								}
							}

						});
		contestQuesBankGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {				
				$("<input type='text'>").data("columnId", args.column.id)
						.val(contestQuesBankColumnFilters[args.column.id]).appendTo(
								args.node);				
			}
		});
		contestQuesBankGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		contestQuesBankDataView.beginUpdate();
		contestQuesBankDataView.setItems(contestQuesBankData);
		//contestQuesBankDataView.setFilter(filter);
		contestQuesBankDataView.endUpdate();
		contestQuesBankDataView.syncGridSelection(contestQuesBankGrid, true);
		//contestQuesBankGrid.resizeCanvas();
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
	}
	
	
	
	
	
	function contestWinnerResetFilters(type){
		if(type='SUMMARY'){
			contestWinnerSearchString='';
			contestWinnerColumnFilters = {};
		}else{
			contestGrandWinnerSearchString='';
			contestGrandWinnerColumnFilters = {};
		}
		
		viewWinners(type);
	}
	
	var contestWinnerPagingInfo;
	var contestWinnerDataView;
	var contestWinnerGrid;
	var contestWinnerData = [];
	var contestWinnerGridPager;
	var contestWinnerSearchString='';
	var contestWinnerColumnFilters = {};
	var userContestWinnerColumnsStr = '<%=session.getAttribute("contestWinnergrid")%>';

	var userContestWinnerColumns = [];
	var allContestWinnerColumns = [
			 {
				id : "custName",
				field : "custName",
				name : "First Name",
				width : 80,
				sortable : true
			}, {
				id : "lastName",
				field : "lastName",
				name : "Last Name",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Email",
				width : 80,
				sortable : true
			}, {
				id : "phone",
				field : "phone",
				name : "Phone",
				width : 80,
				sortable : true
			},{
				id : "rewardPoints",
				field : "rewardPoints",
				name : "Reward Points",
				width : 80,
				sortable : true
			}/* , {
				id : "rewardRank",
				field : "rewardRank",
				name : "Reward Rank",
				width : 80,
				sortable : true
			} */];

	if (userContestWinnerColumnsStr != 'null' && userContestWinnerColumnsStr != '') {
		columnOrder = userContestWinnerColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestWinnerColumns.length; j++) {
				if (columnWidth[0] == allContestWinnerColumns[j].id) {
					userContestWinnerColumns[i] = allContestWinnerColumns[j];
					userContestWinnerColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestWinnerColumns = allContestWinnerColumns;
	}
	
	var contestWinnerOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var contestWinnerGridSortcol = "id";
	var contestWinnerGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestWinnerGridComparer(a, b) {
		var x = a[contestWinnerGridSortcol], y = b[contestWinnerGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshContestWinnerGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestWinnerData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestWinnerData[i] = {});
				d["id"] = i;
				d["winnerId"] = data.id;
				d["custName"] = data.custName;
				d["lastName"] = data.lastName;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["rewardPoints"] = data.rewardPoints;
				//d["optionD"] = data.optionD;
				//d["rewardRank"] = data.rewardRank;
				//d["rewardTickets"] = data.rewardTickets;
				
			}
		}

		contestWinnerDataView = new Slick.Data.DataView();
		contestWinnerGrid = new Slick.Grid("#contestWinner_grid", contestWinnerDataView,
				userContestWinnerColumns, contestWinnerOptions);
		contestWinnerGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestWinnerGrid.setSelectionModel(new Slick.RowSelectionModel());
		
		contestWinnerGridPager = new Slick.Controls.Pager(contestWinnerDataView,
					contestWinnerGrid, $("#contestWinner_pager"),
					contestWinnerPagingInfo);
		var contestWinnerGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestWinnerColumns, contestWinnerGrid, contestWinnerOptions);
			
		contestWinnerGrid.onSort.subscribe(function(e, args) {
			contestWinnerGridSortdir = args.sortAsc ? 1 : -1;
			contestWinnerGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestWinnerDataView.fastSort(contestWinnerGridSortcol, args.sortAsc);
			} else {
				contestWinnerDataView.sort(contestWinnerGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestWinnerGrid
		contestWinnerDataView.onRowCountChanged.subscribe(function(e, args) {
			contestWinnerGrid.updateRowCount();
			contestWinnerGrid.render();
		});
		contestWinnerDataView.onRowsChanged.subscribe(function(e, args) {
			contestWinnerGrid.invalidateRows(args.rows);
			contestWinnerGrid.render();
		});
		$(contestWinnerGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestWinnerSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								contestWinnerColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in contestWinnerColumnFilters) {
										if (columnId !== undefined
												&& contestWinnerColumnFilters[columnId] !== "") {
											contestWinnerSearchString += columnId
													+ ":"
													+ contestWinnerColumnFilters[columnId]
													+ ",";
										}
									}
									viewWinners('SUMMARY');
								}
							}

						});
		contestWinnerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {				
				$("<input type='text'>").data("columnId", args.column.id)
						.val(contestWinnerColumnFilters[args.column.id]).appendTo(
								args.node);				
			}
		});
		contestWinnerGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		contestWinnerDataView.beginUpdate();
		contestWinnerDataView.setItems(contestWinnerData);
		//contestWinnerDataView.setFilter(filter);
		contestWinnerDataView.endUpdate();
		contestWinnerDataView.syncGridSelection(contestWinnerGrid, true);
		//contestWinnerGrid.resizeCanvas();
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
	}
	
	
	function viewWinners(type){
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		var contestId = 0;
		if(tempContestsRowIndex >= 0) {
			contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
		}
		if(contestId <=0 ){
			jAlert("Please select contest to view winners.");
			return;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/ViewContestWinners",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&type="+type+"&headerFilter="+contestWinnerSearchString+"&headerFilter1="+contestGrandWinnerSearchString,
			success : function(response){
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
				if(type=='SUMMARY'){
					contestWinnerPagingInfo = jsonData.summaryWinnerPagingInfo;
					refreshContestWinnerGridValues(jsonData.summaryWinners);
				}else if(type=='GRAND'){
					contestGrandWinnerPagingInfo = jsonData.grandWinnerPagingInfo;
					refreshContestGrandWinnerGridValues(jsonData.grandWinners);
				}else{
					contestWinnerPagingInfo = jsonData.summaryWinnerPagingInfo;
					refreshContestWinnerGridValues(jsonData.summaryWinners);
					
					contestGrandWinnerPagingInfo = jsonData.grandWinnerPagingInfo;
					refreshContestGrandWinnerGridValues(jsonData.grandWinners);
				}
				$('#view-contest-winners').modal('show');			
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	
	var contestGrandWinnerPagingInfo;
	var contestGrandWinnerDataView;
	var contestGrandWinnerGrid;
	var contestGrandWinnerData = [];
	var contestGrandWinnerGridPager;
	var contestGrandWinnerSearchString='';
	var contestGrandWinnerColumnFilters = {};
	var userContestGrandWinnerColumnsStr = '<%=session.getAttribute("contestGrandWinnergrid")%>';

	var userContestGrandWinnerColumns = [];
	var allContestGrandWinnerColumns = [
			 {
				id : "custName",
				field : "custName",
				name : "First Name",
				width : 80,
				sortable : true
			}, {
				id : "lastName",
				field : "lastName",
				name : "Last Name",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Phone",
				width : 80,
				sortable : true
			}, {
				id : "rewardTickets",
				field : "rewardTickets",
				name : "Reward Tickets",
				width : 80,
				sortable : true
			}, {
				id : "jackpotType",
				field : "jackpotType",
				name : "Jackpot Type",
				width : 80,
				sortable : true
			}, {
				id : "winnerType",
				field : "winnerType",
				name : "Winner Type",
				width : 80,
				sortable : true
			}, {
				id : "notes",
				field : "notes",
				name : "Question No",
				width : 80,
				sortable : true
			}, {
				id : "orderId",
				field : "orderId",
				name : "Order Id",
				width : 80,
				sortable : true
			}, {
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			}, {
				id : "expiryDateStr",
				field : "expiryDateStr",
				name : "Expiry Date",
				width : 80,
				sortable : true
			}, {
				id : "createdDateStr",
				field : "createdDateStr",
				name : "Created Date",
				width : 80,
				sortable : true
			}];

	if (userContestGrandWinnerColumnsStr != 'null' && userContestGrandWinnerColumnsStr != '') {
		columnOrder = userContestGrandWinnerColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestGrandWinnerColumns.length; j++) {
				if (columnWidth[0] == allContestGrandWinnerColumns[j].id) {
					userContestGrandWinnerColumns[i] = allContestGrandWinnerColumns[j];
					userContestGrandWinnerColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestGrandWinnerColumns = allContestGrandWinnerColumns;
	}
	
	var contestGrandWinnerOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var contestGrandWinnerGridSortcol = "id";
	var contestGrandWinnerGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestGrandWinnerGridComparer(a, b) {
		var x = a[contestGrandWinnerGridSortcol], y = b[contestGrandWinnerGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshContestGrandWinnerGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestGrandWinnerData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestGrandWinnerData[i] = {});
				d["id"] = i;
				d["winnerId"] = data.id;
				d["custName"] = data.custName;
				d["lastName"] = data.lastName;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["jackpotType"] = data.jackpotType;
				d["rewardTickets"] = data.rewardTickets;
				d["orderId"] = data.orderId;
				d["status"] = data.status;
				d["winnerType"] = data.winnerType;
				d["notes"] = data.notes;
				d["expiryDateStr"] = data.expiryDateStr;
				d["createdDateStr"] = data.createdDateStr;
				
			}
		}

		contestGrandWinnerDataView = new Slick.Data.DataView();
		contestGrandWinnerGrid = new Slick.Grid("#contestGrandWinner_grid", contestGrandWinnerDataView,
				userContestGrandWinnerColumns, contestGrandWinnerOptions);
		contestGrandWinnerGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestGrandWinnerGrid.setSelectionModel(new Slick.RowSelectionModel());
		
		contestGrandWinnerGridPager = new Slick.Controls.Pager(contestGrandWinnerDataView,
					contestGrandWinnerGrid, $("#contestGrandWinner_pager"),
					contestGrandWinnerPagingInfo);
		var contestGrandWinnerGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestGrandWinnerColumns, contestGrandWinnerGrid, contestGrandWinnerOptions);
			
		contestGrandWinnerGrid.onSort.subscribe(function(e, args) {
			contestGrandWinnerGridSortdir = args.sortAsc ? 1 : -1;
			contestGrandWinnerGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestGrandWinnerDataView.fastSort(contestGrandWinnerGridSortcol, args.sortAsc);
			} else {
				contestGrandWinnerDataView.sort(contestGrandWinnerGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestGrandWinnerGrid
		contestGrandWinnerDataView.onRowCountChanged.subscribe(function(e, args) {
			contestGrandWinnerGrid.updateRowCount();
			contestGrandWinnerGrid.render();
		});
		contestGrandWinnerDataView.onRowsChanged.subscribe(function(e, args) {
			contestGrandWinnerGrid.invalidateRows(args.rows);
			contestGrandWinnerGrid.render();
		});
		$(contestGrandWinnerGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestGrandWinnerSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								contestGrandWinnerColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in contestGrandWinnerColumnFilters) {
										if (columnId !== undefined
												&& contestGrandWinnerColumnFilters[columnId] !== "") {
											contestGrandWinnerSearchString += columnId
													+ ":"
													+ contestGrandWinnerColumnFilters[columnId]
													+ ",";
										}
									}
									viewGrandWinners('SUMMARY');
								}
							}

						});
		contestGrandWinnerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {				
				$("<input type='text'>").data("columnId", args.column.id)
						.val(contestGrandWinnerColumnFilters[args.column.id]).appendTo(
								args.node);				
			}
		});
		contestGrandWinnerGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		contestGrandWinnerDataView.beginUpdate();
		contestGrandWinnerDataView.setItems(contestGrandWinnerData);
		//contestGrandWinnerDataView.setFilter(filter);
		contestGrandWinnerDataView.endUpdate();
		contestGrandWinnerDataView.syncGridSelection(contestGrandWinnerGrid, true);
		//contestGrandWinnerGrid.resizeCanvas();
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
	}
	
	
	function saveUserContestQuesBankPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestQuesBankGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('contestquesbankgrid', colStr);
	}
	
	function getSelectedContestQuesBankGridId() {
		var tempContestQuesBankRowIndex = contestQuesBankGrid.getSelectedRows();
		
		var quesBankIdStr='';
		$.each(tempContestQuesBankRowIndex, function (index, value) {
			quesBankIdStr += ','+contestQuesBankGrid.getDataItem(value).questionBankId;
		});
		
		if(quesBankIdStr != null && quesBankIdStr!='') {
			quesBankIdStr = quesBankIdStr.substring(1, quesBankIdStr.length);
			 return quesBankIdStr;
		}
	}
	
	function addQuestionsFromQuesBank(){
		var contestId = $('#contestId_QuesBank').val();
		var ids = getSelectedContestQuesBankGridId();
		if(ids==null || ids==''){
			jAlert("Please select atleast one Question(s) to add Contest Question");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateQuestion",
				type : "post",
				dataType : "json",
				data : "qContestId="+contestId+"&questionBankIds="+ids+"&action=ADDNEW",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#add-question-bank').modal('hide');
						questionPagingInfo = jsonData.questionPagingInfo;
						questionColumnFilters = {};
						refreshContestQuestionGridValues(jsonData.questionList);
						clearAllSelections();
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${contestsPagingInfo}));
		refreshContestsGridValues(JSON.parse(JSON.stringify(${contestsList})));
		$('#contests_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestsPreference()'>");
		$('#question_pager> div')
		.append(
				"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserQuestionPreference()'>");
		
		enableMenu();
	};
		
</script>