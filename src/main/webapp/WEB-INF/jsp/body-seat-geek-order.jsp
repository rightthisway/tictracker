<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

 <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
	<script src="../resources/js/app/invoiceRelatedPO.js"></script>
	
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
  background:#FFFFFF;
  color:#000000;
  border: 1px solid gray;
  padding: 2px;
  display: inline-block;
  min-width: 200px;
  
  -moz-box-shadow: 2px 2px 2px silver;
  -webkit-box-shadow: 2px 2px 2px silver;
  z-index: 99999;
}
#contextMenu li {
  padding: 4px 4px 4px 14px;
  list-style: none;
  cursor: pointer;
}
#contextMenu li:hover {
	color:#FFFFFF;
  background-color:#4d94ff;
}

input{
		color : black !important;
	}
</style>

<script>
var jq2 = $.noConflict(true);
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
	 $('#fromDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "bottom",
			todayHighlight: true
	    });
		
		$('#toDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "bottom",
			todayHighlight: true
	    });
				
		$("#sgNewOrders").click(function(){
			callTabOnChange('NewOrders');
		});
		$("#sgConfirmed").click(function(){
			callTabOnChange('Confirmed');
		});
		$("#sgFullfilled").click(function(){
			callTabOnChange('FullFilled');
		});
		$("#sgRejected").click(function(){
			callTabOnChange('Rejected');
		});
		$("#sgAll").click(function(){
			callTabOnChange('All');
		});
		
		<c:choose>
			<c:when test="${selectedTab == 'NewOrders'}">
				$('#newOrders').addClass('active');
				$('#newOrdersTab').addClass('active');
			</c:when>
			<c:when test="${selectedTab == 'Confirmed'}">
				$('#confirmed').addClass('active');
				$('#confirmedTab').addClass('active');
			</c:when>
			<c:when test="${selectedTab == 'Rejected'}">
				$('#rejected').addClass('active');
				$('#rejectedTab').addClass('active');
			</c:when>
			<c:when test="${selectedTab == 'FullFilled'}">	
				$('#fullfilled').addClass('active');
				$('#fullfilledTab').addClass('active');
			</c:when>
			<c:when test="${selectedTab == 'All'}">	
				$('#all').addClass('active');
				$('#allTab').addClass('active');
			</c:when>
			<c:otherwise>
				$('#newOrders').addClass('active');
				$('#newOrdersTab').addClass('active');
			</c:otherwise>
		</c:choose>

		$('#menuContainer').click(function(){
		  if($('.ABCD').length >0){
			   $('#menuContainer').removeClass('ABCD');
		  }else{
			   $('#menuContainer').addClass('ABCD');
		  }
		  openOrderGrid.resizeCanvas();
		});
	
		$('.searchcontrol').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
			    $('#searchSeatGeekBtn').click();
			    return false;  
			  }
		});
		
});

	function callTabOnChange(selectedTab) {
		$("#action").val('search');
		
		var data = "?selectedTab="+selectedTab;
		var frmDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		//var orderNumber = $('#orderNo').val();
		
		if(frmDate == null || frmDate == ""){}
		else{
			data += "&fromDate="+frmDate; 
		}
		if(toDate == null || toDate == ""){}
		else{
			data += "&toDate="+toDate;
		}
		/*if(orderNumber == null || orderNumber == ""){}
		else{
			data += "&orderNo="+orderNumber;
		}*/
		
		window.location = "${pageContext.request.contextPath}/SeatGeek/SgOpenOrders"+data;
	}

	function saveUserOpenOrderPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = openOrderGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('seatgeekordergrid',colStr);
	}

	function trackMyOrder(trackingNo){
		var url = "https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber="+trackingNo;
		popupCenter(url,"Fedex Tracking","1100","900");
	}
	
function resetItem(){
	$('#resetLink').hide();
	$('#artistId').val("");
	$('#venueId').val("");
	$('#eventSelect').empty();
	$('#selectedItem').text("");
	$('#artistVenueName').val("");
	$('#eventSelect').append("<option value=''>--select--</option>");
}

function getEventforArtistOrVenue(type,id){
	$.ajax({
			url : "${pageContext.request.contextPath}/Deliveries/GetEventsByVenueOrArtist",
			type : "post",
			dataType:"json",
			data:"type="+type+"&id="+id,
			success : function(response){
				events = JSON.parse(JSON.stringify(response.events));
				fillEventCombo(events);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
}

function fillEventCombo(eventData){
	var template = '';
	$('#eventSelect').empty();
	$('#eventSelect').append("<option value=''>--select--</option>");
	$.each(eventData, function(i,value) {
		template += "<option value='"+value.eventId+"'>"+value.eventName+" - "+value.eventDateStr+" "+value.eventTimeStr+" "+value.venue+"</option>";
	});
	$('#eventSelect').append(template);
}

function viewAction(id,eventId){
	popupCenter("GetSoldTickets?eventId="+eventId+"&id="+id,"Tickets Details",1000,1000);
}

function updateSelectedRecords(action) {
	var temprEventRowIndex = openOrderGrid.getSelectedRows();
	var arr = [];
	var flag = true;
	$.each(temprEventRowIndex, function (index, value) {
		var id = openOrderGrid.getDataItem(value).id;
		var orderId = openOrderGrid.getDataItem(value).orderId;
		var status = openOrderGrid.getDataItem(value).status;
		if(status!='failed' && status != 'submitted') {
			flag = false;
			alert('Please select FAILED or SUBMITTED orders to update as '+action);
			return;
		}
		arr.push(id);
		arr.push(orderId+'_');
	});
	if(flag && arr.length>0){
		updateRecordAjax(arr,action);
	}
	
}
function updateRecordAjax(arr,action){
	var status = $('#status').val();
	$.ajax({
		url : "${pageContext.request.contextPath}/SeatGeek/UpdateSgOrders",
		type : "post",
		data:"updateOrderrecords="+arr+"&status="+status+"&action="+action,
		success : function(response){
			jAlert(response);
			//if(response=='Selected record updated.') {
				submitSearchForm();
			//}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function refreshPage(){
	$.ajax({
		url : "${pageContext.request.contextPath}/Deliveries/RefreshOpenOrder",
		type : "post",
		dataType:"text",
		success : function(response){
			if(response=="OK"){
				location.reload();
			}else{
				jAlert("There is something wrong. Please try again");
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function exportToExcel(){
	//var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&orderNo="+$('#orderNo').val()+"&selectedTab="+$('#selectedTab').val();
	var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&selectedTab="+$('#selectedTab').val()+"&headerFilter="+seatgeekGridSearchString;
    //var url = "${pageContext.request.contextPath}/SeatGeek/ExportToExcel?"+appendData;
    var url = apiServerUrl+"SeatGeekOrdersExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function submitSearchForm(){
	$('#action').val("search");
	//$('#sgOpenOrders').submit();
	seatgeekGridSearchString='';
	columnFilters = {};
	getSeatGeekOrderGridData(0);
}

function resetFilters(){
	$('#action').val("search");
	seatgeekGridSearchString='';
	columnFilters = {};
	getSeatGeekOrderGridData(0);
}
</script>

<div class="row">
<ul id="contextMenu" style="display:none;position:absolute">
  <li data="openInvoice">Open Invoice</li>
  <li data="view invoice po">View Related Invoice/Purchase Orders</li>
</ul>
 <iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">SeatGeek Orders</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>SeatGeek Orders</li>
		</ol>
	</div>
</div>

<div class="row">
		<div class="col-xs-12 filters-div">
			<form:form role="form" id="sgOpenOrders" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/SeatGeek/SgOpenOrders">
			
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<input type="hidden" id="action" name="action" value="" />
					<input type="hidden" id="selectedTab" name="selectedTab" value="${selectedTab}" />
					<input type="hidden" id="productType" name="productType" value="SEATGEEK" />
					<label for="fromDate" class="control-label">From Date</label> 
					<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">					
				</div>
				
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">To Date</label> 
					<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
				</div>
				<!-- 
				<div class="form-group col-xs-2 col-md-2">
					<label for="name" class="control-label">Order No</label> 
					<input class="form-control searchcontrol" placeholder="Order" type="text" id="orderNo" name="orderNo" value="${orderNo}">
				</div>
				-->
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<button type="button" id="searchSeatGeekBtn" class="btn btn-primary" onclick="submitSearchForm()" style="width:100px;margin-top:19px;">search</button>
				</div>
		</form:form>
	</div>
</div>

<div id="sgOrdersDiv" class="full-width">
	<div class="full-width">
		<section class="panel">
		<ul class="nav nav-tabs">
			<li id="newOrdersTab" class=""><a id="sgNewOrders" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#newOrders">New Orders</a></li>
			<li id="confirmedTab" class=""><a id="sgConfirmed" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#confirmed">Confirmed</a></li>
			<li id="fullfilledTab" class=""><a id="sgFullfilled" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#fullfilled">Full Filled</a>
			<li id="rejectedTab" class=""><a id="sgRejected" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#rejected">Rejected</a></li>
			<li id="allTab" class=""><a id="sgAll" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#all">All</a></li>
		</ul>
		</section>
	</div>
	<div class="full-width">
		<div class="tab-content">			
			<div id="newOrders" class="tab-pane">
				<c:if test="${empty selectedTab or selectedTab =='NewOrders'}">
				<div class="full-width mb-20 full-width-btn">
					<button type="button" onclick="updateSelectedRecords('denied');" class="btn btn-cancel">Reject</button>
					<button type="button" onclick="updateSelectedRecords('confirmed');" class="btn btn-primary">Confirm</button>
				</div>
				<br/><br />
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>SeatGeek Orders</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>				
				</c:if>
			</div>
			
			<div id="confirmed" class="tab-pane">
				<c:if test="${selectedTab =='Confirmed'}">				
				<div class="table-responsive grid-table">
					<div class="grid-header  full-width">
						<label>Open Orders</label>
						<div class="pull-right">
						<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
						<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
				</c:if>
			</div>
			<div id="fullfilled" class="tab-pane">
				<c:if test="${selectedTab =='FullFilled'}">
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Open Orders</label>
						<div class="pull-right">
						<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
						<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
				</c:if>
			</div>
			<div id="rejected" class="tab-pane">
			<c:if test="${selectedTab =='Rejected'}">
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Open Orders</label>
						<div class="pull-right">
						<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
						<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
			</c:if>
			</div>
			<div id="all" class="tab-pane">
				<c:if test="${selectedTab =='All'}">
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Open Orders</label>
						<div class="pull-right">
						<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
						<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
</div>


<!-- popup View Related Invoice/Purchase Orders -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-related-invoices-po">View Related Invoice/Purchase Orders</button> -->
	<div id="view-related-invoices-po" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">View Related Invoice/Purchase Orders - Invoice No : <span id="invoiceId_Hdr_RelatedInvoice" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Accounting
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Purchase Orders</li>
							</ol>
						</div>
					</div>
					
					<div id="relInvPO_successDiv" class="alert alert-success fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="relInvPO_successMsg"></span></strong>
					</div>					
					<div id="relInvPO_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="relInvPO_errorMsg"></span></strong>
					</div>
					<br />

					<div class="full-width mt-10" style="position: relative" id="purchaseOrder">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Purchase Order</label> <span id="openOrder_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"></span>
							</div>
							<div id="po_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="po_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					
					<div class="full-width mt-20 mb-20" style="position: relative" id="invoice">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Invoices</label> <span id="openOrder_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"></span>
							</div>
							<div id="invoice_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="invoice_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup View Related Invoice/Purchase Orders -->



<script>
var orderCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	var pagingInfo;
	var openOrderView;
	var openOrderGrid;
	var openOrderData = [];
	var seatgeekGridSearchString='';
	var columnFilters = {};
	var userOpenOrderColumnsStr = '<%=session.getAttribute("seatgeekordergrid")%>';
	var userOpenOrderColumns =[];
	var loadOpenOrderColumns = ["orderId", "orderDateTime", "eventName", "eventDate", "eventTime", "venue", "section", "row", "quantity", "totalSale", "totalPayment", "status"];
	var allOpenOrderColumns = [ orderCheckboxSelector.getColumnDefinition(),
	{
		id : "orderId",
		name : "Order Id",
		field : "orderId",
		width:80,
		sortable : true
	},{
		id : "orderDateTime",
		name : "Order Date",
		field : "orderDateTime",
		width:80,
		sortable : true
	}, {
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		width:80,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width:80,
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width:80,
		sortable : true
	}, {
		id : "venue",
		name : "venue",
		field : "venue",
		width:80,
		sortable : true
	},
	{id:"section", name:"Section", field: "section",width:80, sortable: true},
	{id:"row", name:"Row", field: "row",width:80, sortable: true},
	{id:"quantity", name:"Quantity", field: "quantity",width:80, sortable: true},
    {id:"totalSale", name:"Total Sale", field: "totalSale",width:80, sortable: true},
    {id:"totalPayment", name:"Total Payment", field: "totalPayment",width:80, sortable: true},
    {id:"status", name:"Status", field: "status",width:80,  sortable: true},
    {id:"lastUpdatedDate", name:"Last Updated", field: "lastUpdatedDate",width:80,  sortable: true},
    {id:"ticTrackerOrderId", name:"System Order", field: "ticTrackerOrderId",width:80,  sortable: true},
    {id:"ticTrackerInvoiceId", name:"System Invoice", field: "ticTrackerInvoiceId",width:80,  sortable: true}
	];

	if(userOpenOrderColumnsStr!='null' && userOpenOrderColumnsStr!=''){
		var columnOrder = userOpenOrderColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allOpenOrderColumns.length;j++){
				if(columnWidth[0] == allOpenOrderColumns[j].id){
					userOpenOrderColumns[i] =  allOpenOrderColumns[j];
					userOpenOrderColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadOpenOrderColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allOpenOrderColumns.length;j++){
				if(columnWidth == allOpenOrderColumns[j].id){
					userOpenOrderColumns[i] = allOpenOrderColumns[j];
					userOpenOrderColumns[i].width=80;
					break;
				}
			}			
		}
		//userOpenOrderColumns = allOpenOrderColumns;
	}
	
	var openOrderOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var openOrderGridSortcol = "orderId";
	var openOrderGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var openOrderGridSearchString = "";

	//function for edit functionality
	function viewLinkFormatter(row,cell,value,columnDef,dataContext){
		//the id is so that you can identify the row when the particular button is clicked
		/* var button = "<input class='edit' value='Edit' type='button' id='"+ dataContext.id +"' />"; */
		var link = "<img class='editClickableImage' style='height:17px;' src='../resources/images/viewIcon.png' onclick='viewAction(" +openOrderGrid.getDataItem(row).id+","+openOrderGrid.getDataItem(row).eventId+")'/>";
		return link;
	}
	
	function trackOrderFormatter(row,cell,value,columnDef,dataContext){
		var trackingNo = openOrderGrid.getDataItem(row).trackingNo;
		var link='';
		if(trackingNo!=null && trackingNo!=''){
			link = "<a href='javascript:trackMyOrder("+trackingNo+")'><u>"+trackingNo+"</u></a>";
		}
		return link;
	}
	function deleteRecordFromOpenOrderGrid(id) {
		openOrderView.deleteItem(id);
		openOrderGrid.invalidate();
	}
	/*
	function openOrderGridFilter(item, args) {
		var x= item["orderId"];
		if (args.openOrderGridSearchString  != ""
				&& x.indexOf(args.openOrderGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.openOrderGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function openOrderComparer(a, b) {
		var x = a[openOrderGridSortcol], y = b[openOrderGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function openOrderGridToggleFilterRow() {
		openOrderGrid.setTopPanelVisibility(!openOrderGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#openOrder_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover");
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover");
			});
	*/
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getSeatGeekOrderGridData(pageNo);
	}
	
	function getSeatGeekOrderGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/SeatGeek/SgOpenOrders.json",
			type : "post",
			dataType: "json",
			data : $("#sgOpenOrders").serialize()+"&pageNo="+pageNo+"&headerFilter="+seatgeekGridSearchString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshSeatGeekOrderGridValues(jsonData.seatGeekOrders);
				clearAllSelections();
				$('#openOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserSeatGeekPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	function refreshSeatGeekOrderGridValues(jsonData){
		openOrderData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (openOrderData[i] = {});
		
				d["id"] = data.id;
				//d["id"] = "id_" + i;
				d["orderId"] = data.orderId;
				d["orderDateTime"] = data.orderDateStr;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["venue"] = data.venueName;
				d["quantity"] = data.quantity;
				d["section"] = data.section;
				d["row"] = data.row;
				d["totalSale"] = data.totalSaleAmt;
				d["totalPayment"] = data.totalPaymentAmt;
				d["status"] = data.status;
				d["lastUpdatedDate"] = data.lastUpdatedDateStr;
				d["ticTrackerOrderId"] = data.ticTrackerOrderId;
				d["ticTrackerInvoiceId"] = data.ticTrackerInvoiceId;
			}
		}
	

		openOrderView = new Slick.Data.DataView();
		openOrderGrid = new Slick.Grid("#openOrders_grid", openOrderView, userOpenOrderColumns, openOrderOptions);
		openOrderGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		openOrderGrid.setSelectionModel(new Slick.RowSelectionModel());
		openOrderGrid.registerPlugin(orderCheckboxSelector);
		if(pagingInfo != null){
			var openOrderPager = new Slick.Controls.Pager(openOrderView, openOrderGrid, $("#openOrder_pager"),pagingInfo);
		}
		var openOrderColumnpicker = new Slick.Controls.ColumnPicker(allOpenOrderColumns, openOrderGrid,
				openOrderOptions);

		// move the filter panel defined in a hidden div into openOrderGrid top panel
		//$("#openOrder_inlineFilterPanel").appendTo(openOrderGrid.getTopPanel()).show();

		openOrderGrid.onSort.subscribe(function(e, args) {
			openOrderSortdir = args.sortAsc ? 1 : -1;
			openOrderGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				openOrderView.fastSort(openOrderGridSortcol, args.sortAsc);
			} else {
				openOrderView.sort(openOrderComparer, args.sortAsc);
			}
		});
		// wire up model openOrders to drive the openOrderGrid
		openOrderView.onRowCountChanged.subscribe(function(e, args) {
			openOrderGrid.updateRowCount();
			openOrderGrid.render();
		});
		openOrderView.onRowsChanged.subscribe(function(e, args) {
			openOrderGrid.invalidateRows(args.rows);
			openOrderGrid.render();
		});
		
		$(openOrderGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	seatgeekGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  seatgeekGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getSeatGeekOrderGridData(0);
				}
			  }
			  
		});
		openOrderGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();			
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id == 'orderDateTime' || args.column.id == 'eventDate' || args.column.id == 'lastUpdatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				}
		});
		openOrderGrid.init();
		// wire up the search textbox to apply the filter to the model
		/*$("#openOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			openOrderGridSearchString = this.value;
			updateOpenOrderGridFilter();
		});
		function updateOpenOrderGridFilter() {
			openOrderDataView.setFilterArgs({
				openOrderSearchString : openOrderSearchString
			});
			openOrderView.refresh();
		}
		*/
		// initialize the model after all the openOrders have been hooked up
		openOrderView.beginUpdate();
		openOrderView.setItems(openOrderData);
		/*openOrderView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			openOrderGridSearchString : openOrderGridSearchString
		});
		openOrderView.setFilter(openOrderGridFilter);*/
		openOrderView.endUpdate();
		openOrderView.syncGridSelection(openOrderGrid, true);
		openOrderGrid.resizeCanvas();
		
		var openOrderrowIndex;
		openOrderGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprOpenOrderRowIndex = openOrderGrid.getSelectedRows([0])[0];
			if (temprOpenOrderRowIndex != openOrderrowIndex) {
				openOrderrowIndex = temprOpenOrderRowIndex;
				//getCustomerInfoForInvoice(tempropenOrderRowIndex);
			}
		});

		openOrderGrid.onContextMenu.subscribe(function (e) {
	      e.preventDefault();
	      var cell = openOrderGrid.getCellFromEvent(e);
		  openOrderGrid.setSelectedRows([cell.row]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if(height < $("#contextMenu").height()){
				height = e.pageY - $("#contextMenu").height();
			}else{
				height = e.pageY;
			}
			if(width < $("#contextMenu").width()){
				width =  e.pageX- $("#contextMenu").width();
			}else{
				width =  e.pageX;
			}
		 $("#contextMenu")
	          .data("row", cell.row)
	          .css("top", height)
	          .css("left", width)
	          .show();
	      $("body").one("click", function () {
	        $("#contextMenu").hide();
	      });
	    }); 
		 $("div#divLoading").removeClass('show');
	}
	/*
	function eventGridComparer(a, b) {
		var x = a[openOrderGridSortcol], y = b[openOrderGridSortcol];
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	*/
	$("#contextMenu").click(function (e) {
		if (!$(e.target).is("li")) {
		  return;
		}
		var index = openOrderGrid.getSelectedRows([0])[0];
		if(index>=0){
			var invoiceId = openOrderGrid.getDataItem(index).ticTrackerInvoiceId;
			var productType = $('#productType').val();
			if ($(e.target).attr("data") == 'view invoice po') {
				if(invoiceId>0){
					getRelatedInvoicesPO(invoiceId, productType);
					/*var url = "${pageContext.request.contextPath}/Accounting/ViewRelatedPOInvoices?invoiceId="+ invoiceId+"&productType="+ productType;
					popupCenter(url, "View Related PO(s)", "1000","700");*/
				}else{
					jAlert("Invoice is not Created yet for selected Order.");
				}
			} else if($(e.target).attr("data") == 'openInvoice'){
				if(invoiceId>0){
				window.location = "${pageContext.request.contextPath}/Accounting/Invoices?action=search&invoiceNo="+invoiceId+"&productType="+productType;
				}else{
					jAlert("Invoice is not Created yet for selected Order.");
				}
			}
		}else{
			jAlert("Please select Order to view Invoice.");
		}
	});
	
	//show the pop window center
	function popupCenter(url, title, w, h) {  
	    // Fixes dual-screen position                         Most browsers      Firefox  
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
	              
	    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
	    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
	              
	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
	  
	    // Puts focus on the newWindow  
	    if (window.focus) {  
	        newWindow.focus();  
	    }  
	} 

	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshSeatGeekOrderGridValues(JSON.parse(JSON.stringify(${seatGeekOrders})));
		$('#openOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserSeatGeekPreference()'>");
	});
 
	function saveUserSeatGeekPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = openOrderGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('seatgeekordergrid',colStr);
	}
	
	//Start View Related Invoices
	function getRelatedInvoicesPO(invoiceId, productType){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetRelatedPOInvoices",
			type : "post",
			data : "invoiceId="+invoiceId+"&productType="+productType,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Related PO/Invoice(s) Found.");
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setRelatedInvoicesPO(jsonData);
					$('#invoiceId_Hdr_RelatedInvoice').text(invoiceId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setRelatedInvoicesPO(jsonData){
		$('#view-related-invoices-po').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#relInvPO_successDiv').hide();
			$('#relInvPO_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#relInvPO_errorDiv').hide();
			$('#relInvPO_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#relInvPO_successDiv').show();
			$('#relInvPO_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#relInvPO_errorDiv').show();
			$('#relInvPO_errorMsg').text(jsonData.errorMessage);
		} */
		relatedPOGridValues(jsonData.poList, jsonData.poPagingInfo);
		relatedInvoiceGridValues(jsonData.ticketList, jsonData.pagingInfo);
	}
	//End View Related Invoices
</script>