<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}
/* .light_box {
	display: none;
	position: absolute;
	top: 10%;
	left: 25%;
	width: 45%;
	height: 100%;
	padding: 20px;
	border: 1px solid #888;
	background-color: #fefefe;
	z-index: 1;
	overflow: auto;
} */

#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}

</style>
 
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Customer Order
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Customer Order</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Customer Order Summery</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage} </strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>	
</div>

<br/>
<div class="row">
<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 13px;font-family: arial;" class="panel-heading">
		<b>Order Info</b> </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Order ID :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.order.id}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Order Total :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.orderTotal}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Ticket Quantity :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.order.qty}</span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Event Name :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.order.eventName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Event Date & Time :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.order.eventDateStr}&nbsp;${affiliateHistory.order.eventTimeStr}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>

<div class="row">
<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 13px;font-family: arial;" class="panel-heading">
		<b>Customer Info</b> </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Customer Name :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.customer.customerName} &nbsp;&nbsp; ${affiliateHistory.customer.lastName}</span>
				</div>
				<div class="col-sm-6">
					<label style="font-size: 13px;font-weight:bold;">Email :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.customer.email}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>

<div class="row">
<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 13px;font-family: arial;" class="panel-heading">
		<b>Affiliate Cash</b> </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Earned Cash :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.creditedCash} &nbsp;&nbsp; ${affiliateHistory.customer.lastName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Status :&nbsp;</label>
					<span style="font-size: 15px;">${affiliateHistory.rewardStatus}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Active Date :&nbsp;</label>
					<span style="font-size: 15px;">${activeDate}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>