<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script src="../resources/amcharts/core.js" type="text/javascript"></script>
<script src="../resources/amcharts/maps.js" type="text/javascript"></script>
<script src="../resources/amcharts/charts.js" type="text/javascript"></script>

<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});
	
});

function callAPIReport(reportURL){
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function openContestReportModal(reportName){
	$('#contestReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
}

function openCustChainReportModal(){
	$('#custChainReportModal').modal('show'); 
}

function generateReport(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var reportURL = $('#contestReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function generateChainReport(){
	var userId = $('#userId').val(); 
	if(null == userId || userId == "" || userId == " "){
		$('#userIdErrId').html("Please Enter Valid User ID."); 
		return;
	}else{
		$('#userIdErrId').html(""); 
		window.location.href = "${pageContext.request.contextPath}/Reports/DownloadCustomerStatsReport?userId="+userId;
	}
}

function generateBotReport(){ 
	window.location.href = "${pageContext.request.contextPath}/Reports/DownloadBotReport";
}




				
				
function getReportChart(report){
	var chart = am4core.create("chartdiv", am4charts.XYChart);

				chart.colors.list = [
				  am4core.color("#FFC75F"),
				  am4core.color("#D65DB1"),
				  am4core.color("#FF6F91")
				];
				var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
					categoryAxis.dataFields.category = "name";
					categoryAxis.renderer.grid.template.location = 0;
					categoryAxis.renderer.minGridDistance = 30;

					var label = categoryAxis.renderer.labels.template;
					label.wrap = true;
					label.maxWidth = 120;

					var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
					
					// Create series
					var series = chart.series.push(new am4charts.ColumnSeries());
					series.dataFields.valueY = "viewCount";
					series.dataFields.categoryX = "name";
					series.name = "viewCount";
					series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
					series.columns.template.fillOpacity = .8;


				    var valueLabel = series.bullets.push(new am4charts.LabelBullet());
				    valueLabel.label.text = "{valueY}";
				    valueLabel.label.horizontalCenter = "left";
				    valueLabel.label.dx = 10;
				    valueLabel.label.hideOversized = false;
				    valueLabel.label.truncate = false;
				  
					var columnTemplate = series.columns.template;
					columnTemplate.strokeWidth = 2;
					columnTemplate.strokeOpacity = 1;

				// Add data
				chart.data =  JSON.parse(report);
				
				// Enable export
				chart.exporting.menu = new am4core.ExportMenu();
		}


//call functions once page loaded
	window.onload = function() {
		getReportChart('${report}');
		$('#pollingContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingContestPreference()'>");
		
		enableMenu();
	};		

</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-lg-12">
					
					<ol class="breadcrumb">
						
						<li><i class="fa fa-laptop"></i>Polling Video Stats</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
				
</div>

<div id="chartdiv" style="height: 500px;" class="col-md-8"></div>
<!--<div id="chartdivp" style="height: 300px;" class="col-md-7"></div>-->



