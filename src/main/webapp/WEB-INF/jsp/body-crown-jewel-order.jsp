<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
    background: #87ceeb;
    text-overflow: clip;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.slick-headerrow-column input {
     margin: 0;
     padding: 0;
     width: 100%;
     height: 100%;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

input {
	color: black !important;
}
</style>

<script>
var jq2 = $.noConflict(true);
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
	 $('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
/*$(window).load(function() {
		$('#crownJewelOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCrownJewelOrderPreference()'>");
		
	});
*/	
	$("#outStandingOrders").click(function(){
		callTabOnChange('Outstanding');
	});
	$("#approvedOrders").click(function(){
		callTabOnChange('Approved');
	});
	$("#rejectedOrders").click(function(){
		callTabOnChange('Rejected');
	});
	$("#allOrders").click(function(){
		callTabOnChange('All');
	});
	
	<c:choose>
		<c:when test="${status == 'Outstanding'}">
			$('#outstanding').addClass('active');
			$('#outstandingTab').addClass('active');
		</c:when>
		<c:when test="${status == 'Approved'}">
			$('#approved').addClass('active');
			$('#approvedTab').addClass('active');
		</c:when>
		<c:when test="${status == 'Rejected'}">	
			$('#rejected').addClass('active');
			$('#rejectedTab').addClass('active');
		</c:when>
		<c:otherwise>
			$('#all').addClass('active');
			$('#allTab').addClass('active');
		</c:otherwise>
	</c:choose>
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  crownJewelOrderGrid.resizeCanvas();
	});

	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchJewelOrderBtn').click();
		    return false;  
		  }
	});

});

function callTabOnChange(selectedTab) {
	var data = "";
	var frmDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var teamName = $('#teamName').val();
	var leagueName = $('#leagueName').val();
	var parentCategory = $('#parentCategory').val();
	var grandChildCategory = $('#grandChildCategory').val();
	
	if(selectedTab != null && selectedTab != ""){
		data += "?status="+selectedTab;
	}
	if(frmDate == null || frmDate == ""){}
	else{
		data += "&fromDate="+frmDate; 
	}
	if(toDate == null || toDate == ""){}
	else{
		data += "&toDate="+toDate;
	}
	if(parentCategory == null || parentCategory == ""){}
	else{
		data += "&parentCategory="+parentCategory;
	}
	if(grandChildCategory == null || grandChildCategory == ""){}
	else{
		data += "&grandChildCategory="+grandChildCategory;
	}
	if(teamName == null || teamName == ""){}
	else{
		data += "&teamName="+teamName;
	}
	if(leagueName == null || leagueName == ""){}
	else{
		data += "&leagueName="+leagueName;
	}
	
	window.location = "${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelOrders"+data;
}

function saveUserCrownJewelOrderPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = crownJewelOrderGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('crownjewelordergrid',colStr);
}
var approveReject='';
var selecteRecords='';
function approveRejectOrder(isApproved){
	selecteRecords = getSelectedOrderGridId();
	if(selecteRecords != null && selecteRecords!='') {
		approveReject=isApproved;
		$('#reason').val('');
		$('#myModal-2').modal('show');
	}else{
		approveReject='';
		jAlert("Select atleast one order.");
	}
}

function updateReason(){
	var reason = $('#reason').val();
	if(reason!=null && reason!='' && reason!=undefined){
		updateOrderWithReason(reason);
		$('#myModal-2').modal('hide');
	}else{
		approveReject='';
		jAlert("Reason is mendatory, please add reason");
	}
}
function updateOrderWithReason(reason) {
	$.ajax({
		url : "${pageContext.request.contextPath}/CrownJewelEvents/UpdateCrownJewelOrder",
		type : "post",
		dataType:"json",
		data : "status="+approveReject+"&reason="+reason+"&orderIds="+ selecteRecords,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			refreshCrownJewelOrderGridValues(true,jsonData.orders);
			clearAllSelections();
			jAlert(jsonData.msg);
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
		}
	});
}

function submitSearchForm(){
	if($('#outstanding').hasClass('active')){
		$('#status').val('Outstanding');
	}else if($('#approved').hasClass('active')){
		$('#status').val('Approved');
	}else if($('#rejected').hasClass('active')){
		$('#status').val('Rejected');
	}	
	//$('#searchCrownJewelOrder').submit();
	getCrownJewelOrderGridData(0);
}

function clearAllSelections(){
	var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
	for(var j=0;j<eventTable.length;j++){
		var eventCheckbox = eventTable[j].getElementsByTagName('input');
		for ( var i = 0; i < eventCheckbox.length; i++) {
			if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
			}
		}
	}
}

	
</script>

<div class="row">
	<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="openInvoice">Open Invoice</li>
  <li data="viewInvoice">View Invoice</li>
</ul>-->
 <iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Fantasy Sports Tickets
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Fantasy Sports Tickets Orders</a>
			</li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Fantasy Sports Tickets Orders</li>
		</ol>
	</div>
</div>


<div class="row">
	<div class="col-xs-12 filters-div">
		<form:form role="form" id="searchCrownJewelOrder" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelOrders">
			<input type="hidden" value="" id="status" name="status" />
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">From Date</label> 
				<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">To Date</label> 
				<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
			</div>
			<!--<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Parent Category</label> 
				<select name="parentCategory" id="parentCategory" class="form-control">
					<option value="">---All---</option>
					<c:forEach var="parentCategory" items="${parentCategories}">
						<option <c:if test="${parntCategory == parentCategory.name}"> Selected </c:if> value="${parentCategory.name}">
							<c:out value="${parentCategory.name}" />
						</option>
					</c:forEach>
				</select>
			</div>-->
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Grand Child Category</label> 
				<select name="grandChildCategory" id="grandChildCategory" class="form-control">
					<option value="">---All---</option>
					<c:forEach var="grandChildCategory" items="${grandChildCategories}">
						<option <c:if test="${grandChildCategry == grandChildCategory.name}"> Selected </c:if> value="${grandChildCategory.name}">
							<c:out value="${grandChildCategory.name}" />
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Team Name</label> 
				<input class="form-control searchcontrol" type="text" id="teamName" name="teamName" value="${teamName}">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Event Name</label> 
				<input class="form-control searchcontrol" type="text" id="leagueName" name="leagueName" value="${leagueName}">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<button type="button" id="searchJewelOrderBtn" class="btn btn-primary" onclick="submitSearchForm()" style="width: 100px; margin-top: 19px;">search</button>
			</div>
	</div>
	</form:form>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">Close</button>
				<h4 class="modal-title">Add Approve/Reject Reason</h4>
			</div>
			<div class="panel-body">
				<div class="modal-body">
					<div class="form-group">
						<div align="center">
							<label>Reason <span class="required">*</span> <textarea id="reason" name="reason" class="form-control" cols="50" rows="7"></textarea>
						</div>
					</div>
					<div align="center">
						<button class="btn btn-primary" type="button" onclick="updateReason()">Save</button>
						<button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ordersDiv">
	<div class="row">
		<section class="panel">
		<ul class="nav nav-tabs" style="margin-left: 10px;">
			<li id="outstandingTab" class=""><a id="outStandingOrders" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#outstanding">Outstanding</a></li>
			<li id="approvedTab" class=""><a id="approvedOrders" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#approved">Approved</a>
			<li id="rejectedTab" class=""><a id="rejectedOrders" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#rejected">Rejected</a></li>
			<li id="allTab" class=""><a id="allOrders" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#all">All</a></li>
		</ul>
		</section>
	</div>
	<div class="full-width">
		<div class="tab-content">
			<div id="outstanding" class="tab-pane">
				<c:if test="${status =='Outstanding'}">
					<!-- <div class="form-group col-xs-12 col-md-12">
						<button type="button" class="btn btn-primary" onclick="approveRejectOrder('REJECTED')" style="float: right; margin-left: 10px;">Reject</button>
						<button type="submit" class="btn btn-primary" onclick="approveRejectOrder('APPROVED')" style="float: right; margin-left: 10px;">Approve</button>						
					</div>-->
					<div class="full-width mb-20 full-width-btn">
						<button name="createOrder" id="createOrder" class="btn btn-primary" type="button" onclick="createOrder()">Create Order</button>
					</div>
					<br />
					<br />
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fantasy Sports Tickets Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
							</div>
						</div>
						<div id="crownJewelOrder_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="crownJewelOrder_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</c:if>
			</div>

			<div id="approved" class="tab-pane">
				<c:if test="${status =='Approved'}">
					<div class="full-width mb-20 full-width-btn">
						<button name="createOrder" id="createOrder" class="btn btn-primary" type="button" onclick="createOrder()">Create Order</button>
					</div>
					<br />
					<br />
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fantasy Sports Tickets Orders</label> 
							<div class="pull-right">
								<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
							</div>
						</div>
						<div id="crownJewelOrder_grid" style="width: 100%; height: 220px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="crownJewelOrder_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</c:if>
			</div>
			
			<div id="rejected" class="tab-pane">
				<c:if test="${status =='Rejected'}">
					<div class="full-width mb-20 full-width-btn">
						<button name="createOrder" id="createOrder" class="btn btn-primary" type="button" onclick="createOrder()">Create Order</button>
					</div>
					<br />
					<br />
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fantasy Sports Tickets Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
						</div>
						<div id="crownJewelOrder_grid" style="width: 100%; height: 220px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="crownJewelOrder_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</c:if>
			</div>
			
			<div id="all" class="tab-pane">
				<c:if test="${empty status}">
					<div class="full-width mb-20 full-width-btn">
						<button name="createOrder" id="createOrder" class="btn btn-primary" type="button" onclick="createOrder()">Create Order</button>
					</div>
					<br />
					<br />
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fantasy Sports Tickets Orders</label> 
							<div class="pull-right">
								<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
							</div>
						</div>
						<div id="crownJewelOrder_grid" style="width: 100%; height: 220px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="crownJewelOrder_pager" style="width: 100%; height: 10px;"></div>
						<!-- <br />
						<br />
						<div class="col-xs-12">
							<button type="submit" class="btn btn-primary" onclick="approveRejectOrder('APPROVED')">Approve</button>
							<button type="button" class="btn btn-primary" onclick="approveRejectOrder('REJECTED')">Reject</button>
						</div> -->
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>
<!-- 
<div id="crownJewelOrder_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
	Show records with invoices for customer including <input type="text" id="crownJewelOrderGridSearch">
</div> -->

<!-- popup View Orders -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-order">View Orders</button> -->

	<div id="view-order" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">View Orders</h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							 
							<ol class="breadcrumb">
								<li><i class="fa fa-laptop"></i>Fantasy Order</li>
							</ol>
							
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div id="vOrder_successDiv" class="alert alert-success fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="vOrder_successMsg"></span></strong>
							</div>					
							<div id="vOrder_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="vOrder_errorMsg"></span></strong>
							</div>
						</div>	
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<section class="panel"> 
							<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Order Details </b></header>
								<div class="panel-body">
								
								<div class="row">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Order Number : </label>
										<span style="font-size: 12px;" id="vOrder_id">${cjeOrder.id} </span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Created Date : </label>
										<span style="font-size: 12px;" id="vOrder_createdDate">${cjeOrder.createdDateStr}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Status : </label>
										<span style="font-size: 12px;" id="vOrder_status">${cjeOrder.status}</span>
									</div>
								
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Used Reward points : </label>
										<span style="font-size: 12px;" id="vOrder_pointsSpent">${customerLoyalty.pointsSpent}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Regular Order Id : </label>
										<span style="font-size: 12px;" id="vOrder_regOrderId">${cjeOrder.regularOrderId}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Is Package : </label>
										<%--<c:if test="${cjeOrder.isPackageSelected ne null and cjeOrder.isPackageSelected == true}"><span style="font-size: 12px;">Yes</span></c:if>
										<c:if test="${cjeOrder.isPackageSelected eq null or cjeOrder.isPackageSelected== false}"><span style="font-size: 12px;">No</span></c:if>--%><!---->
										<span style="font-size: 12px;" id="vOrder_isPackage"></span>
									</div>
								</div>
								
							</div>
							</section>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<section class="panel"> 
							<header style="font-size: 25px;font-family: arial;" class="panel-heading"><b>Customer Details </b></header>
								<div class="panel-body">
									<div class="row">
										<div class="form-group col-md-4 col-sm-6 col-xs-12">
											<label style="font-size: 13px;font-weight:bold;">Customer Name : </label>
											<span style="font-size: 12px;" id="vOrder_customerName">${customer.customerName}&nbsp;${customer.lastName}</span>
										</div>
										<div class="form-group col-md-4 col-sm-6 col-xs-12">
											<label style="font-size: 13px;font-weight:bold;">Phone : </label>
											<span style="font-size: 12px;" id="vOrder_phone">${customer.phone}</span>
										</div>
										<div class="form-group col-md-4 col-sm-6 col-xs-12">
											<label style="font-size: 13px;font-weight:bold;">Email : </label>
											<span style="font-size: 12px;" id="vOrder_email">${customer.email}</span>
										</div>
									</div>
								
									<%-- <div class="col-sm-4">
											<label style="font-size: 13px;font-weight:bold;">Product Type : </label><span style="font-size: 12px;">${customer.productType}</span>
										</div>
										<div class="col-sm-4">
											&nbsp;
										</div>
										<div class="col-sm-4">
											&nbsp;
										</div> --%>
								
								</div>
							</section>
						</div>
					</div>

					<!-- Shipping Address section -->
					<div class="row">
						<div class="col-xs-12">
							<section class="panel">
							 <header style="font-size: 25px;font-family: arial;" class="panel-heading"><b>Event Details</b>
							 </header>
								<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px; font-weight: bold;">Event Name :</label>
										<span style="font-size: 12px;" id="vOrder_eventName">${event.eventName}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px; font-weight: bold;">Event Date :</label>
										<span style="font-size: 12px;" id="vOrder_eventDateTime">${event.eventDateStr} ${event.eventTimeStr}</span>
									</div>
									
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px; font-weight: bold;">Fantasy Event Name :</label>
										<span style="font-size: 12px;" id="vOrder_leagueName">${cjeOrder.leagueName}</span>
									</div>
									
								
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px; font-weight: bold;">Team Name :</label>
										<span style="font-size: 12px;" id="vOrder_teamName">${cjeOrder.teamName}</span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<%-- <label style="font-size: 13px; font-weight: bold;">Venue Name :</label><span style="font-size: 12px;">${eventInfo.building}</span> --%>
									</div>				
								</div>
								
							</div>
							</section>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<section class="panel"> <header style="font-size: 25px;font-family: arial;" class="panel-heading">
								<b>Ticket Details</b>
							 </header>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Section :</label>
										<span style="font-size: 12px;" id="vOrder_section">${ticket.section}</span>
									</div>
									<%--<c:choose>
									<c:when test="${cjeOrder.regularOrderId != null}">--%>
									<div class="form-group col-md-4 col-sm-6 col-xs-12" id="vOrder_row_div" style="display:none">
										<label style="font-size: 13px;font-weight:bold;">Row :</label>
										<span style="font-size: 12px;" id="vOrder_row">${ticket.row}</span>
									</div>
									<%--</c:when>
									</c:choose>--%><!---->
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Quantity :</label>
										<span style="font-size: 12px;" id="vOrder_quantity">${ticket.quantity}</span>
									</div>				
								
									<%--<c:choose>
										<c:when test="${cjeOrder.regularOrderId != null}">
										<div class="form-group col-md-4 col-sm-6 col-xs-12">
											<label style="font-size: 13px;font-weight:bold;">Price :</label>					
											<span style="font-size: 12px;">${ticket.price}</span>
										</div>
										</c:when>
										<c:otherwise>--%>
										<div class="form-group col-md-4 col-sm-6 col-xs-12">
											<label style="font-size: 13px;font-weight:bold;">Price :</label>					
											<span style="font-size: 12px;" id="vOrder_price">${ticket.zonePrice}</span>
										</div>
										<%--</c:otherwise>
									</c:choose>--%><!---->
								</div>
								
							</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup View Orders -->


<script>
var orderCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	var pagingInfo;
	var crownJewelOrderView;
	var crownJewelOrderGrid;
	var crownJewelOrderData = [];
	var crownJewelOrderGridSearchString='';
	var columnFilters = {};
	var userCrownJewelOrderColumnsStr = '<%=session.getAttribute("crownjewelordergrid")%>';
	var userCrownJewelOrderColumns = [];
	var allCrownJewelOrderColumns = [
			orderCheckboxSelector.getColumnDefinition(),
               {id: "editCol", field:"editCol", name:"View Orders", width:10, formatter:editFormatter}, 
               {
				id : "id",
				name : "Fantasy Sports Tickets Order Id",
				field : "id",
				width : 80,
				sortable : true
			}, {
				id : "regularOrderId",
				name : "Order Id",
				field : "regularOrderId",
				width : 80,
				sortable : true
			}, {
				id : "leagueName",
				name : "Event Name",
				field : "leagueName",
				width : 80,
				sortable : true
			}, {
				id : "teamName",
				name : "Team Name",
				field : "teamName",
				width : 80,
				sortable : true
			}, {
				id : "venue",
				name : "Venue",
				field : "venue",
				width : 80,
				sortable : true
			}, {
				id : "city",
				name : "City",
				field : "city",
				width : 80,
				sortable : true
			}, {
				id : "state",
				name : "State",
				field : "state",
				width : 80,
				sortable : true
			}, {
				id : "country",
				name : "Country",
				field : "country",
				width : 80,
				sortable : true
			}, {
				id : "isPackageSelected",
				name : "Package Selected",
				field : "isPackageSelected",
				width : 80,
				sortable : true
			}, {
				id : "packageCost",
				name : "Package Cost",
				field : "packageCost",
				width : 80,
				sortable : true
			}, /* {
				id : "packageNote",
				name : "Package Note",
				field : "packageNote",
				width : 80,
				sortable : true
			}, */ {
				id : "eventId",
				name : "Event Id",
				field : "eventId",
				width : 80,
				sortable : true
			}, {
				id : "ticketId",
				name : "Ticket Id",
				field : "ticketId",
				width : 80,
				sortable : true
			}, {
				id : "zone",
				name : "Zone",
				field : "zone",
				width : 80,
				sortable : true
			}, {
				id : "ticketQty",
				name : "ticket Qty.",
				field : "ticketQty",
				width : 80,
				sortable : true
			}, {
				id : "ticketPrice",
				name : "Ticket Price",
				field : "ticketPrice",
				width : 80,
				sortable : true
			}, /* {
				id : "ticketPoints",
				name : "Ticket Points",
				field : "ticketPoints",
				width : 80,
				sortable : true
			}, */ {
				id : "requirePoints",
				name : "Require Points",
				field : "requirePoints",
				width : 80,
				sortable : true
			}, /* {
				id : "packagePoints",
				name : "Package Points",
				field : "packagePoints",
				width : 80,
				sortable : true
			}, */ {
				id : "customerName",
				name : "Customer Name",
				field : "customerName",
				width : 80,
				sortable : true
			}, /* {
				id : "remainingPoints",
				name : "Remaining Points",
				field : "remainingPoints",
				width : 80,
				sortable : true
			}, */ {
				id : "grandChildCategoryName",
				name : "Grand Child Name",
				field : "grandChildCategoryName",
				width : 80,
				sortable : true
			}, {
				id : "platform",
				name : "Platform",
				field : "platform",
				width : 80,
				sortable : true
			}, {
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			}, {
				id : "createdDate",
				name : "Created Date",
				field : "createdDate",
				width : 80,
				sortable : true
			}, {
				id : "lastUpdatedDate",
				name : "Last UpdatedDate",
				field : "lastUpdatedDate",
				width : 80,
				sortable : true
			} ];

	if (userCrownJewelOrderColumnsStr != 'null'
			&& userCrownJewelOrderColumnsStr != '') {
		var columnOrder = userCrownJewelOrderColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allCrownJewelOrderColumns.length; j++) {
				if (columnWidth[0] == allCrownJewelOrderColumns[j].id) {
					userCrownJewelOrderColumns[i] = allCrownJewelOrderColumns[j];
					userCrownJewelOrderColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userCrownJewelOrderColumns = allCrownJewelOrderColumns;
	}

	var crownJewelOrderOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
		/*editable : true,
		enableCellNavigation : true,
		asyncEditorLoading : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		*/
		//enableAddRow: true,
		//autoHeight: true
	};
	var crownJewelOrderGridSortcol = "id";
	var crownJewelOrderGridSortdir = 1;
	var percentCompleteThreshold = 0;
	//var crownJewelOrderGridSearchString = "";
/*
	function crownJewelOrderGridFilter(item, args) {
		var x = item["id"];
		if (args.crownJewelOrderGridSearchString != ""
				&& x.indexOf(args.crownJewelOrderGridSearchString) == -1) {

			if (typeof x === 'string' || x instanceof String) {
				if (x.toLowerCase().indexOf(
						args.crownJewelOrderGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
*/
	function editFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.id +"'/>";
	    return button;
	}
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    var delFlag = viewFantasyTicket(id);
	});
	
	function crownJewelOrderComparer(a, b) {
		var x = a[crownJewelOrderGridSortcol], y = b[crownJewelOrderGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	/*
	function crownJewelOrderGridToggleFilterRow() {
		crownJewelOrderGrid.setTopPanelVisibility(!crownJewelOrderGrid
				.getOptions().showTopPanel);
	}

	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#crownJewelOrder_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	*/
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getCrownJewelOrderGridData(pageNo);
	}
	
	function getCrownJewelOrderGridData(pageNo){
		if($('#outstanding').hasClass('active')){
			$('#status').val('Outstanding');
		}else if($('#approved').hasClass('active')){
			$('#status').val('Approved');
		}else if($('#rejected').hasClass('active')){
			$('#status').val('Rejected');
		}else{
			$('#status').val('');
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelOrders.json",
			type : "post",
			dataType: "json",
			data : $("#searchCrownJewelOrder").serialize()+"&pageNo="+pageNo+"&headerFilter="+crownJewelOrderGridSearchString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshCrownJewelOrderGridValues(false,jsonData.orders);
				clearAllSelections();
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshCrownJewelOrderGridValues(isAjax, jsonData) {
		if (isAjax) {
			for ( var i = 0; i < crownJewelOrderData.length; i++) {
				for ( var j = 0; j < jsonData.length; j++) {
					if (crownJewelOrderData[i].id == jsonData[j].id) {
						crownJewelOrderData[i].status = approveReject;
						break;
					}
				}
			}
		} else {
			crownJewelOrderData=[];
			if(jsonData!=null && jsonData.length > 0){
				for (var i = 0; i < jsonData.length; i++) {
					var  data= jsonData[i];
					var d = (crownJewelOrderData[i] = {});
					d["id"] = data.id;
					d["regularOrderId"] = data.regularOrderId;
					d["leagueName"] = data.eventName;
					d["teamName"] = data.teamName;
					d["venue"] = data.venue;
					d["city"] = data.city;
					d["state"] = data.state;
					d["country"] = data.country;
					d["isPackageSelected"] = data.isPackageSelected;					
					d["packageCost"] = data.packageCost;
					d["packageNote"] = data.packageNote;
					d["eventId"] = data.eventId;
					d["ticketId"] = data.ticketId;
					d["zone"] = data.zone;
					d["ticketQty"] = data.ticketQty;
					d["ticketPrice"] = data.ticketPrice;
					//d["ticketPoints"] = data.ticketPoints;
					d["requirePoints"] = data.requirePoints;
					//d["packagePoints"] = data.packagePoints;
					d["customerName"] = data.customerName;
					//d["remainingPoints"] = data.remainingPoints;
					d["grandChildCategoryName"] = data.grandChildCategoryName;
					d["platform"] = data.platform;
					d["status"] = data.status;
					d["createdDate"] = data.createdDateStr;
					d["lastUpdatedDate"] = data.lastUpdatedStr;
				}
			}
		}

		crownJewelOrderView = new Slick.Data.DataView();
		crownJewelOrderGrid = new Slick.Grid("#crownJewelOrder_grid",
				crownJewelOrderView, userCrownJewelOrderColumns,
				crownJewelOrderOptions);
		crownJewelOrderGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		crownJewelOrderGrid.setSelectionModel(new Slick.RowSelectionModel());
		crownJewelOrderGrid.registerPlugin(orderCheckboxSelector);
		if(pagingInfo != null){
		var crownJewelOrderPager = new Slick.Controls.Pager(crownJewelOrderView, crownJewelOrderGrid,
				$("#crownJewelOrder_pager"),pagingInfo);
		}
		var crownJewelOrderColumnpicker = new Slick.Controls.ColumnPicker(
				allCrownJewelOrderColumns, crownJewelOrderGrid,
				crownJewelOrderOptions);

		// move the filter panel defined in a hidden div into crownJewelOrderGrid top panel
		//$("#crownJewelOrder_inlineFilterPanel").appendTo(crownJewelOrderGrid.getTopPanel()).show();

		crownJewelOrderGrid.onSort
				.subscribe(function(e, args) {
					crownJewelOrderSortdir = args.sortAsc ? 1 : -1;
					crownJewelOrderGridSortcol = args.sortCol.field;
					if ($.browser.msie && $.browser.version <= 8) {
						crownJewelOrderView.fastSort(
								crownJewelOrderGridSortcol, args.sortAsc);
					} else {
						crownJewelOrderView.sort(crownJewelOrderComparer,
								args.sortAsc);
					}
				});
		// wire up model crownJewelOrders to drive the crownJewelOrderGrid
		crownJewelOrderView.onRowCountChanged.subscribe(function(e, args) {
			crownJewelOrderGrid.updateRowCount();
			crownJewelOrderGrid.render();
		});
		crownJewelOrderView.onRowsChanged.subscribe(function(e, args) {
			crownJewelOrderGrid.invalidateRows(args.rows);
			crownJewelOrderGrid.render();
		});
		
		$(crownJewelOrderGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
			 crownJewelOrderGridSearchString='';
				 var columnId = $(this).data("columnId");
				  if (columnId != null) {
					columnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in columnFilters) {
						  if (columnId !== undefined && columnFilters[columnId] !== "") {
							  crownJewelOrderGridSearchString += columnId + ":" +columnFilters[columnId]+",";
						  }
						}
						getCrownJewelOrderGridData(0);
					}
				  }
			 
			});
			crownJewelOrderGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id != 'editCol'){
						if(args.column.id == 'createdDate' || args.column.id == 'lastUpdatedDate'){
							$("<input type='text' placeholder='mm/dd/yyyy'>")
						   .data("columnId", args.column.id)
						   .val(columnFilters[args.column.id])
						   .appendTo(args.node);
						} 
						else{
							$("<input type='text'>")
						   .data("columnId", args.column.id)
						   .val(columnFilters[args.column.id])
						   .appendTo(args.node);
						}
					}
				}
			});
			crownJewelOrderGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#crownJewelOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			crownJewelOrderGridSearchString = this.value;
			updateCrownJewelOrderGridFilter();
		});
		
		function updateCrownJewelOrderGridFilter() {
			crownJewelOrderDataView.setFilterArgs({
				crownJewelOrderSearchString : crownJewelOrderSearchString
			});
			crownJewelOrderView.refresh();
		}
		*/
		// initialize the model after all the crownJewelOrders have been hooked up
		crownJewelOrderView.beginUpdate();
		crownJewelOrderView.setItems(crownJewelOrderData);
		/*crownJewelOrderView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			crownJewelOrderGridSearchString : crownJewelOrderGridSearchString
		});
		crownJewelOrderView.setFilter(crownJewelOrderGridFilter);*/
		crownJewelOrderView.endUpdate();
		crownJewelOrderView.syncGridSelection(crownJewelOrderGrid, true);
		crownJewelOrderGrid.resizeCanvas();

		var crownJewelOrderrowIndex;
		crownJewelOrderGrid.onSelectedRowsChanged.subscribe(function() {
			var temprCrownJewelOrderRowIndex = crownJewelOrderGrid
					.getSelectedRows([ 0 ])[0];
			if (temprCrownJewelOrderRowIndex != crownJewelOrderrowIndex) {
				crownJewelOrderrowIndex = temprCrownJewelOrderRowIndex;
				//getCustomerInfoForInvoice(temprCrownJewelOrderRowIndex);
			}
		});

		/* crownJewelOrderGrid.onContextMenu.subscribe(function (e) {
		  e.preventDefault();
		  var cell = crownJewelOrderGrid.getCellFromEvent(e);
		  crownJewelOrderGrid.setSelectedRows([cell.row]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if(height < $("#contextMenu").height()){
				height = e.pageY - $("#contextMenu").height();
			}else{
				height = e.pageY;
			}
			if(width < $("#contextMenu").width()){
				width =  e.pageX- $("#contextMenu").width();
			}else{
				width =  e.pageX;
			}
		 $("#contextMenu")
		      .data("row", cell.row)
		      .css("top", height)
		      .css("left", width)
		      .show();
		  $("body").one("click", function () {
		    $("#contextMenu").hide();
		  });
		}); */
		$("div#divLoading").removeClass('show');
	}

	function viewFantasyTicket(orderId){
		if(orderId == null){
			jAlert("There is something wrong.Please try again.");
		}else{
			getViewOrderDetails(orderId);
			/*var url = "${pageContext.request.contextPath}/CrownJewelEvents/ViewOrder?orderId="+orderId;
			popupCenter(url, "View Order Details", "900", "800");*/
		}
	}
	
	function getSelectedOrderGridId() {
		var temprRowIndex = crownJewelOrderGrid.getSelectedRows();
		var idStr = '';
		$.each(temprRowIndex, function(index, value) {
			idStr += ',' + crownJewelOrderGrid.getDataItem(value).id;
		});

		if (idStr != null && idStr != '') {
			idStr = idStr.substring(1, idStr.length);
			return idStr;
		}
	}

	function createOrder(){
		popupCenter("${pageContext.request.contextPath}/CrownJewelEvents/CreateFantasyOrder","Create Fantasy Order","1200","800");
	}
	
	function exportToExcel(){
		if($('#outstanding').hasClass('active')){
			$('#status').val('Outstanding');
		}else if($('#approved').hasClass('active')){
			$('#status').val('Approved');
		}else if($('#rejected').hasClass('active')){
			$('#status').val('Rejected');
		}else{
			$('#status').val('');
		}
		//"&parentCategory="+$('#parentCategory').val()
		var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&teamName="+$('#teamName').val()+"&leagueName="+$('#leagueName').val();
		appendData += "&grandChildCategory="+$('#grandChildCategory').val()+"&status="+$('#status').val()+"&headerFilter="+crownJewelOrderGridSearchString;
	    //var url = "${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelOrdersExportToExcel?"+appendData;
	    var url = apiServerUrl+"CrownJewelOrdersExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		if($('#outstanding').hasClass('active')){
			$('#status').val('Outstanding');
		}else if($('#approved').hasClass('active')){
			$('#status').val('Approved');
		}else if($('#rejected').hasClass('active')){
			$('#status').val('Rejected');
		}else{
			$('#status').val('');
		}
		
		crownJewelOrderGridSearchString='';
		columnFilters = {};
		getCrownJewelOrderGridData(0);
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}

	//Start View Order
	function getViewOrderDetails(orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/CrownJewelEvents/ViewOrder",
			type : "post",
			data : "orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Order Details Found.");
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				} else {
					setViewOrderDetails(jsonData);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setViewOrderDetails(jsonData){
		$('#view-order').modal('show');
		/* if(jsonData.successMessage != null){
			$('#vOrder_successDiv').show();
			$('#vOrder_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#vOrder_errorDiv').show();
			$('#vOrder_errorMsg').text(jsonData.errorMessage);
		} */
		var ticket = jsonData.ticket;
		var cjeOrder = jsonData.cjeOrder;
		var customer = jsonData.customer;
		var event = jsonData.event;
		var customerLoyalty = jsonData.customerLoyalty;
		
		$('#vOrder_id').text(cjeOrder.id);
		$('#vOrder_createdDate').text(cjeOrder.createdDateStr);
		$('#vOrder_status').text(cjeOrder.status);
		$('#vOrder_leagueName').text(cjeOrder.leagueName);
		$('#vOrder_teamName').text(cjeOrder.teamName);
		$('#vOrder_regOrderId').text(cjeOrder.regularOrderId != null ? cjeOrder.regularOrderId : "");
		$('#vOrder_isPackage').text(cjeOrder.isPackageSelected);
		/* if(cjeOrder.isPackageSelected == 'true' || cjeOrder.isPackageSelected == true){
			$('#vOrder_isPackage').text('Yes');
		}
		if(cjeOrder.isPackageSelected == 'false' || cjeOrder.isPackageSelected == false){
			$('#vOrder_isPackage').text('No');
		} */
		
		if(customerLoyalty != null && customerLoyalty != ""){
			$('#vOrder_pointsSpent').text(customerLoyalty.pointsSpent);
		}
		
		$('#vOrder_customerName').text(customer.customerName+" "+customer.lastName);
		$('#vOrder_email').text(customer.email);
		$('#vOrder_phone').text(customer.phone);
		
		$('#vOrder_eventName').text(event.eventName);
		$('#vOrder_eventDateTime').text(event.eventDateStr+" "+event.eventTimeStr);
		
		if(ticket != null && ticket != ""){
			$('#vOrder_section').text(ticket.section);
			$('#vOrder_quantity').text(ticket.quantity);
			$('#vOrder_price').text(ticket.price);
			if(ticket.row != null && ticket.row != ""){
				$('#vOrder_row_div').show();
				$('#vOrder_row').text(ticket.row);
			}else{
				$('#vOrder_row_div').hide();
			}
		}
	}
	//End View Order
	
	//call functions once page loaded
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshCrownJewelOrderGridValues(false, JSON.parse(JSON.stringify(${orders})));
		$('#crownJewelOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCrownJewelOrderPreference()'>");
	});
 
</script>