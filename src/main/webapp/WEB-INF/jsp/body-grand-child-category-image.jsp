<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script type="text/javascript">
	
</script>

<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

  
</style>

<script type="text/javascript">

var jq2 = $.noConflict(true);
$(document).ready(function(){
	
	var allGrandChilds = '${grandChildsCheckAll}';
	var isUpdate = 'true';
	function selectCheckBox(){
		if(allGrandChilds=='true'){
			$('#grandChildsCheckAll').attr("checked","checked");
		}
		allGrandChilds='false';
	}
	selectCheckBox();
	
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	
	//jq2('#autoParentChild').autocomplete("AutoCompleteParentAndChild", {
	 /*jq2('#autoParentChild').autocomplete("AutoCompleteGrandChild", {
			width: 650,
			max: 1000,
			minChars: 2,		
			dataType: "text",
			
			formatItem: function(row, i, max) {
				if(row[0]=='CHILD'){
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				} else {
					return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
				}
			}
		}).result(function (event,row,formatted){
			
				$('#autoParentChild').val('');
				$('#selectedValue').text(row[2]);
				if(row[0]=='CHILD'){
					$('#selectedOption').text('Child');
					$("#child").val(row[1]);
					$("#parent").val('');
					getGrandChilds('childId',row[1]);
					
				} else {
					$('#selectedOption').text('Parent');
					$("#parent").val(row[1]);
					$("#child").val('');
					getGrandChilds('parent',row[1]);
				} 
		}); */
});

function callHideGridDiv() {
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
}

function getGrandChilds(isChild,id){
	
	var url = "";
	if(isChild == 'childId'){
		url = "GetGrandChildByParentAndChild?childId="+id;
	} else{
		url = "GetGrandChildByParentAndChild?parentId="+id;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#grandChilds').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
                if(data.name.toUpperCase()!='NONE' && data.name.toUpperCase()!='OTHER') {
					var rowText = "<option value="+data.id+ ">"+ data.name+"</option>";
					$('#grandChilds').append(rowText);
                }
            }	
		}
	}); 
} 
function callSearchBtnClick(action){
	/*
	$("#infoMainDiv").hide();
	$('#gridDiv').hide();
	 var flag = true;
	 var grandChild=$("#grandChilds").val();
	  if(grandChild==null || grandChild==''){
		  jAlert('Please select the grand childs.');
		  flag= false;
		  return false;
	  }
		
	 if(flag) {
	 	$("#action").val(action);
	 	$("#grandChildImageForm").submit();
	 }
	 */
	 $("#action").val(action);
	 $("#grandChildImageForm").submit();
}
function selectAllGrandChilds(){
	if((document.getElementById("grandChildsCheckAll").checked)){
		$("#grandChilds").each(function(){
			$("#grandChilds option").attr("selected","selected"); 
		});
	}
	else{
		$("#grandChilds").each(function(){
			$("#grandChilds option").removeAttr("selected"); 
		});
	}
}

function callChangeImage(grandChildId){
	$('#imageUploadDiv_'+grandChildId).show();
	$('#imageDisplayDiv_'+grandChildId).hide();
	$('#fileExisting_'+grandChildId).val('N');
	$('#fileDeleted_'+grandChildId).val('Y');
	selectRow(grandChildId);
}
function callChangeCircleImage(grandChildId){
	$('#circleImageUploadDiv_'+grandChildId).show();
	$('#circleImageDisplayDiv_'+grandChildId).hide();
	$('#circleFileExisting_'+grandChildId).val('N');
	$('#circleFileDeleted_'+grandChildId).val('Y');
	selectRow(grandChildId);
}

function selectRow(grandChildId) {
	$('#checkbox_'+grandChildId).attr('checked', true);
}
var validFilesTypes = ["jpg", "jpeg","png","gif"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
        	file.focus();
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
            file.value = null;
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
        	file.focus();
            jAlert("Image Size Should be Greater than 0 and less than 1 MB.");
            file.value = null;
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null || file.value == '') {
    		jAlert("Please select valid image to upload.");
    		file.focus();
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callSaveBtnOnClick(action) {
	
	var flag = true;
	
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		var isMinimamOneImage=false;
		id = this.id.replace('checkbox','file');
		var file = $('#'+id).val();
		if(file != null && file != '') {
			id = this.id.replace('checkbox','file');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
			isMinimamOneImage = true;
		} else {
			id = this.id.replace('checkbox','fileExisting');
			var isFileExisting = $('#'+id).val();
			if(isFileExisting != null && isFileExisting=='Y') {
				isMinimamOneImage = true;
			}
		}
		if(!flag) {
			return false;
		}
		id = this.id.replace('checkbox','circleFile');
		var circleFile = $('#'+id).val();
		if(circleFile != null && circleFile != '') {
			id = this.id.replace('checkbox','circleFile');
			var fileObj = document.getElementById(id);
			flag = CheckFile(fileObj);
			isMinimamOneImage = true;
		} else {
			id = this.id.replace('checkbox','circleFileExisting');
			var isCircleFileExisting = $('#'+id).val();
			if(isCircleFileExisting != null && isCircleFileExisting=='Y') {
				isMinimamOneImage = true;
			}
		}
		
		if(!isMinimamOneImage) {
			jAlert('Select image or circle image to save grand child.');
			id = this.id.replace('checkbox','file');
			$("#"+id).focus();
			flag= false;
			return false;
		}
		if(!flag) {
			return false;
		}
		
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one grand child to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to save selected grand child images ?.","Confirm",function(r){
		  if(r) {
				$("#action").val(action);
			 	$("#grandChildImageForm").submit();
			}
	  });
	}
	
	
}
function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing grand child image to delete.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	 jConfirm("Are you sure you want to remove selected grand child images ?.","Confirm",function(r){
		 if(r) { 
				$("#action").val(action);
				$("#grandChildImageForm").submit();
			 }
	 });
	}
	
	
}
function callImageOnChange(grandChildId) {
	$('#fileDeleted_'+grandChildId).val('N');
	selectRow(grandChildId);
}
function callCircleImageOnChange(grandChildId) {
	$('#circleFileDeleted_'+grandChildId).val('N');
	selectRow(grandChildId);
}
</script>

<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Images</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Images</a></li>
						<li><i class="fa fa-laptop"></i>Grand Child</li>						  	
					</ol>
				</div>
</div>

<div class="container">
<div class="row">

	<div class="alert alert-success fade in" id="infoMainDiv"
	<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="grandChildImageForm" id="grandChildImageForm" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/GrandChildCategoryImages">
	<input type="hidden" id="action" name="action" />
	<div class="row filters-div">
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">			
			<label for="imageText" class="control-label"><b>Grand Child :</b></label>
        </div> 
		<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">		
			<input type="hidden" id="child" name="child" value ="${child}">
			<input type="hidden" id="parent" name="parent" value ="${parent}">
					<!-- <input class="form-control input-sm m-bot15 fullWidth" type="text" name="autoParentChild" id="autoParentChild" onchange="callHideGridDiv();"> -->
					
			<select class="form-control input-sm fullWidth" type="text" name="grandChildSearch" id="grandChildSearch" onchange="callSearchBtnClick('search')"> 	
			  <option value="">--All--</option>
			  <c:forEach items="${grandChildsList}" var="grandChild">
				<option value="${grandChild.id}" <c:if test="${grandChild.id == grandChildSearch}">selected</c:if>>${grandChild.name}</option>
			 </c:forEach> 
			</select>	
                    	
                    			
	          		<!-- 
	          		<div >
	          			<button type="button" onclick="getGrandChilds();" class="btn btn-primary btn-sm">Search</button>	          
          			</div>-->
          
         <!--  <div class="col-lg-2">
			<button type="button" onclick="callSearchBtnClick('search');" class="btn btn-primary btn-sm">Search</button>
          </div> -->
        </div>
          <!--  <div class="form-group" >
	          <label for="imageText" class="col-lg-3 col-lg-offset-2 control-label">
	          	<b><span id="selectedOption" >${selectedOption}</span></b>
	          </label>
          <div class="col-lg-4 ">
          	<label for="imageText" class="control-label">
	          	<b><span id="selectedValue" >${selectedValue}</span></b>
          </label>
          </div>
			<div class="col-lg-2"></div>           
          </div>
          <div class="form-group" >
          <label for="imageText" class="col-lg-3 col-lg-offset-2 control-label"><b>Grand Childs : </b></label>
          <div class="col-lg-4">
          		<input type="hidden" id="grandChildStr" name="grandChildStr" value ="${grandChildStr}"/> 
				<input type="checkbox" id="grandChildsCheckAll" name="grandChildsCheckAll" onclick ="selectAllGrandChilds();" 
					<c:if test ="${grandChildsCheckAll}"> checked="checked" </c:if>/> 
				<label class="control-label" for="grandChildsCheckAll">Select All</label>
				<div class="form-group" >
                    <div >
                    <select multiple class="form-control fullWidth" name="grandChilds" id="grandChilds" onchange="callHideGridDiv();">
                      <c:forEach items="${grandChilds}" var="grandChild">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${grandChild.id}${temp}"/>
						<option 
							<c:if test="${fn:contains(grandChildStr,temp2)}"> Selected </c:if>
							value="${grandChild.id}"> ${grandChild.name}</option>
						</c:forEach> 
                     </select>
                    	
                    </div>
                </div>
          </div>
          <div class="col-lg-2">
          </div>
           </div> 
          <div class="form-group" >
	          <div class="col-lg-2 col-lg-offset-4">
	          <button type="button" onclick="callSearchBtnClick('search');" class="btn btn-primary btn-sm">Search</button>
	          </div>
          </div>
		  -->
	</div>
<div class="row clearfix" id="gridDiv">
	<c:if test="${not empty grandChildImageList}">
	<div class="full-width column">
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>
			<br />
			<br />
			<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-3">
							Grand Child
						</th>
						<th class="col-lg-4">
							Image
						</th>
						<!-- <th class="col-lg-4">
							Circle Image
						</th> -->
						
						
					</tr>
				</thead>
				<tbody>
				 <c:forEach var="gcImage" varStatus="vStatus" items="${grandChildImageList}">
                    <tr <c:if test="${gcImage.id ne null}">style="background-color: #caf4ca;"</c:if> >
						<td>
	                      	<input type="checkbox" class="selectCheck" id="checkbox_${gcImage.grandChildCategory.id}" name="checkbox_${gcImage.grandChildCategory.id}" />
							<input type="hidden" name="id_${gcImage.grandChildCategory.id}" id="id_${gcImage.grandChildCategory.id}" value="${gcImage.id}" />
							<input type="hidden" name="fileExisting_${gcImage.grandChildCategory.id}" id="fileExisting_${gcImage.grandChildCategory.id}" 
							<c:if test="${gcImage.imageFileUrl ne null}"> value="Y" </c:if>
							<c:if test="${gcImage.imageFileUrl eq null}"> value="N" </c:if> />
							<input type="hidden" name="circleFileExisting_${gcImage.grandChildCategory.id}" id="circleFileExisting_${gcImage.grandChildCategory.id}" 
							<c:if test="${gcImage.circleImageFileUrl ne null}"> value="Y" </c:if>
							<c:if test="${gcImage.circleImageFileUrl eq null}"> value="N" </c:if> />
							<input type="hidden" name="circleFileDeleted_${gcImage.grandChildCategory.id}" id="circleFileDeleted_${gcImage.grandChildCategory.id}" value="N" />
							<input type="hidden" name="fileDeleted_${gcImage.grandChildCategory.id}" id="fileDeleted_${gcImage.grandChildCategory.id}" value="N" />
                      </td>
                    <td style="font-size: 13px;" align="center">
                     		<b><label for="grandChild" class="list-group-item" id="grandChild_${gcImage.grandChildCategory.id}">${gcImage.grandChildCategory.name}</label></b>
                     		
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="orm-group-top" id="imageUploadDiv_${gcImage.grandChildCategory.id}" 
                                  <c:if test="${gcImage.imageFileUrl ne null }">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-md-3 col-sm-12 co-xs-12">Image File</label>
                                      <div class="col-lg-9 col-sm-12 co-xs-12 text-center">
                                          <input type="file" id="file_${gcImage.grandChildCategory.id}" name="file_${gcImage.grandChildCategory.id}" onchange="callImageOnChange('${gcImage.grandChildCategory.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="imageDisplayDiv_${gcImage.grandChildCategory.id}" 
                                   <c:if test="${gcImage.imageFileUrl eq null}">style="display: none" </c:if>>
                                      <div class="col-xs-12 ">
                                       <c:if test="${gcImage.imageFileUrl ne null}">
                                          <img src='${api.server.url}GetImageFile?type=grandChildCategoryImage&filePath=${gcImage.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage('${gcImage.grandChildCategory.id}');">
											Change Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td>
						<%-- <td style="font-size: 13px;" align="center">
							<div class="form-horizontal">
                                  <div class="form-group form-group-top" id="circleImageUploadDiv_${gcImage.grandChildCategory.id}" 
                                  <c:if test="${gcImage.circleImageFileUrl ne null }">style="display: none;" </c:if>>
                                      <label for="circleImageFile" class="col-lg-3 control-label">Circle Image File</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="circleFile_${gcImage.grandChildCategory.id}" name="circleFile_${gcImage.grandChildCategory.id}" onchange="callCircleImageOnChange('${gcImage.grandChildCategory.id}');">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top" id="circleImageDisplayDiv_${gcImage.grandChildCategory.id}" 
                                   <c:if test="${gcImage.circleImageFileUrl eq null}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${gcImage.circleImageFileUrl ne null}">
                                          <img src='${pageContext.request.contextPath}/RewardTheFan/GetImageFile?type=grandChildCategoryImage&filePath=${gcImage.circleImageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeCircleImage('${gcImage.grandChildCategory.id}');">
											Change Circle Image</a>
                                         </c:if>
                                      </div>
                                  </div>
                              </div>
						</td> --%>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnOnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>
		</div>
	</c:if>
	<c:if test="${empty grandChildImageList and not empty grandChildStr}">
	<div class="alert alert-block alert-danger fade in" id="infoMainDiv">
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span>No records Found.</span>
    </div>
	</c:if>
	</div>
	</form>
</div>
