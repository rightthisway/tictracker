<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
    <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker.css" type="text/css"/>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
.slick-viewport {
    overflow-x: hidden !important;
}

#addCustomer {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

input{
		color : black !important;
	}
	
#totalPrice{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}

#totalQty{
  font-family: arial;
  font-size: 10pt;
  font-weight: bold;
}
</style>
    
<script>
	function goToInvoice(invoiceId){
		window.opener.location = "${pageContext.request.contextPath}/Accounting/Invoices?status=Outstanding&invoiceNo="+invoiceId;
		window.close();
	}
	
	/* window.onload = function() {
		refreshOrderDetails();
	};
	
	function refreshOrderDetails(){
		var primaryPaymentMethod = '${order.primaryPaymentMethod}';
		var secondaryPaymentMethod = '${order.secondaryPaymentMethod}';
		var primaryPaymentAmount = '${order.primaryPayAmt}';
		var secondaryPaymentAmount = '${order.secondaryPayAmt}';
		
		if(primaryPaymentMethod == "CREDITCARD"){
			$('#paidCardAmount').text(primaryPaymentAmount);
			$('#usedRewardPoints').text(secondaryPaymentAmount);
		}else if(primaryPaymentMethod == "PARTIAL_REWARDS"){
			$('#paidCardAmount').text(secondaryPaymentAmount);
			$('#usedRewardPoints').text(primaryPaymentAmount);
		}else if(primaryPaymentMethod == "FULL_REWARDS"){
			$('#paidCardAmount').text(secondaryPaymentAmount);
			$('#usedRewardPoints').text(primaryPaymentAmount);
		}
	} */
</script>

 
<div class="row">
	<div class="col-lg-12">
		 
		<ol class="breadcrumb">
			<li><i class="fa fa-laptop"></i>View Invoice</li>
		</ol>
		
	</div>
</div>

<div class="row">
<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong
					style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					${successMessage} <a href="javascript:goToInvoice(${invoice.id})" style="font-size:15px;">${invoice.id}</a>
				</strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong
					style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>	
</div>
<br/><br/>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Order Details </b></header>
			<div class="panel-body">
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Invoice Number : </label>
					<span style="font-size: 12px;" >${invoice.id} </span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Order No : </label><span style="font-size: 12px;">${order.id}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Order Total : </label><span style="font-size: 12px;">${order.orderTotal}</span>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Primary Payment Method : </label><span style="font-size: 12px;">
					<c:if test="${order.primaryPaymentMethod ne null}">${order.primaryPaymentMethod}</c:if></span>
                </div>
                <div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Secondary Payment Method : </label><span style="font-size: 12px;">
					<c:if test="${order.secondaryPaymentMethod ne null}">${order.secondaryPaymentMethod}</c:if></span>
                </div>
                <div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Third Payment Method : </label><span style="font-size: 12px;">
					<c:if test="${order.thirdPaymentMethod ne null}">${order.thirdPaymentMethod}</c:if></span>
                </div>
              </div>  
              <div class="form-group"> 
              	<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Primary Payment Amount : </label><span style="font-size: 12px;">${order.primaryPayAmt}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Secondary Payment Amount : </label><span style="font-size: 12px;">${order.secondaryPayAmt}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Third Payment Amount : </label><span style="font-size: 12px;">${order.thirdPayAmt}</span>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Earned Reward Points : </label><span style="font-size: 12px;">${customerLoyalty.pointsEarned}</span>
                </div>
				<div class="col-sm-4">
					&nbsp;
				</div>
				<div class="col-sm-4">
					&nbsp;
				</div>
			</div>
			
		</div>
		</section>
	</div>
</div>

<br/><br/>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> 
		<header style="font-size: 25px;font-family: arial;" class="panel-heading"><b>Invoice Billing Details </b></header>
			<div class="panel-body">
			
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Customer Name : </label>
					<span style="font-size: 12px;" id="customerNameDiv">${orderDetails.blFirstName}&nbsp;${orderDetails.blLastName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Phone : </label><span style="font-size: 12px;">${orderDetails.blPhone1}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Address Line 1 : </label><span style="font-size: 12px;">${orderDetails.blAddress1}</span>
                </div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Address Line 2: </label><span style="font-size: 12px;">${orderDetails.blAddress2}</span>
                </div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">City : </label><span style="font-size: 12px;">${orderDetails.blCity}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">State : </label><span style="font-size: 12px;">${orderDetails.blStateName}</span>
				</div>
				
			</div>
			<div class="form-group">
				<div class="col-sm-4"> 
					<label style="font-size: 13px;font-weight:bold;">Country :</label><span style="font-size: 12px;">${orderDetails.blCountryName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">ZipCode :</label><span style="font-size: 12px;">${orderDetails.blZipCode}</span>
				</div>
				<div class="col-sm-4">&nbsp;
				</div>
				<div class="col-sm-4">&nbsp;
				</div>
			</div>
			
		</div>
		</section>
	</div>
</div>

<!-- Shipping Address section -->
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
		 <header style="font-size: 25px;font-family: arial;" class="panel-heading"><b>Invoice Shipping Address</b>
		 </header>
			<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Address Line 1 :</label><span style="font-size: 12px;">${orderDetails.shAddress1}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Address Line 2 :</label><span style="font-size: 12px;">${orderDetails.shAddress2}</span>
				</div>
				
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Phone :</label><span style="font-size: 12px;">${orderDetails.shPhone1}</span>
				</div>
				
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">City :</label><span style="font-size: 12px;">${orderDetails.shCity}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">State :</label><span style="font-size: 12px;">${orderDetails.shStateName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Country :</label><span style="font-size: 12px;">${orderDetails.shCountryName}</span>
				</div>
				
			</div>
			
			<div class="form-group">
				<div class="col-sm-12">
					<label style="font-size: 13px; font-weight: bold;">ZipCode :</label><span style="font-size: 12px;">${orderDetails.shZipCode}</span>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 25px;font-family: arial;" class="panel-heading" id="eventDetailDiv">
			<b>${order.eventName}, ${order.eventDateStr},${order.eventTimeStr},${order.venueName}</b>
		 </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Section :</label>
					<span style="font-size: 12px;">${order.section}</span>
				</div>
				
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Quantity :</label>
					<span style="font-size: 12px;">${order.qty}</span>
				</div>
				
				<div class="col-sm-4">
					<label style="font-size: 13px;font-weight:bold;">Ticket Price :</label>
					<span style="font-size: 12px;">${order.ticketPrice}</span>
				</div>
				
			</div>
			
			<div class="form-group">
				<div class="col-sm-12">
					<label style="font-size: 13px;font-weight:bold;">Sold Price :</label>
					<span style="font-size: 12px;">${order.soldPrice}</span>
				</div>
				
			</div>
			
		</div>
		</section>
	</div>
</div>