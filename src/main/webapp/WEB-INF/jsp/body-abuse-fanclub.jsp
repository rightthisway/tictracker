<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(abuseCommentGrid != null && abuseCommentGrid != undefined){
			abuseCommentGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${type == 'FANCLUB'}">	
			$('#fanclub').addClass('active');
			$('#fanclubTab').addClass('active');
		</c:when>
		<c:when test="${type == 'POST'}">
			$('#post').addClass('active');
			$('#postTab').addClass('active');
		</c:when>
		<c:when test="${type == 'EVENT'}">
			$('#event').addClass('active');
			$('#eventTab').addClass('active');
		</c:when>
		<c:when test="${type == 'VIDEO'}">
			$('#video').addClass('active');
			$('#videoTab').addClass('active');
		</c:when>
		<c:when test="${type == 'COMMENT'}">
			$('#comment').addClass('active');
			$('#commentTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#fanclub1").click(function(){
		callTabOnChange('FANCLUB');
	});
	$("#post1").click(function(){
		callTabOnChange('POST');
	});
	$("#event1").click(function(){
		callTabOnChange('EVENT');
	});
	$("#video1").click(function(){
		callTabOnChange('VIDEO');
	});
	$("#comment1").click(function(){
		callTabOnChange('COMMENT');
	});
	
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ManageFanClubAbuse?type="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Abuse Reported</a></li>
			<li><i class="fa fa-laptop"></i>Manage Abuse Fanclub</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="fanclubTab" class=""><a id="fanclub1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#fanclub">Abuse reported Fanclub</a></li>
				<li id="postTab" class=""><a id="post1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#post">Abuse reported Post</a></li>
				<li id="eventTab" class=""><a id="event1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#event">Abuse reported Event</a></li>
				<li id="videoTab" class=""><a id="video1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#video">Abuse reported Video</a></li>
				<li id="commentTab" class=""><a id="comment1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#comment">Abuse reported Comment</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="fanclub" class="tab-pane">
			<c:if test="${type =='FANCLUB'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="removeFanclub('FANCLUB');">Remove Fanclub</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="blockFanclub('FANCLUB')">Block Fanclub User</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="abuseFanclubGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Abuse Reported Fanclubs</label>
							<div class="pull-right">
								<a href="javascript:abuseFanclubExportToExcel('FANCLUB')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseFanclubResetFilters('FANCLUB')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abuseFanclub_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abuseFanclub_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="post" class="tab-pane">
			<c:if test="${type =='POST'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="removeFanclub('POST');">Remove Post</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="blockFanclub('POST')">Block Post User</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="abusePostGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Abuse Reported Posts</label>
							<div class="pull-right">
								<a href="javascript:abuseFanclubExportToExcel('POST')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseFanclubResetFilters('POST')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abusePost_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abusePost_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
			<div id="event" class="tab-pane">
			<c:if test="${type =='EVENT'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="removeFanclub('EVENT');">Remove Event</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="blockFanclub('EVENT')">Block Event User</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="abuseEventGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Abuse Reported Events</label>
							<div class="pull-right">
								<a href="javascript:abuseFanclubExportToExcel('EVENT')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseFanclubResetFilters('EVENT')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abuseEvent_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abuseEvent_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
			<div id="video" class="tab-pane">
			<c:if test="${type =='VIDEO'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="removeFanclub('VIDEO');">Remove Video</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="blockFanclub('VIDEO')">Block Video User</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="abuseVideoGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Abuse Reported Videos</label>
							<div class="pull-right">
								<a href="javascript:abuseFanclubExportToExcel('VIDEO')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseFanclubResetFilters('VIDEO')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abuseVideo_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abuseVideo_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
			<div id="comment" class="tab-pane">
			<c:if test="${type =='COMMENT'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="removeFanclub('COMMENT');">Remove Comment</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="blockFanclub('COMMENT')">Block Comment User</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="abuseCommentGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Abuse Reported Comments</label>
							<div class="pull-right">
								<a href="javascript:abuseFanclubExportToExcel('COMMENT')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:abuseFanclubResetFilters('COMMENT')" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="abuseComment_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="abuseComment_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>

<script type="text/javascript">

	//Add Reward Comment Bank	
	function removeFanclub(type){		
		var tempAbuseCommentRowIndex = abuseCommentGrid.getSelectedRows([0])[0];
		if (tempAbuseCommentRowIndex == null) {
			jAlert("Please select comment to remove.", "info");
			return false;
		}else {
			var commentId = abuseCommentGrid.getDataItem(tempAbuseCommentRowIndex).abuseCommentId;
			var requestUrl = "${pageContext.request.contextPath}/UpdateAbuseFanclub.json?type="+type;
			var dataString = $('#abuseCommentForm').serialize();
			$.ajax({
				url : requestUrl,
				type : "post",
				dataType: "json",
				data: dataString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						$('#qBModal').modal('hide');
						pagingInfo = jsonData.pagingInfo;
						abuseCommentColumnFilters = {};
						refreshAbuseCommentGridValues(jsonData.abuseDatas);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	
	function getSelectedDataId(type){
		var tmpRowIndex = null;
		var dataId = null;
		if(type == 'FANCLUB'){
			tmpRowIndex = abuseFanclubGrid.getSelectedRows([0])[0];
			if (tmpRowIndex == null) {
				jAlert("Please select fanclub to remove.", "info");
				return null;
			}
			dataId = abuseFanclubGrid.getDataItem(tmpRowIndex).abuseFanclubId;
		}else if(type == 'POST'){
			tmpRowIndex = abusePostGrid.getSelectedRows([0])[0];
			if (tmpRowIndex == null) {
				jAlert("Please select fanclub post to remove.", "info");
				return null;
			}
			dataId = abusePostGrid.getDataItem(tmpRowIndex).abusePostId;
		}else if(type == 'EVENT'){
			tmpRowIndex = abuseEventGrid.getSelectedRows([0])[0];
			if (tmpRowIndex == null) {
				jAlert("Please select fanclub event to remove.", "info");
				return null;
			}
			dataId = abuseEventGrid.getDataItem(tmpRowIndex).abuseEventId;
		}else if(type == 'VIDEO'){
			tmpRowIndex = abuseVideoGrid.getSelectedRows([0])[0];
			if (tmpRowIndex == null) {
				jAlert("Please select fanclub video to remove.", "info");
				return null;
			}
			dataId = abuseVideoGrid.getDataItem(tmpRowIndex).abuseVideoId;
		}else if(type == 'COMMENT'){
			tmpRowIndex = abuseCommentGrid.getSelectedRows([0])[0];
			if (tmpRowIndex == null) {
				jAlert("Please select fanclub comment to remove.", "info");
				return null;
			}
			dataId = abuseCommentGrid.getDataItem(tmpRowIndex).abuseCommentId;
		}
		return dataId;
	}
	
	function removeFanclub(type){
		var dataId = getSelectedDataId(type);
		if(dataId == null){
			jAlert("Please select record to remove.", "info");
			return false;
		}
		removeBlockFanclub(type,dataId,"REMOVE");
	}
	
	function blockFanclub(type){
		var dataId = getSelectedDataId(type);
		if(dataId == null){
			jAlert("Please select record to block.", "info");
			return false;
		}
		removeBlockFanclub(type,dataId,"BLOCK");
	}
	
	
	function removeBlockFanclub(type,dataId,action){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateFanClubAbuseData.json",
			type : "post",
			dataType: "json",
			data: "type="+type+"&dataId="+dataId+"&action="+action,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					pagingInfo = jsonData.pagingInfo;
					if(type == 'FANCLUB'){
						abuseFanclubColumnFilters = {};
						refreshAbuseFanclubGridValues(jsonData.abuseData);
					}else if(type == 'POST'){
						abusePostColumnFilters = {};
						refreshAbusePostGridValues(jsonData.abuseData);
					}else if(type == 'EVENT'){
						abuseEventColumnFilters = {};
						refreshAbuseEventGridValues(jsonData.abuseData);
					}else if(type == 'VIDEO'){
						abuseVideoColumnFilters = {};
						refreshAbuseVideoGridValues(jsonData.abuseData);
					}else if(type == 'COMMENT'){
						abuseCommentColumnFilters = {};
						refreshAbuseCommentGridValues(jsonData.abuseData);
					}
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	
	function abuseFanclubExportToExcel(type){
		var appendData = "headerFilter="+abuseCommentSearchString+"&type="+type;
	    var url =apiServerUrl+"abuseFanclubExport?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function abuseFanclubResetFilters(type){
		if(type == 'FANCLUB'){
			abuseFanclubSearchString='';
			abuseFanclubColumnFilters = {};
			getAbuseFanclubGridData(0);
		}else if(type == 'POST'){
			abusePostSearchString='';
			abusePostColumnFilters = {};
			getAbusePostGridData(0);
		}else if(type == 'EVENT'){
			abuseEventSearchString='';
			abuseEventColumnFilters = {};
			getAbuseEventGridData(0);
		}else if(type == 'VIDEO'){
			abuseVideoSearchString='';
			abuseVideoColumnFilters = {};
			getAbuseVideoGridData(0);
		}else if(type == 'COMMENT'){
			abuseCommentSearchString='';
			abuseCommentColumnFilters = {};
			getAbuseCommentGridData(0);
		}
		
	}
	
	function pagingControl(move, id) {
		if(id == 'abuseComment_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getAbuseCommentGridData(pageNo);
		}else if(id == 'abuseFanclub_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getAbuseFanclubGridData(pageNo);
		}else if(id == 'abusePost_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getAbusePostGridData(pageNo);
		}else if(id == 'abuseVideo_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getAbuseVideoGridData(pageNo);
		}else if(id == 'abuseEvent_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getAbuseEventGridData(pageNo);
		}
	}
	
	function getAbuseCommentGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageFanClubAbuse.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+abuseCommentSearchString+"&type=${type}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshAbuseCommentGridValues(jsonData.abuseData);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var abuseCommentCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var abuseCommentDataView;
	var abuseCommentGrid;
	var abuseCommentData = [];
	var abuseCommentGridPager;
	var abuseCommentSearchString='';
	var abuseCommentColumnFilters = {};
	var userAbuseCommentColumnsStr = '<%=session.getAttribute("abuseFanclubCommentGrid")%>';

	var userAbuseCommentColumns = [];
	var allAbuseCommentColumns = [
			 {
				id : "reportedByUserId",
				field : "reportedByUserId",
				name : "Reported By User Id",
				width : 80,
				sortable : true
			},{
				id : "reportedByEmail",
				field : "reportedByEmail",
				name : "Reported By Email",
				width : 80,
				sortable : true
			},{
				id : "reportedByPhone",
				field : "reportedByPhone",
				name : "Reported By Phone",
				width : 80,
				sortable : true
			},{
				id : "abuseType",
				field : "abuseType",
				name : "Abuse Type",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Reported Date",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "Comment User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Comment Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Comment Phone",
				width : 80,
				sortable : true
			} ,{
				id : "title",
				field : "title",
				name : "Comment Text",
				width : 80,
				sortable : true
			} ,{
				id : "mediaUrl",
				field : "mediaUrl",
				name : "Comment Media Preview",
				width : 80,
				formatter: previewMediaFormatter
			},,{
				id : "status",
				field : "status",
				name : "Blocked/Removed?",
				width : 80,
				sortable : true
			} ,{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			} ,{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userAbuseCommentColumnsStr != 'null' && userAbuseCommentColumnsStr != '') {
		columnOrder = userAbuseCommentColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allAbuseCommentColumns.length; j++) {
				if (columnWidth[0] == allAbuseCommentColumns[j].id) {
					userAbuseCommentColumns[i] = allAbuseCommentColumns[j];
					userAbuseCommentColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAbuseCommentColumns = allAbuseCommentColumns;
	}
	
	function previewMediaFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.mediaUrl != null &&  dataContext.mediaUrl != 'null' && dataContext.mediaUrl != ''){
			var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
			return button;
		}else if(dataContext.imageUrl != null &&  dataContext.imageUrl != 'null' && dataContext.imageUrl != ''){
			var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.imageUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
		
	}
	
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var abuseCommentOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var abuseCommentGridSortcol = "abuseCommentId";
	var abuseCommentGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function abuseCommentGridComparer(a, b) {
		var x = a[abuseCommentGridSortcol], y = b[abuseCommentGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshAbuseCommentGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		abuseCommentData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (abuseCommentData[i] = {});
				d["id"] = i;
				d["abuseCommentId"] = data.id;
				d["commentId"] = data.commentId;
				d["customerId"] = data.customerId;
				d["createdDate"] = data.createdDateStr;
				d["reportedByUserId"] = data.reportedByUserId;
				d["reportedByEmail"] = data.reportedByEmail;
				d["reportedByPhone"] = data.reportedByPhone;
				d["abuseType"] = data.abuseType;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["title"] = data.title;
				d["mediaUrl"] = data.mediaUrl;
				d["imageUrl"] = data.imageUrl;
				d["status"] = data.status;
				d["createdBy"] = data.createdUserId;
				d["updatedBy"] = data.updatedUserId;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		abuseCommentDataView = new Slick.Data.DataView();
		abuseCommentGrid = new Slick.Grid("#abuseComment_grid", abuseCommentDataView,
				userAbuseCommentColumns, abuseCommentOptions);
		abuseCommentGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		abuseCommentGrid.setSelectionModel(new Slick.RowSelectionModel());
		abuseCommentGrid.registerPlugin(abuseCommentCheckboxSelector);
		
		abuseCommentGridPager = new Slick.Controls.Pager(abuseCommentDataView,
					abuseCommentGrid, $("#abuseComment_pager"),
					pagingInfo);
		/* var abuseCommentGridColumnpicker = new Slick.Controls.ColumnPicker(
				allAbuseCommentColumns, abuseCommentGrid, abuseCommentOptions); */
		
		abuseCommentGrid.onSort.subscribe(function(e, args) {
			abuseCommentGridSortdir = args.sortAsc ? 1 : -1;
			abuseCommentGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				abuseCommentDataView.fastSort(abuseCommentGridSortcol, args.sortAsc);
			} else {
				abuseCommentDataView.sort(abuseCommentGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the abuseCommentGrid
		abuseCommentDataView.onRowCountChanged.subscribe(function(e, args) {
			abuseCommentGrid.updateRowCount();
			abuseCommentGrid.render();
		});
		abuseCommentDataView.onRowsChanged.subscribe(function(e, args) {
			abuseCommentGrid.invalidateRows(args.rows);
			abuseCommentGrid.render();
		});
		$(abuseCommentGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							abuseCommentSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								abuseCommentColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in abuseCommentColumnFilters) {
										if (columnId !== undefined
												&& abuseCommentColumnFilters[columnId] !== "") {
											abuseCommentSearchString += columnId
													+ ":"
													+ abuseCommentColumnFilters[columnId]
													+ ",";
										}
									}
									getAbuseCommentGridData(0);
								}
							}

						});
		abuseCommentGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			//if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'mediaUrl' && args.column.id != 'imageUrl'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(abuseCommentColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(abuseCommentColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			//}

		});
		abuseCommentGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		abuseCommentDataView.beginUpdate();
		abuseCommentDataView.setItems(abuseCommentData);
		//abuseCommentDataView.setFilter(filter);
		abuseCommentDataView.endUpdate();
		abuseCommentDataView.syncGridSelection(abuseCommentGrid, true);
		abuseCommentGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAbuseCommentPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = abuseCommentGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('abuseFanclubCommentGrid', colStr);
	}
	
	
	//Fan club grid
	function getAbuseFanclubGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageFanClubAbuse.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+abuseFanclubSearchString+"&type=${type}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshAbuseFanclubGridValues(jsonData.abuseData);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var abuseFanclubCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var abuseFanclubDataView;
	var abuseFanclubGrid;
	var abuseFanclubData = [];
	var abuseFanclubGridPager;
	var abuseFanclubSearchString='';
	var abuseFanclubColumnFilters = {};
	var userAbuseFanclubColumnsStr = '<%=session.getAttribute("abuseFanclubGrid")%>';

	var userAbuseFanclubColumns = [];
	var allAbuseFanclubColumns = [
			 {
				id : "reportedByUserId",
				field : "reportedByUserId",
				name : "Reported By User Id",
				width : 80,
				sortable : true
			},{
				id : "reportedByEmail",
				field : "reportedByEmail",
				name : "Reported By Email",
				width : 80,
				sortable : true
			},{
				id : "reportedByPhone",
				field : "reportedByPhone",
				name : "Reported By Phone",
				width : 80,
				sortable : true
			},{
				id : "abuseType",
				field : "abuseType",
				name : "Abuse Type",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Reported Date",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "Fanclub User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Fanclub Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Fanclub Phone",
				width : 80,
				sortable : true
			} ,{
				id : "title",
				field : "title",
				name : "Fanclub Title",
				width : 80,
				sortable : true
			}  ,{
				id : "description",
				field : "description",
				name : "Fanclub Description",
				width : 80,
				sortable : true
			}  ,{
				id : "count",
				field : "count",
				name : "Fanclub Member count",
				width : 80,
				sortable : true
			} ,{
				id : "mediaUrl",
				field : "mediaUrl",
				name : "Fanclub Media Preview",
				width : 80,
				formatter: previewFanclubImage
			},{
				id : "status",
				field : "status",
				name : "Blocked/Removed?",
				width : 80,
				sortable : true
			} ,{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			} ,{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userAbuseFanclubColumnsStr != 'null' && userAbuseFanclubColumnsStr != '') {
		columnOrder = userAbuseFanclubColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allAbuseFanclubColumns.length; j++) {
				if (columnWidth[0] == allAbuseFanclubColumns[j].id) {
					userAbuseFanclubColumns[i] = allAbuseFanclubColumns[j];
					userAbuseFanclubColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAbuseFanclubColumns = allAbuseFanclubColumns;
	}
	
	function previewFanclubImage(row,cell,value,columnDef,dataContext){
		if(dataContext.mediaUrl != null &&  dataContext.mediaUrl != 'null' && dataContext.mediaUrl != ''){
			var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.mediaUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
		
	}
	
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var abuseFanclubOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var abuseFanclubGridSortcol = "abuseFanclubId";
	var abuseFanclubGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function abuseFanclubGridComparer(a, b) {
		var x = a[abuseFanclubGridSortcol], y = b[abuseFanclubGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshAbuseFanclubGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		abuseFanclubData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (abuseFanclubData[i] = {});
				d["id"] = i;
				d["abuseFanclubId"] = data.id;
				d["fanclubId"] = data.fanclubId;
				d["customerId"] = data.customerId;
				d["createdDate"] = data.createdDateStr;
				d["reportedByUserId"] = data.reportedByUserId;
				d["reportedByEmail"] = data.reportedByEmail;
				d["reportedByPhone"] = data.reportedByPhone;
				d["abuseType"] = data.abuseType;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["title"] = data.title;
				d["description"] = data.description;
				d["count"] = data.count;
				d["mediaUrl"] = data.posterUrl;
				d["status"] = data.status;
				d["createdBy"] = data.createdUserId;
				d["updatedBy"] = data.updatedUserId;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		abuseFanclubDataView = new Slick.Data.DataView();
		abuseFanclubGrid = new Slick.Grid("#abuseFanclub_grid", abuseFanclubDataView,
				userAbuseFanclubColumns, abuseFanclubOptions);
		abuseFanclubGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		abuseFanclubGrid.setSelectionModel(new Slick.RowSelectionModel());
		abuseFanclubGrid.registerPlugin(abuseFanclubCheckboxSelector);
		
		abuseFanclubGridPager = new Slick.Controls.Pager(abuseFanclubDataView,
					abuseFanclubGrid, $("#abuseFanclub_pager"),
					pagingInfo);
		/* var abuseFanclubGridColumnpicker = new Slick.Controls.ColumnPicker(
				allAbuseFanclubColumns, abuseFanclubGrid, abuseFanclubOptions); */
		
		abuseFanclubGrid.onSort.subscribe(function(e, args) {
			abuseFanclubGridSortdir = args.sortAsc ? 1 : -1;
			abuseFanclubGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				abuseFanclubDataView.fastSort(abuseFanclubGridSortcol, args.sortAsc);
			} else {
				abuseFanclubDataView.sort(abuseFanclubGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the abuseFanclubGrid
		abuseFanclubDataView.onRowCountChanged.subscribe(function(e, args) {
			abuseFanclubGrid.updateRowCount();
			abuseFanclubGrid.render();
		});
		abuseFanclubDataView.onRowsChanged.subscribe(function(e, args) {
			abuseFanclubGrid.invalidateRows(args.rows);
			abuseFanclubGrid.render();
		});
		$(abuseFanclubGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							abuseFanclubSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								abuseFanclubColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in abuseFanclubColumnFilters) {
										if (columnId !== undefined
												&& abuseFanclubColumnFilters[columnId] !== "") {
											abuseFanclubSearchString += columnId
													+ ":"
													+ abuseFanclubColumnFilters[columnId]
													+ ",";
										}
									}
									getAbuseFanclubGridData(0);
								}
							}

						});
		abuseFanclubGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'mediaUrl'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(abuseFanclubColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(abuseFanclubColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		abuseFanclubGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		abuseFanclubDataView.beginUpdate();
		abuseFanclubDataView.setItems(abuseFanclubData);
		//abuseFanclubDataView.setFilter(filter);
		abuseFanclubDataView.endUpdate();
		abuseFanclubDataView.syncGridSelection(abuseFanclubGrid, true);
		abuseFanclubGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAbuseFanclubPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = abuseFanclubGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('abuseFanclubGrid', colStr);
	}
	
	
	//Fan club post grid
	function getAbusePostGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageFanClubAbuse.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+abusePostSearchString+"&type=${type}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshAbusePostGridValues(jsonData.abuseData);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var abusePostCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var abusePostDataView;
	var abusePostGrid;
	var abusePostData = [];
	var abusePostGridPager;
	var abusePostSearchString='';
	var abusePostColumnFilters = {};
	var userAbusePostColumnsStr = '<%=session.getAttribute("abusePostGrid")%>';

	var userAbusePostColumns = [];
	var allAbusePostColumns = [
			 {
				id : "reportedByUserId",
				field : "reportedByUserId",
				name : "Reported By User Id",
				width : 80,
				sortable : true
			},{
				id : "reportedByEmail",
				field : "reportedByEmail",
				name : "Reported By Email",
				width : 80,
				sortable : true
			},{
				id : "reportedByPhone",
				field : "reportedByPhone",
				name : "Reported By Phone",
				width : 80,
				sortable : true
			},{
				id : "abuseType",
				field : "abuseType",
				name : "Abuse Type",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Reported Date",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "Post User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Post Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Post Phone",
				width : 80,
				sortable : true
			} ,{
				id : "title",
				field : "title",
				name : "Post Title",
				width : 80,
				sortable : true
			} ,{
				id : "count",
				field : "count",
				name : "Post Like Counts",
				width : 80,
				sortable : true
			} ,{
				id : "status",
				field : "status",
				name : "Blocked/Removed?",
				width : 80,
				sortable : true
			} ,{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			} ,{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userAbusePostColumnsStr != 'null' && userAbusePostColumnsStr != '') {
		columnOrder = userAbusePostColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allAbusePostColumns.length; j++) {
				if (columnWidth[0] == allAbusePostColumns[j].id) {
					userAbusePostColumns[i] = allAbusePostColumns[j];
					userAbusePostColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAbusePostColumns = allAbusePostColumns;
	}
	
	var abusePostOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var abusePostGridSortcol = "abusePostId";
	var abusePostGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function abusePostGridComparer(a, b) {
		var x = a[abusePostGridSortcol], y = b[abusePostGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshAbusePostGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		abusePostData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (abusePostData[i] = {});
				d["id"] = i;
				d["abusePostId"] = data.id;
				d["postId"] = data.postId;
				d["customerId"] = data.customerId;
				d["createdDate"] = data.createdDateStr;
				d["reportedByUserId"] = data.reportedByUserId;
				d["reportedByEmail"] = data.reportedByEmail;
				d["reportedByPhone"] = data.reportedByPhone;
				d["abuseType"] = data.abuseType;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["title"] = data.title;
				d["count"] = data.count;
				d["status"] = data.status;
				d["createdBy"] = data.createdUserId;
				d["updatedBy"] = data.updatedUserId;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		abusePostDataView = new Slick.Data.DataView();
		abusePostGrid = new Slick.Grid("#abusePost_grid", abusePostDataView,
				userAbusePostColumns, abusePostOptions);
		abusePostGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		abusePostGrid.setSelectionModel(new Slick.RowSelectionModel());
		abusePostGrid.registerPlugin(abusePostCheckboxSelector);
		
		abusePostGridPager = new Slick.Controls.Pager(abusePostDataView,
					abusePostGrid, $("#abusePost_pager"),
					pagingInfo);
		/* var abusePostGridColumnpicker = new Slick.Controls.ColumnPicker(
				allAbusePostColumns, abusePostGrid, abusePostOptions); */
		
		abusePostGrid.onSort.subscribe(function(e, args) {
			abusePostGridSortdir = args.sortAsc ? 1 : -1;
			abusePostGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				abusePostDataView.fastSort(abusePostGridSortcol, args.sortAsc);
			} else {
				abusePostDataView.sort(abusePostGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the abusePostGrid
		abusePostDataView.onRowCountChanged.subscribe(function(e, args) {
			abusePostGrid.updateRowCount();
			abusePostGrid.render();
		});
		abusePostDataView.onRowsChanged.subscribe(function(e, args) {
			abusePostGrid.invalidateRows(args.rows);
			abusePostGrid.render();
		});
		$(abusePostGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							abusePostSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								abusePostColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in abusePostColumnFilters) {
										if (columnId !== undefined
												&& abusePostColumnFilters[columnId] !== "") {
											abusePostSearchString += columnId
													+ ":"
													+ abusePostColumnFilters[columnId]
													+ ",";
										}
									}
									getAbusePostGridData(0);
								}
							}

						});
		abusePostGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(abusePostColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(abusePostColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		abusePostGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		abusePostDataView.beginUpdate();
		abusePostDataView.setItems(abusePostData);
		//abusePostDataView.setFilter(filter);
		abusePostDataView.endUpdate();
		abusePostDataView.syncGridSelection(abusePostGrid, true);
		abusePostGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAbusePostPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = abusePostGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('abusePostGrid', colStr);
	}
	
	
	
	
	//Fan club Event grid
	function getAbuseEventGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageFanClubAbuse.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+abuseEventSearchString+"&type=${type}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshAbuseEventGridValues(jsonData.abuseData);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var abuseEventCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var abuseEventDataView;
	var abuseEventGrid;
	var abuseEventData = [];
	var abuseEventGridPager;
	var abuseEventSearchString='';
	var abuseEventColumnFilters = {};
	var userAbuseEventColumnsStr = '<%=session.getAttribute("abuseEventGrid")%>';

	var userAbuseEventColumns = [];
	var allAbuseEventColumns = [
			 {
				id : "reportedByUserId",
				field : "reportedByUserId",
				name : "Reported By User Id",
				width : 80,
				sortable : true
			},{
				id : "reportedByEmail",
				field : "reportedByEmail",
				name : "Reported By Email",
				width : 80,
				sortable : true
			},{
				id : "reportedByPhone",
				field : "reportedByPhone",
				name : "Reported By Phone",
				width : 80,
				sortable : true
			},{
				id : "abuseType",
				field : "abuseType",
				name : "Abuse Type",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Reported Date",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "Event User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Event Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Event Phone",
				width : 80,
				sortable : true
			} ,{
				id : "title",
				field : "title",
				name : "Event Title",
				width : 80,
				sortable : true
			}  ,{
				id : "description",
				field : "description",
				name : "Event Venue",
				width : 80,
				sortable : true
			}  ,{
				id : "count",
				field : "count",
				name : "Interested count",
				width : 80,
				sortable : true
			} ,{
				id : "mediaUrl",
				field : "mediaUrl",
				name : "Event Image Preview",
				width : 80,
				formatter: previewEventImage
			},{
				id : "status",
				field : "status",
				name : "Blocked/Removed?",
				width : 80,
				sortable : true
			} ,{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			} ,{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userAbuseEventColumnsStr != 'null' && userAbuseEventColumnsStr != '') {
		columnOrder = userAbuseEventColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allAbuseEventColumns.length; j++) {
				if (columnWidth[0] == allAbuseEventColumns[j].id) {
					userAbuseEventColumns[i] = allAbuseEventColumns[j];
					userAbuseEventColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAbuseEventColumns = allAbuseEventColumns;
	}
	
	function previewEventImage(row,cell,value,columnDef,dataContext){
		if(dataContext.mediaUrl != null &&  dataContext.mediaUrl != 'null' && dataContext.mediaUrl != ''){
			var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.mediaUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
		
	}
	
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var abuseEventOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var abuseEventGridSortcol = "abuseEventId";
	var abuseEventGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function abuseEventGridComparer(a, b) {
		var x = a[abuseEventGridSortcol], y = b[abuseEventGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshAbuseEventGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		abuseEventData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (abuseEventData[i] = {});
				d["id"] = i;
				d["abuseEventId"] = data.id;
				d["eventId"] = data.eventId;
				d["customerId"] = data.customerId;
				d["createdDate"] = data.createdDateStr;
				d["reportedByUserId"] = data.reportedByUserId;
				d["reportedByEmail"] = data.reportedByEmail;
				d["reportedByPhone"] = data.reportedByPhone;
				d["abuseType"] = data.abuseType;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["title"] = data.title;
				d["description"] = data.description;
				d["count"] = data.count;
				d["mediaUrl"] = data.posterUrl;
				d["status"] = data.status;
				d["createdBy"] = data.createdUserId;
				d["updatedBy"] = data.updatedUserId;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		abuseEventDataView = new Slick.Data.DataView();
		abuseEventGrid = new Slick.Grid("#abuseEvent_grid", abuseEventDataView,
				userAbuseEventColumns, abuseEventOptions);
		abuseEventGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		abuseEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		abuseEventGrid.registerPlugin(abuseEventCheckboxSelector);
		
		abuseEventGridPager = new Slick.Controls.Pager(abuseEventDataView,
					abuseEventGrid, $("#abuseEvent_pager"),
					pagingInfo);
		/* var abuseEventGridColumnpicker = new Slick.Controls.ColumnPicker(
				allAbuseEventColumns, abuseEventGrid, abuseEventOptions); */
		
		abuseEventGrid.onSort.subscribe(function(e, args) {
			abuseEventGridSortdir = args.sortAsc ? 1 : -1;
			abuseEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				abuseEventDataView.fastSort(abuseEventGridSortcol, args.sortAsc);
			} else {
				abuseEventDataView.sort(abuseEventGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the abuseEventGrid
		abuseEventDataView.onRowCountChanged.subscribe(function(e, args) {
			abuseEventGrid.updateRowCount();
			abuseEventGrid.render();
		});
		abuseEventDataView.onRowsChanged.subscribe(function(e, args) {
			abuseEventGrid.invalidateRows(args.rows);
			abuseEventGrid.render();
		});
		$(abuseEventGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							abuseEventSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								abuseEventColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in abuseEventColumnFilters) {
										if (columnId !== undefined
												&& abuseEventColumnFilters[columnId] !== "") {
											abuseEventSearchString += columnId
													+ ":"
													+ abuseEventColumnFilters[columnId]
													+ ",";
										}
									}
									getAbuseEventGridData(0);
								}
							}

						});
		abuseEventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'mediaUrl'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(abuseEventColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(abuseEventColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		abuseEventGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		abuseEventDataView.beginUpdate();
		abuseEventDataView.setItems(abuseEventData);
		//abuseEventDataView.setFilter(filter);
		abuseEventDataView.endUpdate();
		abuseEventDataView.syncGridSelection(abuseEventGrid, true);
		abuseEventGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAbuseEventPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = abuseEventGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('abuseEventGrid', colStr);
	}
	
	
	
	//Fan club Video grid
	function getAbuseVideoGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageFanClubAbuse.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+abuseVideoSearchString+"&type=${type}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshAbuseVideoGridValues(jsonData.abuseData);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var abuseVideoCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var abuseVideoDataView;
	var abuseVideoGrid;
	var abuseVideoData = [];
	var abuseVideoGridPager;
	var abuseVideoSearchString='';
	var abuseVideoColumnFilters = {};
	var userAbuseVideoColumnsStr = '<%=session.getAttribute("abuseVideoGrid")%>';

	var userAbuseVideoColumns = [];
	var allAbuseVideoColumns = [
			 {
				id : "reportedByUserId",
				field : "reportedByUserId",
				name : "Reported By User Id",
				width : 80,
				sortable : true
			},{
				id : "reportedByEmail",
				field : "reportedByEmail",
				name : "Reported By Email",
				width : 80,
				sortable : true
			},{
				id : "reportedByPhone",
				field : "reportedByPhone",
				name : "Reported By Phone",
				width : 80,
				sortable : true
			},{
				id : "abuseType",
				field : "abuseType",
				name : "Abuse Type",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Reported Date",
				width : 80,
				sortable : true
			},{
				id : "userId",
				field : "userId",
				name : "Video User Id",
				width : 80,
				sortable : true
			},{
				id : "email",
				field : "email",
				name : "Video Email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				field : "phone",
				name : "Video Phone",
				width : 80,
				sortable : true
			} ,{
				id : "title",
				field : "title",
				name : "Video Title",
				width : 80,
				sortable : true
			}  ,{
				id : "description",
				field : "description",
				name : "Video Description",
				width : 80,
				sortable : true
			}  ,{
				id : "count",
				field : "count",
				name : "Video Like count",
				width : 80,
				sortable : true
			} ,{
				id : "mediaUrl",
				field : "mediaUrl",
				name : "Video Preview",
				width : 80,
				formatter: previewVideoFormatter
			},{
				id : "status",
				field : "status",
				name : "Blocked/Removed?",
				width : 80,
				sortable : true
			} ,{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			} ,{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userAbuseVideoColumnsStr != 'null' && userAbuseVideoColumnsStr != '') {
		columnOrder = userAbuseVideoColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allAbuseVideoColumns.length; j++) {
				if (columnWidth[0] == allAbuseVideoColumns[j].id) {
					userAbuseVideoColumns[i] = allAbuseVideoColumns[j];
					userAbuseVideoColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userAbuseVideoColumns = allAbuseVideoColumns;
	}
	
	function previewVideoFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.mediaUrl != null &&  dataContext.mediaUrl != 'null' && dataContext.mediaUrl != ''){
			var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.mediaUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
		
	}
	
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var abuseVideoOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var abuseVideoGridSortcol = "abuseVideoId";
	var abuseVideoGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function abuseVideoGridComparer(a, b) {
		var x = a[abuseVideoGridSortcol], y = b[abuseVideoGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshAbuseVideoGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		abuseVideoData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (abuseVideoData[i] = {});
				d["id"] = i;
				d["abuseVideoId"] = data.id;
				d["videoId"] = data.videoId;
				d["customerId"] = data.customerId;
				d["createdDate"] = data.createdDateStr;
				d["reportedByUserId"] = data.reportedByUserId;
				d["reportedByEmail"] = data.reportedByEmail;
				d["reportedByPhone"] = data.reportedByPhone;
				d["abuseType"] = data.abuseType;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["title"] = data.title;
				d["description"] = data.description;
				d["count"] = data.count;
				d["mediaUrl"] = data.videoUrl;
				d["status"] = data.status;
				d["createdBy"] = data.createdUserId;
				d["updatedBy"] = data.updatedUserId;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		abuseVideoDataView = new Slick.Data.DataView();
		abuseVideoGrid = new Slick.Grid("#abuseVideo_grid", abuseVideoDataView,
				userAbuseVideoColumns, abuseVideoOptions);
		abuseVideoGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		abuseVideoGrid.setSelectionModel(new Slick.RowSelectionModel());
		abuseVideoGrid.registerPlugin(abuseVideoCheckboxSelector);
		
		abuseVideoGridPager = new Slick.Controls.Pager(abuseVideoDataView,
					abuseVideoGrid, $("#abuseVideo_pager"),
					pagingInfo);
		/* var abuseVideoGridColumnpicker = new Slick.Controls.ColumnPicker(
				allAbuseVideoColumns, abuseVideoGrid, abuseVideoOptions); */
		
		abuseVideoGrid.onSort.subscribe(function(e, args) {
			abuseVideoGridSortdir = args.sortAsc ? 1 : -1;
			abuseVideoGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				abuseVideoDataView.fastSort(abuseVideoGridSortcol, args.sortAsc);
			} else {
				abuseVideoDataView.sort(abuseVideoGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the abuseVideoGrid
		abuseVideoDataView.onRowCountChanged.subscribe(function(e, args) {
			abuseVideoGrid.updateRowCount();
			abuseVideoGrid.render();
		});
		abuseVideoDataView.onRowsChanged.subscribe(function(e, args) {
			abuseVideoGrid.invalidateRows(args.rows);
			abuseVideoGrid.render();
		});
		$(abuseVideoGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							abuseVideoSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								abuseVideoColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in abuseVideoColumnFilters) {
										if (columnId !== undefined
												&& abuseVideoColumnFilters[columnId] !== "") {
											abuseVideoSearchString += columnId
													+ ":"
													+ abuseVideoColumnFilters[columnId]
													+ ",";
										}
									}
									getAbuseVideoGridData(0);
								}
							}

						});
		abuseVideoGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'mediaUrl'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(abuseVideoColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(abuseVideoColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		abuseVideoGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		abuseVideoDataView.beginUpdate();
		abuseVideoDataView.setItems(abuseVideoData);
		//abuseVideoDataView.setFilter(filter);
		abuseVideoDataView.endUpdate();
		abuseVideoDataView.syncGridSelection(abuseVideoGrid, true);
		abuseVideoGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserAbuseVideoPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = abuseVideoGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('abuseVideoGrid', colStr);
	}
	
	
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		<c:choose>
			<c:when test="${type == 'COMMENT'}">
				refreshAbuseCommentGridValues(${abuseData});
				$('#abuseComment_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAbuseCommentPreference()'>");
			</c:when>
			<c:when test="${type == 'FANCLUB'}">
				refreshAbuseFanclubGridValues(${abuseData});
				$('#abuseFanclub_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAbuseFanclubPreference()'>");
			</c:when>
			<c:when test="${type == 'POST'}">
				refreshAbusePostGridValues(${abuseData});
				$('#abusePost_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAbusePostPreference()'>");
			</c:when> 
			<c:when test="${type == 'EVENT'}">
				refreshAbuseEventGridValues(${abuseData});
				$('#abuseEvent_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAbuseEventPreference()'>");
			</c:when> 
			<c:when test="${type == 'VIDEO'}">
				refreshAbuseVideoGridValues(${abuseData});
				$('#abuseVideo_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserAbuseVideoPreference()'>");
			</c:when> 
	</c:choose>
		enableMenu();
	};
		
</script>