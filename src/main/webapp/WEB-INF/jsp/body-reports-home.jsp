<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<style>
.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}

.dropdown {
  position: absolute;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
</style>
<script type="text/javascript">
var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true
	});
	
});

function callAPIReport(reportURL){
	window.location.href = apiServerUrl+"/Reports/"+reportURL;
}

function callAPIReportCharts(reportURL){
	$("div#divLoading").addClass('show');
	var url = "${pageContext.request.contextPath}/Reports/"+reportURL;
	window.location.href = url;
}


function openContestReportModal(reportName){
	$('#contestReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
}

function openContestGraphReportModal(reportName, graphReportName){
	$('#contestGraphReportModal').modal('show');
	$('#contestReportNameHdr').text(reportName);
	$('#contestReportUrl').val(reportName);
	$('#contestGraphReportUrl').val(graphReportName);
}
function openCustChainReportModal(){
	$('#custChainReportModal').modal('show'); 
}

function generateReport(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var reportURL = $('#contestReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = apiServerUrl+"Reports/"+reportURL;
}

function generateGraphicalReport(){
	var fromDate = $('#fDate').val();
	var toDate = $('#tDate').val();
	var reportURL = $('#contestGraphReportUrl').val();
	
	reportURL += "?fromDate="+fromDate+"&toDate="+toDate;
	
	window.location.href = "${pageContext.request.contextPath}/Reports/"+reportURL;
}
function generateChainReport(){
	var userId = $('#userId').val(); 
	if(null == userId || userId == "" || userId == " "){
		$('#userIdErrId').html("Please Enter Valid User ID."); 
		return;
	}else{
		$('#userIdErrId').html(""); 
		window.location.href = "${pageContext.request.contextPath}/Reports/DownloadCustomerStatsReport?userId="+userId;
	}
}

function generateBotReport(){ 
	window.location.href = "${pageContext.request.contextPath}/Reports/DownloadBotReport";
}

function showUploadPOModal(){
	$('#uploadPOModal').modal('show');
}

function uploadPOFile(){
	var validFilesTypes = ["xls", "xlsx"];
	var file = $('#poFile').val();
	if(file==null || file== ''){
		jAlert("Please Select file to upload ");
		return;
	}
    var ext = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
	var isValidFile = false;
	for (var i = 0; i < validFilesTypes.length; i++) {
        if (ext == validFilesTypes[i]) {
            isValidFile = true;
            break;
        }
    }
	if(!isValidFile){
		jAlert("Please select valid Excel file.");
		return;
	}
	//$('#uploadPOForm').attr('action',apiServerUrl + "Reports/UploadPOFile");
	//$('#uploadPOForm').submit();
	
	$('#uploadPOModal').modal('hide');
	 var form = $('#uploadPOForm')[0];
     var data = new FormData(form);
	
	 $.ajax({
		url : apiServerUrl + "Reports/UploadPOFile",
		type : "post",
		enctype:"multipart/form-data",
		 processData : false,
         contentType : false,
         cache : false,
		data : data,
		success : function(res){
			$('#uploadPOModal').modal('hide');
			var jsonData  = JSON.parse(JSON.stringify(res))
			jAlert(jsonData.msg);
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});  
}

function showUploaduserWisePOModal(){
	$('#uploadUserWisePOModal').modal('show');
}

function uploadUserWisePOFile(){
	var validFilesTypes = ["xls", "xlsx"];
	var file = $('#poRefundFile').val();
	if(file==null || file== ''){
		jAlert("Please Select file to upload ");
		return;
	}
    var ext = file.substring(file.lastIndexOf('.') + 1).toLowerCase();
	var isValidFile = false;
	for (var i = 0; i < validFilesTypes.length; i++) {
        if (ext == validFilesTypes[i]) {
            isValidFile = true;
            break;
        }
    }
	if(!isValidFile){
		jAlert("Please select valid Excel file.");
		return;
	}
	
	$('#uploadUserWisePOModal').modal('hide');
	 var form = $('#uploadUserWisePOForm')[0];
     var data = new FormData(form);
	
	 $.ajax({
		url : apiServerUrl + "Reports/UploadPORefundFile",
		type : "post",
		enctype:"multipart/form-data",
		 processData : false,
         contentType : false,
         cache : false,
		data : data,
		success : function(res){
			$('#uploadUserWisePOModal').modal('hide');
			var jsonData  = JSON.parse(JSON.stringify(res))
			jAlert(jsonData.msg);
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});  
}

function selectReportType(reportName) {
  document.getElementById(reportName).classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Reports</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Reports</a></li>
						<li><i class="fa fa-laptop"></i>Reports Home</li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
</div>

<div class="row">
      <div class="col-lg-4">
          <section class="panel">
              <header class="panel-heading">
                 <strong>Reports</strong> 
              </header>
              <div class="list-group">
               	<a class="list-group-item " href="javascript:showUploadPOModal();">
                      Upload RTF Purchase Refunds File
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('ticketMasterRefundsAmt')">
                      Download RTF Purchase Refunds File
                  </a>
                  <a class="list-group-item " href="javascript:showUploaduserWisePOModal();">
                      Upload RTF User-Wise Purchase Refunds File
                  </a>
             	   <a class="list-group-item " href="EventsStatisticsReport">
                      RTF Events Statistics
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadEventsNotInRTFReport')">
                      Events in tmat but not in RTF
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadTNDEventSalesReport')">
                      Events Last 3 Days Sales
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFNext15DaysUnsentReport')">
                      RTF Next 15 Days Unsent Report
                  </a>
                   <a class="list-group-item " href="LoadRTFZonesNotHavingColorsReport">
                      RTF Map Not Having Zone Colors
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadMissingSVGandCSVZones')">
                       SVG and CSV Missing Zones
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFUnFilledShortsWithPossibleLongInventoryReport')">
                      RTF UnFilled Shorts With Possible Long Inventory Reports
                  </a>
                  <a class="list-group-item " href="UnsoldTickets">
                      RTF UnSold Tickets
                  </a>
                  <a class="list-group-item " href="PoInvoiceReport">
                      RTF PO/Invoice Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFSalesReport')">
                      RTF Sales Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFSalesReportByReferral')">
                      RTF Sales Report By Referral
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFFilledReport')">
                      RTF Filled Reports
                  </a>
                   <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFUnFilledReport')">
                      RTF UnFilled Reports
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadPointsSummeryReport')">
                      Points Summary Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFCustomerOrderDetailsReport')">
                      Customer Order Details Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFCustomerOrdersDuplicateReport')">
                      Repeated Orders Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFChannelWiseCustomerOrdersReport')">
                      Channel Wise Customer Orders
                  </a>
                  <a class="list-group-item " href="LoadRTFTMATSeatAllocationReport">
                      RTF TMAT Seat Allocation Report
                  </a>
                  <!-- <a class="list-group-item " href="DownloadRTFProfitAndLossReport">
                      RTF Profit/Loss Report
                  </a> -->
                   <a class="list-group-item " href="javascript:callAPIReport('DownloadClientMasterReport')">
                      Client Master Report
                  </a>
                 <a class="list-group-item " href="javascript:callAPIReport('DownloadUnsubscribedClientMasterReport')">
                      Unsubscribed Client Master Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadPurchasedMasterReport')">
                      Purchased Master Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFRegisteredButnotPurchasedReport')">
                      Registered But Not Purchased Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFCombinedPurchasedNonPurchasedReport')">
                      Combined Purchased NonPurchased Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFLoyalFanReport')">
                      LoyalFan Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFLoyalFanPurchasedReport')">
                      LoyalFan Purchased Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFLoyalFanNonPurchasedReport')">
                      LoyalFan Non Purchased Report
                  </a>
                  <a class="list-group-item " href="LoadRTFCustomerSpentReport">
                      Customer Spent Report
                  </a>
              </div>
          </section>
      </div>
      <div class="col-lg-4">
          <section class="panel">
          <!--  Heading changed and rearranged the Reports -- Shiva Modified on 17 Oct 2019 -->
              <header class="panel-heading">
                 <strong>Contest Detailed Reports</strong>
              </header>
              <div class="list-group">
                  <a class="list-group-item " href="javascript:openContestGraphReportModal('DownloadContestTicketCostReport','GetTicketCostReport')">
                     Ticket Cost Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadContestWinnerInvoiceReport')">
                     Winner invoice Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadCustomerRewardDollarsBreakdownDetailedReport')">
                      Customer Reward Dollars Breakdown Detailed Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomerEarnedContestRewardDollarsReport')">
                      Customer Earned Contest Reward Dollars Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadCustomerPurchaseRewardDollarsReport')">
                     Customer Purchase on Reward Dollars Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadRewardDollarSpentOnContestAndCustomer')">		<!-- Shiva Modified -->
                     Reward Dollar Spent on Contest and Customer Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomerRewardDollarsLiabilityReport');">
                     Customer Reward Dollars (Liability) Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadRegisteredCustomersReport');">
                     Registered Customer Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomerWhoPlayedContestReport');">
                     Registered Customer Who Played Contest Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadRegisteredCustomersWhoDidNotPlayedContestReport');">
                     Registered Customer But Not Played Contest Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('GetNoOfGamesPlayedReport');">
                      Customer No Of Games Played Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestGraphReportModal('GetBotsReport','GetBotsCustomerReport')">
                      Bots Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadRefereeReferralsDailyReport')">
                      Referee / Referrals Daily Report
                  </a>
                  <a class="list-group-item " href="javascript:openCustChainReportModal()">
                      Customer Referral Chain Report
                  </a>
                  <!-- Shiva Added New Report "Customers Using Referral Code" on 17 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomersUsingReferralCodesReport')">
                      Customers Using Referral Code DFSKARMA & RTFBWAY
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomerLivesEarnedReport')">
                      Customer Lives Earned Report
                  </a>
                  <a class="list-group-item " href="javascript:selectReportType('superFanStars');">
                      Customer Available SuperFan Stars Report
                  </a>
                          <div id="superFanStars" class="dropdown-content">
                                <a href="javascript:callAPIReport('GetCustomerSuperFanReport');">Download Excel Report</a>
                                <a href="javascript:callAPIReportCharts('GetSuperFanStarsReport');">View Graphical Chart</a>
                          </div>
                  <!-- Shiva Added New Report "Fantasy Football Contest Winners Report" on 14 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadFantasyFootballContestWinnersReport')">
                      Fantasy Football Contest Winners Report
                  </a>
                  <!-- Shiva Added New Report "Outstanding Contest Orders to be fulfilled within 5Days" on 14 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadOutstandingContestOrdersToBeFulfilledReport')">
                      Outstanding Contest Orders to be Fulfilled within 5Days
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadContestDetailedTrackingReport')">
                      Contest Detailed Tracking Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestGraphReportModal('GetPollingReport','GetPollingStatsReport')">
                      Polling and TV Video Played Details and Stats Report
                  </a>
                  <!-- Shiva Added New Report "RTF Gift Card Purchased & Winners Report" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadGiftCardPurchasedWinnersReport')">
                      Gift Card Purchased & Winners Report
                  </a>
                  <!-- Shiva Added New Report "Grand, Mega & MINI Jackpot Ticket Winner Count" on 23 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadGiftCardOrdersOutstandingPendingDetailedReport');">
                      Gift Card Orders Outstanding & Pending Detailed Report
                  </a>
                  
                  <!--  <a class="list-group-item " href="javascript:openContestReportModal('DownloadContestReport')">
                      Contest Report
                  </a> -->
                  <!--  <a class="list-group-item " href="javascript:openContestReportModal('DownloadContestCustomerRefferalPercentageReport')">
                     Contest Customer Referral Percentage Report
                  </a> -->
                  <!-- <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomerReferredByReferredToReport')">
                      Referred By - Reffered to Report
                  </a> -->

              </div>
          </section>
      </div>
      <c:if test="${sessionScope.isAdmin == true && sessionScope.isContest == true}">
       <div class="col-lg-4">
          <section class="panel">
          <!--  Heading changed and rearranged the Reports -- Shiva Modified on 17 Oct 2019 -->
              <header class="panel-heading">
                 <strong>Contest Statistics Reports</strong>
              </header>
              <div class="list-group">
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadContestCustomerRefferalAverageReport')">
                     Contest Customer Referral Average Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadContestCustomerRefferalAverageExcludingBotsReport')">
                     Contest Customer Referral Average Report - Excluding Bots
                  </a>
                  <a class="list-group-item " href="javascript:openContestGraphReportModal('DownloadContestQuestionwiseUserAnswerReport','GetQueWiseUserAnswerCountReport')">
                     Contest Question Wise User Answer Count Report.
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadAvgQuestionAnsweredByCustomerReport')">
                     Average No of Question Answered by Customers Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadNoOfGamePlayedByCustomerReport')">
                     Average No of Games Played by Customers Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadContestWiseNewCustomersReport');">
                     Contest Wise First Time Participants Count Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('GetMonthWiseRegisteredCustomerCount');">
                      Monthly Registered Customer Count Report  
                  </a>
                  <!-- Shiva Added New Report "Monthly Customer Participants Count Statistics" on 15 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadMonthlyCustomerParticipantsCountStatistics');">
                      Monthly Customer Participants Count Statistics Report
                  </a>
                  <!-- Shiva Added New Report "Monthly Customer Participants Count Breakdown Statistics" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadMonthlyCustomerParticipantsCountBreakdownStatistics');">
                      Monthly Customer Participants Count Breakdown Statistics Report
                  </a>
                  <!-- Shiva Added New Report "RTF Customer & Contest Statistics for Business" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFCustomerContestStatisticsforBusiness');">
                      RTF Customer & Contest Statistics Report for Business
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReport('GetActiveInActiveCustomerCount');">
                      Active/In Active Customer Counts Report
                  </a>
                  <!-- Shiva Added New Report "Customer Time Spent On Each Contest" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadCustomerTimeSpentOnEachContest');">
                      Customer Time Spent On Each Contest Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('GetAverageTimeTakenByContests');">
                      Customer Average Time Taken per Contest Report
                  </a>
                  <!-- Shiva Added New Report "Customer Average Time Taken per Contest Statistics" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadRTFCustomerAverageTimeTakenperContestStatistics');">
                      Customer Average Time Taken per Contest Statistics Report
                  </a>
                  <!-- Shiva Added New Report "Customer Average Time Accessing RTF Trivia" on 15 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadCustomerAverageTimeAccessingRTFTrivia');">
                      Customer Average Time Accessing RTF Trivia Report
                  </a>
                  <a class="list-group-item " href="javascript:openContestReportModal('GetFantasyGameCustomerCount');">
                      Fantasy Contest Customer Count Report
                  </a>
                  <!-- Shiva Added New Report "Grand, Mega & MINI Jackpot Ticket Winner Count" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:openContestReportModal('DownloadGrandMegaMINIJackpotTicketWinnerCount');">
                      Grand, Mega & MINI Jackpot Ticket Winner Count Report
                  </a>
                  <!-- Shiva Added New Report "Grand, Mega & MINI Jackpot Ticket Winner Count" on 23 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadGiftCardOrdersOutstandingPendingCount');">
                      Gift Card Orders Outstanding & Pending Count Report
                  </a>
                  <!-- Shiva Added New Report "Polling Statistics" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadPollingStatistics');">
                      Polling Statistics Report
                  </a>
                  <!-- Shiva Added New Report "RTF TV Statistics" on 16 Oct 2019 -->
                  <a class="list-group-item " href="javascript:callAPIReport('DownloadRTFTVStatistics');">
                      RTF TV Statistics Report
                  </a>
                  <a class="list-group-item " href="javascript:callAPIReportCharts('GetContestParticipantReport');">
                      Contest No Of Participant Report
                  </a>
                  <!-- <a class="list-group-item " href="javascript:selectReportType('Bots');">
                     Bots Report
                  </a>
                  <div id="Bots" class="dropdown-content">
                        <a href="javascript:callAPIReport('GetBotsReport');">Download Excel Report</a>
                        <a href="javascript:callAPIReportCharts('GetBotsCustomerReport');">View Graphical Chart</a>
                  </div> -->
                  <!-- <a class="list-group-item " href="javascript:selectReportType('pollingVideo');">
                      Polling and TV Video Played Details and Stats Report
                  </a>
                          <div id="pollingVideo" class="dropdown-content">
                                <a href="javascript:callAPIReport('GetPollingReport');">Download Excel Report</a>
                                <a href="javascript:callAPIReportCharts('GetPollingStatsReport');">View Graphical Chart</a>
                          </div> -->
              </div>
          </section>
      </div
      </c:if>
</div>
<div align="center" aria-hidden="true" aria-labelledby="contestReportModal" role="dialog" tabindex="-1" id="contestReportModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="contestReportNameHdr"></h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-4 col-xs-4">
						<label>From Date
						</label> <input class="form-control" type="text" id="fromDate" name="fromDate" value="${fromDate}">
					</div>	
				</div>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-4 col-xs-4">
						<label>To Date
						</label> <input class="form-control" type="text" id="toDate" name="toDate" value="${toDate}">
						<input class="form-control" type="hidden" id="contestReportUrl" name="contestReportUrl">
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="generateContestReport" type="button" onclick="generateReport()">Generate Report</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<div align="center" aria-hidden="true" aria-labelledby="contestReportModal" role="dialog" tabindex="-1" id="custChainReportModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		
		<div class="modal-content full-width">
		 
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Customer Referral Chain Report</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-4 col-xs-4">
						<label>User ID
						</label> <input class="form-control" type="text" id="userId" name="userId" >
						<span id="userIdErrId" style="color: red;font-weight: bold;"></span>
						 
						
					</div>	
				</div>
			</div> 
			 
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="generateCustChainReportReport" type="button" onclick="generateChainReport()">Get Report</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<div align="center" aria-hidden="true" aria-labelledby="contestReportModal" role="dialog" tabindex="-1" id="contestGraphReportModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="contestReportNameHdr"></h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-4 col-xs-4">
						<label>From Date
						</label> <input class="form-control" type="text" id="fDate" name="fDate" value="${fromDate}">
					</div>	
				</div>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-4 col-xs-4">
						<label>To Date
						</label> <input class="form-control" type="text" id="tDate" name="tDate" value="${toDate}">
						<input class="form-control" type="hidden" id="contestReportUrl" name="contestReportUrl">
						<input class="form-control" type="hidden" id="contestGraphReportUrl" name="contestGraphReportUrl">
					</div>	
				</div>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="generateGraphContestReport" type="button" onclick="generateReport()">Generate Excel Report</button>
				<button class="btn btn-primary" id="generateContestReport" type="button" onclick="generateGraphicalReport()">View Graphical Report</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<form class="form-horizontal" id="uploadPOForm" enctype="multipart/form-data" action ="${pageContext.request.contextPath}/UploadPOFile" method="post" >
	<div class="modal fade" id="uploadPOModal" tabindex="-1" role="dialog" aria-labelledby="uploadPOModal" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content full-width">
               <div class="modal-header full-width">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Upload PO file</h4>
               </div>
			   <div class="modal-body full-width">
					<div class="full-width tab-fields">
						<div class="form-group col-md-4 col-sm-6 col-xs-12">
							<label>Upload file</label> 
							<input class="form-control" id="poFile" name="poFile" type="file" />
						</div>
					</div>
					<div class="full-width text-center">
					   <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					   <button class="btn btn-success" type="button" onclick="uploadPOFile();" >Upload</button>
				   </div>
			   </div>
          	</div>
        </div>
     </div>
</form>

<form class="form-horizontal" id="uploadUserWisePOForm" enctype="multipart/form-data" action ="${pageContext.request.contextPath}/UploadPORefundFile" method="post" >
	<div class="modal fade" id="uploadUserWisePOModal" tabindex="-1" role="dialog" aria-labelledby="uploadUserWisePOModal" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content full-width">
               <div class="modal-header full-width">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Upload User wise PO Refund file</h4>
               </div>
			   <div class="modal-body full-width">
					<div class="full-width tab-fields">
						<div class="form-group col-md-4 col-sm-6 col-xs-12">
							<label>Upload file</label> 
							<input class="form-control" id="poRefundFile" name="poRefundFile" type="file" />
						</div>
					</div>
					<div class="full-width text-center">
					   <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					   <button class="btn btn-success" type="button" onclick="uploadUserWisePOFile();" >Upload</button>
				   </div>
			   </div>
          	</div>
        </div>
     </div>
</form>

