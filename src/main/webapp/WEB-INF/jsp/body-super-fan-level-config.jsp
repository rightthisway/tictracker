<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(superfanLevelGrid != null && superfanLevelGrid != undefined){
			superfanLevelGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${status == 'ACTIVE'}">	
			$('#activeLevel').addClass('active');
			$('#activeLevelTab').addClass('active');
		</c:when>
		<c:when test="${status == 'DELETED'}">
			$('#inactiveLevel').addClass('active');
			$('#inactiveLevelTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeLevel1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#inactiveLevel1").click(function(){
		callTabOnChange('DELETED');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/SuperFanLevelConfig?status="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling</a></li>
			<li><i class="fa fa-laptop"></i>Manage Superfan Levels</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeLevelTab" class=""><a id="activeLevel1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeLevel">Active Superfan Levels</a></li>
				<li id="inactiveLevelTab" class=""><a id="inactiveLevel1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveLevel">Deleted Superfan Levels</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeLevel" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetLevelModal();">Add New Superfan Level</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editLevel()">Edit Superfan Level</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteLevel()">Delete Superfan Level</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="superfanLevelGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Superfan Levels</label>
							<div class="pull-right">
								<a href="javascript:superfanLevelExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:superfanLevelResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="superfanLevel_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="superfanLevel_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="inactiveLevel" class="tab-pane">
			<c:if test="${status =='DELETED'}">	
				<div class="full-width full-width-btn mb-20">
					<!-- <button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPointLevel()">Edit Reward Config</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePointLevel()">Delete Reward Config</button> -->
				</div>
				<br />
				<br />
				<div style="position: relative" id="superfanLevelGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Superfan Levels</label>
							<div class="pull-right">
								<a href="javascript:superfanLevelExportToExcel('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:superfanLevelResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="superfanLevel_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="superfanLevel_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Reward Config Bank -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="qBModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Superfan Level</h4>
			</div>
			<div class="modal-body full-width">
				<form name="superfanLevelForm" id="superfanLevelForm" method="post">
					<input type="hidden" id="levelId" name="levelId" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label>Level No<span class="required">*</span>
							</label> <input class="form-control" type="text" id="levelNo" name="levelNo">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Level Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Stars From<span class="required">*</span>
							</label> <input class="form-control" type="text" id="starsFrom" name="starsFrom">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Stars To<span class="required">*</span>
							</label> <input class="form-control" type="text" id="starsTo" name="starsTo">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qBSaveBtn" type="button" onclick="saveLevel('save')">Save</button>
				<button class="btn btn-primary" id="qBUpdateBtn" type="button" onclick="saveLevel('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Reward Level Bank end here  -->
<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'superfanLevel_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getSuperfanLevelGridData(pageNo);
		}
	}
	
	//Reward Level Bank Grid
	
	function getSuperfanLevelGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/SuperFanLevelConfig.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+superfanLevelSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshSuperfanLevelGridValues(jsonData.levels);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function superfanLevelExportToExcel(status){
		var appendData = "headerFilter="+superfanLevelSearchString+"&status="+status;
	    var url =apiServerUrl+"SuperFanLevelExport?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function superfanLevelResetFilters(){
		superfanLevelSearchString='';
		superfanLevelColumnFilters = {};
		getSuperfanLevelGridData(0);
	}
	
	var superfanLevelCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var superfanLevelDataView;
	var superfanLevelGrid;
	var superfanLevelData = [];
	var superfanLevelGridPager;
	var superfanLevelSearchString='';
	var superfanLevelColumnFilters = {};
	var userSuperfanLevelColumnsStr = '<%=session.getAttribute("superfanLevelGrid")%>';

	var userSuperfanLevelColumns = [];
	var allSuperfanLevelColumns = [
			 {
				id : "title",
				field : "title",
				name : "Title",
				width : 80,
				sortable : true
			},{
				id : "levelNo",
				field : "levelNo",
				name : "Level No.",
				width : 80,
				sortable : true
			},{
				id : "starsFrom",
				field : "starsFrom",
				name : "Stars From",
				width : 80,
				sortable : true
			},{
				id : "starsTo",
				field : "starsTo",
				name : "Stars To",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userSuperfanLevelColumnsStr != 'null' && userSuperfanLevelColumnsStr != '') {
		columnOrder = userSuperfanLevelColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allSuperfanLevelColumns.length; j++) {
				if (columnWidth[0] == allSuperfanLevelColumns[j].id) {
					userSuperfanLevelColumns[i] = allSuperfanLevelColumns[j];
					userSuperfanLevelColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userSuperfanLevelColumns = allSuperfanLevelColumns;
	}
	
	var superfanLevelOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var superfanLevelGridSortcol = "superfanLevelId";
	var superfanLevelGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function superfanLevelGridComparer(a, b) {
		var x = a[superfanLevelGridSortcol], y = b[superfanLevelGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshSuperfanLevelGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		superfanLevelData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (superfanLevelData[i] = {});
				d["id"] = i;
				d["levelId"] = data.id;
				d["title"] = data.title;
				d["levelNo"] = data.levelNo;
				d["starsFrom"] = data.starsFrom;
				d["starsTo"] = data.starsTo;
				d["status"] = data.status ;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		superfanLevelDataView = new Slick.Data.DataView();
		superfanLevelGrid = new Slick.Grid("#superfanLevel_grid", superfanLevelDataView,
				userSuperfanLevelColumns, superfanLevelOptions);
		superfanLevelGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		superfanLevelGrid.setSelectionModel(new Slick.RowSelectionModel());
		superfanLevelGrid.registerPlugin(superfanLevelCheckboxSelector);
		
		superfanLevelGridPager = new Slick.Controls.Pager(superfanLevelDataView,
					superfanLevelGrid, $("#superfanLevel_pager"),
					pagingInfo);
		/* var superfanLevelGridColumnpicker = new Slick.Controls.ColumnPicker(
				allSuperfanLevelColumns, superfanLevelGrid, superfanLevelOptions); */
		
		superfanLevelGrid.onSort.subscribe(function(e, args) {
			superfanLevelGridSortdir = args.sortAsc ? 1 : -1;
			superfanLevelGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				superfanLevelDataView.fastSort(superfanLevelGridSortcol, args.sortAsc);
			} else {
				superfanLevelDataView.sort(superfanLevelGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the superfanLevelGrid
		superfanLevelDataView.onRowCountChanged.subscribe(function(e, args) {
			superfanLevelGrid.updateRowCount();
			superfanLevelGrid.render();
		});
		superfanLevelDataView.onRowsChanged.subscribe(function(e, args) {
			superfanLevelGrid.invalidateRows(args.rows);
			superfanLevelGrid.render();
		});
		$(superfanLevelGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							superfanLevelSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								superfanLevelColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in superfanLevelColumnFilters) {
										if (columnId !== undefined
												&& superfanLevelColumnFilters[columnId] !== "") {
											superfanLevelSearchString += columnId
													+ ":"
													+ superfanLevelColumnFilters[columnId]
													+ ",";
										}
									}
									getSuperfanLevelGridData(0);
								}
							}

						});
		superfanLevelGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(superfanLevelColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(superfanLevelColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		superfanLevelGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		superfanLevelDataView.beginUpdate();
		superfanLevelDataView.setItems(superfanLevelData);
		//superfanLevelDataView.setFilter(filter);
		superfanLevelDataView.endUpdate();
		superfanLevelDataView.syncGridSelection(superfanLevelGrid, true);
		superfanLevelGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserSuperfanLevelPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = superfanLevelGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('superfanLevelGrid', colStr);
	}
	
	// Add Reward Level Bank	
	function resetLevelModal(){		
		$('#qBModal').modal('show');
		$('#levelId').val('');
		$('#levelNo').val('');
		$('#title').val('');
		$('#levelNo').attr('readonly',false);
		$('#starsFom').val('');
		$('#qBSaveBtn').show();
		$('#qBUpdateBtn').hide();	
	}

	function saveLevel(action){
		
		var levelNo = $('#levelNo').val();
		var starsFom = $('#starsFom').val();
		
		if(levelNo == ''){
			jAlert("Level No. is Mandatory.");
			return;
		}
		if(starsFom == ''){
			jAlert("Min. required stars is mendatory.");
			return;
		}
		var requestUrl = "${pageContext.request.contextPath}/UpdateSuperFanLevelConfig.json";
		var dataString = "";
		if(action == 'save'){		
			dataString  = $('#superfanLevelForm').serialize()+"&action=SAVE&pageNo=0";
		}else if(action == 'update'){
			dataString = $('#superfanLevelForm').serialize()+"&action=UPDATE&pageNo=0";
		}
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			data: dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#qBModal').modal('hide');
					pagingInfo = jsonData.pagingInfo;
					superfanLevelColumnFilters = {};
					refreshSuperfanLevelGridValues(jsonData.levels);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit Reward Level Bank
	function editLevel(){
		var tempSuperfanLevelRowIndex = superfanLevelGrid.getSelectedRows([0])[0];
		if (tempSuperfanLevelRowIndex == null) {
			jAlert("Plese select superfan level record to Edit", "info");
			return false;
		}else {
			var levelId = superfanLevelGrid.getDataItem(tempSuperfanLevelRowIndex).levelId;
			getEditSuperfanLevel(levelId);
		}
	}
	
	function getEditSuperfanLevel(levelId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateSuperFanLevelConfig.json",
			type : "post",
			dataType: "json",
			data: "levelId="+levelId+"&action=EDIT&pageNo=0",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#qBModal').modal('show');
					setEditSuperfanLevel(jsonData.level);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditSuperfanLevel(data){
		$('#qBSaveBtn').hide();
		$('#qBUpdateBtn').show();
		$('#levelId').val(data.id);
		$('#levelNo').attr('readonly',true);
		$('#levelNo').val(data.levelNo);
		$('#title').val(data.title);
		$('#starsFrom').val(data.starsFrom);
		$('#starsTo').val(data.starsTo);
		
	}
	
	//Delete Reward Level Bank
	function deleteLevel(){
		var tempSuperfanLevelRowIndex = superfanLevelGrid.getSelectedRows([0])[0];
		if (tempSuperfanLevelRowIndex == null) {
			jAlert("Plese select superfan level to delete", "info");
			return false;
		}else {
			var levelId = superfanLevelGrid.getDataItem(tempSuperfanLevelRowIndex).levelId;
			getDeleteSuperfanLevel(levelId);
		}
	}
	
	
	function getDeleteSuperfanLevel(levelId){
		jConfirm("Are you sure to delete selected superfan level ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateSuperFanLevelConfig.json",
						type : "post",
						dataType: "json",
						data : "levelId="+levelId+"&action=DELETE&pageNo=0",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.pagingInfo;
								superfanLevelColumnFilters = {};
								refreshSuperfanLevelGridValues(jsonData.levels);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
								return;
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		refreshSuperfanLevelGridValues(${levels});
		$('#superfanLevel_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserSuperfanLevelPreference()'>");
		
		enableMenu();
	};
		
</script>