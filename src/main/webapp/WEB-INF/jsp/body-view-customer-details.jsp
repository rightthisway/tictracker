<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script src="../resources/js/app/viewCustomerDetail.js"></script>
<script src="../resources/js/app/customerRewardPoints.js"></script>
<script src="../resources/js/app/customerInvoiceTicket.js"></script>
<script src="../resources/js/app/getCityStateCountry.js"></script>

<script>
$(document).ready(function(){
//$("div#divLoading").addClass('show');
	
	$('.autoArtist').autocomplete("${pageContext.request.contextPath}/AutoCompleteCustomerArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			
		}
	}).result(function (event,row,formatted){
			$('#taglabel').text(row[2]);
			$('#loyalFanArtist').val('');
			$('#loyalFanArtists').val(row[1]);
	});
	
	$('.autoCityState').autocomplete("${pageContext.request.contextPath}/AutoCompleteCityStateCountry", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[6] ;
			
		}
	}).result(function (event,row,formatted){
			$('#taglabel1').text(row[7]);
			$('#loyalFanCity').val('');
			$('#loyalFanSelectionValue').val(row[1]);
			$('#loyalFanCityName').val(row[2]);
			$('#loyalFanStateName').val(row[3]);
			$('#loyalFanCountryName').val(row[4]);
			$('#loyalFanZipCode').val(row[5]);
	});
	
	$('.autoEvent').autocomplete("${pageContext.request.contextPath}/AutoCompleteEventDetails", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0] == 'EVENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[3] ;
			}
			
		}
	}).result(function (event,row,formatted){
			if(row[0] == "EVENT"){				
				$('#tagFavoriteEvent').text(row[3]);
				$('#favoriteEvent').val('');
				$('#favoriteEventId').val(row[1]);
			}
	});
	
	$("#vCust_sameBillingEmail").change(function(){
		 if($("#vCust_sameBillingEmail").is(':checked')){
		 	var email = $('#vCust_email').val();
		 	if(email=='' || email==undefined){
		 		jAlert("Please add Primary email address from top level info tab.");
		 		$("#vCust_sameBillingEmail").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_blEmail").val(email);
	 	 }else{
	 		$("#vCust_blEmail").val('');
	 		
	 	 }
	 });
	 $("#vCust_sameBillingPhone").change(function(){
		 if($("#vCust_sameBillingPhone").is(':checked')){
		 	var phone = $('#vCust_phone').val();
		 	if(phone=='' || phone==undefined){
		 		jAlert("Please add Primary Phone from top level info tab.");
		 		$("#vCust_sameBillingPhone").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_blPhone").val(phone);
	 	 }else{
	 		$("#vCust_blPhone").val('');
	 		
	 	 }
	 });
	 $("#vCust_sameShippingEmail").change(function(){
		 if($("#vCust_sameShippingEmail").is(':checked')){
		 	var email = $('#vCust_email').val();
		 	if(email=='' || email==undefined){
		 		jAlert("Please add Primary email address from top level info tab.");
		 		$("#vCust_sameShippingEmail").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_shEmail").val(email);
	 	 }else{
	 		$("#vCust_shEmail").val('');
	 		
	 	 }
	 });
	 $("#vCust_sameShippingPhone").change(function(){
		 if($("#vCust_sameShippingPhone").is(':checked')){
		 	var phone = $('#vCust_phone').val();
		 	if(phone=='' || phone==undefined){
		 		jAlert("Please add Primary Phone from top level info tab.");
		 		$("#vCust_sameShippingPhone").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_shPhone").val(phone);
	 	 }else{
	 		$("#vCust_shPhone").val('');
	 		
	 	 }
	 });
	 	
	$('#shippingTab').click(function(){
		setTimeout(function(){ 
		custShippingGridValues(shippingInfo);},10);
	});
	$('#invoiceTab').click(function(){
		setTimeout(function(){
		invoiceResetFilters();},10);
	});
	$('#cardTab').click(function(){
		setTimeout(function(){ 
		createCreditCardGrid(cardInfo);},10);
	});
	$('#poTab').click(function(){
		setTimeout(function(){
		poResetFilters();},10);
	});
	$('#loyalFanTab').click(function(){
		setTimeout(function(){
			loyalFanResetFilters();},10);
	});
	$('#favouriteEventTab').click(function(){
		setTimeout(function(){
			eventResetFilters();},10);
	});
});
</script>

<!-- popup View Customer Details -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-customer-detail">View Customer Details</button> -->

	<div id="view-customer-detail" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Customer Details - <span id="idHdr_CustomerDetails" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Invoice</a>
							</li>
							<li><i class="fa fa-laptop"></i>View Customer</li>
						</ol>
					</div>
				</div>
				<br/>
				<div id="editCustomerDiv">
					<div class="row">
						<section class="col-xs-12">
							<ul class="nav nav-tabs vcustnav" style="">
								<li id ="defaultTabViewCust" class="active"><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#topLevelInfo">Top-Level
										Info</a></li>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#address">Billing Address</a></li>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#shippingAddress" id="shippingTab">Shipping/Other Addresses</a>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#creditCard-2" id="cardTab">Credit
										Card</a></li>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#invoice-2" id="invoiceTab">Invoice</a></li>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#po-2" id="poTab">Purchase Order</a>
								</li>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#credit-2">Wallet Credit</a></li>
								<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#rewardPoints-2">Reward Points</a></li>
								<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#loyalFan" id="loyalFanTab">Loyal Fan</a></li>
								<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#favouriteEvent" id="favouriteEventTab">Favourite Event</a></li>
								<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#notes">Notes</a></li>
							</ul>
						</section>
					</div>
						<div class="full-width mt-20">
							<div class="tab-content vcustdiv">
								<div id="topLevelInfo" class="tab-pane active">
									<input id="action" value="action" name="action" type="hidden" />
									<input id="productType" value="" name="productType" type="hidden" />
									<input id="custId" value="${customerId}" name="custId" type="hidden" />
									<input id="billingAddressId" value="${billingAddress.id}" name="billingAddressId" type="hidden" />
									<div class="form-group tab-fields full-width">
										<div class="form-group col-sm-3 col-xs-6">
											<label>Type <span class="required">*</span> </label>
											<select name="customerType"
												class="form-control input-sm m-bot15" id="vCust_customerType">
												<option value="PHONE_CUSTOMER">Phone Customer</option>
												<option value="WEB_CUSTOMER">Web Customer</option>
												<option value="RETAIL_CUSTOMER">Retail Customer</option>
												<option value="BROKER">Broker</option>
												<option value="VENDOR">Vendor</option>
												<option value="CORPORATE">Corporate</option>
												<option value="CONSUMER">Consumer</option>
												<option value="EMPLOYEE">Employee</option>
												<option value="INTERNAL_USE_ONLY">Internal Use Only</option>
												<option value="EXCHANGES">Exchanges</option>
												<option value="EBAY_CUSTOMER">Ebay Customer</option>
												<option value="DIVISIONS">Divisions</option>
												<option value="EBAY_ESL">Ebay ESL</option>
												<option value="EBAY_CAT">Ebay CAT</option>
												<option value="CORPORATION">Corporation</option>
												<option value="AFFILIATE">Affiliate</option>
												<option value="EBAY_ZONES">Ebay Zones</option>
												<option value="AO_WEB_CUSTOMER">AO Web Customer</option>
												<option value="EBAY_RATA">Ebay RATA</option>
												<option value="TND_ZONES">TND Zones</option>
											</select>

										</div>
										<div class="form-group col-sm-3 col-xs-6">
											<label>Customer status <span class="required">*</span> </label>
											<select name="customerLevel"
												class="form-control input-sm m-bot15" id="vCust_customerLevel">
												<option value="">--select--</option>
												<option value="GOLD">Gold</option>
												<option value="SILVER">Silver</option>
												<option value="PLATINUM">Platinum</option>
											</select>

										</div>

										<div class="form-group col-sm-3 col-xs-6">
											<label> Signup Type <span class="required">*</span> </label>
											<select name="signupType"
												class="form-control input-sm m-bot15" id="vCust_signupType">
												<option value="">--select--</option>
												<option value="REWARDTHEFAN">REWARD THE FAN</option>
												<option value="FACEBOOK">Facebook</option>
												<option value="GOOGLE">Google</option>
											</select>
										</div>
										<div class="form-group col-sm-3 col-xs-6">
											<label>Client/Broker <span class="required">*</span> </label>
											<select name="clientBrokerType"
												class="form-control input-sm m-bot15" id="vCust_clientBrokerType">
												<option value="client">Client</option>
												<option value="broker">Broker</option>
												<option value="both">Both</option>
											</select>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="form-group tab-fields full-width">
										<div class="form-group col-sm-4 col-xs-6">
											<label>First Name<span class="required">*</span> </label>
											<input class="form-control" id="vCust_firstName"
												name="customerName" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Last Name<span class="required">*</span> </label>
											<input class="form-control" id="vCust_lastName"
												name="lastName" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Company Name</label>
											<input class="form-control" id="vCust_companyName"
												name="companyName" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>E-Mail <span class="required">*</span> </label>
											<input class="form-control" id="vCust_email"
												type="email" name="email" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Phone <span class="required">*</span> </label>
											<input class="form-control" id="vCust_phone"
												name="phone" type="text" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Ext</label>
											<input class="form-control"  id="vCust_extension"
												name="extension" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Other Phone </label>
											<input class="form-control"  id="vCust_otherPhone"
												name="Otherphone" type="text" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Representative </label>
											<input class="form-control"
												id="vCust_representativeName" name="representativeName" type="text" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<!--<label class="full-width">&nbsp </label>-->
											<button class="btn btn-primary" type="button" style="margin-top: 19px;" onclick="saveCustomerInfo('customerInfo');">Update</button>
										</div>
									</div>
									
									<!-- <div class="row" align="center">
										<div class="col-xs-12 mt-20">
											<div id="editCustomerAction" class="form-group">							
												<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('customerInfo');">Update</button>
												<button class="btn btn-cancel" onclick="cancelAction();" type="button">Cancel</button>
											</div>
										</div>
									</div> -->
								</div>
								<div id="address" class="tab-pane">
									<header class="panel-heading"> Billing Address </header>
									<div class="form-group tab-fields mt-20">
										<input id="updateBillingAddressAction" value="action" name="action" type="hidden" />
										<input id="vCust_blCustId" value="${customerId}" name="custId" type="hidden" />
										<input type="hidden" name="billingAddressId" id="vCust_billingAddressId" value="${billingAddress.id}"/>
										<div class="form-group col-sm-4 col-xs-6">
											<label>First Name <span class="required">*</span></label> 
											<input class="form-control" id="vCust_blFirstName" name="blFirstName" type="text" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Last Name <span class="required">*</span></label>
											<input class="form-control" id="vCust_blLastName" name="blLastName" type="text" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Email <span class="required">*</span></label>
											<input class="form-control" id="vCust_blEmail" name="blEmail" type="email" />
											<input type="checkbox" id="vCust_sameBillingEmail">Same as Primary Email
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Phone <span class="required">*</span></label>
											<input class="form-control" id="vCust_blPhone" name="blPhone" type="text" />
											<input type="checkbox" id="vCust_sameBillingPhone">Same as Primary Phone
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Street1 <span class="required">*</span></label>
											<input class="form-control" id="vCust_blStreet1" type="text" name="addressLine1" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Street2</label>
											<input class="form-control" id="vCust_blStreet2" type="text" name="addressLine2" />
										</div>										
										<div class="form-group col-sm-4 col-xs-6">
											<label>Zip Code <span class="required">*</span></label>		
											<input class="form-control" id="vCust_blZipCode" type="text" name="zipCode" onblur="getCityStateCountry(this.value, 'body-view-customer-details-billing')"/>
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>City <span class="required">*</span></label>
											<input class="form-control" id="vCust_blCity" type="text" name="city" />
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Country <span class="required">*</span></label>		
											<select id="vCust_blCountryName" name="countryName" class="form-control input-sm m-bot15" onchange="loadCustShippingState(vCust_blCountryName,vCust_blStateName,'')">
												<%--<option value="-1">--select--</option>
												<c:forEach var="country" items="${countries}">
													<option value="${country.id}" 
													<c:if test="${billingAddress.country.id == country.id}"> selected </c:if>> 
														<c:out value="${country.name}"/>
													</option>	
												</c:forEach>--%><!---->
											</select>
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>State <span class="required">*</span></label>
											<select id="vCust_blStateName" name="stateName" class="form-control input-sm m-bot15" onchange="">
												 <%--<option value="-1">--select--</option>
												  <c:forEach var="state" items="${states}">
														<option value="${state.id}" 
															<c:if test="${billingAddress.state.id == state.id}"> selected </c:if>>
															<c:out value="${state.name}"/>		
														</option>
												 </c:forEach> --%><!---->
											</select>
										</div>	
										
										<div class="form-group col-sm-4 col-xs-6">
											<!--<label class="full-width">&nbsp </label>-->
											<button class="btn btn-primary" type="button" style="margin-top: 19px;" onclick="saveCustomerInfo('billingInfo');">Update</button>
										</div>
									</div>
									<!-- <div class="row" align="center">
										<div class="col-xs-12 mt-20">
											<div class="form-group full-width"> -->
												<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
												<!-- <button class="btn btn-primary" type="button" onclick="saveCustomerInfo('billingInfo');">Update</button>
												<button class="btn btn-cancel" onclick="cancelAction();" type="button">Cancel</button>
											</div>
										</div>
									</div> -->
								</div>
								<div id="shippingAddress" class="tab-pane">
								
									<header class="panel-heading mb-10">Shipping/Other Address</header>
									
									<a data-toggle="modal" data-target="#myModal-2" class="btn btn-primary" id="addShippingAddress" onclick="addModal();">Add</i></a>
									<!-- <div class="form-group">
										  <form>
											<div class="col-xs-12">
												<table class="table table-striped table-advance table-hover" id="shippingTable">
													
												</table>
											</div>
											</form>
										</div> -->
									<div class="full-width mt-20" style="position: relative">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Customer Shipping/Other Addresses</label> <!-- <span id="shipping_grid_toogle_search" style="float: right"
													class="ui-icon ui-icon-search" title="Toggle search panel"
													onclick="shippingToggleFilterRow()"></span> -->
											</div>
											<div id="custShipping_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="custShipping_pager" style="width: 100%; height: 20px;"></div>
										</div>
									</div>
									<!--<div id="customer_inlineFilterPanel" class="full-width mt-20" style="display:none;background:#dddddd;padding:3px;color:black;">
										Show records with invoice including <input type="text" id="txtSearch2">
									</div>-->
								</div>
								<div id="invoice-2" class="tab-pane">
									<!-- Grid view for customers -->
									<div class="full-width" style="position: relative">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Customer Invoices</label>
												<div class="pull-right">
												<a href="javascript:invoiceResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
												</div>
											</div>
											<div id="custInvoice_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="custInvoice_pager" style="width: 100%; height: 20px;"></div>
										</div>
									</div>					
								</div>
								
								<div id="po-2" class="tab-pane">
								<!-- Grid view for customers -->
									<div class="full-width" style="position: relative">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Customer Purchase Order</label>
												<a href="javascript:poResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
											</div>
											<div id="custPO_grid" style="width: 100%;height:200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="custPO_pager" style="width: 100%; height: 20px;"></div>
										</div>
									</div>
								</div>
								
								<div id="rewardPoints-2" class="tab-pane">
									<div class="full-width" style="position: relative">
										<div class="full-width">
											<div class="row">						
												<div class="form-group">	
													<div class="form-group col-md-4 col-sm-6 col-xs-12">
													
														<label for="name" class="control-label">Reward Points</label>
														<input class="form-control" id="vCust_newRewardPoints" name="newRewardPoints" />
													</div>
													<div class="form-group col-md-4 col-sm-6 col-xs-12">
														<!--<label class="full-width">&nbsp </label>-->														
														<button class="btn btn-primary" type="button" style="margin-top: 19px;" onclick="updateRewardPoints()">Save Points</button>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								
									<br/>
									<div class="full-width mt-20" style="position: relative">
										<div style="width: 100%;">
											<section class="panel">
												<header style="font-size: 13px;font-family: arial;" class="panel-heading">Customer Reward Points </header>
											
												<div class="panel-body">
													<div class="form-group">
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Current Reward Point Balance: </label>
															<span style="font-size: 12px;" id="vCust_rewardPointBalance"></span>
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Available Reward Points: </label>
															<span id="vCust_actPoints"></span>
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Pending Reward Points: </label>
															<span id="vCust_pendPoints"></span>
														</div>
														<!-- <div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Latest Earned Points: </label>
															<span id=latEarnPoints></span>
														</div>-->
														
													</div>
													
													<div class="form-group">
														<!-- <div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Latest Spent Points: </label>
															<span id=latSpentPoints></span>
														</div> -->
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Total Lifetime Earned Points: </label>
															<span style="font-size: 12px;" id="vCust_totEarnPoints"></span> &nbsp; 
																<img src="../resources/images/icons/info.png" onclick="openPointsHistory('Earned Points');" height="15" width="15" />					
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Total Points Redeemed to Date: </label>
															<span id="vCust_totSpentPoints"></span> &nbsp; 
																<img src="../resources/images/icons/info.png" onclick="openPointsHistory('Redeemed Points');" height="15" width="15" />
														</div>				
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Voided Points To Date: </label>
															<span id="vCust_voidPoints"></span>
														</div>
														
													</div>
													
												</div>
											</section>			
										</div>
									</div>
								</div>
								<div id="credit-2" class="tab-pane">
								
									<div class="full-width" style="position: relative">
										<div class="full-width">
											<div class="filters-div">
												<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
													<label for="name" class="control-label">Wallet Amount</label>
													<input class="form-control" id="vCust_customerWalletAmount" name="vCust_customerWalletAmount" />
												</div>
												<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
													<label for="name" class="control-label">Transaction Type</label>
													<select class="form-control input-sm m-bot15" name="vCust_walletTransactionType" id="vCust_walletTransactionType">
														<option value="">--SELECT--</option>
														<option value="CREDIT">CREDIT</option>
														<option value="DEBIT">DEBIT</option>
													</select>
												</div>
												<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
													<!--<label>&nbsp</label>-->
													<button class="btn btn-primary" type="button" style="margin-top: 17px;" onclick="updateCustomerWallet()">Save</button>
												</div>
											</div>
										</div>
									</div>
									
									<div class="full-width" style="position: relative">
										<div class="full-width">
											<section class="panel"> 
												<header style="font-size: 13px;font-family: arial;" class="panel-heading">
													Wallet Credit Details 
												</header>
												<div class="panel-body">
													<div class="form-group">
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Active Credit : </label>
															<span style="font-size: 12px;" id="vCust_activeCredit"></span>
															
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Total Earned Credit : </label>
															<span id="vCust_totalEarnedCredit"></span>
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Total Used Credit : </label>
															<span id="vCust_totalUsedCredit"></span>
														</div>
													</div>
												</div>
											</section>			
										</div>
									</div>
								</div>
								<div id="notes" class="tab-pane">
									<input type="hidden" name="action" value="action" id="saveNote"/>
									<input class="form-control" id="vCust_noteCustId" value="${customerId}" name="custId" type="hidden" />
									<span style="font-size: 13px;font-family: arial; float:left;">Add a new note</span>
									
									<div class="full-width mt-20">
									  <textarea name="notes" id="vCust_notesTxt" class="form-control noresize" rows="8"></textarea>
									</div>
									
									<div class="full-width mt-20 mb-20" align="center">
										
												<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('notes');">Save Note</button>
											
									</div>
								</div>
								<div id="creditCard-2" class="tab-pane">
									<div class="full-width mb-20" style="position: relative">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Customer Credit Cards</label>
													<!--<span style="float: right"
													class="ui-icon ui-icon-search" title="Toggle search panel"
													onclick="creditCardToggleFilterRow()"></span>-->
											</div>
											<div id="custCreditCardGrid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="creditCard_pager" style="width: 100%; height: 20px;"></div>
										</div>
									</div>
									<!--<div id="customer_inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
									Show records with Credit Card including <input type="text" id="txtSearch2">
									</div>-->
								</div>
								<div id="loyalFan" class="tab-pane">
									<div id="vCust_loyalFanDiv">
										<div style="position: relative">
											<div class="full-width">
												<section class="panel"> 
												<header style="font-size: 13px;font-family: arial;" class="panel-heading">Artist Details </header>
												<div class="panel-body">
													<div class="form-group">
														<div class="col-sm-4" id="vCust_loyalArtistDiv">
															<label style="font-size: 13px; font-weight: bold;">Artist
																Name : </label> <span style="font-size: 12px;" id="vCust_loyalArtistName"></span>
														</div>
														<div class="col-sm-4" id="vCust_loyalCityDiv">
															<label style="font-size: 13px; font-weight: bold;">State
																Name : </label> <span style="font-size: 12px;" id="vCust_loyalCityName"></span>
														</div>
														<div class="col-sm-4">
															<label style="font-size: 13px; font-weight: bold;">Parent Category : </label>
																<span id="vCust_loyalCategoryName"></span>
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Tickets Purchased : </label>
															<span id="vCust_loyalTicketsPurchasedSpan"></span>
															<input type="hidden" name="vCust_loyalTicketsPurchased" id="vCust_loyalTicketsPurchased" />
														</div>
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">Start Date : </label>
															<span style="font-size: 12px;" id="vCust_loyalStartDate"></span>											
														</div>
													</div>
													<div class="form-group">
														<div class="form-group col-md-4 col-sm-6 col-xs-12">
															<label style="font-size: 13px;font-weight:bold;">End Date : </label>
															<span id="vCust_loyalEndDate"></span>
														</div>
													</div>
												</div>
												</section>			
											</div>
										</div>
									</div><br/>	
									
									<div class="full-width" style="position: relative">
										<div class="full-width">
											<div class="filters-div">
												<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
													<label for="name" class="control-label">Parent Category</label>
													<select name="parentType" id="parentType" class="form-control input-sm m-bot15" onchange="loadAutoComplete();">
														<option value="">--- Select ---</option>
														<option value="SPORTS">Sports</option> 
														<option value="CONCERTS">Concerts</option>
														<option value="THEATER">Theater</option>
													</select>
												</div>
												<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4" id="artistDiv" style="display:none;">
													<label>Artist</label>
													<input class="form-control autoArtist" type="text" name="loyalFanArtist" id="loyalFanArtist"  />
													<label id="taglabel"></label>
													<input type="hidden" name="loyalFanArtists" id="loyalFanArtists" />
												</div>
												<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4" id="cityStateDiv" style="display:none;">
													<label>State</label>
													<input class="form-control autoCityState" type="text" name="loyalFanCity" id="loyalFanCity"  />
													<label id="taglabel1"></label>
													<input type="hidden" name="loyalFanSelectionValue" id="loyalFanSelectionValue" />
													<input type="hidden" name="loyalFanCityName" id="loyalFanCityName" />
													<input type="hidden" name="loyalFanStateName" id="loyalFanStateName" />
													<input type="hidden" name="loyalFanCountryName" id="loyalFanCountryName" />
													<input type="hidden" name="loyalFanZipCode" id="loyalFanZipCode" />
												</div>
											</div>
										</div>
										
										<div class="full-width mb-20 mt-20 full-width-btn">						
											<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('loyalFan');">Save</button>						
										</div>
									</div>
									
									<!--<div class="full-width mb-20" style="position: relative">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Artist Details</label>
												<div class="pull-right">
												<a href="javascript:loyalFanResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
												</div>
											</div>
											<div id="custArtist_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="custArtist_pager" style="width: 100%; height: 10px;"></div>
										</div>
									</div>
									
									<div class="full-width text-center mb-20" style="position: relative">
															
												<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('loyalFan');">Save Artist</button>
											
									</div>-->
								</div>						
								<div id="favouriteEvent" class="tab-pane">
									
									<div style="position: relative">
										<div class="col-xs-12">
											<div class="form-group">
												<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<label>Event</label>								
													<input class="form-control autoEvent" type="text" name="favoriteEvent" id="favoriteEvent"  />
													<label id="tagFavoriteEvent"></label>
													<input type="hidden" id="favoriteEventId" name="favoriteEventId" />
												</div>
												<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<!--<label>&nbsp</label>-->
													<button class="btn btn-primary" type="button" style="margin-top: 19px;" onclick="saveCustomerInfo('favouriteEvent');">Save Events</button>
												</div>
											</div>
										</div>
									</div>
									<br/>
									
									<div class="full-width mb-20" style="position: relative">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Event Details</label>
												<div class="pull-right">
													<a href="javascript:eventResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
												</div>
											</div>
											<div id="custEvent_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="custEvent_pager" style="width: 100%; height: 10px;"></div>
										</div>
									</div>
									<!-- <div class="full-width mb-20 text-center" style="position: relative">					
										<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('favouriteEvent');">Save Events</button>
									</div> -->
								</div>								
							</div>							
						</div>					
				</div>	
			</div>
			<div class="modal-footer full-width">
				<div id="editCustomerAction" class="full-width">							
					<!--<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('customerInfo');">Update</button>-->
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	  </div>
	</div>
	
	<!-- Add Shipping/Other Address-->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal-2">Add Shipping/Other Address</button> -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
		tabindex="-1" id="myModal-2" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Shipping/Other Address - Email : <span id="customerEmailHdr_Shipping" class="headerTextClass"></span></h4>
				</div>
					<div class="modal-body full-width">
					 
						<input id="action" value="addShippingAddress" name="action" type="hidden" />
						<input type="hidden" name="shippingAddrId"  id="vCust_shippingAddrId" value=""/>
						<input class="form-control" id="vCust_shCustId" value="${customerId}" name="custId" type="hidden" />
						<input type="hidden" name="count" id="count" value="${count}" />
						<div class="form-group tab-fields">
							<div class="form-group col-sm-4 col-xs-6">
								<label>First Name <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shFirstName" name="shFirstName"
									type="text" value="" />
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>Last Name <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shLastName" name="shLastName"
									type="text" value="" />
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>Email <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shEmail" name="shEmail"
									type="text" value="" />
								<input type="checkbox" id="vCust_sameShippingEmail">Same as Primary Email
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>Phone <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shPhone" name="shPhone"
									type="text" value="" />
								<input type="checkbox" id="vCust_sameShippingPhone">Same as Primary Phone
							</div>
							
							<div class="form-group col-sm-4 col-xs-6">
								<label>Street1 <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shAddressLine1" type="text"
									name="shAddressLine1" value="" />
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>Street2</label> 
								<input class="form-control" id="vCust_shAddressLine2" type="text" name="shAddressLine2"
									value="" />
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>Zip Code <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shZipCode" type="text"
									name="shZipCode" value="" onblur="getCityStateCountry(this.value, 'body-view-customer-details-shipping')"/>
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>City <span class="required">*</span></label> 
								<input class="form-control" id="vCust_shCity" type="text" name="shCity"
									value="" />
							</div>							
							<div class="form-group col-sm-4 col-xs-6">
								<label>Country <span class="required">*</span></label> 
								<select id="vCust_shCountryName" name="shCountryName"
									class="form-control input-sm m-bot15" onchange="loadCustShippingState(vCust_shCountryName,vCust_shStateName,'')">
									<%--<option value="-1">--select--</option>
									<c:forEach var="country" items="${countries}">
										<option value="${country.id}">
											<c:out value="${country.name}" />
										</option>
									</c:forEach>--%>
								</select>
							</div>
							<div class="form-group col-sm-4 col-xs-6">
								<label>State <span class="required">*</span></label> 
								<select id="vCust_shStateName" name="shStateName"
									class="form-control input-sm m-bot15" onchange="">
									<%--<option value="-1">--select--</option>
									 <c:forEach var="state" items="${states}">
										<option value="${state.id}">
											<c:out value="${state.name}" />
										</option>
									</c:forEach> --%>
								</select>
							</div>
														
						</div>
									
					</div>
					<div class="modal-footer full-width">
						<button class="btn btn-primary" type="button" onclick="saveCustomerInfo('shippingInfo')">Save</button>
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					</div>			
			</div>
		</div>
	</div>
	<!-- Ends of Add Shipping/Other Address popup-->
	
	<!-- popup Edit Reward Points -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-reward-points">Edit Reward Points</button> -->
	<div id="edit-reward-points" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Reward Points - Email : <span id="customerEmailHdr_RewardPoint" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Customers
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Customers</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Reward Points</li>
							</ol>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div id="custRewards_successDiv" class="alert alert-success fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="custRewards_successMsg"></span></strong>
							</div>					
							<div id="custRewards_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="custRewards_errorMsg"></span></strong>
							</div>
						</div>
					</div>
					<br />

					<div class="row">
						<div class="col-xs-12">
							<input type="hidden" id="custRewards_breakUpMsg" name="breakUpMsg" value="${rewardPointBreakUp}">
							<input id="custRewards_customerId" value="${customerId}" name="customerId" type="hidden" />
							<div class="full-width mb-20" style="position: relative">
								<div class="table-responsive grid-table">
									<div class="grid-header full-width">
										<label>Reward Points</label>
										<a href="javascript:custRewardPointsResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
									</div>
									<div id="custRewardPoints_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
									<div id="custRewardPoints_pager" style="width: 100%; height: 20px;"></div>
								</div>
							</div>
						</div>
					</div>
					
						
					
					<%--<c:if test="${empty rewardPointsList}">
							<div class="form-group full-width" align="center">
								
									<label style="font-size: 15px;font-weight:bold;">No reward points found, Please purchase any ticket.</label>
								
							</div>
						</c:if> --%>
						
					<!--</div>-->
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- End popup Edit Reward Points -->
		
<!-- popup View Invoice Tickets -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-invoice-tickets">View Invoice Ticket</button> -->
	<div id="view-invoice-tickets" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Invoice Ticket - Email : <span id="customerEmailHdr_InvoiceTicket" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Customers
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Customers</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Invoice</li>
							</ol>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div id="custInvoiceTicket_successDiv" class="alert alert-success fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="custInvoiceTicket_successMsg"></span></strong>
							</div>					
							<div id="custInvoiceTicket_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
								<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
								<span id="custInvoiceTicket_errorMsg"></span></strong>
							</div>
						</div>
					</div>
					<br />

					<div class="row">
						<div class="col-xs-12">
							<input id="invoiceTicket_invoiceId" value="${invoiceId}" name="invoiceId" type="hidden" />
							<div class="full-width mb-20" style="position: relative">
								<div class="table-responsive grid-table">
									<div class="grid-header full-width">
										<label>Invoice Details</label>
										<!-- <a href="javascript:custInvoiceTicketResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a> -->
									</div>
									<div id="custInvoiceTicket_grid" style="width: 100%;height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
									<div id="custInvoiceTicket_pager" style="width: 100%; height: 20px;"></div>
								</div>
							</div>
						</div>
					</div>
					
					<%--<c:if test="${empty invoiceInfo}">
							<div class="form-group" align="center">
								<div class="col-sm-12">
									<label style="font-size: 15px;font-weight:bold;">No Download Tickets found, Please add ticket to download.</label>
								</div>
							</div>
						</c:if>--%>
						
					<!--</div>-->
				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup View Invoice Tickets -->
	
<!-- End popup View Customer Details -->
