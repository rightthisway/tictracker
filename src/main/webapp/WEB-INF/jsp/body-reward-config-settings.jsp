<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(rewardConfigGrid != null && rewardConfigGrid != undefined){
			rewardConfigGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${status == 'ACTIVE'}">	
			$('#activeConfig').addClass('active');
			$('#activeConfigTab').addClass('active');
		</c:when>
		<c:when test="${status == 'INACTIVE'}">
			$('#inactiveConfig').addClass('active');
			$('#inactiveConfigTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeConfig1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#inactiveConfig1").click(function(){
		callTabOnChange('INACTIVE');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/RewardConfigSettings?status="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling</a></li>
			<li><i class="fa fa-laptop"></i>Manage reward settings</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeConfigTab" class=""><a id="activeConfig1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeConfig">Active Reward Config.</a></li>
				<li id="inactiveConfigTab" class=""><a id="inactiveConfig1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveConfig">InActive Reward Config.</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeConfig" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetRewardConfigModal();">Add Reward Config</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editRewardConfig()">Edit Reward Config</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteRewardConfig()">Delete Reward Config</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="rewardConfigGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Reward config settings</label>
							<div class="pull-right">
								<a href="javascript:rewardConfigExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:rewardConfigResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="rewardConfig_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="rewardConfig_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="inactiveConfig" class="tab-pane">
			<c:if test="${status =='INACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<!-- <button class="btn btn-primary" id="editQBBtn" type="button" onclick="editRewardConfig()">Edit Reward Config</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteRewardConfig()">Delete Reward Config</button> -->
				</div>
				<br />
				<br />
				<div style="position: relative" id="rewardConfigGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Reward config settings</label>
							<div class="pull-right">
								<a href="javascript:rewardConfigExportToExcel('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:rewardConfigResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="rewardConfig_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="rewardConfig_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Reward Config Bank -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="qBModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Reward config setting</h4>
			</div>
			<div class="modal-body full-width">
				<form name="rewardConfigForm" id="rewardConfigForm" method="post">
					<input type="hidden" id="configId" name="configId" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Reward Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Reward Type<span class="required">*</span></label> 
							<select id="actionType" name="actionType" class="form-control input-sm m-bot15" onChange="changeActionType()" >
									<option value="LIKE_VIDEO">LIKE VIDEO</option>
									<option value="SHARE_VIDEO">SHARE VIDEO</option>
									<option value="WATCH_VIDEO">WATCH VIDEO</option>
									<option value="UPLOAD_FAN_FREAKOUT_VIDEO">UPLOAD FAN FREAKOUT VIDEO</option>
									<option value="UPLOAD_FANCLUB_VIDEO">UPLOAD FANCLUB VIDEO</option>
									<option value="LIKE_POST">LIKE POST</option>
									<option value="SHOW_INTEREST_TO_EVENT">SHOW INTEREST TO EVENT</option>
									<option value="CREATE_POST">CREATE NEW POST</option>
									<option value="CREATE_EVENT">CREATE NEW EVENT</option>
									<option value="SCRATCH_AND_WIN">SCRATCH & WIN</option>
									<option value="PRIMARYREFERRAL">REFERRAL</option>
									<!-- <option value="TIREONEREFERRAL">TIRE ONE REFERRAL</option> -->
								</select>
						</div>		
						<div class="form-group col-sm-4 col-xs-4" id="livesDiv">
							<label>Lives<span class="required">*</span>
							</label> <input class="form-control" type="text" id="lives" name="lives">
						</div>
						<!-- <div class="form-group col-sm-4 col-xs-4">
							<label>Eraser<span class="required">*</span>
							</label> <input class="form-control" type="text" id="magicWand" name="magicWand">
						</div> -->
						<div class="form-group col-sm-4 col-xs-4" id="starsDiv">
							<label>Superfan Stars<span class="required">*</span>
							</label> <input class="form-control" type="text" id="stars" name="stars">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Points<span class="required">*</span>
							</label> <input class="form-control" type="text" id="points" name="points">
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="maxRewardDiv">
							<label>Max. Reward Per Day(Points)<span class="required">*</span>
							</label> <input class="form-control" type="text" id="maxReward" name="maxReward">
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="noOfLikesDiv">
							<label>No, Post/Video/Event Likes/Interested<span class="required">*</span>
							</label> <input class="form-control" type="text" id="noOfLikes" name="noOfLikes">
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="minVideoPlayTimeDiv">
							<label>Min. Video play time(seconds)<span class="required">*</span>
							</label> <input class="form-control" type="text" id="minVideoTime" name="minVideoTime">
						</div>
						<div class="form-group col-sm-4 col-xs-4" id="rewardIntarvalDiv">
							<label>Reward Interval(Minutes)<span class="required">*</span>
							</label> <input class="form-control" type="text" id="rewardInterval" name="rewardInterval">
						</div>
						<!-- <div class="form-group col-sm-4 col-xs-4">
							<label>Reward Dollars<span class="required">*</span>
							</label> <input class="form-control" type="text" id="dollars" name="dollars">
						</div> -->
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qBSaveBtn" type="button" onclick="saveConfig('save')">Save</button>
				<button class="btn btn-primary" id="qBUpdateBtn" type="button" onclick="saveConfig('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Reward Config Bank end here  -->
<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'rewardConfig_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getRewardConfigGridData(pageNo);
		}
	}
	
	//Reward Config Bank Grid
	
	function getRewardConfigGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/RewardConfigSettings.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+rewardConfigSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshRewardConfigGridValues(jsonData.configs);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function rewardConfigExportToExcel(status){
		var appendData = "headerFilter="+rewardConfigSearchString+"&status="+status;
	    var url =apiServerUrl+"RewardConfigExport?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function rewardConfigResetFilters(){
		rewardConfigSearchString='';
		rewardConfigColumnFilters = {};
		getRewardConfigGridData(0);
	}
	
	var rewardConfigCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var rewardConfigDataView;
	var rewardConfigGrid;
	var rewardConfigData = [];
	var rewardConfigGridPager;
	var rewardConfigSearchString='';
	var rewardConfigColumnFilters = {};
	var userRewardConfigColumnsStr = '<%=session.getAttribute("rewardConfigGrid")%>';

	var userRewardConfigColumns = [];
	var allRewardConfigColumns = [
			 {
				id : "mediaDescription",
				field : "mediaDescription",
				name : "Reward Title",
				width : 80,
				sortable : true
			},{
				id : "actionType",
				field : "actionType",
				name : "Reward Type",
				width : 80,
				sortable : true
			},{
				id : "lives",
				field : "lives",
				name : "Lives",
				width : 80,
				sortable : true
			},/* {
				id : "magicWands",
				field : "magicWands",
				name : "Magic Wands",
				width : 80,
				sortable : true
			}, */{
				id : "points",
				field : "points",
				name : "Points",
				width : 80,
				sortable : true
			},{
				id : "sfStars",
				field : "sfStars",
				name : "SuperFan Stars",
				width : 80,
				sortable : true
			},{
				id : "rtfPoints",
				field : "rtfPoints",
				name : "Points",
				width : 80,
				sortable : true
			},/*{
				id : "rwdDollars",
				field : "rwdDollars",
				name : "Reward Dollars",
				width : 80,
				sortable : true
			}, */{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "maxRewards",
				field : "maxRewards",
				name : "Max. Rewards",
				width : 80,
				sortable : true
			},{
				id : "noOfLikes",
				field : "noOfLikes",
				name : "Post/Video Like Count",
				width : 80,
				sortable : true
			},{
				id : "minVideoPlayTime",
				field : "minVideoPlayTime",
				name : "Min. Video Play Time",
				width : 80,
				sortable : true
			},{
				id : "videoRewardInterval",
				field : "videoRewardInterval",
				name : "Video Rewards Interval",
				width : 80,
				sortable : true
			},{
				id : "activeDate",
				field : "activeDate",
				name : "Activated Date",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userRewardConfigColumnsStr != 'null' && userRewardConfigColumnsStr != '') {
		columnOrder = userRewardConfigColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allRewardConfigColumns.length; j++) {
				if (columnWidth[0] == allRewardConfigColumns[j].id) {
					userRewardConfigColumns[i] = allRewardConfigColumns[j];
					userRewardConfigColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userRewardConfigColumns = allRewardConfigColumns;
	}
	
	var rewardConfigOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var rewardConfigGridSortcol = "rewardConfigId";
	var rewardConfigGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function rewardConfigGridComparer(a, b) {
		var x = a[rewardConfigGridSortcol], y = b[rewardConfigGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshRewardConfigGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		rewardConfigData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (rewardConfigData[i] = {});
				d["id"] = i;
				d["rewardConfigId"] = data.id;
				d["mediaDescription"] = data.mediaDescription;
				d["actionType"] = data.actionType;
				d["rtfPoints"] = data.rtfPoints;
				d["sfStars"] = data.sfStars;
				d["lives"] = data.lives;
				d["magicWands"] = data.magicWands;
				d["rwdDollars"] = data.rwdDollars;
				d["status"] = "INACTIVE";
				if(data.status == true || data.status == 'true'){
					d["status"] = "ACTIVE";
				}
				d["maxRewards"] = data.maxRewardsPerDay;
				d["noOfLikes"] = data.batchSize;
				d["minVideoPlayTime"] = data.minVideoPlayTime;
				d["videoRewardInterval"] = data.videoRewardInterval;
				d["activeDate"] = data.activeDateStr;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		rewardConfigDataView = new Slick.Data.DataView();
		rewardConfigGrid = new Slick.Grid("#rewardConfig_grid", rewardConfigDataView,
				userRewardConfigColumns, rewardConfigOptions);
		rewardConfigGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		rewardConfigGrid.setSelectionModel(new Slick.RowSelectionModel());
		rewardConfigGrid.registerPlugin(rewardConfigCheckboxSelector);
		
		rewardConfigGridPager = new Slick.Controls.Pager(rewardConfigDataView,
					rewardConfigGrid, $("#rewardConfig_pager"),
					pagingInfo);
		/* var rewardConfigGridColumnpicker = new Slick.Controls.ColumnPicker(
				allRewardConfigColumns, rewardConfigGrid, rewardConfigOptions); */
		
		rewardConfigGrid.onSort.subscribe(function(e, args) {
			rewardConfigGridSortdir = args.sortAsc ? 1 : -1;
			rewardConfigGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				rewardConfigDataView.fastSort(rewardConfigGridSortcol, args.sortAsc);
			} else {
				rewardConfigDataView.sort(rewardConfigGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the rewardConfigGrid
		rewardConfigDataView.onRowCountChanged.subscribe(function(e, args) {
			rewardConfigGrid.updateRowCount();
			rewardConfigGrid.render();
		});
		rewardConfigDataView.onRowsChanged.subscribe(function(e, args) {
			rewardConfigGrid.invalidateRows(args.rows);
			rewardConfigGrid.render();
		});
		$(rewardConfigGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							rewardConfigSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								rewardConfigColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in rewardConfigColumnFilters) {
										if (columnId !== undefined
												&& rewardConfigColumnFilters[columnId] !== "") {
											rewardConfigSearchString += columnId
													+ ":"
													+ rewardConfigColumnFilters[columnId]
													+ ",";
										}
									}
									getRewardConfigGridData(0);
								}
							}

						});
		rewardConfigGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate' || args.column.id == 'activeDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(rewardConfigColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(rewardConfigColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		rewardConfigGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		rewardConfigDataView.beginUpdate();
		rewardConfigDataView.setItems(rewardConfigData);
		//rewardConfigDataView.setFilter(filter);
		rewardConfigDataView.endUpdate();
		rewardConfigDataView.syncGridSelection(rewardConfigGrid, true);
		rewardConfigGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserRewardConfigPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = rewardConfigGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('rewardConfigGrid', colStr);
	}
	
	// Add Reward Config Bank	
	function resetRewardConfigModal(){		
		$('#qBModal').modal('show');
		$('#configId').val('');
		$('#title').val('');
		$('#actionType').val('LIKE_VIDEO');
		$('#actionType option').attr('disabled',false);
		changeActionType();
		$('#lives').val('');
		//$('#magicWand').val('');
		$('#stars').val('');
		$('#points').val('');
		//$('#dollars').val('');
		$('#maxReward').val('');
		$('#noOfLikes').val('');
		$('#minVideoTime').val('');
		$('#rewardInterval').val('');
		$('#qBSaveBtn').show();
		$('#qBUpdateBtn').hide();	
	}

	function saveConfig(action){
		
		var title = $('#title').val();
		var actionType = $('#actionType').val();
		
		if(title == ''){
			jAlert("Reward Title is Mandatory.");
			return;
		}
		if(actionType == ''){
			jAlert("Reward type is Mandatory.");
			return;
		}
		var requestUrl = "${pageContext.request.contextPath}/UpdateRewardConfig.json";
		var dataString = "";
		if(action == 'save'){		
			dataString  = $('#rewardConfigForm').serialize()+"&action=SAVE&pageNo=0";
		}else if(action == 'update'){
			dataString = $('#rewardConfigForm').serialize()+"&action=UPDATE&pageNo=0";
		}
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			data: dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#qBModal').modal('hide');
					pagingInfo = jsonData.pagingInfo;
					rewardConfigColumnFilters = {};
					refreshRewardConfigGridValues(jsonData.configs);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit Reward Config Bank
	function editRewardConfig(){
		var tempRewardConfigRowIndex = rewardConfigGrid.getSelectedRows([0])[0];
		if (tempRewardConfigRowIndex == null) {
			jAlert("Plese select reward config record to Edit", "info");
			return false;
		}else {
			var rewardConfigId = rewardConfigGrid.getDataItem(tempRewardConfigRowIndex).rewardConfigId;
			getEditRewardConfig(rewardConfigId);
		}
	}
	
	function getEditRewardConfig(configId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateRewardConfig.json",
			type : "post",
			dataType: "json",
			data: "configId="+configId+"&action=EDIT&pageNo=0",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#qBModal').modal('show');
					setEditRewardConfig(jsonData.config);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditRewardConfig(data){
		$('#qBSaveBtn').hide();
		$('#qBUpdateBtn').show();
		$('#configId').val(data.id);
		$('#title').val(data.mediaDescription);
		$('#actionType option').attr('disabled',false);
		$('#actionType').val(data.actionType);
		$('#actionType option:not(:selected)').attr('disabled',true);
		$('#lives').val(data.lives);
		$('#magicWand').val(data.magicWands);
		$('#stars').val(data.sfStars);
		$('#points').val(data.rtfPoints);
		$('#dollars').val(data.rwdDollars);
		$('#maxReward').val(data.maxRewardsPerDay);
		$('#noOfLikes').val(data.batchSize);
		$('#minVideoTime').val(data.minVideoPlayTime);
		$('#rewardInterval').val(data.videoRewardInterval);
		changeActionType();
	}
	
	
	function changeActionType(){
		var actionType = $('#actionType').val();
		$('#livesDiv').hide();
		$('#starsDiv').hide();
		$('#noOfLikesDiv').hide();
		$('#rewardIntarvalDiv').hide();
		$('#minVideoPlayTimeDiv').hide();
		if(actionType == 'WATCH_VIDEO'){
			$('#minVideoPlayTimeDiv').show();
			$('#rewardIntarvalDiv').show();
		}else if(actionType == 'SHARE_VIDEO'){
			$('#starsDiv').show();
		}else if(actionType == 'LIKE_VIDEO' || actionType == 'LIKE_POST' || actionType == 'SHOW_INTEREST_TO_EVENT'){
			$('#noOfLikesDiv').show();
		}else if(actionType == 'PRIMARYREFERRAL'){
			$('#livesDiv').show();
		}
	}
	
	//Delete Reward Config Bank
	function deleteRewardConfig(){
		var tempRewardConfigRowIndex = rewardConfigGrid.getSelectedRows([0])[0];
		if (tempRewardConfigRowIndex == null) {
			jAlert("Plese select Reward Config to delete", "info");
			return false;
		}else {
			var configId = rewardConfigGrid.getDataItem(tempRewardConfigRowIndex).rewardConfigId;
			getDeleteRewardConfig(configId);
		}
	}
	
	
	function getDeleteRewardConfig(configId){
		jConfirm("Are you sure to delete selected Reward Config ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateRewardConfig.json",
						type : "post",
						dataType: "json",
						data : "configId="+configId+"&action=DELETE&pageNo=0",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.pagingInfo;
								rewardConfigColumnFilters = {};
								refreshRewardConfigGridValues(jsonData.configs);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
								return;
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		refreshRewardConfigGridValues(${configs});
		$('#rewardConfig_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserRewardConfigPreference()'>");
		
		enableMenu();
	};
		
</script>