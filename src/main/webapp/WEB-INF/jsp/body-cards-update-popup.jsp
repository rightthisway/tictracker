<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
 <!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
   
    <!-- custom select -->
    <script src="../resources/js/jquery.customSelect.min.js" ></script>
	<%-- <script src="../resources/assets/chart-master/Chart.js"></script> --%>
    <script src="../resources/js/owl.carousel.js" ></script>
    <!--custome script for all page-->
    <script src="../resources/js/scripts.js"></script>
	<script src="../resources/js/jquery.slimscroll.min.js"></script>
	
	<link href="../resources/css/datepicker.css" rel="stylesheet">
	<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
	<script src="../resources/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/jquery.alerts.js"></script>
	

<style>
input {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #FFFFFF;
    padding-bottom: 5px;
    margin-bottom: 5px;
    margin-left: 0px;
    margin-right: 0px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
    margin-left: 0px;
    margin-right: 0px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
  
</style>

<script type="text/javascript">
function callParent(info) {
	if(info != null && info!='') {
		window.close();
		window.opener.callChildWindowClose(info);
		
	}
		
}
callParent('${info}');

var eventInfo = "";
$(document).ready(function(){
	
	 //$('.autoArtist').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
	$('.autoArtist').autocomplete("${pageContext.request.contextPath}/Admin/AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		  //var rowId = $(this).attr('id').split("_")[1];
		  
			$('#tmatArtist').val('');
			$('#tmatArtistText').text(row[2]);
			$('#artistId').val(row[1]);
			$('#venueId').val('');
			$('#parentCatId').val('');
			$('#childCatId').val('');
			$('#grandChildCatId').val('');
			
	});
	
	
	$('.autoCategory').autocomplete("${pageContext.request.contextPath}/Client/AutoCompleteArtistAndVenueAndCategory", {
		width: 650,
		max: 1000,
		minChars: 2,		
		//dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='PARENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ALL'){
				return "";
			}
		}
	}).result(function (event,row,formatted){
		  //var rowId = $(this).attr('id').split("_")[1];
		  
			$('#categories').val('');
			
			if(row[0]=='PARENT'){
				$('#parentCatId').val(row[1]);
				$('#artistId').val('');
				$('#venueId').val('');
				$('#childCatId').val('');
				$('#grandChildCatId').val('');
			}else if(row[0]=='CHILD'){
				$('#childCatId').val(row[1]);
				$('#artistId').val('');
				$('#venueId').val('');
				$('#parentCatId').val('');
				$('#grandChildCatId').val('');
			}else if(row[0]=='GRAND'){
				$('#grandChildCatId').val(row[1]);
				$('#artistId').val('');
				$('#venueId').val('');
				$('#parentCatId').val('');
				$('#childCatId').val('');
			}else if(row[0]=='ARTIST'){
				$('#artistId').val(row[1]);
				$('#venueId').val('');
				$('#parentCatId').val('');
				$('#childCatId').val('');
				$('#grandChildCatId').val('');
			}else if(row[0]=='VENUE'){
				$('#venueId').val(row[1]);				
				$('#artistId').val('');
				$('#parentCatId').val('');
				$('#childCatId').val('');
				$('#grandChildCatId').val('');
			}
			$('#categoryName').val(row[0]);
			$('#categoryText').text(row[0]+" : "+row[2]);
			
	});
	 
	 $('input[type=radio][name=eventInfo]').change(function() {
			eventInfo = this.value;
		if (this.value == 'Yes') {
			$('#noOfEventsDiv').show();
		}else if(this.value == 'No'){
			$('#noOfEventsDiv').hide();
			$('#noOfEvents').val('');
		}
	 });
});


var validFilesTypes = ["jpg", "jpeg"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
            file.value = null;
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
            file.value = null;
            jAlert("File Size Should be Greater than 0 and less than 1 MB.");
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null) {
    		jAlert("Please select valid file to update card.");
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callCardTypeChange(obj) {
	if(obj.value=='PROMOTION') {
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		
		$('#mobileImageFileDiv').hide();
		$('#cardMobileImageDiv').hide();
		
		$('#cardDetailDiv').show();
		$('#tmatArtistDiv').show();
		$('#eventInfoDiv').show();
		$('#searchCategoryDiv').hide();
		$('#categoryName').val('');
		
	} else if(obj.value=='MAINPROMOTION') {
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		
		$('#mobileImageFileDiv').show();
		$('#cardMobileImageDiv').hide();
		$('#mobileFileRequired').val('Y');
		
		$('#cardDetailDiv').show();
		$('#tmatArtistDiv').hide();
		$('#eventInfoDiv').hide();
		$('#noOfEventsDiv').hide();
		$('#noOfEvents').val('');
		$('#searchCategoryDiv').show();
		$('#categoryName').val('ARTIST');
		
	} else if(obj.value=='YESNO') {
		$('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		
		$('#mobileImageFileDiv').hide();
		$('#cardMobileImageDiv').hide();
		$('#mobileFileRequired').val('N');
		
		$('#cardDetailDiv').show();
		$('#tmatArtistDiv').hide();
		$('#eventInfoDiv').hide();
		$('#noOfEventsDiv').hide();
		$('#noOfEvents').val('');
		$('#searchCategoryDiv').hide();
		$('#categoryName').val('');
		$('#artistId').val('');
		$('#venueId').val('');
		$('#parentCatId').val('');
		$('#childCatId').val('');
		$('#grandChildCatId').val('');
		
	} else {
		$('#cardDetailDiv').hide();
		$('#tmatArtistDiv').hide();
		$('#eventInfoDiv').hide();
		$('#noOfEventsDiv').hide();
		$('#noOfEvents').val('');
		$('#searchCategoryDiv').hide();
		$('#categoryName').val('');
		$('#artistId').val('');
		$('#venueId').val('');
		$('#parentCatId').val('');
		$('#childCatId').val('');
		$('#grandChildCatId').val('');
		$('#fileRequired').val('N');
		$('#mobileFileRequired').val('N');
	}
}

function validateForm() {
var flag = true;

	var cardType = document.getElementById("cardType");
	if(cardType == null || cardType.value=='') {
		jAlert('Please select Cardtype to update card.');
		flag = false;
		return false;
	}
	
	var fileRequired = $('#fileRequired').val();
	var mobileFileRequired = $('#mobileFileRequired').val();
	var fileObj = document.getElementById("file");
	var mobileFileObj = document.getElementById("mobileFile");
	if(cardType.value == 'PROMOTION') {
		if(fileRequired == '' || fileRequired == 'Y') {
			flag = CheckFile(fileObj);
		}		
		
		if(flag) {
			
			var imageTextObj = document.getElementById("imageText");
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please enter Valid Image Text to update promotion card.');
				flag = false;
				return false;
			}
			var tmatArtistIdObj = document.getElementById("artistId");
			if(tmatArtistIdObj == null || tmatArtistIdObj.value=='') {
				jAlert('Please select Valid TMAT artist to update promotion card.');
				flag = false;
				return false;
			}			
			var showEventData = $("input[name='eventInfo']:checked").val();
			if(showEventData == null || showEventData == ''){
				jAlert('Please select Show Event Data to update promotion card.');
				flag = false;
				return false;
			}
			var noOfEvents = $('#noOfEvents').val();
			if(showEventData == 'Yes'){				
				if(noOfEvents == null || noOfEvents == ''){
					jAlert('Please Enter No.Of Events to update promotion card.');
					flag = false;
					return false;
				}
			}
		}
		
	} else if(cardType.value == 'MAINPROMOTION') {
		if(fileRequired == '' || fileRequired == 'Y') {
			flag = CheckFile(fileObj);
		}
		if(mobileFileRequired == '' || mobileFileRequired == 'Y') {
			flag = CheckFile(mobileFileObj);
		}
		
		if(flag) {
			var imageTextObj = document.getElementById("imageText");
			/* if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please enter Valid Image Text to update Main Promotion Card.');
				flag = false;
				return false;
			} */
			var artistIdObj = document.getElementById("artistId");
			var venueIdObj = document.getElementById("venueId");
			var parentCategoryIdObj = document.getElementById("parentCatId");
			var childCategoryIdObj = document.getElementById("childCatId");
			var grandChildCategoryIdObj = document.getElementById("grandChildCatId");
			if((artistIdObj == null || artistIdObj.value == '') && 
				(venueIdObj == null || venueIdObj.value == '') && 
				(parentCategoryIdObj == null || parentCategoryIdObj == '') && 
				(childCategoryIdObj == null || childCategoryIdObj == '') && 
				(grandChildCategoryIdObj == null || grandChildCategoryIdObj == '')) {
					jAlert('Please select Valid Search Category to update Main Promotion Card.');
					flag = false;
					return false;
			}
		}
	} else if(cardType.value == 'YESNO') {
		if(fileRequired == '' || fileRequired == 'Y') {
			flag = CheckFile(fileObj);
		}
		
		if(flag) {
			var imageTextObj = document.getElementById("imageText");
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please Enter valid image text to update Yes/No card.');
				flag = false;
				return false;
			}
		}
	}
	if(flag) {
		var regex = /[0-9]|\./;
		var desktopPosition = document.getElementById("desktopPosition").value;
		if(desktopPosition=='' || !regex.test(desktopPosition)) {
			jAlert('Please Enter valid desktop position.');
			flag = false;
			return false;
		} 
		
		var mobilePosition = document.getElementById("mobilePosition").value;
		if(mobilePosition=='' || !regex.test(mobilePosition)) {
			jAlert('Please Enter valid mobile position.');
			flag = false;
			return false;
		} 
	}
	return flag;
}


function callSaveBtnOnClick() {
	
	var flag = validateForm();
	
	if(flag) {
		var cardId = $("#id").val();
		var mobilePosition = document.getElementById("mobilePosition").value;
		var desktopPosition = document.getElementById("desktopPosition").value;
		var isExistingCard = checkExistingCard(cardId,desktopPosition,mobilePosition);
		if(isExistingCard) {
			flag = false;
			return false;
		}	
	}

	
}
function callSubmitForm(action) {
	$("#action").val(action);
	$("#fileForm").attr('action', '${pageContext.request.contextPath}/RewardTheFan/updateCards');
	$("#fileForm").submit();
}

	function checkExistingCard(cardId,desktopPosition,mobilePosition) {
		var productId=$("#productId").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/CheckCardPosition",
			type : "post",
			data : "productId="+productId+"&cardId="+ cardId+"&dPosition="+desktopPosition+"&mPosition="+mobilePosition,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.msg=="No Card Exist for this Positions.") {
					callSubmitForm('update');
					return false;					
				} else {
					jAlert(jsonData.msg);
					return true;
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return true;
			}
		});
		return true;
	}

	function callDeleteBtnOnClick() {
		jConfirm("Are you sure you want to delete this card ?","Confirm",function(r){
		if(r){
			callSubmitForm('delete');
		}else{}
		});
	}
	
function callModifyBtnOnClick() {
	$('#imageRow').show();
	$('#cardImageRow').hide();
	var cardType = document.getElementById("cardType");
	callCardTypeChange(cardType,rowNumber);
}

function callChangeImage(){
	$('#imageFileDiv').show();
	$('#cardImageDiv').hide();
	$('#fileRequired').val('Y');
}

function callChangeMobileImage(){
	$('#mobileImageFileDiv').show();
	$('#cardMobileImageDiv').hide();
	$('#mobileFileRequired').val('Y');
}
</script>


<div class="container">
    <div class="row clearfix">
		<div class="full-width column">
		<form role="form" name="fileForm" enctype="multipart/form-data" id="fileForm" method="post" modelAttribute="cards" action="updateCards">
			<input type="hidden" name="action" id="action" value="action"/>
			<input type="hidden" name="productId" id="productId" value="${product.id}"/>
			<div class="table-responsive card-table">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-3">
							Card Type
						</th>
						<th class="col-lg-4">
							Card Details
						</th>
						<th class="col-lg-3">
							Category Details
						</th>
						<th class="col-lg-1">
							Desktop Position
						</th>
						<th class="col-lg-1">
							Mobile Position
						</th>
					</tr>
				</thead>
				<tbody>
                      <tr id="imageRow">
						<td style="vertical-align: middle;  ">
						<input type="hidden" name="id" id="id" value="${card.id}" />
						<input type="hidden" name="fileRequired" id="fileRequired" value="N" />
						<input type="hidden" name="mobileFileRequired" id="mobileFileRequired" value="N" />
						<input type="hidden" name="imageFileUrl" id="imageFileUrl" value="${card.imageFileUrl}"/>
						<input type="hidden" name="mobileImageFileUrl" id="mobileImageFileUrl" value="${card.mobileImageFileUrl}"/>
						<select name="cardType" id="cardType" class="form-control input-sm m-bot15" onchange="callCardTypeChange(this);">
                        	<option value="">--- Select ---</option>
							<option value="UPCOMMINGEVENT" <c:if test="${card.cardType eq 'UPCOMMINGEVENT'}">selected</c:if> >UpComming Event Card</option>
							<option value="MAINPROMOTION" <c:if test="${card.cardType eq 'MAINPROMOTION'}">selected</c:if>>Main Promotion Card</option>
							<option value="PROMOTION" <c:if test="${card.cardType eq 'PROMOTION'}">selected</c:if>>Promotion Card</option>							
							<option value="YESNO" <c:if test="${card.cardType eq 'YESNO'}">selected</c:if> >Yes/No Card</option>
                       </select>
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="text-left" id="cardDetailDiv" 
							<c:if test="${(card.cardType eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO' and card.cardType ne 'MAINPROMOTION'))}">style="display: none;" </c:if>>
                                  <div class="form-group form-group-top full-width" id="imageFileDiv" 
                                  <c:if test="${(card.id ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO' or card.cardType eq 'MAINPROMOTION'))}">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-md-3 col-sm-12 col-xs-12 control-label">Image File</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                          <input type="file" id="file" name="file">
                                      </div>
                                  </div>
                                   <div class="form-group form-group-top full-width" id="cardImageDiv" 
                                   <c:if test="${(card.id eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO' and card.cardType ne 'MAINPROMOTION'))}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${card.cardType ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO' or card.cardType eq 'MAINPROMOTION')}">
											Desktop Image
                                          <img src='${api.server.url}GetImageFile?type=rtwCards&filePath=${card.imageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeImage();">
											Change Image</a>
                                         </c:if>
                                      </div>                                      
                                  </div>								  
                                  <div class="form-group form-group-top full-width" id="mobileImageFileDiv" 
                                  <c:if test="${(card.id ne null and (card.cardType ne 'PROMOTION' or card.cardType ne 'YESNO' or card.cardType eq 'MAINPROMOTION'))}">style="display: none;" </c:if>>
                                      <label for="imageFile" class="col-md-3 col-sm-12 col-xs-12 control-label">Mobile Image</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                          <input type="file" id="mobileFile" name="mobileFile">
                                      </div>
                                  </div>
                                  <div class="form-group form-group-top full-width" id="cardMobileImageDiv" 
                                   <c:if test="${(card.id eq null and card.cardType ne 'MAINPROMOTION')}">style="display: none" </c:if>>
                                      <div class="col-lg-12" >
                                       <c:if test="${card.cardType ne null and (card.cardType eq 'MAINPROMOTION')}">
											Mobile Image
                                          <img src='${api.server.url}GetImageFile?type=rtwCards&filePath=${card.mobileImageFileUrl}' height="150" width="400" />
                                          <a style="color:blue " href="javascript:callChangeMobileImage();">
											Change Image</a>
                                         </c:if>
                                      </div>                                      
                                  </div>
                                  <div class="form-group full-width" id="imageTextDiv">
                                      <label for="imageText" class="col-md-3 col-sm-12 col-xs-12 control-label">Image Text </label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                      	<input class="form-control input-sm m-bot15" type="text" name="imageText" id="imageText" value="${card.imageText}" size="40" >
                                      </div>
                                  </div>
								  <div class="form-group full-width" id="searchCategoryDiv"
                                  	<c:if test="${card.cardType eq null or card.cardType ne 'MAINPROMOTION'}">style="display: none;" </c:if>>
                                      <label for="categoryTxt" class="col-md-3 col-sm-12 col-xs-12 control-label">Artist/Venue/ Category</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                          <input class="form-control input-sm m-bot15 autoCategory" type="text" name="categories" id="categories" placeholder="Search Categories">
                                          <!--<input type="hidden" id="artistId" name="artistId" value ="${card.artistId}">-->
										  <input type="hidden" id="venueId" name="venueId" value ="${card.venueId}">
										  <input type="hidden" id="parentCatId" name="parentCatId" value ="${card.parentCatId}">
										  <input type="hidden" id="childCatId" name="childCatId" value ="${card.childCatId}">
										  <input type="hidden" id="grandChildCatId" name="grandChildCatId" value ="${card.grandChildCatId}">
										  <input type="hidden" id="categoryName" name="categoryName" value ="${card.categoryName}">
                                          <b><label for="image1Text" class="text-left pull-left selected-artist" id="categoryText">
										  <c:if test="${card.artist ne null}">ARTIST : ${card.artist.name}</c:if>
										  <c:if test="${card.venue ne null}">VENUE : ${card.venue.building}</c:if>
										  <c:if test="${card.parentCategory ne null}">PARENT : ${card.parentCategory.name}</c:if>
										  <c:if test="${card.childCategory ne null}">CHILD : ${card.childCategory.name}</c:if>
										  <c:if test="${card.grandChildCategory ne null}">GRAND : ${card.grandChildCategory.name}</c:if>
										  </label></b>
                                      </div>
                                  </div>
                                  <div class="form-group full-width" id="tmatArtistDiv"
                                  	<c:if test="${card.cardType eq null or card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
                                      <label for="tmatArtist" class="col-md-3 col-sm-12 col-xs-12 control-label">TMAT Artist</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                          <input class="form-control input-sm m-bot15 autoArtist" type="text" name="tmatArtist" id="tmatArtist" placeholder="Search TMAT artist">
                                          <input type="hidden" id="artistId" name="artistId" value ="${card.artistId}">
                                          <b><label for="imageText" class="text-left pull-left selected-artist" id="tmatArtistText"><c:if test="${card.artist ne null}">${card.artist.name}</c:if></label></b>
                                      </div>
                                  </div>
                                  <div class="form-group full-width" id="eventInfoDiv"
                                  	<c:if test="${card.cardType eq null or card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
                                      <label for="showEvent" class="col-md-3 col-sm-12 col-xs-12 control-label">Show Event Data</label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                      	  <input type="radio" name="eventInfo" id="eventInfo" value="Yes" <c:if test="${card.showEventData ne null and card.showEventData eq true}">checked</c:if> >Yes
                                      	  <input type="radio" name="eventInfo" id="eventInfo" value="No" <c:if test="${card.showEventData ne null and card.showEventData eq false}">checked</c:if> >No                                          
                                      </div>
                                  </div>
                                  <div class="form-group full-width" id="noOfEventsDiv"
                                    <c:if test="${card.cardType eq null or card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>
									<c:if test="${card.showEventData ne null and card.showEventData eq false}">style="display: none;"</c:if>>
                                      <label for="noOfEvents" class="col-md-3 col-sm-12 col-xs-12 control-label">No. Of Events </label>
                                      <div class="col-md-9 col-sm-12 col-xs-12">
                                      	<input class="form-control input-sm m-bot15" type="text" name="noOfEvents" id="noOfEvents" value="${card.noOfEvents}" size="40" >
                                      </div>
                                  </div>
                              </div>
						</td>
						<td style="vertical-align: middle;  ">
						<select name="categoryType" id="categoryType" class="form-control input-sm m-bot15">
                        	<option value="">--- Select ---</option>
                        	<option value ="HOMEPAGE" selected> Home Page </option>
                        	<c:forEach var="parentCategory" items="${parentCategoryList}">
         						<option value="pc_${parentCategory.id}" <c:if test="${card.parentCategoryId eq parentCategory.id}">selected</c:if> ><c:out value="${parentCategory.name}"/></option>
     						</c:forEach>
     						<c:forEach var="childCategory" items="${childCategoryList}">
         						<option value="cc_${childCategory.id}" <c:if test="${card.childCategoryId eq childCategory.id}">selected</c:if> ><c:out value="${childCategory.name}"/></option>
     						</c:forEach>
     						<c:forEach var="grandChildCategory" items="${grandChildCategoryLsit}">
         						<option value="gc_${grandChildCategory.id}" <c:if test="${card.grandChildCategoryId eq grandChildCategory.id}">selected</c:if> ><c:out value="${grandChildCategory.name}"/></option>
     						</c:forEach>							
	                   </select>
						</td>
						<td style="vertical-align: middle;">
                            	<input class="form-control input-sm m-bot15" type="text" name="desktopPosition" size="4" id="desktopPosition" value="${card.desktopPosition}">
                            	<div id="imgContainer"></div>
						</td>
						<td style="vertical-align: middle;">
                            	<input class="form-control input-sm m-bot15" type="text" name="mobilePosition" size="4" id="mobilePosition" value="${card.mobilePosition}">
                            	<div id="imgContainer"></div>
						</td>
						
						</tr>
				</tbody>
			</table>
			</div>
			<div class="full-width form-group">
                <div class="col-lg-offset-5 col-lg-7">
                	<button type="button" class="btn btn-primary" onclick="callSaveBtnOnClick();">Save</button>
                	<c:if test="${card.id ne null }">
                    	<button type="button" class="btn btn-danger" onclick="callDeleteBtnOnClick();">Delete</button>
                    </c:if>
                    <button type="button" class="btn btn-default" onclick="window.close();">Close</button>
                    
                </div>
            </div>
           </form>
		</div>
		</div>
		</div>



