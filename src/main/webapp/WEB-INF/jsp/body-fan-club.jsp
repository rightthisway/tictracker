<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page import="com.rtw.tracker.datas.TrackerUser"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Manage Fan Clubs</title>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.columnpicker1.css"
	type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script
	src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>


<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage {
	cursor: pointer;
	vertical-align: middle;
}

img.deleteClickableImage {
	cursor: pointer;
	vertical-align: middle;
}

img.auditClickableImage {
	cursor: pointer;
	vertical-align: middle;
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#eventContextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#eventContextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#eventContextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>

<script type="text/javascript">	
$(document).ready(function(){
	
	$('#eventDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	$('#FCMemberTab').click(function(){		
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId > 0){
			setTimeout(function(){
				getFanClubMemberGridData(fanClubId,0);
			});
		}else{
			jAlert("Please select Fan club to view member details.");
			return;
		}
	});
	
	$('#FCPostTab').click(function(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId > 0){
			setTimeout(function(){
				getFanClubPostGridData(fanClubId,0);
			});
		}else{
			jAlert("Please select Fan club to view post details.");
			return;
		}
	});
	
	$('#FCEventTab').click(function(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId > 0){
			setTimeout(function(){
				getFanClubEventGridData(fanClubId,0);
			});
		}else{
			jAlert("Please select Fan club to view event details.");
			return;
		}
	});
	
	$('#FCVideoTab').click(function(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId > 0){
			setTimeout(function(){
				getFanClubVideoGridData(fanClubId,0);
			});
		}else{
			jAlert("Please select Fan club to view video details.");
			return;
		}
	});
	
	$('#activeCommentsTab').click(function(){		
		viewPostComments('ACTIVE',1);
	});
	$('#blockedCommentsTab').click(function(){		
		viewPostComments('BLOCKED',1);
	});
	
	$('#activeVideoCommentsTab').click(function(){		
		viewVideoComments('ACTIVE',1);
	});
	$('#blockedVideoCommentsTab').click(function(){		
		viewVideoComments('BLOCKED',1);
	});
		
	$(window).load(function() {
		<c:choose>
			<c:when test="${status == 'ACTIVE'}">	
				$('#activeFanClub').addClass('active');
				$('#activeFanClubTab').addClass('active');
			</c:when>
			<c:when test="${status == 'DELETED'}">
				$('#deletedFanClub').addClass('active');
				$('#deletedFanClubTab').addClass('active');
			</c:when>
		</c:choose>
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		memberPagingInfo = JSON.parse(JSON.stringify(${memberPagingInfo}));
		refreshFanClubGridValues(JSON.parse(JSON.stringify(${clubs})));
		refreshMemberGridValues('');
		$('#fanClub_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserFanClubPreference()'>");
		$('#member_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserMemberPreference()'>");
		
	});
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  fanClubGrid.resizeCanvas();	  
	  if(memberGrid!=null && memberGrid != undefined){
		  memberGrid.resizeCanvas();
	  }
	  if(postGrid != null && postGrid != undefined){
		  postGrid.resizeCanvas();
	  }
	  if(eventGrid != null && eventGrid != undefined){
		  eventGrid.resizeCanvas();
	  }
	  if(videoGrid != null && videoGrid != undefined){
		  videoGrid.resizeCanvas();
	  }
	});
	
	$("#activeFanClub1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#deletedFanClub1").click(function(){
		callTabOnChange('DELETED');
	});
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ManageFanClubs?status="+selectedTab;
}


function exportToExcel(){	
	var appendData ="headerFilter="+fanClubGridSearchString;
    var url = apiServerUrl+"FanClubsExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	fanClubGridSearchString='';
	sortingString ='';
	columnFilters = {};
	
	getFanClubGridData(0);
}
</script>

</head>
<body>
	<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="Edit Member">View</li>
  <li data="Create Invoice">Create Invoice</li>
</ul>
<ul id="eventContextMenu" style="display:none;position:absolute">
  <li data="View Sold Members">View Sold Members</li>
  <li data="View Zones Map/Members">View Zones Map/Members</li>
  <li data="BroadCast Members">BroadCast All Members</li>
  <li data="UnBroadCast Members">UnBroadCast All Members</li>
</ul> -->
	<div class="row">
		<iframe id="download-frame" src="" width="1" height="1"
			style="display: none"></iframe>
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="#">Fan Club</a></li>
				<li><i class="fa fa-laptop"></i>Manage Fan Club</li>
			</ol>

		</div>
	</div>

	<!-- fanClub Grid Code Starts -->
	<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeFanClubTab" class=""><a id="activeFanClub1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeFanClub">Active FanClubs</a></li>
				<li id="deletedFanClubTab" class=""><a id="deletedFanClub1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#deletedFanClub">Removed/Blocked FanClubs</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeFanClub" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addFanClubBtn" type="button" data-toggle="modal" onclick="resetFabClubModal();">Add New FanClub</button>
					<button class="btn btn-primary" id="editFanClubBtn" type="button" data-toggle="modal"  onclick="editFanClub()">Edit FanClub</button>
					<button class="btn btn-primary" id="deleteFanClubBtn" type="button" onclick="deleteFanClub()">Block/Remove FanClub</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fan Clubs</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel()" name='Export to Excel'
									style='float: right; margin-right: 10px;'>Export to Excel</a> <a
									href="javascript:resetFilters()" name='Reset Filters'
									style='float: right; margin-right: 10px;'>Reset Filters &nbsp;
									|</a>
							</div>
						</div>
						<div id="fanClub_grid"
							style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="fanClub_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="deletedFanClub" class="tab-pane">
			<c:if test="${status =='DELETED'}">	
				<br />
				<div style="position: relative" id="giftCardGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fan Clubs</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel()" name='Export to Excel'
									style='float: right; margin-right: 10px;'>Export to Excel</a> <a
									href="javascript:resetFilters()" name='Reset Filters'
									style='float: right; margin-right: 10px;'>Reset Filters &nbsp;
									|</a>
							</div>
						</div>
						<div id="fanClub_grid"
							style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="fanClub_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
		</div>
	</div>
</div>
	
	<br />
	<div id="fanClubTabsDiv" >
		<div class="full-width">
			<section class="invoice-panel panel">
				<ul class="nav nav-tabs" style="">
					<li id="FCmemberTab" class="active"><a id="member1"
						style="font-size: 13px; font-family: arial;" data-toggle="tab"
						href="#member2">Members</a></li>
					<li id="FCPostTab" class=""><a id="post1"
						style="font-size: 13px; font-family: arial;" data-toggle="tab"
						href="#post2">Posts</a>
					<li id="FCEventTab" class=""><a id="event1"
						style="font-size: 13px; font-family: arial;" data-toggle="tab"
						href="#event2">Events</a></li>
					<li id="FCVideoTab" class=""><a id="video1"
						style="font-size: 13px; font-family: arial;" data-toggle="tab"
						href="#video2">Video</a></li>
				</ul>
			</section>
		</div>
		<div class="panel-body1 full-width">
			<div class="tab-content">
				<div id="member2" class="tab-pane active">
					<c:if test="${status =='ACTIVE'}">	
					<div class="full-width mb-20 full-width-btn" id="actionButton">
					 	<button type="button" class="btn btn-primary" id="removeMemberBtn" onclick="removeFanClubMember();">Block/Remove Member</button>
					</div>
					</c:if>
					<br /> <br /> <br />
					<div style="position: relative" id="memberGridDiv">
						<div class="table-responsive grid-table long-width grid-table-2">
							<div class="grid-header full-width">
								<label id="memberGridHeaderLabel">Fan Club Members</label>
								<div class="pull-right">
									<a href="javascript:memberExportToExcel()"
										name='Export to Excel'
										style='float: right; margin-right: 10px;'>Export to Excel</a>
									<a href="javascript:memberResetFilters()" name='Reset Filters'
										style='float: right; margin-right: 10px;'>Reset Filters
										&nbsp; |</a>
								</div>
							</div>
							<div id="member_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
							<div id="member_pager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>
				</div>

				<div id="post2" class="tab-pane">
					<c:if test="${status =='ACTIVE'}">	
					<div class="full-width mb-20 full-width-btn" id="actionButton">
						<button type="button" class="btn btn-primary" id="addPostBtn" data-toggle="modal" onclick="resetPostModal();">Add New Post</button>	
						<button type="button" class="btn btn-primary" id="editPostBtn" data-toggle="modal" onclick="editPost();">Edit Post</button>	
						<button type="button" class="btn btn-primary" id="removePostBtn" onclick="removePost();">Block/Remove Post</button>								
						<button type="button" class="btn btn-primary" id="viewPostCommentBtn" data-toggle="modal" onclick="viewPostComments('ACTIVE',1);">View Post Comments</button>
					</div> 
					</c:if>
					<br /> <br />
					<div style="position: relative" id="postGridDiv">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label id="postGridHeaderLabel">Fan Club Posts</label>
								<div class="pull-right">
									<a href="javascript:postExportToExcel()" name='Export to Excel'
										style='float: right; margin-right: 10px;'>Export to Excel</a>
									<a href="javascript:postResetFilters()" name='Reset Filters'
										style='float: right; margin-right: 10px;'>Reset Filters
										&nbsp; |</a>
								</div>
							</div>
							<div id="post_grid"
								style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
							<div id="post_pager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>
				</div>

				<div id="event2" class="tab-pane">
					<c:if test="${status =='ACTIVE'}">	
					<div class="full-width mb-20 full-width-btn" id="actionButton">
						<button type="button" class="btn btn-primary" id="addEventBtn" data-toggle="modal" onclick="resetEventModal();">Add New Event</button>	
						<button type="button" class="btn btn-primary" id="editEventBtn" data-toggle="modal" onclick="editEvent();">Edit Event</button>	
						<button type="button" class="btn btn-primary" id="removeEventBtn" data-toggle="modal" onclick="removeEvent();">Block/Remove Event</button>								
					</div>
					</c:if>
					<br /> <br />
					<div style="position: relative" id="eventGridDiv">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label id="lockedGridHeaderLabel">Fan Club Events</label>
								<div class="pull-right">
									<a href="javascript:fanClubExportToExcel()"
										name='Export to Excel'
										style='float: right; margin-right: 10px;'>Export to Excel</a>
									<a href="javascript:eventResetFilters()" name='Reset Filters'
										style='float: right; margin-right: 10px;'>Reset Filters
										&nbsp; |</a>
								</div>
							</div>
							<div id="event_grid"
								style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
							<div id="event_pager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>
				</div>

				<div id="video2" class="tab-pane">
					<c:if test="${status =='ACTIVE'}">	
					<div class="full-width mb-20 full-width-btn" id="actionButton">
						<button type="button" class="btn btn-primary" id="addVideoBtn" data-toggle="modal" onclick="resetVideoModal();">Add New Video</button>	
						<button type="button" class="btn btn-primary" id="editVideoBtn" data-toggle="modal" onclick="editVideo();">Edit Video</button>	
						<button type="button" class="btn btn-primary" id="removeVideoBtn"  onclick="removeVideo();">Block/Remove Video</button>	
						<!-- <button type="button" class="btn btn-primary" id="viewVideoCommentBtn" onclick="viewVideoComments()">View Video Comments</button>	 -->
						<button type="button" class="btn btn-primary" id="viewVideoCommentBtn" data-toggle="modal" onclick="viewVideoComments('ACTIVE',1);">View Video Comments</button>						
					</div>
					</c:if>
					<br /> <br />
					<div style="position: relative" id="videoGridDiv">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label id="soldGridHeaderLabel">Fan Club Videos</label>
								<div class="pull-right">
									<a href="javascript:videoExportToExcel()"
										name='Export to Excel'
										style='float: right; margin-right: 10px;'>Export to Excel</a>
									<a href="javascript:videoResetFilters()" name='Reset Filters'
										style='float: right; margin-right: 10px;'>Reset Filters
										&nbsp; |</a>
								</div>
							</div>
							<div id="video_grid"
								style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
							<div id="video_pager" style="width: 100%; height: 20px;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
<!-- FANCLUB MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fanClubModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Fan Club</h4>
			</div>
			<div class="modal-body full-width">
				<form name="fanClubForm" id="fanClubForm" method="post">
					<input type="hidden" id="fanClubId" name="fanClubId" />
					<input type="hidden" id="fanClubAction" name="action" />
					
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="fanClubTitle" name="title">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="fanClubDescription" name="description">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label><strong>Category</strong><span class="required">*</span></label> 
							<select name="category" id="fanClubCategory" class="form-control" >
								 <c:forEach items="${categories}" var="cat">
									<option value="${cat.id}" >${cat.categoryName}</option>
								</c:forEach>
							</select>
						</div>
						<div id="fanClubImageDiv" class="form-group col-sm-4 col-xs-4">
							<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 			<input type="file" id="fanClubimageFile" name="imageFile">
             			</div>
					</div>
					<div class="form-group form-group-top" id="fanClubImageDispDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="fanClubImageDisp" src="" height="150" width="400" />
               				 <a style="color:blue " href="javascript:callChangeFanClubImage();">Change Image</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveFanClubBtn" type="button" onclick="saveFanClub('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateFanClubBtn" type="button" onclick="saveFanClub('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- POST MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="postModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Fan Club Post</h4>
			</div>
			<div class="modal-body full-width">
				<form name="postForm" id="postForm" method="post">
					<input type="hidden" id="postId" name="postId" />
					<input type="hidden" id="postFanClubId" name="fanClubId" />
					<input type="hidden" id="postAction" name="action" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Post Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="postText" name="postText">
						</div>
						<!-- <div id="postImageDiv" class="form-group col-sm-4 col-xs-4">
							<label for="imageFile" class="col-lg-3 control-label">Post Image</label>
                 			<input type="file" id="postImage" name="imageFile">
             			</div>
             			<div id="postVideoDiv" class="form-group col-sm-4 col-xs-4">
							<label for="videoFile" class="col-lg-3 control-label">Post Video</label>
                 			<input type="file" id="postVIdeoFile" name="videoFile">
             			</div>
             			<div id="dispPostVideoSpan" class="form-group col-sm-4 col-xs-4">
							<a style="color:blue" id="previewPostVideo" href=""  target="_blank">Video Preview</a><br/><a style="color:blue " href="javascript:callChangePostVideo();">Change Video</a>
             			</div>
             			<div id="postThumbDiv" class="form-group col-sm-4 col-xs-4">
							<label for="postThumbnail" class="col-lg-3 control-label">Thumbnail Image</label>
                 			<input type="file" id="postThumbnail" name="thumbnailImage">
             			</div> -->
					</div>
					<!-- <div class="form-group form-group-top" id="postImageDispDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="postImageDisp" src="" height="150" width="400" />
               				 <a style="color:blue " href="javascript:callChangePostImage();">Change Image</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
    				<div class="form-group form-group-top" id="postThumbDispDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <a style="color:blue" id="previewPostVideo" href=""><img id="postThumbDisp" src="" height="150" width="400" /></a>
               				 <a style="color:blue " href="javascript:callChangePostThubmnail();">Change Thumbnail</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div> -->
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="savePostBtn" type="button" onclick="savePost('SAVE')">Save</button>
				<button class="btn btn-primary" id="updatePostBtn" type="button" onclick="savePost('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- VIDEO MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="videoModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Fan Club Video</h4>
			</div>
			<div class="modal-body full-width">
				<form name="videoForm" id="videoForm" method="post">
					<input type="hidden" id="videoFanClubId" name="fanClubId" />
					<input type="hidden" id="videoId" name="videoId" />
					<input type="hidden" id="videoAction" name="action" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="videoTitle" name="title">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="videoDescription" name="description">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label><strong>Category</strong><span class="required">*</span></label> 
							<select name="category" id="videoCategory" class="form-control" >
								<c:forEach items="${categories}" var="cat">
									<option value="${cat.id}" >${cat.categoryName}</option>
								</c:forEach> 
							</select>
						</div>
						<div id="videoFileDiv" class="form-group col-sm-4 col-xs-4">
							<label for="videoFile" class="col-lg-3 ">Video</label>
                 			<input type="file" id="videoFile" name="videoFile">
             			</div>
             			<div id="dispVideoSpan" class="form-group col-sm-4 col-xs-4">
							<a style="color:blue" id="previewVideo" href="" target="_blank">Video Preview</a><br/><a style="color:blue " href="javascript:callChangeVideo();">Change Video</a>
             			</div>
						<div id="videoImageDiv" class="form-group col-sm-4 col-xs-4">
							<label for="imageFile" class="col-lg-3 control-label">Thumbnail Image</label>
                 			<input type="file" id="videoThumbnail" name="thumbnailImage">
             			</div>
					</div>
					<div class="form-group form-group-top" id="videoImageDispDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="videoImageDisp" src="" height="150" width="400" />
               				 <a style="color:blue " href="javascript:callChangeVideoImage();">Change Thumbnail</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveVideoBtn" type="button" onclick="saveVideo('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateVideoBtn" type="button" onclick="saveVideo('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- EVENT MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="eventModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Fan Club Event</h4>
			</div>
			<div class="modal-body full-width">
				<form name="eventForm" id="eventForm" method="post">
					<input type="hidden" id="eventId" name="eventId" />
					<input type="hidden" id="eventFanClubId" name="fanClubId" />
					<input type="hidden" id="eventAction" name="action" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Event Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="eventName" name="eventName">
						</div>
						<div class="form-group col-sm-8 col-xs-8">
							<label>Venue<span class="required">*</span>
							</label> <input class="form-control" type="text" id="venue" name="venue">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Event Date<span class="required">*</span>
							</label> <input class="form-control" type="text" id="eventDate" name="eventDate">
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Event Time <span class="required">*</span>
							<select name="eventHour" id="eventHour" class="form-control" style="width:120px;" >
								<option value="0">00</option>
								<option value="1">01</option>
								<option value="2">02</option>
								<option value="3">03</option>
								<option value="4">04</option>
								<option value="5">05</option>
								<option value="6">06</option>
								<option value="7">07</option>
								<option value="8">08</option>
								<option value="9">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Minutes <span class="required">*</span>
							<select name="eventMinute" id="eventMinute" class="form-control" style="width:120px;" >
								<option value="0">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
						</div>
						<div id="eventImageDiv" class="form-group col-sm-4 col-xs-4">
							<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 			<input type="file" id="eventimageFile" name="imageFile">
             			</div>
						</div>
						<div class="form-group form-group-top" id="eventImageDispDiv" style="width: 100%">
	            			<div class="col-lg-3"> &nbsp; </div>
								<div class="col-lg-6">
	               				 <img id="eventImageDisp" src="" height="150" width="400" />
	               				 <a style="color:blue " href="javascript:callChangeEventImage();">Change Image</a>
	           					 </div>
							<div class="col-lg-3">&nbsp;</div>
	    				</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveEventBtn" type="button" onclick="saveEvent('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateEventBtn" type="button" onclick="saveEvent('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>




<!-- Post comment Modal start  -->
	<div id="view-post-comments" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Post Comments : <span id="contestName_Hdr_QuesBank" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
			<div id="postCommentsTabsDiv" >
				<div class="full-width">
					<section class="invoice-panel panel">
						<ul class="nav nav-tabs" style="">
							<li id="activeCommentsTab" class="active"><a id="activeComment"
								style="font-size: 13px; font-family: arial;" data-toggle="tab"
								href="activeC">Active Comments</a></li>
							<li id="blockedCommentsTab" class=""><a id="blockedComment"
								style="font-size: 13px; font-family: arial;" data-toggle="tab"
								href="blockedC">Removed/Blocked Comments</a></li>
						</ul>
					</section>
				</div>
				<div class="tab-content">
					<div id="activeC" class="tab-pane active">
						<div class="full-width mb-20 full-width-btn" id="actionButton">
							<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="blockPostComment();">Remove/Block Comment</button>
							<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="blockPostCommentUser();">Block Customer</button>
						</div>
					</div>
					<div id="blockedC" class="tab-pane active">
						<div class="row">
						</div>
					</div>
				</div>
				<br />
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Poster Comments</label>
							<div class="pull-right">
								<!--<a href="javascript:winnerExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
								<a href="javascript:postCommentResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="postComment_grid" style="width: 100%; height: 200px; border:1px solid gray"></div>
						<div id="postComment_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="postId_comment" name="postId_comment" />
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	</div>
	<!-- Post comment Modal End  -->
	
	

<!-- Video comment Modal start  -->
	<div id="view-video-comments" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Video Comments : <span id="contestName_Hdr_QuesBank" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
			<div id="videoCommentsTabsDiv" >
				<div class="full-width">
					<section class="invoice-panel panel">
						<ul class="nav nav-tabs" style="">
							<li id="activeVideoCommentsTab" class="active"><a id="activeVideoCommentsTab"
								style="font-size: 13px; font-family: arial;" data-toggle="tab"
								href="activeVideoC">Active Comments</a></li>
							<li id="blockedVideoCommentsTab" class=""><a id="blockedVideoCommentsTab"
								style="font-size: 13px; font-family: arial;" data-toggle="tab"
								href="blockedVideoC">Removed/Blocked Comments</a></li>
						</ul>
					</section>
				</div>
				<div class="tab-content">
					<div id="activeVideoC" class="tab-pane active">
						<div class="full-width mb-20 full-width-btn" id="actionButton">
							<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="blockVideoComment();">Remove/Block Comment</button>
							<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="blockVideoCommentUser();">Block Customer</button>
						</div>
					</div>
					<div id="blockedVideoC" class="tab-pane active">
						<div class="row">
						</div>
					</div>
				</div>
				<br />
				<div class="full-width" style="position: relative">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Video Comments</label>
							<div class="pull-right">
								<!--<a href="javascript:winnerExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
								<a href="javascript:videoCommentResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="videoComment_grid" style="width: 100%; height: 200px; border:1px solid gray"></div>
						<div id="videoComment_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" id="videoId_comment" name="videoId_comment" />
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
	</div>
	<!-- Video comment Modal End  -->

	<!-- slickgrid related scripts -->

	<script type="text/javascript">
	function resetFabClubModal(){
		$('#fanClubModal').modal('show');
		$('#saveFanClubBtn').show();
		$('#updateFanClubBtn').hide();
		$('#fanClubImageDiv').show();
		$('#fanClubImageDispDiv').hide();
		$('#fanClubId').val('');
		$('#fanClubTitle').val('');
		$('#fanClubDescription').val('');
		$('#fanClubCategory').val('0');
		$('#fanClubimageFile').val('');
	}
	function editFanClub(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club record to edit it.");
			return;
		}else{
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateFanClubs.json",
				type : "post",
				dataType: "json",
				data: "fanClubId="+fanClubId+"&action=EDIT",
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){					
						$('#fanClubModal').modal('show');
						setEditFanClub(jsonData.club);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function setEditFanClub(data){
		$('#saveFanClubBtn').hide();
		$('#updateFanClubBtn').show();
		$('#fanClubId').val(data.id);
		$('#fanClubTitle').val(data.title);
		$('#fanClubDescription').val(data.description);
		$('#fanClubCategory').val(data.categoryId);
		$('#fanClubImageDispDiv').show();
		$('#fanClubImageDiv').hide();
		$('#fanClubImageDisp').attr("src",data.imageUrl);
	}
	
	function callChangeFanClubImage(){
		$('#fanClubImageDispDiv').hide();
		$('#fanClubImageDiv').show();
	}
	
	function saveFanClub(action){
		var title  = $('#fanClubTitle').val();
		var description  = $('#fanClubDescription').val();
		var category  = $('#fanClubCategory').val();
		var imageFile  = $('#fanClubimageFile').val();
		
		if(title == ''){
			jAlert("FanClub title is mendatory.");
			return;
		}
		if(description == ''){
			jAlert("FanClub description is mendatory.");
			return;
		}
		if(category == ''){
			jAlert("FanClub category is mendatory.");
			return;
		}
		if(imageFile == '' && action == 'SAVE'){
			jAlert("FanClub image is mendatory.");
			return;
		}
		$('#fanClubAction').val(action);
		var requestUrl = "${pageContext.request.contextPath}/UpdateFanClubs.json";
		var form = $('#fanClubForm')[0];
		var dataString = new FormData(form);
		
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#fanClubModal').modal('hide');
					pagingInfo = jsonData.pagingInfo;
					columnFilters = {};
					refreshFanClubGridValues(jsonData.clubs);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	function deleteFanClub(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club Block/Remove it.");
			return;
		}else{
			jConfirm("Are you sure to remove selected fan club?","Confirm",function(r){
				if (r) {
					$.ajax({
						url : "${pageContext.request.contextPath}/UpdateFanClubs.json",
						type : "post",
						dataType: "json",
						data: "fanClubId="+fanClubId+"&action=DELETE",
						success : function(response){				
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){					
								pagingInfo =jsonData.pagingInfo;
								columnFilters = {};
								refreshFanClubGridValues(jsonData.clubs);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
								return;
							}
						},
						error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		}
	}
	function removeFanClubMember(){
		var fanClubId =getSelectedFanClubGridId();
		var memberIds = getSelectedMemberGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club to Remove/Block Members.");
			return;
		}
		if(memberIds == undefined || memberIds == ''){
			jAlert("Please select Fan Club to Remove/Block Members.");
			return;
		}
		jConfirm("Are you sure to remove selected member from fab club?","Confirm",function(r){
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/RemoveFanClubMember.json",
					type : "post",
					dataType: "json",
					data: "fanClubId="+fanClubId+"&memberIds="+memberIds,
					success : function(response){				
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.status == 1){					
							memberPagingInfo = jsonData.pagingInfo;
							refreshMemberGridValues(jsonData.members);
							clearAllSelections();
							//$('#member_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserMemberPreference()'>");
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
							return;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});
	}
	function resetPostModal(){
		$('#postModal').modal('show');
		$('#savePostBtn').show();
		$('#updatePostBtn').hide();
		/* $('#postImageDiv').show();
		$('#postVideoDiv').show();
		$('#postThumbDiv').show();
		$('#dispPostVideoSpan').hide();
		$('#postImageDispDiv').hide();
		$('#postThumbDispDiv').hide(); */
		$('#postText').val('');
		$('#postId').val('');
		//$('#postVIdeoFile').val('');
		//$('#postImage').val('');
	}
	
	function savePost(action){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Fan club id not found.");
			return;
		}
		var postText  = $('#postText').val();
		
		if(postText == ''){
			jAlert("Post text is mendatory.");
			return;
		}
		$('#postAction').val(action);
		$('#postFanClubId').val(fanClubId);
		var requestUrl = "${pageContext.request.contextPath}/UpdateFanClubPosts.json";
		var form = $('#postForm')[0];
		var dataString = new FormData(form);
		
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#postModal').modal('hide');
					postPagingInfo = jsonData.pagingInfo;					
					refreshPostValues(jsonData.posts);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function editPost(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club post record to edit it.");
			return;
		}
		var postRowIndex = postGrid.getSelectedRows([0])[0];
		if (postRowIndex < 0) {
			jAlert("Please select Fan Club post record to edit it.");
			return;
		}
		var postId =postGrid.getDataItem(postRowIndex).postId;
		
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateFanClubPosts.json",
			type : "post",
			dataType: "json",
			data: "fanClubId="+fanClubId+"&postId="+postId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#postModal').modal('show');
					setEditFanClubPost(jsonData.post);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function setEditFanClubPost(data){
		$('#savePostBtn').hide();
		$('#updatePostBtn').show();
		/* if(data.imageUrl == null || data.imageUrl ==''){
			$('#postImageDiv').show();
			$('#postImageDispDiv').hide();
		}else{
			$('#postImageDiv').hide();
			$('#postImageDispDiv').show();
		}
		if(data.videoUrl == null || data.videoUrl ==''){
			$('#postVideoDiv').show();
			$('#postThumbDiv').show();
			$('#dispPostVideoSpan').hide();
			$('#postThumbDispDiv').hide();
		}else{
			$('#postVideoDiv').hide();
			$('#postThumbDiv').hide();
			$('#dispPostVideoSpan').show();
			$('#postThumbDispDiv').show();
		} */
		
		
		
		$('#postText').val(data.postText);
		$('#postId').val(data.id);
		//$('#previewPostVideo').attr("href",data.videoUrl);
		//$('#postImageDisp').attr("src",data.imageUrl);
		//$('#postThumbDisp').attr("src",data.thumbnailUrl);
	}
	
	function callChangePostVideo(){
		$('#dispPostVideoSpan').hide();
		$('#postVideoDiv').show();
	}
	function callChangePostImage(){
		$('#postImageDispDiv').hide();
		$('#postImageDiv').show();
	}
	function callChangePostThubmnail(){
		$('#postThumbDispDiv').hide();
		$('#postThumbDiv').show();
	}
	
	
	function removePost(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club post record to delete it.");
			return;
		}
		var postRowIndex = postGrid.getSelectedRows([0])[0];
		if (postRowIndex < 0) {
			jAlert("Please select Fan Club post record to delete it.");
			return;
		}
		var postId =postGrid.getDataItem(postRowIndex).postId;
		jConfirm("Are you sure to remove selected post from fan club?","Confirm",function(r){
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/UpdateFanClubPosts.json",
					type : "post",
					dataType: "json",
					data: "fanClubId="+fanClubId+"&postId="+postId+"&action=DELETE",
					success : function(response){				
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.status == 1){					
							postPagingInfo = jsonData.pagingInfo;					
							refreshPostValues(jsonData.posts);
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
							return;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});
		
	}
	function resetEventModal(){
		$('#eventModal').modal('show');
		$('#saveEventBtn').show();
		$('#updateEventBtn').hide();
		$('#eventImageDiv').show();
		$('#eventImageDispDiv').hide();
		$('#eventName').val('');
		$('#venue').val('');
		$('#eventId').val('');
		$('#eventDate').val('');
		$('#eventHour').val('0');
		$('#eventMinute').val('0');
	}
	function saveEvent(action){
		var eventName  = $('#eventName').val();
		var venue  = $('#venue').val();
		var eventDate  = $('#eventDate').val();
		var eventHour  = $('#eventHour').val();
		var eventMinute  = $('#eventMinute').val();
		var eventimageFile  = $('#eventimageFile').val();
		
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Fan club id not found.");
			return;
		}
		if(eventName == ''){
			jAlert("Event Name is mendatory.");
			return;
		}
		if((eventimageFile == '' || eventimageFile == null) && action == 'SAVE'){
			jAlert("Event Image is mendatory.");
			return;
		}
		if(venue == ''){
			jAlert("Event Venue is mendatory.");
			return;
		}
		if(eventDate == ''){
			jAlert("Event Date is mendatory.");
			return;
		}
		if(eventHour == ''){
			jAlert("Event time hour is mendatory.");
			return;
		}
		if(eventMinute == ''){
			jAlert("Event time minute is mendatory.");
			return;
		}
		$('#eventAction').val(action);
		$('#eventFanClubId').val(fanClubId);
		var form = $('#eventForm')[0];
		var dataString = new FormData(form);
		
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateFanClubEvents.json",
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#eventModal').modal('hide');
					eventPagingInfo = jsonData.pagingInfo;
					refreshEventGridValues(jsonData.events);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function editEvent(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club Event record to edit it.");
			return;
		}
		var eventRowIndex = eventGrid.getSelectedRows([0])[0];
		if (eventRowIndex < 0) {
			jAlert("Please select Fan Club Event record to edit it.");
			return;
		}
		var eventId =eventGrid.getDataItem(eventRowIndex).eventId;
		
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateFanClubEvents.json",
			type : "post",
			dataType: "json",
			data: "fanClubId="+fanClubId+"&eventId="+eventId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#eventModal').modal('show');
					setEditFanClubEvent(jsonData.event);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function setEditFanClubEvent(data){
		$('#eventModal').modal('show');
		$('#saveEventBtn').hide();
		$('#updateEventBtn').show();
		$('#eventImageDiv').hide();
		$('#eventImageDispDiv').show();
		$('#eventName').val(data.eventName);
		$('#venue').val(data.venue);
		$('#eventId').val(data.id);
		$('#eventDate').val(data.dateStr);
		$('#eventHour').val(data.hours);
		$('#eventMinute').val(data.minutes);
		$('#eventImageDisp').attr('src',data.eventImage);
	}
	
	function callChangeEventImage(){
		$('#eventImageDiv').show();
		$('#eventImageDispDiv').hide();
	}
	
	function removeEvent(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club event record to delete it.");
			return;
		}
		var eventRowIndex = eventGrid.getSelectedRows([0])[0];
		if (eventRowIndex < 0) {
			jAlert("Please select Fan Club event record to delete it.");
			return;
		}
		var eventId =eventGrid.getDataItem(eventRowIndex).eventId;
		jConfirm("Are you sure to remove selected event from fan club?","Confirm",function(r){
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/UpdateFanClubEvents.json",
					type : "post",
					dataType: "json",
					data: "fanClubId="+fanClubId+"&eventId="+eventId+"&action=DELETE",
					success : function(response){				
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.status == 1){					
							eventPagingInfo = jsonData.pagingInfo;
							refreshEventGridValues(jsonData.events);
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
							return;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});
	}
	function resetVideoModal(){
		$('#videoModal').modal('show');
		$('#saveVideoBtn').show();
		$('#updateVideoBtn').hide();
		$('#videoFileDiv').show();
		$('#dispVideoSpan').hide();
		$('#videoImageDiv').show();
		$('#videoImageDispDiv').hide();
		$('#videoId').val('');
		$('#videoTitle').val('');
		$('#videoDescription').val('');
		$('#videoCategory').val('1');
		$('#videoFile').val('');
		$('#videoThumbnail').val('');
	}
	function saveVideo(action){
		var title  = $('#videoTitle').val();
		var description  = $('#videoDescription').val();
		var category  = $('#videoCategory').val();
		var videoFile  = $('#videoFile').val();
		
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Fan club id not found.");
			return;
		}
		if(title == ''){
			jAlert("VIdeo Title is mendatory.");
			return;
		}
		if(description == ''){
			jAlert("Video Description is mendatory.");
			return;
		}
		if(category == '0'){
			jAlert("Video Category is mendatory.");
			return;
		}
		if(videoFile == '' && action == 'SAVE'){
			jAlert("Video File is mendatory.");
			return;
		}
		
		$('#videoAction').val(action);
		$('#videoFanClubId').val(fanClubId);
		var form = $('#videoForm')[0];
		var dataString = new FormData(form);
		
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateFanClubVideos.json",
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#videoModal').modal('hide');
					videoPagingInfo = jsonData.pagingInfo;
					refreshVideoGridValues(jsonData.videos);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function editVideo(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club Video record to edit it.");
			return;
		}
		var videoRowIndex = videoGrid.getSelectedRows([0])[0];
		if (videoRowIndex < 0) {
			jAlert("Please select Fan Club Video record to edit it.");
			return;
		}
		var videoId =videoGrid.getDataItem(videoRowIndex).videoId;
		
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateFanClubVideos.json",
			type : "post",
			dataType: "json",
			data: "fanClubId="+fanClubId+"&videoId="+videoId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#videoModal').modal('show');
					setEditFanClubVideo(jsonData.video);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function setEditFanClubVideo(data){
		$('#videoModal').modal('show');
		$('#saveVideoBtn').hide();
		$('#updateVideoBtn').show();
		$('#videoFileDiv').hide();
		$('#dispVideoSpan').show();
		$('#videoImageDiv').hide();
		$('#videoImageDispDiv').show();
		$('#videoId').val(data.id);
		$('#videoTitle').val(data.title);
		$('#videoDescription').val(data.description);
		$('#videoCategory').val(data.categoryId);
		$('#previewVideo').attr('href',data.videoUrl);
		$('#videoImageDisp').attr('src',data.thumbnailUrl);
	}
	
	function callChangeVideo(){
		$('#videoFileDiv').show();
		$('#dispVideoSpan').hide();
	}
	function callChangeVideoImage(){
		$('#videoImageDiv').show();
		$('#videoImageDispDiv').hide();
	}
	function removeVideo(){
		var fanClubId =getSelectedFanClubGridId();
		if(fanClubId <= 0){
			jAlert("Please select Fan Club Video record to delete it.");
			return;
		}
		var videoRowIndex = videoGrid.getSelectedRows([0])[0];
		if (videoRowIndex < 0) {
			jAlert("Please select Fan Club Video record to delete it.");
			return;
		}
		var videoId =videoGrid.getDataItem(videoRowIndex).videoId;
		jConfirm("Are you sure to remove selected video from fan club?","Confirm",function(r){
			if (r) {
				$.ajax({
					url : "${pageContext.request.contextPath}/UpdateFanClubVideos.json",
					type : "post",
					dataType: "json",
					data: "fanClubId="+fanClubId+"&videoId="+videoId+"&action=DELETE",
					success : function(response){				
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.status == 1){					
							videoPagingInfo = jsonData.pagingInfo;
							refreshVideoGridValues(jsonData.videos);
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
							return;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});
	}
		
	
	
	
	var fanClubDetailHeader;
	var pagingInfo;
	var fanClubDataView;
	var fanClubGrid;
	var fanClubData = [];
	var fanClubGridSearchString='';
	var sortingString='';
	var columnFilters = {};
	var userFanClubColumnsStr = '<%=session.getAttribute("fanClubgrid")%>';
	var userFanClubColumns =[];	
	var loadFanClubColumns = ["title", "description", "imageUrl", "userId", "email", "phone", "category", "status", "updatedBy","memberCount","createdDate","updatedDate"];
	var allFanClubColumns = [ 
	 { id : "title", name : "Title", field : "title", width:80, sortable : true }, 
	 { id : "description",		name : "Description",		field : "description",		width:80,		sortable : true 	}, 
	 { id : "imageUrl",		name : "Image Preview",		field : "imageUrl",		width:80,		formatter: previewFanClubImageFormatter	}, 
	 { id : "userId",		name : "Created By User Id",		field : "userId",		width:80,		sortable : true 	},
	 {
		id : "email",
		name : "Created By Email",
		field : "email",
		width:80,
		sortable : true
	}, {
		id : "phone",
		name : "Create By Phone",
		field : "phone",
		width:80,
		sortable : true
	},{
		id : "createdBy",
		name : "CreatedBy TFF UserID",
		field : "createdBy",
		width:80,
		sortable : true
	},{
		id : "category",
		name : "Category",
		field : "category",
		width:80,
		sortable : true
	},{
		id : "status",
		name : "Status",
		field : "status",
		width:80,
		sortable : true
	},{
		id : "updatedBy",
		name : "Updated By",
		field : "updatedBy",
		width:80,
		sortable : true
	}, {
		id : "memberCount",
		name : "Member Count",
		field : "memberCount",
		width:80,
		sortable : true
	},  {id: "createdDate", 
		name: "Created Date", 
		field: "createdDate",
		width:80,	
		sortable: true
	},{
		id: "updatedDate", 
		name: "Updated Date", 
		field: "updatedDate", 
		width:80,
		sortable: true
	} ];

	
	if(userFanClubColumnsStr!='null' && userFanClubColumnsStr!=''){
		columnOrder = userFanClubColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allFanClubColumns.length;j++){
				if(columnWidth[0] == allFanClubColumns[j].id){
					userFanClubColumns[i] = allFanClubColumns[j];
					userFanClubColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		columnOrder = loadFanClubColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allFanClubColumns.length;j++){
				if(columnWidth == allFanClubColumns[j].id){
					userFanClubColumns[i] = allFanClubColumns[j];
					userFanClubColumns[i].width=80;
					break;
				}
			}			
		}
		//userFanClubColumns = allFanClubColumns;
	}
	
	
	function previewFanClubImageFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.imageUrl != null &&  dataContext.imageUrl != 'null' && dataContext.imageUrl != ''){
			var button = "<img class='fanClubImage' src='../resources/images/down.png' id='"+ dataContext.imageUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
		
	}
	
	$('.fanClubImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var fanClubOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var fanClubGridSortcol = "fanClubName";
	var fanClubGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function fanClubGridComparer(a, b) {
		var x = a[fanClubGridSortcol], y = b[fanClubGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	function pagingControl(move,id){
		if(id=='fanClub_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getFanClubGridData(pageNo);
		}else if(id=='member_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(memberPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(memberPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(memberPagingInfo.pageNum)-1;
			}
			var fanClubId =  getSelectedFanClubGridId();
			getFanClubMemberGridData(fanClubId, pageNo);
		}else if(id=='post_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(postPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(postPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(postPagingInfo.pageNum)-1;
			}
			var fanClubId = getSelectedFanClubGridId();
			getFanClubPostGridData(fanClubId, pageNo);
		}else if(id=='event_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(eventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(eventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(eventPagingInfo.pageNum)-1;
			}
			var fanClubId = getSelectedFanClubGridId();
			getFanClubEventGridData(fanClubId, pageNo);
		}else if(id=='video_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(videoPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(videoPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(videoPagingInfo.pageNum)-1;
			}
			var fanClubId = getSelectedFanClubGridId();
			getFanClubVideoGridData(fanClubId,pageNo);
		} else if(id=='postComment_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(videoPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(videoPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(videoPagingInfo.pageNum)-1;
			}
			var postId = getSelectedPostGridId();
			var status = 'ACTIVE'
				var element = $('#blockedCommentsTab');
			if((element.className).indexOf('active') > -1) {
				status = 'BLOCKED'
			}
			viewPostComments(postId,status,pageNo);
		} else if(id=='videoComment_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(videoCommentPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(videoCommentPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(videoCommentPagingInfo.pageNum)-1;
			}
			var postId = getSelectedVideoGridId();
			var status = 'ACTIVE'
				var element = $('#blockedVideoCommentsTab');
			if((element.className).indexOf('active') > -1) {
				status = 'BLOCKED'
			}
			viewPostComments(postId,status,pageNo);
		}
	}
	
	function getFanClubGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/ManageFanClubs.json",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+fanClubGridSearchString+"&sortingString="+sortingString+"&status=${status}",
			success : function(res){
				var jsonData = res;
				if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} 
				pagingInfo = jsonData.pagingInfo;
				refreshFanClubGridValues(jsonData.clubs);
				getFanClubMemberGridData('', '');
				$('#fanClubId').val('');
				clearAllSelections();
				$('#fanClub_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserFanClubPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function refreshFanClubGridValues(jsonData) {
		fanClubData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (fanClubData[i] = {});
				d["id"] = i;
				d["fanClubId"] = data.id;
				d["title"] = data.title;
				d["description"] = data.description;
				d["imageUrl"] = data.imageUrl;
				d["createdBy"] = data.createdBy;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["category"] = data.category;
				d["status"] = data.status;
				d["updatedBy"] = data.updatedBy;
				d["memberCount"] = data.memberCount;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
			}
		}

		fanClubDataView = new Slick.Data.DataView();
		fanClubGrid = new Slick.Grid("#fanClub_grid", fanClubDataView, userFanClubColumns, fanClubOptions);
		fanClubGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		fanClubGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var fanClubGridPager = new Slick.Controls.Pager(fanClubDataView, fanClubGrid, $("#fanClub_pager"),pagingInfo);
		}
		var fanClubGridColumnpicker = new Slick.Controls.ColumnPicker(allFanClubColumns, fanClubGrid,
				fanClubOptions);

		// move the filter panel defined in a hidden div into fanClubGrid top panel
		//$("#fanClub_inlineFilterPanel").appendTo(fanClubGrid.getTopPanel()).show();

		fanClubGrid.onSort.subscribe(function(e, args) {
			fanClubGridSortcol = args.sortCol.field;			
			if(sortingString.indexOf(fanClubGridSortcol) < 0){
				fanClubGridSortdir = 'ASC';
			}else{
				if(fanClubGridSortdir == 'DESC' ){
					fanClubGridSortdir = 'ASC';
				}else{
					fanClubGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+fanClubGridSortcol+',SORTINGORDER:'+fanClubGridSortdir+',';
			getFanClubGridData(0);
		});
		
		// wire up model fanClubs to drive the fanClubGrid
		fanClubDataView.onRowCountChanged.subscribe(function(e, args) {
			fanClubGrid.updateRowCount();
			fanClubGrid.render();
		});
		fanClubDataView.onRowsChanged.subscribe(function(e, args) {
			fanClubGrid.invalidateRows(args.rows);
			fanClubGrid.render();
		});
		
		$(fanClubGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			fanClubGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  fanClubGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getFanClubGridData(0);
				}
			  }
		 
		});
		fanClubGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
			}
		});
		fanClubGrid.init();
		
		// initialize the model after all the fanClubs have been hooked up
		fanClubDataView.beginUpdate();
		fanClubDataView.setItems(fanClubData);
		/*fanClubDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			fanClubGridSearchString : fanClubGridSearchString
		});
		fanClubDataView.setFilter(fanClubGridFilter);*/
		fanClubDataView.endUpdate();
		fanClubDataView.syncGridSelection(fanClubGrid, true);
		$("#gridContainer").resizable();
		fanClubGrid.resizeCanvas();
		
		/* fanClubGrid.onContextMenu.subscribe(function (e) {
	      e.preventDefault();
	      var cell = fanClubGrid.getCellFromFanClub(e);
		  fanClubGrid.setSelectedRows([cell.row]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if(height < $("#fanClubContextMenu").height()){
				height = e.pageY - $("#fanClubContextMenu").height();
			}else{
				height = e.pageY;
			}
			if(width < $("#fanClubContextMenu").width()){
				width =  e.pageX- $("#fanClubContextMenu").width();
			}else{
				width =  e.pageX;
			}
		 $("#fanClubContextMenu")
	          .data("row", cell.row)
	          .css("top", height)
	          .css("left", width)
	          .show();
	      $("body").one("click", function () {
	        $("#fanClubContextMenu").hide();
	      });
	    }); */
		
		var fanClubrowIndex;
		fanClubGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprFanClubRowIndex = fanClubGrid.getSelectedRows([0])[0];
			if (temprFanClubRowIndex != fanClubrowIndex) {
				fanClubrowIndex = temprFanClubRowIndex;
				 var fanClubId =fanClubGrid.getDataItem(temprFanClubRowIndex).fanClubId;
				 getFanClubMemberGridData(fanClubId,0);
			}
		});
	}
	
	
		
	/* $("#fanClubContextMenu").click(function (e) {
		if (!$(e.target).is("li")) {
		  return;
		}
		if($(e.target).attr("data") == 'View Zones Map/Members'){
			var fanClubId = getSelectedFanClubGridId();
			if(fanClubId>0){
				popupCenter("${pageContext.request.contextPath}/GetZoneMapMembers?fanClubId="+fanClubId,"Zones And Members",'1200','900');
			}else{
				jAlert("Please select fanClub to see Members.");
			}
		}
		if($(e.target).attr("data") == 'BroadCast Members'){
			broadCastMembers("true");
		}
		if($(e.target).attr("data") == 'UnBroadCast Members'){
			broadCastMembers("false");
		}
	}); */
	
	function getSelectedFanClubGridId() {
		var temprFanClubRowIndex = fanClubGrid.getSelectedRows([0])[0];
		if(temprFanClubRowIndex > -1) {
			 return fanClubGrid.getDataItem(temprFanClubRowIndex).fanClubId;
		}
	}
	
	
	function resetTabSelection(TAB){
		if(TAB == 'MEMBER'){
			$('#FCPostTab').removeClass("active");
			$('#post2').removeClass("active");
			$('#FCEventTab').removeClass("active");
			$('#event2').removeClass("active");
			$('#FCVideoTab').removeClass("active");
			$('#video2').removeClass("active");
			$('#FCmemberTab').addClass('active');
			$('#member2').addClass('active');
		}else if(TAB == 'POST'){
			$('#FCmemberTab').removeClass('active');
			$('#member2').removeClass('active');
			$('#FCEventTab').removeClass("active");
			$('#event2').removeClass("active");
			$('#FCVideoTab').removeClass("active");
			$('#video2').removeClass("active");
			$('#FCPostTab').addClass("active");
			$('#post2').addClass("active");
		}else if(TAB == 'EVENT'){
			$('#FCmemberTab').removeClass('active');
			$('#member2').removeClass('active');
			$('#FCPostTab').removeClass("active");
			$('#post2').removeClass("active");
			$('#FCVideoTab').removeClass("active");
			$('#video2').removeClass("active");
			$('#FCEventTab').addClass("active");
			$('#event2').addClass("active");
		}else if(TAB == 'VIDEO'){
			$('#FCmemberTab').removeClass('active');
			$('#member2').removeClass('active');
			$('#FCPostTab').removeClass("active");
			$('#post2').removeClass("active");
			$('#FCEventTab').removeClass("active");
			$('#event2').removeClass("active");
			$('#FCVideoTab').addClass("active");
			$('#video2').addClass("active");
		}
	}
	
	function resetGridArea(TAB){
		if(TAB == 'MEMBER'){
			$('#postGridDiv').hide();
			$('#eventGridDiv').hide();
			$('#videoGridDiv').hide();
			$('#memberGridDiv').show();
		}else if(TAB == 'POST'){
			$('#memberGridDiv').hide();
			$('#eventGridDiv').hide();
			$('#videoGridDiv').hide();
			$('#postGridDiv').show();
		}else if(TAB == 'EVENT'){
			$('#postGridDiv').hide();
			$('#memberGridDiv').hide();
			$('#videoGridDiv').hide();
			$('#eventGridDiv').show();
		}else if(TAB == 'VIDEO'){
			$('#postGridDiv').hide();
			$('#memberGridDiv').hide();
			$('#eventGridDiv').hide();
			$('#videoGridDiv').show();
		}
	}
	
	function getFanClubMemberGridData(fanClubId,pageNo){		
		resetTabSelection("MEMBER");
		$.ajax({
			url : "${pageContext.request.contextPath}/GetFanClubMembers.json",
			type : "get",
			data : "fanClubId="+ fanClubId+"&pageNo="+pageNo+"&headerFilter="+memberGridSearchString,
			dataType:"json",
			/* async : false, */
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				//jAlert(jsonData);
				if(jsonData==null || jsonData=="") {
					jAlert("No Members Found.");
				}
				memberPagingInfo = jsonData.pagingInfo;
				refreshMemberGridValues(jsonData.members);
				clearAllSelections();
				$('#member_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserMemberPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
    }
	
	function saveUserFanClubPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = fanClubGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('fanClubgrid',colStr);
	}
	
	function saveUserMemberPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = memberGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('FCMembergrid',colStr);
	}
	
	
	function memberExportToExcel(){	
		var fanClubId =getSelectedFanClubGridId();
		var appendData = "fanClubId="+fanClubId+"&headerFilter="+memberGridSearchString;
	    var url = apiServerUrl + "MemberExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function memberResetFilters(){
		memberGridSearchString='';
		memberGridcolumnFilters = {};
		var fanClubId =getSelectedFanClubGridId();
		getFanClubMemberGridData(fanClubId);
	}
	

	
	//MEMBER GRID CODE
	
	var memberCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	var memberPagingInfo;
	var memberDataView;
	var memberGrid;
	var memberData = [];
	var memberGridSearchString='';
	var memberGridcolumnFilters = {};
	var memberColumnStr = '<%=session.getAttribute("FCmembergrid")%>';
	var userMemberColumns = [];
	var allMemberColumns = [ memberCheckboxSelector.getColumnDefinition(),
	                         { id : "userId", name : "User ID", field : "userId", width:80, sortable : true }, 
	                    	 { id : "email",		name : "Email",		field : "email",		width:80,		sortable : true 	}, 
	                    	 { id : "phone",		name : "Phone",		field : "phone",		width:80,		sortable : true 	}, 
	                    	 { id : "joinedDate",		name : "Joined Date",		field : "joinedDate",		width:80,		sortable : true 	},
	                    	 { id : "exitDate", name : "Exit Date", field : "exitDate", width:80, sortable : true }, 
	                    	 { id : "status",		name : "Status",		field : "status",		width:80,		sortable : true 	}
	                    	
	                    	 ];

	if(memberColumnStr!='null' && memberColumnStr!=''){
		var columnOrder = memberColumnStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allMemberColumns.length;j++){
				if(columnWidth[0] == allMemberColumns[j].id){
					userMemberColumns[i] =  allMemberColumns[j];
					userMemberColumns[i].width=(columnWidth[1]-5);
					console.log(userMemberColumns[i].id+"--"+userMemberColumns[i].width)
					break;
				}
			}
			
		}
	}else{
		userMemberColumns = allMemberColumns;
	}
	
	var memberOptions = {
		editable: true,
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var memberGridSortcol = "userId";
	var memberGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	
	
	function memberGridComparer(a, b) {
		var x = a[memberGridSortcol], y = b[memberGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	function refreshMemberGridValues(jsonData) {
		resetGridArea('MEMBER');
		memberData = [];
		if(jsonData!=null && jsonData != ''){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (memberData[i] = {});
				d["id"] = data.id;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["joinedDate"] = data.joinedDateStr;
				d["exitDate"] = data.exitDateStr;
				d["status"] =  data.status;
			}
		}
		
		memberDataView = new Slick.Data.DataView();
		memberGrid = new Slick.Grid("#member_grid", memberDataView, userMemberColumns, memberOptions);
		memberGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		memberGrid.setSelectionModel(new Slick.RowSelectionModel());
		memberGrid.registerPlugin(memberCheckboxSelector);
		if(memberPagingInfo!=null){
			var memberGridPager = new Slick.Controls.Pager(memberDataView, memberGrid, $("#member_pager"),memberPagingInfo);
		}
		
		var memberGridColumnpicker = new Slick.Controls.ColumnPicker(allMemberColumns, memberGrid,
				memberOptions);

		
		/* memberGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = memberGrid.getCellFromEvent(e);
			  memberGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    }); */
		
		memberGrid.onSort.subscribe(function(e, args) {
			memberGridSortdir = args.sortAsc ? 1 : -1;
			memberGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				memberDataView.fastSort(memberGridSortcol, args.sortAsc);
			} else {
				memberDataView.sort(memberGridComparer, args.sortAsc);
			}
		});
		// wire up model members to drive the memberGrid
		memberDataView.onRowCountChanged.subscribe(function(e, args) {
			memberGrid.updateRowCount();
			memberGrid.render();
		});
		memberDataView.onRowsChanged.subscribe(function(e, args) {
			memberGrid.invalidateRows(args.rows);
			memberGrid.render();
		});
		$(memberGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			memberGridSearchString = '';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				memberGridcolumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in memberGridcolumnFilters) {
					  if (columnId !== undefined && memberGridcolumnFilters[columnId] !== "") {
						  memberGridSearchString += columnId + ":" +memberGridcolumnFilters[columnId]+",";
					  }
					}
					var fanClubId =getSelectedFanClubGridId();
					getFanClubMemberGridData(fanClubId);
				}
			  }
		 
		});
		memberGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(memberGridcolumnFilters[args.column.id])
			   .appendTo(args.node);
			}
		});
		memberGrid.init();
		
		// initialize the model after all the members have been hooked up
		memberDataView.beginUpdate();
		memberDataView.setItems(memberData);
		
		memberDataView.endUpdate();
		memberDataView.syncGridSelection(memberGrid, true);
		$("#gridContainer").resizable();
		memberGrid.resizeCanvas();
		/*  $("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'Edit Member'){
				editMember();
			}else if($(e.target).attr("data") == 'Create Invoice'){
				createInvoicePopup();
			}
		}); */
	}
	
	function getSelectedMemberGridId() {
		var memberRowIndex = memberGrid.getSelectedRows();
		
		var memberIds='';
		$.each(memberRowIndex, function (index, value) {
			memberIds += ','+memberGrid.getDataItem(value).id;
		});
		
		if(memberIds != null && memberIds!='') {
			memberIds = memberIds.substring(1, memberIds.length);
			 return memberIds;
		}
	}
	
//Sold Tickets Grid

	var postDataView;
	var postPagingInfo;
	var postGrid;
	var postData = [];
	var postSearchString='';
	var postColumnFilters = {};
	var postColumnStr = '<%=session.getAttribute("FCPostgrid")%>';
	var postColumns = [
	                  { id : "userId", name : "Post by User ID", field : "userId", width:80, sortable : true }, 
                  	 { id : "email",		name : "Post By Email",		field : "email",		width:80,		sortable : true 	}, 
                  	 { id : "phone",		name : "Post By Phone",		field : "phone",		width:80,		sortable : true 	}, 
                  	 { id : "postText",		name : "Post Text",		field : "postText",		width:80,		sortable : true 	},
                  	 { id : "imageUrl", name : "Image Preview", field : "imageUrl", width:80, formatter: previewPostImageFormatter }, 
                  	 { id : "videoUrl",		name : "Video Preview",		field : "videoUrl",		width:80,		formatter: previewPostVideoFormatter	},
                  	 { id : "thumbnailUrl",		name : "Thumbnail Preview",		field : "thumbnailUrl",		width:80,		formatter: previewPostThubmFormatter	}, 
                  	 { id : "createdBy",		name : "TFF Created By User ID",		field : "createdBy",		width:80,		sortable : true 	}, 
                  	 { id : "updatedBy",		name : "TFF Updated By User ID",		field : "updatedBy",		width:80,		sortable : true 	}, 
                  	 { id : "status",		name : "Status",		field : "status",		width:80,		sortable : true 	}, 
                  	 { id : "likeCount",		name : "Like Count",		field : "likeCount",		width:80,		sortable : true 	}, 
                  	 { id : "createdDate",		name : "Created Date",		field : "createdDate",		width:80,		sortable : true 	}, 
                  	 { id : "updatedDate",		name : "Updated Date",		field : "updatedDate",		width:80,		sortable : true 	}, 
                  	 { id : "updatReason",		name : "Update Reason",		field : "updatReason",		width:80,		sortable : true 	}
                  	
	                 ];
	
	function previewPostImageFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.imageUrl != null &&  dataContext.imageUrl != 'null' && dataContext.imageUrl != ''){
			var button = "<img class='postImage' src='../resources/images/down.png' id='"+ dataContext.imageUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
	}
	function previewPostVideoFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.videoUrl != null &&  dataContext.videoUrl != 'null' && dataContext.videoUrl != ''){
			var button = "<img class='postImage' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
	}
	function previewPostThubmFormatter(row,cell,value,columnDef,dataContext){
		if(dataContext.thumbnailUrl != null &&  dataContext.thumbnailUrl != 'null' && dataContext.thumbnailUrl != ''){
			var button = "<img class='postImage' src='../resources/images/down.png' id='"+ dataContext.thumbnailUrl +"'/>";
			return button;
		}else{
			var button = "<label'/>N/A</label>";
			return button;
		}
	}

	$('.postImage').live('click', function(){
		var me = $(this), id = me.attr('id');
		window.open(id,'MyWindow',width=600,height=300);
	});
		
	var postOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var postGridSortcol = "postText";
	var postGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	
	function postGridComparer(a, b) {
		var x = a[postGridSortcol], y = b[postGridSortcol];
		if(!isNaN(x)){
		  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	function getFanClubPostGridData(fanClubId,pageNo) {	
		resetTabSelection('POST');
			$.ajax({
				url : "${pageContext.request.contextPath}/ManageFanClubPosts.json",
				type : "post",
				data : "fanClubId="+fanClubId+"&pageNo="+pageNo+"&headerFilter="+postSearchString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));					
					if(jsonData==null || jsonData=='') {
						jAlert("No Data Found.");
					}
					postPagingInfo = jsonData.pagingInfo;					
					refreshPostValues(jsonData.posts);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
			
		}
		
	function refreshPostValues(jsonData){
		resetGridArea('POST');
		postData = [];
		if(jsonData!=null){			
			for (var i = 0; i < jsonData.length; i++) {				
				var data = jsonData[i];
				var d = (postData[i] = {});
				d["id"] = i;
				d["postId"] = data.id;
				d["userId"] = data.userId;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["postText"] = data.postText;
				d["imageUrl"] = data.imageUrl;
				d["videoUrl"] = data.videoUrl;
				d["thumbnailUrl"] = data.thumbnailUrl;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
				d["status"] = data.status;
				d["likeCount"] = data.likeCount;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["updatReason"] = data.updatReason;
			}
		}				
	
	postDataView = new Slick.Data.DataView();
	postGrid = new Slick.Grid("#post_grid", postDataView, postColumns, postOptions);
	postGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	postGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(postPagingInfo!=null){
		var postGridPager = new Slick.Controls.Pager(postDataView, postGrid, $("#post_pager"),postPagingInfo);
	}
	
	var postGridColumnPicker = new Slick.Controls.ColumnPicker(postColumns, postGrid,
			postOptions);
	
	postGrid.onSort.subscribe(function(e, args) {
		postGridSortdir = args.sortAsc ? 1 : -1;
		postGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			postDataView.fastSort(postGridSortcol, args.sortAsc);
		} else {
			postDataView.sort(postGridComparer, args.sortAsc);
		}
	});
	// wire up model tickets to drive the postGrid
	postDataView.onRowCountChanged.subscribe(function(e, args) {
		postGrid.updateRowCount();
		postGrid.render();
	});
	postDataView.onRowsChanged.subscribe(function(e, args) {
		postGrid.invalidateRows(args.rows);
		postGrid.render();
	});
	
	$(postGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	postSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				postColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in postColumnFilters) {
					  if (columnId !== undefined && postColumnFilters[columnId] !== "") {
						 postSearchString += columnId + ":" +postColumnFilters[columnId]+",";
					  }
					}
					var fanClubId =getSelectedFanClubGridId();
					getFanClubPostGridData(fanClubId, 0);
				}
			  }		 
		});
	postGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){			
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(postColumnFilters[args.column.id])
			   .appendTo(args.node);
			}			
		});
	postGrid.init();	 
	// initialize the model after all the tickets have been hooked up
	postDataView.beginUpdate();
	postDataView.setItems(postData);
	
	postDataView.endUpdate();
	postDataView.syncGridSelection(postGrid, true);
	postDataView.refresh();
	$("#gridContainer").resizable();
	postGrid.resizeCanvas();
}

function postResetFilters(){
	postSearchString='';
	postColumnFilters = {};
	var fanClubId =getSelectedFanClubGridId();
	getFanClubPostGridData(fanClubId,0);
}

function postExportToExcel(){
	var fanClubId =getSelectedFanClubGridId();
	var appendData = "fanClubId="+fanClubId+"&headerFilter="+postSearchString;
    var url = apiServerUrl + "PostExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}	

//Locked Tickets Grid	
	
var eventPagingInfo;
var eventGrid;
var eventDataView;
var eventData=[];
var eventSearchString = "";
var eventColumnFilters = {};
var eventColumn = [ 
                 { id : "userId", name : "Event by User ID", field : "userId", width:80, sortable : true }, 
               	{ id : "email",		name : "Event By Email",		field : "email",		width:80,		sortable : true 	}, 
               	{ id : "phone",		name : "Event By Phone",		field : "phone",		width:80,		sortable : true 	}, 
               {id:"eventName", name:"Event Name", field: "eventName", sortable: true},
               {id:"eventDate", name:"Event Date", field: "eventDate", sortable: true},
               {id:"venue", name:"Venue", field: "venue", sortable: true},
               { id : "eventImage",		name : "Image Preview",		field : "eventImage",	width:80,	formatter: previewEventImageFormatter	}, 
               {id:"status", name:"Status", field: "status", sortable: true},
               {id:"interestedCount", name:"Interested Count", field: "interestedCount", sortable: true},
               {id:"createdDate", name:"Created Date", field: "createdDate", sortable: true},
               {id:"updatedDate", name:"Updated Date", field: "updatedDate", sortable: true},
               {id:"createdBy", name:"TFF Created By User ID", field: "createdBy", sortable: true},
               {id:"updatedBy", name:"TFF Updated By User ID", field: "updatedBy", sortable: true}
               
              ];
              
var eventOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "eventName";
var sortdir = 1;
var percentCompleteThreshold = 0;

function eventComparer(a, b) {
	var x = a[sortcol], y = b[sortcol];
	if(!isNaN(x)){
		return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
    if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function previewEventImageFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.eventImage != null &&  dataContext.eventImage != 'null' && dataContext.eventImage != ''){
		var button = "<img class='eventtImageClass' src='../resources/images/down.png' id='"+ dataContext.eventImage +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}

$('.eventtImageClass').live('click', function(){
	var me = $(this), id = me.attr('id');
	window.open(id,'MyWindow',width=600,height=300);
});
 

function getFanClubEventGridData(fanClubId, pageNo) {	
	resetTabSelection('EVENT');
	$.ajax({
		url : "${pageContext.request.contextPath}/ManageFanClubEvents.json",
		type : "post",
		data : "fanClubId="+fanClubId+"&pageNo="+pageNo+"&headerFilter="+eventSearchString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData==null || jsonData=='') {
				jAlert("No Data Found.");
			}
			eventPagingInfo = jsonData.pagingInfo;
			refreshEventGridValues(jsonData.events);
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
	
function refreshEventGridValues(jsonData) {	
	resetGridArea('EVENT');
	eventData=[];
	var i=0;
	if(jsonData!=null){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (eventData[i] = {});
			d["id"] = i;
			d["eventId"] = data.id;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDateStr;
			d["venue"] = data.venue;
			d["eventImage"] = data.eventImage;
			d["status"] = data.status;
			d["interestedCount"] = data.interestedCount;
			d["createdDate"] = data.createdDateStr;
			d["updatedDate"] = data.updatedDateStr;
			d["createdBy"] = data.createdBy;
			d["updatedBy"] = data.updatedBy;
		}
	}
					
	eventDataView = new Slick.Data.DataView();
	eventGrid = new Slick.Grid("#event_grid", eventDataView, eventColumn, eventOptions);
	eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	eventGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(eventPagingInfo != null){
		var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid, $("#event_pager"),eventPagingInfo);
	}
	var eventGridColumnpicker = new Slick.Controls.ColumnPicker(eventColumn, eventGrid, eventOptions);
	 
	  eventGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      eventDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      eventDataView.sort(eventComparer, args.sortAsc);
	    }
	  });
	  
	  // wire up model events to drive the eventGrid
	  eventDataView.onRowCountChanged.subscribe(function (e, args) {
	    eventGrid.updateRowCount();
	    eventGrid.render();
	  });
	  eventDataView.onRowsChanged.subscribe(function (e, args) {
	    eventGrid.invalidateRows(args.rows);
	    eventGrid.render();
	  });

		$(eventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	eventSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				eventColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in eventColumnFilters) {
					  if (columnId !== undefined && eventColumnFilters[columnId] !== "") {
						 eventSearchString += columnId + ":" +eventColumnFilters[columnId]+",";
					  }
					}
					var fanClubId =getSelectedFanClubGridId();
					getFanClubEventGridData(fanClubId,0);
				}
			  }		 
		});
		eventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(eventColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else {
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(eventColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}			
		});
		eventGrid.init();	  	 
		 
	  eventDataView.beginUpdate();
	  eventDataView.setItems(eventData);
	  
	  eventDataView.endUpdate();
	  eventDataView.syncGridSelection(eventGrid, true);
	  eventDataView.refresh();
	  $("#gridContainer").resizable();
	  eventGrid.resizeCanvas();
}	

function eventResetFilters(){	
	eventSearchString='';
	eventColumnFilters = {};	
	var fanClubId =getSelectedFanClubGridId();
	getFanClubEventGridData(fanClubId,0);
}

function eventExportToExcel(){
	var fanClubId =getSelectedFanClubGridId();
	var appendData = "fanClubId="+fanClubId+"&headerFilter="+eventSearchString;
    var url = apiServerUrl + "EventExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}



//VIdeo Tickets Grid	

var videoPagingInfo;
var videoGrid;
var videoDataView;
var videoData=[];
var videoSearchString = "";
var videoColumnFilters = {};
var videoColumn = [
                { id : "userId", name : "Video by User ID", field : "userId", width:80, sortable : true }, 
               	{ id : "email",		name : "Video By Email",		field : "email",		width:80,		sortable : true 	}, 
               	{ id : "phone",		name : "Video By Phone",		field : "phone",		width:80,		sortable : true 	}, 
               {id:"title", name:"Title", field: "title", sortable: true},
               {id:"description", name:"Description", field: "description", sortable: true},
               {id:"videoUrl", name:"Preview Video", field: "videoUrl", sortable: true,	formatter: previewVideoFormatter	},
               {id:"thumbnailUrl", name:"Preview Thumbnail", field: "thumbnailUrl", sortable: true,	formatter: previewVideoThumbFormatter	},
               {id:"status", name:"Status", field: "status", sortable: true},
               {id:"createdDate", name:"Upload Date", field: "createdDate", sortable: true},
               {id:"updatedDate", name:"Updated Date", field: "updatedDate", sortable: true},
               {id:"createdBy", name:"TFF Created By User ID", field: "createdBy", sortable: true},
               {id:"updatedBy", name:"TFF Updated By User ID", field: "updatedBy", sortable: true},
               {id:"blockBy", name:"Blocked By", field: "blockBy", sortable: true},
               {id:"blockDate", name:"Blocked Date", field: "blockDate", sortable: true},
               {id:"blockReason", name:"Blocked Reason", field: "blockReason", sortable: true},
               {id:"category", name:"Category", field: "category", sortable: true}
              ];
              
var videoOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "title";
var sortdir = 1;
var percentCompleteThreshold = 0;

function videoComparer(a, b) {
	var x = a[sortcol], y = b[sortcol];
	if(!isNaN(x)){
		return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
    if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function previewVideoFormatter(row,cell,value,columnDef,dataContext){
	var button = "<img class='videoClass' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
	return button;
}
function previewVideoThumbFormatter(row,cell,value,columnDef,dataContext){
	var button = "<img class='videoClass' src='../resources/images/down.png' id='"+ dataContext.thumbnailUrl +"'/>";
	return button;
}
$('.videoClass').live('click', function(){
	var me = $(this), id = me.attr('id');
	window.open(id,'MyWindow',width=600,height=300);
});
 

function getFanClubVideoGridData(fanClubId, pageNo) {
	resetTabSelection('VIDEO');
	$.ajax({
		url : "${pageContext.request.contextPath}/ManageFanClubVideos.json",
		type : "post",
		data : "fanClubId="+fanClubId+"&pageNo="+pageNo+"&headerFilter="+videoSearchString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData==null || jsonData=='') {
				jAlert("No Data Found.");
			}
			videoPagingInfo = jsonData.pagingInfo;
			refreshVideoGridValues(jsonData.videos);
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
	
function refreshVideoGridValues(jsonData) {	
	resetGridArea('VIDEO');
	videoData=[];
	var i=0;
	if(jsonData!=null){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (videoData[i] = {});
			d["id"] = i;
			d["videoId"] = data.id;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["title"] = data.title;
			d["description"] = data.description;
			d["videoUrl"] = data.videoUrl;
			d["thumbnailUrl"] = data.thumbnailUrl;
			d["status"] = data.status;
			d["createdDate"] = data.createdDateStr;
			d["updatedDate"] = data.updatedDateStr;
			d["createdBy"] = data.createdBy;
			d["updatedBy"] = data.updatedBy;
			d["blockBy"] = data.blockBy;
			d["blockDate"] = data.blockDateStr;
			d["blockReason"] = data.blockReason;
			d["category"] = data.category;
		}
	}
					
	videoDataView = new Slick.Data.DataView();
	videoGrid = new Slick.Grid("#video_grid", videoDataView, videoColumn, videoOptions);
	videoGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	videoGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(videoPagingInfo != null){
		var videoGridPager = new Slick.Controls.Pager(videoDataView, videoGrid, $("#video_pager"),videoPagingInfo);
	}
	var videoGridColumnpicker = new Slick.Controls.ColumnPicker(videoColumn, videoGrid, videoOptions);
	 
	  videoGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      videoDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      videoDataView.sort(videoComparer, args.sortAsc);
	    }
	  });
	  
	  // wire up model videos to drive the videoGrid
	  videoDataView.onRowCountChanged.subscribe(function (e, args) {
	    videoGrid.updateRowCount();
	    videoGrid.render();
	  });
	  videoDataView.onRowsChanged.subscribe(function (e, args) {
	    videoGrid.invalidateRows(args.rows);
	    videoGrid.render();
	  });

		$(videoGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	videoSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				videoColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in videoColumnFilters) {
					  if (columnId !== undefined && videoColumnFilters[columnId] !== "") {
						 videoSearchString += columnId + ":" +videoColumnFilters[columnId]+",";
					  }
					}
					var fanClubId =getSelectedFanClubGridId();
					getFanClubVideoGridData(fanClubId,0);
				}
			  }		 
		});
		videoGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDatees'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(videoColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(videoColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}			
		});
		videoGrid.init();	  	 
		 
	  videoDataView.beginUpdate();
	  videoDataView.setItems(videoData);
	  
	  videoDataView.endUpdate();
	  videoDataView.syncGridSelection(videoGrid, true);
	  videoDataView.refresh();
	  $("#gridContainer").resizable();
	  videoGrid.resizeCanvas();
}	

function videoResetFilters(){	
	videoSearchString='';
	videoColumnFilters = {};	
	var fanClubId =getSelectedFanClubGridId();
	getFanClubVideoGridData(fanClubId,0);
}

function videoExportToExcel(){
	var fanClubId =getSelectedFanClubGridId();
	var appendData = "fanClubId="+fanClubId+"&headerFilter="+videoSearchString;
    var url = apiServerUrl + "VideoExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

jQuery(document).ready(function () {
	var winSize = jQuery(window).width();
	if (winSize <= 991) {
		setTimeout(function () {
			var grid_top2 = jQuery('.grid-table-2').offset().top + 90;
			jQuery('ul#contextMenu').css('top',grid_top2);
		}, 4000);
	}
});

function clearAllSelections(){
	var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
	for(var j=0;j<eventTable.length;j++){
		var eventCheckbox = eventTable[j].getElementsByTagName('input');
		for ( var i = 0; i < eventCheckbox.length; i++) {
			if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
			}
		}
	}
}

function postCommentResetFilters(){
	postCommentSearchString='';
	postCommentColumnFilters = {};
	
	var status = 'ACTIVE'
		var element = $('#blockedCommentsTab');
	if((element.className).indexOf('active') > -1) {
		status = 'BLOCKED'
	}
	viewPostComments(status,1);
}

var postCommentPagingInfo;
var postCommentDataView;
var postCommentGrid;
var postCommentData = [];
var postCommentGridPager;
var postCommentSearchString='';
var postCommentColumnFilters = {};
var userPostCommentColumnsStr = '<%=session.getAttribute("postCommentGrid")%>';

var userPostCommentColumns = [];
var allPostCommentColumns = [
	{ id : "userId", name : "Comment by User ID", field : "userId", width:80, sortable : true }, 
 	 { id : "email",		name : "Post By Email",		field : "email",		width:80,		sortable : true 	}, 
 	 { id : "phone",		name : "Post By Phone",		field : "phone",		width:80,		sortable : true 	}, 
 	 { id : "commentText",		name : "Comment Text",		field : "commentText",		width:80,		sortable : true 	},
 	 { id : "imageUrl", name : "Image Preview", field : "imageUrl", width:80, formatter: previewCommentImageFormatter }, 
 	 { id : "videoUrl",		name : "Video Preview",		field : "videoUrl",		width:80,		formatter: previewCommentVideoFormatter	},
 	 { id : "thumbnailUrl",		name : "Thumbnail Preview",		field : "thumbnailUrl",		width:80,		formatter: previewCommentThubmFormatter	}, 
 	 { id : "createdBy",		name : "TFF Created By User ID",		field : "createdBy",		width:80,		sortable : true 	}, 
 	 { id : "updatedBy",		name : "TFF Updated By User ID",		field : "updatedBy",		width:80,		sortable : true 	}, 
 	 { id : "status",		name : "Status",		field : "status",		width:80,		sortable : true 	}, 
 	 { id : "createdDate",		name : "Created Date",		field : "createdDate",		width:80,		sortable : true 	}, 
 	 { id : "updatedDate",		name : "Updated Date",		field : "updatedDate",		width:80,		sortable : true 	}, 
 	 { id : "blockReason",		name : "Block Reason",		field : "blockReason",		width:80,		sortable : true 	}
	];

function previewCommentImageFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.imageUrl != null &&  dataContext.imageUrl != 'null' && dataContext.imageUrl != ''){
		var button = "<img class='postImage' src='../resources/images/down.png' id='"+ dataContext.imageUrl +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}
function previewCommentVideoFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.videoUrl != null &&  dataContext.videoUrl != 'null' && dataContext.videoUrl != ''){
		var button = "<img class='postImage' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}
function previewCommentThubmFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.thumbnailUrl != null &&  dataContext.thumbnailUrl != 'null' && dataContext.thumbnailUrl != ''){
		var button = "<img class='postImage' src='../resources/images/down.png' id='"+ dataContext.thumbnailUrl +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}

/*$('.postImage').live('click', function(){
	var me = $(this), id = me.attr('id');
	window.open(id,'MyWindow',width=600,height=300);
});*/

if (userPostCommentColumnsStr != 'null' && userPostCommentColumnsStr != '') {
	columnOrder = userPostCommentColumnsStr.split(',');
	var columnWidth = [];
	for ( var i = 0; i < columnOrder.length; i++) {
		columnWidth = columnOrder[i].split(":");
		for ( var j = 0; j < allPostCommentColumns.length; j++) {
			if (columnWidth[0] == allPostCommentColumns[j].id) {
				userPostCommentColumns[i] = allPostCommentColumns[j];
				userPostCommentColumns[i].width = (columnWidth[1] - 5);
				break;
			}
		}

	}
} else {
	userPostCommentColumns = allPostCommentColumns;
}

var postCommentOptions = {
	editable: true,
	enableCellNavigation : true,
	asyncEditorLoading: false,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};
var postCommentGridSortcol = "id";
var postCommentGridSortdir = 1;
var percentCompleteThreshold = 0;

function postCommentGridComparer(a, b) {
	var x = a[postCommentGridSortcol], y = b[postCommentGridSortcol];
	if (!isNaN(x)) {
		return (parseFloat(x) == parseFloat(y) ? 0
				: (parseFloat(x) > parseFloat(y) ? 1 : -1));
	}
	if (x == '' || x == null) {
		return 1;
	} else if (y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String)
			&& (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));
	}
}

function refreshPostCommentGridValues(jsonData,status) {
	$("div#divLoading").addClass('show');
	postCommentData = [];
	if (jsonData != null && jsonData.length > 0) {
		for ( var i = 0; i < jsonData.length; i++) {
			var data = jsonData[i];
			var d = (postCommentData[i] = {});
			d["id"] = i;
			d["commentId"] = data.id;
			d["postId"] = data.postId;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["commentText"] = data.commentText;
			d["imageUrl"] = data.imageUrl;
			d["videoUrl"] = data.mediaUrl;
			d["thumbnailUrl"] = data.posterUrl;
			d["createdBy"] = data.createdBy;
			d["updatedBy"] = data.updatedBy;
			d["status"] = data.status;
			d["createdDate"] = data.createdDateStr;
			d["updatedDate"] = data.updatedDateStr;
			d["blockReason"] = data.blockReason;
			
		}
	}

	postCommentDataView = new Slick.Data.DataView();
	postCommentGrid = new Slick.Grid("#postComment_grid", postCommentDataView,
			userPostCommentColumns, postCommentOptions);
	postCommentGrid.registerPlugin(new Slick.AutoTooltips({
		enableForHeaderCells : true
	}));
	postCommentGrid.setSelectionModel(new Slick.RowSelectionModel());
	
	postCommentGridPager = new Slick.Controls.Pager(postCommentDataView,
				postCommentGrid, $("#postComment_pager"),
				postCommentPagingInfo);
	var postCommentGridColumnpicker = new Slick.Controls.ColumnPicker(
			allPostCommentColumns, postCommentGrid, postCommentOptions);
		
	postCommentGrid.onSort.subscribe(function(e, args) {
		postCommentGridSortdir = args.sortAsc ? 1 : -1;
		postCommentGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			postCommentDataView.fastSort(postCommentGridSortcol, args.sortAsc);
		} else {
			postCommentDataView.sort(postCommentGridComparer, args.sortAsc);
		}
	});
	
	// wire up model discountCodes to drive the postCommentGrid
	postCommentDataView.onRowCountChanged.subscribe(function(e, args) {
		postCommentGrid.updateRowCount();
		postCommentGrid.render();
	});
	postCommentDataView.onRowsChanged.subscribe(function(e, args) {
		postCommentGrid.invalidateRows(args.rows);
		postCommentGrid.render();
	});
	$(postCommentGrid.getHeaderRow())
			.delegate(
					":input",
					"keyup",
					function(e) {
						var keyCode = (e.keyCode ? e.keyCode : e.which);
						postCommentSearchString = '';
						var columnId = $(this).data("columnId");
						if (columnId != null) {
							postCommentColumnFilters[columnId] = $.trim($(this)
									.val());
							if (keyCode == 13) {
								for ( var columnId in postCommentColumnFilters) {
									if (columnId !== undefined
											&& postCommentColumnFilters[columnId] !== "") {
										postCommentSearchString += columnId
												+ ":"
												+ postCommentColumnFilters[columnId]
												+ ",";
									}
								}
								viewPostComments(status,1);
							}
						}

					});
	postCommentGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
		if (args.column.id.indexOf('checkbox') == -1) {				
			$("<input type='text'>").data("columnId", args.column.id)
					.val(postCommentColumnFilters[args.column.id]).appendTo(
							args.node);				
		}
	});
	postCommentGrid.init();
	// initialize the model after all the discountCodes have been hooked up
	postCommentDataView.beginUpdate();
	postCommentDataView.setItems(postCommentData);
	//postCommentDataView.setFilter(filter);
	postCommentDataView.endUpdate();
	postCommentDataView.syncGridSelection(postCommentGrid, true);
	//postCommentGrid.resizeCanvas();
	$("#gridContainer").resizable();
	$("div#divLoading").removeClass('show');
}


function viewPostComments(status,pageNo){
	var tempPostRowIndex = postGrid.getSelectedRows([0])[0];
	var postId = 0;
	if(tempPostRowIndex >= 0) {
		postId = postGrid.getDataItem(tempPostRowIndex).postId;
	}
	if(postId <=0 ){
		jAlert("Please select post to view comments.");
		return;
	}
	resetPostCommentTabSelection(status);
	
	$.ajax({
		url : "${pageContext.request.contextPath}/GetFanClubPostComments.json",
		type : "post",
		dataType: "json",
		data: "commentType=POST&commentTypeId="+postId+"&status="+status+"&pageNo="+pageNo+"&headerFilter="+postCommentSearchString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
				return;
			}
			postCommentPagingInfo = jsonData.pagingInfo;
			refreshPostCommentGridValues(jsonData.postComments,status);
			$('#postComment_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUPostCommentsPreference()'>");
			$('#view-post-comments').modal('show');			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function blockPostComment(){
	var tempRowIndex = postCommentGrid.getSelectedRows([0])[0];
	var postcommentId=0;
	var postId=0;
	if(tempRowIndex >= 0) {
		postcommentId = postCommentGrid.getDataItem(tempRowIndex).commentId;
		postId = postCommentGrid.getDataItem(tempRowIndex).postId;
	}
	
	if (postcommentId<= 0 || postId <= 0) {
		jAlert("Please Select Comment to Remove/Block.", "info");
		return false;
	}
	jConfirm("Are you sure to remove selected comment from fan club post?","Confirm",function(r){
		if (r) {
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateFanClubPostComments.json",
				type : "post",
				dataType: "json",
				data: "commentId="+postcommentId+"&action=DELETE&commentType=POST&commentTypeId="+postId,
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){					
						postPagingInfo = jsonData.pagingInfo;					
						refreshPostCommentGridValues(jsonData.postComments);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	});
}
function blockPostCommentUser(){
	var tempRowIndex = postCommentGrid.getSelectedRows([0])[0];
	var postcommentId=0;
	var customerId=0;
	if(tempRowIndex >= 0) {
		postcommentId = postCommentGrid.getDataItem(tempRowIndex).commentId;
		customerId = postCommentGrid.getDataItem(tempRowIndex).customerId;
	}
	
	if (postcommentId<= 0 || customerId <= 0) {
		jAlert("Please Select Comment to Block Customer.", "info");
		return false;
	}
	jConfirm("Are you sure to Block selected comment Customer?","Confirm",function(r){
		if (r) {
			$.ajax({
				url : "${pageContext.request.contextPath}/BlockCustomerComment.json",
				type : "post",
				dataType: "json",
				data: "customerId="+customerId+"&sourceId="+postcommentId+"&type=POSTCOMMENT",
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){					
						//postPagingInfo = jsonData.pagingInfo;					
						//refreshPostCommentGridValues(jsonData.postComments);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	});
}
function resetPostCommentTabSelection(TAB){
	if(TAB == 'ACTIVE'){
		$('#activeCommentsTab').addClass('active');
		$('#activeC').addClass('active');
		$('#blockedCommentsTab').removeClass("active");
		$('#blockedC').removeClass("active");
	}else if(TAB == 'BLOCKED'){
		//alert(TAB);
		//$('#activeCommentsTab').removeClass('active');
		$('#blockedCommentsTab').addClass("active");
		$('#blockedC').addClass("active");
		$('#activeCommentsTab').removeClass("active");
		$('#activeC').removeClass('active');
	}
}
function getSelectedPostGridId() {
	var tempRowIndex = postGrid.getSelectedRows([0])[0];
	var postId=0;
	if(tempRowIndex >= 0) {
		postId = postGrid.getDataItem(tempRowIndex).postId;
	}
	return postId;
}
function saveUPostCommentsPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = postCommentGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+","
	}
	saveUserPreference('postCommentGrid',colStr);
}
/* video comment grid starts  */
 
 
function videoCommentResetFilters(){
	videoCommentSearchString='';
	videoCommentColumnFilters = {};
	
	var status = 'ACTIVE'
		var element = $('#blockedVideoCommentsTab');
	if((element.className).indexOf('active') > -1) {
		status = 'BLOCKED'
	}
	viewVideoComments(status,1);
}

var videoCommentPagingInfo;
var videoCommentDataView;
var videoCommentGrid;
var videoCommentData = [];
var videoCommentGridPager;
var videoCommentSearchString='';
var videoCommentColumnFilters = {};
var userVideoCommentColumnsStr = '<%=session.getAttribute("videoCommentGrid")%>';

var userVideoCommentColumns = [];
var allVideoCommentColumns = [
	{ id : "userId", name : "Comment by User ID", field : "userId", width:80, sortable : true }, 
 	 { id : "email",		name : "Comment By Email",		field : "email",		width:80,		sortable : true 	}, 
 	 { id : "phone",		name : "Comment By Phone",		field : "phone",		width:80,		sortable : true 	}, 
 	 { id : "commentText",		name : "Comment Text",		field : "commentText",		width:80,		sortable : true 	},
 	 { id : "imageUrl", name : "Image Preview", field : "imageUrl", width:80, formatter: previewVideoCommentImageFormatter }, 
 	 { id : "videoUrl",		name : "Video Preview",		field : "videoUrl",		width:80,		formatter: previewVideoCommentVideoFormatter	},
 	 { id : "thumbnailUrl",		name : "Thumbnail Preview",		field : "thumbnailUrl",		width:80,		formatter: previewVideoCommentThubmFormatter	}, 
 	 { id : "createdBy",		name : "TFF Created By User ID",		field : "createdBy",		width:80,		sortable : true 	}, 
 	 { id : "updatedBy",		name : "TFF Updated By User ID",		field : "updatedBy",		width:80,		sortable : true 	}, 
 	 { id : "status",		name : "Status",		field : "status",		width:80,		sortable : true 	}, 
 	 { id : "createdDate",		name : "Created Date",		field : "createdDate",		width:80,		sortable : true 	}, 
 	 { id : "updatedDate",		name : "Updated Date",		field : "updatedDate",		width:80,		sortable : true 	}, 
 	 { id : "blockReason",		name : "Block Reason",		field : "blockReason",		width:80,		sortable : true 	}
	];

function previewVideoCommentImageFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.imageUrl != null &&  dataContext.imageUrl != 'null' && dataContext.imageUrl != ''){
		var button = "<img class='videoImage' src='../resources/images/down.png' id='"+ dataContext.imageUrl +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}
function previewVideoCommentVideoFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.videoUrl != null &&  dataContext.videoUrl != 'null' && dataContext.videoUrl != ''){
		var button = "<img class='videoImage' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}
function previewVideoCommentThubmFormatter(row,cell,value,columnDef,dataContext){
	if(dataContext.thumbnailUrl != null &&  dataContext.thumbnailUrl != 'null' && dataContext.thumbnailUrl != ''){
		var button = "<img class='videoImage' src='../resources/images/down.png' id='"+ dataContext.thumbnailUrl +"'/>";
		return button;
	}else{
		var button = "<label'/>N/A</label>";
		return button;
	}
}

/*$('.videoImage').live('click', function(){
	var me = $(this), id = me.attr('id');
	window.open(id,'MyWindow',width=600,height=300);
});*/

if (userVideoCommentColumnsStr != 'null' && userVideoCommentColumnsStr != '') {
	columnOrder = userVideoCommentColumnsStr.split(',');
	var columnWidth = [];
	for ( var i = 0; i < columnOrder.length; i++) {
		columnWidth = columnOrder[i].split(":");
		for ( var j = 0; j < allVideoCommentColumns.length; j++) {
			if (columnWidth[0] == allVideoCommentColumns[j].id) {
				userVideoCommentColumns[i] = allVideoCommentColumns[j];
				userVideoCommentColumns[i].width = (columnWidth[1] - 5);
				break;
			}
		}

	}
} else {
	userVideoCommentColumns = allVideoCommentColumns;
}

var videoCommentOptions = {
	editable: true,
	enableCellNavigation : true,
	asyncEditorLoading: false,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};
var videoCommentGridSortcol = "id";
var videoCommentGridSortdir = 1;
var percentCompleteThreshold = 0;

function videoCommentGridComparer(a, b) {
	var x = a[videoCommentGridSortcol], y = b[videoCommentGridSortcol];
	if (!isNaN(x)) {
		return (parseFloat(x) == parseFloat(y) ? 0
				: (parseFloat(x) > parseFloat(y) ? 1 : -1));
	}
	if (x == '' || x == null) {
		return 1;
	} else if (y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String)
			&& (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));
	}
}

function refreshVideoCommentGridValues(jsonData,status) {
	$("div#divLoading").addClass('show');
	videoCommentData = [];
	if (jsonData != null && jsonData.length > 0) {
		for ( var i = 0; i < jsonData.length; i++) {
			var data = jsonData[i];
			var d = (videoCommentData[i] = {});
			d["id"] = i;
			d["commentId"] = data.id;
			d["videoId"] = data.videoId;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["commentText"] = data.commentText;
			d["imageUrl"] = data.imageUrl;
			d["videoUrl"] = data.mediaUrl;
			d["thumbnailUrl"] = data.posterUrl;
			d["createdBy"] = data.createdBy;
			d["updatedBy"] = data.updatedBy;
			d["status"] = data.status;
			d["createdDate"] = data.createdDateStr;
			d["updatedDate"] = data.updatedDateStr;
			d["blockReason"] = data.blockReason;
			
		}
	}

	videoCommentDataView = new Slick.Data.DataView();
	videoCommentGrid = new Slick.Grid("#videoComment_grid", videoCommentDataView,
			userVideoCommentColumns, videoCommentOptions);
	videoCommentGrid.registerPlugin(new Slick.AutoTooltips({
		enableForHeaderCells : true
	}));
	videoCommentGrid.setSelectionModel(new Slick.RowSelectionModel());
	
	videoCommentGridPager = new Slick.Controls.Pager(videoCommentDataView,
				videoCommentGrid, $("#videoComment_pager"),
				videoCommentPagingInfo);
	var videoCommentGridColumnpicker = new Slick.Controls.ColumnPicker(
			allVideoCommentColumns, videoCommentGrid, videoCommentOptions);
		
	videoCommentGrid.onSort.subscribe(function(e, args) {
		videoCommentGridSortdir = args.sortAsc ? 1 : -1;
		videoCommentGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			videoCommentDataView.fastSort(videoCommentGridSortcol, args.sortAsc);
		} else {
			videoCommentDataView.sort(videoCommentGridComparer, args.sortAsc);
		}
	});
	
	// wire up model discountCodes to drive the videoCommentGrid
	videoCommentDataView.onRowCountChanged.subscribe(function(e, args) {
		videoCommentGrid.updateRowCount();
		videoCommentGrid.render();
	});
	videoCommentDataView.onRowsChanged.subscribe(function(e, args) {
		videoCommentGrid.invalidateRows(args.rows);
		videoCommentGrid.render();
	});
	$(videoCommentGrid.getHeaderRow())
			.delegate(
					":input",
					"keyup",
					function(e) {
						var keyCode = (e.keyCode ? e.keyCode : e.which);
						videoCommentSearchString = '';
						var columnId = $(this).data("columnId");
						if (columnId != null) {
							videoCommentColumnFilters[columnId] = $.trim($(this)
									.val());
							if (keyCode == 13) {
								for ( var columnId in videoCommentColumnFilters) {
									if (columnId !== undefined
											&& videoCommentColumnFilters[columnId] !== "") {
										videoCommentSearchString += columnId
												+ ":"
												+ videoCommentColumnFilters[columnId]
												+ ",";
									}
								}
								viewVideoComments(status,1);
							}
						}

					});
	videoCommentGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
		if (args.column.id.indexOf('checkbox') == -1) {				
			$("<input type='text'>").data("columnId", args.column.id)
					.val(videoCommentColumnFilters[args.column.id]).appendTo(
							args.node);				
		}
	});
	videoCommentGrid.init();
	// initialize the model after all the discountCodes have been hooked up
	videoCommentDataView.beginUpdate();
	videoCommentDataView.setItems(videoCommentData);
	//videoCommentDataView.setFilter(filter);
	videoCommentDataView.endUpdate();
	videoCommentDataView.syncGridSelection(videoCommentGrid, true);
	//videoCommentGrid.resizeCanvas();
	$("#gridContainer").resizable();
	$("div#divLoading").removeClass('show');
}


function viewVideoComments(status,pageNo){
	var tempVideoRowIndex = videoGrid.getSelectedRows([0])[0];
	var videoId = 0;
	if(tempVideoRowIndex >= 0) {
		videoId = videoGrid.getDataItem(tempVideoRowIndex).videoId;
	}
	if(videoId <=0 ){
		jAlert("Please select video to view comments.");
		return;
	}
	resetVideoCommentTabSelection(status);
	
	$.ajax({
		url : "${pageContext.request.contextPath}/GetFanClubPostComments.json",
		type : "post",
		dataType: "json",
		data: "commentType=VIDEO&commentTypeId="+videoId+"&status="+status+"&pageNo="+pageNo+"&headerFilter="+videoCommentSearchString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
				return;
			}
			videoCommentPagingInfo = jsonData.pagingInfo;
			refreshVideoCommentGridValues(jsonData.postComments,status);
			$('#videoComment_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUVideoCommentsPreference()'>");
			$('#view-video-comments').modal('show');			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function blockVideoComment(){
	var tempRowIndex = videoCommentGrid.getSelectedRows([0])[0];
	var videocommentId=0;
	var videoId=0;
	if(tempRowIndex >= 0) {
		videocommentId = videoCommentGrid.getDataItem(tempRowIndex).commentId;
		videoId = videoCommentGrid.getDataItem(tempRowIndex).videoId;
	}
	
	if (videocommentId<= 0 || videoId <= 0) {
		jAlert("Please Select Comment to Remove/Block.", "info");
		return false;
	}
	jConfirm("Are you sure to remove selected comment from fan club video?","Confirm",function(r){
		if (r) {
			$.ajax({
				url : "${pageContext.request.contextPath}/UpdateFanClubPostComments.json",
				type : "post",
				dataType: "json",
				data: "commentId="+videocommentId+"&action=DELETE&commentType=VIDEO&commentTypeId="+videoId,
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){					
						videoCommentPagingInfo = jsonData.pagingInfo;					
						refreshVideoCommentGridValues(jsonData.postComments);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	});
}
function blockVideoCommentUser(){
	var tempRowIndex = videoCommentGrid.getSelectedRows([0])[0];
	var videocommentId=0;
	var customerId=0;
	if(tempRowIndex >= 0) {
		videocommentId = videoCommentGrid.getDataItem(tempRowIndex).commentId;
		customerId = videoCommentGrid.getDataItem(tempRowIndex).customerId;
	}
	
	if (videocommentId<= 0 || customerId <= 0) {
		jAlert("Please Select Comment to Block Customer.", "info");
		return false;
	}
	jConfirm("Are you sure to Block selected comment Customer?","Confirm",function(r){
		if (r) {
			$.ajax({
				url : "${pageContext.request.contextPath}/BlockCustomerComment.json",
				type : "video",
				dataType: "json",
				data: "customerId="+customerId+"&sourceId="+videocommentId+"&source=VIDEOCOMMENT",
				success : function(response){				
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){					
						//videoCommentPagingInfo = jsonData.pagingInfo;					
						//refreshVideoCommentGridValues(jsonData.videoComments);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
						return;
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	});
}
function resetVideoCommentTabSelection(TAB){
	if(TAB == 'ACTIVE'){
		$('#activeVideoCommentsTab').addClass('active');
		$('#activeVideoC').addClass('active');
		$('#blockedVideoCommentsTab').removeClass("active");
		$('#blockedVideoC').removeClass("active");
	}else if(TAB == 'BLOCKED'){
		//alert(TAB);
		//$('#activeCommentsTab').removeClass('active');
		$('#blockedVideoCommentsTab').addClass("active");
		$('#blockedVideoC').addClass("active");
		$('#activeVideoCommentsTab').removeClass("active");
		$('#activeVideoC').removeClass('active');
	}
}
function getSelectedVideoGridId() {
	var tempRowIndex = videoGrid.getSelectedRows([0])[0];
	var videoId=0;
	if(tempRowIndex >= 0) {
		videoId = videoGrid.getDataItem(tempRowIndex).videoId;
	}
	return videoId;
}
function saveUVideoCommentsPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = videoCommentGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+","
	}
	saveUserPreference('videoCommentGrid',colStr);
}
 
 /* video comment grid ends */
	
</script>

</body>
</html>