<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Tracking By Date</title>
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css"
	type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet"
	href="../resources/js/slick/controls/slick.columnpicker1.css"
	type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
	src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/app/getCityStateCountry.js"></script>
<style>
.cell-title {
	font-weight: bold;
}

.slick-viewport {
	overflow-x: hidden !important;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

.slick-viewport {
	overflow-x: hidden !important;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$("div#divLoading").addClass('show');
	
	var todaysDate = new Date(); // Gets today's date
		
	// Max date attribute is in "MM/DD/YYYY".
	var year = todaysDate.getFullYear();                        // YYYY
	var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);  // MM
	var day = ("0" + todaysDate.getDate()).slice(-2);           // DD
	
	var maxDate = month + "/" + day + "/" + year; 
	
	 $('#fromDate').datepicker({
	    format: "mm/dd/yyyy",
		endDate: maxDate, //todayDate
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		endDate: maxDate, //todayDate
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  discountCodeTrackGrid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
		    $('#searchSharingBtn').click();
		    return false;  
		  }
	});
		
});
	
function saveUserDiscountCodeTrackPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = discountCodeTrackGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('discountcodetrackgrid',colStr);
}

function resetFilters(){
	discountCodeTrackSearchString='';
	columnFilters = {};
	getDiscountCodeTrackGridData(0);
}

function exportToExcel(){
	var appendData = "fromDate="+$('#fromDate').val()+"&fromTime="+$('#fromTime').val()+"&toDate="+$('#toDate').val()+"&toTime="+$('#toTime').val();
	appendData += "&headerFilter="+discountCodeTrackSearchString;
    //var url = "${pageContext.request.contextPath}/Client/TrackSharingDiscountCodeExportToExcel?"+appendData;
	var url = apiServerUrl+"TrackSharingDiscountCodeExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function submitSearchForm(){
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var date1 = new Date(fromDate);
	var date2 = new Date(toDate);
	if(date1<= date2){
		discountCodeTrackSearchString='';
		columnFilters = {};
		getDiscountCodeTrackGridData(0);
	}else{
		jAlert("Please select From Date less than To Date.", "info");
		return false;
	}	
}
</script>

<style>
input {
	color: black !important;
}
</style>
</head>
<body>

	<div class="row">
		<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
		<div class="col-lg-12">
			<h3 class="page-header">
				<i class="fa fa-laptop"></i> CUSTOMERS
			</h3>
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="#">Customers</a></li>
				<li><i class="fa fa-laptop"></i>Track Discount Code Sharing</li>
			</ol>

		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 filters-div">
			<form:form role="form" id="trackDiscountCodeSearch" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/Client/GetTrackSharingDiscountCode">			
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="fromDate" class="control-label">From Date</label> 
					<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">					
				</div>
				<div class="form-group col-lg-1 col-md-2 col-sm-2 col-xs-2" style="width:100px;">
					<label for="fromDate" class="control-label">From Time</label> 
					<select class="form-control" name="fromTime" id="fromTime">						
						<option value="00">12 am</option>
						<option value="01">1 am</option>
						<option value="02">2 am</option>
						<option value="03">3 am</option>
						<option value="04">4 am</option>
						<option value="05">5 am</option>
						<option value="06">6 am</option>
						<option value="07">7 am</option>
						<option value="08">8 am</option>
						<option value="09">9 am</option>
						<option value="10">10 am</option>
						<option value="11">11 am</option>
						<option value="12">12 pm</option>
						<option value="13">1 pm</option>
						<option value="14">2 pm</option>
						<option value="15">3 pm</option>
						<option value="16">4 pm</option>
						<option value="17">5 pm</option>
						<option value="18">6 pm</option>
						<option value="19">7 pm</option>
						<option value="20">8 pm</option>
						<option value="21">9 pm</option>
						<option value="22">10 pm</option>
						<option value="23">11 pm</option>
					</select>					
				</div>
				<div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-1">
					&nbsp;					
				</div>
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<label for="name" class="control-label">To Date</label> 
					<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
				</div>
				<div class="form-group col-lg-1 col-md-2 col-sm-2 col-xs-2" style="width:100px;">
					<label for="fromDate" class="control-label">To Time</label> 
					<select class="form-control" name="toTime" id="toTime">						
						<option value="00">12 am</option>
						<option value="01">1 am</option>
						<option value="02">2 am</option>
						<option value="03">3 am</option>
						<option value="04">4 am</option>
						<option value="05">5 am</option>
						<option value="06">6 am</option>
						<option value="07">7 am</option>
						<option value="08">8 am</option>
						<option value="09">9 am</option>
						<option value="10">10 am</option>
						<option value="11">11 am</option>
						<option value="12">12 pm</option>
						<option value="13">1 pm</option>
						<option value="14">2 pm</option>
						<option value="15">3 pm</option>
						<option value="16">4 pm</option>
						<option value="17">5 pm</option>
						<option value="18">6 pm</option>
						<option value="19">7 pm</option>
						<option value="20">8 pm</option>
						<option value="21">9 pm</option>
						<option value="22">10 pm</option>
						<option value="23">11 pm</option>						
					</select>					
				</div>
				<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
					<button type="button" id="searchSharingBtn" class="btn btn-primary" onclick="submitSearchForm()" style="width:100px;margin-top:19px;">search</button>
				</div>
			</form:form>
		</div>
	</div>

	<div style="position: relative">
		<div style="width: 100%;">
			<div class="table-responsive grid-table mt-20">
				<div class="grid-header full-width">
					<label>Track Discount Code Sharing</label>
					<div class="pull-right">
						<a href="javascript:exportToExcel()" name="Export to Excel" style="float: right; margin-right: 10px;">Export to Excel</a> 
						<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
					</div>
				</div>
				<div id="myGrid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
				<div id="pager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
	</div>


<script>
	
	var pagingInfo;
	var discountCodeTrackDataView;
	var discountCodeTrackGrid;
	var discountCodeTrackData = [];
	var discountCodeTrackSearchString='';
	var columnFilters = {};
	var discountCodeTrackColumnsStr = '<%=session.getAttribute("discountcodetrackgrid")%>';
	var discountCodeTrackColumns =[];
	var loadDiscountCodeTrackColumns = ["firstName", "lastName", "email", "referrerCode", "facebookCnt", "twitterCnt", 
										"linkedinCnt", "whatsappCnt", "androidCnt", "imessageCnt", "googleCnt", "outlookCnt"];
	var allDiscountCodeTrackColumns = [ {
		id : "customerId",
		name : "Customer Id",
		field : "customerId",
		sortable : true
	}, {
		id : "firstName",
		name : "First Name",
		field : "firstName",
		sortable : true
	}, {
		id : "lastName",
		name : "Last Name",
		field : "lastName",
		sortable : true
	}, {
		id : "email",
		name : "Email",
		field : "email",
		sortable : true
	}, {
		id : "referrerCode",
		name : "Referrer Code",
		field : "referrerCode",
		sortable : true
	}, {
		id : "facebookCnt",
		name : "Facebook",
		field : "facebookCnt",
		sortable : true
	}, {
		id : "twitterCnt",
		name : "Twitter",
		field : "twitterCnt",
		sortable : true
	}, {
		id : "linkedinCnt",
		name : "LinkedIn",
		field : "linkedinCnt",
		sortable : true
	}, {
		id : "whatsappCnt",
		name : "WhatsApp",
		field : "whatsappCnt",
		sortable : true
	},{
		id : "androidCnt",
		name : "Android",
		field : "androidCnt",
		sortable : true
	}, {
		id : "imessageCnt",
		name : "IMessage",
		field : "imessageCnt",
		sortable : true
	}, {
		id : "googleCnt",
		name : "Google",
		field : "googleCnt",
		sortable : true
	}, {
		id : "outlookCnt",
		name : "Outlook",
		field : "outlookCnt",
		sortable : true
	}	
	];
	
	if(discountCodeTrackColumnsStr!='null' && discountCodeTrackColumnsStr!=''){
		columnOrder = discountCodeTrackColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allDiscountCodeTrackColumns.length;j++){
				if(columnWidth[0] == allDiscountCodeTrackColumns[j].id){
					discountCodeTrackColumns[i] =  allDiscountCodeTrackColumns[j];
					discountCodeTrackColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}			
		}
	}else{
		var columnOrder = loadDiscountCodeTrackColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allDiscountCodeTrackColumns.length;j++){
				if(columnWidth == allDiscountCodeTrackColumns[j].id){
					discountCodeTrackColumns[i] = allDiscountCodeTrackColumns[j];
					discountCodeTrackColumns[i].width=80;
					break;
				}
			}			
		}
		//discountCodeTrackColumns = allDiscountCodeTrackColumns;
	}
	
	var options = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
		multiSelect: false,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "customerIpAddress";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
	var searchString = "";
		
	/*function myFilter(item, args) {
		var x= item["customerIpAddress"];
		if (args.searchString  != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}*/
	
	function comparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*function toggleFilterRow() {
		discountCodeTrackGrid.setTopPanelVisibility(!discountCodeTrackGrid.getOptions().showTopPanel);
	}
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
		$(e.target).addClass("ui-state-hover")
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
	});*/
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getDiscountCodeTrackGridData(pageNo);
	}
	
	function getDiscountCodeTrackGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/Client/TrackSharingDiscountCode.json",
			type : "post",
			dataType: "json",
			data : $('#trackDiscountCodeSearch').serialize()+"&pageNo="+pageNo+"&headerFilter="+discountCodeTrackSearchString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshDiscountCodeTrackGridValues(jsonData.discountCodeTrackingList);
				clearAllSelections();
				$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserDiscountCodeTrackPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function refreshDiscountCodeTrackGridValues(jsonData){
		discountCodeTrackData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (discountCodeTrackData[i] = {});
			
				d["id"] = data.customerId;
				d["customerId"] = data.customerId;
				d["firstName"] = data.firstName;
				d["lastName"] = data.lastName;
				d["email"] = data.email;
				d["referrerCode"] = data.referrerCode;
				d["facebookCnt"] = data.facebookCnt;
				d["twitterCnt"] = data.twitterCnt;
				d["linkedinCnt"] = data.linkedinCnt;
				d["whatsappCnt"] = data.whatsappCnt;
				d["androidCnt"] = data.androidCnt;
				d["imessageCnt"] = data.imessageCnt;
				d["googleCnt"] = data.googleCnt;
				d["outlookCnt"] = data.outlookCnt;
			}
		}
	
		discountCodeTrackDataView = new Slick.Data.DataView();
		discountCodeTrackGrid = new Slick.Grid("#myGrid", discountCodeTrackDataView, discountCodeTrackColumns, options);
		discountCodeTrackGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		
		discountCodeTrackGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo != null){
			var pager = new Slick.Controls.Pager(discountCodeTrackDataView, discountCodeTrackGrid, $("#pager"),pagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(allDiscountCodeTrackColumns, discountCodeTrackGrid,options);
		
		// move the filter panel defined in a hidden div into discountCodeTrackGrid top panel
		//$("#inlineFilterPanel").appendTo(discountCodeTrackGrid.getTopPanel()).show();
			
		discountCodeTrackGrid.onSort.subscribe(function(e, args) {
			sortdir = args.sortAsc ? 1 : -1;
			sortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				discountCodeTrackDataView.fastSort(sortcol, args.sortAsc);
			} else {
				discountCodeTrackDataView.sort(comparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the discountCodeTrackGrid
		discountCodeTrackDataView.onRowCountChanged.subscribe(function(e, args) {
			discountCodeTrackGrid.updateRowCount();
			discountCodeTrackGrid.render();
		});
		
		discountCodeTrackDataView.onRowsChanged.subscribe(function(e, args) {
			discountCodeTrackGrid.invalidateRows(args.rows);
			discountCodeTrackGrid.render();
		});
		
		$(discountCodeTrackGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			discountCodeTrackSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  discountCodeTrackSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getDiscountCodeTrackGridData(0);
				}
			  }			  
		});
		discountCodeTrackGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'facebookCnt' && args.column.id != 'twitterCnt' && args.column.id != 'linkedinCnt' && args.column.id != 'whatsappCnt' && 
					args.column.id != 'androidCnt' && args.column.id != 'imessageCnt' && args.column.id != 'googleCnt' && 
					args.column.id != 'outlookCnt'){
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
				}
			}
			
		});
		discountCodeTrackGrid.init();
			
		//var h_runfilters = null;
		// wire up the slider to apply the filter to the model
		/*$("#pcSlider,#pcSlider2").slider({
		  "range": "min",
		  "slide": function (event, ui) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			if (percentCompleteThreshold != ui.value) {
			  window.clearTimeout(h_runfilters);
			  h_runfilters = window.setTimeout(updateFilter, 10);
			  percentCompleteThreshold = ui.value;
			}
		  }
		});*/
		
		/*
		// wire up the search textbox to apply the filter to the model
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			discountCodeTrackDataView.setFilterArgs({
				searchString : searchString
			});
			discountCodeTrackDataView.refresh();
		}*/
		
		discountCodeTrackDataView.beginUpdate();
		discountCodeTrackDataView.setItems(discountCodeTrackData);
		
		/*discountCodeTrackDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		discountCodeTrackDataView.setFilter(myFilter);*/
		
		discountCodeTrackDataView.endUpdate();
		
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		discountCodeTrackDataView.syncGridSelection(discountCodeTrackGrid, true);
		$("#gridContainer").resizable();
		discountCodeTrackGrid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}
	
			
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
	
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshDiscountCodeTrackGridValues(JSON.parse(JSON.stringify(${discountCodeTrackingList})));
		$('#pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserDiscountCodeTrackPreference()'>");		
	});
</script>
</body>
</html>
