<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
	<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
	<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script>
$(document).ready(function(){
	
});
window.onresize = function(){
	poGrid.resizeCanvas();
	invoiceGrid.resizeCanvas();
}
</script>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Accounting
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
			</li>
			<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Purchase Orders</li>
		</ol>
	</div>
</div>
<br />
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
	</div>
	<br />
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-block alert-danger fade in">
		<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
	</div>
	<br />
</c:if>

<div style="position: relative" id="purchaseOrder">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Purchase Order</label> <span id="openOrder_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"></span>
		</div>
		<div id="po_grid" style="width: 100%; height: 100px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="po_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<br/><br/>
<div style="position: relative" id="invoice">
	<div style="width: 100%;">
		<div class="grid-header" style="width: 100%">
			<label>Invoices</label> <span id="openOrder_grid_toogle_search" style="float: right"
				class="ui-icon ui-icon-search" title="Toggle search panel"></span>
		</div>
		<div id="invoice_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="invoice_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>
<script>
var poPagingInfo=JSON.parse(JSON.stringify(${poPagingInfo}));
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var poDataView;
var poGrid;
var poData = [];
var poColumns = [ {
	id : "id",
	name : "PO ID",
	field : "id",
	sortable : true
},{
	id : "total",
	name : "PO Total",
	field : "total",
	sortable : true
},{
	id : "totalQuantity",
	name : "Total Quantity",
	field : "totalQuantity",
	sortable : true
},{
	id : "usedQunatity",
	name : "Used Qunatity",
	field : "usedQunatity",
	sortable : true
},{
	id : "vendor",
	name : "Vendor/Brocker",
	field : "vendor",
	sortable : true
},{
	id : "vendorCsr",
	name : "Vendor CSR",
	field : "vendorCsr",
	sortable : true
}, {
	id : "createdOn",
	name : "Created On",
	field : "createdOn",
	width:80,
	sortable : true
}, {
	id : "intNote",
	name : "Int Note",
	field : "intNote",
	sortable : true
}, {
	id : "extNote",
	name : "Ext Note",
	field : "extNote",
	sortable : true
}];

var poOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
};
var poSortcol = "id";
var poSortdir = 1;
var poSearchString = "";

function poFilter(item, args) {
	var x= item["id"];
	if (args.eventGridSearchString  != ""
			&& x.indexOf(args.poSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.poSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function poComparer(a, b) {
	var x = a[poSortcol], y = b[poSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function refreshPOGridValues(jsonData) {
		
		poData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (poData[i] = {});
				d["id"] = data.id;
				d["total"] = data.poTotal;
				d["totalQuantity"] = data.totalQuantity;
				d["usedQunatity"] = data.usedQunatity;
				d["vendor"] = data.customerName;
				d["vendorCsr"] = data.csr;
				d["createdOn"] = data.createTimeStr;
				d["intNote"] = data.internalNotes;
				d["extNote"] = data.externalNotes;
			}
		}
					
		poDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		poGrid = new Slick.Grid("#po_grid", poDataView, poColumns, poOptions);
		poGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		poGrid.setSelectionModel(new Slick.RowSelectionModel());
		var poPager = new Slick.Controls.Pager(poDataView, poGrid, $("#po_pager"),poPagingInfo);
		var poColumnpicker = new Slick.Controls.ColumnPicker(poColumns, poGrid,
			poOptions);

	poGrid.onSort.subscribe(function(e, args) {
		poSortdir = args.sortAsc ? 1 : -1;
		poSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			poDataView.fastSort(poSortcol, args.sortAsc);
		} else {
			poDataView.sort(poComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	poDataView.onRowCountChanged.subscribe(function(e, args) {
		poGrid.updateRowCount();
		poGrid.render();
	});
	poDataView.onRowsChanged.subscribe(function(e, args) {
		poGrid.invalidateRows(args.rows);
		poGrid.render();
	});

	// initialize the model after all the events have been hooked up
	poDataView.beginUpdate();
	poDataView.setItems(poData);
	poDataView.endUpdate();
	poDataView.syncGridSelection(poGrid, true);
	$("#gridContainer").resizable();
	poGrid.resizeCanvas();
}
var invoiceDataView;
var invoiceGrid;
var invoiceData = [];
var invoiceColumns = [ {
	id : "invoiceId",
	name : "Invoice Id",
	field : "invoiceId",
	sortable : true
},{
	id : "eventName",
	name : "Event Name",
	field : "eventName",
	sortable : true
},{
	id : "section",
	name : "Section",
	field : "section",
	sortable : true
},{
	id : "row",
	name : "Row",
	field : "row",
	sortable : true
}, {
	id : "seat",
	name : "Seat",
	field : "seat",
	width:80,
	sortable : true
}, {
	id : "retailPrice",
	name : "Retail Price",
	field : "retailPrice",
	sortable : true
}, {
	id : "facePrice",
	name : "Face Price",
	field : "facePrice",
	sortable : true
}, {
	id : "cost",
	name : "Cost",
	field : "cost",
	sortable : true
}, {
	id : "wholesalePrice",
	name : "Wholesale Price",
	field : "wholesalePrice",
	sortable : true
}, {
	id : "actualSoldPrice",
	name : "Actual Sold Price",
	field : "actualSoldPrice",
	sortable : true
}, {
	id : "purchasedBy",
	name : "Purchased By",
	field : "purchasedBy",
	sortable : true
}];

var invoiceOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
};
var invoiceSortcol = "id";
var invoiceSortdir = 1;
var invoiceSearchString = "";

function invoiceFilter(item, args) {
	var x= item["id"];
	if (args.eventGridSearchString  != ""
			&& x.indexOf(args.invoiceSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.invoiceSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function invoiceComparer(a, b) {
	var x = a[invoiceSortcol], y = b[invoiceSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function refreshInvoiceGridValues(jsonData) {
	
	invoiceData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];
			var d = (invoiceData[i] = {});
			d["id"] = data.id;
			d["invoiceId"] = data.invoiceId;
			d["eventName"] = data.eventName;
			d["section"] = data.section;
			d["row"] = data.row;
			d["seat"] = data.seat;
			d["retailPrice"] = data.retailPrice;
			d["facePrice"] = data.facePrice;
			d["cost"] = data.cost;
			d["wholesalePrice"] = data.wholesalePrice;
			d["actualSoldPrice"] = data.actualSoldPrice;
			d["purchasedBy"] = data.purchasedBy;
		}
	}
					
		invoiceDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		invoiceGrid = new Slick.Grid("#invoice_grid", invoiceDataView, invoiceColumns, invoiceOptions);
		invoiceGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		invoiceGrid.setSelectionModel(new Slick.RowSelectionModel());
		var invoicePager = new Slick.Controls.Pager(invoiceDataView, invoiceGrid, $("#invoice_pager"),pagingInfo);
		var invoiceColumnpicker = new Slick.Controls.ColumnPicker(invoiceColumns, invoiceGrid,
			invoiceOptions);

	invoiceGrid.onSort.subscribe(function(e, args) {
		invoiceSortdir = args.sortAsc ? 1 : -1;
		invoiceSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			invoiceDataView.fastSort(invoiceSortcol, args.sortAsc);
		} else {
			invoiceDataView.sort(invoiceComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	invoiceDataView.onRowCountChanged.subscribe(function(e, args) {
		invoiceGrid.updateRowCount();
		invoiceGrid.render();
	});
	invoiceDataView.onRowsChanged.subscribe(function(e, args) {
		invoiceGrid.invalidateRows(args.rows);
		invoiceGrid.render();
	});

	// initialize the model after all the events have been hooked up
	invoiceDataView.beginUpdate();
	invoiceDataView.setItems(invoiceData);
	invoiceDataView.endUpdate();
	invoiceDataView.syncGridSelection(invoiceGrid, true);
	$("#gridContainer").resizable();
	invoiceGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
}
window.onload = function(){
	refreshPOGridValues(JSON.parse(JSON.stringify(${poList})));
	refreshInvoiceGridValues(JSON.parse(JSON.stringify(${ticketList})));	
};
</script>
