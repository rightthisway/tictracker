<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
    <link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
    
	<script src="../resources/js/jquery-1.8.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
    <script src="../resources/js/bootstrap.min.js"></script>
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/jquery.alerts.js"></script>
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: #f5f5f5;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

.slick-viewport {
	overflow-x: hidden !important;
}

#addCustomer {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

#totalPrice {
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
}

#totalQty {
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
}
</style>

<script>
$(document).ready(function(){
	var primaryPaymentMethod = '${order.primaryPaymentMethod}';
	var secondaryPaymentMethod = '${order.secondaryPaymentMethod}';
	var primaryPaymentAmount = '${order.primaryPayAmt}';
	var secondaryPaymentAmount = '${order.secondaryPayAmt}';

	if (primaryPaymentMethod == "CREDITCARD") {
		$('#paidCardAmount').text(primaryPaymentAmount);
		$('#usedRewardPoints').text(secondaryPaymentAmount);
	} else if (primaryPaymentMethod == "PARTIAL_REWARDS") {
		$('#paidCardAmount').text(secondaryPaymentAmount);
		$('#usedRewardPoints').text(primaryPaymentAmount);
	} else if (primaryPaymentMethod == "FULL_REWARDS") {
		$('#paidCardAmount').text(secondaryPaymentAmount);
		$('#usedRewardPoints').text(primaryPaymentAmount);
	}
	
});

function addNote(){
	var note = $('#manualNote').val();
	if(note=='' || note == null){
		alert("Please Add Some values in note.");
		return false;
	}
	$('#addNote').submit();
}
</script>


<div class="row">
	<div class="col-lg-12">

		<ol class="breadcrumb">
			<li><i class="fa fa-laptop"></i>View Order Details</li>
		</ol>

	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<c:if test="${successMessage != null}">
			<div class="alert alert-success fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;"> ${successMessage} <a href="javascript:goToInvoice(${invoice.id})"
					style="font-size: 15px;">${invoice.id}</a> </strong>
			</div>
		</c:if>
		<c:if test="${errorMessage != null}">
			<div class="alert alert-block alert-danger fade in">
				<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
			</div>
		</c:if>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<form id="searchOrderDetails" name="searchOrderDetails" action="${pageContext.request.contextPath}/Accounting/ViewOrderDetails">
			<div class="form-group">
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Order No.</label>
					<input type="text"  class="form-control" name="orderNo" id="orderNo" value="${orderNoStr}"> 
				</div>
				<%-- <div class="col-sm-2">
					<label style="font-size: 17px; font-weight: bold;">OR</label>
				</div>
				<div class="col-sm-3">
					<label style="font-size: 13px; font-weight: bold;">Invoice No.</label>
					<input type="text"  class="form-control" name="invoiceId" id="invoiceId" value="${invoiceIdStr}"> 
				</div> --%>
				<div class="col-sm-3">
					<button type="submit" class="btn btn-primary" style="margin-top: 22px;margin-left:-30%;">Search</button> 
				</div>
			</div>
		</form>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 20px;font-family: arial;" class="panel-heading">
		<b>Order Details </b></header>
		<div class="panel-body">

			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Order No : </label><span style="font-size: 12px;">${order.id}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Customer Name : </label> <span style="font-size: 12px;" id="customerNameDiv">${orderDetails.blFirstName}&nbsp;${orderDetails.blLastName}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Customer Email : </label> <span style="font-size: 12px;" >${orderDetails.blEmail}</span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Created On : </label> <span style="font-size: 12px;">${order.createdDateStr}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Shipping Method : </label> <span style="font-size: 12px;">${order.shippingMethod}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Order Total : </label><span style="font-size: 12px;">${order.orderTotal}</span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Payment Method : </label><span style="font-size: 12px;">${order.primaryPaymentMethod}</span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Paid Card Amount : </label><span id="paidCardAmount" style="font-size: 12px;"></span>
				</div>
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Used Reward points : </label><span id="usedRewardPoints" style="font-size: 12px;"></span>
				</div>
			</div>

		</div>
		</section>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 25px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> <b>${order.eventName},
			${order.eventDateStr},${order.eventTimeStr},${order.venueName}</b> </header>
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Section :</label> <span style="font-size: 12px;">${order.section}</span>
				</div>

				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Quantity :</label> <span style="font-size: 12px;">${order.qty}</span>
				</div>

				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Ticket Price :</label> <span style="font-size: 12px;">${order.ticketPrice}</span>
				</div>

			</div>

			<div class="form-group">
				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Seat Low :</label> <span style="font-size: 12px;">${order.seatLow}</span>
				</div>

				<div class="col-sm-4">
					<label style="font-size: 13px; font-weight: bold;">Seat High :</label> <span style="font-size: 12px;">${order.seatHigh}</span>
				</div>
				<div class="col-sm-12">
					<label style="font-size: 13px; font-weight: bold;">Sold Price :</label> <span style="font-size: 12px;">${order.soldPrice}</span>
				</div>
			</div>

		</div>
		</section>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<form id="addNotes" name="addNotes"  method="post" action="${pageContext.request.contextPath}/Accounting/ViewOrderDetails">
			<section class="panel"> 
				<header style="font-size: 25px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> <b>Add Note:</b> </header>
					<div class="panel-body" align="center">
						<div class="form-group">
							<input type="hidden" id="action" name="action" value="addNote" />
							<input type="hidden" id="invoiceId" name="invoiceId" value="${invoice.id}" />
							<div class="col-sm-8">
								 <textarea name="manualNote" id="manualNote" rows="5" cols="110" ></textarea>
							</div>
							<div class="col-sm-3">
								<button type="submit" class="btn btn-primary" style="margin-top: 19px;" onclick="addNote();">Add Note</button> 
							</div>
						</div>
					</div>
				</section>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div style="position: relative">
			<div style="width: 100%;">
				<br /> <br />
				<div class="grid-header" style="width: 100%">
					<label>Order Details</label> <!-- <a href="javascript:orderResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a> -->
				</div>
				<div id="orderGrid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
				<div id="orderPager" style="width: 100%; height: 20px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header style="font-size: 25px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> <b>Note:</b> </header>
		<div class="panel-body" align="center">
			<div class="form-group">
				<div class="col-sm-4">
					<textarea name="auditNote" id="auditNote" readonly rows="5" cols="110" ></textarea>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
var selectedRow='';
var id='';
var orderData=[];
var orderDataView;
var pagingInfo=JSON.parse(JSON.stringify(${pagingInfo}));
var orderGrid;
var orderData = [];
var orderColumns = [ {
	id : "id",
	name : "ID",
	field : "id",
	sortable : true
},{
	id : "invoiceId",
	name : "Invoice Id",
	field : "invoiceId",
	sortable : true
},{
	id : "action",
	name : "Action",
	field : "action",
	sortable : true
},{
	id : "createdDate",
	name : "Date & Time",
	field : "createdDate",
	sortable : true
}, {
	id : "createdBy",
	name : "Performed By",
	field : "createdBy",
	width:80,
	sortable : true
},{
	id : "note",
	name : "Note",
	field : "note",
	sortable : true
}];

var orderOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var orderSortcol = "id";
var orderSortdir = 1;
var orderSearchString = "";
var percentCompleteThreshold = 0;

function orderFilter(item, args) {
	var x= item["id"];
	if (args.orderSearchString  != ""
			&& x.indexOf(args.orderSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.orderSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function orderComparer(a, b) {
	var x = a[orderSortcol], y = b[orderSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function openOrderGridToggleFilterRow() {
		orderGrid.setTopPanelVisibility(!orderGrid.getOptions().showTopPanel);
	}

function refreshOrderGridValues(jsonData) {
	orderData=[];	
	if(jsonData!=null && jsonData!= '' && jsonData.length > 0){
		for(var i=0;i<jsonData.length;i++){
			var  data= jsonData[i];
			var d = (orderData[i] = {});
			d["id"] = data.id;			
			d["invoiceId"] = data.invoiceId;
			d["action"] = data.action;
			d["createdDate"] = data.createdDate;
			d["createdBy"] = data.createdBy;
			d["note"] = data.note;
		}	
	}
	
	orderDataView = new Slick.Data.DataView({
		inlineFilters : true
	});
	orderGrid = new Slick.Grid("#orderGrid", orderDataView, orderColumns, orderOptions);
	orderGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	orderGrid.setSelectionModel(new Slick.RowSelectionModel());		
	var orderPager = new Slick.Controls.Pager(orderDataView, orderGrid, $("#orderPager"),pagingInfo);
	var orderColumnpicker = new Slick.Controls.ColumnPicker(orderColumns, orderGrid,
		orderOptions);
	
	
	orderGrid.onSort.subscribe(function(e, args) {
		orderSortdir = args.sortAsc ? 1 : -1;
		orderSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			orderDataView.fastSort(orderSortcol, args.sortAsc);
		} else {
			orderDataView.sort(orderComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	orderDataView.onRowCountChanged.subscribe(function(e, args) {
		orderGrid.updateRowCount();
		orderGrid.render();
	});
	orderDataView.onRowsChanged.subscribe(function(e, args) {
		orderGrid.invalidateRows(args.rows);
		orderGrid.render();
	});
		
	function updateOrderGridFilter() {
		orderDataView.setFilterArgs({
			orderSearchString : orderSearchString
		});
		orderDataView.refresh();
	}
	var eventrowIndex;
	orderGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = orderGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
			}
			selectedRow = orderGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				$('#auditNote').val(orderGrid.getDataItem(temprEventRowIndex).note);
			}else{
				$('#auditNote').val('');
			}			
		});

	// initialize the model after all the events have been hooked up
	orderDataView.beginUpdate();
	orderDataView.setItems(orderData);
	
	orderDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			orderSearchString : orderSearchString
		});
	orderDataView.setFilter(orderFilter);
		
	orderDataView.endUpdate();
	orderDataView.syncGridSelection(orderGrid, true);
	$("#gridContainer").resizable();
	orderGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
	
}
window.onload = function() {
	refreshOrderGridValues(JSON.parse(JSON.stringify(${audits})));
	
};
window.onresize = function(){
	orderGrid.resizeCanvas();
};
</script>