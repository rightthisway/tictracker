<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(custProfileQuestionGrid != null && custProfileQuestionGrid != undefined){
			custProfileQuestionGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${status == 'ACTIVE'}">	
			$('#activeQuestion').addClass('active');
			$('#activeQuestionTab').addClass('active');
		</c:when>
		<c:when test="${status == 'DELETED'}">
			$('#deletedQuestion').addClass('active');
			$('#deletedQuestionTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeQuestion1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#deletedQuestion1").click(function(){
		callTabOnChange('DELETED');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/CustomerProfileQuestion?status="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Manage Customer</a></li>
			<li><i class="fa fa-laptop"></i>Customer Profile Questionsk</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeQuestionTab" class=""><a id="activeQuestion1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeQuestion">Active Questions</a></li>
				<li id="deletedQuestionTab" class=""><a id="deletedQuestion1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#deletedQuestion">Deleted Questions</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeQuestion" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetCustProfileQuestionModal();">Add Question</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editCustProfileQuestion()">Edit Question</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteCustProfileQuestion()">Delete Question</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="custProfileQuestionGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Customer Profile Questions</label>
							<div class="pull-right">
								<a href="javascript:custProfileQuestionExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:custProfileQuestionResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="custProfileQuestion_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="custProfileQuestion_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="deletedQuestion" class="tab-pane">
			<c:if test="${status =='DELETED'}">	
				<div class="full-width full-width-btn mb-20">
					<!-- <button class="btn btn-primary" id="editQBBtn" type="button" onclick="editCustProfileQuestion()">Edit Question</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deleteCustProfileQuestion()">Delete Question</button> -->
				</div>
				<br />
				<br />
				<div style="position: relative" id="custProfileQuestionGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Customer Profile Questions</label>
							<div class="pull-right">
								<a href="javascript:custProfileQuestionExportToExcel('DELETED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:custProfileQuestionResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="custProfileQuestion_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="custProfileQuestion_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Question Bank -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="qBModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Customer Profile Question</h4>
			</div>
			<div class="modal-body full-width">
				<form name="custProfileQuestionForm" id="custProfileQuestionForm" method="post">
					<input type="hidden" id="questionId" name="questionId" />
					<input type="hidden" id="questionType" name="questionType" value="TEXT" />
					<input type="hidden" id="profileType" name="profileType" value="FANDOM" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Question Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="question" name="question">
						</div>						
						<div class="form-group col-sm-8 col-xs-8">
							<label>Question Points<span class="required">*</span>
							</label> <input class="form-control" type="text" id="points" name="points">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qBSaveBtn" type="button" onclick="saveQuestion('save')">Save</button>
				<button class="btn btn-primary" id="qBUpdateBtn" type="button" onclick="saveQuestion('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Question Bank end here  -->
<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'custProfileQuestion_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getCustProfileQuestionGridData(pageNo);
		}
	}
	
	//Question Bank Grid
	
	function getCustProfileQuestionGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/CustomerProfileQuestion.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+custProfileQuestionSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshCustProfileQuestionGridValues(jsonData.questions);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function custProfileQuestionExportToExcel(status){
		var appendData = "headerFilter="+custProfileQuestionSearchString+"&status="+status;
	    var url =apiServerUrl+"CustomerProfileQuestionExport?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function custProfileQuestionResetFilters(){
		custProfileQuestionSearchString='';
		custProfileQuestionColumnFilters = {};
		getCustProfileQuestionGridData(0);
	}
	
	var questionCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var custProfileQuestionDataView;
	var custProfileQuestionGrid;
	var custProfileQuestionData = [];
	var custProfileQuestionGridPager;
	var custProfileQuestionSearchString='';
	var custProfileQuestionColumnFilters = {};
	var userCustProfileQuestionColumnsStr = '<%=session.getAttribute("customerProfileQuestionGird")%>';

	var userCustProfileQuestionColumns = [];
	var allCustProfileQuestionColumns = [
			 {
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "serialNo",
				field : "serialNo",
				name : "Serial No",
				width : 80,
				sortable : true
			},{
				id : "points",
				field : "points",
				name : "Points",
				width : 80,
				sortable : true
			},{
				id : "questionType",
				field : "questionType",
				name : "Question Type",
				width : 80,
				sortable : true
			},{
				id : "profileType",
				field : "profileType",
				name : "Profile Type",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "moveUp",
				field : "moveUp",
				name : "Move UP ",
				width : 80,
				formatter: moveUpFormatter
			} ,{
				id : "moveDown",
				field : "moveDown",
				name : "Move Down ",
				width : 80,
				formatter: moveDownFormatter
			} ];

	if (userCustProfileQuestionColumnsStr != 'null' && userCustProfileQuestionColumnsStr != '') {
		columnOrder = userCustProfileQuestionColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allCustProfileQuestionColumns.length; j++) {
				if (columnWidth[0] == allCustProfileQuestionColumns[j].id) {
					userCustProfileQuestionColumns[i] = allCustProfileQuestionColumns[j];
					userCustProfileQuestionColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userCustProfileQuestionColumns = allCustProfileQuestionColumns;
	}
	
	function moveUpFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='moveUpClickable' src='../resources/images/up.png'' id='"+ dataContext.custProfileQuestionId +"'/>";
	    return button;
	}
	$('.moveUpClickable').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    changeQuestionPosition('UP',id);
	});
	
	function moveDownFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='moveDonwlClickable' src='../resources/images/down.png'' id='"+ dataContext.custProfileQuestionId +"'/>";		
		return button;
	}
	
	
	$('.moveDonwlClickable').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    changeQuestionPosition('DOWN',id);
	});	
	
	var custProfileQuestionOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var custProfileQuestionGridSortcol = "custProfileQuestionId";
	var custProfileQuestionGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function custProfileQuestionGridComparer(a, b) {
		var x = a[custProfileQuestionGridSortcol], y = b[custProfileQuestionGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshCustProfileQuestionGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		custProfileQuestionData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (custProfileQuestionData[i] = {});
				d["id"] = i;
				d["custProfileQuestionId"] = data.id;
				d["question"] = data.question;
				d["serialNo"] = data.serialNo;
				d["points"] = data.points;
				d["questionType"] = data.questionType;
				d["profileType"] = data.profileType;
				d["status"] = data.status;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		custProfileQuestionDataView = new Slick.Data.DataView();
		custProfileQuestionGrid = new Slick.Grid("#custProfileQuestion_grid", custProfileQuestionDataView,
				userCustProfileQuestionColumns, custProfileQuestionOptions);
		custProfileQuestionGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		custProfileQuestionGrid.setSelectionModel(new Slick.RowSelectionModel());
		custProfileQuestionGrid.registerPlugin(questionCheckboxSelector);
		
		custProfileQuestionGridPager = new Slick.Controls.Pager(custProfileQuestionDataView,
					custProfileQuestionGrid, $("#custProfileQuestion_pager"),
					pagingInfo);
		/* var custProfileQuestionGridColumnpicker = new Slick.Controls.ColumnPicker(
				allCustProfileQuestionColumns, custProfileQuestionGrid, custProfileQuestionOptions); */
		
		custProfileQuestionGrid.onSort.subscribe(function(e, args) {
			custProfileQuestionGridSortdir = args.sortAsc ? 1 : -1;
			custProfileQuestionGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				custProfileQuestionDataView.fastSort(custProfileQuestionGridSortcol, args.sortAsc);
			} else {
				custProfileQuestionDataView.sort(custProfileQuestionGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the custProfileQuestionGrid
		custProfileQuestionDataView.onRowCountChanged.subscribe(function(e, args) {
			custProfileQuestionGrid.updateRowCount();
			custProfileQuestionGrid.render();
		});
		custProfileQuestionDataView.onRowsChanged.subscribe(function(e, args) {
			custProfileQuestionGrid.invalidateRows(args.rows);
			custProfileQuestionGrid.render();
		});
		$(custProfileQuestionGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							custProfileQuestionSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								custProfileQuestionColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in custProfileQuestionColumnFilters) {
										if (columnId !== undefined
												&& custProfileQuestionColumnFilters[columnId] !== "") {
											custProfileQuestionSearchString += columnId
													+ ":"
													+ custProfileQuestionColumnFilters[columnId]
													+ ",";
										}
									}
									getCustProfileQuestionGridData(0);
								}
							}

						});
		custProfileQuestionGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editCustProfileQuestion' && args.column.id != 'delCustProfileQuestion'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(custProfileQuestionColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(custProfileQuestionColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		custProfileQuestionGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		custProfileQuestionDataView.beginUpdate();
		custProfileQuestionDataView.setItems(custProfileQuestionData);
		//custProfileQuestionDataView.setFilter(filter);
		custProfileQuestionDataView.endUpdate();
		custProfileQuestionDataView.syncGridSelection(custProfileQuestionGrid, true);
		custProfileQuestionGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserCustProfileQuestionPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = custProfileQuestionGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('customerProfileQuestionGird', colStr);
	}
	
	// Add Question Bank	
	function resetCustProfileQuestionModal(){		
		$('#qBModal').modal('show');
		$('#question').prop('disabled', false);
		$('#questionId').val('');
		$('#question').val('');
		$('#points').val('');
		$('#qBSaveBtn').show();
		$('#qBUpdateBtn').hide();	
	}

	function saveQuestion(action){
		
		var question = $('#question').val();
		var points = $('#points').val();
		
		if(question == ''){
			jAlert("Question Text is Mandatory.");
			return;
		}
		if(points == ''){
			jAlert("Question Points are Mandatory.");
			return;
		}
		var requestUrl = "${pageContext.request.contextPath}/UpdateCustomerProfileQuestion.json";
		var dataString = "";
		if(action == 'save'){		
			dataString  = $('#custProfileQuestionForm').serialize()+"&action=SAVE&pageNo=0";
		}else if(action == 'update'){
			dataString = $('#custProfileQuestionForm').serialize()+"&action=UPDATE&pageNo=0";
		}
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			data: dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#qBModal').modal('hide');
					pagingInfo = jsonData.pagingInfo;
					custProfileQuestionColumnFilters = {};
					refreshCustProfileQuestionGridValues(jsonData.questions);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit Question Bank
	function editCustProfileQuestion(){
		var tempCustProfileQuestionRowIndex = custProfileQuestionGrid.getSelectedRows([0])[0];
		if (tempCustProfileQuestionRowIndex == null) {
			jAlert("Plese select Question to Edit", "info");
			return false;
		}else {
			var custProfileQuestionId = custProfileQuestionGrid.getDataItem(tempCustProfileQuestionRowIndex).custProfileQuestionId;
			getEditCustProfileQuestion(custProfileQuestionId);
		}
	}
	
	function changeQuestionPosition(action, id){
		$.ajax({
			url : "${pageContext.request.contextPath}/ChangeCustomerProfileQuestionPosition.json",
			type : "post",
			dataType: "json",
			data: "questionId="+id+"&action="+action,
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					pagingInfo = jsonData.pagingInfo;
					custProfileQuestionColumnFilters = {};
					refreshCustProfileQuestionGridValues(jsonData.questions);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function getEditCustProfileQuestion(questionId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateCustomerProfileQuestion.json",
			type : "post",
			dataType: "json",
			data: "questionId="+questionId+"&action=EDIT&pageNo=0",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#qBModal').modal('show');
					setEditCustProfileQuestion(jsonData.question);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditCustProfileQuestion(data){
		if(data.profileType == 'BASIC'){
			$('#question').prop('disabled', true);
		}else{
			$('#question').prop('disabled', false);
		}
		$('#qBSaveBtn').hide();
		$('#qBUpdateBtn').show();
		$('#questionId').val(data.id);
		$('#question').val(data.question);
		$('#points').val(data.points);
	}
	
	function getSelectedQuestionGridId() {
		var tempQuestionRowIndex = custProfileQuestionGrid.getSelectedRows();
		
		var questionIdStr='';
		$.each(tempQuestionRowIndex, function (index, value) {
			questionIdStr += ','+custProfileQuestionGrid.getDataItem(value).custProfileQuestionId;
		});
		
		if(questionIdStr != null && questionIdStr!='') {
			questionIdStr = questionIdStr.substring(1, questionIdStr.length);
			 return questionIdStr;
		}
	}
	//Delete Question Bank
	function deleteCustProfileQuestion(){
		var tempCustProfileQuestionRowIndex = custProfileQuestionGrid.getSelectedRows([0])[0];
		if (tempCustProfileQuestionRowIndex == null) {
			jAlert("Plese select Question to delete", "info");
			return false;
		}else {
			var questionId = custProfileQuestionGrid.getDataItem(tempCustProfileQuestionRowIndex).custProfileQuestionId;
			getDeleteCustProfileQuestion(questionId);
		}
	}
	
	
	function getDeleteCustProfileQuestion(questionId){
		jConfirm("Are you sure to delete selected Question ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateCustomerProfileQuestion.json",
						type : "post",
						dataType: "json",
						data : "questionId="+questionId+"&action=DELETE&pageNo=0",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.pagingInfo;
								custProfileQuestionColumnFilters = {};
								refreshCustProfileQuestionGridValues(jsonData.questions);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
								return;
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		refreshCustProfileQuestionGridValues(${questions});
		$('#custProfileQuestion_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserCustProfileQuestionPreference()'>");
		
		enableMenu();
	};
		
</script>