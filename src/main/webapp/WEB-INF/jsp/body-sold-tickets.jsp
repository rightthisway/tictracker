<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link href="../resources/css/jquery.alerts.css" rel=" stylesheet">
<script src="../resources/js/jquery-1.8.3.min.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<script src="../resources/js/jquery.alerts.js"></script>
<br />

<div align="center">
	<input id="closeButton" class="largeButton" name="closeButton" type="button" value="Click Here to Close & Refresh" onclick="closeChildWin();">
</div>
<style>
.odd {
	background-color: #BDBDBD;
}
.even {
	background-color: #F2F2F2;
}
table, th, td {
    border: 1px solid black;
}
</style>

<c:if test="${openOrderStatus ne null}">
	<div>
		<table>
			<tr>
				<td colspan="3"><b>Invoice No </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.invoiceNo}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Invoice Date </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.invoiceDateStr}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Event Name </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.eventName}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Event Date </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.eventDateStr}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Venue </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.venueName}, ${openOrderStatus.venueCity}, ${openOrderStatus.venueState}, ${openOrderStatus.venueCountry}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Section </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.section}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Row </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.row}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Sold Quantity </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.soldQty}</b>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b>Sold Price </b></td>
				<td colspan="3"><b> : &nbsp;&nbsp; ${openOrderStatus.actualSoldPrice}</b>
				</td>
			</tr>
		</table>
	</div>
</c:if>

<div style="float: right">
	<input id="nocrawl" name="nocrawl" type="hidden" value="${noCrawl}"> <input id="reload" name="reload" type="hidden" value="${reload}"> <input id="focusRowId" name="focusRowId"
		type="hidden" value="${openOrderStatus.id}"> <span class="refreshLink" style="display: none"> <img src="../resources/images/ico-reload.gif" align="absbottom" />
		Page updated <span id="numMinsSinceLastUpdate"></span> </span> <span class="refreshedRecently" style="display: none"> <img src="${pageContext.request.contextPath}//images/ico-reload.gif"
		align="absbottom" /> <i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}" />mn ago)</i> </span> <span class="refreshCrawlInfoSpan" style="display: none">
		<img src="${pageContext.request.contextPath}//images/process-running.gif" align="absbottom" /> <span class="refreshCrawlInfo" style="color: green"></span> </span>
</div>
<br />
<br />
<div>
	<c:choose>
		<c:when test="${noCrawl eq true}">
			<c:choose>
				<c:when test="${not empty tickets}">
					<table border="1">
						<tr class="tableHeader">
							<th>Id</th>
							<th>Event Name</th>
							<th>Event Date</th>
							<th>venue</th>
							<th>Quantity</th>
							<th>Section</th>
							<th>Row</th>
							<th>Purchase Price</th>
							<th>Ticket Delivery Type</th>
							<th>Seller</th>

						</tr>
						<c:forEach items="${tickets}" var="ticket" varStatus="loopStatus">
							<tr class="${loopStatus.count % 2 == 0 ? 'even' : 'odd'}">
								<td align="center">${ticket.id}</td>
								<td align="center">${ticket.event.name}</td>
								<td align="center">${ticket.event.formatedDate}</td>
								<td align="center">${ticket.event.venue.building}, ${ticket.event.venue.city }, ${ticket.event.venue.state}</td>
								<td align="center">${ticket.quantity}</td>
								<td align="center" >${ticket.section}</td>
								<td align="center">${ticket.row}</td>
								<td align="center">${ticket.purchasePrice}</td>
								<td align="center">${ticket.ticketDeliveryType}</td>
								<td align="center"><a href="RedirectToItemPage?id=${ticket.id}">${ticket.seller}</a></td>
							</tr>
						</c:forEach>
					</table>
					<input id="redirect" name="redirect" type="hidden" value="false">
				</c:when>
				<c:otherwise>
					<input id="redirect" name="redirect" type="hidden" value="true">
				</c:otherwise>
			</c:choose>
			<br />
			<br />
			<a href="http://tmatbrowse.rightthisway.com/a1/BrowseTickets?eventId=${ticket.eventId}" style="float: right; font-size: large;"><b>Go to Browse Ticket Page for This Event</b>
			</a>
		</c:when>
		<c:otherwise>
			<span class="message" style="color: red; font: bold; font-size: large;">Latest Tickets will be fetched shortly.</span>
		</c:otherwise>
	</c:choose>
</div>
<br />
<br />
<script type="text/javascript">

var totalCrawls;
updateCrawls($('#nocrawl').val());
function updateCrawls(isNoCrawl) {
	
	if(isNoCrawl=='true'){
		var red = $("#redirect").val();
		if('true'== red){
			$(".message").html('There is not ticket available for this order.. we are redirecting you to event page to show available tickets.');
			jConfirm("There is no ticket availables for this order..would you like to redirect event page to show available tickets.?","confirm",function(r){
				if(r){
					document.location.href = "http://tmatbrowse.rightthisway.com/a1/BrowseTickets?scroll=0&eventId="+ ${eventId} + "&reload=" + new Date().getTime();
				}
			});
		}else{
			var time= parseInt($('#reload').val());
		  	lastUpdated = new Date(time);
		  	//updateLatestTMATInformation();
          	showRefreshedRecently();
	      	return;
		}
	}
	
    
    $.ajax({
		url:"${pageContext.request.contextPath}/ForceRecrawlForEvent?eventId=" + ${eventId},
		success: function(res){
			var tokens = res.split('|');
			//jAlert(res);
			  if (tokens[0] != 'OK') {
				lastUpdated = new Date();
				showRefreshedRecently();
			 } else {
				totalCrawls = parseInt(tokens[1]);
				var runningCrawls = parseInt(tokens[2]);
			    if (totalCrawls == 0) {
					window.location.href = "GetSoldTickets?eventId=${eventId}&id=${id}&nocrawl=true&crawlTime=${crawlTime}";
				 //lastUpdated = new Date(parseInt(tokens[3]));
		          //showRefreshedRecently();
		          return;
		        }
		      	$('.refreshLink').hide();
		        $('.refreshedRecently').hide();
		      	$('.refreshCrawlInfoSpan').show();
		      	$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
		        setTimeout("getCrawlerStatus()", 10000);
		      }
		}
	});
  	return false;
}

function getCrawlerStatus() {
	$.ajax({
		url:"${pageContext.request.contextPath}/GetCrawlerStatus?eventId=" + ${eventId} + "&crawlTime=" + ${crawlTime},
		success: function(res){
			var tokens = res.split('|');
			  if (tokens[0] != 'OK') {
				lastUpdated = new Date();
		        showRefreshedRecently();
			 } else {
			    totalCrawls = parseInt(tokens[1]);
		        var runningCrawls = parseInt(tokens[2]);
			    if (totalCrawls == 0) {
				  //lastUpdated = new Date(tokens[3]);
				  window.location.href = "GetSoldTickets?eventId=${eventId}&id=${id}&nocrawl=true&crawlTime=${crawlTime}";
		          //showRefreshedRecently();
		          return;
		        }
		      	$('.refreshLink').hide();
		        $('.refreshedRecently').hide();
		      	$('.refreshCrawlInfoSpan').show();
		      	$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
				if(runningCrawls == 	totalCrawls){
					location.href = 'GetSoldTickets?eventId=${eventId}&crawlTime=${crawlTime}&id=${id}&nocrawl=true&reload=' + (new Date().getTime()) ;
				}else{
					setTimeout("getCrawlerStatus()", 10000);
				}
		      }
		}
	});
}
function checkCrawlsFinished() {
    CrawlerDwr.getCrawlsExecutedForEventCount(${eventId}, function(response) {
    	var result= response.split(":");
        var count = parseInt(result[0]) + parseInt(result[1]);
		$('.refreshCrawlInfo').html(parseInt(100 * (count / (totalCrawls))) + "% updated");
		if(count < (totalCrawls)) {
			setTimeout("checkCrawlsFinished()", 10000);
		} else {
			// force reload (avoid caching by adding a dummy parameter)
			$('.refreshCrawlInfo').html("100% updated! Reloading page...");
			location.href = 'GetSoldTickets?eventId=${eventId}&id=${id}&crawlTime=${crawlTime}&nocrawl=true&reload=' + (new Date().getTime()) + "&scroll=" + window.scrollY + anchor;
		}
    });
}
function showRefreshedRecently() {
	refreshRecentlySpanFirstTime();
/*
$('.refreshLink').hide();
$('.refreshedRecently').show();
var now = new Date();
var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
if (numMins >= ${1000 * ticketListingCrawler.minCrawlPeriod}) {
    refreshRecentlySpanFirstTime();
} else {
  setTimeout(refreshRecentlySpanFirstTime, ${1000 * ticketListingCrawler.minCrawlPeriod} - numMins);
}
*/
}

function refreshRecentlySpanFirstTime() {
	$('.refreshedRecently').hide();
	$('.refreshLink').show();
	refreshRecentlySpan();
	setInterval("refreshRecentlySpan()", 60000);
}


function refreshRecentlySpan() {
	var now = new Date();
	var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
	if (numMins == 0) {
	  $('#numMinsSinceLastUpdate').text("just now");
	} else {
	  $('#numMinsSinceLastUpdate').text(numMins + "mn ago");
	}
}

function closeChildWin() {
	var focusRowIdValue = $('#focusRowId').val();
	window.close();
	window.opener.location.href="OpenOrders?focusRow="+focusRowIdValue;
}

/* function updateLatestTMATInformation() {
	$.ajax({
		url:"GetRTWOrderTicketStatus?eventId=" + ${eventId} + "&orderId=" + ${id},
		success: function(res){
			var tokens = res.split('|');
			if (tokens[0] == 'OK') {
				//we can write any code
		    }
		}
	});
} */
</script>
