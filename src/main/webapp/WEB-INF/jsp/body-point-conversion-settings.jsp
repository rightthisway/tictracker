<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(pointSettingGrid != null && pointSettingGrid != undefined){
			pointSettingGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${status == 'ACTIVE'}">	
			$('#activeSetting').addClass('active');
			$('#activeSettingTab').addClass('active');
		</c:when>
		<c:when test="${status == 'INACTIVE'}">
			$('#inactiveSetting').addClass('active');
			$('#inactiveSettingTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeSetting1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#inactiveSetting1").click(function(){
		callTabOnChange('INACTIVE');
	});
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/PointConversionSettings?status="+selectedTab;
}

</script>

<!-- <ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
  <li data="edit question">Edit Question</li>
</ul> -->
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Polling</a></li>
			<li><i class="fa fa-laptop"></i>Manage reward settings</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeSettingTab" class=""><a id="activeSetting1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeSetting">Active Point Conversion Setting.</a></li>
				<li id="inactiveSettingTab" class=""><a id="inactiveSetting1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveSetting">InActive Point Conversion Setting.</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeSetting" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetPointSettingModal();">Add New Point Conversion</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPointSetting()">Edit Point Conversion</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePointSetting()">Delete Point Conversion</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="pointSettingGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>RTF Points Conversion settings</label>
							<div class="pull-right">
								<a href="javascript:pointSettingExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pointSettingResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pointSetting_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="pointSetting_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="inactiveSetting" class="tab-pane">
			<c:if test="${status =='INACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<!-- <button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPointSetting()">Edit Reward Config</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePointSetting()">Delete Reward Config</button> -->
				</div>
				<br />
				<br />
				<div style="position: relative" id="pointSettingGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>RTF Points Conversion settings</label>
							<div class="pull-right">
								<a href="javascript:pointSettingExportToExcel('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pointSettingResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pointSetting_grid" style="width: 100%; height: 300px; border: 1px solid gray"></div>
						<div id="pointSetting_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit Reward Config Bank -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="qBModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Point conversion setting</h4>
			</div>
			<div class="modal-body full-width">
				<form name="pointSettingForm" id="pointSettingForm" method="post">
					<input type="hidden" id="settingId" name="settingId" />
					<div id="startQBDiv" class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label>Point Conversion Type<span class="required">*</span></label> 
							<select id="powerUpType" name="powerUpType" class="form-control input-sm m-bot15" >
									<option value="Lives">Lives</option>
									<option value="MagicWands">Eraser</option>
									<option value="RewardDollars">Reward Dollars</option>
								</select>
						</div>		
						<div class="form-group col-sm-4 col-xs-4">
							<label>Quantity<span class="required">*</span>
							</label> <input class="form-control" type="text" id="qty" name="qty">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Points<span class="required">*</span>
							</label> <input class="form-control" type="text" id="points" name="points">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qBSaveBtn" type="button" onclick="saveSetting('save')">Save</button>
				<button class="btn btn-primary" id="qBUpdateBtn" type="button" onclick="saveSetting('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit Reward Setting Bank end here  -->
<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'pointSetting_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getPointSettingGridData(pageNo);
		}
	}
	
	//Reward Setting Bank Grid
	
	function getPointSettingGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/PointConversionSettings.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+pointSettingSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshPointSettingGridValues(jsonData.settings);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function pointSettingExportToExcel(status){
		var appendData = "headerFilter="+pointSettingSearchString+"&status="+status;
	    var url =apiServerUrl+"PointConversionSettingExport?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function pointSettingResetFilters(){
		pointSettingSearchString='';
		pointSettingColumnFilters = {};
		getPointSettingGridData(0);
	}
	
	var pointSettingCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var pointSettingDataView;
	var pointSettingGrid;
	var pointSettingData = [];
	var pointSettingGridPager;
	var pointSettingSearchString='';
	var pointSettingColumnFilters = {};
	var userPointSettingColumnsStr = '<%=session.getAttribute("pointSettingGrid")%>';

	var userPointSettingColumns = [];
	var allPointSettingColumns = [
			 {
				id : "powerUpType",
				field : "powerUpType",
				name : "Conversion Type",
				width : 80,
				sortable : true
			},{
				id : "qty",
				field : "qty",
				name : "Quantity",
				width : 80,
				sortable : true
			},{
				id : "points",
				field : "points",
				name : "RTF Points",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			} ];

	if (userPointSettingColumnsStr != 'null' && userPointSettingColumnsStr != '') {
		columnOrder = userPointSettingColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPointSettingColumns.length; j++) {
				if (columnWidth[0] == allPointSettingColumns[j].id) {
					userPointSettingColumns[i] = allPointSettingColumns[j];
					userPointSettingColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPointSettingColumns = allPointSettingColumns;
	}
	
	var pointSettingOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var pointSettingGridSortcol = "pointSettingId";
	var pointSettingGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function pointSettingGridComparer(a, b) {
		var x = a[pointSettingGridSortcol], y = b[pointSettingGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPointSettingGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		pointSettingData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (pointSettingData[i] = {});
				d["id"] = i;
				d["pointSettingId"] = data.id;
				d["powerUpType"] = data.powerUpType;
				if(data.powerUpType == 'MagicWands'){
					d["powerUpType"] = 'Eraser';
				}
				
				d["qty"] = data.qty;
				d["points"] = data.rtfPoints;
				d["status"] = "INACTIVE";
				if(data.status == true || data.status == 'true'){
					d["status"] = "ACTIVE";
				}
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		pointSettingDataView = new Slick.Data.DataView();
		pointSettingGrid = new Slick.Grid("#pointSetting_grid", pointSettingDataView,
				userPointSettingColumns, pointSettingOptions);
		pointSettingGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		pointSettingGrid.setSelectionModel(new Slick.RowSelectionModel());
		pointSettingGrid.registerPlugin(pointSettingCheckboxSelector);
		
		pointSettingGridPager = new Slick.Controls.Pager(pointSettingDataView,
					pointSettingGrid, $("#pointSetting_pager"),
					pagingInfo);
		/* var pointSettingGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPointSettingColumns, pointSettingGrid, pointSettingOptions); */
		
		pointSettingGrid.onSort.subscribe(function(e, args) {
			pointSettingGridSortdir = args.sortAsc ? 1 : -1;
			pointSettingGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				pointSettingDataView.fastSort(pointSettingGridSortcol, args.sortAsc);
			} else {
				pointSettingDataView.sort(pointSettingGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the pointSettingGrid
		pointSettingDataView.onRowCountChanged.subscribe(function(e, args) {
			pointSettingGrid.updateRowCount();
			pointSettingGrid.render();
		});
		pointSettingDataView.onRowsChanged.subscribe(function(e, args) {
			pointSettingGrid.invalidateRows(args.rows);
			pointSettingGrid.render();
		});
		$(pointSettingGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							pointSettingSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								pointSettingColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in pointSettingColumnFilters) {
										if (columnId !== undefined
												&& pointSettingColumnFilters[columnId] !== "") {
											pointSettingSearchString += columnId
													+ ":"
													+ pointSettingColumnFilters[columnId]
													+ ",";
										}
									}
									getPointSettingGridData(0);
								}
							}

						});
		pointSettingGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(pointSettingColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(pointSettingColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		pointSettingGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		pointSettingDataView.beginUpdate();
		pointSettingDataView.setItems(pointSettingData);
		//pointSettingDataView.setFilter(filter);
		pointSettingDataView.endUpdate();
		pointSettingDataView.syncGridSelection(pointSettingGrid, true);
		pointSettingGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserPointSettingPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = pointSettingGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('pointSettingGrid', colStr);
	}
	
	// Add Reward Setting Bank	
	function resetPointSettingModal(){		
		$('#qBModal').modal('show');
		$('#setttingId').val('');
		$('#qty').val('');
		$('#powerUpType').val('Lives');
		$('#powerUpType option').attr('disabled',false);
		$('#points').val('');
		$('#qBSaveBtn').show();
		$('#qBUpdateBtn').hide();	
	}

	function saveSetting(action){
		
		var qty = $('#qty').val();
		var points = $('#points').val();
		
		if(qty == ''){
			jAlert("Quantity is Mandatory.");
			return;
		}
		if(points == ''){
			jAlert("Rtf Points are Mandatory.");
			return;
		}
		var requestUrl = "${pageContext.request.contextPath}/UpdateRtfPointsConvSetting.json";
		var dataString = "";
		if(action == 'save'){		
			dataString  = $('#pointSettingForm').serialize()+"&action=SAVE&pageNo=0";
		}else if(action == 'update'){
			dataString = $('#pointSettingForm').serialize()+"&action=UPDATE&pageNo=0";
		}
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			data: dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#qBModal').modal('hide');
					pagingInfo = jsonData.pagingInfo;
					pointSettingColumnFilters = {};
					refreshPointSettingGridValues(jsonData.settings);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit Reward Setting Bank
	function editPointSetting(){
		var tempPointSettingRowIndex = pointSettingGrid.getSelectedRows([0])[0];
		if (tempPointSettingRowIndex == null) {
			jAlert("Plese select points conversation setting record to Edit", "info");
			return false;
		}else {
			var pointSettingId = pointSettingGrid.getDataItem(tempPointSettingRowIndex).pointSettingId;
			getEditPointSetting(pointSettingId);
		}
	}
	
	function getEditPointSetting(pointSettingId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateRtfPointsConvSetting.json",
			type : "post",
			dataType: "json",
			data: "settingId="+pointSettingId+"&action=EDIT&pageNo=0",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#qBModal').modal('show');
					setEditPointSetting(jsonData.setting);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditPointSetting(data){
		$('#qBSaveBtn').hide();
		$('#qBUpdateBtn').show();
		$('#settingId').val(data.id);
		$('#powerUpType option').attr('disabled',false);
		$('#powerUpType').val(data.powerUpType);
		$('#powerUpType option:not(:selected)').attr('disabled',true);
		$('#qty').val(data.qty);
		$('#points').val(data.rtfPoints);
	}
	
	//Delete Reward Setting Bank
	function deletePointSetting(){
		var tempPointSettingRowIndex = pointSettingGrid.getSelectedRows([0])[0];
		if (tempPointSettingRowIndex == null) {
			jAlert("Plese select point conversation setting to delete", "info");
			return false;
		}else {
			var settingId = pointSettingGrid.getDataItem(tempPointSettingRowIndex).pointSettingId;
			getDeletePointSetting(settingId);
		}
	}
	
	
	function getDeletePointSetting(settingId){
		jConfirm("Are you sure to delete selected point conversation setting ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateRtfPointsConvSetting.json",
						type : "post",
						dataType: "json",
						data : "settingId="+settingId+"&action=DELETE&pageNo=0",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.pagingInfo;
								pointSettingColumnFilters = {};
								refreshPointSettingGridValues(jsonData.settings);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
								return;
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${pagingInfo};
		refreshPointSettingGridValues(${settings});
		$('#pointSetting_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPointSettingPreference()'>");
		
		enableMenu();
	};
		
</script>