<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<!-- Bootstrap CSS -->    
    <link href="../resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="../resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="../resources/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../resources/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="../resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="../resources/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="../resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="../resources/css/owl.carousel.css" type="text/css">
	<link href="../resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="../resources/css/fullcalendar.css">
	<link href="../resources/css/widgets.css" rel="stylesheet">
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/style-responsive.css" rel="stylesheet" />
	<link href="../resources/css/xcharts.min.css" rel=" stylesheet">	
	<link href="../resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->
    
    <!-- javascripts -->
    <%-- <script src="../resources/js/jquery.js"></script>
	<script src="../resources/js/jquery-ui-1.10.4.min.js"></script> --%>
    <script src="../resources/js/jquery-1.8.3.min.js"></script>
    <%-- <script type="text/javascript" src="../resources/js/jquery-ui-1.9.2.custom.min.js"></script> --%>
    <!-- bootstrap -->
    <script src="../resources/js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../resources/js/jquery.scrollTo.min.js"></script>
    <script src="../resources/js/jquery.nicescroll.js" type="text/javascript"></script>


<script>
	function doCustomerValidations(){
		$.ajax({
					url : "${pageContext.request.contextPath}/CheckCustomer",
					type : "get",
					data : "userName="+ $("#email").val(),
					/* async : false, */
					success : function(response){
						if(response == "true"){
							$("#updateCustomer").submit();
						}else{
							jAlert(response);
							return false;
						}
					},
					error : function(error){
						jAlert("There is something wrong. Please try again"+error,"Error");
						return false;
					}
			});
	}

	function deleteUser(userId) {		
		if (userId == '') {
			jAlert("Please try again.");
			return false;
		}
		jConfirm("Are you sure to delete an shipping address ?","Confirm",function(r){
			if (r) {
				$.ajax({
							url : "${pageContext.request.contextPath}/DeleteShippingAddress",
							type : "post",
							data : "userName=" + $("#email").val() + "&userId="
									+ userId,
							success : function(response) {
								if (response == "true") {
									jAlert("Shipping address deleted successfully.");
									window.location.reload();
									return true;
								} else {
									jAlert(response);
									return false;
								}
							},
							error : function(error) {
								jAlert("There is something wrong. Please try again"
										+ error,"Error");
								return false;
							}
						});
			} else {
				return false;
			}
		});
		
		return false;
	}


	//toggle the add shipping address modal
	//$(document).ready(function () {
		//$("#showAddressForm").hide();
		//$("#addCustomerAction").hide();
		//$("#addShippingAddress").click(function () {
			//$("#editCustomerAction").hide();
			//$("#showAddressForm").show();
			//$("#addCustomerAction").show();
			 
		//});

		
	//});

	//toggle the add shipping address modal to add
	function addModal(){
		var shippingAddressCount = $('#count').val();
		if(shippingAddressCount == 5){
			jAlert("You have reached the limit of adding shipping address");
		}else{
			$('#myModal-2').modal('show');	
		}
	}
	
	//toggle the edit shipping address modal to edit
	function editModal(addrId){
		$('#myModal-2').modal('show');	
		$('#shFirstName').val($('#firstName_'+addrId).text());
		$('#shLastName').val($('#lastName_'+addrId).text());
		$('#shAddressLine1').val($('#street1_'+addrId).text());
		$('#shAddressLine2').val($('#street2_'+addrId).text());
		$('#shCountryName').val($('#country_'+addrId).text());
		$('#shStateName').val($('#state_'+addrId).text());
		$('#shCity').val($('#city_'+addrId).text());
		$('#shZipCode').val($('#zipCode_'+addrId).text());
		$('#shippingAddrId').val(addrId);
	}

	
	//Customer cancel action
	function cancelAction(){
		//window.location.href = "${pageContext.request.contextPath}/Client/EditCustomer";
		window.close();
	}
</script>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Customer
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="${pageContext.request.contextPath}/Client/ManageDetails">Customer</a>
			</li>
			<li><i class="fa fa-laptop"></i>Edit Customer</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel"> <header class="panel-heading">
		Fill Customer Info </header>
		<div class="panel-body">
							  <c:if test="${successMessage != null}">
                              	<div class="alert alert-success fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${successMessage}</strong>
                              	</div>
                              </c:if>
                              <c:if test="${errorMessage != null}">
                              	<div class="alert alert-block alert-danger fade in">
                              		<strong style="font-family:arial, helvetica;font-size:17px;display:block;text-align:center;">${errorMessage}</strong>
                              	</div>
                              </c:if>
			<form:form role="form" class="form-horizontal" commandName="customer" modelAttribute="customer" id="updateCustomer" method="post" action="${pageContext.request.contextPath}/Client/UpdateCustomer">
				<input id="action" value="action" name="action" type="hidden" />
				<input class="form-control" id="custId" value="${customerId}" name="custId" type="hidden" />
				<input type="hidden" name="billingAddressId" value="${billingAddress.id}"/>
				<div class="form-group">
					<div class="col-sm-3">
						<label>Type <span class="required">*</span> </label>
						<form:select path="customerType" name="customerType"
							class="form-control input-sm m-bot15">
							<form:option value="PHONE_CUSTOMER">Phone Customer</form:option>
							<form:option value="WEB_CUSTOMER">Web Customer</form:option>
						</form:select>

					</div>
					<div class="col-sm-3">
						<label>Customer status <span class="required">*</span> </label>
						<form:select path="customerLevel" name="customerLevel"
							class="form-control input-sm m-bot15">
							<form:option value="GOLD">Gold</form:option>
							<form:option value="SILVER">Silver</form:option>
							<form:option value="PLATINUM">Platinum</form:option>
						</form:select>

					</div>

					<div class="col-sm-5">
						<label> Signup Type <span class="required">*</span> </label>
						<form:select path="signupType" name="signupType" class="form-control input-sm m-bot15">
							<form:option value="REWARDTHEFAN">REWARD THE FAN</form:option>
							<form:option value="FACEBOOK">Facebook</form:option>
							<form:option value="GOOGLE">Google</form:option>
							<!--<form:option value="phone">Phone</form:option>-->
						</form:select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
					<div class="col-sm-5">
						<label> Name<span class="required">*</span> </label>
						<form:input class="form-control" path="customerName" id="name" name="customerName" />
					</div>
					<div class="col-sm-5">
						<label>E-Mail <span class="required">*</span>
						</label><form:input class="form-control" path="email" id="email" type="email" name="email" />
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
					<div class="col-sm-5">
						<label>Phone <span class="required">*</span> </label> <form:input
							class="form-control" path="phone" id="phone" name="phone" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Ext</label> <form:input class="form-control" path="extension" id="fedexNo"
							name="extension" />
					</div>
					<div class="col-sm-5">
						<label>Other Phone </label> <form:input class="form-control" path="otherPhone" id="phone"
							name="phone" type="text" />
					</div>
					<div class="col-sm-5">
						<label>Representative </label> <form:input class="form-control" path="representativeName" id="representativeName"
							name="representativeName" type="text" />
					</div>
				</div>

				<header class="panel-heading"> Billing Address </header>
				<div class="form-group">
					<div class="col-sm-5">
						<label>First Name <span class="required">*</span></label> <input class="form-control"
							id="firstName" name="firstName" type="text" value="${billingAddress.firstName}"/>
					</div>
					<div class="col-sm-5">
						<label>Last Name <span class="required">*</span></label>
					    <input class="form-control" id="lastName" name="lastName" type="text" value="${billingAddress.lastName}" />
					</div>

					<div class="col-sm-5">
						<label>Street1 <span class="required">*</span></label>
					    <input class="form-control" id="street1" type="text" name="addressLine1" value="${billingAddress.addressLine1}" />
					</div>
					<div class="col-sm-5">
						<label>Street2</label>
						<input class="form-control" id="street2" type="text" name="addressLine2" value="${billingAddress.addressLine2}"/>
					</div> 
					<div class="col-sm-5">
						<label>Country <span class="required">*</span></label>		
						<select id="countryName" name="countryName" class="form-control input-sm m-bot15" onchange="">
							<c:forEach var="country" items="${countries}">
         							<option value="${country.id}" 
         							<c:if test="${billingAddress.country.id == country.id}"> selected </c:if>>
         								<c:out value="${country.name}"/>
         							</option>
         						
     						 </c:forEach>
						</select>
					</div>
					<div class="col-sm-5">
						<label>State <span class="required">*</span></label>
						<select id="state" name="stateName" class="form-control input-sm m-bot15" onchange="">
							 <c:forEach var="state" items="${states}">
         							<option value="${state.id}" 
         								<c:if test="${billingAddress.state.id == state.id}"> selected </c:if>>
										<c:out value="${state.name}"/>		
									</option>
     						 </c:forEach>
						</select>
					</div>	
					<div class="col-sm-5">
						<label>City <span class="required">*</span></label>
						<input class="form-control" id="city" type="text" name="city" value="${billingAddress.city}"/>
					</div>
					<div class="col-sm-5">
						<label>Zip Code <span class="required">*</span></label>		
						<input class="form-control" id="zipCode" type="text" name="zipCode" value="${billingAddress.zipCode}"/>
					</div>
				</div>
				<header class="panel-heading"> 
					Shipping Address
					<a data-toggle="modal" class="btn btn-primary" id="addShippingAddress" onclick="addModal();">Add</i></a>

				</header>
				<div class="form-group">
					<div class="col-lg-12">
						<table class="table table-striped table-advance table-hover">
							<tbody>
								<tr>
									<!--<th></th>-->
									<th><i class=""></i> First Name</th>
									<th><i class=""></i> Last Name</th>
									<th><i class=""></i> Street1</th>
									<th><i class=""></i> Street2</th>
									<th><i class=""></i> Country</th>
									<th><i class=""></i> State</th>
									<th><i class=""></i> City</th>
									<th><i class=""></i> Zip Code</th>
									<th><i class="icon_cogs"></i> Action</th>
								</tr>
								
							<c:forEach var="shippingAddress" items="${shippingAddressList}">
								<tr>
									<input type="hidden" name="shippingAddressId" value="${shippingAddress.id}"/>
									<!-- <td><label class="label_radio" for="radio-01">
                                                  <input name="sample-radio" id="radio-01" value="1" type="radio"/>
                                          </label></td>-->
									<td id="firstName_${shippingAddress.id}">${shippingAddress.firstName}</td>
									<td id="lastName_${shippingAddress.id}">${shippingAddress.lastName}</td>
									<td id="street1_${shippingAddress.id}">${shippingAddress.addressLine1}</td>
									<td id="street2_${shippingAddress.id}">${shippingAddress.addressLine2}</td>
									<td id="country_${shippingAddress.id}">${shippingAddress.getCountry().name}</td>
									<td id="state_${shippingAddress.id}">${shippingAddress.getState().name}</td>
									<td id="city_${shippingAddress.id}">${shippingAddress.city}</td>
									<td id="zipCode_${shippingAddress.id}">${shippingAddress.zipCode}</td>
									<td>
										<div class="btn-group">
											<!-- <a class="btn btn-primary" id="add"><i
												class="icon_plus_alt2" ></i> </a> -->
											<a class="btn btn-success" id="edit" onclick="editModal('${shippingAddress.id}')"><i class="icon_check_alt2"></i> </a>  
											<a class="btn btn-danger" id="delete" onclick="deleteUser('${shippingAddress.id}');"><i
												class="icon_close_alt2"></i> </a>
										</div>
									</td>
								</tr >
									
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>

				<div id="editCustomerAction" class="form-group">
					<div class="col-sm-6">
						<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
						<button class="btn btn-primary" type="submit" onclick=";">Update</button>
						<button class="btn btn-default" onclick="cancelAction();" type="button">Cancel</button>
					</div>
				</div>
			</form:form>

				<!-- Toggle add shipping address form-->
			<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
				tabindex="-1" id="myModal-2" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button aria-hidden="true" data-dismiss="modal" class="close"
								type="button">Close</button>
							<h4 class="modal-title">Add Shipping Address</h4>
						</div>
						<div class="panel-body">
							<div class="modal-body">
							 <form role="form" class="form-horizontal" id="addShippingAddress" method="post" action="${pageContext.request.contextPath}/Client/AddShippingAddress">
							 	<input id="action" value="addShippingAddress" name="action" type="hidden" />
							 	<input type="hidden" name="shippingAddrId"  id="shippingAddrId" value=""/>
							 	<input class="form-control" id="custId" value="${customerId}" name="custId" type="hidden" />
								<input type="hidden" name="count" id="count" value="${count}" />
								<div class="form-group">
									<div class="col-sm-5">
										<label>First Name <span class="required">*</span>
										</label> <input class="form-control" id="shFirstName" name="shFirstName"
											type="text" value="" />
									</div>
									<div class="col-sm-5">
										<label>Last Name <span class="required">*</span>
										</label> <input class="form-control" id="shLastName" name="shLastName"
											type="text" value="" />
									</div>

									<div class="col-sm-5">
										<label>Street1 <span class="required">*</span>
										</label> <input class="form-control" id="shAddressLine1" type="text"
											name="shAddressLine1" value="" />
									</div>
									<div class="col-sm-5">
										<label>Street2</label> <input class="form-control"
											id="shAddressLine2" type="text" name="shAddressLine2"
											value="" />
									</div>
									<div class="col-sm-5">
										<label>Country <span class="required">*</span>
										</label> <select id="shCountryName" name="shCountryName"
											class="form-control input-sm m-bot15" onchange="">
											<c:forEach var="country" items="${countries}">
												<option value="${country.id}">
													<c:out value="${country.name}" />
												</option>

											</c:forEach>
										</select>
									</div>
									<div class="col-sm-5">
										<label>State <span class="required">*</span>
										</label> <select id="shStateName" name="shStateName"
											class="form-control input-sm m-bot15" onchange="">
											<c:forEach var="state" items="${states}">
												<option value="${state.id}">
													<c:out value="${state.name}" />
												</option>
											</c:forEach>
										</select>
									</div>
									<div class="col-sm-5">
										<label>City <span class="required">*</span>
										</label> <input class="form-control" id="shCity" type="text" name="shCity"
											value="" />
									</div>
									<div class="col-sm-5">
										<label>Zip Code <span class="required">*</span>
										</label> <input class="form-control" id="shZipCode" type="text"
											name="shZipCode" value="" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<!--<button type="submit" class="btn btn-info pull-right">Submit</button>-->
										<button class="btn btn-primary" type="submit">Save</button>
										<button class="btn btn-default" onclick="cancelAction();"
											type="button">Cancel</button>
									</div>
								</div>
								</form>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
		</section>
	</div>
</div>