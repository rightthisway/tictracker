<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<!-- <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script> -->
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.giftCardBrandLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(giftCardBrandGrid != null && giftCardBrandGrid != undefined){
			giftCardBrandGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${gbStatus == 'ACTIVE'}">	
			$('#activeGiftCardBrand').addClass('active');
			$('#activeGiftCardBrandTab').addClass('active');
		</c:when>
		<c:when test="${gbStatus == 'DELETED'}">
			$('#deletedGiftCardBrand').addClass('active');
			$('#deletedGiftCardBrandTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeGiftCardBrand1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#deletedGiftCardBrand1").click(function(){
		callTabOnChange('DELETED');
	});
});

 function callChangeImage(){
	$('#imageFileDiv').show();
	$('#cardImageDiv').hide();
	$('#fileRequired').val('Y');
} 
 function callChangeImage1(){
		$('#imageFileDiv1').show();
		$('#cardImageDiv1').hide();
		$('#fileRequired1').val('Y');
	} 

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/GiftCardBrands?gbStatus="+selectedTab;
}

</script>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Gift Cards</a></li>
			<li><i class="fa fa-laptop"></i>Manage Gift Card Brand</li>
		</ol>
	</div>
</div>
<div id="giftCardDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeGiftCardBrandTab" class=""><a id="activeGiftCardBrand1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeGiftCardBrand">Active GiftCard Brands</a></li>
				<li id="deletedGiftCardBrandTab" class=""><a id="deletedGiftCardBrand1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#deletedGiftCardBrand">Deleted GiftCard Brands</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeGiftCardBrand" class="tab-pane">
			<c:if test="${gbStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addGBBtn" type="button" data-toggle="modal" onclick="resetGiftCardBrandModal();">Add GiftCard Brand</button>
					<button class="btn btn-primary" id="editGBBtn" type="button" onclick="editGiftCardBrand()">Edit GiftCard Brand</button>
					<button class="btn btn-primary" id="deleteGBBtn" type="button" onclick="deleteGiftCardBrand()">Delete GiftCard Brand</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="giftCardBrandGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Card Brands</label>
							<div class="pull-right">
								<a href="javascript:giftCardBrandExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardBrandResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCardBrand_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCardBrand_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="deletedGiftCardBrand" class="tab-pane">
			<c:if test="${gbStatus =='DELETED'}">	
				<div style="position: relative" id="giftCardBrandGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Gift Card Brands</label>
							<div class="pull-right">
								<a href="javascript:giftCardBrandExportToExcel('DELETED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:giftCardBrandResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="giftCardBrand_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="giftCardBrand_pager" style="width: 100%; height: 10px;"></div>
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit GiftCardBrand  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="giftCardBrandModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Gift Card Brand</h4>
			</div>
			<div class="modal-body full-width">
				<form name="giftCardBrandForm" id="giftCardBrandForm" method="post">
					<input type="hidden" id="brandId" name="brandId" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="gbStatus" name="gbStatus" value="${gbStatus}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Brand Name<span class="required">*</span>
							</label> <input class="form-control" type="text" id="name" name="name">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="description" name="description">
						</div>
					<!-- 	<div class="form-group col-sm-6 col-xs-6">
							<label>Quantity Limit<span class="required">*</span>
							</label> <input class="form-control" type="text" id="quantity" name="quantity">
						</div> -->
						<div class="form-group col-sm-6 col-xs-6">
							<label>Display Out of Stock Image?<span class="required">*</span></label> 
							<select id="displayOutOfStock" name="displayOutOfStock" class="form-control">
								<option value="YES">YES</option>
								<option value="NO">NO</option>
							</select>
						</div>
						<div id="imageFileDiv" class="form-group col-sm-5 col-xs-5">
						 	<input type="hidden" name="fileRequired" id="fileRequired" />
							<label for="imageFile" class="col-lg-3 control-label">Image File</label>
                 			<input type="file" id="imageFile" name="imageFile">
             			</div>
             			<div id="imageFileDiv1" class="form-group col-sm-5 col-xs-5">
						 	<input type="hidden" name="fileRequired1" id="fileRequired1" />
							<label for="imageFile1" class="col-lg-3 control-label">Out of stock Image File</label>
                 			<input type="file" id="imageFile1" name="imageFile1">
             			</div>
						</div>
						<div class="form-group form-group-top" id="cardImageDiv" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="cardImageTag" src="" height="100" width="300" />
               				 <a style="color:blue " href="javascript:callChangeImage();">Change Image</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
    				<div class="form-group form-group-top" id="cardImageDiv1" style="width: 100%">
            			<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
               				 <img id="cardImageTag1" src="" height="100" width="300" />
               				 <a style="color:blue " href="javascript:callChangeImage1();">Change Image</a>
           					 </div>
						<div class="col-lg-3">&nbsp;</div>
    				</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="saveGiftCardBrand('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="saveGiftCardBrand('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit GiftCardBrand  end here  -->

<script type="text/javascript">
	function pagingControl(move, id) {
		if(id == 'giftCardBrand_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getGiftCardBrandGridData(pageNo);
		}
	}
	
	//GiftCardBrand  Grid
	
	function getGiftCardBrandGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/GiftCardBrands.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+giftCardBrandSearchString+"&gbStatus=${gbStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.brandPagingInfo;
				refreshGiftCardBrandGridValues(jsonData.brands);	
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function giftCardBrandExportToExcel(status){
		var appendData = "headerFilter="+giftCardBrandSearchString+"&gbStatus="+gcStatus;
	    var url =apiServerUrl+"GiftCardBrandsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function giftCardBrandResetFilters(){
		giftCardBrandSearchString='';
		sortingString ='';
		giftCardBrandColumnFilters = {};
		getGiftCardBrandGridData(0);
	}
	
	/* var giftCardBrandCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		}); */
	
	var pagingInfo;
	var giftCardBrandDataView;
	var giftCardBrandGrid;
	var giftCardBrandData = [];
	var giftCardBrandGridPager;
	var giftCardBrandSearchString='';
	var sortingString='';
	var giftCardBrandColumnFilters = {};
	var userGiftCardBrandColumnsStr = '<%=session.getAttribute("giftcardbrandgrid")%>';

	var userGiftCardBrandColumns = [];
	var allGiftCardBrandColumns = [/* giftCardBrandCheckboxSelector.getColumnDefinition(), */
			 {
				id : "giftCardBrandId",
				field : "giftCardBrandId",
				name : "Brand Id",
				width : 80,
				sortable : true
			},{
				id : "name",
				field : "name",
				name : "Name",
				width : 80,
				sortable : true
			},{
				id : "description",
				field : "description",
				name : "Description",
				width : 80,
				sortable : true
			}, {
				id : "imageUrl",
				field : "imageUrl",
				name : "Preview Image",
				width : 80,
				formatter: imagePreviewFormatter1
			}, {
				id : "outOfStockImageUrl",
				field : "outOfStockImageUrl",
				name : "Preiview out stock Image",
				width : 80,
				formatter: imagePreviewFormatter
			}, {
				id : "displayOutOfStock",
				field : "displayOutOfStock",
				name : "display Out Of Stock",
				width : 80,
				sortable : true
			},/* {
				id : "quantity",
				field : "quantity",
				name : "Quantity Limit",
				width : 80,
				sortable : true
			}, */{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			}  ];

	if (userGiftCardBrandColumnsStr != 'null' && userGiftCardBrandColumnsStr != '') {
		columnOrder = userGiftCardBrandColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allGiftCardBrandColumns.length; j++) {
				if (columnWidth[0] == allGiftCardBrandColumns[j].id) {
					userGiftCardBrandColumns[i] = allGiftCardBrandColumns[j];
					userGiftCardBrandColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userGiftCardBrandColumns = allGiftCardBrandColumns;
	}
	
	function imagePreviewFormatter(row,cell,value,columnDef,dataContext){
		var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.outOfStockImageUrl +"'/>";
		return button;
	}
	
	function imagePreviewFormatter1(row,cell,value,columnDef,dataContext){
		var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.imageUrl +"'/>";
		return button;
	}
	
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var giftCardBrandOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var giftCardBrandGridSortcol = "giftCardBrandId";
	var giftCardBrandGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function giftCardBrandGridComparer(a, b) {
		var x = a[giftCardBrandGridSortcol], y = b[giftCardBrandGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshGiftCardBrandGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		giftCardBrandData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (giftCardBrandData[i] = {});
				d["id"] = i;
				d["giftCardBrandId"] = data.id;
				d["name"] = data.name;
				d["description"] = data.description;
				//d["quantity"] = data.quantity;
				d["status"] = data.status;
				d["createdDate"] = data.createdDateStr;
				d["updatedDate"] = data.updatedDateStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
				d["imageUrl"] = data.imageUrl;
				d["outOfStockImageUrl"] = data.outOfStockImageUrl;
				d["displayOutOfStock"] = "NO";
				if(data.displayOutOfStock == true || data.displayOutOfStock == 'true'){
					d["displayOutOfStock"] = "YES";
				}
					
			}
		}

		giftCardBrandDataView = new Slick.Data.DataView();
		giftCardBrandGrid = new Slick.Grid("#giftCardBrand_grid", giftCardBrandDataView,
				userGiftCardBrandColumns, giftCardBrandOptions);
		giftCardBrandGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		giftCardBrandGrid.setSelectionModel(new Slick.RowSelectionModel());
		//giftCardBrandGrid.registerPlugin(giftCardBrandCheckboxSelector);
		
		giftCardBrandGridPager = new Slick.Controls.Pager(giftCardBrandDataView,
					giftCardBrandGrid, $("#giftCardBrand_pager"),
					pagingInfo);
		var giftCardBrandGridColumnpicker = new Slick.Controls.ColumnPicker(
				allGiftCardBrandColumns, giftCardBrandGrid, giftCardBrandOptions);
		
		
		/* giftCardBrandGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = giftCardBrandGrid.getCellFromEvent(e);
		      giftCardBrandGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy giftCardBrand'){
				var tempGiftCardBrandRowIndex = giftCardBrandGrid.getSelectedRows([0])[0];
				if (tempGiftCardBrandRowIndex == null) {
					jAlert("Plese select GiftCardBrand to Copy", "info");
					return false;
				}else {
					var giftCardBrandText = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).giftCardBrand;
					var a = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).optionA;
					var b = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).optionB;
					var c = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).optionC;
					var answer = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).answer;
					var copyText = giftCardBrandText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}else if($(e.target).attr("data") == 'edit giftCardBrand'){
				editGiftCardBrand();
			}
		}); */
		
		
		giftCardBrandGrid.onSort.subscribe(function(e, args) {
			giftCardBrandGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(giftCardBrandGridSortcol) < 0){
				giftCardBrandGridSortdir = 'ASC';
			}else{
				if(giftCardBrandGridSortdir == 'DESC' ){
					giftCardBrandGridSortdir = 'ASC';
				}else{
					giftCardBrandGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+giftCardBrandGridSortcol+',SORTINGORDER:'+giftCardBrandGridSortdir+',';
			getGiftCardBrandGridData(0);
		});
		
		// wire up model discountCodes to drive the giftCardBrandGrid
		giftCardBrandDataView.onRowCountChanged.subscribe(function(e, args) {
			giftCardBrandGrid.updateRowCount();
			giftCardBrandGrid.render();
		});
		giftCardBrandDataView.onRowsChanged.subscribe(function(e, args) {
			giftCardBrandGrid.invalidateRows(args.rows);
			giftCardBrandGrid.render();
		});
		$(giftCardBrandGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							giftCardBrandSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								giftCardBrandColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in giftCardBrandColumnFilters) {
										if (columnId !== undefined
												&& giftCardBrandColumnFilters[columnId] !== "") {
											giftCardBrandSearchString += columnId
													+ ":"
													+ giftCardBrandColumnFilters[columnId]
													+ ",";
										}
									}
									getGiftCardBrandGridData(0);
								}
							}

						});
		giftCardBrandGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'imageUrl' && args.column.id != 'outOfStockImageUrl'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(giftCardBrandColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(giftCardBrandColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		giftCardBrandGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		giftCardBrandDataView.beginUpdate();
		giftCardBrandDataView.setItems(giftCardBrandData);
		//giftCardBrandDataView.setFilter(filter);
		giftCardBrandDataView.endUpdate();
		giftCardBrandDataView.syncGridSelection(giftCardBrandGrid, true);
		giftCardBrandGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserGiftCardBrandPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = giftCardBrandGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('giftcardbrandgrid', colStr);
	}
	
	// Add GiftCardBrand 	
	function resetGiftCardBrandModal(){		
		$('#giftCardBrandModal').modal('show');
		$('#brandId').val('');
		$('#name').val('');
		$('#description').val('');
		//$('#quantity').val('');
	    $('#imageFileDiv').show();
		$('#cardImageDiv').hide();
		$('#fileRequired').val('Y');
		$('#imageFileDiv1').show();
		$('#cardImageDiv1').hide();
		$('#fileRequired1').val('Y');
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		$('#cardImageDiv').hide();	
		$('#cardImageDiv1').hide();	
		
	}

	function saveGiftCardBrand(action){
		
		var name = $('#name').val();
		var description = $('#description').val();
		//var quantity = $('#quantity').val();
		var imageFile = $('#imageFile').val();
		var fileRequired = $('#fileRequired').val();
		var imageFile1 = $('#imageFile1').val();
		var fileRequired1 = $('#fileRequired1').val();
		if(name == ''){
			jAlert("Gift Card Brand Name is mendatory.");
			return;
		}
		if(description == ''){
			jAlert("Gift Card Brand description is mendatory.");
			return;
		}
		/* if(quantity == ''){
			jAlert("Gift Card Brand quantity limit is mendatory.");
			return;
		} */
		if(fileRequired == 'Y' && imageFile == ''){
			jAlert("Gift Card Brand Image is mendatory.");
			return;
		} 
		if(fileRequired1 == 'Y' && imageFile1 == ''){
			jAlert("Gift Card Brand out of stock Image is mendatory.");
			return;
		}
		$('#action').val(action);
		var requestUrl = "${pageContext.request.contextPath}/UpdateGiftCardBrand";
		var form = $('#giftCardBrandForm')[0];
		var dataString = new FormData(form);
		
		
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#giftCardBrandModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.brandPagingInfo);
					giftCardBrandColumnFilters = {};
					refreshGiftCardBrandGridValues(JSON.parse(jsonData.brands));
					//clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//Edit GiftCardBrand 
	function editGiftCardBrand(){
		var tempGiftCardBrandRowIndex = giftCardBrandGrid.getSelectedRows([0])[0];
		if (tempGiftCardBrandRowIndex == null) {
			jAlert("Plese select Gift Card Brand to Edit", "info");
			return false;
		}else {
			var giftCardBrandId = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).giftCardBrandId;
			getEditGiftCardBrand(giftCardBrandId);
		}
	}
	
	function getEditGiftCardBrand(giftCardBrandId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateGiftCardBrand",
			type : "post",
			dataType: "json",
			data: "brandId="+giftCardBrandId+"&action=EDIT&pageNo=0&gbStatus=${gbStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#giftCardBrandModal').modal('show');
					$('#cardImageDiv').show();	
					$('#imageFileDiv').hide();
					$('#fileRequired').val('N'); 
					$('#cardImageDiv1').show();	
					$('#imageFileDiv1').hide();
					$('#fileRequired1').val('N'); 
					setEditGiftCardBrand(JSON.parse(jsonData.brand));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditGiftCardBrand(giftCardBrand){
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = giftCardBrand;
		$('#brandId').val(data.id);
		$('#name').val(data.name);
		$('#description').val(data.description);
		//$('#quantity').val(data.quantity);
		$('#displayOutOfStock').val(data.displayOutOfStock);
		$("#cardImageTag").attr("src",data.imageUrl);
		$("#cardImageTag1").attr("src",data.outOfStockImageUrl);
	}
	
	function getSelectedGiftCardBrandGridId() {
		var tempGiftCardBrandRowIndex = giftCardBrandGrid.getSelectedRows();
		
		var giftCardBrandIdStr='';
		$.each(tempGiftCardBrandRowIndex, function (index, value) {
			giftCardBrandIdStr += ','+giftCardBrandGrid.getDataItem(value).giftCardBrandId;
		});
		
		if(giftCardBrandIdStr != null && giftCardBrandIdStr!='') {
			giftCardBrandIdStr = giftCardBrandIdStr.substring(1, giftCardBrandIdStr.length);
			 return giftCardBrandIdStr;
		}
	}
	//Delete GiftCardBrand 
	function deleteGiftCardBrand(){
		var giftCardBrandIds = getSelectedGiftCardBrandGridId();
		if (giftCardBrandIds == null || giftCardBrandIds == '' || giftCardBrandIds == undefined) {
			jAlert("Plese select Gift Card Brand to Delete", "info");
			return false;
		}else {
			//var giftCardBrandId = giftCardBrandGrid.getDataItem(tempGiftCardBrandRowIndex).giftCardBrandId;
			getDeleteGiftCardBrand(giftCardBrandIds);
		}
	}
	
	
	function getDeleteGiftCardBrand(giftCardBrandIds){
		jConfirm("Are you sure to delete selected Gift Card Brands ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateGiftCardBrand",
						type : "post",
						dataType: "json",
						data : "brandId="+giftCardBrandIds+"&action=DELETE&gbStatus=${gbStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.brandPagingInfo);
								giftCardBrandColumnFilters = {};
								refreshGiftCardBrandGridValues(JSON.parse(jsonData.brands));
								//clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${brandPagingInfo};
		refreshGiftCardBrandGridValues(${brands});
		
		$('#giftCardBrand_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGiftCardBrandPreference()'>");
		
		enableMenu();
	};
		
</script>