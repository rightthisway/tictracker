<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#dateOfGame').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	
	$('#category').autocomplete("AutoCompleteArtistEventAndCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		formatItem: function(row, i, max) {
			if(row[0] == 'ALL'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='PARENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='EVENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[3] ;
			}
		}
	}).result(function (event,row,formatted){
		$('#categoryType').val(row[0]);
		$('#categoryName').val(row[2]);
		if(row[0] == "ALL"){
			$('#selectedItem').text(row[2]);
		}else if(row[0]=="ARTIST"){
			$('#categoryId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=="EVENT"){
			$('#categoryId').val(row[1]);
			$('#selectedItem').text(row[3]);
		}else if(row[0]=='PARENT'){
			$('#categoryId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=='CHILD'){
			$('#categoryId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}else if(row[0]=='GRAND'){
			$('#categoryId').val(row[1]);
			$('#selectedItem').text(row[2]);
		}
		$('#category').val("");
		$('#resetLink').show();
	});
	
	
	
	$('#category1').autocomplete("AutoCompleteArtistEventAndCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		formatItem: function(row, i, max) {
			if(row[0] == 'ALL'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='PARENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='EVENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[3] ;
			}
		}
	}).result(function (event,row,formatted){
		$('#categoryType1').val(row[0]);
		$('#categoryName1').val(row[2]);
		if(row[0] == "ALL"){
			$('#selectedItem1').text(row[2]);
		}else if(row[0]=="ARTIST"){
			$('#categoryId1').val(row[1]);
			$('#selectedItem1').text(row[2]);
		}else if(row[0]=="EVENT"){
			$('#categoryId1').val(row[1]);
			$('#selectedItem1').text(row[3]);
		}else if(row[0]=='PARENT'){
			$('#categoryId1').val(row[1]);
			$('#selectedItem1').text(row[2]);
		}else if(row[0]=='CHILD'){
			$('#categoryId1').val(row[1]);
			$('#selectedItem1').text(row[2]);
		}else if(row[0]=='GRAND'){
			$('#categoryId1').val(row[1]);
			$('#selectedItem1').text(row[2]);
		}
		$('#category1').val("");
		$('#resetLink1').show();
	});
	
	
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(preContestGrid != null && preContestGrid != undefined){
			preContestGrid.resizeCanvas();
		}
		if(preContestLogsGrid != null && preContestLogsGrid != undefined){
			preContestLogsGrid.resizeCanvas();
		}
		
	});
	
	<c:choose>
		<c:when test="${preContestStatus == 'ACTIVE'}">	
			$('#activeContestChecklist').addClass('active');
			$('#activeContestChecklistTab').addClass('active');
		</c:when>
		<c:when test="${preContestStatus == 'EXPIRED'}">
			$('#expiredContestChecklist').addClass('active');
			$('#expiredContestChecklistTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activePreContesthecklist1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#expiredContestChecklist1").click(function(){
		callTabOnChange('EXPIRED');
	});
	
});

function resetItem(){
	$('#resetLink').hide();
	$('#category').val("");
	$('#categoryId').val("");
	$('#categoryType').val("");
	$('#selectedItem').text("");
	$('#categoryName').val("");
}

function resetItem1(){
	$('#resetLink1').hide();
	$('#category1').val("");
	$('#categoryId1').val("");
	$('#categoryType1').val("");
	$('#selectedItem1').text("");
	$('#categoryName1').val("");
}
function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/PreContestChecklistMobile?status="+selectedTab;
}

function addContestChecklist(){
	resetPreContestModal();
	$('#action').val("SAVE");
	$('#contestChecklistModal').modal('show');
}
function savePreContest(){
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdatePreContestChecklist",
		type : "post",
		dataType: "json",
		data: $('#contestChecklistForm').serialize()+"&status=${preContestStatus}",
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				pagingInfo = JSON.parse(jsonData.preContestPagingInfo);
				refreshPreContestGridValues(JSON.parse(jsonData.preContestList));	
				$('#contestChecklistModal').modal('hide');
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function resetPreContestModal(){		
	$('#dateOfGame').val('');
	$('#action').val('');
	$('#preContestId').val('');
	$('#contestName').val('');
	$('#extendedName').val('');
	$('#zone').val('');
	$('#noOfQuestion').val('');
	$('#grandPrizeEvent').val('');
	$('#timeOfGame1').val('');
	$('#timeOfGame2').val('');
	$('#rewardDollarsPrize').val('');
	$('#grandPrizeEvent').val('');
	$('#noOfTickets').val('');
	$('#noOfGrandWinner').val('');
	$('#category').val('');
	$('#categoryName').val("");
	$('#categoryId').val('');
	$('#categoryType').val('');
	$('#category1').val('');
	$('#categoryName1').val("");
	$('#categoryId1').val('');
	$('#categoryType1').val('');
	$('#resetLink').hide();
	$('#selectedItem').text("");
	$('#resetLink1').hide();
	$('#selectedItem1').text("");
	$('#pricePerTicket').val('');
	$('#discountCode1').val('');
	$('#discountPercentage').val('');
	
	$('#CLhostName').val('');
	$('#CLproducerName').val('');
	$('#CLdirectorName').val('');
	$('#CLtechDirectorName').val('');
	$('#CLopearatorName').val('');
	$('#CLquestionEnteredBy').val('');
	$('#CLquestionVerifiedBy').val('');
	$('#CLrewardDollarsPerQue').val('');
	$('#CLapprovedBy').val('');
	
	document.getElementById('CLisQuestionEntered').checked = false;
	document.getElementById('CLisQuestionVerified').checked = false;	
}



//Edit PreContest Bank
function editPreContest(){
	var index = preContestGrid.getSelectedRows([0])[0];
	if (index < 0) {
		jAlert("Please select any pre Contest checklist to edit.");
		return;
	}
	var preContestId = preContestGrid.getDataItem(index).preContestId;
	if(preContestId < 0){
		jAlert("Please select any pre Contest checklist to edit.");
		return;
	}
	getEditPreContest(preContestId);
}

function getEditPreContest(preContestId){
	$.ajax({
		url : "${pageContext.request.contextPath}/UpdatePreContestChecklist",
		type : "post",
		dataType: "json",
		data: "preContestId="+preContestId+"&type="+$('#type').val()+"&action=EDIT&status=${preContestStatus}",
		success : function(response){
			var jsonData1 = JSON.parse(JSON.stringify(response));
			var jsonData = JSON.parse(jsonData1.preContest);
			if(jsonData1.status == 1){
				$('#dateOfGame').val(jsonData.contestDateStr);
				$('#action').val("UPDATE");
				$('#qUpdateConfigBtn').text('Update');
				$('#preContestId').val(jsonData.id);
				$('#contestName').val(jsonData.contestName);
				$('#extendedName').val(jsonData.extendedName);
				$('#zone').val(jsonData.zone);
				$('#noOfQuestion').val(jsonData.questionSize);
				$('#participantStar').val(data.participantStar);
				$('#referralStar').val(data.referralStar);
				$('#grandPrizeEvent').val(jsonData.grandPrizeEvent);
				$('#timeOfGame1').val(jsonData.hours);
				$('#timeOfGame2').val(jsonData.minutes);
				$('#rewardDollarsPrize').val(jsonData.rewardDollarsPrize);
				//$('#grandPrizeEvent').val(jsonData.);
				$('#noOfTickets').val(jsonData.grandWinnerTicketPrize);
				$('#noOfGrandWinner').val(jsonData.grandPrizeWinner);
				$('#categoryName').val(jsonData.categoryName);
				$('#selectedItem').text(jsonData.categoryName);
				$('#categoryId').val(jsonData.categoryId);
				$('#categoryType').val(jsonData.categoryType);
				$('#categoryName1').val(jsonData.promoCategoryName);
				$('#selectedItem1').text(jsonData.promoCategoryName);
				$('#categoryId1').val(jsonData.promoCategoryId);
				$('#categoryType1').val(jsonData.promoCategoryType);
				$('#pricePerTicket').val(jsonData.pricePerTicket);
				$('#discountCode1').val(jsonData.discountCode);
				$('#discountPercentage').val(jsonData.discountPercentage);
				
				$('#CLhostName').val(jsonData.hostName);
				$('#CLproducerName').val(jsonData.producerName);
				$('#CLdirectorName').val(jsonData.directorName);
				$('#CLtechDirectorName').val(jsonData.technicalDirectorName);
				$('#CLopearatorName').val(jsonData.backendOperatorName);
				$('#CLrewardDollarsPerQue').val(jsonData.rewardDollarsPerQuestion);
				
				
				document.getElementById('CLisQuestionEntered').checked = false;
				document.getElementById('CLisQuestionVerified').checked = false;
				if(jsonData.isQuestionEntered == true || jsonData.isQuestionEntered == 'true'){
					document.getElementById('CLisQuestionEntered').checked = true;
					$('#CLquestionEnteredBy').val(jsonData.questionEnteredBy);
				}
				if(jsonData.isQuestionVerified == true || jsonData.isQuestionVerified == 'true'){
					document.getElementById('CLisQuestionVerified').checked = true;
					$('#CLquestionVerifiedBy').val(jsonData.questionVerifiedBy);
				}
				
				$('#CLquestionEnteredBy').val(jsonData.questionEnteredBy);
				$('#CLquestionVerifiedBy').val(jsonData.questionVerifiedBy);
				$('#CLapprovedBy').val(jsonData.approvedBy);
				$('#contestChecklistModal').modal('show');
			}else{
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function getSelectedPreContestGridId() {
	var tempPreContestRowIndex = preContestGrid.getSelectedRows();
	
	var questionIdStr='';
	$.each(tempPreContestRowIndex, function (index, value) {
		questionIdStr += ','+preContestGrid.getDataItem(value).preContestId;
	});
	
	if(questionIdStr != null && questionIdStr!='') {
		questionIdStr = questionIdStr.substring(1, questionIdStr.length);
		 return questionIdStr;
	}
}
//Delete PreContest Bank
function deletePreContest(){
	var index = preContestGrid.getSelectedRows([0])[0];
	if (index < 0) {
		jAlert("Please select any pre Contest checklist to Delete.");
		return;
	}
	var preContestId = preContestGrid.getDataItem(index).preContestId;
	if(preContestId < 0){
		jAlert("Please select any pre Contest checklist to Delete.");
		return;
	}
	
	getDeletePreContest(preContestId);
}


function getDeletePreContest(preContestId){
	jConfirm("Are you sure to delete selected PreContest ?","Confirm",function(r){
		if (r) {
			$.ajax({
					url : "${pageContext.request.contextPath}/UpdatePreContestChecklist",
					type : "post",
					dataType: "json",
					data : "preContestId="+preContestId+"&type="+$('#type').val()+"&action=DELETE&status=${preContestStatus}",
					success : function(response) {
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.status == 1){
							pagingInfo = JSON.parse(jsonData.preContestPagingInfo);
							refreshPreContestGridValues(JSON.parse(jsonData.preContestList));	
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
					});
		} else {
			return false;
		}
	});
	return false;
}

</script>
<ul id="contextMenu" style="display: none; position: absolute">
	<li data="View Logs">View Logs</li>
	<!-- <li data="Mark it Expired">Mark it Expired</li> -->
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Pre Contest Checklist</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeContestChecklistTab" class=""><a id="activePreContesthecklist1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeContestChecklist">Active Pre-Contest Checklist</a></li>
				<li id="expiredContestChecklistTab" class=""><a id="expiredContestChecklist1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#expiredContestChecklist">Expired Pre-Contest Checklist</a></li>
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeContestChecklist" class="tab-pane">
			<c:if test="${preContestStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="addContestChecklist();">Add Pre-Contest Checklist</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="editPreContest()">Edit Pre-Contest Checklist</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePreContest()">Delete Pre-Contest Checklist</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="preContestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Pre-Contest Checklist</label>
							<div class="pull-right">
								<!-- <a href="javascript:preContestExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:preContestResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="preContest_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="preContest_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</c:if>				
			</div>
			<div id="expiredContestChecklist" class="tab-pane">
			<c:if test="${preContestStatus =='EXPIRED'}">	
				<div style="position: relative" id="preContestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Pre-Contest Checklist</label>
							<div class="pull-right">
								<!-- <a href="javascript:preContestExportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:preContestResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="preContest_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="preContest_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="contestChecklistLogsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pre Contest Checklist Logs</h4>
			</div>
			<div class="modal-body full-width">
				<div style="position: relative" id="preContestLogsGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Pre Contest Checklist Logs</label>
							<div class="pull-right">
								<!-- <a href="javascript:preContestLogsExportToExcel('EXPIRED')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:preContestLogsResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="preContestLogs_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="preContestLogs_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
			</div>	
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="contestChecklistModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest CheckList</h4>
			</div>
			<div class="modal-body full-width">
				<form name="contestChecklistForm" id="contestChecklistForm" method="post">
					<div class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Contest Name : </label>
							<input class="form-control" type="text" id="contestName" name="contestName"> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Contest Extended Name : </label>
							<input class="form-control" type="text" id="extendedName" name="extendedName"> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Date Of Game : </label>
							<input class="form-control" type="text" id="dateOfGame" name="dateOfGame">
							<input class="form-control" type="hidden" id="action" name="action">
							<input class="form-control" type="hidden" id="type" name="type" value="MOBILE">
							<input class="form-control" type="hidden" id="preContestId" name="preContestId">
						</div>	
						<div class="form-group col-sm-2 col-xs-2">
							<label>Hour<span class="required">*</span>
							<select name="timeOfGame1" id="timeOfGame1" class="form-control" style="width:120px;">
								<option value="00">00</option>
								<option value="1">01</option>
								<option value="01">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>
						</div>
						<div class="form-group col-sm-2 col-xs-2">
							<label>Minutes <span class="required">*</span>
							<select name="timeOfGame2" id="timeOfGame2" class="form-control" style="width:120px;">
								<option value="00">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">No. Of Questions : </label>
							<input class="form-control" type="text" id="noOfQuestion" name="noOfQuestion"> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Reward Dollars Prize : </label>
							<input class="form-control" type="text" id="rewardDollarsPrize" name="rewardDollarsPrize"> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Grand Prize Event : </label>
							<input class="form-control" type="text" id="grandPrizeEvent" name="grandPrizeEvent"> 
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;"># Of Tickets : </label> 
							<input class="form-control" type="text" id="noOfTickets" name="noOfTickets">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;"># Of Grand Prize Winners : </label> 
							<input class="form-control" type="text" id="noOfGrandWinner" name="noOfGrandWinner">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Contest Category/Artist : </label> 
							<input class="form-control" type="text" id="category" name="category">
							<input class="form-control" type="hidden" id="categoryName" name="categoryName">
							<input class="form-control" type="hidden" id="categoryId" name="categoryId">
							<input class="form-control" type="hidden" id="categoryType" name="categoryType">
							<label for="name" id="selectedItem">${artistName}</label> <a href="javascript:resetItem()" id="resetLink">remove</a>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Price Per Ticket : </label> 
							<input class="form-control" type="text" id="pricePerTicket" name="pricePerTicket">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Discount Code : </label> 
							<input class="form-control" type="text" id="discountCode1" name="discountCode">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Discount Code Category : </label> 
							<input class="form-control" type="text" id="category1" name="category1">
							<input class="form-control" type="hidden" id="categoryName1" name="categoryName1">
							<input class="form-control" type="hidden" id="categoryId1" name="categoryId1">
							<input class="form-control" type="hidden" id="categoryType1" name="categoryType1">
							<label for="name" id="selectedItem1">${artistName}</label> <a href="javascript:resetItem1()" id="resetLink1">remove</a>
						</div>	
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">% Off / Event : </label> 
							<input class="form-control" type="text" id="discountPercentage" name="discountPercentage">
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Contest Category<span class="required">*</span></label> 
							<select name="contestCategory" id="contestCategory" class="form-control" >
								<option value="NONE">-- Select Category --</option>
								<option value="ALL">ALL</option>
								<option value="SUPERFAN">SUPERFAN</option>
								<option value="SPORTS">SPORTS</option>
								<option value="CONCERTS">CONCERTS</option>
								<option value="THEATER">THEATER</option>
								<option value="MOVIE">MOVIE</option>
								<option value="BASKETBALL">BASKETBALL</option>
								<option value="BASEBALL">BASEBALL</option>
								<option value="FOOTBALL">FOOTBALL</option>
								<option value="HOCKEY">HOCKEY</option>
								<option value="SOCCER">SOCCER</option>
								<option value="CUSTOM">CUSTOM</option>
								<option value="MARKET">MARKETING</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Participant's SuperFan Game Star<span class="required">*</span></label> 
							<input class="form-control" type="text" id="participantStar" name="participantStar" >
						</div>
						<div class="form-group col-sm-3 col-xs-3" >
							<label>Referral's SuperFan Game Star<span class="required">*</span></label> 
							<input class="form-control" type="text" id="referralStar" name="referralStar" >
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label style="font-size: 15px;">Zone : </label> 
							<input class="form-control" type="text" id="zone" name="zone">
						</div>
							
					</div>	
					<div class="form-group tab-fields">
							<div class="form-group col-sm-4 col-xs-4">
								<label>Host 
								</label> <input class="form-control" type="text" id="CLhostName" name="hostName">
							</div>
							<div class="form-group col-sm-4 col-xs-4">
								<label>Producer
								</label> <input class="form-control" type="text" id="CLproducerName" name="producerName">
							</div>
							<div class="form-group col-sm-4 col-xs-4">
								<label>Director
								</label> <input class="form-control" type="text" id="CLdirectorName" name="directorName">
							</div>
							<div class="form-group col-sm-4 col-xs-4">
								<label>Technical Director
								</label> <input class="form-control" type="text" id="CLtechDirectorName" name="techDirectorName">
							</div>
							<div class="form-group col-sm-4 col-xs-4">
								<label>BackEnd Operator
								</label> <input class="form-control" type="text" id="CLopearatorName" name="opearatorName">
							</div>						
					</div>
					<div class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label>Question Entered Into TF / by 
							</label><input type="checkbox" id="CLisQuestionEntered" name="isQuestionEntered">
							<select name="questionEnteredBy" id="CLquestionEnteredBy" class="form-control">
								<option value="-1">--select--</option>
								<c:forEach items="${users}" var="user">
									<option value="${user.id}">${user.firstName} ${user.lastName }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Question Verified / By
							</label><input type="checkbox" id="CLisQuestionVerified" name="isQuestionVerified">
							 <select name="questionVerifiedBy" id="CLquestionVerifiedBy" class="form-control">
							 	<option value="-1">--select--</option>
							 	<c:forEach items="${users}" var="user1">
									<option value="${user1.id}">${user1.firstName} ${user1.lastName }</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Reward Dollars per Question
							</label> <input class="form-control" type="text" id="CLrewardDollarsPerQue" name="rewardDollarsPerQue">
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label>Approved By</label> 
							<select name="approvedBy" id="CLapprovedBy" class="form-control">
								<option value="-1">--select--</option>
								<c:forEach items="${users}" var="user2">
									<option value="${user2.id}">${user2.firstName} ${user2.lastName }</option>
								</c:forEach>
							</select>
						</div>
					</div>					
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qUpdateConfigBtn" type="button" onclick="savePreContest()">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function pagingControl(move, id) {
		if(id == 'preContest_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getPreContestGridData(pageNo);
		}
	}
	
	//PreContest checklist Grid
	
	function getPreContestGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetPreContestChecklist",
			type : "post",
			dataType: "json",
			data: "type="+$('#type').val()+"&action=GET&headerFilter="+preContestSearchString+"&status=${preContestStatus}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.preContestPagingInfo;
				refreshPreContestGridValues(jsonData.preContestList);	
				clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function preContestExportToExcel(status){
		var appendData = "headerFilter="+preContestSearchString+"&status=${preContestStatus}";
	    var url =apiServerUrl+"PreContestExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function preContestResetFilters(){
		preContestSearchString='';
		preContestColumnFilters = {};
		getPreContestGridData(0);
	}
	
	function reLoadPreContest(value){
		preContestSearchString = 'category:'+value+',';
		getPreContestGridData(0);
	}
	
	var pagingInfo;
	var preContestDataView;
	var preContestGrid;
	var preContestData = [];
	var preContestGridPager;
	var preContestSearchString='';
	var preContestColumnFilters = {};
	var userPreContestColumnsStr = '<%=session.getAttribute("preContestGrid")%>';

	var userPreContestColumns = [];
	var allPreContestColumns = [
			 {
				id : "preContestId",
				field : "preContestId",
				name : "Pre Contest Id",
				width : 80,
				sortable : true
			},{
				id : "contestName",
				field : "contestName",
				name : "Contest Name",
				width : 80,
				sortable : true
			},{
				id : "extendedName",
				field : "extendedName",
				name : "Extended Name",
				width : 80,
				sortable : true
			},{
				id : "contestDateTimeStr",
				field : "contestDateTimeStr",
				name : "Contest DateTime",
				width : 80,
				sortable : true
			},{
				id : "grandPrizeEvent",
				field : "grandPrizeEvent",
				name : "Grand Prize Event",
				width : 80,
				sortable : true
			},{
				id : "questionSize",
				field : "questionSize",
				name : "Question Size",
				width : 80,
				sortable : true
			},{
				id : "rewardDollarsPrize",
				field : "rewardDollarsPrize",
				name : "Reward Dollars Prize",
				width : 80,
				sortable : true
			},{
				id : "participantStar",
				name : "Participant Star",
				field : "participantStar",
				width : 80,
				sortable : true
			},{
				id : "referralStar",
				name : "Referral Star",
				field : "referralStar",
				width : 80,
				sortable : true
			}, {
				id : "contestCategory",
				name : "Contest Category",
				field : "contestCategory",
				width : 80,
				sortable : true
			},{
				id : "grandWinnerTicketPrize",
				field : "grandWinnerTicketPrize",
				name : "Grand WinnerTicket Prize",
				width : 80,
				sortable : true
			},{
				id : "grandPrizeWinner",
				field : "grandPrizeWinner",
				name : "Grand Prize Winner",
				width : 80,
				sortable : true
			},{
				id : "categoryType",
				field : "categoryType",
				name : "Category Type",
				width : 80,
				sortable : true
			}, {
				id : "categoryName",
				field : "categoryName",
				name : "Category Name",
				width : 80,
				sortable : true
			},{
				id : "promoCategoryType",
				field : "promoCategoryType",
				name : "Promo Category Type",
				width : 80,
				sortable : true
			}, {
				id : "promoCategoryName",
				field : "promoCategoryName",
				name : "Promo Category Name",
				width : 80,
				sortable : true
			}, {
				id : "zone",
				field : "zone",
				name : "Zone",
				width : 80,
				sortable : true
			},{
				id : "pricePerTicket",
				field : "pricePerTicket",
				name : "Price Per Ticket",
				width : 80,
				sortable : true
			},{
				id : "discountCode",
				field : "discountCode",
				name : "Discount Code",
				width : 80,
				sortable : true
			},{
				id : "discountPercentage",
				field : "discountPercentage",
				name : "Discount Percentage",
				width : 80,
				sortable : true
			},{
				id : "hostName",
				field : "hostName",
				name : "Host Name",
				width : 80,
				sortable : true
			},{
				id : "producerName",
				field : "producerName",
				name : "Producer Name",
				width : 80,
				sortable : true
			},{
				id : "directorName",
				field : "directorName",
				name : "Director Name",
				width : 80,
				sortable : true
			},{
				id : "technicalDirectorName",
				field : "technicalDirectorName",
				name : "Technical Director Name",
				width : 80,
				sortable : true
			},{
				id : "backendOperatorName",
				field : "backendOperatorName",
				name : "Backend Operator Name",
				width : 80,
				sortable : true
			},{
				id : "isQuestionEntered",
				field : "isQuestionEntered",
				name : "Question Entered?",
				width : 80,
				sortable : true
			},{
				id : "isQuestionVerified",
				field : "isQuestionVerified",
				name : "Question Verified?",
				width : 80,
				sortable : true
			},{
				id : "questionEnteredBy",
				field : "questionEnteredBy",
				name : "Question Entered By ",
				width : 80,
				sortable : true
			},{
				id : "questionVerifiedBy",
				field : "questionVerifiedBy",
				name : "Question Verified By",
				width : 80,
				sortable : true
			},{
				id : "rewardDollarsPerQuestion",
				field : "rewardDollarsPerQuestion",
				name : "Reward Dollars Per Question",
				width : 80,
				sortable : true
			},{
				id : "approvedBy",
				field : "approvedBy",
				name : "Approved By",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "createdDateStr",
				field : "createdDateStr",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDateStr",
				field : "updatedDateStr",
				name : "Updated Date",
				width : 80,
				sortable : true
			}, ];

	if (userPreContestColumnsStr != 'null' && userPreContestColumnsStr != '') {
		columnOrder = userPreContestColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPreContestColumns.length; j++) {
				if (columnWidth[0] == allPreContestColumns[j].id) {
					userPreContestColumns[i] = allPreContestColumns[j];
					userPreContestColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPreContestColumns = allPreContestColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.preContestId +"'/>";
	    return button;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditPreContest(id);
	});
	
	function deleteQBFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delQBClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.preContestId +"'/>";		
		return button;
	}
	
	
	$('.delQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getDeletePreContest(id);
	});	
	
	var preContestOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var preContestGridSortcol = "preContestId";
	var preContestGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function preContestGridComparer(a, b) {
		var x = a[preContestGridSortcol], y = b[preContestGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPreContestGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		preContestData = [];
		if (jsonData != null && jsonData.length >= 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (preContestData[i] = {});
				d["id"] = i;
				d["preContestId"] = data.id;
				d["contestName"] = data.contestName;
				d["extendedName"] = data.extendedName;
				d["questionSize"] = data.questionSize;
				d["zone"] = data.zone;
				d["grandPrizeEvent"] = data.grandPrizeEvent;
				d["contestDateTimeStr"] = data.contestDateTimeStr;
				d["rewardDollarsPrize"] = data.rewardDollarsPrize;
				d["grandWinnerTicketPrize"] = data.grandWinnerTicketPrize;
				d["grandPrizeWinner"] = data.grandPrizeWinner;
				d["contestCategory"] = data.contestCategory;
				d["participantStar"] = data.participantStar;
				d["referralStar"] = data.referralStar;
				d["categoryType"] = data.categoryType;
				d["categoryName"] = data.categoryName;
				d["promoCategoryType"] = data.promoCategoryType;
				d["promoCategoryName"] = data.promoCategoryName;
				d["pricePerTicket"] = data.pricePerTicket;
				d["discountCode"] = data.discountCode;
				d["discountPercentage"] = data.discountPercentage;
				d["hostName"] = data.hostName;
				d["producerName"] = data.producerName;
				d["directorName"] = data.directorName;
				d["technicalDirectorName"] = data.technicalDirectorName;
				d["backendOperatorName"] = data.backendOperatorName;
				if(data.isQuestionEntered == 'true' ||  data.isQuestionEntered == true){
					d["isQuestionEntered"] = "Yes";
				}else{
					d["isQuestionEntered"] = "No";
				}
				if(data.isQuestionVerified == 'true' ||  data.isQuestionVerified == true){
					d["isQuestionVerified"] = "Yes";
				}else{
					d["isQuestionVerified"] = "No";
				}
				
				d["questionEnteredBy"] = data.questionEnteredByName;
				d["questionVerifiedBy"] = data.questionVerifiedByName;
				d["rewardDollarsPerQuestion"] = data.rewardDollarsPerQuestion;
				d["approvedBy"] = data.approvedByName;
				d["updatedBy"] = data.updatedBy;
				d["createdBy"] = data.createdBy;
				d["status"] = data.status;
				d["createdDateStr"] = data.createdDateStr;
				d["updatedDateStr"] = data.updatedDateStr;
				
			}
		}

		preContestDataView = new Slick.Data.DataView();
		preContestGrid = new Slick.Grid("#preContest_grid", preContestDataView,
				userPreContestColumns, preContestOptions);
		preContestGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		preContestGrid.setSelectionModel(new Slick.RowSelectionModel());
		/* preContestGrid.registerPlugin(questionCheckboxSelector); */
		
		preContestGridPager = new Slick.Controls.Pager(preContestDataView,
					preContestGrid, $("#preContest_pager"),
					pagingInfo);
		var preContestGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPreContestColumns, preContestGrid, preContestOptions);
		
		
		preContestGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = preContestGrid.getCellFromEvent(e);
		      preContestGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'View Logs'){
				getPreContestLogs();
			}else if($(e.target).attr("data") == 'edit question'){
				changeStatus();
			}
		});
		
		
		preContestGrid.onSort.subscribe(function(e, args) {
			preContestGridSortdir = args.sortAsc ? 1 : -1;
			preContestGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				preContestDataView.fastSort(preContestGridSortcol, args.sortAsc);
			} else {
				preContestDataView.sort(preContestGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the preContestGrid
		preContestDataView.onRowCountChanged.subscribe(function(e, args) {
			preContestGrid.updateRowCount();
			preContestGrid.render();
		});
		preContestDataView.onRowsChanged.subscribe(function(e, args) {
			preContestGrid.invalidateRows(args.rows);
			preContestGrid.render();
		});
		$(preContestGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							preContestSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								preContestColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in preContestColumnFilters) {
										if (columnId !== undefined
												&& preContestColumnFilters[columnId] !== "") {
											preContestSearchString += columnId
													+ ":"
													+ preContestColumnFilters[columnId]
													+ ",";
										}
									}
									getPreContestGridData(0);
								}
							}

						});
		preContestGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editPreContest' && args.column.id != 'delPreContest'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(preContestColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(preContestColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		preContestGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		preContestDataView.beginUpdate();
		preContestDataView.setItems(preContestData);
		//preContestDataView.setFilter(filter);
		preContestDataView.endUpdate();
		preContestDataView.syncGridSelection(preContestGrid, true);
		preContestGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserPreContestPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = preContestGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('preContestGrid', colStr);
	}
	
	
	//Pre Contest Log Grid
	
	function preContestLogsExportToExcel(status){
		var appendData = "headerFilter="+preContestSearchString+"&status=${preContestStatus}";
	    var url =apiServerUrl+"PreContestLogsExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function preContestLogsResetFilters(){
		preContestLogsSearchString='';
		preContestLogsColumnFilters = {};
		getPreContestLogs();
	}
	
	var preContestLogsPagingInfo;
	var preContestLogsDataView;
	var preContestLogsGrid;
	var preContestLogsData = [];
	var preContestLogsGridPager;
	var preContestLogsSearchString='';
	var preContestLogsColumnFilters = {};
	var userPreContestLogsColumnsStr = '<%=session.getAttribute("preContestLogsGrid")%>';

	var userPreContestLogsColumns = [];
	var allPreContestLogsColumns = [
			 {
				id : "preContestLogsId",
				field : "preContestLogsId",
				name : "Log Id",
				width : 80,
				sortable : true
			},{
				id : "contestName",
				field : "contestName",
				name : "Contest Name",
				width : 80,
				sortable : true
			},{
				id : "extendedName",
				field : "extendedName",
				name : "Extended Name",
				width : 80,
				sortable : true
			},{
				id : "contestDateTimeStr",
				field : "contestDateTimeStr",
				name : "Contest DateTime",
				width : 80,
				sortable : true
			},{
				id : "grandPrizeEvent",
				field : "grandPrizeEvent",
				name : "Grand Prize Event",
				width : 80,
				sortable : true
			},{
				id : "questionSize",
				field : "questionSize",
				name : "Question Size",
				width : 80,
				sortable : true
			},{
				id : "rewardDollarsPrize",
				field : "rewardDollarsPrize",
				name : "Reward Dollars Prize",
				width : 80,
				sortable : true
			},{
				id : "grandWinnerTicketPrize",
				field : "grandWinnerTicketPrize",
				name : "Grand WinnerTicket Prize",
				width : 80,
				sortable : true
			},{
				id : "grandPrizeWinner",
				field : "grandPrizeWinner",
				name : "Grand Prize Winner",
				width : 80,
				sortable : true
			},{
				id : "categoryType",
				field : "categoryType",
				name : "Category Type",
				width : 80,
				sortable : true
			}, {
				id : "categoryName",
				field : "categoryName",
				name : "Category Name",
				width : 80,
				sortable : true
			},{
				id : "promoCategoryType",
				field : "promoCategoryType",
				name : "Promo Category Type",
				width : 80,
				sortable : true
			}, {
				id : "promoCategoryName",
				field : "promoCategoryName",
				name : "Promo Category Name",
				width : 80,
				sortable : true
			},{
				id : "zone",
				field : "zone",
				name : "Zone",
				width : 80,
				sortable : true
			}, {
				id : "pricePerTicket",
				field : "pricePerTicket",
				name : "Price Per Ticket",
				width : 80,
				sortable : true
			},{
				id : "discountCode",
				field : "discountCode",
				name : "Discount Code",
				width : 80,
				sortable : true
			},{
				id : "discountPercentage",
				field : "discountPercentage",
				name : "Discount Percentage",
				width : 80,
				sortable : true
			},{
				id : "hostName",
				field : "hostName",
				name : "Host Name",
				width : 80,
				sortable : true
			},{
				id : "producerName",
				field : "producerName",
				name : "Producer Name",
				width : 80,
				sortable : true
			},{
				id : "directorName",
				field : "directorName",
				name : "Director Name",
				width : 80,
				sortable : true
			},{
				id : "technicalDirectorName",
				field : "technicalDirectorName",
				name : "Technical Director Name",
				width : 80,
				sortable : true
			},{
				id : "backendOperatorName",
				field : "backendOperatorName",
				name : "Backend Operator Name",
				width : 80,
				sortable : true
			},{
				id : "isQuestionEntered",
				field : "isQuestionEntered",
				name : "Question Entered?",
				width : 80,
				sortable : true
			},{
				id : "isQuestionVerified",
				field : "isQuestionVerified",
				name : "Question Verified?",
				width : 80,
				sortable : true
			},{
				id : "questionEnteredBy",
				field : "questionEnteredBy",
				name : "Question Entered By ",
				width : 80,
				sortable : true
			},{
				id : "questionVerifiedBy",
				field : "questionVerifiedBy",
				name : "Question Verified By",
				width : 80,
				sortable : true
			},{
				id : "rewardDollarsPerQuestion",
				field : "rewardDollarsPerQuestion",
				name : "Reward Dollars Per Question",
				width : 80,
				sortable : true
			},{
				id : "approvedBy",
				field : "approvedBy",
				name : "Approved By",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "createdDateStr",
				field : "createdDateStr",
				name : "Created Date",
				width : 80,
				sortable : true
			} ];

	if (userPreContestLogsColumnsStr != 'null' && userPreContestLogsColumnsStr != '') {
		columnOrder = userPreContestLogsColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPreContestLogsColumns.length; j++) {
				if (columnWidth[0] == allPreContestLogsColumns[j].id) {
					userPreContestLogsColumns[i] = allPreContestLogsColumns[j];
					userPreContestLogsColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPreContestLogsColumns = allPreContestLogsColumns;
	}
	
	var preContestLogsOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var preContestLogsGridSortcol = "preContestLogsId";
	var preContestLogsGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function preContestLogsGridComparer(a, b) {
		var x = a[preContestLogsGridSortcol], y = b[preContestLogsGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPreContestLogsGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		preContestLogsData = [];
		if (jsonData != null && jsonData.length >= 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (preContestLogsData[i] = {});
				d["id"] = i;
				d["preContestLogsId"] = data.id;
				d["contestName"] = data.contestName;
				d["extendedName"] = data.extendedName;
				d["questionSize"] = data.questionSize;
				d["zone"] = data.zone;
				d["grandPrizeEvent"] = data.grandPrizeEvent;
				d["contestDateTimeStr"] = data.contestDateTimeStr;
				d["rewardDollarsPrize"] = data.rewardDollarsPrize;
				d["grandWinnerTicketPrize"] = data.grandWinnerTicketPrize;
				d["grandPrizeWinner"] = data.grandPrizeWinner;
				d["categoryType"] = data.categoryType;
				d["categoryName"] = data.categoryName;
				d["promoCategoryType"] = data.promoCategoryType;
				d["promoCategoryName"] = data.promoCategoryName;
				d["pricePerTicket"] = data.pricePerTicket;
				d["discountCode"] = data.discountCode;
				d["discountPercentage"] = data.discountPercentage;
				d["hostName"] = data.hostName;
				d["producerName"] = data.producerName;
				d["directorName"] = data.directorName;
				d["technicalDirectorName"] = data.technicalDirectorName;
				d["backendOperatorName"] = data.backendOperatorName;
				if(data.isQuestionEntered == 'true' ||  data.isQuestionEntered == true){
					d["isQuestionEntered"] = "Yes";
				}else{
					d["isQuestionEntered"] = "No";
				}
				if(data.isQuestionVerified == 'true' ||  data.isQuestionVerified == true){
					d["isQuestionVerified"] = "Yes";
				}else{
					d["isQuestionVerified"] = "No";
				}
				
				d["questionEnteredBy"] = data.questionEnteredByName;
				d["questionVerifiedBy"] = data.questionVerifiedByName;
				d["rewardDollarsPerQuestion"] = data.rewardDollarsPerQuestion;
				d["approvedBy"] = data.approvedByName;
				d["createdBy"] = data.createdBy;
				d["createdDateStr"] = data.createdDateStr;
				
			}
		}

		preContestLogsDataView = new Slick.Data.DataView();
		preContestLogsGrid = new Slick.Grid("#preContestLogs_grid", preContestLogsDataView,
				userPreContestLogsColumns, preContestLogsOptions);
		preContestLogsGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		preContestLogsGrid.setSelectionModel(new Slick.RowSelectionModel());
		/* preContestLogsGrid.registerPlugin(questionCheckboxSelector); */
		
		preContestLogsGridPager = new Slick.Controls.Pager(preContestLogsDataView,
					preContestLogsGrid, $("#preContestLogs_pager"),
					preContestLogsPagingInfo);
		var preContestLogsGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPreContestLogsColumns, preContestLogsGrid, preContestLogsOptions);
		
		
		preContestLogsGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = preContestLogsGrid.getCellFromEvent(e);
		      preContestLogsGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'View Logs'){
				preContestLogsSearchString = '';
				getPreContestLogs();
			}else if($(e.target).attr("data") == 'edit question'){
				changeStatus();
			}
		});
		
		
		preContestLogsGrid.onSort.subscribe(function(e, args) {
			preContestLogsGridSortdir = args.sortAsc ? 1 : -1;
			preContestLogsGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				preContestLogsDataView.fastSort(preContestLogsGridSortcol, args.sortAsc);
			} else {
				preContestLogsDataView.sort(preContestLogsGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the preContestLogsGrid
		preContestLogsDataView.onRowCountChanged.subscribe(function(e, args) {
			preContestLogsGrid.updateRowCount();
			preContestLogsGrid.render();
		});
		preContestLogsDataView.onRowsChanged.subscribe(function(e, args) {
			preContestLogsGrid.invalidateRows(args.rows);
			preContestLogsGrid.render();
		});
		$(preContestLogsGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							preContestLogsSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								preContestLogsColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in preContestLogsColumnFilters) {
										if (columnId !== undefined
												&& preContestLogsColumnFilters[columnId] !== "") {
											preContestLogsSearchString += columnId
													+ ":"
													+ preContestLogsColumnFilters[columnId]
													+ ",";
										}
									}
									getPreContestLogs();
								}
							}

						});
		preContestLogsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editPreContestLogs' && args.column.id != 'delPreContestLogs'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(preContestLogsColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(preContestLogsColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		preContestLogsGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		preContestLogsDataView.beginUpdate();
		preContestLogsDataView.setItems(preContestLogsData);
		//preContestLogsDataView.setFilter(filter);
		preContestLogsDataView.endUpdate();
		preContestLogsDataView.syncGridSelection(preContestLogsGrid, true);
		preContestLogsGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserPreContestLogsPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = preContestLogsGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('preContestLogsGrid', colStr);
	}
	
	
	
	
	function getPreContestLogs(){
		var index = preContestGrid.getSelectedRows([0])[0];
		if (index < 0) {
			jAlert("Please select any pre Contest checklist to view Logs.");
			return;
		}
		var preContestId = preContestGrid.getDataItem(index).preContestId;
		if(preContestId < 0){
			jAlert("Please select any pre Contest checklist to view Logs.");
			return;
		}
		$.ajax({
			url : "${pageContext.request.contextPath}/GetPreContestChecklistLogs",
			type : "post",
			dataType: "json",
			data: "preContestId="+preContestId+"&headerFilter="+preContestLogsSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status=1){
					$('#contestChecklistLogsModal').modal('show');
					preContestLogsPagingInfo = jsonData.preContestLogsPagingInfo;
					refreshPreContestLogsGridValues(jsonData.preContestListLogs);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${preContestPagingInfo};
		refreshPreContestGridValues(${preContestList});
		$('#preContest_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPreContestPreference()'>");
		
		enableMenu();
	}
		
</script>