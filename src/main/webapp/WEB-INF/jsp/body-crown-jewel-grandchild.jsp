<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

input {
	color: black !important;
}
</style>

<script>
var jq2 = $.noConflict(true);
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
		
	$("#childCategoryTab").click(function(){
		callTabOnChange('CrownJewelChild');
	});
	
	$("#grandChildCategoryTab").click(function(){
		callTabOnChange('CrownJewelGrandChild');
	});
	
		
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshGrandChildCategoryGridValues(JSON.parse(JSON.stringify(${grandChildCategory})));
		$('#grandChildCategory_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGrandChildCategoryPreference()'>");
	});
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  grandChildCategoryGrid.resizeCanvas();
	});
	
		
});

	function callTabOnChange(selectedTab) {		
		var data = selectedTab;			
		window.location = "${pageContext.request.contextPath}/CrownJewelEvents/"+data;
	}


	function saveUserGrandChildCategoryPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = grandChildCategoryGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('grandchildcategorygrid',colStr);
	}

	function resetFilters(){
		$('#action').val('search');
		grandChildCategoryGridSearchString='';
		columnFilters = {};
		getGrandChildCategoryGridData(0);
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+grandChildCategoryGridSearchString;
	    //var url = "${pageContext.request.contextPath}/CrownJewelEvents/GrandChildCategoryExportToExcel?"+appendData;
	    var url = apiServerUrl+"GrandChildCategoryExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
</script>

<div class="row">	
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Fantasy Sports Tickets
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Fantasy Sports Tickets</a></li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Child/Grand Child Category</li>
		</ol>
	</div>
</div>

<div id="childCategoryDiv">
	<div class="full-width">
		<section class="panel">
		<ul class="nav nav-tabs">
			<li class=""><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="childCategoryTab" href="#childCategory">Child Category</a></li>
			<li class="active"><a style="font-size: 13px; font-family: arial;" data-toggle="tab" id="grandChildCategoryTab" href="#grandChildCategory">Grand Child Category</a></li>
		</ul>
		</section>
	</div>
	<div class="full-width">
		<div class="tab-content">
		
		<div id="AddGrandChildCategorySuccessMsg" style="display: none;" class="alert alert-success fade in">
			Grand Child Category Created/Updated successfully.
		</div>
		<div id="">
			<b> APPLICABLE ONLY FOR SPORTS CATEGORY </b> 
		</div>	
		<div>
				<button type="button" style="margin-top:18px;" class="btn btn-primary" id="addGrandChildCategory" onclick="addModal();">Add Grand Child Category</button>
				<button type="button" style="margin-top:18px;" class="btn btn-primary" id="removeGrandChildCategory" onclick="removeGrandChildCategory()">Remove Grand Child Category</button>
		</div>
		<br/>
			<div id="childCategory" class="tab-pane active">
				<div class="table-responsive grid-table">
					<div class="grid-header" style="width: 100%">
						<label>Grand Child Category</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'> Reset Filters &nbsp; | </a>
						</div>
					</div>
					<div id="grandChildCategory_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="grandChildCategory_pager" style="width: 100%; height: 10px;"></div>
				</div>
			</div>
			<div id="grandChildCategory"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h4 class="modal-title">Add Grand Child Category</h4>
               </div>
               <div class="modal-body">
					
					<form role="form" class="form-horizontal" id="addGrandChildCategoryForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/updateGrandChildCategory">
						<input id="action" value="create" name="action" type="hidden" />
						<input type="hidden" id="id" name="id">
						<div class="form-group">									
							<div class="col-sm-4">
								<label>Parent Category</label>
									<select id="parentId" name="parentId" class="form-control" onChange="loadChild('childId','')">
									  <option value="-1">--select--</option>
									  <c:forEach items="${parentCategoryList}" var="parentCat">
										<option value="${parentCat.id}">
											 ${parentCat.name}
										</option>
									</c:forEach>
									</select>
							</div>
							<div class="col-sm-4">
								<label>Child Category</label>
									<select id="childId" name="childId" class="form-control ">
									  <option value="-1">--select--</option>
									  <!--<c:forEach items="${childCategoryList}" var="childCat">
										<option value="${childCat.id}">
											 ${childCat.name}
										</option>
									</c:forEach>-->
									</select>
							</div>
							<div class="col-sm-4">
								<label>Grand Child Category</label> 
								<input class="form-control"	id="name" type="text" name="name"/>
							</div>
							<div class="col-sm-4">
								<label>Package Cost</label> 
								<input class="form-control"	id="cost" type="text" name="cost"/>
							</div>
							<div class="col-sm-8 full-width">
								<label>&nbsp; &nbsp;</label>
							</div>																		
							<div class="col-sm-12 col-xs-12 full-width">
								<label>Package Description</label> 
								<input class="form-control"	id="description1" type="text" name="description1" placeholder="Package Line 1"/>
							</div>
							<div class="col-sm-12 col-xs-12 full-width">
								<label>&nbsp;</label>
								<input class="form-control"	id="description2" type="text" name="description2" placeholder="Package Line 2"/>
							</div>
							<div class="col-sm-12 col-xs-12 full-width">
								<label>&nbsp;</label>
								<input class="form-control"	id="description3" type="text" name="description3" placeholder="Package Line 3"/>
							</div>
							<div class="col-sm-12 col-xs-12 full-width">
								<label>&nbsp;</label>
								<input class="form-control"	id="description4" type="text" name="description4" placeholder="Package Line 4"/>
							</div>
							<div class="col-sm-12 col-xs-12 full-width">
								<label>&nbsp;</label>
								<input class="form-control"	id="description5" type="text" name="description5" placeholder="Package Line 5"/>
							</div>
							<div class="col-sm-12 col-xs-12 full-width">
								<label>&nbsp;</label>
								<input class="form-control"	id="description6" type="text" name="description6" placeholder="Package Line 6"/>
							</div>
						</div>							
					</form>
				</div>
				<div class="modal-footer full-width">				
					<div class="full-width" align="center">
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
						<button class="btn btn-success" type="button" onclick="addGrandChildCategory();" >Save</button>
					</div>						
				</div>
		</div>
	</div>
</div>

<script>
var grandChildCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	var pagingInfo;
	var grandChildCategoryView;
	var grandChildCategoryGrid;
	var grandChildCategoryData = [];
	var grandChildCategoryGridSearchString='';
	var columnFilters = {};
	var userGrandChildCategoryColumnsStr = '<%=session.getAttribute("grandchildcategorygrid")%>';
	var userGrandChildCategoryColumns = [];
	
	var allGrandChildCategoryColumns = [ grandChildCheckboxSelector.getColumnDefinition(), 
		{id : "id", name : "ID", field : "id", width : 20, sortable : true}, 
		{id : "parentCategory", name : "Parent Category", field : "parentCategory", width : 80, sortable : true}, 
		{id : "childCategory", name : "Child Category", field : "childCategory", width : 80, sortable : true},
		{id : "grandChildCategory", name : "Grand Child Category", field : "grandChildCategory", width : 80, sortable : true},
		{id : "packageCost", name : "Package Cost", field : "packageCost", width : 80, sortable : true},
		{id : "lastUpdatedBy", name : "Last Updated By", field : "lastUpdatedBy", width : 80, sortable : true},
		{id : "lastUpdated", name : "Last Updated", field : "lastUpdated", width : 80, sortable : true},
		{id : "editCol", name : "Edit",	field : "editCol", width : 20, sortable : true, formatter : editFormatter} ];

	if (userGrandChildCategoryColumnsStr != 'null' && userGrandChildCategoryColumnsStr != '') {
		var columnOrder = userGrandChildCategoryColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allGrandChildCategoryColumns.length; j++) {
				if (columnWidth[0] == allGrandChildCategoryColumns[j].id) {
					userGrandChildCategoryColumns[i] = allGrandChildCategoryColumns[j];
					userGrandChildCategoryColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userGrandChildCategoryColumns = allGrandChildCategoryColumns;
	}

	var grandChildCategoryOptions = {
		editable : true,
		enableCellNavigation : true,
		asyncEditorLoading : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var grandChildCategoryGridSortcol = "id";
	var openOrderGridSortdir = 1;
	var percentCompleteThreshold = 0;

	//function for edit functionality
	function editFormatter(row, cell, value, columnDef, dataContext) {		
		var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.id +"'/>";
		return button;
	}
	
	//Function to hook up the edit button event
	$('.editClickableImage').live('click', function() {
		var me = $(this), id = me.attr('id');
		editGrandChildCategory(id);
	});
	
	function grandChildCategoryComparer(a, b) {
		var x = a[grandChildCategoryGridSortcol], y = b[grandChildCategoryGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getGrandChildCategoryGridData(pageNo);
	}
	
	function getGrandChildCategoryGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelGrandChild.json",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+grandChildCategoryGridSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */				
				pagingInfo = jsonData.pagingInfo;
				refreshGrandChildCategoryGridValues(jsonData.grandChildCategory);
				clearAllSelections();
				$('#grandChildCategory_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserGrandChildCategoryPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshGrandChildCategoryGridValues(jsonData) {
		grandChildCategoryData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (grandChildCategoryData[i] = {});
				d["id"] = data.id;
				d["parentCategory"] = data.parentCategoryName;
				d["childCategory"] = data.childCategoryName;
				d["grandChildCategory"] = data.name;
				d["packageCost"] = data.packageCost;
				d["lastUpdatedBy"] = data.lastUpdatedBy;
				d["lastUpdated"] = data.lastUpdated;
			}
		}

		grandChildCategoryView = new Slick.Data.DataView();
		grandChildCategoryGrid = new Slick.Grid("#grandChildCategory_grid", grandChildCategoryView,
				userGrandChildCategoryColumns, grandChildCategoryOptions);
		grandChildCategoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true}));
		grandChildCategoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		grandChildCategoryGrid.registerPlugin(grandChildCheckboxSelector);
		if(pagingInfo != null){
			var grandChildCategoryPager = new Slick.Controls.Pager(grandChildCategoryView,
					grandChildCategoryGrid, $("#grandChildCategory_pager"),pagingInfo);
		}
		
		var grandChildCategoryColumnPicker = new Slick.Controls.ColumnPicker(
				allGrandChildCategoryColumns, grandChildCategoryGrid, grandChildCategoryOptions);

		grandChildCategoryGrid.onSort.subscribe(function(e, args) {
			childCategorySortdir = args.sortAsc ? 1 : -1;
			grandChildCategoryGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				grandChildCategoryView.fastSort(grandChildCategoryGridSortcol, args.sortAsc);
			} else {
				grandChildCategoryView.sort(grandChildCategoryComparer, args.sortAsc);
			}
		});
		
		// wire up model childCategory to drive the grandChildCategoryGrid
		grandChildCategoryView.onRowCountChanged.subscribe(function(e, args) {
			grandChildCategoryGrid.updateRowCount();
			grandChildCategoryGrid.render();
		});
		grandChildCategoryView.onRowsChanged.subscribe(function(e, args) {
			grandChildCategoryGrid.invalidateRows(args.rows);
			grandChildCategoryGrid.render();
		});
		
		$(grandChildCategoryGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			grandChildCategoryGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  grandChildCategoryGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getGrandChildCategoryGridData(0);
				}
			  }		 
		});
		grandChildCategoryGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'lastUpdated'){
					$("<input type='text' placeholder='mm/dd/yyyy hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id != 'editCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		grandChildCategoryGrid.init();
		
		// initialize the model after all the childCategory have been hooked up
		grandChildCategoryView.beginUpdate();
		grandChildCategoryView.setItems(grandChildCategoryData);
		
		grandChildCategoryView.endUpdate();
		grandChildCategoryView.syncGridSelection(grandChildCategoryGrid, true);
		grandChildCategoryGrid.resizeCanvas();

		var openOrderrowIndex;
		grandChildCategoryGrid.onSelectedRowsChanged
				.subscribe(function() {
					var temprOpenOrderRowIndex = grandChildCategoryGrid
							.getSelectedRows([ 0 ])[0];
					if (temprOpenOrderRowIndex != openOrderrowIndex) {
						openOrderrowIndex = temprOpenOrderRowIndex;
						$('#AddGrandChildCategorySuccessMsg').hide();
					}
				});

		
		$("div#divLoading").removeClass('show');
	}
	
	//toggle the add manual tickets creation modal to add
	function addModal(){
		$('#AddGrandChildCategorySuccessMsg').hide();
		grandChildCategoryFormUpdate(false,null,0);			
	}
	
	function editGrandChildCategory(grandChildId) {
		$('#AddGrandChildCategorySuccessMsg').hide();
		$.ajax({			  
			url : "${pageContext.request.contextPath}/CrownJewelEvents/getGrandChildCategoryValue",
			type : "post",
			dataType:"json",
			data : "grandChildId="+grandChildId,
			success : function(res){
				var jsonData = res;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg =  jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Grand Child Category Found.");
				} */ 
				grandChildCategoryFormUpdate(true,jsonData.grandChildCat, grandChildId);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function grandChildCategoryFormUpdate(isUpdateForm, jsonData, grandChildId) {
		if(isUpdateForm) {
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (grandChildCategoryData[i] = {});
				if(grandChildId == data.id){		
					$('#id').val(data.id);
					$('#name').val(data.name);
					$('#parentId').val(data.parentCatId);	
					loadChild('childId',data.childCatName);
					$('#childId').val(data.childCatId);
					$('#cost').val(data.cost);
					$('#description1').val(data.description1);
					$('#description2').val(data.description2);
					$('#description3').val(data.description3);
					$('#description4').val(data.description4);
					$('#description5').val(data.description5);
					$('#description6').val(data.description6);
				}
			}
		} else {
			$('#id').val('');
			$('#name').val('');
			$('#parentId').val('');
			$('#childId').val('');
			$('#cost').val('');
			$('#description1').val('');
			$('#description2').val('');
			$('#description3').val('');
			$('#description4').val('');
			$('#description5').val('');
			$('#description6').val('');
		}
		$('#myModal').modal('show');
	}
	
	function addGrandChildCategory(){
		$('#AddGrandChildCategorySuccessMsg').hide();
		var grandChildId = $('#id').val();
		var childId = $('#childId').val();
		var parentId = $('#parentId').val();
		var name  = $('#name').val();
		var packageCost = $('#cost').val();
		var packageLine1 = $('#description1').val();
		var packageLine2 = $('#description2').val();
		var packageLine3 = $('#description3').val();
		var packageLine4 = $('#description4').val();
		var packageLine5 = $('#description5').val();
		var packageLine6 = $('#description6').val();
		var flag = true;
		if(parentId < 0){
			jAlert("Please select valid Parent Category.");
			flag = false;
			return;
		}
		if(childId < 0){
			jAlert("Please select valid Child Category.");
			flag = false;
			return;
		}
		if(name == ''){
			jAlert("Please add valid name for Grand Child Category.");
			flag = false;
			return;
		}
		if(packageCost == ''){
			jAlert("Please add Package Cost.");
			$('#cost').focus();
			flag = false;
			return;
		}
		if(packageCost != '' && isNaN(packageCost)){
			jAlert("Please add valid Package Cost.");
			$('#cost').focus();
			flag = false;
			return;
		}
		if(packageLine1 == ''){
			jAlert("Please add Package Line 1.");
			$('#description1').focus();
			flag = false;
			return;
		}else if((packageLine2 == '') && (packageLine3 != '' || packageLine4 != '' || packageLine5 != '' || packageLine6 != '')){
			jAlert("Please add Package Line 2, then add other Line.");
			$('#description2').focus();
			flag = false;
			return;
		}else if((packageLine3 == '') && (packageLine4 != '' || packageLine5 != '' || packageLine6 != '')){
			jAlert("Please add Package Line 3, then add other Line.");
			$('#description3').focus();
			flag = false;
			return;
		}else if((packageLine4 == '') && (packageLine5 != '' || packageLine6 != '')){
			jAlert("Please add Package Line 4, then add other Line.");
			$('#description4').focus();
			flag = false;
			return;
		}else if((packageLine5 == '') && (packageLine6 != '')){
			jAlert("Please add Package Line 5, then add other Line.");
			$('#description5').focus();
			flag = false;
			return;
		}
		if(grandChildId != null && grandChildId != ""){
			$('#action').val('update');
		}else{
			$('#action').val('create');
		}
		if(flag){
			$.ajax({			  
				url : "${pageContext.request.contextPath}/CrownJewelEvents/updateGrandChildCategory",
				type : "post",
				data : $("#addGrandChildCategoryForm").serialize(),
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					$('#myModal').modal('hide');
					$('#AddGrandChildCategorySuccessMsg').show();
					getGrandChildCategoryGridData(0);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function removeGrandChildCategory() {
		var grandChildIds = getSelectedGrandChildCategoryGridId();
		var flag = true;
		if(grandChildIds == null || grandChildIds=='') {
			flag= false;
			jAlert("Select a Grand Child to remove from Grand Child Category.");
			return false;
		}
		
		if(flag) {
			jConfirm("Are you sure you want to remove selected child category?","confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/CrownJewelEvents/updateGrandChildCategory",
						type : "post",
						data : "action=delete&&grandChildIds="+ grandChildIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							$('#AddGrandChildCategorySuccessMsg').text(jsonData.msg);
							$('#AddGrandChildCategorySuccessMsg').show();
							getGrandChildCategoryGridData(0);
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		} else{
			jAlert("Select a Grand Child to remove from Grand Child Category.");
		} 
	}
	
	function getSelectedGrandChildCategoryGridId() {
		var tempChildRowIndex = grandChildCategoryGrid.getSelectedRows();
		
		var grandChildIdStr='';
		$.each(tempChildRowIndex, function (index, value) {
			grandChildIdStr += ','+grandChildCategoryGrid.getDataItem(value).id;
		});
		
		if(grandChildIdStr != null && grandChildIdStr!='') {
			grandChildIdStr = grandChildIdStr.substring(1, grandChildIdStr.length);
			 return grandChildIdStr;
		}
	}
	
	function loadChild(id,name){
		var parentIdStr =0;
		if(id=='childId'){
			parentIdStr = $('#parentId').val();
		}		
		if(parentIdStr>0){
			$.ajax({
				url : "${pageContext.request.contextPath}/GetFantasyChildCategory",
				type : "post",
				dataType:"json",
				data : "parentIdStr="+parentIdStr,
				success : function(res){
					var respData = JSON.parse(JSON.stringify(res));
					var jsonData = respData.fantasyChildCategoryList;
					updateChildCategoryCombo(jsonData,id,name);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$('#'+id).empty();
			$('#'+id).append("<option value='-1'>--select--</option>");
		}
	}

	function updateChildCategoryCombo(jsonData,id,childName){
		$('#'+id).empty();
		$('#'+id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				$('#'+id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
			}
			if(childName!=''){
				$("#"+id+" option:contains(" + childName + ")").attr('selected', 'selected');
			}
		}
	}

	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
</script>