<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Promotional Offer</title>
 <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
  <link href="../resources/css/datepicker.css" rel="stylesheet">
  
  <script src="../resources/js/slick/lib/firebugx.js"></script>
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

  <style>
    .cell-title {
      font-weight: bold;
    }
    .cell-effort-driven {
      text-align: center;
    }
     .slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .cell-selection {
      border-right-color: silver;
      border-right-style: solid;
      background: silver;
      color: gray;
      text-align: right;
      font-size: 10px;
    }
    .slick-row.selected .cell-selection {
      background-color: transparent; /* show default selected row background */
    }

#addUser {
  font-family: arial;
  font-size: 8pt;
  font-weight: bold;
  color: #2b2b2b;
  background-color: rgba(0, 122, 255, 0.32);
  border: 1px solid gray;
}

</style>
</head>
<body>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> MANAGE CUSTOMER
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Customer</a>
			</li>
			<li><i class="fa fa-laptop"></i>Promotional Code</li>
		</ol>
	</div>		
</div>
<br/>
<br/>

<div style="position: relative">
	<div class="table-responsive grid-table">	
		<div class="grid-header full-width">
			<label>Promotional Offer With Purchase</label>
			<div class="pull-right">
				<a href="javascript:promoTrackingExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:promoTrackingResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="promoTracking_Grid"style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="promoTracking_pager" style="width: 100%; height: 20px;"></div>
		<br />
	</div>
</div>
<br/>
<br/>

<div style="position: relative">
	<div class="table-responsive grid-table">	
		<div class="grid-header full-width">
			<label>Promotional Offer Without Purchase</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="customerPC_Grid"style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
		<div id="customerPC_pager" style="width: 100%; height: 20px;"></div>
		<br />
	</div>
</div>

<!-- popup View Affiliate Order Details -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-order-details">View Order Details</button> -->
	<div id="view-order-details" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
		<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">View Order Details</h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Customer Promotional Order
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a  style="font-size: 13px;font-family: arial;" href="#">Customer Order</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Promotional Order Summery</li>
							</ol>
						</div>
					</div>

					<br/>
					<div class="row">
					<div class="col-xs-12">
							<section class="panel full-width"> 
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">
							<b>Order Info</b> </header>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Order ID :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_id"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Order Total :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_total"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Ticket Quantity :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_qty"></span>
									</div>
								
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Event Name :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_eventName"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Event Date & Time :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_eventDateTime"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Venue :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_venue"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Venue City:&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_venueCity"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Venue State:&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_venueState"></span>
									</div>
									<div class="form-group col-md-4 col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Platform :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_platform"></span>
									</div>
								</div>
							</div>
							</section>
						</div>
					</div>

					<div class="row">
					<div class="col-xs-12">
							<section class="panel"> 
							<header style="font-size: 13px;font-family: arial;" class="panel-heading">
							<b>Customer Info</b> </header>
							<div class="panel-body">
								<div class="row">
									<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Customer Name :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_customerName"></span>
									</div>
									<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Email :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_email"></span>
									</div> 
									<!--<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Referrer Code :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_referrerCode"></span>
									</div> -->
									<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Promotional Code :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_promoCode"></span>
									</div>
									<div class="form-group col-sm-6 col-xs-12">
										<label style="font-size: 13px;font-weight:bold;">Sign-Up Date :&nbsp;</label>
										<span style="font-size: 15px;" id="vOrder_signUpDate"></span>
									</div>
								</div>
							</div>
							</section>
						</div>
					</div>

				</div>
				<div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- End popup View Order Details -->


<script>

	//Promotional Offer Tracking Grid
	var promoTrackingPagingInfo;
	var promoTrackingDataView;
	var promoTrackingGrid;
	var promoTrackingData = [];
	var promoTrackingSearchString='';
	var promoTrackingColumnFilters = {};
	var promoTrackingPrefColumnStr = '<%=session.getAttribute("promoTrackingPrefGrid")%>';
	var promoTrackingColumns =[];
	var allPromoTrackingColumns = [ /*{
		id : "customerId",
		name : "Customer Id",
		field : "customerId",
		width:80,
		sortable : true
	},*/{
		id : "firstName",
		name : "First Name",
		field : "firstName",
		width:80,
		sortable : true
	}, {
		id : "lastName",
		name : "Last Name",
		field : "lastName",
		width:80,
		sortable : true
	}, {
		id : "email",
		name : "Email",
		field : "email",
		width:80,
		sortable : true
	}, {
		id : "promoCode",
		name : "Promotional Code",
		field : "promoCode",
		width : 80,
		sortable : true
	}, {
		id: "discount",
		name: "Discount",
		field: "discount",
		width:80,
		sortable: true
	}, {
		id: "orderId",
		name: "Order Id",
		field: "orderId",
		width:80,
		sortable: true
	}, {
		id: "orderTotal",
		name: "Order Total",
		field: "orderTotal",
		width:80,
		sortable: true
	}, {
		id: "quantity",
		name: "Quantity",
		field: "quantity",
		width:80,
		sortable: true
	}, {
		id: "ipAddress",
		name: "IP Address",
		field: "ipAddress",
		width:80,
		sortable: true
	}, {
		id: "primaryPaymentMethod",
		name: "Primary Payment Method",
		field: "primaryPaymentMethod",
		width:80,
		sortable: true
	}, {
		id: "secondaryPaymentMethod",
		name: "Secondary Payment Method",
		field: "secondaryPaymentMethod",
		width:80,
		sortable: true
	}, {
		id: "thirdPaymentMethod",
		name: "Third Payment Method",
		field: "thirdPaymentMethod",
		width:80,
		sortable: true
	}, {
		id: "platform",
		name: "Platform",
		field: "platform",
		width:80,
		sortable: true
	},/*  {
		id: "status",
		name: "Status",
		field: "status",
		width:80,
		sortable: true
	}, */ {
		id: "createdDate",
		name: "Order Created Date",
		field: "createdDate",
		width:80,
		sortable: true
	}, {
		id : "viewOrder",
		name : "View",
		field : "viewOrder",
		width : 80,
		sortable : true,
		formatter : viewLinkFormatter
	}];

	if(promoTrackingPrefColumnStr!='null' && promoTrackingPrefColumnStr!=''){
		var columnOrder = promoTrackingPrefColumnStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allPromoTrackingColumns.length;j++){
				if(columnWidth[0] == allPromoTrackingColumns[j].id){
					promoTrackingColumns[i] =  allPromoTrackingColumns[j];
					promoTrackingColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		promoTrackingColumns = allPromoTrackingColumns;
	}
	
	var promoTrackingOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "customerId";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
	
	//function for view functionality
	function viewLinkFormatter(row, cell, value, columnDef, dataContext) {		
		var link = "<img class='editClickableImage' style='height:17px;' src='../resources/images/viewIcon.png' onclick='viewAction("
				+ promoTrackingGrid.getDataItem(row).orderId+ ")'/>";		
		return link;
	}
	/*
	function myFilter(item, args) {
		var x= item["customerName"];
		if (args.searchString != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function promoTrackingComparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function toggleFilterRow() {
		promoTrackingGrid.setTopPanelVisibility(!promoTrackingGrid.getOptions().showTopPanel);
	}
	$(".promoTrackingGrid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	
	function getPromoTrackingGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/Client/GetCustomerPromotionalOfferTracking",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+promoTrackingSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				if(jsonData.status == 1) {
					promoTrackingPagingInfo = jsonData.promoOfferTrackingPagingInfo;
					refreshPromoTrackingGridValues(jsonData.promoOfferTrackingList);
					clearAllSelections();
					$('#promoTracking_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='savePromoTrackingPreference()'>");
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshPromoTrackingGridValues(jsonData) {
		promoTrackingData = [];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  promoOffer = jsonData[i];
				var d = (promoTrackingData[i] = {});
				d["id"] = promoOffer.id;
				d["customerId"] = promoOffer.customerId;
				d["firstName"] = promoOffer.firstName;
				d["lastName"] = promoOffer.lastName;
				d["email"] = promoOffer.email;
				d["promoCode"] = promoOffer.promoCode;
				d["discount"] = promoOffer.discount;
				d["orderId"] = promoOffer.orderId;
				d["orderTotal"] = promoOffer.orderTotal;
				d["quantity"] = promoOffer.quantity;
				d["ipAddress"] = promoOffer.ipAddress;
				d["primaryPaymentMethod"] = promoOffer.primaryPaymentMethod;
				d["secondaryPaymentMethod"] = promoOffer.secondaryPaymentMethod;
				d["thirdPaymentMethod"] = promoOffer.thirdPaymentMethod;
				d["platform"] = promoOffer.platform;
				d["status"] = promoOffer.status;
				d["createdDate"] = promoOffer.createdDate;
			}
		}
		
		promoTrackingDataView = new Slick.Data.DataView();
		promoTrackingGrid = new Slick.Grid("#promoTracking_Grid", promoTrackingDataView, promoTrackingColumns, promoTrackingOptions);
		promoTrackingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		promoTrackingGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(promoTrackingPagingInfo!=null){
			var promoTrackingPager = new Slick.Controls.Pager(promoTrackingDataView, promoTrackingGrid, $("#promoTracking_pager"), promoTrackingPagingInfo);
		}
		
		var promoTrackingColumnPicker = new Slick.Controls.ColumnPicker(allPromoTrackingColumns, promoTrackingGrid,
				promoTrackingOptions);

		// move the filter panel defined in a hidden div into promoTrackingGrid top panel
		//$("#inlineFilterPanel").appendTo(promoTrackingGrid.getTopPanel()).show();

		promoTrackingGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < promoTrackingDataView.getLength(); i++) {
				rows.push(i);
			}
			promoTrackingGrid.setSelectedRows(rows);
			e.preventDefault();
		});
				
		promoTrackingGrid.onSort.subscribe(function(e, args) {
			sortcol = args.sortCol.field;
			if(sortingString.indexOf(sortcol) < 0){
				sortdir = 'ASC';
			}else{
				if(sortdir == 'DESC' ){
					sortdir = 'ASC';
				}else{
					sortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+sortcol+',SORTINGORDER:'+sortdir+',';
			getPromoTrackingGridData(0);
		});
		// wire up model events to drive the promoTrackingGrid
		promoTrackingDataView.onRowCountChanged.subscribe(function(e, args) {
			promoTrackingGrid.updateRowCount();
			promoTrackingGrid.render();
		});
		promoTrackingDataView.onRowsChanged.subscribe(function(e, args) {
			promoTrackingGrid.invalidateRows(args.rows);
			promoTrackingGrid.render();
		});
		$(promoTrackingGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	promoTrackingSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				promoTrackingColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in promoTrackingColumnFilters) {
					  if (columnId !== undefined && promoTrackingColumnFilters[columnId] !== "") {
						  promoTrackingSearchString += columnId + ":" +promoTrackingColumnFilters[columnId]+",";
					  }
					}
					getPromoTrackingGridData(0);
				}
			  }
		 
		});
		promoTrackingGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'viewOrder' && args.column.id != 'discount'){
					if(args.column.id == 'createdDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(promoTrackingColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(promoTrackingColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}
			
		});
		promoTrackingGrid.init();
			
		/*
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			promoTrackingDataView.setFilterArgs({
				searchString : searchString
			});
			promoTrackingDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		promoTrackingDataView.beginUpdate();
		promoTrackingDataView.setItems(promoTrackingData);
		/*promoTrackingDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		promoTrackingDataView.setFilter(myFilter);*/
		promoTrackingDataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		promoTrackingDataView.syncGridSelection(promoTrackingGrid, true);
		//$("#gridContainer").resizable();
		promoTrackingGrid.resizeCanvas();
	}
	
	function promoTrackingResetFilters(){
		promoTrackingSearchString='';
		promoTrackingColumnFilters = {};
		sortingString ='';
		getPromoTrackingGridData(0);
	}
	
	function promoTrackingExportToExcel(){
		var appendData = "headerFilter="+promoTrackingSearchString;
	    //var url = "${pageContext.request.contextPath}/Client/PromoOfferTrackingExportToExcel?"+appendData;
		var url = apiServerUrl+"PromoOfferTrackingExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function savePromoTrackingPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = custPromoGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('promoTrackingPrefGrid',colStr);
	}	
	
	
	//Customer Promotional Offer Grid
	var pagingInfo;
	var customrId;
	var custPromoDataView;
	var custPromoGrid;
	var custPromoData = [];
	var custPromoTrackingSearchString='';
	var custPromoColumnFilters = {};
	var sortingString='';
	var custPromoPrefColumnStr = '<%=session.getAttribute("custPromoPrefGrid")%>';
	var custPromoColumns =[];
	var allCustPromoColumns = [ /*{
		id : "customerId",
		name : "Customer Id",
		field : "customerId",
		width:80,
		sortable : true
	},*/{
		id : "firstName",
		name : "First Name",
		field : "firstName",
		width:80,
		sortable : true
	}, {
		id : "lastName",
		name : "Last Name",
		field : "lastName",
		width:80,
		sortable : true
	}, {
		id : "email",
		name : "Email",
		field : "email",
		width:80,
		sortable : true
	}, {
		id : "promoCode",
		name : "Promotional Code",
		field : "promoCode",
		width : 80,
		sortable : true
	}, {
		id: "discount",
		name: "Discount",
		field: "discount",
		width:80,
		sortable: true
	}, {
		id: "startDate",
		name: "Start Date",
		field: "startDate",
		width:80,
		sortable: true
	}, {
		id: "endDate",
		name: "End Date",
		field: "endDate",
		width:80,
		sortable: true
	}, {
		id: "status",
		name: "Status",
		field: "status",
		width:80,
		sortable: true
	}, {
		id: "createdDate",
		name: "Created Date",
		field: "createdDate",
		width:80,
		sortable: true
	}, {
		id: "modifiedDate",
		name: "Last Updated",
		field: "modifiedDate",
		width:80,
		sortable: true
	}/* , {
		id : "viewOrder",
		name : "View",
		field : "viewOrder",
		width : 80,
		sortable : true,
		formatter : viewLinkFormatter
	} */];

	if(custPromoPrefColumnStr!='null' && custPromoPrefColumnStr!=''){
		var columnOrder = custPromoPrefColumnStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allCustPromoColumns.length;j++){
				if(columnWidth[0] == allCustPromoColumns[j].id){
					custPromoColumns[i] =  allCustPromoColumns[j];
					custPromoColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		custPromoColumns = allCustPromoColumns;
	}
	
	var custPromoOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var sortcol = "customerId";
	var sortdir = 1;
	var percentCompleteThreshold = 0;
	
	//function for view functionality
	/*function viewLinkFormatter(row, cell, value, columnDef, dataContext) {
		var promoOfferStatus = custPromoGrid.getDataItem(row).status;
		var link = '';
		if(promoOfferStatus != '' && promoOfferStatus == 'REDEEMED'){
			link = "<img class='editClickableImage' style='height:17px;' src='../resources/images/viewIcon.png' onclick='viewAction("
				+ custPromoGrid.getDataItem(row).id+ ")'/>";
		}
		return link;
	}
	
	function myFilter(item, args) {
		var x= item["customerName"];
		if (args.searchString != ""
				&& x.indexOf(args.searchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function custPromoComparer(a, b) {
		var x = a[sortcol], y = b[sortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function toggleFilterRow() {
		custPromoGrid.setTopPanelVisibility(!custPromoGrid.getOptions().showTopPanel);
	}
	$(".custPromoGrid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	function pagingControl(move,id){
		if(id=='customerPC_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getCustPromoGridData(pageNo);
		}else if(id == 'promoTracking_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(promoTrackingPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(promoTrackingPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(promoTrackingPagingInfo.pageNum)-1;
			}
			getPromoTrackingGridData(pageNo);
		}
	}
	
	function getCustPromoGridData(pageNo) {
		$.ajax({
			url : "${pageContext.request.contextPath}/Client/CustomerPromotionalOffer.json",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+custPromoTrackingSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				if(jsonData.status == 1) {
					pagingInfo = jsonData.promoOfferPagingInfo;
					refreshCustPromoGridValues(jsonData.promoOfferList);
					clearAllSelections();
					$('#customerPC_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveCustomerPromoPreference()'>");
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function refreshCustPromoGridValues(jsonData) {
		custPromoData = [];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  promoOffer = jsonData[i];
				var d = (custPromoData[i] = {});
				d["id"] = promoOffer.id;
				d["customerId"] = promoOffer.customerId;
				d["firstName"] = promoOffer.firstName;
				d["lastName"] = promoOffer.lastName;
				d["email"] = promoOffer.email;
				d["promoCode"] = promoOffer.promoCode;
				d["discount"] = promoOffer.discount;
				d["startDate"] = promoOffer.startDate;
				d["endDate"] = promoOffer.endDate;
				d["status"] = promoOffer.status;
				d["createdDate"] = promoOffer.createdDate;
				d["modifiedDate"] = promoOffer.modifiedDate;
			}
		}
		
		custPromoDataView = new Slick.Data.DataView();
		custPromoGrid = new Slick.Grid("#customerPC_Grid", custPromoDataView, custPromoColumns, custPromoOptions);
		custPromoGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custPromoGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var custPromoPager = new Slick.Controls.Pager(custPromoDataView, custPromoGrid, $("#customerPC_pager"), pagingInfo);
		}
		
		var columnpicker = new Slick.Controls.ColumnPicker(allCustPromoColumns, custPromoGrid,
				custPromoOptions);

		// move the filter panel defined in a hidden div into custPromoGrid top panel
		//$("#inlineFilterPanel").appendTo(custPromoGrid.getTopPanel()).show();

		custPromoGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < custPromoDataView.getLength(); i++) {
				rows.push(i);
			}
			custPromoGrid.setSelectedRows(rows);
			e.preventDefault();
		});
		custPromoGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = custPromoGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != customrId) {
				customrId = custPromoGrid.getDataItem(temprEventRowIndex).customerId;
			}
		});
		
		custPromoGrid.onSort.subscribe(function(e, args) {			
			sortcol = args.sortCol.field;
			if(sortingString.indexOf(sortcol) < 0){
				sortdir = 'ASC';
			}else{
				if(sortdir == 'DESC' ){
					sortdir = 'ASC';
				}else{
					sortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+sortcol+',SORTINGORDER:'+sortdir+',';
			getCustPromoGridData(0);
		});
		// wire up model events to drive the custPromoGrid
		custPromoDataView.onRowCountChanged.subscribe(function(e, args) {
			custPromoGrid.updateRowCount();
			custPromoGrid.render();
		});
		custPromoDataView.onRowsChanged.subscribe(function(e, args) {
			custPromoGrid.invalidateRows(args.rows);
			custPromoGrid.render();
		});
		$(custPromoGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	custPromoTrackingSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				custPromoColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in custPromoColumnFilters) {
					  if (columnId !== undefined && custPromoColumnFilters[columnId] !== "") {
						  custPromoTrackingSearchString += columnId + ":" +custPromoColumnFilters[columnId]+",";
					  }
					}
					getCustPromoGridData(0);
				}
			  }
		 
		});
		custPromoGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'viewOrder'  && args.column.id != 'discount'){
					if(args.column.id == 'startDate' || args.column.id == 'endDate' || args.column.id == 'createdDate' || args.column.id == 'modifiedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(custPromoColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(custPromoColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}
			
		});
		custPromoGrid.init();
			
		/*
		$("#txtSearch,#txtSearch2").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			searchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			custPromoDataView.setFilterArgs({
				searchString : searchString
			});
			custPromoDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		custPromoDataView.beginUpdate();
		custPromoDataView.setItems(custPromoData);
		/*custPromoDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			searchString : searchString
		});
		custPromoDataView.setFilter(myFilter);*/
		custPromoDataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		custPromoDataView.syncGridSelection(custPromoGrid, true);
		//$("#gridContainer").resizable();
		custPromoGrid.resizeCanvas();
	}
	
	function resetFilters(){
		custPromoTrackingSearchString='';
		sortingString ='';
		custPromoColumnFilters = {};
		getCustPromoGridData(0);
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+custPromoTrackingSearchString;
	    //var url = "${pageContext.request.contextPath}/Client/CustomerPromoOfferTrackingExportToExcel?"+appendData;
		var url = apiServerUrl+"CustomerPromoOfferTrackingExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function viewAction(id){
		getCustomerPromotionalOrderSummary(id);
	}
	
	//Start View Order Summary
	function getCustomerPromotionalOrderSummary(promoOfferOrderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Client/GetCustomerPromotionalOrderSummary",
			type : "post",
			data : "promoOfferOrderId="+promoOfferOrderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setCustomerPromotionalOrderSummary(jsonData);
				}else{
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setCustomerPromotionalOrderSummary(jsonData){
		$('#view-order-details').modal('show');
		
		var promoOfferHistory = jsonData.promotionalOfferHistory;
		$('#vOrder_id').text(promoOfferHistory.orderId);
		$('#vOrder_total').text(promoOfferHistory.orderTotal);
		$('#vOrder_qty').text(promoOfferHistory.orderQuantity);
		$('#vOrder_eventName').text(promoOfferHistory.eventName);
		$('#vOrder_eventDateTime').text(promoOfferHistory.eventDate+" "+promoOfferHistory.eventTime);
		$('#vOrder_venue').text(promoOfferHistory.venue);
		$('#vOrder_venueCity').text(promoOfferHistory.venueCity);
		$('#vOrder_venueState').text(promoOfferHistory.venueState);
		$('#vOrder_platform').text(promoOfferHistory.platform);
		$('#vOrder_customerName').text(promoOfferHistory.firstName+" "+promoOfferHistory.lastName);
		$('#vOrder_email').text(promoOfferHistory.email);
		//$('#vOrder_referrerCode').text(promoOfferHistory.referrerCode);
		$('#vOrder_promoCode').text(promoOfferHistory.promoCode);
		$('#vOrder_signUpDate').text(promoOfferHistory.signUpDate);		
	}
	//End View Order Summary
	
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}
	
	function saveCustomerPromoPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = custPromoGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('custPromoPrefGrid',colStr);
	}
	
	$(document).ready(function(){
		
		$('#menuContainer').click(function(){
			 if($('.ABCD').length >0){
			   $('#menuContainer').removeClass('ABCD');
			 }else{
			   $('#menuContainer').addClass('ABCD');
			 }
			 if(promoTrackingGrid != null && promoTrackingGrid != undefined){
				promoTrackingGrid.resizeCanvas();
			 }
			 if(custPromoGrid != null && custPromoGrid != undefined){
				custPromoGrid.resizeCanvas();
			 }		  
		});
		
	});

	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${promoOfferPagingInfo}));
		refreshCustPromoGridValues(JSON.parse(JSON.stringify(${promoOfferList})));
		$('#customerPC_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveCustomerPromoPreference()'>");
		promoTrackingPagingInfo = JSON.parse(JSON.stringify(${promoOfferTrackingPagingInfo}));
		refreshPromoTrackingGridValues(JSON.parse(JSON.stringify(${promoOfferTrackingList})));
		$('#promoTracking_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='savePromoTrackingPreference()'>");
	};

	
</script>
</body>
</html>