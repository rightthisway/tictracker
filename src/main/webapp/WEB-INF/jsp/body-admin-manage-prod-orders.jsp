<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenu1 {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu1 li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu1 li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
.overlay_ul_new {
    margin: 0;
    padding: 0;
    list-style: none;
}
.full-width-btn .btn {
    margin: -5px 10px 10px 0;
}
</style>
<script>
var questionArray = [];
var j = 0;
var jq2 = $.noConflict(true);
$(document).ready(function() {
	$("#newOrder1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#openOrderTest1").click(function(){
		callTabOnChange('ACCEPTED');
	});
	$("#fulfilledOrder1").click(function(){
		callTabOnChange('FULFILLED');
	});
	$("#voidedOrder1").click(function(){
		callTabOnChange('VOIDED');
	});
	$("#rejectedOrder1").click(function(){
		callTabOnChange('REJECTED');
	});
	$("#allOrder1").click(function(){
		callTabOnChange('ALL');
	});
		
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(orderGrid != null && orderGrid != undefined){
			orderGrid.resizeCanvas();
		}
	});
	
	$('#expDelDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$('#delDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
	});
	
	
	<c:choose>
	<c:when test="${status == 'ACTIVE'}">	
		$('#newOrder').addClass('active');
		$('#newOrderTab').addClass('active');
	</c:when>
	<c:when test="${status == 'ACCEPTED'}">	
		$('#openOrderTest').addClass('active');
		$('#openOrderTestTab').addClass('active');
	</c:when>
	<c:when test="${status == 'FULFILLED'}">
		$('#fulfilledOrder').addClass('active');
		$('#fulfilledOrderTab').addClass('active');
	</c:when>
	<c:when test="${status == 'VOIDED'}">
		$('#voidedOrder').addClass('active');
		$('#voidedOrderTab').addClass('active');
	</c:when>
	<c:when test="${status == 'REJECTED'}">	
		$('#rejectedOrder').addClass('active');
		$('#rejectedOrderTab').addClass('active');
	</c:when>
		<c:when test="${status == 'ALL'}">	
		$('#allOrder').addClass('active');
		$('#allOrderTab').addClass('active');
	</c:when>
	</c:choose>
	
});



function resetModal(){
	$('#orderModal').modal('show');
	$('#orderId').val('');
	$('#fName').val('');
	$('#lName').val('');
	$('#compName').val('');
	$('#email').val('');	
	$('#phone').val('');	
	$('#addLine1').val('');
	$('#addLine2').val('');
	$('#city').val('');
	$('#state').val('');
	$('#country').val('');
	$('#pincode').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();
}

function orderSave(action){
	
	var fName = $('#fName').val();
	var lName = $('#lName').val();
	var compName = $('#compName').val();	
	var email = $('#email').val();
	var phone = $('#phone').val();
	var addLine1 = $('#addLine1').val();
	var addLine2 = $('#addLine2').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	

	
	
	if(fName == ''){
		jAlert("First name is mendatory.");
		return;
	}
	if(lName == ''){
		jAlert("Last name is mendatory.");
		return;
	}
	if(compName == ''){
		jAlert("Company name is mendatory.");
		return;
	}
	if(email == ''){
		jAlert("Email is mendatory.");
		return;
	}
	
	
	var requestUrl = "${pageContext.request.contextPath}/ecomm/UpdateOrder";
	var dataString = "";
	if(action == 'SAVE'){		
		dataString  = $('#orderForm').serialize()+"&status=${status}&action=SAVE";
	}else if(action == 'UPDATE'){
		dataString = $('#orderForm').serialize()+"&status=${status}&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#orderModal').modal('hide');
				pagingInfo = jsonData.pagingInfo;
				columnFilters = {};
				refreshOrderGridValues(jsonData.orderList);
				clearAllOrderSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ecomm/ManageProdOrders?status="+selectedTab;
}

function sellerSave(action){
	
	var orderProdId = $('#orderProdId').val();
	var orderId = $('#orderId').val();
	var shMethod = $('#shMethod').val();
	var shTrackId = $('#shTrackId').val();	
	var shClass = $('#shClass').val();
	var expDelDate = $('#expDelDate').val();
	var delNote = $('#delNote').val();
	//var prodDelivered = $('#prodDelivered').val();
	/* if(shMethod == ''){
		jAlert("Shipping Method is mendatory.");
		return;
	}
	if(expDelDate == ''){
		jAlert("Expected Delivery Date is mendatory.");
		return;
	} */
	if(shMethod == '' && shTrackId == '' && shClass == '' && expDelDate == '' && delNote == '') {
		jAlert("Please Provide atleast one shipping detail to update.");
		return;
	}
	
	var requestUrl = "${pageContext.request.contextPath}/ecomm/UpdateOrderProductShpipping";
	var dataString = $('#sellerForm').serialize()+"&status=${status}";
	var dataListSize =0;
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#prodhShipModel').modal('hide');
				pagingInfo = jsonData.pagingInfo;
				columnFilters = {};
				dataListSize = JSON.parse(jsonData.prodsList).length;
				refreshSellerGridValues(JSON.parse(jsonData.prodsList));
				clearAllSelections();
			}
			jAlert(jsonData.msg);
			if(jsonData.sts == 1 && dataListSize == 0) {
				getORderGridData(0);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateVoidProduct(action){
	
	var orderProdId = $('#pr_orderProdId').val();
	var orderId = $('#pr_orderId').val();
	var refundAmt = $('#refundAmt').val();
	if(refundAmt == ''){
		jAlert("Refund Amount is mendatory.");
		return;
	}
	var requestUrl = "${pageContext.request.contextPath}/ecomm/VoidOrderProduct";
	var dataString = $('#prodRefundForm').serialize()+"&status=${status}";
	var dataListSize =0;
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#prodRefundModel').modal('hide');
				pagingInfo = jsonData.pagingInfo;
				columnFilters = {};
				dataListSize = JSON.parse(jsonData.prodsList).length;
				refreshSellerGridValues(JSON.parse(jsonData.prodsList));
				clearAllSelections();
			}
			jAlert(jsonData.msg);
			if(jsonData.sts == 1 && dataListSize == 0) {
				getORderGridData(0);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function viewOrderDetails(id,index){
	
		var rowData = orderGrid.getDataItem(id);
		
		$('#ov_orderId').text(checkNull(rowData.orderId));
		$('#ov_orderDate').text(checkNull(rowData.orderDate));
		$('#ov_orderTotal').text(checkNull(rowData.orderDate));
		$('#ov_noOfProducts').text(checkNull(rowData.noOfItems));
		
		$('#ov_pPayType').text(checkNull(rowData.pPayType));
		$('#ov_pPayAmt').text(checkNull(rowData.pPayAmt));
		$('#ov_sPayType').text(checkNull(rowData.sPayType));
		$('#ov_sPayAmt').text(checkNull(rowData.sPayAmt));
		$('#ov_shCharges').text(checkNull(rowData.shCharges));
		$('#ov_tax').text(checkNull(rowData.tax));
		$('#ov_firstName').text(checkNull(rowData.firstName));
		$('#ov_lastName').text(checkNull(rowData.lastName));
		$('#ov_email').text(checkNull(rowData.email));
		$('#ov_phone').text(checkNull(rowData.phone));
		$('#ov_status').text(checkNull(rowData.status));
		$('#ov_shFName').text(checkNullEmpty(rowData.shFName));
		$('#ov_shLName').text(checkNullEmpty(rowData.shLName));
		$('#ov_shPhone').text(checkNullEmpty(rowData.shPhone));
		$('#ov_shEmail').text(checkNullEmpty(rowData.shEmail));
		$('#ov_shAddr1').text(checkNullEmpty(rowData.shAddr1));
		$('#ov_shAddr2').text(checkNullEmpty(rowData.shAddr2));
		$('#ov_shCity').text(checkNullEmpty(rowData.shCity));
		$('#ov_shState').text(checkNullEmpty(rowData.shState));
		$('#ov_shZip').text(checkNullEmpty(rowData.shZip));
		$('#ov_shCountry').text(checkNullEmpty(rowData.shCountry));
		
		$('#viewOrderDtlsModel').modal('show');
}
function checkNull(value) {
	if(value == null || value =="null") {
		return "-";
	} 
	return value;
}
function checkNullEmpty(value) {
	if(value == null || value =="null") {
		return "";
	} 
	return value;
}

function viewProdDetails(id,index){
	
	var rowData = prodSelrGrid.getDataItem(id);
	
	$('#pv_pName').text(checkNull(rowData.pName));
	$('#pv_orderId').text(checkNull(rowData.orderId));
	$('#pv_pDesc').text(checkNull(rowData.pDesc));
	$('#pv_qty').text(checkNull(rowData.qty));
	$('#pv_qtyType').text(checkNull(rowData.qtyType));
	
	$('#pv_fSelPrice').text(checkNull(rowData.fSelPrice));
	$('#pv_vOptNameValCom').text(checkNull(rowData.vOptNameValCom));
	$('#pv_status').text(checkNull(rowData.status));
	$('#pv_pCpnCode').text(checkNull(rowData.pCpnCode));
	$('#pv_custCpnCode').text(checkNull(rowData.custCpnCode));
	$('#pv_shMethod').text(checkNull(rowData.shMethod));
	$('#pv_shClass').text(checkNull(rowData.shClass));
	$('#pv_shippingStatus').text(checkNull(rowData.shippingStatus));
	$('#pv_shTrackId').text(checkNull(rowData.shTrackId));
	$('#pv_delNote').text(checkNull(rowData.delNote));
	$('#pv_expDelDate').text(checkNull(rowData.expDelDate));
	
	$('#pv_firstName').text(checkNull(rowData.firstName));
	$('#pv_lastName').text(checkNull(rowData.lastName));
	$('#pv_email').text(checkNull(rowData.email));
	$('#pv_phone').text(checkNull(rowData.phone));
	$('#pv_shFName').text(checkNullEmpty(rowData.shFName));
	$('#pv_shLName').text(checkNullEmpty(rowData.shLName));
	$('#pv_shPhone').text(checkNullEmpty(rowData.shPhone));
	$('#pv_shEmail').text(checkNullEmpty(rowData.shEmail));
	$('#pv_shAddr1').text(checkNullEmpty(rowData.shAddr1));
	$('#pv_shAddr2').text(checkNullEmpty(rowData.shAddr2));
	$('#pv_shCity').text(checkNullEmpty(rowData.shCity));
	$('#pv_shState').text(checkNullEmpty(rowData.shState));
	$('#pv_shZip').text(checkNullEmpty(rowData.shZip));
	$('#pv_shCountry').text(checkNullEmpty(rowData.shCountry));
	$('#pv_productUrl').text(checkNullEmpty(rowData.productUrl));
	$("#pv_productUrlA").attr("href", checkNullEmpty(rowData.productUrl))
	if(rowData.productUrl == '' || rowData.productUrl == null || rowData.productUrl == undefined){
		$('#pv_productUrl').text("N/A");
		$("#pv_productUrlA").attr("href", "#");
	}
	$('#viewProdDtlsModel').modal('show');
}

</script>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
			<li><i class="fa fa-laptop"></i>Manage Orders</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="newOrderTab" class=""><a id="newOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#newOrder">New Orders</a></li>
				<li id="openOrderTestTab" class=""><a id="openOrderTest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#openOrderTest">Open Orders</a></li>
				<li id="fulfilledOrderTab" class=""><a id="fulfilledOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#fulfilledOrder">Fulfilled Orders</a></li>
				<li id="voidedOrderTab" class=""><a id="voidedOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#voidedOrder">Voided Orders</a></li>
				<li id="rejectedOrderTab" class=""><a id="rejectedOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#rejectedOrder">Rejected Orders</a></li>
				<li id="allOrderTab" class=""><a id="allOrder1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#allOrder">All Orders</a></li>
			</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
		<div id="newOrder" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>New Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcelOrders('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetORderFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="orders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="orders_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
				</c:if>
			</div>
			<div id="openOrderTest" class="tab-pane">
			<c:if test="${status =='ACCEPTED'}">	
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Open Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcelOrders('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetORderFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="orders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="orders_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
				</c:if>
			</div>
			<div id="fulfilledOrder" class="tab-pane">
			<c:if test="${status =='FULFILLED'}">	
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Fulfilled Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcelOrders('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetORderFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="orders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="orders_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
			</c:if>
			</div>
			<div id="voidedOrder" class="tab-pane">
			<c:if test="${status =='VOIDED'}">	
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Voided Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcelOrders('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetORderFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="orders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="orders_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
			</c:if>
			</div>	
			<div id="rejectedOrder" class="tab-pane">
			<c:if test="${status =='REJECTED'}">	
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Rejected Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcelOrders('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetORderFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="orders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="orders_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
			</c:if>
			</div>
			<div id="allOrder" class="tab-pane">
			<c:if test="${status =='ALL'}">	
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>All Orders</label>
							<div class="pull-right">
								<a href="javascript:exportToExcelOrders('ALL')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetORderFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="orders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="orders_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
			</c:if>
			</div>																				
		</div>
	</div>	
</div>
<div id="productsDiv">
	<div class="panel-body1 full-width">
		<div class="tab-content">
				<div class="full-width full-width-btn mb-20">
				<br />
				<br />
				<%-- <c:if test="${status =='ACTIVE'}">	
					<button type="button" class="btn btn-primary" onclick="updateOrderProdStatus('ACCEPTED');">Accept Order Product</button>
					<button type="button" class="btn btn-primary" onclick="updateOrderProdStatus('REJECTED');">Reject Order Product</button>
				</c:if> --%>
				<%-- <c:if test="${status =='ACCEPTED'}">	
					<button type="button" class="btn btn-primary" onclick="updateProdShipment();">Update Shipment</button>
					<c:if test="${sessionScope.isAdmin == true}">
						<button type="button" class="btn btn-primary" onclick="editVoidProduct();">Void Product</button>
					</c:if>
				</c:if> --%>
				</div>
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Order Products</label>
							<div class="pull-right">
								<input type="hidden" id="prod_order_id" name="prod_order_id" />
								<a href="javascript:exportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="sellers_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="sellers_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
		</div>
	</div>
</div>


<!-- Add Contest -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="prodhShipModel" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Order Product</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<form name="sellerForm" id="sellerForm" method="post">
					<input type="hidden" id="orderProdId" name="orderProdId" />
					<input type="hidden" id="shippingStatus" name="shippingStatus" />
					<input type="hidden" id="orderId" name="orderId" />
					<input type="hidden" id="status" name="${status}" />
					
						<div class="form-group col-sm-6 col-xs-6">
							<label>Shipping Method </label>
							 <input class="form-control" type="text" id="shMethod" name="shMethod" placeholder="FEDX,UPS,USPS,OTHER">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Shipping Desc <span class="required">*</span>
							</label> <input class="form-control" type="text" id="shClass" name="shClass" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Shipping Tracking Id <span class="required">*</span>
							</label> <input class="form-control" type="text" id="shTrackId" name="shTrackId" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Delivery Note</span>
							</label> <input class="form-control" type="text" id="delNote" name="delNote" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Expected Delivery Date<span class="required">*</span>
							</label> <input class="form-control" type="text" id="expDelDate" name="expDelDate" >
						</div>
						<%-- <div class="form-group col-sm-6 col-xs-6">
							<label>Product Delivered<span class="required">*</span>
							</label> <input class="form-control" type="checkbox" id="prodDelivered" value="Y" name="prodDelivered" <c:if test="${shippingStatus= 'DELIVERED'}">checked</c:if> >
						</div> --%>
					</form>
				</div>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="sellerSave('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Add Contest -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="prodRefundModel" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Void Product</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<form name="prodRefundForm" id="prodRefundForm" method="post">
					<input type="hidden" id="pr_orderProdId" name="orderProdId" />
					<input type="hidden" id="pr_shippingStatus" name="shippingStatus" />
					<input type="hidden" id="pr_orderId" name="orderId" />
					<input type="hidden" id="pr_status" name="${status}" />
					
						<div class="form-group col-sm-6 col-xs-6">
							<label>Refund Amount </label>
							 <input class="form-control" type="text" id="refundAmt" name="refundAmt" >
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="updateVoidBtn" type="button" onclick="updateVoidProduct()">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- View Order Details -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="viewOrderDtlsModel" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Order Details</h4>
			</div>
			<div class="modal-body full-width">
			<!-- <ul class="overlay_ul_new">
				<li><label >Order Id</label><label id="ov_orderId"></label></li>
				<li><label >Order Date</label><label id="ov_orderDate"></label></li>
				<li><label >Order Total</label><label id="ov_orderTotal"></label></li>
				<li><label >No Of Products</label><label id="ov_noOfProducts"></label></li>
				<li><label >Primary Payment Type</label><label id="ov_orderId"></label></li>
			</ul> -->
			<table style="width:50%">
				<tr><th style="width:30%"></th><th style="width:70%"></th></tr>
			  <tr><td><b>Order Id</b></td><td><span id="ov_orderId"></span></td></tr>
			  <tr><td><b>Order Date</b></td><td><span id="ov_orderDate"></span></td></tr>
			  <tr><td><b>Order Total</b></td><td><span id="ov_orderTotal"></span></td></tr>
			  <tr><td><b>No Of Products</b></td><td><span id="ov_noOfProducts"></span></td></tr>
			  <tr><td><b>Primary Payment Type</b></td><td><span id="ov_pPayType"></span></td></tr>
			  <tr><td><b>Primary Payment Amount</b></td><td><span id="ov_pPayAmt"></span></td></tr>
			  <tr><td><b>Secondary Payment Type</b></td><td><span id="ov_sPayType"></span></td></tr>
			  <tr><td><b>Secondary Payment Amount</b></td><td><span id="ov_sPayAmt"></span></td></tr>
			  <tr><td><b>Shipping Charges</b></td><td><span id="ov_shCharges"></span></td></tr>
			  <tr><td><b>Tax Amount</b></td><td><span id="ov_tax"></span></td></tr>
			  <tr><td><b>First Name</b></td><td><span id="ov_firstName"></span></td></tr>
			  <tr><td><b>Last Name</b></td><td><span id="ov_lastName"></span></td></tr>
			  <tr><td><b>Email</b></td><td><span id="ov_email"></span></td></tr>
			  <tr><td><b>Phone</b></td><td><span id="ov_phone"></span></td></tr>
			  <tr><td><b>Order Status</b></td><td><span id="ov_status"></span></td></tr>
			   <tr><td><b>Shipping Address</b></td><td><span id="ov_shFName"></span> &nbsp;<span id="ov_shLName"></span></td></tr>
			  <tr><td><b></b></td><td><span id="ov_shAddr1"></span>  &nbsp;<span id="ov_shAddr2"></span></td></tr>
			  <tr><td><b></b></td><td><span id="ov_shCity"></span>&nbsp;<span id="ov_shState"></span>&nbsp;<span id="ov_shZip"></span></td></tr>
			  <tr><td><b></b></td><td><span id="ov_shCountry"></span></td></tr>
			  <tr><td><b></b></td><td><span id="ov_shPhone"></span></td></tr>
			  <tr><td><b></b></td><td><span id="ov_shEmail"></span></td></tr>
			  
			 </table>
				
			</div>
			<div class="modal-footer full-width">				
				<!-- <button class="btn btn-primary" id="updateBtn" type="button" onclick="sellerSave('UPDATE')">Update</button> -->
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- View Product Details -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="viewProdDtlsModel" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Product Details</h4>
			</div>
			<div class="modal-body full-width">
			<table style="width:50%">
				<tr><th style="width:30%"></th><th style="width:70%"></th></tr>
			  <tr><td><b>Product Name</b></td><td><span id="pv_pName"></span></td></tr>
			   <tr><td><b>Order Id</b></td><td><span id="pv_orderId"></span></td></tr>
			    <tr><td><b>Shipping Address</b></td><td><span id="pv_shFName"></span> &nbsp;<span id="pv_shLName"></span></td></tr>
			  <tr><td><b></b></td><td><span id="pv_shAddr1"></span>&nbsp;<span id="pv_shAddr2"></span></td></tr>
			  <tr><td><b></b></td><td><span id="pv_shCity"></span>&nbsp;<span id="pv_shState"></span>&nbsp;<span id="pv_shZip"></span></td></tr>
			  <tr><td><b></b></td><td><span id="pv_shCountry"></span></td></tr>
			  <tr><td><b></b></td><td><span id="pv_shPhone"></span></td></tr>
			  <tr><td><b></b></td><td><span id="pv_shEmail"></span></td></tr>
			  <tr><td><b>Product URL</b></td><td><a id="pv_productUrlA" href="" target="_blank"><span id="pv_productUrl"></span></a></td></tr>
			  <tr><td><b>Sold Price</b></td><td><span id="pv_fSelPrice"></span></td></tr>
			  <tr><td><b>Description</b></td><td><span id="pv_pDesc"></span></td></tr>
			  <tr><td><b>Quantity</b></td><td><span id="pv_qty"></span></td></tr>
			  <tr><td><b>Quantity Type</b></td><td><span id="pv_qtyType"></span></td></tr>
			  <tr><td><b>Variant Details</b></td><td><span id="pv_vOptNameValCom"></span></td></tr>
			  <tr><td><b>Status</b></td><td><span id="pv_status"></span></td></tr>
			  <tr><td><b>Product Coupon Code</b></td><td><span id="pv_pCpnCode"></span></td></tr>
			  <tr><td><b>Customer Coupon Code</b></td><td><span id="pv_custCpnCode"></span></td></tr>
			  <tr><td><b>Shipping Method</b></td><td><span id="pv_shMethod"></span></td></tr>
			  <tr><td><b>shipping Description</b></td><td><span id="pv_shClass"></span></td></tr>
			  <tr><td><b>Shipping Status</b></td><td><span id="pv_shippingStatus"></span></td></tr>
			  <tr><td><b>Shipping Tracking Id</b></td><td><span id="pv_shTrackId"></span></td></tr>
			  <tr><td><b>Delivery Notes</b></td><td><span id="pv_delNote"></span></td></tr>
			  <tr><td><b>Expected Delivery Date</b></td><td><span id="pv_expDelDate"></span></td></tr>
			   <tr><td><b>First Name</b></td><td><span id="pv_firstName"></span></td></tr>
			  <tr><td><b>Last Name</b></td><td><span id="pv_lastName"></span></td></tr>
			  <tr><td><b>Email</b></td><td><span id="pv_email"></span></td></tr>
			  <tr><td><b>Phone</b></td><td><span id="pv_phone"></span></td></tr>
			 
			  
			 </table>
				
			</div>
			<div class="modal-footer full-width">				
				<!-- <button class="btn btn-primary" id="updateBtn" type="button" onclick="sellerSave('UPDATE')">Update</button> -->
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Order Fulfillment -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="updProdDeliveyModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Update Product Delivered</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<form name="prodDeliveryUpdate" id="prodDeliveryUpdate" method="post">
					<input type="hidden" id="updOrderProdId" name="updOrderProdId" />
					<input type="hidden" id="updOrderId" name="updOrderId"/>
					<!-- <input type="hidden" id="updIsimgupd" name="updIsimgupd" value="FALSE"/> -->
						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Delivery Date<span class="required">*</span>
							</label> <input class="form-control" type="text" id="delDate" name="delDate" >
						</div>
						<div id="prodImageDiv" class="form-group col-sm-6 col-xs-6">
							<label for="imageFile" class="col-lg-3 control-label">Delivery Image File</label>
                 			<input type="file" id="deliveryImage" name="deliveryImage">
             			</div>
             			<!-- <div class="form-group form-group-top" id="prodImageDispDiv" style="width: 100%">
            				<div class="col-lg-3"> &nbsp; </div>
							<div class="col-lg-6">
	               				 <img id="prodImageDisp" src="" height="150" width="400" />
	               				 <a style="color:blue " href="javascript:callChangeProdImage();">Change Image</a>
           					</div>
							<div class="col-lg-3">&nbsp;</div>
    					</div> -->
					</form>
				</div>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="updateProdToDelivered()">Delivered</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	//Contest Grid
	function getORderGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/ManageProdOrders.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+orderSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshOrderGridValues(jsonData.orderList);
				clearAllOrderSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcelOrders(type){
		var appendData = "headerFilter="+orderSearchString+"&status=";
	    var url = apiServerUrl+"ContestExportToExcel?"+appendData;
	    //$('#download-frame').attr('src', url);
	}
	
	
	function resetORderFilters(){
		orderSearchString='';
		columnFilters = {};
		getORderGridData(0);
		//refreshQuestionGridValues('');
	}

	/* var orderCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var pagingInfo;
	var orderDataView;
	var orderGrid;
	var orderData = [];
	var orderGridPager;
	var orderSearchString='';
	var columnFilters = {};
	var userOrderColumnsStr = '<%=session.getAttribute("custordergrid")%>';
	
	var userOrderColumns = [];
	var loadOrderColumns = ["orderId","orderTotal","orderDate","noOfItems","email", "phone","viewOrders"];
	
	var allOrderColumns = [ 
			{
				id : "orderId",
				name : "Order Id",
				field : "orderId",
				width : 80,
				sortable : true
			},{
				id : "orderTotal",
				name : "Order Total",
				field : "orderTotal",
				width : 80,
				sortable : true
			}, {
				id : "orderDate",
				name : "Order Date",
				field : "orderDate",
				width : 80,
				sortable : true
			},{
				id : "noOfItems",
				name : "No Of Products",
				field : "noOfItems",
				width : 80,
				sortable : true
			},{
				id : "pPayType",
				name : "Primary Payment Type",
				field : "pPayType",
				width : 80,
				sortable : true
			},{
				id : "pPayAmt",
				name : "Primary Payment Amount",
				field : "pPayAmt",
				width : 80,
				sortable : true
			},{
				id : "sPayType",
				name : "Secondary Payment Type",
				field : "sPayType",
				width : 80,
				sortable : true
			},{
				id : "sPayAmt",
				name : "Secondary Payment Amount",
				field : "sPayAmt",
				width : 80,
				sortable : true
			},{
				id : "shCharges",
				name : "Shipping Charges",
				field : "shCharges",
				width : 80,
				sortable : true
			},{
				id : "tax",
				name : "Tax Amt",
				field : "tax",
				width : 80,
				sortable : true
			},{
				id : "firstName",
				name : "Customer First Name",
				field : "firstName",
				width : 80,
				sortable : true
			}, {
				id : "lastName",
				name : "Customer Last Name",
				field : "lastName",
				width : 80,
				sortable : true
			},{
				id : "email",
				name : "Customer Email",
				field : "email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				name : "Customer Phone",
				field : "phone",
				width : 80,
				sortable : true
			}/* ,{
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			} */,{
				id : "shAddr1",
				name : "Address1",
				field : "shAddr1",
				width : 80,
				sortable : true
			},{
				id : "shAddr2",
				name : "Address2",
				field : "shAddr2",
				width : 80,
				sortable : true
			},{
				id : "shCity",
				name : "City",
				field : "shCity",
				width : 80,
				sortable : true
			},{
				id : "shState",
				name : "State",
				field : "shState",
				width : 80,
				sortable : true
			},{
				id : "shCountry",
				name : "Country",
				field : "shCountry",
				width : 80,
				sortable : true
			},{
				id : "shZip",
				name : "Zip Code",
				field : "shZip",
				width : 80,
				sortable : true
			},{
				id : "shFName",
				name : "First Name",
				field : "shFName",
				width : 80,
				sortable : true
			},{
				id : "shLName",
				name : "Last Name",
				field : "shLName",
				width : 80,
				sortable : true
			},{
				id : "shPhone",
				name : "Phone",
				field : "shPhone",
				width : 80,
				sortable : true
			},{
				id : "shEmail",
				name : "Email",
				field : "shEmail",
				width : 80,
				sortable : true
			},{
				id : "viewOrders",
				name : "View",
				field : "viewOrders",
				width : 80,
				formatter : orderViewLinkFormatter
			}];

	if (userOrderColumnsStr != 'null' && userOrderColumnsStr != '') {
		columnOrder = userOrderColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allOrderColumns.length; j++) {
				if (columnWidth[0] == allOrderColumns[j].id) {
					userOrderColumns[i] = allOrderColumns[j];
					userOrderColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//userOrderColumns = allOrderColumns;
		var columnOrder = loadOrderColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allOrderColumns.length;j++){
				if(columnWidth == allOrderColumns[j].id){
					userOrderColumns[i] = allOrderColumns[j];
					userOrderColumns[i].width=80;
					break;
				}
			}			
		}
	} 

	function orderViewLinkFormatter(row, cell, value, columnDef, dataContext) {
		//the id is so that you can identify the row when the particular button is clicked
		/* var button = "<input class='edit' value='Edit' type='button' id='"+ dataContext.id +"' />"; */
		var link = "<img class='vwOrderClickableImage' style='height:17px;' src='../resources/images/viewIcon.png' onclick='viewOrderDetails("
				+ row
				+ ","
				+ row + ")'/>";
		return link;
	}

	var orderOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var orderGridSortcol = "orderId";
	var orderGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function orderGridComparer(a, b) {
		var x = a[orderGridSortcol], y = b[orderGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshOrderGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		orderData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (orderData[i] = {});
				
				d["id"] = i;
				d["orderId"] = data.id;
				d["orderTotal"] = data.netTotStr;
				d["orderDate"] = data.crDateTimeStr;
				d["noOfItems"] = data.noOfItems;
				d["pPayType"] = data.pPayType;
				d["pPayAmt"] = data.pPayAmt;
				d["sPayType"] = data.sPayType;
				d["sPayAmt"] = data.sPayAmt;
				d["shCharges"] = data.shCharges;
				d["tax"] = data.tax;
				d["firstName"] = data.custFirstName;
				d["lastName"] = data.custLastName;
				d["email"] = data.custEmail;
				d["phone"] = data.custPhone;
				d["status"] = data.status;
				
				d["shAddr1"] = data.shAddr1;
				d["shAddr2"] = data.shAddr2;
				d["shCity"] = data.shCity;
				d["shState"] = data.shState;
				d["shCountry"] = data.shCountry;
				d["shZip"] = data.shZip;
				d["shFName"] = data.shFName;
				d["shLName"] = data.shLName;
				d["shPhone"] = data.shPhone;
				d["shEmail"] = data.shEmail;
				
			}
		}

		orderDataView = new Slick.Data.DataView();
		orderGrid = new Slick.Grid("#orders_grid", orderDataView,
				userOrderColumns, orderOptions);
		orderGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		orderGrid.setSelectionModel(new Slick.RowSelectionModel());
		//orderGrid.registerPlugin(orderCheckboxSelector);
		
			orderGridPager = new Slick.Controls.Pager(orderDataView,
					orderGrid, $("#orders_pager"),
					pagingInfo);
		var orderGridColumnpicker = new Slick.Controls.ColumnPicker(
				allOrderColumns, orderGrid, orderOptions);
					
		orderGrid.onSort.subscribe(function(e, args) {
			orderGridSortdir = args.sortAsc ? 1 : -1;
			orderGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				orderDataView.fastSort(orderGridSortcol, args.sortAsc);
			} else {
				orderDataView.sort(orderGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the orderGrid
		orderDataView.onRowCountChanged.subscribe(function(e, args) {
			orderGrid.updateRowCount();
			orderGrid.render();
		});
		orderDataView.onRowsChanged.subscribe(function(e, args) {
			orderGrid.invalidateRows(args.rows);
			orderGrid.render();
		});
		$(orderGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							orderSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											orderSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getORderGridData(0);
								}
							}

						});
		orderGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editOrder' && args.column.id != 'delOrder'){
					if(args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		orderGrid.init();
		
		var orderRowIndex = -1;
		orderGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempOrderRowIndex = orderGrid.getSelectedRows([0])[0];
			if (tempOrderRowIndex != orderRowIndex) {
				var orderId = orderGrid.getDataItem(tempOrderRowIndex).orderId;
				$('#prod_order_id').val(orderId);
				getSellerGridData(orderId,0);
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		orderDataView.beginUpdate();
		orderDataView.setItems(orderData);
		//orderDataView.setFilter(filter);
		orderDataView.endUpdate();
		orderDataView.syncGridSelection(orderGrid, true);
		orderGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveOrderPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = orderGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('custordergrid', colStr);
	}
	
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(pagingInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(pagingInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(pagingInfo.pageNum) - 1;
		}
		getORderGridData(pageNo);
	}
	
			
	function clearAllOrderSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
/* order grids ends */
	
	
/* products grid starts  */


function getSellerGridData(orderId,pageNo) {
	var sellerId = "";
	var userId = "";
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/GetCustOrderProducts.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+prodSelrSearchString+"&status=${status}&orderId="+orderId+"&sellerId="+sellerId,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				prodPagInfo = jsonData.pagingInfo;
				refreshSellerGridValues(jsonData.prodsList);
				$('#sellers_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveSellerPreference()'>");
				clearAllSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcel(type){
		var appendData = "headerFilter="+prodSelrSearchString+"&status=";
	    var url = apiServerUrl+"ContestExportToExcel?"+appendData;
	    //$('#download-frame').attr('src', url);
	}
	
	
	function resetFilters(){
		prodSelrSearchString='';
		prodSlrclmFilters = {};
		var orderId = $('#prod_order_id').val();
		getSellerGridData(orderId,0);
		//refreshQuestionGridValues('');
	}

	/* var sellerCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var prodPagInfo;
	var prodSelDataView;
	var prodSelrGrid;
	var prodSelrData = [];
	var prodSelrGridPager;
	var prodSelrSearchString='';
	var prodSlrclmFilters = {};
	var prodUserSelrColumnsStr = '<%=session.getAttribute("custorderprodgrid")%>';

	var prodUserSelerColumns = [];
	var prodLoadSelerColumns = ["pName", "qty","firstName","lastName","email","viewProds","action"];
	var allSellerColumns = [ 
			{
				id : "pName",
				name : "Product Name",
				field : "pName",
				width : 80,
				sortable : true
			},{
				id : "pDesc",
				name : "Description",
				field : "pDesc",
				width : 80,
				sortable : true
			}, {
				id : "qty",
				name : "Quantity",
				field : "qty",
				width : 40,
				sortable : true
			},{
				id : "qtyType",
				name : "Quantity Type",
				field : "qtyType",
				width : 40,
				sortable : true
			},{
				id : "fSelPrice",
				name : "Sold Price",
				field : "fSelPrice",
				width : 80,
				sortable : true
			},{
				id : "vOptNameValCom",
				name : "Variant Details",
				field : "vOptNameValCom",
				width : 80,
				sortable : true
			},{
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			},{
				id : "pCpnCode",
				name : "Product Coupon Code",
				field : "pCpnCode",
				width : 80,
				sortable : true
			},{
				id : "custCpnCode",
				name : "Customer Coupon Code",
				field : "custCpnCode",
				width : 80,
				sortable : true
			},{
				id : "shMethod",
				name : "Shipping Method",
				field : "shMethod",
				width : 80,
				sortable : true
			},{
				id : "shClass",
				name : "shipping Desc",
				field : "shClass",
				width : 80,
				sortable : true
			},{
				id : "shippingStatus",
				name : "Shipping Status",
				field : "shippingStatus",
				width : 80,
				sortable : true
			},{
				id : "shTrackId",
				name : "Shipping Tracking Id",
				field : "shTrackId",
				width : 80,
				sortable : true
			}, {
				id : "delNote",
				name : "Delivery Notes",
				field : "delNote",
				width : 80,
				sortable : true
			}, {
				id : "expDelDate",
				name : "Expected Delivery Date",
				field : "expDelDate",
				width : 80,
				sortable : true
			},{
				id : "firstName",
				name : "Customer First Name",
				field : "firstName",
				width : 80,
				sortable : true
			}, {
				id : "lastName",
				name : "Customer Last Name",
				field : "lastName",
				width : 80,
				sortable : true
			},{
				id : "email",
				name : "Customer Email",
				field : "email",
				width : 80,
				sortable : true
			},{
				id : "shFName",
				name : "First Name",
				field : "shFName",
				width : 80,
				sortable : true
			},{
				id : "shLName",
				name : "Last Name",
				field : "shLName",
				width : 80,
				sortable : true
			},{
				id : "phone",
				name : "Customer Phone",
				field : "phone",
				width : 80,
				sortable : true
			},{
				id : "shAddr1",
				name : "Address1",
				field : "shAddr1",
				width : 80,
				sortable : true
			},{
				id : "shAddr2",
				name : "Address2",
				field : "shAddr2",
				width : 80,
				sortable : true
			},{
				id : "shCity",
				name : "City",
				field : "shCity",
				width : 80,
				sortable : true
			},{
				id : "shState",
				name : "State",
				field : "shState",
				width : 80,
				sortable : true
			},{
				id : "shCountry",
				name : "Country",
				field : "shCountry",
				width : 80,
				sortable : true
			},{
				id : "shZip",
				name : "Zip Code",
				field : "shZip",
				width : 80,
				sortable : true
			},{
				id : "shPhone",
				name : "Phone",
				field : "shPhone",
				width : 80,
				sortable : true
			},{
				id : "shEmail",
				name : "Email",
				field : "shEmail",
				width : 80,
				sortable : true
			},{
				id : "viewProds",
				name : "View",
				field : "viewProds",
				width : 40,
				formatter : prodViewLinkFormatter
			},{
				id : "action",
				name : "Action",
				field : "action",
				width : 280,
				formatter:buttonProdFormatter
			}];

	if (prodUserSelrColumnsStr != 'null' && prodUserSelrColumnsStr != '') {
		columnOrder = prodUserSelrColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allSellerColumns.length; j++) {
				if (columnWidth[0] == allSellerColumns[j].id) {
					prodUserSelerColumns[i] = allSellerColumns[j];
					prodUserSelerColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//prodUserSelerColumns = allSellerColumns;
		var columnOrder = prodLoadSelerColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allSellerColumns.length;j++){
				if(columnWidth == allSellerColumns[j].id){
					prodUserSelerColumns[i] = allSellerColumns[j];
					prodUserSelerColumns[i].width=80;
					break;
				}
			}			
		}
	}

	function prodViewLinkFormatter(row, cell, value, columnDef, dataContext) {
		//the id is so that you can identify the row when the particular button is clicked
		/* var button = "<input class='edit' value='Edit' type='button' id='"+ dataContext.id +"' />"; */
		var link = "<img class='vwOrderClickableImage' style='height:17px;' src='../resources/images/viewIcon.png' onclick='viewProdDetails("
				+ row
				+ ","
				+ row + ")'/>";
		return link;
	}
	function buttonProdFormatter(row, cell, value, columnDef, dataContext) {
		var status = '${status}';
		var button = "";	
		if(status == 'ACTIVE') {
			button ="<div class=\"full-width full-width-btn mb-10\"><button type=\"button\" class=\"btn btn-primary\"  onclick=\"updateOrderProdStatus('ACCEPTED','"+ dataContext.orderProdId +"','"+ dataContext.orderId +"');\">Accept</button>"+
			"<button type=\"button\" class=\"btn btn-danger\"  onclick=\"updateOrderProdStatus('REJECTED','"+ dataContext.orderProdId +"','"+ dataContext.orderId +"');\">Reject</button></div>";	
		} else if(status == 'ACCEPTED') {
			button ="<div class=\"full-width full-width-btn mb-10\"><button type=\"button\" class=\"btn btn-primary\"  onclick=\"updateProdShipment('"+ dataContext.orderProdId +"');\">Update Shipment</button>"+
			" <button type=\"button\" class=\"btn btn-primary\"  onclick=\"loadUpdateDeliveryPopup('FULFILLED','"+ dataContext.orderProdId +"','"+ dataContext.orderId +"');\">Update to Delivered</button>"+
			" <button type=\"button\" class=\"btn btn-primary\"  onclick=\"updateOrderProdStatus('ACCEPT_TO_ACTIVE','"+ dataContext.orderProdId +"','"+ dataContext.orderId +"');\">Move to New</button>";
			if('${sessionScope.isAdmin}') {
				button +="<button type=\"button\" class=\"btn btn-danger\"  onclick=\"editVoidProduct('"+ dataContext.orderProdId +"');\">Void</button>";	
			}
			button += "</div>";
			
		} else if(status == 'FULFILLED') {
			button ="<div class=\"full-width full-width-btn mb-10\"><button type=\"button\" class=\"btn btn-primary\"  onclick=\"updateOrderProdStatus('FULFILL_TO_ACCEPT','"+ dataContext.orderProdId +"','"+ dataContext.orderId +"');\">Move to Open</button>";
			if('${sessionScope.isAdmin}') {
				button +="<button type=\"button\" class=\"btn btn-danger\"  onclick=\"editVoidProduct('"+ dataContext.orderProdId +"');\">Void</button>";	
			}
			button += "</div>";
			
		} else if(status == 'REJECTED') {
			if('${sessionScope.isAdmin}') {
				button ="<div class=\"full-width full-width-btn mb-10\"><button type=\"button\" class=\"btn btn-danger\"  onclick=\"editVoidProduct('"+ dataContext.orderProdId +"');\">Void</button></div>";
			}
		}  
		 
		
		return button;
	}
	
	var sellerOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var prodSelrGridSortcol = "sellerId";
	var prodSelrGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function prodSelrGridComparer(a, b) {
		var x = a[prodSelrGridSortcol], y = b[prodSelrGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshSellerGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		prodSelrData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (prodSelrData[i] = {});
				d["id"] = i;
				d["orderProdId"] = data.id;
				d["orderId"] = data.orderId;
				d["pName"] = data.pName;
				d["pDesc"] = data.pDesc;
				d["qty"] = data.qty;
				d["qtyType"] = data.qtyType;
				d["fSelPrice"] = data.fSelPrice;
				d["vOptNameValCom"] = data.vOptNameValCom;
				d["tax"] = data.tax;
				d["pCpnCode"] = data.pCpnCode;
				d["shMethod"] = data.shMethod;
				d["shClass"] = data.shClass;
				d["shTrackId"] = data.shTrackId;
				d["custCpnCode"] = data.custCpnCode;
				d["shippingStatus"] = data.shippingStatus;
				d["delNote"] = data.delNote;
				d["expDelDate"] = data.expDelDateStr;
				d["status"] = data.status;
				
				d["firstName"] = data.custFirstName;
				d["lastName"] = data.custLastName;
				d["email"] = data.custEmail;
				d["phone"] = data.custPhone;
				
				d["shAddr1"] = data.shAddr1;
				d["shAddr2"] = data.shAddr2;
				d["shCity"] = data.shCity;
				d["shState"] = data.shState;
				d["shCountry"] = data.shCountry;
				d["shZip"] = data.shZip;
				d["shFName"] = data.shFName;
				d["shLName"] = data.shLName;
				d["shPhone"] = data.shPhone;
				d["shEmail"] = data.shEmail;
				d["productUrl"] = data.productUrl;
			}
		}

		prodSelDataView = new Slick.Data.DataView();
		prodSelrGrid = new Slick.Grid("#sellers_grid", prodSelDataView,
				prodUserSelerColumns, sellerOptions);
		prodSelrGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		prodSelrGrid.setSelectionModel(new Slick.RowSelectionModel());
		//prodSelrGrid.registerPlugin(sellerCheckboxSelector);
		
			prodSelrGridPager = new Slick.Controls.Pager(prodSelDataView,
					prodSelrGrid, $("#sellers_pager"),
					prodPagInfo);
		var prodSelrGridColumnpicker = new Slick.Controls.ColumnPicker(
				allSellerColumns, prodSelrGrid, sellerOptions);
					
		prodSelrGrid.onSort.subscribe(function(e, args) {
			prodSelrGridSortdir = args.sortAsc ? 1 : -1;
			prodSelrGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				prodSelDataView.fastSort(prodSelrGridSortcol, args.sortAsc);
			} else {
				prodSelDataView.sort(prodSelrGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the prodSelrGrid
		prodSelDataView.onRowCountChanged.subscribe(function(e, args) {
			prodSelrGrid.updateRowCount();
			prodSelrGrid.render();
		});
		prodSelDataView.onRowsChanged.subscribe(function(e, args) {
			prodSelrGrid.invalidateRows(args.rows);
			prodSelrGrid.render();
		});
		$(prodSelrGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							prodSelrSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								prodSlrclmFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in prodSlrclmFilters) {
										if (columnId !== undefined
												&& prodSlrclmFilters[columnId] !== "") {
											prodSelrSearchString += columnId
													+ ":"
													+ prodSlrclmFilters[columnId]
													+ ",";
										}
									}
									var orderId = $('#prod_order_id').val();
									getSellerGridData(orderId,0);
								}
							}

						});
		/* prodSelrGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editProd' && args.column.id != 'delSeller'){
					if(args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(prodSlrclmFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(prodSlrclmFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		}); */
		prodSelrGrid.init();
		
		var sellerRowIndex = -1;
		prodSelrGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempSellerRowIndex = prodSelrGrid.getSelectedRows([0])[0];
		});
		// initialize the model after all the discountCodes have been hooked up
		prodSelDataView.beginUpdate();
		prodSelDataView.setItems(prodSelrData);
		//prodSelDataView.setFilter(filter);
		prodSelDataView.endUpdate();
		prodSelDataView.syncGridSelection(prodSelrGrid, true);
		prodSelrGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveSellerPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = prodSelrGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('custorderprodgrid', colStr);
	}
	
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(prodPagInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(prodPagInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(prodPagInfo.pageNum) - 1;
		}
		var orderId = $('#prod_order_id').val();
		getSellerGridData(orderId,pageNo);
	}
	
			
	//Edit Contest
	function updateProdShipment(orderProdId){
		//var tempSellerRowIndex = prodSelrGrid.getSelectedRows([0])[0];
		if (orderProdId == null) {
			jAlert("Plese select Order Product to Update", "info");
			return false;
		}else {
			//var sellerId = prodSelrGrid.getDataItem(tempSellerRowIndex).orderProdId;
			getEditSeller(orderProdId);
		}
	}
	function editVoidProduct(orderProdId) {
		//var tempSellerRowIndex = prodSelrGrid.getSelectedRows([0])[0];
		if (orderProdId == null) {
			jAlert("Plese select Order Product to Void", "info");
			return false;
		}else {
			//var sellerId = prodSelrGrid.getDataItem(tempSellerRowIndex).orderProdId;
			getEditVoidProduct(orderProdId);
		}
	}
	function getEditVoidProduct(orderProdId){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/GetOrderProduct",
			type : "post",
			dataType: "json",
			data: "orderProdId="+orderProdId+"&action=EDIT&status=${status}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					$('#prodRefundModel').modal('show');
					setEditVoidProduct(jsonData);
				}else{
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function setEditVoidProduct(data){
		$('#updateVoidBtn').show();
		var product = JSON.parse(data.product);
		$('#pr_orderProdId').val(product.id);
		$('#pr_orderId').val(product.orderId);
		$('#refundAmt').val(product.refundAmt);	
	}
	
	
	
	function getEditSeller(orderProdId){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/GetOrderProduct",
			type : "post",
			dataType: "json",
			data: "orderProdId="+orderProdId+"&action=EDIT&status=${status}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					$('#prodhShipModel').modal('show');
					setEditSeller(jsonData);
				}else{
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function setEditSeller(data){
		$('#updateBtn').show();
		var product = JSON.parse(data.product);
		$('#orderProdId').val(product.id);
		$('#orderId').val(product.orderId);
		$('#shMethod').val(product.shMethod);
		$('#shTrackId').val(product.shTrackId);
		$('#shClass').val(product.shClass);
		$('#expDelDate').val(product.expDelDateStr);	
		$('#delNote').val(product.delNote);	
	}
	
	function loadUpdateDeliveryPopup(action,orderProdId,orderId) {
		$('#updOrderProdId').val(orderProdId);
		$('#updOrderId').val(orderId);
		$('#updProdDeliveyModal').modal('show');
		
		$('#updOrderId').val(orderId);
	}
	function updateProdToDelivered() {
		
		var delDate = $('#delDate').val();
		if(delDate == null || delDate == '' || delDate == undefined){
			jAlert("Delivery date is mendatory to mark order as delivered");
			return;
		}
		
		
		var confirmMsg = "Are you sure to Update Product as Delivered to Customer.?";
		jConfirm(confirmMsg,"Confirm",function(r){
			if (r) {
				var dataListSize = 0;
				var form = $('#prodDeliveryUpdate')[0];
				var dataString = new FormData(form);
				var orderProdId = $('#updOrderProdId').val();
				var orderId = $('#updOrderId').val();
				dataString.append('orderProdId',orderProdId);
				dataString.append('orderId',orderId);
				dataString.append('action','FULFILLED');
				dataString.append('delDate',delDate);
				if(document.getElementById("deliveryImage").value != "") {
					dataString.append('isimgupd','TRUE');
				}else{
					dataString.append('isimgupd','FALSE');
				}
				
				$.ajax({
						url : "${pageContext.request.contextPath}/ecomm/UpdateOrderProductToDelivered",
						type : "post",
						dataType: "json",
						enctype:"multipart/form-data",
						processData : false,
						contentType : false,
						cache : false,
						data: dataString,
						//data : "orderProdId="+orderProdId+"&orderId="+orderId+"&action="+action+"&status=${status}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.sts == 1){
								$('#updProdDeliveyModal').modal('hide');
								columnFilters = {};
								pagingInfo = jsonData.pagingInfo;
								dataListSize = JSON.parse(jsonData.prodsList).length;
								refreshSellerGridValues(JSON.parse(jsonData.prodsList));
								clearAllSelections();	
							}
							jAlert(jsonData.msg);
							if(jsonData.sts == 1 && dataListSize == 0) {
								getORderGridData(0);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
			} else {
				return false;
			}
		});
		
	}
	
	//Delete Contest
	function updateOrderProdStatus(action,orderProdId,orderId) {		
		//var tempSellerRowIndex = prodSelrGrid.getSelectedRows([0])[0];
		var actionMsg = "";
		var confirmMsg = "";
		if(action == 'ACCEPTED') {
			
			actionMsg = "select Product to Accept";
			confirmMsg = "Are you sure to Accept a product Order?";
			
		} else if(action == 'REJECTED'){
			
			actionMsg = "select Product to Reject";
			confirmMsg = "Are you sure to Reject a product Order?";
			
		} else if(action == 'FULFILLED'){
			
			actionMsg = "select Product to Update Fulfilled";
			confirmMsg = "Are you sure to Update Product as Delivered to Customer.?";
			
		} else if(action == 'FULFILL_TO_ACCEPT'){
			
			actionMsg = "select product to move from Fulfilled to Accepted.";
			confirmMsg = "Are you sure to Move Product from Fulfilled from Open.?";
			
		} else if(action == 'ACCEPT_TO_ACTIVE'){
			
			actionMsg = "select product to move from Accepted to New.";
			confirmMsg = "Are you sure to Move Product from Open from New.?";
		}
		if (orderProdId == null) {
			var alerTMsg = "Plese select Product to "+actionMsg;
			jAlert(alerTMsg, "info");
			return false;
		}else {
			//var orderProdId = prodSelrGrid.getDataItem(tempSellerRowIndex).orderProdId;
			//var orderId = prodSelrGrid.getDataItem(tempSellerRowIndex).orderId;
			//getDeleteSeller(sellerId);
			jConfirm(confirmMsg,"Confirm",function(r){
				if (r) {
					var dataListSize = 0;
					$.ajax({
							url : "${pageContext.request.contextPath}/ecomm/UpdateOrderProductStatus",
							type : "post",
							dataType: "json",
							data : "orderProdId="+orderProdId+"&orderId="+orderId+"&action="+action+"&status=${status}",
							success : function(response) {
								var jsonData = JSON.parse(JSON.stringify(response));
								if(jsonData.sts == 1){
									columnFilters = {};
									pagingInfo = jsonData.pagingInfo;
									dataListSize = JSON.parse(jsonData.prodsList).length;
									refreshSellerGridValues(JSON.parse(jsonData.prodsList));
									clearAllSelections();	
								}
								jAlert(jsonData.msg);
								if(jsonData.sts == 1 && dataListSize == 0) {
									getORderGridData(0);
								}
							},
							error : function(error) {
								jAlert("Your login session is expired please refresh page and login again.", "Error");
								return false;
							}
						});
				} else {
					return false;
				}
			});
			
		}
	}
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	//call functions once page loaded
	window.onload = function() {
		prodPagInfo = JSON.parse(JSON.stringify(${prodPagInfo}));
		refreshSellerGridValues(JSON.parse(JSON.stringify(${sellerList})));
		$('#sellers_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveSellerPreference()'>");
		enableMenu();
	};
		

/* product grid ends  */
//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshOrderGridValues(JSON.parse(JSON.stringify(${orderList})));
		$('#orders_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveOrderPreference()'>");
		enableMenu();
	};

		
</script>