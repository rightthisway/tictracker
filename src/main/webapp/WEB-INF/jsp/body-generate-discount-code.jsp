<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
.promoAll{
	margin-right:5px;
	background: #87ceeb;
	font-size: 10pt;
	height:20px;
}
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}
#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var j = 0;
var promoAutoGenerate;
var promoFlatDiscount;
var varSelectedList = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	/*$('#fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});

	$('#toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});*/
	
	$('#promo_fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});

	$('#promo_toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$('input[type=radio][name=promoAutoGenerate]').change(function(){
		promoAutoGenerate = this.value;
		if (this.value == 'Yes') {
            //$('#promoCodeDiv').hide();
			$("#promo_code").attr("disabled", "disabled");
        }
        else if (this.value == 'No') {
			//$('#promoCodeDiv').show();
			$("#promo_code").removeAttr("disabled");
        }
	});
	
	$('input[type=radio][name=promoFlatDiscount]').change(function(){
		promoFlatDiscount = this.value;		
		if (this.value == 'Yes') {
            $('#dollarSpan').show();
			$('#percSpan').hide();
			$('#mobileDollarSpan').show();
			$('#mobilePercSpan').hide();
			//$('#promoOrderDiv').show();
			$("#promo_orderThreshold").removeAttr("disabled");
        }
        else if (this.value == 'No') {
			$('#dollarSpan').hide();
			$('#percSpan').show();
			$('#mobileDollarSpan').hide();
			$('#mobilePercSpan').show();
			//$('#promoOrderDiv').hide();
			$("#promo_orderThreshold").attr("disabled", "disabled");
        }
	});
	
	
	var promoType;
	jq2('#promo_artistVenue').autocomplete("AutoCompleteArtistAndEventAndVenueAndCategory", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			if(varSelectedList.indexOf(row[0]+"_"+row[1]) > 0){
				return "";
			}
			if(row[0] == 'ALL'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='PARENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='GRAND'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='EVENT'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[3] ;
			}
		}
	}).result(function (event,row,formatted){
		varSelectedList = varSelectedList +","+ row[0] + "_" + row[1];		
		if(row[0] == "ALL"){
			$('#promoTypeId').val(row[1]);
			promoType = "<input type='hidden' name='promoTypeId_"+j+"' id='promoTypeId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=="ARTIST"){
			$('#artistId').val(row[1]);
			promoType = "<input type='hidden' name='artistId_"+j+"' id='artistId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=="EVENT"){
			$('#eventId').val(row[1]);
			promoType = "<input type='hidden' name='eventId_"+j+"' id='eventId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=='VENUE'){
			$('#venueId').val(row[1]);
			promoType = "<input type='hidden' name='venueId_"+j+"' id='venueId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=='PARENT'){
			$('#parentId').val(row[1]);
			promoType = "<input type='hidden' name='parentId_"+j+"' id='parentId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=='CHILD'){
			$('#childId').val(row[1]);
			promoType = "<input type='hidden' name='childId_"+j+"' id='childId_"+j+"' value="+row[1]+" />";
		}else if(row[0]=='GRAND'){
			$('#grandChildId').val(row[1]);
			promoType = "<input type='hidden' name='grandChildId_"+j+"' id='grandChildId_"+j+"' value="+row[1]+" />";
		}
		promoType += "<input type='hidden' name='artistVenueName_"+j+"' id='artistVenueName_"+j+"' value="+row[2]+" />";
		$('#promo_artistVenue').val("");
		$('#promoCategory').append($('.block:last').before('<div class="block promoAll">'+row[0]+' : '+row[2]+' '+promoType+'<span class="remove"><a href="javascript:removeItem('+j+')">X</a></span></div>'));
		//$('.block:last').before('<div class="block">'+row[0]+' : '+row[2]+' '+promoType+'<span class="remove" onclick="removeItem('+j+')"><button type="button" class="btn btn-primary">Remove</button></span></div>');
		//$('#promoSelectedItem').append(row[2]);
		$('#artistVenueName').val(row[2]);
		//$('#resetLink').show();
		j++;
	});
	
	$('body').on('click', '.remove', function () {
		$(this).parent('.block').remove();
	});
});

function resetModal(){
	$('#promo_code').val('');
	$('#promo_fromDate').val('');
	$('#promo_toDate').val('');
	$('#promo_maxOrders').val('');
	$('#promo_discountPerc').val('');
	$('#promo_mobileDiscountPerc').val('');
	$('#promo_orderThreshold').val('');
	$('#promoCategory').empty();
	
	$('#promo_ag_yes').show();
	if ($('input[type=radio][name=promoAutoGenerate]').is(':checked')) {
		$('#promo_autoGenerate_yes').prop('checked',false);
		$('#promo_autoGenerate_no').prop('checked',false);
		$("#promo_code").removeAttr("disabled");
	}
	if ($('input[type=radio][name=promoFlatDiscount]').is(':checked')) {
		$('#promo_flatDiscount_yes').prop('checked',false);
		$('#promo_flatDiscount_no').prop('checked',false);
		$("#promo_orderThreshold").removeAttr("disabled");
	}
	j=0;
	$('#saveBtn').show();
	$('#updateBtn').hide();
	varSelectedList = '';
}

/*	
function resetItem(){
	$('#resetLink').hide();
	$('#artistVenue').val("");
	$('#selectedItem').text("");
	$('#artistVenueName').val("");
	$('#artistId').val("");
	$('#venueId').val("");
	$('#parentId').val("");
	$('#childId').val("");
	$('#grandChildId').val("");	
}

function saveDiscountCode(){
	var parentId = $('#parentId').val();
	var childId = $('#childId').val();
	var grandChildId = $('#grandChildId').val();
	var artistId = $('#artistId').val();
	var venueId = $('#venueId').val();
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	var maxOrders = $('#maxOrder').val();
	var discountPerc = $('#discountPerc').val();
	
	if(parentId == '' && childId == '' && grandChildId== '' && artistId == '' && venueId== ''){
		jAlert("Please select one from Artist,venue,parent,child,grand child from autocomplete to generate discount code.");
		return;
	}
	if(fromDate == '' || toDate == ''){
		jAlert("Start Date and End Date is Mendatory.");
		return;
	}
	
	if(discountPerc == ''){
		jAlert("Please add discount percentage.");
		return;
	}
	
	if(maxOrders == ''){
		jAlert("Max. Orders is Mendatory.");
		return;
	}
	
	$.ajax({
		url : "${pageContext.request.contextPath}/Admin/SaveDiscountCode",
		type : "post",
		dataType:"json",
		data:$('#discountCodeForm').serialize(),
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				pagingInfo = jsonData.discountCodePagingInfo;
				refreshDiscountCodeGridValues(jsonData.discountCodeList);
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});	
}*/

function updateOffer(status){
	var discountCodeIds = getSelectedDiscountCodeGridId();
	if(discountCodeIds == '' || discountCodeIds == undefined){
		jAlert("Please select Records to update.");
		return;
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/Client/UpdateDiscountCodeStatus",
		type : "post",
		dataType:"json",
		data:"status="+status+"&ids="+discountCodeIds,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				getDiscountCodeGridData(0);
				refreshPromoOfferDtlGridValues('');
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function getDiscountCodeGridData(pageNo){
	$.ajax({
		url : "${pageContext.request.contextPath}/Client/GenerateDiscountCode.json",
		type : "post",
		dataType : "json",
		data : "pageNo="+pageNo+"&headerFilter="+discountCodeSearchString+"&sortingString="+sortingString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				pagingInfo = jsonData.discountCodePagingInfo;
				refreshDiscountCodeGridValues(jsonData.discountCodeList);
				clearAllSelections();				
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function removeItem(valu){
	var splitSeletedList = new Array();
	splitSeletedList = varSelectedList.split(",");
	for(var n=0; n<j; n++){
		if(valu == n){
			varSelectedList = varSelectedList.replace(splitSeletedList[n+1], " ");
		}
	}

	$('#artistVenue').val("");
	$('#artistVenueName').val("");
	$('#artistId_'+valu).val("");
	$('#eventId_'+valu).val("");
	$('#venueId_'+valu).val("");
	$('#parentId_'+valu).val("");
	$('#childId_'+valu).val("");
	$('#grandChildId_'+valu).val("");
}

function promoSaveDiscountCode(action){
	var count = 0;
	var promoTypeFlag = false; 
	var flag = false;
	
	/*var parentId = $('#parentId').val();
	var childId = $('#childId').val();
	var grandChildId = $('#grandChildId').val();
	var artistId = $('#artistId').val();
	var venueId = $('#venueId').val();*/
	
	var promoCode = $('#promo_code').val();
	var fromDate = $('#promo_fromDate').val();
	var toDate = $('#promo_toDate').val();
	var maxOrders = $('#promo_maxOrders').val();
	var discountPerc = $('#promo_discountPerc').val();
	var mobileDiscountPerc = $('#promo_mobileDiscountPerc').val();
	var orderThreshold = $('#promo_orderThreshold').val();
	
	if(promoAutoGenerate == 'Yes'){}
	else if(promoAutoGenerate == 'No'){
		if(promoCode == ''){
			jAlert("Promotional Code is Mandatory.");
			return;
		}else{
			var letters = /^([a-zA-Z0-9]){3,}$/; ///^[0-9a-zA-Z]+$/
			if($('#promo_code').val().match(letters)){}
			else{
				jAlert("Please input alphanumeric characters only, should be minimum 3 characters long.");
				return;
			}
		}		
	}else if(promoAutoGenerate == '' || promoAutoGenerate == null || promoAutoGenerate == undefined){
		jAlert("Please select auto generate option.");
		return;
	}

	for(var k=0;k<j;k++){
		if($('#promoTypeId_'+k).val() == '' && $('#parentId_'+k).val() == '' && $('#childId_'+k).val() == '' && $('#grandChildId_'+k).val() == '' && $('#artistId_'+k).val() == '' && $('#venueId_'+k).val() == '' && $('#eventId_'+k).val() == ''){
			count++;
		}
		if($('#promoTypeId_'+k).val() == 0){
			promoTypeFlag = true;
		}
	}
	if(count == j){
		jAlert("Please select one from Artist/Event/Venue/Parent/Child/Grand child from autocomplete to generate discount code.");
		return;
	}
	if(promoTypeFlag){
		for(var k=0;k<j;k++){
			if(($('#parentId_'+k).val() != "" && $('#parentId_'+k).val() != undefined)|| ($('#childId_'+k).val() != "" && $('#childId_'+k).val() != undefined)
				|| ($('#grandChildId_'+k).val() != "" && $('#grandChildId_'+k).val() != undefined) || ($('#artistId_'+k).val() != "" && $('#artistId_'+k).val() != undefined)
				|| ($('#venueId_'+k).val() != "" && $('#venueId_'+k).val() != undefined)){
				flag = true;
			}
		}
	}
	if(flag){
		jAlert("Please select either ALL or one from Artist/Event/Venue/Parent/Child/Grand child from autocomplete to generate discount code.");
		return;
	}
	/*if(parentId == '' && childId == '' && grandChildId== '' && artistId == '' && venueId== ''){
		jAlert("Please select one from Artist,venue,parent,child,grand child from autocomplete to generate discount code.");
		return;
	}*/
	
	if(promoFlatDiscount == 'Yes'){
		if(discountPerc == '' || isNaN(discountPerc)){
			jAlert("Please add Valid discount in dollar.");
			return;
		}		
		if(mobileDiscountPerc == '' || isNaN(mobileDiscountPerc)){
			jAlert("Please add Valid mobile discount in dollar.");
			return;
		}
		if(orderThreshold == ''){
			jAlert("Order Threshold is Mandatory.");
			return;
		}
	}else if(promoFlatDiscount == 'No'){
		if(discountPerc == '' || isNaN(discountPerc)){
			jAlert("Please add Valid discount in percentage.");
			return;
		}
		if(mobileDiscountPerc == '' || isNaN(mobileDiscountPerc)){
			jAlert("Please add Valid mobile discount in percentage.");
			return;
		}
	}else{
		jAlert("Please select flat discount option.");
		return;
	}
	
	if(fromDate == '' || toDate == ''){
		jAlert("Start Date and End Date is Mandatory.");
		return;
	}
		
	if(maxOrders == ''){
		jAlert("Max. Orders is Mandatory.");
		return;
	}
	var requestUrl = "";
	var dataString = "";
	if(action == 'save'){
		requestUrl = "${pageContext.request.contextPath}/Client/SaveDiscountCode";
		dataString  = $('#discountCodeForm').serialize()+"&count="+j;
	}else if(action == 'update'){
		requestUrl = "${pageContext.request.contextPath}/Client/EditDiscountCode";
		dataString = $('#discountCodeForm').serialize()+"&count="+j+"&action=update";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#myModal-2').modal('hide');
				pagingInfo = jsonData.discountCodePagingInfo;
				columnFilters = {};
				refreshDiscountCodeGridValues(jsonData.discountCodeList);
				refreshPromoOfferDtlGridValues('');
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

</script>
<!--<ul id="contextMenu" style="display: none; position: absolute">
	<li data="edit promo code">Edit</li>
</ul>-->

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Admin</a>
			</li>
			<li><i class="fa fa-laptop"></i>Generate Promotional Code</li>
		</ol>
	</div>
</div>


<div class="row">
	<div class="alert alert-success fade in" id="infoMainDiv" <c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
		<button data-dismiss="alert" class="close close-sm" type="button">
			<i class="icon-remove"></i>
		</button>
		<span id="infoMsgDiv">${info}</span>
	</div>
</div>
<!--<div class="row">
	<div class="col-xs-12 filters-div">
		<form name="discountCodeForm" id="discountCodeForm" onsubmit="return false;" method="post">
			<input type="hidden" id="action" name="action" />
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Artist/Venue/Categories</label> <input class="form-control searchcontrol" type="text" id="artistVenue" name="artistVenue"> <input type="hidden"
					value="${artistId}" id="artistId" name="artistId" /> 
					<input type="hidden" value="${venueId}" id="venueId" name="venueId" /> <input type="hidden" id="artistVenueName"
					name="artistVenueName" /><input type="hidden"  id="parentId" name="parentId" />
					<input type="hidden"  id="childId" name="childId" /><input type="hidden"  id="grandChildId" name="grandChildId" />
					<label for="name" id="selectedItem"></label> <a href="javascript:resetItem()" id="resetLink">remove</a>
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Start Date</label> <input class="form-control" type="text" id="fromDate" name="fromDate">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">End Date</label> <input class="form-control" type="text" id="toDate" name="toDate">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Max Orders</label> <input class="form-control" type="text" id="maxOrder" name="maxOrder">
			</div>
			
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Discount Percentage</label> <input class="form-control" type="text" id="discountPerc" name="discountPerc">
			</div>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<button type="button" id="saveCode" class="btn btn-primary" style="margin-top: 19px;" onclick="saveDiscountCode();">Save</button>
			</div>
		</form>
	</div>
</div>-->
<div class="full-width full-width-btn mb-20">
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-2" onclick="resetModal();">Add Promotional Code</button>
	<button type="button" id="enableOffer" class="btn btn-primary" onclick="updateOffer('ENABLED');">Enable</button>
	<button type="button" id="disableOffer" class="btn btn-primary" onclick="updateOffer('DISABLED');">Disable</button>
</div>
	<br/><br/>
	<div style="position: relative" id="artistGridDiv">
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Promotional Offer</label>
				<div class="pull-right">
					<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
					<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
				</div>
			</div>
			<div id="discountCode_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
			<div id="discountCode_pager" style="width: 100%; height: 10px;"></div>

		</div>
	</div>
	
	<div class="row">
		<input type="hidden" name="promotionalOfferId" id="promotionalOfferId"/>
	</div>
	<br/>
	
	<div style="position: relative" id="promoOfferDtlGridDiv">
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Promotional Offer Details</label>
				<div class="pull-right">
					<a href="javascript:promoOfferDtlExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
					<a href="javascript:promoOfferDtlResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters &nbsp; |</a>
				</div>
			</div>
			<div id="promoOfferDtl_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
			<div id="promoOfferDtl_pager" style="width: 100%; height: 10px;"></div>

		</div>
	</div>
	
	<!-- Add Promotional Code -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal-2">Add Promotional Code</button> -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
		tabindex="-1" id="myModal-2" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Promotional Code</h4>
				</div>
					<div class="modal-body full-width">
						<form name="discountCodeForm" id="discountCodeForm" method="post">
						<input id="promo_id" name="promoId" type="hidden" />
						<div class="form-group tab-fields">
							<div class="form-group col-sm-6 col-xs-6">
								<label>Auto Generate <span class="required">*</span></label> 
								<span id="promo_ag_yes"><input class="form-control" id="promo_autoGenerate_yes" name="promoAutoGenerate" type="radio" value="Yes"/>Yes</span>
								<input class="form-control" id="promo_autoGenerate_no" name="promoAutoGenerate" type="radio" value="No"/>No
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Flat Discount <span class="required">*</span></label> 
								<input class="form-control" id="promo_flatDiscount_yes" name="promoFlatDiscount" type="radio" value="Yes"/>Yes
								<input class="form-control" id="promo_flatDiscount_no" name="promoFlatDiscount" type="radio" value="No"/>No
							</div>
							<div id="promoCodeDiv" class="form-group col-sm-6 col-xs-6">
								<label>Promotional Code <span class="required">*</span></label> 
								<input class="form-control" id="promo_code" name="promoCode" type="text" />
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>Artist/Venue/Categories <span class="required">*</span></label> 
								<input class="form-control searchcontrol" type="text" id="promo_artistVenue" name="promoArtistVenue">
								<input type="hidden" value="" id="promoTypeId" name="promoTypeId" /> 
								<input type="hidden" value="${artistId}" id="artistId" name="artistId" />
								<input type="hidden" value="${eventId}" id="eventId" name="eventId" />
								<input type="hidden" value="${venueId}" id="venueId" name="venueId" /> 
								<input type="hidden" id="artistVenueName" name="artistVenueName" />
								<input type="hidden" id="parentId" name="parentId" />
								<input type="hidden" id="childId" name="childId" />
								<input type="hidden" id="grandChildId" name="grandChildId" />
								<div id="promoCategory"><div class="block"></div></div> 
							</div>
														
							<div class="form-group col-sm-6 col-xs-6">
								<label>Desktop Discount <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promo_discountPerc" name="promoDiscountPerc">
								<span id="dollarSpan" style="display:none; float:left;" class="required">$</span>
								<span id="percSpan" style="display:none; float:left;" class="required">%</span>
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>APP Discount <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promo_mobileDiscountPerc" name="promoMobileDiscountPerc">
								<span id="mobileDollarSpan" style="display:none; float:left;" class="required">$</span>
								<span id="mobilePercSpan" style="display:none; float:left;" class="required">%</span>
							</div>
							<div id="promoOrderDiv" class="form-group col-sm-6 col-xs-6">
								<label>Order Threshold <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promo_orderThreshold" name="promoOrderThreshold">
								<span class="required">$</span>
							</div>								
							<div class="form-group col-sm-6 col-xs-6">
								<label>Max Orders <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promo_maxOrders" name="promoMaxOrders">
							</div>							
							<div class="form-group col-sm-6 col-xs-6">
								<label>Start Date <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promo_fromDate" name="promoFromDate">
							</div>
							<div class="form-group col-sm-6 col-xs-6">
								<label>End Date <span class="required">*</span></label> 
								<input class="form-control" type="text" id="promo_toDate" name="promoToDate">
							</div>
						</div>
						</form>			
					</div>
					<div class="modal-footer full-width">
						<button class="btn btn-primary" id="saveBtn" type="button" onclick="promoSaveDiscountCode('save')">Save</button>
						<button class="btn btn-primary" id="updateBtn" type="button" onclick="promoSaveDiscountCode('update')">Update</button>
						<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
					</div>			
			</div>
		</div>
	</div>
	<!-- Ends of Add Shipping/Other Address popup-->
	
	<script type="text/javascript">
	var discountCodeCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	function exportToExcel(){
		var appendData = "headerFilter="+discountCodeSearchString;
	    //var url = "${pageContext.request.contextPath}/Client/DiscountCodeExportToExcel?"+appendData;
		var url = apiServerUrl+"DiscountCodeExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		discountCodeSearchString='';
		columnFilters = {};
		getDiscountCodeGridData(0);
		sortingString = '';
		refreshPromoOfferDtlGridValues('');
	}

	var pagingInfo;
	var discountCodeDataView;
	var discountCodeGrid;
	var discountCodeData = [];
	var discountCodeGridPager;
	var discountCodeSearchString='';
	var columnFilters = {};
	var sortingString ='';
	var userDiscountCodeColumnsStr = '<%=session.getAttribute("discountcodegrid")%>';

		var userDiscountCodeColumns = [];
		var allDiscountCodeColumns = [discountCodeCheckboxSelector.getColumnDefinition(),
				/*{
					id : "promoOfferId",
					name : "Promo ID",
					field : "promoOfferId",
					width : 80,
					sortable : true
				},*/ {
					id : "promocode",
					name : "Promo Code",
					field : "promocode",
					width : 80,
					sortable : true
				}, {
					id : "discountPerc",
					name : "Discount Perc.",
					field : "discountPerc",
					width : 80,
					sortable : true
				}, {
					id : "mobileDiscountPerc",
					name : "APP Discount Perc.",
					field : "mobileDiscountPerc",
					width : 80,
					sortable : true
				}, {
					id : "startDate",
					name : "Start Date",
					field : "startDate",
					width : 80,
					sortable : true
				}, {
					id : "endDate",
					name : "End Date",
					field : "endDate",
					width : 80,
					sortable : true
				}, {
					id : "maxOrder",
					name : "Max. Order",
					field : "maxOrder",
					width : 80,
					sortable : true
				}, {
					id : "orders",
					name : "Orders",
					field : "orders",
					width : 80,
					sortable : true
				}, /* {
					id : "promoType",
					field : "promoType",
					name : "Promo Type",
					width : 80,
					sortable : true
				},{
					id : "artist",
					field : "artist",
					name : "Artist",
					width : 80,
					sortable : true
				},{
					id : "venue",
					field : "venue",
					name : "Venue",
					width : 80,
					sortable : true
				},{
					id : "parent",
					field : "parent",
					name : "Parent Category",
					width : 80,
					sortable : true
				},{
					id : "child",
					field : "child",
					name : "Child Category",
					width : 80,
					sortable : true
				},{
					id : "grandChild",
					field : "grandChild",
					name : "Grand Child Category",
					width : 80,
					sortable : true
				}, */ {
					id : "status",
					field : "status",
					name : "Status",
					width : 80,
					sortable : true
				}, {
					id : "createdBy",
					field : "createdBy",
					name : "Created By",
					width : 80,
					sortable : true
				}, {
					id : "createdDate",
					field : "createdDate",
					name : "Created Date",
					width : 80,
					sortable : true
				}, {
					id : "modifiedBy",
					field : "modifiedBy",
					name : "ModifiedBy ",
					width : 80,
					sortable : true
				},{
					id : "editOffer",
					field : "editOffer",
					name : "Edit ",
					width : 80,
					formatter:buttonFormatter
				}];

		if (userDiscountCodeColumnsStr != 'null' && userDiscountCodeColumnsStr != '') {
			columnOrder = userDiscountCodeColumnsStr.split(',');
			var columnWidth = [];
			for ( var i = 0; i < columnOrder.length; i++) {
				columnWidth = columnOrder[i].split(":");
				for ( var j = 0; j < allDiscountCodeColumns.length; j++) {
					if (columnWidth[0] == allDiscountCodeColumns[j].id) {
						userDiscountCodeColumns[i] = allDiscountCodeColumns[j];
						userDiscountCodeColumns[i].width = (columnWidth[1] - 5);
						break;
					}
				}

			}
		} else {
			userDiscountCodeColumns = allDiscountCodeColumns;
		}

		function buttonFormatter(row,cell,value,columnDef,dataContext){  
			    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.promoOfferId +"'/>";
			    return button;
		}
		$('.editClickableImage').live('click', function(){
		    var me = $(this), id = me.attr('id');
		    getEditPromotionalCode(id);//confirm("Are you sure,Do you want to Delete it?");
		});
		
		var discountCodeOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect : false,
			topPanelHeight : 25,
			showHeaderRow : true,
			headerRowHeight : 30,
			explicitInitialization : true
		};
		var discountCodeGridSortcol = "promoCode";
		var discountCodeGridSortdir = 1;
		var percentCompleteThreshold = 0;
		var discountCodeGridSearchString = "";

		function discountCodeGridComparer(a, b) {
			var x = a[discountCodeGridSortcol], y = b[discountCodeGridSortcol];
			if (!isNaN(x)) {
				return (parseFloat(x) == parseFloat(y) ? 0
						: (parseFloat(x) > parseFloat(y) ? 1 : -1));
			}
			if (x == '' || x == null) {
				return 1;
			} else if (y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String)
					&& (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));
			}
		}
				
		function refreshDiscountCodeGridValues(jsonData) {
			$("div#divLoading").addClass('show');
			discountCodeData = [];
			if (jsonData != null && jsonData.length > 0) {
				for ( var i = 0; i < jsonData.length; i++) {
					var data = jsonData[i];
					var d = (discountCodeData[i] = {});
					d["id"] = i;
					d["promoOfferId"] = data.id;
					d["promocode"] = data.promocode;
					d["discountPerc"] = data.discountPerc;
					d["mobileDiscountPerc"] = data.mobileDiscountPerc;
					d["startDate"] = data.startDate;
					d["endDate"] = data.endDate;
					d["maxOrder"] = data.maxOrder;
					d["orders"] = data.orders;
					/* d["promoType"] = data.promoType;
					d["artist"] = data.artist;
					d["venue"] = data.venue;
					d["parent"] = data.parent;
					d["child"] = data.child;
					d["grandChild"] = data.grandChild;
					d["artistId"] = data.artistId;
					d["venueId"] = data.venueId;
					d["parentId"] = data.parentId;
					d["childId"] = data.childId;
					d["grandChildId"] = data.grandChildId; */
					d["status"] = data.status;
					d["createdBy"] = data.createdBy;
					d["createdDate"] = data.createdDate;
					d["modifiedBy"] = data.modifiedBy;
				}
			}

			discountCodeDataView = new Slick.Data.DataView();
			discountCodeGrid = new Slick.Grid("#discountCode_grid", discountCodeDataView,
					userDiscountCodeColumns, discountCodeOptions);
			discountCodeGrid.registerPlugin(new Slick.AutoTooltips({
				enableForHeaderCells : true
			}));
			discountCodeGrid.setSelectionModel(new Slick.RowSelectionModel());
			discountCodeGrid.registerPlugin(discountCodeCheckboxSelector);
			
				discountCodeGridPager = new Slick.Controls.Pager(discountCodeDataView,
						discountCodeGrid, $("#discountCode_pager"),
						pagingInfo);
			var discountCodeGridColumnpicker = new Slick.Controls.ColumnPicker(
					allDiscountCodeColumns, discountCodeGrid, discountCodeOptions);
			
			/* discountCodeGrid.onContextMenu.subscribe(function(e) {
				e.preventDefault();
				var cell = discountCodeGrid.getCellFromEvent(e);
				discountCodeGrid.setSelectedRows([ cell.row ]);
				var row = discountCodeGrid.getSelectedRows([ 0 ])[0];			
				var promoId = discountCodeGrid.getDataItem(row).promoOfferId;
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				
				if (height < ($("#contextMenu").height() + 100)) {
					height = e.pageY - $("#contextMenu").height();
				} else {
					height = e.pageY;
				}
				if (width < $("#contextMenu").width()) {
					width = e.pageX - $("#contextMenu").width();
				} else {
					width = e.pageX;
				}
				$("#contextMenu").data("row", cell.row).css("top", height).css(
						"left", width).show();
				$("body").one("click", function() {
					$("#contextMenu").hide();
				});
				
			}); */
		
			discountCodeGrid.onSort.subscribe(function(e, args) {
				discountCodeGridSortcol = args.sortCol.field;			
				if(sortingString.indexOf(discountCodeGridSortcol) < 0){
					discountCodeGridSortdir = 'ASC';
				}else{
					if(discountCodeGridSortdir == 'DESC' ){
						discountCodeGridSortdir = 'ASC';
					}else{
						discountCodeGridSortdir = 'DESC';
					}
				}
				sortingString = '';
				sortingString +=',SORTINGCOLUMN:'+discountCodeGridSortcol+',SORTINGORDER:'+discountCodeGridSortdir+',';
				getDiscountCodeGridData(0)
			});
			// wire up model discountCodes to drive the discountCodeGrid
			discountCodeDataView.onRowCountChanged.subscribe(function(e, args) {
				discountCodeGrid.updateRowCount();
				discountCodeGrid.render();
			});
			discountCodeDataView.onRowsChanged.subscribe(function(e, args) {
				discountCodeGrid.invalidateRows(args.rows);
				discountCodeGrid.render();
			});
			$(discountCodeGrid.getHeaderRow())
					.delegate(
							":input",
							"keyup",
							function(e) {
								var keyCode = (e.keyCode ? e.keyCode : e.which);
								discountCodeSearchString = '';
								var columnId = $(this).data("columnId");
								if (columnId != null) {
									columnFilters[columnId] = $.trim($(this)
											.val());
									if (keyCode == 13) {
										for ( var columnId in columnFilters) {
											if (columnId !== undefined
													&& columnFilters[columnId] !== "") {
												discountCodeSearchString += columnId
														+ ":"
														+ columnFilters[columnId]
														+ ",";
											}
										}
										getDiscountCodeGridData(0);
									}
								}

							});
			discountCodeGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if (args.column.id.indexOf('checkbox') == -1) {
					if(args.column.id == 'startDate' || args.column.id == 'endDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}

			});
			discountCodeGrid.init();
			
			var discountCodeRowIndex = -1;
			discountCodeGrid.onSelectedRowsChanged.subscribe(function() { 
				var temprDiscountRowIndex = discountCodeGrid.getSelectedRows([0])[0];
				if (temprDiscountRowIndex != discountCodeRowIndex) {
					var promoOffrId = discountCodeGrid.getDataItem(temprDiscountRowIndex).promoOfferId;
					getPromoOfferDtlGridData(promoOffrId,0);
				}
			});
			// initialize the model after all the discountCodes have been hooked up
			discountCodeDataView.beginUpdate();
			discountCodeDataView.setItems(discountCodeData);
			//discountCodeDataView.setFilter(filter);
			discountCodeDataView.endUpdate();
			discountCodeDataView.syncGridSelection(discountCodeGrid, true);
			discountCodeGrid.resizeCanvas();
			/* $("#gridContainer").resizable(); */
			$("div#divLoading").removeClass('show');
		}

		function getSelectedDiscountCodeGridId() {
			var temprDiscountCodeRowIndex = discountCodeGrid.getSelectedRows();

			var discountCodeIdStr = '';
			$.each(temprDiscountCodeRowIndex, function(index, value) {
				discountCodeIdStr += ',' + discountCodeGrid.getDataItem(value).promoOfferId;
			});

			if (discountCodeIdStr != null && discountCodeIdStr != '') {
				discountCodeIdStr = discountCodeIdStr.substring(1, discountCodeIdStr.length);
				return discountCodeIdStr;
			}
		}

		$('#menuContainer').click(function() {
			if ($('.ABCD').length > 0) {
				$('#menuContainer').removeClass('ABCD');
			} else {
				$('#menuContainer').addClass('ABCD');
			}
			if(discountCodeGrid != null && discountCodeGrid != undefined){
				discountCodeGrid.resizeCanvas();
			}
			if(promoOfferDtlGrid != null && promoOfferDtlGrid != undefined){
				promoOfferDtlGrid.resizeCanvas();
			}			
		});

		function pagingControl(move, id) {
			if(id == 'discountCode_pager'){
				var pageNo = 0;
				if (move == 'FIRST') {
					pageNo = 0;
				} else if (move == 'LAST') {
					pageNo = parseInt(pagingInfo.totalPages) - 1;
				} else if (move == 'NEXT') {
					pageNo = parseInt(pagingInfo.pageNum) + 1;
				} else if (move == 'PREV') {
					pageNo = parseInt(pagingInfo.pageNum) - 1;
				}
				getDiscountCodeGridData(pageNo);
			}else if(id == 'promoOfferDtl_pager'){
				var pageNo = 0;
				if (move == 'FIRST') {
					pageNo = 0;
				} else if (move == 'LAST') {
					pageNo = parseInt(promoOfferDtlPagingInfo.totalPages) - 1;
				} else if (move == 'NEXT') {
					pageNo = parseInt(promoOfferDtlPagingInfo.pageNum) + 1;
				} else if (move == 'PREV') {
					pageNo = parseInt(promoOfferDtlPagingInfo.pageNum) - 1;
				}
				var promoOfrId = $('#promotionalOfferId').val();
				getPromoOfferDtlGridData(promoOfrId, pageNo);
			}
		}
		
		
		//Promotional Offer Details Grid
		function promoOfferDtlExportToExcel(){
			var promoOfrId = $('#promotionalOfferId').val();
			var appendData = "promoOfferId="+promoOfrId+"&headerFilter="+promoOfferDtlSearchString;
		    //var url = "${pageContext.request.contextPath}/Client/PromoOfferDtlExportToExcel?"+appendData;
			var url = apiServerUrl+"PromoOfferDtlExportToExcel?"+appendData;
		    $('#download-frame').attr('src', url);
		}
		
		function promoOfferDtlResetFilters(){
			promoOfferDtlSearchString='';
			promoOfferDtlColumnFilters = {};
			var promoOfrId = $('#promotionalOfferId').val();
			getPromoOfferDtlGridData(promoOfrId, 0);
		}

		function getPromoOfferDtlGridData(promoOfferId, pageNo){
			$.ajax({
				url : "${pageContext.request.contextPath}/Client/GetDiscountCodeDetail",
				type : "post",
				dataType:"json",
				data:"promoOfferId="+promoOfferId+"&pageNo="+pageNo+"&headerFilter="+promoOfferDtlSearchString,
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}					
					promoOfferDtlPagingInfo = jsonData.promoOfferDtlPagingInfo;
					refreshPromoOfferDtlGridValues(jsonData.promoOfferDtlList);					
					$('#promotionalOfferId').val(promoOfferId);
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}

		var promoOfferDtlPagingInfo;
		var promoOfferDtlDataView;
		var promoOfferDtlGrid;
		var promoOfferDtlData = [];
		var promoOfferDtlGridPager;
		var promoOfferDtlSearchString='';
		var promoOfferDtlColumnFilters = {};
		var userPromoOfferDtlColumnsStr = '<%=session.getAttribute("promoOfferDtlGrid")%>';

		var userPromoOfferDtlColumns = [];
		var allPromoOfferDtlColumns = [
				/*{
					id : "promoOfferDtlId",
					name : "Promo ID",
					field : "promoOfferDtlId",
					width : 80,
					sortable : true
				},*/{
					id : "promoType",
					field : "promoType",
					name : "Promo Type",
					width : 80,
					sortable : true
				},{
					id : "artist",
					field : "artist",
					name : "Artist",
					width : 80,
					sortable : true
				},{
					id : "event",
					field : "event",
					name : "Event",
					width : 80,
					sortable : true
				},{
					id : "venue",
					field : "venue",
					name : "Venue",
					width : 80,
					sortable : true
				},{
					id : "parent",
					field : "parent",
					name : "Parent Category",
					width : 80,
					sortable : true
				},{
					id : "child",
					field : "child",
					name : "Child Category",
					width : 80,
					sortable : true
				},{
					id : "grandChild",
					field : "grandChild",
					name : "Grand Child Category",
					width : 80,
					sortable : true
				}];

		if (userPromoOfferDtlColumnsStr != 'null' && userPromoOfferDtlColumnsStr != '') {
			columnOrder = userPromoOfferDtlColumnsStr.split(',');
			var columnWidth = [];
			for ( var i = 0; i < columnOrder.length; i++) {
				columnWidth = columnOrder[i].split(":");
				for ( var j = 0; j < allPromoOfferDtlColumns.length; j++) {
					if (columnWidth[0] == allPromoOfferDtlColumns[j].id) {
						userPromoOfferDtlColumns[i] = allPromoOfferDtlColumns[j];
						userPromoOfferDtlColumns[i].width = (columnWidth[1] - 5);
						break;
					}
				}

			}
		} else {
			userPromoOfferDtlColumns = allPromoOfferDtlColumns;
		}
			
		var promoOfferDtlOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect : false,
			topPanelHeight : 25,
			showHeaderRow : true,
			headerRowHeight : 30,
			explicitInitialization : true
		};
		var promoOfferDtlGridSortcol = "promoCode";
		var promoOfferDtlGridSortdir = 1;
		var percentCompleteThreshold = 0;

		function promoOfferDtlGridComparer(a, b) {
			var x = a[promoOfferDtlGridSortcol], y = b[promoOfferDtlGridSortcol];
			if (!isNaN(x)) {
				return (parseFloat(x) == parseFloat(y) ? 0
						: (parseFloat(x) > parseFloat(y) ? 1 : -1));
			}
			if (x == '' || x == null) {
				return 1;
			} else if (y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String)
					&& (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));
			}
		}
		

		function refreshPromoOfferDtlGridValues(jsonData) {
			$("div#divLoading").addClass('show');
			promoOfferDtlData = [];
			if (jsonData != null && jsonData.length > 0) {
				for ( var i = 0; i < jsonData.length; i++) {
					var data = jsonData[i];
					var d = (promoOfferDtlData[i] = {});
					d["id"] = i;
					d["promoOfferDtlId"] = data.id;
					d["promoType"] = data.promoType;
					d["artist"] = data.artist;
					d["event"] = data.event;
					d["venue"] = data.venue;
					d["parent"] = data.parent;
					d["child"] = data.child;
					d["grandChild"] = data.grandChild;
					d["artistId"] = data.artistId;
					d["eventId"] = data.eventId;
					d["venueId"] = data.venueId;
					d["parentId"] = data.parentId;
					d["childId"] = data.childId;
					d["grandChildId"] = data.grandChildId;
				}
			}

			promoOfferDtlDataView = new Slick.Data.DataView();
			promoOfferDtlGrid = new Slick.Grid("#promoOfferDtl_grid", promoOfferDtlDataView,
					userPromoOfferDtlColumns, promoOfferDtlOptions);
			promoOfferDtlGrid.registerPlugin(new Slick.AutoTooltips({
				enableForHeaderCells : true
			}));
			promoOfferDtlGrid.setSelectionModel(new Slick.RowSelectionModel());
			/*promoOfferDtlGrid.registerPlugin(discountCodeCheckboxSelector);*/
			
				promoOfferDtlGridPager = new Slick.Controls.Pager(promoOfferDtlDataView,
						promoOfferDtlGrid, $("#promoOfferDtl_pager"),
						promoOfferDtlPagingInfo);
			var discountCodeGridColumnpicker = new Slick.Controls.ColumnPicker(
					allPromoOfferDtlColumns, promoOfferDtlGrid, promoOfferDtlOptions);
			
		
			promoOfferDtlGrid.onSort.subscribe(function(e, args) {
				promoOfferDtlGridSortdir = args.sortAsc ? 1 : -1;
				promoOfferDtlGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					promoOfferDtlDataView.fastSort(promoOfferDtlGridSortcol, args.sortAsc);
				} else {
					promoOfferDtlDataView.sort(promoOfferDtlGridComparer, args.sortAsc);
				}
			});
			// wire up model discountCodes to drive the promoOfferDtlGrid
			promoOfferDtlDataView.onRowCountChanged.subscribe(function(e, args) {
				promoOfferDtlGrid.updateRowCount();
				promoOfferDtlGrid.render();
			});
			promoOfferDtlDataView.onRowsChanged.subscribe(function(e, args) {
				promoOfferDtlGrid.invalidateRows(args.rows);
				promoOfferDtlGrid.render();
			});
			$(promoOfferDtlGrid.getHeaderRow())
					.delegate(
							":input",
							"keyup",
							function(e) {
								var keyCode = (e.keyCode ? e.keyCode : e.which);
								promoOfferDtlSearchString = '';
								var columnId = $(this).data("columnId");
								if (columnId != null) {
									promoOfferDtlColumnFilters[columnId] = $.trim($(this)
											.val());
									if (keyCode == 13) {
										for ( var columnId in promoOfferDtlColumnFilters) {
											if (columnId !== undefined
													&& promoOfferDtlColumnFilters[columnId] !== "") {
												promoOfferDtlSearchString += columnId
														+ ":"
														+ promoOfferDtlColumnFilters[columnId]
														+ ",";
											}
										}
										var promoOfrId = $('#promotionalOfferId').val();
										getPromoOfferDtlGridData(promoOfrId, 0);
									}
								}

							});
			promoOfferDtlGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if (args.column.id.indexOf('checkbox') == -1) {
					if(args.column.id == 'startDate' || args.column.id == 'endDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(promoOfferDtlColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(promoOfferDtlColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}

			});
			promoOfferDtlGrid.init();
			// initialize the model after all the discountCodes have been hooked up
			promoOfferDtlDataView.beginUpdate();
			promoOfferDtlDataView.setItems(promoOfferDtlData);
			//promoOfferDtlDataView.setFilter(filter);
			promoOfferDtlDataView.endUpdate();
			promoOfferDtlDataView.syncGridSelection(promoOfferDtlGrid, true);
			promoOfferDtlGrid.resizeCanvas();
			/* $("#gridContainer").resizable(); */
			$("div#divLoading").removeClass('show');
		}
		
		function saveUserPromoOfferDtlPreference() {
			var cols = visibleColumns;
			if (cols == null || cols == '' || cols.length == 0) {
				cols = promoOfferDtlGrid.getColumns();
			}
			var colStr = '';
			for ( var i = 0; i < cols.length; i++) {
				colStr += cols[i].id + ":" + cols[i].width + ",";
			}
			saveUserPreference('promoOfferDtlGrid', colStr);
		}
		/* $("#contextMenu").click(function(e) {
			if (!$(e.target).is("li")) {
				return;
			}
			var row = discountCodeGrid.getSelectedRows([ 0 ])[0];
			if (row < 0) {
				jAlert("Please select Promotional Code.");
				return;
			}
			var promoOfferId = discountCodeGrid.getDataItem(row).promoOfferId;
			if ($(e.target).attr("data") == 'edit promo code') {
				getEditPromotionalCode(promoOfferId);
			}
		}); */
	
		function getEditPromotionalCode(promoOfferId){
			$.ajax({
				url : "${pageContext.request.contextPath}/Client/EditDiscountCode",
				type : "post",
				dataType: "json",
				data: "promoId="+promoOfferId+"&action=edit",
				success : function(response){
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						$('#promoCategory').empty();
						$('#myModal-2').modal('show');
						setEditPromotionalCode(jsonData.promoCodeList);
					}else{
						jAlert(jsonData.msg);
					}
				},
				error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	
		function setEditPromotionalCode(promoCodes){
			$('#saveBtn').hide();			
			$('#updateBtn').show();
			j = promoCodes.length;
			varSelectedList = '';
			if(promoCodes != null && promoCodes.length > 0){				
				for (var i = 0; i < promoCodes.length; i++){
					var promoName = "";
					var data = promoCodes[i];
					$('#promo_id').val(data.id);
					$('#promo_code').val(data.promocode);
					$('#promo_fromDate').val(data.fromDate);
					$('#promo_toDate').val(data.toDate);
					$('#promo_maxOrders').val(data.maxOrder);
					$('#promo_discountPerc').val(data.discountPerc);
					$('#promo_mobileDiscountPerc').val(data.mobileDiscountPerc);
					$('#promo_orderThreshold').val(data.orderThreshold);
					promoAutoGenerate = data.isAutoGenerate;
					promoFlatDiscount = data.isFlatDiscount;
													
					$('#promo_ag_yes').hide();
					if(data.isAutoGenerate == 'Yes'){
						$('#promo_autoGenerate_yes').prop('checked',true);
						$("#promo_code").attr("disabled", "disabled");
					}else if(data.isAutoGenerate == 'No'){
						$('#promo_autoGenerate_no').prop('checked',true);
						//$("#promo_code").removeAttr("disabled");
						$("#promo_code").attr("disabled", "disabled");
					}
					if(data.isFlatDiscount == 'Yes'){
						$('#promo_flatDiscount_yes').prop('checked',true);
						$("#promo_orderThreshold").removeAttr("disabled");
						$('#dollarSpan').show();
						$('#percSpan').hide();
						$('#mobileDollarSpan').show();
						$('#mobilePercSpan').hide();
					}else if(data.isFlatDiscount == 'No'){
						$('#promo_flatDiscount_no').prop('checked',true);
						$("#promo_orderThreshold").attr("disabled", "disabled");
						$('#dollarSpan').hide();
						$('#percSpan').show();
						$('#mobileDollarSpan').hide();
						$('#mobilePercSpan').show();
					}
					
					var promoTypeEd = data.promoType;
					varSelectedList += ","+promoTypeEd;
					if(promoTypeEd == "ALL"){
						promoTypes = "<input type='hidden' name='promoTypeId_"+i+"' id='promoTypeId_"+i+"' value='0' />";
						promoName = "ALL";
						varSelectedList += "_"+0;
					}else if(promoTypeEd=="ARTIST"){
						promoTypes = "<input type='hidden' name='artistId_"+i+"' id='artistId_"+i+"' value="+data.artistId+" />";
						promoName = data.artist;
						varSelectedList += "_"+data.artistId;
					}else if(promoTypeEd=="EVENT"){
						promoTypes = "<input type='hidden' name='eventId_"+i+"' id='eventId_"+i+"' value="+data.eventId+" />";
						promoName = data.event;
						varSelectedList += "_"+data.eventId;
					}else if(promoTypeEd=='VENUE'){
						promoTypes = "<input type='hidden' name='venueId_"+i+"' id='venueId_"+i+"' value="+data.venueId+" />";
						promoName = data.venue;
						varSelectedList += "_"+data.venueId;
					}else if(promoTypeEd=='PARENT'){
						promoTypes = "<input type='hidden' name='parentId_"+i+"' id='parentId_"+i+"' value="+data.parentId+" />";
						promoName = data.parent;
						varSelectedList += "_"+data.parentId;
					}else if(promoTypeEd=='CHILD'){
						promoTypes = "<input type='hidden' name='childId_"+i+"' id='childId_"+i+"' value="+data.childId+" />";
						promoName = data.child;
						varSelectedList += "_"+data.childId;
					}else if(promoTypeEd=='GRAND'){
						promoTypes = "<input type='hidden' name='grandChildId_"+i+"' id='grandChildId_"+i+"' value="+data.grandChildId+" />";
						promoName = data.grandChild;
						varSelectedList += "_"+data.grandChildId;
					}
					promoTypes += "<input type='hidden' name='artistVenueName_"+i+"' id='artistVenueName_"+i+"' value="+promoName+" />";
					$('#promo_artistVenue').val("");
					$('#promoCategory').append($('.block:last').before('<div class="block promoAll">'+promoTypeEd+' : '+promoName+' '+promoTypes+'<span class="remove"><a href="javascript:removeItem('+i+')">X</a></span></div>'));
					//j++;
				}
			}
		}
		
		function saveUserDiscountCodePreference() {
			var cols = visibleColumns;
			if (cols == null || cols == '' || cols.length == 0) {
				cols = discountCodeGrid.getColumns();
			}
			var colStr = '';
			for ( var i = 0; i < cols.length; i++) {
				colStr += cols[i].id + ":" + cols[i].width + ",";
			}
			saveUserPreference('discountcodegrid', colStr);
		}

		//call functions once page loaded
		window.onload = function() {
			pagingInfo = JSON.parse(JSON.stringify(${discountCodePagingInfo}));
			promoOfferDtlPagingInfo = JSON.parse(JSON.stringify(${promoOfferDtlPagingInfo}));
			refreshDiscountCodeGridValues(JSON.parse(JSON.stringify(${discountCodeList})));
			$('#discountCode_pager> div')
					.append(
							"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserDiscountCodePreference()'>");
			enableMenu();
		};
		
	</script>