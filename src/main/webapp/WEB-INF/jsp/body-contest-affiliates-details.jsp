<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script src="../resources/js/app/affiliatePromoCodeHistory.js"></script>

<style>
input {
	color: black !important;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
  background:#FFFFFF;
  color:#000000;
  border: 1px solid gray;
  padding: 2px;
  display: inline-block;
  min-width: 200px;
  
  -moz-box-shadow: 2px 2px 2px silver;
  -webkit-box-shadow: 2px 2px 2px silver;
  z-index: 99999;
}
#contextMenu li {
  padding: 4px 4px 4px 14px;
  list-style: none;
  cursor: pointer;
}
#contextMenu li:hover {
	color:#FFFFFF;
  background-color:#4d94ff;
}

#addCustomer, #addUser, #active, #inactive {
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	color: #2b2b2b;
	background-color: rgba(0, 122, 255, 0.32);
	border: 1px solid gray;
}

input {
	color: black !important;
}

</style>
<script>
$(document).ready(function(){
	
	$('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	
	$('#customerText').autocomplete("AutoCompleteCustomer", {
		width: 650,
		max: 1000,
		minChars: 2,
		dataType: "text",
		formatItem: function(row, i, max) {
			return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] +" - "+ row[3] ;
		}
	}).result(function (event,row,formatted){
		$('#customerId').val(row[1]);
		$('#selectedItem').text(row[2]+" - "+row[3]);
		$('#customerText').val("");
	});
	
	$('#menuContainer').click(function(){
		if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
		}else{
		   $('#menuContainer').addClass('ABCD');
		}
		if(affiliatesAllGrid != null && affiliatesAllGrid != undefined){
			affiliatesAllGrid.resizeCanvas();
		}
	});
	
	$("#activeAffiliate").click(function(){
			callTabOnChange('ACTIVE');
	});
	$("#inactiveAffiliate").click(function(){
		callTabOnChange('INACTIVE');
	});
	
	$("#pendingEarnigs").click(function(){
		$('#pending').show();
		$('#completed').hide();
		getAffiliateEarningGridData(0,'PENDING');
	});
	$("#completedEarnigs").click(function(){
		$('#pending').hide();
		$('#completed').show();
		getAffiliateEarningGridData(0,'COMPLETED');
	});
	
	
	
	<c:choose>
		<c:when test="${affStatus =='ACTIVE'}">
			$('#activeDiv').addClass('active');
			$('#activeTab').addClass('active');
		</c:when>
		<c:when test="${affStatus =='INACTIVE'}">
			$('#inactiveDiv').addClass('active');
			$('#inactiveTab').addClass('active');
		</c:when>
	</c:choose>
});

function callTabOnChange(selectedTab) {		
	var data = "?affStatus="+selectedTab;
	window.location = "${pageContext.request.contextPath}/ContestAffiliates"+data;
}
	
function exportToExcel(){
	if($('#activeDiv').hasClass('active')){
		$('#affStatus').val('ACTIVE');
	}else if($('#inactiveDiv').hasClass('active')){
		$('#affStatus').val('INACTIVE');
	}
	var appendData = "user=AFFILIATE&affStatus="+$('#affStatus').val()+"&headerFilter="+affiliatesAllGridSearchString;
    var url = apiServerUrl + "ContestAffiliateExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	affiliatesAllGridSearchString='';
	columnFilters = {};
	getAffiliatesAllGridData(0);
}

function addNewAffiliate(){
	resetAffiliatePopup();
	$('#action').val('SAVE');
	$('#addAffiliate').modal('show');
}
function resetAffiliatePopup(){
	$('#customerText').attr("disabled",false);
	$('#fromDate').val('');
	$('#customerId').val('');
	$('#toDate').val('');
	$('#rewardType').val('');
	$('#rewardValue').val('');
	$('#livesCount').val('');
	$('#affilaiteLivesCount').val('');
	$('#custRewardDollar').val('');
	$('#custSuperFanStars').val('');
	$('#selectedItem').text('');
	$('#saveBtn').text('Save');
}

function changeSetting(){
	$('#"affiliateSettingModal"').modal('show');
}

function saveAffiliateSetting(){
	var settingDigit = $('#settingDigit').val();
	if(settingDigit == null || settingDigit == ''){
		jAlert("Please add some value.");
		return;
	}
	$.ajax({				  
		url : "${pageContext.request.contextPath}/SaveContestAffiliateSetting",
		type : "post",
		data : "settingDigit="+settingDigit,
		success : function(response){
			if (response.status == 1) {
				jAlert(response.msg);
				$('#addAffiliate').modal('hide');
				getAffiliatesAllGridData(0);
			} else {
				jAlert(response.msg,"Info","Info");
				return false;
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

//call functions once page loaded
$(window).load(function() {
	pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
	var affiliatesList = ${affiliates};
	if(affiliatesList != null && affiliatesList.length > 0){
		refreshAffiliatesAllGridValues(JSON.parse(JSON.stringify(${affiliates})));
	}
	$('#affiliates_allpager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveAffiliateUserPreference()'>");
});

</script>
<ul id="contextMenu" style="display: none; position: absolute">
	<li data="view earnings">View Affiliate Earnings</li>
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i>Affiliates and Brokers
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest Affiliates</a></li>
			<li><i class="fa fa-laptop"></i>Manage Contest Affiliates</li>
		</ol>
	</div>
</div>

<!-- Grid view for affiliates -->
<div style="position: relative">
	<div class="full-width">
		<section class="invoice-panel panel">
		<ul class="nav nav-tabs" style="">
			<li id="activeTab" class=""><a id="activeAffiliate" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeDiv">Active</a></li>
			<li id="inactiveTab" class=""><a id="inactiveAffiliate" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inactiveDiv">Inactive</a>
		</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content"> <input type="hidden" value="" id="affStatus" name="affStatus" />
			<div id="activeDiv" class="tab-pane">
				<c:if test="${affStatus =='ACTIVE'}">
				<div class="full-width mb-20 full-width-btn">
					<button type="button" class="btn btn-primary" onclick="addNewAffiliate();">Add New Affiliate</button>
					<button type="button" class="btn btn-primary" onclick="updateStatus('INACTIVE');">In Activate</button>
					
				</div>
				<br />	
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Manage Affiliates</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="affiliatesAllGrid" style="width: 100%; height: 200px; border: 1px solid gray;"></div>
					<div id="affiliates_allpager" style="width: 100%; height: 20px;"></div>
				</div>
				</c:if>
			</div>
			
			<div id="inactiveDiv" class="tab-pane">
				<c:if test="${affStatus =='INACTIVE'}">	
				<div class="form-group col-xs-12 col-md-12">
					<button type="button" class="btn btn-primary" onclick="updateStatus('ACTIVE');">Activate</button>
				</div>
				<br />
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Manage Affiliates</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="affiliatesAllGrid" style="width: 100%; height: 200px; border: 1px solid gray;"></div>
					<div id="affiliates_allpager" style="width: 100%; height: 20px;"></div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
	
</div>
<br/>


<!-- Toggle add Modal-->
<div id="addAffiliate" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	<div class="modal-content full-width">
	  <div class="modal-header full-width">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add New Affiliate</h4>
	  </div>
	  <div class="modal-body full-width">
		<form role="form" class="tab-fields full-width" id="addNewAffiliateForm" method="post" action="">
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group tab-fields">
					<div class="form-group col-sm-4 col-xs-4">
						<label>Customer : </label>
						<input class="form-control" type="text" id="customerText" name="customerText">
						<input class="form-control" type="hidden" id="customerId" name="customerId">
						<label id="selectedItem"></label>
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>From Date : </label>
						<input class="form-control" type="text" id="fromDate" name="fromDate">
						<input class="form-control" type="hidden" id="action" name="action">
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>To Date</label>
						<input class="form-control" type="text" id="toDate" name="toDate">
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>Reward Type : </label>
						<select id="rewardType" name="rewardType" class="form-control input-sm m-bot15">
						 	<option value="CASH_REWARD">CASH REWARD</option>
							<option value="REWARD_DOLLARS">REWARD DOLLARS</option>	
						</select>
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>Reward Value : </label>
						<input type="text" class="form-control" id="rewardValue" name ="rewardValue"/>
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>Customer Lives : </label>
						<input type="text" class="form-control" id="livesCount" name ="livesCount"/>
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>Customer Reward Dollars : </label>
						<input type="text" class="form-control" id="custRewardDollar" name ="custRewardDollar"/>
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>Customer Superfan Stars : </label>
						<input type="text" class="form-control" id="custSuperFanStars" name ="custSuperFanStars"/>
					</div>
					<div class="form-group col-sm-4 col-xs-4">
						<label>Affiliate Lives : </label>
						<input type="text" class="form-control" id="affilaiteLivesCount" name ="affilaiteLivesCount"/>
					</div>
	  		</div>
		</div>
	</div>
	</form>
</div>
 <div class="modal-footer full-width text-center">
		<div class="col-xs-12 text-center">
			<button type="button" onclick="saveAffiliate();" id="saveBtn" class="btn btn-primary">Save</button>
			<button data-dismiss="modal" class="btn btn-cancel"  type="button">Close</button>
		</div>
	</div>
</div>
</div>
</div>




<div id="affiliateEarningModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Affiliate Earnings</h4>
		  </div>
		  <div class="modal-body full-width">
		  		<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Contest Affiliate
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Contest Affiliate</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Contest Affiliate Earnings</li>
						</ol>
					</div>
				</div>
				<br/>
				<div id="affilaiteEarningDiv">
					<div class="row">
						<section class="col-xs-12">
							<ul class="nav nav-tabs vcustnav" style="">
								<li id ="deafaultPending" class="active"><a id="pendingEarnigs" style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#pending">Pending</a></li>
								<li class=""><a id="completedEarnigs" style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#completed">Completed</a></li>
							</ul>
						</section>
					</div>
					<div class="full-width mt-20">
							<div class="tab-content">
								<div id="pending" class="tab-pane active">
									<div class="full-width" style="position: relative;">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Affiliate Referrals Pending</label>
												<div class="pull-right">
												<a href="javascript:resetAffiliatePendingGrid()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
												</div>
											</div>
											<div id="affiliatePendingGrid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="affiliatePendingPager" style="width: 100%; height: 10px;"></div>
										</div>
									</div>
								</div>
								<div id="completed" class="tab-pane active">
									<div class="full-width" style="position: relative;">
										<div class="table-responsive grid-table">
											<div class="grid-header full-width">
												<label>Affiliate Referrals Earnings</label>
												<div class="pull-right">
												<a href="javascript:resetAffiliateEarningGrid()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters</a>
												</div>
											</div>
											<div id="affiliateEarningGrid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
											<div id="affiliateEarningPager" style="width: 100%; height: 10px;"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
		 	<div class="modal-footer full-width text-center">
				<div class="col-xs-12 text-center">
					<button data-dismiss="modal" class="btn btn-cancel"  type="button">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="affiliateSettingModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Affiliate Earnings</h4>
		  </div>
		  <div class="modal-body full-width">
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label>Setting : </label>
							<input class="form-control" type="text" id="settingDigit" name="settingDigit">
						</div>
					</div>
				</div>
			</div>
		</div>
	 	<div class="modal-footer full-width text-center">
			<div class="col-xs-12 text-center">
				<button type="button" onclick="saveAffiliateSetting();" id="saveBtn" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-cancel"  type="button">Close</button>
			</div>
		</div>
		</div>
	</div>
</div>


<script>


function saveAffiliate(){
	var customerId = $('#customerId').val();
	var fromDate = $('#fromDate').val();
	var rewardType = $('#rewardType').val();
	var rewardValue = $('#rewardValue').val();
	var livesCount = $('#livesCount').val();
	var affilaiteLivesCount = $('#affilaiteLivesCount').val();
	var custRewardDollar = $('#custRewardDollar').val();
	var custSuperFanStars = $('#custSuperFanStars').val();
	
	
	if(customerId == ''){
		jAlert('Please select customer to created affiliate.');
		return;
	}
	if(fromDate == ''){
		jAlert('Please select from date.');
		return;
	}
	if(toDate == ''){
		jAlert('Please select to Date.');
		return;
	}
	if(rewardType == ''){
		jAlert('Please select reward type.');
		return;
	}
	if(rewardValue == ''){
		jAlert('Please add reward value.');
		return;
	}
	if(livesCount == ''){
		jAlert('Please Customer lives.');
		return;
	}
	if(affilaiteLivesCount == ''){
		jAlert('Please add affiliate lives.');
		return;
	}
	if(custRewardDollar == ''){
		jAlert('Customer Reward dollars are mendatory, possible value can be greater than equals to 0.');
		return;
	}
	if(custSuperFanStars == ''){
		jAlert('Customer Superfan Stars are mendatory, possible value can be greater than equals to 0.');
		return;
	}
	
	
	$.ajax({				  
		url : "${pageContext.request.contextPath}/UpdateContestAffiliate",
		type : "post",
		data : $("#addNewAffiliateForm").serialize(),
		success : function(response){
			if (response.status == 1) {
				jAlert(response.msg);
				$('#addAffiliate').modal('hide');
				getAffiliatesAllGridData(0);
			} else {
				jAlert(response.msg,"Info","Info");
				return false;
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
	
}

//Start Edit Affiliates
function editAffiliateDetails(customerId){
	if(customerId <= 0){
		jAlert("Please select Affiliate to Edit.");
		return;
	}
	$.ajax({		  
		url : "${pageContext.request.contextPath}/UpdateContestAffiliate",
		type : "post",
		data : "customerId="+customerId+"&action=EDIT",
		dataType:"json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData != null && jsonData != "") {
				if(jsonData.status == 1){
					setAffiliateDetailsForEdit(jsonData.affiliate);
				}
				else{
					jAlert(jsonData.msg);
				}
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}
	
function setAffiliateDetailsForEdit(affiliate){
	$('#addAffiliate').modal('show');
	$('#action').val('UPDATE');
	$('#saveBtn').text('Update');
	$('#selectedItem').text(affiliate.firstName);
	$('#fromDate').val(affiliate.startDateStr);
	$('#customerId').val(affiliate.customerId);
	$('#toDate').val(affiliate.endDateStr);
	$('#rewardType').val(affiliate.rewardType);
	$('#rewardValue').val(affiliate.rewardValue);
	$('#livesCount').val(affiliate.customerLives);
	$('#affilaiteLivesCount').val(affiliate.affiliateLives);
	$('#custRewardDollar').val(affiliate.customerRewardDollars);
	$('#custSuperFanStars').val(affiliate.customerSuperFanStars);
	$('#customerText').attr("disabled",true);
	
}


function updateStatus(status){
	var tempUserRowIndex = affiliatesAllGrid.getSelectedRows([0])[0];
	var customerId = affiliatesAllGrid.getDataItem(tempUserRowIndex).customerId;
	if(customerId <= 0){
		jAlert("Please select affiliate.");
		return;
	}
	$.ajax({		  
		url : "${pageContext.request.contextPath}/ChangeStatusContestAffiliate",
		type : "post",
		data : "customerId="+customerId+"&status="+status,
		dataType:"json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData != null && jsonData != "") {
				if(jsonData.status == 1){
					jAlert(jsonData.msg);
					getAffiliatesAllGridData(0);
				}
				else{
					jAlert(jsonData.msg);
				}
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


//Affiliates User Grid
var pagingInfo;
var customerId='';
var affiliatesAllGrid;
var affiliatesRowIndex;
var affiliatesAllDataView;
var affiliatesAllData=[];
var affiliatesAllGridSearchString='';
var columnFilters = {};
var affiliatesAllColumnsStr = '<%=session.getAttribute("contestAffiliatesAllGrid")%>';
var affiliatesAllColumns =[];

var loadAffiliatesAllColumns = ["customerType", "firstName", "lastName","userId","email", "phone","referralCode","fromDate","toDate","rewardType","rewardValue","customerLives",
                               "affiliateLives","activeCashReward","custRewardDollar","custSuperFanStar","debitedCashReward","totalCashReward","voidedCashReward","totalCreditedRewardDollar","editCol"];
var affiliateGridColumns = [
				 {id:"customerType", name:"Customer Type", field: "customerType", sortable: true, width: 50},
	               {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
	               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
	               {id:"userId", name:"User Id", field: "userId", sortable: true, width: 100},
	               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
	               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
	               {id:"referralCode", name:"Referral Code", field: "referralCode", width:100, sortable: true},
	               {id:"fromDate", name:"From Date", field: "fromDate", sortable: true},
	               {id:"toDate", name:"To Date", field: "toDate", sortable: true},
	               {id:"rewardType", name:"Reward Type", field: "rewardType", sortable: true},
	               {id:"rewardValue", name:"Reward Value", field: "rewardValue", sortable: true},
	               {id:"status", name:"Status", field: "status", sortable: true},
	               {id:"custRewardDollar", name:"Customer Reward Dollars", field: "custRewardDollar", sortable: true},
	               {id:"custSuperFanStar", name:"Customer Superfan Stars", field: "custSuperFanStar", sortable: true},
	               {id:"customerLives", name:"Customer Lives", field: "customerLives", sortable: true},
	               {id:"affiliateLives", name:"Affiliate Lives", field: "affiliateLives", sortable: true},
	               {id:"activeCashReward", name:"Active Cash", field: "activeCashReward", width:100, sortable: true},
	               {id:"debitedCashReward", name:"Debited Cash", field: "debitedCashReward", width:100, sortable: true},
	               {id:"totalCashReward", name:"Total Earned Cash", field: "totalCashReward", width:100, sortable: true},
	               {id:"voidedCashReward", name:"Voided Cash", field: "voidedCashReward", width:100, sortable: true},
	               {id:"totalCreditedRewardDollar", name:"Total Credited Reward Dollars", field: "totalCreditedRewardDollar", width:100, sortable: true},
	               {id:"createdBy", name:"Created By", field: "createdBy", width:100, sortable: true},
	               {id:"createdDate", name:"Created Date", field: "createdDate", width:100, sortable: true},
	               {id:"updatedBy", name:"Updated By", field: "updatedBy", width:100, sortable: true},
	               {id:"updatedDate", name:"Updated Date", field: "updatedDate", width:100, sortable: true},
	               {id: "editCol", field:"editCol", name:"Edit Customer", width:10, formatter:editFormatter}               
              ];
  
	if(affiliatesAllColumnsStr!='null' && affiliatesAllColumnsStr!=''){
		var columnOrder = affiliatesAllColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<affiliateGridColumns.length;j++){
				if(columnWidth[0] == affiliateGridColumns[j].id){
					affiliatesAllColumns[i] =  affiliateGridColumns[j];
					affiliatesAllColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadAffiliatesAllColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<affiliateGridColumns.length;j++){
				if(columnWidth == affiliateGridColumns[j].id){
					affiliatesAllColumns[i] = affiliateGridColumns[j];
					affiliatesAllColumns[i].width=80;
					break;
				}
			}			
		}
		//affiliatesAllColumns = affiliateGridColumns;
	}
  
var affiliateGridOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var affiliateSortCol = "customerId";
var affiliateSortdir = 1;
var percentCompleteThreshold = 0;

//function for edit functionality
function editFormatter(row, cell, value, columnDef, dataContext) {
	//the id is so that you can identify the row when the particular button is clicked	
	var button = "<img class='editClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.customerId +"'/>";
	return button;
}

//Function to hook up the edit button event
$('.editClickableImage').live('click', function() {
	var me = $(this), id = me.attr('id');
	editAffiliateDetails(id);
});

function affiliateComparer(a, b) {
	  var x = a[affiliateSortCol], y = b[affiliateSortCol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	  	if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

function pagingControl(move,id){
	if(id=='affiliates_allpager'){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getAffiliatesAllGridData(pageNo);
	}
	 if(id=='affiliateEarningPager'){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(aePagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(aePagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(aePagingInfo.pageNum)-1;
		}
		getAffiliateEarningGridData(0,'COMPLETED');
	}
	 if(id=='affiliatePendingPager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(aePagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(aePagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(aePagingInfo.pageNum)-1;
			}
			getAffiliateEarningGridData(0,'PENDING');
		} 
}

function getAffiliatesAllGridData(pageNo) {
	if($('#activeDiv').hasClass('active')){
		$('#affStatus').val('ACTIVE');
	}else if($('#inactiveDiv').hasClass('active')){
		$('#affStatus').val('INACTIVE');
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/ContestAffiliates.json",
		type : "post",
		dataType: "json",
		data : "pageNo="+pageNo+"&headerFilter="+affiliatesAllGridSearchString+"&affStatus="+$('#affStatus').val(),
		success : function(res){
			var jsonData = res;
			if(jsonData.status ==0){
				jAlert(msg);
				return;
			}
			pagingInfo = jsonData.pagingInfo;
			refreshAffiliatesAllGridValues(jsonData.affiliates);
			$('#customerId').val('');
			$('#affiliates_allpager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveAffiliateUserPreference()'>");
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function refreshAffiliatesAllGridValues(jsonData) {
	affiliatesAllData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (affiliatesAllData[i] = {});			
			d["id"] = i;
			d["affiliateId"] = data.id;
			d["customerId"] = data.customerId;
			d["firstName"] = data.firstName;
			d["lastName"] = data.lastName;
			d["customerType"] = data.customerType;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["referralCode"] = data.referralCode;
			d["fromDate"] = data.fromDateStr;
			d["toDate"] = data.toDateStr;
			d["rewardType"] = data.rewardType;
			d["rewardValue"] = data.rewardValue;
			d["status"] = data.status;
			d["customerLives"] = data.customerLives;
			d["affiliateLives"] = data.affiliateLives;
			d["activeCashReward"] = data.activeCashReward;
			d["debitedCashReward"] = data.debitedCashReward;
			d["totalCashReward"] = data.totalCashReward;
			d["voidedCashReward"] = data.voidedCashReward;
			d["totalCreditedRewardDollar"] = data.totalCreditedRewardDollar;
			d["createdBy"] = data.createdBy;
			d["createdDate"] = data.createdDateStr;
			d["updatedBy"] = data.updatedBy;
			d["updatedDate"] = data.updatedDateStr;
			d["custRewardDollar"] = data.customerRewardDollars;
			d["custSuperFanStar"] = data.customerSuperFanStars;
		}
	}
	affiliatesAllDataView = new Slick.Data.DataView();
	affiliatesAllGrid = new Slick.Grid("#affiliatesAllGrid", affiliatesAllDataView, affiliatesAllColumns, affiliateGridOptions);
	affiliatesAllGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = affiliatesAllGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  affiliatesAllGrid.setColumns(colTest);
	  }
	  affiliatesAllGrid.invalidate();
	  affiliatesAllGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(pagingInfo!=null){
		  var pager = new Slick.Controls.Pager(affiliatesAllDataView, affiliatesAllGrid, $("#affiliates_allpager"),pagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(affiliateGridColumns,affiliatesAllGrid, affiliateGridOptions);
	 
	  affiliatesAllGrid.onSort.subscribe(function (e, args) {
	    affiliateSortdir = args.sortAsc ? 1 : -1;
	    affiliateSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      affiliatesAllDataView.fastSort(affiliateSortCol, args.sortAsc);
	    } else {
	      affiliatesAllDataView.sort(affiliateComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the affiliatesAllGrid
	  affiliatesAllDataView.onRowCountChanged.subscribe(function (e, args) {
	    affiliatesAllGrid.updateRowCount();
	    affiliatesAllGrid.render();
	  });
	  affiliatesAllDataView.onRowsChanged.subscribe(function (e, args) {
	    affiliatesAllGrid.invalidateRows(args.rows);
	    affiliatesAllGrid.render();
	  });
	  
	  $(affiliatesAllGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	affiliatesAllGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  affiliatesAllGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getAffiliatesAllGridData(0);
				}
			  }
		 
		});
	  	affiliatesAllGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'editCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(columnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		affiliatesAllGrid.init();
		
		affiliatesAllGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = affiliatesAllGrid.getSelectedRows([0])[0];
			if (temprEventRowIndex != affiliatesRowIndex) {
				affiliatesRowIndex = temprEventRowIndex;
				customerId = affiliatesAllGrid.getDataItem(temprEventRowIndex).customerId;
				$('#customerId').val(customerId);
				//getAffiliateGridData(customerId,0);
			}
		});
	  	
		affiliatesAllGrid.onContextMenu.subscribe(function (e) {
	      e.preventDefault();
	      var cell = affiliatesAllGrid.getCellFromEvent(e);
	      affiliatesAllGrid.setSelectedRows([cell.row]);
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if(height < $("#contextMenu").height()){
				height = e.pageY - $("#contextMenu").height();
			}else{
				height = e.pageY;
			}
			if(width < $("#contextMenu").width()){
				width =  e.pageX- $("#contextMenu").width();
			}else{
				width =  e.pageX;
			}
		 $("#contextMenu")
	          .data("row", cell.row)
	          .css("top", height)
	          .css("left", width)
	          .show();
	      $("body").one("click", function () {
	        $("#contextMenu").hide();
	      });
	    }); 
		
	  affiliatesAllDataView.beginUpdate();
	  affiliatesAllDataView.setItems(affiliatesAllData);
	  
	  affiliatesAllDataView.endUpdate();
	  affiliatesAllDataView.syncGridSelection(affiliatesAllGrid, true);
	  $("#gridContainer").resizable();
	  $("div#divLoading").removeClass('show');
	  affiliatesAllGrid.resizeCanvas();
	  
	  
	  
	  $("#contextMenu").click(function(e){
			if (!$(e.target).is("li")) {
				return;
			}
			var row = affiliatesAllGrid.getSelectedRows([ 0 ])[0];
			if (row < 0) {
				jAlert("Please select Affiliate.");
				return;
			}
			if ($(e.target).attr("data") == 'view earnings') {
				var customerId = affiliatesAllGrid.getDataItem(row).customerId;
				showAffiliateEarningsModal(customerId,'PENDING');
			}
	  });
}


//affiliate Earning Grid
var aePagingInfo;
var affiliateEarningGrid;
var affiliateEarningDataView;
var affiliateEarningData=[];
var affiliatesErningGridSearchString='';
var aeColumnFilters = {};
var affiliateEarningColumnsStr = '<%=session.getAttribute("contestAffiliateEarningGrid")%>';
var affiliateEarningColumns =[];

var affiliateEarningGridOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};

var loadAffiliateEarningColumns = [ "firstName", "lastName","userId","email", "phone","rewardType","affiliateLives",
                                   "cashReward","rewardDollars","createdDate","updatedDate","status"]
var affiliateGridColumns = [
				/*  {id:"affilateUserId", name:"Affiliate UserId", field: "affilateUserId", sortable: true, width: 50}, */
	               {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
	               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
	               {id:"userId", name:"User Id", field: "userId", sortable: true, width: 100},
	               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
	               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
	              /*  {id:"referralCode", name:"Referral Code", field: "referralCode", sortable: true, width: 140}, */
	               {id:"rewardType", name:"Reward Type", field: "rewardType", sortable: true, width: 140},
	               /* {id:"customerLives", name:"Customer Lives", field: "customerLives", sortable: true, width: 140}, */
	               {id:"affiliateLives", name:"Affiliate Lives", field: "affiliateLives", sortable: true, width: 140},
	               {id:"cashReward", name:"Cash Reward", field: "cashReward", sortable: true, width: 140},
	               {id:"rewardDollars", name:"Reward Dollars", field: "rewardDollars", sortable: true, width: 140},
	               {id:"createdDate", name:"Created Date", field: "createdDate", sortable: true, width: 140},
	               {id:"updatedDate", name:"Updated Date", field: "updatedDate", sortable: true, width: 140},
	               {id:"status", name:"Status", field: "status", sortable: true, width: 140}
	                            
              ];
  
	if(affiliateEarningColumnsStr!='null' && affiliateEarningColumnsStr!=''){
		var columnOrder = affiliateEarningColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<affiliateGridColumns.length;j++){
				if(columnWidth[0] == affiliateGridColumns[j].id){
					affiliateEarningColumns[i] =  affiliateGridColumns[j];
					affiliateEarningColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder = loadAffiliateEarningColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<affiliateGridColumns.length;j++){
				if(columnWidth == affiliateGridColumns[j].id){
					affiliateEarningColumns[i] = affiliateGridColumns[j];
					affiliateEarningColumns[i].width=80;
					break;
				}
			}			
		}
		//affiliateEarningColumns = affiliateGridColumns;
	}

function refreshAffiliateEarningGridValues(jsonData) {
	affiliateEarningData=[];
	if(jsonData!=null){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (affiliateEarningData[i] = {});			
			d["id"] = i;
			d["firstName"] = data.firstName;
			d["lastName"] = data.lastName;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["rewardType"] = data.rewardType;
			d["affiliateLives"] = data.affiliateLives;
			d["cashReward"] = data.cashReward;
			d["rewardDollars"] = data.rewardDollars;
			d["createdDate"] = data.createdDateStr;
			d["updatedDate"] = data.updatedDateStr;
			d["status"] = data.status;
			
			
		}
	}
	affiliateEarningDataView = new Slick.Data.DataView();
	affiliateEarningGrid = new Slick.Grid("#affiliateEarningGrid", affiliateEarningDataView, affiliateEarningColumns, affiliateEarningGridOptions);
	affiliateEarningGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = affiliateEarningGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  affiliateEarningGrid.setColumns(colTest);
	  }
	  affiliateEarningGrid.invalidate();
	  affiliateEarningGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(aePagingInfo!=null){
		  var pager = new Slick.Controls.Pager(affiliateEarningDataView, affiliateEarningGrid, $("#affiliateEarningPager"),aePagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(affiliateGridColumns,affiliateEarningGrid, affiliateEarningGridOptions);
	 
	  affiliateEarningGrid.onSort.subscribe(function (e, args) {
	    affiliateSortdir = args.sortAsc ? 1 : -1;
	    affiliateSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      affiliateEarningDataView.fastSort(affiliateSortCol, args.sortAsc);
	    } else {
	      affiliateEarningDataView.sort(affiliateComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the affiliateEarningGrid
	  affiliateEarningDataView.onRowCountChanged.subscribe(function (e, args) {
	    affiliateEarningGrid.updateRowCount();
	    affiliateEarningGrid.render();
	  });
	  affiliateEarningDataView.onRowsChanged.subscribe(function (e, args) {
	    affiliateEarningGrid.invalidateRows(args.rows);
	    affiliateEarningGrid.render();
	  });
	  
	  $(affiliateEarningGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	affiliateEarningGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				aeColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in aeColumnFilters) {
					  if (columnId !== undefined && aeColumnFilters[columnId] !== "") {
						  affiliatesErningGridSearchString += columnId + ":" +aeColumnFilters[columnId]+",";
					  }
					}
					getAffiliateEarningGridData(0,'COMPLETED');
				}
			  }
		 
		});
	  	affiliateEarningGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'editCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(aeColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		});
		affiliateEarningGrid.init();
		
		affiliateEarningGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = affiliateEarningGrid.getSelectedRows([0])[0];
		});
	  	 
		
	  affiliateEarningDataView.beginUpdate();
	  affiliateEarningDataView.setItems(affiliateEarningData);
	  
	  affiliateEarningDataView.endUpdate();
	  affiliateEarningDataView.syncGridSelection(affiliateEarningGrid, true);
	  $("#gridContainer").resizable();
	  $("div#divLoading").removeClass('show');
	  affiliateEarningGrid.resizeCanvas();
}


function resetAffiliateEarningGrid(){
	affiliatesErningGridSearchString='';
	aeColumnFilters = {};
	getAffiliateEarningGridData(0,'COMPLETED');
}


//Affiliate Pending earning
var apPagingInfo;
var affiliatePendingGrid;
var affiliatePendingDataView;
var affiliatePendingData=[];
var affiliatesPendingGridSearchString='';
var aeColumnFilters = {};
var affiliatePendingColumnsStr = '<%=session.getAttribute("contestAffiliatePendingGrid")%>';
var affiliatePendingColumns =[];

var affiliatePendingGridOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};


var affiliatePendingGridColumns = [
	               {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
	               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
	               {id:"userId", name:"User Id", field: "userId", sortable: true, width: 100},
	               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
	               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
	               {id:"createdDate", name:"Created Date", field: "createdDate", sortable: true, width: 140},
	               {id:"signupDate", name:"Signup Date", field: "signupDate", sortable: true, width: 140},
	               {id:"playedDate", name:"Played Date", field: "playedDate", sortable: true, width: 140},
	               {id:"status1", name:"Status", field: "status1", sortable: true, width: 140}
	                            
              ];
  
	

function refreshAffiliatePendingGridValues(jsonData) {
	affiliatePendingData=[];
	if(jsonData!=null){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];			
			var d = (affiliatePendingData[i] = {});			
			d["id"] = i;
			d["firstName"] = data.firstName;
			d["lastName"] = data.lastName;
			d["userId"] = data.userId;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["createdDate"] = data.createdDateStr;
			d["signupDate"] = data.signupDateStr;
			d["playedDate"] = data.playedDateStr;
			d["status1"] = data.status;
			
			
		}
	}
	affiliatePendingDataView = new Slick.Data.DataView();
	affiliatePendingGrid = new Slick.Grid("#affiliatePendingGrid", affiliatePendingDataView, affiliatePendingGridColumns, affiliatePendingGridOptions);
	affiliatePendingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	var cols = affiliatePendingGrid.getColumns();
	
	 var colTest = [];
	  for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colTest.push(cols[c]);
		 // }  
		  affiliatePendingGrid.setColumns(colTest);
	  }
	  affiliatePendingGrid.invalidate();
	  affiliatePendingGrid.setSelectionModel(new Slick.RowSelectionModel());
	  if(apPagingInfo!=null){
		  var pager = new Slick.Controls.Pager(affiliatePendingDataView, affiliatePendingGrid, $("#affiliatePendingPager"),apPagingInfo);
	  }
	  var columnpicker = new Slick.Controls.ColumnPicker(affiliatePendingGridColumns,affiliatePendingGrid, affiliatePendingGridOptions);
	 
	  affiliatePendingGrid.onSort.subscribe(function (e, args) {
	    affiliateSortdir = args.sortAsc ? 1 : -1;
	    affiliateSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      affiliatePendingDataView.fastSort(affiliateSortCol, args.sortAsc);
	    } else {
	      affiliatePendingDataView.sort(affiliateComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the affiliatePendingGrid
	  affiliatePendingDataView.onRowCountChanged.subscribe(function (e, args) {
	    affiliatePendingGrid.updateRowCount();
	    affiliatePendingGrid.render();
	  });
	  affiliatePendingDataView.onRowsChanged.subscribe(function (e, args) {
	    affiliatePendingGrid.invalidateRows(args.rows);
	    affiliatePendingGrid.render();
	  });
	  
	  $(affiliatePendingGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	affiliatePendingGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				apColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in apColumnFilters) {
					  if (columnId !== undefined && apColumnFilters[columnId] !== "") {
						  affiliatesPendingGridSearchString += columnId + ":" +apColumnFilters[columnId]+",";
					  }
					}
					getAffiliateEarningGridData(0,'PENDING');
				}
			  }
		 
		});
	  	affiliatePendingGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			//if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'editCol'){
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(aeColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			//}
		});
		affiliatePendingGrid.init();
		
		affiliatePendingGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprEventRowIndex = affiliatePendingGrid.getSelectedRows([0])[0];
		});
	  	 
		
	  affiliatePendingDataView.beginUpdate();
	  affiliatePendingDataView.setItems(affiliatePendingData);
	  
	  affiliatePendingDataView.endUpdate();
	  affiliatePendingDataView.syncGridSelection(affiliatePendingGrid, true);
	  $("#gridContainer").resizable();
	  $("div#divLoading").removeClass('show');
	  affiliatePendingGrid.resizeCanvas();
}


function resetAffiliatePendingGrid(){
	affiliatesPendingGridSearchString='';
	apColumnFilters = {};
	getAffiliateEarningGridData(0,'PENDING');
}




function showAffiliateEarningsModal(customerId,status){
	$('#affiliateEarningModal').modal('show');
	if($('#pending').hasClass('active')){
		$('#completed').hide();
		status= "PENDING";
	}else if($('#completed').hasClass('active')){
		status= "COMPLETED";
		$('#pending').hide();
	}
	getAffiliateEarningGridData(customerId,status);
	
}
function getAffiliateEarningGridData(customerId,status){
	if(customerId == 0){
		var index = affiliatesAllGrid.getSelectedRows([0])[0];
		customerId = affiliatesAllGrid.getDataItem(index).customerId;
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/ContestAffiliatesEarning",
		type : "post",
		dataType: "json",
		data : "customerId="+customerId+"&headerFilter="+affiliatesErningGridSearchString+"&referralStatus="+status,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.status==1){
				if(status == 'COMPLETED'){
					setTimeout(function(){
						aePagingInfo = jsonData.pagingInfo;
						refreshAffiliateEarningGridValues(jsonData.earnings);
					},500);
				}else if(status == 'PENDING'){
					setTimeout(function(){
						apPagingInfo = jsonData.pagingInfo;
						refreshAffiliatePendingGridValues(jsonData.earnings);
					},500);
				}
				
			}else{
				refreshAffiliateEarningGridValues(jsonData.earnings);
				refreshAffiliatePendingGridValues(jsonData.earnings);
				jAlert(jsonData.msg);
				return;
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}



function saveAffiliateUserPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = affiliatesAllGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('contestAffiliatesAllGrid',colStr);
}

function saveAffiliateEarnigPreference(){
	var cols = visibleColumns;
	if(cols==null || cols =='' || cols.length==0){
		cols = affiliateEarningGrid.getColumns();
	}
	var colStr = '';
	for(var i=0;i<cols.length;i++){
		colStr += cols[i].id+":"+cols[i].width+",";
	}
	saveUserPreference('contestAffiliatesEarningGrid',colStr);
}

</script>