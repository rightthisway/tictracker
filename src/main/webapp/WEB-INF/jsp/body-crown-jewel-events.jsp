<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script type="text/javascript">
	
</script>

<style>
input {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.fullWidth {
    width: 100%; 
}
.list-group-item {
	border:0px;
}

  
</style>

<script type="text/javascript">
var jq2 = $.noConflict(true);
function callEventAutoComplete() {
	jq2('.autoEvent').autocomplete("${pageContext.request.contextPath}/AutoCompleteEvents", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			
		}
	}).result(function (event,row,formatted){
		  var rowId = $(this).attr('id').split("_")[1];
		  
			$('#event_'+rowId).val(row[2]);
			//$('#tmatEventText_'+rowId).text(row[2]);
			$('#eventId_'+rowId).val(row[1]);
			//$('#tmatEventText_'+rowId).show();
			$('#checkbox_'+rowId).attr('checked', true);
	});
 }
$(document).ready(function(){
	
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllEvent').click(function(){
		if($('#copyAllEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('name');
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	/*
	$('#copyAllTMAT').click(function(){
		if($('#copyAllTMAT').attr('checked')){
			$('.selectCheck').attr('checked', true);
			//copyTextField('eventId');
			setAllTMATEvent();
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});*/
	$('#copyAllChild').click(function(){
		if($('#copyAllChild').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('childCategoryType');
			setAllGrandChildCategory();
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllGrandChild').click(function(){
		if($('#copyAllGrandChild').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('grandChildCategoryType');
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyPackage').click(function(){
		if($('#copyPackage').attr('checked')){
			$('.selectCheck').attr('checked', true);
			$('.selectPackage').attr('checked', true);
		}else{
			$('.selectPackage').attr('checked', false);
		}
	});
	
	callEventAutoComplete();
	
});

function callSelectRow(rowNumber) {
	$('#checkbox_'+rowNumber).attr('checked', true);
}

function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else {
			$("#" + id ).val(firstFieldValue);
			if(fieldName == "grandChildCategoryType"){				
				if(firstFieldValue != $("#" + id ).val()){					
					jAlert("Selected value has not set some of the grand child.");
					$("#"+id).focus();
				}
			}
		}
	});
}

function setAllGrandChildCategory(){
	for(var i=2;i<=rCount;i++){
		loadGrandChildCategory("grandChildCategoryType_"+i,"",i);
	}
}
/*
function setAllTMATEvent(){
	for(var i=2;i<=rCount;i++){
		if($('#eventTextbox_'+i).css('display') == 'none'){
			 $('#eventTextbox_'+i).show();
			 $('#changeLink_'+i).text('Close');
			 $('#oldEventId_'+i).val($('#eventId_1').val());
			 $('#tmatEventText_'+i).text($('#tmatEventText_1').text());
			 $('#event_'+i).val($('#event_1').val());
		}	
	}
}
*/
 function callSaveBtnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
			
		var id,value;
		id = this.id.replace('checkbox','name');
		value = $.trim($("#"+id).val());
		if(value==''){
			jAlert('Please enter valid event name.');
			$("#"+id).focus();
			flag = false;
			return false;
		}
		id = this.id.replace('checkbox','childCategoryType');
		value = $.trim($("#"+id).val());
		if(value == '-1'){
			jAlert('Please select Child Category.');
			$("#"+id).focus();
			flag = false;
			return false;
		}
		id = this.id.replace('checkbox','grandChildCategoryType');
		value = $.trim($("#"+id).val());
		if(value == '-1'){
			jAlert('Please select Grandchild Category.');
			$("#"+id).focus();
			flag = false;
			return false;
		}
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one event to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
		jConfirm("Are you sure you want to save selected events ?.","Confirm",function(r){
			if(r) {
				$("#action").val(action);
				$("#leagueForm").submit();
			 }
		});
	}
 }
 
 function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
			
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one Existing event to delete.');
		flag = false;
		return false;
	}
	if(flag) {
		jConfirm("Are you sure you want to remove selected events and associated teams ?.","Confirm",function(r){
			if(r) { 
				$("#action").val(action);
				$("#leagueForm").submit();
			 }
		});
	}
 }
 function callParentTypeOnChange(action) {
	 $('#gridDiv').hide();
	 $("#action").val(action);
	 $("#leagueForm").submit();
 }
 function eventOnchange(rowId) {
	 $("#eventId_"+rowId).val('');
	 callSelectRow(rowId);
 }
 function editLeagueEvent(rowId){
	if($('#eventTextbox_'+rowId).css('display') == 'none'){
		 $('#eventTextbox_'+rowId).show();
		 $('#oldEventId_'+rowId).val($('#eventId_'+rowId).val());
		 $('#changeLink_'+rowId).hide();
		 $('#eventLabel_'+rowId).empty();
		 $('#event_'+rowId).val('');
		 $('#eventId_'+rowId).val('');
	 }
	 callSelectRow(rowId);
 }

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="#">Fantasy Sports Tickets</a></li>
				<li><i class="fa fa-laptop"></i>Events</li>						  	
			</ol>
		</div>
</div>

<div class="full-width">
<div class="row">

<div class="alert alert-success fade in" id="infoMainDiv"
<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form  name="leagueForm" id="leagueForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelEvents" >
	<input type="hidden" id="action" name="action" />
	<div class="row">
		<div class="form-group full-width">
          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Parent Type</label>
          <div class="col-lg-4 col-md-4 col-xs-7">
          <label>SPORTS</label>
          </div>
      </div>
     </div>
	  
	<div class="row">   
		<div class="form-group full-width">
		  <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Child Type</label>
		  <div class="col-lg-4 col-md-4 col-xs-7">
			<select name="childType" id="childType" class="form-control input-sm m-bot15 full-width" onchange="loadGrandChild('search');" style="max-width:200px">		
					<option value="">--Select--</option>			
					<%-- <option value="0" <c:if test="${childId ne null and '0' eq childId}"> selected </c:if>>ALL</option> --%>
				<c:forEach items="${childCategories}" var="childCat">
					<option value="${childCat.id}" <c:if test="${childId ne null and childCat.id eq childId}"> selected </c:if>>${childCat.name}</option>
				</c:forEach>
			</select>
		  </div>
		</div>
	</div>
	
	<c:if test="${childId ne null and childId ne '0' and childId ne '-1'}">
	<div class="row">     
	      <div class="form-group full-width">
	          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Grand Child Type</label>
	          <div class="col-lg-4 col-md-4 col-xs-7">
	          	<select name="grandChildType" id="grandChildType" class="form-control input-sm m-bot15" onchange="callParentTypeOnChange('search');" style="max-width:200px">
	          		<option value="">--Select--</option>
					<%-- <option value="0" <c:if test="${grandChildId ne null and '0' eq grandChildId}"> selected </c:if>>ALL</option> --%>
					<c:forEach items="${grandChildCategories}" var="grandChildCat">							
						<option value="${grandChildCat.id}" <c:if test="${grandChildId ne null and grandChildCat.id eq grandChildId}"> selected </c:if>>${grandChildCat.name}</option>
					</c:forEach>
				</select>				
	          </div>
	      </div>
      </div>
	 </c:if>
	
    <div class="row clearfix">
		<div class="col-md-12 column" id="gridDiv">
		<!-- 
			<div class="pull-right">
				<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
			</div>	
			<br />
			<br />-->
		  <c:choose>		  	
            <c:when test="${not empty grandChildId}">
            	<div class="pull-right mb-20">
				<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				<a href="javascript:exportToExcel()" style="float: right; margin-left: 10px;">Export to Excel</a>
				</div>
				
				<div class="table-responsive full-width">
				<table class="table table-bordered table-hover" id="tab_logic" align="center">
					<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-2">
							Event Name<br />
							<input type="checkbox" name="copyAllEvent" id="copyAllEvent" >
						</th>
						<th class="col-lg-3">
							Final - TMAT Event <!-- <br />
							<input type="checkbox" name="copyAllTMAT" id="copyAllTMAT" > -->							
						</th>
						<th class="col-lg-2">
							Child Category<br />
							<input type="checkbox" name="copyAllChild" id="copyAllChild" >
						</th>
						<th class="col-lg-2">
							Grand Child Category<br />
							<input type="checkbox" name="copyAllGrandChild" id="copyAllGrandChild" >
						</th>
						<th class="col-lg-2">
							Calculate only by package<br />
							<input type="checkbox" name="copyPackage" id="copyPackage" >
						</th>
						<th class="col-lg-1">
							Last Updated By
						</th>
						<th class="col-lg-1">
							Last Updated
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty leaguesList}">
				 <c:forEach var="cjLeague" varStatus="vStatus" items="${leaguesList}">
                      <tr id="leagueRow_${vStatus.count}">
                      <td>
                      	<input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" />
                      	<input type="hidden" name="rowNumber_${vStatus.count}" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
						<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjLeague.id}" />
                      </td>
						<td style="font-size: 13px;" align="center">
                            <div class="form-group">
                                	<div >
                               		<input class="form-control input-sm m-bot15 fullWidth" type="text" name="name_${vStatus.count}" id="name_${vStatus.count}" value="${cjLeague.name}"  onchange="callSelectRow('${vStatus.count}');">
                               	 </div>
                            </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group">
                              <c:if test = "${cjLeague.eventId ne null}">
                                                               <input type="hidden" id="oldEventId_${vStatus.count}" name="oldEventId_${vStatus.count}">
                                <input type="hidden" id="eventId_${vStatus.count}" name="eventId_${vStatus.count}" value ="${cjLeague.eventId}">
                               	<div id="eventLabel_${vStatus.count}">
                               			<b><label for="EventText" class="list-group-item" id="tmatEventText_${vStatus.count}">
                               			${cjLeague.venueString} 
                               			</label></b> 
                               			<a href="javascript:editLeagueEvent('${vStatus.count}')" id="changeLink_${vStatus.count}">Change</a>
                               	 </div>
                              	 <div id="eventTextbox_${vStatus.count}" style="display:none">
                              	 	<input class="form-control input-sm m-bot15 autoEvent fullWidth" type="text" name="event_${vStatus.count}" id="event_${vStatus.count}"  onchange="eventOnchange('${vStatus.count}')";/>
                              	 </div>
                              </c:if>
                              <c:if test = "${empty cjLeague.eventId}">
                                <input type="hidden" id="eventId_${vStatus.count}" name="eventId_${vStatus.count}" value ="">
                              	 <div id="eventTextbox_${vStatus.count}">
                              	 	<input class="form-control input-sm m-bot15 autoEvent fullWidth" type="text" name="event_${vStatus.count}" id="event_${vStatus.count}"  onchange="eventOnchange('${vStatus.count}')";/>
                              	 </div>
                              </c:if>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group">
                               	<div>
                               		<select name="childCategoryType_${vStatus.count}" id="childCategoryType_${vStatus.count}" class="form-control input-sm m-bot15" onchange="loadGrandChildCategory('grandChildCategoryType_${vStatus.count}','','${vStatus.count}'); callSelectRow('${vStatus.count}');">
									<b>	
									<option value="-1">--Select--</option>														
									<c:forEach var="childCategory" items="${childCategories}">
										<option value="${childCategory.id}" <c:if test="${cjLeague.childId ne null and cjLeague.childId eq childCategory.id}"> selected </c:if>> ${childCategory.name}</option>
									</c:forEach>																			
									</b>
									</select>
                               	 </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group">
                               	<div>
                               		<select name="grandChildCategoryType_${vStatus.count}" id="grandChildCategoryType_${vStatus.count}" class="form-control input-sm m-bot15" onchange="callSelectRow('${vStatus.count}');">
									<b>
									<option value="-1">--Select--</option>
									<c:forEach var="grandChild" items="${cjLeague.grandChildCategory}">
										<option value="${grandChild.id}" <c:if test="${cjLeague.grandChildId ne null and cjLeague.grandChildId eq grandChild.id}"> selected </c:if>> ${grandChild.name} </option>
									</c:forEach>									
									</b>
									</select>
                               	 </div>
                              </div>
						</td>
						<td style="vertical-align: middle;">
						<input type="checkbox" class="selectPackage" id="packageApplicable_${vStatus.count}" name="packageApplicable_${vStatus.count}"
										onclick="callSelectRow('${vStatus.count}');" <c:if test="${cjLeague.isPackageCost}">checked="checked" </c:if> />
						</td>
						<td style="font-size: 13px;" align="center">
                            <div class="form-group">
                                	<div >
                               		<label id="lastUpdatedBy_${vStatus.count}">${cjLeague.lastUpdateddBy}</label>
                               	 </div>
                            </div>
						</td>
						<td style="font-size: 13px;" align="center">
                            <div class="form-group">
                                	<div >
                               		<label id="lastUpdated_${vStatus.count}">${cjLeague.lastUpdatedStr}</label>
                               	 </div>
                            </div>
						</td>
						</tr>
					</c:forEach>
					</c:if>
				</tbody>
			</table>
			</div>
			</c:when>
			<c:otherwise>
			<%-- <c:if test="${parentType ne null}">	
			<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic" align="center">
				<thead>
					<tr >
						<th class="col-lg-1">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-2">
							Artist/Team Name<br />
							<input type="checkbox" name="copyAllEvent" id="copyAllEvent" >
						</th>
						<th class="col-lg-7">
							TMAT Artist
						</th>
						<!-- <th class="col-lg-3">
							Fantasy Category
						</th> -->
						<th class="col-lg-1">
							Last Updated By
						</th>
						<th class="col-lg-1">
							Last Updated
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty leaguesList}">
				 <c:forEach var="cjLeague" varStatus="vStatus" items="${leaguesList}">
                      <tr id="leagueRow_${vStatus.count}">
                      	<td>
                     		<input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" />
                     		<input type="hidden" name="rowNumber_${vStatus.count}" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
							<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjLeague.id}" /> 
                   		</td>
                   		<td style="font-size: 13px;" align="center">
                            <div class="form-group">
                                	<div >
                               		<input class="form-control input-sm m-bot15 fullWidth" type="text" name="name_${vStatus.count}" id="name_${vStatus.count}" value="${cjLeague.name}"  onchange="callSelectRow('${vStatus.count}');">
                               	 </div>
                            </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group">
                               	<div >
                               		<input class="form-control input-sm m-bot15 autoArtist fullWidth" type="text" name="artist_${vStatus.count}" id="artist_${vStatus.count}" value="${cjLeague.artist.name}" onchange="artistOnchange('${vStatus.count}');"/>
                               		<input type="hidden" id="artistId_${vStatus.count}" name="artistId_${vStatus.count}" value ="${cjLeague.artist.id}">
                               		<b><label for="ArtistText" class="list-group-item" id="tmatArtistText_${vStatus.count}"><c:if test="${cjLeague.artist ne null}">${cjLeague.artist.name}</c:if></label></b>
                               	 </div>
                              </div>
						</td>
						<!-- <td style="font-size: 13px;" align="center">
                              <div class="form-group">
                               	<div >
                               		<label for="imageText" class="col-lg-3 list-group-item"><b>															
									<c:forEach var="childCategory" items="${childCategoryList}">
										<c:if test="${cjLeague.childId ne null and cjLeague.childId eq childCategory.id}"> ${childCategory.name} </c:if>
									</c:forEach>
									<c:forEach var="grandChild" items="${cjLeague.grandChildCategory}">
										<option value="gc_${grandChild.id}" <c:if test="${cjLeague.grandChildId ne null and cjLeague.grandChildId eq grandChild.id}"> selected </c:if>> ${grandChild.name} </option>
									</c:forEach>									
									</b> </label>
                               	 </div>
                              </div>
						</td> -->
						<td style="font-size: 13px;" align="center">
                            <div class="form-group">
                                	<div >
                               		<label id="lastUpdatedBy_${vStatus.count}">${cjLeague.lastUpdateddBy}</label>
                               	 </div>
                            </div>
						</td>
						<td style="font-size: 13px;" align="center">
                            <div class="form-group">
                                	<div >
                               		<label id="lastUpdated_${vStatus.count}">${cjLeague.lastUpdatedStr}</label>
                               	 </div>
                            </div>
						</td>
						</tr>
					</c:forEach>
					</c:if>
				</tbody>
			</table>
			</div>
			</c:if> --%>
			</c:otherwise>
			</c:choose>
			
			<c:if test="${not empty grandChildId}">
				<a id="add_row" onclick="addNewLeagueRow();" class="btn btn-default btn-sm pull-left">Add Row</a>
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
			</c:if>
			<%-- <c:if test="${parentType ne null and parentType ne 'SPORTS'}">
				<a id="add_row" onclick="addNewLeagueRow();" class="btn btn-default btn-sm pull-left">Add Row</a>
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-danger btn-sm">Delete</button>
				</div>
			</c:if> --%>
		</div>
		
		</div>
</form>
</div>
<script type="text/javascript">
var rCount=$('#tab_logic tr').length;
var k = rCount;
function addNewLeagueRow() {
	
	rCount++;
	$('#tab_logic').append('<tr id="leagueRow_'+(rCount)+'"></tr>');
	
		
	//$('.autoArtist').autocomplete(autocomp_opt);
   var rowHtml = '';
   rowHtml = rowHtml + "<td>"+
		       "<input type='checkbox' class='selectCheck' id='checkbox_"+rCount+"' name='checkbox_"+rCount+"' />"+
		       "<input type='hidden' name='rowNumber_"+rCount+"' id='rowNumber_"+rCount+"' value="+rCount+"/>"+
               "<input type='hidden' name='id_"+rCount+"' id='id_"+rCount+"' />"+
		       "</td>";
	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
       "<div class='form-group' >"+
       "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='name_"+rCount+"' id='name_"+rCount+"' onchange='callSelectRow("+rCount+");' >"+
       "</div></div></td>";
       
	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
       "<div class='form-group' >"+
       "<div ><input class='form-control input-sm m-bot15 autoEvent fullWidth' type='text' name='event_"+rCount+"' id='event_"+rCount+"'  onchange='eventOnchange("+rCount+");'>"+
       "<input type='hidden' id='eventId_"+rCount+"' name='eventId_"+rCount+"' />"+
       "<input type='hidden' id='oldEventId_"+rCount+"' name='oldEventId_"+rCount+"' />"+
	//"<b><label for='EventText' class='list-group-item' id='tmatEventText_"+rCount+"' style='display: none;'></label></b>"+
       "</div></div></td>";
	
	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'>"+
	"<div class='form-group'> <div>"+
	"<select name='childCategoryType_"+rCount+"' id='childCategoryType_"+rCount+"' class='form-control input-sm m-bot15' onchange=loadGrandChildCategory('grandChildCategoryType_"+rCount+"','',"+rCount+");>"+
	"<option value='-1'>--Select--</option>"+
	"<c:forEach items='${childCategories}' var='childCat'>"+
		"<option value='${childCat.id}'> ${childCat.name}</option>"+
	"</c:forEach>"+
	"</select>"+
	"</div></div></td>";
	
	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'>"+
	"<div class='form-group'> <div>"+
	"<select name='grandChildCategoryType_"+rCount+"' id='grandChildCategoryType_"+rCount+"' class='form-control input-sm m-bot15' onchange='callSelectRow("+rCount+");' >"+										
	"<option value='-1'>--Select--</option>"+		
	"</select>"+
	"</div></div></td>";
	
	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'>"+
	"<div class='form-group'> <div>"+
	"<input type='checkbox' class='selectPackage' id='packageApplicable_"+rCount+"' name='packageApplicable_"+rCount+"'	onclick='callSelectRow("+rCount+");' >";+
	"</div></div></td>";
	
	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'>"+
       "<div class='form-group'> <div >"+
  		"<label id='lastUpdatedBy_"+rCount+"'></label>"+
  	 	"</div> </div> </td>";
  	 	
  	 	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'>"+
       "<div class='form-group'> <div >"+
  		"<label id='lastUpdated_"+rCount+"'></label>"+
  		"</div> </div> </td>";
  		
	$('#leagueRow_'+rCount).html(rowHtml);
	callEventAutoComplete();
	
}

var teamTable = document.getElementById("tab_logic");
if(teamTable != null && teamTable != '' && $('#tab_logic tr').length <= 1) {
	addNewLeagueRow();	
}

function loadChild(action){
	 $('#gridDiv').hide();
	 $("#action").val(action);
	 $("#leagueForm").submit();
}

function loadGrandChild(action){
	 $('#gridDiv').hide();
	 $("#action").val(action);
	 $('#grandChildType').val('');
	 $("#leagueForm").submit();
}

function loadGrandChildCategory(id,name,rowcount){
	var childIdStr = 0;	
	childIdStr = $('#childCategoryType_'+rowcount).val();
	if(childIdStr!=''){
		$.ajax({
			url : "${pageContext.request.contextPath}/GetFantasyGrandChildCategory",
			type : "post",
			dataType:"json",
			data : "childIdStr="+childIdStr,
			success : function(res){
				var respData = JSON.parse(JSON.stringify(res));
				var jsonData = respData.fantasyGrandChildCategoryList;
				updateGrandChildCategoryCombo(jsonData,id,name);
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}else{
		$('#'+id).empty();
		$('#'+id).append("<option value='-1'>--select--</option>");
	}
}

function updateGrandChildCategoryCombo(jsonData,id,childName){
	$('#'+id).empty();
	$('#'+id).append("<option value='-1'>--select--</option>");
	if(jsonData!='' && jsonData!=null){
		for(var i=0;i<jsonData.length;i++){
			$('#'+id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
		}
		if(childName!=''){
			$("#"+id+" option:contains(" + childName + ")").attr('selected', 'selected');
		}
	}
}

function exportToExcel(){
	var childType = $('#childType').val();
	var grandChildType = $('#grandChildType').val();

	var appendData = "parentType=SPORTS";
	if(childType != null || childType != '' || childType != undefined){
		appendData += "&childType="+$('#childType').val();
	}
	if(grandChildType != null || grandChildType != '' || grandChildType != undefined){
		appendData += "&grandChildType="+$('#grandChildType').val();
	}else{
		jAlert("Please Select Grand Child Category.");
		return
	}	
    var url = apiServerUrl + "CrownJewelTicketsExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

/* $("#leagueRow_1").clone().find("input,label").each(function() {       
	
	currentId=this.id;
    var splitedTag = currentId.split('_');
    $(this).attr('id',splitedTag[0] +"_"+k);
    $(this).attr('name',splitedTag[0] +"_"+k);
    currentId=this.id;	        

    if(this.tagName == 'INPUT') {
        if(($(this).attr('name')) == ('rowNumber_'+k)) {
      		$(this).attr('value',k);
      		
        } else {
     	   $(this).attr('value','');
        } 
     } else if(this.tagName == 'LABEL') {
            	$(this).text('');
     }		       
}).end().appendTo("#tab_logic");   */

</script>
