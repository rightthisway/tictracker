<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenu1 {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu1 li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu1 li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var questionArray = [];
var j = 0;
var jq2 = $.noConflict(true);
$(document).ready(function() {
	$("#activeSeller1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#inActiveSeller1").click(function(){
		callTabOnChange('INACTIVE');
	});
		
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(sellerGrid != null && sellerGrid != undefined){
			sellerGrid.resizeCanvas();
		}
	});
	
	
	<c:choose>
	<c:when test="${status == 'ACTIVE'}">	
		$('#activeSeller').addClass('active');
		$('#activeSellerTab').addClass('active');
	</c:when>
	<c:when test="${status == 'INACTIVE'}">
		$('#inActiveSeller').addClass('active');
		$('#inActiveSellerTab').addClass('active');
	</c:when>
	</c:choose>
});



function resetModal(){
	$('#sellerModal').modal('show');
	$('#sellerId').val('');
	$('#fName').val('');
	$('#lName').val('');
	$('#compName').val('');
	$('#email').val('');	
	$('#phone').val('');	
	$('#addLine1').val('');
	$('#addLine2').val('');
	$('#city').val('');
	$('#state').val('');
	$('#country').val('');
	$('#pincode').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();
}

function sellerSave(action){
	
	var fName = $('#fName').val();
	var lName = $('#lName').val();
	var compName = $('#compName').val();	
	var email = $('#email').val();
	var phone = $('#phone').val();
	var addLine1 = $('#addLine1').val();
	var addLine2 = $('#addLine2').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var country = $('#country').val();
	var pincode = $('#pincode').val();
	

	
	
	if(fName == ''){
		jAlert("First name is mendatory.");
		return;
	}
	if(lName == ''){
		jAlert("Last name is mendatory.");
		return;
	}
	if(compName == ''){
		jAlert("Company name is mendatory.");
		return;
	}
	if(email == ''){
		jAlert("Email is mendatory.");
		return;
	}
	
	
	var requestUrl = "${pageContext.request.contextPath}/ecomm/UpdateSeller";
	var dataString = "";
	if(action == 'SAVE'){		
		dataString  = $('#sellerForm').serialize()+"&status=${status}&action=SAVE";
	}else if(action == 'UPDATE'){
		dataString = $('#sellerForm').serialize()+"&status=${status}&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.sts == 1){
				$('#sellerModal').modal('hide');
				pagingInfo = jsonData.pagingInfo;
				columnFilters = {};
				refreshSellerGridValues(jsonData.sellerList);
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}


function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ecomm/ManageSellers?status="+selectedTab;
}


</script>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Admin</a></li>
			<li><i class="fa fa-laptop"></i>Manage Sellers</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activeSellerTab" class=""><a id="activeSeller1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activeSeller">Active Sellers</a></li>
				<li id="inActiveSellerTab" class=""><a id="inActiveSeller1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#inActiveSeller">InActive Sellers</a></li>
			</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activeSeller" class="tab-pane">
			<c:if test="${status =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary"  onclick="resetModal();">Add Seller</button>
					<button type="button" class="btn btn-primary" onclick="editSeller();">Edit Seller</button>
					<button type="button" class="btn btn-primary" onclick="changeStatus('INACTIVE');">InActivate Seller</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Active Sellers</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="sellers_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="sellers_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
				</c:if>
			</div>
			<div id="inActiveSeller" class="tab-pane">
			<c:if test="${status =='INACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary" onclick="changeStatus('ACTIVE');">Move to Active</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>InActive Sellers</label>
							<div class="pull-right">
								<a href="javascript:exportToExcel('INACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="sellers_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="sellers_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br />
			</c:if>
			</div>																				
		</div>
	</div>	
</div>


<!-- Add Contest -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sellerModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Seller</h4>
			</div>
			<div class="modal-body full-width">
				<div class="form-group tab-fields">
					<form name="sellerForm" id="sellerForm" method="post">
					<input type="hidden" id="sellerId" name="sellerId" />
					<input type="hidden" id="status" name="${status}" />
						<div class="form-group col-sm-6 col-xs-6">
							<label>First Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="fName" name="fName" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Last Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="lName" name="lName" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Company Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="compName" name="compName" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Email<span class="required">*</span>
							</label> <input class="form-control" type="text" id="email" name="email" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Phone<span class="required">*</span>
							</label> <input class="form-control" type="text" id="phone" name="phone" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Address line1
							</label> <input class="form-control" type="text" id="addLine1" name="addLine1" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Address line2
							</label> <input class="form-control" type="text" id="addLine2" name="addLine2" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>City
							</label> <input class="form-control" type="text" id="city" name="city" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>State
							</label> <input class="form-control" type="text" id="state" name="state" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Country
							</label> <input class="form-control" type="text" id="country" name="country" >
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Zipcode
							</label> <input class="form-control" type="text" id="pincode" name="pincode" >
						</div>	
					</form>
				</div>
			</div>
			<div class="modal-footer full-width">				
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="sellerSave('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="sellerSave('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	//Contest Grid
	function getSellerGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/ManageSellers.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+sellerSearchString+"&status=${status}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshSellerGridValues(jsonData.sellerList);
				clearAllSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcel(type){
		var appendData = "headerFilter="+sellerSearchString+"&status=";
	    var url = apiServerUrl+"ContestExportToExcel?"+appendData;
	    //$('#download-frame').attr('src', url);
	}
	
	
	function resetFilters(){
		sellerSearchString='';
		columnFilters = {};
		getSellerGridData(0);
		//refreshQuestionGridValues('');
	}

	/* var sellerCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var pagingInfo;
	var sellerDataView;
	var sellerGrid;
	var sellerData = [];
	var sellerGridPager;
	var sellerSearchString='';
	var columnFilters = {};
	var userSellerColumnsStr = '<%=session.getAttribute("sellergrid")%>';

	var userSellerColumns = [];
	var loadSellerColumns = ["sellerId","firstName", "lastName","compName", "email", "phone","addr1","addr2",
	                           "city", "state","country", "zip","status","updatedDate", "updatedBy","editSeller","delSeller"];
	var allSellerColumns = [ 
			{
				id : "sellerId",
				name : "Seller ID",
				field : "sellerId",
				width : 80,
				sortable : true
			},{
				id : "firstName",
				name : "First Name",
				field : "firstName",
				width : 80,
				sortable : true
			}, {
				id : "lastName",
				name : "Last Name",
				field : "lastName",
				width : 80,
				sortable : true
			},{
				id : "compName",
				name : "Company Name",
				field : "compName",
				width : 80,
				sortable : true
			},{
				id : "email",
				name : "Email",
				field : "email",
				width : 80,
				sortable : true
			},{
				id : "phone",
				name : "Phone",
				field : "phone",
				width : 80,
				sortable : true
			},{
				id : "addr1",
				name : "Address line1",
				field : "add1",
				width : 80,
				sortable : true
			},{
				id : "addr2",
				name : "Address line2",
				field : "addr2",
				width : 80,
				sortable : true
			},{
				id : "city",
				name : "City",
				field : "city",
				width : 80,
				sortable : true
			},{
				id : "state",
				name : "State",
				field : "state",
				width : 80,
				sortable : true
			},{
				id : "country",
				name : "Country",
				field : "country",
				width : 80,
				sortable : true
			},{
				id : "zip",
				name : "Zipcode",
				field : "zip",
				width : 80,
				sortable : true
			}, {
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			}, {
				id : "updatedDate",
				name : "Updated Date",
				field : "updatedDate",
				width : 80,
				sortable : true
			}, {
				id : "updatedBy",
				name : "Updated Date",
				field : "updatedBy",
				width : 80,
				sortable : true
			},  {
				id : "editSeller",
				field : "editSeller",
				name : "Edit ",
				width : 80,
				formatter: editFormatter
			}, {
				id : "delSeller",
				field : "delSeller",
				name : "Delete ",
				width : 80,
				formatter:deleteFormatter
			}];

	if (userSellerColumnsStr != 'null' && userSellerColumnsStr != '') {
		columnOrder = userSellerColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allSellerColumns.length; j++) {
				if (columnWidth[0] == allSellerColumns[j].id) {
					userSellerColumns[i] = allSellerColumns[j];
					userSellerColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		//userSellerColumns = allSellerColumns;
		var columnOrder = loadSellerColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allSellerColumns.length;j++){
				if(columnWidth == allSellerColumns[j].id){
					userSellerColumns[i] = allSellerColumns[j];
					userSellerColumns[i].width=80;
					break;
				}
			}			
		}
	}

	function editFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.sellerId +"'/>";
	    return button;
	}
	
	
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditSeller(id);
	});
	
	function deleteFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.sellerId +"'/>";		
		return button;
	}
	$('.delClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getDeleteSeller(id);
	});
	
	var sellerOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var sellerGridSortcol = "sellerId";
	var sellerGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function sellerGridComparer(a, b) {
		var x = a[sellerGridSortcol], y = b[sellerGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshSellerGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		sellerData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (sellerData[i] = {});
				d["id"] = i;
				d["sellerId"] = data.sellerId;
				d["firstName"] = data.fName;
				d["lastName"] = data.lName;
				d["compName"] = data.compName;
				d["email"] = data.email;
				d["phone"] = data.phone;
				d["addr1"] = data.addLine1;
				d["addr2"] = data.addLine2;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["zip"] = data.pincode;
				d["status"] = data.status;
				d["updatedDate"] = data.updDateStr;
				d["updatedBy"] = data.updBy;
			}
		}

		sellerDataView = new Slick.Data.DataView();
		sellerGrid = new Slick.Grid("#sellers_grid", sellerDataView,
				userSellerColumns, sellerOptions);
		sellerGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		sellerGrid.setSelectionModel(new Slick.RowSelectionModel());
		//sellerGrid.registerPlugin(sellerCheckboxSelector);
		
			sellerGridPager = new Slick.Controls.Pager(sellerDataView,
					sellerGrid, $("#sellers_pager"),
					pagingInfo);
		var sellerGridColumnpicker = new Slick.Controls.ColumnPicker(
				allSellerColumns, sellerGrid, sellerOptions);
					
		sellerGrid.onSort.subscribe(function(e, args) {
			sellerGridSortdir = args.sortAsc ? 1 : -1;
			sellerGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				sellerDataView.fastSort(sellerGridSortcol, args.sortAsc);
			} else {
				sellerDataView.sort(sellerGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the sellerGrid
		sellerDataView.onRowCountChanged.subscribe(function(e, args) {
			sellerGrid.updateRowCount();
			sellerGrid.render();
		});
		sellerDataView.onRowsChanged.subscribe(function(e, args) {
			sellerGrid.invalidateRows(args.rows);
			sellerGrid.render();
		});
		$(sellerGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							sellerSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											sellerSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getSellerGridData(0);
								}
							}

						});
		sellerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editSeller' && args.column.id != 'delSeller'){
					if(args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		sellerGrid.init();
		
		var sellerRowIndex = -1;
		sellerGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempSellerRowIndex = sellerGrid.getSelectedRows([0])[0];
		});
		// initialize the model after all the discountCodes have been hooked up
		sellerDataView.beginUpdate();
		sellerDataView.setItems(sellerData);
		//sellerDataView.setFilter(filter);
		sellerDataView.endUpdate();
		sellerDataView.syncGridSelection(sellerGrid, true);
		sellerGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveSellerPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = sellerGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('sellergrid', colStr);
	}
	
	function pagingControl(move, id) {
		var pageNo = 0;
		if (move == 'FIRST') {
			pageNo = 0;
		} else if (move == 'LAST') {
			pageNo = parseInt(pagingInfo.totalPages) - 1;
		} else if (move == 'NEXT') {
			pageNo = parseInt(pagingInfo.pageNum) + 1;
		} else if (move == 'PREV') {
			pageNo = parseInt(pagingInfo.pageNum) - 1;
		}
		getSellerGridData(pageNo);
	}
	
			
	//Edit Contest
	function editSeller(){
		var tempSellerRowIndex = sellerGrid.getSelectedRows([0])[0];
		if (tempSellerRowIndex == null) {
			jAlert("Plese select seller to Edit", "info");
			return false;
		}else {
			var sellerId = sellerGrid.getDataItem(tempSellerRowIndex).sellerId;
			getEditSeller(sellerId);
		}
	}
	
	
	
	function getEditSeller(sellerId){
		$.ajax({
			url : "${pageContext.request.contextPath}/ecomm/UpdateSeller",
			type : "post",
			dataType: "json",
			data: "sellerId="+sellerId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.sts == 1){
					$('#sellerModal').modal('show');
					setEditSeller(jsonData.seller);
				}else{
					jAlert(jsonData.msg);
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function setEditSeller(data){
		$('#saveBtn').hide();			
		$('#updateBtn').show();
		
		$('#sellerId').val(data.sellerId);
		$('#fName').val(data.fName);
		$('#lName').val(data.lName);
		$('#compName').val(data.compName);
		$('#email').val(data.email);	
		$('#phone').val(data.phone);	
		$('#addLine1').val(data.addLine1);
		$('#addLine2').val(data.addLine2);
		$('#city').val(data.city);
		$('#state').val(data.state);
		$('#country').val(data.country);
		$('#pincode').val(data.pincode);
	}
	
	//Delete Contest
	function deleteSeller(){		
		var tempSellerRowIndex = sellerGrid.getSelectedRows([0])[0];
		if (tempSellerRowIndex == null) {
			jAlert("Plese select seller to Delete", "info");
			return false;
		}else {
			var sellerId = sellerGrid.getDataItem(tempSellerRowIndex).sellerId;
			getDeleteSeller(sellerId);
		}
	}
	
	function getDeleteSeller(sellerId){
		if (sellerId == '') {
			jAlert("Please select a Seller to Delete.","Info");
			return false;S
		}
		jConfirm("Are you sure to delete a seller ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ecomm/UpdateSeller",
						type : "post",
						dataType: "json",
						data : "sellerId="+sellerId+"&action=DELETE&status=${status}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.sts == 1){
								pagingInfo = jsonData.pagingInfo;
								columnFilters = {};
								refreshSellerGridValues(jsonData.sellerList);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	
	function changeStatus(action){
		var tempSellerRowIndex = sellerGrid.getSelectedRows([0])[0];
		if (tempSellerRowIndex == null) {
			jAlert("Plese select seller to inactivate", "info");
			return false;
		}else {
			var sellerId = sellerGrid.getDataItem(tempSellerRowIndex).sellerId;
			$.ajax({
				url : "${pageContext.request.contextPath}/ecomm/UpdateSeller",
				type : "post",
				dataType: "json",
				data : "sellerId="+sellerId+"&action="+action+"&status=${status}",
				success : function(response) {
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.sts == 1){
						pagingInfo = jsonData.pagingInfo;
						columnFilters = {};
						refreshSellerGridValues(jsonData.sellerList);
					}
					jAlert(jsonData.msg);
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		
	}
	
	
	function clearAllSelections(){
	    var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
	    var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
				eventCheckbox[i].checked = false;
				}
			}
	    }	  
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshSellerGridValues(JSON.parse(JSON.stringify(${sellerList})));
		$('#sellers_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveSellerPreference()'>");
		enableMenu();
	};
		
</script>