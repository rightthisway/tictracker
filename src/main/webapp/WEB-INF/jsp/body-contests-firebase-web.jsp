<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>
var QSIZE = '${contest.questionSize}';
var questionArray = [];
var config = {
    apiKey: "AIzaSyBElK2WLH6nrRO8gz5Ew7JUbFrBciqnUWU",
    authDomain: "rewardthefanweb.firebaseapp.com",
    databaseURL: "https://rewardthefanweb.firebaseio.com",
    projectId: "rewardthefanweb",
    storageBucket: "rewardthefanweb.appspot.com",
    messagingSenderId: "930796571972"
 };
firebase.initializeApp(config);
var questionRef = firebase.database().ref().child('Question');
var userCountRef = firebase.database().ref().child('UserCount');
var totalCountRef = firebase.database().ref().child('TotalUserCount');
var winnerRef = firebase.database().ref().child('GrandWinners');

var j = 0;
var promoAutoGenerate;
var promoFlatDiscount;
var varSelectedList = '';
var jq2 = $.noConflict(true);
$(document).ready(function() {
	$('#contest_fromDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});

	$('#contest_toDate').datepicker({
		format : "mm/dd/yyyy",
		autoclose : true,
		orientation : "bottom",
		todayHighlight : true,
		startDate: new Date()
	});
	
	$("#allContest1").click(function(){
		callTabOnChange('ALL');
	});
	$("#todayContest1").click(function(){
		callTabOnChange('TODAY');
	});
	
	<c:choose>
		<c:when test="${status == 'ALL'}">	
			$('#allContest').addClass('active');
			$('#allContestTab').addClass('active');
		</c:when>
		<c:otherwise>
			$('#todayContest').addClass('active');
			$('#todayContestTab').addClass('active');
		</c:otherwise>
	</c:choose>
	

	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(contestsGrid != null && contestsGrid != undefined){
			contestsGrid.resizeCanvas();
		}
		if(questionGrid != null && questionGrid != undefined){
			questionGrid.resizeCanvas();
		}			
	});
});

function callTabOnChange(selectedTab) {	
	/* var qNo = $('#runningQuestionNo').val();
	if((parseInt(qNo)) > 0 || (parseInt(qNo)) < 10){
		jAlert("Contest is running, if you leave page all contest data will be lost.");
		return;
	} */
	window.location = "${pageContext.request.contextPath}/ContestsFirebaseWeb?status="+selectedTab;
}


function resetModal(){
	$('#contest_id').val('');
	$('#contest_name').val('');
	$('#contest_fromDate').val('');
	$('#startDateHour').val('00');
	$('#startDateMinute').val('00');
	$('#maxTickets').val('');	
	//$('#pointsPerWinner').val('');
	$('#contestMode').val('');	
	$('#questionSize').val('');	
	$('#rewardPoints').val('');
	$('#ticketsPerWinner').val('');
	
	$('#saveBtn').show();
	$('#updateBtn').hide();	
}

function startContest(){
	var index = contestsGrid.getSelectedRows([0])[0];
	if (index < 0) {
		jAlert("Please select Contest to start it.");
		return;
	}
	var contestId = contestsGrid.getDataItem(index).contestId;
	if(contestId < 0){
		jAlert("Please select Contest to start it.");
		return;
	}
	//$('#runningContestId').val(contestId);
	$('#runningQuestionNo').val(0);
	//$('#queNo').text("0.  ");
	$('#runningQuestionText').text('');
	$('#runningQuestionOptionA').text('');
	$('#runningQuestionOptionB').text('');
	$('#runningQuestionOptionC').text('');
	$('#runningQuestionAnswer').text('');
	//$('#contestStartModal').modal('show');
	//$('#startContestDiv').hide();
	$('#nextQuestion').text('Show Question-1');
	//nextQuestion();
}

function nextQuestion(){
	var buttonType = $('#nextQuestion').text();
	var type = '';
	var userCountUpdater;
	if(buttonType!='' && buttonType != undefined){
		if(buttonType.indexOf('Question') >= 0 || buttonType.indexOf('Summary') >= 0
				|| buttonType.indexOf('End') >= 0){
			type='QUESTION';
			if(buttonType.indexOf('Summary') >= 0){
				type = 'SUMMARY';
			}
		}else if(buttonType.indexOf('Answer') >= 0){
			type='ANSWER';
		}else if(buttonType.indexOf('Start') >= 0){
			type='START';
		}else if(buttonType.indexOf('Lottery') >= 0){
			type = 'LOTTERY';
		}else if(buttonType.indexOf('Winner') >= 0){
			type = 'WINNER';
		}
	}
	$.ajax({
		url : "${pageContext.request.contextPath}/NextContestQuestionFireBase",
		type : "post",
		dataType: "json",
		data: "runningContestId="+$('#runningContestId').val()+"&runningQuestionNo="+$('#runningQuestionNo').val()+'&type='+type,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				if(jsonData.questionList != null && jsonData.questionList.length > 0){
					for (var i = 0; i < jsonData.questionList.length; i++){
						var data = jsonData.questionList[i];
						$('#runningContestId').val(data.contestId);
						$('#runningQuestionNo').val(data.serialNo);
						//$('#queNo').text(data.serialNo+".  ");
						$('#runningQuestionText').text(data.serialNo+".  "+data.question);
						$('#runningQuestionOptionA').text(data.optionA);
						$('#runningQuestionOptionB').text(data.optionB);
						$('#runningQuestionOptionC').text(data.optionC);
						$('#questionRewardDollar').text('');
						$('#questionOptionACount').text('');
						$('#questionOptionBCount').text('');
						$('#questionOptionCCount').text('');
						//$('#runningQuestionOptionD').text(data.optionD);
						$('#optionADiv').css('background','#ccc');
						$('#optionBDiv').css('background','#ccc');
						$('#optionCDiv').css('background','#ccc');
						var timestamp = new Date().getTime();
						questionRef.set({
							qId:data.id,
							qNo:data.serialNo,
							text:data.question,
							optionA:data.optionA,
							optionB:data.optionB,
							optionC:data.optionC,
							answer:data.answer,
							timeStamp:timestamp
						});
						if(type=='START'){
							var qNo = parseInt(data.serialNo);
							$('#runningQuestionText').text(jsonData.contestMsg);
							$('#nextQuestion').text('Show Question-'+(qNo+1));
							$('#runningQuestionNo').val(0);
							userCountUpdater = setInterval(function(){updateTotalUserCount()},5000);
						}else if(type=='QUESTION'){
							$('#optionDiv').show();
							$('#nextQuestion').text('Show Answer-'+data.serialNo);
							$('#runningQuestionAnswer').text('');
						}else if(type=='ANSWER'){
							userCountRef.set({
								optionACount:jsonData.userCount.optionACount,
								optionBCount:jsonData.userCount.optionBCount,
								optionCCount:jsonData.userCount.optionCCount,
								rewardDollar:jsonData.userCount.questionRewards,
								timeStamp:timestamp
							});
							var qNo = parseInt(data.serialNo);
							$('#nextQuestion').text('Show Question-'+(qNo+1));
							if(data.answer == 'A'){
								$('#optionADiv').css('background','#00a0df');
							}else if(data.answer == 'B'){
								$('#optionBDiv').css('background','#00a0df');
							}else if(data.answer == 'C'){
								$('#optionCDiv').css('background','#00a0df');
							}
							$('#questionRewardDollar').text("Reward $"+jsonData.userCount.questionRewards);
							$('#questionOptionACount').text(jsonData.userCount.optionACount);
							$('#questionOptionBCount').text(jsonData.userCount.optionBCount);
							$('#questionOptionCCount').text(jsonData.userCount.optionCCount);
							if(qNo == QSIZE){
								$('#nextQuestion').text('Show Summary');
							}
						}else if(type=='SUMMARY'){
							$('#runningQuestionText').text('Press the button below to enter all the winners into lottery.');
							$('#optionADiv').hide();
							$('#optionBDiv').hide();
							$('#optionCDiv').hide();
							clearInterval(userCountUpdater);
							$('#nextQuestion').text('Enter to Lottery');
						}else if(type=='LOTTERY'){
							$('#runningQuestionText').text('Press the button below to randomnly choose Tickets Winner.');
							$('#optionADiv').hide();
							$('#optionBDiv').hide();
							$('#optionCDiv').hide();
							$('#nextQuestion').text('Declare Winners');
						}else if(type=='WINNER'){
							$('#runningQuestionText').text('Press the button below to end contest.');
							$('#optionADiv').hide();
							$('#optionBDiv').hide();
							$('#optionCDiv').hide();
							$('#nextQuestion').text('End Contest');
							if(jsonData.winnerStatus == 1){
								if(jsonData.winners.length <= 0){
									jAlert("No winners are found for selected contest.");
									return;
								}
								winnerRef.set(jsonData.winners);
							}else{
								jAlert(jsonData.winnerMsg);
							}
						}
						
						
					}
				}
			}
			if(jsonData.msg != '' && jsonData.msg != null){
				$('#nextQuestion').hide();
				$('#runningQuestionText').text('');
				$('#optionADiv').hide();
				$('#optionBDiv').hide();
				$('#optionCDiv').hide();
				questionRef.set({
					qId:0,qNo:0,
					text:'',
					optionA:'',
					optionB:'',
					optionC:'',
					answer:'',
					
				});
				var timestamp = new Date().getTime();
				userCountRef.set({optionACount:0,optionBCount:0,optionCCount:0,timeStamp:timestamp});
				//winnerRef.set([0]);
				jAlert(jsonData.msg);
			}
			
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateTotalUserCount(){
	$.ajax({
		url : "${pageContext.request.contextPath}/GetTotalUserCount",
		type : "post",
		dataType: "json",
		data: "runningContestId="+$('#runningContestId').val(),
		global:false,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				totalCountRef.set(jsonData.totalUserCount);
				console.log('Total Count : '+jsonData.totalUserCount);
			}
		},
		error : function(error){
			console.log('Error occured while getting total user count.');
		}
	});
}


function resetQModal(){
	var contestId = $('#q_contest_id').val();
	if(contestId == ''){
		jAlert("Please select Contest to add questions.");
		return;
	}
	$('#questionModal').modal('show');
	$('#questionId').val('');
	$('#questionText').val('');
	$('#optionA').val('');
	$('#optionB').val('');
	$('#optionC').val('');
	//$('#optionD').val('');	
	$('#answer').val('');
	$('#questionReward').val('');
	//$('#questionNo').val('');
	
	$('#qSaveBtn').show();
	$('#qUpdateBtn').hide();	
}


function questionSave(action){
	
	var text = $('#questionText').val();
	var oA = $('#optionA').val();
	var oB = $('#optionB').val();
	var oC = $('#optionC').val();
	//var oD = $('#optionD').val();	
	var answer = $('#answer').val();
	var qReward = $('#questionReward').val();
	//var qNo = $('#questionNo').val();
	
	if(text == ''){
		jAlert("Question Text is Mandatory.");
		return;
	}
	if(oA == ''){
		jAlert("Option A is Mandatory.");
		return;
	}
	if(oB == ''){
		jAlert("Option B is Mandatory.");
		return;
	}
	if(oC == ''){
		jAlert("Option C is Mandatory.");
		return;
	}
	/* if(oD == ''){
		jAlert("Option D is Mandatory.");
		return;
	} */
	if(answer == ''){
		jAlert("Answer is Mandatory.");
		return;
	}
	 if(qReward == ''){
		jAlert("Question Reward is mendatory.");
		return;
	} 
	
	var requestUrl = "${pageContext.request.contextPath}/UpdateQuestion";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#questionForm').serialize()+"&action=SAVE";
	}else if(action == 'update'){
		dataString = $('#questionForm').serialize()+"&action=UPDATE";
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#questionModal').modal('hide');
				questionPagingInfo = jsonData.questionPagingInfo;
				questionColumnFilters = {};
				refreshContestQuestionGridValues(jsonData.questionList);
				//refreshQuestionGridValues('');
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function getContestConfigSettings(){
	
	var requestUrl = "${pageContext.request.contextPath}/UpdateQuizConfigSettings";
	var dataString = "";
	dataString  ="action=SEARCH";
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#videoSourceUrl').val(jsonData.configSettings.videoSourceUrl);
				$('#appSyncUrl').val(jsonData.configSettings.appSyncUrl);
				$('#appSyncToken').val(jsonData.configSettings.appSyncToken);
				$('#jwPlayerLicenceKeyAndroid').val(jsonData.configSettings.jwPlayerLicenceKeyAndroid);
				$('#jwPlayerLicenceKeyIOS').val(jsonData.configSettings.jwPlayerLicenceKeyIOS);
				$('#partnerId').val(jsonData.configSettings.partnerId);
				$('#sourceId').val(jsonData.configSettings.sourceId);
				$('#entryId').val(jsonData.configSettings.entryId);
				$('#liveStreamUrl').val(jsonData.configSettings.liveStreamUrl);
				$('#videoSourceUrlModal').modal('show');
			} else {
				jAlert(jsonData.msg);
			}
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateQuizConfigSettings(action){
	
	var text = $('#videoSourceUrl').val();
	if(text == ''){
		jAlert("Video Source Url is Mandatory.");
		return;
	}
	text = $('#appSyncUrl').val();
	if(text == ''){
		jAlert("App Sync Url is Mandatory.");
		return;
	}
	text = $('#appSyncToken').val();
	if(text == ''){
		jAlert("App Sync Token is Mandatory.");
		return;
	}
	text = $('#jwPlayerLicenceKeyAndroid').val();
	if(text == ''){
		jAlert("Android JW Player Licence Key is Mandatory.");
		return;
	}
	text = $('#jwPlayerLicenceKeyIOS').val();
	if(text == ''){
		jAlert("IOS JW Player Licence Key is Mandatory.");
		return;
	}
	text = $('#partnerId').val();
	if(text == ''){
		jAlert("Partner Id is Mandatory.");
		return;
	}
	text = $('#sourceId').val();
	if(text == ''){
		jAlert("Source Id is Mandatory.");
		return;
	}
	text = $('#entryId').val();
	if(text == ''){
		jAlert("Entry Id is Mandatory.");
		return;
	}
	text = $('#liveStreamUrl').val();
	if(text == ''){
		jAlert("Live Stream URL is Mandatory.");
		return;
	}
	
	var requestUrl = "${pageContext.request.contextPath}/UpdateQuizConfigSettings";
	var dataString = "";
	dataString = $('#videoSourceUrlForm').serialize()+"&action=UPDATE";
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#videoSourceUrlModal').modal('hide');
				//questionPagingInfo = jsonData.videoSourceUrl;
				//refreshQuestionGridValues('');
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function changeContestType(){
	var contestMode = $('#contestMode').val();
	if(contestMode == 'FIXED_QUESTIONS'){
		$('#contestQSizeDiv').show();
	}else{
		$('#questionSize').val(0);
		$('#contestQSizeDiv').hide();
	}
}

function contestSave(action){
	
	var contestName = $('#contest_name').val();
	var fromDate = $('#contest_fromDate').val();
	var maxTickes = $('#maxTickets').val();	
	//var pointsPerWinner = $('#pointsPerWinner').val();
	var rewardPoints = $('#rewardPoints').val();
	var ticketsPerWinner = $('#ticketsPerWinner').val();
	var contestMode = $('#contestMode').val();
	var questionSize = $('#questionSize').val();
	
	if(contestMode == 'FIXED_QUESTIONS'){
		if(questionSize == '' || questionSize <=0){
			jAlert('Number of Question for the contest should be greater than 0.')
			return;
		}
	}
	
	
	if(contestName == ''){
		jAlert("Contest Name is Mandatory.");
		return;
	}
	if(maxTickes == ''){
		jAlert("Please enter Max. Ticket winners(Max. No. of users can we tickets.)");
		return;
	}
	/* if(pointsPerWinner == ''){
		jAlert("Please enter Point per winner.");
		return;
	} */
	if(rewardPoints == ''){
		jAlert("Please enter Reward Point Prize.");
		return;
	}
	if(ticketsPerWinner == ''){
		jAlert("Please enter Tickets per winner.");
		return;
	}
	if(fromDate == ''){
		jAlert("Start Date is Mandatory.");
		return;
	}
	
	var requestUrl = "${pageContext.request.contextPath}/UpdateContest";
	var dataString = "";
	if(action == 'save'){		
		dataString  = $('#contestForm').serialize()+"&action=SAVE&type="+$('#type').val();
	}else if(action == 'update'){
		dataString = $('#contestForm').serialize()+"&action=UPDATE&type="+$('#type').val();
	}
	$.ajax({
		url : requestUrl,
		type : "post",
		dataType: "json",
		data: dataString,
		success : function(response){
			var jsonData = JSON.parse(JSON.stringify(response));
			if(jsonData.status == 1){
				$('#myModal-2').modal('hide');
				pagingInfo = jsonData.contestsPagingInfo;
				columnFilters = {};
				refreshContestsGridValues(jsonData.contestsList);
				//refreshQuestionGridValues('');
				clearAllSelections();
			}
			jAlert(jsonData.msg);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Contest</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
		<ul class="nav nav-tabs" style="">
			<li id="allContestTab" class=""><a id="allContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#allContest">All Contests</a></li>
			<li id="todayContestTab" class=""><a id="todayContest1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#todayContest">Today's Contest</a>
		</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="allContest" class="tab-pane">
				<div class="full-width full-width-btn mb-20">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-2" onclick="resetModal();">Add Contest</button>
					<button type="button" class="btn btn-primary" onclick="editContest();">Edit Contest</button>
					<button type="button" class="btn btn-primary" onclick="deleteContest();">Delete Contest</button>
					<button type="button" class="btn btn-primary" onclick="resetContest();">Reset Contest</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="contestGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>All Contests</label>
							<div class="pull-right">
								<!--<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
								<a href="javascript:resetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="contests_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
						<div id="contests_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<input type="hidden" name="contestIdStr" id="contestIdStr"/>
				<br />
			 	<div style="position: relative" id="questionGridDiv">
					<div class="full-width mb-20 mt-20 full-width-btn">
						<button class="btn btn-primary" id="addQBtn" type="button" data-toggle="modal" onclick="resetQModal();">Add Question</button>
						<button class="btn btn-primary" id="editQBtn" type="button" onclick="editQuestion()">Edit Question</button>
						<button class="btn btn-primary" id="deleteQBtn" type="button" onclick="deleteQuestion()">Delete Question</button>
						<button class="btn btn-primary" id="uqrBtn" type="button" onclick="createQuestionRewardUI()">Update Question Rewards</button>
						<button type="button" class="btn btn-primary" onclick="getContestConfigSettings();">Update Settings</button>
						<!-- <button type="button" class="btn btn-primary" onclick="startContest();">Start Contest</button> -->
					</div>
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Contests Questions</label>
							<div class="pull-right">
								<!-- <a href="javascript:questionExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a> -->
								<a href="javascript:questionResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="question_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="question_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
			</div>
			<div id="todayContest" class="tab-pane">
				<form name="runningContestForm" id="runningContestForm" method="post">
					<input type="hidden" id="runningContestId" name="runningContestId" value="${contest.id}" />
					<input type="hidden" id="runningQuestionNo" name="runningQuestionNo"  value="0"/>
					<div class="form-group tab-fields" style="text-align: center;">
						<div class="form-group col-sm-12 col-xs-12">
							<label style="font-size: 22px;">${contest.contestName}</label><br/>
							<label style="font-size: 18px;">${contest.startDateTimeStr}</label><br/>
							<c:if test="${empty msg}">
								<label style="font-size: 12px;">Note : Once contest is started please do not refresh or leave page or contest data will be lost.</label>
							</c:if>
						</div>
					</div>
					<div class="form-group tab-fields">
					<hr style="width: 100%; color: black; height: 1px; background-color:black;" />
						<div class="form-group col-sm-12 col-xs-12">
							<!-- <label id="queNo" style="font-size: 15px;"><b>&nbsp;&nbsp;&nbsp;</b></label> -->
							<label id="runningQuestionText" style="font-size: 22px;"></label>
							
						</div>
					</div>
					<br/><br/>
					<div class="form-group tab-fields" style="display:none;" id="optionDiv">
						<div id="optionADiv" class="form-group col-sm-7 col-xs-7" style="margin-left: 20px;">
							<label style="font-size: 20px;">A.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionA" style="font-size: 20px;"></label>
							<label id="questionOptionACount" style="font-size: 20px;float:right;"></label>
						</div>
						<div class="form-group col-sm-4 col-xs-4" style="margin-left: 60px;">
							<label id="questionRewardDollar" style="font-size: 22px;"></label>
						</div>
						<div id="optionBDiv" class="form-group col-sm-7 col-xs-7" style="margin-left: 20px;">
							<label style="font-size: 20px;">B.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionB" style="font-size: 20px;"></label>
							<label id="questionOptionBCount" style="font-size: 20px;float:right;"></label>
						</div>
						<div id="optionCDiv" class="form-group col-sm-7 col-xs-7" style="margin-left: 20px;">
							<label style="font-size: 20px;">C.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionC" style="font-size: 20px;"></label>
							<label id="questionOptionCCount" style="font-size: 20px;float:right;"></label>
						</div>
						<!-- <div class="form-group col-sm-12 col-xs-12" style="background-color: greenyellow;">
							<label style="font-size: 15px;"><b>Answer:&nbsp;&nbsp;&nbsp;</b></label>
							<label id="runningQuestionAnswer" style="font-size: 15px;"></label>
						</div> -->
						<!-- <div class="form-group col-sm-6 col-xs-6">
							<label>D.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionD"></label>
						</div> -->
						
						
					</div>
				</form>
				<div class="form-group tab-fields" style="text-align: center;">
						<div class="form-group col-sm-12 col-xs-12">
						<c:if test="${empty msg}">
							<button class="btn btn-primary" id="nextQuestion" type="button" onclick="nextQuestion()">Start Contest</button>
						</c:if>
						<c:if test="${not empty msg}">
							<label style="font-size: 18px;;color:red;">${msg}</label>	
						</c:if>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>


	

<!-- Add Contest -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest</h4>
			</div>
			<div class="modal-body full-width">
				<form name="contestForm" id="contestForm" method="post">
					<input type="hidden" id="contest_id" name="contestId" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-6 col-xs-6">
							<label>Contest Name <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_name" name="contestName">
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Start Date <span class="required">*</span>
							</label> <input class="form-control" type="text" id="contest_fromDate" name="contestFromDate">
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Start Time <span class="required">*</span>
							<select name="startDateHour" id="startDateHour" class="form-control" style="width:120px;">
								<option value="00">00</option>
								<option value="1">01</option>
								<option value="01">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Minutes <span class="required">*</span>
							<select name="startDateMinute" id="startDateMinute" class="form-control" style="width:120px;">
								<option value="00">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
							<input type="hidden" id="type" name="type" value="WEB" />
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Max. Tickets Winners<span class="required">*</span>
							</label> <input class="form-control" type="text" id="maxTickets" name="maxTickets">
						</div>
						<!-- <div class="form-group col-sm-3 col-xs-3">
							<label>Points/Winner<span class="required">*</span>
							</label> <input class="form-control" type="text" id="pointsPerWinner" name="pointsPerWinner">
						</div> -->
						<div class="form-group col-sm-3 col-xs-3">
							<label>Reward Points Prize <span class="required">*</span>
							</label> <input class="form-control" type="text" id="rewardPoints" name="rewardPoints">
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Tickets/Winner<span class="required">*</span>
							</label> <input class="form-control" type="text" id="ticketsPerWinner" name="ticketsPerWinner">
						</div>
						<div class="form-group col-sm-3 col-xs-3">
							<label>Contest Type<span class="required">*</span></label> 
							<select onchange="changeContestType()" name="contestMode" id="contestMode" class="form-control" >
								<option value="FIXED_QUESTIONS">Fixed Question</option>
								<option value="KNOCKOUT">Knock out</option>
							</select>
						</div>
						<div class="form-group col-sm-3 col-xs-3" id="contestQSizeDiv">
							<label>Number of Questions<span class="required">*</span></label> 
							<input class="form-control" type="text" id="questionSize" name="questionSize">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="contestSave('save')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="contestSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Ends of Add Shipping/Other Address popup-->

<!-- Video Source Url Update - start  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="videoSourceUrlModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Video Url</h4>
			</div>
			<div class="modal-body full-width">
				<form name="videoSourceUrlForm" id="videoSourceUrlForm" method="post">
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Video Source Url<span class="required">*</span>
							</label> <input class="form-control" type="text" id="videoSourceUrl" name="videoSourceUrl">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>App Sync Url<span class="required">*</span>
							</label> <input class="form-control" type="text" id="appSyncUrl" name="appSyncUrl">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>App Sync Token<span class="required">*</span>
							</label> <input class="form-control" type="text" id="appSyncToken" name="appSyncToken">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>Android JW Player Licence Key<span class="required">*</span>
							</label> <input class="form-control" type="text" id="jwPlayerLicenceKeyAndroid" name="jwPlayerLicenceKeyAndroid">
						</div>	
						<div class="form-group col-sm-12 col-xs-12">
							<label>IOS JW Player Licence Key<span class="required">*</span>
							</label> <input class="form-control" type="text" id="jwPlayerLicenceKeyIOS" name="jwPlayerLicenceKeyIOS">
						</div>
					</div>
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Partner Id<span class="required">*</span>
							</label> <input class="form-control" type="text" id="partnerId" name="partnerId">
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Source Id<span class="required">*</span>
							</label> <input class="form-control" type="text" id="sourceId" name="sourceId">
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Entry Id<span class="required">*</span>
							</label> <input class="form-control" type="text" id="entryId" name="entryId">
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Live Stream URL<span class="required">*</span>
							</label> <input class="form-control" type="text" id="liveStreamUrl" name="liveStreamUrl">
						</div>						
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qUpdateConfigBtn" type="button" onclick="updateQuizConfigSettings('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Video Source Url Update - Ends  -->

<!-- Add Questions -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="questionModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Question</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionForm" id="questionForm" method="post">
					<input type="hidden" id="q_contest_id" name="qContestId" />
					<input type="hidden" id="questionId" name="questionId" />
					<div id="startContestDiv" class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label>Question Text<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionText" name="questionText">
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>Option A <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionA" name="optionA">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Option B <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionB" name="optionB">
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Option C <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionC" name="optionC">
						</div>
						<!-- <div class="form-group col-sm-6 col-xs-6">
							<label>Option D <span class="required">*</span>
							</label> <input class="form-control" type="text" id="optionD" name="optionD">
						</div> -->
						<div class="form-group col-sm-6 col-xs-6">
							<label>Answer <span class="required">*</span>
							<select name="answer" id="answer" class="form-control">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<!-- <option value="D">D</option> -->
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Question Reward(in Cent)<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionReward" name="questionReward">
						</div>
						<!-- <div class="form-group col-sm-6 col-xs-6">
							<label>Question Sr. No<span class="required">*</span>
							</label> <input class="form-control" type="text" id="questionNo" name="questionNo">
						</div> -->
						
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qSaveBtn" type="button" onclick="questionSave('save')">Save</button>
				<button class="btn btn-primary" id="qUpdateBtn" type="button" onclick="questionSave('update')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add Question end here  -->


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="questionRewardModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Question Rewards</h4>
			</div>
			<div class="modal-body full-width">
				<form name="questionRewardForm" id="questionRewardForm" method="post">
					<div id="questionRewardDiv" class="form-group tab-fields">
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="qrUpdateBtn" type="button" onclick="updateQuestionReward()">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Start contest modal -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="contestStartModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Contest Started</h4>
			</div>
			<div class="modal-body full-width">
				<form name="runningContestForm" id="runningContestForm" method="post">
					<input type="hidden" id="runningContestId" name="runningContestId" />
					<input type="hidden" id="runningQuestionNo" name="runningQuestionNo" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-12 col-xs-12">
							<label id="queNo">&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionText"></label>
						</div>						
						<div class="form-group col-sm-6 col-xs-6">
							<label>A.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionA"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>B.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionB"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>C.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionC"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>D.&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionOptionD"></label>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Answer:&nbsp;&nbsp;&nbsp;</label>
							<label id="runningQuestionAnswer"></label>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="nextQuestion" type="button" onclick="nextQuestion()">Question-1</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div> -->
<!-- Start contest modal  -->



<script type="text/javascript">
		
	//Contest Grid
	
	function getContestsGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestsFirebaseWeb.json",
			type : "post",
			dataType: "json",
			data: "headerFilter="+contestsSearchString+"&status=ALL",
			success : function(response){
				var jsonData = response;
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.contestsPagingInfo;
				refreshContestsGridValues(jsonData.contestsList);
				clearAllSelections();				
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+contestsSearchString;
	    var url = "${pageContext.request.contextPath}/ContestExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		contestsSearchString='';
		columnFilters = {};
		getContestsGridData(0);
		//refreshQuestionGridValues('');
	}

	/* var contestsCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	}); */
	
	var pagingInfo;
	var contestsDataView;
	var contestsGrid;
	var contestsData = [];
	var contestsGridPager;
	var contestsSearchString='';
	var columnFilters = {};
	var userContestsColumnsStr = '<%=session.getAttribute("contestsgrid")%>';

	var userContestsColumns = [];
	var allContestsColumns = [ {
				id : "contestName",
				name : "Contest Name",
				field : "contestName",
				width : 80,
				sortable : true
			}, {
				id : "startDate",
				name : "Start Date Time",
				field : "startDate",
				width : 80,
				sortable : true
			},{
				id : "partipantCount",
				name : "Partipant Count",
				field : "partipantCount",
				width : 80,
				sortable : true
			}, {
				id : "winnerCount",
				name : "Winner Count",
				field : "winnerCount",
				width : 80,
				sortable : true
			}, {
				id : "ticketWinnerCount",
				name : "Ticket Winner Count",
				field : "ticketWinnerCount",
				width : 80,
				sortable : true
			},{
				id : "pointWinnerCount",
				name : "Point Winner Count",
				field : "pointWinnerCount",
				width : 80,
				sortable : true
			},{
				id : "freeTicketPerWinner",
				name : "Free Ticket/Winner",
				field : "freeTicketPerWinner",
				width : 80,
				sortable : true
			},{
				id : "ticketWinnerThreshhold",
				name : "Max. Ticket Winner",
				field : "ticketWinnerThreshhold",
				width : 80,
				sortable : true
			},/* {
				id : "pointsPerWinner",
				name : "Points/Winner",
				field : "pointsPerWinner",
				width : 80,
				sortable : true
			}, */
			{
				id : "rewardPoints",
				name : "Reward Points",
				field : "rewardPoints",
				width : 80,
				sortable : true
			},{
				id : "contestMode",
				name : "Contest Mode",
				field : "contestMode",
				width : 80,
				sortable : true
			},{
				id : "questionSize",
				name : "Number of Que.",
				field : "questionSize",
				width : 80,
				sortable : true
			},{
				id : "status",
				name : "Status",
				field : "status",
				width : 80,
				sortable : true
			}, {
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			}, {
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			}, {
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			}, {
				id : "updatedBy",
				field : "updatedBy",
				name : "ModifiedBy ",
				width : 80,
				sortable : true
			},  {
				id : "editContest",
				field : "editContest",
				name : "Edit ",
				width : 80,
				formatter: editFormatter
			}, {
				id : "delContest",
				field : "delContest",
				name : "Delete ",
				width : 80,
				formatter:buttonFormatter
			}];

	if (userContestsColumnsStr != 'null' && userContestsColumnsStr != '') {
		columnOrder = userContestsColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allContestsColumns.length; j++) {
				if (columnWidth[0] == allContestsColumns[j].id) {
					userContestsColumns[i] = allContestsColumns[j];
					userContestsColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userContestsColumns = allContestsColumns;
	}

	function editFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.contestId +"'/>";
	    return button;
	}
	$('.editClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getEditContest(id);
	});
	
	function buttonFormatter(row, cell, value, columnDef, dataContext) {
		var button = "<img class='delClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.contestId +"'/>";		
		return button;
	}
	$('.delClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	    getDeleteContest(id);
	});
	
	var contestsOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var contestsGridSortcol = "contestId";
	var contestsGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function contestsGridComparer(a, b) {
		var x = a[contestsGridSortcol], y = b[contestsGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
			
	function refreshContestsGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		contestsData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (contestsData[i] = {});
				d["id"] = i;
				d["contestId"] = data.id;
				d["contestName"] = data.contestName;
				d["startDate"] = data.startDateTimeStr;
				d["partipantCount"] = data.partipantCount;
				d["winnerCount"] = data.winnerCount;
				d["ticketWinnerCount"] = data.ticketWinnerCount;
				d["pointWinnerCount"] = data.pointWinnerCount;
				d["ticketWinnerThreshhold"] = data.ticketWinnerThreshhold;
				d["freeTicketPerWinner"] = data.freeTicketPerWinner;
				//d["pointsPerWinner"] = data.pointsPerWinner;
				d["rewardPoints"] = data.rewardPoints;
				d["contestMode"] = data.contestMode;
				d["questionSize"] = data.questionSize;
				d["status"] = data.status;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		contestsDataView = new Slick.Data.DataView();
		contestsGrid = new Slick.Grid("#contests_grid", contestsDataView,
				userContestsColumns, contestsOptions);
		contestsGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		contestsGrid.setSelectionModel(new Slick.RowSelectionModel());
		//contestsGrid.registerPlugin(contestsCheckboxSelector);
		
			contestsGridPager = new Slick.Controls.Pager(contestsDataView,
					contestsGrid, $("#contests_pager"),
					pagingInfo);
		var contestsGridColumnpicker = new Slick.Controls.ColumnPicker(
				allContestsColumns, contestsGrid, contestsOptions);
					
		contestsGrid.onSort.subscribe(function(e, args) {
			contestsGridSortdir = args.sortAsc ? 1 : -1;
			contestsGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				contestsDataView.fastSort(contestsGridSortcol, args.sortAsc);
			} else {
				contestsDataView.sort(contestsGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the contestsGrid
		contestsDataView.onRowCountChanged.subscribe(function(e, args) {
			contestsGrid.updateRowCount();
			contestsGrid.render();
		});
		contestsDataView.onRowsChanged.subscribe(function(e, args) {
			contestsGrid.invalidateRows(args.rows);
			contestsGrid.render();
		});
		$(contestsGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							contestsSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								columnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in columnFilters) {
										if (columnId !== undefined
												&& columnFilters[columnId] !== "") {
											contestsSearchString += columnId
													+ ":"
													+ columnFilters[columnId]
													+ ",";
										}
									}
									getContestsGridData(0);
								}
							}

						});
		contestsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editContest' && args.column.id != 'delContest'){
					if(args.column.id == 'startDate' || args.column.id == 'updatedDate' || args.column.id == 'createdDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(columnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		contestsGrid.init();
		
		var contestsRowIndex = -1;
		contestsGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
			if (tempContestsRowIndex != contestsRowIndex) {
				var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
				$('#q_contest_id').val(contestId);
				getQuestionGridData(contestId,0);
			}
		});
		// initialize the model after all the discountCodes have been hooked up
		contestsDataView.beginUpdate();
		contestsDataView.setItems(contestsData);
		//contestsDataView.setFilter(filter);
		contestsDataView.endUpdate();
		contestsDataView.syncGridSelection(contestsGrid, true);
		contestsGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserContestsPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = contestsGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('contestsgrid', colStr);
	}
	
	function pagingControl(move, id) {
		if(id == 'contests_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getContestsGridData(pageNo);
		} else if(id == 'question_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(questionPagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(questionPagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(questionPagingInfo.pageNum) - 1;
			}
			var contestsId = $('#contestIdStr').val();
			getQuestionGridData(contestsId, pageNo);
		} 
	}
	
		
	//Question Grid		
	
	function getQuestionGridData(contestId, pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestQuestions",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&pageNo="+pageNo+"&headerFilter="+questionSearchString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				questionPagingInfo = jsonData.questionPagingInfo;
				refreshContestQuestionGridValues(jsonData.questionList);					
				$('#q_contest_id').val(contestId);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function questionExportToExcel(){
		var contId = $('#contestIdStr').val();
		var appendData = "contestId="+contId+"&headerFilter="+questionSearchString;
	    var url = "${pageContext.request.contextPath}/QuestionExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function questionResetFilters(){
		questionSearchString='';
		questionColumnFilters = {};
		var contstId = $('#q_contest_id').val();
		getQuestionGridData(contstId, 0);
	}
	
	var questionPagingInfo;
	var questionDataView;
	var questionGrid;
	var questionData = [];
	var questionGridPager;
	var questionSearchString='';
	var questionColumnFilters = {};
	var userQuestionColumnsStr = '<%=session.getAttribute("questiongrid")%>';

	var userQuestionColumns = [];
	var allQuestionColumns = [
			/* {
				id : "srNo",
				field : "srNo",
				name : "Sr. No",
				width : 80,
				sortable : true
			}, */{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			},/* {
				id : "optionD",
				field : "optionD",
				name : "option D",
				width : 80,
				sortable : true
			}, */{
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "questionReward",
				field : "questionReward",
				name : "Question/Reward",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "updatedDate",
				field : "updatedDate",
				name : "Updated Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "updatedBy",
				field : "updatedBy",
				name : "Updated By",
				width : 80,
				sortable : true
			}];

	if (userQuestionColumnsStr != 'null' && userQuestionColumnsStr != '') {
		columnOrder = userQuestionColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allQuestionColumns.length; j++) {
				if (columnWidth[0] == allQuestionColumns[j].id) {
					userQuestionColumns[i] = allQuestionColumns[j];
					userQuestionColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userQuestionColumns = allQuestionColumns;
	}
		
	var questionOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
		showHeaderRow : true,
		headerRowHeight : 30,
		explicitInitialization : true
	};
	var questionGridSortcol = "questionId";
	var questionGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function questionGridComparer(a, b) {
		var x = a[questionGridSortcol], y = b[questionGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	

	function refreshContestQuestionGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		questionData = [];
		if (jsonData != null && jsonData.length > 0) {
			questionArray = jsonData;
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (questionData[i] = {});
				d["id"] = i;
				d["questionId"] = data.id;
				d["contestId"] = data.contestId;
				//d["srNo"] = data.serialNo;
				d["question"] = data.question;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC;
				//d["optionD"] = data.optionD;
				d["answer"] = data.answer;
				d["questionReward"] = data.rewardPerQuestionStr;
				d["createdDate"] = data.createdDateTimeStr;
				d["updatedDate"] = data.updatedDateTimeStr;
				d["createdBy"] = data.createdBy;
				d["updatedBy"] = data.updatedBy;
			}
		}

		questionDataView = new Slick.Data.DataView();
		questionGrid = new Slick.Grid("#question_grid", questionDataView,
				userQuestionColumns, questionOptions);
		questionGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		questionGrid.setSelectionModel(new Slick.RowSelectionModel());
		//questionGrid.registerPlugin(contestsCheckboxSelector);
		
		questionGridPager = new Slick.Controls.Pager(questionDataView,
					questionGrid, $("#question_pager"),
					questionPagingInfo);
		var questionGridColumnpicker = new Slick.Controls.ColumnPicker(
				allQuestionColumns, questionGrid, questionOptions);
		
	
		questionGrid.onSort.subscribe(function(e, args) {
			questionGridSortdir = args.sortAsc ? 1 : -1;
			questionGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				questionDataView.fastSort(questionGridSortcol, args.sortAsc);
			} else {
				questionDataView.sort(questionGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the questionGrid
		questionDataView.onRowCountChanged.subscribe(function(e, args) {
			questionGrid.updateRowCount();
			questionGrid.render();
		});
		questionDataView.onRowsChanged.subscribe(function(e, args) {
			questionGrid.invalidateRows(args.rows);
			questionGrid.render();
		});
		$(questionGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							questionSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								questionColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in questionColumnFilters) {
										if (columnId !== undefined
												&& questionColumnFilters[columnId] !== "") {
											questionSearchString += columnId
													+ ":"
													+ questionColumnFilters[columnId]
													+ ",";
										}
									}
									var contstsId = $('#q_contest_id').val();
									getQuestionGridData(contstsId, 0);
								}
							}

						});
		questionGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(questionColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(questionColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		questionGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		questionDataView.beginUpdate();
		questionDataView.setItems(questionData);
		//questionDataView.setFilter(filter);
		questionDataView.endUpdate();
		questionDataView.syncGridSelection(questionGrid, true);
		questionGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserQuestionPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = questionGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('questiongrid', colStr);
	}
	
	
	//Edit Contest
	function editContest(){
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select contest to Edit", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getEditContest(contestId);
		}
	}
	
	//Edit Contest
	function editQuestion(){
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (tempQuestionRowIndex == null) {
			jAlert("Plese select Question to Edit", "info");
			return false;
		}else {
			var questionId = questionGrid.getDataItem(tempQuestionRowIndex).questionId;
			getEditQuestion(questionId);
		}
	}
	
	function getEditContest(contestId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateContest",
			type : "post",
			dataType: "json",
			data: "contestId="+contestId+"&action=EDIT&type="+$('#type').val(),
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					//$('#contestCategoryDiv').empty();					
					$('#myModal-2').modal('show');
					setEditContest(jsonData.contestsList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function getEditQuestion(questionId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdateQuestion",
			type : "post",
			dataType: "json",
			data: "questionId="+questionId+"&action=EDIT",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					//$('#contestCategoryDiv').empty();					
					$('#questionModal').modal('show');
					setEditQuestion(jsonData.questionList);
				}else{
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
		
	function setEditContest(contestsEd){
		$('#saveBtn').hide();			
		$('#updateBtn').show();
		if(contestsEd != null && contestsEd.length > 0){
			for (var i = 0; i < contestsEd.length; i++){					
				var data = contestsEd[i];
				$('#contest_id').val(data.id);
				$('#contest_name').val(data.contestName);
				$('#contest_fromDate').val(data.startDateStr);
				$('#startDateHour').val(data.hours);
				$('#startDateMinute').val(data.minutes);
				$('#maxTickets').val(data.ticketWinnerThreshhold);	
				//$('#pointsPerWinner').val(data.pointsPerWinner);
				$('#rewardPoints').val(data.rewardPoints);
				$('#contestMode').val(data.contestMode);	
				$('#questionSize').val(data.questionSize);
				$('#ticketsPerWinner').val(data.freeTicketPerWinner);
			}
		}
	}
	
	function setEditQuestion(question){
		$('#qSaveBtn').hide();			
		$('#qUpdateBtn').show();
		if(question != null && question.length > 0){
			for (var i = 0; i < question.length; i++){					
				var data = question[i];
				$('#questionId').val(data.id);
				$('#q_contest_id').val(data.contestId);
				$('#questionText').val(data.question);
				$('#optionA').val(data.optionA);
				$('#optionB').val(data.optionB);
				$('#optionC').val(data.optionC);
				//$('#optionD').val(data.optionD);	
				$('#answer').val(data.answer);
				$('#questionReward').val(data.rewardPerQuestionStr);
				//$('#questionNo').val(data.serialNo);
			}
		}
	}
	
	//Delete Contest
	function deleteContest(){		
		var tempContestsRowIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempContestsRowIndex == null) {
			jAlert("Plese select contest to Delete", "info");
			return false;
		}else {
			var contestId = contestsGrid.getDataItem(tempContestsRowIndex).contestId;
			getDeleteContest(contestId);
		}
	}
	
	function deleteQuestion(){		
		var tempQuestionRowIndex = questionGrid.getSelectedRows([0])[0];
		if (tempQuestionRowIndex == null) {
			jAlert("Plese select Question to Delete", "info");
			return false;
		}else {
			var questionId = questionGrid.getDataItem(tempQuestionRowIndex).questionId;
			var contestId = questionGrid.getDataItem(tempQuestionRowIndex).contestId;
			getDeleteQuestion(questionId,contestId);
		}
	}
	
	function getDeleteContest(contestId){
		if (contestId == '') {
			jAlert("Please select a Contest to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete a Contest ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId+"&action=DELETE&type="+$('#type').val(),
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.contestsPagingInfo;
								columnFilters = {};
								refreshContestsGridValues(jsonData.contestsList);
								refreshContestQuestionGridValues([]);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function resetContest(){
		var tempIndex = contestsGrid.getSelectedRows([0])[0];
		if (tempIndex == undefined || tempIndex < 0) {
			jAlert("Plese select Contest to reset.", "info");
			return false;
		}
		
		var contestId = contestsGrid.getDataItem(tempIndex).contestId;
		jConfirm("Are you sure to reset Contest data ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/ResetContest",
						type : "post",
						dataType: "json",
						data : "contestId="+contestId,
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	function getDeleteQuestion(questionId,contestId){
		if (questionId == '') {
			jAlert("Please select a Question to Delete.","Info");
			return false;
		}
		jConfirm("Are you sure to delete selected Question ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateQuestion",
						type : "post",
						dataType: "json",
						data : "questionId="+questionId+"&qContestId="+contestId+"&action=DELETE",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								questionPagingInfo = jsonData.questionPagingInfo;
								questionColumnFilters = {};
								refreshContestQuestionGridValues(jsonData.questionList);
							}
							jAlert(jsonData.msg);
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	function createQuestionRewardUI(){
		var inputStr = '';
		var index = contestsGrid.getSelectedRows([0])[0];
		if (index == undefined) {
			jAlert('Please select contest first to update question rewards.');
			return;
		}
		var contestId = contestsGrid.getDataItem(index).contestId;
		
		$('#questionRewardDiv').empty();
		for(var i=0;i<questionArray.length;i++){
			var d = questionArray[i];
			inputStr += '<div class="form-group col-sm-3 col-xs-3">'+
			'<label>Reward for Question-'+(i+1)+'<span class="required">*</span></label>'+ 
			'<input class="form-control" type="text" id="questionReward_'+i+'" value="'+d.rewardPerQuestionStr+'" name="questionReward_'+i+'"></div>';
		}
		$('#questionRewardModal').modal('show');
		$('#questionRewardDiv').append(inputStr);
		
	}
	
	function updateQuestionReward(){
		var index = contestsGrid.getSelectedRows([0])[0];
		if (index == undefined) {
			jAlert('Please select contest first to update question rewards.');
			return;
		}
		var contestId = contestsGrid.getDataItem(index).contestId;
		$.ajax({
				url : "${pageContext.request.contextPath}/UpdateQuestionRewards",
				type : "post",
				dataType: "json",
				data :$('#questionRewardForm').serialize()+"&contestId="+contestId,
				success : function(response) {
					var jsonData = JSON.parse(JSON.stringify(response));
					if(jsonData.status == 1){
						questionPagingInfo = jsonData.questionPagingInfo;
						questionColumnFilters = {};
						refreshContestQuestionGridValues(jsonData.questionList);
						$('#questionRewardModal').modal('hide');
					}
					jAlert(jsonData.msg);
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
				});
			
	}
							

	//call functions once page loaded
	window.onload = function() {
		<c:if test="${status=='ALL'}">
		pagingInfo = JSON.parse(JSON.stringify(${contestsPagingInfo}));
		refreshContestsGridValues(JSON.parse(JSON.stringify(${contestsList})));
		$('#contests_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserContestsPreference()'>");
		</c:if>
		
		enableMenu();
	};
		
	</script>