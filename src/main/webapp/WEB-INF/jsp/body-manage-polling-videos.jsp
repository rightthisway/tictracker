<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.pollingVideoLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

var jq2 = $.noConflict(true);
$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(pollingVideoGrid != null && pollingVideoGrid != undefined){
			pollingVideoGrid.resizeCanvas();
		}
	});
	
	<c:choose>
		<c:when test="${videoStatus == 'ACTIVE'}">	
			$('#activePollingVideo').addClass('active');
			$('#activePollingVideoTab').addClass('active');
		</c:when>
		<c:when test="${videoStatus == 'PASSIVE'}">
			$('#deletedPollingVideo').addClass('active');
			$('#deletedPollingVideoTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activePollingVideo1").click(function(){
		callTabOnChange('ACTIVE');
	});
	$("#deletedPollingVideo1").click(function(){
		callTabOnChange('PASSIVE');
	});
	
	
	 $('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true,
		startDate: new Date()
    });
	
	
	
});

function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/PollingVideos?videoStatus="+selectedTab;
}

</script>


<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">RTF Media</a></li>
			<li><i class="fa fa-laptop"></i>Manage RTF Videos</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width">
		<section class="contest-panel panel">
			<ul class="nav nav-tabs" style="">
				<li id="activePollingVideoTab" class=""><a id="activePollingVideo1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#activePollingVideo">Active RTF Media</a></li>
				 <li id="deletedPollingVideoTab" class=""><a id="deletedPollingVideo1" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#deletedPollingVideo">Inactive RTF Media</a></li> 
			</ul>
		</section>
	</div>	
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="activePollingVideo" class="tab-pane">
			<c:if test="${videoStatus =='ACTIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="addQBBtn" type="button" data-toggle="modal" onclick="resetPollingVideoModal();">Add RTF Video</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="updatePollingVideo('PASSIVE')">Disable Video (OFF)</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingVideo()">Delete RTF Video</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="updatePollingVideo('DEFAULT')">Set Default Video</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="updatePollingVideo('NORMAL')">Remove Default Video</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="pollingVideoGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Active Videos</label>
							<div class="pull-right">
								<a href="javascript:pollingVideoExportToExcel('ACTIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingVideoResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pollingVideo_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="pollingVideo_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
				</div>
				<br/><br/>
			</c:if>				
			</div>
			<div id="deletedPollingVideo" class="tab-pane">
			<c:if test="${videoStatus =='PASSIVE'}">	
				<div class="full-width full-width-btn mb-20">
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="updatePollingVideo('ACTIVE')">Enable Video (ON)</button>
					<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="deletePollingVideo()">Delete RTF Video</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="updatePollingVideo('DEFAULT')">Set Default Video</button>
					<button class="btn btn-primary" id="editQBBtn" type="button" onclick="updatePollingVideo('NORMAL')">Remove Default Video</button>
				</div>
				<br />
				<br />
				<div style="position: relative" id="pollingVideoGridDiv">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Deleted Videos</label>
							<div class="pull-right">
								<a href="javascript:pollingVideoExportToExcel('PASSIVE')" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
								<a href="javascript:pollingVideoResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
							</div>
						</div>
						<div id="pollingVideo_grid" style="width: 100%; height: 200px; border: 1px solid gray"></div>
						<div id="pollingVideo_pager" style="width: 100%; height: 10px;"></div>
				
					</div>
					<br/><br/>
				</div>
			</c:if>				
			</div>
	</div>
</div>


<!-- Add/Edit PollingVideo  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pollingVideoModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">RTF Video</h4>
			</div>
			<div class="modal-body full-width">
				<form name="pollingVideoForm" id="pollingVideoForm" method="post" enctype="multipart/form-data">
					<input type="hidden" id="videoId" name="videoId" />
					<input type="hidden" id="action" name="action" />
					<input type="hidden" id="rtfVideoFile" name="videoFile" />
					<input type="hidden" id="rtfImageFile" name="imageFile" />
					<input type="hidden" id="videoStatus" name="videoStatus" value="${videoStatus}" />
					<div class="form-group tab-fields">
						<div class="form-group col-sm-4 col-xs-4">
							<label>Title<span class="required">*</span>
							</label> <input class="form-control" type="text" id="title" name="title">
						</div>	
						<div class="form-group col-sm-4 col-xs-4">
							<label><strong>Category</strong><span class="required">*</span>
							</label> 
							<select name="category" id="category" class="form-control" >
								
								<c:forEach items="${category}" var="cat">
									<option value="${cat.id}" >${cat.categoryName}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-sm-4 col-xs-4">
							<label><strong>Sponsor</strong>
							</label> 
							<select name="sponsor" id="sponsor" class="form-control" >
									<option value="0" >-- Select Sponsor --</option>
								<c:forEach items="${sponsors}" var="sponsor">
									<option value="${sponsor.id}" >${sponsor.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-sm-12 col-xs-12">
							<label>Description<span class="required">*</span>
							</label> <input class="form-control" type="text" id="description" name="description">
						</div>	
						<div class="form-group col-sm-6 col-xs-6">
							<label>Polling Video<span class="required">*</span>
							</label> <input class="form-control" type="file" id="videoFile" name="videoFile">
							<a id="downloadVideo" href="javascript:downloadVideoFile()" ></a>
						</div>
						<div class="form-group col-sm-6 col-xs-6">
							<label>Polling Video Thumbnail Image
							</label> <input class="form-control" type="file" id="imageFile" name="imageFile">
							<!-- <a id="downloadVideo" href="javascript:downloadVideoFile()" ></a> -->
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer full-width">
				<button class="btn btn-primary" id="saveBtn" type="button" onclick="savePollingVideo('SAVE')">Save</button>
				<button class="btn btn-primary" id="updateBtn" type="button" onclick="savePollingVideo('UPDATE')">Update</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Add/Edit PollingVideo  end here  -->

<script type="text/javascript">

	function pagingControl(move, id) {
		if(id == 'pollingVideo_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getPollingVideoGridData(pageNo);
		}
	}
	
	//PollingVideo  Grid
	
	function getPollingVideoGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/PollingVideos.json",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+pollingVideoSearchString+"&videoStatus=${videoStatus}"+"&sortingString="+sortingString,			
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.videoPagingInfo;
				refreshPollingVideoGridValues(jsonData.videos);	
				clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function downloadVideoFile(){
		var videoId = $('#videoId').val();
		if(videoId == null || videoId == ''){
			jAlert("Video Id not found.");
			return;
		}
	 	 //var url = "/Accounting/DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;
		 var url = apiServerUrl+"DownloadPollingVideo?videoId="+videoId;
		 $('#download-frame').attr('src', url);
	}
	
	function pollingVideoExportToExcel(status){
		var appendData = "headerFilter="+pollingVideoSearchString+"&videoStatus=${videoStatus}";
	    var url =apiServerUrl+"PollingVideosExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function pollingVideoResetFilters(){
		pollingVideoSearchString='';
		sortingString ='';
		pollingVideoColumnFilters = {};
		getPollingVideoGridData(0);
	}
	
	
	var pollingVideoCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	var pagingInfo;
	var pollingVideoDataView;
	var pollingVideoGrid;
	var pollingVideoData = [];
	var pollingVideoGridPager;
	var pollingVideoSearchString='';
	var sortingString='';
	var pollingVideoColumnFilters = {};
	var userPollingVideoColumnsStr = '<%=session.getAttribute("pollingvideogrid")%>';

	var userPollingVideoColumns = [];
	var allPollingVideoColumns = [pollingVideoCheckboxSelector.getColumnDefinition(),
	         {
				id : "pollingVideoId",
				field : "pollingVideoId",
				name : "Video ID",
				width : 80,
				sortable : true
			},{
				id : "title",
				field : "title",
				name : "Title",
				width : 80,
				sortable : true
			},{
				id : "description",
				field : "description",
				name : "Description",
				width : 80,
				sortable : true
			},{
				id : "category",
				field : "category",
				name : "Category",
				width : 80,
				sortable : true
			},{
				id : "sponsorName",
				field : "sponsorName",
				name : "Sponsor",
				width : 80,
				sortable : true
			},{
				id : "videoUrl",
				field : "videoUrl",
				name : "Video Url",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Created By",
				width : 80,
				sortable : true
			},{
				id : "isDefault",
				field : "isDefault",
				name : "Default ",
				width : 80,
				sortable: true,
				formatter: isDefault
			},{
				id : "downloadVideo",
				field : "downloadVideo",
				name : "Preview Video",
				width : 80,
				formatter: editQBFormatter
			},{
				id : "downloadPoster",
				field : "downloadPoster",
				name : "Preview Poster",
				width : 80,
				formatter: previewPosterFormatter
			} ];

	if (userPollingVideoColumnsStr != 'null' && userPollingVideoColumnsStr != '') {
		columnOrder = userPollingVideoColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allPollingVideoColumns.length; j++) {
				if (columnWidth[0] == allPollingVideoColumns[j].id) {
					userPollingVideoColumns[i] = allPollingVideoColumns[j];
					userPollingVideoColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userPollingVideoColumns = allPollingVideoColumns;
	}
	
	function editQBFormatter(row,cell,value,columnDef,dataContext){  
	    var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.videoUrl +"'/>";
		return button;
	}
	function previewPosterFormatter(row,cell,value,columnDef,dataContext){
		var button = "<img class='editQBClickableImage' src='../resources/images/down.png' id='"+ dataContext.posterUrl +"'/>";
		return button;
	}
	
	
	function markAsPlayFormatter(row, cell, value, columnDef, dataContext) {
		var played = pollingVideoGrid.getDataItem(row).status;
		var link = '';
		if(played == 'ACTIVE'|| played == '1'||played==true){
			var but = "<label>ON</label>";
		}else {
			var but ="<label>OFF</label>";
			}
		return but;
	}
	
	function isDefault(row, cell, value, columnDef, dataContext) {
		var isDefault = pollingVideoGrid.getDataItem(row).isDefault;
		var link = '';
		if( isDefault == true){
			var but = "<label>DEFAULT</label>";
		}else {
			var but ="<label>--</label>";
			}
		return but;
	}
	$('.editQBClickableImage').live('click', function(){
	    var me = $(this), id = me.attr('id');
	   window.open(id,'MyWindow',width=600,height=300);
	});
	
	var pollingVideoOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var pollingVideoGridSortcol = "pollingVideoId";
	var pollingVideoGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function pollingVideoGridComparer(a, b) {
		var x = a[pollingVideoGridSortcol], y = b[pollingVideoGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshPollingVideoGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		pollingVideoData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (pollingVideoData[i] = {});
				d["id"] = i;
				d["pollingVideoId"] = data.id;
				d["title"] = data.title;
				d["description"] = data.description;
				d["category"] = data.category;
				d["sponsorName"] = data.sponsorName;
				d["videoUrl"] = data.videoUrl;
				d["status"] =  data.status;
				d["isDefault"] =  data.isDefault;
				d["createdBy"] = data.createdBy;
				d["posterUrl"] = data.posterUrl;
				d["createdDate"] = data.createdDateStr;
			}
		}

		pollingVideoDataView = new Slick.Data.DataView();
		pollingVideoGrid = new Slick.Grid("#pollingVideo_grid", pollingVideoDataView,
				userPollingVideoColumns, pollingVideoOptions);
		pollingVideoGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		var colTest = [];
		for ( var c = 0; c < userPollingVideoColumns.length; c++) {
			colTest.push(userPollingVideoColumns[c]);
			// }  
			pollingVideoGrid.setColumns(colTest);
		}
		pollingVideoGrid.invalidate();
		pollingVideoGrid.setSelectionModel(new Slick.RowSelectionModel());
		pollingVideoGrid.registerPlugin(pollingVideoCheckboxSelector);
		
		pollingVideoGridPager = new Slick.Controls.Pager(pollingVideoDataView,
					pollingVideoGrid, $("#pollingVideo_pager"),
					pagingInfo);
		var pollingVideoGridColumnpicker = new Slick.Controls.ColumnPicker(
				allPollingVideoColumns, pollingVideoGrid, pollingVideoOptions);
		
		
		/* pollingVideoGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = pollingVideoGrid.getCellFromEvent(e);
		      pollingVideoGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy pollingVideo'){
				var tempPollingVideoRowIndex = pollingVideoGrid.getSelectedRows([0])[0];
				if (tempPollingVideoRowIndex == null) {
					jAlert("Plese select PollingVideo to Copy", "info");
					return false;
				}else {
					var pollingVideoText = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).pollingVideo;
					var a = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).optionA;
					var b = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).optionB;
					var c = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).optionC;
					var answer = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).answer;
					var copyText = pollingVideoText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}else if($(e.target).attr("data") == 'edit pollingVideo'){
				editPollingVideo();
			}
		}); */
		
		
		pollingVideoGrid.onSort.subscribe(function(e, args) {
			pollingVideoGridSortcol = args.sortCol.field;
			if(sortingString.indexOf(pollingVideoGridSortcol) < 0){
				pollingVideoGridSortdir = 'ASC';
			}else{
				if(pollingVideoGridSortdir == 'DESC' ){
					pollingVideoGridSortdir = 'ASC';
				}else{
					pollingVideoGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+pollingVideoGridSortcol+',SORTINGORDER:'+pollingVideoGridSortdir+',';
			getPollingVideoGridData(0);
		});
		
		// wire up model discountCodes to drive the pollingVideoGrid
		pollingVideoDataView.onRowCountChanged.subscribe(function(e, args) {
			pollingVideoGrid.updateRowCount();
			pollingVideoGrid.render();
		});
		pollingVideoDataView.onRowsChanged.subscribe(function(e, args) {
			pollingVideoGrid.invalidateRows(args.rows);
			pollingVideoGrid.render();
		});
		$(pollingVideoGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							pollingVideoSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								pollingVideoColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in pollingVideoColumnFilters) {
										if (columnId !== undefined
												&& pollingVideoColumnFilters[columnId] !== "") {
											pollingVideoSearchString += columnId
													+ ":"
													+ pollingVideoColumnFilters[columnId]
													+ ",";
										}
									}
									getPollingVideoGridData(0);
								}
							}

						});
		pollingVideoGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id != 'downloadPoster' && args.column.id != 'downloadVideo'){
				if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(pollingVideoColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>").data("columnId", args.column.id)
							.val(pollingVideoColumnFilters[args.column.id]).appendTo(
									args.node);
				}
			}

		});
		pollingVideoGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		pollingVideoDataView.beginUpdate();
		pollingVideoDataView.setItems(pollingVideoData);
		//pollingVideoDataView.setFilter(filter);
		pollingVideoDataView.endUpdate();
		pollingVideoDataView.syncGridSelection(pollingVideoGrid, true);
		pollingVideoGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserPollingVideoPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = pollingVideoGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('pollingvideogrid', colStr);
	}
	
	// Add PollingVideo 	
	function resetPollingVideoModal(){		
		$('#pollingVideoModal').modal('show');
		$('#videoId').val('');
		$('#action').val('SAVE');
		$('#title').val('');
		$('#description').val('');
		$('#category').val('');
		$('#sponsor').val(0);
		$('#imageFileDiv').show();
		$('#videoImageDiv').hide();
		$('#saveBtn').show();
		$('#updateBtn').hide();	
		
	}

	function savePollingVideo(action){
		
		var title = $('#title').val();
		var description = $('#description').val();
		var category = $('#category').val();
		var videoFile = $('#videoFile').val();
		var imageFile = $('#imageFile').val();
		
		
		if(title == ''){
			jAlert("RTF Video title is mandatory.");
			return;
		}
		if(description == ''){
			jAlert("RTF Video Description is mandatory.");
			return;
		}
		if(category == ''){
			jAlert("RTF Video Category is mandatory.");
			return;
		}
		if(videoFile == '' || videoFile == null){
			jAlert("RTF Video is mandatory.");
			return;
		}
		if(validateVideoFile($('#videoFile').val()) == false){
			jAlert("Invalid Video File. only MP4 is supported","Info");
			return false;
		}
		$('#action').val(action);
		$('#rtfVideoFile').val(videoFile);
		$('#rtfImageFile').val(imageFile);
		
		var requestUrl = "${pageContext.request.contextPath}/UpdatePollingVideo";
		var form = $('#pollingVideoForm')[0];
		var dataString = new FormData(form);
		
		/* if(action == 'SAVE'){		
			dataString  = dataString+"&action=SAVE&pageNo=0";
		}else if(action == 'UPDATE'){
			dataString = dataString+"&action=UPDATE&pageNo=0";
		} */
		$.ajax({
			url : requestUrl,
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : dataString,
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){
					$('#pollingVideoModal').modal('hide');
					pagingInfo = JSON.parse(jsonData.videoPagingInfo);
					pollingVideoColumnFilters = {};
					refreshPollingVideoGridValues(JSON.parse(jsonData.videos));
					clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is deleted please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	

	function validateVideoFile(videoFile) {
		var ext = $('#videoFile').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['mp4','MP4']) == -1) {
			return false;
		}else{
			return true;
		}
	}
	//Edit PollingVideo 
	function editPollingVideo(){
		var tempPollingVideoRowIndex = pollingVideoGrid.getSelectedRows([0])[0];
		if (tempPollingVideoRowIndex == null) {
			jAlert("Plese select PollingVideo to Edit", "info");
			return false;
		}else {
			var pollingVideoId = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).pollingVideoId;
			getEditPollingVideo(pollingVideoId);
		}
	}
	
	function getEditPollingVideo(pollingVideoId){
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePollingVideo",
			type : "post",
			dataType: "json",
			data: "videoId="+pollingVideoId+"&action=EDIT&pageNo=0&videoStatus=${videoStatus}",
			success : function(response){				
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status == 1){					
					$('#pollingVideoModal').modal('show');
					setEditPollingVideo(JSON.parse(jsonData.video));
					//setPollingVideoQuantityGroups(JSON.parse(jsonData.qtyList));
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setEditPollingVideo(pollingVideo){
		$('#saveBtn').hide();
		$('#updateBtn').show();
		var data = pollingVideo;
		$('#videoId').val(data.id);
		$('#title').val(data.title);
		$('#description').val(data.description);
		$('#category').val(data.category);
		if(data.sponsorId != null){
			$('#sponsor').val(data.sponsorId);
		}
		$('#downloadVideoA').text(data.fileName);
		
		//$("#videoImageTag").attr("src", apiServerUrl+"GetImageFile?type=pollingVideoImage&filePath="+data.fileName);
	}
	
	function getSelectedPollingVideoGridId() {
		var tempPollingVideoRowIndex = pollingVideoGrid.getSelectedRows();
		
		var pollingVideoIdStr='';
		$.each(tempPollingVideoRowIndex, function (index, value) {
			pollingVideoIdStr += ','+pollingVideoGrid.getDataItem(value).pollingVideoId;
		});
		
		if(pollingVideoIdStr != null && pollingVideoIdStr!='') {
			pollingVideoIdStr = pollingVideoIdStr.substring(1, pollingVideoIdStr.length);
			 return pollingVideoIdStr;
		}
	}
	//Delete PollingVideo 
	function deletePollingVideo(){
		var pollingVideoIds = getSelectedPollingVideoGridId();
		if (pollingVideoIds == null || pollingVideoIds == '' || pollingVideoIds == undefined) {
			jAlert("Plese select PollingVideo to Delete", "info");
			return false;
		}else {
			//var pollingVideoId = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).pollingVideoId;
			getDeletePollingVideo(pollingVideoIds);
		}
	}
	
	
	function getDeletePollingVideo(pollingVideoIds){
		jConfirm("Are you sure to delete selected PollingVideos ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingVideo",
						type : "post",
						dataType: "json",
						data : "videoId="+pollingVideoIds+"&action=DELETE&videoStatus=${videoStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.videoPagingInfo);
								pollingVideoColumnFilters = {};
								refreshPollingVideoGridValues(JSON.parse(jsonData.videos));
								clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//update status of RTF videos
	function updatePollingVideo(action){
		var action = action;
		var pollingVideoIds = getSelectedPollingVideoGridId();
		if (pollingVideoIds == null || pollingVideoIds == '' || pollingVideoIds == undefined) {
			jAlert("Plese select PollingVideo to Update", "info");
			return false;
		}else {
			//var pollingVideoId = pollingVideoGrid.getDataItem(tempPollingVideoRowIndex).pollingVideoId;
			getUpdatePollingVideo(pollingVideoIds, action);
		}
	}
	
	
	function getUpdatePollingVideo(pollingVideoIds, action){
		jConfirm("Are you sure to update selected PollingVideos ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePollingVideo",
						type : "post",
						dataType: "json",
						data : "videoId="+pollingVideoIds+"&action="+action+"&videoStatus=${videoStatus}",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = JSON.parse(jsonData.videoPagingInfo);
								pollingVideoColumnFilters = {};
								refreshPollingVideoGridValues(JSON.parse(jsonData.videos));
								clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${videoPagingInfo};		
		refreshPollingVideoGridValues(${videos});
		$('#pollingVideo_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPollingVideoPreference()'>");
		
		enableMenu();
	};
		
</script>