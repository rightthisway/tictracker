<!DOCTYPE HTML>
<html>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>Manage Popular Artists</title>
  
  <link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css"/>
  <link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css"/>
	
	<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
	<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
	<script src="../resources/js/slick/slick.core.js"></script>
	<script src="../resources/js/slick/slick.formatters.js"></script>
	<script src="../resources/js/slick/slick.editors.js"></script>
	<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
	<script src="../resources/js/slick/slick.grid.js"></script>
	<script src="../resources/js/slick/slick.dataview.js"></script>
	<script src="../resources/js/slick/controls/slick.pager.js"></script>
	<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
	<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
	<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
	
<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
      background: #87ceeb;
      text-overflow: clip;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
    .slick-headerrow-column input {
      margin: 0;
      padding: 0;
      width: 100%;
      height: 100%;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

img.editClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.deleteClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

img.auditClickableImage{
   cursor: pointer; 
   vertical-align: middle;
}

</style>
</head>
<body>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Artist</a>
			</li>
			<li><i class="fa fa-laptop"></i>Manage Popular Artist</li>
		</ol>
	</div>
	
	<!-- 
	<div class="col-lg-12">
                
			<form:form class="form-inline" role="form" id="artistSearch" onsubmit="return false" method="post" action="${pageContext.request.contextPath}/RewardTheFan/PopularArtist">
			
				<div class="col-sm-1"> </div>
				
				<div class="col-sm-2">
					<select id="searchType" name="searchType" class="form-control input-sm m-bot15" style = "width:100%">
 					  <option value="artistName">Artist</option>
					  <option value="grandChildCategoryName" <c:if test="${grandChildCategoryName eq 'artistName'}">selected</c:if> >GrandChildCategory</option>
					  <option value="childCategoryName" <c:if test="${searchType eq 'childCategoryName'}">selected</c:if> >ChildCategory</option>
					  <option value="parentCategoryName" <c:if test="${searchType eq 'parentCategoryName'}">selected</c:if> >ParentCategory</option>
					</select>
				</div>
               <div class="col-sm-3">
				<div class="navbar-form">
					<input class="form-control" placeholder="Search" type="text" id="searchValue" name="searchValue" value="${searchValue}">
				</div>
				</div>
				</form:form>
				<div class="col-sm-3">
				 	 <button type="button" class="btn btn-primary" onclick="searchArtistGrid();">search</button>
				 </div>
				 <div class="col-sm-1"> </div>
                          
           </div>
           --> 
           <input type="hidden" id="productId" name="productId" value="${product.id}" />
</div>
<br/>

<!-- artist Grid Code Starts -->
<div style="position: relative" id="artistGridDiv">
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label>Artist Details</label>
			<div class="pull-right">
				<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="artist_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="artist_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>

<script>

$(document).ready(function() {
	$('#artistSearch').submit(function(){
		e.preventDefault();
	});
	
	$('#searchValue').keypress(function (event) {
		
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 searchArtistGrid();
		    //$('#searchPopularArtistBtn').click();
		    //return false;  
		  }
	});
});

var artistCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});
	
	var pagingInfo;
	var artistDataView;
	var artistGrid;
	var artistData = [];
	var artistSearchString='';
	var sortingString='';
	var columnFilters = {};
	var userArtistColumnsStr = '<%=session.getAttribute("artistgrid")%>';
	var userArtistColumns =[];
	var allArtistColumns = [ artistCheckboxSelector.getColumnDefinition(),
	{id: "artist", 
		name: "Artist", 
		field: "artist",
		width:80,		
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,			
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	} ];

	if(userArtistColumnsStr!='null' && userArtistColumnsStr!=''){
		columnOrder = userArtistColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allArtistColumns.length;j++){
				if(columnWidth[0] == allArtistColumns[j].id){
					userArtistColumns[i] =  allArtistColumns[j];
					userArtistColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userArtistColumns = allArtistColumns;
	}
	
	var artistOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var artistGridSortcol = "artistName";
	var artistGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromartistGrid(id) {
		artistDataView.deleteItem(id);
		artistGrid.invalidate();
	}
	/*
	function artistGridFilter(item, args) {
		var x= item["artistName"];
		if (args.artistGridSearchString  != ""
				&& x.indexOf(args.artistGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.artistGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function artistGridComparer(a, b) {
		var x = a[artistGridSortcol], y = b[artistGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function artistGridToggleFilterRow() {
		artistGrid.setTopPanelVisibility(!artistGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#artist_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
			$(e.target).addClass("ui-state-hover")
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover")
		});
	*/
	function pagingControl(move,id){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(pagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(pagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(pagingInfo.pageNum)-1;
		}
		getArtistGridData(pageNo);
	}
	
	function searchArtistGrid(){
		var searchValue = $("#searchValue").val();
		if(searchValue== null || searchValue == '') {
			jAlert('Enter valid key to searh artists');
			return false;
		}else{
			getArtistGridData(0);
		}
	}
	
	function exportToExcel(){
		var appendData = "headerFilter="+artistSearchString;
	    var url = apiServerUrl + "ArtistExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function resetFilters(){
		artistSearchString='';
		columnFilters = {};
		getArtistGridData(0);
	}
	
	function getArtistGridData(pageNo) {
		var searchValue = $("#searchValue").val();
		$.ajax({			  
			url : "${pageContext.request.contextPath}/ArtistImages.json",
			type : "post",
			dataType: "json",
			data : "searchValue="+searchValue+"&searchType="+$("#searchType").val()+"&pageNo="+pageNo+"&headerFilter="+artistSearchString+"&sortingString="+sortingString,
			//dataType:"application/json",
			/* async : false, */
			success : function(res){
				var jsonData = res;
				//jAlert(jsonData);
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Artist found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.artistPagingInfo;
				refreshartistGridValues(jsonData.artistList);//,true);
				clearAllSelections();
				$('#artist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	
function refreshartistGridValues(jsonData){
	 $("div#divLoading").addClass('show');
		artistData = [];		
		
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (artistData[i] = {});
				d["artistId"] = data.artistId;
				d["id"] =  i;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				d["artist"] = data.artistName;
			}
		  }

		artistDataView = new Slick.Data.DataView();
		artistGrid = new Slick.Grid("#artist_grid", artistDataView, userArtistColumns, artistOptions);
		artistGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		artistGrid.setSelectionModel(new Slick.RowSelectionModel());
		artistGrid.registerPlugin(artistCheckboxSelector);
		if(pagingInfo != null){
			var artistGridPager = new Slick.Controls.Pager(artistDataView, artistGrid, $("#artist_pager"),pagingInfo);
		}
		var artistGridColumnpicker = new Slick.Controls.ColumnPicker(allArtistColumns, artistGrid,
				artistOptions);

		// move the filter panel defined in a hidden div into artistGrid top panel
		//$("#artist_inlineFilterPanel").appendTo(artistGrid.getTopPanel()).show();

		/*artistGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < artistDataView.getLength(); i++) {
				rows.push(i);
			}
			artistGrid.setSelectedRows(rows);
			e.prartistDefault();
		});*/
		
		artistGrid.onSort.subscribe(function(e, args) {
			artistGridSortcol = args.sortCol.field;			
			if(sortingString.indexOf(artistGridSortcol) < 0){
				artistGridSortdir = 'ASC';
			}else{
				if(artistGridSortdir == 'DESC' ){
					artistGridSortdir = 'ASC';
				}else{
					artistGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+artistGridSortcol+',SORTINGORDER:'+artistGridSortdir+',';
			getArtistGridData(0)			
		});
		// wire up model artists to drive the artistGrid
		artistDataView.onRowCountChanged.subscribe(function(e, args) {
			artistGrid.updateRowCount();
			artistGrid.render();
		});
		artistDataView.onRowsChanged.subscribe(function(e, args) {
			artistGrid.invalidateRows(args.rows);
			artistGrid.render();
		});
		$(artistGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	artistSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						  artistSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getArtistGridData(0);
				}
			  }
		 
		});
		artistGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(columnFilters[args.column.id])
			   .appendTo(args.node);
			}
			
		});
		artistGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#artistGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			artistGridSearchString = this.value;
			updateartistGridFilter();
		});
		function updateartistGridFilter() {
			artistDataView.setFilterArgs({
				artistGridSearchString : artistGridSearchString
			});
			artistDataView.refresh();
		}
		*/
		// initialize the model after all the artists have been hooked up
		artistDataView.beginUpdate();
		artistDataView.setItems(artistData);
		/*artistDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			artistGridSearchString : artistGridSearchString
		});
		artistDataView.setFilter(artistGridFilter);*/
		artistDataView.endUpdate();
		artistDataView.syncGridSelection(artistGrid, true);
		artistGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		 $("div#divLoading").removeClass('show');
		/* var artistrowIndex;
		artistGrid.onSelectedRowsChanged.subscribe(function() { 
			var temprartistRowIndex = artistGrid.getSelectedRows([0])[0];
			if (temprartistRowIndex != artistrowIndex) {
				artistrowIndex = temprartistRowIndex;
				 var artistId =artistGrid.getDataItem(temprartistRowIndex).id;
				//jAlert(artistId);
				//getCategoryTicketGroupsforartist(artistId);
			}
		}); */
	}
	
	function getSelectedartistGridId() {
		var temprartistRowIndex = artistGrid.getSelectedRows();
		
		var artistIdStr='';
		$.each(temprartistRowIndex, function (index, value) {
			artistIdStr += ','+artistGrid.getDataItem(value).artistId;
		});
		
		if(artistIdStr != null && artistIdStr!='') {
			artistIdStr = artistIdStr.substring(1, artistIdStr.length);
			 return artistIdStr;
		}
	}
	function getSelectedArtistGridRowCount() {
		var temprartistRowIndex = artistGrid.getSelectedRows();
		
		var rowCount=0;
		$.each(temprartistRowIndex, function (index, value) {
			rowCount++;
		});
		
		return rowCount;
	}
	
	function updatePopularArtist(action,artistIds,productId) {
		$.ajax({
			url : "${pageContext.request.contextPath}/UpdatePopularArtist",
			type : "post",
			data : "action="+action+"&productId="+productId+"&artistIds="+ artistIds+"&headerFilter="+popArtistSearchString,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.popArtists.length >0 && action != 'search'){
					jAlert("Selected Artist added to popular artists.")
				}
				refreshpopArtistGridValues(jsonData.popArtists);
				artistGrid.setSelectedRows([]);
				clearAllSelections();
				$('#popArtist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPopArtistPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	function addPopularArtists() {
		var artistIds = getSelectedartistGridId();
		var productId=$("#productId").val();
		var flag = true;
		if(artistIds == null || artistIds=='') {
			flag= false;
			jAlert("Select a artists to add as Popular artist.");
			return false;
		}
		
		var selectedArtistsCount = getSelectedArtistGridRowCount();
		if(flag) {
			$.ajax({
				url : "${pageContext.request.contextPath}/GetPopularArtistsCount",
				type : "post",
				data : "productId="+productId,
				success : function(res){
					//var jsonData = JSON.parse(JSON.stringify(res));
					if((selectedArtistsCount+ parseInt(res.count)) > 20){
						jAlert("Maximum Eligible popluar artists are 20.\n"
								+"Remove some existing poluar artists(Count : "+parseInt(res.count)+") to add selected artists as popular artists.");
						return false;
					} else {
						updatePopularArtist('create',artistIds,productId);
					}				
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		return false;	
		} 
	}

</script>

<!-- artist Grid Code Starts -->
<br />
<!-- <header class="panel-heading"> 
	<a data-toggle="modal" class="btn btn-primary" id="addCategoryTickets" onclick="addModal();">Add</a>
</header> -->

<!-- popartist Grid Code Starts -->
<br />
<div style="position: relative;" id="popArtistGridDiv">
<div class="full-width mb-20 mt-20 full-width-btn">
	<a data-toggle="modal" class="btn btn-primary" id="addPopularArtists" onclick="addPopularArtists();">Add Popular Artist</a>
</div>
<div id="AddpopArtistGroupSuccessMsg" style="display: none;" class="alert alert-success fade in">
 	Popular Artist Added Successfully.
  </div>
  
	<div class="table-responsive grid-table">
		<div class="grid-header full-width">
			<label id="popArtistGridHeaderLabel">Popular Artists</label>
			<div class="pull-right">
				<a href="javascript:popArtistExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
				<a href="javascript:popArtistResetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
			</div>
		</div>
		<div id="popArtist_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
		<div id="popArtist_pager" style="width: 100%; height: 10px;"></div>
	</div>
</div>

<br />
<br />
<div class="full-width mb-20 mt-20 full-width-btn">
	<a data-toggle="modal" class="btn btn-primary" id="deletePopularArtists" onclick="deletePopularArtists();">Delete Popular Artists</a>
</div>
<script>

function popArtistExportToExcel(){
	var appendData = "headerFilter="+popArtistSearchString;
    var url = apiServerUrl + "PopularArtistExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function popArtistResetFilters(){
	popArtistSearchString='';
	popArtistColumnFilters = {};
	sortingString ='';
	var artistIds = getSelectedartistGridId();
	var productId=$("#productId").val();
	updatePopularArtist('search',artistIds,productId);
}

var popArtistCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	
	var popArtistPagingInfo;
	var popArtistDataView;
	var popArtistGrid;
	var popArtistData = [];
	var popArtistSearchString='';
	var popArtistColumnFilters = {};
	var userPopArtistColumnsStr = '<%=session.getAttribute("popartistgrid")%>';
	var userPopArtistColumns =[];
	var allPopArtistColumns =  [ popArtistCheckboxSelector.getColumnDefinition(),
	{id: "artist", 
		name: "Artist", 
		field: "artist", 
		width:80,
		sortable: true
	}, {id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		width:80,
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	},{id: "createdBy", 
		name: "Created By", 
		field: "createdBy", 
		width:80,
		sortable: true
	}, {
		id : "createdDate",
		field : "createdDate",
		name : "Created Date",
		width:80,
		sortable: true
	} ];

	if(userPopArtistColumnsStr!='null' && userPopArtistColumnsStr!=''){
		var columnOrder = userPopArtistColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allPopArtistColumns.length;j++){
				if(columnWidth[0] == allPopArtistColumns[j].id){
					userPopArtistColumns[i] =  allPopArtistColumns[j];
					userPopArtistColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		userPopArtistColumns = allPopArtistColumns;
	}
	
	var popArtistOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var popArtistGridSortcol = "ArtistName";
	var popArtistGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var popArtistGridSearchString = "";

	function deleteRecordFromArtistGrid(id) {
		popArtistDataView.deleteItem(id);
		popArtistGrid.invalidate();
	}
	/*
	function popArtistGridFilter(item, args) {
		var x= item["artistName"];
		if (args.popArtistGridSearchString  != ""
				&& x.indexOf(args.popArtistGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.popArtistGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function popArtistGridComparer(a, b) {
		var x = a[popArtistGridSortcol], y = b[popArtistGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function popArtistGridToggleFilterRow() {
		popArtistGrid.setTopPanelVisibility(!popArtistGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#popArtist_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});

	*/	
	function refreshpopArtistGridValues(jsonData){		
		/*if(ArtistdetailStr != '0') {
			$('#popArtistGridDiv').show();
			//$('#popArtistGridHeaderLabel').text(ArtistdetailStr);
		}*/
		popArtistData = [];
		
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (popArtistData[i] = {});
				d["artistId"] = data.artistId;
				d["id"] = i;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				d["artist"] = data.artistName;
				d["createdBy"] = data.createdBy;
				d["createdDate"] = data.createdDate;
			}
		}		
		
		popArtistDataView = new Slick.Data.DataView();
		popArtistGrid = new Slick.Grid("#popArtist_grid", popArtistDataView, userPopArtistColumns, popArtistOptions);
		popArtistGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		popArtistGrid.setSelectionModel(new Slick.RowSelectionModel());
		popArtistGrid.registerPlugin(popArtistCheckboxSelector);
		if(popArtistPagingInfo != null){
			var popArtistGridPager = new Slick.Controls.Pager(popArtistDataView, popArtistGrid, $("#popArtist_pager"),popArtistPagingInfo);
		}
		var popArtistGridColumnpicker = new Slick.Controls.ColumnPicker(allPopArtistColumns, popArtistGrid,
				popArtistOptions);

		// move the filter panel defined in a hidden div into popArtistGrid top panel
		//$("#popArtist_inlineFilterPanel").appendTo(popArtistGrid.getTopPanel()).show();

		/* popArtistGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < popArtistDataView.getLength(); i++) {
				rows.push(i);
			}
			popArtistGrid.setSelectedRows(rows);
			e.preventDefault();
		}); */
		
		popArtistGrid.onSort.subscribe(function(e, args) {
			popArtistGridSortdir = args.sortAsc ? 1 : -1;
			popArtistGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				popArtistDataView.fastSort(popArtistGridSortcol, args.sortAsc);
			} else {
				popArtistDataView.sort(popArtistGridComparer, args.sortAsc);
			}
		});
		// wire up model popArtists to drive the popArtistGrid
		popArtistDataView.onRowCountChanged.subscribe(function(e, args) {
			popArtistGrid.updateRowCount();
			popArtistGrid.render();
		});
		popArtistDataView.onRowsChanged.subscribe(function(e, args) {
			popArtistGrid.invalidateRows(args.rows);
			popArtistGrid.render();
		});
		$(popArtistGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	popArtistSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  popArtistColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in popArtistColumnFilters) {
					  if (columnId !== undefined && popArtistColumnFilters[columnId] !== "") {
						  popArtistSearchString += columnId + ":" +popArtistColumnFilters[columnId]+",";
					  }
					}
					var artistIds = getSelectedartistGridId();
					var productId=$("#productId").val();
					updatePopularArtist('search',artistIds,productId);
				}
			  }
		 
		});
		popArtistGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(popArtistColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(popArtistColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
			
		});
		popArtistGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#popArtistGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			popArtistGridSearchString = this.value;
			updatePopArtistGridFilter();
		});
		function updatePopArtistGridFilter() {
			popArtistDataView.setFilterArgs({
				popArtistGridSearchString : popArtistGridSearchString
			});
			popArtistDataView.refresh();
		}
		*/
		// initialize the model after all the popArtists have been hooked up
		popArtistDataView.beginUpdate();
		popArtistDataView.setItems(popArtistData);
		/*popArtistDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			popArtistGridSearchString : popArtistGridSearchString
		});
		popArtistDataView.setFilter(popArtistGridFilter);*/
		popArtistDataView.endUpdate();
		popArtistDataView.syncGridSelection(popArtistGrid, true);
		popArtistGrid.setSelectedRows([]);
		popArtistGrid.resizeCanvas();
	}
	
	<!-- popArtist Grid Code Ends -->
	
	function getSelectedpopArtistGridId() {
		var temprArtistRowIndex = popArtistGrid.getSelectedRows();
		
		var artistIdStr='';
		$.each(temprArtistRowIndex, function (index, value) {
			artistIdStr += ','+popArtistGrid.getDataItem(value).artistId;
		});
		
		if(artistIdStr != null && artistIdStr!='') {
			artistIdStr = artistIdStr.substring(1, artistIdStr.length);
			 return artistIdStr;
		}
	}
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  artistGrid.resizeCanvas();
	  popArtistGrid.resizeCanvas();
	});
	
	function deletePopularArtists() {
		var artistIds = getSelectedpopArtistGridId();
		var productId=$("#productId").val();
		if(artistIds != null && artistIds!='') {
			jConfirm("Are you sure you want to remove selected artists from popular artists?","confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/UpdatePopularArtist",
						type : "post",
						data : "action=delete&productId="+productId+"&artistIds="+ artistIds,
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							jAlert("Selected Artist removed from popular Artists.");
							refreshpopArtistGridValues(jsonData.popArtists) ;
							clearAllSelections();
						}, error : function(error){
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}
			});
		} else{
			jAlert("Select an Artist to remove from Popular Artists.");
		}
	}
	
	function clearAllSelections(){
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					eventCheckbox[i].checked = false;
				}
			}
		}
		
	}
	
	function saveUserArtistPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = artistGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('artistgrid',colStr);
	}
	function saveUserPopArtistPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = popArtistGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('popartistgrid',colStr);
	}
	
	
	//call functions once page loaded
	$(window).load(function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshartistGridValues(JSON.parse(JSON.stringify(${artistList})));
		popArtistPagingInfo = JSON.parse(JSON.stringify(${popArtistPagingInfo}));
		refreshpopArtistGridValues(JSON.parse(JSON.stringify(${popArtists})));
		//refreshartistGridValues(null,false);
		//refreshpopArtistGridValues(null,false);
		$('#artist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");
		$('#popArtist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserPopArtistPreference()'>");
		enableMenu();
	});
	
</script>
	
</body>
</html>