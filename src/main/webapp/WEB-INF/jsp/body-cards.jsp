<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>

<script type="text/javascript">
	
</script>

<style>
input {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
  
</style>

<script type="text/javascript">
var jq2 = $.noConflict(true);
$(document).ready(function(){
	
	//jq2('.autoArtist').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
	jq2('.autoArtist').autocomplete("${pageContext.request.contextPath}/Admin/AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		  var rowId = $(this).attr('id').split("_")[1];
		  
			$('#tmatArtist_'+rowId).val('');
			$('#tmatArtistText_'+rowId).text(row[2]);
			$('#artistId_'+rowId).val(row[1]);
			
	});
});


var validFilesTypes = ["jpg", "jpeg"];

    function CheckExtension(file) {
        /*global document: false */
        var filePath = file.value;
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        var isValidFile = false;

        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
        if (!isValidFile) {
            file.value = null;
            jAlert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
        }

        return isValidFile;
    }
    
    var validFileSize = 1 * 1024 * 1024;
    function CheckFileSize(file) {
        /*global document: false */
        var fileSize = file.files[0].size;
        var isValidFile = false;
        if (fileSize !== 0 && fileSize <= validFileSize) {
            isValidFile = true;
        }
        else {
            file.value = null;
            jAlert("File Size Should be Greater than 0 and less than 1 MB.");
        }
        return isValidFile;
    }
    
    function CheckFile(file) {
    	
    	if(file == null || file.value == null) {
    		jAlert("Please select valid file to update card.");
    		isValidFile = false;
    		return false;
    	}
        var isValidFile = CheckExtension(file);

        if (isValidFile)
            isValidFile = CheckFileSize(file);

        return isValidFile;
    }

function callCardTypeChange(obj,rowId) {
	if(obj.value=='PROMOTION') {
		$('#cardDetailDiv_'+rowId).show();
		$('#tmatArtistDiv_'+rowId).show();
		$('#searchCategoryDiv_'+rowId).hide();
	} else if(obj.value=='MAINPROMOTION') {
		$('#cardDetailDiv_'+rowId).show();
		$('#tmatArtistDiv_'+rowId).hide();
		$('#searchCategoryDiv_'+rowId).show();
	} else if(obj.value=='YESNO') {
		$('#cardDetailDiv_'+rowId).show();
		$('#tmatArtistDiv_'+rowId).hide();
		$('#searchCategoryDiv_'+rowId).hide();
	} else {
		$('#cardDetailDiv_'+rowId).hide();
		$('#tmatArtistDiv_'+rowId).hide();
		$('#searchCategoryDiv_'+rowId).hide();
	}
}


function callBtnOnClick(rowNumber) {
	
	var flag = true;
	var mobileFlag = true;
	
	var cardType = document.getElementById("cardType_"+rowNumber);
	if(cardType == null || cardType.value=='') {
		jAlert('Please select Cardtype to update card.');
		flag = false;
		return false;
	}
	var fileObj = document.getElementById("file_"+rowNumber);
	var mobileFileObj = document.getElementById("mobileFile_"+rowNumber);
	if(cardType.value == 'PROMOTION') {
		flag = CheckFile(fileObj);
		
		if(flag) {
			
			var imageTextObj = document.getElementById("imageText_"+rowNumber);
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please enter Valid Image Text to update Promotion Card.');
				flag = false;
				return false;
			}
			var tmatArtistIdObj = document.getElementById("file_"+rowNumber);
			if(tmatArtistIdObj == null || tmatArtistIdObj.value=='') {
				jAlert('Please select Valid TMAT artist to update Promotion Card.');
				flag = false;
				return false;
			}
		}
		
	} else if(cardType.value == 'MAINPROMOTION') {
		flag = CheckFile(fileObj);
		mobileFlag = CheckFile(mobileFileObj);
		
		if(flag && mobileFlag) {
			
			var imageTextObj = document.getElementById("imageText_"+rowNumber);
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please enter Valid Image Text to update Main Promotion Card.');
				flag = false;
				return false;
			}
			var searchCategoryIdObj = document.getElementById("file_"+rowNumber);
			if(searchCategoryIdObj == null || searchCategoryIdObj.value=='') {
				jAlert('Please select Valid Search Category to update Main Promotion Card.');
				flag = false;
				return false;
			}
		}
		
	} else if(cardType.value == 'YESNO') {
		flag = CheckFile(fileObj);
		
		if(flag) {
			var imageTextObj = document.getElementById("imageText_"+rowNumber);
			if(imageTextObj == null || imageTextObj.value=='') {
				jAlert('Please enter Valid Image Text to update Yes/No Card.');
				flag = false;
				return false;
			}
		}
	}
	if(flag) {
		var cardPositionObj = document.getElementById("cardPosition_"+rowNumber);
		if(cardPositionObj == null || cardPositionObj.value=='') {
			jAlert('Please Enter valid card position.');
			flag = false;
			return false;
		} 
			var regex = /[0-9]|\./;
		  if( !regex.test(cardPositionObj.value) ) {
			  jAlert('Please Enter valid card position.');
				flag = false;
				return false;
		  }

		var cardId = $("#cardId_"+rowNumber).val();
		var isExistingCard = checkExistingCard(cardId,cardPositionObj.value);
		if(isExistingCard) {
			flag = false;
			return false;
		}
	}
	
	if(flag) {
		$("#fileForm_"+rowNumber).attr('action', '${pageContext.request.contextPath}/RewardTheFan/updateCards');
		$("#fileForm_"+rowNumber).submit();	
	}
	
}
	function checkExistingCard(cardId,cardPosition) {
		var productId=$("#productId").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/CheckCardPosition",
			type : "post",
			data : "productId="+productId+"&cardId="+ cardId+"&cardPosition="+cardPosition,
			success : function(res){
				if(res=="Card Already Exist for this Position.") {
					jAlert(res);
					return true;
				} else {
					return false;
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return true;
			}
		});
	}

function callModifyBtnOnClick(rowNumber) {
	//$('#imageRow_'+rowNumber).show();
	//$('#cardImageRow_'+rowNumber).hide();
	//var cardType = document.getElementById("cardType_"+rowNumber);
	//callCardTypeChange(cardType,rowNumber);
	var cardId = document.getElementById("cardId_"+rowNumber).value;
	var productId = document.getElementById("productId").value;
	popupUpdateCard(cardId,productId);
}

function callCreateBtnOnClick() {
	var productId = document.getElementById("productId").value;
	popupUpdateCard(null,productId);
}

//popup user edit
function popupUpdateCard(cardId,productId) {
	$('#infoMainDiv').hide();
	var params = "productId="+productId;
	if(cardId!=null) {
		params = params + "&cardId=" + cardId;
	}
	var editUserUrl = "${pageContext.request.contextPath}/RewardTheFan/CardsUpdatePopup?"+params;
	popupCenter(editUserUrl, 'Update Cards', '1300', '500');
}

//show the pop window center
function popupCenter(url, title, w, h) {
	// Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    } 
}

function callChildWindowClose(info) {
	$('#infoDiv').text(info);
	$('#infoMainDiv').show();
	window.location="${pageContext.request.contextPath}/RewardTheFan/Cards?info="+info;
}
</script>

<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> ${productName}</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">${productName}</a></li>
						<li><i class="fa fa-laptop"></i>Cards</li>						  	
					</ol>
				</div>
</div>

<div class="container">
<div class="row clearfix">

<div class="alert alert-success fade in" id="infoMainDiv"
<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
                                  <button data-dismiss="alert" class="close close-sm" type="button">
                                      <i class="icon-remove"></i>
                                  </button>
                                  <span id="infoDiv">${info}</span>
                              </div>
</div>
    <div class="row clearfix">
		<div class="full-width column">
			<input type="hidden" name="action" id="action" value="action"/>
			<input type="hidden" name="productId" id="productId" value="${product.id}"/>
			<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="col-lg-3">
							Card Type
						</th>
						<th class="col-lg-4">
							Card Details
						</th>
						<th class="col-lg-2">
							Category Details
						</th>
						<th class="col-lg-1">
							Desktop Position
						</th>
						<th class="col-lg-1">
							Mobile Position
						</th>
						<th class="col-lg-1">
							Action
						</th>
						
					</tr>
				</thead>
				<tbody>
				 <c:forEach var="card" varStatus="vStatus" items="${cardsList}">
				<form role="form" name="fileForm" enctype="multipart/form-data" id="fileForm_${vStatus.count}" method="post" modelAttribute="cards" action="${pageContext.request.contextPath}/Client/AddCategoryTickets">
                      <tr id="imageRow_${vStatus.count}" 
                      <c:if test="${card.id ne null}">style="display: none;"</c:if> >
						<td style="vertical-align: middle;  ">
						<input type="hidden" name="rowNumber" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
						<input type="hidden" name="cardId" id="cardId_${vStatus.count}" value="${card.id}" />
						<select name="cardType" id="cardType_${vStatus.count}" class="form-control input-sm m-bot15" onchange="callCardTypeChange(this,'${vStatus.count}');">
                        	<option value="">--- Select ---</option>
							<option value="UPCOMMINGEVENT" <c:if test="${card.cardType eq 'UPCOMMINGEVENT'}">selected</c:if> >UpComming Event Card</option>
							<option value="MAINPROMOTION" <c:if test="${card.cardType eq 'MAINPROMOTION'}">selected</c:if>>Main Promotion Card</option>
							<option value="PROMOTION" <c:if test="${card.cardType eq 'PROMOTION'}">selected</c:if>>Promotion Card</option>
							<option value="YESNO" <c:if test="${card.cardType eq 'YESNO'}">selected</c:if> >Yes/No Card</option>
                       </select>
						</td>
						<td style="font-size: 13px;" align="center">
							<div class="form-horizontal" id="cardDetailDiv_${vStatus.count}" 
								<c:if test="${card.id ne null or (card.cardType eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO' and card.cardType ne 'MAINPROMOTION'))}">style="display: none;" </c:if>>
                                  <div class="form-group form-group-top" id="imageFileDiv_${vStatus.count}">
                                      <label for="imageFile" class="col-lg-3 control-label">Image File</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="file_${vStatus.count}" name="file">
                                      </div>
                                  </div>
								  <div class="form-group form-group-top" id="mobileImageFileDiv_${vStatus.count}">
                                      <label for="imageFile" class="col-lg-3 control-label">Mobile Image</label>
                                      <div class="col-lg-9">
                                          <input type="file" id="mobileFile_${vStatus.count}" name="mobileFile">
                                      </div>
                                  </div>
                                  <div class="form-group" id="imageTextDiv_${vStatus.count}">
                                      <label for="imageText" class="col-lg-3 control-label">Image Text </label>
                                      <div class="col-lg-9">
                                      	<input class="form-control input-sm m-bot15" type="text" name="imageText" id="imageText_${vStatus.count}" value="${card.imageText}" size="40" >
                                      </div>
                                  </div>
								  <div class="form-group" id="searchCategoryDiv_${vStatus.count}"
                                  	<c:if test="${card.cardType eq null or card.cardType ne 'MAINPROMOTION'}">style="display: none;" </c:if>>
                                      <label for="categoryTxt" class="col-lg-3 control-label">Artist/Venue/ Category</label>
                                      <div class="col-lg-9">
                                          <input class="form-control input-sm m-bot15 autoCategory" type="text" name="categories" id="categories_${vStatus.count}">
                                          <!--<input type="hidden" id="artistId" name="artistId" value ="${card.artistId}">-->
										  <input type="hidden" id="venueId_${vStatus.count}" name="venueId" value ="${card.venueId}">
										  <input type="hidden" id="parentCatId_${vStatus.count}" name="parentCatId" value ="${card.parentCatId}">
										  <input type="hidden" id="childCatId_${vStatus.count}" name="childCatId" value ="${card.childCatId}">
										  <input type="hidden" id="grandChildCatId_${vStatus.count}" name="grandChildCatId" value ="${card.grandChildCatId}">
										  <input type="hidden" id="categoryName_${vStatus.count}" name="categoryName" value ="${card.categoryName}">
                                          <p class="help-block" id="categoryText_${vStatus.count}">
										  <c:if test="${card.artist ne null}">ARTIST : ${card.artist.name}</c:if>
										  <c:if test="${card.venue ne null}">VENUE : ${card.venue.building}</c:if>
										  <c:if test="${card.parentCategory ne null}">PARENT : ${card.parentCategory.name}</c:if>
										  <c:if test="${card.childCategory ne null}">CHILD : ${card.childCategory.name}</c:if>
										  <c:if test="${card.grandChildCategory ne null}">GRAND : ${card.grandChildCategory.name}</c:if>
										  </p>
                                      </div>
                                  </div>
                                  <div class="form-group" id="tmatArtistDiv_${vStatus.count}"
                                  	<c:if test="${card.cardType eq null or card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
                                      <label for="tmatArtist" class="col-lg-3 control-label">TMAT Artist</label>
                                      <div class="col-lg-9">
                                          <input class="form-control input-sm m-bot15 autoArtist" type="text" name="tmatArtist" id="tmatArtist_${vStatus.count}">
                                          <input type="hidden" id="artistId_${vStatus.count}" name="artistId" value ="${card.artistId}">
                                          <p class="help-block" id="tmatArtistText_${vStatus.count}"><c:if test="${card.artist ne null}">${card.artist.name}</c:if></p>
                                      </div>
                                  </div>
                              </div>
						</td>
						<td style="vertical-align: middle;  ">
						<select name="categoryType" id="categoryType" class="form-control input-sm m-bot15">
                        	<option value="">--- Select ---</option>
                        	<option value ="HOMEPAGE" selected> Home Page </option>
                        	<c:forEach var="parentCategory" items="${parentCategoryList}">
         						<option value="pc_${parentCategory.id}" <c:if test="${card.parentCategoryId eq parentCategory.id}">selected</c:if> ><c:out value="${parentCategory.name}"/></option>
     						</c:forEach>
     						<c:forEach var="childCategory" items="${childCategoryList}">
         						<option value="cc_${childCategory.id}" <c:if test="${card.childCategoryId eq childCategory.id}">selected</c:if> ><c:out value="${childCategory.name}"/></option>
     						</c:forEach>
     						<c:forEach var="grandChildCategory" items="${grandChildCategoryLsit}">
         						<option value="gc_${grandChildCategory.id}" <c:if test="${card.grandChildCategoryId eq grandChildCategory.id}">selected</c:if> ><c:out value="${grandChildCategory.name}"/></option>
     						</c:forEach>							
	                   </select>
						</td>
						<td style="vertical-align: middle;">
                            	<input class="form-control input-sm m-bot15" type="text" name="desktopPosition" size="4" id="desktopPosition_${vStatus.count}" value="${card.desktopPosition}">
                            	<div id="imgContainer"></div>
						</td>
						<td style="vertical-align: middle;">
                            	<input class="form-control input-sm m-bot15" type="text" name="mobilePosition" size="4" id="mobilePosition_${vStatus.count}" value="${card.mobilePosition}">
                            	<div id="imgContainer"></div>
						</td>
						<td style="vertical-align: middle;">
							<a class="btn btn-primary btn-sm" onclick="callBtnOnClick('${vStatus.count}');" name="btnUpload" id="btnUpload_${vStatus.count}" title="Update Card">Update</a>
						</td>
						</tr>
						<c:if test="${card.id ne null}">
						<tr id="cardImageRow_${vStatus.count}">
						<td style="vertical-align: middle;  ">
	                       <label for="imageText" class="col-lg-12 list-group-item"><b>
		                       <c:if test="${card.cardType eq 'UPCOMMINGEVENT'}">UpComming Event Card</c:if>
							   <c:if test="${card.cardType eq 'MAINPROMOTION'}">Main Promotion Card</c:if>
		                       <c:if test="${card.cardType eq 'PROMOTION'}">Promotion Card</c:if>
		                       <c:if test="${card.cardType eq 'YESNO'}">Yes/No Card</c:if>
		                       </b> 
		                   </label>
                       </td>
						
						<td style="font-size: 13px;" align="center">
                              <div class="form-horizontal" id="cardImageAllDiv_${vStatus.count}" 
								<c:if test="${card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO' and card.cardType ne 'MAINPROMOTION'}">style="display: none;" </c:if>>
                                  <div class="form-group form-group-top" id="cardImageDiv_${vStatus.count}">
                                      <div class="col-lg-12" >
                                       <c:if test="${card.cardType ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO' or card.cardType eq 'MAINPROMOTION')}">
											Desktop Image
                                          <img src='${api.server.url}GetImageFile?type=rtwCards&filePath=${card.imageFileUrl}' height="150" width="400" />
                                        </c:if>
										<c:if test="${card.cardType ne null and (card.cardType eq 'MAINPROMOTION')}">
											Mobile Image
                                          <img src='${api.server.url}GetImageFile?type=rtwCards&filePath=${card.mobileImageFileUrl}' height="150" width="400" />
                                        </c:if> 
                                      </div>
                                  </div>
                                  <div class="form-group" id="cardImageTextDiv_${vStatus.count}">
                                      <label for="imageText" class="col-lg-3 control-label">Image Text </label>
                                      <div class="col-lg-9">
                                     	 <label for="imageText" class="col-lg-9 list-group-item"><b>${card.imageText}</b> </label>
                                      </div>
                                  </div>
								  <div class="form-group" id="cardSearchCategoryDiv_${vStatus.count}"
                                  	<c:if test="${card.cardType ne 'MAINPROMOTION'}">style="display: none;" </c:if>>
                                     	 <label for="categoryTxt" class="col-lg-3 control-label">Artist/Venue/ Category</label>
                                      <div class="col-lg-9">
                                     	 <label for="imageText" class="col-lg-9 list-group-item">
										 <b><c:if test="${card.artist ne null}">ARTIST : ${card.artist.name}</c:if>
										  <c:if test="${card.venue ne null}">VENUE : ${card.venue.building}</c:if>
										  <c:if test="${card.parentCategory ne null}">PARENT : ${card.parentCategory.name}</c:if>
										  <c:if test="${card.childCategory ne null}">CHILD : ${card.childCategory.name}</c:if>
										  <c:if test="${card.grandChildCategory ne null}">GRAND : ${card.grandChildCategory.name}</c:if></b> 
										 </label>
                                      </div>
                                  </div>
                                  <div class="form-group" id="cardTmatArtistDiv_${vStatus.count}"
                                  	<c:if test="${card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
                                     	 <label for="tmatArtist" class="col-lg-3 control-label">TMAT Artist</label>
                                      <div class="col-lg-9">
                                     	 <label for="imageText" class="col-lg-9 list-group-item"><b>${card.artist.name}</b> </label>
                                      </div>
                                  </div>
                                  <div class="form-group" id="eventInfoDiv_${vStatus.count}"
                                  	<c:if test="${card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
                                      <label for="showEvent" class="col-lg-3 control-label">Show Event Data</label>
                                      <div class="col-lg-9">
                                     	 <label for="eventText" class="col-lg-9 list-group-item"><b>${card.showEventData}</b> </label>
                                      </div>
                                  </div>
                                  <div class="form-group" id="noOfEventsDiv_${vStatus.count}"
                                    <c:if test="${card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>
									<c:if test="${card.showEventData ne null and card.showEventData eq false}">style="display: none;" </c:if> >
                                      <label for="noOfEvents" class="col-lg-3 control-label">No. Of Events </label>
                                      <div class="col-lg-9">
                                     	 <label for="noOfEventsText" class="col-lg-9 list-group-item"><b>${card.noOfEvents}</b> </label>
                                      </div>                                      
                                  </div>
                              </div>
						</td>
						<td style="vertical-align: middle;  ">	
							<label for="imageText" class="col-lg-9 list-group-item"><b>						
                        	<c:forEach var="parentCategory" items="${parentCategoryList}">
								<c:if test="${card.parentCategoryId eq parentCategory.id}"> ${parentCategory.name} </c:if>
     						</c:forEach>
     						<c:forEach var="childCategory" items="${childCategoryList}">
								<c:if test="${card.childCategoryId eq childCategory.id}"> ${childCategory.name} </c:if>
     						</c:forEach>
     						<c:forEach var="grandChildCategory" items="${grandChildCategoryLsit}">
								<c:if test="${card.grandChildCategoryId eq grandChildCategory.id}"> ${grandChildCategory.name} </c:if>
     						</c:forEach>
							<c:if test="${empty card.parentCategoryId and empty card.childCategoryId and empty card.grandChildCategoryId}">
								Home Page
							</c:if>
							</b> </label>
						</td>
						<td style="vertical-align: middle;">
							<label class="col-lg-12 list-group-item"><b>${card.desktopPosition}</b></label>
						</td>
						<td style="vertical-align: middle;">
							<label class="col-lg-12 list-group-item"><b>${card.mobilePosition}</b></label>
						</td>
						<td style="vertical-align: middle;">
							<a class="btn btn-primary btn-sm" onclick="callModifyBtnOnClick('${vStatus.count}');" name="btnModify" id="btnModify_${vStatus.count}" title="Modify Value">Modify Card</a>
						</td>
						</tr>
						</c:if>
						</form>
					</c:forEach>
				</tbody>
			</table>
			</div>
			<a data-toggle="modal" class="btn btn-primary" id="createCards" onclick="callCreateBtnOnClick();">Add Cards</a>			
		</div>
		</div>
		</div>


<!-- popup Add Cards -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#card-add">Add Cards</button> -->

	<div id="card-add" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Cards</h4>
			</div>
			<div class="modal-body full-width">
				<div class="full-width clearfix">
					<div class="full-width column">
					<form role="form" name="fileForm" enctype="multipart/form-data" id="fileForm" method="post" modelAttribute="cards" action="${pageContext.request.contextPath}/Client/AddCategoryTickets">
						<input type="hidden" name="action" id="action" value="action"/>
						<input type="hidden" name="productId" id="productId" value="${product.id}"/>
						<div class="table-responsive card-table">
						<table class="table table-bordered table-hover" id="tab_logic">
							<thead>
								<tr >
									<th class="col-lg-3">
										Card Type
									</th>
									<th class="col-lg-4">
										Card Details
									</th>
									<th class="col-lg-3">
										Category Details
									</th>
									<th class="col-lg-1">
										Desktop Position
									</th>
									<th class="col-lg-1">
										Mobile Position
									</th>
								</tr>
							</thead>
							<tbody>
								  <tr id="imageRow">
									<td style="vertical-align: middle;  ">
									<input type="hidden" name="id" id="id" value="${card.id}" />
									<input type="hidden" name="fileRequired" id="fileRequired" value="N" />
									<input type="hidden" name="imageFileUrl" id="imageFileUrl" value="${card.imageFileUrl}"/>
									<select name="cardType" id="cardType" class="form-control input-sm m-bot15" onchange="callCardTypeChange(this);">
										<option value="">--- Select ---</option>
										<option value="UPCOMMINGEVENT" <c:if test="${card.cardType eq 'UPCOMMINGEVENT'}">selected</c:if> >UpComming Event Card</option>
										<option value="PROMOTION" <c:if test="${card.cardType eq 'PROMOTION'}">selected</c:if>>Promotion Card</option>
										<option value="YESNO" <c:if test="${card.cardType eq 'YESNO'}">selected</c:if> >Yes/No Card</option>
								   </select>
									</td>
									<td style="font-size: 13px;" align="center">
										<div class="text-left" id="cardDetailDiv" 
										<c:if test="${(card.cardType eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO'))}">style="display: none;" </c:if>>
											  <div class="form-group form-group-top full-width" id="imageFileDiv" 
											  <c:if test="${(card.id ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO'))}">style="display: none;" </c:if>>
												  <label for="imageFile" class="col-md-3 col-sm-12 col-xs-12 control-label">Image File</label>
												  <div class="col-md-9 col-sm-12 col-xs-12">
													  <input type="file" id="file" name="file">
												  </div>
											  </div>
											   <div class="form-group form-group-top full-width" id="cardImageDiv" 
											   <c:if test="${(card.id eq null or (card.cardType ne 'PROMOTION' and card.cardType ne 'YESNO'))}">style="display: none" </c:if>>
												  <div class="col-lg-12" >
												   <c:if test="${card.cardType ne null and (card.cardType eq 'PROMOTION' or card.cardType eq 'YESNO')}">
													  <img src='${api.server.url}GetImageFile?type=rtwCards&filePath=${card.imageFileUrl}' height="150" width="400" />
													  <a style="color:blue " href="javascript:callChangeImage();">
														Change Image</a>
													 </c:if>
												  </div>
											  </div>
											  <div class="form-group full-width" id="imageTextDiv">
												  <label for="imageText" class="col-md-3 col-sm-12 col-xs-12 control-label">Image Text </label>
												  <div class="col-md-9 col-sm-12 col-xs-12">
													<input class="form-control input-sm m-bot15" type="text" name="imageText" id="imageText" value="${card.imageText}" size="40" >
												  </div>
											  </div>
											  <div class="form-group full-width" id="tmatArtistDiv"
												<c:if test="${card.cardType eq null or card.cardType ne 'PROMOTION'}">style="display: none;" </c:if>>
												  <label for="tmatArtist" class="col-md-3 col-sm-12 col-xs-12 control-label">TMAT Artist</label>
												  <div class="col-md-9 col-sm-12 col-xs-12">
													  <input class="form-control input-sm m-bot15 autoArtist" type="text" name="tmatArtist" id="tmatArtist" placeholder="Search TMAT artist">
													  <input type="hidden" id="artistId" name="artistId" value ="${card.artistId}">
													  <b><label for="imageText" class="text-left pull-left selected-artist" id="tmatArtistText"><c:if test="${card.artist ne null}">${card.artist.name}</c:if></label></b>
												  </div>
											  </div>
										  </div>
									</td>
									<td style="vertical-align: middle;  ">
									<select name="categoryType" id="categoryType" class="form-control input-sm m-bot15">
										<option value="">--- Select ---</option>
										<option value ="HOMEPAGE" selected> Home Page </option>
										<c:forEach var="parentCategory" items="${parentCategoryList}">
											<option value="pc_${parentCategory.id}" <c:if test="${card.parentCategoryId eq parentCategory.id}">selected</c:if> ><c:out value="${parentCategory.name}"/></option>
										</c:forEach>
										<c:forEach var="childCategory" items="${childCategoryList}">
											<option value="cc_${childCategory.id}" <c:if test="${card.childCategoryId eq childCategory.id}">selected</c:if> ><c:out value="${childCategory.name}"/></option>
										</c:forEach>
										<c:forEach var="grandChildCategory" items="${grandChildCategoryLsit}">
											<option value="gc_${grandChildCategory.id}" <c:if test="${card.grandChildCategoryId eq grandChildCategory.id}">selected</c:if> ><c:out value="${grandChildCategory.name}"/></option>
										</c:forEach>							
								   </select>
									</td>
									<td style="vertical-align: middle;">
											<input class="form-control input-sm m-bot15" type="text" name="desktopPosition" size="4" id="desktopPosition" value="${card.desktopPosition}">
											<div id="imgContainer"></div>
									</td>
									<td style="vertical-align: middle;">
											<input class="form-control input-sm m-bot15" type="text" name="mobilePosition" size="4" id="mobilePosition" value="${card.mobilePosition}">
											<div id="imgContainer"></div>
									</td>
									
									</tr>
							</tbody>
						</table>
						</div>
						
					   </form>
					</div>
				</div>
			</div>
			<div class="modal-footer full-width">
				<button type="button" class="btn btn-primary" onclick="callSaveBtnOnClick();">Save</button>
				<c:if test="${card.id ne null }">
					<button type="button" class="btn btn-cancel" onclick="callDeleteBtnOnClick();">Delete</button>
				</c:if>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Add Cards -->