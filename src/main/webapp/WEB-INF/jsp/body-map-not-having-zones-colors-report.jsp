<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>	


<script type="text/javascript">

var isUser = '<%=session.getAttribute("isUser")%>';
$(document).ready(function(){
	if(isUser != 'false'){
		$("#adminRpt").hide();
	}
	
	$('#startDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#endDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	//$('#artist1Search').autocomplete("${pageContext.request.contextPath}/AutoCompleteArtist", {
	$('#artist1Search').autocomplete("${pageContext.request.contextPath}/Admin/AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2];
			}
		}
	}).result(function (event,row,formatted){
		//var htmlString ='';
		//htmlString = '<span class="tagSpan1" id="artist_'+artistArray.length+'"><span>'+row[2]+'&nbsp;</span>';
		//htmlString += '<a href="javascript:removeArtistLabel('+artistArray.length+');">x</a></span>';
		$('#artistId').val(row[1]);
		$('#artistName').val(row[2]);
		$('#searchResultDiv').html(row[2]);
		$('#artist1Search').val('');
	});

});


</script>

<style>
	input{
		color : black !important;
	}
</style>


<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Reports</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="/Reports/Home">Reports</a></li>
						<li><i class="fa fa-laptop"></i>RTF Map Not Having Zone Colors </li>						  	
					</ol>
					<!-- <a style="margin-top:-0.8%;margin-left:5%;" class="btn btn-primary" href="#" title="Sales Report">Sales Report</a> -->
				</div>
</div>

<div class="row">
<form name="noColorMap" id="noColorMap" method="post" action="${pageContext.request.contextPath}/Reports/DownloadRTFZonesNotHavingColorsReport">
		<input type="hidden" id="action" name="action" />
			<div class="full-width">
				<div class="form-group full-width">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Start Date</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
					<input class="form-control searchcontrol" type="text" id="startDate" name="startDate" value="${startDate}" >
						<%-- <select name="childType" id="childType" class="form-control input-sm m-bot15 full-width" onchange="loadGrandChild('search');" style="max-width:200px">
							<option value="">--Select--</option>
							<option value="0" <c:if test="${childId ne null and '0' eq childId}"> selected </c:if>>ALL</option>
							<c:forEach items="${childCategories}" var="childCat">
								<option value="${childCat.id}" <c:if test="${childId ne null and childCat.id eq childId}"> selected </c:if>>${childCat.name}</option>
							</c:forEach>
						</select> --%>
					</div>
				</div>
			</div>
			<div class="full-width">
				<div class="form-group full-width">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">End Date</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<input class="form-control searchcontrol" type="text" id="endDate" name="endDate" value="${endDate}" >
					</div>
				</div>
			</div>
			<div class="full-width">
				<div class="form-group full-width">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Artist</label>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<input class="form-control searchcontrol" type="text" id="artist1Search" name="artistSearch">
						<input type="hidden" value="" id="promoTypeId" name="promoTypeId" /> 
						<input type="hidden" id="artistId" name="artistId" value="${artistId}" />
						<input type="hidden" id="artistName" name="artistName" value="${artistName}" />
						<div id="searchResultDiv"><div class="block">${artistName}</div></div> 
					</div>
				</div>
			</div>
           
           <div class="full-width">
				<div class="form-group full-width">
					<div class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3"></div>
					<div class="col-lg-4 col-md-4 col-xs-7">
						<button type="button" id="searchSeatGeekBtn" class="btn btn-primary" onclick="downloadRTFZonesNotHavingColorsReport();" style="margin-top:19px;">Download Report</button>
					</div>
				</div>
			</div>
 </form>
</div>

<script>
function downloadRTFZonesNotHavingColorsReport(){
	window.location.href = apiServerUrl+"Reports/DownloadRTFZonesNotHavingColorsReport?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val()+"&artistId="+$("#artistId").val();
	//$("#noColorMap").submit();
}
</script>