<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<link href="../resources/css/jquery-autocomplete.css" rel="stylesheet">
<script type="text/javascript" src="../resources/js/jquery-autocomplete.js"></script>
<script type='text/javascript' src='../resources/js/jquery-all.js'></script>




<style>
.form-control {
	color: black !important;
}

.form-horizontal .form-group {
    border-bottom: 1px solid #eff2f7;
    padding-bottom: 5px;
    margin-bottom: 5px;
  }
 .form-group-top {
    padding-top: 5px;
    margin-top: 5px;
  }
 
 .td {
    text-align: center;
    vertical-align: middle;
}
.list-group-item {
	border:0px;
	background-color: inherit; 
}

.noresize {
  resize: none; 
}
.fullWidth {
    width: 100%; 
}

  
</style>

<script type="text/javascript">
var jq2 = $.noConflict(true);
function callParentTypeChange() {
	$("#searchForm").submit();
}

$(document).ready(function(){
	 $('.datepicker1').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "top",
		todayHighlight: true
	});
	 
	 $('.selectCheck').attr('checked', false);
	$('#selectAll').click(function(){
		if($('#selectAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllOdds').click(function(){
		if($('#copyAllOdds').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('odds');
			
			copyTextField('fractionalOdd');
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	$('#copyAllFractionalOdds').click(function(){
		if($('#copyAllFractionalOdds').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('fractionalOdd');
			if($('#fractionalOdd_1').val()!=''){
				copyTextField('odds');
			}
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllMarkup').click(function(){
		if($('#copyAllMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('markup');
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllCutoffDates').click(function(){
		if($('#copyAllCutoffDates').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('cutoffDate');
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	
	$('#copyAllTeam').click(function(){
		if($('#copyAllTeam').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('name');
		}else{
			//$('.selectCheck').attr('checked', true);
		}
	});
	
	<c:forEach var="teamVar" varStatus="tStat" items="${teamsList}">
		callTeamAutoComplete('${tStat.count}');
	</c:forEach>
	
	
});
function copyTeams(){
	var leagues=''; 
	 var selectedLeagues = []; 
	 $('#leagueId :selected').each(function(i, selected){ 
		 if(!$(selected).val()==''){
			 selectedLeagues[i] = $(selected).val();
		 }
	 });
	var copyLeagueId =  $('#copyFromLeagueId').val();
	
	if(selectedLeagues.length ==0){
		jAlert("Please select Event.");
		return false;
	}
	if(copyLeagueId=='' || copyLeagueId <=0){
		jAlert("Please select to Source event to copy Teams");
		return false;
	}
	if(selectedLeagues.length==1){
		if(copyLeagueId==selectedLeagues[0]){
			jAlert("Copy source and destination event must be different.");
			return false;
		}
	}
	
	for(var i=0;i<selectedLeagues.length;i++){
		 leagues = leagues + selectedLeagues[i]+',';
	}
	$('#copyToLeagueIds').val(leagues);
	$('#gridDiv').hide();
 	$("#action").val("copy");
 	$("#teamForm").submit();
}

function callTeamAutoComplete(rowNumber) {
	
	jq2('#name_'+rowNumber).autocomplete("${pageContext.request.contextPath}/AutoCompleteFantasyCategoryTeams?grandChildId"+$('#grandChildType').val(), {
		width: 650,
		max: 1000,
		minChars: 2,		
		dataType: "text",
		formatItem: function(row, i, max) {
			return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
		}
	}).result(function (event,row,formatted){
		  var rowId = $(this).attr('id').split("_")[1];
		  fillTeamInfo(rowId,row[1]);
		  $('#checkbox_'+rowId).attr('checked', true);
			
	});
}

function fillTeamInfo(rowId,teamId){
	$.ajax({
		url : "${pageContext.request.contextPath}/getFantasyCategoryTeam",
		type : "post",
		dataType:"json",
		data : "teamId="+teamId,
		success : function(res){
			var respData = JSON.parse(JSON.stringify(res));
			var jsonData = respData.fantasyCategoryTeam;
			if(jsonData.id!='' && jsonData.id != undefined){
				$('#categoryTeamId_'+rowId).val(jsonData.id);
				$('#name_'+rowId).val(jsonData.name);
				$('#fractionalOdd_'+rowId).val(jsonData.fractionOdds);
				$('#odds_'+rowId).val(jsonData.odds);
				$('#markup_'+rowId).val(jsonData.markup);
				$('#cutoffDate_'+rowId).val(jsonData.cutoffDate);
			}
			
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else {
			$("#" + id ).val(firstFieldValue);
		}
	});
}

function countFractionalOdd(rowNumber){
	var fractionalValue = $('#fractionalOdd_'+rowNumber).val();
	if(fractionalValue!=null && fractionalValue != ''){
		if(fractionalValue.indexOf("/") > 0){
			var array = fractionalValue.split("/");
			var N = parseFloat(array[0]);
			var D = parseFloat(array[1]);
			if(N==0 && D==0){
				$('#fractionalOdd_' + rowNumber).val('');
				jAlert("Please enter valid factional odd value.");
				return;
			}else{
				var odd = parseFloat((D/(D+N))*100);
				$('#odds_'+rowNumber).val(odd.toFixed(2));
				callSelectRow(rowNumber);
			}
		}else{
			$('#fractionalOdd_' + rowNumber).val('');
			jAlert('Please enter valid fraction value.');
			return;
		}
	}
}
function onOddChange(rowNumber){
	var odd = $('#odds_'+rowNumber).val();
	var fractionalValue = $('#fractionalOdd_'+rowNumber).val();
	if(isNaN(odd) || odd < 0){
		$('#odds_' + rowNumber).val('');
		jAlert("Please enter valid odd value.");
	}
	if(fractionalValue!=null && fractionalValue != '' && odd!=null && odd != ''){
		jAlert("Please remove Fractional Odds.");
		$('#odds_'+rowNumber).val('');
		return;
	}
	callSelectRow(rowNumber);
}

function callSelectRow(rowNumber) {
	$('#checkbox_'+rowNumber).attr('checked', true);
}

 function callSaveBtnClick(action) {
	 $("#infoMainDiv").hide();
	
	var oddsMsg = "";
	var flag= true;
	var regex = /^[0-9]{0,10}(\.[0-9]{0,3})?$/;
	
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
			
		var id,value;
		id = this.id.replace('checkbox','name');
		value = $.trim($("#"+id).val());
		if(value==''){
			jAlert('Please enter valid team name.');
			$("#"+id).focus();
			flag = false;
			return false;
		}
		id = this.id.replace('checkbox','odds');
		value = $.trim($("#"+id).val());
		if(value=='' || !regex.test(value) || value < 0) {
			jAlert('Please Enter valid odds, should be greater than equal to 0');
			$("#"+id).focus();
			flag = false;
			return false;
		}
		if(value <= 0){
			oddsMsg = "All Teams with 0 odd will not be visible on Reward The Fan. ";
		}
		
		id = this.id.replace('checkbox','categoryTeamId');
		value = $.trim($("#"+id).val());
		if(value=='' || !regex.test(value)) {
			jAlert('Some of the records does not have real team attached.');
			id = this.id.replace('checkbox','name');
			$("#"+id).focus();
			flag = false;
			return false;
		} 
		isMinimamOnerecord = true;
	});
	if(flag && !isMinimamOnerecord) {
		jAlert('Select minimum one team to save.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
		jConfirm(oddsMsg+"Are you sure you want to save selected teams ?.","Confirm",function(r){
			if(r) {
				$("#action").val(action);
				$("#teamForm").submit();
			 }
		});
	}
 }
 
 function callDeleteBtnOnClick(action) {
	 $("#infoMainDiv").hide();
	 
	var flag= true;
	var isMinimamOnerecord = false;
	$('.selectCheck:checkbox:checked').each(function () {
			
		var id,value;
		id = this.id.replace('checkbox','id');
		value = $.trim($("#"+id).val());
		if(value != ''){
			isMinimamOnerecord = true;	
		}
		
	});
	if(!isMinimamOnerecord) {
		jAlert('Select minimum one Existing team to delete.');
		$("#"+id).focus();
		flag = false;
		return false;
	}
	if(flag) {
	  jConfirm("Are you sure you want to remove selected teams? All associated orders are voided.","Confirm",function(r){
		  if(r){
			  $("#action").val(action);
			  $("#teamForm").submit(); 
		  }
	  });
	}
 }
 
 function callParentTypeOnChange(action) {	 
	 $('#gridDiv').hide();
	 $('#leagueId').val('');
	 $('#copyToLeagueIds').val('');
	 $("#action").val(action);
	 $("#teamForm").submit();
 }
 
 function loadChild(action){
	 $('#gridDiv').hide();
	 $('#leagueId').val('');
	 $('#copyToLeagueIds').val('');
	 $("#action").val(action);
	 $("#teamForm").submit();
}

function loadGrandChild(action){
	 $('#gridDiv').hide();
	 $('#grandChildType').val('');
	 $('#leagueId').val('');
	 $('#copyToLeagueIds').val('');
	 $("#action").val(action);
	 $("#teamForm").submit();
}

 function callLeagueOnChange(action) {
	 $("#infoMainDiv").hide();
	 var selectedLeagues = []; 
	 $('#leagueId :selected').each(function(i, selected){ 
		 if(!$(selected).val()==''){
			 selectedLeagues[i] = $(selected).val();
		 }
	 });
	 
	 if(selectedLeagues.length > 1){
		 return false;
	 }
	 
	 var flag = true;
	 /*var parentType = $('#parentType').val();
	 if(parentType == null || parentType == ''){
		 jAlert('Please select valid parent type.');
		 document.getElementById("parentType").focus();
		 flag = false;
		 return false;
	 }
	 var leagueId = $('#leagueId').val();
	 if(leagueId == null || leagueId == ''){
		 $('#gridDiv').hide();
		 jAlert('Please select valid League.');
		 document.getElementById("leagueId").focus();
		 flag = false;
		 return false;
	 }*/
		
	 if(flag) {
	 	$('#gridDiv').hide();
	 	$("#action").val(action);
	 	$("#teamForm").submit();
	 }
 }
/* function callCityOnChange(action) {
	 
	$("#infoMainDiv").hide();
	 
	 var flag = true;
	 var parentType = $('#parentType').val();
	 if(parentType == null || parentType == ''){
		 jAlert('Please select valid parent type.');
		 document.getElementById("parentType").focus();
		 flag = false;
		 return false;
	 }
	 var leagueId = $('#leagueId').val();
	 if(leagueId == null || leagueId == ''){
		 $('#gridDiv').hide();
		 jAlert('Please select valid Event.');
		 document.getElementById("leagueId").focus();
		 flag = false;
		 return false;
	 }
		
	 if(flag) {
	 	$('#gridDiv').hide();
	 	$("#action").val(action);
	 	$("#teamForm").submit();
	 }
 } */

function exportToExcel(){
	var appendData = "leagueId="+$('#leagueId').val()+"&city="+$('#city').val();
    var url = apiServerUrl + "TeamsExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

 /*function callCityOnChange(rowId) {	 
 	var url = "";
	var artistId = $("#artistId").val();
	var city = $("#city_"+rowId).val();
	url = "${pageContext.request.contextPath}/GetEventsByArtistAndCity?artistId="+artistId+"&city="+city;
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			document.getElementById('eventId_'+rowId).options.length=1;
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.nameWithDateandVenue+"</option>";
				$('#eventId_'+rowId).append(rowText);
            }	
		}
	}); 
 }*/

</script>

<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Fantasy Sports Tickets</a></li>
						<li><i class="fa fa-laptop"></i>Teams</li>						  	
					</ol>
				</div>
</div>

<div class="full-width">
<div class="row">

<div class="alert alert-success fade in" id="infoMainDiv"
<c:if test="${info eq null or info eq ''}">style="display: none;"</c:if>>
        <button data-dismiss="alert" class="close close-sm" type="button">
            <i class="icon-remove"></i>
        </button>
        <span id="infoMsgDiv">${info}</span>
    </div>
</div>
<form name="teamForm" id="teamForm" method="post" action="${pageContext.request.contextPath}/CrownJewelEvents/CrownJewelTeams">
	<input type="hidden" id="action" name="action" />
	<div class="row">
		<div class="form-group full-width">
          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Parent Type</label>
          <div class="col-lg-4 col-md-4 col-xs-7">
              <label>SPORTS</label>
          </div>
          <div class="col-lg-2">
          </div>
           </div>
        </div>
        
	<div class="row">   
		<div class="form-group full-width">
		  <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Child Type</label>
		  <div class="col-lg-4 col-md-4 col-xs-7">
			<select name="childType" id="childType" class="form-control input-sm m-bot15" onchange="loadGrandChild('search');" style="max-width:200px">
				<option value="">--Select--</option>				
				<%-- <option value="0" <c:if test="${childId ne null and '0' eq childId}"> selected </c:if>>ALL</option> --%>
				<c:forEach items="${childCategories}" var="childCat">
					<option value="${childCat.id}" <c:if test="${childId ne null and childCat.id eq childId}"> selected </c:if>>${childCat.name}</option>
				</c:forEach>
			</select>
		  </div>
		</div>
	</div>
	
	<c:if test="${not empty childId}">
	<div class="row">     
	      <div class="form-group full-width">
	          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Grand Child Type</label>
	          <div class="col-lg-4 col-md-4 col-xs-7">
	          	<select name="grandChildType" id="grandChildType" class="form-control input-sm m-bot15" onchange="callParentTypeOnChange('search');" style="max-width:200px">
	          		<option value="">--Select--</option>
					<%-- <option value="0" <c:if test="${grandChildId ne null and '0' eq grandChildId}"> selected </c:if>>ALL</option> --%>
					<c:forEach items="${grandChildCategories}" var="grandChildCat">							
						<option value="${grandChildCat.id}" <c:if test="${grandChildId ne null and grandChildCat.id eq grandChildId}"> selected </c:if>>${grandChildCat.name}</option>
					</c:forEach>
				</select>				
	          </div>
	      </div>
      </div>
	 </c:if>
	 
        <div class="row">  
           <div class="form-group full-width">
          
          	<c:choose>
          	<c:when test="${not empty grandChildId}">
          			<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Event</label>  
          			 <input type="hidden" id="copyToLeagueIds" name="copyToLeagueIds" value="" />        		
	          		<div class="col-lg-4 col-md-4 col-xs-7">
			          	<select multiple name="leagueId" id="leagueId" style="height:100px; width:330px;" class="form-control input-sm m-bot15" onchange="callLeagueOnChange('search');">
			                	<!-- <option value="">--- Select ---</option> -->
								<c:forEach var="league" items="${leaguesList}">
								    <option value="${league.id}" <c:if test="${leagueId ne null and league.id eq leagueId}">selected</c:if>>${league.name}</option>
								  </c:forEach>
			            </select>
			            <input type="hidden" id="artistId" name="artistId" value="${artistId}" />
			          </div>
					  <div class="col-lg-2"></div>
          	</c:when>
          	<c:otherwise>
          		<%-- <c:if test="${not empty parentType and parentType ne 'SPORTS'}">
					<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Team/Artist</label>
          			<div class="col-lg-4 col-md-4 col-xs-7">
		          	<select name="leagueId" id="leagueId" class="form-control input-sm m-bot15" onchange="callLeagueOnChange('search');">
		                	<option value="">--- Select ---</option>
							<c:forEach var="league" items="${leaguesList}">
							    <option value="${league.id}" <c:if test="${leagueId ne null and league.id eq leagueId}">selected</c:if>>${league.name}</option>
							  </c:forEach>
		            </select>
		            <input type="hidden" id="artistId" name="artistId" value="${artistId}" />
		            </div>
					<div class="col-lg-2"></div>
				</c:if> --%>
			</c:otherwise>
          	</c:choose>
                  
          </div>
           <div class="form-group full-width">
          	<c:if test="${not empty grandChildId}">
          			<label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">Copy Teams From Event</label>          		
	          		<div class="col-lg-4 col-md-4 col-xs-7">
			          	<select name="copyFromLeagueId" id="copyFromLeagueId" class="form-control input-sm m-bot15" >
			                	<option value="">--- Select ---</option>
								<c:forEach var="league" items="${leaguesList}">
								    <option value="${league.id}" <c:if test="${copyFromLeagueId ne null and league.id eq copyFromLeagueId}">selected</c:if>>${league.name}</option>
								  </c:forEach>
			            </select>
			          </div>
					  <div class="col-lg-2"><button type="button" style="" onclick="copyTeams();" class="btn btn-primary btn-sm">Copy</button></div>
          	</c:if>
          </div>
          	<%-- <c:if test="${parentType ne null and parentType ne 'SPORTS'}">
		          <div class="form-group full-width">
			          <label for="imageText" class="col-lg-2 col-md-3 col-xs-5 col-lg-offset-3 control-label">City</label>
			          <div class="col-lg-4 col-md-4 col-xs-7">
		         			<select name="city" id="city" class="form-control input-sm m-bot15" onchange="callCityOnChange('search');">
		                    	<option value="">--- All ---</option>
								<c:forEach var="city" items="${cityList}">
									<option value="${city}" <c:if test="${selectedCity == city}">selected</c:if>>${city}</option>
		  						</c:forEach>
		                     </select>
			          </div>
						<div class="col-lg-2">
			          </div>           
		          </div>
          </c:if> --%>
	</div>

<div class="row clearfix">
		<div class="col-md-12 column" id="gridDiv">
		<c:if test="${leagueId ne null and leagueId ne ''}">
            <div class="pull-right">
				<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
				<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-cancel btn-sm">Delete</button>
				<a href="javascript:exportToExcel()" style="float: right; margin-left: 10px;">Export to Excel</a>
			</div>
			<br />
			<br />
			<div class="table-responsive">
			<table class="table table-bordered table-hover" id="tab_logic" align="center">
				<thead>
					<tr >
						<th class="col-lg-1 ">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-3 ">
							Team Name<br />
							<input type="checkbox" name="copyAllTeam" id="copyAllTeam" >
						</th>
						<th class="col-lg-1 ">
							Markup %<br />
							<input type="checkbox" name="copyAllMarkup" id="copyAllMarkup" >
						</th>
						<th class="col-lg-1 ">
							Fractional Odds<br />
							D/(D+N)*100
							<input type="checkbox" name="copyAllFractionalOdds" id="copyAllFractionalOdds" >
						</th>
						<th class="col-lg-1 ">
							Odds %<br />
							<input type="checkbox" name="copyAllOdds" id="copyAllOdds" >
						</th>
						<th class="col-lg-2 ">
							Cutoff Date
							<input type="checkbox" name="copyAllCutoffDates" id="copyAllCutoffDates" >
						</th >
						<!--<th class="col-lg-1 " >
							Package<br />
							<input type="checkbox" name="copyAllPackageApplicable" id="copyAllPackageApplicable" >
						</th>
						<th class="col-lg-1 ">
							Package Cost $<br />
							<input type="checkbox" name="copyAllPackageCost" id="copyAllPackageCost" >
						</th>
						<th class="col-lg-1 ">
							Package Notes<br />
							<input type="checkbox" name="copyAllPackageNotes" id="copyAllPackageNotes" >
						</th>-->
						<th class="col-lg-1">
							Last Updated By
						</th>
						<th class="col-lg-1">
							Last Updated
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty teamsList}">
				 <c:forEach var="cjTeam" varStatus="vStatus" items="${teamsList}">
                      <tr id="teamRow_${vStatus.count}">
                      <td>
                      	<input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" />
                      	<input type="hidden" name="rowNumber" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
						<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjTeam.id}" />
						<input type="hidden" name="leagueId_${vStatus.count}" id="leagueId_${vStatus.count}" value="${cjTeam.leagueId}" /> 
						<input type="hidden" name="categoryTeamId_${vStatus.count}" id="categoryTeamId_${vStatus.count}" value="${cjTeam.categoryTeamId}" /> 
                      </td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="name_${vStatus.count}" id="name_${vStatus.count}" value="${cjTeam.name}"  onchange="callSelectRow('${vStatus.count}');">
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="markup_${vStatus.count}" id="markup_${vStatus.count}" value="${cjTeam.markup}"  onchange="callSelectRow('${vStatus.count}');">
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="fractionalOdd_${vStatus.count}" id="fractionalOdd_${vStatus.count}" value="${cjTeam.fractionalOdd}"  onchange="countFractionalOdd('${vStatus.count}');">
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
                              <div class="form-group" >
                                  <div >
                                  	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="odds_${vStatus.count}" id="odds_${vStatus.count}" value="${cjTeam.odds}" onchange="onOddChange('${vStatus.count}');" >
                                  </div>
                              </div>
						</td>
						<td style="font-size: 13px;" align="center">
			                <div class="form-group" >
             	               <div >
                                  	<input class="datepicker1 form-control input-sm m-bot15 fullWidth" type="text" name="cutoffDate_${vStatus.count}" id="cutoffDate_${vStatus.count}" value="${cjTeam.cutOffDateStr}" onchange="callSelectRow('${vStatus.count}');">
                               </div>
                            </div>
						</td>
						<%--<td style="vertical-align: middle;">
                            	<input type="checkbox" class="selectPackage" id="packageApplicable_${vStatus.count}" name="packageApplicable_${vStatus.count}" onclick="callSelectRow('${vStatus.count}');" 
                            	<c:if test="${cjTeam.packageApplicable}">checked="checked" </c:if> />
						</td>
						<td style="vertical-align: middle;">
						
                            	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="packageCost_${vStatus.count}" size="8" id="packageCost_${vStatus.count}" value="<fmt:formatNumber pattern="0.00" value="${cjTeam.packageCost}"/>" onchange="callSelectRow('${vStatus.count}');">
						</td>
						<td style="vertical-align: middle;">
							<input class="form-control input-sm m-bot15 fullWidth" type="text" name="packageNotes_${vStatus.count}" id="packageNotes_${vStatus.count}" value="${cjTeam.packageNotes}"  onchange="callSelectRow('${vStatus.count}');">
						</td>--%><!-- -->
						<td style="vertical-align: middle;">
							<label for="imageText" class="col-lg-3 list-group-item" name="lastUpdatedBy_${vStatus.count}" id="lastUpdatedBy_${vStatus.count}">${cjTeam.lastUpdateddBy}</label>
						</td>
						<td style="vertical-align: middle;">
							<label for="imageText" class="col-lg-3 list-group-item" name="lastUpdated_${vStatus.count}" id="lastUpdated_${vStatus.count}">${cjTeam.lastUpdatedStr}</label>
						</td>
						</tr>
					</c:forEach>
					</c:if>
					

				</tbody>
			</table>
			</div>
				<a id="add_row" onclick="addNewTeamRow();" class="btn btn-default btn-sm pull-left">Add Row</a>
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-cancel btn-sm">Delete</button>
				</div>
				<%-- <c:choose>
				<c:when test="${not empty teamsList}">
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-cancel btn-sm">Delete</button>
					<a href="javascript:exportToExcel()" style="float: right; margin-left: 10px;">Export to Excel</a>
				</div>
				<br />
				<br />
				<div class="table-responsive">
				<table class="table table-bordered table-hover" id="tab_logic" align="center">
				<thead>
					<tr >
						<th class="col-lg-1 ">
							<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" /> All
						</th>
						<th class="col-lg-1 ">
							City
						</th>
						<th class="col-lg-2 ">
							Event
						</th>
						<th class="col-lg-1 ">
							Event Date
						</th>
						<th class="col-lg-2 ">
							Venue
						</th>
						<!-- <th class="col-lg-1 ">
							Tickets Count
						</th >  -->
						<!--<th class="col-lg-1 " >
							Package<br />
							<input type="checkbox" name="copyAllPackageApplicable" id="copyAllPackageApplicable" >
						</th>
						<th class="col-lg-1 ">
							Package Cost $<br />
							<input type="checkbox" name="copyAllPackageCost" id="copyAllPackageCost" >
						</th>
						<th class="col-lg-1 ">
							Package Notes<br />
							<input type="checkbox" name="copyAllPackageNotes" id="copyAllPackageNotes" >
						</th>-->
						<th class="col-lg-1">
							Last Updated By
						</th>
						<th class="col-lg-1">
							Last Updated
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty teamsList}">
				 <c:forEach var="cjTeam" varStatus="vStatus" items="${teamsList}">
                      <tr id="teamRow_${vStatus.count}"
                      <c:if test="${cjTeam.id ne null}">style="background-color: #caf4ca;"</c:if> >
                      <td>
                      	<input type="checkbox" class="selectCheck" id="checkbox_${vStatus.count}" name="checkbox_${vStatus.count}" />
                      	<input type="hidden" name="rowNumber" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
						<input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${cjTeam.id}" />
						<input type="hidden" name="leagueId_${vStatus.count}" id="leagueId_${vStatus.count}" value="${cjTeam.leagueId}" /> 
                      </td>
						<td style="font-size: 13px;" align="center">
			               	<select name="city_${vStatus.count}" id="city_${vStatus.count}" class="form-control input-sm m-bot15" onchange="callCityOnChange('${vStatus.count}');">
                        		<option value="">--- Select ---</option>
								<c:forEach var="city" items="${cityList}">
								    <option value="${city}" <c:if test="${cjTeam.event.city == city}">selected</c:if>>${city}</option>
  								</c:forEach>
                     		</select>
                     		<b><label for="CityText" class="list-group-item" id="cityText_${vStatus.count}">${cjTeam.event.city}</label></b>
                     		<input type="hidden" name="city_${vStatus.count}" id="city_${vStatus.count}" value="${cjTeam.event.city}"/>
                     		
						</td>
						<td style="font-size: 13px;" align="center">
							<select name="eventId_${vStatus.count}" id="eventId_${vStatus.count}" class="form-control input-sm m-bot15" onchange="callSelectRow('${vStatus.count}');">
                        		<option value="">--- Select ---</option>
								<c:forEach var="event" items="${cjTeam.eventsByArtistCity}">
								    <option value="${event.eventId}" <c:if test="${cjTeam.event.eventId eq event.eventId}">selected</c:if>>${event.nameWithDateandVenue}</option>
  								</c:forEach>
                     		</select>
							
                     		<b><label for="EventText" class="list-group-item" id="eventNameText_${vStatus.count}">${cjTeam.event.eventName}</label></b>
                     		<input type="hidden" name="eventId_${vStatus.count}" id="eventId_${vStatus.count}" value="${cjTeam.event.eventId}"/>
						</td>
						<td style="font-size: 13px;" align="center">
                     		<b><label for="EventText" class="list-group-item" id="eventDateText_${vStatus.count}">${cjTeam.event.eventDateTimeStr}</label></b>
						</td>
						
						<td style="font-size: 13px;" align="center">
                     		<b><label for="EventText" class="list-group-item" id="venueText_${vStatus.count}">${cjTeam.event.formattedVenueDescription}</label></b>
						</td>
						<td style="font-size: 13px;" align="center">
			                <div class="form-group" >
             	               <div >
                                  	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="ticketsCount_${vStatus.count}" id="ticketsCount_${vStatus.count}" value="${cjTeam.ticketsCount}" onchange="callSelectRow('${vStatus.count}');">
                               </div>
                            </div>
						</td> 
						<td style="vertical-align: middle;">
                            	<input type="checkbox" class="selectPackage" id="packageApplicable_${vStatus.count}" name="packageApplicable_${vStatus.count}" onclick="callSelectRow('${vStatus.count}');" 
                            	<c:if test="${cjTeam.packageApplicable}">checked="checked" </c:if> />
						</td>
						<td style="vertical-align: middle;">
						
                            	<input class="form-control input-sm m-bot15 fullWidth" type="text" name="packageCost_${vStatus.count}" size="8" id="packageCost_${vStatus.count}" value="<fmt:formatNumber pattern="#0" value="${cjTeam.packageCost}"/>" onchange="callSelectRow('${vStatus.count}');">
						</td>
						<td style="vertical-align: middle;">
							<input class="form-control input-sm m-bot15 fullWidth" type="text" name="packageNotes_${vStatus.count}" id="packageNotes_${vStatus.count}" value="${cjTeam.packageNotes}"  onchange="callSelectRow('${vStatus.count}');">
						</td>
						<td style="vertical-align: middle;">
							<label for="imageText" class="col-lg-3 list-group-item" name="lastUpdatedBy_${vStatus.count}" id="lastUpdatedBy_${vStatus.count}">${cjTeam.lastUpdateddBy}</label>
						</td>
						<td style="vertical-align: middle;">
							<label for="imageText" class="col-lg-3 list-group-item" name="lastUpdated_${vStatus.count}" id="lastUpdated_${vStatus.count}">${cjTeam.lastUpdatedStr}</label>
						</td>
						
						</tr>
					</c:forEach>
					</c:if>
					

				</tbody>
			</table>
			</div>
				<div class="pull-right">
					<button type="button" onclick="callSaveBtnClick('update');" class="btn btn-primary btn-sm">Save</button>
					<button type="button" onclick="callDeleteBtnOnClick('delete');" class="btn btn-cancel btn-sm">Delete</button>
				</div>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="alert alert-danger fade in" id="infoMainDiv">
					       	 <button data-dismiss="alert" class="close close-sm" type="button">
					            <i class="icon-remove"></i>
					        </button>
					        <span id="infoMsgDiv">No Records Found.</span>
					    </div>
					</div>
				</c:otherwise>
				</c:choose>
 --%>				
		</c:if>
		</div>
	</div>
</form>
</div>

<script type="text/javascript">

var rCount=$('#tab_logic tr').length;

function addNewTeamRow() {
	$("#infoMainDiv").hide();
   	
	rCount++;
	$('#tab_logic').append('<tr id="teamRow_'+(rCount)+'"></tr>');
	   var rowHtml = '';

	   rowHtml = rowHtml + "<td>"+
			       "<input type='checkbox' class='selectCheck' id='checkbox_"+rCount+"' name='checkbox_"+rCount+"' />"+
			       "<input type='hidden' name='rowNumber_"+rCount+"' id='rowNumber_"+rCount+"' value="+rCount+"/>"+
		           "<input type='hidden' name='id_"+rCount+"' id='id_"+rCount+"' />"+
		           "<input type='hidden' name='leagueId_"+rCount+"' id='leagueId_"+rCount+"' />"+
			       "<input type='hidden' name='categoryTeamId_"+rCount+"' id='categoryTeamId_"+rCount+"' />"+
			       "</td>";
	   rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
	              "<div class='form-group' >"+
	              "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='name_"+rCount+"' id='name_"+rCount+"'onchange='callSelectRow("+rCount+");'>"+
	              "</div></div></td>";
	              
	   rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
	              "<div class='form-group' >"+
	              "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='markup_"+rCount+"' id='markup_"+rCount+"'onchange='callSelectRow("+rCount+");'>"+
	              "</div></div></td>";
	              
	   rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
	              "<div class='form-group' >"+
	              "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='fractionalOdd_"+rCount+"' id='fractionalOdd_"+rCount+"'onchange='countFractionalOdd("+rCount+");'>"+
	              "</div></div></td>";
	    
	   rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
            "<div class='form-group' >"+
            "<div ><input class='form-control input-sm m-bot15 fullWidth' type='text' name='odds_"+rCount+"' id='odds_"+rCount+"' onchange='callSelectRow("+rCount+");'>"+
            "</div></div></td>";
            
       	rowHtml = rowHtml + "<td style='font-size: 13px;' align='center'> "+
            "<div class='form-group' >"+
            "<div ><input class='datepicker1 form-control input-sm m-bot15 fullWidth' type='text' name='cutoffDate_"+rCount+"' id='cutoffDate_"+rCount+"' onchange='callSelectRow("+rCount+");'>"+
            "</div></div></td>";
            
        /*rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
        		"<input type='checkbox' class='selectPackage' id='packageApplicable_"+rCount+"' name='packageApplicable_"+rCount+"' onclick='callSelectRow("+rCount+");'>"+
        	    "</td>";
   	    rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
	              "<input class='form-control input-sm m-bot15 fullWidth' type='text' name='packageCost_"+rCount+"' id='packageCost_"+rCount+"'  onchange='callSelectRow("+rCount+");'>"+
	              "</td>";
	              
        rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
				"<input class='form-control input-sm m-bot15 fullWidth' type='text' name='packageNotes_"+rCount+"' id='packageNotes_"+rCount+"' onchange='callSelectRow("+rCount+");'>"+		              
	              "</td>";*/
	              
	    rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
					"<label for='imageText' id='lastUpdatedBy_"+rCount+"'></label>"+		              
		              "</td>";
	    
           rowHtml = rowHtml + "<td style='vertical-align: middle;'> "+
					"<label for='imageText' id='lastUpdated_"+rCount+"'></label>"+		              
		              "</td>";
					
	              var row = $(rowHtml);
	    $('#teamRow_'+rCount).html(row);
	    $('.datepicker1').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
		});
	    callTeamAutoComplete(rCount);
		    
}
var teamTable = document.getElementById("tab_logic");
if(teamTable != null && teamTable != '' && $('#tab_logic tr').length <= 1) {
	addNewTeamRow();	
}

</script>