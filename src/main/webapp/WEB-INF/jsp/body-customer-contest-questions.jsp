<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />

<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<style>
.slick-headerrow-column {
	background: #87ceeb;
	text-overflow: clip;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.promoAll {
	margin-right: 5px;
	background: #87ceeb;
	font-size: 10pt;
	height: 20px;
}

.cell-title {
	font-weight: bold;
}

.questionLabel{
	background-color:#124597;
	color:white;
	font-size:20px;
	cursor:pointer;
	margin:20px;
}


.cell-effort-driven {
	text-align: center;
}

.slick-headerrow-column input {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}
</style>
<script>

$(document).ready(function() {
	
	$('#menuContainer').click(function() {
		if ($('.ABCD').length > 0) {
			$('#menuContainer').removeClass('ABCD');
		} else {
			$('#menuContainer').addClass('ABCD');
		}
		if(submitTriviaGrid != null && submitTriviaGrid != undefined){
			submitTriviaGrid.resizeCanvas();
		}
	});
	
	/* <c:choose>
		<c:when test="${statusType == 'PENDING'}">	
			$('#pendingQuestion').addClass('active');
			$('#pendingQuestionTab').addClass('active');
		</c:when>
		<c:when test="${statusType == 'USED'}">
			$('#usedQuestion').addClass('active');
			$('#usedQuestionTab').addClass('active');
		</c:when>
	</c:choose>
	
	$("#activeQuestion1").click(function(){
		callTabOnChange('PENDING');
	});
	$("#usedQuestion1").click(function(){
		callTabOnChange('USED');
	}); */
	
});

/* function callTabOnChange(selectedTab) {	
	window.location = "${pageContext.request.contextPath}/ContestCustomerQuestions?status="+selectedTab;
} */

</script>

<ul id="contextMenu" style="display:none;position:absolute">
  <li data="copy question">Copy Question</li>
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="#">Contest</a></li>
			<li><i class="fa fa-laptop"></i>Submitted Trivia Questions</li>
		</ol>
	</div>
</div>
<div id="contestDiv">
	<div class="full-width full-width-btn mb-20">
		<button class="btn btn-primary" id="moveBtn" type="button" onclick="updateQuestion('MOVE');">Move To Question Bank</button>
		<button class="btn btn-primary" id="deleteQBBtn" type="button" onclick="updateQuestion('DELETE')">Delete Question</button>
	</div>
	<br />
	<br />
	<div style="position: relative" id="submitTriviaGridDiv">
		<div class="table-responsive grid-table">
			<div class="grid-header full-width">
				<label>Submitted Trivia Qustions</label>
				<div class="pull-right">
					<!--<a href="javascript:submitTriviaExportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>-->
					<a href="javascript:submitTriviaResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a>
				</div>
			</div>
			<div id="submitTrivia_grid" style="width: 100%; height: 350px; border: 1px solid gray"></div>
			<div id="submitTrivia_pager" style="width: 100%; height: 10px;"></div>
	
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function pagingControl(move, id) {
		if(id == 'submitTrivia_pager'){
			var pageNo = 0;
			if (move == 'FIRST') {
				pageNo = 0;
			} else if (move == 'LAST') {
				pageNo = parseInt(pagingInfo.totalPages) - 1;
			} else if (move == 'NEXT') {
				pageNo = parseInt(pagingInfo.pageNum) + 1;
			} else if (move == 'PREV') {
				pageNo = parseInt(pagingInfo.pageNum) - 1;
			}
			getSubmitTriviaGridData(pageNo);
		}
	}
	
	function updateQuestion(action){
		var ids = getSelectedQuestionGridId();
		if(ids ==''){
			jALert('Please slelect question first.');
			return;
		}
		var confirmMsg = '';
		if(action=='MOVE'){
			confirmMsg = 'Are you sure you want to move selected question to question bank?';
		}else if(action=='DELETE'){
			confirmMsg = 'Are you sure you want to delete question parmenantly?';
		}
		jConfirm(confirmMsg,"Confirm",function(r){
			if(r){
				$.ajax({
					url : "${pageContext.request.contextPath}/UpdateSubmitTrivia",
					type : "post",
					dataType: "json",
					data: "ids="+ids+"&action="+action+"&status=PENDING",
					success : function(response){				
						var jsonData = JSON.parse(JSON.stringify(response));
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
						}
						pagingInfo =  JSON.parse(jsonData.submitTriviaPagingInfo);
						refreshSubmitTriviaGridValues(JSON.parse(jsonData.submitTriviaList));	
						clearAllSelections();
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		});
	}
	
	//Submit Trivia Grid
	
	function getSubmitTriviaGridData(pageNo){
		$.ajax({
			url : "${pageContext.request.contextPath}/ContestCustomerQuestions",
			type : "post",
			dataType: "json",
			data: "pageNo="+pageNo+"&headerFilter="+submitTriviaSearchString+"&status=${statusType}",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo =  JSON.parse(jsonData.submitTriviaPagingInfo);
				refreshSubmitTriviaGridValues(JSON.parse(jsonData.submitTriviaList));	
				clearAllSelections();
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function submitTriviaExportToExcel(){
		var appendData = "headerFilter="+submitTriviaSearchString;
	    var url = "${pageContext.request.contextPath}/submitTriviaExportToExcel?"+appendData;
	    $('#download-frame').attr('src', url);
	}
	
	function submitTriviaResetFilters(){
		submitTriviaSearchString='RESET:RESET,';
		submitTriviaColumnFilters = {};
		getSubmitTriviaGridData(0);
	}
	
	
	
	var questionCheckboxSelector = new Slick.CheckboxSelectColumn({
		  cssClass: "slick-cell-checkboxsel"
		});
	
	var pagingInfo;
	var submitTriviaDataView;
	var submitTriviaGrid;
	var submitTriviaData = [];
	var submitTriviaGridPager;
	var submitTriviaSearchString='';
	var submitTriviaColumnFilters = {};
	var userSubmitTriviaColumnsStr = '<%=session.getAttribute("submitTriviagrid")%>';

	var userSubmitTriviaColumns = [];
	var allSubmitTriviaColumns = [questionCheckboxSelector.getColumnDefinition(),
			 {
				id : "submitTriviaId",
				field : "submitTriviaId",
				name : "Ques. No",
				width : 80,
				sortable : true
			},{
				id : "question",
				field : "question",
				name : "Question",
				width : 80,
				sortable : true
			},{
				id : "optionA",
				field : "optionA",
				name : "option A",
				width : 80,
				sortable : true
			},{
				id : "optionB",
				field : "optionB",
				name : "option B",
				width : 80,
				sortable : true
			},{
				id : "optionC",
				field : "optionC",
				name : "option C",
				width : 80,
				sortable : true
			},{
				id : "answer",
				field : "answer",
				name : "Answer",
				width : 80,
				sortable : true
			},{
				id : "status",
				field : "status",
				name : "Status",
				width : 80,
				sortable : true
			},{
				id : "createdDate",
				field : "createdDate",
				name : "Created Date",
				width : 80,
				sortable : true
			},{
				id : "createdBy",
				field : "createdBy",
				name : "Submitted By",
				width : 80,
				sortable : true
			}];

	if (userSubmitTriviaColumnsStr != 'null' && userSubmitTriviaColumnsStr != '') {
		columnOrder = userSubmitTriviaColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allSubmitTriviaColumns.length; j++) {
				if (columnWidth[0] == allSubmitTriviaColumns[j].id) {
					userSubmitTriviaColumns[i] = allSubmitTriviaColumns[j];
					userSubmitTriviaColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		userSubmitTriviaColumns = allSubmitTriviaColumns;
	}
	
	var submitTriviaOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var submitTriviaGridSortcol = "submitTriviaId";
	var submitTriviaGridSortdir = 1;
	var percentCompleteThreshold = 0;

	function submitTriviaGridComparer(a, b) {
		var x = a[submitTriviaGridSortcol], y = b[submitTriviaGridSortcol];
		if (!isNaN(x)) {
			return (parseFloat(x) == parseFloat(y) ? 0
					: (parseFloat(x) > parseFloat(y) ? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	
	function refreshSubmitTriviaGridValues(jsonData) {
		$("div#divLoading").addClass('show');
		submitTriviaData = [];
		if (jsonData != null && jsonData.length > 0) {
			for ( var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				var d = (submitTriviaData[i] = {});
				d["id"] = i;
				d["submitTriviaId"] = data.id;
				d["question"] = data.questionText;
				d["optionA"] = data.optionA;
				d["optionB"] = data.optionB;
				d["optionC"] = data.optionC;
				d["answer"] = data.answer;
				if(data.answer == 'A'){
					d["answer"] = data.answer+' - '+ data.optionA;
				}else if(data.answer == 'B'){
					d["answer"] = data.answer+' - '+ data.optionB;
				}else if(data.answer == 'C'){
					d["answer"] = data.answer+' - '+ data.optionC;
				}
				d["status"] = data.status;
				d["createdDate"] = data.createdDateStr;
				d["createdBy"] = data.submittedBy;
			}
		}

		submitTriviaDataView = new Slick.Data.DataView();
		submitTriviaGrid = new Slick.Grid("#submitTrivia_grid", submitTriviaDataView,
				userSubmitTriviaColumns, submitTriviaOptions);
		submitTriviaGrid.registerPlugin(new Slick.AutoTooltips({
			enableForHeaderCells : true
		}));
		submitTriviaGrid.setSelectionModel(new Slick.RowSelectionModel());
		submitTriviaGrid.registerPlugin(questionCheckboxSelector);
		
		submitTriviaGridPager = new Slick.Controls.Pager(submitTriviaDataView,
					submitTriviaGrid, $("#submitTrivia_pager"),
					pagingInfo);
		var submitTriviaGridColumnpicker = new Slick.Controls.ColumnPicker(
				allSubmitTriviaColumns, submitTriviaGrid, submitTriviaOptions);
		
		
		submitTriviaGrid.onContextMenu.subscribe(function (e) {
		      e.preventDefault();
		      var cell = submitTriviaGrid.getCellFromEvent(e);
		      submitTriviaGrid.setSelectedRows([cell.row]);
				var height = screen.height - e.pageY;
				var width = screen.width - e.pageX;
				if(height < $("#contextMenu").height()){
					height = e.pageY - $("#contextMenu").height();
				}else{
					height = e.pageY
				}
				if(width < $("#contextMenu").width()){
					width =  e.pageX- $("#contextMenu").width();
				}else{
					width =  e.pageX;
				}
			 $("#contextMenu")
		          .data("row", cell.row)
		          .css("top", height)
		          .css("left", width)
		          .show();
		      $("body").one("click", function () {
		        $("#contextMenu").hide();
		      });
		    });
		
		$("#contextMenu").click(function (e) {
			if (!$(e.target).is("li")) {
			  return;
			}
			
			if($(e.target).attr("data") == 'copy question'){
				var tempSubmitTriviaRowIndex = submitTriviaGrid.getSelectedRows([0])[0];
				if (tempSubmitTriviaRowIndex == null) {
					jAlert("Plese select Question to Copy", "info");
					return false;
				}else {
					var questionText = submitTriviaGrid.getDataItem(tempSubmitTriviaRowIndex).question;
					var a = submitTriviaGrid.getDataItem(tempSubmitTriviaRowIndex).optionA;
					var b = submitTriviaGrid.getDataItem(tempSubmitTriviaRowIndex).optionB;
					var c = submitTriviaGrid.getDataItem(tempSubmitTriviaRowIndex).optionC;
					var answer = submitTriviaGrid.getDataItem(tempSubmitTriviaRowIndex).answer;
					var copyText = questionText +' | '+a+' | '+b+' | '+c+' | '+answer;
					console.log(copyText);
					var element = document.createElement("textarea");
					element.value = copyText;
					document.body.appendChild(element);
					element.select();
					document.execCommand("copy");
					console.log("copyEle : "+element.value);
					document.body.removeChild(element);
					
				}
			}
		});
		
		
		submitTriviaGrid.onSort.subscribe(function(e, args) {
			submitTriviaGridSortdir = args.sortAsc ? 1 : -1;
			submitTriviaGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				submitTriviaDataView.fastSort(submitTriviaGridSortcol, args.sortAsc);
			} else {
				submitTriviaDataView.sort(submitTriviaGridComparer, args.sortAsc);
			}
		});
		
		// wire up model discountCodes to drive the submitTriviaGrid
		submitTriviaDataView.onRowCountChanged.subscribe(function(e, args) {
			submitTriviaGrid.updateRowCount();
			submitTriviaGrid.render();
		});
		submitTriviaDataView.onRowsChanged.subscribe(function(e, args) {
			submitTriviaGrid.invalidateRows(args.rows);
			submitTriviaGrid.render();
		});
		$(submitTriviaGrid.getHeaderRow())
				.delegate(
						":input",
						"keyup",
						function(e) {
							var keyCode = (e.keyCode ? e.keyCode : e.which);
							submitTriviaSearchString = '';
							var columnId = $(this).data("columnId");
							if (columnId != null) {
								submitTriviaColumnFilters[columnId] = $.trim($(this)
										.val());
								if (keyCode == 13) {
									for ( var columnId in submitTriviaColumnFilters) {
										if (columnId !== undefined
												&& submitTriviaColumnFilters[columnId] !== "") {
											submitTriviaSearchString += columnId
													+ ":"
													+ submitTriviaColumnFilters[columnId]
													+ ",";
										}
									}
									getSubmitTriviaGridData(0);
								}
							}

						});
		submitTriviaGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if (args.column.id.indexOf('checkbox') == -1) {
				if(args.column.id != 'editSubmitTrivia' && args.column.id != 'delSubmitTrivia'){
					if(args.column.id == 'createdDate' || args.column.id == 'updatedDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(submitTriviaColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>").data("columnId", args.column.id)
								.val(submitTriviaColumnFilters[args.column.id]).appendTo(
										args.node);
					}
				}
			}

		});
		submitTriviaGrid.init();
		// initialize the model after all the discountCodes have been hooked up
		submitTriviaDataView.beginUpdate();
		submitTriviaDataView.setItems(submitTriviaData);
		//submitTriviaDataView.setFilter(filter);
		submitTriviaDataView.endUpdate();
		submitTriviaDataView.syncGridSelection(submitTriviaGrid, true);
		submitTriviaGrid.resizeCanvas();
		/* $("#gridContainer").resizable(); */
		$("div#divLoading").removeClass('show');
	}
	
	function saveUserSubmitTriviaPreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = submitTriviaGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('submitTriviagrid', colStr);
	}
	
	function getSelectedQuestionGridId() {
		var tempQuestionRowIndex = submitTriviaGrid.getSelectedRows();
		
		var questionIdStr='';
		$.each(tempQuestionRowIndex, function (index, value) {
			questionIdStr += ','+submitTriviaGrid.getDataItem(value).submitTriviaId;
		});
		
		if(questionIdStr != null && questionIdStr!='') {
			questionIdStr = questionIdStr.substring(1, questionIdStr.length);
			 return questionIdStr;
		}
	}
	//Delete Question Bank
	function deleteSubmitTrivia(){
		var questionIds = getSelectedQuestionGridId();
		if (questionIds == null || questionIds == '' || questionIds == undefined) {
			jAlert("Plese select Question to Delete", "info");
			return false;
		}else {
			//var submitTriviaId = submitTriviaGrid.getDataItem(tempSubmitTriviaRowIndex).submitTriviaId;
			getDeleteSubmitTrivia(questionIds);
		}
	}
	
	
	function getDeleteSubmitTrivia(questionIds){
		/* if (questionIds == '') {
			jAlert("Please select a Question to Delete.","Info");
			return false;
		} */
		jConfirm("Are you sure to delete selected Questions ?","Confirm",function(r){
			if (r) {
				$.ajax({
						url : "${pageContext.request.contextPath}/UpdateSubmitTrivia",
						type : "post",
						dataType: "json",
						data : "qBId="+questionIds+"&action=DELETE&pageNo=0",
						success : function(response) {
							var jsonData = JSON.parse(JSON.stringify(response));
							if(jsonData.status == 1){
								pagingInfo = jsonData.submitTriviaPagingInfo;
								submitTriviaColumnFilters = {};
								refreshSubmitTriviaGridValues(jsonData.submitTriviaList);
								clearAllSelections();
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
						});
			} else {
				return false;
			}
		});
		return false;
	}
	
	
	//call functions once page loaded
	window.onload = function() {
		pagingInfo = ${submitTriviaPagingInfo};
		refreshSubmitTriviaGridValues(${submitTriviaList});
		$('#submitTrivia_pager> div')
				.append(
						"<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserSubmitTriviaPreference()'>");
		
		enableMenu();
	};
		
</script>