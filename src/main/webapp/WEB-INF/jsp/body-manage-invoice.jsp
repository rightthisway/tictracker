<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<link rel="stylesheet" href="../resources/js/slick/slick.grid.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.pager.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/examples/examples.css" type="text/css" />
<link rel="stylesheet" href="../resources/js/slick/controls/slick.columnpicker1.css" type="text/css" />
<!-- slickgrid related scripts -->
<script src="../resources/js/slick/lib/jquery-ui-1.11.3.min.js"></script>
<script src="../resources/js/slick/lib/jquery.event.drag-2.2.js"></script>
<script src="../resources/js/slick/slick.core.js"></script>
<script src="../resources/js/slick/slick.formatters.js"></script>
<script src="../resources/js/slick/slick.editors.js"></script>
<script src="../resources/js/slick/plugins/slick.rowselectionmodel.js"></script>
<script src="../resources/js/slick/slick.grid.js"></script>
<script src="../resources/js/slick/slick.dataview.js"></script>
<script src="../resources/js/slick/controls/slick.pager.js"></script>
<script src="../resources/js/slick/controls/slick.columnpicker.js"></script>
<script src="../resources/js/slick/plugins/slick.autotooltips.js"></script>
<script src="../resources/js/slick/plugins/slick.checkboxselectcolumn.js"></script>

<link href="../resources/css/datepicker.css" rel="stylesheet">
<script src="../resources/js/bootstrap-datepicker.js"></script>
<script src="../resources/js/app/invoiceRelatedPO.js"></script>
<script src="../resources/js/app/invoicePaymentHistory.js"></script>
<script src="../resources/js/app/invoiceAddRealTicket.js"></script>
<script src="../resources/js/app/invoiceShippingAndBillingAddress.js"></script>
<script src="../resources/js/app/invoiceRefundOrCredit.js"></script>
<script src="../resources/js/app/invoiceRefundDetail.js"></script>
<script src="../resources/js/app/invoiceDownloadTicketDetail.js"></script>
<script src="../resources/js/app/invoiceOrderDetail.js"></script>
<script src="../resources/js/app/getCityStateCountry.js"></script>
<script src="../resources/js/app/invoicePayInvoice.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<style>
.cell-title {
	font-weight: bold;
}

.cell-effort-driven {
	text-align: center;
}
.slick-headerrow-column {
    background: #87ceeb;
    text-overflow: clip;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.slick-headerrow-column input {
     margin: 0;
     padding: 0;
     width: 100%;
     height: 100%;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
}
.cell-selection {
	border-right-color: silver;
	border-right-style: solid;
	background: silver;
	color: gray;
	text-align: right;
	font-size: 10px;
}

.slick-row.selected .cell-selection {
	background-color: transparent;
	/* show default selected row background */
}

input {
	color: black !important;
}

#contextMenu {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenu li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenu li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

#contextMenuOther {
	background: #FFFFFF;
	color: #000000;
	border: 1px solid gray;
	padding: 2px;
	display: inline-block;
	min-width: 200px;
	-moz-box-shadow: 2px 2px 2px silver;
	-webkit-box-shadow: 2px 2px 2px silver;
	z-index: 99999;
}

#contextMenuOther li {
	padding: 4px 4px 4px 14px;
	list-style: none;
	cursor: pointer;
}

#contextMenuOther li:hover {
	color: #FFFFFF;
	background-color: #4d94ff;
}

</style>

<script>
$(document).ready(function(){
	 $("div#divLoading").addClass('show');
    /*  var i=0;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='section_"+i+"' type='text' placeholder='Section' class='form-control input-md' /> </td> " +
	   "<td><input  name='row_"+i+"' type='text' placeholder='Row'  class='form-control input-md'></td> "+
     	"<td><input  name='seatHigh_"+i+"' type='text' placeholder='Seat High'  class='form-control input-md'></td> " +
		"<td><input  name='seatLow_"+i+"' type='text' placeholder='Seat Low'  class='form-control input-md'></td> " +
		"<td><input  name='qty_"+i+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> " +
		"<td><input  name='price_"+i+"' type='text' placeholder='Price'  class='form-control input-md'></td> ");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
	  $('#rowCount').val(i);
      i++; 
     });
     $("#delete_row").click(function(){
    	 if(i>1){
		 $("#addr"+(i-1)).html('');
		 i--;
		 }
	 });
*/
	 $('#fromDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#toDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		orientation: "bottom",
		todayHighlight: true
    });
	
	$('#ei_expDeliveryDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true
    });
	
	$("#outStandingInvoice").click(function(){
		callTabOnChange('Outstanding');
	});
	$("#completedInvoice").click(function(){
		callTabOnChange('Completed');
	});
	$("#disputesInvoice").click(function(){
		callTabOnChange('Disputed');
	});
	$("#voidInvoice").click(function(){
		callTabOnChange('Voided');
	});
	$("#allInvoice").click(function(){
		callTabOnChange('All');
	});
	
	<c:choose>
		<c:when test="${status == 'Outstanding'}">
			$('#outstanding').addClass('active');
			$('#outstandingTab').addClass('active');
		</c:when>
		<c:when test="${status == 'Voided'}">
			$('#void').addClass('active');
			$('#voidTab').addClass('active');
		</c:when>
		<c:when test="${status == 'Completed'}">	
			$('#completed').addClass('active');
			$('#completedTab').addClass('active');
		</c:when>
		<c:when test="${status == 'Disputed'}">	
		$('#disputes').addClass('active');
		$('#disputesTab').addClass('active');
	</c:when>
		<c:otherwise>
			$('#all').addClass('active');
			$('#allTab').addClass('active');
		</c:otherwise>
	</c:choose>
	
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	  eventGrid.resizeCanvas();
	});
	
	$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getInvoiceGridData(0);
		    return false;  
		  }
	});
	
});

var prodctType;
function getCompanyProduct(obj){	
	prodctType = obj.value;
}
function callTabOnChange(selectedTab) {		
	var data="?action=search";
	var frmDate = $('#fromDate').val();
	var toDate = $('#toDate').val();	
	var prodType = "${selectedProduct}";
	if(prodctType == null || prodctType == "" || prodctType == 'undefined'){
		prodctType = prodType;
	}
	/*var orderNumber = $('#orderNo').val();
	var invoiceNo = $('#invoiceNo').val();
	var csr = $('#csr').val();
	var customer = $('#customer').val(); 
	var externalOrderId = $('#externalOrderId').val(); */
	 if(frmDate == null || frmDate == ""){}
	else{
		data += "&fromDate="+frmDate; 
	}
	if(toDate == null || toDate == ""){}
	else{
		data += "&toDate="+toDate;
	}
	if(prodctType != null || prodctType != ''){
		data += "&productType="+prodctType;
	}
	/*if(orderNumber == null || orderNumber == ""){}
	else{
		data += "&orderNo="+orderNumber;
	}
	if(invoiceNo == null || invoiceNo == ""){}
	else{
		data += "&invoiceNo="+invoiceNo;
	}
	if(csr == null || csr == ""){}
	else{
		data += "&csr="+csr;
	}
	if(customer == null || customer == ""){}
	else{
		data += "&customer="+customer;
	}
	if(externalOrderId == null || externalOrderId == ""){}
	else{
		data += "&externalOrderId="+externalOrderId;
	} */
	if(selectedTab == 'All'){}
	else{
		data += "&status="+selectedTab;
	}		
	window.location = "${pageContext.request.contextPath}/Accounting/Invoices"+data;
}
	
function getFedexLabel(invoiceId){
	$.ajax({
		url : "${pageContext.request.contextPath}/Accounting/CheckFedexLabelGenerated",
		type : "get",
		data : "invoiceId="+ invoiceId,
		dataType:"json",
		success : function(res){
			if(res.msg=='OK'){
				 //var url = "${pageContext.request.contextPath}/Accounting/GetFedexLabelPdf?invoiceId="+invoiceId;
				 var url = apiServerUrl+"GetFedexLabelPdf?invoiceId="+invoiceId;
				 $('#download-frame').attr('src', url);
			}else{
				jAlert("Fedex Label not found for selected invoice, Please Generate fedex Label.");
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
	
}

function oncChildClosed(){
	jAlert("Record updated succesfully.");
}
function searchInvoices(){
	if($('#outstanding').hasClass('active')){
		$('#status').val('Outstanding');
	}else if($('#void').hasClass('active')){
		$('#status').val('Voided');
	}else if($('#completed').hasClass('active')){
		$('#status').val('Completed');
	}
	$("#action").val('search');
	invoiceGridSearchString='';
	columnFilters = {};
	getInvoiceGridData(0);
}

function saveOpenOrder(){
	$("#action").val('openOrderSave');
	$("#openOrdersForm").submit();
}

function exportToExcel(){	
	var status = "${status}";
	//var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&invoiceNo="+$('#invoiceNo').val()+"&orderNo="+$('#orderNo').val()+"&csr="+$('#csr').val()+"&customer="+$('#customer').val()+"&productType="+$('#productType').val()+"&externalOrderId="+$('#externalOrderId').val()+"&status="+status;
	var appendData = "fromDate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&productType="+$('#productType').val()+"&status="+status+"&headerFilter="+invoiceGridSearchString;
	appendData += "&brokerId="+$('#brokerId').val();
    //var url = "${pageContext.request.contextPath}/Accounting/InvoiceExportToExcel?"+appendData;
    var url = apiServerUrl+"InvoiceExportToExcel?"+appendData;
    $('#download-frame').attr('src', url);
}

function resetFilters(){
	if($('#outstanding').hasClass('active')){
		$('#status').val('Outstanding');
	}else if($('#void').hasClass('active')){
		$('#status').val('Voided');
	}else if($('#completed').hasClass('active')){
		$('#status').val('Completed');
	}else{
		$('#status').val('');
	}
	$("#action").val('search');
	invoiceGridSearchString='';
	sortingString ='';
	columnFilters = {};
	getInvoiceGridData(0);
}
</script>
<ul id="contextMenu" style="display: none; position: absolute">
	<li data="edit invoice">Edit Invoice</li>
	<li data="pdf">Download Pdf</li>
	<li data="print invoice">View/Print Invoice</li>
	<li data="Send Invoice PDF">Send Invoice To Customer</li>
	<li data="view payment history">View Payment History</li>
	<li data="view related po">View Related PO(s)</li>
	<li data="view modify notes">View/Modify notes</li>
	<li data="view customer details">View Customer Details</li>
	<li data="edit shipping address">Edit Shipping Address</li>
	<li data="edit billing address">Edit Billing Address</li>
	<!-- <li data="upload tickets">Upload Tickets</li> -->
	<li data="edit invoice csr">Edit Invoice CSR</li>
	<li data="mark invoice paid">Mark Invoice Complete</li>
	<li data="mark invoice outstanding">Mark Invoice Outstanding</li>
	<!-- <li data="mark invoice void">Mark Invoice Void</li> -->
	<li data="view download ticket details">View Download Ticket Details</li>
	<li data="refund invoice">Refund/Credit Invoice</li>
	<li data="view refund details">View Refund Details</li>
	<li data="View Order Details">View Order Details</li>
	<!-- <li data="Pay Invoice">Pay Invoice</li> -->
</ul>
<ul id="contextMenuOther" style="display: none; position: absolute">
	<li data="pdf">Download Pdf</li>
	<li data="print invoice">View/Print Invoice</li>
	<li data="view payment history">View Payment History</li>
	<li data="view related po">View Related PO(s)</li>
	<li data="view customer details">View Customer Details</li>
</ul>
<div class="row">
	<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
	<div class="col-xs-12">
		<h3 class="page-header">
			<i class="fa fa-laptop"></i> Deliveries
		</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a style="font-size: 13px; font-family: arial;" href="#">Accounting</a>
			</li>
			<li style="font-size: 13px; font-family: arial;"><i class="fa fa-laptop"></i>Invoice</li>
		</ol>
	</div>
	
	<div class="col-xs-12 filters-div">
		<!--  search form start -->
		<form:form role="form" id="invoiceSearch" method="post" onsubmit="return false" action="${pageContext.request.contextPath}/Accounting/Invoices">
			<input type="hidden" value="" id="status" name="status" /> <input type="hidden" id="invoiceNo" name="invoiceNo" value="${invoiceNo}" />
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<input type="hidden" id="action" name="action" value="" /> <label for="name" class="control-label">From Date</label> 
				<input class="form-control searchcontrol" type="text" id="fromDate" name="fromDate" value="${fromDate}">
			</div>
			
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">To Date</label> 
				<input class="form-control searchcontrol" type="text" id="toDate" name="toDate" value="${toDate}">
			</div>
			<!-- 
			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Invoice No</label> 
				<input class="form-control searchcontrol" placeholder="Invoice" type="text" id="invoiceNo" name="invoiceNo" value="${invoiceNo}">
			</div>

			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Order No</label> 
				<input class="form-control searchcontrol" placeholder="Order" type="text" id="orderNo" name="orderNo" value="${orderNo}">
			</div>

			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">CSR</label> 
				<input class="form-control searchcontrol" placeholder="CSR" type="text" id="csr" name="csr" value="${csr}">
			</div>
			
			<div class="form-group col-xs-2 col-md-2">
				<label for="name" class="control-label">Customer</label> 
				<input class="form-control searchcontrol" placeholder="Customer" type="text" id="customer" name="customer" value="${customer}">
			</div>
			 -->
			 <c:if test="${sessionScope.isAdmin == true}">
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<label for="name" class="control-label">Company Product</label> 
				<select id="productType" name="productType" class="form-control" onchange="getCompanyProduct(this);">
					<!-- <option <c:if test="${selectedProduct=='ALL'}"> Selected </c:if> value="ALL">All</option>  -->
					<option <c:if test="${selectedProduct=='REWARDTHEFAN'}"> Selected </c:if> value="REWARDTHEFAN">Reward The Fan</option>
					<option <c:if test="${selectedProduct=='RTW'}"> Selected </c:if> value="RTW">RTW</option>
					<option <c:if test="${selectedProduct=='RTW2'}"> Selected </c:if> value="RTW2">RTW2</option>
					<option <c:if test="${selectedProduct=='SEATGEEK'}"> Selected </c:if> value="SEATGEEK">SEATGEEK</option>
				</select>
			</div>
			</c:if>
			<div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<input type="hidden" name="brokerId" id="brokerId" value="${brokerId}">
				<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="searchInvoices();">search</button>
			</div>	
			
			</form:form>		
	</div>
	<!-- 
	<div class="col-xs-12">		
		<div class="form-group col-xs-3 col-md-3">
			<label for="name" class="control-label">External Order Id</label>
			 <input class="form-control searchcontrol" type="text" id="externalOrderId" name="externalOrderId" value="${externalOrderId}" placeholder="External order id">
		</div>
		
		<div class="form-group col-xs-2 col-md-2">
			<button type="button" id="searchInvoiceBtn" class="btn btn-primary" style="margin-top: 19px;" onclick="searchInvoices();">search</button>
		</div>
	</div>
	-->

</div>
<div id="invoiceDiv">
	<div class="full-width">
		<section class="invoice-panel panel">
		<ul class="nav nav-tabs" style="">
			<li id="outstandingTab" class=""><a id="outStandingInvoice" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#outstanding">Outstanding</a></li>
			<li id="completedTab" class=""><a id="completedInvoice" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#completed">Completed</a>
			<li id="voidTab" class=""><a id="voidInvoice" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#void">Void</a></li>
			<li id="disputesTab" class=""><a id="disputesInvoice" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#disputes">Disputes</a>
			<li id="allTab" class=""><a id="allInvoice" style="font-size: 13px; font-family: arial;" data-toggle="tab" href="#all">All</a></li>
		</ul>
		</section>
	</div>
	<div class="panel-body1 full-width">
		<div class="tab-content">
			<div id="outstanding" class="tab-pane">
				<c:if test="${status =='Outstanding'}">								
				<div class="full-width mb-20 full-width-btn" id="actionButton">
					<button type="button" class="btn btn-primary" onclick="deleteFedexLabel()">Remove Fedex Label</button>
					<button type="button" class="btn btn-primary" onclick="createFedexLabel()">Create Fedex Label</button>
					<button type="submit" class="btn btn-primary" onclick="createRealTicket()">Add Real Ticket</button>					
				</div>
				<br />
				<br />	
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Invoice Details</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>					
				</div>
				</c:if>
			</div>
			
			<div id="completed" class="tab-pane">
				<c:if test="${status =='Completed'}">
				<div class="form-group col-xs-12 col-md-12" id="actionButton">
					<!-- <button type="button" class="btn btn-primary" onclick="deleteFedexLabel()" style="float: right; margin-left: 10px;">Remove Fedex Label</button>
					<button type="button" class="btn btn-primary" onclick="createFedexLabel()" style="float: right; margin-left: 10px;">Create Fedex Label</button>
					<button type="submit" class="btn btn-primary" onclick="createRealTicket()" style="float: right; margin-left: 10px;">Add Real Ticket</button>
					<button type="button" class="btn btn-primary" onclick="exportToExcel()" style="float: right; margin-left: 10px;">Export to Excel</button> -->
				</div>
				<br />
				<br />
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Invoice Details</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
					<!-- <br />
					<br />-->
				</div>
				</c:if>
			</div>
			<div id="disputes" class="tab-pane">
				<c:if test="${status =='Disputed'}">
				<div class="form-group col-xs-12 col-md-12" id="actionButton">
				</div>
				<br />
				<br />
				<div class="table-responsive grid-table">
					<div class="grid-header full-width">
						<label>Invoice Details</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
				</c:if>
			</div>
			<div id="void" class="tab-pane">
			<c:if test="${status =='Voided'}">
				<div class="form-group col-xs-12 col-md-12" id="actionButton">
					<!-- <button type="button" class="btn btn-primary" onclick="deleteFedexLabel()" style="float: right; margin-left: 10px;">Remove Fedex Label</button>
					<button type="button" class="btn btn-primary" onclick="createFedexLabel()" style="float: right; margin-left: 10px;">Create Fedex Label</button>
					<button type="submit" class="btn btn-primary" onclick="createRealTicket()" style="float: right; margin-left: 10px;">Add Real Ticket</button> -->
				</div>
				<br />
				<br />
				<div class="table-responsive grid-table">
					<div class="grid-header" style="width: 100%">
						<label>Invoice Details</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
					<!-- <br />
					<br />-->
				</div>
			</c:if>
			</div>
			<div id="all" class="tab-pane">
				<c:if test="${empty status}">
				<div class="full-width mb-20 full-width-btn" id="actionButton">
					<button type="button" class="btn btn-primary" onclick="deleteFedexLabel()">Remove Fedex Label</button>
					<button type="button" class="btn btn-primary" onclick="createFedexLabel()">Create Fedex Label</button>
					<button type="submit" class="btn btn-primary" onclick="createRealTicket()">Add Real Ticket</button>
				</div>
				<br />
				<br />
				<div class="table-responsive grid-table">				
					<div class="grid-header full-width">
						<label>Invoice Details</label>
						<div class="pull-right">
							<a href="javascript:exportToExcel()" name='Export to Excel' style='float:right; margin-right:10px;'>Export to Excel</a>
							<a href="javascript:resetFilters()" name='Reset Filters' style='float:right; margin-right:10px;'>Reset Filters &nbsp; |</a>
						</div>
					</div>
					<div id="openOrders_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
					<div id="openOrder_pager" style="width: 100%; height: 10px;"></div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
</div>

<!-- ===============****************========= Pop Up Windows Starts here ==============*********************========= -->

<!-- popup Edit Invoice -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-invoice">Edit Invoice</button> -->

	<div id="edit-invoice" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Edit Invoice - Invoice No : <span id="invoiceId_Hdr" class="headerTextClass"></span></h4>
		  </div>
		  <div class="modal-body full-width">
			
				<iframe id="download-frame" src="" width="1" height="1" style="display: none"></iframe>
				<div class="row">
					<div class="col-xs-12">
						<!-- <h3 class="page-header">
							<i class="fa fa-laptop"></i> Deliveries
						</h3> -->
						<ol class="breadcrumb">
							<!-- <li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Deliveries</a>
							</li> -->
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Open Order</li>
						</ol>
					</div>
				</div>

				<div id="ei_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="ei_successMsg"></span></strong>
				</div>					
				<div id="ei_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="ei_errorMsg"></span></strong>
				</div>
				<br />

				<div class="row">
					<div class="col-xs-12">
						<section class="full-width mb-20"> <header style="font-size: 13px;font-family: arial;" class="panel-heading">
						Customer Details </header>
						<div class="panel-body">
						<%-- <c:forEach var="openOrder" items="${openOrders}"> --%>
							<div class="form-group">
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">Customer Name:</label>
									<span style="font-size: 13px;" id="ei_customerName">${openOrder.customerName}</span> 
								</div>
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">Email:</label>
									<span style="font-size: 15px;" id="ei_userName">${openOrder.username}</span>
								</div>
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">Phone:</label>
									<span style="font-size: 15px;" id="ei_phone">${openOrder.phone}</span>
								</div>
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">Address Line:</label>
									<span style="font-size: 15px;" id="ei_addressLine1">${openOrder.addressLine1}</span>
								</div>
							</div>
							<div class="form-group"></div>
							<div class="form-group">
								<div class="col-sm-3"> 
									<label style="font-size: 13px;font-weight:bold;">Country:</label>
									<span style="font-size: 15px;" id="ei_country">${openOrder.country}</span>
								</div>
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">State:</label>
									<span style="font-size: 15px;" id="ei_state">${openOrder.state}</span>
								</div>
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">City:</label>
									<span style="font-size: 15px;" id="ei_city">${openOrder.city}</span>
								</div>
								<div class="col-sm-3">
									<label style="font-size: 13px;font-weight:bold;">ZipCode:</label>
									<span style="font-size: 15px;" id="ei_zipCode">${openOrder.zipCode}</span>
								</div>
							</div>
							<%-- </c:forEach> --%>
						</div>
						</section>
					</div>
				</div>
				

				<!-- Shipping Address section -->
				<div class="row">
					<div class="col-xs-12">
						<section class="panel">
						 <header style="font-size: 13px;font-family: arial;" class="panel-heading">Shipping Address &nbsp;&nbsp;&nbsp;&nbsp;
						 <!-- <input type="checkbox" data-toggle="modal" class="btn btn-primary" name="showShAddr" onclick="showShippingAddress();" id="showShAddr" style="font-size: 13px;font-family: arial;"/>Show Shipping Addres -->
						 </header>
							<div class="panel-body">
							<%--<c:forEach var="shippingAddress" items="${shippingAddress}">--%>
							<div class="form-group full-width">
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Address Line 1:</label>
									<span style="font-size: 13px;" id="ei_sa_addessLine1">${shippingAddress.addressLine1}</span>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Address Line 2:</label>
									<span style="font-size: 15px;" id="ei_sa_addressLine2">${shippingAddress.addressLine2}</span>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Phone:</label>
									<span style="font-size: 15px;" id="ei_sa_phone1">${shippingAddress.phone1}</span>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Country:</label>
									<span style="font-size: 15px;" id="ei_sa_country">${shippingAddress.countryName}</span>
								</div>
							</div>
							<div class="form-group full-width">
								
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">State:</label>
									<span style="font-size: 15px;" id="ei_sa_state">${shippingAddress.stateName}</span>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">City:</label>
									<span style="font-size: 15px;" id="ei_sa_city">${shippingAddress.city}</span>
								</div>
								<div class="col-md-3 col-sm-6 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">ZipCOde:</label>
									<span style="font-size: 15px;" id="ei_sa_zipCode">${shippingAddress.zipCode}</span>
								</div>
							</div>
							<%--</c:forEach>--%>
						</div>
						</section>
					</div>
				</div>


				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<section class="panel"> <header style="font-size: 13px;font-family: arial;" class="panel-heading" id="ei_eventDetailDiv">
							${customerOrder.eventName}, ${customerOrder.eventDateStr},${customerOrder.eventTimeStr},${customerOrder.venueName}
						 </header>
						 <%-- <c:forEach var="openOrder" items="${openOrders}" > --%>
						<div class="panel-body">
							<div class="row" id="ei_categoryTiketDiv">
								<div>
									<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Section:</label>
									<span style="font-size: 12px;" class="col-sm-2" id="ei_section_span">${openOrder.section}</span>
								</div>
								<div>
									<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Row:</label>
									<span class="col-sm-2" id="ei_row_span">${openOrder.row}</span>
								</div>
								<div >
									<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Product Type :</label>
									<span class="col-sm-2" id="ei_productType">${openOrder.productType}</span>
								</div>
							</div>
							
							<div class="row">
								<div>
									<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Shipping Method : </label>
									<span class="col-sm-2" id="ei_shippingMethod_span">${openOrder.shippingMethod}</span>
								</div>
								<div>
									<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Quantity :</label>
									<span class="col-sm-2" id="ei_quantity">${openOrder.qty}</span>
								</div>
								<div id="ei_priceDiv">
									<label style="font-size: 13px;font-weight:bold;" class="col-sm-2">Price:</label>
									<span class="col-sm-2" id="ei_price">${openOrder.price}</span>
								</div>
								
							</div>
							
						</div>
						<%-- </c:forEach> --%>
						</section>
					</div>
				</div>

				<!-- Add Real ticket details -->
				<div class="row">
					<section class="mb-20 col-xs-12">
						<ul class="nav nav-tabs" style="background:#f7f7f7;">
							<li id ="defaultTab" class="active"><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#addTickets">Add Tickets</a></li>
							<li class=""><a style="font-size: 13px;font-family: arial;" data-toggle="tab" href="#uploadTickets">Upload Tickets</a></li>
						</ul>
					</section>
				</div>
				<div class="full-width">
					<form class="form-validate" id="ei_openOrderForm" method="post" enctype="multipart/form-data"
								action="${pageContext.request.contextPath}/Accounting/SaveOpenOrders">
							<div class="tab-content">
								<div id="addTickets" class="tab-pane active">
									<input type="hidden" name="action" id="ei_action" value="action"/>
									<input type="hidden" name="rowCount" id="ei_rowCount"/>
									<input type="hidden" name="invoiceId" id="ei_invoiceId" value="${openOrder.invoiceId}"/>			
									<input type="hidden" name="eventId" id="ei_eventId" value="${customerOrder.eventId}" />
									<input type="hidden" name="eventName" id="ei_eventName" value="${customerOrder.eventName}"/>
									<input type="hidden" name="eventDate" id="ei_eventDate" value="${customerOrder.eventDateStr}"/>
									<input type="hidden" name="eventTime" id="ei_eventTime" value="${customerOrder.eventTimeStr}"/>
									<!-- <input type="hidden" name="productType" value="${openOrder.productType}"/> -->
									<input type="hidden" name="section" id="ei_section" value="${openOrder.section}"/>
									<input type="hidden" name="row" id="ei_row" value="${openOrder.row}"/>
									<input type="hidden" id="ei_primaryPayment" name="primaryPayment" value="${customerOrder.primaryPaymentMethod}" />
									<input type="hidden" id="ei_primaryPaymentAmt" name="primaryPaymentAmt" value="${customerOrder.primaryAvailableAmt}" />
									<input type="hidden" id="ei_secondaryPayment" name="secondaryPayment" value="${customerOrder.secondaryPaymentMethod}" />
									<input type="hidden" id="ei_secondaryPaymentAmt" name="secondaryPaymentAmt" value="${customerOrder.secondaryAvailableAmt}" />
									<input type="hidden" id="ei_thirdPayment" name="thirdPayment" value="${customerOrder.thirdPaymentMethod}" />
									<input type="hidden" id="ei_thirdPaymentAmt" name="thirdPaymentAmt" value="${customerOrder.thirdAvailableAmt}" />
									<div class="table-responsive">
									<table class="table table-bordered table-hover" id="tab_logic">
										<thead>
											<tr>
												<th class="text-center">
													#
												</th>
												<th class ="checkLongInventory" style="font-size: 13px;font-family: arial;" class="text-center">
													Map PO
												</th>
												<th style="font-size: 13px;font-family: arial;" class="text-center">
													Section
												</th>
												<th style="font-size: 13px;font-family: arial;" class="text-center">
													Row
												</th>
												<th style="font-size: 13px;font-family: arial;" class="text-center">
													Seat Low
												</th>
												<th style="font-size: 13px;font-family: arial;" class="text-center">
													Seat High
												</th>
												
												<th style="font-size: 13px;font-family: arial;" class="text-center">
													Qty
												</th>
												<th style="font-size: 13px;font-family: arial;" class="text-center">
													Price
												</th>
												<!-- <th style="font-size: 13px;font-family: arial;" class="text-center">
													Shipping Method
												</th> -->
											</tr>
										</thead>
										<tbody id="ei_ticketGroups">
										<%--<c:if test="${not empty ticketGroups}">
										 <c:forEach var="ticketGroup" varStatus="vStatus" items="${ticketGroups}">
											  <tr id="addr_${vStatus.count}">
											  <td>${vStatus.count}
												  <input type="hidden" name="rowNumber" id="rowNumber_${vStatus.count}" value="${vStatus.count}" />
												  <input type="hidden" name="id_${vStatus.count}" id="id_${vStatus.count}" value="${ticketGroup.id}" />
											  </td>
											  <td style="font-size: 13px;" align="center">
											  		<!-- <button type="button" data-toggle="modal" data-target="#myZoneTicket">View Customer Detail</button> -->
													<a href="javascript:loadPOByEvent(${customerOrder.eventId},${vStatus.count},${ticketGroup.id})">Use Long Inventory</a>
													 <input type="hidden" name="ticketGroup_${vStatus.count}" id="ticketGroup_${vStatus.count}" value="${ticketGroup.id}" />
												</td>
												<td style="font-size: 13px;" align="center">
													<input name="section_${vStatus.count}" id="section_${vStatus.count}" readonly="readonly" type='text' value="${ticketGroup.section}" placeholder='Section' class='form-control input-md' />
												</td>
												<td style="font-size: 13px;" align="center">
													<input name="row_${vStatus.count}" id="row_${vStatus.count}" type='text' readonly="readonly" value="${ticketGroup.row}" placeholder='Row' class='form-control input-md' />
												</td>
												<td style="font-size: 13px;" align="center">
													<input name="seatLow_${vStatus.count}" id="seatLow_${vStatus.count}" type='text' value="${ticketGroup.mappedSeatLow}" placeholder='Seat Low' class='form-control input-md' />
												</td>
												<td style="font-size: 13px;" align="center">
													<input name="seatHigh_${vStatus.count}" id="seatHigh_${vStatus.count}" type='text' value="${ticketGroup.mappedSeatHigh}" placeholder='Seat High' class='form-control input-md' />
												</td>
												<td style="font-size: 13px;" align="center">
													<input name="qty_${vStatus.count}" id="qty_${vStatus.count}" type='text' placeholder='Qty' value="${ticketGroup.mappedQty}" class='form-control input-md' />
												</td>
												<td style="font-size: 13px;" align="center">
													<input name="price_${vStatus.count}" id="price_${vStatus.count}" type='text' readonly="readonly" placeholder='Price' value="${ticketGroup.priceStr}" class='form-control input-md' />
												</td>
											</tr>
											</c:forEach>
											</c:if>--%>
										</tbody>
									</table>
									</div>
									<div class="full-width mb-10 mt-10">
										<a id="ei_add_row" onclick="callAddRealTicketRow();" class="btn btn-primary" style="margin-right:15px;"  onclick="callAddRealTicketRow();">Add Row</a>
										<a id='ei_delete_row' class="btn btn-cancel" onclick="callDeleteRealTicketRow();">Delete Row</a>
									</div>
									<br/>
								<div class="row" style="">
									<div class="full-width">
										  
										  <div class="col-lg-3 col-sm-3 ">
											<label>Shipping Method</label>
											<select id="ei_shippingMethod" name="shippingMethod" class="form-control input-sm m-bot15" onchange="showHideFedexInfo()">
											  <%--<option value="-1">--select--</option>
											  <c:forEach items="${shippingMethods}" var="shippingMethod">
												<option <c:if test="${shippingMethod.id==openOrder.shippingMethodId}"> Selected </c:if>
													value="${shippingMethod.id}"> ${shippingMethod.name}
												</option>
											</c:forEach>--%>
											</select>
										  </div>
										 
										<div id="ei_trackingInfo" class="col-lg-3 col-sm-3 ">
											<label>Tracking No</label>
											<input type="text" id="ei_trackingNo" name="trackingNo" class="form-control input-sm m-bot15" value="${invoice.trackingNo}">
										 </div>
										  
										  <%--<div class="col-lg-3 col-sm-3 ">
											<label>Shipment Completed</label>
											<select id="ei_realTixDelivered" name="realTixDelivered" class="form-control input-sm m-bot15">
												<option value='0'<c:if test="${openOrder ne null and !openOrder.realTixDelivered}">selected</c:if>>No</option>
												<option value='1' <c:if test="${openOrder ne null and openOrder.realTixDelivered}">selected</c:if>>Yes</option> 
											</select>
										  </div>--%>
										  <div class="col-lg-3 col-sm-3 ">
											<label>Expected Delivery Date</label>
											<input type="text" id="ei_expDeliveryDate" name="expDeliveryDate" class="form-control input-sm m-bot15">
										 </div>
										 </div>
									 </div>
					 <%--<div class="row" style="">
						<div class="full-width">
						  <div class="col-lg-3 col-sm-3 ">
							 <label>Purchase Order No</label>
								<input type="text" id="poNo" class="form-control input-sm m-bot15" name="poNo" value="${invoice.poNo}">
						  </div>
					   </div>
					 </div> --%>
				</div>
				
				<div id="uploadTickets" class="tab-pane" style="">
					<div class="form-group full-width">
					<div class="col-sm-4 col-xs-12">
						<div class="form-group full-width">
							<a id="eticket_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addTicketUploadRow('ei_eticketTable');">Add Row</a>
							<a id='eticket_delete_row' class='btn btn-cancel btn-sm pull-left' style="margin-right:10px;" onclick="deleteTicketUploadRow('ei_eticketTable');">Delete Row</a>
						</div>
						<!-- <div class="form-group full-width">
							<a id="qrcode_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addTicketUploadRow('ei_qrcodeTable');">Add Row</a>
							<a id='qrcode_delete_row' class='btn btn-cancel btn-sm pull-left' onclick="deleteTicketUploadRow('ei_qrcodeTable');">Delete Row</a>
						</div>
						<div class="form-group full-width">
							<a id="barcode_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addTicketUploadRow('ei_barcodeTable');">Add Row</a>
							<a id='barcode_delete_row' class='btn btn-cancel btn-sm pull-left' onclick="deleteTicketUploadRow('ei_barcodeTable');">Delete Row</a>
						</div> -->
						<table class="table table-bordered table-hover" id="ei_eticketTable">
						<input type="hidden" name="eticketTable_count" id="ei_eticketTable_count" value="${eticketCount}">
							<thead>
								<tr >
									<th class="text-center">
										#
									</th>
									<th style="font-size: 13px;font-family: arial;" class="text-center">
										E-ticket
									</th>
								</tr>
							</thead>
							<tbody id="ei_eticketTable_body">
							<c:if test="${not empty eticketAttachments}">
								 <c:forEach var="eticket" varStatus="vStatus" items="${eticketAttachments}">
									<tr id="eticketTable_${eticket.position}" class="savedClass">
										<td>${vStatus.count}</td>
										<td> 
											<input type="file" id="eticket_${eticket.position}" name="eticket_${eticket.position}"/><br/>
											<a href="javascript:downloadTicketFile('${eticket.invoiceId}','${eticket.type}','${eticket.position}')" >${eticket.fileName}</a>
											<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${eticket.invoiceId}','${eticket.type}','${eticket.position}')"/> --%>
										</td>
									</tr>
								</c:forEach>
							</c:if>
							</tbody>
						</table>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="form-group full-width">
								<a id="qrcode_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addTicketUploadRow('ei_qrcodeTable');">Add Row</a>
								<a id='qrcode_delete_row' class="btn btn-cancel btn-sm pull-left" style="margin-right:10px;" onclick="deleteTicketUploadRow('ei_qrcodeTable');">Delete Row</a>
							</div>
							<table class="table table-bordered table-hover" id="ei_qrcodeTable">
							<input type="hidden" name="qrcodeTable_count" id="ei_qrcodeTable_count" value="${qrcodeCount}">
							<thead>
								<tr >
									<th class="text-center">
										#
									</th>
									<th style="font-size: 13px;font-family: arial;" class="text-center">
										Mobile Entry
									</th>
									
								</tr>
							</thead>
							<tbody id="ei_qrcodeTable_body">
							<c:if test="${not empty qrcodeAttachments}">
								 <c:forEach var="qrcode" varStatus="vStatus" items="${qrcodeAttachments}">
									<tr id="qrcodeTable_${qrcode.position}" class="savedClass">
										<td>${vStatus.count}</td>
										<td> 
											<input type="file" id="qrcode_${qrcode.position}" name="qrcode_${qrcode.position}"/><br/>
											<a href="javascript:downloadTicketFile('${qrcode.invoiceId}','${qrcode.type}','${qrcode.position}')" >${qrcode.fileName}</a>
											<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${qrcode.invoiceId}','${qrcode.type}','${qrcode.position}')"/> --%>
										</td>
									</tr>
								</c:forEach>
							</c:if>
							</tbody>
						</table>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="form-group full-width">
								<a id="barcode_add_row" class="btn btn-primary btn-sm pull-left" style="margin-right:10px;"  onclick="addTicketUploadRow('ei_barcodeTable');">Add Row</a>
								<a id='barcode_delete_row' class='btn btn-cancel btn-sm pull-left' style="margin-right:10px;" onclick="deleteTicketUploadRow('ei_barcodeTable');">Delete Row</a>
							</div>
							<table class="table table-bordered table-hover" id="ei_barcodeTable">
							<input type="hidden" name="barcodeTable_count" id="ei_barcodeTable_count" value="${barcodeCount}">
							<thead>
								<tr >
									<th class="text-center">
										#
									</th>
									<th style="font-size: 13px;font-family: arial;" class="text-center">
										Electronic Transfer 
									</th>
								</tr>
							</thead>
							<tbody id="ei_barcodeTable_body">
							<c:if test="${not empty barcodeAttachments}">
								 <c:forEach var="barcode" varStatus="vStatus" items="${barcodeAttachments}">
									<tr id="barcodeTable_${barcode.position}" class="savedClass">
										<td>${vStatus.count}</td>
										<td> 
											<input type="file" id="barcode_${barcode.position}" name="barcode_${barcode.position}"/><br/>
											<a href="javascript:downloadTicketFile('${barcode.invoiceId}','${barcode.type}','${barcode.position}')" >${barcode.fileName}</a>
											<%-- <img class="editClickableImage" style="height:14px;" src="../resources/images/ico-delete.gif" onclick="deleteFile('${barcode.invoiceId}','${barcode.type}','${barcode.position}')"/> --%>
										</td>
									</tr>
								</c:forEach>
							</c:if>
							</tbody>
						</table>
						</div>
					</div>
					<div class="full-width mb-20" align="center">
						 <input type="checkbox" id="ei_sendTicket" name="sendTicket"<%--  <c:if test="${invoice.uploadToExchnge == 'true'}">checked</c:if> --%>>
						<input type="hidden" id="ei_sendTicketToCustomer" name="sendTicketToCustomer" value="">
						 <label >Email Ticket Attachment to Customer</label>
					</div>

						</div>
						</div>
					</div>
			</form>
		  </div>
		  <div class="modal-footer full-width">				
				<div class="full-width" align="center">
					<span id="ei_btnDiv" style="display:none;">
						<button type="button" onclick="saveRealTicket();" class="btn btn-primary">Save & Send</button>
						<span id="ei_btnDiv1" style="display:none;">
							<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#ei_vo_myModal-2" onclick="doVoidInvoice();">Void Invoice</button>
						</span>
					</span>
					<button type="button" data-dismiss="modal" onclick="callCloseBtnOnClick();" class="btn btn-cancel">Close</button>
				</div>
	
		  </div>
		</div>

	  </div>
	</div>
	
	<!-- popup Check Long Inventory -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#long-inventory">Check Long Inventory</button> -->
	<div id="long-inventory" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Check Long Inventory</h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Map Long Inventory(PO)</li>
						</ol>
					</div>
				</div>
				<br />

				<div class="full-width" style="position: relative" id="po">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Purchase Orders</label> <span id="longInventory_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel" onclick="longInventoryGridToggleFilterRow()"></span>
						</div>
						<div id="longInventory_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="longInventory_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				

				<div class="full-width mt-20" id="longInventory_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
								Show records with PO <input type="text" id="longInventoryGridSearch">
				</div>
			</div>
			<div class="modal-footer full-width">
				<input type="hidden" value="${rowId}" id="ei_li_rowId"/>
				<button type="button" onclick="selectRecordCloseWindow();" style="width:100px" class="btn btn-primary">Map</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Check Long Inventory -->

	
	<!-- void invoice popup-->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#ei_vo_myModal-2">Void Invoice</button> -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
		tabindex="-1" id="ei_vo_myModal-2" class="modal fade" style="">
		<div class="modal-dialog modal-lg" style="display:table;">
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Void Invoice</h4>
				</div>
					
					<form class="form-validate form-horizontal" id="ei_void_openOrderForm" method="post" enctype="multipart/form-data"
								action="${pageContext.request.contextPath}/Accounting/SaveOpenOrders">
					<div class="modal-body full-width">
					
						<input type="hidden" id="ei_vo_action" name="action" value="addShippingAddress" /> 
						<input type="hidden" name="shippingAddrId" id="shippingAddrId" value="" /> 
						<input type="hidden" class="form-control" id="custId" name="custId" value="${customerId}" />
						<input type="hidden" name="count" id="count" value="${count}" />
						<div class="form-group">										
							
							<div class="col-xs-12 text-center">
								<label class="full-width">Refund Type <span class="required">*</span> </label> 
								<select id="ei_vo_refundType" name="refundType" class="form-control input-sm m-bot15" 
								onchange="loadRefundType(this)" style="display:inline-block;max-width:250px">
									<option value="">--select--</option>
									<option value="NOREFUND">No Refund</option>
									<option value="FULLREFUND">Full Refund</option>
									<option value="PARTIALREFUND">Partial Refund</option>
								</select>
							</div>
											
						</div>
										
						<div class="form-group full-width">
							<div class="col-md-4 col-sm-3 col-xs-6" style="float: left; display:none;" id="ei_vo_refundAmountDiv">
								<label>Refund Amount <span class="required">*</span> </label> 
								<input class="form-control" id="ei_vo_refundAmount" type="text" name="refundAmount" value="" />
							</div>
							<div class="col-md-4 col-sm-3 col-xs-6" id="ei_vo_walletCCDiv" style="display:none;">
								<label>Credit To Customer Wallet From Credit Card/PayPal<span class="required">*</span> </label> 
								<input class="form-control" id="ei_vo_walletCCAmount" type="text" name="walletCCAmount" value="" />
							</div>
							<div class="col-md-4 col-sm-3 col-xs-6" id="ei_vo_walletDiv" style="display:none;">
								<label>Credit To Customer Wallet From Wallet<span class="required">*</span> </label> 
								<input class="form-control" id="ei_vo_walletAmount" type="text" name="walletAmount" value="" />
							</div>
							<div class="col-md-4 col-sm-3 col-xs-6" id="ei_vo_rewardPointDiv" style="display:none;">
								<label>Reward Points <span class="required">*</span> </label> 
								<input class="form-control" id="ei_vo_rewardPoint" type="text" name="rewardPoint" value="" />
							</div>
							<div class="col-xs-12" id="ei_vo_msgDiv" style="display:none;">
								<div class="alert alert-block alert-danger fade in">
									<span id="ei_vo_msgBox" style="display: block; text-align: center;"></span>
								</div>
							</div>
						</div>
						
					</div>
					<div class="modal-footer full-width">
						<button class="btn btn-primary" type="button" onclick="voidSaveRefund()">Void Invoice</button>
						<button class="btn btn-cancel" data-dismiss="modal" onclick="voidCancelBtn();" type="button">Close</button>
					</div>
				</form>				
			</div>
		</div>
	</div>
	<!-- Ends void invoice popup -->

	<!-- popup Preview Ticket -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#preview-ticket">Check Long Inventory</button> -->
	<div id="preview-ticket" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><span id="previewFileName" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<iframe id="viewer" frameborder="0" scrolling="no" width="100%" height="600"></iframe>
			</div>
			
		</div>

	  </div>
	</div>
<!-- End popup Preview Ticket -->

<!-- End popup Edit Invoice -->

<!-- popup View Payment History -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-payment-history">View Payment History</button> -->

	<div id="view-payment-history" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">View Payment History - Invoice No : <span id="invoiceId_Hdr_PayHistory" class="headerTextClass"></span></h4>
		  </div>
		  <div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Payment History</li>
						</ol>
					</div>
				</div>
				<br />

				<div id="paymentHistorySuccessDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="paymentHistorySuccessMsg"></span></strong>
				</div>					
				<div id="paymentHistoryErrorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="paymentHistoryErrorMsg"></span></strong>
				</div>
				<br />

				<div class="full-width" style="position: relative" id="paymentHistory">
					<div class="table-responsive grid-table">						
						<div class="grid-header full-width">
							<label>Payment History</label> <!--<span id="paymentHistory_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel"></span>-->
						</div>
						<div id="paymentGrid" style="display:none;">
							<div id="paymentHistory_grid" style="width: 100%; float:left; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="paymentHistory_pager" style="width: 100%; float:left; height: 10px;"></div>
						</div>						
						<div id="rtwPaymentGrid" style="display:none;">
							<div id="rtwpaymentHistory_grid" style="width: 100%; float:left; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="rtwpaymentHistory_pager" style="width: 100%; float:left; height: 10px;"></div>
						</div>					
					</div>
				</div>
				<!--
				<div id="inlineFilterPanel" style="display:none;background:#dddddd;padding:3px;color:black;">
									Show records with payment history including <input type="text" id="txtSearch2">
				</div>
				-->
		  </div>
		  <div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View Payment History -->

<!-- popup View Related PO -->
	<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-related-po">View Related PO</button>-->

	<div id="view-related-po" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content full-width">
			  <div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Related PO - Invoice No : <span id="invoiceId_Hdr_RelatedPO" class="headerTextClass"></span></h4>
			  </div>
			  <div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i> Accounting
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
								</li>
								<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Purchase Orders</li>
							</ol>
						</div>
					</div>
					<br />
					
					<div id="relatedPOSuccessDiv" class="alert alert-success fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="relatedPOSuccessMsg"></span></strong>
					</div>					
					<div id="relatedPOErrorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="relatedPOErrorMsg"></span></strong>
					</div>
					<br />

					<div class="full-width mb-20" style="position: relative" id="purchaseOrder">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Purchase Order</label> <span id="openOrder_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"></span>
							</div>
							<div id="po_grid" style="width: 100%; height: 100px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="po_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
					
					<div class="full-width" style="position: relative" id="invoice">
						<div class="table-responsive grid-table">
							<div class="grid-header full-width">
								<label>Invoices</label> <span id="openOrder_grid_toogle_search" style="float: right"
									class="ui-icon ui-icon-search" title="Toggle search panel"></span>
							</div>
							<div id="invoice_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
							<div id="invoice_pager" style="width: 100%; height: 10px;"></div>
						</div>
					</div>
			  </div>
			  <div class="modal-footer full-width">
					<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
				</div>
			</div>
		  </div>
	</div>
<!-- End View Related PO -->

<!-- popup View/Modify Notes -->
	<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-modify-notes">View/Modify Notes</button>-->

	<div id="view-modify-notes" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
		  <div class="modal-header full-width">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">View/Modify Notes - Invoice No : <span id="invoiceId_Hdr_ModNotes" class="headerTextClass"></span></h4>
		  </div>
		  <div class="modal-body full-width">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header">
						<i class="fa fa-laptop"></i>Accounting
					</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Invoice</a>
						</li>
						<li><i class="fa fa-laptop"></i>Note</li>
					</ol>
				</div>
			</div>
			<div id="ModifyNotesMsg" style="display: none;" class="alert alert-success fade in"></div>
			<%--<c:if test="${successMessage != null}">
				<div class="alert alert-success fade in">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${successMessage}</strong>
				</div>
			</c:if>
			<c:if test="${errorMessage != null}">
				<div class="alert alert-block alert-danger fade in">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">${errorMessage}</strong>
				</div>
			</c:if>--%><!---->
			<br />

			<div id="row">
				<div class="col-xs-12">
					<form class="form-validate form-horizontal" id="editInvoiceNote" method="post" action="${pageContext.request.contextPath}/Accounting/EditInvoiceNote">
						<div class="form-group">
							<input type="hidden" id="action" name="action" value="update" />
							 <input type="hidden" id="modifyNotesInvoiceId" name="modifyNotesInvoiceId" />
							<div class="col-xs-12" align="center">
								<label>Invoice Note</label>
								<textarea class="form-control mb-20" id="modifyNotesInvoiceNote" name="modifyNotesInvoiceNote" cols="50" rows="5"></textarea>
							</div>
						</div>
					</form>
				</div>
				
			</div>

		  </div>
		  <div class="modal-footer full-width">
				<button type="button" onclick="updateInvoiceNote();" style="" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View/Modify Notes -->

<!-- popup View Customer Details -->
	<%@include file="body-view-customer-details.jsp"%>
<!-- End popup View Customer Details -->

<!-- popup EditShippingAddress -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-shipping-address">Edit Shipping Address</button> -->

	<div id="edit-shipping-address" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Shipping Address - Invoice No : <span id="invoiceId_Hdr_EdShipping" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i>Accounting
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a href="#">Invoice</a>
								</li>
								<li><i class="fa fa-laptop"></i>Order Shipping Address</li>
							</ol>
						</div>
					</div>
					<br />
					<div id="esa_successDiv" class="alert alert-success fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="esa_successMsg"></span></strong>
					</div>					
					<div id="esa_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="esa_errorMsg"></span></strong>
					</div>
					<br />
					
					<div id="row">
						<div class="col-xs-12">
							<form class="form-validate form-horizontal" id="updateShippingAddress" onsubmit="return false">
								<div class="form-group">
									<input type="hidden" id="esa_action" name="action" value="" /> 
									<input type="hidden" id="esa_orderId" name="orderId" value="${order.orderId}" />
									<div class="col-xs-6">
										<label>First Name <span class="required">*</span></label> 
										<input type="text" id="esa_firstName" name="firstName" value="${order.shFirstName}" class='form-control' />
									</div>
									<div class="col-xs-6">
										<label>Last Name <span class="required">*</span></label> 
										<input type="text" id="esa_lastName" name="lastName" value="${order.shLastName}" class='form-control' />
									</div>

								</div>
								<div class="form-group">
									<div class="col-xs-6">
										<label>Email <span class="required">*</span></label> 
										<input type="text" id="esa_email" name="email" value="${order.shEmail}" class='form-control' />
									</div>
									<div class="col-xs-6">
										<label>Phone <span class="required">*</span></label> 
										<input type="text" id="esa_phone" name="phone" value="${order.shPhone1}" class='form-control' />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-6">
										<label>Street-1 </label> 
										<input type="text" id="esa_street1" name="street1" value="${order.shAddress1}" class='form-control' />
									</div>
									<div class="col-xs-6">
										<label>Street-2 </label> 
										<input type="text" id="esa_street2" name="street2" value="${order.shAddress2}" class='form-control' />
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-6">
										<label>Zipcode <span class="required">*</span></label>
										<input type="text" id="esa_zipcode" name="zipcode" value="${order.shZipCode}" class='form-control' onblur="getCityStateCountry(this.value, 'body-manage-invoice-shipping')"/>
									</div>
									<div class="col-xs-6">
										<label>City <span class="required">*</span></label> 
										<input type="text" id="esa_city" name="city" value="${order.shCity}" class='form-control' />
									</div>
								</div>
								<div class="form-group">									
									<div class="col-xs-6">
										<label>Country <span class="required">*</span></label>
										<select id="esa_country" name="country" class="form-control" onchange="loadState(esa_country,esa_state,'')">
											<%--<option value='-1'>--select--</option>
											<c:forEach var="country" items="${countryList}">
												<option value="${country.id}" <c:if test="${order.shCountryName == country.name}"> selected </c:if>>
													<c:out value="${country.name}" />
												</option>
											</c:forEach>--%>
										</select>
									</div>
									<div class="col-xs-6">
										<label>State <span class="required">*</span></label>
										<select id="esa_state" name="state" class="form-control" onchange="">
											<%--<option value='-1'>--select--</option>
											 <c:forEach var="state" items="${stateList}">
												<option value="${state.id}" <c:if test="${order.shStateName == state.name}"> selected </c:if>>
													<c:out value="${state.name}" />
												</option>
											</c:forEach>--%>
										</select>
									</div>
								</div>
								
								<!--<div id="esa_fedex" style="display:none">
									<div class="form-group">
										<div class="col-xs-6">
											<label>Service Type <span class="required">*</span>
											</label>  <select id="esa_serviceType" name="serviceType" class="form-control" onchange="">
												<option value='select'>--select--</option>
												<option value="FEDEX_EXPRESS_SAVER"> FEDEX EXPRESS SAVER </option>						
												<option value="FIRST_OVERNIGHT"> FIRST OVERNIGHT </option>
												<option value="PRIORITY_OVERNIGHT"> PRIORITY OVERNIGHT </option>
												<option value="STANDARD_OVERNIGHT"> STANDARD OVERNIGHT </option>
												<option value="FEDEX_2_DAY"> FEDEX 2-DAY </option>
												<option value="GROUND_HOME_DELIVERY"> GROUND HOME DELIVERY </option>
												<option value="INTERNATIONAL_PRIORITY"> INTERNATIONAL PRIORITY </option>
											</select>
										</div>
										<div class="col-xs-6">
											<label>Signature Type <span class="required">*</span>
											</label>  <select id="esa_signatureType" name="signatureType" class="form-control" onchange="">
												<option value="NO_SIGNATURE_REQUIRED">NO SIGNATURE REQUIRED</option>
												<option value="SERVICE_DEFAULT">SERVICE DEFAULT</option>
												<option value="INDIRECT">INDIRECT</option>
												<option value="DIRECT">DIRECT</option>
												<option value="ADULT">ADULT</option>
											</select>
										</div>
									</div>
								</div>
								<input type="hidden" id="esa_invoiceId" name="invoiceId" value="${invoiceId}" />-->
							</form>
						</div>
					</div>
						<br />
					</div>
					
					<div class="modal-footer full-width">							
							<div id="esa_btnDiv" style="display:none">
								<button type="button" onclick="updateAddress('SAVE');"  class="btn btn-primary" style="margin-right10px;">Update Address</button>
														
							<!--<div id="esa_btnDiv1" style="display:none">
								<button type="button" onclick="updateAddress('CREATE');"  class="btn btn-primary btn-sm">Save and Create Label</button>
							</div>-->
								<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
							</div>
					</div>
		</div>

	  </div>
	</div>
<!-- End popup EditShippingAddress -->


<!-- popup EditBillingAddress -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-billing-address">Edit Billing Address</button> -->

	<div id="edit-billing-address" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Billing Address - Invoice No : <span id="invoiceId_Hdr_EdBilling" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Invoice</a>
							</li>
							<li><i class="fa fa-laptop"></i>Order Billing Address</li>
						</ol>
					</div>
				</div>
				<br />
				<div id="eba_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="eba_successMsg"></span></strong>
				</div>					
				<div id="eba_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="eba_errorMsg"></span></strong>
				</div>
				<br />
				
				<div id="row">
					<div class="col-xs-12">
						<form class="form-validate form-horizontal" id="updateBillingAddress" method="post" action="${pageContext.request.contextPath}/Accounting/EditBillingAddress">
							<div class="form-group">
								<input type="hidden" id="eba_action" name="action" value="" /> 
								<input type="hidden" id="eba_orderId" name="orderId" value="${order.orderId}" />
								<div class="col-xs-6">
									<label>First Name <span class="required">*</span>
									</label> <input type="text" id="eba_firstName" name="firstName" value="${order.blFirstName}" class='form-control' />
								</div>
								<div class="col-xs-6">
									<label>Last Name <span class="required">*</span>
									</label> <input type="text" id="eba_lastName" name="lastName" value="${order.blLastName}" class='form-control' />
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-6">
									<label>Email <span class="required">*</span></label> 
									<input type="text" id="eba_email" name="email" value="${order.blEmail}" class='form-control' />
								</div>
								<div class="col-xs-6">
									<label>Phone <span class="required">*</span></label> 
									<input type="text" id="eba_phone" name="phone" value="${order.blPhone1}" class='form-control' />
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-6">
									<label>Street-1 </label> 
									<input type="text" id="eba_street1" name="street1" value="${order.blAddress1}" class='form-control' />
								</div>
								<div class="col-xs-6">
									<label>Street-2 </label> 
									<input type="text" id="eba_street2" name="street2" value="${order.blAddress2}" class='form-control' />
								</div>
							</div>
							<div class="form-group">								
								<div class="col-xs-6">
									<label>Zipcode <span class="required">*</span></label> 
									<input type="text" id="eba_zipcode" name="zipcode" value="${order.blZipCode}" class='form-control' onblur="getCityStateCountry(this.value, 'body-manage-invoice-billing')"/>
								</div>
								<div class="col-xs-6">
									<label>City <span class="required">*</span></label> 
									<input type="text" id="eba_city" name="city" value="${order.blCity}" class='form-control' />
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-6">
									<label>Country <span class="required">*</span></label> 
									<select id="eba_country" name="country" class="form-control" onchange="loadState(eba_country,eba_state,'')">
										<%--<option value='-1'>--select--</option>
										<c:forEach var="country" items="${countryList}">
											<option value="${country.id}" <c:if test="${order.blCountryName == country.name}"> selected </c:if>>
												<c:out value="${country.name}" />
											</option>
										</c:forEach>--%>
									</select>
								</div>							
								<div class="col-xs-6">
									<label>State <span class="required">*</span></label> 
									<select id="eba_state" name="state" class="form-control" onchange="">
										<%--<option value='-1'>--select--</option>
										 <c:forEach var="state" items="${stateList}">
											<option value="${state.id}" <c:if test="${order.blStateName == state.name}"> selected </c:if>>
												<c:out value="${state.name}" />
											</option>
										</c:forEach>--%>
									</select>
								</div>
							</div>
														
							<input type="hidden" id="eba_invoiceId" name="invoiceId" value="${invoiceId}" />
						</form>
					</div>
				</div>
					
				
			</div>
			<div class="modal-footer full-width">
				<button type="button" onclick="updateBillingAddress();"  class="btn btn-primary" style="margin-right:10px;">Update Address</button>
				<%-- <c:if test="${not empty invoiceId}">
					<button type="button" onclick="createFedexLabel();"  class="btn btn-primary">Create Fedex Label</button>
				</c:if> --%>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup EditBillingAddress -->

<!-- popup Edit Invoice Csr -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-invoice-csr">Edit Invoice Csr</button> -->

	<div id="edit-invoice-csr" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Invoice CSR - Invoice No : <span id="invoiceId_Hdr_EdCSR" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Invoice</a>
							</li>
							<li><i class="fa fa-laptop"></i>Invoice CSR</li>
						</ol>
					</div>
				</div>
				<div id="eic_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="eic_successMsg"></span></strong>
				</div>					
				<div id="eic_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="eic_errorMsg"></span></strong>
				</div>
				<br />

				<div id="row">
					<div class="col-xs-12">
						<form class="form-validate form-horizontal" id="eic_changeCsr" method="post" action="${pageContext.request.contextPath}/Accounting/EditInvoiceCsr">
							<div class="form-group">
								<input type="hidden" id="eic_action" name="action" value="" /> 
								<input type="hidden" id="eic_invoiceId" name="invoiceId" value="${invoiceId}" />
								<div class="col-xs-12" align="center">
									<label>CSR</label> 
									<select name="csrUser" id="eic_csrUser" style="font-size: 13px; max-width:300px;" class='form-control input-md'>
										<%--<option value="auto">Auto</option>
										<c:forEach items="${users}" var="user">
											<option value="${user.userName}" <c:if test="${invoiceCsr ==user.userName}">selected</c:if>>${user.userName}</option>
										</c:forEach>--%>
									</select>
								</div>
							</div>
						</form>
					</div>
					
					
				</div>

			</div>
			<div class="modal-footer full-width">
				<button type="button" onclick="changeInvoiceCsr();" style="width:100px" class="btn btn-primary">Save</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup Edit Invoice Csr -->

<!-- popup Refund/Credit Invoice -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#refund-credit-invoice">Refund/Credit Invoice</button> -->

	<div id="refund-credit-invoice" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Refund/Credit Invoice - Invoice No : <span id="invoiceId_Hdr_Refund" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Invoice</a>
							</li>
							<li><i class="fa fa-laptop"></i>Refund</li>
						</ol>
					</div>
				</div>
				<br />
				<div id="rci_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="rci_successMsg"></span></strong>
				</div>					
				<div id="rci_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="rci_errorMsg"></span></strong>
				</div>
				<br />
				<!--<div class="col-xs-12" id="msgDiv" style="display:none;">
					<div class="alert alert-success fade in">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="msgBox"></span>
						</strong>
					</div>
				</div>-->
				<div class="row">
					<div class="col-xs-12">
						<section class="panel"> 
						<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Payment Details</b></header>
							<div class="panel-body">
							
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer Name : </label>
									<span style="font-size: 12px;" id="rci_customerName">${customer.customerName} ${customer.lastName}</span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer Email : </label>
									<span style="font-size: 12px;" id="rci_customerEmail">${customer.email}</span>
								</div>
								<%--<div class="col-sm-6  col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer phone :</label>
									<span style="font-size: 12px;">${customer.phone}</span>
								</div>--%><!---->
							</div>
							
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Invoice Number : </label>
									<span style="font-size: 12px;" id="rci_invoiceId">${invoice.id} </span>
								</div> 
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Order No : </label>
									<span style="font-size: 12px;" id="rci_orderId_span">${order.id}</span>
								</div>
							</div>
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Order Created On : </label>
									<span style="font-size: 12px;" id="rci_orderCreateDate">${order.createdDateStr}</span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Order Total : </label>
									<span style="font-size: 12px;" id="rci_orderTotal">${order.orderTotal}</span>
								</div>
							</div>
							
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Primary Payment Method : </label>
									<span style="font-size: 12px;" id="rci_primaryPaymentMethod">${order.primaryPaymentMethod}</span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Secondary Payment Method : </label>
									<span style="font-size: 12px;" id="rci_secondaryPaymentMethod"></span>
								</div>
							</div>
							
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Primary Payment Amount : </label>
									<span style="font-size: 12px;" id="rci_primaryAvailableAmount_span">${order.primaryAvailableAmt}</span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Secondary Payment Amount : </label>
									<span style="font-size: 12px;" id="rci_secondaryAvailableAmount_span">${order.secondaryAvailableAmt}</span>
								</div>				
							</div>
							<div class="full-width">			
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Third Payment Method : </label>
									<span style="font-size: 12px;" id="rci_thirdPaymentMethod"></span>
								</div>
								<div class="col-sm-6">
									<label style="font-size: 13px;font-weight:bold;">Third Payment Amount : </label>
									<span style="font-size: 12px;" id="rci_thirdAvailableAmount_span">${order.thirdAvailableAmt}</span>
								</div>
							</div>
						</div>
						</section>
					</div>
				</div>
				<%--<c:if test="${isValid==true}">--%>
				<div id="rci_form_div" style="display:none">
				<div id="row">
					<div class="col-xs-12">
						<form class="form-validate form-horizontal" id="rci_invoiceRefund" method="post" action="${pageContext.request.contextPath}/Accounting/InvoiceRefund">
							<div class="form-group">
								<div class="col-xs-12">
									<label>Note</label>
									<textarea name="refundNote" class="form-control" id="rci_refundNote" rows="8" cols="80"></textarea>
								</div>
							</div>
							<div class="form-group">
								<input type="hidden" id="rci_action" name="action" value="" /> 
								<input type="hidden" id="rci_orderId" name="orderId" value="${order.id}" />
								<input type="hidden" id="rci_primaryAvailableAmt" name="primaryPaymentAmt" value="${order.primaryAvailableAmt}" />
								<input type="hidden" id="rci_secondaryAvailableAmt" name="secondaryPaymentAmt" value="${order.secondaryAvailableAmt}" />
								<input type="hidden" id="rci_thirdAvailableAmt" name="thirdPaymentAmt" value="${order.thirdAvailableAmt}" />
								<div class="col-sm-4 col-xs-6 mb-10" id="rci_refundDiv" style="display: none;">
									<label>Refund Amount 
									</label> <input type="text" id="rci_refundAmount" name="refundAmount"  class='form-control' />
								</div>
								<div class="col-sm-4 col-xs-6 mb-10" id="rci_walletCCDiv" style="display: none;">
									<label>Credit to Customer Wallet From Credit Card
									</label> <input type="text" id="rci_creditWalletAmount" name="creditWalletAmount" class='form-control' />
								</div>
								<div class="col-sm-4 col-xs-6 mb-10" id="rci_creditDiv" style="display: none;">
									<label>Credit to Customer Wallet From Wallet
									</label> <input type="text" id="rci_creditAmount" name="creditAmount" class='form-control' />
								</div>
								<div class="col-sm-4 col-xs-6 mb-10" id="rci_rewarPointDiv" style="display: none;">
									<label>Revert Reward points
									</label> <input type="text" id="rci_rewardPoints" name="rewardPoints" class='form-control' />
								</div>
								<!--<div class="col-sm-4 col-xs-6 mb-10">
									<label class="full-width">&nbsp</label>
									<button type="button" id="refundButton" class="btn btn-primary" style="margin-top: 19px;" onclick="makeRefund();">Make Refund</button>
								</div>-->
							</div>
						</form>
					</div>
				</div>
			<%--</c:if>--%>
				</div>
			
			</div>
			<div class="modal-footer full-width">
				<button type="button" id="refundButton" class="btn btn-primary" onclick="makeRefund();">Make Refund</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
	  </div>
	</div>
	</div>
<!-- End popup Refund/Credit Invoice -->

<!-- popup View Refund Details -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-refund-details">View Refund Details</button> -->

	<div id="view-refund-details" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Refund Detail - Invoice No : <span id="invoiceId_Hdr_RefDetails" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Stripe Refund Detail</li>
						</ol>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div id="vr_successDiv" class="alert alert-success fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="vr_successMsg"></span></strong>
						</div>					
						<div id="vr_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="vr_errorMsg"></span></strong>
						</div>
					</div>
				</div>
				<br />

				<div class="full-width mb-20" style="position: relative" id="cstr">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Stripe Refund Informations</label> <!--<span id="refundDetail_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel" onclick="refundDetailGridToggleFilterRow()"></span>-->
						</div>
						<div id="refundDetail_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="refundDetail_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				

				<!--<div id="refundDetail_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
								Show records with Stripe Refund <input type="text" id="refundDetail_GridSearch">
				</div>-->
			</div>
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View Refund Details -->

<!-- popup View Order Detail -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view-order-detail">View Order Detail</button> -->

	<div id="view-order-detail" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Order Detail - Invoice No : <span id="invoiceId_Hdr_OrdDetail" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">

						<ol class="breadcrumb">
							<li><i class="fa fa-laptop"></i>View Order Details</li>
						</ol>

					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div id="vo_successDiv" class="alert alert-success fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="vo_successMsg"></span></strong>
						</div>					
						<div id="vo_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
							<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
							<span id="vo_errorMsg"></span></strong>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="full-width">
						<form id="searchOrderDetails" name="searchOrderDetails" action="${pageContext.request.contextPath}/Accounting/ViewOrderDetails">
							<div class="form-group">
								<div class="col-sm-3 col-xs-6">
									<label style="font-size: 13px; font-weight: bold;">Order No.</label>
									<input type="text"  class="form-control" name="orderNo" id="vo_orderNo" value="${orderNoStr}"> 
								</div>
								<%-- <div class="col-sm-2 col-xs-12">
									<label style="font-size: 17px; font-weight: bold;">OR</label>
								</div>
								<div class="col-sm-3 col-xs-6">
									<label style="font-size: 13px; font-weight: bold;">Invoice No.</label>
									<input type="text"  class="form-control" name="invoiceId" id="invoiceId" value="${invoiceIdStr}"> 
								</div> --%><!---->
								<div class="col-sm-3  col-xs-6">
									<button type="button" class="btn btn-primary" style="margin-top: 22px;margin-left:-30%;" onclick="getInvoiceOrderDetails('',vo_orderNo)">Search</button> 
								</div>
							</div>
						</form>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-12">
						<section class="panel"> <header style="font-size: 20px;font-family: arial;" class="panel-heading">
						<b>Order Details </b></header>
						<div class="panel-body">

							<div class="form-group">
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Order No : </label>
									<span style="font-size: 12px;" id="vo_orderId">${order.id}</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Customer Name : </label> 
									<span style="font-size: 12px;" id="vo_customerName">${orderDetails.blFirstName}&nbsp;${orderDetails.blLastName}</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Customer Email : </label> 
									<span style="font-size: 12px;" id="vo_customerEmail">${orderDetails.blEmail}</span>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Created On : </label> 
									<span style="font-size: 12px;" id="vo_orderCreateDate">${order.createdDateStr}</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Shipping Method : </label> 
									<span style="font-size: 12px;" id="vo_shippingMethod">${order.shippingMethod}</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Order Total : </label>
									<span style="font-size: 12px;" id="vo_orderTotal">${order.orderTotal}</span>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Payment Method : </label>
									<span style="font-size: 12px;" id="vo_primaryPayMethod">${order.primaryPaymentMethod}</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Paid Card Amount : </label>
									<span id="vo_primaryPayAmt" style="font-size: 12px;"></span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Used Reward points : </label>
									<span id="vo_usedRewardPoints" style="font-size: 12px;"></span>
								</div>
							</div>

						</div>
						</section>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<section class="panel"> 
							<header style="font-weight:bold; font-size:11px; font-family: arial;" class="panel-heading" id="vo_eventDetailHeader"> <b>
							${order.eventName},${order.eventDateStr},${order.eventTimeStr},${order.venueName}</b> </header>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Section :</label> 
									<span style="font-size: 12px;" id="vo_section">${order.section}</span>
								</div>

								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Quantity :</label> 
									<span style="font-size: 12px;" id="vo_quantity">${order.qty}</span>
								</div>

								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Ticket Price :</label> 
									<span style="font-size: 12px;" id="vo_ticketPrice">${order.ticketPrice}</span>
								</div>

							</div>

							<div class="form-group">
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Seat Low :</label> 
									<span style="font-size: 12px;" id="vo_seatLow">${order.seatLow}</span>
								</div>

								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Seat High :</label> 
									<span style="font-size: 12px;" id="vo_seatHigh">${order.seatHigh}</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<label style="font-size: 13px; font-weight: bold;">Sold Price :</label> 
									<span style="font-size: 12px;" id="vo_soldPrice">${order.soldPrice}</span>
								</div>
							</div>

						</div>
						</section>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<form id="vo_addNotes" name="addNotes"  method="post" action="${pageContext.request.contextPath}/Accounting/ViewOrderDetails">
							<section class="full-width"> 
								<header style="font-size: 25px;font-family: arial;" class="panel-heading"> <b>Add Note:</b> </header>
									<div class="full-width mt-20">
										<div class="form-group">
											<input type="hidden" id="vo_note_action" name="action" value="addNote" />
											<input type="hidden" id="vo_note_invoiceId" name="invoiceId" value="${invoice.id}" />
											<div class="row">
												<div class="col-sm-9 col-xs-12">
												 	<textarea class="form-control full-width mb-20" name="manualNote" id="vo_manualNote" rows="5"></textarea>
												</div>
												<div class="col-sm-3 col-xs-12">
													<button type="button" class="btn btn-primary" onclick="addNote();">Add Note</button> 
												</div>
											</div>
										</div>
									</div>
								</section>
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="full-width mt-20" style="position: relative">
							<div class="table-responsive grid-table">
								<div class="grid-header full-width">
									<label>Order Details</label> <!-- <a href="javascript:orderResetFilters()" name='Reset Filters' style='float: right; margin-right: 10px;'>Reset Filters</a> -->
								</div>
								<div id="orderDetail_grid" style="width: 100%; height: 200px; border-right: 1px solid gray; border-left: 1px solid gray"></div>
								<div id="orderDetail_pager" style="width: 100%; height: 20px;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<section class="full-width mt-20"> 
							<header style="font-size: 25px;font-family: arial;" class="panel-heading" id="eventDetailDiv"> <b>Note:</b> </header>
						<div class="full-width mt-20">
							<div class="form-group">								
									<textarea class="form-control" name="auditNote" id="auditNote" readonly rows="5" cols="110" ></textarea>								
							</div>
						</div>
					</section>
				</div>
			</div>
			</div>
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		  </div>
		</div>
	</div>
<!-- End popup View Order Detail -->

<!-- popup View Dwnload Ticket Detail -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#download-ticket-detail">View Dwnload Ticket Detail</button>  -->

	<div id="download-ticket-detail" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">View Download Ticket Detail - Invoice No : <span id="invoiceId_Hdr_DowTicketDetail" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i> Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a style="font-size: 13px;font-family: arial;" href="#">Invoice</a>
							</li>
							<li style="font-size: 13px;font-family: arial;"><i class="fa fa-laptop"></i>Download Ticket Detail</li>
						</ol>
					</div>
				</div>
				<br />

				<div id="vdt_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="vdt_successMsg"></span></strong>
				</div>					
				<div id="vdt_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="vdt_errorMsg"></span></strong>
				</div>
				<br />

				<div style="position: relative" id="ctd">
					<div class="table-responsive grid-table">
						<div class="grid-header full-width">
							<label>Ticket Download Informations</label> <!--<span id="downloadTicket_grid_toogle_search" style="float: right"
								class="ui-icon ui-icon-search" title="Toggle search panel" onclick="downloadTicketGridToggleFilterRow()"></span>-->
						</div>
						<div id="downloadTicket_grid" style="width: 100%; height: 200px;border-right:1px solid gray;border-left:1px solid gray"></div>
						<div id="downloadTicket_pager" style="width: 100%; height: 10px;"></div>
					</div>
				</div>
				<br/><br/>

				<div id="downloadTicket_inlineFilterPanel" style="display: none; background: #dddddd; padding: 3px; color: black;">
								Show records with CTD <input type="text" id="downloadTicketGridSearch">
				</div>
			</div>
			<div class="modal-footer full-width">
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
		</div>

	  </div>
	</div>
<!-- End popup View Dwnload Ticket Detail -->

<!-- popup Refund/Credit Invoice -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#pay-invoice">Refund/Credit Invoice</button> -->

	<div id="pay-invoice" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content full-width">
			<div class="modal-header full-width">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pay Invoice - Invoice No : <span id="invoiceId_Hdr_PayInvoice" class="headerTextClass"></span></h4>
			</div>
			<div class="modal-body full-width">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header">
							<i class="fa fa-laptop"></i>Accounting
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="#">Invoice</a>
							</li>
							<li><i class="fa fa-laptop"></i>Pay Invoice</li>
						</ol>
					</div>
				</div>
				<br />
				<div id="pinv_successDiv" class="alert alert-success fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="pinv_successMsg"></span></strong>
				</div>					
				<div id="pinv_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
					<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
					<span id="pinv_errorMsg"></span></strong>
				</div>
				<br />
				<div class="row">
					<div class="col-xs-12">
						<section class="panel"> 
						<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Invoice Details</b></header>
							<div class="panel-body">
														
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Invoice Number : </label>
									<span style="font-size: 12px;" id="pinv_invoiceId"></span>
								</div> 
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Order No : </label>
									<span style="font-size: 12px;" id="pinv_orderId_span"></span>
								</div>
							</div>
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Order Created On : </label>
									<span style="font-size: 12px;" id="pinv_orderCreateDate"></span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Order Total : </label>
									<span style="font-size: 12px;" id="pinv_orderTotal_span"></span>
								</div>
							</div>							
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Primary Payment Method : </label>
									<span style="font-size: 12px;" id="pinv_primaryPaymentMethod"></span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Primary Payment Amount : </label>
									<span style="font-size: 12px;" id="pinv_primaryPayAmount_span"></span>
								</div>
							</div>							
							
						</div>
						</section>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<section class="panel"> 
						<header style="font-size: 20px;font-family: arial;" class="panel-heading"><b>Customer Details</b></header>
							<div class="panel-body">
							
							<div class="full-width">
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer Name : </label>
									<span style="font-size: 12px;" id="pinv_customerName"></span>
								</div>
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Customer Email : </label>
									<span style="font-size: 12px;" id="pinv_customerEmail"></span>
								</div>
							</div>							
							<div class="full-width">		
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Active Reward Points : </label>
									<span style="font-size: 12px;" id="pinv_activeRewardPoints_span"></span>
								</div> 
								<div class="col-sm-6 col-xs-12">
									<label style="font-size: 13px;font-weight:bold;">Wallet Credit : </label>
									<span style="font-size: 12px;" id="pinv_walletCredit_span"></span>
								</div>
							</div>
							
						</div>
						</section>
					</div>
				</div>
								
				<div id="row">
					<div class="col-xs-12">
						<form class="form-validate form-horizontal" id="pinv_form" method="post" action="${pageContext.request.contextPath}/Accounting/InvoiceRefund">
							<div class="form-group">
								<div class="col-xs-12">
									<label>Note</label>
									<textarea name="pinv_Note" class="form-control" id="pinv_Note" rows="8" cols="80"></textarea>
								</div>
							</div>
							<div class="form-group">
								<input type="hidden" id="pinv_action" name="pinv_action" value="" /> 
								<input type="hidden" id="pinv_orderId" name="pinv_orderId" />
								<input type="hidden" name="pinv_orderTotal" id="pinv_orderTotal" />
								<input type="hidden" name="pinv_activeRewardPoints" id="pinv_activeRewardPoints" />
								<input type="hidden" name="pinv_walletCredit" id="pinv_walletCredit" />
								<input type="hidden" name="customerId" id="customerId" />
								<input type="hidden" name="ticketId" id="ticketId" />
								<input type="hidden" name="eventId" id="payInvoiceEventId" />
								<div class="col-sm-4">
									<label style="font-size: 13px;font-weight:bold;">Payment Method : </label>
									<select id="paymentMethod" name="paymentMethod" class="form-control input-sm m-bot15" onchange="changePaymentMethod()">
									  <option value="CREDITCARD">Credit Card</option>
										<option value="REWARDPOINTS">Reward Points</option>
										<option value="WALLET">Wallet</option>
										<option value="BANK_OR_CHEQUE">Bank Check</option>
										<option value="WALLET_CREDITCARD">Wallet + Credit Card</option>
										<option value="PARTIAL_REWARD_CC">Reward Points + Credit Card</option>
										<option value="PARTIAL_REWARD_WALLET">Reward Points  + Wallet</option>
										<option value="PARTIAL_REWARD_WALLET_CC">Reward Points  + Wallet + Credit Card</option>
										<option value="BANK_OR_CHEQUE_CC">Bank Check + Credit Card</option>
										<option value="BANK_OR_CHEQUE_WALLET">Bank Check + Wallet</option>
										<option value="BANK_OR_CHEQUE_PARTIAL_REWARD">Bank Check + Reward Points</option>
									</select>
								</div>								
							</div>
							<div class="form-group" id="creditCardDiv">
								<div class="col-sm-3" class="cardInfo">
									<label style="font-size: 13px;font-weight:bold;">Available Cards : </label>
									<select id="custCards" name="custCards" class="form-control input-sm m-bot15" onChange="custChangeSavedCard()">
										<option value="0">Get New Card</option>
									</select>
									<input type="hidden" id="sTempToken" name = "sTempToken"/>
									<input type="hidden" id="sBySavedCard" name = "sBySavedCard"/>
									<input type="hidden" id="sSavedCardId" name = "sSavedCardId"/>
								</div>
								
								<div class="col-sm-3" class="cardInfo">
									<label style="font-size: 13px;font-weight:bold;">Card Number :</label>
									<input type="text" class="form-control" id="cardNumber" name="cardNumber" data-stripe="number">
								</div>
								
								<div class="col-sm-3" class="cardInfo">
									<label style="font-size: 13px;font-weight:bold;">Expiry Month :</label>
									<select id="expMonth" name="expMonth" class="form-control input-sm m-bot15">
										<c:forEach begin="01" end="12" var="expirtMonth">
											<option value="${expirtMonth}">${expirtMonth}</option>
										</c:forEach> 
									</select>
								</div>
								
								<div class="col-sm-3" class="cardInfo">
									<label style="font-size: 13px;font-weight:bold;">Expiry Year :</label>
									<c:set value="true" var="yearFlag"></c:set>
									<c:set value="${currentYear + 1 }" var="currentYearInc"></c:set>
									<select id="expYear" name="expYear" class="form-control input-sm m-bot15">
									  <c:forEach begin="1" end="20">
									  
										<c:choose>
											<c:when test="${yearFlag == true}">
												<option value="${currentYear}">${currentYear}</option>
												<c:set value="false" var="yearFlag"></c:set>
											</c:when>
											<c:otherwise>
												<option value="${currentYearInc}">
													${currentYearInc }
													<c:set value="${currentYearInc + 1 }" var="currentYearInc"></c:set>
												</option>
											</c:otherwise>
										</c:choose>
									  </c:forEach>
									</select>
								</div>
								<div class="col-sm-3" class="cardInfo">
									<label style="font-size: 13px;font-weight:bold;">Name in the card :</label>
									<input type="text" class="form-control" id="name" name="name" data-stripe="name">
								</div>
								<div class="col-sm-3" class="cardInfo">
									<label style="font-size: 13px;font-weight:bold;">Cvv :</label>
									<input type="text" class="form-control" id="cvv" name="cvv" data-stripe="cvc">					
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3" id="cardAmountDiv" style="display:none">
									<label style="font-size: 13px;font-weight:bold;">Card Amount :</label>
									<input type="text" class="form-control" id="cardAmount" name="cardAmount" onchange="calRewardPoints()">
								</div>
								<div class="col-sm-3" id="rewardPointsDiv" style="display:none">
									<label style="font-size: 13px;font-weight:bold;">Reward Points :</label>
									<input type="text" class="form-control" id="rewardPoints" name="rewardPoints" >					
								</div>
								<div class="col-sm-3" id="walletAmountDiv" style="display:none">
									<label style="font-size: 13px;font-weight:bold;">Wallet Credit Amount :</label>
									<input type="text" class="form-control" id="walletAmount" name="walletAmount" onchange="calRewardPoints()">					
								</div>
								<div class="col-sm-3" id="bankChequeAmtDiv" style="display:none">
									<label style="font-size: 13px;font-weight:bold;">Bank Check Amount :</label>
									<input type="text" class="form-control" id="bankChequeAmt" name="bankChequeAmt" >					
								</div>
								<div class="col-sm-3" id="bankChequeDiv" style="display:none">
									<label style="font-size: 13px;font-weight:bold;">Bank Transaction Id/Check No. :</label>
									<input type="text" class="form-control" id="bankChequeId" name="bankChequeId" >					
								</div>
								<input type="hidden" size="12" id="stripeExp"  data-stripe="exp">
								<input type="hidden"  id="cvvMaxDigit" name="cvvMaxDigit" /> 				
							</div>
						</form>
					</div>
				</div>
			
			</div>
						
			<div class="modal-footer full-width">
				<button type="button" class="btn btn-primary" onclick="toPayInvoice();">Pay Invoice</button>
				<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			</div>
	  </div>
	</div>
</div>
<!-- End popup Refund/Credit Invoice -->


<!-- popup Create Fedex Label -->
	<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#create-fedex-label">Create Fedex Label</button> -->

	<div id="create-fedex-label" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content full-width">
				<div class="modal-header full-width">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Create Fedex Label - Invoice No : <span id="invoiceId_Hdr_CreateFedex" class="headerTextClass"></span></h4>
				</div>
				<div class="modal-body full-width">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="page-header">
								<i class="fa fa-laptop"></i>Accounting
							</h3>
							<ol class="breadcrumb">
								<li><i class="fa fa-home"></i><a href="#">Invoice</a>
								</li>
								<li><i class="fa fa-laptop"></i>Order Shipping Address</li>
							</ol>
						</div>
					</div>
					<br />
					<div id="cf_successDiv" class="alert alert-success fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="cf_successMsg"></span></strong>
					</div>					
					<div id="cf_errorDiv" class="alert alert-block alert-danger fade in" style="display:none;">
						<strong style="font-family: arial, helvetica; font-size: 17px; display: block; text-align: center;">
						<span id="cf_errorMsg"></span></strong>
					</div>
					<br />
					
					<div id="row">
						<div class="full-width">
							<form class="form-validate" id="cf_orderShippingAddress" onsubmit="return false">
								<div class="tab-fields full-width">
									<input type="hidden" id="cf_action" name="action" value="" /> 
									<input type="hidden" id="cf_orderId" name="orderId" value="${order.orderId}" />
									<div class="form-group col-sm-4 col-xs-6">
										<label>First Name <span class="required">*</span></label>
										<input type="text" id="cf_firstName" name="firstName" value="${order.shFirstName}" class='form-control' />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Last Name <span class="required">*</span></label>
										<input type="text" id="cf_lastName" name="lastName" value="${order.shLastName}" class='form-control' />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Company Name <span class="required">*</span></label> 
										<input type="text" id="cf_companyName" name="companyName" class='form-control' />
									</div>
								
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street-1 </label> 
										<input type="text" id="cf_street1" name="street1" value="${order.shAddress1}" class='form-control' />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Street-2 </label> 
										<input type="text" id="cf_street2" name="street2" value="${order.shAddress2}" class='form-control' />
									</div>
									
									<div class="form-group col-sm-4 col-xs-6">
										<label>Zipcode <span class="required">*</span></label> 
										<input type="text" id="cf_zipcode" name="zipcode" value="${order.shZipCode}" class='form-control' onblur="getCityStateCountry(this.value, 'body-manage-invoice-fedex')" />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>City <span class="required">*</span></label>
										<input type="text" id="cf_city" name="city" value="${order.shCity}" class='form-control' />
									</div>
									
									<div class="form-group col-sm-4 col-xs-6">
										<label>Country <span class="required">*</span></label> 
										<select id="cf_country" name="country" class="form-control" onchange="loadState(cf_country,cf_state,'')">
											<option value='-1'>--select--</option>
											<%--<c:forEach var="country" items="${countryList}">
												<option value="${country.id}" <c:if test="${order.shCountryName == country.name}"> selected </c:if>>
													<c:out value="${country.name}" />
												</option>											
											</c:forEach>--%><!---->
										</select>
									</div>
								
									<div class="form-group col-sm-4 col-xs-6">
										<label>State <span class="required">*</span></label> 
										<select id="cf_state" name="state" class="form-control" onchange="">
											<option value='-1'>--select--</option>
											 <%--<c:forEach var="state" items="${stateList}">
												<option value="${state.id}" <c:if test="${order.shStateName == state.name}"> selected </c:if>>
													<c:out value="${state.name}" />
												</option>
											</c:forEach>--%><!---->
										</select>
									</div>
									
								
									<div class="form-group col-sm-4 col-xs-6">
										<label>Email <span class="required">*</span></label> 
										<input type="text" id="cf_email" name="email" value="${order.shEmail}" class='form-control' />
									</div>
									<div class="form-group col-sm-4 col-xs-6">
										<label>Phone <span class="required">*</span></label> 
										<input type="text" id="cf_phone" name="phone" value="${order.shPhone1}" class='form-control' />
									</div>
									
									<%--<c:if test="${not empty invoiceId}">--%>
									<div id="cf_fedex_div" style="display:none">
										<div class="form-group col-sm-4 col-xs-6">
											<label>Service Type <span class="required">*</span></label>  
											<select id="cf_serviceType" name="serviceType" class="form-control" onchange="">
												<option value='select'>--select--</option>
												<option value="FEDEX_EXPRESS_SAVER"> FEDEX EXPRESS SAVER </option>						
												<option value="FIRST_OVERNIGHT"> FIRST OVERNIGHT </option>
												<option value="PRIORITY_OVERNIGHT"> PRIORITY OVERNIGHT </option>
												<option value="STANDARD_OVERNIGHT"> STANDARD OVERNIGHT </option>
												<option value="FEDEX_2_DAY"> FEDEX 2-DAY </option>
												<option value="GROUND_HOME_DELIVERY"> GROUND HOME DELIVERY </option>
												<option value="INTERNATIONAL_PRIORITY"> INTERNATIONAL PRIORITY </option>
											</select>
										</div>
										<div class="form-group col-sm-4 col-xs-6">
											<label>Signature Type <span class="required">*</span></label>  
											<select id="cf_signatureType" name="signatureType" class="form-control" onchange="">
												<option value="NO_SIGNATURE_REQUIRED">NO SIGNATURE REQUIRED</option>
												<option value="SERVICE_DEFAULT">SERVICE DEFAULT</option>
												<option value="INDIRECT">INDIRECT</option>
												<option value="DIRECT">DIRECT</option>
												<option value="ADULT">ADULT</option>
											</select>
										</div>
									</div>
									<%--</c:if>--%>
								<input type="hidden" id="cf_invoiceId" name="invoiceId" value="${invoiceId}" />
								</div>
							</form>
						</div>
					</div>
						
					</div>
						
							<div class="modal-footer full-width">
								<div id="cf_btnDiv" style="display:none">
									<button type="button" onclick="updateAddress('SAVE');"  class="btn btn-primary">Update Address</button>
								</div>							
								<div id="cf_btnDiv1" style="display:none">
									<button type="button" onclick="updateAddress('CREATE');"  class="btn btn-primary">Save and Create Label</button>
								</div>
								<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
						
				</div>
			</div>
		</div>
	</div>
<!-- End popup Create Fedex Label -->


<script>
	var pagingInfo;
	var eventDataView;
	var eventGrid;
	var eventData = [];
	var eventColumns =[];
	var invoiceGridSearchString='';
	var sortingString='';
	var columnFilters = {};
	var userInvoiceColumnsStr = '<%=session.getAttribute("invoicegrid")%>';
	var userInvoiceColumns = [];
	var loadInvoiceColumns = ["invId", "customerName", "companyName", "ticketCount", "invoiceAged", "invoiceTotal", 
					"eventName", "eventDate", "eventTime", "productType", "trackingNo", "view", "createdBy", "brokerId", "loyalFan"];
	var allInvoiceColumns = [ {
		id : "invId",
		name : "Invoice Id",
		field : "invId",
		width : 80,
		sortable : true
	}, {
		id : "customerId",
		name : "Customer Id",
		field : "customerId",
		width : 80,
		sortable : true
	}, {
		id : "customerName",
		name : "Customer Name",
		field : "customerName",
		width : 80,
		sortable : true
	}, {
		id : "username",
		name : "Username",
		field : "username",
		width : 80,
		sortable : true
	}, {
		id : "ticketCount",
		name : "Ticket Qty",
		field : "ticketCount",
		width : 80,
		sortable : true
	}, {
		id : "invoiceTotal",
		name : "Invoice Total",
		field : "invoiceTotal",
		width : 80,
		sortable : true
	}, {
		id : "invoiceAged",
		name : "Aged",
		field : "invoiceAged",
		width : 80,
		sortable : true
	},{
		id : "createdDate",
		name : "Created Date",
		field : "createdDate",
		width : 80,
		sortable : true
	}, {
		id : "createdBy",
		name : "Created By",
		field : "createdBy",
		width : 80,
		sortable : true
	}, {
		id : "csr",
		name : "CSR",
		field : "csr",
		width : 80,
		sortable : true
	}, {
		id : "orderId",
		name : "Order Id",
		field : "orderId",
		width : 80,
		sortable : true
	}, {
		id : "shippingMethod",
		name : "Shipping Method",
		field : "shippingMethod",
		width : 80,
		sortable : true
	}, {
		id : "customerType",
		name : "Customer Type",
		field : "customerType",
		width : 80,
		sortable : true
	}, {
		id : "companyName",
		name : "Company Name",
		field : "companyName",
		width : 80,
		sortable : true
	}, {
		id : "productType",
		name : "Product Type",
		field : "productType",
		width : 80,
		sortable : true
	}, {
		id : "street1",
		name : "Address Line",
		field : "street1",
		width : 80,
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		width : 80,
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		width : 80,
		sortable : true
	}, {
		id : "country",
		name : "Country",
		field : "country",
		width : 80,
		sortable : true
	}, {
		id : "zipCode",
		name : "zipCode",
		field : "zipCode",
		width : 80,
		sortable : true
	}, {
		id : "phone",
		name : "Phone",
		field : "phone",
		width : 100,
		sortable : true
	}, {
		id : "secondaryOrderId",
		name : "Secondary Order Id",
		field : "secondaryOrderId",
		width : 100,
		sortable : true
	}, {
		id : "secondaryOrderType",
		name : "Secondary Order Type",
		field : "secondaryOrderType",
		width : 100,
		sortable : true
	}, {
		id : "voidedDate",
		name : "Voided Date",
		field : "voidedDate",
		width : 100,
		sortable : true
	},{
		id : "lastUpdatedDate",
		name : "LastUpdated Date",
		field : "lastUpdatedDate",
		width : 100,
		sortable : true
	}, {
		id : "lastUpdatedBy",
		name : "LastUpdatedBy",
		field : "lastUpdatedBy",
		width : 100,
		sortable : true
	}, {
		id : "status",
		name : "status",
		field : "status",
		width : 100,
		sortable : true
	}, {
		id : "trackingNo",
		name : "Tracking No",
		field : "trackingNo",
		width : 100,
		sortable : true,
		formatter : trackOrderFormatter
	}, {
		id : "view",
		name : "Fedex Label",
		field : "view",
		width : 100,
		sortable : true,
		formatter : viewLinkFormatter
	}, {
		id : "eventId",
		name : "Event Id",
		field : "eventId",
		width : 100,
		sortable : true
	}, {
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		width : 100,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width : 100,
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width : 100,
		sortable : true
	}, {
		id : "brokerId",
		name : "Broker Id",
		field : "brokerId",
		width : 100,
		sortable : true
	}, {
		id : "loyalFan",
		name : "Loyal Fan Order",
		field : "loyalFan",
		width : 100,
		sortable : true
	}, {
		id : "platform",
		name : "Platform",
		field : "platform",
		width : 100,
		sortable : true
	}, {
		id : "primaryPaymentMethod",
		name : "Primary Payment",
		field : "primaryPaymentMethod",
		width : 100,
		sortable : true
	}, {
		id : "secondaryPaymentMethod",
		name : "Secondary Payment",
		field : "secondaryPaymentMethod",
		width : 100,
		sortable : true
	}, {
		id : "thirdPaymentMethod",
		name : "Third Payment",
		field : "thirdPaymentMethod",
		width : 100,
		sortable : true
	}, {
		id : "isPOMapped",
		name : "PO",
		field : "isPOMapped",
		width : 100,
		sortable : true
	} ];

	if (userInvoiceColumnsStr != 'null' && userInvoiceColumnsStr != '') {
		var columnOrder = userInvoiceColumnsStr.split(',');
		var columnWidth = [];
		for ( var i = 0; i < columnOrder.length; i++) {
			columnWidth = columnOrder[i].split(":");
			for ( var j = 0; j < allInvoiceColumns.length; j++) {
				if (columnWidth[0] == allInvoiceColumns[j].id) {
					userInvoiceColumns[i] = allInvoiceColumns[j];
					userInvoiceColumns[i].width = (columnWidth[1] - 5);
					break;
				}
			}

		}
	} else {
		var columnOrder = loadInvoiceColumns;
		var columnWidth;
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i];
			for(var j=0;j<allInvoiceColumns.length;j++){
				if(columnWidth == allInvoiceColumns[j].id){
					userInvoiceColumns[i] = allInvoiceColumns[j];
					userInvoiceColumns[i].width=80;
					break;
				}
			}			
		}
		//userInvoiceColumns = allInvoiceColumns;
	}

	var openOrderOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var openOrderGridSortcol = "invId";
	var eventGridSortdir = 1;

	var percentCompleteThreshold = 0;
	var eventGridSearchString = "";

	function viewLinkFormatter(row, cell, value, columnDef, dataContext) {
		//var trackingNo = eventGrid.getDataItem(row).trackingNo;
		var fedexLabelCreated = eventGrid.getDataItem(row).view;
		var link = '';
		//if (trackingNo != null && trackingNo != '') {
		if(fedexLabelCreated == 'Yes'){
			link = "<img class='editClickableImage' style='height:17px;' src='../resources/images/viewPdf.png' onclick='getFedexLabel("
					+ eventGrid.getDataItem(row).invId + ")'/>";
		}
		return link;
	}

	function trackOrderFormatter(row, cell, value, columnDef, dataContext) {
		var trackingNo = eventGrid.getDataItem(row).trackingNo;
		var link = '';
		if (trackingNo != null && trackingNo != '') {
			link = "<a href='javascript:trackMyOrder(" + trackingNo + ")'><u>"
					+ trackingNo + "</u></a>";
		}
		return link;
	}

	function deleteRecordFromEventGrid(id) {
		eventDataView.deleteItem(id);
		eventGrid.invalidate();
	}
	/*
	function eventGridFilter(item, args) {
		var x = item["invId"];
		if (args.eventGridSearchString != ""
				&& x.indexOf(args.eventGridSearchString) == -1) {

			if (typeof x === 'string' || x instanceof String) {
				if (x.toLowerCase().indexOf(
						args.eventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function eventGridComparer(a, b) {
		var x = a[openOrderGridSortcol], y = b[openOrderGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if (x == '' || x == null) {
			return 1;
		} else if (y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String)
				&& (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));
		}
	}
	/*
	function openOrderGridToggleFilterRow() {
		eventGrid.setTopPanelVisibility(!eventGrid.getOptions().showTopPanel);
	}

	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#openOrder_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	*/
	function pagingControl(move,id){
		if(id == 'openOrder_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(pagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(pagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(pagingInfo.pageNum)-1;
			}
			getInvoiceGridData(pageNo);
		}
		if(id=='custArtist_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custArtistPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custArtistPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custArtistPagingInfo.pageNum)-1;
			}
			getArtistGridData(pageNo);
		}
		if(id=='custEvent_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custEventPagingInfo.pageNum)-1;
			}
			getCustEventsGridData(pageNo);
		}
		if(id=='custRewardPoints_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custRewardPointsPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custRewardPointsPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custRewardPointsPagingInfo.pageNum)-1;
			}	
			getRewardPointsGridData(pageNo);
		}
		if(id == "custInvoiceTicket_pager"){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum)-1;
			}
		}
	}
	
	function getInvoiceGridData(pageNo) {
		var invoiceNo = $('#invoiceNo').val(); 
		if(invoiceNo != null && invoiceNo != ""){			
			$('#invoiceNo').val('');			
			$("#action").val('search');
		}
		if($('#outstanding').hasClass('active')){
			$('#status').val('Outstanding');
		}else if($('#void').hasClass('active')){
			$('#status').val('Voided');
		}else if($('#completed').hasClass('active')){
			$('#status').val('Completed');
		}else if($('#disputes').hasClass('active')){
			$('#status').val('Disputed');
		}else{
			$('#status').val('');
		}
		$("#action").val('search');
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/Invoices.json",
			type : "post",
			dataType: "json",
			data : $("#invoiceSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+invoiceGridSearchString+"&sortingString="+sortingString,
			success : function(res){
				var jsonData = res;
				/* if(jsonData==null || jsonData=='') {
					jAlert("No Data Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				pagingInfo = jsonData.pagingInfo;
				refreshInvoiceGridValues(jsonData.openOrders);				
				clearAllSelections();
				$('#openOrder_pager> div').append("<span class='slick-pager-status'> <b>Invoice Grand Total : <span id='invoiceTotal'>"+jsonData.invoiceTotal+"</span></b></span>");
				$('#openOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserInvoicePreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	function refreshInvoiceGridValues(jsonData) {
		eventData=[];
		var prodType;
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (eventData[i] = {});
				prodType = data.productType;
				d["id"] = i;
				d["invId"] = data.invoiceId;
				d["customerId"] = data.customerId;
				d["customerName"] = data.customerName;
				d["username"] = data.username;
				d["customerType"] = data.customerType;
				d["companyName"] = data.custCompanyName;
				d["productType"] = data.productType;
				d["ticketCount"] = data.ticketCount;
				d["invoiceTotal"] = data.invoiceTotal;
				d["street1"] = data.addressLine1;
				d["country"] = data.country;
				d["state"] = data.state;
				d["city"] = data.city;
				d["zipCode"] = data.zipCode;
				d["platform"] = data.platform;
				d["phone"] = data.phone;
				d["invoiceAged"] = data.invoiceAged;
				d["createdDate"] = data.createdDateStr;
				d["createdBy"] = data.createdBy;
				d["csr"] = data.csr;
				d["secondaryOrderType"] = data.secondaryOrderType;
				d["secondaryOrderId"] = data.secondaryOrderId;
				d["voidedDate"] = data.voidedDateStr;
				d["emailSent"] = data.isInvoiceEmailSent;
				d["purchaseOrderNo"] = data.purchaseOrderNo;
				d["transactionId"] = "";
				d["externalInvoiceNo"] = "";
				d["eTicketAttached"] = "";
				d["shippingMethod"] = data.shippingMethod;
				d["trackingNo"] = data.trackingNo;
				d["instantDownload"] = "";
				d["internalNotes"] = "";
				d["externalNotes"] = "";
				d["invoiceType"] = "";
				d["externalPoNo"] = "";
				d["externalInvoiceNo"] = "";
				d["orderId"] = data.customerOrderId;
				d["auto"] = "";
				d["source"] = "";
				d["lastUpdatedDate"] = data.lastUpdatedDateStr;
				d["lastUpdatedBy"] = data.lastUpdatedBy;
				d["status"] = data.status;
				d["brokerId"] = data.brokerId;
				d["loyalFan"] = data.loyalFanOrder;
				d["view"] = data.fedexLabelCreated;
				d["eventId"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["primaryPaymentMethod"] = data.primaryPaymentMethod;
				d["secondaryPaymentMethod"] = data.secondaryPaymentMethod;
				d["thirdPaymentMethod"] = data.thirdPaymentMethod;
				d["isPOMapped"] = data.isPoMapped;
			}
		}
			
		if(prodType == "RTW" || prodType == "RTW2")
			$("#actionButton").hide();

		eventDataView = new Slick.Data.DataView();
		eventGrid = new Slick.Grid("#openOrders_grid", eventDataView,
				userInvoiceColumns, openOrderOptions);
		eventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		eventGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(pagingInfo!=null){
			var eventGridPager = new Slick.Controls.Pager(eventDataView, eventGrid,
					$("#openOrder_pager"),pagingInfo);
		}
		
		var eventGridColumnpicker = new Slick.Controls.ColumnPicker(
				allInvoiceColumns, eventGrid, openOrderOptions);

		// move the filter panel defined in a hidden div into eventGrid top panel
		//$("#event_inlineFilterPanel").appendTo(eventGrid.getTopPanel()).show();

		eventGrid.onContextMenu.subscribe(function(e) {
			e.preventDefault();
			var cell = eventGrid.getCellFromEvent(e);
			eventGrid.setSelectedRows([ cell.row ]);
			var row = eventGrid.getSelectedRows([ 0 ])[0];			
			var productType = eventGrid.getDataItem(row).productType;
			var height = screen.height - e.pageY;
			var width = screen.width - e.pageX;
			if((productType == "REWARDTHEFAN") || (productType == "SEATGEEK")){
				if (height < ($("#contextMenu").height() + 100)) {
					height = e.pageY - $("#contextMenu").height();
				} else {
					height = e.pageY;
				}
				if (width < $("#contextMenu").width()) {
					width = e.pageX - $("#contextMenu").width();
				} else {
					width = e.pageX;
				}
				$("#contextMenu").data("row", cell.row).css("top", height).css(
						"left", width).show();
				$("body").one("click", function() {
					$("#contextMenu").hide();
				});
			}else if((productType == "RTW") || (productType == "RTW2")){
				$("#contextMenuOther").data("row", cell.row).css("top", e.pageY).css(
						"left", e.pageX).show();
				$("body").one("click", function() {
					$("#contextMenuOther").hide();
				});
			}
		});

		eventGrid.onSort.subscribe(function(e, args) {
			openOrderGridSortcol = args.sortCol.field;			
			if(sortingString.indexOf(openOrderGridSortcol) < 0){
				eventGridSortdir = 'ASC';
			}else{
				if(eventGridSortdir == 'DESC' ){
					eventGridSortdir = 'ASC';
				}else{
					eventGridSortdir = 'DESC';
				}
			}
			sortingString = '';
			sortingString +=',SORTINGCOLUMN:'+openOrderGridSortcol+',SORTINGORDER:'+eventGridSortdir+',';
			getInvoiceGridData(0)
		});		
		
		// wire up model events to drive the eventGrid
		eventDataView.onRowCountChanged.subscribe(function(e, args) {
			eventGrid.updateRowCount();
			eventGrid.render();
		});
		eventDataView.onRowsChanged.subscribe(function(e, args) {
			eventGrid.invalidateRows(args.rows);
			eventGrid.render();
		});
		$(eventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	invoiceGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in columnFilters) {
					  if (columnId !== undefined && columnFilters[columnId] !== "") {
						 invoiceGridSearchString += columnId + ":" +columnFilters[columnId]+",";
					  }
					}
					getInvoiceGridData(0);
				}
			  }
		 
		});
		eventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'productType'){
					if(args.column.id == 'eventTime'){
						$("<input type='text' placeholder='hh:mm a'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else if(args.column.id == 'createdDate' || args.column.id == 'voidedDate' || args.column.id == 'lastUpdatedDate' || args.column.id == 'eventDate'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					} 
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}
		});
		eventGrid.init();
		/* wire up the search textbox to apply the filter to the model
		$("#openOrderGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			eventGridSearchString = this.value;
			updateEventGridFilter();
		});
		function updateEventGridFilter() {
			eventDataView.setFilterArgs({
				eventGridSearchString : eventGridSearchString
			});
			eventDataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		eventDataView.beginUpdate();
		eventDataView.setItems(eventData);
		/*
		eventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			eventGridSearchString : eventGridSearchString
		});
		eventDataView.setFilter(eventGridFilter);*/
		eventDataView.endUpdate();
		eventDataView.syncGridSelection(eventGrid, true);
		$("#gridContainer").resizable();
		eventGrid.resizeCanvas();
		var eventrowIndex;
		eventGrid.onSelectedRowsChanged.subscribe(function() {
			var temprEventRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				//getCustomerInfoForInvoice(temprEventRowIndex);
			}
		});
		$("div#divLoading").removeClass('show');
	}

	function uploadTicket() {
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice to upload ticket");
		} else {
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			$("#invId").val(eventGrid.getDataItem(customerRowIndex).invId);
			$("#tixUploadAction").val('uploadTicket');
			$("#tixUploadForm").submit();
		}
	}
	
	$("#contextMenuOther").click(function(e){
		if (!$(e.target).is("li")) {
			return;
		}
		var row = eventGrid.getSelectedRows([ 0 ])[0];
		if (row < 0) {
			jAlert("Please select Invoice.");
			return;
		}
		var productType = eventGrid.getDataItem(row).productType;
		if ($(e.target).attr("data") == 'view customer details') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var customerId = eventGrid.getDataItem(row).customerId;
			if(customerId >0){
				$('#idHdr_CustomerDetails').text("Invoice No : "+invoiceId);
				getCustomerInfoForEdit(customerId);
				/*var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="
					+ customerId;
				popupCenter(url, "View Customer", "800", "800");*/
			}else{
				jAlert("Customer Information is not found for selected invoice.");
			}
			
		}else if ($(e.target).attr("data") == 'view payment history') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getPaymentHistory(invoiceId, productType);
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewPaymentHistory?invoiceId="
					+ invoiceId+"&productType="+productType;
			popupCenter(url, "Payment History", "1000", "500");*/
		}else if ($(e.target).attr("data") == 'view related po') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getRelatedPOInvoices(invoiceId, productType);
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewRelatedPOInvoices?invoiceId="
					+ invoiceId+"&productType="+productType;
			popupCenter(url, "View Related PO(s)", "1000",
					"700");*/
		}else if ($(e.target).attr("data") == 'pdf') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			generateRTWRTW2InvoicePdf(invoiceId,productType,'attachment');
		} else if ($(e.target).attr("data") == 'print invoice') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			generateRTWRTW2InvoicePdf(invoiceId,productType,'inline');
		}
	});
	
	$("#contextMenu").click(function(e) {
		if (!$(e.target).is("li")) {
			return;
		}
		var row = eventGrid.getSelectedRows([ 0 ])[0];
		if (row < 0) {
			jAlert("Please select Invoice.");
			return;
		}
		var productType = eventGrid.getDataItem(row).productType;
		if ($(e.target).attr("data") == 'edit invoice') {
			createRealTicket();
		} else if ($(e.target).attr("data") == 'pdf') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			generateInvoicePdf(invoiceId, 'attachment');
		} else if ($(e.target).attr("data") == 'print invoice') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			generateInvoicePdf(invoiceId, 'inline');
		} else if ($(e.target).attr("data") == 'mark invoice void') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			updateInvoice(invoiceId, 'VOIDED');
		} else if ($(e.target).attr("data") == 'mark invoice paid') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			updateInvoice(invoiceId, 'COMPLETED');
		} else if ($(e.target).attr("data") == 'mark invoice outstanding') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var status = eventGrid.getDataItem(row).status;
			if(status != 'Completed'){
				jAlert("Selected invoice is not completed yet, could not change status.");
			}else{			
				updateInvoice(invoiceId, 'OUTSTANDING');
			}
		}else if ($(e.target).attr("data") == 'view customer details') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var customerId = eventGrid.getDataItem(row).customerId;
			$('#idHdr_CustomerDetails').text("Invoice No : "+invoiceId);
			getCustomerInfoForEdit(customerId);			
			/*var url = "${pageContext.request.contextPath}/Client/ViewCustomer?custId="
					+ customerId;
			popupCenter(url, "View Customer", "800", "800");*/
		} else if ($(e.target).attr("data") == 'edit shipping address') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var orderId = eventGrid.getDataItem(row).orderId;
			getCustomerOrderShippingAddress(invoiceId, orderId);
			/*var url = "${pageContext.request.contextPath}/Accounting/EditShippingAddress?orderId="
					+ orderId;
			popupCenter(url, "Edit Shipping Address", "800",
					"800");*/
			
		}  else if ($(e.target).attr("data") == 'edit billing address') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var orderId = eventGrid.getDataItem(row).orderId;
			getCustomerOrderBillingAddress(invoiceId, orderId);
			/*var url = "${pageContext.request.contextPath}/Accounting/EditBillingAddress?orderId="
					+ orderId;
			popupCenter(url, "Edit Billing Address", "800",
					"800");*/
		} else if ($(e.target).attr("data") == 'upload tickets') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var url = "${pageContext.request.contextPath}/Accounting/UploadTickets?invoiceId="
					+ invoiceId;
			popupCenter(url, "Upload Tickets", "1000", "500");
		} else if ($(e.target).attr("data") == 'view payment history') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getPaymentHistory(invoiceId, productType);
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewPaymentHistory?invoiceId="
					+ invoiceId+"&productType="+productType;
			popupCenter(url, "Payment History", "800", "500");*/
		} else if ($(e.target).attr("data") == 'view related po') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getRelatedPOInvoices(invoiceId, productType);
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewRelatedPOInvoices?invoiceId="
					+ invoiceId+"&productType="+productType;
			popupCenter(url, "View Related PO(s)", "1000",
					"700");*/
		} else if ($(e.target).attr("data") == 'edit invoice csr') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getInvoiceCSR(invoiceId);
			/*var url = "${pageContext.request.contextPath}/Accounting/EditInvoiceCsr?invoiceId="
					+ invoiceId;
			popupCenter(url, "Edit Invoice CSR", "500", "400");*/
		} else if ($(e.target).attr("data") == 'view modify notes') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getInvoiceNote(invoiceId);			
			/*var url = "${pageContext.request.contextPath}/Accounting/EditInvoiceNote?invoiceId="
					+ invoiceId;
			popupCenter(url, "Edit Invoice Note", "500", "400");*/
		}else if ($(e.target).attr("data") == 'view download ticket details') {
			viewDownloadTicketDetail();
		}else if ($(e.target).attr("data") == 'view refund details') {
			viewRefundTicketDetail();
		}else if ($(e.target).attr("data") == 'View Order Details') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			getInvoiceOrderDetails(invoiceId,'');
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewOrderDetails?invoiceId="
					+ invoiceId;
			popupCenter(url, "Edit Invoice Note", "1000", "900");*/
		}else if ($(e.target).attr("data") == 'refund invoice') {
			var invoiceId = eventGrid.getDataItem(row).invId;
			var orderId = eventGrid.getDataItem(row).orderId;
			getInvoiceRefund(invoiceId, orderId);
			/*var url = "${pageContext.request.contextPath}/Accounting/InvoiceRefund?orderId="+ orderId;
			popupCenter(url, "Invoice Refund", "800","800");*/
		}else if($(e.target).attr("data") == 'Pay Invoice'){
			var invoiceId = eventGrid.getDataItem(row).invId;
			var orderId = eventGrid.getDataItem(row).orderId;
			var status = eventGrid.getDataItem(row).status;
			if(status != 'Voided' && status != 'Disputed'){
				getPayInvoice(invoiceId, orderId);
			}else{
				jAlert("Selected invoice is already Voided/Disputed.");
			}
		}else if($(e.target).attr("data") == 'Send Invoice PDF'){
			var invoiceId = eventGrid.getDataItem(row).invId;
			var status = eventGrid.getDataItem(row).status;
			if(status != 'Voided' && status != 'Disputed'){
				generateInvoicePDFAndSendMail(invoiceId);
			}else{
				jAlert("Selected invoice is Voided/Disputed can't send Invoice.");
			}
		}
		
	});

	//Start - View Modify Notes Script
	function getInvoiceNote(invoiceId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetInvoiceNote",
			type : "post",
			data : "invoiceId="+invoiceId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					viewModifyNotesFormUpdate(jsonData);
					$('#invoiceId_Hdr_ModNotes').text(invoiceId);
				}/*else{
					jAlert(jsonData.msg);
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function viewModifyNotesFormUpdate(jsonData){
		$('#modifyNotesInvoiceId').val(jsonData.id);
		$('#modifyNotesInvoiceNote').val(jsonData.internalNote);
		$('#view-modify-notes').modal('show');
	}
	
	function updateInvoiceNote(){
		$.ajax({
			url : "${pageContext.request.contextPath}/Accounting/UpdateInvoiceNote",
			type : "post",
			data : $("#editInvoiceNote").serialize(),
			dataType : "json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#view-modify-notes').modal('hide');				
					getInvoiceGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				//$('#ModifyNotesMsg').show();
				//$('#ModifyNotesMsg').text(res);
				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	//End Modify Notes Script
	
	
	//Start - View Related PO Invoices	
	function getRelatedPOInvoices(invoiceId, productType){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetRelatedPOInvoices",
			type : "post",
			data : "invoiceId="+invoiceId+"&productType="+productType,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/*if(jsonData==null || jsonData=="") {
					jAlert("No Related PO/Invoice(s) Found.");
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setRelatedPOInvoices(jsonData);
					$('#invoiceId_Hdr_RelatedPO').text(invoiceId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setRelatedPOInvoices(jsonData){
		$('#view-related-po').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#relatedPOSuccessDiv').hide();
			$('#relatedPOSuccessMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#relatedPOErrorDiv').hide();
			$('#relatedPOErrorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#relatedPOSuccessDiv').show();
			$('#relatedPOSuccessMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#relatedPOErrorDiv').show();
			$('#relatedPOErrorMsg').text(jsonData.errorMessage);
		}*/
		relatedPOGridValues(jsonData.poList, jsonData.poPagingInfo);
		relatedInvoiceGridValues(jsonData.ticketList, jsonData.pagingInfo);
	}
	//End Related PO Invoices
	
	
	//Start - View Payment History	
	function getPaymentHistory(invoiceId, productType){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetPaymentHistory",
			type : "post",
			data : "invoiceId="+invoiceId+"&productType="+productType,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setPaymentHistory(jsonData);
					$('#invoiceId_Hdr_PayHistory').text(invoiceId);
				} /*else {
					jAlert(jsonData.msg);
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setPaymentHistory(jsonData){
		$('#view-payment-history').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#paymentHistorySuccessDiv').hide();
			$('#paymentHistorySuccessMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#paymentHistoryErrorDiv').hide();
			$('#paymentHistoryErrorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#paymentHistorySuccessDiv').show();
			$('#paymentHistorySuccessMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#paymentHistoryErrorDiv').show();
			$('#paymentHistoryErrorMsg').text(jsonData.errorMessage);
		}*/
		if(jsonData.paymentHistory != null){
			$('#paymentGrid').show();
			$('#rtwPaymentGrid').hide();
			refreshPaymentHistoryGridValues(jsonData.paymentHistory, jsonData.pagingInfo);
			paymentHistoryPagingInfo = jsonData.pagingInfo;
		}
		if(jsonData.rtwPaymentHistory != null){
			$('#rtwPaymentGrid').show();
			$('#paymentGrid').hide();
			refreshrtwPaymentHistoryGridValues(jsonData.rtwPaymentHistory, jsonData.pagingInfo);
			paymentHistoryPagingInfo = jsonData.pagingInfo;
		}
	}
	//End Payment History	

	//Start - Edit Invoice
	function addRealTicket(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/AddRealTicket",
			type : "post",
			data : "invoiceId="+invoiceId+"&orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData !=null && jsonData !="") {
					if(jsonData.status == 1){
						setRealTicket(jsonData);
						showHideFedexInfo();
						$('#invoiceId_Hdr').text(invoiceId);
					}/* else{
						jAlert(jsonData.msg);
					} */
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function showHideFedexInfo(){		
		if(($('#ei_shippingMethod :selected').text() == "fedex") || ($('#ei_shippingMethod :selected').text() == "FedEx")){
			$('#ei_trackingInfo').show();
		}else{
			$('#ei_trackingInfo').hide();
		}	
	}
	//End Edit Invoice

	//Start Edit Shipping Address	
	function getCustomerOrderShippingAddress(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetCustomerOrderShippingAddress",
			type : "post",
			data : "orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setCustomerOrderShippingAddress(jsonData);
					$('#invoiceId_Hdr_EdShipping').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End Edit Shipping Address
	
	//Start Edit Billing Address	
	function getCustomerOrderBillingAddress(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetCustomerOrderBillingAddress",
			type : "post",
			data : "orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setCustomerOrderBillingAddress(jsonData);
					$('#invoiceId_Hdr_EdBilling').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End Edit Billing Address
	
	//Start Edit Invoice CSR
	function getInvoiceCSR(invoiceId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetInvoiceCsr",
			type : "post",
			data : "invoiceId="+invoiceId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setInvoiceCSR(jsonData);
					$('#invoiceId_Hdr_EdCSR').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	
	function setInvoiceCSR(jsonData){
		$('#edit-invoice-csr').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#eic_successDiv').hide();
			$('#eic_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#eic_errorDiv').hide();
			$('#eic_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#eic_successDiv').show();
			$('#eic_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#eic_errorDiv').show();
			$('#eic_errorMsg').text(jsonData.errorMessage);
		}*/
		updateComboBox(jsonData.users,eic_csrUser,jsonData.invoiceCsr);
		$('#eic_invoiceId').val(jsonData.invoiceId);
	}
	
	function updateComboBox(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='auto'>Auto</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].name == listName){
					$(id).append("<option value="+jsonData[i].name+" selected>"+jsonData[i].name+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].name+">"+jsonData[i].name+"</option>");
				}
			}
		}
	}
	
	function changeInvoiceCsr(){
		$('#eic_action').val('changeCSR');
		$.ajax({
				url : "${pageContext.request.contextPath}/Accounting/GetInvoiceCsr",
				type : "post",
				dataType : "json",
				data :$("#eic_changeCsr").serialize(),
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#edit-invoice-csr').modal('hide');
						getInvoiceGridData(0);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					/*if(jsonData.successMessage != null && jsonData.successMessage != ""){
						//$('#eic_successDiv').show();
						//$('#eic_successMsg').text(jsonData.successMessage);
						jAlert(jsonData.successMessage);
					}
					if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
						//$('#eic_errorDiv').show();
						//$('#eic_errorMsg').text(jsonData.errorMessage);
						jAlert(jsonData.errorMessage);
					}*/
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
	}
	//End Edit Invoice CSR
	
	//Start Refund/Credit Invoice
	function getInvoiceRefund(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/CreateInvoiceRefundOrCredit",
			type : "post",
			data : "orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setInvoiceRefund(jsonData);
					$('#invoiceId_Hdr_Refund').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	//End Refund/Credit Invoice
	
	//Start View Refund Details
	function getInvoiceRefundDetails(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetInvoiceRefund",
			type : "post",
			data : "orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Invoice Refund Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setInvoiceRefundDetails(jsonData);
					$('#invoiceId_Hdr_RefDetails').text(invoiceId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	
	function setInvoiceRefundDetails(jsonData){
		$('#view-refund-details').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#vr_successDiv').hide();
			$('#vr_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#vr_errorDiv').hide();
			$('#vr_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#vr_successDiv').show();
			$('#vr_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#vr_errorDiv').show();
			$('#vr_errorMsg').text(jsonData.errorMessage);
		} */
		refundDetailGridValues(jsonData.invoiceRefund, jsonData.pagingInfo);
	}
	//End View Refund Details
	
	//Start View Download Ticket Details
	function getInvoiceDownloadTicketDetails(invoiceId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetInvoiceDownloadTicket",
			type : "post",
			data : "invoiceId="+invoiceId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/* if(jsonData==null || jsonData=="") {
					jAlert("No Invoice Download Ticket Details Found.");
				} */ 
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}else {
					setInvoiceDownloadTicketDetails(jsonData);
					$('#invoiceId_Hdr_DowTicketDetail').text(invoiceId);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	
	function setInvoiceDownloadTicketDetails(jsonData){
		$('#download-ticket-detail').modal('show');
		/* if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#vdt_successDiv').hide();
			$('#vdt_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#vdt_errorDiv').hide();
			$('#vdt_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#vdt_successDiv').show();
			$('#vdt_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#vdt_errorDiv').show();
			$('#vdt_errorMsg').text(jsonData.errorMessage);
		} */
		downloadTicketGridValues(jsonData.ticketDownload, jsonData.pagingInfo);
	}
	//End View Download Ticket Details
	
	//Start View Order Details
	function getInvoiceOrderDetails(invoiceId, orderId){
		var orderNo = "";
		if(orderId != '' && orderId != null){
			orderNo = $(orderId).val();
		}
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetCustomerOrderDetails",
			type : "post",
			data : "invoiceId="+invoiceId+"&orderNo="+orderNo,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setInvoiceOrderDetails(jsonData);
					$('#invoiceId_Hdr_OrdDetail').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End View Order Details
		
	//Start Pay Invoice
	function getPayInvoice(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/CreatePayInvoice",
			type : "post",
			data : "orderId="+orderId,
			dataType: "json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					resetPayInvoiceValues();					
					setPayInvoice(jsonData);
					$('#invoiceId_Hdr_PayInvoice').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}	
	//End Pay Invoice
		
	//Start Create Fedex Label
	function getCustomerOrderFedex(invoiceId, orderId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GetCustomerOrderShippingAddress",
			type : "post",
			data : "invoiceId="+invoiceId+"&orderId="+orderId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					setCustomerOrderFedex(jsonData);
					$('#invoiceId_Hdr_CreateFedex').text(invoiceId);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});	
	}
	//End Create Fedex Label
	
	function generateInvoicePdf(invoiceId, inline) {		
		/* var url = "${pageContext.request.contextPath}/Accounting/GetInvoicePdf?invoiceId="
				+ invoiceId + "&inline=" + inline; */
		var url = apiServerUrl+"GetInvoicePdf?invoiceId="+ invoiceId + "&inline=" + inline+"&brokerId="+$('#brokerId').val();
		if (inline == 'inline') {
			popupCenter(url, "Invoice PDF", "1000", "900");
		} else {
			$('#download-frame').attr('src', url);
		}
	}

	function generateRTWRTW2InvoicePdf(invoiceId,productType, inline) {
		/* var url = "${pageContext.request.contextPath}/Accounting/GetRTWRTW2InvoicePdf?invoiceId="
				+ invoiceId +"&productType=" +productType+"&inline=" + inline; */
		var url = apiServerUrl+"GetRTWRTW2InvoicePdf?invoiceId="+ invoiceId +"&productType=" +productType+"&inline=" + inline;
		if (inline == 'inline') {
			popupCenter(url, "RTW/RTW2 Invoice PDF", "1000", "900");
		} else {
			$('#download-frame').attr('src', url);
		}
	}
	
	function updateInvoice(invoiceId, status) {
		jConfirm(
				"Are you Sure you want update selected invoice",
				"Confirm",
				function(r) {
					if (r) {
						$.ajax({
							url : "${pageContext.request.contextPath}/Accounting/ChangeInvoiceStatus",
							type : "post",
							dataType : "json",
							data : "invoiceId=" + invoiceId
									+ "&status=" + status,
							success : function(res) {
								var jsonData = JSON.parse(JSON.stringify(res));
								if(jsonData.status == 1){
									getInvoiceGridData(pagingInfo.pageNum);
								}								
								if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
									jAlert(jsonData.msg);
								}								
							},
							error : function(error) {
								jAlert("Your login session is expired please refresh page and login again.", "Error");
								return false;
							}
						});
					}
				});
	}

	function doSaveBarCode() {
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice to upload barcode");
		} else {
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			$("#invoiceIdBarCode").val(
					eventGrid.getDataItem(customerRowIndex).invId);
			if ($("#barCode").val() == '') {
				jAlert("Please enter barcode");
				return false;
			} else {
				$("#barCodeAction").val('saveBarCode');
				$("#barCodeForm").submit();
			}
		}
	}

	//poupup create real ticket
	function createRealTicket() {
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice for customer to add real ticket");
		} else {
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			var orderId = eventGrid.getDataItem(customerRowIndex).orderId;
			addRealTicket(invoiceId, orderId);
			/*
			var createRealTix = "${pageContext.request.contextPath}/Accounting/CreateRealTix?invoiceId="
					+ invoiceId;
			popupCenter(createRealTix, 'create real ticket for invoice',
					'1100', '1000');*/
		}
	}

	function trackMyOrder(trackingNo) {
		var url = "https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber="
				+ trackingNo;
		popupCenter(url, "Fedex Tracking", "1100", "900");
		/* $.ajax({
			url : "${pageContext.request.contextPath}/Accounting/TrackOrder",
			type : "post",
			dataType:"json",
			data : "trackingNo="+trackingNo,
			success : function(res){
				
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error);
				return false;
			}
		}); */
	}

	function deleteFedexLabel() {
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice to remove fedex label.");
		} else {
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			var trackingNo = eventGrid.getDataItem(customerRowIndex).trackingNo;
			var fedexLabelCreated = eventGrid.getDataItem(customerRowIndex).view;
			if(fedexLabelCreated != null && fedexLabelCreated == "Yes"){
			jConfirm("Are you sure you want to delete fedex label for selected invoice ?","Confirm",function(r){
				if(r){
					$.ajax({
						url : "${pageContext.request.contextPath}/Accounting/RemoveFedexLabel",
						type : "post",
						dataType : "json",
						data : "invoiceId=" + invoiceId,
						success : function(res) {
							var jsonData = JSON.parse(JSON.stringify(res));
							if(jsonData.status == 1){
								getInvoiceGridData(pagingInfo.pageNum);
							}
							if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
								jAlert(jsonData.msg);
							}							
						},
						error : function(error) {
							jAlert("Your login session is expired please refresh page and login again.", "Error");
							return false;
						}
					});
				}else{
					return false;
				}
			});
			}else{
				jAlert("No FedEx label is created for selected invoice.");
				return;
			}
		}
	}

	function createFedexLabel() {
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice to create fedex label.");
		} else {
			var shippingMethod = eventGrid.getDataItem(customerRowIndex).shippingMethod;
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			//var trackingNo = eventGrid.getDataItem(customerRowIndex).trackingNo;
			var fedexLabelCreated = eventGrid.getDataItem(customerRowIndex).view;
			if (shippingMethod != "FedEx") {
				jAlert("Could not create Fedex label, Selected invoice shipping method is not FedEx.");
				return;
			}			
			if(fedexLabelCreated != null && fedexLabelCreated == 'Yes'){
				jAlert("Label is already created for selected invoice, to recreate again. Please remove label first.");
				return;
			}	
			
			var orderId = eventGrid.getDataItem(customerRowIndex).orderId;
			getCustomerOrderFedex(invoiceId, orderId);
			/*var url = "${pageContext.request.contextPath}/Accounting/EditShippingAddress?orderId="
					+ orderId+"&invoiceId="+invoiceId;
			popupCenter(url, "Edit Shipping Address", "800",
					"700");*/			
		}
	}
	
	//Download Ticket Details
	function viewDownloadTicketDetail(){
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice for customer to view download ticket");
		}
		else{
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			var status = eventGrid.getDataItem(customerRowIndex).status;
			if(status != 'Completed'){
				jAlert("Selected invoice is not completed yet, no ticket download information available");
			}else{
			getInvoiceDownloadTicketDetails(invoiceId);	
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewDownloadTicket?invoiceId="
					+ invoiceId;
			popupCenter(url, "Edit Download Ticket", "500", "400");*/
			}
		}
	}
	
	//Refund Ticket Details
	function viewRefundTicketDetail(){
		var customerRowIndex = eventGrid.getSelectedRows([ 0 ])[0];
		if (customerRowIndex == null) {
			jAlert("Please select an invoice for customer to view refund ticket");
		}
		else{
			var invoiceId = eventGrid.getDataItem(customerRowIndex).invId;
			var orderId = eventGrid.getDataItem(customerRowIndex).orderId;
			var status = eventGrid.getDataItem(customerRowIndex).status;
			getInvoiceRefundDetails(invoiceId, orderId);
			/*var url = "${pageContext.request.contextPath}/Accounting/ViewInvoiceRefund?orderId="
					+ orderId;
			popupCenter(url, "Edit Stripe Refund", "500", "400");*/
		}
	}
	
	//Send Invoice PDF
	function generateInvoicePDFAndSendMail(invoiceId){
		$.ajax({		  
			url : "${pageContext.request.contextPath}/Accounting/GenerateInvoicePdfAndSendMail",
			type : "post",
			data : "invoiceId="+invoiceId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					//jAlert(jsonData.msg);
				}/* else{
					jAlert(jsonData.msg);
				} */
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	//show the pop window center
	function popupCenter(url, title, w, h) {
		// Fixes dual-screen position                         Most browsers      Firefox  
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft
				: screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop
				: screen.top;

		width = window.innerWidth ? window.innerWidth
				: document.documentElement.clientWidth ? document.documentElement.clientWidth
						: screen.width;
		height = window.innerHeight ? window.innerHeight
				: document.documentElement.clientHeight ? document.documentElement.clientHeight
						: screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w
				+ ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow  
		if (window.focus) {
			newWindow.focus();
		}
	}

	//Get selected customer
	/* function getCustomerInfoForInvoice(customerGridIndex) {
			$("#customerIdDiv").val(eventGrid.getDataItem(customerGridIndex).id);
			$("#customerNameDiv").text(eventGrid.getDataItem(customerGridIndex).customerName);
			$("#emailDiv").text(eventGrid.getDataItem(customerGridIndex).username);
			$("#phoneDiv").text(eventGrid.getDataItem(customerGridIndex).phone);
			var address=eventGrid.getDataItem(customerGridIndex).street1;
			$("#addressLineDvi").text(address);
			$("#countryDiv").text(eventGrid.getDataItem(customerGridIndex).country);
			$("#stateDiv").text(eventGrid.getDataItem(customerGridIndex).state);
			$("#zipCodeDiv").text(eventGrid.getDataItem(customerGridIndex).zipCode);
			$("#cityDiv").text(eventGrid.getDataItem(customerGridIndex).city);
	} */

	/* function getSelectedEventGridId() {
		var temprEventRowIndex = eventGrid.getSelectedRows([0])[0];
		if(temprEventRowIndex != null && temprEventRowIndex!='') {
			 return eventGrid.getDataItem(temprEventRowIndex).id;
		}
	} */

	function saveUserInvoicePreference() {
		var cols = visibleColumns;
		if (cols == null || cols == '' || cols.length == 0) {
			cols = eventGrid.getColumns();
		}
		var colStr = '';
		for ( var i = 0; i < cols.length; i++) {
			colStr += cols[i].id + ":" + cols[i].width + ",";
		}
		saveUserPreference('invoicegrid', colStr);
	}

	//call functions once page loaded
	window.onload = function() {
		pagingInfo = JSON.parse(JSON.stringify(${pagingInfo}));
		refreshInvoiceGridValues(JSON.parse(JSON.stringify(${openOrders})));
		$('#openOrder_pager> div').append("<span class='slick-pager-status'> <b>Invoice Grand Total : <span id='invoiceTotal'>${invoiceTotal}</span></b></span>");
		$('#openOrder_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserInvoicePreference()'>");

	};
</script>