
var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
		"Sept", "Oct", "Nov", "Dec" ];
var weekDays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
var allTicketGroup = [];
var allTickets = [];
var qtyfilteredTickets = [];
var priceFilteredTickets = [];
var eventDetail = '';
var ticketQuantity = [ "All" ];
var classDesign = "even";	


jQuery(document).ready(function($){
	var mouseDraggedClick = false;
	var currentSliderValue=0;
	var svg=document.getElementById("svg1");
	
	$('#tickets li').mouseover(function (){
		$('.selectedSVG').each(function() {
			$(this).attr('class','');
		});
		$('[id*=zone-\\:-'+$(this).attr('data-zone')+']').attr('class','selectedSVG');
	});
	
	$('#zone_letters').show();
	$('#section_letters').show();
	
	$('#svgEnlargeMap').find('#zone_letters').show();
	$('#svgEnlargeMap').find('#section_letters').show();
	
	$("#shapes path,rect,polygon").each(function(){
		$(this).css({'fill':'#FFFFFF','fill-opacity':0.1});
	});
	
	$("#tickets li").each(function(){
		var id = $(this).attr("data-zone");
		$("polygon[id^='"+id+"-:-'],path[id^='"+id+"-:-'],rect[id^='"+id+"-:-']").css({'fill':$(this).attr("data-color"),'fill-opacity':0.7});
	});
	
	$('#section_letters').children().each(function(){
		$(this).css('font-family','Arial Black');
	//	$(this).css('font-weight',900);
	});
	
	$('#zone_letters').children().each(function(){
		$(this).css('font-family','Arial Black');
		//$(this).css('font-weight',900);
	});
	
	var pt=svg.createSVGPoint();
	var loc;
	
	
	function getCursor(evt){
		pt.x=evt.clientX;
		pt.y=evt.clientY;
		return pt.matrixTransform(svg.getScreenCTM().inverse());
	}
	var selectedZone='';
	$('#svg1 path,rect,polygon').click(function (){
		var zone = $(this).attr('id');
		var zone1 = 'zone '+zone.split('-')[0];
		$('.headerInput').val('');
		$('.selectedSVG').each(function(){
			$(this).attr('class','');
			$(this).css('stroke-width','3px');
		});
		if(zone1 == selectedZone){
			selectedZone = "";
			$('#sectionHeader').val('');
			columnFilters['section'] = '';
		    ticketDataView.refresh();
		}else{
			$('#shapes').find($('[id^='+zone.split('-:-')[0]+'-\\:-]')).each(function(){
				$(this).css('stroke-width','0px');
			});
			
			$('#zone_borders').find($('[id^=zone-\\:-'+zone.split('-:-')[0]+']')).each(function(){
				$(this).attr('class','selectedSVG');
				$(this).css('stroke-width','10px');
			});	
			$('#sectionHeader').val(zone1);
			columnFilters['section'] = zone1;
		    ticketDataView.refresh();
		}
		selectedZone =zone1; 
	});
	
	$('#svg1 #zone_letters text').click(function (){
		var zone = $(this).attr('id');
		var zone1 = 'zone '+zone.split('-')[0];;
		$('.headerInput').val('');
		$('.selectedSVG').each(function(){
			$(this).attr('class','');
			$(this).css('stroke-width','3px');
		});
		if(zone1 == selectedZone){
			selectedZone = "";
			$('#sectionHeader').val('');
			columnFilters['section'] = '';
		    ticketDataView.refresh();
		}else{
			$('#zone_borders').find($('[id^=zone-\\:-'+zone.split('_')[1]+']')).each(function(){
				$(this).attr('class','selectedSVG');
				$(this).css('stroke-width','10px');
			});
			
			$('#sectionHeader').val(zone1);
			columnFilters['section'] = zone1;
		    ticketDataView.refresh();
		}
		selectedZone = zone1;
	});
	
	$('#svg1 #section_letters text').click(function (){
		var zoneString = $(this).attr('id').split("-:-");
		var zone = zoneString[0].split("_")[2];
		var zone1 = 'zone '+zone;
		$('.headerInput').val('');
		$('.selectedSVG').each(function(){
			$(this).attr('class','');
			$(this).css('stroke-width','3px');
		});
		if(zone1 == selectedZone){
			selectedZone = "";
			$('#sectionHeader').val('');
			columnFilters['section'] = '';
		    ticketDataView.refresh();
		}else{
			$('#shapes').find($('[id^='+zone+'-\\:-]')).each(function(){
				$(this).css('stroke-width','0px');
			});
			
			$('#zone_borders').find($('[id^=zone-\\:-'+zone+']')).each(function(){
				$(this).attr('class','selectedSVG');
				$(this).css('stroke-width','10px');
			});
			$('#sectionHeader').val(zone1);
			columnFilters['section'] = zone1;
		    ticketDataView.refresh();
		}
		selectedZone = zone1;
		
		
	});

	
	
	//SVG Map zoom Code starts
	var eventsHandler;
	eventsHandler = {
	  haltEventListeners: ['touchstart', 'touchend', 'touchmove', 'touchleave', 'touchcancel']
	, init: function(options) {
	    var instance = options.instance
	      , initialScale = 1
	      , pannedX = 0
	      , pannedY = 0

	    // Init Hammer
	    // Listen only for pointer and touch events
	    this.hammer = Hammer(options.svgElement, {
	      inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput
	    });

	    // Enable pinch
	    this.hammer.get('pinch').set({enable: true});

	    // Handle double tap
	    this.hammer.on('doubletap', function(ev){
	      instance.zoomIn();
	    });

	    // Handle pan
	    this.hammer.on('panstart panmove', function(ev){
	      // On pan start reset panned variables
	      if (ev.type === 'panstart') {
	        pannedX = 0;
	        pannedY = 0;
	      }

	      // Pan only the difference
	      instance.panBy({x: ev.deltaX - pannedX, y: ev.deltaY - pannedY});
	      pannedX = ev.deltaX;
	      pannedY = ev.deltaY;
	    });

	    // Handle pinch
	    this.hammer.on('pinchstart pinchmove', function(ev){
	      // On pinch start remember initial zoom
	      if (ev.type === 'pinchstart') {
	        initialScale = instance.getZoom();
	        instance.zoom(initialScale * ev.scale);
	      }

	      instance.zoom(initialScale * ev.scale);

	    });

	    // Prevent moving the page on some devices when panning over SVG
	    options.svgElement.addEventListener('touchmove', function(e){ e.preventDefault(); });
	  }

	, destroy: function(){
	    this.hammer.destroy();
	  }
	}



	document.getElementById('svgZoomIn').addEventListener('click', function(ev){
	  ev.preventDefault();

	  panZoom.zoomIn();
	});

	document.getElementById('svgZoomOut').addEventListener('click', function(ev){
	  ev.preventDefault();

	  panZoom.zoomOut();
	});



	 var beforePan

	beforePan = function(oldPan, newPan){
	  var stopHorizontal = false
	    , stopVertical = false
	    , gutterWidth = 100
	    , gutterHeight = 100
	      // Computed variables
	    , sizes = this.getSizes()
	    , leftLimit = -((sizes.viewBox.x + sizes.viewBox.width) * sizes.realZoom) + gutterWidth
	    , rightLimit = sizes.width - gutterWidth - (sizes.viewBox.x * sizes.realZoom)
	    , topLimit = -((sizes.viewBox.y + sizes.viewBox.height) * sizes.realZoom) + gutterHeight
	    , bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.y * sizes.realZoom)

	  customPan = {}
	  customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x))
	  customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y))

	  return customPan
	}

	window.panZoom = svgPanZoom('#svg1', {
	  zoomEnabled: false
	, controlIconsEnabled: false
	, fit: 1
	, center: 1
	, beforePan: beforePan
	, customEventsHandler: eventsHandler
	});
	
});
