
function initializeJS() {

    //tool tips
    jQuery('.tooltips').tooltip();

    //popovers
    jQuery('.popovers').popover();

    //custom scrollbar
        //for html
    jQuery("html").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '6', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: '', zindex: '1000'});
        //for sidebar
    jQuery("#sidebar").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '3', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: ''});
        // for scroll panel
    jQuery(".scroll-panel").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '3', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: ''});
    
    //sidebar dropdown menu
    jQuery('#sidebar .sub-menu > a').click(function () {
        var last = jQuery('.sub-menu.open', jQuery('#sidebar'));        
        jQuery('.menu-arrow').removeClass('arrow_carrot-right');
        jQuery('.sub', last).slideUp(200);
        var sub = jQuery(this).next();
        if (sub.is(":visible")) {
            jQuery('.menu-arrow').addClass('arrow_carrot-right');            
            sub.slideUp(200);
        } else {
            jQuery('.menu-arrow').addClass('arrow_carrot-down');            
            sub.slideDown(200);
        }
        var o = (jQuery(this).offset());
        diff = 200 - o.top;
        if(diff>0)
            jQuery("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            jQuery("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

    // sidebar menu toggle
	
	//==== sidebar and main content =====
	
	
	
	function responsiveView() {
		var wSize = jQuery(window).width();
		if (wSize <= 767) {
			jQuery('#container').addClass('sidebar-close');
			jQuery('#sidebar > ul').hide();
			jQuery('.toggle-nav').click(function() {
				jQuery('#sidebar li.sub-menu.active ul.sub').css('display','block');
			});
		}

		if (wSize >= 768) {
			jQuery('#sidebar .sidebar-menu .sub-menu.active ul.sub').css('display','block');
			
			jQuery('#container').removeClass('sidebar-close');
			jQuery('#sidebar > ul').show();
			
			jQuery('.toggle-nav, .icon2').click(function() {
				jQuery('#main-content').toggleClass("content-slide");
				jQuery('#sidebar').toggleClass("sidebar-slide");
			});
			jQuery('.toggle-nav').click(function() {
				jQuery('#sidebar li.sub-menu.active ul.sub').css('display','block');
				jQuery('#sidebar.sidebar-slide li.sub-menu.active ul.sub').css('display','none');
			});
			jQuery('#sidebar .sub-menu a').click(function() {
				//jQuery('#main-content').addClass("content-slide");
				//jQuery('#sidebar').addClass("sidebar-slide");
				//#sidebar > ul > li > ul.sub
			});
		}
		
	}
	
	jQuery(window).on('load', responsiveView);
	//jQuery(window).on('resize', responsiveView);
    
    //bar chart
    if (jQuery(".custom-custom-bar-chart")) {
        jQuery(".bar").each(function () {
            var i = jQuery(this).find(".value").html();
            jQuery(this).find(".value").html("");
            jQuery(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

}

/*Function to toggle between the menus*/
function toggleMenu(){
	$('.sub-menu .sub').hide();
	
    $("li:has(ul)").click(function() {
		$('.sub-menu .sub').hide();
        //$("ul", this).show();
		$(this).find("ul").show();
    });

	
	/*$(".sub-menu .sub").hide('fast');
		
	$(".sub-menu span").click(function(){
		$(".sub-menu .sub").hide('fast'); 
		$(this).parent().find("ul").toggle('fast'); 
	});*/
	
	/*$(".sub-menu .sub").hide('fast')
	  $(".sub-menu span").click(function () {
		  $(".sub-menu .sub").hide('fast'); 
        $('li > ul').not($(this).children("ul").toggle()).hide();
        
    });*/
	
}

jQuery(document).ready(function(){	
    initializeJS();
	toggleMenu();
});


jQuery(document).ready(function () {
	var header_height = $("header.header").outerHeight();

	$("#sidebar ul.sidebar-menu").css("margin-top", header_height);
	$("section#main-content .wrapper").css("margin-top", header_height);
	
	var winSize = jQuery(window).width();
	if (winSize => 767) {
		jQuery('.toggle-nav').click(function(e){
			jQuery('ul.sidebar-menu').toggleClass('sidebar-mobile');
			jQuery('#sidebar > ul > li > ul.sub').css('display','none');
		});
	}
	if (winSize <= 991) {
	setTimeout(function () {
	    var grid_top = jQuery('.grid-table').offset().top + 90;
	        jQuery('ul#eventContextMenu, ul#contextMenu').css('top',grid_top);
	    }, 3000);
	}
	
    jQuery('#menuContainer').click(function(e){
        if(jQuery('.slick-columnpicker').show()){
            jQuery('.slick-columnpicker').hide();    
        } 
    });
	
	//==================== Modal header fixed and modal body scroll JS ====================
	
	resizeDiv();
    setTimeout(function() {
        resizeDiv();
    }, 10);
    window.onresize = function(event) {
        resizeDiv();
    }
	jQuery(window).scroll(function() {
		resizeDiv();
	});
    function resizeDiv() {
        window_height = jQuery(window).height();
		jQuery('section#main-content').css({'min-height': window_height });
		
		jQuery('.modal-body').css({'max-height': window_height - 120 + 'px'});
		var winSize2 = jQuery(window).width();
		if (winSize2 <= 767) {
			jQuery('.modal-body').css({'max-height': window_height - 70 + 'px'});
		}
    }
	
	setTimeout(function() {
        $(".modal-header .close, .modal-footer .btn-default, .modal-footer .btn-cancel").click( function() {
		   $(".modal-body").scrollTop(0);
		});
    }, 3000);
	
});
