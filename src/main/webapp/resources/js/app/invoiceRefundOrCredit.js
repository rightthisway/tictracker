	//Invoice Refund/Credit
	var primaryPaymentMethod;
	var secondaryPaymentMethod;
	var thirdPaymentMethod;
	var orderTotal;
	
	function setInvoiceRefund(jsonData){
		$('#refund-credit-invoice').modal('show');
		resetRefundInfo();
		$("#refundButton").show();
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#rci_successDiv').hide();
			$('#rci_successMsg').text('');
			$("#refundButton").show();
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#rci_errorDiv').hide();
			$('#rci_errorMsg').text('');
			$("#refundButton").show();
		}
		if(jsonData.successMessage != null){
			$('#rci_successDiv').show();
			$('#rci_successMsg').text(jsonData.successMessage);
			$("#refundButton").hide();
		}
		if(jsonData.errorMessage != null){
			$('#rci_errorDiv').show();
			$('#rci_errorMsg').text(jsonData.errorMessage);
			$("#refundButton").hide();
		}*/
		var customer = jsonData.customer;
		if(customer != null && customer != ""){
			$('#rci_customerName').text(customer.customerName+" "+customer.lastName);
			$('#rci_customerEmail').text(customer.email);
		}
		
		var invoice = jsonData.invoice;
		if(invoice != null && invoice != ""){
			$('#rci_invoiceId').text(invoice.invoiceId);
			$('#rci_refundNote').val(invoice.invoiceInternalNote);
		}
		
		var order = jsonData.order;
		if(order != null && order != ""){
			$('#rci_orderId_span').text(order.orderId);
			$('#rci_orderCreateDate').text(order.orderCreateDate);
			$('#rci_orderTotal').text(order.orderTotal);
			if(order.primaryPaymentMethod == 'BANK_OR_CHEQUE'){
				$('#rci_primaryPaymentMethod').text('BANK_OR_CHECK');
			}else{
				$('#rci_primaryPaymentMethod').text(order.primaryPaymentMethod);
			}
			$('#rci_primaryAvailableAmount_span').text(order.primaryAvailableAmt);
			//$('#rci_secondaryPaymentMethod').text(order.secondaryPaymentMethod);
			$('#rci_secondaryAvailableAmount_span').text(order.secondaryAvailableAmt);
			//$('#rci_thirdPaymentMethod').text(order.thirdPaymentMethod);
			$('#rci_thirdAvailableAmount_span').text(order.thirdAvailableAmt);
		}
		
		if(jsonData.isValid == true){
			$('#rci_form_div').show();
			$('#rci_orderId').val(order.orderId);
			$('#rci_primaryAvailableAmt').val(order.primaryAvailableAmt);
			$('#rci_secondaryAvailableAmt').val(order.secondaryAvailableAmt);
			$('#rci_thirdAvailableAmt').val(order.thirdAvailableAmt);
		}
		
		if(order != null && order != ""){
			primaryPaymentMethod = order.primaryPaymentMethod;
			secondaryPaymentMethod = order.secondaryPaymentMethod;
			thirdPaymentMethod = order.thirdPaymentMethod;
			orderTotal = order.orderTotal;
			$("#rci_refundDiv").hide();
			$("#rci_walletCCDiv").hide();
			$("#rci_creditDiv").hide();	
		}
		if(primaryPaymentMethod == 'CREDITCARD' || primaryPaymentMethod == 'PAYPAL'
			|| primaryPaymentMethod == 'GOOGLEPAY' || primaryPaymentMethod == 'IPAY'){			
			$("#rci_refundDiv").show();
			$("#rci_walletCCDiv").show();
			//$("#rci_creditDiv").show();		
		}
		else if(primaryPaymentMethod == 'CUSTOMER_WALLET'){
			if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){
				$("#rci_refundDiv").show();
				$("#rci_walletCCDiv").show();
				$("#rci_creditDiv").show();	
				$('#rci_secondaryPaymentMethod').text(secondaryPaymentMethod);
				$('#rci_thirdPaymentMethod').text('');
			}else{
				$("#rci_creditDiv").show();
				$('#rci_secondaryPaymentMethod').text('');
				$('#rci_thirdPaymentMethod').text('');
			}
		}
		else if(primaryPaymentMethod == 'FULL_REWARDS'){
			//$("#rci_rewarPointDiv").show();
			$('#rci_secondaryPaymentMethod').text('');
			$('#rci_thirdPaymentMethod').text('');
		}
		else if(primaryPaymentMethod == 'PARTIAL_REWARDS'){
			//$('#rci_rewardPoints').val($('#primaryPaymentAmt').val());
			if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'PAYPAL'
				|| secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){
				$("#rci_refundDiv").show();
				$("#rci_walletCCDiv").show();
				//$("#rci_creditDiv").show();
				//$("#rci_rewarPointDiv").show();
				$('#rci_secondaryPaymentMethod').text(secondaryPaymentMethod);
				$('#rci_thirdPaymentMethod').text('');
			}
			else if(secondaryPaymentMethod == 'CUSTOMER_WALLET' && (thirdPaymentMethod == 'CREDITCARD'
				|| thirdPaymentMethod == 'GOOGLEPAY' || thirdPaymentMethod == 'IPAY')){
				$("#rci_refundDiv").show();
				$("#rci_walletCCDiv").show();
				$("#rci_creditDiv").show();
				//$("#rci_rewarPointDiv").show();
				$('#rci_secondaryPaymentMethod').text(secondaryPaymentMethod);
				$('#rci_thirdPaymentMethod').text(thirdPaymentMethod);
			}
			else if(secondaryPaymentMethod == 'CUSTOMER_WALLET'){
				$("#rci_creditDiv").show();
				//$("#rci_rewarPointDiv").show();
				$('#rci_secondaryPaymentMethod').text(secondaryPaymentMethod);
				$('#rci_thirdPaymentMethod').text('');
			}
			/* else if(secondaryPaymentMethod == 'IPAY' || secondaryPaymentMethod == 'GOOGLEPAY'){
				$('#secondaryMethod').text(secondaryPaymentMethod);
				$("#msgDiv").show();
				$('#msgBox').text('Cannot refund as selected invoice payment is made using '+secondaryPaymentMethod);
			} */
		}
	}
	
	function makeRefund(){
		var refundAmount = $('#rci_refundAmount').val();
		var walletCCAmt = $('#rci_creditWalletAmount').val();
		var creditAmount = $('#rci_creditAmount').val();
		var rewardPoints = $('#rci_rewardPoints').val();
		var primaryPaymentAmount = $('#rci_primaryAvailableAmt').val();
		var secondaryPaymentAmount = $('#rci_secondaryAvailableAmt').val();
		var thirdPayAmount = $('#rci_thirdAvailableAmt').val();
		refundAmount = parseFloat(refundAmount);
		walletCCAmt = parseFloat(walletCCAmt);
		creditAmount = parseFloat(creditAmount);
		rewardPoints = parseFloat(rewardPoints);
		primaryPaymentAmount = parseFloat(primaryPaymentAmount);
		secondaryPaymentAmount = parseFloat(secondaryPaymentAmount);
		thirdPayAmount = parseFloat(thirdPayAmount);
		var isValid = false;
		
		if(primaryPaymentMethod == 'CREDITCARD' || primaryPaymentMethod == 'PAYPAL'
			|| primaryPaymentMethod == 'GOOGLEPAY' || primaryPaymentMethod == 'IPAY'){
			if(($('#rci_refundAmount').val() != null && $('#rci_refundAmount').val() != '') || ($('#rci_creditWalletAmount').val() != null && $('#rci_creditWalletAmount').val() != '')){
				if($('#rci_refundAmount').val() > 0 || $('#rci_creditWalletAmount').val() > 0){
					if(refundAmount > primaryPaymentAmount){
						jAlert("Refund amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
						return false;
					}
					else if(walletCCAmt > primaryPaymentAmount){
						jAlert("Credit to Wallet from paypal/cc amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
						return false;
					}
					else if((refundAmount+walletCCAmt) > primaryPaymentAmount){
						jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
					return false;
				}
			}
			else{
				jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty");
				return false;
			}
		}
		else if(primaryPaymentMethod == 'CUSTOMER_WALLET'){
			if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){
				if(($('#rci_refundAmount').val() != null && $('#rci_refundAmount').val() != '') || ($('#rci_creditWalletAmount').val() != null && $('#rci_creditWalletAmount').val() != '') || ($('#rci_creditAmount').val() != null && $('#rci_creditAmount').val() != '')){
					if($('#rci_refundAmount').val() > 0 || $('#rci_creditWalletAmount').val() > 0 || $('#rci_creditAmount').val() > 0){
						if(refundAmount > secondaryPaymentAmount){
							jAlert("Refund amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
							return false;
						}
						else if(creditAmount > primaryPaymentAmount){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
							return false;
						}
						else if((refundAmount+walletCCAmt) > secondaryPaymentAmount){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
						return false;
					}
				}
				else{
					jAlert("Refund/Credit to wallet from paypal/cc amount should not be empty.");
					return false;
				}
			}
			else{
				if($('#rci_creditAmount').val() != null && $('#rci_creditAmount').val() != ''){
					if($('#rci_creditAmount').val() > 0){
						if(creditAmount > primaryPaymentAmount){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPaymentAmount+" (Primary Payment Amount).");
							return false;			
						}else{
							isValid = true;
						}
					}else{
						jAlert("Credit to wallet from Wallet amount should be greater than 0");
						return false;
					}
				}
				else{
					jAlert("Credit to wallet from Wallet amount should not be empty.");
					return false;			
				}
			}		
		}
		else if(primaryPaymentMethod == 'FULL_REWARDS'){
			isValid = true;
			/*if($('#rci_rewardPoints').val() != null && $('#rci_rewardPoints').val() != ''){
				if(rewardPoints > primaryPaymentAmount){
					jAlert("Reward points should be less than or equal to "+primaryPaymentAmount);
					return false;
				}else{
					isValid = true;
				}
			}
			else{
				jAlert("Reward points should not be empty.");
				return false;
			}*/
		}
		else if(primaryPaymentMethod == 'PARTIAL_REWARDS'){
			
			if(secondaryPaymentMethod == 'CREDITCARD' || secondaryPaymentMethod == 'PAYPAL'
				|| secondaryPaymentMethod == 'GOOGLEPAY' || secondaryPaymentMethod == 'IPAY'){					
				if(($('#rci_refundAmount').val() != null && $('#rci_refundAmount').val() != '') || ($('#rci_creditWalletAmount').val() != null && $('#rci_creditWalletAmount').val() != '')){
					if($('#rci_refundAmount').val() > 0 || $('#rci_creditWalletAmount').val() > 0){
						if(refundAmount > secondaryPaymentAmount){
							jAlert("Refund amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
							return false;
						}
						else if((refundAmount+walletCCAmt) > secondaryPaymentAmount){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Refund/Wallet amount/Reward Points should not be empty.");
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty.");
					return false;
				}
			}
			else if(secondaryPaymentMethod == 'CUSTOMER_WALLET' && (thirdPaymentMethod == 'CREDITCARD' 
				|| thirdPaymentMethod == 'GOOGLEPAY' || thirdPaymentMethod == 'IPAY')){
				if(($('#rci_refundAmount').val() != null && $('#rci_refundAmount').val() !='') || ($('#rci_creditWalletAmount').val() != null && $('#rci_creditWalletAmount').val() != '')
						|| ($('#rci_creditAmount').val() != null && $('#rci_creditAmount').val() != '')){
					if($('#rci_refundAmount').val() > 0 || $('#rci_creditWalletAmount').val() > 0 || $('#rci_creditAmount').val() > 0){
						if(creditAmount > secondaryPaymentAmount){
							jAlert("Wallet amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
							return false;
						}
						else if(refundAmount > thirdPayAmount){
							jAlert("Refund amount should not be greater than "+thirdPayAmount+" (Third Payment Amount).");
							return false;
						}
						else if((refundAmount+walletCCAmt) > thirdPayAmount){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+thirdPayAmount+" (Third Payment Amount).");
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Refund/Wallet Amount/Reward Points should not be empty.");
					jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should not be empty.");
					return false;
				}
			}
			else if(secondaryPaymentMethod == 'CUSTOMER_WALLET'){
				if(($('#rci_creditAmount').val() != null && $('#rci_creditAmount').val() != '')){
					if($('#rci_creditAmount').val() > 0){
						if(creditAmount > secondaryPaymentAmount){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+secondaryPaymentAmount+" (Secondary Payment Amount).");
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Credit to wallet from Wallet amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Reward Points/Wallet amount should not be empty.");
					jAlert("Credit to wallet from Wallet amount should not be empty.");
					return false;
				}
			}
		}
		if(isValid){
			//jAlert("Pass value to controller..");
			$('#rci_action').val('PARTIAL_REWARDS');
			//$('#invoiceRefund').submit();
			$.ajax({
				url : "/Accounting/CreateInvoiceRefundOrCredit",
				type : "post",
				dataType: "json",
				data : $("#rci_invoiceRefund").serialize(),
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#refund-credit-invoice').modal('hide');
						resetRefundInfo();
						getInvoiceGridData(0);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					/*if(jsonData.successMessage != null && jsonData.successMessage != ""){
						//$('#rci_successDiv').show();
						//$('#rci_successMsg').text(jsonData.successMessage);
						jAlert(jsonData.successMessage);
					}
					if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
						//$('#rci_errorDiv').show();
						//$('#rci_errorMsg').text(jsonData.errorMessage);
						jAlert(jsonData.errorMessage);
					}*/
				}, error : function(error){
					jAlert("There is something wrong. Please try again"+error,"Error");
					return false;
				} 
			});
		}
	}
	/*
	if(primaryPaymentMethod == "CREDITCARD"){
		if((refundAmout+creditAmount) > parseInt(primaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Order total");
			return;
		}
		if((refundAmout+creditAmount) == 0){
			jAlert("Atleast Refund or Credit amount should be greater than 0.");
			return;
		}
		$('#action').val('CREDITCARD');
	}else if(primaryPaymentMethod == "PAYPAL"){
		if((refundAmout+creditAmount) > parseInt(primaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Order total");
			return;
		}
		if((refundAmout+creditAmount) == 0){
			jAlert("Atleast Refund or Credit amount should be greater than 0.");
			return;
		}
		$('#action').val('PAYPAL');
	}else if(primaryPaymentMethod== "PARTIAL_REWARDS" && secondaryPaymentMethod == 'CREDITCARD'){
		if((refundAmout+creditAmount) > parseInt(secondaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Secondary Payment amount.");
			return;
		}
		if((refundAmout+creditAmount+rewardPoints) == 0){
			jAlert("Atleast (Refund + Credit + reward) amount should be greater than 0.");
			return;
		}
		if(rewardPoints > parseInt(primaryPaymentAmount)){
			jAlert("Reward points should be less than Primary payment amount.");
			return;
		}
		$('#action').val('PARTIAL_REWARDS');
	}else if(primaryPaymentMethod== "PARTIAL_REWARDS" && secondaryPaymentMethod == 'PAYPAL'){
		if((refundAmout+creditAmount) > parseInt(secondaryPaymentAmount)){
			jAlert("Refund plus Credit amount should not be greater than Secondary Payment amount.");
			return;
		}
		if((refundAmout+creditAmount+rewardPoints) == 0){
			jAlert("Atleast (Refund + Credit + reward) amount should be greater than 0.");
			return;
		}
		if(rewardPoints > parseInt(primaryPaymentAmount)){
			jAlert("Reward points should be less than Primary payment amount.");
			return;
		}
		$('#action').val('PARTIAL_REWARDS');
	}else if(primaryPaymentMethod == "FULL_REWARDS"){
		if(rewardPoints == 0){
			jAlert("Please add reward point to revert greater than 0.");
			return;
		}
		if(rewardPoints > parseInt(orderTotal)){
			jAlert("Reward points should not be greater than Order total.");
			return;
		}
		$('#action').val('FULL_REWARDS');
	}
	*/

	function resetRefundInfo(){
		$('#rci_refundNote').val('');
		$('#rci_refundAmount').val('');
		$('#rci_creditWalletAmount').val('');
		$('#rci_creditAmount').val('');
		$('#rci_rewardPoints').val('');
	}