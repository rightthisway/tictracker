var flag;
var TICKET_SIZE = 0;
function doValidateReferralCode(){
	var custRowId = customerGrid.getSelectedRows([0])[0];
	if(custRowId==undefined || custRowId < 0){
		jAlert("Please Select Customer.");
		return;
	}
	var referralCode = $('#referralCode').val();
	var customerId = customerGrid.getDataItem(custRowId).customerId; 
	var isLoyalFan = false;
	var isLongSale = false;
	
	if($('#isLoyalFanPrice').val()=='Yes'){
		isLoyalFan = true;
	}
	
	var urlPayload = "customerId="+customerId+"&referralCode="+referralCode+"&orderTotal="+$('#payableAmount').val()+
	"&eventId="+$('#eventId').val()+"&ticketGroupId="+$('#ticketIdStr').val()+"&isFanPrice="+isLoyalFan;
	
	var totalQty = 0;
	if($('#isLongSale').val()=='true' || $('#isLongSale').val()==true){
		isLongSale = true;
		for(var i=0;i<TICKET_SIZE;i++){
			totalQty  = parseInt(totalQty) + parseInt($('#ticketCount_'+i).val());
			urlPayload += "&quantity_"+i+"="+$('#ticketCount_'+i).val();
			urlPayload += "&price_"+i+"="+$('#soldPrice_'+i).val();
		}
	}else{
		var ticketCount = $('#ticketCount_0').val();
		if(ticketCount <= 0){
			jAlert("Please Add ticketCount greater than 0");
			return;
		}
		urlPayload += "&price="+$('#soldPrice_0').val()+"&quantity="+ticketCount;
	}
	urlPayload += "&isLongSale="+isLongSale+"&totalQty="+totalQty+"&ticketSize="+TICKET_SIZE;
	
	if($('#referralCode').val() != null && $('#referralCode').val() != ''){	
	$.ajax({
			url : "/Accounting/ValidateReferralCode",
			type : "get",
			data :urlPayload,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData != '' && jsonData.status==1){
					if(isLongSale == true){
						for(var i=0;i<TICKET_SIZE;i++){
							$('#soldPrice_'+i).val(jsonData.price[i]);
						}
						$('#promoTrackingId').val(jsonData.promoTrackingId);
						$('#affPromoTrackingId').val(jsonData.affPromoTrackingId);
					}else{
						$('#soldPrice_0').val(jsonData.price);
						$('#promoTrackingId').val('');
						$('#affPromoTrackingId').val('');
					}
					$('#payableAmount').val(jsonData.orderTotal);
					$('#totalPayableAmount').val(jsonData.orderTotal);
					$('#earnedRewardPoints').text(jsonData.rewardPoints);
					$('#earnRewardPoint').val(jsonData.rewardPoints);
					soldPriceChange(false);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){				
				jAlert("Your login session is expired please refresh page and login again.", "Error");
			}
	});
	}else{
		$('#promoTrackingId').val('');
		$('#affPromoTrackingId').val('');
	}
}

function validateCreditCard(){
	if($('#cardNumber').val() == null || $('#cardNumber').val() == "" ){
		  jAlert("Please enter valid card number.");
		  return false;
	  }
	  
	  if($('#expMonth').val() == null || $('#expMonth').val() == "" ){
		  jAlert("Please enter expiry month.");
		  return false;
	  }
	  
	  if($('#expYear').val() == null || $('#expYear').val() == "" ){
		  jAlert("Please enter expiry year.");
		  return false;
	  }
	  
	 /*  if($('#cvv').val() == null || $('#cvv').val() == "" ){
		  jAlert("Please enter valid cvv.");
		  return false;
	  } */
	  return true;
}

var flags;
function doSaveInvoice(){
	$('#createInvoiceBtn').attr('disabled','disabled');
	setTimeout('$("#createInvoiceBtn").removeAttr("disabled")', 10000);
	flags="";
	if($('#bAddressId').val() == null || $('#bAddressId').val() == ''){
		jAlert("Billing address is mandatory.");
		return false;
	}
	
	
	if($('#sAddressId').val() == null || $('#sAddressId').val() == ''){
		jAlert("Selected customer does not have Shipping address, please select Shipping same As Billing Address.");
		return false;
	}
	 var paymentMethod = $('#paymentMethod').val();
	 var savedCardId = $('#custCards').val();
	 var requiredAmount =  parseFloat($('#payableAmount').val());
	 
	 if(paymentMethod == null ){
		 jAlert("Please choose payment method.");
		 return false;
	 }else if (paymentMethod == 'CREDITCARD'){
		 if(null == savedCardId || savedCardId == 0){
		 	if(validateCreditCard()){
		 		 $("#sBySavedCard").val("NO");
				  var expMonth = $('#expMonth').val();
			      var expYear = $('#expYear').val();
			      expYear = expYear.substring(2,4);
			      var stripExp = expMonth+"/"+expYear;
			      $('#stripeExp').val(stripExp);
			      var cvv = $('#cvv').val();
			      
				  if (Stripe.card.validateCardNumber($('#cardNumber').val())
				      && Stripe.card.validateExpiry($('#stripeExp').val())
				      && Stripe.card.validateCVC($('#cvv').val())) {
				      Stripe.createToken({
				             name: $('#name').val(),
				             number: $('#cardNumber').val(),
				             exp: stripExp,
				             cvc: $('#cvv').val()
				         }, stripeResponseHandler);
				      
			      } else {
			    	  jAlert("Please enter valid card information.");
			  	  	  return false;
			      }
		 	}else{
		 		return false;
		 	}
		 }else{
			 $("#sBySavedCard").val("YES");
			 $("#sSavedCardId").val(savedCardId);
			 $("#createInvoiceAction").val('saveInvoice');
			 saveInvoiceCall();
		 }
	 }else if(paymentMethod == 'REWARDPOINTS'){
		  var total = parseFloat($('#customerRewardPoints').val());
		  if(total<requiredAmount){
			  jAlert("Selected customer have only "+total+" active reward points.");
			  return false;
		  }
		  $("#rewardPoints").val(requiredAmount);
		  $("#createInvoiceAction").val('saveInvoice');
		  saveInvoiceCall();
	 }else if(paymentMethod == 'WALLET'){
	 	var walletCredit = parseFloat($('#walletCredit').val());
	 	if(walletCredit< requiredAmount){
	 		jAlert("Selected customer have only "+walletCredit+" Wallet Credit.");
			return false;
	 	}
		$("#createInvoiceAction").val('saveInvoice');
		saveInvoiceCall();
	 }else if(paymentMethod == 'ACCOUNT_RECIVABLE'){
		$("#createInvoiceAction").val('saveInvoice');
		saveInvoiceCall();
	}else if(paymentMethod == 'WALLET_CREDITCARD'){
		 var walletCredit = parseFloat($('#walletCredit').val());
		 var cardAmount = parseFloat($('#cardAmount').val());
		 var walletAmount = parseFloat($('#walletAmount').val());
		 if(cardAmount<=0 || isNaN(cardAmount)){
			 jAlert("Please Enter Valid Card Amount, should be greater than 0.");
			 return false;
		 }
		 if(walletAmount <= 0 || isNaN(walletAmount)){
			 jAlert("Please Enter valid Wallet Amount, Should be greater than 0.");
			 return false;
		 }
		 if(walletAmount > walletCredit){
			 jAlert("Customer have only "+walletCredit+" Wallet Amount.");
			 return false;
		 }
		 var total = (cardAmount + walletAmount).toFixed(2);
		 
		 if(requiredAmount != total){
		 	jAlert("Wallet Amount and Card Amount should be equal to "+requiredAmount);
			return false;
		 }
		 if(null == savedCardId || savedCardId == 0){
			 if(validateCreditCard()){
				 $("#sBySavedCard").val("NO");
				  var expMonth = $('#expMonth').val();
			      var expYear = $('#expYear').val();
			      expYear = expYear.substring(2,4);
			      var stripExp = expMonth+"/"+expYear;
			      $('#stripeExp').val(stripExp);
			      var cvv = $('#cvv').val();
			      
				  if (Stripe.card.validateCardNumber($('#cardNumber').val())
				      && Stripe.card.validateExpiry($('#stripeExp').val())
				      && Stripe.card.validateCVC($('#cvv').val())) {
				      Stripe.createToken({
				             name: $('#name').val(),
				             number: $('#cardNumber').val(),
				             exp: stripExp,
				             cvc: $('#cvv').val()
				         }, stripeResponseHandler);
				      
			      } else {
			    	  jAlert("Please enter valid card information.");
			  	  	  return false;
			      }
			 }else{
				 return false;
			 }
		 }else{
			 $("#sBySavedCard").val("YES");
			 $("#sSavedCardId").val(savedCardId);
			 $("#createInvoiceAction").val('saveInvoice');
				saveInvoiceCall();
		 }
	 }else if (paymentMethod == 'PARTIAL_REWARD_CC'){
		 var cardAmount = parseFloat($('#cardAmount').val());
		 var rewardPoints = parseFloat($('#rewardPoints').val());
		 var customerRewardPoints = parseFloat($('#customerRewardPoints').val());
		 if(cardAmount <= 0 || isNaN(cardAmount)){
			  jAlert("Please enter vaid Card amount, Should be greater than 0.");
			  return false;
		  }
		  if(rewardPoints <= 0 || isNaN(rewardPoints)){
			  jAlert("Please enter valid reward point, Should be greater than 0.");
			  return false;
		  }
		  if(rewardPoints > customerRewardPoints){
			  jAlert("Selected customer have only "+customerRewardPoints+" active reward points.");
			  return false;
		  }
		  var total = (rewardPoints + cardAmount).toFixed(2);
		 
		  if(total!=requiredAmount){
			  jAlert("Reward points and Card amount total should be equals to "+requiredAmount);
			  return false;
		  }
		 if(null == savedCardId || savedCardId == 0){
			 if(validateCreditCard()){
				 $("#sBySavedCard").val("NO");
				  var expMonth = $('#expMonth').val();
			      var expYear = $('#expYear').val();
			      expYear = expYear.substring(2,4);
			      var stripExp = expMonth+"/"+expYear;
			      $('#stripeExp').val(stripExp);
			      var cvv = $('#cvv').val();
			      
				  if (Stripe.card.validateCardNumber($('#cardNumber').val())
				      && Stripe.card.validateExpiry($('#stripeExp').val())
				      && Stripe.card.validateCVC($('#cvv').val())) {
				      Stripe.createToken({
				             name: $('#name').val(),
				             number: $('#cardNumber').val(),
				             exp: stripExp,
				             cvc: $('#cvv').val()
				         }, stripeResponseHandler);
				      
			      } else {
			    	  jAlert("Please enter valid card information.");
			  	  	  return false;
			      }
		 	 }else{
		 		 return false; 
		 	 }
		 }else{
			 $("#sBySavedCard").val("YES");
			 $("#sSavedCardId").val(savedCardId);
			 $("#createInvoiceAction").val('saveInvoice');
			 saveInvoiceCall();
		 }
	 }else if (paymentMethod == 'PARTIAL_REWARD_WALLET'){
		 var rewardPoints = parseFloat($('#rewardPoints').val());
		 var walletCredit = parseFloat($('#walletCredit').val());
		 var walletAmount = parseFloat($('#walletAmount').val());
		 var customerRewardPoints = parseFloat($('#customerRewardPoints').val());
		 var cardAmount = parseFloat($('#cardAmount').val());
		 if(rewardPoints <= 0 || isNaN(rewardPoints)){
			  jAlert("Please enter valid reward points, Should be greater than 0.");
			  return false;
		 }
		 if(walletAmount <= 0 || isNaN(walletAmount)){
			  jAlert("Please enter valid Wallet amount, Should be greater than 0.");
			  return false;
		 }
		 if(rewardPoints > customerRewardPoints){
			  jAlert("Selected customer have only "+customerRewardPoints+" active reward points.");
			  return false;
		 }
		 if(walletAmount > walletCredit){
			  jAlert("Selected customer have only "+customerRewardPoints+" Wallet Amount.");
			  return false;
		 }
		 var total = (walletAmount + rewardPoints).toFixed(2);
		 if(total!=requiredAmount){
			  jAlert("Reward points and Wallet amount total should be equals to "+requiredAmount);
			  return false;
		 }
		 $("#createInvoiceAction").val('saveInvoice');
		 saveInvoiceCall();
	 }else if (paymentMethod == 'PARTIAL_REWARD_WALLET_CC'){
		 var cardAmount = parseFloat($('#cardAmount').val());
		 var rewardPoints = parseFloat($('#rewardPoints').val());
		 var walletCredit = parseFloat($('#walletCredit').val());
		 var walletAmount = parseFloat($('#walletAmount').val());
		 var customerRewardPoints = parseFloat($('#customerRewardPoints').val());
		 if(cardAmount<=0 || isNaN(cardAmount)){
			 jAlert("Please enter valid card amount, Should be greater than 0.");
			 return false;
		 }
		 if(rewardPoints <= 0 || isNaN(rewardPoints)){
			  jAlert("Please enter valid reward points, Should be greater than 0.");
			  return false;
		 }
		 if(walletAmount <= 0 || isNaN(walletAmount)){
			  jAlert("Please enter valid wallet amount, Should be greater than 0.");
			  return false;
		 }
		 if(rewardPoints > customerRewardPoints){
			  jAlert("Selected customer have only "+customerRewardPoints+" active reward points.");
			  return false;
		 }
		 if(walletAmount > walletCredit){
			  jAlert("Selected customer have only "+customerRewardPoints+" Wallet Amount.");
			  return false;
		 }
		 var total = (walletAmount + rewardPoints + cardAmount).toFixed(2);
		 if(total!=requiredAmount){
			  jAlert("Reward points + Wallet amount + Card amount total should be equals to "+requiredAmount);
			  return false;
		 }
		 if(null == savedCardId || savedCardId == 0){
			 if(validateCreditCard()){
				 $("#sBySavedCard").val("NO");
				  var expMonth = $('#expMonth').val();
			      var expYear = $('#expYear').val();
			      expYear = expYear.substring(2,4);
			      var stripExp = expMonth+"/"+expYear;
			      $('#stripeExp').val(stripExp);
			      var cvv = $('#cvv').val();
			      
				  if (Stripe.card.validateCardNumber($('#cardNumber').val())
				      && Stripe.card.validateExpiry($('#stripeExp').val())
				      && Stripe.card.validateCVC($('#cvv').val())) {
				      Stripe.createToken({
				             name: $('#name').val(),
				             number: $('#cardNumber').val(),
				             exp: stripExp,
				             cvc: $('#cvv').val()
				         }, stripeResponseHandler);
				      
			      } else {
			    	  jAlert("Please enter valid card information.");
			  	  	  return false;
			      }
		 	 }else{
		 		 return false; 
		 	 }
		 }else{
			 $("#sBySavedCard").val("YES");
			 $("#sSavedCardId").val(savedCardId);
			 $("#createInvoiceAction").val('saveInvoice');
			 saveInvoiceCall();
		 }
	 }else{
		  return false; 
	 }
	}
	function saveInvoiceCall(){
		var type = $('#createInvoiceType').val();
		var saveInvoiceUrl = '';
		if(type=='CATEGORY'){
			saveInvoiceUrl = "/Invoice/SaveInvoice";
		}else if(type=='REAL'){
			saveInvoiceUrl = "/Invoice/SaveInvoiceRealTicket";
		}else if(type == 'HOLD'){
			saveInvoiceUrl = "/Invoice/SaveInvoiceHoldTicket";
		}else{
			jAlert('Could not identify ticket type.');
			return;
		}
		
		$.ajax({
			url :saveInvoiceUrl,
			type : "post",
			data : $("#invoiceForm").serialize(),
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status==1){
					getCategoryTicketGroupsforEvent($('#createInvoiceEventId').val());
					resetCreateInvoiceData();
					$('#create-invoice').modal('hide');
					clearAllSelections();
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	function stripeResponseHandler(status, response) {
	    if (response.error) {
	    	jAlert(response.error.message);
	    	return false;
	    } else {
	        $("#sBySavedCard").val("NO");
	        $("#sTempToken").val(response.id);
	        $("#createInvoiceAction").val('saveInvoice');
		    saveInvoiceCall();
	        return true;
	    }
	  };

	//Invoice cancel createInvoiceAction
	function cancelAction(){
		window.close();
	}
	
	function editAddr(id){
		jAlert('edit action'+id);
		 var currentTD = $(this).parents('tr').find('td');
         if ($(this).html() == 'Edit') {
             currentTD = $(this).parents('tr').find('td');
             $.each(currentTD, function () {
                 $(this).prop('contenteditable', true);
             });
         }else {
             $.each(currentTD, function () {
                  $(this).prop('contenteditable', false);
              });
         }
         
         $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit');
         
	}
	
	function calculateRewardPoints(){
		var payAmount = parseFloat($('#payableAmount').val());
		var earnPoint = (payAmount * 0.10).toFixed(2);
		$('#earnedRewardPoints').text(earnPoint);
		$('#earnRewardPoint').val(earnPoint);
	}
	
	function calRewardPoints(){
		var paymentMethod = $('#paymentMethod').val();
		if(paymentMethod=='PARTIAL_REWARD_CC'){
			var amt = $('#cardAmount').val();
			if(amt!= '' && !isNaN(amt)){
				var rewardpoint = (parseFloat(amt) * 0.10).toFixed(2);
				$('#earnedRewardPoints').text(rewardpoint);
				$('#earnRewardPoint').val(rewardpoint);
			}
		}else if(paymentMethod=='PARTIAL_REWARD_WALLET'){
			var amt = $('#walletAmount').val();
			if(amt!= '' && !isNaN(amt)){
				var rewardpoint = (parseFloat(amt) * 0.10).toFixed(2);
				$('#earnedRewardPoints').text(rewardpoint);
				$('#earnRewardPoint').val(rewardpoint);
			}
		}else if(paymentMethod=='PARTIAL_REWARD_WALLET_CC'){
			var amt = $('#walletAmount').val();
			var amt1 = $('#cardAmount').val();
			if(amt!= '' && amt1!= '' && !isNaN(amt) && !isNaN(amt1)){
				var rewardpoint = ((parseFloat(amt)+parseFloat(amt1))* 0.10).toFixed(2);
				$('#earnedRewardPoints').text(rewardpoint);
				$('#earnRewardPoint').val(rewardpoint);
			}
		}
	}
	
	function changePaymentMethod(){
		var paymentMethod = $('#paymentMethod').val();
		if(paymentMethod=='CREDITCARD'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').show();
			calculateRewardPoints();
		}else if(paymentMethod=='ACCOUNT_RECIVABLE'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').hide();
			calculateRewardPoints();
		}else if(paymentMethod=='WALLET' || paymentMethod=='REWARDPOINTS'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#walletAmountDiv').hide();
			if(paymentMethod=='WALLET'){
				calculateRewardPoints();
			}else{
				$('#earnedRewardPoints').text('0.00');
				$('#earnRewardPoint').val(0.00);
			}
		}else if(paymentMethod=='WALLET_CREDITCARD'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').show();
			$('#creditCardDiv').show();
			$('#walletAmountDiv').show();
			calculateRewardPoints();
		}else if(paymentMethod=='PARTIAL_REWARD_CC'){
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').show();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').show();
			$('#earnedRewardPoints').text('0.00');
			$('#earnRewardPoint').val(0.00);
		}else if(paymentMethod=='PARTIAL_REWARD_WALLET'){
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#rewardPointsDiv').show();
			$('#walletAmountDiv').show();
			$('.cardInfo').hide();
			$('#earnedRewardPoints').text('0.00');
			$('#earnRewardPoint').val(0.00);
		}else if(paymentMethod=='PARTIAL_REWARD_WALLET_CC'){
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').show();
			$('#creditCardDiv').show();
			$('#walletAmountDiv').show();
			$('#earnedRewardPoints').text('0.00');
			$('#earnRewardPoint').val(0.00);
		}
		
	}
	
	$(document).ready(function() {
		$('#searchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getCustomerGridData(0);
				return false;  
			  }
		});
		
		$("#custCards").change(function(){
			var creditCardValue = $("#custCards").val();
			if(creditCardValue != 0){
				$('#cardNumber').attr('disabled', 'disabled'); // mark it as read only
				$('#expMonth').attr('disabled', 'disabled');
				$('#expYear').attr('disabled', 'disabled');
				$('#name').attr('disabled', 'disabled');
				$('#cvv').attr('disabled', 'disabled');
			}
			if(creditCardValue == 0){
				$('#cardNumber').removeAttr('disabled');
				$('#expMonth').removeAttr('disabled');
				$('#expYear').removeAttr('disabled');
				$('#name').removeAttr('disabled');
				$('#cvv').removeAttr('disabled');
			}
		});
	});
	
	function soldPriceChange(flag){
		var totalPayableAmount=0.00;
		if($('#referralCode').val() != null && $('#referralCode').val() != '' && flag){	
			jAlert("Please fix Ticket Price and quantity first and than enter referral code.");
			$('#referralCode').val('');
			for(var i=0;i<TICKET_SIZE;i++){
				$('#soldPrice_'+i).val($('#ciTicketPrice_'+i).text());
			}
			
		}
		for(var i=0;i<TICKET_SIZE;i++){
			if($('#soldPrice_'+i).val()=='' || $('#soldPrice_'+i).val()==null || $('#soldPrice_'+i).val() <=0){
				 jAlert("Sold price cannot be less than 0.");
			}
			var soldPrice = parseFloat($('#soldPrice_'+i).val());
			var ticketCount = parseInt($('#ticketCount_'+i).val());
			var taxAmt = parseFloat($('#createInvoiceTaxAmount_'+i).val());
			var payableAmount = parseFloat((soldPrice * ticketCount)+ taxAmt).toFixed(2);
			totalPayableAmount  =  parseFloat(parseFloat(totalPayableAmount) + parseFloat(payableAmount)).toFixed(2);
		}
		
		$('#totalPayableAmount').text(totalPayableAmount);
		$('#payableAmount').val(totalPayableAmount);
		var payAmount = parseFloat(totalPayableAmount);
		var earnPoint = (payAmount * 0.10).toFixed(2);
		$('#earnedRewardPoints').text(earnPoint);
		$('#earnRewardPoint').val(earnPoint);
		calRewardPoints();
	}
	
	function resetFiltersCreateInvoice(){
		resetCustomerInfo();  //Reset Customer Shipping and Billing address
		customerGridSearchString='';
		columnFilters = {};
		getCustomerGridData(0);
	}
	
	var custPagingInfo;
	var customerGrid;
	var customerDataView;
	var customerData=[];
	var customerGridSearchString='';
	var columnFilters = {};
	var customerColumns = [
				   /*{id:"customerId", name:"Customer ID", field: "customerId", sortable: true, width: 100},*/
	               {id:"customerType", name:"Customer Type", field: "customerType", sortable: true, width: 50},
	               {id:"customerName", name:"First Name", field: "customerName", sortable: true, width: 100},
	               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
	               {id:"userId", name:"User Id", field: "userId", sortable: true, width: 100},
	               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
	               {id:"productType", name:"Product Type", field: "productType", sortable: true, width: 120},
	               {id:"client", name:"Client", field: "client", sortable: true},
	               {id:"broker", name:"Broker", field: "broker", sortable: true},
	               {id:"street1", name:"Street1", field: "street1", sortable: true},
	               {id:"street2", name:"Street2", field: "street2", sortable: true},
	               {id:"city", name:"City", field: "city", sortable: true},
	               {id:"state", name:"State", field: "state", sortable: true},
				   {id:"country", name:"Country", field: "country", sortable: true},
	               {id:"zip", name:"Zip", field: "zip", sortable: true},
	               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
	               {id: "editCol", field:"editCol", name:"Edit Customer", width:10, formatter:editFormatter}
	              ];
	              
	var customerOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25,
			showHeaderRow: true,
			headerRowHeight: 30,
			explicitInitialization: true
		};
		var customerGridSortcol = "customerName";
		var customerGridSortdir = 1;
		var percentCompleteThreshold = 0;

		/*
		function customerGridFilter(item, args) {
			var x= item["customerName"];
			if (args.customerGridSearchString  != ""
					&& x.indexOf(args.customerGridSearchString) == -1) {
				
				if (typeof x === 'string' || x instanceof String) {
					if(x.toLowerCase().indexOf(args.customerGridSearchString.toLowerCase()) == -1) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}
		*/
		function editFormatter(row,cell,value,columnDef,dataContext){  
		    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.customerId +"'/>";
		    return button;
		}
		$('.editClickableImage').live('click', function(){
		    var me = $(this), id = me.attr('id');
		    var delFlag = editCustomer(id);
		});

		function customerGridComparer(a, b) {
			var x = a[customerGridSortcol], y = b[customerGridSortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
		/*
		function customerGridToggleFilterRow() {
			customerGrid.setTopPanelVisibility(!customerGrid.getOptions().showTopPanel);
		}
		
		//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
		$("#customer_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
				.mouseover(function(e) {
					$(e.target).addClass("ui-state-hover")
				}).mouseout(function(e) {
					$(e.target).removeClass("ui-state-hover")
				});
		*/
		function pagingControl(move,id){		
				var pageNo = 0;
				if(move == 'FIRST'){
					pageNo = 0;
				}else if(move == 'LAST'){
					pageNo = parseInt(custPagingInfo.totalPages)-1;
				}else if(move == 'NEXT'){
					pageNo = parseInt(custPagingInfo.pageNum) +1;
				}else if(move == 'PREV'){
					pageNo = parseInt(custPagingInfo.pageNum)-1;
				}
				getCustomerGridData(pageNo);
		}
		
		function getCustomerGridData(pageNo) {
			$('#addTicketDiv').show();
			var searchValue = $("#searchValue").val();
			/*if(searchValue== null || searchValue == '') {
				jAlert('Enter valid key to searh Customers');
				return false;
			}*/		
			$.ajax({
				url : "/Client/ManageDetails.json",
				type : "post",
				data : $("#customerSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+customerGridSearchString,
				dataType:"json",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					custPagingInfo = jsonData.pagingInfo;
					refreshcustomerGridValues(jsonData.customers);
					var type = $('#createInvoiceType').val();
					if(type == 'HOLD'){
						var customrId = $('#holdTicketCustomerId').val();
						setTimeout(function(){ 
						setDefaultRowSelected(customrId);},10);
					}else{
						$('#customerGridDiv').show();
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
		function refreshcustomerGridValues(jsonData) {
			 $("div#divLoading").addClass('show');
			customerData = [];
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (customerData[i] = {});
				d["id"] = i;
				d["customerId"] = data.customerId;
				d["bAddressId"] = data.billingAddressId;
				d["firstName"] = data.customerName;
				d["lastName"] = data.lastName;
				d["userId"] = data.userId;
				d["customerType"] = data.customerType;
				d["customerName"] = data.customerName;
				d["client"] = data.client;
				d["broker"] = data.broker;
				d["email"] = data.customerEmail;
				d["productType"] = data.productType;
				d["street1"] = data.addressLine1;	
				d["street2"] = data.addressLine2;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["zip"] = data.zipCode;
				d["phone"] = data.phone;
			}

			customerDataView = new Slick.Data.DataView();
			customerGrid = new Slick.Grid("#ci_customer_grid", customerDataView, customerColumns, customerOptions);
			customerGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			customerGrid.setSelectionModel(new Slick.RowSelectionModel());
			if(custPagingInfo != null){
				var customerGridPager = new Slick.Controls.Pager(customerDataView, customerGrid, $("#ci_customer_pager"),custPagingInfo);
			}
			var customerGridColumnpicker = new Slick.Controls.ColumnPicker(customerColumns, customerGrid,
					customerOptions);

			// move the filter panel defined in a hidden div into customerGrid top panel
			//$("#customer_inlineFilterPanel").appendTo(customerGrid.getTopPanel()).show();

			customerGrid.onSort.subscribe(function(e, args) {
				customerGridSortdir = args.sortAsc ? 1 : -1;
				customerGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					customerDataView.fastSort(customerGridSortcol, args.sortAsc);
				} else {
					customerDataView.sort(customerGridComparer, args.sortAsc);
				}
			});
			// wire up model customers to drive the customerGrid
			customerDataView.onRowCountChanged.subscribe(function(e, args) {
				customerGrid.updateRowCount();
				customerGrid.render();
			});
			customerDataView.onRowsChanged.subscribe(function(e, args) {
				customerGrid.invalidateRows(args.rows);
				customerGrid.render();
			});
			$(customerGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
			 	customerGridSearchString='';
				 var columnId = $(this).data("columnId");
				  if (columnId != null) {
					columnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in columnFilters) {
						  if (columnId !== undefined && columnFilters[columnId] !== "") {
							  customerGridSearchString += columnId + ":" +columnFilters[columnId]+",";
						  }
						}
						getCustomerGridData(0);
					}
				  }
			 
			});
			customerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id != 'editCol'){
						$("<input type='text' class='customerHeaderSearch'>")
					   .data("columnId", args.column.id)
					   .val(columnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}			
			});
			customerGrid.init();
			
			// initialize the model after all the customers have been hooked up
			customerDataView.beginUpdate();
			customerDataView.setItems(customerData);
			/*customerDataView.setFilterArgs({
				percentCompleteThreshold : percentCompleteThreshold,
				customerGridSearchString : customerGridSearchString
			});
			customerDataView.setFilter(customerGridFilter);*/
			customerDataView.endUpdate();
			customerDataView.syncGridSelection(customerGrid, true);
			$("#gridContainer").resizable();
			
			var customerRowIndex;
			customerGrid.onSelectedRowsChanged.subscribe(function() { 
				var tempcustomerRowIndex = customerGrid.getSelectedRows([0])[0];
				if (tempcustomerRowIndex != customerRowIndex) {
					customerRowIndex = tempcustomerRowIndex;
					resetCustomerInfo();
					var customerEmail=customerGrid.getDataItem(customerRowIndex).email;
					if(customerEmail==null || customerEmail==''){
						jAlert("Invoice cannot be created, Selected customer does not have email address");
						$('#createInvoiceBtn').hide();
						return;
					}
					if(!getCustomerInfoForCreateInvoice(tempcustomerRowIndex)){
						jAlert("Invoice can not be created for selected Customer, Please add customer address.");
						$('#createInvoiceBtn').hide();
					}else{
						getCustomerShippingAddressDetails(tempcustomerRowIndex,'');
						getCustomerSavedCards(tempcustomerRowIndex);
						$('#createInvoiceBtn').show();
					}
					if($("#isBilling").prop('checked') == true){
						$("#isBilling").prop('checked', false);
					}
				}
			});
			 $("div#divLoading").removeClass('show');
		}
		
		function editCustomer(custId){
			if(custId == null){
				jAlert("There is something wrong.Please try again.");
			}else{
				getCustomerInfoForEdit(custId);
				/*var url = "/Client/ViewCustomer?custId="+custId;
				popupCenter(url, "Edit Customer Details", "1000", "800");*/
			}
		}
		
		
		function resetCustomerInfo(){
			$('#bAddressId').val('');
			$("#customerNameDiv").empty();
			$("#emailDiv").empty();
			$("#phoneDiv").empty();
			$("#addressLineDvi").empty();
			$("#countryDiv").empty();
			$("#stateDiv").empty();
			$("#cityDiv").empty();
			$("#zipCodeDiv").empty();
			$('#customerIdDiv').val('');
			$("#customerNameDiv").text('');
			$("#emailDiv").text('');
			$("#phoneDiv").text('');
			$("#addressLineDvi").text('');
			$("#countryDiv").text('');
			$("#stateDiv").text('');
			$("#cityDiv").text('');
			$("#zipCodeDiv").text('');
			
			$('#sAddressId').val('');
			$('#shAddressLine1Div').empty();
			$('#shAddressLine2Div').empty();
			$('#shPhoneDiv').empty();
			$('#shCountryDiv').empty();
			$('#shStateDiv').empty();
			$('#shCityDiv').empty();
			$('#shZipCodeDiv').empty();
		}
		
		
		//Get selected event on button click
		function getCustomerInfoForCreateInvoice(customerGridIndex) {
			var address=customerGrid.getDataItem(customerGridIndex).street1;
			var strret2 = customerGrid.getDataItem(customerGridIndex).street2;
			if(address==undefined){
				address='';
			}
			if(strret2==undefined){
				strret2='';
			}
				$("#customerIdDiv").val(customerGrid.getDataItem(customerGridIndex).customerId);
				$("#customerNameDiv").text(customerGrid.getDataItem(customerGridIndex).customerName+" "+customerGrid.getDataItem(customerGridIndex).lastName);
				$("#emailDiv").text(customerGrid.getDataItem(customerGridIndex).email);
				$("#phoneDiv").text(customerGrid.getDataItem(customerGridIndex).phone);
				$("#bAddressId").val(customerGrid.getDataItem(customerGridIndex).bAddressId);
				$("#blFirstName").val(customerGrid.getDataItem(customerGridIndex).firstName);
				$("#blLastName").val(customerGrid.getDataItem(customerGridIndex).lastName);
				$("#blEmail").val(customerGrid.getDataItem(customerGridIndex).email);
				$("#phone").val(customerGrid.getDataItem(customerGridIndex).phone);
				if(strret2!=null && strret2 != '') {
					address = address +','+strret2;
				}
				$("#addressLineDvi").text(address);
				$("#addressLine1").val(address);
				$("#addressLine2").val(strret2);
				$("#countryDiv").text(customerGrid.getDataItem(customerGridIndex).country);
				$("#country").val(customerGrid.getDataItem(customerGridIndex).country);
				$("#stateDiv").text(customerGrid.getDataItem(customerGridIndex).state);
				$("#state").val(customerGrid.getDataItem(customerGridIndex).state);
				$("#city").val(customerGrid.getDataItem(customerGridIndex).city);
				$("#cityDiv").text(customerGrid.getDataItem(customerGridIndex).city);
				$("#zipCodeDiv").text(customerGrid.getDataItem(customerGridIndex).zip);
				$("#zipCode").val(customerGrid.getDataItem(customerGridIndex).zip);
			if((address==null || address=='') && (strret2==null || strret2=='')){
				return false;
			}else{
				return true;
			}	
			
		}
		
		var shippingAddressData="";
		//Get customer shipping address details
		 function getCustomerShippingAddressDetails(selectedCustRowId,shippingAddressId){
			 var custId=customerGrid.getDataItem(selectedCustRowId).customerId;
			$.ajax({
				url : "/getShippingAddressDetails",
				type : "post",
				dataType:"json",
				data : {
					customerId : custId,
					shippingId : shippingAddressId
				},
				success : function(res){
					//shippingAddressData = JSON.parse(JSON.stringify(res));
					var jsonData = JSON.parse(JSON.stringify(res));
					shippingAddressData = jsonData.shippingAddressList;
					if(shippingAddressData!='' && shippingAddressData.length >0){
						 $.each(shippingAddressData, function(index, element) {
						  $("#sAddressId").val(element.shId);
						  $('#shAddressLine1Div').append(element.street1);
						  $('#shAddressLine1').val(element.street1);
						  $('#shAddressLine2').val(element.street2);
						  $('#shFirstName').val(element.firstName);
						  $('#shLastName').val(element.lastName);
						  $('#shCity').val(element.city);
						  $('#shState').val(element.state);
						  $('#shCountry').val(element.country);
						  $('#shZipCode').val(element.zipCode);
						  $('#shPhone').val(element.phone);
						  $('#shAddressLine2Div').append(element.street2);
						  $('#shPhoneDiv').append(element.phone);
						  $('#shCountryDiv').append(element.country);
						  $('#shStateDiv').append(element.state);
						  $('#shCityDiv').append(element.city);
						  $('#shZipCodeDiv').append(element.zipCode);
						  $('#createInvoiceBtn').show();
					});
					}else{
						$('#sAddressId').val('');
						/* jAlert("Invoice can not be created for selected Customer, please add shipping address.");
						$('#createInvoice').hide(); */
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					/* $('#createInvoice').hide(); */
					return false;
				}
			});
		} 
		
		//Get customer saved card details
		 function getCustomerSavedCards(selectedCustRowId){
			var custId=customerGrid.getDataItem(selectedCustRowId).customerId;
			$.ajax({
				url : "/GetSavedCardsRewardPointsAndLoyaFan",
				type : "post",
				dataType:"json",
				data : {
					customerId : custId,
					eventId : $('#createInvoiceEventId').val()
				},
				
				success : function(res){
					var respData = JSON.parse(JSON.stringify(res));
					var savedCardData = respData.savedCards;
					if(savedCardData!='' && savedCardData.length >0){
						 var select=document.getElementById("custCards");
						 document.getElementById("custCards").innerHTML='';
						 var opt=document.createElement('option');
						 opt.value=0;
						 opt.innerHTML="Get New Card";
						 select.appendChild(opt);
						 $.each(savedCardData, function(index, element) {
							 var cardId = element.cardId;
							 var cardType = element.cardType;
							 var cardNo = element.cardNo;
							 opt=document.createElement('option');
							 opt.value=cardId;
							 opt.innerHTML=cardType+"-"+cardNo;
							 select.appendChild(opt);	
						});
					}else{
						 var select=document.getElementById("custCards");
						 document.getElementById("custCards").innerHTML='';
						 var opt=document.createElement('option');
						 opt.value=0;
						 opt.innerHTML="Get New Card";
						 select.appendChild(opt);
					}
					$('#activeRewardPoints').text(parseFloat(respData.rewardPoints).toFixed(2));
					$('#customerRewardPoints').val(parseFloat(respData.rewardPoints).toFixed(2));
					$('#walletCreditText').text(parseFloat(respData.customerCredit).toFixed(2));
					$('#walletCredit').val(parseFloat(respData.customerCredit).toFixed(2));
					/*if(respData.superFanArtistId ==true || respData.superFanArtistId =='true'){
						$('#loyalFanPriceText').text("Yes");
						$('#isLoyalFanPrice').val("Yes");
						var totalPayableAmt = 0.00;
						for(var i=0;i<TICKET_SIZE;i++){
							$('#soldPrice_'+i).val($('#loyalFanPrice_'+i).val());
							var soldPrice = parseFloat($('#soldPrice_'+i).val());
							var ticketCount = parseInt($('#ticketCount_'+i).val());
							var taxAmt = parseFloat($('#createInvoiceTaxAmount_'+i).val());
							var payableAmount = parseFloat((soldPrice * ticketCount)+ taxAmt).toFixed(2);
							totalPayableAmt = parseFloat(parseFloat(payableAmount) + parseFloat(totalPayableAmt)).toFixed(2);
						}
						
						$('#totalPayableAmount').text(totalPayableAmt);
						$('#payableAmount').val(totalPayableAmt);
						var payAmount = parseFloat(totalPayableAmt);
						var earnPoint = (payAmount * 0.10).toFixed(2);
						$('#earnedRewardPoints').text(earnPoint);
						$('#earnRewardPoint').val(earnPoint);
					}else{*/
						$('#loyalFanPriceText').text("No");
						$('#isLoyalFanPrice').val("No");
						
						var type = $('#createInvoiceType').val();
						var totalPayableAmt = 0.00;
						for(var i=0;i<TICKET_SIZE;i++){
							if(type == 'HOLD'){
								$('#soldPrice_'+i).val($('#soldPrice_'+i).val());
							}else{
								$('#soldPrice_'+i).val($('#ticketPrice_'+i).val());
							}
							var soldPrice = parseFloat($('#soldPrice_'+i).val());
							var ticketCount = parseInt($('#ticketCount_'+i).val());
							var taxAmt = parseFloat($('#createInvoiceTaxAmount_'+i).val());
							var payableAmount = parseFloat((soldPrice * ticketCount)+ taxAmt).toFixed(2);
							totalPayableAmt = parseFloat(parseFloat(payableAmount) + parseFloat(totalPayableAmt)).toFixed(2);
						}
						
						$('#totalPayableAmount').text(totalPayableAmt);
						$('#payableAmount').val(totalPayableAmt);
						var payAmount = parseFloat(totalPayableAmt);
						var earnPoint = (payAmount * 0.10).toFixed(2);
						$('#earnedRewardPoints').text(earnPoint);
						$('#earnRewardPoint').val(earnPoint);
					//}
					if(respData.msg != null && respData.msg != "" && respData.msg != undefined){
						jAlert(respData.msg);
					}
				 }, error : function(error){
					
					 var select=document.getElementById("custCards");
					 document.getElementById("custCards").innerHTML='';
					 
					 var opt=document.createElement('option');
					 opt.value=0;
					 opt.innerHTML="Get New Card";
					 select.appendChild(opt);
				}
			});
		}
			
		//getCustomerGridData(0);
		
		function addCustomerMsg(){
			jAlert("Customer Added successfully.");
		}
		
		function changeShippingAddress(addressId){
			selectedCustRowId = customerGrid.getSelectedRows([0])[0];
			$('#shAddressLine1Div').empty();
			$('#shAddressLine2Div').empty();
			$('#shPhoneDiv').empty();
			$('#shCountryDiv').empty();
			$('#shStateDiv').empty();
			$('#shCityDiv').empty();
			$('#shZipCodeDiv').empty();
			if($("#isBilling").prop('checked') == true){
				$("#isBilling").prop('checked', false);
			}
			getCustomerShippingAddressDetails(selectedCustRowId,addressId);		
		}
		
		
		//Copy billing address
		function copyInvoiceBillingAddress(){
			if($("#isBilling").prop('checked') == true){
				var type = $('#createInvoiceType').val();
				if(type != 'HOLD'){
				  var custRowIndex = customerGrid.getSelectedRows([0])[0];
				  if(custRowIndex == null){
					  jAlert("Please select Customer first.");
					  $("#isBilling").prop('checked', false);
            }
				  }
				  var bAddressId = $("#bAddressId").val();
				  $("#sAddressId").val(bAddressId);
				  $("#shippingIsSameAsBilling").val('Yes');
				  $('#createInvoiceBtn').show();
				
				 // $("#sAddressId").val($("#bAddressId").val());
				  $('#shAddressLine1Div').text($("#addressLine1").val());
				  $('#shAddressLine1').val($("#addressLine1").val());
				  $('#shAddressLine2').val($("#addressLine2").val());
				  $('#shFirstName').val($("#blFirstName").val());
				  $('#shLastName').val($("#blLastName").val());
				  $('#shCity').val($("#city").val());
				  $('#shState').val($("#state").val());
				  $('#shCountry').val($("#country").val());
				  $('#shZipCode').val($("#zipCode").val());
				  $('#shPhone').val($("#phone").val());
				  $('#shAddressLine2Div').text($("#addressLine2").val());
				  $('#shPhoneDiv').text($("#phone").val());
				  $('#shCountryDiv').text($("#country").val());
				  $('#shStateDiv').text($("#state").val());
				  $('#shCityDiv').text($("#city").val());
				  $('#shZipCodeDiv').text($("#zipCode").val());
				  $('#createInvoiceBtn').show();
				  
			}else{
				
				$("#sAddressId").val('');
				$("#shippingIsSameAsBilling").val('No');
				
				  $('#shAddressLine1Div').text('');
				  $('#shAddressLine1').val('');
				  $('#shAddressLine2').val('');
				  $('#shFirstName').val('');
				  $('#shLastName').val('');
				  $('#shCity').val('');
				  $('#shState').val('');
				  $('#shCountry').val('');
				  $('#shZipCode').val('');
				  $('#shPhone').val('');
				  $('#shAddressLine2Div').text('');
				  $('#shPhoneDiv').text('');
				  $('#shCountryDiv').text('');
				  $('#shStateDiv').text('');
				  $('#shCityDiv').text('');
				  $('#shZipCodeDiv').text('');
				 
				  if(shippingAddressData!='' && shippingAddressData.length >0){
						 $.each(shippingAddressData, function(index, element) {
						  $("#sAddressId").val(element.shId);
						  $('#shAddressLine1Div').append(element.street1);
						  $('#shAddressLine1').val(element.street1);
						  $('#shAddressLine2').val(element.street2);
						  $('#shFirstName').val(element.firstName);
						  $('#shLastName').val(element.lastName);
						  $('#shCity').val(element.city);
						  $('#shState').val(element.state);
						  $('#shCountry').val(element.country);
						  $('#shZipCode').val(element.zipCode);
						  $('#shPhone').val(element.phone);
						  $('#shAddressLine2Div').append(element.street2);
						  $('#shPhoneDiv').append(element.phone);
						  $('#shCountryDiv').append(element.country);
						  $('#shStateDiv').append(element.state);
						  $('#shCityDiv').append(element.city);
						  $('#shZipCodeDiv').append(element.zipCode);
						  $('#createInvoiceBtn').show();
					});
				  }
			}
		}
		
		function onQuantityChange(){
			for(var i=0;i<TICKET_SIZE;i++){
				if($('#ticketCount1_'+i).val() == '' || $('#ticketCount1_'+i).val() <= 0){
					jAlert("Please add atleast one quantity.");
					$('#ticketCount1_'+i).val('');
					return;
				}
				var tic1 = parseInt($('#ciQuantity_'+i).text());
				var tic2 = parseInt($('#ticketCount1_'+i).val());
				if(tic1 < tic2){
					jAlert("Quantity should not be greater than "+tic1);
					$('#ticketCount1_'+i).val(tic1);
					return;
				}
				$('#ticketCount_'+i).val($('#ticketCount1_'+i).val());
			}
			soldPriceChange(true);
		}
		
		//Set Default Row Selected in Customer Grid - From Hold Ticket Grid
		function setDefaultRowSelected(customerId){
			var rowIndex;
			for(rowIndex = 0; customerDataView.getLength(); rowIndex++){				
				if(customerGrid.getDataItem(rowIndex).customerId == customerId){				
					customerGrid.setSelectedRows([rowIndex]);
					customerGrid.scrollRowIntoView(rowIndex);
					$('#customerGridDiv').hide();
					break;
				}
			}	
			return rowIndex;
		}
    
		function fillCreateInvoiceData(data, custId){
			customerGridSearchString = "customerId:"+custId+",";
			$('#holdTicketCustomerId').val(custId);
			//getCustomerGridData(0);
			if(data.type=='REAL'){
				$('#ticketCount1').show();
				$('#isLongSale').val('true');
				//$('#ticketCount1').val(data.catTix.quantity);
				//$('#soldPrice').val(data.catTix.price);
				$('#createInvoiceBtn').hide();
			}else if(data.type == 'CATEGORY'){
				$('#ticketCount1').hide();
				$('#isLongSale').val('false');
				$('#createInvoiceBtn').hide();
				//$('#soldPrice').val(data.catTix.price);
			}else if(data.type == 'HOLD'){
				$('#ticketCount1').hide();
				$('#isLongSale').val('true');
				$('#createInvoiceBtn').show();
				//$('#soldPrice').val(data.salePrice);
			}
			/*$('#section').val(data.catTix.section);
			$('#row').val(data.catTix.row);
			$('#ticketPrice').val(data.catTix.price);
			$('#loyalFanPrice').val(data.catTix.loyalFanPrice);
			$('#productType').val(data.catTix.productType);
			$('#shippingMethodId').val(data.shippingMethodId);
			$('#ticketId').val(data.ticketId);
			$('#ticketCount').val(data.catTix.quantity);*/
			
			$('#createInvoiceTaxAmount').val(data.catTix[0].taxAmount);			
			
			$('#createInvoiceEventId').val(data.eventDtl.eventId);
			$('#eventName').val(data.eventDtl.eventName);
			$('#eventDate').val(data.eventDtl.eventDateStr);
			$('#eventTime').val(data.eventDtl.eventTimeStr);
			$('#venueId').val(data.eventDtl.venueId);
			
			$('#eventDetailDiv').text(data.eventDtl.eventName+','+data.eventDtl.eventDateStr+','+data.eventDtl.eventTimeStr+','+data.eventDtl.building);
			$('#createInvoiceType').val(data.type);
			$('#ticketIdStr').val(data.ticketId);
			/*$('#ciSection').text(data.catTix.section);
			$('#ciRow').text(data.catTix.row);
			$('#ciProducType').text(data.catTix.productType);
			$('#ciShippingMethod').text(data.shippingMethod);
			$('#ciQuantity').text(data.catTix.quantity);
			$('#ciTicketPrice').text(data.catTix.price);
			$('#ciTaxAmount').text(data.catTix.taxAmount);*/
			
			if(data.ticketIds != null && data.ticketIds != '' && data.ticketIds != undefined){
				$('#holdTicketIds').val(data.ticketIds);
			}
			Stripe.setPublishableKey(data.sPubKey);
			//$('#createInvoiceBtn').hide();
			//setTimeout(function(){ 
			//alert(setDefaultRowSelected('340281'));},10);
			getCustomerGridData(0);
		}
		
		function resetCreateInvoiceData(){
			$('#invoiceForm')[0].reset();
			$('#invoiceForm').find('span').html('');
			resetCustomerInfo();
			$('#referralCode').val('');
			var creditCardValue = $("#custCards").val();
			if(creditCardValue == 0){
				$('#cardNumber').removeAttr('disabled');
				$('#expMonth').removeAttr('disabled');
				$('#expYear').removeAttr('disabled');
				$('#name').removeAttr('disabled');
				$('#cvv').removeAttr('disabled');
			}
		}
		
function createTicketDiv(data){
	var ticketDiv = '';
	var taxAmount =0.0;
	$('#ticketInfoDiv').empty();
	TICKET_SIZE = data.catTix.length;
	for(var i=0;i<data.catTix.length;i++){
		ticketDiv += '<input type="hidden" name="section_'+i+'" id="section_'+i+'" value="'+data.catTix[i].section+'" />'+
		'<input type="hidden" name="row_'+i+'" id="row_'+i+'" value="'+data.catTix.row+'" />'+
		'<input type="hidden" name="ticketPrice_'+i+'" id="ticketPrice_'+i+'" value="'+data.catTix[i].price+'" />'+
		'<input type="hidden" name="loyalFanPrice_'+i+'" id="loyalFanPrice_'+i+'" value="'+data.catTix[i].loyalFanPrice+'" />'+
		'<input type="hidden" name="productType_'+i+'" id="productType_'+i+'" value="'+data.catTix[i].productType+'" />'+
		'<input type="hidden" name="shippingMethodId_'+i+'" id="shippingMethodId_'+i+'" value="'+data.catTix[i].shippingMethodId+'" />'+
		'<input type="hidden" name="ticketId_'+i+'" id="ticketId_'+i+'" value="'+data.catTix[i].id+'" />'+
		'<div class="form-group">'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Section:</label>'+
		'<span style="font-size: 12px;">'+data.catTix[i].section+'</span>'+
		'</div>'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Row:</label>'+
		'<span>'+data.catTix[i].row+'</span>'+
		'</div>'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Product Type :</label>'+
		'<span>'+data.catTix[i].productType+'</span>'+
		'</div>'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Shipping Method : </label><span>'+data.catTix[i].shippingMethod+'</span>'+
		'</div>'+
		'</div>'+
		'<div class="form-group">'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Quantity :</label>'+
		'<span id="ciQuantity_'+i+'">'+data.catTix[i].quantity+'</span>';
		
		if(data.type == 'REAL'){
			ticketDiv += '<input type="text" class="form-control" value="'+data.catTix[i].quantity+'" id="ticketCount1_'+i+'" name="ticketCount1_'+i+'" onblur="onQuantityChange();">';
		}
		
		ticketDiv += '<input type="hidden" class="form-control" value="'+data.catTix[i].quantity+'" id="ticketCount_'+i+'" name="ticketCount_'+i+'">'+
		'</div>'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Price:</label><span>'+data.catTix[i].price+'</span>'+
		'</div>'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Fees Amount :</label>'+
		'<input type="text" class="form-control" id="createInvoiceTaxAmount_'+i+'" name="taxAmount_'+i+'" value="'+data.catTix[i].taxAmount+'" onchange="soldPriceChange(false)">'+
		'</div>'+
		'<div class="col-sm-3">'+
		'<label style="font-size: 13px;font-weight:bold;">Sold Price Per Ticket:</label>';
		if(data.type == 'HOLD'){
			ticketDiv += '<input type="text" class="form-control" value="'+data.salePrice+'" id="soldPrice_'+i+'" name="soldPrice_'+i+'" onblur="soldPriceChange(true);">';
		}else{
			ticketDiv += '<input type="text" class="form-control" value="'+data.catTix[i].price+'" id="soldPrice_'+i+'" name="soldPrice_'+i+'" onblur="soldPriceChange(true);">';
		}
		ticketDiv += '</div>'+
		'</div>';
		taxAmount = parseFloat(taxAmount) + parseFloat(data.catTix[i].taxAmount);
	}
	ticketDiv += '<input type="hidden" value="'+TICKET_SIZE+'" id="ticketSize" name="ticketSize">';
	$('#ticketInfoDiv').append(ticketDiv);
	soldPriceChange(true);
}

function roundUp(amount){
			return Math.ceil(amount);
		}		
		
