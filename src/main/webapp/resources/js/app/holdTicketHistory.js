var holdHistoryPagingInfo;
var holdTicketsHistoryGrid;
var holdTicketsHistoryDataView;
var holdTicketsHistoryData=[];
var holdTicketsHistorySearchString = "";
var holdHistoryColumnFilters = {};
var holdTicketsHistoryColumn = [ {id:"customerName", name:"Customer Name", field: "customerName", sortable: true},
               {id:"salePrice", name:"Sale Price", field: "salePrice", sortable: true},
               {id:"createdBy", name:"Created By", field: "createdBy", sortable: true},
               {id:"expirationDate", name:"Expiration Date", field: "expirationDate", sortable: true},
               {id:"expirationMinutes", name:"Expiration Minutes", field: "expirationMinutes", sortable: true},
               {id:"shippingMethodId", name:"Shipping Method ID", field: "shippingMethodId", sortable: true},               
               {id:"holdDate", name:"Hold Date", field: "holdDate", sortable: true},
               {id:"internalNote", name:"Internal Notes", field: "internalNote", sortable: true},
               {id:"externalNote", name:"External Notes", field: "externalNote", sortable: true},
               {id:"ticketIds", name:"Ticket ID's", field: "ticketIds", sortable: true},
               {id:"ticketGroupId", name:"Ticket Group ID", field: "ticketGroupId", sortable: true},
               {id:"externalPo", name:"External PO", field: "externalPo", sortable: true},
               {id:"status", name:"Status", field: "status", sortable: true}		   
              ];
              
var holdTicketsOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var sortcol = "customerName";
var sortdir = 1;
var percentCompleteThreshold = 0;

function holdHistoryComparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
 
function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(holdHistoryPagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(holdHistoryPagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(holdHistoryPagingInfo.pageNum)-1;
	}	
	getHoldTicketsHistoryGridData(pageNo);
}

function getHoldTicketsHistoryGridData(pageNo) {
		$.ajax({
			url : "/GetHoldTickets",
			type : "post",
			dataType: "json",
			data : "pageNo="+pageNo+"&headerFilter="+holdTicketsHistorySearchString,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status==1) {
					$('#edit-hold-ticket').modal('show');
					holdHistoryPagingInfo = jsonData.pagingInfo;
					createHoldTicketsHistoryGrid(jsonData.holdTicketsList);
				}else{
					jAlert("Something went wrong, please refresh page and try again.")
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
function createHoldTicketsHistoryGrid(jsonData) {	
	holdTicketsHistoryData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (holdTicketsHistoryData[i] = {});
			d["id"] = i;
			d["customerName"] = data.customerName;
			d["salePrice"] = data.salePrice;
			d["createdBy"] = data.cSR;
			d["expirationDate"] = data.expirationDateStr;
			d["expirationMinutes"] = data.expirationMinutes;
			d["shippingMethodId"] = data.shippingMethodId;
			d["holdDate"] = data.holdDateTimeStr;
			d["internalNote"] = data.internalNote;
			d["externalNote"] = data.externalNote;
			d["ticketIds"] = data.ticketIds;
			d["ticketGroupId"] = data.ticketGroupId;
			d["externalPo"] = data.externalPo;
			d["status"] = data.status;
		}
	}
					
	holdTicketsHistoryDataView = new Slick.Data.DataView();
	holdTicketsHistoryGrid = new Slick.Grid("#holdTicketsHistoryGrid", holdTicketsHistoryDataView, holdTicketsHistoryColumn, holdTicketsOptions);
	holdTicketsHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	holdTicketsHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(holdHistoryPagingInfo != null){
		var shippingGridPager = new Slick.Controls.Pager(holdTicketsHistoryDataView, holdTicketsHistoryGrid, $("#holdTicketsHistoyPager"),holdHistoryPagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(holdTicketsHistoryColumn, holdTicketsHistoryGrid, holdTicketsOptions);

	  /*
	  holdTicketsHistoryGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < holdTicketsHistoryDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    holdTicketsHistoryGrid.setSelectedRows(rows);
	    e.preventDefault();
	  }); */
	  holdTicketsHistoryGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      holdTicketsHistoryDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      holdTicketsHistoryDataView.sort(holdHistoryComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the holdTicketsHistoryGrid
	  holdTicketsHistoryDataView.onRowCountChanged.subscribe(function (e, args) {
	    holdTicketsHistoryGrid.updateRowCount();
	    holdTicketsHistoryGrid.render();
	  });
	  holdTicketsHistoryDataView.onRowsChanged.subscribe(function (e, args) {
	    holdTicketsHistoryGrid.invalidateRows(args.rows);
	    holdTicketsHistoryGrid.render();
	  });

		$(holdTicketsHistoryGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	holdTicketsHistorySearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				holdHistoryColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in holdHistoryColumnFilters) {
					  if (columnId !== undefined && holdHistoryColumnFilters[columnId] !== "") {
						 holdTicketsHistorySearchString += columnId + ":" +holdHistoryColumnFilters[columnId]+",";
					  }
					}
					getHoldTicketsHistoryGridData(0);
				}
			  }		 
		});
		holdTicketsHistoryGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'expirationDate' || args.column.id == 'holdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(holdHistoryColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else{	
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(holdHistoryColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}			
		});
		holdTicketsHistoryGrid.init();	  	 
		 
	  holdTicketsHistoryDataView.beginUpdate();
	  holdTicketsHistoryDataView.setItems(holdTicketsHistoryData);
	  
	  holdTicketsHistoryDataView.endUpdate();
	  holdTicketsHistoryDataView.syncGridSelection(holdTicketsHistoryGrid, true);
	  //holdTicketsHistoryDataView.refresh();
	  $("#gridContainer").resizable();
	  //holdTicketsHistoryGrid.resizeCanvas();
}	

window.onresize = function(){
	holdTicketsHistoryGrid.resizeCanvas();
}

function resetHoldHistoryFilters(){	
	holdTicketsHistorySearchString='';
	holdHistoryColumnFilters = {};
	getHoldTicketsHistoryGridData(0);
}