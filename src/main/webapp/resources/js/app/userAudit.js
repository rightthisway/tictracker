
function getUserAuditGridData(pageNo) {	
	var userId = $('#userAudit_userId').val();
	var fromDate = $('#userAudit_fromDate').val();
	var toDate = $('#userAudit_toDate').val();
	
	if(fromDate == '' || fromDate == null){
		jAlert('Please select From Date.');
		return false;
	}
	else if(toDate == '' || toDate == null){
		jAlert('Please select To Date.');
		return false;
	}
	else{
		$.ajax({
			url : "/Admin/AuditUser",
			type : "post",
			dataType: "json",
			data : "fromDate="+fromDate+"&toDate="+toDate+"&userId="+userId+"&pageNo="+pageNo+"&headerFilter="+userAuditSearchString+"&action=search",
			success : function(res){
				var jsonData = res;
				if(jsonData.status == 1){
					userAuditGridValues(jsonData.userActionList, jsonData.userAuditPagingInfo);
					$('#userAudit_fromDate').val(jsonData.fromDate);
					$('#userAudit_toDate').val(jsonData.toDate);
				}
				if(jsonData.msg != null && jsonData.msg != "") {
					jAlert(jsonData.msg);
					userAuditSearchString='';
					userAuditColumnFilters = {};
					userAuditGridValues(jsonData.userActionList, jsonData.userAuditPagingInfo);
				}				
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
}

var userAuditPagingInfo;
var userAuditGrid;
var userAuditDataView;
var userAuditData=[];
var userAuditSearchString = "";
var userAuditColumnFilters = {};
var userAuditColumns = [ {id:"userName", name:"User Name", field: "userName", sortable: true},
	   {id:"userAction", name:"Action", field: "userAction", sortable: true},
	   {id:"clientIPAddress", name:"Client IP Address", field: "clientIPAddress", sortable: true},
	   {id:"message", name:"Message", field: "message", sortable: true},
	   {id:"updatedDate", name:"Updated Date", field: "updatedDate", sortable: true}			   
	  ];
              
var userAuditOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var userAuditSortcol = "orderNo";
var userAuditSortdir = 1;
var percentCompleteThreshold = 0;

function userAuditComparer(a, b) {
	var x = a[userAuditSortcol], y = b[userAuditSortcol];
	if(!isNaN(x)){
	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
	
function userAuditGridValues(jsonData, pageInfo) {
	userAuditPagingInfo = pageInfo;	
	userAuditData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (userAuditData[i] = {});
			d["id"] = data.id;
			d["userName"] = data.userName;
			d["userAction"] = data.userAction;
			d["clientIPAddress"] = data.clientIPAddress;
			d["message"] = data.userMessage;
			d["updatedDate"] = data.timeStampStr;
		}
	}
					
	userAuditDataView = new Slick.Data.DataView();
	userAuditGrid = new Slick.Grid("#userAudit_grid", userAuditDataView, userAuditColumns, userAuditOptions);
	userAuditGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	userAuditGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(userAuditPagingInfo != null){
		var userAuditGridPager = new Slick.Controls.Pager(userAuditDataView, userAuditGrid, $("#userAudit_pager"),userAuditPagingInfo);
	}
	var userAuditColumnPicker = new Slick.Controls.ColumnPicker(userAuditColumns, userAuditGrid, userAuditOptions);

	/*userAuditGrid.onKeyDown.subscribe(function (e) {
		// select all rows on ctrl-a
		if (e.which != 65 || !e.ctrlKey) {
		  return false;
		}
		var rows = [];
		for (var i = 0; i < userAuditDataView.getLength(); i++) {
		  rows.push(i);
		}
		userAuditGrid.setSelectedRows(rows);
		e.preventDefault();
	}); */
	
	userAuditGrid.onSort.subscribe(function (e, args) {
		userAuditSortdir = args.sortAsc ? 1 : -1;
		userAuditSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
		  userAuditDataView.fastSort(userAuditSortcol, args.sortAsc);
		} else {
		  userAuditDataView.sort(userAuditComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the userAuditGrid
	userAuditDataView.onRowCountChanged.subscribe(function (e, args) {
		userAuditGrid.updateRowCount();
		userAuditGrid.render();
	});
	userAuditDataView.onRowsChanged.subscribe(function (e, args) {
		userAuditGrid.invalidateRows(args.rows);
		userAuditGrid.render();
	});

	$(userAuditGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
	 var keyCode = (e.keyCode ? e.keyCode : e.which);
		userAuditSearchString='';
		 var columnId = $(this).data("columnId");
		  if (columnId != null) {
			userAuditColumnFilters[columnId] = $.trim($(this).val());
			if(keyCode == 13) {
				for (var columnId in userAuditColumnFilters) {
				  if (columnId !== undefined && userAuditColumnFilters[columnId] !== "") {
					 userAuditSearchString += columnId + ":" +userAuditColumnFilters[columnId]+",";
				  }
				}
				getUserAuditGridData(0);
			}
		  }
	 
	});
	userAuditGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
		if(args.column.id.indexOf('checkbox') == -1){
			if(args.column.id != 'userName'){
				if(args.column.id == 'updatedDate'){
					$("<input type='text' placeholder='mm/dd/yyyy hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(userAuditColumnFilters[args.column.id])
				   .appendTo(args.node);
				}else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(userAuditColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}
		}			
	});
	userAuditGrid.init();	  	 
	 
	userAuditDataView.beginUpdate();
	userAuditDataView.setItems(userAuditData);

	userAuditDataView.endUpdate();
	userAuditDataView.syncGridSelection(userAuditGrid, true);
	userAuditDataView.refresh();
	$("#gridContainer").resizable();
	//userAuditGrid.resizeCanvas();
}	


function userAuditResetFilters(){	
	userAuditSearchString='';
	userAuditColumnFilters = {};
	getUserAuditGridData(0);
}