
	//Invoice - Add Real Ticket(PO)
	function setRealTicket(jsonData){
		$('#edit-invoice').modal('show');
		$('#ei_ticketGroups').empty();
		$('#ei_eticketTable_body').empty();
		$('#ei_qrcodeTable_body').empty();
		$('#ei_barcodeTable_body').empty();
		
		var invoiceRT = jsonData.invoice;
		$('#ei_trackingNo').val(invoiceRT.invoiceTrackingNo);
		
		if(invoiceRT.invoiceStatus != 'Voided'){
			$('#ei_btnDiv').show();
			if(invoiceRT.invoiceRealTixMap != 'YES'){
				$('#ei_btnDiv1').show();
			}
		}
		if(jsonData.expDeliveryDate != null){
			$('#ei_expDeliveryDate').val(jsonData.expDeliveryDate.expDeliveryDate);
		}
		setRealTicketShippingMethod(jsonData.shippingMethods, jsonData.openOrder);
		setRealTicketCustomerOrder(jsonData.customerOrder);
		setReatTicketShippingAddress(jsonData.shippingAddress);
		setRealTicketGroups(jsonData.ticketGroups, jsonData.customerOrder);
		setRealTicketAttachments(jsonData.eticketAttachments, jsonData.qrcodeAttachments, jsonData.barcodeAttachments);
		setRealTicketOpenOrder(jsonData.openOrder);
		$('#ei_eticketTable_count').val(jsonData.eticketCount);
		$('#ei_qrcodeTable_count').val(jsonData.qrcodeCount);
		$('#ei_barcodeTable_count').val(jsonData.barcodeCount);
		
		if($('#ei_sendTicket').is(':checked')){
			$("#ei_sendTicket").prop("checked", false);
		}
	}

	function setRealTicketShippingMethod(jsonData, openOrder){
		var shipMethodRows = "<option value='-1'>--select--</option>";
		for(var i=0;i<jsonData.length;i++){
			var  data = jsonData[i];
			if(openOrder.openOrderShippingMethodId != null && data.shippingMethodId == openOrder.openOrderShippingMethodId){
				shipMethodRows += "<option value="+data.shippingMethodId+" selected>"+data.shippingMethodName+"</option>";
			}else{
				shipMethodRows += "<option value="+data.shippingMethodId+">"+data.shippingMethodName+"</option>";
			}
		}
		$('#ei_shippingMethod').append(shipMethodRows);
	}

	function setRealTicketCustomerOrder(jsonData){
		var eventStrHeader = jsonData.eventName+","+jsonData.eventDateStr+","+jsonData.eventTimeStr+","+jsonData.venueName;
		$('#ei_eventDetailDiv').text(eventStrHeader);
		$('#ei_eventId').val(jsonData.eventId);
		$('#ei_eventName').val(jsonData.eventName);
		$('#ei_eventDate').val(jsonData.eventDateStr);
		$('#ei_eventTime').val(jsonData.eventTimeStr);
		$('#ei_primaryPayment').val(jsonData.primaryPaymentMethod);
		$('#ei_primaryPaymentAmt').val(jsonData.primaryAvailableAmt);
		$('#ei_secondaryPayment').val(jsonData.secondaryPaymentMethod);
		$('#ei_secondaryPaymentAmt').val(jsonData.secondaryAvailableAmt);
		$('#ei_thirdPayment').val(jsonData.thirdPaymentMethod);
		$('#ei_thirdPaymentAmt').val(jsonData.thirdAvailableAmt);
	}

	function setRealTicketOpenOrder(jsonData){
		if(jsonData.longSale == 'YES'){
			$('#ei_categoryTiketDiv').hide();
			$('#ei_priceDiv').hide();
			$('.checkLongInventory').hide();
			$('#ei_add_row').hide();
			$('#ei_delete_row').hide();
		}else{
			$('#ei_categoryTiketDiv').show();
			$('#ei_priceDiv').show();
			$('.checkLongInventory').show();
			$('#ei_add_row').show();
			$('#ei_delete_row').show();
		}
		$('#ei_invoiceId').val(jsonData.openOrderInvoiceId);
		$('#ei_section').val(jsonData.openOrderSection);
		$('#ei_row').val(jsonData.openOrderRow);
		$('#ei_customerName').text(jsonData.openOrderCustomerName);
		$('#ei_userName').text(jsonData.openOrderUserName);
		$('#ei_phone').text(jsonData.openOrderPhone);
		$('#ei_addressLine1').text(jsonData.openOrderAddressLine1);
		$('#ei_country').text(jsonData.openOrderCountry);
		$('#ei_state').text(jsonData.openOrderState);
		$('#ei_city').text(jsonData.openOrderCity);
		$('#ei_zipCode').text(jsonData.openOrderZipcode);
		$('#ei_productType').text(jsonData.openOrderProductType);
		$('#ei_quantity').text(jsonData.openOrderQuantity);
		$('#ei_shippingMethod_span').text(jsonData.openOrderShippingMethod);
		$('#ei_price').text(jsonData.openOrderPrice);
		$('#ei_section_span').text(jsonData.openOrderSection);
		$('#ei_row_span').text(jsonData.openOrderRow);
		
		/*var shipCompletedRows = "";
		if(jsonData.openOrderRealTixDelivered != null && !jsonData.openOrderRealTixDelivered){
			shipCompletedRows += "<option value='0' selected>No</option>";
			shipCompletedRows += "<option value='1'>Yes</option>";
		}else{
			shipCompletedRows += "<option value='0'>No</option>";
			shipCompletedRows += "<option value='1' selected>Yes</option>";
		}
		$('#ei_realTixDelivered').append(shipCompletedRows);*/
	}
	
	function setReatTicketShippingAddress(jsonData){		
		/*$('#ei_sa_addessLine1').text(jsonData.shipAddressAddressLine1);
		$('#ei_sa_addressLine2').text(jsonData.shipAddressAddressLine2);
		$('#ei_sa_phone1').text(jsonData.shipAddressPhone1);
		$('#ei_sa_country').text(jsonData.shipAddressCountry);
		$('#ei_sa_state').text(jsonData.shipAddressState);
		$('#ei_sa_city').text(jsonData.shipAddressCity);
		$('#ei_sa_zipCode').text(jsonData.shipAddressZipCode);*/
		$('#ei_sa_addessLine1').text(jsonData.shipAddrAddressLine1);
		$('#ei_sa_addressLine2').text(jsonData.shipAddrAddressLine2);
		$('#ei_sa_phone1').text(jsonData.shipAddrPhone1);
		$('#ei_sa_country').text(jsonData.shipAddrCountryName);
		$('#ei_sa_state').text(jsonData.shipAddrStateName);
		$('#ei_sa_city').text(jsonData.shipAddrCity);
		$('#ei_sa_zipCode').text(jsonData.shipAddrZipCode);
	}
	
	function setRealTicketGroups(jsonData, customerOrder){
		var ticketGroups = "";		
		if(jsonData != null && jsonData != ""){	
			var j=1;			
		for(var i=0; i<jsonData.length; i++){
			//j=1;
			var data = jsonData[i];			
			$('#ei_ticketGroups').append('<tr id="addr_'+j+'"></tr>');
			//$('#tab_logic').append('<tr id="addr_'+j+'"></tr>');
			//ticketGroups += " <tr id='addr_"+j+"'>";
			ticketGroups = "";
			ticketGroups += "<td>"+j;
			ticketGroups += "<input type='hidden' name='rowNumber' id='rowNumber_"+j+"' value='"+j+"' />";
			ticketGroups += "<input type='hidden' name='id_"+j+"' id='id_"+j+"' value="+data.ticketGroupId+" />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center' class ='checkLongInventory'>";
			ticketGroups += "<a  href='javascript:loadPOByEvent("+customerOrder.eventId+","+j+","+data.ticketGroupId+")'>Use Long Inventory</a>";
			ticketGroups += "<input type='hidden' name='ticketGroup_"+j+"' id='ticketGroup_"+j+"' value="+data.ticketGroupId+" />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
			ticketGroups += "<input name='section_"+j+"' id='section_"+j+"' type='text' readonly='readonly' value="+data.ticketGroupSection+" placeholder='Section' class='form-control input-md' />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
			ticketGroups += "<input name='row_"+j+"' id='row_"+j+"' type='text' readonly='readonly' value="+data.ticketGroupRow+" placeholder='Row' class='form-control input-md' />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
			ticketGroups += "<input name='seatLow_"+j+"' id='seatLow_"+j+"' type='text' value="+data.ticketGroupMappedSeatLow+" placeholder='Seat Low' class='form-control input-md' />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
			ticketGroups += "<input name='seatHigh_"+j+"' id='seatHigh_"+j+"' type='text' value="+data.ticketGroupMappedSeatHigh+" placeholder='Seat High' class='form-control input-md' />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
			ticketGroups += "<input name='qty_"+j+"' id='qty_"+j+"' type='text' value="+data.ticketGroupMappedQty+" placeholder='Qty' class='form-control input-md' />";
			ticketGroups += "</td><td style='font-size: 13px;' align='center'>";
			ticketGroups += "<input name='price_"+j+"' id='price_"+j+"' type='text' readonly='readonly' value="+data.ticketGroupPrice+" placeholder='Price' class='form-control input-md' />";
			ticketGroups += "</td>";//</tr>";			
			$('#addr_'+j).html(ticketGroups);
			j++;
		}
		}else{
			callAddRealTicketRow();
		}		
		$('#ei_rowCount').val($('#tab_logic tr').length-1);
		//$('#ei_ticketGroups').html(ticketGroups);
	}
	
	function setRealTicketAttachments(eticket, qrcode, barcode){
		var eTicketRows = "";
		var qrcodeRows = "";
		var barcodeRows = "";
		if(eticket != null){
			var j=1;
			for(var i=0; i<eticket.length; i++){
				var data = eticket[i];
				eTicketRows += "<tr id='ei_eticketTable_"+data.ticketAttachmentPosition+"' class='savedClass'>";
				eTicketRows += "<td>"+j+"</td>";
				eTicketRows += "<td>";
				eTicketRows += "<input type='file' id='ei_eticket_"+data.ticketAttachmentPosition+"' name='eticket_"+data.ticketAttachmentPosition+"' /><br/>";
				eTicketRows += "<a href=javascript:downloadTicketFile("+data.ticketAttachmentInvoiceId+",'"+data.ticketAttachmentType+"',"+data.ticketAttachmentPosition+")>"+data.ticketAttachmentFileName+"</a>";					
				eTicketRows += "</td></tr>";
				j++;
			}
		}
		if(qrcode != null){
			var j=1;
			for(var i=0; i<qrcode.length; i++){
				var data = qrcode[i];
				qrcodeRows += "<tr id='ei_qrcodeTable_"+data.qrcodeAttachmentPosition+"' class='savedClass'>";
				qrcodeRows += "<td>"+j+"</td>";
				qrcodeRows += "<td>";
				qrcodeRows += "<input type='file' id='ei_qrcode_"+data.qrcodeAttachmentPosition+"' name='qrcode_"+data.qrcodeAttachmentPosition+"' /><br/>";
				qrcodeRows += "<a href=javascript:downloadTicketFile("+data.qrcodeAttachmentInvoiceId+",'"+data.qrcodeAttachmentType+"',"+data.qrcodeAttachmentPosition+")>"+data.qrcodeAttachmentFileName+"</a>";					
				qrcodeRows += "</td></tr>";
				j++;
			}
		}
		if(barcode != null){
			var j=1;
			for(var i=0; i<barcode.length; i++){
				var data = barcode[i];
				barcodeRows += "<tr id='ei_barcodeTable_"+data.barcodeAttachmentPosition+"' class='savedClass'>";
				barcodeRows += "<td>"+j+"</td>";
				barcodeRows += "<td>";
				barcodeRows += "<input type='file' id='ei_barcode_"+data.barcodeAttachmentPosition+"' name='barcode_"+data.barcodeAttachmentPosition+"' /><br/>";
				barcodeRows += "<a href=javascript:downloadTicketFile("+data.barcodeAttachmentInvoiceId+",'"+data.barcodeAttachmentType+"',"+data.barcodeAttachmentPosition+")>"+data.barcodeAttachmentFileName+"</a>";					
				barcodeRows += "</td></tr>";
				j++;
			}
		}
		$('#ei_eticketTable_body').append(eTicketRows);
		$('#ei_qrcodeTable_body').append(qrcodeRows);
		$('#ei_barcodeTable_body').append(barcodeRows);
	}
	
function saveRealTicket(){
	var rowCount = $('#ei_rowCount').val(); //$('#tab_logic tr').length;
	var totalQty = 0;
	var invoiceQuntity= parseInt($('#ei_quantity').text());
	
	//$('#ei_rowCount').val($('#tab_logic tr').length);
	if(rowCount>0){
		for(var i=1;i<=rowCount;i++){
			var qty = $('#qty_'+i).val();
			if($('#ticketGroup_'+i).val() >0){
				
			}else{
				jAlert("PO is not mapped, Please Map PO for row No."+i+1);
			}
			/*if(i==1){
				totalQty = qty;
			}else{*/
				totalQty = totalQty + parseInt(qty);
			//}
			var seatLow = parseInt($('#seatLow_'+i).val());
			var seatHigh = parseInt($('#seatHigh_'+i).val());
			if(qty>0 && seatLow>0 && seatHigh>0){
				if(seatHigh < seatLow){
					jAlert("Seat High cannot be less than Seat Low, Please enter correct values.");
				}
				/*if((seatHigh-seatLow+1)!=qty){
					jAlert("SeatLow SeatHigh range is not matching entered Quantity,Please enter range same as Quantity.");
					return;
				}*/
			}else{
				jAlert("SeatLow, SeatHigh and Quntity should be greater than 0");
				return;
			}
		}
	}else{
		jConfirm("Are you Sure you want to clear mapped PO, All uploaded files also removed?","Confirm",function(r) {
			if (r) {
				$("#ei_sendTicketToCustomer").val('No');
				$("#ei_action").val('clearMapping');
				//if($('#ei_shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
				if($('#ei_shippingMethod :selected').text() != "FedEx"){
					$('#ei_trackingNo').val("");
				}
				//$("#ei_openOrderForm").submit();
				invoiceSaveBtn();
			}
		});
		return;
	}
	
	if(totalQty>invoiceQuntity){
		jAlert("Mapped total Quantity cannot be greater than invoice quantity.");
		return;
	}
	if($('#ei_shippingMethod').val()=='-1'){
		jAlert("Please select shipping method.");
		return;
	}
	
	if($('#ei_sendTicket').is(':checked')){
		jConfirm("Are you Sure you want to email ticket attachment to customer?","Confirm",function(r) {
			if (r) {
				$("#ei_sendTicketToCustomer").val('Yes');
				$("#ei_action").val('openOrderSave');
				//if($('#ei_shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
				if($('#ei_shippingMethod :selected').text() != "FedEx"){
					$('#ei_trackingNo').val("");
				}
				//$("#ei_openOrderForm").submit();
				invoiceSaveBtn();
			}
		});
	}else{
		$("#ei_sendTicketToCustomer").val('No');
		$("#ei_action").val('openOrderSave');
		//if($('#ei_shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
		if($('#ei_shippingMethod :selected').text() != "FedEx"){
			$('#ei_trackingNo').val("");
		}
		//$("#ei_openOrderForm").submit();
		invoiceSaveBtn();
	}	
}

function downloadTicketFile(invoiceId,fileType,position){
 	 //var url = "/Accounting/DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;
	 var url = apiServerUrl+"DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;
	 $('#download-frame').attr('src', url);
}

function callCloseBtnOnClick(){
	$('#edit-invoice').modal('hide');
}

var iRow = $('#tab_logic tr').length-1;
$('#ei_rowCount').val(iRow);
function callAddRealTicketRow() {
	iRow = $('#tab_logic tr').length-1;
	$('#ei_rowCount').val(iRow);
	iRow++;
	var ei_eventId = $('#ei_eventId').val();
	$('#tab_logic').append('<tr id="addr_'+iRow+'"></tr>');
	
	 $('#addr_'+iRow).html("<td>"+ (iRow) +"</td>"+
			 "<td><a href='javascript:loadPOByEvent("+ei_eventId+","+iRow+",0)'>Check Long Inventory</a>"+
			 "<input id ='ticketGroup_"+iRow+"' name='ticketGroup_"+iRow+"' type='hidden' value='' /> </td> " +
	 		"<td><input id='section_"+iRow+"' readonly='readonly' name='section_"+iRow+"' type='text' placeholder='Section' class='form-control input-md' /> </td> " +
		   "<td><input id='row_"+iRow+"' readonly='readonly' name='row_"+iRow+"' type='text' placeholder='Row'  class='form-control input-md'></td> "+
		   "<td><input id='seatLow_"+iRow+"'  name='seatLow_"+iRow+"' type='text' placeholder='Seat Low'  class='form-control input-md'></td> " +
		   "<td><input id='seatHigh_"+iRow+"' name='seatHigh_"+iRow+"' type='text' placeholder='Seat High'  class='form-control input-md'></td> " +
			"<td><input id='qty_"+iRow+"' name='qty_"+iRow+"' type='text' placeholder='Quantity'  class='form-control input-md'></td> " +
			"<td><input id='price_"+iRow+"' readonly='readonly' name='price_"+iRow+"' type='text' placeholder='Price'  class='form-control input-md'></td>" +
			"");
	$('#ei_rowCount').val(iRow);
}

function callDeleteRealTicketRow() {
	if($('#tab_logic tr').length > 1){
		var delRow = $('#tab_logic tr').length-1;
		$("#addr_"+delRow).remove();
		delRow--;
	 }
   	$('#ei_rowCount').val(delRow);
}


function addTicketUploadRow(tableName) {	
	var j = $('#'+tableName+' tr').length-1;
	j++;
	$('#'+tableName).append('<tr id="'+tableName+'_'+j+'"></tr>');
	var rowString = '';
	if(tableName=='ei_eticketTable'){
		rowString = '<td>'+ (j) +'</td>';			
		rowString += '<td> <input type="file" id="eticket_'+j+'" name="eticket_'+j+'" onchange="validateFileName(this); PreviewImage(\'etickt_'+j+'\');" />';
		rowString += '<a href="#" onclick="javascript:previewFile(eticket_'+j+')" id="prvAnchor_etickt_'+j+'"></a></td>';			
	}else if(tableName=='ei_qrcodeTable'){
		rowString = '<td>'+ (j) +'</td>';			
		rowString += '<td> <input type="file" id="qrCode_'+j+'" name="qrcode_'+j+'" onchange="validateFileName(this); PreviewImage(\'qrCod_'+j+'\');" />';
		rowString += '<a href="#" onclick="javascript:previewFile(qrCode_'+j+')" id="prvAnchor_qrCod_'+j+'"></a></td>';
	}else if(tableName=='ei_barcodeTable'){
		rowString = '<td>'+ (j) +'</td>';
		rowString += '<td> <input type="file" id="barcode_'+j+'" name="barcode_'+j+'" onchange="validateFileName(this); PreviewImage(\'barCod_'+j+'\');" />';
		rowString += '<a href="#" onclick="javascript:previewFile(barcode_'+j+')" id="prvAnchor_barCod_'+j+'"></a></td>';
	}
	$('#'+tableName+'_count').val(j);
	$('#'+tableName+'_'+j).html(rowString);
}

function PreviewImage(obj) {	
	$("#prvAnchor_"+obj).text('Preview');
};

function previewFile(obj){
	
	var filename = obj.value.split('\\').pop().split('/').pop();
	filename = filename.substring(0, filename.length);
	$('#previewFileName').text(filename);
	
	pdffile=obj.files[0];
	pdffile_url=URL.createObjectURL(pdffile);
	$('#viewer').attr('src',pdffile_url);
	
	$('#preview-ticket').modal('show');	
}
	
function deleteTicketUploadRow(tableName) {
	var count = $('#'+tableName+' tr').length-1;
	if($('#'+tableName+'_'+count).hasClass("savedClass")){
		var fileType = '';
		if(tableName=='ei_eticketTable'){
			fileType = 'ETICKET';
		}else if(tableName=='ei_barcodeTable'){
			fileType = 'BARCODE';
		}else if(tableName=='ei_qrcodeTable'){
			fileType = 'QRCODE';
		} 
		deleteFile(tableName,fileType,count);
	}else{
		$('#'+tableName+'_'+count).remove();
		$('#'+tableName+'_count').val(count-1);
	}
}

function deleteFile(tableName,fileType,position){
	jConfirm("Are you Sure you want delete attach file?","Confirm",function(r) {
		if (r) {
			$.ajax({
				url : "/Accounting/DeleteRealTixAttachment",
				type : "post",
				dataType : "json",
				data : "invoiceId="+$('#ei_invoiceId').val()+"&fileType="+fileType+"&position="+position,
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#'+tableName+'_'+position).remove();
						$('#'+tableName+'_count').val(position-1);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	});	 
}

function invoiceSaveBtn(){
	var action = $('#ei_action').val();
	var dataString = "";
	if(action == 'openOrderSave' || action == 'clearMapping'){
		var form = $('#ei_openOrderForm')[0];
		var data = new FormData(form);
	
		dataString = $("#ei_openOrderForm").serialize();
		$.ajax({
			url : "/Accounting/SaveOpenOrders",
			type : "post",
			dataType: "json",
			enctype:"multipart/form-data",
			 processData : false,
			 contentType : false,
			 cache : false,
			data : data,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#edit-invoice').modal('hide');
					getInvoiceGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				/*if(jsonData.successMessage != null){
					$('#ei_successDiv').show();
					$('#ei_successMsg').text(jsonData.successMessage);
					getInvoiceGridData(0);
				}
				if(jsonData.errorMessage != null){
					$('#ei_errorDiv').show();
					$('#ei_errorMsg').text(jsonData.errorMessage);
				}*/
				$('#ei_vo_myModal-2').modal('hide');
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			} 
		});
	}
	if(action == 'voidOpenOrder'){
		dataString = $("#ei_openOrderForm, #ei_void_openOrderForm").serialize();
		$.ajax({
			url : "/Accounting/SaveOpenOrders",
			type : "post",
			dataType: "json",
			data : dataString,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#edit-invoice').modal('hide');
					getInvoiceGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				/*if(jsonData.successMessage != null){
					$('#ei_successDiv').show();
					$('#ei_successMsg').text(jsonData.successMessage);
					getInvoiceGridData(0);
				}
				if(jsonData.errorMessage != null){
					$('#ei_errorDiv').show();
					$('#ei_errorMsg').text(jsonData.errorMessage);
				}*/
				$('#ei_vo_myModal-2').modal('hide');
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			} 
		});
	}	 
	
}
	//Void Invoice	
function doVoidInvoice(){
	$('#ei_vo_myModal-2').modal('show');
	/*jConfirm("Do you want to void this Invoice?","Confirm",function(r){		
		if(r){
			$("#ei_action").val('voidOpenOrder');
			if($('#ei_shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
				$('#ei_trackingNo').val("");
			}
			$("#ei_openOrderForm").submit();
		}else{
			return false;
		}
	});*/
}

function voidCancelBtn(){
	$('#ei_vo_myModal-2').modal('hide');
	$("#ei_vo_refundType").val("");
	$("#ei_vo_refundAmountDiv").hide();
	$("#ei_vo_walletCCDiv").hide();
	$("#ei_vo_walletDiv").hide();
	$("#ei_vo_rewardPointDiv").hide();
	$('#ei_vo_refundAmount').val('');
	$('#ei_vo_walletCCAmount').val('');
	$('#ei_vo_walletAmount').val('');
	$('#ei_vo_rewardPoint').val('');
	$("#ei_vo_msgDiv").hide();
	$('#ei_vo_msgBox').text('');
}

function loadRefundType(obj){
	var primaryPayMethod = $('#ei_primaryPayment').val();
	var secondaryPayMethod = $('#ei_secondaryPayment').val();
	var thirdPayMethod = $('#ei_thirdPayment').val();
	
	if(obj.value == 'PARTIALREFUND'){
		if(primaryPayMethod == 'CREDITCARD' || primaryPayMethod == 'PAYPAL' 
				|| primaryPayMethod == 'GOOGLEPAY' || primaryPayMethod == 'IPAY'){			
			$("#ei_vo_refundAmountDiv").show();
			$("#ei_vo_walletCCDiv").show();
			//$("#ei_vo_walletDiv").show();
		}
		else if(primaryPayMethod == 'CUSTOMER_WALLET'){
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'IPAY' || 
					secondaryPayMethod == 'GOOGLEPAY'){
				$("#ei_vo_refundAmountDiv").show();
				$("#ei_vo_walletCCDiv").show();
				$("#ei_vo_walletDiv").show();	
			}else{
				$("#ei_vo_walletDiv").show();
			}
		}
		else if(primaryPayMethod == 'FULL_REWARDS'){
			//$("#ei_vo_rewardPointDiv").show();
		}
		else if(primaryPayMethod == 'PARTIAL_REWARDS'){
			//$('#ei_vo_rewardPoint').val($('#ei_primaryPaymentAmt').val());
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'PAYPAL'
				|| secondaryPayMethod == 'IPAY' || secondaryPayMethod == 'GOOGLEPAY'){
				$("#ei_vo_refundAmountDiv").show();
				$("#ei_vo_walletCCDiv").show();
				//$("#ei_vo_walletDiv").show();
				//$("#ei_vo_rewardPointDiv").show();
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET' && thirdPayMethod == 'CREDITCARD'){
				$("#ei_vo_refundAmountDiv").show();
				$("#ei_vo_walletCCDiv").show();
				$("#ei_vo_walletDiv").show();
				//$("#ei_vo_rewardPointDiv").show();
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET'){
				$("#ei_vo_walletDiv").show();
				//$("#ei_vo_rewardPointDiv").show();
			}
			/* else if(secondaryPayMethod == 'IPAY' || secondaryPayMethod == 'GOOGLEPAY'){
				$("#ei_vo_msgDiv").show();
				$('#ei_vo_msgBox').text('Cannot refund as selected invoice payment is made using '+secondaryPayMethod);
			} */
		}
		/* else if(primaryPayMethod == 'IPAY' || primaryPayMethod == 'GOOGLEPAY'){
			$("#ei_vo_msgDiv").show();
			$('#ei_vo_msgBox').text('Cannot refund as selected invoice payment is made using '+primaryPayMethod);
		} */
	}
	else if(obj.value == 'FULLREFUND'){
		/*  if(primaryPayMethod == 'IPAY' || primaryPayMethod == 'GOOGLEPAY'){
			allDivHide();
			$("#ei_vo_msgDiv").show();
			$('#ei_vo_msgBox').text('Cannot refund as selected invoice payment is made using '+primaryPayMethod);
		} 
		else if(primaryPayMethod == 'PARTIAL_REWARDS'){
			if(secondaryPayMethod == 'IPAY' || secondaryPayMethod == 'GOOGLEPAY'){
				allDivHide();
				$("#ei_vo_msgDiv").show();
				$('#ei_vo_msgBox').text('Cannot refund as selected invoice payment is made using '+secondaryPayMethod);
			}else{
				allDivHide();
				$("#ei_vo_msgDiv").hide();
				$('#ei_vo_msgBox').text('');
			}
		}*/		
		allDivHide();
		$("#ei_vo_msgDiv").hide();
		$('#ei_vo_msgBox').text('');
	}else{
		allDivHide();
		$("#ei_vo_msgDiv").hide();
		$('#ei_vo_msgBox').text('');
	}
}

function allDivHide(){
	$("#ei_vo_refundAmountDiv").hide();
	$("#ei_vo_walletCCDiv").hide();
	$("#ei_vo_walletDiv").hide();
	$("#ei_vo_rewardPointDiv").hide();
	$('#ei_vo_refundAmount').val('');
	$('#ei_vo_walletCCAmount').val('');
	$('#ei_vo_walletAmount').val('');
	$('#ei_vo_rewardPoint').val('');
}

function voidSaveRefund(){
	var refundType = $('#ei_vo_refundType').val();
	var primaryPayMethod = $('#ei_primaryPayment').val();
	var secondaryPayMethod = $('#ei_secondaryPayment').val();	
	var thirdPayMethod = $('#ei_thirdPayment').val();
	var primaryPayAmt = parseFloat($('#ei_primaryPaymentAmt').val());
	var secondaryPayAmt = parseFloat($('#ei_secondaryPaymentAmt').val());
	var thirdPayAmt = parseFloat($('#ei_thirdPaymentAmt').val());
	var refundAmount = parseFloat($('#ei_vo_refundAmount').val());
	var walletAmt = parseFloat($('#ei_vo_walletAmount').val());
	var walletCCAmt = parseFloat($('#ei_vo_walletCCAmount').val());
	var rewardPoints = parseFloat($('#ei_vo_rewardPoint').val());
	var isValid = false;
	if(refundType=='PARTIALREFUND'){
		if(primaryPayMethod == 'CREDITCARD' || primaryPayMethod == 'PAYPAL' || primaryPayMethod == 'GOOGLEPAY' || primaryPayMethod == 'IPAY'){
			if(($('#ei_vo_refundAmount').val() != null && $('#ei_vo_refundAmount').val() != '') || ($('#ei_vo_walletCCAmount').val() != null && $('#ei_vo_walletCCAmount').val() != '')){
				if($('#ei_vo_refundAmount').val() > 0 || $('#ei_vo_walletCCAmount').val() > 0){
					if(refundAmount > primaryPayAmt){
						jAlert("Refund amount should not be greater than "+primaryPayAmt);
						return false;
					}
					else if(walletCCAmt > primaryPayAmt){
						jAlert("Credit to Wallet from paypal/cc amount should not be greater than "+primaryPayAmt);
						return false;
					}
					else if((refundAmount+walletCCAmt) > primaryPayAmt){
						jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+primaryPayAmt);
						return false;
					}else{
						isValid = true;
					}
				}else{
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
					return false;
				}
			}
			else{
				jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty");
				return false;
			}
		}
		else if(primaryPayMethod == 'CUSTOMER_WALLET'){
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'GOOGLEPAY' || secondaryPayMethod == 'IPAY'){
				if(($('#ei_vo_refundAmount').val() != null && $('#ei_vo_refundAmount').val() != '') || ($('#ei_vo_walletCCAmount').val() != null && $('#ei_vo_walletCCAmount').val() != '') || ($('#ei_vo_walletAmount').val() != null && $('#ei_vo_walletAmount').val() != '')){
					if($('#ei_vo_refundAmount').val() > 0 || $('#ei_vo_walletCCAmount').val() > 0 || $('#ei_vo_walletAmount').val() > 0){
						if(refundAmount > secondaryPayAmt){
							jAlert("Refund amount should not be greater than "+secondaryPayAmt);
							return false;
						}
						else if(walletAmt > primaryPayAmt){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPayAmt);
							return false;
						}
						else if((refundAmount+walletCCAmt) > secondaryPayAmt){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
						return false;
					}
				}
				else{
					jAlert("Refund/Credit to wallet from paypal/cc amount should not be empty.");
					return false;
				}
			}
			else{
				if($('#ei_vo_walletAmount').val() != null && $('#ei_vo_walletAmount').val() != ''){
					if($('#ei_vo_walletAmount').val() > 0){
						if(walletAmt > primaryPayAmt){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+primaryPayAmt);
							return false;			
						}else{
							isValid = true;
						}
					}else{
						jAlert("Credit to wallet from Wallet amount should be greater than 0");
						return false;
					}
				}
				else{
					jAlert("Credit to wallet from Wallet amount should not be empty.");
					return false;			
				}
			}		
		}
		else if(primaryPayMethod == 'FULL_REWARDS'){
			isValid = true;
			/*if($('#ei_vo_rewardPoint').val() != null && $('#ei_vo_rewardPoint').val() != ''){
				if(rewardPoints > primaryPayAmt){
					jAlert("Reward points should be less than or equal to "+primaryPayAmt);
					return false;
				}else{
					isValid = true;
				}
			}
			else{
				jAlert("Reward points should not be empty.");
				return false;
			}*/
		}
		else if(primaryPayMethod == 'PARTIAL_REWARDS'){
			if(secondaryPayMethod == 'CREDITCARD' || secondaryPayMethod == 'PAYPAL' || secondaryPayMethod == 'GOOGLEPAY' || secondaryPayMethod == 'IPAY'){
				if(($('#ei_vo_refundAmount').val() != null && $('#ei_vo_refundAmount').val() != '') || ($('#ei_vo_walletCCAmount').val() != null && $('#ei_vo_walletCCAmount').val() != '')){ // && ($('#ei_vo_rewardPoint').val() != null && $('#ei_vo_rewardPoint').val() != '')){
					if($('#ei_vo_refundAmount').val() > 0 || $('#ei_vo_walletCCAmount').val() > 0){
						/*if(rewardPoints > primaryPayAmt){
							jAlert("Reward points should be less than or equal to "+primaryPayAmt);
							return false;
						}
						else */
						if(refundAmount > secondaryPayAmt){
							jAlert("Refund amount should not be greater than "+secondaryPayAmt);
							return false;
						}
						else if((refundAmount+walletCCAmt) > secondaryPayAmt){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+secondaryPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from paypal/cc amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Refund/Wallet amount/Reward Points should not be empty.");
					jAlert("Refund amount/Credit to wallet from paypal/cc amount should not be empty.");
					return false;
				}
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET' && (thirdPayMethod == 'CREDITCARD' || secondaryPayMethod == 'GOOGLEPAY' || secondaryPayMethod == 'IPAY')){
				if(($('#ei_vo_refundAmount').val() != null && $('#ei_vo_refundAmount').val() !='') || ($('#ei_vo_walletCCAmount').val() != null && $('#ei_vo_walletCCAmount').val() != '')
						|| ($('#ei_vo_walletAmount').val() != null && $('#ei_vo_walletAmount').val() != '')){ //&& ($('#ei_vo_rewardPoint').val() != null && $('#ei_vo_rewardPoint').val() != '')){
					if($('#ei_vo_refundAmount').val() > 0 || $('#ei_vo_walletCCAmount').val() > 0 || $('#ei_vo_walletAmount').val() > 0){
						/*if(rewardPoints > primaryPayAmt){
							jAlert("Reward points should be less than or equal to "+primaryPayAmt);
							return false;
						}
						else */
						if(walletAmt > secondaryPayAmt){
							jAlert("Wallet amount should not be greater than "+secondaryPayAmt);
							return false;
						}
						else if(refundAmount > thirdPayAmt){
							jAlert("Refund amount should not be greater than "+thirdPayAmt);
							return false;
						}
						else if((refundAmount+walletCCAmt) > thirdPayAmt){
							jAlert("Total of (Refund amount + Credit to wallet from paypal/cc) amount should not be greater than "+thirdPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Refund amount/Credit to wallet from (paypal/cc/wallet) amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Refund/Wallet Amount/Reward Points should not be empty.");
					jAlert("Refund amount/Credit to wallet from paypal/cc Amount should not be empty.");
					return false;
				}
			}
			else if(secondaryPayMethod == 'CUSTOMER_WALLET'){
				if(($('#ei_vo_walletAmount').val() != null && $('#ei_vo_walletAmount').val() != '')){	//($('#ei_vo_rewardPoint').val() != null && $('#ei_vo_rewardPoint').val() != '') && 
					if($('#ei_vo_walletAmount').val() > 0){
						/*if(rewardPoints > primaryPayAmt){
							jAlert("Reward points should be less than or equal to "+primaryPayAmt);
							return false;
						}
						else */
						if(walletAmt > secondaryPayAmt){
							jAlert("Credit to wallet from Wallet amount should not be greater than "+secondaryPayAmt);
							return false;
						}else{
							isValid = true;
						}
					}else{
						jAlert("Credit to wallet from Wallet amount should be greater than 0");
						return false;
					}
				}
				else{
					//jAlert("Reward Points/Wallet amount should not be empty.");
					jAlert("Credit to wallet from Wallet amount should not be empty.");
					return false;
				}
			}
		}
	}else if(refundType=='NOREFUND' || refundType=='FULLREFUND'){
		isValid = true;
	}else{
		isValid = false;
		jAlert("Please select valid refund option.");
	}
	if(isValid){
		//jAlert("Pass value to Controller..");
		$("#ei_action").val('voidOpenOrder');
		//if($('#ei_shippingMethod option:selected').html().toLowerCase().indexOf("fedex") < 0){
		if($('#ei_shippingMethod :selected').text() != "FedEx"){
			$('#ei_trackingNo').val("");
		}
		//$("#ei_openOrderForm").submit();
		invoiceSaveBtn();
	}
	
}

//Invoice - Long Inventory
function loadPOByEvent(eventId,i,ticketId){
	if(ticketId==0){
		ticketId = $('#ticketGroup_'+i).val();
	}
	$.ajax({		  
		url : "/Invoice/GetLongInventoryForInvoice",
		type : "post",
		data : "eventId="+eventId+"&rowId="+i+"&ticketId="+ticketId,
		dataType:"json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
			if(jsonData != '' && jsonData.status == 1) {
				setLongInventory(jsonData);	
			}else {
				//jAlert(jsonData.msg);	
				return;
			}
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
	//popupCenter("/Invoice/GetLongInventoryForInvoice?eventId="+eventId+"&rowId="+i+"&ticketId="+ticketId, "Choose PO", "650", "500");
}

var longInventoryTktGrpId;
function setLongInventory(jsonData){
	$('#long-inventory').modal('show');
	longInventoryTktGrpId = jsonData.selectedTicket;
	$('#ei_li_rowId').val(jsonData.rowId);
	longInventoryGridValues(jsonData.ticketGroups, jsonData.pagingInfo);
}

var longInventorySelectedRow='';
var longInventoryId='';
var longInventoryDataView;
var longInventoryGrid;
var longInventoryPagingInfo;
var longInventoryData = [];
var longInventoryColumns = [ {
	id : "poId",
	name : "PO ID",
	field : "poId",
	sortable : true
},{
	id : "section",
	name : "Section",
	field : "section",
	sortable : true
},{
	id : "row",
	name : "Row",
	field : "row",
	sortable : true
},{
	id : "seatLow",
	name : "Seat Low",
	field : "seatLow",
	sortable : true
}, {
	id : "seatHigh",
	name : "Seat High",
	field : "seatHigh",
	width:80,
	sortable : true
},{
	id : "availableSeatLow",
	name : "Available SeatLow",
	field : "availableSeatLow",
	sortable : true
},{
	id : "availableSeatHigh",
	name : "Available SeatHigh",
	field : "availableSeatHigh",
	sortable : true
}, {
	id : "quantity",
	name : "Total Quantity",
	field : "quantity",
	sortable : true
}, {
	id : "availableQunatity",
	name : "Available Quantity",
	field : "availableQunatity",
	sortable : true
},{
	id : "price",
	name : "price",
	field : "price",
	sortable : true
},{
	id : "createdBy",
	name : "Created By",
	field : "createdBy",
	sortable : true
}];

var longInventoryOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var longInventorySortcol = "id";
var longInventorySortdir = 1;
var longInventorySearchString = "";
var percentCompleteThreshold = 0;

function longInventoryFilter(item, args) {
	var x= item["id"];
	if (args.longInventorySearchString  != ""
			&& x.indexOf(args.longInventorySearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.longInventorySearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function longInventoryComparer(a, b) {
	var x = a[longInventorySortcol], y = b[longInventorySortcol];
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function longInventoryGridToggleFilterRow() {
		longInventoryGrid.setTopPanelVisibility(!longInventoryGrid.getOptions().showTopPanel);
	}

	$("#longInventory_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function longInventoryGridValues(jsonData, pageInfo) {
	longInventoryPagingInfo = pageInfo;
	longInventoryData=[];	
	if(jsonData!=null && jsonData!= '' && jsonData.length > 0){
		for(var i=0;i<jsonData.length;i++){
			var  data = jsonData[i];
			var d = (longInventoryData[i] = {});
			d["id"] = i;
			d["ticketGroupId"] = data.id;
			d["poId"] = data.purchaseOrderId;
			d["section"] = data.section;
			d["row"] = data.row;
			d["seatLow"] = data.seatLow;
			d["seatHigh"] = data.seatHigh;
			d["availableSeatLow"] = data.availableSeatLow;
			d["availableSeatHigh"] = data.availableSeatHigh;
			d["quantity"] = data.quantity;
			d["availableQunatity"] = data.availableQty;
			d["price"] = data.priceStr;
			d["createdBy"] = data.createdBy;
		}
	}
				
	longInventoryDataView = new Slick.Data.DataView({
		inlineFilters : true
	});
	longInventoryGrid = new Slick.Grid("#longInventory_grid", longInventoryDataView, longInventoryColumns, longInventoryOptions);
	longInventoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	longInventoryGrid.setSelectionModel(new Slick.RowSelectionModel());		
	var longInventoryPager = new Slick.Controls.Pager(longInventoryDataView, longInventoryGrid, $("#longInventory_pager"),longInventoryPagingInfo);
	var longInventoryColumnpicker = new Slick.Controls.ColumnPicker(longInventoryColumns, longInventoryGrid,
		longInventoryOptions);
	
	// move the filter panel defined in a hidden div into eventGrid top panel
	$("#longInventory_inlineFilterPanel").appendTo(longInventoryGrid.getTopPanel()).show();
	
	longInventoryGrid.onSort.subscribe(function(e, args) {
		longInventorySortdir = args.sortAsc ? 1 : -1;
		longInventorySortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			longInventoryDataView.fastSort(longInventorySortcol, args.sortAsc);
		} else {
			longInventoryDataView.sort(longInventoryComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	longInventoryDataView.onRowCountChanged.subscribe(function(e, args) {
		longInventoryGrid.updateRowCount();
		longInventoryGrid.render();
	});
	longInventoryDataView.onRowsChanged.subscribe(function(e, args) {
		longInventoryGrid.invalidateRows(args.rows);
		longInventoryGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
	$("#longInventoryGridSearch").keyup(function(e) {
		Slick.GlobalEditorLock.cancelCurrentEdit();
		// clear on Esc
		if (e.which == 27) {
			this.value = "";
		}
		longInventorySearchString = this.value;
		updatelongInventoryGridFilter();
	});
		
	function updatelongInventoryGridFilter() {
		longInventoryDataView.setFilterArgs({
			longInventorySearchString : longInventorySearchString
		});
		longInventoryDataView.refresh();
	}
	var invenRowIndex;
	longInventoryGrid.onSelectedRowsChanged.subscribe(function() {
		var tempInvenRowIndex = longInventoryGrid.getSelectedRows([ 0 ])[0];
			if (tempInvenRowIndex != invenRowIndex) {
				invenRowIndex = tempInvenRowIndex;
				//getCustomerInfoForInvoice(tempInvenRowIndex);
			}
			
			longInventorySelectedRow = longInventoryGrid.getSelectedRows([0])[0];
			if (longInventorySelectedRow >=0) {
				longInventoryId = longInventoryGrid.getDataItem(tempInvenRowIndex).id;
			}else{
				longInventoryId='';
			}			
		});

	// initialize the model after all the events have been hooked up
	longInventoryDataView.beginUpdate();
	longInventoryDataView.setItems(longInventoryData);
	
	longInventoryDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			longInventorySearchString : longInventorySearchString
		});
	longInventoryDataView.setFilter(longInventoryFilter);
		
	longInventoryDataView.endUpdate();
	longInventoryDataView.syncGridSelection(longInventoryGrid, true);
	$("#gridContainer").resizable();
	//longInventoryGrid.resizeCanvas();
	//$("div#divLoading").removeClass('show');
	if(longInventoryTktGrpId>0){
		for(var i=0;i<longInventoryGrid.getDataLength();i++){
			if(longInventoryGrid.getDataItem(i).id==longInventoryTktGrpId){
				longInventoryGrid.setSelectedRows([i]);
				break;
			}
		}
	}	
}

function selectRecordCloseWindow(){
	if(longInventorySelectedRow>=0){
		var i = document.getElementById('ei_li_rowId').value;
		if(i>=0){
			$('#ticketGroup_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).ticketGroupId);
			$('#section_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).section);
			$('#row_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).row);
			$('#seatLow_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).availableSeatLow);
			$('#seatHigh_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).availableSeatHigh);
			$('#qty_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).availableQunatity);
			$('#price_'+i).val(longInventoryGrid.getDataItem(longInventorySelectedRow).price);
			$('#long-inventory').modal('hide');
		}else{
			jAlert("Something went wrong, Please try againg by refreshing window.");
		}		
	}else{
		jAlert("Cannot Map, No Record is selected.");
	}
}

function validateFileName(obj){
	var filename = obj.value.split('\\').pop().split('/').pop();
    filename = filename.substring(0, filename.length);
	if(filename != null && filename != ""){
		$.ajax({
			url : "/Accounting/checkFileName",
			type : "post",
			dataType: "json",
			data : "fileName="+filename,
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					jAlert("File with same name is already uploaded for following invoice.("+jsonData.invoiceId+")");
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			} 
		});
	}
}

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    }  
}