
function getRewardPointsGridData(pageNo) {
	var Message = $('#custRewards_breakUpMsg').val();
	var customerId = $('#custRewards_customerId').val();
	$.ajax({
		url : "/Client/GetCustomerInfoForRewardPoints.json",
		type : "post",
		dataType: "json",
		data : "pageNo="+pageNo+"&headerFilter="+custRewardPointsSearchString+"&Message="+Message+"&customerId="+customerId,
		success : function(res){
			var jsonData = res;
			if(jsonData.status == 1){}
			else{
				jAlert(jsonData.msg);
			}
			custRewardPointsGridValues(jsonData.rewardPointsList, jsonData.pagingInfo);			
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

var custRewardPointsPagingInfo;
var custRewardPointsGrid;
var custRewardPointsDataView;
var custRewardPointsData=[];
var custRewardPointsSearchString = "";
var custRewardPointsColumnFilters = {};
var custRewardPointsColumns = [ {id:"orderNo", name:"Order No", field: "orderNo", sortable: true},
	   {id:"orderDate", name:"Order Date", field: "orderDate", sortable: true},
	   {id:"orderType", name:"Order Type", field: "orderType", sortable: true},
	   /*{id:"availableDate", name:"Available Date", field: "availableDate", sortable: true},*/
	   {id:"rewardPoints", name:"Reward Points", field: "rewardPoints", sortable: true},
	   {id:"eventName", name:"Event Name", field: "eventName", sortable: true},
	   {id:"eventDate", name:"Event Date", field: "eventDate", sortable: true},
		{id:"eventTime", name:"Event Time", field: "eventTime", sortable: true}			   
	  ];
              
var custRewardPointsOptions = {	
    enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25,
	showHeaderRow: true,
	headerRowHeight: 30,
	explicitInitialization: true
};

var custRewardPointsSortcol = "orderNo";
var custRewardPointsSortdir = 1;
var percentCompleteThreshold = 0;

function custRewardPointsComparer(a, b) {
	var x = a[custRewardPointsSortcol], y = b[custRewardPointsSortcol];
	if(!isNaN(x)){
	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
	
function custRewardPointsGridValues(jsonData, pageInfo) {
	custRewardPointsPagingInfo = pageInfo;	
	custRewardPointsData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (custRewardPointsData[i] = {});
			d["id"] = i;
			d["orderNo"] = data.orderNo;
			d["orderDate"] = data.orderDate;
			d["orderType"] = data.orderType;
			d["rewardPoints"] = data.rewardPoint;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDate;
			d["eventTime"] = data.eventTime;
		}
	}
					
	custRewardPointsDataView = new Slick.Data.DataView();
	custRewardPointsGrid = new Slick.Grid("#custRewardPoints_grid", custRewardPointsDataView, custRewardPointsColumns, custRewardPointsOptions);
	custRewardPointsGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	custRewardPointsGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(custRewardPointsPagingInfo != null){
		var custRewardPointsGridPager = new Slick.Controls.Pager(custRewardPointsDataView, custRewardPointsGrid, $("#custRewardPoints_pager"),custRewardPointsPagingInfo);
	}
	var custRewardPointsColumnPicker = new Slick.Controls.ColumnPicker(custRewardPointsColumns, custRewardPointsGrid, custRewardPointsOptions);

	/*custRewardPointsGrid.onKeyDown.subscribe(function (e) {
		// select all rows on ctrl-a
		if (e.which != 65 || !e.ctrlKey) {
		  return false;
		}
		var rows = [];
		for (var i = 0; i < custRewardPointsDataView.getLength(); i++) {
		  rows.push(i);
		}
		custRewardPointsGrid.setSelectedRows(rows);
		e.preventDefault();
	}); */
	
	custRewardPointsGrid.onSort.subscribe(function (e, args) {
		custRewardPointsSortdir = args.sortAsc ? 1 : -1;
		custRewardPointsSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
		  custRewardPointsDataView.fastSort(custRewardPointsSortcol, args.sortAsc);
		} else {
		  custRewardPointsDataView.sort(custRewardPointsComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the custRewardPointsGrid
	custRewardPointsDataView.onRowCountChanged.subscribe(function (e, args) {
		custRewardPointsGrid.updateRowCount();
		custRewardPointsGrid.render();
	});
	custRewardPointsDataView.onRowsChanged.subscribe(function (e, args) {
		custRewardPointsGrid.invalidateRows(args.rows);
		custRewardPointsGrid.render();
	});

	$(custRewardPointsGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
	 var keyCode = (e.keyCode ? e.keyCode : e.which);
		custRewardPointsSearchString='';
		 var columnId = $(this).data("columnId");
		  if (columnId != null) {
			custRewardPointsColumnFilters[columnId] = $.trim($(this).val());
			if(keyCode == 13) {
				for (var columnId in custRewardPointsColumnFilters) {
				  if (columnId !== undefined && custRewardPointsColumnFilters[columnId] !== "") {
					 custRewardPointsSearchString += columnId + ":" +custRewardPointsColumnFilters[columnId]+",";
				  }
				}
				getRewardPointsGridData(0);
			}
		  }
	 
	});
	custRewardPointsGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
		if(args.column.id.indexOf('checkbox') == -1){			
			$("<input type='text'>")
		   .data("columnId", args.column.id)
		   .val(custRewardPointsColumnFilters[args.column.id])
		   .appendTo(args.node);
		}			
	});
	custRewardPointsGrid.init();	  	 
	 
	custRewardPointsDataView.beginUpdate();
	custRewardPointsDataView.setItems(custRewardPointsData);

	custRewardPointsDataView.endUpdate();
	custRewardPointsDataView.syncGridSelection(custRewardPointsGrid, true);
	custRewardPointsDataView.refresh();
	$("#gridContainer").resizable();
	//custRewardPointsGrid.resizeCanvas();
}	

/*function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(custRewardPointsPagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(custRewardPointsPagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(custRewardPointsPagingInfo.pageNum)-1;
	}	
	getRewardPointsGridData(pageNo);
}*/

function custRewardPointsResetFilters(){	
	custRewardPointsSearchString='';
	custRewardPointsColumnFilters = {};
	getRewardPointsGridData(0);
}