	var poPagingInfo;
	var invoicePagingInfo;
	var poDataView;
	var poGrid;
	var poData = [];
	var poColumns = [ {
		id : "id",
		name : "PO ID",
		field : "id",
		sortable : true
	},{
		id : "total",
		name : "PO Total",
		field : "total",
		sortable : true
	},{
		id : "totalQuantity",
		name : "Total Quantity",
		field : "totalQuantity",
		sortable : true
	},{
		id : "usedQunatity",
		name : "Used Qunatity",
		field : "usedQunatity",
		sortable : true
	},{
		id : "vendor",
		name : "Vendor/Brocker",
		field : "vendor",
		sortable : true
	},{
		id : "vendorCsr",
		name : "Vendor CSR",
		field : "vendorCsr",
		sortable : true
	}, {
		id : "createdOn",
		name : "Created On",
		field : "createdOn",
		width:80,
		sortable : true
	}, {
		id : "intNote",
		name : "Int Note",
		field : "intNote",
		sortable : true
	}, {
		id : "extNote",
		name : "Ext Note",
		field : "extNote",
		sortable : true
	}];

	var poOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
	};
	var poSortcol = "id";
	var poSortdir = 1;
	var poSearchString = "";

	function poFilter(item, args) {
		var x= item["id"];
		if (args.eventGridSearchString  != ""
				&& x.indexOf(args.poSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.poSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function poComparer(a, b) {
		var x = a[poSortcol], y = b[poSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

	function relatedPOGridValues(jsonData, pageInfo) {
		poPagingInfo = pageInfo;
		poData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (poData[i] = {});
				d["id"] = data.id;
				d["total"] = data.poTotal;
				d["totalQuantity"] = data.totalQuantity;
				d["usedQunatity"] = data.usedQunatity;
				d["vendor"] = data.customerName;
				d["vendorCsr"] = data.csr;
				d["createdOn"] = data.createTimeStr;
				d["intNote"] = data.internalNotes;
				d["extNote"] = data.externalNotes;
			}
		}
					
		poDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		poGrid = new Slick.Grid("#po_grid", poDataView, poColumns, poOptions);
		poGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		poGrid.setSelectionModel(new Slick.RowSelectionModel());
		var poPager = new Slick.Controls.Pager(poDataView, poGrid, $("#po_pager"),poPagingInfo);
		var poColumnpicker = new Slick.Controls.ColumnPicker(poColumns, poGrid,
			poOptions);

		poGrid.onSort.subscribe(function(e, args) {
			poSortdir = args.sortAsc ? 1 : -1;
			poSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				poDataView.fastSort(poSortcol, args.sortAsc);
			} else {
				poDataView.sort(poComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the eventGrid
		poDataView.onRowCountChanged.subscribe(function(e, args) {
			poGrid.updateRowCount();
			poGrid.render();
		});
		poDataView.onRowsChanged.subscribe(function(e, args) {
			poGrid.invalidateRows(args.rows);
			poGrid.render();
		});

		// initialize the model after all the events have been hooked up
		poDataView.beginUpdate();
		poDataView.setItems(poData);
		poDataView.endUpdate();
		poDataView.syncGridSelection(poGrid, true);
		$("#gridContainer").resizable();
		//poGrid.resizeCanvas();
	}
	
	
	var relatedInvoiceDataView;
	var relatedInvoiceGrid;
	var relatedInvoiceData = [];
	var relatedInvoiceColumns = [ {
		id : "invoiceId",
		name : "Invoice Id",
		field : "invoiceId",
		sortable : true
	},{
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		sortable : true
	},{
		id : "section",
		name : "Section",
		field : "section",
		sortable : true
	},{
		id : "row",
		name : "Row",
		field : "row",
		sortable : true
	}, {
		id : "seat",
		name : "Seat",
		field : "seat",
		width:80,
		sortable : true
	}, {
		id : "retailPrice",
		name : "Retail Price",
		field : "retailPrice",
		sortable : true
	}, {
		id : "facePrice",
		name : "Face Price",
		field : "facePrice",
		sortable : true
	}, {
		id : "cost",
		name : "Cost",
		field : "cost",
		sortable : true
	}, {
		id : "wholesalePrice",
		name : "Wholesale Price",
		field : "wholesalePrice",
		sortable : true
	}, {
		id : "actualSoldPrice",
		name : "Actual Sold Price",
		field : "actualSoldPrice",
		sortable : true
	}, {
		id : "purchasedBy",
		name : "Purchased By",
		field : "purchasedBy",
		sortable : true
	}];

	var relatedInvoiceOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
	};
	var relatedInvoiceSortcol = "id";
	var relatedInvoiceSortdir = 1;
	var relatedInvoiceSearchString = "";

	function relatedInvoiceFilter(item, args) {
		var x= item["id"];
		if (args.eventGridSearchString  != ""
				&& x.indexOf(args.relatedInvoiceSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.relatedInvoiceSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function relatedInvoiceComparer(a, b) {
		var x = a[relatedInvoiceSortcol], y = b[relatedInvoiceSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

	function relatedInvoiceGridValues(jsonData, pageInfo) {
		invoicePagingInfo = pageInfo;
		relatedInvoiceData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (relatedInvoiceData[i] = {});
				d["id"] = data.id;
				d["invoiceId"] = data.invoiceId;
				d["eventName"] = data.eventName;
				d["section"] = data.section;
				d["row"] = data.row;
				d["seat"] = data.seat;
				d["retailPrice"] = data.retailPrice;
				d["facePrice"] = data.facePrice;
				d["cost"] = data.cost;
				d["wholesalePrice"] = data.wholesalePrice;
				d["actualSoldPrice"] = data.actualSoldPrice;
				d["purchasedBy"] = data.purchasedBy;
			}
		}
						
		relatedInvoiceDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		relatedInvoiceGrid = new Slick.Grid("#invoice_grid", relatedInvoiceDataView, relatedInvoiceColumns, relatedInvoiceOptions);
		relatedInvoiceGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		relatedInvoiceGrid.setSelectionModel(new Slick.RowSelectionModel());
		var relatedInvoicePager = new Slick.Controls.Pager(relatedInvoiceDataView, relatedInvoiceGrid, $("#invoice_pager"),invoicePagingInfo);
		var relatedInvoiceColumnpicker = new Slick.Controls.ColumnPicker(relatedInvoiceColumns, relatedInvoiceGrid,
			relatedInvoiceOptions);

		relatedInvoiceGrid.onSort.subscribe(function(e, args) {
			relatedInvoiceSortdir = args.sortAsc ? 1 : -1;
			relatedInvoiceSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				relatedInvoiceDataView.fastSort(relatedInvoiceSortcol, args.sortAsc);
			} else {
				relatedInvoiceDataView.sort(relatedInvoiceComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the eventGrid
		relatedInvoiceDataView.onRowCountChanged.subscribe(function(e, args) {
			relatedInvoiceGrid.updateRowCount();
			relatedInvoiceGrid.render();
		});
		relatedInvoiceDataView.onRowsChanged.subscribe(function(e, args) {
			relatedInvoiceGrid.invalidateRows(args.rows);
			relatedInvoiceGrid.render();
		});

		// initialize the model after all the events have been hooked up
		relatedInvoiceDataView.beginUpdate();
		relatedInvoiceDataView.setItems(relatedInvoiceData);
		relatedInvoiceDataView.endUpdate();
		relatedInvoiceDataView.syncGridSelection(relatedInvoiceGrid, true);
		$("#gridContainer").resizable();
		//relatedInvoiceGrid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}