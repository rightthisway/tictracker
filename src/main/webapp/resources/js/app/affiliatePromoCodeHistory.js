
	//Promotional Code History Grid
	var promoCodeDataView;
	var promoCodePagingInfo;
	var promoCodeGrid;
	var promoCodeData = [];
	var promoCodeColumns = [ {
		id : "id",
		name : "ID",
		field : "id",
		sortable : true
	},{
		id : "promoCode",
		name : "Promotional Code",
		field : "promoCode",
		sortable : true
	}, /*{
		id : "createdDate",
		name : "Created Date",
		field : "createdDate",
		sortable : true
	},{
		id : "lastUpdate",
		name : "Last Updated",
		field : "lastUpdate",
		sortable : true
	},*/ {
		id : "status",
		name : "Status",
		field : "status",
		sortable : true
	}, {
		id : "fromDate",
		name : "From Date",
		field : "fromDate",
		sortable : true
	}, {
		id : "toDate",
		name : "To Date",
		field : "toDate",
		sortable : true
	}];

	var promoCodeOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect : false,
		topPanelHeight : 25,
	};
		
	var promoCodeSortCol = "id";
	var promoCodeSortdir = 1;
	var promoCodeSearchString = "";
	var percentCompleteThreshold = 0;

	function promoCodeFilter(item, args) {
		var x = item["id"];
		if (args.promoCodeSearchString  != ""
				&& x.indexOf(args.promoCodeSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.promoCodeSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function promoCodeComparer(a, b) {
		var x = a[promoCodeSortCol], y = b[promoCodeSortCol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

	function promoCodeGridToggleFilterRow() {
			promoCodeGrid.setTopPanelVisibility(!promoCodeGrid.getOptions().showTopPanel);
		}

	//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
		$("#promoCode_grid_toogle_search").addClass(
				"ui-state-default-sg ui-corner-all").mouseover(function(e) {
			$(e.target).addClass("ui-state-hover");
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover");
		});
		
	function promoCodeHistoryGridValues(jsonData, pageInfo) {
		promoCodePagingInfo = pageInfo;
		promoCodeData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];			
				var d = (promoCodeData[i] = {});
				d["id"] = data.id;	
				d["promoCode"] = data.promoCode;
			    /*d["createdDate"] = data.createDate;
				d["lastUpdate"] = data.lastUpdated;*/ 
				d["status"] = data.status;
				d["fromDate"] = data.effectiveFromDate;
				d["toDate"] = data.effectiveToDate;
			}
		}
		
		promoCodeDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		promoCodeGrid = new Slick.Grid("#promocode_grid", promoCodeDataView, promoCodeColumns, promoCodeOptions);
		promoCodeGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		promoCodeGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var promoCodePager = new Slick.Controls.Pager(promoCodeDataView, promoCodeGrid, $("#promocode_pager"),promoCodePagingInfo);
		var promoCodeColumnPicker = new Slick.Controls.ColumnPicker(promoCodeColumns, promoCodeGrid,
			promoCodeOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#promocode_inlineFilterPanel").appendTo(promoCodeGrid.getTopPanel()).show();
		
		promoCodeGrid.onSort.subscribe(function(e, args) {
			promoCodeSortdir = args.sortAsc ? 1 : -1;
			promoCodeSortCol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				promoCodeDataView.fastSort(promoCodeSortCol, args.sortAsc);
			} else {
				promoCodeDataView.sort(promoCodeComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the eventGrid
		promoCodeDataView.onRowCountChanged.subscribe(function(e, args) {
			promoCodeGrid.updateRowCount();
			promoCodeGrid.render();
		});
		promoCodeDataView.onRowsChanged.subscribe(function(e, args) {
			promoCodeGrid.invalidateRows(args.rows);
			promoCodeGrid.render();
		});
		
		// wire up the search textbox to apply the filter to the model
		$("#promoCodeGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			promoCodeSearchString = this.value;
			updatePromoCodeGridFilter();
		});
			
		function updatePromoCodeGridFilter() {
			promoCodeDataView.setFilterArgs({
				promoCodeSearchString : promoCodeSearchString
			});
			promoCodeDataView.refresh();
		}
		
		/*var promoCodeRowIndex;
		promoCodeGrid.onSelectedRowsChanged.subscribe(function() {
			var temprEventRowIndex = promoCodeGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != promoCodeRowIndex) {
				promoCodeRowIndex = temprEventRowIndex;
			}
			
			selectedRow = promoCodeGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = promoCodeGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});*/

		// initialize the model after all the events have been hooked up
		promoCodeDataView.beginUpdate();
		promoCodeDataView.setItems(promoCodeData);
		
		promoCodeDataView.setFilterArgs({
				percentCompleteThreshold : percentCompleteThreshold,
				promoCodeSearchString : promoCodeSearchString
			});
		promoCodeDataView.setFilter(promoCodeFilter);
			
		promoCodeDataView.endUpdate();
		promoCodeDataView.syncGridSelection(promoCodeGrid, true);
		//$("#gridContainer").resizable();
		//promoCodeGrid.resizeCanvas();
		//$("div#divLoading").removeClass('show');	
	}
