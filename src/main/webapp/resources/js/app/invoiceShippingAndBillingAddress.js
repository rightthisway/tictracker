    
	//Invoice Edit Shipping Address
	function setCustomerOrderShippingAddress(jsonData){
		$('#edit-shipping-address').modal('show');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#esa_successDiv').hide();
			$('#esa_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#esa_errorDiv').hide();
			$('#esa_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#esa_successDiv').show();
			$('#esa_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#esa_errorDiv').show();
			$('#esa_errorMsg').text(jsonData.errorMessage);
		}
		$('#esa_btnDiv').show();
		/*if(jsonData.invoiceId != null && jsonData.invoiceId != ''){
			$('#esa_fedex').show();
			$('#esa_btnDiv').hide();
			$('#esa_btnDiv1').show();			
		}else{
			$('#esa_fedex').hide();
			$('#esa_btnDiv').show();
			$('#esa_btnDiv1').hide();
		}*/
		setShippingAddress(jsonData.order);
		var customerOrder = jsonData.order;		
		var shCountryName = customerOrder.shipAddrCountryName;
		var shStateName = customerOrder.shipAddrStateName;
		updateStateCountryCombo(jsonData.countryList,esa_country,shCountryName);
		loadState(esa_country,esa_state,shStateName);
		//updateStateCountryCombo(jsonData.stateList,esa_state,shStateName);
	}
	
	function setShippingAddress(jsonData){
		$('#esa_orderId').val(jsonData.shipAddrOrderId);
		$('#esa_firstName').val(jsonData.shipAddrFirstName);
		$('#esa_lastName').val(jsonData.shipAddrLastName);
		$('#esa_street1').val(jsonData.shipAddrAddressLine1);
		$('#esa_street2').val(jsonData.shipAddrAddressLine2);
		$('#esa_email').val(jsonData.shipAddrEmail);
		$('#esa_phone').val(jsonData.shipAddrPhone1);
		$('#esa_city').val(jsonData.shipAddrCity);
		$('#esa_zipcode').val(jsonData.shipAddrZipCode);
	}
	
	function updateStateCountryCombo(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].name == listName){
					$(id).append("<option value="+jsonData[i].id+" selected>"+jsonData[i].name+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
				}
			}
		}
	}
    function validateShippingForm(){
    	if ($('#esa_firstName').val() == '') {
			jAlert("Shipping Address - FirstName is Mendatory.");
			return false;
		} else if ($('#esa_lastName').val() == '') {
			jAlert("Shipping Address - LastName is Mendatory.");
			return false;
		} else if ($('#esa_street1').val() == '') {
			jAlert("Shipping Address - Street-1 is Mendatory.");
			return false;
		} else if ($('#esa_city').val() == '') {
			jAlert("Shipping Address - City is Mendatory.");
			return false;
		} else if ($('#esa_state').val() <= 0) {
			jAlert("Shipping Address - State is Mendatory.");
			return false;
		} else if ($('#esa_country').val() <= 0) {
			jAlert("Shipping Address - Country is Mendatory.");
			return false;
		} else if ($('#esa_zipcode').val() == '') {
			jAlert("Shipping Address - Zipcode is Mendatory.");
			return false;
		} else if ($('#esa_phone').val() == '') {
			jAlert("Shipping Address - Phone is Mendatory.");
			return false;
		} else if ($('#esa_email').val() == '') {
			jAlert("Shipping Address - Email is Mendatory.");
			return false;
		}
    	return true;
    }
    
	function updateAddress(action) {
		var dataString = "";
		if(action == 'SAVE'){
			dataString = $("#updateShippingAddress").serialize();
			if(validateShippingForm() == true){
				$.ajax({
					url : "/Accounting/UpdateShippingAddress",
					type : "post",
					dataType : "json",
					data :dataString,
					success : function(res) {
						var jsonData = JSON.parse(JSON.stringify(res));						
						if(jsonData.status == 1){
							$('#edit-shipping-address').modal('hide');
							getInvoiceGridData(0);
						}
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		}else{
			dataString = $("#cf_orderShippingAddress").serialize();
			if(validateShippingFormForFedex() == true){
				$.ajax({
					url : "/Accounting/UpdateShippingAddress",
					type : "post",
					dataType : "json",
					data :dataString,
					success : function(res) {
						var jsonData = JSON.parse(JSON.stringify(res));
						if(jsonData.status == 1){
							createFedexLabelForInvoice();
						}/*else{
							jAlert(jsonData.msg);
						}*/
						if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
							jAlert(jsonData.msg);
						}
						//createFedexLabelForInvoice();
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			}
		}		
	}
	
	function loadState(countryId, stateId, findName){
		var countryId = $(countryId).val();
		if(countryId>0){
			$.ajax({
				url : "/GetStates",
				type : "post",
				dataType:"json",
				data : "countryId="+countryId,
				success : function(res){
					var jsonData = res.states;//JSON.parse(JSON.stringify(res));
					updateStateCountryCombo(jsonData,stateId,findName);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$(stateId).empty();
			$(stateId).append("<option value='-1'>--select--</option>");
		}
	}

	//CreateFedexLabel
	function setCustomerOrderFedex(jsonData){
		$('#create-fedex-label').modal('show');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#cf_successDiv').hide();
			$('#cf_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#cf_errorDiv').hide();
			$('#cf_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#cf_successDiv').show();
			$('#cf_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#cf_errorDiv').show();
			$('#cf_errorMsg').text(jsonData.errorMessage);
		}
		if(jsonData.invoiceId != null && jsonData.invoiceId != ''){
			$('#cf_invoiceId').val(jsonData.invoiceId);
			$('#cf_fedex_div').show();
			$('#cf_btnDiv').hide();
			$('#cf_btnDiv1').show();			
		}else{
			$('#cf_fedex_div').hide();
			$('#cf_btnDiv').show();
			$('#cf_btnDiv1').hide();
		}
		setShippingAddressForFedex(jsonData.order);
		var customerOrder = jsonData.order;		
		var shCountryName = customerOrder.shipAddrCountryName;
		var shStateName = customerOrder.shipAddrStateName;
		updateStateCountryCombo(jsonData.countryList,cf_country,shCountryName);
		loadState(cf_country,cf_state,shStateName);
		$('#cf_companyName').val(jsonData.customer.companyName);
	}
	
	function setShippingAddressForFedex(jsonData){
		$('#cf_orderId').val(jsonData.shipAddrOrderId);
		$('#cf_firstName').val(jsonData.shipAddrFirstName);
		$('#cf_lastName').val(jsonData.shipAddrLastName);
		$('#cf_street1').val(jsonData.shipAddrAddressLine1);
		$('#cf_street2').val(jsonData.shipAddrAddressLine2);
		$('#cf_email').val(jsonData.shipAddrEmail);
		$('#cf_phone').val(jsonData.shipAddrPhone1);
		$('#cf_city').val(jsonData.shipAddrCity);
		$('#cf_zipcode').val(jsonData.shipAddrZipCode);
	}
	
	
	function validateShippingFormForFedex(){
		if ($('#cf_firstName').val() == '') {
			jAlert("Shipping Address - FirstName is Mendatory.");
			return false;
		} else if ($('#cf_lastName').val() == '') {
			jAlert("Shipping Address - LastName is Mendatory.");
			return false;
		} else if ($('#cf_street1').val() == '') {
			jAlert("Shipping Address - Street-1 is Mendatory.");
			return false;
		} else if ($('#cf_city').val() == '') {
			jAlert("Shipping Address - City is Mendatory.");
			return false;
		} else if ($('#cf_state').val() <= 0) {
			jAlert("Shipping Address - State is Mendatory.");
			return false;
		} else if ($('#cf_country').val() <= 0) {
			jAlert("Shipping Address - Country is Mendatory.");
			return false;
		} else if ($('#cf_zipcode').val() == '') {
			jAlert("Shipping Address - Zipcode is Mendatory.");
			return false;
		} else if ($('#cf_phone').val() == '') {
			jAlert("Shipping Address - Phone is Mendatory.");
			return false;
		} else if ($('#cf_email').val() == '') {
			jAlert("Shipping Address - Email is Mendatory.");
			return false;
		}
    	return true;
	}

	function createFedexLabelForInvoice(){
		var serviceType = $('#cf_serviceType').val();
		var signatureType = $('#cf_signatureType').val();
		var companyName = $('#cf_companyName').val();
		if(serviceType == 'select'){
			jAlert("Please choose service type");
			return false;
		}		
		$.ajax({
			url : "/Accounting/CreateFedexLabel",
			type : "post",
			dataType : "json",
			data : "invoiceId=" + $('#cf_invoiceId').val()+"&serviceType="+serviceType+"&signatureType="+signatureType+"&companyName="+companyName,
			success : function(res) {
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){
					$('#create-fedex-label').modal('hide');
					getInvoiceGridData(0);
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}								
			},
			error : function(error) {
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
			});	
	}
	
	//Invoice Edit Billing Address	
	function setCustomerOrderBillingAddress(jsonData){
		$('#edit-billing-address').modal('show');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#eba_successDiv').hide();
			$('#eba_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#eba_errorDiv').hide();
			$('#eba_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#eba_successDiv').show();
			$('#eba_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#eba_errorDiv').show();
			$('#eba_errorMsg').text(jsonData.errorMessage);
		}
		setBillingAddress(jsonData.order);
		var customerOrder = jsonData.order;		
		var blCountryName = customerOrder.billAddrCountryName;
		var blStateName = customerOrder.billAddrStateName;
		updateStateCountryCombo(jsonData.countryList,eba_country,blCountryName);
		loadState(eba_country,eba_state,blStateName);
		//updateStateCountryCombo(jsonData.stateList,eba_state,blStateName);
	}
	
	function setBillingAddress(jsonData){
		$('#eba_orderId').val(jsonData.billAddrOrderId);
		$('#eba_firstName').val(jsonData.billAddrFirstName);
		$('#eba_lastName').val(jsonData.billAddrLastName);
		$('#eba_street1').val(jsonData.billAddrAddressLine1);
		$('#eba_street2').val(jsonData.billAddrAddressLine2);
		$('#eba_email').val(jsonData.billAddrEmail);
		$('#eba_phone').val(jsonData.billAddrPhone1);
		$('#eba_city').val(jsonData.billAddrCity);
		$('#eba_zipcode').val(jsonData.billAddrZipCode);
	}
	
	function updateBillingAddress() {
		if ($('#eba_firstName').val() == '') {
			jAlert("Billing Address - FirstName is Mendatory.");
			return false;
		} else if ($('#eba_lastName').val() == '') {
			jAlert("Billing Address - LastName is Mendatory.");
			return false;
		} else if ($('#eba_street1').val() == '') {
			jAlert("Billing Address - Street-1 is Mendatory.");
			return false;
		} else if ($('#eba_city').val() == '') {
			jAlert("Billing Address - City is Mendatory.");
			return false;
		} else if ($('#eba_state').val() <= 0) {
			jAlert("Billing Address - State is Mendatory.");
			return false;
		} else if ($('#eba_country').val() <= 0) {
			jAlert("Billing Address - Country is Mendatory.");
			return false;
		} else if ($('#eba_zipcode').val() == '') {
			jAlert("Billing Address - Zipcode is Mendatory.");
			return false;
		} else if ($('#eba_phone').val() == '') {
			jAlert("Billing Address - Phone is Mendatory.");
			return false;
		} else if ($('#eba_email').val() == '') {
			jAlert("Billing Address - Email is Mendatory.");
			return false;
		}
		$("#eba_action").val('update');
		//$("#updateBillingAddress").submit();
		$.ajax({
				url : "/Accounting/GetCustomerOrderBillingAddress",
				type : "post",
				dataType : "json",
				data :$("#updateBillingAddress").serialize(),
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#edit-billing-address').modal('hide');
						getInvoiceGridData(0);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					/*if(jsonData.successMessage != null){
						jAlert(jsonData.successMessage);						
					}
					if(jsonData.errorMessage != null){
						jAlert(jsonData.errorMessage);
					}*/					
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
	}