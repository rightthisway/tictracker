/**
 * PAY INVOICE
 */

	function setPayInvoice(respData){
		$('#pay-invoice').modal('show');
		var invoiceData = respData.invoice;
		$('#pinv_invoiceId').text(invoiceData.invoiceId);
		
		var cusOrderData = respData.order;
		$('#pinv_orderId_span').text(cusOrderData.orderId);
		$('#pinv_orderId').val(cusOrderData.orderId);
		$('#pinv_orderCreateDate').text(cusOrderData.orderCreateDate);
		$('#pinv_orderTotal_span').text(cusOrderData.orderTotal);
		$('#pinv_orderTotal').val(cusOrderData.orderTotal);
		$('#pinv_primaryPaymentMethod').text(cusOrderData.primaryPaymentMethod);
		$('#pinv_primaryPayAmount_span').text(cusOrderData.primaryPayAmt);
		$('#ticketId').val(cusOrderData.ticketGroupId);
		$('#payInvoiceEventId').val(cusOrderData.eventId);
		
		var customerData = respData.customer;
		$('#customerId').val(customerData.customerId);
		$('#pinv_customerName').text(customerData.customerName+" "+customerData.lastName);
		$('#pinv_customerEmail').text(customerData.email);		
		
		var savedCardData = respData.savedCards;
		if(savedCardData!='' && savedCardData.length >0){
			 var select=document.getElementById("custCards");
			 document.getElementById("custCards").innerHTML='';
			 var opt=document.createElement('option');
			 opt.value=0;
			 opt.innerHTML="Get New Card";
			 select.appendChild(opt);
			 $.each(savedCardData, function(index, element) {
				 var cardId = element.cardId;
				 var cardType = element.cardType;
				 var cardNo = element.cardNo;
				 opt=document.createElement('option');
				 opt.value=cardId;
				 opt.innerHTML=cardType+"-"+cardNo;
				 select.appendChild(opt);	
			});
		}else{
			 var select=document.getElementById("custCards");
			 document.getElementById("custCards").innerHTML='';
			 var opt=document.createElement('option');
			 opt.value=0;
			 opt.innerHTML="Get New Card";
			 select.appendChild(opt);
		}
		$('#pinv_activeRewardPoints_span').text(parseFloat(respData.rewardPoints).toFixed(2));
		$('#pinv_activeRewardPoints').val(parseFloat(respData.rewardPoints).toFixed(2));
		$('#pinv_walletCredit_span').text(parseFloat(respData.customerCredit).toFixed(2));
		$('#pinv_walletCredit').val(parseFloat(respData.customerCredit).toFixed(2));
		Stripe.setPublishableKey(respData.sPubKey);
	}
	
	function changePaymentMethod(){
		var paymentMethod = $('#paymentMethod').val();
		var paymentType = $('#paymentType').val();
		if(paymentMethod=='CREDITCARD'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').show();
			$('#bankChequeDiv').hide();
			$('#bankChequeAmtDiv').hide();
			if(paymentType == 'PARTIAL'){
				$('#cardAmountDiv').show();
			}
			//calculateRewardPoints();
		}else if(paymentMethod=='WALLET' || paymentMethod=='REWARDPOINTS'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#walletAmountDiv').hide();
			$('#bankChequeDiv').hide();
			$('#bankChequeAmtDiv').hide();
			if(paymentMethod=='WALLET'){
				//calculateRewardPoints();
				if(paymentType == 'PARTIAL'){
					$('#walletAmountDiv').show();
				}
			}else{
				//$('#earnedRewardPoints').text('0.00');
				//$('#earnRewardPoint').val(0.00);
				if(paymentType == 'PARTIAL'){
					$('#rewardPointsDiv').show();
				}
			}
		}else if(paymentMethod=='WALLET_CREDITCARD'){
			$('#rewardPointsDiv').hide();			
			$('#cardAmountDiv').show();
			$('#creditCardDiv').show();
			$('#walletAmountDiv').show();
			$('#bankChequeDiv').hide();
			$('#bankChequeAmtDiv').hide();
			//calculateRewardPoints();
		}else if(paymentMethod=='PARTIAL_REWARD_CC'){
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').show();
			$('#walletAmountDiv').hide();			
			$('#creditCardDiv').show();
			$('#bankChequeDiv').hide();
			$('#bankChequeAmtDiv').hide();
			//$('#earnedRewardPoints').text('0.00');
			//$('#earnRewardPoint').val(0.00);
		}else if(paymentMethod=='PARTIAL_REWARD_WALLET'){
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#rewardPointsDiv').show();
			$('#walletAmountDiv').show();
			$('#bankChequeDiv').hide();
			$('#bankChequeAmtDiv').hide();
			$('.cardInfo').hide();
			//$('#earnedRewardPoints').text('0.00');
			//$('#earnRewardPoint').val(0.00);
		}else if(paymentMethod=='PARTIAL_REWARD_WALLET_CC'){
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').show();
			$('#creditCardDiv').show();
			$('#walletAmountDiv').show();
			$('#bankChequeDiv').hide();
			$('#bankChequeAmtDiv').hide();
			//$('#earnedRewardPoints').text('0.00');
			//$('#earnRewardPoint').val(0.00);
		}else if(paymentMethod == 'BANK_OR_CHEQUE'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#walletAmountDiv').hide();
			$('#bankChequeDiv').show();
			$('#bankChequeAmtDiv').hide();
			$('.cardInfo').hide();
			$('#bankChequeAmtDiv').hide();
			if(paymentType == 'PARTIAL'){
				$('#bankChequeDiv').show();
				$('#bankChequeAmtDiv').show();
			}
		}else if(paymentMethod == 'BANK_OR_CHEQUE_CC'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').show();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').show();
			$('#bankChequeAmtDiv').show();
			$('#bankChequeDiv').show();
			$('.cardInfo').show();
		}else if(paymentMethod == 'BANK_OR_CHEQUE_WALLET'){
			$('#rewardPointsDiv').hide();
			$('#cardAmountDiv').hide();
			$('#walletAmountDiv').show();
			$('#creditCardDiv').hide();
			$('#bankChequeAmtDiv').show();
			$('#bankChequeDiv').show();
			$('.cardInfo').hide();
		}else if(paymentMethod == 'BANK_OR_CHEQUE_PARTIAL_REWARD'){
			$('#rewardPointsDiv').show();
			$('#cardAmountDiv').hide();
			$('#walletAmountDiv').hide();
			$('#creditCardDiv').hide();
			$('#bankChequeAmtDiv').show();
			$('#bankChequeDiv').show();
			$('.cardInfo').hide();
		}		
	}
	
	function custChangeSavedCard(){
		var creditCardValue = $("#custCards").val();
		if(creditCardValue != 0){
			$('#cardNumber').attr('disabled', 'disabled'); // mark it as read only
			$('#expMonth').attr('disabled', 'disabled');
			$('#expYear').attr('disabled', 'disabled');
			$('#name').attr('disabled', 'disabled');
			$('#cvv').attr('disabled', 'disabled');
		}
		if(creditCardValue == 0){
			$('#cardNumber').removeAttr('disabled');
			$('#expMonth').removeAttr('disabled');
			$('#expYear').removeAttr('disabled');
			$('#name').removeAttr('disabled');
			$('#cvv').removeAttr('disabled');
		}
	}
	
	function toPayInvoice(){
		 var paymentMethod = $('#paymentMethod').val();
		 var paymentType = $('#paymentType').val();
		 var savedCardId = $('#custCards').val();
		 var requiredAmount =  parseFloat($('#pinv_orderTotal').val());
		 if(paymentType == 'PARTIAL'){
			 if (paymentMethod == 'CREDITCARD'){
				 var cardAmount = $('#cardAmount').val();
				 if(cardAmount == undefined  || cardAmount == ''){
					 jAlert("Please enter card Amount to be charged.");
					 return;
				 }
			 }else if (paymentMethod == 'REWARDPOINTS'){
				 var rewardPoints = $('#rewardPoints').val();
				 if(rewardPoints == undefined  || rewardPoints == ''){
					 jAlert("Please enter Reward Dollar Amount to be charged.");
					 return;
				 }
			 }else if (paymentMethod == 'WALLET'){
				 var walletAmount = $('#walletAmount').val();
				 if(walletAmount == undefined  || walletAmount == ''){
					 jAlert("Please enter Wallet Amount to be charged.");
					 return;
				 }
			 }else if (paymentMethod == 'BANK_OR_CHEQUE'){
				 var bankChequeAmt = $('#bankChequeAmt').val();
				 if(bankChequeAmt == undefined  || bankChequeAmt == ''){
					 jAlert("Please enter Bank/Check Amount to be charged.");
					 return;
				 }
			 }else{
				 jAlert("Partial payment is allowed only with payment method : CREDITCARD,REWARDPOINTS,WALLET or BANK_OR_CHEQUE.");
				 return;
			 }
		 }
		 if(paymentMethod == null ){
			 jAlert("Please choose payment method.");
			 return false;
		 }else if (paymentMethod == 'CREDITCARD'){
			 if(null == savedCardId || savedCardId == 0){
				if(validateCreditCard()){
					 $("#sBySavedCard").val("NO");
					  var expMonth = $('#expMonth').val();
					  var expYear = $('#expYear').val();
					  expYear = expYear.substring(2,4);
					  var stripExp = expMonth+"/"+expYear;
					  $('#stripeExp').val(stripExp);
					  var cvv = $('#cvv').val();
					  
					  if (Stripe.card.validateCardNumber($('#cardNumber').val())
						  && Stripe.card.validateExpiry($('#stripeExp').val())
						  && Stripe.card.validateCVC($('#cvv').val())) {
						  Stripe.createToken({
								 name: $('#name').val(),
								 number: $('#cardNumber').val(),
								 exp: stripExp,
								 cvc: $('#cvv').val()
							 }, stripeResponseHandler);
						  
					  } else {
						  jAlert("Please enter valid card information.");
						  return false;
					  }
				}else{
					return false;
				}
			 }else{
				 $("#sBySavedCard").val("YES");
				 $("#sSavedCardId").val(savedCardId);
				 $("#pinv_action").val('savePayInvoice');
				 savePayInvoice();
			 }
		 }else if(paymentMethod == 'REWARDPOINTS'){
			  var total = parseFloat($('#pinv_activeRewardPoints').val());
			  if(total<requiredAmount){
				  jAlert("Selected customer have only "+total+" active reward points.");
				  return false;
			  }
			  $("#partialRewardPoints").val($("#rewardPoints").val());
			  $("#rewardPoints").val(requiredAmount);
			  $("#pinv_action").val('savePayInvoice');
			  savePayInvoice();
		 }else if(paymentMethod == 'WALLET'){
			var walletCredit = parseFloat($('#pinv_walletCredit').val());
			if(walletCredit< requiredAmount){
				jAlert("Selected customer have only "+walletCredit+" Wallet Credit.");
				return false;
			}
			$("#pinv_action").val('savePayInvoice');
			savePayInvoice();
		 }else if(paymentMethod == 'WALLET_CREDITCARD'){
			 var walletCredit = parseFloat($('#pinv_walletCredit').val());
			 var cardAmount = parseFloat($('#cardAmount').val());
			 var walletAmount = parseFloat($('#walletAmount').val());
			 if(cardAmount<=0 || isNaN(cardAmount)){
				 jAlert("Please Enter Valid Card Amount, should be greater than 0.");
				 return false;
			 }
			 if(walletAmount <= 0 || isNaN(walletAmount)){
				 jAlert("Please Enter valid Wallet Amount, Should be greater than 0.");
				 return false;
			 }
			 if(walletAmount > walletCredit){
				 jAlert("Customer have only "+walletCredit+" Wallet Amount.");
				 return false;
			 }
			 var total = (cardAmount + walletAmount).toFixed(2);
			 
			 if(requiredAmount != total){
				jAlert("Wallet Amount and Card Amount should be equal to "+requiredAmount);
				return false;
			 }
			 if(null == savedCardId || savedCardId == 0){
				 if(validateCreditCard()){
					 $("#sBySavedCard").val("NO");
					  var expMonth = $('#expMonth').val();
					  var expYear = $('#expYear').val();
					  expYear = expYear.substring(2,4);
					  var stripExp = expMonth+"/"+expYear;
					  $('#stripeExp').val(stripExp);
					  var cvv = $('#cvv').val();
					  
					  if (Stripe.card.validateCardNumber($('#cardNumber').val())
						  && Stripe.card.validateExpiry($('#stripeExp').val())
						  && Stripe.card.validateCVC($('#cvv').val())) {
						  Stripe.createToken({
								 name: $('#name').val(),
								 number: $('#cardNumber').val(),
								 exp: stripExp,
								 cvc: $('#cvv').val()
							 }, stripeResponseHandler);
						  
					  } else {
						  jAlert("Please enter valid card information.");
						  return false;
					  }
				 }else{
					 return false;
				 }
			 }else{
				 $("#sBySavedCard").val("YES");
				 $("#sSavedCardId").val(savedCardId);
				 $("#pinv_action").val('savePayInvoice');
					savePayInvoice();
			 }
		 }else if (paymentMethod == 'PARTIAL_REWARD_CC'){
			 var cardAmount = parseFloat($('#cardAmount').val());
			 var rewardPoints = parseFloat($('#rewardPoints').val());
			 var customerRewardPoints = parseFloat($('#pinv_activeRewardPoints').val());
			 if(cardAmount <= 0 || isNaN(cardAmount)){
				  jAlert("Please enter vaid Card amount, Should be greater than 0.");
				  return false;
			  }
			  if(rewardPoints <= 0 || isNaN(rewardPoints)){
				  jAlert("Please enter valid reward point, Should be greater than 0.");
				  return false;
			  }
			  if(rewardPoints > customerRewardPoints){
				  jAlert("Selected customer have only "+customerRewardPoints+" active reward points.");
				  return false;
			  }
			  var total = (rewardPoints + cardAmount).toFixed(2);
			 
			  if(total!=requiredAmount){
				  jAlert("Reward points and Card amount total should be equals to "+requiredAmount);
				  return false;
			  }
			 if(null == savedCardId || savedCardId == 0){
				 if(validateCreditCard()){
					 $("#sBySavedCard").val("NO");
					  var expMonth = $('#expMonth').val();
					  var expYear = $('#expYear').val();
					  expYear = expYear.substring(2,4);
					  var stripExp = expMonth+"/"+expYear;
					  $('#stripeExp').val(stripExp);
					  var cvv = $('#cvv').val();
					  
					  if (Stripe.card.validateCardNumber($('#cardNumber').val())
						  && Stripe.card.validateExpiry($('#stripeExp').val())
						  && Stripe.card.validateCVC($('#cvv').val())) {
						  Stripe.createToken({
								 name: $('#name').val(),
								 number: $('#cardNumber').val(),
								 exp: stripExp,
								 cvc: $('#cvv').val()
							 }, stripeResponseHandler);
						  
					  } else {
						  jAlert("Please enter valid card information.");
						  return false;
					  }
				 }else{
					 return false; 
				 }
			 }else{
				 $("#sBySavedCard").val("YES");
				 $("#sSavedCardId").val(savedCardId);
				 $("#pinv_action").val('savePayInvoice');
				 savePayInvoice();
			 }
		 }else if (paymentMethod == 'PARTIAL_REWARD_WALLET'){
			 var rewardPoints = parseFloat($('#rewardPoints').val());
			 var walletCredit = parseFloat($('#pinv_walletCredit').val());
			 var walletAmount = parseFloat($('#walletAmount').val());
			 var customerRewardPoints = parseFloat($('#pinv_activeRewardPoints').val());
			 var cardAmount = parseFloat($('#cardAmount').val());
			 if(rewardPoints <= 0 || isNaN(rewardPoints)){
				  jAlert("Please enter valid reward points, Should be greater than 0.");
				  return false;
			 }
			 if(walletAmount <= 0 || isNaN(walletAmount)){
				  jAlert("Please enter valid Wallet amount, Should be greater than 0.");
				  return false;
			 }
			 if(rewardPoints > customerRewardPoints){
				  jAlert("Selected customer have only "+customerRewardPoints+" active reward points.");
				  return false;
			 }
			 if(walletAmount > walletCredit){
				  jAlert("Selected customer have only "+customerRewardPoints+" Wallet Amount.");
				  return false;
			 }
			 var total = (walletAmount + rewardPoints).toFixed(2);
			 if(total!=requiredAmount){
				  jAlert("Reward points and Wallet amount total should be equals to "+requiredAmount);
				  return false;
			 }
			 $("#pinv_action").val('savePayInvoice');
			 savePayInvoice();
		 }else if (paymentMethod == 'PARTIAL_REWARD_WALLET_CC'){
			 var cardAmount = parseFloat($('#cardAmount').val());
			 var walletCredit = parseFloat($('#pinv_walletCredit').val());
			 var walletAmount = parseFloat($('#walletAmount').val());
			 var rewardPoints = parseFloat($('#rewardPoints').val());
			 var customerRewardPoints = parseFloat($('#pinv_activeRewardPoints').val());
			 if(cardAmount<=0 || isNaN(cardAmount)){
				 jAlert("Please enter valid card amount, Should be greater than 0.");
				 return false;
			 }
			 if(rewardPoints <= 0 || isNaN(rewardPoints)){
				  jAlert("Please enter valid reward points, Should be greater than 0.");
				  return false;
			 }
			 if(walletAmount <= 0 || isNaN(walletAmount)){
				  jAlert("Please enter valid wallet amount, Should be greater than 0.");
				  return false;
			 }
			 if(rewardPoints > customerRewardPoints){
				  jAlert("Selected customer have only "+customerRewardPoints+" active reward points.");
				  return false;
			 }
			 if(walletAmount > walletCredit){
				  jAlert("Selected customer have only "+customerRewardPoints+" Wallet Amount.");
				  return false;
			 }
			 var total = (walletAmount + rewardPoints + cardAmount).toFixed(2);
			 if(total!=requiredAmount){
				  jAlert("Reward points + Wallet amount + Card amount total should be equals to "+requiredAmount);
				  return false;
			 }
			 if(null == savedCardId || savedCardId == 0){
				 if(validateCreditCard()){
					 $("#sBySavedCard").val("NO");
					  var expMonth = $('#expMonth').val();
					  var expYear = $('#expYear').val();
					  expYear = expYear.substring(2,4);
					  var stripExp = expMonth+"/"+expYear;
					  $('#stripeExp').val(stripExp);
					  var cvv = $('#cvv').val();
					  
					  if (Stripe.card.validateCardNumber($('#cardNumber').val())
						  && Stripe.card.validateExpiry($('#stripeExp').val())
						  && Stripe.card.validateCVC($('#cvv').val())) {
						  Stripe.createToken({
								 name: $('#name').val(),
								 number: $('#cardNumber').val(),
								 exp: stripExp,
								 cvc: $('#cvv').val()
							 }, stripeResponseHandler);
						  
					  } else {
						  jAlert("Please enter valid card information.");
						  return false;
					  }
				 }else{
					 return false; 
				 }
			 }else{
				 $("#sBySavedCard").val("YES");
				 $("#sSavedCardId").val(savedCardId);
				 $("#pinv_action").val('savePayInvoice');
				 savePayInvoice();
			 }
		 }else if(paymentMethod == 'BANK_OR_CHEQUE'){
			var bankChequeId = $('#bankChequeId').val();
			if(bankChequeId == null || bankChequeId == ''){
				jAlert("Please enter valid information in Check No./Bank Transaction Id.");
				return false;
			}else{
				$("#pinv_action").val('savePayInvoice');
				savePayInvoice();
			}
		 }else if(paymentMethod == 'BANK_OR_CHEQUE_CC'){
			 var cardAmount = parseFloat($('#cardAmount').val());
			 var chequeAmt = parseFloat($('#bankChequeAmt').val());
			 var bankChequeId = $('#bankChequeId').val();
			 if(bankChequeId == null || bankChequeId == ''){
				jAlert("Please enter valid information in Check No./Bank Transaction Id.");
				return false;
			 }
			 if(cardAmount<=0 || isNaN(cardAmount)){
				 jAlert("Please enter valid card amount, Should be greater than 0.");
				 return false;
			 }
			 if(chequeAmt<=0 || isNaN(chequeAmt)){
				  jAlert("Please enter valid Bank/Check amount, Should be greater than 0.");
				  return false;
			 }
			 var total = (chequeAmt + cardAmount).toFixed(2);
			 if(total!=requiredAmount){
				  jAlert("card amount + Bank/Check amount total should be equals to "+requiredAmount);
				  return false;
			 }
			 if(null == savedCardId || savedCardId == 0){
				 if(validateCreditCard()){
					 $("#sBySavedCard").val("NO");
					  var expMonth = $('#expMonth').val();
					  var expYear = $('#expYear').val();
					  expYear = expYear.substring(2,4);
					  var stripExp = expMonth+"/"+expYear;
					  $('#stripeExp').val(stripExp);
					  var cvv = $('#cvv').val();
					  
					  if (Stripe.card.validateCardNumber($('#cardNumber').val())
						  && Stripe.card.validateExpiry($('#stripeExp').val())
						  && Stripe.card.validateCVC($('#cvv').val())) {
						  Stripe.createToken({
								 name: $('#name').val(),
								 number: $('#cardNumber').val(),
								 exp: stripExp,
								 cvc: $('#cvv').val()
							 }, stripeResponseHandler);
						  
					  } else {
						  jAlert("Please enter valid card information.");
						  return false;
					  }
				 }else{
					 return false; 
				 }
			 }else{
				 $("#sBySavedCard").val("YES");
				 $("#sSavedCardId").val(savedCardId);
				 $("#pinv_action").val('savePayInvoice');
				 savePayInvoice();
			 }
			 
		}else if(paymentMethod == 'BANK_OR_CHEQUE_WALLET'){
			 var walletCredit = parseFloat($('#pinv_walletCredit').val());
			 var walletAmount = parseFloat($('#walletAmount').val());
			 var chequeAmt = parseFloat($('#bankChequeAmt').val());
			 var bankChequeId = $('#bankChequeId').val();
			 if(bankChequeId == null || bankChequeId == ''){
				jAlert("Please enter valid information in Check No./Bank Transaction Id.");
				return false;
			 }
			 if(walletAmount<=0 || isNaN(walletAmount)){
				 jAlert("Please enter valid wallet amount, Should be greater than 0.");
				 return false;
			 }
			 if(chequeAmt<=0 || isNaN(chequeAmt)){
				  jAlert("Please enter valid Bank/Check amount, Should be greater than 0.");
				  return false;
			 }
			 if(walletCredit<=0 || isNaN(walletCredit)){
				  jAlert("Customer does not have any wallet credit amount.");
				  return false;
			 }
			 if(walletAmount>walletCredit){
				  jAlert("Customer have only "+walletAmount+" wallet credit amount.");
				  return false;
			 }
			 var total = (chequeAmt + walletAmount).toFixed(2);
			 if(total!=requiredAmount){
				  jAlert("Wallet amount + Bank/Check amount total should be equals to "+requiredAmount);
				  return false;
			 }
			 $("#pinv_action").val('savePayInvoice');
			 savePayInvoice();
		}else if(paymentMethod == 'BANK_OR_CHEQUE_PARTIAL_REWARD'){
			 var rewardPoints = parseFloat($('#rewardPoints').val());
			 var customerRewardPoints = parseFloat($('#pinv_activeRewardPoints').val());
			 var chequeAmt = parseFloat($('#bankChequeAmt').val());
			 var bankChequeId = $('#bankChequeId').val();
			 if(bankChequeId == null || bankChequeId == ''){
				jAlert("Please enter valid information in Check No./Bank Transaction Id.");
				return false;
			 }
			 if(rewardPoints<=0 || isNaN(rewardPoints)){
				 jAlert("Please enter valid Reward points, Should be greater than 0.");
				 return false;
			 }
			 if(chequeAmt<=0 || isNaN(chequeAmt)){
				  jAlert("Please enter valid Bank/Check amount, Should be greater than 0.");
				  return false;
			 }
			 if(customerRewardPoints<=0 || isNaN(customerRewardPoints)){
				  jAlert("Customer does not have any Reward points.");
				  return false;
			 }
			 if(rewardPoints>customerRewardPoints){
				  jAlert("Customer have only "+rewardPoints+" active Reward points.");
				  return false;
			 }
			 var total = (chequeAmt + rewardPoints).toFixed(2);
			 if(total!=requiredAmount){
				  jAlert("Reward points + Bank/Check amount total should be equals to "+requiredAmount);
				  return false;
			 }
			 $("#pinv_action").val('savePayInvoice');
			 savePayInvoice();
		}else{
			return false; 
		}
	}
	
	function validateCreditCard(){
		  if($('#cardNumber').val() == null || $('#cardNumber').val() == "" ){
			  jAlert("Please enter valid card number.");
			  return false;
		  }
		  
		  if($('#expMonth').val() == null || $('#expMonth').val() == "" ){
			  jAlert("Please enter expiry month.");
			  return false;
		  }
		  
		  if($('#expYear').val() == null || $('#expYear').val() == "" ){
			  jAlert("Please enter expiry year.");
			  return false;
		  }
		  
		  /*if($('#cvv').val() == null || $('#cvv').val() == "" ){
			  jAlert("Please enter valid cvv.");
			  return false;
		  }*/
		  return true;
	}

	function savePayInvoice(){
		$.ajax({
			url : "/Accounting/CreatePayInvoice",
			type : "post",
			data : $("#pinv_form").serialize()+"&orderId="+$('#pinv_orderId').val(),
			dataType: "json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status==1){					
					$('#pay-invoice').modal('hide');					
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function stripeResponseHandler(status, response) {
	    if (response.error) {
	    	jAlert(response.error.message);
	    	return false;
	    } else {
	        $("#sBySavedCard").val("NO");
	        $("#sTempToken").val(response.id);	       
			$("#pinv_action").val('savePayInvoice');
			savePayInvoice();
	        return true;
	    }
	};
	
	function resetPayInvoiceValues(){
		$('#pinv_Note').val('');
		$('#paymentMethod').val('CREDITCARD');
		var select=document.getElementById("custCards");
		document.getElementById("custCards").innerHTML='';
		var opt=document.createElement('option');
		opt.value=0;
		opt.innerHTML="Get New Card";
		select.appendChild(opt);
		changePaymentMethod();
		custChangeSavedCard();
		$('#cardAmount').val('');
		$('#rewardPoints').val('');
		$('#walletAmount').val('');
		$('#bankChequeId').val('');
		$('#cardNumber').val('');
		$('#name').val('');
		$('#cvv').val('');
		$('#bankChequeAmt').val('');
		
	}