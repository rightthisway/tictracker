
var shippingCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});

var shippingPagingInfo;
var shippingGrid;
var shippingDataView;
var shippingData=[];
var shippingColumn = [ shippingCheckboxSelector.getColumnDefinition(), 
			   {id:"shippingId", name:"Shipping ID", field: "shippingId", sortable: true},
               {id:"firstname", name:"First Name", field: "firstname", sortable: true},
               {id:"lastname", name:"Last Name", field: "lastname", sortable: true},
               {id:"street1", name:"Street1", field: "street1", sortable: true},
               {id:"street2", name:"Street2", field: "street2", sortable: true},
               {id:"country", name:"Country", field: "country", sortable: true},
               {id:"state", name:"State", field: "state", sortable: true},
               {id:"city", name:"City", field: "city", sortable: true},
               {id:"zipcode", name:"Zip Code", field: "zipcode", sortable: true}
              ];
              
var shippingOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var shippingSortCol = "shippingId";
var shippingSortdir = 1;
var percentCompleteThreshold = 0;
var shippingSearchString = "";

//filter option
function invoiceShippingGridFilter(item, args) {
	var x= item["shippingId"];
	if (args.shippingSearchString  != ""
			&& x.indexOf(args.shippingSearchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.shippingSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}


function invoiceShippingGridComparer(a, b) {
	  var x = a[shippingSortCol], y = b[shippingSortCol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
function shippingToggleFilterRow() {
	shippingGrid.setTopPanelVisibility(!shippingGrid.getOptions().showTopPanel);
}

 
function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(shippingPagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(shippingPagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(shippingPagingInfo.pageNum)-1;
	}	
}

function createShippingGrid(jsonData) {	
	var i=0;
	shippingData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (shippingData[i] = {});
			d["id"] = data.id;
			d["shippingId"] = data.id;
			d["firstname"] = data.firstName;
			d["lastname"] = data.lastName;
			d["street1"] = data.addressLine1;
			d["street2"] = data.addressLine2;
			d["country"] = data.country;
			d["state"] = data.state;
			d["city"] = data.city;
			d["zipcode"] = data.zipCode;
		}
	}
	
	shippingDataView = new Slick.Data.DataView({inlineFilters: true });
	shippingGrid = new Slick.Grid("#custShippingGrid", shippingDataView, shippingColumn, shippingOptions);
	shippingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	shippingGrid.setSelectionModel(new Slick.RowSelectionModel());
	shippingGrid.registerPlugin(shippingCheckboxSelector);
	if(shippingPagingInfo != null){
		var shippingGridPager = new Slick.Controls.Pager(shippingDataView, shippingGrid, $("#shipping_pager"),shippingPagingInfo);
	}
	var columnpicker = new Slick.Controls.ColumnPicker(shippingColumn, shippingGrid, shippingOptions);

	  
	  // move the filter panel defined in a hidden div into shippingGrid top panel
	  //$("#shipping_inlineFilterPanel").appendTo(shippingGrid.getTopPanel()).show();
	  
	  shippingGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < shippingDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    shippingGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  shippingGrid.onSort.subscribe(function (e, args) {
	    shippingSortdir = args.sortAsc ? 1 : -1;
	    shippingSortCol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      // using temporary Object.prototype.toString override
	      // more limited and does lexicographic sort only by default, but can be much faster
	      // use numeric sort of % and lexicographic for everything else
	      shippingDataView.fastSort(shippingSortCol, args.sortAsc);
	    } else {
	      // using native sort with invoiceShippingGridComparer
	      // preferred method but can be very slow in IE with huge datasets
	      shippingDataView.sort(invoiceShippingGridComparer, args.sortAsc);
	    }
	  });
	  // wire up model events to drive the shippingGrid
	  shippingDataView.onRowCountChanged.subscribe(function (e, args) {
	    shippingGrid.updateRowCount();
	    shippingGrid.render();
	  });
	  shippingDataView.onRowsChanged.subscribe(function (e, args) {
	    shippingGrid.invalidateRows(args.rows);
	    shippingGrid.render();
	  });
	  
	  // wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    shippingSearchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    shippingDataView.setFilterArgs({
	      shippingSearchString: shippingSearchString
	    });
	    shippingDataView.refresh();
	  }
	  
	  shippingDataView.beginUpdate();
	  shippingDataView.setItems(shippingData);
	  shippingDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    shippingSearchString: shippingSearchString
	  });
	  shippingDataView.setFilter(invoiceShippingGridFilter);
	  shippingDataView.endUpdate();
	  // if you don't want the items that are not visible (due to being filtered out
	  // or being on a different page) to stay selected, pass 'false' to the second arg
	  shippingDataView.syncGridSelection(shippingGrid, true);
	  shippingDataView.refresh();
	  $("#gridContainer").resizable();
	  shippingGrid.resizeCanvas();
}	


function updateShippingAddress(){
	//var count=0; 	
	var shippingId;
	var row = shippingGrid.getSelectedRows([0])[0];
	if(row>=0){
		shippingId = shippingGrid.getDataItem(row).shippingId;
	
		/*var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					if(eventCheckbox[i].checked == true){
						count++;
					}
				}
			}
		}
		if(count==1){
			
		}else{
			jAlert('Please select only one shipping address.');
		}*/
		changeShippingAddress(shippingId);
		$('#change-address').modal('hide');
	}else{
		jAlert('Please select shipping address.');
	}	
}

function showShippingAddress(){
	var custId = $('#customerIdDiv').val();
	if(custId == null || custId==''){
		jAlert("Please select Customer first.");
		return false;
	}else{
		$.ajax({
			url : "/Invoice/getShippingAddress",
			type : "post",
			data : "custId="+custId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status==1){
					if(jsonData.count == 0){
						$('#shippingAddressMsg').show();
					}
					$('#change-address').modal('show');
					shippingPagingInfo = jsonData.shippingPagingInfo;
					createShippingGrid(jsonData.shippingAddressList);
				}/*else{
					jAlert(jsonData.msg);
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
				
			}, error : function(error){				
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				flag = false;
			}
		});
	}
}