
var paymentHistoryPagingInfo;
var paymentHistoryDataView;
var paymentHistoryGrid;
var paymentHistoryData = [];
var paymentHistoryColumns = [ {
	id : "primaryPaymentMethod",
	name : "Primary Payment Method",
	field : "primaryPaymentMethod",
	sortable : true
},{
	id : "secondaryPaymentMethod",
	name : "Secondary Payment Method",
	field : "secondaryPaymentMethod",
	sortable : true
},{
	id : "thirdPaymentMethod",
	name : "Third Payment Method",
	field : "thirdPaymentMethod",
	sortable : true
},{
	id : "primaryAmount",
	name : "Primary Amount",
	field : "primaryAmount",
	sortable : true
},{
	id : "secondaryAmount",
	name : "Secondary Amount",
	field : "secondaryAmount",
	sortable : true
},{
	id : "thirdAmount",
	name : "Third Amount",
	field : "thirdAmount",
	sortable : true
}, {
	id : "paidOn",
	name : "Paid On",
	field : "paidOn",
	width:80,
	sortable : true
}, {
	id : "primaryTransaction",
	name : "Primary Transaction Id",
	field : "primaryTransaction",
	sortable : true
}, {
	id : "secondaryTransaction",
	name : "Secondary Transaction Id",
	field : "secondaryTransaction",
	sortable : true
},{
	id : "thirdTransaction",
	name : "Third Transaction Id",
	field : "thirdTransaction",
	sortable : true
},{id:"csr", name:"CSR", field: "csr", sortable: true},
{id:"cardType", name:"Card Type", field: "cardType", sortable: true},
{id:"creditCardNumber", name:"Credit Card Number", field: "creditCardNumber", sortable: true}];

var paymentHistoryOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
};
var paymentHistorySortcol = "id";
var paymentHistorySortdir = 1;
var paymentHistorySearchString = "";

function paymentHistoryFilter(item, args) {
	var x= item["invoiceId"];
	if (args.eventGridSearchString  != ""
			&& x.indexOf(args.eventGridSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.eventGridSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function paymentHistoryComparer(a, b) {
	var x = a[paymentHistorySortcol], y = b[paymentHistorySortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

	function refreshPaymentHistoryGridValues(jsonData, pageInfo) {
		paymentHistoryPagingInfo = pageInfo;
		paymentHistoryData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (paymentHistoryData[i] = {});
				d["id"] = i;
				if(data.primaryPaymentMethod == 'BANK_OR_CHEQUE'){
					d["primaryPaymentMethod"] = 'BANK_OR_CHECK';
				}else{
					d["primaryPaymentMethod"] = data.primaryPaymentMethod;
				}
				d["secondaryPaymentMethod"] = data.secondaryPaymentMethod;
				d["thirdPaymentMethod"] = data.thirdPaymentMethod;
				d["primaryAmount"] = data.primaryAmount;
				d["secondaryAmount"] = data.secondaryAmount;
				d["thirdAmount"] = data.thirdPayAmt;
				d["paidOn"] = data.paidOnStr;
				d["secondaryTransaction"] = data.secondaryTransaction;
				d["primaryTransaction"] = data.primaryTransaction;
				d["thirdTransaction"] = data.thirdTransactionId;
				d["csr"] = data.csr;
				d["cardType"] = data.cardType;
				d["creditCardNumber"] = data.creditCardNumber;
			}
		}	
			
		paymentHistoryDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		paymentHistoryGrid = new Slick.Grid("#paymentHistory_grid", paymentHistoryDataView, paymentHistoryColumns, paymentHistoryOptions);
		paymentHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		paymentHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		var paymentHistoryPager = new Slick.Controls.Pager(paymentHistoryDataView, paymentHistoryGrid, $("#paymentHistory_pager"),paymentHistoryPagingInfo);
		var paymentHistoryColumnpicker = new Slick.Controls.ColumnPicker(paymentHistoryColumns, paymentHistoryGrid,
			paymentHistoryOptions);

	paymentHistoryGrid.onSort.subscribe(function(e, args) {
		paymentHistorySortdir = args.sortAsc ? 1 : -1;
		paymentHistorySortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			paymentHistoryDataView.fastSort(paymentHistorySortcol, args.sortAsc);
		} else {
			paymentHistoryDataView.sort(paymentHistoryComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	paymentHistoryDataView.onRowCountChanged.subscribe(function(e, args) {
		paymentHistoryGrid.updateRowCount();
		paymentHistoryGrid.render();
	});
	paymentHistoryDataView.onRowsChanged.subscribe(function(e, args) {
		paymentHistoryGrid.invalidateRows(args.rows);
		paymentHistoryGrid.render();
	});

	// initialize the model after all the events have been hooked up
	paymentHistoryDataView.beginUpdate();
	paymentHistoryDataView.setItems(paymentHistoryData);
	paymentHistoryDataView.endUpdate();
	paymentHistoryDataView.syncGridSelection(paymentHistoryGrid, true);
	$("#gridContainer").resizable();
	//paymentHistoryGrid.resizeCanvas();
	//$("div#divLoading").removeClass('show');
}

var rtwpaymentHistoryDataView;
var rtwpaymentHistoryGrid;
var rtwpaymentHistoryData = [];
var rtwpaymentHistoryColumns = [ {
	id : "creditCardNumber",
	name : "Credit Card Number",
	field : "creditCardNumber",
	sortable : true
},{
	id : "checkNumber",
	name : "Check Number",
	field : "checkNumber",
	sortable : true
},{
	id : "amount",
	name : "Amount",
	field : "amount",
	sortable : true
},{
	id : "note",
	name : "Note",
	field : "note",
	sortable : true
}, {
	id : "paidOn",
	name : "Paid On",
	field : "paidOn",
	width:80,
	sortable : true
}, {
	id : "systemUser",
	name : "System User",
	field : "systemUser",
	sortable : true
}, {
	id : "paymentType",
	name : "Payment Type",
	field : "paymentType",
	sortable : true
},{
	id : "transOffice",
	name : "Transaction Office",
	field : "transOffice",
	sortable : true
}];

var rtwpaymentHistoryOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	topPanelHeight : 25,
};
var rtwpaymentHistorySortcol = "id";
var rtwpaymentHistorySortdir = 1;
var percentCompleteThreshold = 0;
var rtwpaymentHistorySearchString = "";

function rtwpaymentHistoryFilter(item, args) {
	var x= item["invoiceId"];
	if (args.rtwpaymentHistorySearchString  != ""
			&& x.indexOf(args.rtwpaymentHistorySearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.rtwpaymentHistorySearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function rtwpaymentHistoryComparer(a, b) {
	var x = a[rtwpaymentHistorySortcol], y = b[rtwpaymentHistorySortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function paymentToggleFilterRow() {
	rtwpaymentHistoryGrid.setTopPanelVisibility(!rtwpaymentHistoryGrid.getOptions().showTopPanel);
}
	
$("#rtwpaymentHistory_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});

function refreshrtwPaymentHistoryGridValues(jsonData, pageInfo) {
		paymentHistoryPagingInfo = pageInfo;
		rtwpaymentHistoryData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i];
				var d = (rtwpaymentHistoryData[i] = {});
				d["id"] = i;
				d["creditCardNumber"] = data.creditCardNumber;
				d["checkNumber"] = data.checkNumber;
				d["amount"] = data.amount;
				d["note"] = data.note;
				d["paidOn"] = data.paidOnStr;
				d["systemUser"] = data.systemUser;
				d["paymentType"] = data.paymentType;
				d["transOffice"] = data.transOffice;
			}
		}
			
		rtwpaymentHistoryDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		rtwpaymentHistoryGrid = new Slick.Grid("#rtwpaymentHistory_grid", rtwpaymentHistoryDataView, rtwpaymentHistoryColumns, rtwpaymentHistoryOptions);
		rtwpaymentHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		rtwpaymentHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		var rtwpaymentHistoryPager = new Slick.Controls.Pager(rtwpaymentHistoryDataView, rtwpaymentHistoryGrid, $("#rtwpaymentHistory_pager"),paymentHistoryPagingInfo);
		var rtwpaymentHistoryColumnpicker = new Slick.Controls.ColumnPicker(rtwpaymentHistoryColumns, rtwpaymentHistoryGrid,
			rtwpaymentHistoryOptions);

		// move the filter panel defined in a hidden div into rtwpaymentHistoryGrid top panel
	  //$("#inlineFilterPanel").appendTo(rtwpaymentHistoryGrid.getTopPanel()).show();
	  
	  rtwpaymentHistoryGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < rtwpaymentHistoryDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    rtwpaymentHistoryGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  
	rtwpaymentHistoryGrid.onSort.subscribe(function(e, args) {
		rtwpaymentHistorySortdir = args.sortAsc ? 1 : -1;
		rtwpaymentHistorySortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			rtwpaymentHistoryDataView.fastSort(rtwpaymentHistorySortcol, args.sortAsc);
		} else {
			rtwpaymentHistoryDataView.sort(rtwpaymentHistoryComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	rtwpaymentHistoryDataView.onRowCountChanged.subscribe(function(e, args) {
		rtwpaymentHistoryGrid.updateRowCount();
		rtwpaymentHistoryGrid.render();
	});
	rtwpaymentHistoryDataView.onRowsChanged.subscribe(function(e, args) {
		rtwpaymentHistoryGrid.invalidateRows(args.rows);
		rtwpaymentHistoryGrid.render();
	});

	// wire up the search textbox to apply the filter to the model
	  $("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    rtwpaymentHistorySearchString = this.value;
	    updateFilter();
	  });
	  function updateFilter() {
	    rtwpaymentHistoryDataView.setFilterArgs({
	      rtwpaymentHistorySearchString: rtwpaymentHistorySearchString
	    });
	    rtwpaymentHistoryDataView.refresh();
	}
	  
	// initialize the model after all the events have been hooked up
	rtwpaymentHistoryDataView.beginUpdate();
	rtwpaymentHistoryDataView.setItems(rtwpaymentHistoryData);
	
	rtwpaymentHistoryDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    rtwpaymentHistorySearchString: rtwpaymentHistorySearchString
	  });
	rtwpaymentHistoryDataView.setFilter(rtwpaymentHistoryFilter);
	  
	rtwpaymentHistoryDataView.endUpdate();
	rtwpaymentHistoryDataView.syncGridSelection(rtwpaymentHistoryGrid, true);
	$("#gridContainer").resizable();
	//rtwpaymentHistoryGrid.resizeCanvas();
	//$("div#divLoading").removeClass('show');
}
