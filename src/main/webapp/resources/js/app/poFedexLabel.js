var poFedexShippingCheckboxSelector = new Slick.CheckboxSelectColumn({
  cssClass: "slick-cell-checkboxsel"
});

var poFedexShippingPagingInfo;
var poFedexShippingGrid;
var poFedexShippingDataView;
var poFedexShippingData=[];
var poFedexShippingColumn = [ poFedexShippingCheckboxSelector.getColumnDefinition(), 
	 /*{id:"id", name:"ID", field: "id", sortable: true}, */
	   {id:"shippingId", name:"Shipping ID", field: "shippingId", sortable: true},
	   {id:"firstname", name:"First Name", field: "firstname", sortable: true},
	   {id:"lastname", name:"Last Name", field: "lastname", sortable: true},
	   {id:"street1", name:"Street1", field: "street1", sortable: true},
	   {id:"street2", name:"Street2", field: "street2", sortable: true},
	   {id:"country", name:"Country", field: "country", sortable: true},
	   {id:"state", name:"State", field: "state", sortable: true},
	   {id:"city", name:"City", field: "city", sortable: true},
	   {id:"zipcode", name:"Zip Code", field: "zipcode", sortable: true}
	  ];
              
var poFedexShippingOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var poFedexShippingSortcol = "shippingId";
var poFedexShippingSortdir = 1;
var percentCompleteThreshold = 0;
var poFedexShippingSearchString = "";

//filter option
function poFedexShippingFilter(item, args) {
	var x= item["shippingId"];
	if (args.poFedexShippingSearchString  != ""
			&& x.indexOf(args.poFedexShippingSearchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.poFedexShippingSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function poFedexShippingComparer(a, b) {
	var x = a[poFedexShippingSortcol], y = b[poFedexShippingSortcol];
	if(!isNaN(x)){
	 return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
	
function poFedexShippingToggleFilterRow() {
	poFedexShippingGrid.setTopPanelVisibility(!poFedexShippingGrid.getOptions().showTopPanel);
}
	
$("#poFedexShipping_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});

function poFedexShippingGridValues(jsonData, pageInfo) {	
	poFedexShippingPagingInfo = pageInfo;
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (poFedexShippingData[i] = {});
			d["id"] = data.id;
			d["shippingId"] = data.id;
			d["firstname"] = data.firstName;
			d["lastname"] = data.lastName;
			d["street1"] = data.addressLine1;
			d["street2"] = data.addressLine2;
			d["country"] = data.country;
			d["state"] = data.state;
			d["city"] = data.city;
			d["zipcode"] = data.zipCode;
		}
	}
	
	poFedexShippingDataView = new Slick.Data.DataView({inlineFilters: true });
	poFedexShippingGrid = new Slick.Grid("#poFedexShipping_Grid", poFedexShippingDataView, poFedexShippingColumn, poFedexShippingOptions);
	poFedexShippingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	poFedexShippingGrid.setSelectionModel(new Slick.RowSelectionModel());
	poFedexShippingGrid.registerPlugin(poFedexShippingCheckboxSelector);
	if(poFedexShippingPagingInfo != null){
		var poFedexShippingGridPager = new Slick.Controls.Pager(poFedexShippingDataView, poFedexShippingGrid, $("#poFedexShipping_pager"),poFedexShippingPagingInfo);
	}
	var poFedexShippingColumnPicker = new Slick.Controls.ColumnPicker(poFedexShippingColumn, poFedexShippingGrid, poFedexShippingOptions);

	  
	// move the filter panel defined in a hidden div into poFedexShippingGrid top panel
	//$("#poFedexShipping_inlineFilterPanel").appendTo(poFedexShippingGrid.getTopPanel()).show();
	  
	poFedexShippingGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < poFedexShippingDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    poFedexShippingGrid.setSelectedRows(rows);
	    e.preventDefault();
	});
	poFedexShippingGrid.onSort.subscribe(function (e, args) {
	    poFedexShippingSortdir = args.sortAsc ? 1 : -1;
	    poFedexShippingSortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      poFedexShippingDataView.fastSort(poFedexShippingSortcol, args.sortAsc);
	    } else {
	      poFedexShippingDataView.sort(poFedexShippingComparer, args.sortAsc);
	    }
	});
	
	// wire up model events to drive the poFedexShippingGrid
	poFedexShippingDataView.onRowCountChanged.subscribe(function (e, args) {
	    poFedexShippingGrid.updateRowCount();
	    poFedexShippingGrid.render();
	});
	poFedexShippingDataView.onRowsChanged.subscribe(function (e, args) {
	    poFedexShippingGrid.invalidateRows(args.rows);
	    poFedexShippingGrid.render();
	});
	  
	// wire up the search textbox to apply the filter to the model
	$("#txtSearch,#txtSearch2").keyup(function (e) {
	    Slick.GlobalEditorLock.cancelCurrentEdit();
	    // clear on Esc
	    if (e.which == 27) {
	      this.value = "";
	    }
	    poFedexShippingSearchString = this.value;
	    updatepoFedexShippingFilter();
	});
	function updatepoFedexShippingFilter() {
	    poFedexShippingDataView.setFilterArgs({
	    poFedexShippingSearchString: poFedexShippingSearchString
	   });
	   poFedexShippingDataView.refresh();
	}
	  
	poFedexShippingDataView.beginUpdate();
	poFedexShippingDataView.setItems(poFedexShippingData);
	poFedexShippingDataView.setFilterArgs({
	    percentCompleteThreshold: percentCompleteThreshold,
	    poFedexShippingSearchString: poFedexShippingSearchString
	});
	poFedexShippingDataView.setFilter(poFedexShippingFilter);
	poFedexShippingDataView.endUpdate();
	  
	poFedexShippingDataView.syncGridSelection(poFedexShippingGrid, true);
	poFedexShippingDataView.refresh();
	$("#gridContainer").resizable();
	//poFedexShippingGrid.resizeCanvas();
}
 
function pagingControl(move,id){		
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(poFedexShippingPagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(poFedexShippingPagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(poFedexShippingPagingInfo.pageNum)-1;
	}	
}	

function createFedexLabelForPO(){
	var serviceType = $('#poFedex_serviceType').val();
	var signatureType = $('#poFedex_signatureType').val();
	var companyName = $('#poFedex_companyName').val();
	var count=0; 	
	var shippingId;
	var row = poFedexShippingGrid.getSelectedRows([0])[0];
	if(row>=0){
		shippingId = poFedexShippingGrid.getDataItem(row).shippingId;
	
		var eventTable = document.getElementsByClassName('slick-cell-checkboxsel');
		for(var j=0;j<eventTable.length;j++){
			var eventCheckbox = eventTable[j].getElementsByTagName('input');
			for ( var i = 0; i < eventCheckbox.length; i++) {
				if(eventCheckbox[i].type =='checkbox'){
					if(eventCheckbox[i].checked == true){
						count++;
					}
				}
			}
		}	
		if(count==1){
			
			if(serviceType == 'select'){
				jAlert("Please choose service type");
				return false;
			}		
			$.ajax({
				url : "/Accounting/CreateFedexLabelForPO",
				type : "post",
				dataType : "json",
				data : "poId="+$('#poFedex_poId').val()+"&shippingId="+shippingId+"&serviceType="+serviceType+"&signatureType="+signatureType+"&companyName="+companyName,
				success : function(res) {
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						$('#create-fedex-label').modal('hide');
						getPOGridData(0);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
				},
				error : function(error) {
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});	
		}else{
			jAlert('Please select only one shipping address.');
			return false;
		}
	}else{
		jAlert('Please select shipping address.');
		return false;
	}
}