
	//Invoice - Order Details
	function setInvoiceOrderDetails(jsonData){
		$('#view-order-detail').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#vo_successDiv').hide();
			$('#vo_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#vo_errorDiv').hide();
			$('#vo_errorMsg').text('');
		}
		if(jsonData.successMessage != null){
			$('#vo_successDiv').show();
			$('#vo_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#vo_errorDiv').show();
			$('#vo_errorMsg').text(jsonData.errorMessage);
		}*/
		if(jsonData.msg != null && jsonData.msg != ""){
			jAlert(jsonData.msg);
		}
		$('#vo_orderNo').val(jsonData.orderNoStr);
		$('#vo_manualNote').val('');
		$('#auditNote').val('');
		
		var primaryPaymentMethod;
		var secondaryPaymentMethod;
		var primaryPaymentAmount;
		var secondaryPaymentAmount;		
		var order = jsonData.order;
		if(order != null && order != ""){
			$('#vo_orderId').text(order.id);
			$('#vo_orderCreateDate').text(order.orderCreateDate);
			$('#vo_shippingMethod').text(order.shippingMethod);
			$('#vo_orderTotal').text(order.orderTotal);
			if(order.primaryPaymentMethod == 'BANK_OR_CHEQUE'){
				$('#vo_primaryPayMethod').text('BANK_OR_CHECK');
			}else{
				$('#vo_primaryPayMethod').text(order.primaryPaymentMethod);
			}
			$('#vo_eventDetailHeader').text(order.eventName+","+order.eventDateStr+","+order.eventTimeStr+","+order.venueName);
			$('#vo_section').text(order.section);
			$('#vo_quantity').text(order.qty);
			$('#vo_ticketPrice').text(order.ticketPrice);
			$('#vo_seatLow').text(order.seatLow);
			$('#vo_seatHigh').text(order.seatHigh);
			$('#vo_soldPrice').text(order.soldPrice);
			primaryPaymentMethod = order.primaryPaymentMethod;
			secondaryPaymentMethod = order.secondaryPaymentMethod;
			primaryPaymentAmount = order.primaryPayAmt;
			secondaryPaymentAmount = order.secondaryPayAmt;
		}else{
			$('#vo_orderId').text('');
			$('#vo_orderCreateDate').text('');
			$('#vo_shippingMethod').text('');
			$('#vo_orderTotal').text('');
			$('#vo_primaryPayMethod').text('');
			$('#vo_eventDetailHeader').text('');
			$('#vo_section').text('');
			$('#vo_quantity').text('');
			$('#vo_ticketPrice').text('');
			$('#vo_seatLow').text('');
			$('#vo_seatHigh').text('');
			$('#vo_soldPrice').text('');
		}
		var orderDetails = jsonData.orderDetails;
		if(orderDetails != null && orderDetails != ""){
			$('#vo_customerName').text(orderDetails.billingFirstName+" "+orderDetails.billingLastName);
			$('#vo_customerEmail').text(orderDetails.billingEmail);
		}else{
			$('#vo_customerName').text('');
			$('#vo_customerEmail').text('');
		}
		var invoice = jsonData.invoice;
		if(invoice != null && invoice != ""){
			$('#vo_note_invoiceId').val(invoice.invoiceId);
		}
		if(jsonData.audits != null && jsonData.pagingInfo != null){
			orderDetailGridValues(jsonData.audits, jsonData.pagingInfo);
		}else{
			orderDetailGridValues(jsonData.audits, '');
		}
				
		if (primaryPaymentMethod == "CREDITCARD") {
			$('#vo_primaryPayAmt').text(primaryPaymentAmount);
			$('#vo_usedRewardPoints').text(secondaryPaymentAmount);
		} else if (primaryPaymentMethod == "PARTIAL_REWARDS") {
			$('#vo_primaryPayAmt').text(secondaryPaymentAmount);
			$('#vo_usedRewardPoints').text(primaryPaymentAmount);
		} else if (primaryPaymentMethod == "FULL_REWARDS") {
			$('#vo_primaryPayAmt').text(secondaryPaymentAmount);
			$('#vo_usedRewardPoints').text(primaryPaymentAmount);
		}else{
			$('#vo_primaryPayAmt').text('');
			$('#vo_usedRewardPoints').text('');
		}
	}

function addNote(){
	var note = $('#vo_manualNote').val();
	if(note=='' || note == null){
		alert("Please Add Some values in note.");
		return false;
	}
	//$('#vo_addNotes').submit();
	$.ajax({
		url : "/Accounting/GetCustomerOrderDetails",
		type : "post",
		dataType: "json",
		data : $("#vo_addNotes").serialize(),
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			/*if(jsonData.successMessage != null){
				$('#vo_successDiv').show();
				$('#vo_successMsg').text(jsonData.successMessage);				
			}
			if(jsonData.errorMessage != null){
				$('#vo_errorDiv').show();
				$('#vo_errorMsg').text(jsonData.errorMessage);
			}*/
			if(jsonData.status == 1){
				$('#vo_manualNote').val('');
			}
			if(jsonData.audits != null && jsonData.pagingInfo != null){
				orderDetailGridValues(jsonData.audits, jsonData.pagingInfo);
			}else{
				orderDetailGridValues(jsonData.audits, '');
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
			}
		}, error : function(error){
			jAlert("There is something wrong. Please try again"+error,"Error");
			return false;
		} 
	});
}


var orderDetailSelectedRow='';
var id='';
var orderDetailData=[];
var orderDetailDataView;
var orderDetailPagingInfo;
var orderDetailGrid;
var orderDetailColumns = [ {
	id : "id",
	name : "ID",
	field : "id",
	sortable : true
},{
	id : "invoiceId",
	name : "Invoice Id",
	field : "invoiceId",
	sortable : true
},{
	id : "action",
	name : "Action",
	field : "action",
	sortable : true
},{
	id : "createdDate",
	name : "Date & Time",
	field : "createdDate",
	sortable : true
}, {
	id : "createdBy",
	name : "Performed By",
	field : "createdBy",
	width:80,
	sortable : true
},{
	id : "note",
	name : "Note",
	field : "note",
	sortable : true
}];

var orderDetailOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var orderDetailSortcol = "id";
var orderDetailSortdir = 1;
var orderDetailSearchString = "";
var percentCompleteThreshold = 0;

function orderDetailFilter(item, args) {
	var x= item["id"];
	if (args.orderDetailSearchString  != ""
			&& x.indexOf(args.orderDetailSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.orderDetailSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function orderDetailComparer(a, b) {
	var x = a[orderDetailSortcol], y = b[orderDetailSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function orderDetailGridToggleFilterRow() {
		orderDetailGrid.setTopPanelVisibility(!orderDetailGrid.getOptions().showTopPanel);
	}

function orderDetailGridValues(jsonData, pageInfo) {
	orderDetailPagingInfo = pageInfo;
	orderDetailData=[];	
	if(jsonData!=null && jsonData!= '' && jsonData.length > 0){
		for(var i=0;i<jsonData.length;i++){
			var  data= jsonData[i];
			var d = (orderDetailData[i] = {});
			d["id"] = data.id;			
			d["invoiceId"] = data.invoiceId;
			d["action"] = data.action;
			d["createdDate"] = data.createdDate;
			d["createdBy"] = data.createdBy;
			d["note"] = data.note;
		}	
	}
	
	orderDetailDataView = new Slick.Data.DataView({
		inlineFilters : true
	});
	orderDetailGrid = new Slick.Grid("#orderDetail_grid", orderDetailDataView, orderDetailColumns, orderDetailOptions);
	orderDetailGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	orderDetailGrid.setSelectionModel(new Slick.RowSelectionModel());		
	var orderDetailPager = new Slick.Controls.Pager(orderDetailDataView, orderDetailGrid, $("#orderDetail_pager"),orderDetailPagingInfo);
	var orderDetailColumnpicker = new Slick.Controls.ColumnPicker(orderDetailColumns, orderDetailGrid,
		orderDetailOptions);
	
	
	orderDetailGrid.onSort.subscribe(function(e, args) {
		orderDetailSortdir = args.sortAsc ? 1 : -1;
		orderDetailSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			orderDetailDataView.fastSort(orderDetailSortcol, args.sortAsc);
		} else {
			orderDetailDataView.sort(orderDetailComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	orderDetailDataView.onRowCountChanged.subscribe(function(e, args) {
		orderDetailGrid.updateRowCount();
		orderDetailGrid.render();
	});
	orderDetailDataView.onRowsChanged.subscribe(function(e, args) {
		orderDetailGrid.invalidateRows(args.rows);
		orderDetailGrid.render();
	});
		
	function updateOrderGridFilter() {
		orderDetailDataView.setFilterArgs({
			orderDetailSearchString : orderDetailSearchString
		});
		orderDetailDataView.refresh();
	}
	var eventrowIndex;
	orderDetailGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = orderDetailGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
			}
			orderDetailSelectedRow = orderDetailGrid.getSelectedRows([0])[0];
			if (orderDetailSelectedRow >=0) {
				$('#auditNote').val(orderDetailGrid.getDataItem(temprEventRowIndex).note);
			}else{
				$('#auditNote').val('');
			}			
		});

	// initialize the model after all the events have been hooked up
	orderDetailDataView.beginUpdate();
	orderDetailDataView.setItems(orderDetailData);
	
	orderDetailDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			orderDetailSearchString : orderDetailSearchString
		});
	orderDetailDataView.setFilter(orderDetailFilter);
		
	orderDetailDataView.endUpdate();
	orderDetailDataView.syncGridSelection(orderDetailGrid, true);
	$("#gridContainer").resizable();
	//orderDetailGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');	
}