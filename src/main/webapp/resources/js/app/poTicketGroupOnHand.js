
	//PO Ticket Group ON-HAND/NOT-ON-HAND Status
	$("div#divLoading").addClass('show');
	var poTicketGroupCheckboxSelector = new Slick.CheckboxSelectColumn({
		 cssClass: "slick-cell-checkboxsel"
	});
	
	var poTicketGroupPagingInfo;
	var poTicketGroupDataView;
	var poTicketGroupGrid;
	var poTicketGroupData = [];
	var poTicketGroupColumns = [ poTicketGroupCheckboxSelector.getColumnDefinition(),
	{
		id : "id",
		name : "Ticket Group Id",
		field : "id",
		width:80,
		sortable : true
	},{
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		width:80,
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width:80,
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		width:80,
		sortable : true
	},  {
		id: "section", 
		name: "Section", 
		field: "section",
		width:80,		
		sortable: true
	},{
		id: "row", 
		name: "Row", 
		field: "row", 
		width:80,
		sortable: true,
	},{
		id: "seatLow", 
		name: "Seat Low", 
		field: "seatLow", 
		width:80,
		sortable: true
	} , {
		id: "seatHigh", 
		name: "Seat High", 
		field: "seatHigh",
		width:80,
		sortable: true
	} , {
		id: "quantity", 
		name: "Quantity", 
		field: "quantity",
		width:80,
		sortable: true
	} , {
		id: "price", 
		name: "Price", 
		field: "price",
		width:80,
		sortable: true
	} , {
		id: "onhand", 
		name: "On Hand", 
		field: "onhand",
		width:80,
		sortable: true
	}, {
		id: "invoiceId", 
		name: "Invoice Id", 
		field: "invoiceId",
		width:80,
		sortable: true
	}];
	
	var poTicketGroupOptions = {
		editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25
	};
	var poTicketGroupGridSortcol = "id";
	var poTicketGroupGridSortdir = 1;
	var percentCompleteThreshold = 0;
	var poTicketGroupGridSearchString = "";
	
	function poTicketGroupGridFilter(item, args) {
		var x= item["id"];
		if (args.poTicketGroupGridSearchString  != ""
				&& x.indexOf(args.poTicketGroupGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.poTicketGroupGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function poTicketGroupGridComparer(a, b) {
		var x = a[poTicketGroupGridSortcol], y = b[poTicketGroupGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	function poTicketGroupGridToggleFilterRow() {
		poTicketGroupGrid.setTopPanelVisibility(!poTicketGroupGrid.getOptions().showTopPanel);
	}
	
	$("#poTicketGroup_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
		$(e.target).addClass("ui-state-hover")
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
	});
	
	function poTicketGroupGridValues(jsonData, pageInfo) {
		poTicketGroupPagingInfo = pageInfo;
		poTicketGroupData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (poTicketGroupData[i] = {});
				d["id"] = data.id;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDate;
				d["venue"] = data.venue;
				d["section"] = data.section;
				d["row"] = data.row;
				d["seatLow"] = data.seatLow;
				d["seatHigh"] = data.seatHigh;
				d["quantity"] = data.quantity;
				d["price"] = data.price;
				d["onhand"] = data.onHand;
				d["invoiceId"] = data.invoiceId;
			}
		}

		poTicketGroupDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		poTicketGroupGrid = new Slick.Grid("#poTicketGroup_grid", poTicketGroupDataView, poTicketGroupColumns, poTicketGroupOptions);
		poTicketGroupGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		poTicketGroupGrid.setSelectionModel(new Slick.RowSelectionModel());
		poTicketGroupGrid.registerPlugin(poTicketGroupCheckboxSelector);
		if(poTicketGroupPagingInfo != null){
			var poTicketGroupGridPager = new Slick.Controls.Pager(poTicketGroupDataView, poTicketGroupGrid, $("#poTicketGroup_pager"),poTicketGroupPagingInfo);
		}
		var poTicketGroupGridColumnpicker = new Slick.Controls.ColumnPicker(poTicketGroupColumns, poTicketGroupGrid,
				poTicketGroupOptions);

		// move the filter panel defined in a hidden div into poTicketGroupGrid top panel
		$("#poTicketGroup_inlineFilterPanel").appendTo(poTicketGroupGrid.getTopPanel()).show();
		
		poTicketGroupGrid.onSort.subscribe(function(e, args) {
			poTicketGroupGridSortdir = args.sortAsc ? 1 : -1;
			poTicketGroupGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				poTicketGroupDataView.fastSort(poTicketGroupGridSortcol, args.sortAsc);
			} else {
				poTicketGroupDataView.sort(poTicketGroupGridComparer, args.sortAsc);
			}
		});

		poTicketGroupGrid.onCellChange.subscribe(function (e,args) { 
			var tempPOTicketGroupGridRowIndex = poTicketGroupGrid.getSelectedRows();
			var poTicketGroupId;
			var poTicketGroupNotes; 
			$.each(tempPOTicketGroupGridRowIndex, function (index, value) {
				poTicketGroupId = poTicketGroupGrid.getDataItem(value).id;
				poTicketGroupNotes = poTicketGroupGrid.getDataItem(value).notes;
			});
			saveNotes(poTicketGroupId,poTicketGroupNotes);
		});
		
		// wire up model ticketGroups to drive the poTicketGroupGrid
		poTicketGroupDataView.onRowCountChanged.subscribe(function(e, args) {
			poTicketGroupGrid.updateRowCount();
			poTicketGroupGrid.render();
		});
		poTicketGroupDataView.onRowsChanged.subscribe(function(e, args) {
			poTicketGroupGrid.invalidateRows(args.rows);
			poTicketGroupGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#poTicketGroupGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			poTicketGroupGridSearchString = this.value;
			updatePOTicketGroupGridFilter();
		});
		function updatePOTicketGroupGridFilter() {
			poTicketGroupDataView.setFilterArgs({
				poTicketGroupGridSearchString : poTicketGroupGridSearchString
			});
			poTicketGroupDataView.refresh();
		}
		
		// initialize the model after all the ticketGroups have been hooked up
		poTicketGroupDataView.beginUpdate();
		poTicketGroupDataView.setItems(poTicketGroupData);
		poTicketGroupDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			poTicketGroupGridSearchString : poTicketGroupGridSearchString
		});
		poTicketGroupDataView.setFilter(poTicketGroupGridFilter);
		
		poTicketGroupDataView.endUpdate();
		poTicketGroupDataView.syncGridSelection(poTicketGroupGrid, true);
		$("#gridContainer").resizable();
		//poTicketGroupGrid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}
	
	function updatePOTicketOnHand(onHand){
		var poId = $('#poTicketGroup_poId').val();
		var ids = getSelectedPOTicketGroupGridId();
		if(ids==null || ids==''){
			jAlert("Please select atleast one record to update");
			return;
		}else{
			//window.location="${pageContext.request.contextPath}/Accounting/UpdatePOTicketGroupOnhandStatus?onHand="+onHand+"&ticketGroupIds="+ids+"&poId=${poId}";
			$.ajax({
				url : "/Accounting/UpdatePOTicketGroupOnHandStatus",
				type : "post",
				dataType : "json",
				data : "onHand="+onHand+"&ticketGroupIds="+ids+"&poId="+poId,
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1){
						getPOTicketGroupOnHandStatus(poId);
						getPOGridData(0);
					}
					if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
						jAlert(jsonData.msg);
					}
					/*if(jsonData.successMessage != null){
						$('#poTicketGroup_successDiv').show();
						$('#poTicketGroup_successMsg').text(jsonData.successMessage);						
					}
					if(jsonData.errorMessage != null){
						$('#poTicketGroup_errorDiv').show();
						$('#poTicketGroup_errorMsg').text(jsonData.errorMessage);
					}*/
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}
	}
	
	function getSelectedPOTicketGroupGridId() {
		var tempPOTicketGroupGridRowIndex = poTicketGroupGrid.getSelectedRows();
		
		var poTicketGroupIdStr='';
		$.each(tempPOTicketGroupGridRowIndex, function (index, value) {
			poTicketGroupIdStr += ','+poTicketGroupGrid.getDataItem(value).id;
		});
		
		if(poTicketGroupIdStr != null && poTicketGroupIdStr!='') {
			poTicketGroupIdStr = poTicketGroupIdStr.substring(1, poTicketGroupIdStr.length);
			 return poTicketGroupIdStr;
		}
	}
