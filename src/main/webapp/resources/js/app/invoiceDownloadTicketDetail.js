
//var selectedRow='';
var id='';
var downloadTicketDataView;
var downloadTicketPagingInfo;
var downloadTicketGrid;
//var downloadTicketInvoiceId="${invoiceId}";
var downloadTicketData = [];
var downloadTicketColumns = [ {
	id : "id",
	name : "ID",
	field : "id",
	sortable : true
},{
	id : "invoiceId",
	name : "Invoice Id",
	field : "invoiceId",
	sortable : true
},{
	id : "customerId",
	name : "Customer Id",
	field : "customerId",
	sortable : true
},{
	id : "ticketType",
	name : "Ticket Type",
	field : "ticketType",
	sortable : true
}, {
	id : "ticketName",
	name : "Ticket Name",
	field : "ticketName",
	width:80,
	sortable : true
},{
	id : "email",
	name : "E-Mail",
	field : "email",
	width:80,
	sortable : true
},{
	id : "ipAddress",
	name : "IP Address",
	field : "ipAddress",
	width:80,
	sortable : true
},{
	id : "downloadPlatform",
	name : "Download Platform",
	field : "downloadPlatform",
	sortable : true
},{
	id : "downloadDateTime",
	name : "Download Date",
	field : "downloadDateTime",
	sortable : true
}];

var downloadTicketOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var downloadTicketSortcol = "id";
var downloadTicketSortdir = 1;
var downloadTicketSearchString = "";
var percentCompleteThreshold = 0;

function downloadTicketFilter(item, args) {
	var x= item["id"];
	if (args.downloadTicketSearchString  != ""
			&& x.indexOf(args.downloadTicketSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.downloadTicketSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function downloadTicketComparer(a, b) {
	var x = a[downloadTicketSortcol], y = b[downloadTicketSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function downloadTicketGridToggleFilterRow() {
		downloadTicketGrid.setTopPanelVisibility(!downloadTicketGrid.getOptions().showTopPanel);
	}

	$("#downloadTicket_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function downloadTicketGridValues(jsonData, pageInfo) {
	downloadTicketPagingInfo = pageInfo;
	downloadTicketData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];
			var d = (downloadTicketData[i] = {});
			d["id"] = data.id;			
			d["invoiceId"] = data.invoiceId;
			d["customerId"] = data.customerId;
			d["ticketType"] = data.ticketType;
			d["ticketName"] = data.ticketName;
			d["email"] = data.email;
			d["ipAddress"] = data.ipAddress;
			d["downloadPlatform"] = data.downloadPlatform;
			d["downloadDateTime"] = data.downloadDateTime;
		}
	}
			
		downloadTicketDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		downloadTicketGrid = new Slick.Grid("#downloadTicket_grid", downloadTicketDataView, downloadTicketColumns, downloadTicketOptions);
		downloadTicketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		downloadTicketGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var ctdPager = new Slick.Controls.Pager(downloadTicketDataView, downloadTicketGrid, $("#downloadTicket_pager"),downloadTicketPagingInfo);
		var ctdColumnpicker = new Slick.Controls.ColumnPicker(downloadTicketColumns, downloadTicketGrid,
			downloadTicketOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		//$("#downloadTicket_inlineFilterPanel").appendTo(downloadTicketGrid.getTopPanel()).show();
		
		downloadTicketGrid.onSort.subscribe(function(e, args) {
		downloadTicketSortdir = args.sortAsc ? 1 : -1;
		downloadTicketSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			downloadTicketDataView.fastSort(downloadTicketSortcol, args.sortAsc);
		} else {
			downloadTicketDataView.sort(downloadTicketComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	downloadTicketDataView.onRowCountChanged.subscribe(function(e, args) {
		downloadTicketGrid.updateRowCount();
		downloadTicketGrid.render();
	});
	downloadTicketDataView.onRowsChanged.subscribe(function(e, args) {
		downloadTicketGrid.invalidateRows(args.rows);
		downloadTicketGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
		$("#downloadTicketGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			downloadTicketSearchString = this.value;
			updateDownloadTicketGridFilter();
		});
		
	function updateDownloadTicketGridFilter() {
		downloadTicketDataView.setFilterArgs({
			downloadTicketSearchString : downloadTicketSearchString
		});
		downloadTicketDataView.refresh();
	}
	/*var eventrowIndex;
	downloadTicketGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = downloadTicketGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				//getCustomerInfoForInvoice(temprEventRowIndex);
			}
			
			selectedRow = downloadTicketGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = downloadTicketGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});*/

	// initialize the model after all the events have been hooked up
	downloadTicketDataView.beginUpdate();
	downloadTicketDataView.setItems(downloadTicketData);
	
	downloadTicketDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			downloadTicketSearchString : downloadTicketSearchString
		});
	downloadTicketDataView.setFilter(downloadTicketFilter);
		
	downloadTicketDataView.endUpdate();
	downloadTicketDataView.syncGridSelection(downloadTicketGrid, true);
	$("#gridContainer").resizable();
	/*downloadTicketGrid.resizeCanvas();
	$("div#divLoading").removeClass('show');
	if(downloadTicketInvoiceId>0){
		for(var i=0;i<downloadTicketGrid.getDataLength();i++){
			if(downloadTicketGrid.getDataItem(i).id==downloadTicketInvoiceId){
				downloadTicketGrid.setSelectedRows([i]);
				break;
			}
		}
	}*/	
}