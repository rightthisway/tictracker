
	//PO Payment History
	var poPayHistoryPagingInfo;
	var poPaymentHistoryDataView;
	var poPaymentHistoryGrid;
	var poPaymentHistoryData = [];
	var poPaymentHistoryColumns = [ {
		id : "name",
		name : "Card/Account Name",
		field : "name",
		sortable : true
	},{
		id : "paymentType",
		name : "Payment Type",
		field : "paymentType",
		sortable : true
	},{
		id : "cardType",
		name : "Card Type",
		field : "cardType",
		sortable : true
	},{
		id : "cardLastFourDigit",
		name : "Card Last Four Digit",
		field : "cardLastFourDigit",
		sortable : true
	},{
		id : "routingLastFourDigit",
		name : "Routing Last Four Digit",
		field : "routingLastFourDigit",
		sortable : true
	}, {
		id : "accountLastFourDigit",
		name : "Account Last Four Digit",
		field : "accountLastFourDigit",
		width:80,
		sortable : true
	},{
		id : "chequeNo",
		name : "Cheque No",
		field : "chequeNo",
		sortable : true
	},{
		id : "paymentStatus",
		name : "Payment Status",
		field : "paymentStatus",
		sortable : true
	},{
		id : "amount",
		name : "amount",
		field : "amount",
		sortable : true
	},{
		id : "paymentDate",
		name : "Payment Date",
		field : "paymentDate",
		sortable : true
	},{
		id : "paymentNote",
		name : "Payment Note",
		field : "paymentNote",
		sortable : true
	}];


	var poPaymentHistoryOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
	};
	var poPaymentHistorySortcol = "id";
	var poPaymentHistorySortdir = 1;
	var poPaymentHistorySearchString = "";

	function poPaymentHistoryFilter(item, args) {
		var x= item["invoiceId"];
		if (args.poPaymentHistorySearchString  != ""
				&& x.indexOf(args.poPaymentHistorySearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.poPaymentHistorySearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function poPaymentHistoryComparer(a, b) {
		var x = a[poPaymentHistorySortcol], y = b[poPaymentHistorySortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

	function poPaymentHistoryGridValues(jsonData, pageInfo) {
		poPayHistoryPagingInfo = pageInfo;
		poPaymentHistoryData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (poPaymentHistoryData[i] = {});				
				d["id"] = data.id;
				d["name"] = data.name;
				d["paymentType"] = data.paymentType;
				d["cardType"] = data.cardType;
				d["cardLastFourDigit"] = data.cardLastFourDigit;
				d["routingLastFourDigit"] = data.routingLastFourDigit;
				d["accountLastFourDigit"] = data.accountLastFourDigit;
				d["chequeNo"] = data.chequeNo;
				d["paymentStatus"] = data.paymentStatus;
				d["amount"] = data.amount;
				d["paymentDate"] = data.paymentDate;
				d["paymentNote"] = data.paymentNote;
			}
		}
				
		poPaymentHistoryDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		poPaymentHistoryGrid = new Slick.Grid("#poPaymentHistory_grid", poPaymentHistoryDataView, poPaymentHistoryColumns, poPaymentHistoryOptions);
		poPaymentHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		poPaymentHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		var paymentHistoryPager = new Slick.Controls.Pager(poPaymentHistoryDataView, poPaymentHistoryGrid, $("#poPaymentHistory_pager"),poPayHistoryPagingInfo);
		var paymentHistoryColumnpicker = new Slick.Controls.ColumnPicker(poPaymentHistoryColumns, poPaymentHistoryGrid,
			poPaymentHistoryOptions);

		poPaymentHistoryGrid.onSort.subscribe(function(e, args) {
			poPaymentHistorySortdir = args.sortAsc ? 1 : -1;
			poPaymentHistorySortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				poPaymentHistoryDataView.fastSort(poPaymentHistorySortcol, args.sortAsc);
			} else {
				poPaymentHistoryDataView.sort(poPaymentHistoryComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the eventGrid
		poPaymentHistoryDataView.onRowCountChanged.subscribe(function(e, args) {
			poPaymentHistoryGrid.updateRowCount();
			poPaymentHistoryGrid.render();
		});
		poPaymentHistoryDataView.onRowsChanged.subscribe(function(e, args) {
			poPaymentHistoryGrid.invalidateRows(args.rows);
			poPaymentHistoryGrid.render();
		});

		// initialize the model after all the events have been hooked up
		poPaymentHistoryDataView.beginUpdate();
		poPaymentHistoryDataView.setItems(poPaymentHistoryData);
		poPaymentHistoryDataView.endUpdate();
		poPaymentHistoryDataView.syncGridSelection(poPaymentHistoryGrid, true);
		$("#gridContainer").resizable();
		//poPaymentHistoryGrid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}

		
	var rtwPOPaymentHistoryDataView;
	var rtwPOPaymentHistoryGrid;
	var rtwPOPaymentHistoryData = [];
	var rtwPOPaymentHistoryColumns = [ 
	{
		id : "creditCardNumber",
		name : "Credit Card Number",
		field : "creditCardNumber",
		sortable : true
	},{
		id : "checkNumber",
		name : "Check Number",
		field : "checkNumber",
		sortable : true
	},{
		id : "amount",
		name : "Amount",
		field : "amount",
		sortable : true
	},{
		id : "note",
		name : "Note",
		field : "note",
		sortable : true
	}, {
		id : "paidOn",
		name : "Paid On",
		field : "paidOn",
		width:80,
		sortable : true
	}, {
		id : "systemUser",
		name : "System User",
		field : "systemUser",
		sortable : true
	}, {
		id : "paymentType",
		name : "Payment Type",
		field : "paymentType",
		sortable : true
	},{
		id : "transOffice",
		name : "Transaction Office",
		field : "transOffice",
		sortable : true
	}];

	var rtwPOPaymentHistoryOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
	};
	var rtwPOPaymentHistorySortcol = "id";
	var rtwPOPaymentHistorySortdir = 1;
	var percentCompleteThreshold = 0;
	var rtwPOPaymentHistorySearchString = "";

	function rtwPOPaymentHistoryFilter(item, args) {
		var x= item["invoiceId"];
		if (args.rtwPOPaymentHistorySearchString  != ""
				&& x.indexOf(args.rtwPOPaymentHistorySearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.rtwPOPaymentHistorySearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function rtwPOPaymentHistoryComparer(a, b) {
		var x = a[rtwPOPaymentHistorySortcol], y = b[rtwPOPaymentHistorySortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*function rtwPOPaymentToggleFilterRow() {
		rtwPOPaymentHistoryGrid.setTopPanelVisibility(!rtwPOPaymentHistoryGrid.getOptions().showTopPanel);
	}
		
	$("#rtwPOPaymentHistory_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover")
	});*/

	function rtwPOPaymentHistoryGridValues(jsonData, pageInfo) {
		poPayHistoryPagingInfo = pageInfo;
		rtwPOPaymentHistoryData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (rtwPOPaymentHistoryData[i] = {});
				d["id"] = i;
				d["creditCardNumber"] = data.creditCardNumber;
				d["checkNumber"] = data.checkNumber;
				d["amount"] = data.amount;
				d["note"] = data.note;
				d["paidOn"] = data.paidOnStr;
				d["systemUser"] = data.systemUser;
				d["paymentType"] = data.paymentType;
				d["transOffice"] = data.transOffice;
			}
		}
			
		rtwPOPaymentHistoryDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		rtwPOPaymentHistoryGrid = new Slick.Grid("#rtwPOPaymentHistory_grid", rtwPOPaymentHistoryDataView, rtwPOPaymentHistoryColumns, rtwPOPaymentHistoryOptions);
		rtwPOPaymentHistoryGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		rtwPOPaymentHistoryGrid.setSelectionModel(new Slick.RowSelectionModel());
		var rtwPOPaymentHistoryPager = new Slick.Controls.Pager(rtwPOPaymentHistoryDataView, rtwPOPaymentHistoryGrid, $("#rtwPOPaymentHistory_pager"),poPayHistoryPagingInfo);
		var rtwPOPaymentHistoryColumnpicker = new Slick.Controls.ColumnPicker(rtwPOPaymentHistoryColumns, rtwPOPaymentHistoryGrid,
			rtwPOPaymentHistoryOptions);

		// move the filter panel defined in a hidden div into rtwPOPaymentHistoryGrid top panel
	    //$("#inlineFilterPanel").appendTo(rtwPOPaymentHistoryGrid.getTopPanel()).show();
	  
		rtwPOPaymentHistoryGrid.onKeyDown.subscribe(function (e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
			  return false;
			}
			var rows = [];
			for (var i = 0; i < rtwPOPaymentHistoryDataView.getLength(); i++) {
			  rows.push(i);
			}
			rtwPOPaymentHistoryGrid.setSelectedRows(rows);
			e.preventDefault();
		});
	  
		rtwPOPaymentHistoryGrid.onSort.subscribe(function(e, args) {
			rtwPOPaymentHistorySortdir = args.sortAsc ? 1 : -1;
			rtwPOPaymentHistorySortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				rtwPOPaymentHistoryDataView.fastSort(rtwPOPaymentHistorySortcol, args.sortAsc);
			} else {
				rtwPOPaymentHistoryDataView.sort(rtwPOPaymentHistoryComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the eventGrid
		rtwPOPaymentHistoryDataView.onRowCountChanged.subscribe(function(e, args) {
			rtwPOPaymentHistoryGrid.updateRowCount();
			rtwPOPaymentHistoryGrid.render();
		});
		rtwPOPaymentHistoryDataView.onRowsChanged.subscribe(function(e, args) {
			rtwPOPaymentHistoryGrid.invalidateRows(args.rows);
			rtwPOPaymentHistoryGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#txtSearch,#txtSearch2").keyup(function (e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
			  this.value = "";
			}
			rtwPOPaymentHistorySearchString = this.value;
			updatertwPOPaymentHistoryFilter();
		});
		
		function updatertwPOPaymentHistoryFilter() {
			rtwPOPaymentHistoryDataView.setFilterArgs({
			  rtwPOPaymentHistorySearchString: rtwPOPaymentHistorySearchString
			});
			rtwPOPaymentHistoryDataView.refresh();
		}
		  
		// initialize the model after all the events have been hooked up
		rtwPOPaymentHistoryDataView.beginUpdate();
		rtwPOPaymentHistoryDataView.setItems(rtwPOPaymentHistoryData);
		
		rtwPOPaymentHistoryDataView.setFilterArgs({
			percentCompleteThreshold: percentCompleteThreshold,
			rtwPOPaymentHistorySearchString: rtwPOPaymentHistorySearchString
		});
		rtwPOPaymentHistoryDataView.setFilter(rtwPOPaymentHistoryFilter);
		  
		rtwPOPaymentHistoryDataView.endUpdate();
		rtwPOPaymentHistoryDataView.syncGridSelection(rtwPOPaymentHistoryGrid, true);
		$("#gridContainer").resizable();
		//rtwPOPaymentHistoryGrid.resizeCanvas();
		$("div#divLoading").removeClass('show');
	}