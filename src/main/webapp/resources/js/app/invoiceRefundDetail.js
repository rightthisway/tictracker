
var selectedRow='';
var id='';
var refundDetailDataView;
var refundDetailGrid;
var refundDetailPagingInfo;
var refundDetailOrderId="${orderId}";
var refundDetailData = [];
var refundDetailColumns = [ /*{id : "id",name : "ID",field : "id",sortable : true},*/
	{
	id : "orderId",
	name : "Order Id",
	field : "orderId",
	sortable : true
},{
	id : "refundType",
	name : "Refund Type",
	field : "refundType",
	sortable : true
},{
	id : "transactionId",
	name : "Transaction Id",
	field : "transactionId",
	sortable : true
},{
	id : "refundId",
	name : "Refund Id",
	field : "refundId",
	sortable : true
}, {
	id : "amount",
	name : "Amount",
	field : "amount",
	width:80,
	sortable : true
},{
	id : "status",
	name : "Status",
	field : "status",
	sortable : true
},{
	id : "reason",
	name : "Reason",
	field : "reason",
	sortable : true
},{
	id : "revertedUsedRewardPoints",
	name : "Used Reward Points",
	field : "revertedUsedRewardPoints",
	sortable : true
},{
	id : "revertedEarnedRewardPoints",
	name : "Earned Reward Points",
	field : "revertedEarnedRewardPoints",
	sortable : true
},{
	id : "createdTime",
	name : "Refund Date",
	field : "createdTime",
	sortable : true
},{
	id : "refundedBy",
	name : "Refunded By",
	field : "refundedBy",
	sortable : true
}];

var refundDetailOptions = {
	enableCellNavigation : true,
	forceFitColumns : true,
	multiSelect : false,
	topPanelHeight : 25,
};
	
var refundDetailSortcol = "id";
var refundDetailSortdir = 1;
var refundDetailSearchString = "";
var percentCompleteThreshold = 0;

function refundDetailFilter(item, args) {
	var x= item["id"];
	if (args.refundDetailSearchString  != ""
			&& x.indexOf(args.refundDetailSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.refundDetailSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function refundDetailComparer(a, b) {
	var x = a[refundDetailSortcol], y = b[refundDetailSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}

function refundDetailGridToggleFilterRow() {
		refundDetailGrid.setTopPanelVisibility(!refundDetailGrid.getOptions().showTopPanel);
	}

//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
	$("#refundDetail_grid_toogle_search").addClass(
			"ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
	}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});
	
function refundDetailGridValues(jsonData, pageInfo) {
	refundDetailPagingInfo = pageInfo;
	refundDetailData=[];
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];
			var d = (refundDetailData[i] = {});
			d["id"] = data.invoiceRefundId;	
			d["refundType"] = data.refundType;
			d["orderId"] = data.refundOrderId;
			d["transactionId"] = data.refundTransactionId;
			d["refundId"] = data.refundId;
			d["amount"] = data.refundAmount;
			d["status"] = data.refundStatus;
			d["reason"] = data.refundReason;
			d["revertedUsedRewardPoints"] = data.revertedUsedRewardPoints;
			d["revertedEarnedRewardPoints"] = data.revertedEarnedRewardPoints;
			d["createdTime"] = data.refundCreatedTime;
			d["refundedBy"] = data.refundedBy;
		}
	}
			
		refundDetailDataView = new Slick.Data.DataView({
			inlineFilters : true
		});
		refundDetailGrid = new Slick.Grid("#refundDetail_grid", refundDetailDataView, refundDetailColumns, refundDetailOptions);
		refundDetailGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		refundDetailGrid.setSelectionModel(new Slick.RowSelectionModel());		
		var refundDetailPager = new Slick.Controls.Pager(refundDetailDataView, refundDetailGrid, $("#refundDetail_pager"),refundDetailPagingInfo);
		var refundDetailColumnPicker = new Slick.Controls.ColumnPicker(refundDetailColumns, refundDetailGrid,
			refundDetailOptions);
		
		// move the filter panel defined in a hidden div into eventGrid top panel
		$("#refundDetail_inlineFilterPanel").appendTo(refundDetailGrid.getTopPanel()).show();
		
		refundDetailGrid.onSort.subscribe(function(e, args) {
		refundDetailSortdir = args.sortAsc ? 1 : -1;
		refundDetailSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			refundDetailDataView.fastSort(refundDetailSortcol, args.sortAsc);
		} else {
			refundDetailDataView.sort(refundDetailComparer, args.sortAsc);
		}
	});
	// wire up model events to drive the eventGrid
	refundDetailDataView.onRowCountChanged.subscribe(function(e, args) {
		refundDetailGrid.updateRowCount();
		refundDetailGrid.render();
	});
	refundDetailDataView.onRowsChanged.subscribe(function(e, args) {
		refundDetailGrid.invalidateRows(args.rows);
		refundDetailGrid.render();
	});
	
	// wire up the search textbox to apply the filter to the model
		$("#refundDetail_GridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			refundDetailSearchString = this.value;
			updateRefundDetailGridFilter();
		});
		
	function updateRefundDetailGridFilter() {
		refundDetailDataView.setFilterArgs({
			refundDetailSearchString : refundDetailSearchString
		});
		refundDetailDataView.refresh();
	}
	var eventrowIndex;
	refundDetailGrid.onSelectedRowsChanged.subscribe(function() {
		var temprEventRowIndex = refundDetailGrid.getSelectedRows([ 0 ])[0];
			if (temprEventRowIndex != eventrowIndex) {
				eventrowIndex = temprEventRowIndex;
				//getCustomerInfoForInvoice(temprEventRowIndex);
			}
			
			selectedRow = refundDetailGrid.getSelectedRows([0])[0];
			if (selectedRow >=0) {
				id = refundDetailGrid.getDataItem(temprEventRowIndex).id;
			}else{
				id='';
			}			
		});

	// initialize the model after all the events have been hooked up
	refundDetailDataView.beginUpdate();
	refundDetailDataView.setItems(refundDetailData);
	
	refundDetailDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			refundDetailSearchString : refundDetailSearchString
		});
	refundDetailDataView.setFilter(refundDetailFilter);
		
	refundDetailDataView.endUpdate();
	refundDetailDataView.syncGridSelection(refundDetailGrid, true);
	$("#gridContainer").resizable();
	//refundDetailGrid.resizeCanvas();
	//$("div#divLoading").removeClass('show');
	if(refundDetailOrderId>0){
		for(var i=0;i<refundDetailGrid.getDataLength();i++){
			if(refundDetailGrid.getDataItem(i).id==refundDetailOrderId){
				refundDetailGrid.setSelectedRows([i]);
				break;
			}
		}
	}	
}