
var shippingInfo=[];
var invoiceInfo=[];
var cardInfo=[];
var poInfo=[];
var customerId;//='${customerId}';

function cancelAction(){
	$('#myModal-2').modal('hide');	
}

function invoiceResetFilters(){
	custInvoiceGridSearchString='';
	custInvoiceGridColumnFilters = {};
	getCustomerInfoForInvoice();
}

function poResetFilters(){
	custPOGridSearchString='';
	custPOGridColumnFilters = {};
	getCustomerInfoForPO();
}

function loyalFanResetFilters(){
	custArtistGridSearchString='';
	custArtistGridColumnFilters = {};
	getArtistGridData(0);
}

function eventResetFilters(){
	custEventGridSearchString='';
	custEventGridColumnFilters = {};
	getCustEventsGridData(0);
}

//function for deleting shipping address functionality
function custShipGridDeleteFormatter(row,cell,value,columnDef,dataContext){  
   /*  var button = "<input class='del' value='Delete' type='button' id='"+ dataContext.id +"' />"; */
    var button = "<img class='custShipGridDeleteClickableImage' src='../resources/images/ico-delete.gif' id='"+ dataContext.shippingId +"'/>";
    //the id is so that you can identify the row when the particular button is clicked
    return button;
    //Now the row will display your button
}

//function for edit functionality
function custShipGridEditFormatter(row,cell,value,columnDef,dataContext){
	//the id is so that you can identify the row when the particular button is clicked
	var button = "<img class='custShipGridEditClickableImage' src='../resources/js/slick/images/pencil.gif' id='"+ dataContext.shippingId +"'/>";
	return button;
}

//Function to hook up the edit button event for Shipping Address
$('.custShipGridEditClickableImage').live('click', function(){
	var me = $(this), id = me.attr('id');
	editCustomerShippingAddress(id);
});

//Now you can use jquery to hook up your delete button event for Shipping Address
$('.custShipGridDeleteClickableImage').live('click', function(){
    var me = $(this), id = me.attr('id');
    var delFlag = deleteShippingAddress(id);//confirm("Are you sure,Do you want to Delete it?");
});

function getCustomerInfoForEdit(customerId){
	$('#custId').val(customerId);
	$.ajax({
		url : "/Client/GetCustomerInfo.json",
		type : "post",
		data:"custId="+customerId,
		success : function(response){
			var data = JSON.parse(JSON.stringify(response));
			$('#view-customer-detail').modal('show');
			showCustomerInfo(data);
		},
		error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function showCustomerInfo(data){
	$('#editCustomerDiv').show();
	$(".vcustnav li").removeClass("active");
	$(".vcustdiv div").removeClass("active");
	//$(window).scrollTop($('#topLevelInfo').offset().top);
	$('#defaultTabViewCust').addClass('active');
	$('#topLevelInfo').addClass('active');
	
	
	//fill basic info
	fillBasicInfo(data.customerInfo);
	if(data.countries != null && data.countries != '' && data.countries != undefined){
		updateStateCountryComboById(data.countries,vCust_blCountryName,data.billingInfo.countryId);
	}
	if(data.states!=null && data.states != '' && data.states!=undefined){
		updateStateCountryComboById(data.states,vCust_blStateName,data.billingInfo.stateId);
	}
	
	//billing info
	fillBillingInfo(data.billingInfo);

	if(data.shippingInfo !=null && data.shippingInfo != '' && data.shippingInfo!=undefined){
		shippingInfo = data.shippingInfo;
	}else{
		shippingInfo =[];
	}
	
	//shipping info
	if(data.shippingPagingInfo!=null && data.shippingPagingInfo!=undefined){
		custShippingPagingInfo = data.shippingPagingInfo;
	}else{
		custShippingPagingInfo = {};
	}
	updateStateCountryComboById(data.countries,vCust_shCountryName,'');
	fillShippingInfo(data.shippingInfo,data.shippingCount);
	
	
	if(data.cardInfo !=null && data.cardInfo != '' && data.cardInfo!=undefined){
		cardInfo = data.cardInfo;
	}else{
		cardInfo =[];
	}
	if(data.cardPagingInfo !=null && data.cardPagingInfo != '' && data.cardPagingInfo!=undefined){
		custCreditCardPagingInfo = data.cardPagingInfo;
	}else{
		custCreditCardPagingInfo =[];
	}
	
	fillCreditInfo(data.creditInfo);
	
	//reward points info
	fillRewardPointsInfo(data.rewardPointsInfo);
	
	fillLoyalFanInfo(data.loyalFanInfo);
}

function fillBasicInfo(customerInfo){
	$('#productType').val(customerInfo.productType);
	$('#custId').val(customerInfo.id);
	$('#vCust_customerType').val(customerInfo.type);
	$('#vCust_customerLevel').val(customerInfo.level);
	$('#vCust_clientBrokerType').val(customerInfo.clientBrokerType);
	$('#vCust_signupType').val(customerInfo.signupType);
	$('#vCust_firstName').val(customerInfo.name);
	$('#vCust_lastName').val(customerInfo.lastName);
	$('#vCust_companyName').val(customerInfo.companyName);
	$('#vCust_email').val(customerInfo.email);
	$('#vCust_phone').val(customerInfo.phone);
	$('#vCust_extension').val(customerInfo.ext);
	$('#vCust_otherPhone').val(customerInfo.otherPhone);
	$('#vCust_representativeName').val(customerInfo.representative);
	$('#vCust_noteCustId').val(customerInfo.id);
	$('#vCust_notesTxt').val(customerInfo.notes);
	$('#customerEmailHdr_Shipping').text(customerInfo.email);
	$('#customerEmailHdr_RewardPoint').text(customerInfo.email);
	$('#customerEmailHdr_InvoiceTicket').text(customerInfo.email);
}

function fillBillingInfo(billingInfo){
	$('#vCust_blCustId').val($('#custId').val());
	$('#vCust_billingAddressId').val(billingInfo.id);
	$('#vCust_blFirstName').val(billingInfo.firstName);
	$('#vCust_blLastName').val(billingInfo.lastName);
	$('#vCust_blEmail').val(billingInfo.email);
	$('#vCust_blPhone').val(billingInfo.phone);
	$('#vCust_blStreet1').val(billingInfo.street1);
	$('#vCust_blStreet2').val(billingInfo.street2);
	//$('#vCust_blCountryName').val(billingInfo.country);
	$("#vCust_blCountryName option:contains(" + billingInfo.country + ")").attr('selected', 'selected');
	//$('#vCust_blStateName').val(billingInfo.state);
	$("#vCust_blStateName option:contains(" + billingInfo.state + ")").attr('selected', 'selected');
	$('#vCust_blCity').val(billingInfo.city);
	$('#vCust_blZipCode').val(billingInfo.zip);
}	

function fillShippingInfo(shippingInfo,shippingCount){
		//$('#shippingTable').empty();
		$('#count').val(0);
		/*var template='<tbody>'+
			'<tr>'+
				'<th><i class=""></i> First Name</th>'+
				'<th><i class=""></i> Last Name</th>'+
				'<th><i class=""></i> Street1</th>'+
				'<th><i class=""></i> Street2</th>'+
				'<th><i class=""></i> Country</th>'+
				'<th><i class=""></i> State</th>'+
				'<th><i class=""></i> City</th>'+
				'<th><i class=""></i> Zip Code</th>'+
				'<th><i class=""></i> Address Type</th>'+
				'<th><i class="icon_cogs"></i> Action</th>'+
			'</tr>'+
							
		'</tbody>';*/
		if(shippingInfo!=null && shippingInfo!='' && shippingInfo.length>0){
		if(shippingCount!=null && shippingCount!=''){
			$('#count').val(shippingCount);
		}
		}
		/*
		for(var i=0;i<shippingInfo.length;i++){
			template += '<tr id="'+shippingInfo[i].id+'">'+
			'<input type="hidden" name="shippingAddressId" value="'+shippingInfo[i].id+'"/>'+
			'<td id="firstName_'+shippingInfo[i].id+'">'+shippingInfo[i].firstName+'</td>'+
			'<td id="lastName_'+shippingInfo[i].id+'">'+shippingInfo[i].lastName+'</td>'+
			'<td id="street1_'+shippingInfo[i].id+'">'+shippingInfo[i].street1+'</td>'+
			'<td id="street2_'+shippingInfo[i].id+'">'+shippingInfo[i].street2+'</td>'+
			'<td id="country_'+shippingInfo[i].id+'">'+shippingInfo[i].country+'</td>'+
			'<td id="state_'+shippingInfo[i].id+'">'+shippingInfo[i].state+'</td>'+
			'<td id="city_'+shippingInfo[i].id+'">'+shippingInfo[i].city+'</td>'+
			'<td id="zipCode_'+shippingInfo[i].id+'">'+shippingInfo[i].zip+'</td>'+
			'<td id="addressType_'+shippingInfo[i].id+'">'+shippingInfo[i].addressType+'</td>'+
			'<td>'+
				'<div class="btn-group">'+
					'<a class="btn btn-success" id="edit" onclick="editCustomerShippingAddress('+shippingInfo[i].id+');"><i class="icon_check_alt2"></i> </a> '+ 
					'<a class="btn btn-danger" id="delete" onclick="deleteShippingAddress('+shippingInfo[i].id+');"><i class="icon_close_alt2"></i> </a>'+
				'</div>'+
			'</td>'+
			'</tr>';
		}
	$('#shippingTable').append(template);*/
}
function fillCreditInfo(customerCredit){
	$('#vCust_activeCredit').html(customerCredit.activeCredit);
	$('#vCust_totalEarnedCredit').html(customerCredit.totalEarnedCredit);
	$('#vCust_totalUsedCredit').html(customerCredit.totalUsedCredit);
}

function fillRewardPointsInfo(rewardPointsInfo){
	$('#vCust_totEarnPoints').html(rewardPointsInfo.totalEarnedPoints);
	$('#vCust_totSpentPoints').html(rewardPointsInfo.totalSpentPoints);
	//$('#latEarnPoints').html(rewardPointsInfo.latestEarnedPoints);
	//$('#latSpentPoints').html(rewardPointsInfo.latestSpentPoints);
	$('#vCust_rewardPointBalance').html((parseFloat(rewardPointsInfo.activePoints) + parseFloat(rewardPointsInfo.pendingPoints)).toFixed(2));
	$('#vCust_actPoints').html(rewardPointsInfo.activePoints);
	$('#vCust_pendPoints').html(rewardPointsInfo.pendingPoints);
	$('#vCust_voidPoints').html(rewardPointsInfo.voidedPoints);
}

function fillLoyalFanInfo(loyalFanInfo){	
	if(loyalFanInfo != null && loyalFanInfo != ''){
		if(loyalFanInfo.artistName != null && loyalFanInfo.artistName != '' && loyalFanInfo.categoryName != null && loyalFanInfo.categoryName != ''){
			$('#vCust_loyalArtistName').html(loyalFanInfo.artistName);
			$('#vCust_loyalCategoryName').html(loyalFanInfo.categoryName);
			$('#vCust_loyalTicketsPurchasedSpan').html(loyalFanInfo.ticketsPurchased);
			$('#vCust_loyalTicketsPurchased').val(loyalFanInfo.ticketsPurchased);
			$('#vCust_loyalStartDate').html(loyalFanInfo.startDate);
			$('#vCust_loyalEndDate').html(loyalFanInfo.endDate);
			$('#vCust_loyalFanDiv').show();
			$('#vCust_loyalArtistDiv').show();
			$('#vCust_loyalCityDiv').hide();
		}else if(loyalFanInfo.cityName != null && loyalFanInfo.cityName != '' && loyalFanInfo.categoryName!= null && loyalFanInfo.categoryName != ''){
			$('#vCust_loyalCityName').html(loyalFanInfo.cityName);
			$('#vCust_loyalCategoryName').html(loyalFanInfo.categoryName);			
			$('#vCust_loyalTicketsPurchasedSpan').html(loyalFanInfo.ticketsPurchased);
			$('#vCust_loyalTicketsPurchased').val(loyalFanInfo.ticketsPurchased);
			$('#vCust_loyalStartDate').html(loyalFanInfo.startDate);
			$('#vCust_loyalEndDate').html(loyalFanInfo.endDate);
			$('#vCust_loyalFanDiv').show();
			$('#vCust_loyalArtistDiv').hide();
			$('#vCust_loyalCityDiv').show();
		}else{
			$('#vCust_loyalFanDiv').hide();
			$('#vCust_loyalTicketsPurchased').val('');
		}
		$('#loyalFanArtists').val('');
		$('#loyalFanSelectionValue').val('');
		$('#loyalFanCityName').val('');
		$('#loyalFanStateName').val('');
		$('#loyalFanCountryName').val('');
		$('#loyalFanZipCode').val('');
		$('#taglabel').text('');
		$('#taglabel1').text('');
		$('#artistDiv').hide();
		$('#cityStateDiv').hide();
		$('#parentType').val('');
	}
}

/* function deleteCustomer(userId){
	if(userId == ''){
		jAlert("Userid is not found please refresh page and try again.");
		return false;
	}
	jConfirm("Are you sure to delete an customer ?","Confrim",function(r){
		if(r){
			$.ajax({
				url : "${pageContext.request.contextPath}/DeleteCustomer",
				type : "post",
				data : "userName="+ $("#userName").val() + "&userId=" + userId,
				success : function(response){
					if(response != "" && response!=null){
						jAlert("Customer deleted successfully.");
						refreshCustomerGridValues(null,false,true,response);
						return true;
					}else{
						jAlert(response);
						return false;
					}
				},
				error : function(error){
					jAlert("There is something wrong. Please try again"+error,"Error");
					return false;
				}
			});
		} else {
			return false;
		}
	});
	return false;
} */

//show the pop window center
function popupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
  
    // Puts focus on the newWindow  
    if (window.focus) {  
        newWindow.focus();  
    }  
}  

/*-----------*/

	//Customer Shipping Grid
	var custShippingPagingInfo;
	var custShippingGrid;
	var custShippingDataView;
	var custShippingData=[];
	var custShippingColumn = [
		  /*  {id:"id", name:"ID", field: "id", sortable: true}, */
		   {id:"shippingId", name:"Shipping ID", field: "shippingId", sortable: true},
		   {id:"firstname", name:"First Name", field: "firstname", sortable: true},
		   {id:"lastname", name:"Last Name", field: "lastname", sortable: true},
		   {id:"email", name:"Email", field: "email", sortable: true},
		   {id:"phone", name:"Phone", field: "phone", sortable: true},
		   {id:"street1", name:"Street1", field: "street1", sortable: true},
		   {id:"street2", name:"Street2", field: "street2", sortable: true},
		   {id:"country", name:"Country", field: "country", sortable: true},
		   {id:"state", name:"State", field: "state", sortable: true},
		   {id:"city", name:"City", field: "city", sortable: true},
		   {id:"zipcode", name:"Zip Code", field: "zipcode", sortable: true},
		   {id:"address", name:"Address Type", field: "address", sortable: true},
		   {id: "editCol", field:"editCol", name:"Edit", width:20, formatter:custShipGridEditFormatter},
		   {id:"delCol", name:"Delete", field: "delCol", width:10, formatter:custShipGridDeleteFormatter}
		  ];
              
	var custShippingOptions = {		  
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		topPanelHeight : 25,
		//autoHeight: true
	};

	var custShippingSortcol = "shippingId";
	var custShippingSortdir = 1;
	var percentCompleteThreshold = 0;
	var custShippingGridSearchString = "";

	//filter option
	function custShippingGridFilter(item, args) {
		var x= item["shippingId"];
		if (args.custShippingGridSearchString  != ""
				&& x.indexOf(args.custShippingGridSearchString ) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.custShippingGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function custShippingGridComparer(a, b) {
		  var x = a[custShippingSortcol], y = b[custShippingSortcol];
		  if(!isNaN(x)){
			return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		  }
		  if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
		  if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
	function shippingToggleFilterRow() {
		custShippingGrid.setTopPanelVisibility(!custShippingGrid.getOptions().showTopPanel);
	}
		
	//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$("#shipping_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
	});
	 
	function custShippingGridValues(jsonData) {
		var custShippingData=[];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];
			var d = (custShippingData[i] = {});
			d["id"] = data.id;
			d["shippingId"] = data.id;
			d["firstname"] = data.firstName;
			d["lastname"] = data.lastName;
			d["email"] = data.email;
			d["phone"] = data.phone;
			d["street1"] = data.street1;
			d["street2"] = data.street2;
			d["country"] = data.country;
			d["state"] = data.state;
			d["city"] = data.city;
			d["zipcode"] = data.zip;
			d["address"] = data.addressType;
		}
		
		custShippingDataView = new Slick.Data.DataView({inlineFilters: true });
		custShippingGrid = new Slick.Grid("#custShipping_grid", custShippingDataView, custShippingColumn, custShippingOptions);
		custShippingGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custShippingGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(custShippingPagingInfo!=null){
			var shippingGridPager = new Slick.Controls.Pager(custShippingDataView, custShippingGrid, $("#custShipping_pager"),custShippingPagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(custShippingColumn, custShippingGrid, custShippingOptions);
		  
		// move the filter panel defined in a hidden div into custShippingGrid top panel
		$("#inlineFilterPanel").appendTo(custShippingGrid.getTopPanel()).show();

		custShippingGrid.onKeyDown.subscribe(function (e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
			  return false;
			}
			var rows = [];
			for (var i = 0; i < custShippingDataView.getLength(); i++) {
			  rows.push(i);
			}
			custShippingGrid.setSelectedRows(rows);
			e.preventDefault();
		});
		custShippingGrid.onSort.subscribe(function (e, args) {
			custShippingSortdir = args.sortAsc ? 1 : -1;
			custShippingSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
			  // using temporary Object.prototype.toString override
			  // more limited and does lexicographic sort only by default, but can be much faster
			  // use numeric sort of % and lexicographic for everything else
			  custShippingDataView.fastSort(custShippingSortcol, args.sortAsc);
			} else {
			  // using native sort with custShippingGridComparer
			  // preferred method but can be very slow in IE with huge datasets
			  custShippingDataView.sort(custShippingGridComparer, args.sortAsc);
			}
		});
		  
		// wire up model events to drive the custShippingGrid
		custShippingDataView.onRowCountChanged.subscribe(function (e, args) {
			custShippingGrid.updateRowCount();
			custShippingGrid.render();
		});
		custShippingDataView.onRowsChanged.subscribe(function (e, args) {
			custShippingGrid.invalidateRows(args.rows);
			custShippingGrid.render();
		});

		// wire up the search textbox to apply the filter to the model
		$("#txtSearch,#txtSearch2").keyup(function (e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
			  this.value = "";
			}
			custShippingGridSearchString = this.value;
			updateFilter();
		});
		function updateFilter() {
			custShippingDataView.setFilterArgs({
			custShippingGridSearchString: custShippingGridSearchString
		});
		custShippingDataView.refresh();
		}

		custShippingDataView.beginUpdate();
		custShippingDataView.setItems(custShippingData);
		custShippingDataView.setFilterArgs({
			percentCompleteThreshold: percentCompleteThreshold,
			custShippingGridSearchString: custShippingGridSearchString
		});
		custShippingDataView.setFilter(custShippingGridFilter);
		custShippingDataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		custShippingDataView.syncGridSelection(custShippingGrid, true);
		custShippingDataView.refresh();
		$("#gridContainer").resizable();
	}
	
	//Customer Credit Card Grid
	var custCreditCardPagingInfo;
	var custCreditCardGrid;
	var custCreditCardDataView;
	var custCreditCardData=[];
	var custCreditCardColumn = [
		  /*  {id:"id", name:"ID", field: "id", sortable: true}, */
		   {id:"stripeCustomerId", name:"Stripe Customer Id", field: "stripeCustomerId", sortable: true},
		   {id:"stripeCardId", name:"Stripe Card Id", field: "stripeCardId", sortable: true},
		   {id:"cardType", name:"CardType", field: "cardType", sortable: true},
		   {id:"cardLastFourDigit", name:"Card Last Four Digit", field: "cardLastFourDigit", sortable: true},
		   {id:"expiryMonth", name:"Expiry Month", field: "expiryMonth", sortable: true},
		   {id:"expiryYear", name:"Expiry Year", field: "expiryYear", sortable: true},
		   {id:"status", name:"Status", field: "status", sortable: true}
		  ];
              
	var custCreditCardOptions = {		  
		//editable: true,
		//enableAddRow: true,
		enableCellNavigation : true,
		//asyncEditorLoading: true,
		forceFitColumns : true,
		topPanelHeight : 25,
		//autoHeight: true
	};

	var custCreditCardSortcol = "id";
	var custCreditCardSortdir = 1;
	var percentCompleteThreshold = 0;
	var custCreditCardGridSearchString = "";

	//filter option
	function custCreditCardFilter(item, args) {
		var x= item["id"];
		if (args.custCreditCardGridSearchString  != ""
				&& x.indexOf(args.custCreditCardGridSearchString ) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.custCreditCardGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	function custCreditCardComparer(a, b) {
		var x = a[custCreditCardSortcol], y = b[custCreditCardSortcol];
		if(!isNaN(x)){
		return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}

	function creditCardToggleFilterRow() {
		custCreditCardGrid.setTopPanelVisibility(!custCreditCardGrid.getOptions().showTopPanel);
	}
		
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover")
	});
	 
	function createCreditCardGrid(jsonData) {
		custCreditCardData=[];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i];
			var d = (custCreditCardData[i] = {});
			d["id"] = data.id;
			d["stripeCustomerId"] = data.stripeCustomerId;
			d["stripeCardId"] = data.stripeCardId;
			d["cardType"] = data.cardType;
			d["cardLastFourDigit"] = data.cardLastFourDigit;
			d["expiryMonth"] = data.expiryMonth;
			d["expiryYear"] = data.expiryYear;
			d["status"] = data.status;
		}
		
		custCreditCardDataView = new Slick.Data.DataView({inlineFilters: true });
		custCreditCardGrid = new Slick.Grid("#custCreditCardGrid", custCreditCardDataView, custCreditCardColumn, custCreditCardOptions);
		custCreditCardGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custCreditCardGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(custCreditCardPagingInfo!=null){
			var creditCardGridPager = new Slick.Controls.Pager(custCreditCardDataView, custCreditCardGrid, $("#creditCard_pager"),custCreditCardPagingInfo);
		}
		var columnpicker = new Slick.Controls.ColumnPicker(custCreditCardColumn, custCreditCardGrid, custCreditCardOptions);
		  
		// move the filter panel defined in a hidden div into custCreditCardGrid top panel
		$("#inlineFilterPanel").appendTo(custCreditCardGrid.getTopPanel()).show();

		custCreditCardGrid.onKeyDown.subscribe(function (e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
			  return false;
			}
			var rows = [];
			for (var i = 0; i < custCreditCardDataView.getLength(); i++) {
			  rows.push(i);
			}
			custCreditCardGrid.setSelectedRows(rows);
			e.preventDefault();
		});
		
		custCreditCardGrid.onSort.subscribe(function (e, args) {
			custCreditCardSortdir = args.sortAsc ? 1 : -1;
			custCreditCardSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
			  // using temporary Object.prototype.toString override
			  // more limited and does lexicographic sort only by default, but can be much faster
			  // use numeric sort of % and lexicographic for everything else
			  custCreditCardDataView.fastSort(custCreditCardSortcol, args.sortAsc);
			} else {
			  // using native sort with custCreditCardComparer
			  // preferred method but can be very slow in IE with huge datasets
			  custCreditCardDataView.sort(custCreditCardComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the custCreditCardGrid
		custCreditCardDataView.onRowCountChanged.subscribe(function (e, args) {
			custCreditCardGrid.updateRowCount();
			custCreditCardGrid.render();
		});
		custCreditCardDataView.onRowsChanged.subscribe(function (e, args) {
			custCreditCardGrid.invalidateRows(args.rows);
			custCreditCardGrid.render();
		});
		  
		// wire up the search textbox to apply the filter to the model
		$("#txtSearch,#txtSearch2").keyup(function (e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
			  this.value = "";
			}
			custCreditCardGridSearchString = this.value;
			updateFilter();
		});
		
		function updateFilter() {
			custCreditCardDataView.setFilterArgs({
			  custCreditCardGridSearchString: custCreditCardGridSearchString
			});
			custCreditCardDataView.refresh();
		}
		  
		custCreditCardDataView.beginUpdate();
		custCreditCardDataView.setItems(custCreditCardData);
		custCreditCardDataView.setFilterArgs({
			percentCompleteThreshold: percentCompleteThreshold,
			custCreditCardGridSearchString: custCreditCardGridSearchString
		});
		
		custCreditCardDataView.setFilter(custCreditCardFilter);
		custCreditCardDataView.endUpdate();
		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		custCreditCardDataView.syncGridSelection(custCreditCardGrid, true);
		custCreditCardDataView.refresh();
		$("#gridContainer").resizable();
	}
	
	//Customer Invoice Grid
	function getCustomerInfoForInvoice(){
		var customrId = $('#custId').val();//'${customerId}';
		$.ajax({
			url : "/Client/GetCustomerInfoForInvoice.json",
			type : "post",
			data:"custId="+customrId+"&headerFilter="+custInvoiceGridSearchString,
			success : function(response){
				var data = JSON.parse(JSON.stringify(response));
				if(data.invoiceInfo !=null && data.invoiceInfo != '' && data.invoiceInfo!=undefined){
					invoiceInfo = data.invoiceInfo;	
				}else{
					invoiceInfo =[];
					//jAlert(data.msg);
				}
				if(data.msg != null && data.msg != "" && data.msg != undefined){
					jAlert(data.msg);
				}
				custInvoicePagingInfo = data.invoicePagingInfo;
				custInvoiceGridValues(invoiceInfo);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var custInvoiceGridSearchString='';
	var custInvoiceGridColumnFilters = {};
	var custInvoicePagingInfo;
	var custInvoiceGrid;
	var custInvoiceDataView;
	var custInvoiceData=[];
	var custInvoiceColumn = [
	/* {id:"id", name:"ID", field: "id", sortable: true}, */
	   {id:"invoiceId", name:"Invoice Id", field: "invoiceId", sortable: true},
	   {id:"invoiceTotal", name:"Invoice Total", field: "invoiceTotal", sortable: true},
	   {id:"createdDate", name:"Created Date", field: "createdDate", sortable: true},
	   {id:"createdBy", name:"Created By", field: "createdBy", sortable: true},
	   {id:"ticketCount", name:"Ticket Count", field: "ticketCount", sortable: true},
	   {id:"productType", name:"Product Type", field: "productType", sortable: true}, 
	   {id:"invoiceType", name:"Invoice Type", field: "invoiceType", sortable: true},
	/* {id:"extNo", name:"Ext PO", field: "extNo", sortable: true} */
	   {id:"orderType", name:"Order Type", field: "orderType", sortable: true},
	   {id:"status", name:"Status", field: "status", sortable: true},
	   {id:"viewTickets", name:"View Tickets", field: "viewTickets", formatter : viewLinkFormatter}
	  ];
				  
	var custInvoiceOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};

	var custInvoiceSortcol = "invoiceId";
	var custInvoiceSortdir = 1;
	var percentCompleteThreshold = 0;

	/*
	function custInvoiceFilter(item, args) {
		var x= item["invoiceId"];
		if (args.custInvoiceGridSearchString  != ""
				&& x.indexOf(args.custInvoiceGridSearchString ) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.custInvoiceGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/

	function custInvoiceComparer(a, b) {
	  var x = a[custInvoiceSortcol], y = b[custInvoiceSortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	  	if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*function custInvoiceToggleFilterRow() {
		custInvoiceGrid.setTopPanelVisibility(!custInvoiceGrid.getOptions().showTopPanel);
	}
		
	$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
		$(e.target).addClass("ui-state-hover");
		}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover");
	});*/
	 
	function custInvoiceGridValues(jsonData) {
		custInvoiceData=[];
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (custInvoiceData[i] = {});
			d["id"] = i;
			d["invoiceId"] = data.invoiceId;
			d["invoiceTotal"] = data.invoiceTotal;
			d["createdDate"] = data.createdDate;
			d["createdBy"] = data.createdBy;
			d["ticketCount"] = data.ticketCount;
			d["productType"] = data.productType;
			d["orderType"] = data.orderType;
			d["status"] = data.status;
			d["invoiceType"] = data.invoiceType;
		}
	
		custInvoiceDataView = new Slick.Data.DataView();
		custInvoiceGrid = new Slick.Grid("#custInvoice_grid", custInvoiceDataView, custInvoiceColumn, custInvoiceOptions);
		custInvoiceGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custInvoiceGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(custInvoicePagingInfo!=null){
			var custInvoiceGridPager = new Slick.Controls.Pager(custInvoiceDataView, custInvoiceGrid, $("#custInvoice_pager"),custInvoicePagingInfo);
		}
		var custInvoiceColumnPicker = new Slick.Controls.ColumnPicker(custInvoiceColumn, custInvoiceGrid, custInvoiceOptions);
		  
		// move the filter panel defined in a hidden div into custInvoiceGrid top panel
		//$("#inlineFilterPanel").appendTo(custInvoiceGrid.getTopPanel()).show();

		custInvoiceGrid.onKeyDown.subscribe(function (e) {
		// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for (var i = 0; i < custInvoiceDataView.getLength(); i++) {
			  rows.push(i);
			}
			custInvoiceGrid.setSelectedRows(rows);
			e.preventDefault();
		});
		
		custInvoiceGrid.onSort.subscribe(function (e, args) {
		  custInvoiceSortdir = args.sortAsc ? 1 : -1;
		  custInvoiceSortcol = args.sortCol.field;
		  if ($.browser.msie && $.browser.version <= 8) {
			custInvoiceDataView.fastSort(custInvoiceSortcol, args.sortAsc);
		  } else {
			custInvoiceDataView.sort(custInvoiceComparer, args.sortAsc);
		  }
		});

		// wire up model events to drive the custInvoiceGrid
		custInvoiceDataView.onRowCountChanged.subscribe(function (e, args) {
			custInvoiceGrid.updateRowCount();
			custInvoiceGrid.render();
		});
		custInvoiceDataView.onRowsChanged.subscribe(function (e, args) {
			custInvoiceGrid.invalidateRows(args.rows);
			custInvoiceGrid.render();
		});

		$(custInvoiceGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			custInvoiceGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  custInvoiceGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in custInvoiceGridColumnFilters) {
					  if (columnId !== undefined && custInvoiceGridColumnFilters[columnId] !== "") {
						 custInvoiceGridSearchString += columnId + ":" +custInvoiceGridColumnFilters[columnId]+",";
					  }
					}
					getCustomerInfoForInvoice();
				}
			  }		 
		});
		custInvoiceGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){			
				if(args.column.id == 'createdDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(custInvoiceGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				} 
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(custInvoiceGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}			
			}
		});
		custInvoiceGrid.init();

		/*$("#txtSearch,#txtSearch2").keyup(function (e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
			  this.value = "";
			}
			custInvoiceGridSearchString = this.value;
			updatecustInvoiceFilter();
		});
		
		function updatecustInvoiceFilter() {
			custInvoiceDataView.setFilterArgs({
			  custInvoiceGridSearchString: custInvoiceGridSearchString
			});
			custInvoiceDataView.refresh();
		}*/

		custInvoiceDataView.beginUpdate();
		custInvoiceDataView.setItems(custInvoiceData);

		/*custInvoiceDataView.setFilterArgs({
			percentCompleteThreshold: percentCompleteThreshold,
			custInvoiceGridSearchString: custInvoiceGridSearchString
		});	  
		custInvoiceDataView.setFilter(custInvoiceFilter);*/

		custInvoiceDataView.endUpdate();

		// if you don't want the items that are not visible (due to being filtered out
		// or being on a different page) to stay selected, pass 'false' to the second arg
		custInvoiceDataView.syncGridSelection(custInvoiceGrid, true);
		custInvoiceDataView.refresh();
		$("#gridContainer").resizable();
	}
	
	//Customer PO Grid	
	function getCustomerInfoForPO(){
		var customrId = $('#custId').val();//'${customerId}';	
		$.ajax({
			url : "/Client/GetCustomerInfoForPO.json",
			type : "post",
			data:"custId="+customrId+"&headerFilter="+custPOGridSearchString,
			success : function(response){
				var data = JSON.parse(JSON.stringify(response));
				if(data.poInfo !=null && data.poInfo != '' && data.poInfo!=undefined){
					poInfo = data.poInfo;
				}else{
					poInfo=[];
					//jAlert(data.msg);
				}
				if(data.msg != null && data.msg != "" && data.msg != undefined){
					jAlert(data.msg);
				}
				custPOPagingInfo = data.poPagingInfo;
				custPOGridValues(poInfo);
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}

	var custPOPagingInfo;
	var custPOGridSearchString = "";
	var custPOGridColumnFilters = {};
	var custPODataView;
	var custPOGrid;
	var custPOData = [];
	var custPOColumns = [ 
	{
		id : "id",
		name : "PO ID",
		field : "id",
		sortable : true
	}, {
		id : "customerName",
		name : "Customer",
		field : "customerName"
	}, {
		id : "customerType",
		name : "Customer Type",
		field : "customerType",
		sortable : true
	}, {
		id : "poTotal",
		name : "Grand Total",
		field : "poTotal",
		sortable : true
	},{
		id : "ticketQty",
		name : "Ticket Qty",
		field : "ticketQty",
		sortable : true
	},{
		id : "created",
		name : "Created",
		field : "created",
		sortable : true
	}, {
		id : "shippingType",
		name : "Shipping Type",
		field : "shippingType",
		sortable : true
	}, {
		id : "productType",
		name : "Product Type",
		field : "productType",
		sortable : true
	}, {
		id: "status",
		name: "Status", 
		field: "status", 
		sortable: true
	} ];

	var custPOOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var custPOGridSortcol = "customerName";
	var custPOGridSortdir = 1;
	var percentCompleteThreshold = 0;
	/*
	function deleteRecordFrompoGrid(id) {
		custPODataView.deleteItem(id);
		custPOGrid.invalidate();
	}
	
	function custPOGridFilter(item, args) {
		var x= item["customerName"];
		if (args.custPOGridSearchString != ""
				&& x.indexOf(args.custPOGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.custPOGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	*/
	function custPOGridComparer(a, b) {
		var x = a[custPOGridSortcol], y = b[custPOGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	/*
	function custPOToggleFilterRow() {
		custPOGrid.setTopPanelVisibility(!custPOGrid.getOptions().showTopPanel);
	}
	
	$("#custPO_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
			.mouseover(function(e) {
				$(e.target).addClass("ui-state-hover")
			}).mouseout(function(e) {
				$(e.target).removeClass("ui-state-hover")
			});
	*/
	
	function custPOGridValues(jsonData) {
		var i = 0;
		custPOData=[];
		for(var i=0;i<jsonData.length;i++){
			var  data= jsonData[i];
			var d = (custPOData[i] = {});
			d["id"] = data.poId;	  		
			d["customerName"] = data.customerName;
			d["customerType"] = data.customerType;
			d["poTotal"] = data.total;
			d["ticketQty"] = data.ticketQty;
			d["created"] = data.created;
			d["shippingType"] = data.shippingType;
			d["productType"] = data.productType;
			d["status"] = data.status;
		}

		custPODataView = new Slick.Data.DataView();
		custPOGrid = new Slick.Grid("#custPO_grid", custPODataView, custPOColumns, custPOOptions);
		custPOGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custPOGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(custPOPagingInfo!=null){
			var custPOGridPager = new Slick.Controls.Pager(custPODataView, custPOGrid, $("#custPO_pager"),custPOPagingInfo);
		}
		var custPOGridColumnpicker = new Slick.Controls.ColumnPicker(custPOColumns, custPOGrid,
				custPOOptions);
		var cols = custPOGrid.getColumns();
	
		var colPO = [];
		for(var c=0;c<cols.length;c++) {
		  //if(cols[c].name!='Title' && cols[c].name!='Start') {
		 	 colPO.push(cols[c]);
		 // }  
		  custPOGrid.setColumns(colPO);
		}
		custPOGrid.invalidate();

		// move the filter panel defined in a hidden div into custPOGrid top panel
		//$("#custPO_inlineFilterPanel").appendTo(custPOGrid.getTopPanel()).show();

		custPOGrid.onSort.subscribe(function(e, args) {
			custPOGridSortdir = args.sortAsc ? 1 : -1;
			custPOGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				custPODataView.fastSort(custPOGridSortcol, args.sortAsc);
			} else {
				custPODataView.sort(custPOGridComparer, args.sortAsc);
			}
		});
		// wire up model events to drive the custPOGrid
		custPODataView.onRowCountChanged.subscribe(function(e, args) {
			custPOGrid.updateRowCount();
			custPOGrid.render();
		});
		custPODataView.onRowsChanged.subscribe(function(e, args) {
			custPOGrid.invalidateRows(args.rows);
			custPOGrid.render();
		});

		$(custPOGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
		 	custPOGridSearchString='';
			 var columnId = $(this).data("columnId");
			  if (columnId != null) {
				  custPOGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in custPOGridColumnFilters) {
					  if (columnId !== undefined && custPOGridColumnFilters[columnId] !== "") {
						 custPOGridSearchString += columnId + ":" +custPOGridColumnFilters[columnId]+",";
					  }
					}
					getCustomerInfoForPO();
				}
			  }		 
		});
		custPOGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'customerName' && args.column.id != 'customerType'){
					if(args.column.id == 'created'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(custPOGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(custPOGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}			
		});
		custPOGrid.init();
		/*
		// wire up the search textbox to apply the filter to the model
		$("#custPOGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			custPOGridSearchString = this.value;
			updatecustPOGridFilter();
		});
		function updatecustPOGridFilter() {
			custPODataView.setFilterArgs({
				custPOGridSearchString : custPOGridSearchString
			});
			custPODataView.refresh();
		}
		*/
		// initialize the model after all the events have been hooked up
		custPODataView.beginUpdate();
		custPODataView.setItems(custPOData);
		/*custPODataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			custPOGridSearchString : custPOGridSearchString
		});
		custPODataView.setFilter(custPOGridFilter);*/
		custPODataView.endUpdate();	
		custPODataView.syncGridSelection(custPOGrid, true);
		$("#gridContainer").resizable();		
	}
	
	//Customer Artist Grid
	function getArtistGridData(pageNo) {
		var cusId = $('#custId').val();//'${customerId}';
		$.ajax({
			url : "/Client/GetArtistsForCustomer.json",
			type : "post",
			dataType: "json",
			data : "customerId="+cusId+"&pageNo="+pageNo+"&headerFilter="+custArtistGridSearchString,
			success : function(res){
				var jsonData = res;
				/*if(jsonData==null || jsonData=='') {
					jAlert("No Artist found.");
				} 
				custArtistPagingInfo = jsonData.artistPagingInfo;
				custArtistGridValues(jsonData.artistList);
				fillLoyalFanInfo(jsonData.loyalFanInfo);
				clearAllSelections();
				$('#custArtist_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveUserArtistPreference()'>");*/
				
				if(jsonData != null && jsonData != ''){
					fillLoyalFanInfo(jsonData.loyalFanInfo);
				}else{
					$('#loyalFanDiv').hide();
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var custArtistGridCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});
	var custArtistPagingInfo;
	var custArtistDataView;
	var custArtistGrid;
	var custArtistData = [];
	var custArtistGridPager;
	var custArtistGridSearchString='';
	var custArtistGridColumnFilters = {};
	var userArtistPrefColumnsStr = '<%=session.getAttribute("artistsearchgrid")%>';
	var loadUserArtistColumns =[];
	var allCustArtistColumns = [ custArtistGridCheckboxSelector.getColumnDefinition(),
	{	
		id: "artist", 
		name: "Artist", 
		field: "artist",
		width:80,		
		sortable: true
	}, {
		id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	}, {
		id: "childCategory", 
		name: "Child Category", 
		field: "childCategory",
		width:80,			
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	}/*,{
		id : "visibleInSearch",
		field : "visibleInSearch",
		name : "Visible / In Visible",
		width:80,
		sortable: true
	}*/ ];

	if(userArtistPrefColumnsStr!='null' && userArtistPrefColumnsStr!=''){
		var columnOrder = userArtistPrefColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder.length;i++){
			columnWidth = columnOrder[i].split(":");
			for(var j=0;j<allCustArtistColumns.length;j++){
				if(columnWidth[0] == allCustArtistColumns[j].id){
					loadUserArtistColumns[i] =  allCustArtistColumns[j];
					loadUserArtistColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}			
		}
	}else{
		loadUserArtistColumns = allCustArtistColumns;
	}
	
	var custArtistOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var custArtistGridSortcol = "artistName";
	var custArtistGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function custArtistGridComparer(a, b) {
		var x = a[custArtistGridSortcol], y = b[custArtistGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
		
	function custArtistGridValues(jsonData) {
	 $("div#divLoading").addClass('show');
		custArtistData = [];
		if(jsonData!=null && jsonData !=null ){
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (custArtistData[i] = {});
				d["artistId"] = data.artistId;
				d["id"] =  i;
				d["grandChildCategory"] = data.grandChildCategory;
				d["childCategory"] = data.childCategory;
				d["parentCategory"] = data.parentCategory;
				d["artist"] = data.artistName;
				/*d["visibleInSearch"] = data.displayOnSearch;*/
			}
		}

		custArtistDataView = new Slick.Data.DataView();
		custArtistGrid = new Slick.Grid("#custArtist_grid", custArtistDataView, allCustArtistColumns, custArtistOptions);
		custArtistGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custArtistGrid.setSelectionModel(new Slick.RowSelectionModel());
		custArtistGrid.registerPlugin(custArtistGridCheckboxSelector);
		if(custArtistPagingInfo!=null){
			custArtistGridPager = new Slick.Controls.Pager(custArtistDataView, custArtistGrid, $("#custArtist_pager"),custArtistPagingInfo);
		}
		var custArtistGridColumnPicker = new Slick.Controls.ColumnPicker(allCustArtistColumns, custArtistGrid,
				custArtistOptions);
		
		custArtistGrid.onSort.subscribe(function(e, args) {
			custArtistGridSortdir = args.sortAsc ? 1 : -1;
			custArtistGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				custArtistDataView.fastSort(custArtistGridSortcol, args.sortAsc);
			} else {
				custArtistDataView.sort(custArtistGridComparer, args.sortAsc);
			}
		});

		// wire up model artists to drive the custArtistGrid
		custArtistDataView.onRowCountChanged.subscribe(function(e, args) {
			custArtistGrid.updateRowCount();
			custArtistGrid.render();
		});
		custArtistDataView.onRowsChanged.subscribe(function(e, args) {
			custArtistGrid.invalidateRows(args.rows);
			custArtistGrid.render();
		});

		$(custArtistGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			custArtistGridSearchString='';
			var columnId = $(this).data("columnId");
			if(columnId != null) {
				custArtistGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in custArtistGridColumnFilters) {
					  if (columnId !== undefined && custArtistGridColumnFilters[columnId] !== "") {
						custArtistGridSearchString += columnId + ":" +custArtistGridColumnFilters[columnId]+",";
					  }
					}
					getArtistGridData(0);
				}
			}		 
		});
		custArtistGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				$("<input type='text'>")
			   .data("columnId", args.column.id)
			   .val(custArtistGridColumnFilters[args.column.id])
			   .appendTo(args.node);
			}			
		});
		custArtistGrid.init();

		// initialize the model after all the artists have been hooked up
		custArtistDataView.beginUpdate();
		custArtistDataView.setItems(custArtistData);
		custArtistDataView.endUpdate();
		custArtistDataView.syncGridSelection(custArtistGrid, true);
		//custArtistGrid.resizeCanvas();
		 $("div#divLoading").removeClass('show');
	}
	
	
	//Customer Favourite Event Grid
	function getCustEventsGridData(pageNo) {
		var customrId = $('#custId').val();
		$.ajax({
			url : "/Client/GetFavouriteEventsForCustomer.json",
			type : "post",
			dataType: "json",
			data : "customerId="+customrId+"&pageNo="+pageNo+"&headerFilter="+custEventGridSearchString,
			success : function(res){
				var jsonData = res;
				if(jsonData.status == 1){}
				/*else{
					jAlert(jsonData.msg);
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					var msg = jsonData.msg.replace(/"/g, '');
					if(msg != null && msg != ""){
						jAlert(msg);
					}
				}
				custEventPagingInfo = jsonData.pagingInfo;
				custEventGridValues(jsonData.events);
				custEventGrid.setSelectedRows([]);
				$('#tagFavoriteEvent').text('');
				$('#favoriteEvent').val('');
				$('#favoriteEventId').val('');
				$('#custEvent_pager> div').append("<input type='button' name='Save Preference' value='Save Preference' style='float:right;' onclick='saveCustEventUserPreference()'>");
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	var custEventCheckboxSelector = new Slick.CheckboxSelectColumn({
	  cssClass: "slick-cell-checkboxsel"
	});	
	var custEventPagingInfo;
	var custEventDataView;
	var custEventGrid;
	var custEventData = [];
	var custEventGridSearchString='';
	var custEventGridColumnFilters = {};
	var userEventPrefColumnsStr = '<%=session.getAttribute("custeventsgrid")%>';
	var usrEventGridColumns =[];	
	var loadUserEventColumns = ["eventName", "eventDate", "eventTime", "dayOfTheWeek", "venue", "city", "state", "country"];
	var allCustEventColumns = [ /*custEventCheckboxSelector.getColumnDefinition(),
	{	id : "eventMarketId",
		name : "Event Market Id",
		field : "eventMarketId",
		width:80,
		sortable : true
	},*/{
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		width:80,
		sortable : true
	},{
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		width:80,
		sortable : true
	},{
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		width:80,
		sortable : true
	},{
		id : "dayOfTheWeek",
		name : "Day of Week",
		field : "dayOfTheWeek",
		width:80,
		sortable : true
	},{
		id : "venue",
		name : "Venue",
		field : "venue",
		width:80,
		sortable : true
	},/*{
		id : "venueId",
		name : "Venue Id",
		field : "venueId",
		width:80,
		sortable : true
	},{
		id : "noOfTix",
		name : "No of Tix",
		field : "noOfTix",
		width:80,
		sortable : true
	},{
		id : "noOfTixSold",
		name : "No of Tix Sold",
		field : "noOfTixSold",
		width:80,
		sortable : true
	},*/{
		id : "city",
		name : "City",
		field : "city",
		width:80,
		sortable : true
	},{
		id : "state",
		name : "State",
		field : "state",
		width:80,
		sortable : true
	},{
		id: "country", 
		name: "Country", 
		field: "country",
		width:80,	
		sortable: true
	},/*{
		id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		width:80,
		sortable: true
	},{
		id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		width:80,
		sortable: true
	},{
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		width:80,
		sortable: true
	},{
		id : "notes",
		field : "notes",
		name : "Notes",
		width:80,
		sortable: true,
		editor:Slick.Editors.LongText
	},*/{
		id : "eventCreated",
		field : "eventCreated",
		name : "Event Created",
		width:80,
		sortable: true
	},{
		id : "eventLastUpdated",
		field : "eventLastUpdated",
		name : "Event Last Updated",
		width:80,
		sortable: true
	} ];
	
	if(userEventPrefColumnsStr!='null' && userEventPrefColumnsStr!=''){
		var columnOrder1 = userEventPrefColumnsStr.split(',');
		var columnWidth = [];
		for(var i=0;i<columnOrder1.length;i++){
			columnWidth = columnOrder1[i].split(":");
			for(var j=0;j<allCustEventColumns.length;j++){
				if(columnWidth[0] == allCustEventColumns[j].id){
					usrEventGridColumns[i] = allCustEventColumns[j];
					usrEventGridColumns[i].width=(columnWidth[1]-5);
					break;
				}
			}
			
		}
	}else{
		var columnOrder1 = loadUserEventColumns;
		var columnWidth;
		for(var i=0;i<columnOrder1.length;i++){
			columnWidth = columnOrder1[i];
			for(var j=0;j<allCustEventColumns.length;j++){
				if(columnWidth == allCustEventColumns[j].id){
					usrEventGridColumns[i] = allCustEventColumns[j];
					usrEventGridColumns[i].width=80;
					break;
				}
			}			
		}
		//usrEventGridColumns = allCustEventColumns;
	}
	
	var custEventOptions = {
		editable: true,
		enableCellNavigation : true,
		asyncEditorLoading: false,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var custEventGridSortcol = "eventName";
	var custEventGridSortdir = 1;
	var percentCompleteThreshold = 0;
		
	function custEventGridComparer(a, b) {
		var x = a[custEventGridSortcol], y = b[custEventGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
		
	function custEventGridValues(jsonData) {
		custEventData=[];
		if(jsonData!=null && jsonData.length > 0){
			for (var i = 0; i < jsonData.length; i++) {
				var  data = jsonData[i];
				var d = (custEventData[i] = {});
				d["id"] = i;
				d["eventMarketId"] = data.eventId;
				d["eventName"] = data.eventName;
				d["eventDate"] = data.eventDateStr;
				d["eventTime"] = data.eventTimeStr;
				d["dayOfTheWeek"] = data.dayOfWeek;
				d["venue"] = data.building;
				d["venueId"] = data.venueId;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["eventCreated"] = data.eventCreationStr;
				d["eventLastUpdated"] = data.eventUpdatedStr;
				//d["noOfTix"] = data.noOfTixCount;
				//d["noOfTixSold"] = data.noOfTixSoldCount;
				//d["grandChildCategory"] = data.grandChildCategoryName;
				//d["childCategory"] = data.childCategoryName;
				//d["parentCategory"] = data.parentCategoryName;
				//d["notes"] = data.notes;
			}
		}

		custEventDataView = new Slick.Data.DataView();
		custEventGrid = new Slick.Grid("#custEvent_grid", custEventDataView, allCustEventColumns, custEventOptions);
		custEventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		custEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		custEventGrid.registerPlugin(custEventCheckboxSelector);
		if(custEventPagingInfo!=null){
			var custEventGridPager = new Slick.Controls.Pager(custEventDataView, custEventGrid, $("#custEvent_pager"),custEventPagingInfo);
		}
		var custEventGridColumnPicker = new Slick.Controls.ColumnPicker(allCustEventColumns, custEventGrid,
				custEventOptions);
		
		custEventGrid.onSort.subscribe(function(e, args) {
			custEventGridSortdir = args.sortAsc ? 1 : -1;
			custEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				custEventDataView.fastSort(custEventGridSortcol, args.sortAsc);
			} else {
				custEventDataView.sort(custEventGridComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the custEventGrid
		custEventDataView.onRowCountChanged.subscribe(function(e, args) {
			custEventGrid.updateRowCount();
			custEventGrid.render();
		});
		custEventDataView.onRowsChanged.subscribe(function(e, args) {
			custEventGrid.invalidateRows(args.rows);
			custEventGrid.render();
		});
		
		$(custEventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			custEventGridSearchString='';
			var columnId = $(this).data("columnId");
			if (columnId != null) {
				custEventGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in custEventGridColumnFilters) {
					  if (columnId !== undefined && custEventGridColumnFilters[columnId] !== "") {
						  custEventGridSearchString += columnId + ":" +custEventGridColumnFilters[columnId]+",";
					  }
					}
					getCustEventsGridData(0);
				}
			}		 
		});
		custEventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id != 'dayOfTheWeek'){
					if(args.column.id == 'eventTime'){
						$("<input type='text' placeholder='hh:mm a'>")
					   .data("columnId", args.column.id)
					   .val(custEventGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
					else if(args.column.id == 'eventDate' || args.column.id == 'eventCreated' || args.column.id == 'eventLastUpdated'){
						$("<input type='text' placeholder='mm/dd/yyyy'>")
					   .data("columnId", args.column.id)
					   .val(custEventGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}else{
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(custEventGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			}
		});
		custEventGrid.init();
				
		// initialize the model after all the events have been hooked up
		custEventDataView.beginUpdate();
		custEventDataView.setItems(custEventData);
		
		custEventDataView.endUpdate();
		custEventDataView.syncGridSelection(custEventGrid, true);
		$("#gridContainer").resizable();
		//custEventGrid.resizeCanvas();
		
		var custEventRowIndex;
		custEventGrid.onSelectedRowsChanged.subscribe(function() { 
			var cusTempEventRowIndex = custEventGrid.getSelectedRows([0])[0];
			if (cusTempEventRowIndex != custEventRowIndex) {
				custEventRowIndex = cusTempEventRowIndex;
				/* var eventId =custEventGrid.getDataItem(cusTempEventRowIndex).eventMarketId;
				 var eventdetailStr = custEventGrid.getDataItem(cusTempEventRowIndex).eventName+', '+custEventGrid.getDataItem(cusTempEventRowIndex).eventDate+', '+
				custEventGrid.getDataItem(cusTempEventRowIndex).eventTime+', '+custEventGrid.getDataItem(cusTempEventRowIndex).venue;
				 
				 $('#AddTicketGroupSuccessMsg').hide();
				 
				getCategoryTicketGroupsforEvent(eventId,eventdetailStr); */
			}
		});
	}
		
function viewLinkFormatter(row, cell, value, columnDef, dataContext) {
	var downloadTicket = custInvoiceGrid.getDataItem(row).viewTickets;
	var link = '';
	if(downloadTicket == 'Completed'){
		link = "<img class='editClickImage' style='height:17px;' src='../resources/images/viewPdf.png' onclick='openInvoiceTicketPopup("+ custInvoiceGrid.getDataItem(row).invoiceId + ")'/>";
	}
	return link;
}

 function openInvoiceTicketPopup(invId){
	 getInvoiceTicketHistory(invId);
		/*var url = "/Client/GetCustomerInfoForInvoiceTickets?invoiceId="+ invId;
		popupCenter(url, "Invoice Tickets", "800", "500");*/
	}
 
	function deleteShippingAddress(shippingId) {		
		/* var productType = $('#productType').val();
		if(productType!='REWARDTHEFAN'){
			jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
			return;
		} */
		
		if (shippingId == '') {
			jAlert("Shippingid is not found please refresh page and try again.");
			return false;
		}
		jConfirm("Are you sure to delete an shipping address ?","Confirm",function(r){
			if (r) {
				$.ajax({
					url : "/DeleteShippingAddress",
					type : "post",
					data : "userName=" + $("#vCust_email").val() + "&shippingId="+ shippingId,
					success : function(response) {
						if (response.status == 1) {
							getCustomerInfoForEdit($('#custId').val());
							//jAlert("Shipping address deleted successfully.");
						} /*else {
							jAlert(response);
							return false;
						}*/
						if(response.msg != null && response.msg != "" && response.msg != undefined){
							jAlert(response.msg);
						}
					},
					error : function(error) {
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
				});
			} else {
				return false;
			}
		});
		return false;
	}

	//toggle the add shipping address modal to add
	function addModal(){
		var shippingAddressCount = $('#count').val();
		/* var productType = $('#productType').val();
		if(productType!='REWARDTHEFAN'){
			jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
			return;
		} */
		if(shippingAddressCount == 5){
			jAlert("You have reached the limit of adding shipping address");
		}else{
			$('#myModal-2').modal('show');
			$('#vCust_shFirstName').val('');
			$('#vCust_shLastName').val('');
			$('#vCust_shEmail').val('');
			$('#vCust_shPhone').val('');
			$('#vCust_shAddressLine1').val('');
			$('#vCust_shAddressLine2').val('');
			$('#vCust_shCountryName').val('');
			$('#vCust_shStateName').val('');
			$('#vCust_shCity').val('');
			$('#vCust_shZipCode').val('');
			$('#vCust_shippingAddrId').val('');
		}
	}
	
	//toggle the edit shipping address modal to edit
	function editCustomerShippingAddress(addrId){
		/* var productType = $('#productType').val();
		if(productType!='REWARDTHEFAN'){
			jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
			return;
		} */
		$('#myModal-2').modal('show');
		for (var i = 0; i < shippingInfo.length; i++) {
			var  data= shippingInfo[i];
			var d = (custShippingData[i] = {}); 
			if(addrId == data.id){
				$('#vCust_shFirstName').val(data.firstName);
				$('#vCust_shLastName').val(data.lastName);
				$('#vCust_shEmail').val(data.email);
				$('#vCust_shPhone').val(data.phone);
				$('#vCust_shAddressLine1').val(data.street1);
				$('#vCust_shAddressLine2').val(data.street2);
				$("#vCust_shCountryName option:contains(" + data.country + ")").attr('selected', 'selected');
				$('#vCust_shCity').val(data.city);
				$('#vCust_shZipCode').val(data.zip);
				$('#vCust_shippingAddrId').val(data.id);
				loadCustShippingState(vCust_shCountryName,vCust_shStateName,data.stateId);
				$("#vCust_shStateName option:contains(" + data.state + ")").attr('selected', 'selected');				
			}
		}
	}	

function saveCustomerInfo(actionType){
	/* var productType = $('#productType').val();
	if(productType!='REWARDTHEFAN'){
		jAlert("Not Allowed to update Customer that belongs to other than REWARD THE FAN Product.");
		return;
	} */
	var paramString = '';
	if(actionType == 'customerInfo'){
		if(validateCustomerInfo()){
			paramString = 'action=customerInfo&customerType='+$('#vCust_customerType').val()+'&customerLevel='+$('#vCust_customerLevel').val()+
						'&signupType='+$('#vCust_signupType').val()+'&name='+$('#vCust_firstName').val()+'&lastName='+$('#vCust_lastName').val()+'&email='+$('#vCust_email').val()+
						'&phone='+$('#vCust_phone').val()+'&extension='+$('#vCust_extension').val()+'&otherPhone='+$('#vCust_otherPhone').val()+'&companyName='+$('#vCust_companyName').val()+
						'&representativeName='+$('#vCust_representativeName').val()+'&clientBrokerType='+$('#vCust_clientBrokerType').val()+'&id='+$('#custId').val();
		}
		
	}else if(actionType == 'billingInfo'){
		if(validateBillingInfo()){
			paramString = 'action=billingInfo&blFirstName='+$('#vCust_blFirstName').val()+'&blLastName='+$('#vCust_blLastName').val()+
						  '&addressLine1='+$('#vCust_blStreet1').val()+'&addressLine2='+$('#vCust_blStreet2').val()+'&countryName='+$('#vCust_blCountryName').val()+
						  '&stateName='+$('#vCust_blStateName').val()+'&city='+$('#vCust_blCity').val()+'&zipCode='+$('#vCust_blZipCode').val()+
						  '&blEmail='+$('#vCust_blEmail').val()+'&blPhone='+$('#vCust_blPhone').val()+'&id='+$('#custId').val();
						  
		}
	}else if(actionType == 'shippingInfo'){
		if(validateShippingInfo()){
			paramString = 'action=shippingInfo&shFirstName='+$('#vCust_shFirstName').val()+'&shLastName='+$('#vCust_shLastName').val()+
						  '&shAddressLine1='+$('#vCust_shAddressLine1').val()+'&shAddressLine2='+$('#vCust_shAddressLine2').val()+
						  '&shCountryName='+$('#vCust_shCountryName').val()+'&shippingAddrId='+$('#vCust_shippingAddrId').val()+
						'&shStateName='+$('#vCust_shStateName').val()+'&shCity='+$('#vCust_shCity').val()+'&shZipCode='+$('#vCust_shZipCode').val()+
						'&shEmail='+$('#vCust_shEmail').val()+'&shPhone='+$('#vCust_shPhone').val()+'&id='+$('#custId').val();
						  
		}
	}else if(actionType == 'invoiceInfo'){

	}else if(actionType == 'poInfo'){

	}else if(actionType == 'notes'){
		if($('#vCust_notesTxt').val()=='' || $('#vCust_notesTxt').val()==null){
			jAlert("Please Add notes to save.");
			return;
		}
		paramString = 'action=notes&notes='+$('#vCust_notesTxt').val()+'&id='+$('#custId').val();
	}else if(actionType == 'loyalFan'){

		var parentCategory = $('#parentType').val();
		var ticketPurchased = $('#vCust_loyalTicketsPurchased').val();		
		if(parentCategory != ""){
			if(parentCategory == 'SPORTS'){
				if($('#loyalFanArtists').val() != '' && $('#loyalFanArtists').val() != null){
					if(ticketPurchased == '' || ticketPurchased == null){							
						paramString = 'action=loyalFan';
						paramString += '&artistSelection='+$('#loyalFanArtists').val();
						paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();
					}
					if(ticketPurchased == 'Yes'){
						jConfirm('Are you sure to Update Loyal Fan ?','Confirm',function(r){
							if(r){ 
								paramString = 'action=loyalFan';
								paramString += '&artistSelection='+$('#loyalFanArtists').val();
								paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();
								updateCustomerDetails(paramString, actionType);
							}
							else{ return; }
						});
					}
					if(ticketPurchased == 'No'){							
						paramString = 'action=loyalFan';
						paramString += '&artistSelection='+$('#loyalFanArtists').val();
						paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();
					}					
				}else{
					jAlert("Please Select Artist to save Loyal Fan.");
					return;					
				}
			}
			if(parentCategory == 'CONCERTS' || parentCategory == 'THEATER'){
				if($('#loyalFanSelectionValue').val() != '' && $('#loyalFanSelectionValue').val() != null){
					if(ticketPurchased == '' || ticketPurchased == null){							
						paramString = 'action=loyalFan';
						paramString += '&loyalFanSelection='+$('#loyalFanSelectionValue').val();
						paramString += '&city='+$('#loyalFanCityName').val();
						paramString += '&state='+$('#loyalFanStateName').val();
						paramString += '&country='+$('#loyalFanCountryName').val();
						paramString += '&zipCode='+$('#loyalFanZipCode').val();
						paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();
					}
					if(ticketPurchased == 'Yes'){							
						jConfirm('Are you sure to Update Loyal Fan ?','Confirm',function(r){
							if(r){ 
								paramString = 'action=loyalFan';
								paramString += '&loyalFanSelection='+$('#loyalFanSelectionValue').val();
								paramString += '&city='+$('#loyalFanCityName').val();
								paramString += '&state='+$('#loyalFanStateName').val();
								paramString += '&country='+$('#loyalFanCountryName').val();
								paramString += '&zipCode='+$('#loyalFanZipCode').val();
								paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();
								updateCustomerDetails(paramString, actionType);
							}
							else{ return; }
						});
					}
					if(ticketPurchased == 'No'){							
						paramString = 'action=loyalFan';
						paramString += '&loyalFanSelection='+$('#loyalFanSelectionValue').val();
						paramString += '&city='+$('#loyalFanCityName').val();
						paramString += '&state='+$('#loyalFanStateName').val();
						paramString += '&country='+$('#loyalFanCountryName').val();
						paramString += '&zipCode='+$('#loyalFanZipCode').val();
						paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();						
					}
				}else{
					jAlert("Please Select State to save Loyal Fan.");
					return;					
				}
			}				
			
		}else{
			jAlert("Please select Parent Category to save.");
			return;
		}
		/*var parentCategory = $('#parentType').val();
		if(parentCategory != ""){
			paramString = 'action=loyalFan';
			if(parentCategory == 'SPORTS'){
				if($('#loyalFanArtists').val() != '' && $('#loyalFanArtists').val() != null){
					paramString += '&artistSelection='+$('#loyalFanArtists').val();
				}else{
					jAlert("Please select artist to save.");
					return;					
				}
			}
			if(parentCategory == 'CONCERTS' || parentCategory == 'THEATER'){
				if($('#loyalFanSelectionValue').val() != '' && $('#loyalFanSelectionValue').val() != null){
					paramString += '&loyalFanSelection='+$('#loyalFanSelectionValue').val();
					paramString += '&city='+$('#loyalFanCityName').val();
					paramString += '&state='+$('#loyalFanStateName').val();
					paramString += '&country='+$('#loyalFanCountryName').val();
					paramString += '&zipCode='+$('#loyalFanZipCode').val();
				}else{
					jAlert("Please select city or state to save.");
					return;					
				}
			}
			paramString += '&categoryName='+parentCategory+'&id='+$('#custId').val();
		}else{
			jAlert("Please select Parent Category to save.");
			return;
		}*/
	}else if(actionType == 'favouriteEvent'){
		//var eventIds  = getViewCustSelectedEventGridId();
		var eventIds = $('#favoriteEventId').val();
		if(eventIds=='' || eventIds.length==0){
			jAlert("Please Select Event to add in favourites.");
			return;
		}else{
			paramString = 'action=favouriteEvent&eventIds='+eventIds+'&id='+$('#custId').val();
		}
	}

	setTimeout(function(){ 
		if(paramString!=''){
			updateCustomerDetails(paramString, actionType);
		}
	},10);
}

function updateCustomerDetails(paramString, actionType){
	$.ajax({
		url : "/Client/UpdateCustomerDetails.json",
		type : "post",
		dataType:"json",
		data : paramString,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				var msg = jsonData.msg.replace(/"/g, '');
				if(msg != null && msg != ""){
					jAlert(msg);
				}
			}
			/*if(jsonData.topLevelInfoMsg != undefined){
				jAlert(jsonData.topLevelInfoMsg);
			}*/
			if(jsonData.shippingInfo != undefined){
				//if(jsonData.shippingInfoStatus == 1){
					$('#myModal-2').modal('hide');	
				//}
				//jAlert(jsonData.shippingInfoMsg);				
				shippingInfo = jsonData.shippingInfo;
				custShippingGridValues(jsonData.shippingInfo);
			}
			if(jsonData.billingInfo != undefined){
				if(jsonData.billingInfoStatus == 1){}
				fillBillingInfo(jsonData.billingInfo);
				//jAlert(jsonData.billingInfoMsg);
			}
			if(jsonData.loyalFanInfo != undefined){
				//jAlert(jsonData.Message);
				//loyalFanResetFilters();
				//getArtistGridData(0);
				fillLoyalFanInfo(jsonData.loyalFanInfo);
				$('#loyalFanArtists').val('');
				$('#loyalFanSelectionValue').val('');
				$('#loyalFanCityName').val('');
				$('#loyalFanStateName').val('');
				$('#loyalFanCountryName').val('');
				$('#loyalFanZipCode').val('');
				$('#taglabel').text('');
				$('#taglabel1').text('');
				$('#artistDiv').hide();
				$('#cityStateDiv').hide();
				$('#parentType').val('');
			}
			if(actionType == 'favouriteEvent'){
				//jAlert(jsonData.FavEventMessage);
				eventResetFilters();
				getCustEventsGridData(0);
				$('#favoriteEventId').val('');
				$('#tagFavoriteEvent').text('');
			}
			/*if(jsonData.notesMsg != undefined){
				jAlert(jsonData.notesMsg);
			}*/
			//jAlert("Record updated successfully.");
			clearAllSelections();
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateRewardPoints(){
	var rewardPointsStr = $('#vCust_newRewardPoints').val();
	if(rewardPointsStr == null || rewardPointsStr == "" || rewardPointsStr < 0){
		jAlert("Please enter valid reward points.");
		return;
	}
		
	$.ajax({
		url : "/Client/AddRewardPointsForCustomer.json",
		type : "post",
		dataType:"json",
		data : "customerId="+$('#custId').val()+"&points="+rewardPointsStr,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.status == 1){
				$('#vCust_newRewardPoints').val('');
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);
				fillRewardPointsInfo(jsonData.rewardPointsInfo);
			}			
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function updateCustomerWallet(){
	var walletAmountStr = $('#vCust_customerWalletAmount').val();
	var transactionTypes = $('#vCust_walletTransactionType').val();
	if(walletAmountStr < 0){
		jAlert("Please enter valid Wallet Amount.");
		return;
	}
	if(transactionTypes == null || transactionTypes == ''){
		jAlert("Please select valid Transaction Type.");
		return;
	}
		
	$.ajax({
		url : "/Client/AddWalletForCustomer.json",
		type : "post",
		dataType:"json",
		data : "customerId="+$('#custId').val()+"&customerWalletAmount="+walletAmountStr+"&transactionType="+transactionTypes,
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			if(jsonData.status == 1){
				$('#vCust_customerWalletAmount').val('');
				$('#vCust_walletTransactionType').val('');
			}
			if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
				jAlert(jsonData.msg);				
				fillCreditInfo(jsonData.creditInfo);
			}			
		}, error : function(error){
			jAlert("Your login session is expired please refresh page and login again.", "Error");
			return false;
		}
	});
}

function validateShippingInfo(){
	if($('#vCust_shFirstName').val()=='' || $('#vCust_shFirstName').val()==null){
		jAlert("Shipping Address - First name is mendatory.");
		return false;
	}
	if($('#vCust_shLastName').val()=='' || $('#vCust_shLastName').val()==null){
		jAlert("Shipping Address - Last name is mendatory.");
		return false;
	}
	if($('#vCust_shEmail').val()=='' || $('#vCust_shEmail').val()==null){
		jAlert("Shipping Address - Email is Mendatory.");
		return false;
	}
	if($('#vCust_shPhone').val()=='' || $('#vCust_shPhone').val()==null){
		jAlert("Shipping Address - Phone is Mendatory.");
		return false;
	}
	if($('#vCust_shAddressLine1').val()=='' || $('#vCust_shAddressLine1').val()==null){
		jAlert("Shipping Address - Street1 is mendatory.");
		return false;
	}
	if($('#vCust_shCity').val()=='' || $('#vCust_shCity').val()==null){
		jAlert("Shipping Address - City is mendatory.");
		return false;
	}
	if($('#vCust_shCountryName').val() <= 0){
		jAlert("Shipping Address - Country is mendatory.");
		return false;
	}
	if($('#vCust_shStateName').val() <= 0){
		jAlert("Shipping Address - State is mendatory.");
		return false;
	}
	if($('#vCust_shZipCode').val()=='' || $('#vCust_shZipCode').val()==null){
		jAlert("Shipping Address - Zipcode is mendatory.");
		return false;
	}
	return true;
}

function validateBillingInfo(){
	if($('#vCust_blFirstName').val()=='' || $('#vCust_blFirstName').val()==null){
		jAlert("Billing Address - First name is mendatory.");
		return false;
	}
	if($('#vCust_blLastName').val()=='' || $('#vCust_blLastName').val()==null){
		jAlert("Billing Address - Last name is mendatory.");
		return false;
	}
	if($('#vCust_blEmail').val()=='' || $('#vCust_blEmail').val()==null){
		jAlert("Billing address - Email is mendatory.");
		return false;
	}
	if($('#vCust_blPhone').val()=='' || $('#vCust_blPhone').val()==null){
		jAlert("Billing address - Phone is mendatory.");
		return false;
	}
	if($('#vCust_blStreet1').val()=='' || $('#vCust_blStreet1').val()==null){
		jAlert("Billing Address - Street1 is mendatory.");
		return false;
	}
	if($('#vCust_blCity').val()=='' || $('#vCust_blCity').val()==null){
		jAlert("Billing Address - City is mendatory.");
		return false;
	}
	if($('#vCust_blCountryName').val() <= 0){
		jAlert("Billing Address - Country is mendatory.");
		return false;
	}
	if($('#vCust_blStateName').val() <= 0){
		jAlert("Billing Address - State is mendatory.");
		return false;
	}
	if($('#vCust_blZipCode').val()=='' || $('#vCust_blZipCode').val()==null){
		jAlert("Billing Address - Zip Code is mendatory.");
		return false;
	}
	return true;
}

function validateCustomerInfo(){
	if($('#vCust_firstName').val()=='' || $('#vCust_firstName').val()==null){
		jAlert("First Name is mendatory.");
		return false;
	}
	if($('#vCust_lastName').val()=='' || $('#vCust_lastName').val()==null){
		jAlert("Last Name is mendatory.");
		return false;
	}
	if($('#vCust_email').val()=='' || $('#vCust_email').val()==null){
		jAlert("Email is mendatory.");
		return false;
	}
	if($('#vCust_phone').val()=='' || $('#vCust_phone').val()==null){
		jAlert("Phone is mendatory.");
		return false;
	}
	if($('#vCust_customerLevel').val()=='' || $('#vCust_customerLevel').val()==null){
		jAlert("Customer Status is mendatory.");
		return false;
	}
	if($('#vCust_signupType').val()=='' || $('#vCust_signupType').val()==null){
		jAlert("Signup Type is mendatory.");
		return false;
	}
	if($('#vCust_customerLevel').val()=='' || $('#vCust_customerLevel').val()==null){
		jAlert("Customer Status is mendatory.");
		return false;
	}
	if($('#vCust_signupType').val()=='' || $('#vCust_signupType').val()==null){
		jAlert("Signup Type is mendatory.");
		return false;
	}
	
	return true;
}

		
	function saveUserArtistPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = custArtistGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+",";
		}
		saveUserPreference('artistsearchgrid',colStr);
	}	

	function openPointsHistory(msg){
		var custmrId = $('#custId').val();//'${customerId}';
		getRewardPointsHistory(custmrId, msg);
		/*var url = "${pageContext.request.contextPath}/Client/EditRewardPoints?customerId="
			+ custmrId+"&Message="+msg;
		popupCenter(url, "Edit Reward Points", "800", "500");*/
	}
	
	function getViewCustSelectedEventGridId() {
		var viewCustEventRowIndex = custEventGrid.getSelectedRows();
		
		var eventIdStr='';
		$.each(viewCustEventRowIndex, function (index, value) {
			eventIdStr += ','+custEventGrid.getDataItem(value).eventMarketId;
		});
		
		if(eventIdStr != null && eventIdStr!='') {
			eventIdStr = eventIdStr.substring(1, eventIdStr.length);
			 return eventIdStr;
		}
	}
			
	function saveCustEventUserPreference(){
		var cols = visibleColumns;
		if(cols==null || cols =='' || cols.length==0){
			cols = custEventGrid.getColumns();
		}
		var colStr = '';
		for(var i=0;i<cols.length;i++){
			colStr += cols[i].id+":"+cols[i].width+","
		}
		saveUserPreference('custeventsgrid',colStr);
	}
	
	//Start Reward Points
	function getRewardPointsHistory(customerId, message){
		$.ajax({		  
			url : "/Client/EditCustomerInfoForRewardPoints.json",
			type : "post",
			data : "customerId="+customerId+"&Message="+message,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setRewardPointsHistory(jsonData);
				} /*else {
					jAlert(jsonData.msg);
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setRewardPointsHistory(jsonData){
		$('#edit-reward-points').modal('show');
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#custRewards_successDiv').hide();
			$('#custRewards_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#custRewards_errorDiv').hide();
			$('#custRewards_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != ""){
			$('#custRewards_successDiv').show();
			$('#custRewards_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != ""){
			$('#custRewards_errorDiv').show();
			$('#custRewards_errorMsg').text(jsonData.errorMessage);
		}*/
		$('#custRewards_customerId').val(jsonData.customerId);
		$('#custRewards_breakUpMsg').val(jsonData.rewardPointBreakUp);
		custRewardPointsSearchString='';
		custRewardPointsColumnFilters = {};
		custRewardPointsGridValues(jsonData.rewardPointsList, jsonData.pagingInfo);
	}
	//End Reward Points
	
	function loadCustShippingState(countryId, stateId, findName){
		var countryId = $(countryId).val();
		if(countryId>0){
			$.ajax({
				url : "/GetStates",
				type : "post",
				dataType:"json",
				data : "countryId="+countryId,
				success : function(res){
					var jsonData = res.states;//JSON.parse(JSON.stringify(res));
					updateStateCountryComboById(jsonData,stateId,findName);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$(stateId).empty();
			$(stateId).append("<option value='-1'>--select--</option>");
		}
	}
	
	function updateStateCountryComboById(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].id == listName){
					$(id).append("<option value="+jsonData[i].id+" selected>"+jsonData[i].name+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
				}
			}
		}
	}
	
	function loadAutoComplete(){
		var parentCategory = $('#parentType').val();
		if(parentCategory != ''){
			if(parentCategory == 'SPORTS'){
				$('#artistDiv').show();
				$('#cityStateDiv').hide();
			}else if(parentCategory == 'CONCERTS' || parentCategory == 'THEATER'){
				$('#artistDiv').hide();
				$('#cityStateDiv').show();
			}
		}
	}
	
	//Start View Invoice Tickets
	function getInvoiceTicketHistory(invoiceId){
		$.ajax({		  
			url : "/Client/GetCustomerInfoForInvoiceTickets.json",
			type : "post",
			data : "invoiceId="+invoiceId,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1) {
					setInvoiceTicketHistory(jsonData);
				} /*else {
					jAlert(jsonData.msg);
				}*/
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
	function setInvoiceTicketHistory(jsonData){
		$('#view-invoice-tickets').modal('show');
		$('#invoiceTicket_invoiceId').val(jsonData.invoiceId);
		createInvoiceTicketGridValues(jsonData.invoiceTicketList, jsonData.pagingInfo);
	}
	//End View Invoice Tickets
	
	/*
	function pagingControl(move,id){
		if(id=='custArtist_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custArtistPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custArtistPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custArtistPagingInfo.pageNum)-1;
			}
			getArtistGridData(pageNo);
		}
		if(id=='custEvent_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(custEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(custEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(custEventPagingInfo.pageNum)-1;
			}
			getCustEventsGridData(pageNo);
		}
	}
	
	$(document).ready(function(){
	 //$("div#divLoading").addClass('show');
	 $("#vCust_sameBillingEmail").change(function(){
		 if($("#vCust_sameBillingEmail").is(':checked')){
		 	var email = $('#vCust_email').val();
		 	if(email=='' || email==undefined){
		 		jAlert("Please add Primary email address from top level info tab.");
		 		$("#vCust_sameBillingEmail").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_blEmail").val(email);
	 	 }else{
	 		$("#vCust_blEmail").val('');
	 		
	 	 }
	 });
	 $("#vCust_sameBillingPhone").change(function(){
		 if($("#vCust_sameBillingPhone").is(':checked')){
		 	var phone = $('#vCust_phone').val();
		 	if(phone=='' || phone==undefined){
		 		jAlert("Please add Primary Phone from top level info tab.");
		 		$("#vCust_sameBillingPhone").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_blPhone").val(phone);
	 	 }else{
	 		$("#vCust_blPhone").val('');
	 		
	 	 }
	 });
	 $("#vCust_sameShippingEmail").change(function(){
		 if($("#vCust_sameShippingEmail").is(':checked')){
		 	var email = $('#vCust_email').val();
		 	if(email=='' || email==undefined){
		 		jAlert("Please add Primary email address from top level info tab.");
		 		$("#vCust_sameShippingEmail").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_shEmail").val(email);
	 	 }else{
	 		$("#vCust_shEmail").val('');
	 		
	 	 }
	 });
	 $("#vCust_sameShippingPhone").change(function(){
		 if($("#vCust_sameShippingPhone").is(':checked')){
		 	var phone = $('#vCust_phone').val();
		 	if(phone=='' || phone==undefined){
		 		jAlert("Please add Primary Phone from top level info tab.");
		 		$("#vCust_sameShippingPhone").attr('checked',false);
		 		return false;
		 	}
		 	$("#vCust_shPhone").val(phone);
	 	 }else{
	 		$("#vCust_shPhone").val('');
	 		
	 	 }
	 });
	 //getCustomerInfoForEdit(customerId);
	$('#editCustomerDiv').hide();
	$('#shippingTab').click(function(){
		setTimeout(function(){ 
		custShippingGridValues(shippingInfo);},10);
	});
	$('#invoiceTab').click(function(){
		setTimeout(function(){
		getCustomerInfoForInvoice();},10);
	});
	$('#cardTab').click(function(){
		setTimeout(function(){ 
		createCreditCardGrid(cardInfo);},10);
	});
	$('#poTab').click(function(){
		setTimeout(function(){
		getCustomerInfoForPO();},10);
	});
	$('#loyalFanTab').click(function(){
		setTimeout(function(){
			getArtistGridData(0);},10);
	});
	$('#favouriteEventTab').click(function(){
		setTimeout(function(){
			getCustEventsGridData(0);},10);
	});
	$('#menuContainer').click(function(){
	  if($('.ABCD').length >0){
		   $('#menuContainer').removeClass('ABCD');
	  }else{
		   $('#menuContainer').addClass('ABCD');
	  }
	
	});
	});*/