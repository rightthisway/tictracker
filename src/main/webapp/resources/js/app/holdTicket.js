$(document).ready(function(){
	 $('#expDate').datepicker({
        format: "mm/dd/yyyy",
		autoclose : true,
		todayHighlight: true,
		startDate:'today' 
     });
});

function isSequential(ticketId){
	var arrayTicketId = ticketId.split(",");
	var ticketIdInitialValue = parseInt(arrayTicketId[0]);
	var flag = true;
    for(var i=0; i<arrayTicketId.length; i++){
		if(parseInt(arrayTicketId[i]) == ticketIdInitialValue){
			flag = true;
		}else{
			flag = false; 
			break;
		}
		ticketIdInitialValue = parseInt(ticketIdInitialValue) + 1;
	}
	return flag;
}

function updateTicketHold(onHand){
	if(onHand == 'HOLD'){
		var temprTicketRowIndex = editHoldTicketGrid.getSelectedRows();
		var ticketIdStr='';
		var validTickets = true;
		$.each(temprTicketRowIndex, function (index, value) {
			if(onHand=='HOLD' && editHoldTicketGrid.getDataItem(value).status != 'ACTIVE'){
				validTickets =false;
			}/*else if(onHand=='UNHOLD' && editHoldTicketGrid.getDataItem(value).status != 'ONHOLD'){
				validTickets =false;			
			}*/
			ticketIdStr += ','+editHoldTicketGrid.getDataItem(value).id;
		});
		
		/*if(onHand=='UNHOLD' && !validTickets){
			jAlert("Only ONHOLD tickets can be added back to ACTIVE.");
			return;
		}*/
		if(onHand=='HOLD' && !validTickets){
			jAlert("Only ACTIVE tickets can be added to on Hold.");
			return;
		}
		
		if(ticketIdStr != null && ticketIdStr!='') {
			ticketIdStr = ticketIdStr.substring(1, ticketIdStr.length);
		}
		if(!isSequential(ticketIdStr)){
			jAlert("Please select sequential seat order for hold ticket.");
			return;
		}
		
		var expDate = $('#expDate').val();
		var expMinutes = $('#expMinutes').val();
		var salesPrice = $('#holdSalesPrice').val();
		var holdTicketId = $('#holdTicketId').val();
		
		if(ticketIdStr==null || ticketIdStr==''){
			jAlert("Please select atleast one record to update.");
			return;
		}
		
		if(holdTicketId == '' || holdTicketId == undefined){
			jAlert("Selected ticket Group is not found, please refresh page and try again.");
			return;
		}	
	
		/*if(custRowId < 0){
			jAlert("Please select customer.");
			return;
		}*/
		if(expDate == '' && expMinutes==''){
			jAlert("Please add expiry date or expiry minutes.");
			return;
		}
		/*if(expDate!=null && expDate != ''){
			if(new Date().getTime() <=  new Date(expDate).getTime()){
				jAlert("expiry date must be future date.");
				return;
			}
		}*/
		if(salesPrice <= 0){
			jAlert("Please add valid sold price.");
			return;
		}
		var custRowId = editHoldCustomerGrid.getSelectedRows([0])[0];
		if((custRowId == undefined || custRowId == '') && custRowId != 0){
			jAlert("Please select customer.");
			return;
		}
		var customerId = editHoldCustomerGrid.getDataItem(custRowId).customerId;
		
		$.ajax({
			url : "/Invoice/HoldTicket.json",
			type : "post",
			data : "ticketGroupId="+holdTicketId+"&action="+onHand+"&ticketIds="+ticketIdStr+"&customerId="+customerId+"&salesPrice="+salesPrice+"&expiryDate="+expDate+"&expiryMinutes="+expMinutes,
			dataType:"json",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));
				if(jsonData.status==1){
					//refreshEditHoldTicketGridValues(jsonData.tickets);
					//getEditHoldCustomerGridData(0);
					$('#hold-ticket').modal('hide');
					ticketResetFilters();
					jAlert(jsonData.msg);
				}else{
					jAlert("Something went wrong, please try again.");
					return;
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
		
		/*var customerId = editHoldCustomerGrid.getDataItem(custRowId).customerId;
		window.location="/Invoice/HoldTicket?ticketGroupId=${ticketGroupId}&action="
		+onHand+"&ticketIds="+ticketIdStr+"&customerId="+customerId+"&salesPrice="+salesPrice+"&expiryDate="+expDate+"&expiryMinutes="+expMinutes;*/
	}else if(onHand == 'UNHOLD'){
		var temprHoldTicketRowIndex = holdTicketsGrid.getSelectedRows([0])[0];		
		var validTickets = true;
		var holdTicketGroupId = holdTicketsGrid.getDataItem(temprHoldTicketRowIndex).ticketGroupId;
		var ticketIdStr = holdTicketsGrid.getDataItem(temprHoldTicketRowIndex).ticketIds;
		
		if(onHand=='UNHOLD' && holdTicketsGrid.getDataItem(temprHoldTicketRowIndex).status != 'ACTIVE'){
			validTickets =false;			
		}
		
		if(onHand=='UNHOLD' && !validTickets){
			jAlert("Only ACTIVE tickets can be added back to DELETED.");
			return;
		}
		//window.location="/Invoice/HoldTicket?ticketGroupId=${ticketGroupId}&action="+onHand+"&ticketIds="+ticketIdStr;
		
		$.ajax({
			url : "/Invoice/UnHoldTicket",
			type : "post",
			data : "ticketGroupId="+holdTicketGroupId+"&action="+onHand+"&ticketIds="+ticketIdStr,
			dataType:"json",
			success : function(response){
				var jsonData = JSON.parse(JSON.stringify(response));				
				if(jsonData.status==1){
					holdResetFilters();				
				}
				if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
					jAlert(jsonData.msg);
				}
			},
			error : function(error){
				jAlert("Your login session is expired please refresh page and login again.", "Error");
				return false;
			}
		});
	}
	
}

$("div#divLoading").addClass('show');
var holdTicketCheckboxSelector = new Slick.CheckboxSelectColumn({
	 cssClass: "slick-cell-checkboxsel"
});
var editHoldPagingInfo;
var editHoldTicketDataView;
var editHoldTicketGrid;
var editHoldTicketData = [];
var editHoldTicketColumns = [ holdTicketCheckboxSelector.getColumnDefinition(),
{
	id : "id",
	name : "Ticket Id",
	field : "id",
	width:80,
	sortable : true
},{
	id : "section",
	name : "Section",
	field : "section",
	width:80,
	sortable : true
},{
	id : "row",
	name : "Row",
	field : "row",
	width:80,
	sortable : true
},{
	id : "seatNo",
	name : "Seat No",
	field : "seatNo",
	width:80,
	sortable : true
},{
	id: "onHandStatus", 
	name: "on Hand", 
	field: "onHandStatus",
	width:80,		
	sortable: true
},{
	id: "cost", 
	name: "Cost", 
	field: "cost", 
	width:80,
	sortable: true,
},{
	id: "actualSoldPrice", 
	name: "Actual Sold Price", 
	field: "actualSoldPrice", 
	width:80,
	sortable: true
},{
	id: "facePrice", 
	name: "Face Price", 
	field: "facePrice",
	width:80,
	sortable: true
},{
	id: "wholeSalePrice", 
	name: "WholeSale Price", 
	field: "wholeSalePrice",
	width:80,
	sortable: true
} , {
	id: "retailPrice", 
	name: "Retail Price", 
	field: "retailPrice",
	width:80,
	sortable: true
} , {
	id: "status", 
	name: "Status", 
	field: "status",
	width:80,
	sortable: true
}];

var editHoldTicketOptions = {
	editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	multiSelect: false,
	topPanelHeight : 25
};
var editHoldTicketGridSortcol = "id";
var editHoldTicketGridSortdir = 1;
var percentCompleteThresEditHold = 0;
var editHoldTicketGridSearchString = "";


function deleteRecordFromEditHoldTicketGrid(id) {
	editHoldTickeditHoldtaView.deleteItem(id);
	editHoldTicketGrid.invalidate();
}
function editHoldTicketGridFilter(item, args) {
	var x= item["id"];
	if (args.editHoldTicketGridSearchString  != ""
			&& x.indexOf(args.editHoldTicketGridSearchString) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.editHoldTicketGridSearchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}

function editHoldTicketGridComparer(a, b) {
	var x = a[editHoldTicketGridSortcol], y = b[editHoldTicketGridSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
function editHoldTicketGridToggleFilterRow() {
	editHoldTicketGrid.setTopPanelVisibility(!editHoldTicketGrid.getOptions().showTopPanel);
}

function refreshEditHoldTicketGridValues(jsonData) {
	editHoldTicketData = [];
	for(var i=0;i<jsonData.length;i++){
		var d = (editHoldTicketData[i] = {});
		d["id"] = jsonData[i].id;
		d["cost"] = jsonData[i].cost;
		d["actualSoldPrice"] = jsonData[i].actualSoldPrice;
		d["facePrice"] = jsonData[i].facePrice;
		d["wholeSalePrice"] = jsonData[i].wholeSalePrice;
		d["retailPrice"] = jsonData[i].retailPrice;
		d["section"] = jsonData[i].section;
		d["row"] = jsonData[i].row;
		d["seatNo"] = jsonData[i].seatNo;
		d["onHandStatus"] = jsonData[i].onHandStatus;
		d["status"] = jsonData[i].status;
	}

	editHoldTicketDataView = new Slick.Data.DataView({
		inlineFilters : true
	});
	editHoldTicketGrid = new Slick.Grid("#editHold_ticket_grid", editHoldTicketDataView, editHoldTicketColumns, editHoldTicketOptions);
	editHoldTicketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	editHoldTicketGrid.setSelectionModel(new Slick.RowSelectionModel());
	editHoldTicketGrid.registerPlugin(holdTicketCheckboxSelector);
	if(editHoldPagingInfo != null){
		var editHoldTicketGridPager = new Slick.Controls.Pager(editHoldTicketDataView, editHoldTicketGrid, $("#editHold_ticket_pager"),editHoldPagingInfo);
	}
	var editHoldTicketGridColumnpicker = new Slick.Controls.ColumnPicker(editHoldTicketColumns, editHoldTicketGrid,
			editHoldTicketOptions);

	// move the filter panel defined in a hidden div into editHoldTicketGrid top panel
	$("#ticket_inlineFilterPanel").appendTo(editHoldTicketGrid.getTopPanel()).show();
	editHoldTicketGrid.onSort.subscribe(function(e, args) {
		editHoldTicketGridSortdir = args.sortAsc ? 1 : -1;
		editHoldTicketGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			editHoldTicketDataView.fastSort(editHoldTicketGridSortcol, args.sortAsc);
		} else {
			editHoldTicketDataView.sort(editHoldTicketGridComparer, args.sortAsc);
		}
	});

	editHoldTicketGrid.onCellChange.subscribe(function (e,args) { 
			var temprEventRowIndex = editHoldTicketGrid.getSelectedRows();
			var ticketId;
			var eventNotes; 
			$.each(temprEventRowIndex, function (index, value) {
				ticketId = editHoldTicketGrid.getDataItem(value).id;
				ticketNotes = editHoldTicketGrid.getDataItem(value).notes;
			});
			saveNotes(ticketId,ticketNotes);
     });
	// wire up model tickets to drive the editHoldTicketGrid
	editHoldTicketDataView.onRowCountChanged.subscribe(function(e, args) {
		editHoldTicketGrid.updateRowCount();
		editHoldTicketGrid.render();
	});
	editHoldTicketDataView.onRowsChanged.subscribe(function(e, args) {
		editHoldTicketGrid.invalidateRows(args.rows);
		editHoldTicketGrid.render();
	});

	// wire up the search textbox to apply the filter to the model
	$("#ticketGridSearch").keyup(function(e) {
		Slick.GlobalEditorLock.cancelCurrentEdit();
		// clear on Esc
		if (e.which == 27) {
			this.value = "";
		}
		editHoldTicketGridSearchString = this.value;
		updateEditHoldTicketGridFilter();
	});
	function updateEditHoldTicketGridFilter() {
		editHoldTicketDataView.setFilterArgs({
			editHoldTicketGridSearchString : editHoldTicketGridSearchString
		});
		editHoldTicketDataView.refresh();
	}
	
	// initialize the model after all the tickets have been hooked up
	editHoldTicketDataView.beginUpdate();
	editHoldTicketDataView.setItems(editHoldTicketData);
	editHoldTicketDataView.setFilterArgs({
		percentCompleteThresEditHold : percentCompleteThresEditHold,
		editHoldTicketGridSearchString : editHoldTicketGridSearchString
	});
	editHoldTicketDataView.setFilter(editHoldTicketGridFilter);
	editHoldTicketDataView.endUpdate();
	editHoldTicketDataView.syncGridSelection(editHoldTicketGrid, true);
	$("#gridContainer").resizable();
	//editHoldTicketGrid.resizeCanvas();
	 $("div#divLoading").removeClass('show');
}



var holdTicketPagingInfo;
var editHoldCustomerGrid;
var editHoldCustomerDataView;
var editHoldCustomerData=[];
var editHoldCustomerGridSearchString='';
var editHoldCustomerColumnFilters = {};
var editHoldCustomerColumns = [
               {id:"customerType", name:"Customer Type", field: "customerType", sortable: true, width: 50},
               {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
               {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
               {id:"email", name:"Email", field: "email", sortable: true, width: 140},
               {id:"productType", name:"Product Type", field:"productType", sortable: true, width: 120},
               {id:"client", name:"Client", field: "client", sortable: true},
               {id:"broker", name:"Broker", field: "broker", sortable: true},
               {id:"street1", name:"Street1", field: "street1", sortable: true},
               {id:"street2", name:"Street2", field: "street2", sortable: true},
               {id:"city", name:"City", field: "city", sortable: true},
               {id:"state", name:"State", field: "state", sortable: true},
			   {id:"country", name:"Country", field: "country", sortable: true},
               {id:"zip", name:"Zip", field: "zip", sortable: true},
               {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
              ];
              
var editHoldCustomerOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
var editHoldCustomerGridSortcol = "customerName";
var editHoldCustomerGridSortdir = 1;
var percentCompleteThreshold = 0;


function editHoldCustomerGridComparer(a, b) {
	var x = a[editHoldCustomerGridSortcol], y = b[editHoldCustomerGridSortcol];
	if(!isNaN(x)){
	   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	}
	if(x == '' || x == null) {
		return 1;
	} else if(y == '' || y == null) {
		return -1;
	}
	if(x.indexOf('/') > 0 && x.length==10){
		return commonDateComparator(x,y);
	}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
		return commonDateTimeComparator(x,y);
	}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
		 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
	}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
		return x.toLowerCase().localeCompare(y.toLowerCase());	
	} else {
		return (x == y ? 0 : (x > y ? 1 : -1));	
	}
}
			
function refreshEditHoldCustomerGridValues(jsonData) {
	 $("div#divLoading").addClass('show');
	editHoldCustomerData = [];
	for (var i = 0; i < jsonData.length; i++) {
		var  data= jsonData[i]; 
		var d = (editHoldCustomerData[i] = {});
		d["id"] = i;
		d["customerId"] = data.customerId;
		d["bAddressId"] = data.billingAddressId;
		d["firstName"] = data.customerName;
		d["lastName"] = data.lastName;
		d["customerType"] = data.customerType;
		d["client"] = data.client;
		d["broker"] = data.broker;
		d["email"] = data.customerEmail;
		d["productType"] = data.productType;
		d["street1"] = data.addressLine1;	
		d["street2"] = data.addressLine2;
		d["city"] = data.city;
		d["state"] = data.state;
		d["country"] = data.country;
		d["zip"] = data.zipCode;
		d["phone"] = data.phone;
	}

	editHoldCustomerDataView = new Slick.Data.DataView();
	editHoldCustomerGrid = new Slick.Grid("#editHold_customer_grid", editHoldCustomerDataView, editHoldCustomerColumns, editHoldCustomerOptions);
	editHoldCustomerGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	editHoldCustomerGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(holdTicketPagingInfo!=null){
		var editHoldCustomerGridPager = new Slick.Controls.Pager(editHoldCustomerDataView, editHoldCustomerGrid, $("#editHold_customer_pager"),holdTicketPagingInfo);
	}
	
	var editHoldCustomerGridColumnpicker = new Slick.Controls.ColumnPicker(editHoldCustomerColumns, editHoldCustomerGrid,
			editHoldCustomerColumns);

	// move the filter panel defined in a hidden div into editHoldCustomerGrid top panel
	//$("#customer_inlineFilterPanel").appendTo(editHoldCustomerGrid.getTopPanel()).show();

	editHoldCustomerGrid.onSort.subscribe(function(e, args) {
		editHoldCustomerGridSortdir = args.sortAsc ? 1 : -1;
		editHoldCustomerGridSortcol = args.sortCol.field;
		if ($.browser.msie && $.browser.version <= 8) {
			editHoldCustomerDataView.fastSort(editHoldCustomerGridSortcol, args.sortAsc);
		} else {
			editHoldCustomerDataView.sort(editHoldCustomerGridComparer, args.sortAsc);
		}
	});
	// wire up model customers to drive the editHoldCustomerGrid
	editHoldCustomerDataView.onRowCountChanged.subscribe(function(e, args) {
		editHoldCustomerGrid.updateRowCount();
		editHoldCustomerGrid.render();
	});
	editHoldCustomerDataView.onRowsChanged.subscribe(function(e, args) {
		editHoldCustomerGrid.invalidateRows(args.rows);
		editHoldCustomerGrid.render();
	});
	
	$(editHoldCustomerGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
	 var keyCode = (e.keyCode ? e.keyCode : e.which);
		editHoldCustomerGridSearchString='';
		 var columnId = $(this).data("columnId");
		  if (columnId != null) {
			editHoldCustomerColumnFilters[columnId] = $.trim($(this).val());
			if(keyCode == 13) {
				for (var columnId in editHoldCustomerColumnFilters) {
				  if (columnId !== undefined && editHoldCustomerColumnFilters[columnId] !== "") {
					  editHoldCustomerGridSearchString += columnId + ":" +editHoldCustomerColumnFilters[columnId]+",";
				  }
				}
				getEditHoldCustomerGridData(0);
			}
		  }
	 
	});
	editHoldCustomerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
		$("<input type='text'>")
	   .data("columnId", args.column.id)
	   .val(editHoldCustomerColumnFilters[args.column.id])
	   .appendTo(args.node);
	});
	editHoldCustomerGrid.init();
		
	// initialize the model after all the customers have been hooked up
	editHoldCustomerDataView.beginUpdate();
	editHoldCustomerDataView.setItems(editHoldCustomerData);
	editHoldCustomerDataView.endUpdate();
	editHoldCustomerDataView.syncGridSelection(editHoldCustomerGrid, true);
	$("#gridContainer").resizable();
	$("div#divLoading").removeClass('show');
}


/*function pagingControl(move,id){
	var pageNo = 0;
	if(move == 'FIRST'){
		pageNo = 0;
	}else if(move == 'LAST'){
		pageNo = parseInt(holdTicketPagingInfo.totalPages)-1;
	}else if(move == 'NEXT'){
		pageNo = parseInt(holdTicketPagingInfo.pageNum) +1;
	}else if(move == 'PREV'){
		pageNo = parseInt(holdTicketPagingInfo.pageNum)-1;
	}
	getEditHoldCustomerGridData(pageNo);
}*/

function getEditHoldCustomerGridData(pageNo) {
	//var searchValue = $("#searchValue").val();
	$.ajax({
		url : "/Client/ManageDetails.json",
		type : "post",
		data : $("#customerSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+editHoldCustomerGridSearchString,
		dataType:"json",
		success : function(res){
			var jsonData = JSON.parse(JSON.stringify(res));
			holdTicketPagingInfo = jsonData.pagingInfo;
			refreshEditHoldCustomerGridValues(jsonData.customers);
		}, error : function(error){
			jAlert("There is something wrong. Please try again"+error,"Error");
			return false;
		}
	});
}
			
function resetEditHoldTicketFilters(){
	editHoldCustomerGridSearchString='';
	editHoldCustomerColumnFilters = {};
	getEditHoldCustomerGridData(0);
}

