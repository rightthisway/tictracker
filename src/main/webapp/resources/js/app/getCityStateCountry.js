/**
 * 
 */

	function getCityStateCountry(zipCode, pageName){
		if(zipCode == null || zipCode == ''){
			//jAlert("Please enter zipcode.", "Error");
			//return;
		}else{
			$.ajax({		  
				url : "/GetCityStateCountry",
				type : "post",
				data : "zipCode="+zipCode,
				dataType:"json",
				success : function(res){
					var jsonData = JSON.parse(JSON.stringify(res));
					if(jsonData.status == 1) {
						setCityStateCountry(jsonData.cityStateCountryDetails, jsonData.countryList, pageName);
					}else{
						jAlert(jsonData.msg);
					}
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});	
		}
	}
	

	function setCityStateCountry(jsonData, countryList, pageName){
		if(pageName == 'body-manage-invoice-shipping'){
			if(jsonData != null){
				$('#esa_city').val(jsonData.city);
				updateStateCountryCombo(countryList, esa_country, jsonData.countryName);
				loadState(esa_country, esa_state, jsonData.stateName);
			}else{
				$('#esa_city').val('');
				updateStateCountryCombo(countryList, esa_country, '');
				loadState(esa_country, esa_state, '');
			}
		}
		if(pageName == 'body-manage-invoice-billing'){
			if(jsonData != null){
				$('#eba_city').val(jsonData.city);
				updateStateCountryCombo(countryList, eba_country, jsonData.countryName);
				loadState(eba_country, eba_state, jsonData.stateName);
			}else{
				$('#eba_city').val('');
				updateStateCountryCombo(countryList, eba_country, '');
				loadState(eba_country, eba_state, '');
			}
		}
		if(pageName == 'body-manage-invoice-fedex'){
			if(jsonData != null){
				$('#cf_city').val(jsonData.city);
				updateStateCountryCombo(countryList, cf_country, jsonData.countryName);
				loadState(cf_country, cf_state, jsonData.stateName);
			}else{
				$('#cf_city').val('');
				updateStateCountryCombo(countryList, cf_country, '');
				loadState(cf_country, cf_state, '');
			}
		}
		if(pageName == 'body-client-manage-details-billing'){
			if(jsonData != null){
				$('#city').val(jsonData.city);
				updateStateCombo(countryList, 'countryName', jsonData.countryName);
				loadState('stateName', jsonData.stateName);
			}else{
				$('#city').val('');
				updateStateCombo(countryList, 'countryName', '');
				loadState('stateName', '');
			}
		}
		if(pageName == 'body-client-manage-details-shipping'){
			if(jsonData != null){
				$('#shCity').val(jsonData.city);
				updateStateCombo(countryList, 'shCountryName', jsonData.countryName);
				loadState('shStateName', jsonData.stateName);
			}else{
				$('#shCity').val('');
				updateStateCombo(countryList, 'shCountryName', '');
				loadState('shStateName', '');
			}
		}
		if(pageName == 'body-view-customer-details-billing'){
			if(jsonData != null){
				$('#vCust_blCity').val(jsonData.city);
				updateStateCountryComboById(countryList, vCust_blCountryName, jsonData.countryId);			
				loadCustShippingState(vCust_blCountryName, vCust_blStateName, jsonData.stateId);
			}else{
				$('#vCust_blCity').val('');
				updateStateCountryComboById(countryList, vCust_blCountryName, '');			
				loadCustShippingState(vCust_blCountryName, vCust_blStateName, '');
			}
		}
		if(pageName == 'body-view-customer-details-shipping'){
			if(jsonData != null){
				$('#vCust_shCity').val(jsonData.city);
				updateStateCountryComboById(countryList, vCust_shCountryName, jsonData.countryId);			
				loadCustShippingState(vCust_shCountryName, vCust_shStateName, jsonData.stateId);
			}else{
				$('#vCust_shCity').val('');
				updateStateCountryComboById(countryList, vCust_shCountryName, '');			
				loadCustShippingState(vCust_shCountryName, vCust_shStateName, '');
			}
		}
		if(pageName == 'body-add-new-customer-billing'){
			if(jsonData != null){
				$('#addCust_blCity').val(jsonData.city);
				updateStateCountryCombo(countryList, addCust_blCountryName, jsonData.countryId);
				loadStateAddCustomer(addCust_blCountryName, addCust_blStateName, jsonData.stateId);
			}else{
				$('#addCust_blCity').val('');
				updateStateCountryCombo(countryList, addCust_blCountryName, '');
				loadStateAddCustomer(addCust_blCountryName, addCust_blStateName, '');
			}
		}
		if(pageName == 'body-add-new-customer-shipping'){
			if(jsonData != null){
				$('#addCust_shCity').val(jsonData.city);
				updateStateCountryCombo(countryList, addCust_shCountryName, jsonData.countryId);
				loadStateAddCustomer(addCust_shCountryName, addCust_shStateName, jsonData.stateId);
			}else{
				$('#addCust_shCity').val('');
				updateStateCountryCombo(countryList, addCust_shCountryName, '');
				loadStateAddCustomer(addCust_shCountryName, addCust_shStateName, '');
			}
		}
		if(pageName == 'body-admin-fedex-label-billing'){
			if(jsonData != null){
				$('#manualFedex_blCity').val(jsonData.city);
				updateStateCountryCombo(countryList, manualFedex_blCountryName, jsonData.countryId);
				loadStateCountryManualFedex(manualFedex_blCountryName, manualFedex_blStateName, jsonData.stateId);
			}else{
				$('#manualFedex_blCity').val('');
				updateStateCountryCombo(countryList, manualFedex_blCountryName, '');
				loadStateCountryManualFedex(manualFedex_blCountryName, manualFedex_blStateName, '');
			}
		}
		if(pageName == 'body-admin-fedex-label-shipping'){
			if(jsonData != null){
				$('#manualFedex_shCity').val(jsonData.city);
				updateStateCountryCombo(countryList, manualFedex_shCountryName, jsonData.countryId);
				loadStateCountryManualFedex(manualFedex_shCountryName, manualFedex_shStateName, jsonData.stateId);
			}else{
				$('#manualFedex_shCity').val('');
				updateStateCountryCombo(countryList, manualFedex_shCountryName, '');
				loadStateCountryManualFedex(manualFedex_shCountryName, manualFedex_shStateName, '');
			}
		}
	}
	