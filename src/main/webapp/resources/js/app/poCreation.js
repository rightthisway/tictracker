	
	function setValuesForCreatePO(jsonData){
		updateCombo(jsonData.shippingMethods,cPO_shippingMethod,'');
		if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#cPO_successDiv').hide();					
			$('#cPO_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#cPO_errorDiv').hide();
			$('#cPO_errorMsg').text('');
		}
		cPOCustomerResetFilters();
		//getCreatePOCustomerGridData(0);
		resetCustomerInfo();
		$("#cPO_eventSearchValue").val('');
		$('#cPO_totalQty').val('');
		$('#cPO_totalPrice').val('');
		$('#cPO_myTableBody').empty();
		
		$('#eventGridDiv').hide();
		$('#cPO_addTicketDiv').hide();
		
		$('#cPO_myTable').hide();
		$('#cPO_myTableBody').hide();
		$('#cPO_poInfo').hide();
		
		$("input[type=radio][name=paymentType]:checked").prop('checked',false);
		$('#cPO_cardInfo').hide();
		$('#cPO_cardInfo input').val('');
		$('#cPO_accountInfo').hide();
		$('#cPO_accountInfo input').val('');
		$('#cPO_chequeInfo').hide();
		$('#cPO_chequeInfo input').val('');
		$('#cPO_paymentDate').val('');
		$('#cPO_paymentNote').val('');
		
		$('#cPO_consignmentPoDiv').hide();
		$('#cPO_paymentDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
				
		cPOShowHideFedexInfo();
		
		$('.searchcontrol').keypress(function (event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		 if(keyCode == 13)  // the enter key code
		  {
			 getCreatePOEventGridData(0);
			 return false;
		  }
		});
		
		$('#cPO_eventSearchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			 if(keyCode == 13)  // the enter key code
			  {
				 getCreatePOEventGridData(0);
				 return false;  
			  }
		});	
	}
	
	function addTicketInfo(){
		$('#cPO_myTable').show();
		$('#cPO_myTableBody').show();
		$('#cPO_poInfo').show();
		getEventInfoForPO();
	}
	
	function cPOTypeCheck(){
		if($('#cPO_poTypeCheck').is(':checked')){
			$('#cPO_poType').val("CONSIGNMENT");
			$('#cPO_consignmentPoDiv').show();
		}else{
			$('#cPO_poType').val("REGULAR");
			$('#cPO_consignmentPoDiv').hide();
		}
	}
	
	//function to sum the entered quantity
	function sumQty(qty){
		var sum = 0;
		$('.cPO_qty').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseInt($(this).val());
			}
		});
		$('#cPO_totalQty').val(sum);
	}
	
	//function to sum the entered price
	function sumPrice(price){
		var sum = 0;
		$('.cPO_price').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseFloat($(this).val());
			}
		});
		$('#cPO_totalPrice').val(sum);
	}	
	
	function cPOShowHideFedexInfo(){
		if(($('#cPO_shippingMethod :selected').text() == "fedex") || ($('#cPO_shippingMethod :selected').text() == "FedEx")){
			$('#cPO_trackingInfo').show();
		}else{
			$('#cPO_trackingInfo').hide();
		}
	}
	
	function cPOPaymentType(payType){
		var paymentType = $("input[type=radio][name=paymentType]:checked").val();
		if (paymentType == 'card') {
			$('#cPO_cardInfo').show();
			$('#cPO_accountInfo').hide();
			$('#cPO_chequeInfo').hide();			
		}else if (paymentType == 'account') {
			$('#cPO_chequeInfo').hide();           
			$('#cPO_cardInfo').hide();
			$('#cPO_accountInfo').show();
		}else if (paymentType == 'cheque') {
			$('#cPO_cardInfo').hide();
			$('#cPO_accountInfo').hide();
			$('#cPO_chequeInfo').show();
		}
	}
	
	function addCustomerMsg(){
		jAlert("Customer Added successfully.");
	}
	
	//Save purchase order
	function savePurchaseOrder(){
		var isValid = true;
		var paymentType = $("input[type=radio][name=paymentType]:checked").val();		
		if($('#cPO_shippingMethod :selected').text() != "FedEx"){
			$('#cPO_trackingNo').val("");
		}
		$('.section').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Section.");
			 return;
		}
		$('.rowValue').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Row.");
			 return;
		}
		$('.seatLow').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Seat Low.");
			 return;
		}
		$('.seatHigh').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Seat High.");
			 return;
		}
		$('.cPO_qty').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;				
			}
		});
		if(!isValid){
			alert("Please add Quantity.");
			return;
		}		
		$('.cPO_price').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
			if(isNaN($(this).val())){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Price.");
			 return;
		}
		if($('#cPO_shippingMethod').val() < 0){
			alert("Please select Shipping method.");
			return;
		}
		if(paymentType=='card'){
			if(($('#cardNo').val()!='' && $('#cardNo').val().length !=4) ){
				alert("Please enter Card last 4 digit only");
				return;
			}
		}else if(paymentType=='account'){
			if(($('#cPO_accountNo').val()!='' && $('#cPO_accountNo').val().length !=4) ){
				alert("Please enter Account No. last 4 digit only");
				return;
			}
			if(($('#cPO_routingNo').val()!='' && $('#cPO_routingNo').val().length !=4)){
				alert("Please enter Routing No. last 4 digit only");
				return;
			}
		}
		else if(paymentType=='cheque'){
			if(($('#cPO_accountChequeNo').val()!='' && $('#cPO_accountChequeNo').val().length !=4) ){
				alert("Please enter Account No. last 4 digit only");
				return;
			}
			if(($('#cPO_routingChequeNo').val()!='' && $('#cPO_routingChequeNo').val().length !=4)){
				alert("Please enter Routing No. last 4 digit only");
				return;
			}
		}
		var myTable = document.getElementById("cPO_myTable");
	    var currentIndex = myTable.rows.length;
		if(currentIndex <= 1){
			isValid = false;
		}
		if(!isValid){
			 alert("Please add atleast 1 ticket.");
			 return;
		}
		if($('#cPO_totalQty').val() <= 0){
			alert("Total quantity not less than 0.");
			return;
		}
		if($('#cPO_totalPrice').val() <= 0){
			alert("Total price not less than 0.");
			return;
		}
		$('#cPO_action').val('savePO');		
		//$('#cPO_purchaseOrderForm').submit();
		$.ajax({
			url : "/PurchaseOrder/SavePO",
			type : "post",
			dataType: "json",
			data : $("#cPO_purchaseOrderForm").serialize(),
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				/*if(jsonData.successMessage != null){
					$('#cPO_successDiv').show();					
					$('#cPO_successMsg').text(jsonData.successMessage);
				}
				if(jsonData.errorMessage != null){
					$('#cPO_errorDiv').show();
					$('#cPO_errorMsg').text(jsonData.errorMessage);
				}*/
				if(jsonData.status == 1){
					$('#create-po').modal('hide');
					getPOGridData(0);
				}
				if(jsonData.purchaseOrderId != null && jsonData.purchaseOrderId != ""){
					jAlert("Order Created Successfully. Purchase Order Id is : "+jsonData.purchaseOrderId);					
				}else{
					jAlert(jsonData.msg);
				}
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			} 
		});
	}
	
	//PO cancel action
	function cancelAction(){
		$('#create-po').modal('hide');
	}
	
	function cPOCustomerResetFilters(){
		createPOCustomerGridSearchString='';
		createPOCustomerGridColumnFilters = {};
		getCreatePOCustomerGridData(0);
	}
	
	function cPOEventResetFilters(){
		createPOEventGridSearchString='';
		createPOEventGridColumnFilters = {};
		getCreatePOEventGridData(0);
	}
	
	function searchEventData(){
		cPOEventResetFilters();
	}
	
	function getCreatePOCustomerGridData(pageNo) {
		var searchValue = $("#searchValue").val();
		$.ajax({
			url : "/Client/ManageDetails.json",
			type : "post",
			data : $("#customerSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+createPOCustomerGridSearchString,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				createPOCustomerPagingInfo = jsonData.pagingInfo;
				createPOCustomerGridValues(jsonData.customers);
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	
	var createPOCustomerPagingInfo;
	var createPOCustomerGrid;
	var createPOCustomerDataView;
	var createPOCustomerData=[];
	var createPOCustomerGridSearchString='';
	var createPOCustomerGridColumnFilters = {};
	var createPOCustomerGridColumns = [
		   {id:"customerType", name:"Customer Type", field: "customerType", sortable: true, width: 50},
		   {id:"firstName", name:"First Name", field: "firstName", sortable: true, width: 100},
		   {id:"lastName", name:"Last Name", field: "lastName", sortable: true, width: 100},
		   {id:"email", name:"Email", field: "email", sortable: true, width: 140},
		   {id:"productType", name:"Product Type", field:"productType", sortable: true, width: 120},
		   {id:"client", name:"Client", field: "client", sortable: true},
		   {id:"broker", name:"Broker", field: "broker", sortable: true},
		   {id:"street1", name:"Street1", field: "street1", sortable: true},
		   {id:"street2", name:"Street2", field: "street2", sortable: true},
		   {id:"city", name:"City", field: "city", sortable: true},
		   {id:"state", name:"State", field: "state", sortable: true},
		   {id:"country", name:"Country", field: "country", sortable: true},
		   {id:"zip", name:"Zip", field: "zip", sortable: true},
		   {id:"phone", name:"Phone", field: "phone", width:100, sortable: true},
		   {id: "editCol", field:"editCol", name:"Edit Customer", width:10, formatter:createPOCustomerEditFormatter}
		  ];
	              
	var createPOCustomerOptions = {
			enableCellNavigation : true,
			forceFitColumns : true,
			multiSelect: false,
			topPanelHeight : 25,
			showHeaderRow: true,
			headerRowHeight: 30,
			explicitInitialization: true
		};
		var createPOCustomerGridSortcol = "customerName";
		var createPOCustomerGridSortdir = 1;
		var percentCompleteThreshold = 0;

		/*	
		function createPOCustomerGridFilter(item, args) {
			var x= item["customerName"];
			if (args.createPOCustomerGridSearchString  != ""
					&& x.indexOf(args.createPOCustomerGridSearchString) == -1) {
				
				if (typeof x === 'string' || x instanceof String) {
					if(x.toLowerCase().indexOf(args.createPOCustomerGridSearchString.toLowerCase()) == -1) {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;
		}*/
		
		function createPOCustomerEditFormatter(row,cell,value,columnDef,dataContext){  
		    var button = "<img class='editClickableImage' src='../resources/images/ico-edit.gif' id='"+ dataContext.customerId +"'/>";
		    return button;
		}
		
		$('.editClickableImage').live('click', function(){
		    var me = $(this), id = me.attr('id');
		    var delFlag = editCustomer(id);
		});

		function createPOCustomerGridComparer(a, b) {
			var x = a[createPOCustomerGridSortcol], y = b[createPOCustomerGridSortcol];
			if(!isNaN(x)){
			   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
			}
			if(x == '' || x == null) {
				return 1;
			} else if(y == '' || y == null) {
				return -1;
			}
			if(x.indexOf('/') > 0 && x.length==10){
				return commonDateComparator(x,y);
			}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
				return commonDateTimeComparator(x,y);
			}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
				 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
			}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
				return x.toLowerCase().localeCompare(y.toLowerCase());	
			} else {
				return (x == y ? 0 : (x > y ? 1 : -1));	
			}
		}
		
		/*function createPOCustomerGridToggleFilterRow() {
			createPOCustomerGrid.setTopPanelVisibility(!createPOCustomerGrid.getOptions().showTopPanel);
		}
		
		//$(".grid-header .ui-icon").addClass("ui-state-default-sg ui-corner-all")
		$("#createPOCustomer_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
				.mouseover(function(e) {
					$(e.target).addClass("ui-state-hover")
				}).mouseout(function(e) {
					$(e.target).removeClass("ui-state-hover")
				});
		*/
		
		function createPOCustomerGridValues(jsonData) {
			$("div#divLoading").addClass('show');
			createPOCustomerData = [];
			for (var i = 0; i < jsonData.length; i++) {
				var  data= jsonData[i]; 
				var d = (createPOCustomerData[i] = {});
				d["id"] = i;
				d["customerId"] = data.customerId;
				d["bAddressId"] = data.billingAddressId;
				d["firstName"] = data.customerName;
				d["lastName"] = data.lastName;
				d["customerType"] = data.customerType;
				d["client"] = data.client;
				d["broker"] = data.broker;
				d["email"] = data.customerEmail;
				d["productType"] = data.productType;
				d["street1"] = data.addressLine1;	
				d["street2"] = data.addressLine2;
				d["city"] = data.city;
				d["state"] = data.state;
				d["country"] = data.country;
				d["zip"] = data.zipCode;
				d["phone"] = data.phone;
			}

			createPOCustomerDataView = new Slick.Data.DataView();
			createPOCustomerGrid = new Slick.Grid("#createPOCustomer_grid", createPOCustomerDataView, createPOCustomerGridColumns, createPOCustomerOptions);
			createPOCustomerGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
			createPOCustomerGrid.setSelectionModel(new Slick.RowSelectionModel());
			if(createPOCustomerPagingInfo!=null){
				var createPOCustomerGridPager = new Slick.Controls.Pager(createPOCustomerDataView, createPOCustomerGrid, $("#createPOCustomer_pager"),createPOCustomerPagingInfo);
			}
			
			var createPOCustomerGridColumnPicker = new Slick.Controls.ColumnPicker(createPOCustomerGridColumns, createPOCustomerGrid,
					createPOCustomerOptions);

			// move the filter panel defined in a hidden div into createPOCustomerGrid top panel
			//$("#createPOCustomer_inlineFilterPanel").appendTo(createPOCustomerGrid.getTopPanel()).show();

			createPOCustomerGrid.onSort.subscribe(function(e, args) {
				createPOCustomerGridSortdir = args.sortAsc ? 1 : -1;
				createPOCustomerGridSortcol = args.sortCol.field;
				if ($.browser.msie && $.browser.version <= 8) {
					createPOCustomerDataView.fastSort(createPOCustomerGridSortcol, args.sortAsc);
				} else {
					createPOCustomerDataView.sort(createPOCustomerGridComparer, args.sortAsc);
				}
			});
			
			// wire up model customers to drive the createPOCustomerGrid
			createPOCustomerDataView.onRowCountChanged.subscribe(function(e, args) {
				createPOCustomerGrid.updateRowCount();
				createPOCustomerGrid.render();
			});
			createPOCustomerDataView.onRowsChanged.subscribe(function(e, args) {
				createPOCustomerGrid.invalidateRows(args.rows);
				createPOCustomerGrid.render();
			});
			
			$(createPOCustomerGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
			 var keyCode = (e.keyCode ? e.keyCode : e.which);
				createPOCustomerGridSearchString='';
				var columnId = $(this).data("columnId");
				if (columnId != null) {
					createPOCustomerGridColumnFilters[columnId] = $.trim($(this).val());
					if(keyCode == 13) {
						for (var columnId in createPOCustomerGridColumnFilters) {
						  if (columnId !== undefined && createPOCustomerGridColumnFilters[columnId] !== "") {
							  createPOCustomerGridSearchString += columnId + ":" +createPOCustomerGridColumnFilters[columnId]+",";
						  }
						}
						getCreatePOCustomerGridData(0);
					}
				}			 
			});
			createPOCustomerGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
				$(args.node).empty();
				if(args.column.id.indexOf('checkbox') == -1){
					if(args.column.id != 'editCol'){
						$("<input type='text'>")
					   .data("columnId", args.column.id)
					   .val(createPOCustomerGridColumnFilters[args.column.id])
					   .appendTo(args.node);
					}
				}
			});
			createPOCustomerGrid.init();
				
			/*
			// wire up the search textbox to apply the filter to the model
			$("#createPOCustomerGridSearch").keyup(function(e) {
				Slick.GlobalEditorLock.cancelCurrentEdit();
				// clear on Esc
				if (e.which == 27) {
					this.value = "";
				}
				createPOCustomerGridSearchString = this.value;
				updateCreatePOCustomerGridFilter();
			});
			function updateCreatePOCustomerGridFilter() {
				createPOCustomerDataView.setFilterArgs({
					createPOCustomerGridSearchString : createPOCustomerGridSearchString
				});
				createPOCustomerDataView.refresh();
			}*/
			
			// initialize the model after all the customers have been hooked up
			createPOCustomerDataView.beginUpdate();
			createPOCustomerDataView.setItems(createPOCustomerData);
			/*createPOCustomerDataView.setFilterArgs({
				percentCompleteThreshold : percentCompleteThreshold,
				createPOCustomerGridSearchString : createPOCustomerGridSearchString
			});
			createPOCustomerDataView.setFilter(createPOCustomerGridFilter);*/
			
			createPOCustomerDataView.endUpdate();
			createPOCustomerDataView.syncGridSelection(createPOCustomerGrid, true);
			$("#gridContainer").resizable();
			
			var createPOCustomerGridRowIndex;
			createPOCustomerGrid.onSelectedRowsChanged.subscribe(function() { 
				var tempCreatePOCustomerGridRowIndex = createPOCustomerGrid.getSelectedRows([0])[0];
				if (tempCreatePOCustomerGridRowIndex != createPOCustomerGridRowIndex) {
					createPOCustomerGridRowIndex = tempCreatePOCustomerGridRowIndex;
					resetCustomerInfo();
					setSelectedCustomerInfo(createPOCustomerGridRowIndex);					
				}
			});
			$("div#divLoading").removeClass('show');
		}
	
		
	function getCreatePOEventGridData(pageNo) {
		$('#eventGridDiv').show();
		var searchValue = $("#cPO_eventSearchValue").val();
		$.ajax({			  
			url : "/GetEventDetails",
			type : "post",
			data : $("#eventSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+createPOEventGridSearchString,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				createPOEventPagingInfo = jsonData.eventPagingInfo;
				createPOEventGridValues(jsonData.events);
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	
	var createPOEventPagingInfo;
	var createPOEventDataView;
	var createPOEventGrid;
	var createPOEventData = [];
	var createPOEventGridSearchString='';
	var createPOEventGridColumnFilters = {};
	var createPOEventGridColumns = [ 
	{
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		sortable : true
	}, {
		id: "country", 
		name: "Country", 
		field: "country", 
		sortable: true
	}, {
		id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		sortable: true
	},{
		id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		sortable: true
	} ];

	var createPOEventOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var createPOEventGridSortcol = "eventName";
	var createPOEventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromEventGrid(id) {
		createPOEventDataView.deleteItem(id);
		createPOEventGrid.invalidate();
	}
	
	/*function createPOEventGridFilter(item, args) {
		var x= item["eventName"];
		if (args.createPOEventGridSearchString  != ""
				&& x.indexOf(args.createPOEventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.createPOEventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}*/
	
	function createPOEventGridComparer(a, b) {
		var x = a[createPOEventGridSortcol], y = b[createPOEventGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*function createPOEventGridToggleFilterRow() {
		createPOEventGrid.setTopPanelVisibility(!createPOEventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
	$("#createPOEvent_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
			$(e.target).addClass("ui-state-hover");
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover");
	});	*/
		
	function createPOEventGridValues(jsonData) {
		createPOEventData = [];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i]; 
			var d = (createPOEventData[i] = {});
			d["id"] = i;
			d["eventID"] = data.eventId;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDateStr;
			d["eventTime"] = data.eventTimeStr;
			d["venue"] = data.building;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["grandChildCategory"] = data.grandChildCategoryName;
			d["childCategory"] = data.childCategoryName;
			d["parentCategory"] = data.parentCategoryName;
		}

		createPOEventDataView = new Slick.Data.DataView();
		createPOEventGrid = new Slick.Grid("#createPOEvent_grid", createPOEventDataView, createPOEventGridColumns, createPOEventOptions);
		createPOEventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		createPOEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(createPOEventPagingInfo!=null){
			var createPOEventGridPager = new Slick.Controls.Pager(createPOEventDataView, createPOEventGrid, $("#createPOEvent_pager"),createPOEventPagingInfo);
		}
		
		var createPOEventGridColumnPicker = new Slick.Controls.ColumnPicker(createPOEventGridColumns, createPOEventGrid,
				createPOEventOptions);

		// move the filter panel defined in a hidden div into createPOEventGrid top panel
		//$("#createPOEvent_inlineFilterPanel").appendTo(createPOEventGrid.getTopPanel()).show();

		/*createPOEventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < createPOEventDataView.getLength(); i++) {
				rows.push(i);
			}
			createPOEventGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		createPOEventGrid.onSort.subscribe(function(e, args) {
			createPOEventGridSortdir = args.sortAsc ? 1 : -1;
			createPOEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				createPOEventDataView.fastSort(createPOEventGridSortcol, args.sortAsc);
			} else {
				createPOEventDataView.sort(createPOEventGridComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the createPOEventGrid
		createPOEventDataView.onRowCountChanged.subscribe(function(e, args) {
			createPOEventGrid.updateRowCount();
			createPOEventGrid.render();
		});
		createPOEventDataView.onRowsChanged.subscribe(function(e, args) {
			createPOEventGrid.invalidateRows(args.rows);
			createPOEventGrid.render();
		});
		
		$(createPOEventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			createPOEventGridSearchString='';
			var columnId = $(this).data("columnId");
			if (columnId != null) {
				createPOEventGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in createPOEventGridColumnFilters) {
					  if (columnId !== undefined && createPOEventGridColumnFilters[columnId] !== "") {
						  createPOEventGridSearchString += columnId + ":" +createPOEventGridColumnFilters[columnId]+",";
					  }
					}
					getCreatePOEventGridData(0);
				}
			}		 
		});
		createPOEventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(createPOEventGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(createPOEventGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(createPOEventGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}				
		});
		createPOEventGrid.init();
			
		/*
		// wire up the search textbox to apply the filter to the model
		$("#createPOEventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			createPOEventGridSearchString = this.value;
			updateCreatePOEventGridFilter();
		});
		function updateCreatePOEventGridFilter() {
			createPOEventDataView.setFilterArgs({
				createPOEventGridSearchString : createPOEventGridSearchString
			});
			createPOEventDataView.refresh();
		}*/
		
		// initialize the model after all the events have been hooked up
		createPOEventDataView.beginUpdate();
		createPOEventDataView.setItems(createPOEventData);
		/*createPOEventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			createPOEventGridSearchString : createPOEventGridSearchString
		});
		createPOEventDataView.setFilter(createPOEventGridFilter);*/
		
		createPOEventDataView.endUpdate();
		createPOEventDataView.syncGridSelection(createPOEventGrid, true);
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
		
		/* var createPoEventGridRowIndex;
		createPOEventGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempCreatePoEventGridRowIndex = createPOEventGrid.getSelectedRows([0])[0];
			if (tempCreatePoEventGridRowIndex != createPoEventGridRowIndex) {
				createPoEventGridRowIndex = tempCreatePoEventGridRowIndex;
				 var eventId =createPOEventGrid.getDataItem(tempCreatePoEventGridRowIndex).id;
				//jAlert(eventId);
				//getCategoryTicketGroupsforEvent(eventId);
			}
		}); */
		
		createPOEventGrid.onSelectedRowsChanged.subscribe(function() { 
			$('#cPO_addTicketDiv').show();
		});
	}
		
	/*function pagingControl(move,id){
		if(id=='customer_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(createPOCustomerPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(createPOCustomerPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(createPOCustomerPagingInfo.pageNum)-1;
			}
			getCreatePOCustomerGridData(pageNo);
		}else if(id=='event_pager'){
			var pageNo = 0;
			if(move == 'FIRST'){
				pageNo = 0;
			}else if(move == 'LAST'){
				pageNo = parseInt(createPOEventPagingInfo.totalPages)-1;
			}else if(move == 'NEXT'){
				pageNo = parseInt(createPOEventPagingInfo.pageNum) +1;
			}else if(move == 'PREV'){
				pageNo = parseInt(createPOEventPagingInfo.pageNum)-1;
			}
			getCreatePOEventGridData(pageNo);
		}		
	}*/
	
	function editCustomer(custId){
		if(custId == null){
			jAlert("There is something wrong.Please try again.");
		}else{
			getCustomerInfoForEdit(custId);
			/*var url = "/Client/ViewCustomer?custId="+ custId;
			popupCenter(url, "Edit Customer Details", "1000", "800");*/
		}
	}	
	
	function resetCustomerInfo(){
		$("#cPO_customerNameDiv").empty();
		$("#cPO_emailDiv").empty();
		$("#cPO_phoneDiv").empty();
		$("#cPO_addressLineDiv").empty();
		$("#cPO_countryDiv").empty();
		$("#cPO_stateDiv").empty();
		$("#cPO_cityDiv").empty();
		$("#cPO_zipCodeDiv").empty();
		
		$('#shAddressLine1Div').empty();
		$('#shAddressLine2Div').empty();
		$('#shPhoneDiv').empty();
		$('#shCountryDiv').empty();
		$('#shStateDiv').empty();
		$('#shCityDiv').empty();
		$('#shZipCodeDiv').empty();
	}
		
	//Get selected event on button click
	function setSelectedCustomerInfo(customerGridIndex) {
		var address=createPOCustomerGrid.getDataItem(customerGridIndex).street1;
		var strret2 = createPOCustomerGrid.getDataItem(customerGridIndex).street2;
		if(address==undefined){
			address='';
		}
		if(strret2==undefined){
			strret2='';
		}
		$("#cPO_eventInfo").show();
		 $("#cPO_customerId").val(createPOCustomerGrid.getDataItem(customerGridIndex).customerId);
			$("#cPO_customerNameDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).firstName);
			$("#cPO_emailDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).email);
			$("#cPO_phoneDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).phone);
			if(strret2!=null && strret2 != '') {
				address = address +','+strret2;
			}
			$("#cPO_addressLineDiv").text(address);
			$("#cPO_countryDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).country);
			$("#cPO_stateDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).state);
			$("#cPO_cityDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).city);
			$("#cPO_zipCodeDiv").text(createPOCustomerGrid.getDataItem(customerGridIndex).zip);
		}
	
	
	//Get selected event on button click
	function getEventInfoForPO(){		
		var temprEventRowIndex = createPOEventGrid.getSelectedRows([0])[0];
		if(temprEventRowIndex == null){
			jAlert("Please select event");
		}{
		var eventId = createPOEventGrid.getDataItem(temprEventRowIndex).eventID;
		var eventName = createPOEventGrid.getDataItem(temprEventRowIndex).eventName;
		var eventDate = createPOEventGrid.getDataItem(temprEventRowIndex).eventDate;
		var venue = createPOEventGrid.getDataItem(temprEventRowIndex).eventDate;
		var eventTime = createPOEventGrid.getDataItem(temprEventRowIndex).eventTime;
		
		//Add dynamic rows to table for selected event
		var myTable = document.getElementById("cPO_myTableBody");
		var currentIndex = myTable.rows.length;
		var currentRow = myTable.insertRow(-1);

		var input1 = document.createElement("input");
		input1.setAttribute("type", "hidden");
		input1.setAttribute("name", "rowId_" + currentIndex);
		input1.setAttribute("value", currentIndex);
		input1.setAttribute("class","rowId");
		//input1.setAttribute("id","display");

		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "eventId_" +currentIndex);
		input.setAttribute("value", eventId);
		input.setAttribute("id","display");

		var eventNameLabel = document.createElement("Label");
		eventNameLabel.setAttribute("for","event_"+eventId);
		eventNameLabel.innerHTML = eventName+" "+eventDate+" "+eventTime;
		
		/* var eventDateLabel = document.createElement("Label");
		eventDateLabel.setAttribute("for","event_"+eventId);
		eventDateLabel.innerHTML = eventDate;

		var venueLabel = document.createElement("Label");
		venueLabel.setAttribute("for","event_"+eventId);
		venueLabel.innerHTML = venue; */
		
		var sectionBox = document.createElement("input");
		sectionBox.setAttribute("name", "section_" +currentIndex);
		sectionBox.setAttribute("id","section_"+currentIndex);
		sectionBox.setAttribute("class","section");
		
		var row = document.createElement("input");
		row.setAttribute("name", "row_" +currentIndex);
		row.setAttribute("id","row_"+currentIndex);
		row.setAttribute("class","rowValue");
		
		var seatLow = document.createElement("input");
		seatLow.setAttribute("name", "seatLow_" +currentIndex);
		seatLow.setAttribute("id","seatLow_"+currentIndex);
		seatLow.setAttribute("class","seatLow");
		
		var seatHigh = document.createElement("input");
		seatHigh.setAttribute("name", "seatHigh_" +currentIndex);
		seatHigh.setAttribute("id","seatHigh_"+currentIndex);
		seatHigh.setAttribute("class","seatHigh");
		
		var qty = document.createElement("input");
		qty.setAttribute("type", "number");
		qty.setAttribute("name", "qty_" +currentIndex);
		qty.setAttribute("id","qty_"+currentIndex);
		qty.setAttribute("class","cPO_qty");
		qty.setAttribute("onkeyup","sumQty(this);");
		
		var price = document.createElement("input");
		//price.setAttribute("type", "number");
		price.setAttribute("name", "price_" +currentIndex);
		price.setAttribute("id","price_"+currentIndex);
		price.setAttribute("class","cPO_price");
		price.setAttribute("onkeyup","sumPrice(this);");
		
		var addRowBox = document.createElement("input");
		addRowBox.setAttribute("type", "button");
		addRowBox.setAttribute("value", "Remove");
		addRowBox.setAttribute("onclick", "removeField(this); sumQty(this); sumPrice(this);");
		addRowBox.setAttribute("class", "button");

		//currentCell = currentRow.insertCell(-1);
		//currentCell.appendChild(input1);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(input1);
		currentCell.appendChild(input);

		var currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(eventNameLabel);

		/* currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(eventDateLabel);

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(venueLabel); */

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(sectionBox);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(row);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(seatLow);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(seatHigh);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(qty);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(price);
		
		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(addRowBox);
		
		$(window).scrollTop(1100);
		}
		
		
		/*
		var footer = myTable.createTFoot();
		var row = footer.insertRow();
		var cell = row.insertCell();
		cell.innerHTML = "Total Qty";*/		
		
	}	

	function removeField(id){
		//jAlert('remove');
		var p=id.parentNode.parentNode;
   	 	p.parentNode.removeChild(p);
	}
	
	function updateCombo(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].shippingMethodName == listName){
					$(id).append("<option value="+jsonData[i].shippingMethodId+" selected>"+jsonData[i].shippingMethodName+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].shippingMethodId+">"+jsonData[i].shippingMethodName+"</option>");
				}
			}
		}
	}