	
	//Add New Customer
	var countryList = "";
	function setCustomerValues(jsonData){
		$('#add-new-customer2').modal('show');
		if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
			jAlert(jsonData.msg);										
		}
		/*if(jsonData.successMessage != null){
			$('#addCust_successDiv').show();
			$('#addCust_successMsg').text(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null){
			$('#addCust_errorDiv').show();
			$('#addCust_errorMsg').text(jsonData.errorMessage);
		}*/
		countryList = jsonData.countries;
		updateStateCountryCombo(jsonData.countries,addCust_blCountryName,'');
		updateStateCountryCombo(jsonData.countries,addCust_shCountryName,'');
	}
	
	function doCustomerValidations(){
		if($('#addCust_name').val()==''){
			jAlert("First Name is Mandatory.","Info");
			return false;
		}else if($('#addCust_lastName').val()==''){
			jAlert("Last Name is Mandatory.","Info");
			return false;
		}else if($('#addCust_email').val()==''){
			jAlert("Email is Mandatory.","Info");
			return false;
		}else if($('#addCust_phone').val()==''){
			jAlert("Phone is Mandatory.","Info");
			return false;
		}else if($('#addCust_blFirstName').val()==''){
			jAlert("Billing address - First Name is Mandatory.","Info");
			return false;
		}else if($('#addCust_blLastName').val()==''){
			jAlert("Billing address - Last Name is Mandatory.","Info");
			return false;
		}/*else if($('#addCust_blEmail').val()==''){
			jAlert("Billing address - Email is Mandatory.","Info");
			return false;
		}*/else if($('#addCust_blStreet1').val()==''){
			jAlert("Billing address - Street1 is Mandatory.","Info");
			return false;
		}else if($('#addCust_blCity').val()==''){
			jAlert("Billing address - City is Mandatory.","Info");
			return false;
		}else if($('#addCust_blCountryName').val() < 0){
			jAlert("Billing address - Country is Mandatory.","Info");
			return false;
		}else if($('#addCust_blStateName').val() < 0){
			jAlert("Billing address - State is Mandatory.","Info");
			return false;
		}else if($('#addCust_blZipCode').val()==''){
			jAlert("Billing address - ZipCode is Mandatory.","Info");
			return false;
		}else if($('#addCust_shFirstName').val()==''){
			jAlert("Shipping Address - First Name is Mandatory.","Info");
			return false;
		}else if($('#addCust_shLastName').val()==''){
			jAlert("Shipping Address - Last Name is Mandatory.","Info");
			return false;
		}/*else if($('#addCust_shEmail').val()==''){
			jAlert("Shipping Address - Email is Mandatory.","Info");
			return false;
		}*/else if($('#addCust_shStreet1').val()==''){
			jAlert("Shipping Address - Street1 is Mandatory.","Info");
			return false;
		}else if($('#addCust_shCity').val()==''){
			jAlert("Shipping Address - City is Mandatory.","Info");
			return false;
		}else if($('#addCust_shCountryName').val() < 0){
			jAlert("Shipping address - Country is Mandatory.","Info");
			return false;
		}else if($('#addCust_shStateName').val() < 0){
			jAlert("Shipping address - State is Mandatory.","Info");
			return false;
		}else if($('#addCust_shZipCode').val()==''){
			jAlert("Shipping Address - ZipCode is Mandatory.","Info");
			return false;
		}
		
		$.ajax({
					url : "/CheckCustomer",
					type : "get",
					data : "userName="+ $("#addCust_email").val(),
					/* async : false, */
					success : function(response){
						if(response.msg == "true"){
							//$("#addCustomerForm").submit();							
							$.ajax({
								url : "/Client/AddCustomer.json",
								type : "post",
								dataType : "json",
								data : $("#addCustomerForm").serialize(),
								success : function(res) {
									var jsonData = JSON.parse(JSON.stringify(res));
									if(jsonData.status == 1){}
									if(jsonData.msg != null && jsonData.msg != "" && jsonData.msg != undefined){
										jAlert(jsonData.msg);										
									}
									/*if(jsonData.successMessage != null){
										$('#addCust_successDiv').show();
										$('#addCust_successMsg').text(jsonData.successMessage);
									}
									if(jsonData.errorMessage != null){
										$('#addCust_errorDiv').show();
										$('#addCust_errorMsg').text(jsonData.errorMessage);
									}*/
								},
								error : function(error) {
									jAlert("Your login session is expired please refresh page and login again.", "Error");
									return false;
								}
							});
						}else{
							jAlert(response.msg,"Info");
							return false;
						}
					},
					error : function(error){
						jAlert("Your login session is expired please refresh page and login again.", "Error");
						return false;
					}
			});
		
	}
	
	//Copy billing address
	function copyBillingAddress(){
		var flag=false;
		if($("#addCust_isBilling").prop('checked') == true){			
			$("#addCust_shFirstName").val($("#addCust_blFirstName").val());
			$("#addCust_shLastName").val($("#addCust_blLastName").val());
			$("#addCust_shStreet1").val($("#addCust_blStreet1").val());
			$("#addCust_shStreet2").val($("#addCust_blStreet2").val());
			$("#addCust_shCountryName").val($("#addCust_blCountryName").val());
			loadStateAddCustomer(addCust_shCountryName,addCust_shStateName,$("#addCust_blStateName").val());
			$("#addCust_shCity").val($("#addCust_blCity").val());			
			$("#addCust_shZipCode").val($("#addCust_blZipCode").val());	
			
		}else{
			$("#addCust_shFirstName").val('');
			$("#addCust_shLastName").val('');
			$("#addCust_shStreet1").val('');
			$("#addCust_shStreet2").val('');
			updateStateCountryCombo(countryList,addCust_shCountryName,'');
			loadStateAddCustomer(addCust_shCountryName,addCust_shStateName,'');
			$("#addCust_shCity").val('');			
			$("#addCust_shZipCode").val('');	
		}		
	}
	
	function loadStateAddCustomer(countryId, stateId, findName){
		var countryId = $(countryId).val();
		if(countryId>0){
			$.ajax({
				url : "/GetStates",
				type : "post",
				dataType:"json",
				data : "countryId="+countryId,
				success : function(res){
					var jsonData = res.states; //JSON.parse(JSON.stringify(res));
					updateStateCountryCombo(jsonData,stateId,findName);
				}, error : function(error){
					jAlert("Your login session is expired please refresh page and login again.", "Error");
					return false;
				}
			});
		}else{
			$(stateId).empty();
			$(stateId).append("<option value='-1'>--select--</option>");
		}
	}
	
	function updateStateCountryCombo(jsonData,id,listName){
		$(id).empty();
		$(id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listName!='' && jsonData[i].id == listName){
					$(id).append("<option value="+jsonData[i].id+" selected>"+jsonData[i].name+"</option>");
				}else{
					$(id).append("<option value="+jsonData[i].id+">"+jsonData[i].name+"</option>");
				}
			}
		}
	}