

var custInvoiceTicketPagingInfo;
var custInvoiceTicketGrid;
var custInvoiceTicketDataView;
var custInvoiceTicketData=[];
var custInvoiceTicketColumn = [ /*shippingCheckboxSelector.getColumnDefinition(), 
                {id:"id", name:"ID", field: "id", sortable: true}, */
			   {id:"invoiceId", name:"Invoice ID", field: "invoiceId", sortable: true},
               {id:"fileName", name:"File Name", field: "fileName", sortable: true},
               {id:"fileType", name:"File Type", field: "fileType", sortable: true},
			   /*{id:"position", name:"Position", field: "position", sortable: true},*/
               {id:"view", name : "Download", field : "view", formatter : ticketDownloadFormatter}
              ];
              
var custInvoiceTicketOptions = {		  
	//editable: true,
	//enableAddRow: true,
	enableCellNavigation : true,
	//asyncEditorLoading: true,
	forceFitColumns : true,
	topPanelHeight : 25,
	//autoHeight: true
};

var sortcol = "invoiceId";
var sortdir = 1;
var percentCompleteThreshold = 0;
var searchString = "";

/* //filter option
function myFilter(item, args) {
	var x= item["shippingId"];
	if (args.searchString  != ""
			&& x.indexOf(args.searchString ) == -1) {
		
		if (typeof x === 'string' || x instanceof String) {
			if(x.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1) {
				return false;
			}
		} else {
			return false;
		}
	}
	return true;
}
*/

function custInvoiceTicketComparer(a, b) {
	  var x = a[sortcol], y = b[sortcol];
	  if(!isNaN(x)){
	  	return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
	  }
	  if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
	    if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
/*	
function shippingToggleFilterRow() {
	custInvoiceTicketGrid.setTopPanelVisibility(!custInvoiceTicketGrid.getOptions().showTopPanel);
}
	
//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$("#shipping_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all").mouseover(function(e) {
	$(e.target).addClass("ui-state-hover")}).mouseout(function(e) {
		$(e.target).removeClass("ui-state-hover")
});
*/

function createInvoiceTicketGridValues(jsonData, pageInfo) {
	custInvoiceTicketPagingInfo = pageInfo;
	custInvoiceTicketData=[];
	var i=0;
	if(jsonData!=null && jsonData.length > 0){
		for (var i = 0; i < jsonData.length; i++) {
			var  data = jsonData[i];
			var d = (custInvoiceTicketData[i] = {});
			d["id"] = data.id;
			d["invoiceId"] = data.invoiceId;
			d["fileName"] = data.fileName;
			d["fileType"] = data.fileType;
			d["position"] = data.position;
			d["view"] = data.downloadTicket;
		}
	}
	
	custInvoiceTicketDataView = new Slick.Data.DataView();
	custInvoiceTicketGrid = new Slick.Grid("#custInvoiceTicket_grid", custInvoiceTicketDataView, custInvoiceTicketColumn, custInvoiceTicketOptions);
	custInvoiceTicketGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
	custInvoiceTicketGrid.setSelectionModel(new Slick.RowSelectionModel());
	if(custInvoiceTicketPagingInfo != null){
		var custInvoiceTicketPager = new Slick.Controls.Pager(custInvoiceTicketDataView, custInvoiceTicketGrid, $("#custInvoiceTicket_pager"),custInvoiceTicketPagingInfo);
	}
	var custInvoiceTicketColumnPicker = new Slick.Controls.ColumnPicker(custInvoiceTicketColumn, custInvoiceTicketGrid, custInvoiceTicketOptions);
	  
	  custInvoiceTicketGrid.onKeyDown.subscribe(function (e) {
	    // select all rows on ctrl-a
	    if (e.which != 65 || !e.ctrlKey) {
	      return false;
	    }
	    var rows = [];
	    for (var i = 0; i < custInvoiceTicketDataView.getLength(); i++) {
	      rows.push(i);
	    }
	    custInvoiceTicketGrid.setSelectedRows(rows);
	    e.preventDefault();
	  });
	  
	  custInvoiceTicketGrid.onSort.subscribe(function (e, args) {
	    sortdir = args.sortAsc ? 1 : -1;
	    sortcol = args.sortCol.field;
	    if ($.browser.msie && $.browser.version <= 8) {
	      custInvoiceTicketDataView.fastSort(sortcol, args.sortAsc);
	    } else {
	      custInvoiceTicketDataView.sort(custInvoiceTicketComparer, args.sortAsc);
	    }
	  });
	  
	  // wire up model events to drive the custInvoiceTicketGrid
	  custInvoiceTicketDataView.onRowCountChanged.subscribe(function (e, args) {
	    custInvoiceTicketGrid.updateRowCount();
	    custInvoiceTicketGrid.render();
	  });
	  custInvoiceTicketDataView.onRowsChanged.subscribe(function (e, args) {
	    custInvoiceTicketGrid.invalidateRows(args.rows);
	    custInvoiceTicketGrid.render();
	  });
	  
	  custInvoiceTicketDataView.beginUpdate();
	  custInvoiceTicketDataView.setItems(custInvoiceTicketData);
	  
	  custInvoiceTicketDataView.endUpdate();
	  custInvoiceTicketDataView.syncGridSelection(custInvoiceTicketGrid, true);
	  custInvoiceTicketDataView.refresh();
	  $("#gridContainer").resizable();
	  //custInvoiceTicketGrid.resizeCanvas();
}	

/*function pagingControl(move,id){		
	if(id == "custInvoiceTicket_pager"){
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(custInvoiceTicketPagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(custInvoiceTicketPagingInfo.pageNum)-1;
		}
	}
}*/

function ticketDownloadFormatter(row, cell, value, columnDef, dataContext) {
	var downloadTicket = custInvoiceTicketGrid.getDataItem(row).view;
	var invId = custInvoiceTicketGrid.getDataItem(row).invoiceId;
	var fileTyp = custInvoiceTicketGrid.getDataItem(row).fileType;
	var position = custInvoiceTicketGrid.getDataItem(row).position;
	var link = '';
	if(downloadTicket == 'Yes'){
		link = "<img class='downloadClickableImage' style='height:17px;' src='../resources/images/viewPdf.png' onclick=downloadTicketFile("+invId+",'"+fileTyp+"',"+position+"); />";
	}
	return link;
}
	
function downloadTicketFile(invoiceId,fileType,position){
	 //var url = "/Accounting/DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;	 
	var url = apiServerUrl+"DownloadRealTix?invoiceId="+invoiceId+"&fileType="+fileType+"&position="+position;
	 $('#download-frame').attr('src', url);
}