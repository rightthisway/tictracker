
//$(document).ready(function(){
	function setValuesForEditPO(jsonData){
		$('#ePO_myTableBody').empty();
		$('#ePO_cardInfo').hide();
		$('#ePO_accountInfo').hide();
		$('#ePO_chequeInfo').hide();
		$("div#divLoading").addClass('show');
		$("#ePO_eventSearchValue").val('');
		$('#ePO_eventGridDiv').hide();
		$('#ePO_addTicketDiv').hide();
		$('#editBtnDiv').show();
		$('#closeBtnDiv').hide();
		/*if(jsonData.successMessage == undefined || jsonData.successMessage == 'undefined'){
			$('#ePO_successDiv').hide();					
			$('#ePO_successMsg').text('');
		}
		if(jsonData.errorMessage == undefined || jsonData.errorMessage == 'undefined'){
			$('#ePO_errorDiv').hide();
			$('#ePO_errorMsg').text('');
		}
		if(jsonData.successMessage != null && jsonData.successMessage != "" && jsonData.successMessage != undefined){
			//$('#ePO_successDiv').show();					
			//$('#ePO_successMsg').text(jsonData.successMessage);
			jAlert(jsonData.successMessage);
		}
		if(jsonData.errorMessage != null && jsonData.errorMessage != "" && jsonData.errorMessage != undefined){
			//$('#ePO_errorDiv').show();
			//$('#ePO_errorMsg').text(jsonData.errorMessage);
			$('#editBtnDiv').hide();
			$('#closeBtnDiv').show();
			jAlert(jsonData.errorMessage);
		}*/
		
		var ticketGroup = jsonData.ticketGroup;
		var purchaseOrder = jsonData.purchaseOrder;
		var payment = jsonData.payment;
		var customerInfo = jsonData.customerInfo;
		var customerAddress = jsonData.customerAddress;
		var shippingMethods = jsonData.shippingMethods;
		
		setEditPOCustomerDetails(customerInfo, customerAddress);
		setEditPOTicketGroups(ticketGroup);
		updateShippingCombo(shippingMethods, ePO_shippingMethod, purchaseOrder.poShippingMethod)
		$('#ePO_purchaseOrderId').val(purchaseOrder.poId);
		$('#ePO_customerId').val(purchaseOrder.poCustomerId);
		$('#ePO_trackingNo').val(purchaseOrder.poTrackingNo);
		$('#ePO_totalPrice').val(purchaseOrder.poTotal);
		$('#ePO_totalQty').val(purchaseOrder.poTicketCount);
		$('#ePO_externalPONo').val(purchaseOrder.poExternalPONo);		
		if(purchaseOrder.poTransactionOffice == 'Main'){
			$('#ePO_transactionOffice').append("<option value='Main' selected>Main</option>");
			$('#ePO_transactionOffice').append("<option value='Spec'>Spec</option>");
		}else{
			$('#ePO_transactionOffice').append("<option value='Main'>Main</option>");
			$('#ePO_transactionOffice').append("<option value='Spec' selected>Spec</option>");
		}
		
		var poType = purchaseOrder.poPurchaseOrderType;
		var paymentTypes = "";
		
		if(poType == 'CONSIGNMENT'){
			$('#ePO_poTypeCheck').attr('checked',true);
			$('#ePO_consignmentPoDiv').show();
			$('#ePO_consignmentPo').val(purchaseOrder.poConsignmentPONo);
		}else{
			$('#ePO_consignmentPoDiv').hide();
			$('#ePO_consignmentPo').val('');
		}
		
		$('#ePO_paymentDate').datepicker({
	        format: "mm/dd/yyyy",
			autoclose : true,
			orientation: "top",
			todayHighlight: true
	    });
		
		if(payment != null){
			if(payment.paymentType != '' && payment.paymentType != null){
				paymentTypes = payment.paymentType;
			}
			if(paymentTypes == 'card'){
				$('#radioCard').prop('checked',true);
				$('#ePO_cardInfo').show();
				$('#ePO_name').val(payment.paidName);
				if(payment.cardType == 'American Express'){
					$('#ePO_cardType').val('American Express').attr("selected","selected");
				}else if(payment.cardType == 'Visa'){
					$('#ePO_cardType').val('Visa').attr("selected","selected");
				}else if(payment.cardType == 'MasterCard'){
					$('#ePO_cardType').val('MasterCard').attr("selected","selected");
				}else if(payment.cardType == 'JCB'){
					$('#ePO_cardType').val('JCB').attr("selected","selected");
				}else if(payment.cardType == 'Discover'){
					$('#ePO_cardType').val('Discover').attr("selected","selected");
				}else if(payment.cardType == 'Diners Club'){
					$('#ePO_cardType').val('Diners Club').attr("selected","selected");
				}
				$('#ePO_cardNo').val(payment.cardLastFourDigit);
			}
			if(paymentTypes == 'account'){
				$('#radioAccount').prop('checked',true);
				$('#ePO_accountInfo').show();
				$('#ePO_accountName').val(payment.paidName);
				$('#ePO_routingNo').val(payment.routingLastFourDigit);
				$('#ePO_accountNo').val(payment.accountLastFourDigit);
			}
			if(paymentTypes == 'cheque'){
				$('#radioCheque').prop('checked',true);
				$('#ePO_chequeInfo').show();
				$('#ePO_chequeName').val(payment.paidName);
				$('#ePO_routingChequeNo').val(payment.routingLastFourDigit);
				$('#ePO_accountChequeNo').val(payment.accountLastFourDigit);
				$('#ePO_checkNo').val(payment.chequeNo);
			}
			$('#ePO_paymentDate').val(payment.paymentDate);
			if(payment.paymentStatus == 'Paid'){
				$('#ePO_paymentStatus').append("<option value='Paid' selected>Paid</option>");
				$('#ePO_paymentStatus').append("<option value='Pending'>Pending</option>");
			}else{
				$('#ePO_paymentStatus').append("<option value='Paid'>Paid</option>");
				$('#ePO_paymentStatus').append("<option value='Pending' selected>Pending</option>");
			}
			$('#ePO_paymentNote').val(payment.paymentNote);
		}
				
		$('#ePO_eventSearchValue').keypress(function (event) {
			var keyCode = (event.keyCode ? event.keyCode : event.which);
			if(keyCode == 13)  // the enter key code
			{
				getEditPOEventGridData(0); 
			}
		});
		
		editPOShowHideFedexInfo();				
	}
	
	function ePOAddTicketInfo(){
		$('#ePO_myTable').show();
		$('#ePO_myTableBody').show();
		getEventInfoForEditPO();
	}
	
	function ePOTypeCheck(){
		if($('#ePO_poTypeCheck').is(':checked')){
			$('#ePO_poType').val("CONSIGNMENT");
			$('#ePO_consignmentPoDiv').show();
		}else{
			$('#ePO_poType').val("REGULAR");
			$('#ePO_consignmentPoDiv').hide();
		}
	}
	
	function editPOShowHideFedexInfo(){
		if($('#ePO_shippingMethod :selected').text() == "FedEx"){
			$('#ePO_trackingInfo').show();
		}else{
			$('#ePO_trackingInfo').hide();
		}
	}
	
	function ePOPaymentType(payType){
		var paymentType = $("input[type=radio][name=paymentType]:checked").val();
		if (paymentType == 'card') {
			$('#ePO_cardInfo').show();
			$('#ePO_accountInfo').hide();
			$('#ePO_chequeInfo').hide();
		}else if (paymentType == 'account') {
			$('#ePO_chequeInfo').hide();           
			$('#ePO_cardInfo').hide();
			$('#ePO_accountInfo').show();
		}else if (paymentType == 'cheque') {
			$('#ePO_cardInfo').hide();
			$('#ePO_accountInfo').hide();
			$('#ePO_chequeInfo').show();
		}
	}
	
	function setEditPOCustomerDetails(customerInfo, customerAddress){
		if(customerInfo != null && customerInfo.customerName != null && customerInfo.customerName != '' && customerInfo.customerName != undefined){
			$('#ePO_customerName').text(customerInfo.customerName);
		}
		if(customerInfo != null && customerInfo.userName != null && customerInfo.userName != '' && customerInfo.userName != undefined){
			$('#ePO_userName').text(customerInfo.userName);
		}
		if(customerInfo != null && customerInfo.phone != null && customerInfo.phone != '' && customerInfo.phone != undefined){
			$('#ePO_phone').text(customerInfo.phone);
		}
		if(customerAddress != null && customerAddress.addressLine1 != null && customerAddress.addressLine1 != '' && customerAddress.addressLine1 != undefined){
			$('#ePO_addressLine1').text(customerAddress.addressLine1);
		}
		if(customerAddress != null && customerAddress.addressLine2 != null && customerAddress.addressLine2 != '' && customerAddress.addressLine2 != undefined){
			$('#ePO_addressLine2').text(customerAddress.addressLine2);
		}
		if(customerAddress != null && customerAddress.city != null && customerAddress.city != '' && customerAddress.city != undefined){
			$('#ePO_city').text(customerAddress.city);
		}
		if(customerAddress != null && customerAddress.state != null && customerAddress.state != '' && customerAddress.state != undefined){
			$('#ePO_state').text(customerAddress.state);
		}
		if(customerAddress != null && customerAddress.country != null && customerAddress.country != '' && customerAddress.country != undefined){
			$('#ePO_country').text(customerAddress.country);
		}
		if(customerAddress != null && customerAddress.zipCode != null && customerAddress.zipCode != '' && customerAddress.zipCode != undefined){
			$('#ePO_zipCode').text(customerAddress.zipCode);
		}
	}
	
	function setEditPOTicketGroups(jsonData){
		var ticketGroups = "";		
		if(jsonData != null && jsonData != ""){	
			var j=1;			
			for(var i=0; i<jsonData.length; i++){
				var data = jsonData[i];
				$('#ePO_myTableBody').append("<tr id='ePOtr_"+j+"'></tr>");
				ticketGroups = "";
				ticketGroups += "<td>";
				ticketGroups += "<input type='hidden' id='rowId_"+i+"' name='rowId_"+i+"' value="+data.tgEventId+" >";
				ticketGroups += "<input type='hidden' name='ticketGroup_"+i+"' value="+data.tgId+" >";
				ticketGroups += "</td><td>";
				ticketGroups += "<label name='event_"+data.tgId+"'>";
				ticketGroups += data.tgEventName+" "+data.tgEventDate+" "+data.tgEventTime+"</label>";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' class='section' name='section_"+i+"' value="+data.tgSection+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' class='rowValue' name='row_"+i+"' size='10' value="+data.tgRow+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' class='seatLow' name='seatLow_"+i+"' value="+data.tgSeatLow+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' class='seatHigh' name='seatHigh_"+i+"' value="+data.tgSeatHigh+" />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' name='qty_"+i+"' id='qty_"+i+"' class='ePO_qty' value="+data.tgQuantity+" onkeyup='editPOSumQty(this);' />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='text' name='price_"+i+"' id='price_"+i+"' class='ePO_price' value="+data.tgPrice+" onkeyup='editPOSumPrice(this);' />";
				ticketGroups += "</td><td>";
				ticketGroups += "<input type='button' value='Remove' onclick='removeField(this); editPOSumQty(this); editPOSumPrice(this);' class='button' />";
				ticketGroups += "</td>";
				//$('#ePO_myTable').html(ticketGroups);
				$('#ePOtr_'+j).html(ticketGroups);
				j++;
			}			
		}
	}
	
	function doEditPOValidation(){
		var isValid = true;
		if($('#ePO_shippingMethod :selected').text() != "FedEx"){
			$('#ePO_trackingNo').val("");
		}
		$('.section').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Section.");
			 return;
		}
		$('.rowValue').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Row.");
			 return;
		}
		$('.seatLow').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Seat Low.");
			 return;
		}
		$('.seatHigh').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
		}); 
		if(!isValid){
			 alert("Please add Seat High.");
			 return;
		}
		$('.ePO_qty').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;				
			}
		});
		if(!isValid){
			alert("Please add Quantity.");
			return;
		}		
		$('.ePO_price').each(function() {
			if($(this).val() == '' || $(this).val() == null){
				isValid =false;
			}
			if(isNaN($(this).val())){
				isValid =false;
			}
		});
		if(!isValid){
			 alert("Please add Price.");
			 return;
		} 
		if($('#ePO_shippingMethod').val() < 0){
			jAlert("Please select Shipping method.");
			return;
		}
		if($('#ePO_cardNo').val()!='' && $('#ePO_cardNo').val().length !=4){
			jAlert("Please enter Card last 4 digit only");
			return;
		}
		var myTable = document.getElementById("ePO_myTable");
	    var currentIndex = myTable.rows.length;
		if(currentIndex <= 1){
			isValid = false;
		}
		if(!isValid){
			 alert("Please add atleast 1 ticket to update.");
			 return;
		}
		if($('#ePO_totalQty').val() <= 0){
			jAlert("Total quantity not less than 0.");
			return;
		}
		if($('#ePO_totalPrice').val() <= 0){
			jAlert("Total price not less than 0.");
			return;
		}
		$("#ePO_action").val('editPOInfo');
		//$("#ePO_purchaseOrderForm").submit();
		$.ajax({
			url : "/Accounting/EditPurchaseOrder",
			type : "post",
			dataType: "json",
			data : $("#ePO_purchaseOrderForm").serialize(),
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				if(jsonData.status == 1){	
					$('#edit-po').modal('hide');
					getPOGridData(0);
				}
				jAlert(jsonData.msg);
				/*if(jsonData.successMessage != null && jsonData.successMessage != "" && jsonData.successMessage != undefined){
					//$('#ePO_successDiv').show();					
					//$('#ePO_successMsg').text(jsonData.successMessage);
					jAlert(jsonData.successMessage);
				}
				if(jsonData.errorMessage != null && jsonData.errorMessage != "" && jsonData.errorMessage != undefined){
					//$('#ePO_errorDiv').show();
					//$('#ePO_errorMsg').text(jsonData.errorMessage);
					jAlert(jsonData.errorMessage);
				}*/				
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			} 
		});		
	}
	
	function doVoidPO(){
		var myTable = document.getElementById("ePO_myTable");
	    var currentIndex = myTable.rows.length;
		if(currentIndex <= 1){			
			 jAlert("Please add atleast 1 ticket to void.");
			 return;
		}
		if($('#ePO_totalQty').val() <= 0){
			jAlert("Total quantity not less than 0.");
			return;
		}else{
			jConfirm("Do you want to void this PO ?","Confirm",function(r){
				if(r){
					if($('#ePO_shippingMethod :selected').text() != "FedEx"){
						$('#ePO_trackingNo').val("");
					}
					$("#ePO_action").val('voidPO');
					//$("#purchaseOrderForm").submit();
					$.ajax({
						url : "/Accounting/EditPurchaseOrder",
						type : "post",
						dataType: "json",
						data : $("#ePO_purchaseOrderForm").serialize(),
						success : function(res){
							var jsonData = JSON.parse(JSON.stringify(res));
							if(jsonData.status == 1){
								$('#edit-po').modal('hide');
								getPOGridData(0);
							}
							jAlert(jsonData.msg);
							/*if(jsonData.successMessage != null && jsonData.successMessage != "" && jsonData.successMessage != undefined){
								//$('#ePO_successDiv').show();					
								//$('#ePO_successMsg').text(jsonData.successMessage);
								jAlert(jsonData.successMessage);
							}
							if(jsonData.errorMessage != null && jsonData.errorMessage != "" && jsonData.errorMessage != undefined){
								//$('#ePO_errorDiv').show();
								//$('#ePO_errorMsg').text(jsonData.errorMessage);
								jAlert(jsonData.errorMessage);
							}*/
						}, error : function(error){
							jAlert("There is something wrong. Please try again"+error,"Error");
							return false;
						} 
					});
				}else{
					return false;
				}
			});
		}
	}
	
	//function to sum the entered quantity
	function editPOSumQty(qty){
		var sum = 0;
		$('.ePO_qty').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseInt($(this).val());
			}
		});

		$('#ePO_totalQty').val(sum);
	}
	
	//function to sum the entered price
	function editPOSumPrice(price){
		var sum = 0;
		$('.ePO_price').each(function() {
			if($(this).val() != '' && !isNaN($(this).val())){
				sum += parseFloat($(this).val());
			}
		});

		$('#ePO_totalPrice').val(sum);
	}
	
	function ePOEventResetFilters(){
		editPOEventGridSearchString='';
		editPOEventGridColumnFilters = {};
		getEditPOEventGridData(0);
	}
	
	function ePOSearchEventData(){
		ePOEventResetFilters();
	}
	
	
	//Get selected event on button click
	function getEventInfoForEditPO(){		
		var temprEventRowIndex = editPOEventGrid.getSelectedRows([0])[0];
		if(temprEventRowIndex == null){
			jAlert("Please select event");
		}{			
			var eventId = editPOEventGrid.getDataItem(temprEventRowIndex).eventID;
			var eventName = editPOEventGrid.getDataItem(temprEventRowIndex).eventName;
			var eventDate = editPOEventGrid.getDataItem(temprEventRowIndex).eventDate;
			var venue = editPOEventGrid.getDataItem(temprEventRowIndex).eventDate;
			var eventTime = editPOEventGrid.getDataItem(temprEventRowIndex).eventTime;
			
			//Add dynamic rows to table for selected event
			var myTable = document.getElementById("ePO_myTableBody");
			var currentIndex = myTable.rows.length;
			var currentRow = myTable.insertRow(-1);
			
			var input1 = document.createElement("input");
			input1.setAttribute("type", "hidden");
			input1.setAttribute("name", "rowId_" + currentIndex);
			input1.setAttribute("value", currentIndex);
			//input1.setAttribute("id","display");

			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", "ticketGroup_"+currentIndex);
			input.setAttribute("value", eventId);
			input.setAttribute("id","display");

			var eventNameLabel = document.createElement("Label");
			eventNameLabel.setAttribute("for","event_"+eventId);
			eventNameLabel.innerHTML = eventName+" "+eventDate+" "+eventTime;
			
			/* var eventDateLabel = document.createElement("Label");
			eventDateLabel.setAttribute("for","event_"+eventId);
			eventDateLabel.innerHTML = eventDate;

			var venueLabel = document.createElement("Label");
			venueLabel.setAttribute("for","event_"+eventId);
			venueLabel.innerHTML = venue; */
			
			var sectionBox = document.createElement("input");
			sectionBox.setAttribute("name", "section_" +currentIndex);
			sectionBox.setAttribute("id","section_"+currentIndex);
			sectionBox.setAttribute("class","section");
			
			var row = document.createElement("input");
			row.setAttribute("name", "row_" +currentIndex);
			row.setAttribute("id","row_"+currentIndex);
			row.setAttribute("class","rowValue");
			
			var seatLow = document.createElement("input");
			seatLow.setAttribute("name", "seatLow_" +currentIndex);
			seatLow.setAttribute("id","seatLow_"+currentIndex);
			seatLow.setAttribute("class","seatLow"); 
			
			var seatHigh = document.createElement("input");
			seatHigh.setAttribute("name", "seatHigh_" +currentIndex);
			seatHigh.setAttribute("id","seatHigh_"+currentIndex);
			seatHigh.setAttribute("class","seatHigh");
			
			var qty = document.createElement("input");
			qty.setAttribute("type", "number");
			qty.setAttribute("name", "qty_" +currentIndex);
			qty.setAttribute("id","qty_"+currentIndex);
			qty.setAttribute("class","ePO_qty");
			qty.setAttribute("onkeyup","editPOSumQty(this);");
			
			var price = document.createElement("input");
			//price.setAttribute("type", "number");
			price.setAttribute("name", "price_" +currentIndex);
			price.setAttribute("id","price_"+currentIndex);
			price.setAttribute("class","ePO_price");
			price.setAttribute("onkeyup","editPOSumPrice(this);");
			
			var addRowBox = document.createElement("input");
			addRowBox.setAttribute("type", "button");
			addRowBox.setAttribute("value", "Remove");
			addRowBox.setAttribute("onclick", "removeField(this); editPOSumQty(this); editPOSumPrice(this);");
			addRowBox.setAttribute("class", "button");

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(input1);
			currentCell.appendChild(input);

			var currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(eventNameLabel);

			/* currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(eventDateLabel);

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(venueLabel); */

			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(sectionBox);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(row);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(seatLow);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(seatHigh);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(qty);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(price);
			
			currentCell = currentRow.insertCell(-1);
			currentCell.appendChild(addRowBox);
			
			$(window).scrollTop(300);
		}			
			/*var footer = myTable.createTFoot();
			var row = footer.insertRow();
			var cell = row.insertCell();
			cell.innerHTML = "Total Qty";*/
	}
	
	
	/*function pagingControl(move,id){		
		var pageNo = 0;
		if(move == 'FIRST'){
			pageNo = 0;
		}else if(move == 'LAST'){
			pageNo = parseInt(editPOEventPagingInfo.totalPages)-1;
		}else if(move == 'NEXT'){
			pageNo = parseInt(editPOEventPagingInfo.pageNum) +1;
		}else if(move == 'PREV'){
			pageNo = parseInt(editPOEventPagingInfo.pageNum)-1;
		}
		getEditPOEventGridData(pageNo);
	}*/
	
	function getEditPOEventGridData(pageNo) {
		$('#ePO_eventGridDiv').show();
		var searchValue = $("#ePO_eventSearchValue").val();
		$.ajax({			  
			url : "/GetEventDetails",
			type : "post",
			data : $("#ePO_eventSearch").serialize()+"&pageNo="+pageNo+"&headerFilter="+editPOEventGridSearchString,
			dataType:"json",
			success : function(res){
				var jsonData = JSON.parse(JSON.stringify(res));
				editPOEventPagingInfo = jsonData.eventPagingInfo;
				editPOEventGridValues(jsonData.events);
			}, error : function(error){
				jAlert("There is something wrong. Please try again"+error,"Error");
				return false;
			}
		});
	}
	
	var editPOEventPagingInfo;
	var editPOEventDataView;
	var editPOEventGrid;
	var editPOEventData = [];
	var editPOEventGridSearchString='';
	var editPOEventGridColumnFilters = {};
	var editPOEventGridColumns = [ 
	{
		id : "eventName",
		name : "Event Name",
		field : "eventName",
		sortable : true
	}, {
		id : "eventDate",
		name : "Event Date",
		field : "eventDate",
		sortable : true
	}, {
		id : "eventTime",
		name : "Event Time",
		field : "eventTime",
		sortable : true
	}, {
		id : "venue",
		name : "Venue",
		field : "venue",
		sortable : true
	}, {
		id : "city",
		name : "City",
		field : "city",
		sortable : true
	}, {
		id : "state",
		name : "State",
		field : "state",
		sortable : true
	}, {
		id: "country", 
		name: "Country", 
		field: "country", 
		sortable: true
	}, {
		id: "grandChildCategory", 
		name: "Grand Child Category", 
		field: "grandChildCategory", 
		sortable: true
	},{
		id: "childCategory", 
		name: "Child Category", 
		field: "childCategory", 
		sortable: true
	}, {
		id : "parentCategory",
		field : "parentCategory",
		name : "Parent Category",
		sortable: true
	} ];

	var editPOEventOptions = {
		enableCellNavigation : true,
		forceFitColumns : true,
		multiSelect: false,
		topPanelHeight : 25,
		showHeaderRow: true,
		headerRowHeight: 30,
		explicitInitialization: true
	};
	var editPOEventGridSortcol = "eventName";
	var editPOEventGridSortdir = 1;
	var percentCompleteThreshold = 0;
	
	function deleteRecordFromEventGrid(id) {
		editPOEventDataView.deleteItem(id);
		editPOEventGrid.invalidate();
	}
	
	/*function editPOEventGridFilter(item, args) {
		var x= item["eventName"];
		if (args.editPOEventGridSearchString  != ""
				&& x.indexOf(args.editPOEventGridSearchString) == -1) {
			
			if (typeof x === 'string' || x instanceof String) {
				if(x.toLowerCase().indexOf(args.editPOEventGridSearchString.toLowerCase()) == -1) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}*/
	
	function editPOEventGridComparer(a, b) {
		var x = a[editPOEventGridSortcol], y = b[editPOEventGridSortcol];
		if(!isNaN(x)){
		   return (parseFloat(x) == parseFloat(y) ? 0 : (parseFloat(x) > parseFloat(y )? 1 : -1));
		}
		if(x == '' || x == null) {
			return 1;
		} else if(y == '' || y == null) {
			return -1;
		}
		if(x.indexOf('/') > 0 && x.length==10){
			return commonDateComparator(x,y);
		}else if(x.indexOf('/') > 0 && x.length>10 && x.indexOf(':') > 0){
			return commonDateTimeComparator(x,y);
		}else if(x.indexOf(':') > 0 && (x.toLowerCase().indexOf(' am') >0 || x.toLowerCase().indexOf(' pm') > 0)){
			 return new Date('1970/01/01 ' + x) - new Date('1970/01/01 ' + y);
		}else if ((typeof x === 'string' || x instanceof String) && (typeof y === 'string' || y instanceof String)) {
			return x.toLowerCase().localeCompare(y.toLowerCase());	
		} else {
			return (x == y ? 0 : (x > y ? 1 : -1));	
		}
	}
	
	/*function editPOEventGridToggleFilterRow() {
		editPOEventGrid.setTopPanelVisibility(!editPOEventGrid.getOptions().showTopPanel);
	}
	
	//$(".grid-header .ui-icon-sg").addClass("ui-state-default-sg ui-corner-all")
	$("#editPOEvent_grid_toogle_search").addClass("ui-state-default-sg ui-corner-all")
		.mouseover(function(e) {
			$(e.target).addClass("ui-state-hover");
		}).mouseout(function(e) {
			$(e.target).removeClass("ui-state-hover");
	});	*/
		
	function editPOEventGridValues(jsonData) {
		editPOEventData = [];
		for (var i = 0; i < jsonData.length; i++) {
			var  data= jsonData[i]; 
			var d = (editPOEventData[i] = {});
			d["id"] = i;
			d["eventID"] = data.eventId;
			d["eventName"] = data.eventName;
			d["eventDate"] = data.eventDateStr;
			d["eventTime"] = data.eventTimeStr;
			d["venue"] = data.building;
			d["city"] = data.city;
			d["state"] = data.state;
			d["country"] = data.country;
			d["grandChildCategory"] = data.grandChildCategoryName;
			d["childCategory"] = data.childCategoryName;
			d["parentCategory"] = data.parentCategoryName;
		}

		editPOEventDataView = new Slick.Data.DataView();
		editPOEventGrid = new Slick.Grid("#editPOEvent_grid", editPOEventDataView, editPOEventGridColumns, editPOEventOptions);
		editPOEventGrid.registerPlugin(new Slick.AutoTooltips({ enableForHeaderCells: true }));
		editPOEventGrid.setSelectionModel(new Slick.RowSelectionModel());
		if(editPOEventPagingInfo!=null){
			var editPOEventGridPager = new Slick.Controls.Pager(editPOEventDataView, editPOEventGrid, $("#editPOEvent_pager"),editPOEventPagingInfo);
		}
		
		var editPOEventGridColumnPicker = new Slick.Controls.ColumnPicker(editPOEventGridColumns, editPOEventGrid,
				editPOEventOptions);

		// move the filter panel defined in a hidden div into editPOEventGrid top panel
		//$("#editPOEvent_inlineFilterPanel").appendTo(editPOEventGrid.getTopPanel()).show();

		/*editPOEventGrid.onKeyDown.subscribe(function(e) {
			// select all rows on ctrl-a
			if (e.which != 65 || !e.ctrlKey) {
				return false;
			}
			var rows = [];
			for ( var i = 0; i < editPOEventDataView.getLength(); i++) {
				rows.push(i);
			}
			editPOEventGrid.setSelectedRows(rows);
			e.preventDefault();
		});*/
		
		editPOEventGrid.onSort.subscribe(function(e, args) {
			editPOEventGridSortdir = args.sortAsc ? 1 : -1;
			editPOEventGridSortcol = args.sortCol.field;
			if ($.browser.msie && $.browser.version <= 8) {
				editPOEventDataView.fastSort(editPOEventGridSortcol, args.sortAsc);
			} else {
				editPOEventDataView.sort(editPOEventGridComparer, args.sortAsc);
			}
		});
		
		// wire up model events to drive the editPOEventGrid
		editPOEventDataView.onRowCountChanged.subscribe(function(e, args) {
			editPOEventGrid.updateRowCount();
			editPOEventGrid.render();
		});
		editPOEventDataView.onRowsChanged.subscribe(function(e, args) {
			editPOEventGrid.invalidateRows(args.rows);
			editPOEventGrid.render();
		});
		
		$(editPOEventGrid.getHeaderRow()).delegate(":input", "keyup", function (e) {
		 var keyCode = (e.keyCode ? e.keyCode : e.which);
			editPOEventGridSearchString='';
			var columnId = $(this).data("columnId");
			if (columnId != null) {
				editPOEventGridColumnFilters[columnId] = $.trim($(this).val());
				if(keyCode == 13) {
					for (var columnId in editPOEventGridColumnFilters) {
					  if (columnId !== undefined && editPOEventGridColumnFilters[columnId] !== "") {
						  editPOEventGridSearchString += columnId + ":" +editPOEventGridColumnFilters[columnId]+",";
					  }
					}
					getEditPOEventGridData(0);
				}
			}		 
		});
		editPOEventGrid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			if(args.column.id.indexOf('checkbox') == -1){
				if(args.column.id == 'eventTime'){
					$("<input type='text' placeholder='hh:mm a'>")
				   .data("columnId", args.column.id)
				   .val(editPOEventGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else if(args.column.id == 'eventDate'){
					$("<input type='text' placeholder='mm/dd/yyyy'>")
				   .data("columnId", args.column.id)
				   .val(editPOEventGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
				else{
					$("<input type='text'>")
				   .data("columnId", args.column.id)
				   .val(editPOEventGridColumnFilters[args.column.id])
				   .appendTo(args.node);
				}
			}				
		});
		editPOEventGrid.init();
			
		/*
		// wire up the search textbox to apply the filter to the model
		$("#editPOEventGridSearch").keyup(function(e) {
			Slick.GlobalEditorLock.cancelCurrentEdit();
			// clear on Esc
			if (e.which == 27) {
				this.value = "";
			}
			editPOEventGridSearchString = this.value;
			updateEditPOEventGridFilter();
		});
		function updateEditPOEventGridFilter() {
			editPOEventDataView.setFilterArgs({
				editPOEventGridSearchString : editPOEventGridSearchString
			});
			editPOEventDataView.refresh();
		}*/
		
		// initialize the model after all the events have been hooked up
		editPOEventDataView.beginUpdate();
		editPOEventDataView.setItems(editPOEventData);
		/*editPOEventDataView.setFilterArgs({
			percentCompleteThreshold : percentCompleteThreshold,
			editPOEventGridSearchString : editPOEventGridSearchString
		});
		editPOEventDataView.setFilter(editPOEventGridFilter);*/
		
		editPOEventDataView.endUpdate();
		editPOEventDataView.syncGridSelection(editPOEventGrid, true);
		$("#gridContainer").resizable();
		$("div#divLoading").removeClass('show');
		
		/* var createPoEventGridRowIndex;
		editPOEventGrid.onSelectedRowsChanged.subscribe(function() { 
			var tempCreatePoEventGridRowIndex = editPOEventGrid.getSelectedRows([0])[0];
			if (tempCreatePoEventGridRowIndex != createPoEventGridRowIndex) {
				createPoEventGridRowIndex = tempCreatePoEventGridRowIndex;
				 var eventId =editPOEventGrid.getDataItem(tempCreatePoEventGridRowIndex).id;
				//jAlert(eventId);
				//getCategoryTicketGroupsforEvent(eventId);
			}
		}); */
		
		editPOEventGrid.onSelectedRowsChanged.subscribe(function() { 
			$('#ePO_addTicketDiv').show();
		});
	}
	
	function updateShippingCombo(jsonData,id,listId){
		$(id).empty();
		$(id).append("<option value='-1'>--select--</option>");
		if(jsonData!='' && jsonData!=null){
			for(var i=0;i<jsonData.length;i++){
				if(listId!='' && jsonData[i].shippingMethodId == listId){
					$(id).append("<option value="+jsonData[i].shippingMethodId+" selected>"+jsonData[i].shippingMethodName+"</option>");					
				}else{
					$(id).append("<option value="+jsonData[i].shippingMethodId+">"+jsonData[i].shippingMethodName+"</option>");
				}
			}
		}
	}
	