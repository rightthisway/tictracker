package com.rtw.tracker.filters;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

public class UserActionLogFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		//String path = httpServletRequest.getPathInfo();
		//System.out.println(path);		
		String path = httpServletRequest.getServletPath();
		if(path == null || path.contains("Logout") || path.contains("resources")){
			// skip this and do not track this
		}else{
			TrackerUser trackerUser = (TrackerUser)httpServletRequest.getSession().getAttribute("trackerUser");
			if(trackerUser != null){
				logUserAction(trackerUser, path, httpServletRequest);
			}
		}
		chain.doFilter(request, response);
	}
	
	public boolean logUserAction(TrackerUser trackerUser, String path, HttpServletRequest httpServletRequest){
		try{
			Map<String, String> paramMap = Util.getParameterMap(httpServletRequest);
			paramMap.put("actionURI", path);
			
			String data = Util.getObject(paramMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_USER_ACTION);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				return true;
			}
			
			/*UserAction userAction = new UserAction();
			userAction.setAction(path);
			userAction.setTimeStamp(new Date());
			//userAction.setTrackerUser(trackerUser);
			userAction.setUserId(trackerUser.getId());
			userAction.setIpAddress(httpServletRequest.getRemoteAddr());
			userAction.setUserName(trackerUser.getUserName());
			DAORegistry.getUserActionDAO().save(userAction);
			return true;*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
