package com.rtw.tracker.utils;

import org.json.JSONObject;

import com.rtw.tracker.dao.implementation.DAORegistry;


public class PaginationUtil {
	
	public static final Integer PAGESIZE = 500;
	public static String partialRewardPaymentPrefix = DAORegistry.getPropertyDAO().get("rtf.partial.reward.payment.prefix").getValue();
	public static String fullRewardPaymentPrefix = DAORegistry.getPropertyDAO().get("rtf.full.reward.payment.prefix").getValue();
	
	public static JSONObject calculatePaginationParameters(Integer count,String pageNo){
		JSONObject paginationObject  = new JSONObject();
		try {
			if(pageNo==null || pageNo.isEmpty()){
				pageNo = "0";
			}
			if(count==null){
				count =0;
			}
			paginationObject.put("pageSize", PAGESIZE);
			paginationObject.put("pageNum", pageNo);
			paginationObject.put("totalRows", count);
			Double size =  ((double)count/(double)PAGESIZE);
			paginationObject.put("totalPages", Math.ceil(size));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paginationObject;
	}
	
	public static JSONObject getDummyPaginationParameter(Integer count){
		JSONObject paginationObject  = new JSONObject();
		try {
			paginationObject.put("pageSize", count);
			paginationObject.put("pageNum", 0);
			paginationObject.put("totalRows", count);
			paginationObject.put("totalPages",1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paginationObject;
	}
	
	
	public static Integer getNextPageStatFrom(String pageNo){
		Integer startFrom = 0;
		if(pageNo !=null && !pageNo.isEmpty() && !pageNo.equals("0")){
			startFrom = (Integer.parseInt(pageNo) * PaginationUtil.PAGESIZE)+1;
		}
		return startFrom;
	}

}
