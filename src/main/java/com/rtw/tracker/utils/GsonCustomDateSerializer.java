package com.rtw.tracker.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

public class GsonCustomDateSerializer implements JsonDeserializer<Date> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public Date deserialize(JsonElement arg0, java.lang.reflect.Type arg1, JsonDeserializationContext arg2) {
		String date = arg0.getAsString();
	      try {
	          return dateFormat.parse(date);
	      } catch (Exception e) {
	          try{
	        	  return dateFormat1.parse(date);
	          }catch(Exception e1){
	        	  try{
	            	  return dateFormat2.parse(date);
	              }catch(Exception e2){
	            	  try {
						return new Date(Long.parseLong(date));
					} catch (Exception e3) {
						 System.out.println(e.getMessage());
					}
	            	 
	              }
	          }
	      }
		  return null;
	}
	
}
