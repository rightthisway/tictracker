package com.rtw.tracker.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.Util;
import com.rtw.tracker.pojos.CommonRespInfo;
import com.rtw.tracker.pojos.ContSummaryInfo;

public class ComputeGrandWinnerThread implements Runnable{

	private Integer contestId;
	HttpServletRequest request;
	
	private static Logger contestLog = LoggerFactory.getLogger(ComputeGrandWinnerThread.class);
	public ComputeGrandWinnerThread(Integer contestId,HttpServletRequest request){
		this.contestId = contestId;
		this.request = request;
	}
	
	@Override
	public void run() {
		Map<String, String> map1 = com.rtw.tracker.utils.Util.getParameterMapCass(request);
		map1.put("coId",contestId.toString());
		
		try {
			contestLog.error("RTFAPI INFO: GRAND WINNER COMPUTATION STARTED (GetGrandWinners) ");
			String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_WINNERS_CASS);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
			if(winnerSummary!=null && winnerSummary.getSts()==1){
				contestLog.info("RTFAPI INFO: GRAND WINNER COMPUTATION COMPLETED (GetGrandWinners)");
				Map<String, String> map2 = Util.getParameterMap(null);
				//map2.put("contestId",contestId.toString());
				map2.put("coId",contestId.toString());
				//String data2 = Util.getObject(map2,Constants.BASE_URL+Constants.QUIZ_UPDATE_USER_STATISTICS);
				contestLog.info("RTFAPI INFO: CUSTOMER STATISTICS STARTED (UpdateCustContStats)");
				String data2 = Util.getObject(map2,Constants.BASE_URL+Constants.QUIZ_UPDATE_USER_STATISTICS_CASS);
				Gson gson2 = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject2 = gson2.fromJson(data2, JsonObject.class);
				//QuizCustomerStatsInfo customerStat = gson2.fromJson(((JsonObject)jsonObject2.get("quizCustomerStatsInfo")), QuizCustomerStatsInfo.class);
				CommonRespInfo customerStat = gson2.fromJson(((JsonObject)jsonObject2.get("commonRespInfo")), CommonRespInfo.class);
				contestLog.info("RTFAPI INFO: CUSTOMER STATISTICS COMPLETED (UpdateCustContStats)");
				if(customerStat.getSts() == 0){
					contestLog.error("RTFAPI ERROR (UpdateCustContStats): "+customerStat.getErr().getDesc());
				}
			}else{
				contestLog.error("RTFAPI ERROR (GetGrandWinners): "+winnerSummary.getErr().getDesc());
			}
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
		
	}

}
