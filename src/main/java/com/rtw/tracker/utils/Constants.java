package com.rtw.tracker.utils;




/**
 * Hold constant variables
 */

public final class Constants {
	
	//Local Setup
	//public static final String BASE_URL = "http://34.202.149.190/sandbox/";
	//public static final String TICTRACKER_API_BASE_URL = "http://localhost:8080/ticApi/";
	
	//Sandbox Setup
	//public static final String BASE_URL = "http://10.0.1.236/sandbox/";
	//public static final String TICTRACKER_API_BASE_URL = "http://localhost:8083/ticApi/";
	
	//Production Setup
	public static final String BASE_URL = "http://10.0.1.111/";
	public static final String TICTRACKER_API_BASE_URL = "http://localhost:8080/ticApi/";
	
	//Local Production
	//public static final String BASE_URL = "http://api.rewardthefan.com/";
	//public static final String TICTRACKER_API_BASE_URL = "http://localhost:8080/ticApi/";
	
	
	public static final String Reward_The_Fan_Product="RewardTheFan";
	public static final String Pre_Sale_Zone_Tickets_Product="PreSaleZoneTickets";
	public static final String Category_Ticekts_Product="CategoryTickets";
	public static final String First_Ten_Row_Tickets_Product="FirstTenRowTickets";
	public static final String Last_Ten_Row_Tickets_Product="LastTenRowTickets";
	
	public static final String CSV_EXTENSION = "csv";
	public static final String EXCEL_EXTENSION = "xlsx";
	public static final String NEW_LINE_DELIMITER = "\n";
		
	public static final String TICTRACKER_API_GET_EVENTS = "GetEvents.json";
	public static final String TICTRACKER_API_GET_HOLD_TICKETS_FOR_EVENT = "GetHoldTicketsForEvent.json";
	public static final String TICTRACKER_API_GET_LOCKED_TICKETS_FOR_EVENTS = "GetLockedTicketsForEvent.json";
	public static final String TICTRACKER_API_GET_SOLD_TICKETS_FOR_EVENT = "GetSoldTicketsForEvent.json";
	public static final String TICTRACKER_API_GET_EVENT_DETAILS = "GetEventDetails.json";
	public static final String TICTRACKER_API_GET_EDIT_HOLD_TICKETS = "EditHoldTickets.json";
	public static final String TICTRACKER_API_GET_HOLD_TICKETS = "GetHoldTickets.json";
	public static final String TICTRACKER_API_ADD_CATEGORY_TICKETS = "AddCategoryTickets.json";
	public static final String TICTRACKER_API_UPLOAD_TICKET_DETAILS = "UpdateTicketDetails.json";
	public static final String TICTRACKER_API_HOLD_TICKET = "Invoice/HoldTicket.json";
	public static final String TICTRACKER_API_CHECK_LOCKED_TICKETS = "CheckLockedTickets.json";
	public static final String TICTRACKER_API_CATEGORY_TICKETS_GROUPS = "getCategoryTicketGroups.json";
	public static final String TICTRACKER_API_GET_ZONE_MAP_TICKETS = "GetZoneMapTickets.json";
	
	
	public static final String TICTRACKER_API_AFFILIATES_ORDER_SUMMARY = "GetAffiliateOrderSummary.json";
	public static final String TICTRACKER_API_AUDIT_USERS = "AuditUser.json";
	public static final String TICTRACKER_API_GET_AFFILIATES_PROMO_HISTORY = "GetAffiliatePromoHistory.json";
	public static final String TICTRACKER_API_GENERATE_AFFILIATES_PROMO_CODE = "GenerateAffiliatePromoCode.json";
	public static final String TICTRACKER_API_CHECK_USER_FOR_AUDIT = "CheckUserForEdit.json";
	public static final String TICTRACKER_API_CHECK_USER = "CheckUser.json";
	public static final String TICTRACKER_API_ADD_USER = "AddUser.json";
	public static final String TICTRACKER_API_ADD_USERS = "AddUsers.json";
	public static final String TICTRACKER_API_USER_STATUS_CHANGE = "UserStatusChange.json";
	public static final String TICTRACKER_API_GET_AFFILIATES_ORDER = "GetAffiliatesOrder.json";
	public static final String TICTRACKER_API_MANAGE_BROKERS= "ManageBrokers.json";
	public static final String TICTRACKER_API_SAVE_USER_PREFERENCE = "SaveUserPreference.json";
	public static final String TICTRACKER_API_AFFILIATES = "Affiliates.json";
	public static final String TICTRACKER_API_CONTEST_AFFILIATES = "ContestAffiliates.json";
	public static final String TICTRACKER_API_CONTEST_AFFILIATES_EARNING = "ContestAffiliatesEarning.json";
	public static final String TICTRACKER_API_UPDATE_CONTEST_AFFILIATES = "UpdateContestAffiliate.json";
	public static final String TICTRACKER_API_CHANGE_CONTEST_AFFILIATES = "ChangeStatusContestAffiliate.json";
	public static final String TICTRACKER_API_EDIT_USERS = "EditUsers.json";
	public static final String TICTRACKER_API_EDIT_USER = "EditUser.json";
	public static final String TICTRACKER_API_DELETE_USER = "DeleteUser.json";
	
	public static final String TICTRACKER_API_PAYPAL_TRACKING = "PaypalTracking.json";
	public static final String TICTRACKER_API_MANAGE_USERS = "ManageUsers.json";
	public static final String TICTRACKER_API_USER_STATUS = "UserStatus.json";
	public static final String TICTRACKER_API_TRACKING_HOME = "TrackingHome.json";
	
	
	
	public static final String TICTRACKER_API_GRAND_CHILD_CATEGORY_IMAGES = "GrandChildCategoryImages.json";
	public static final String TICTRACKER_API_LOYAL_FAN_PARENT_CATEGORY_IMAGES = "LoyalFanParentCategoryImages.json";
	public static final String TICTRACKER_API_PARENT_CATEGORY_IMAGES = "ParentCategoryImages.json";
	public static final String TICTRACKER_API_FANTASY_GRAND_CHILD_CATEGORY_IMAGES = "FantasyGrandChildCategoryImages.json";
	public static final String TICTRACKER_API_CHILD_CATEGORY_IMAGES = "ChildCategoryImages.json";
	public static final String TICTRACKER_API_ARTIST_IMAGES = "ArtistImages.json";
	public static final String TICTRACKER_API_UPDATE_ARTIST_IMAGES = "updateArtistImages.json";
	public static final String TICTRACKER_API_GET_ARTIST_IMAGE_FOR_EDIT = "GetArtistImagesForEdit.json";
	public static final String TICTRACKER_API_POPULAR_ARTIST = "PopularArtist.json";
	public static final String TICTRACKER_API_UPDATE_POPULAR_ARTIST = "UpdatePopularArtist.json";
	public static final String TICTRACKER_API_CARDS = "Cards.json";
	public static final String TICTRACKER_API_MANAGE_ARTIST_FOR_SEARCH = "ManageArtistForSearch.json";
	public static final String TICTRACKER_API_POPULAR_ARTISTS_COUNT = "GetPopularArtistsCount.json";
	public static final String TICTRACKER_API_CARDS_UPDATE_POPUP = "CardsUpdatePopup.json";
	public static final String TICTRACKER_API_CHECK_CARD_POSITION = "CheckCardPosition.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_EVENTS = "CrownJewelEvents.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_EVENTS = "AutoCompleteEvents.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_TEAMS = "CrownJewelTeams.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_FINAL_TEAM = "CrownJewelFinalTeam.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_TEAM_ZONES = "CrownJewelTeamZones.json";
	public static final String updateChildCategory = "CrownJewelChild.json";
	public static final String TICTRACKER_API_CROWN_CATEGORY_TEAMS = "CrownJewelCategoryTeams.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_ORDERS = "CrownJewelOrders.json";
	public static final String TICTRACKER_API_UPDATE_CHILD_CATEGORY = "updateChildCategory.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_CHILD = "CrownJewelChild.json";
	public static final String TICTRACKER_API_CHECK_AFFILIATE_PROMOCODE = "CheckAffiliatePromoCode.json";
	public static final String TICTRACKER_API_GET_AFFILIATE_PAYMENT_NOTE = "getAffiliatePaymentNote.json";
	public static final String TICTRACKER_API_SAVE_AFFILIATE_PAYMENT_NOTE = "saveAffiliatePaymentNote.json";
	public static final String TICTRACKER_API_CONTESTS = "Contests.json";
	public static final String TICTRACKER_API_UPDATE_QUESTION_POSITION = "UpdateQuestionPosition.json";
	public static final String TICTRACKER_API_VIEW_CONTEST_WINNER = "ViewContestWinners.json";
	public static final String TICTRACKER_API_WINNER_EXPIRY_DATE = "UpdateExpiryDate.json";
	public static final String TICTRACKER_API_RESET_CONTESTS = "ResetContest.json";
	public static final String TICTRACKER_API_UPDATE_CONTESTS = "UpdateContest.json";
	public static final String TICTRACKER_API_GET_UPCOMING_CONTEST = "getUpcomingContestDetails.json";
	public static final String TICTRACKER_API_QUESTION = "ContestQuestions.json";
	public static final String TICTRACKER_API_UPDATE_QUESTION = "UpdateQuestion.json";
	public static final String TICTRACKER_API_UPDATE_QUESTION_REWARD = "UpdateQuestionRewards.json";
	public static final String TICTRACKER_API_UPDATE_VIDEO_SOURCE_URL = "UpdateVideoSourceUrl.json";
	public static final String TICTRACKER_API_UPDATE_QUIZ_CONFIG_SETTINGS = "UpdateQuizConfigSettings.json";
	public static final String TICTRACKER_API_GET_QUIZ_CONFIG_SETTINGS = "GetContestSettings.json";
	public static final String TICTRACKER_API_UPDATE_REFERRAL_REWARDS_SETTINGS = "UpdateReferralRewardSettings.json";
	public static final String TICTRACKER_API_GET_REFERRAL_REWARDS_SETTINGS = "GetReferralRewardSettings.json";
	public static final String TICTRACKER_API_STATRT_CONTEST_QUESTIONS = "NextContestQuestion.json";
	public static final String TICTRACKER_API_GET_LAST_QUESTION_DETAILs = "getLastQuestionDetails.json";
	public static final String TICTRACKER_API_CONTEST_EVENTS = "ContestEvents.json";
	public static final String TICTRACKER_API_UPDATE_EXCLUDE_CONTEST_EVENTS = "UpdateExcludeContestEvents.json";
	public static final String TICTRACKER_API_EXCLUDE_CONTEST_EVENTS = "ExcludeContestEvents.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_ARTIST_EVENT_CATEGORIES = "AutoCompleteArtistEventAndCategory.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_CUSTOMER = "GetCustomersAutoComplete.json";
	public static final String TICTRACKER_API_UPDATE_QUESTION_BANK = "UpdateQuestionBank.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_QUESTION_BANK_CATEGORY = "AutoCompleteQuestionBankCategory.json";
	public static final String TICTRACKER_API_CONTEST_QUESTION_BANK = "ContestQuestionBank.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_GIFTCARD = "AutoCompleteGiftCard.json";
	
	public static final String TICTRACKER_API_AUTO_COMPLETE_ARTIST_VENUE_GCHILD_TMAT = "AutoCompleteArtistAndVenueGChildTMAT.json";
	public static final String TICTRACKER_API_RTF_CATS_EVENTS_LIST = "RtfCatsEventsList.json";
	public static final String TICTRACKER_API_UPDATE_RTF_CATS_EVENTS = "UpdateRtfCatsEvents.json";
	public static final String TICTRACKER_API_UPDATE_GRAND_CHILD_CATEGORY = "updateGrandChildCategory.json";
	public static final String TICTRACKER_API_CROWN_JEWEL_GRAND_CHILD = "CrownJewelGrandChild.json";
	public static final String TICTRACKER_API_GET_CHILD_CATEGORY_VALUE = "getChildCategoryValue.json";
	public static final String TICTRACKER_API_GET_GRAND_CHILD_CATEGORY_VALUE = "getGrandChildCategoryValue.json";
	public static final String TICTRACKER_API_GET_ARTIST = "GetArtist.json";
	public static final String TICTRACKER_API_GET_CATEGORY_TICKET_GROUP_ID = "getCategoryTicketGroupById.json";
	public static final String TICTRACKER_API_AFFILIATE_REPORT = "AffiliateReport.json";
	
	//public static final String TICTRACKER_API_GET_ARTIST_IMAGES = "GetArtistImages.json";
	public static final String TICTRACKER_API_GET_ARTIST_IMAGES_FOR_EDIT = "GetArtistImagesForEdit.json";
		
	public static final String TICTRACKER_API_MANAGE_CUSTOMERS = "ManageDetails.json";
	//public static final String TICTRACKER_API_GET_MANAGE_CUSTOMERS = "GetCustomers.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO = "GetCustomerInfo.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO_FOR_INVOICE = "GetCustomerInfoForInvoice.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO_FOR_PO = "GetCustomerInfoForPO.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO_FOR_INVOICE_TICKETS = "GetCustomerInfoForInvoiceTickets.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO_FOR_LOYAL_FAN = "GetArtistsForCustomer.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO_FOR_FAVOURITE_EVENT = "GetFavouriteEventsForCustomer.json";
	public static final String TICTRACKER_API_EDIT_CUSTOMER_INFO_FOR_REWARD_POINTS = "EditCustomerInfoForRewardPoints.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_INFO_FOR_REWARD_POINTS = "GetCustomerInfoForRewardPoints.json";
	public static final String TICTRACKER_API_ADD_REWARD_POINTS_FOR_CUSTOMER = "AddRewardPointsForCustomer.json";
	public static final String TICTRACKER_API_ADD_WALLET_FOR_CUSTOMER = "AddWalletForCustomer.json";
	public static final String TICTRACKER_API_UPDATE_CUSTOMER_INFO = "UpdateCustomerDetails.json";
	public static final String TICTRACKER_API_CHECK_CUSTOMER = "CheckCustomer.json";
	public static final String TICTRACKER_API_ADD_CUSTOMER_INFO = "AddCustomer.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_EVENT_DETAILS = "AutoCompleteEventDetails.json";
	public static final String TICTRACKER_API_AUTO_SEARCH_CITY_STATE_COUNTRY = "GetCityStateCountryAutoSearch.json";
	public static final String TICTRACKER_API_DELETE_CUSTOMER = "DeleteCustomer.json";
	public static final String TICTRACKER_API_DELETE_SHIPPING_ADDRESS = "DeleteShippingAddress.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_CITY_STATE_COUNTRY = "AutoCompleteCityStateCountry.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_CUSTOMER_ARTIST = "AutoCompleteCustomerArtist.json";
	
	public static final String TICTRACKER_API_CUSTOMER_PROMOTIONAL_OFFER_DETAILS = "CustomerPromotionalOfferDetails.json";
	//public static final String TICTRACKER_API_GET_CUSTOMER_PROMOTIONAL_OFFER = "GetCustomerPromotionalOffer.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_PROMOTIONAL_OFFER_TRACKING = "GetCustomerPromotionalOfferTracking.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_PROMOTIONAL_ORDER_SUMMARY = "GetCustomerPromotionalOrderSummary.json";
	
	public static final String TICTRACKER_API_TRACK_SHARING_DISCOUNT_CODE = "TrackSharingDiscountCode.json";
	//public static final String TICTRACKER_API_GET_TRACK_SHARING_DISCOUNT_CODE = "GetTrackSharingDiscountCode.json";
	
	public static final String TICTRACKER_API_GENERATE_DISCOUNT_CODE = "GenerateDiscountCode.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_QUESTIONS = "ContestCustomerQuestions.json";
	public static final String TICTRACKER_API_UPDATE_CUSTOMER_QUESTIONS = "UpdateSubmitTrivia.json";
	public static final String TICTRACKER_API_GET_CONTEST_CHECKLIST = "PreContestChecklist.json";
	public static final String TICTRACKER_API_GET_CONTEST_CHECKLIST_LOGS = "GetPreContestChecklistLogs.json";
	public static final String TICTRACKER_API_UPDATE_CONTEST_CHECKLIST = "UpdatePreContestChecklist.json";
	public static final String TICTRACKER_API_COPY_CONTEST_SETUP = "CopyContestSetup.json";
	public static final String TICTRACKER_API_UPDATE_DISCOUNT_CODE_STATUS = "UpdateDiscountCodeStatus.json";
	public static final String TICTRACKER_API_SAVE_DISCOUNT_CODE = "SaveDiscountCode.json";
	public static final String TICTRACKER_API_EDIT_DISCOUNT_CODE = "EditDiscountCode.json";
	public static final String TICTRACKER_API_GET_DISCOUNT_CODE_DETAIL = "GetDiscountCodeDetail.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_ARTIST_EVENT_VENUE_CATEGORY = "AutoCompleteArtistAndEventAndVenueAndCategory.json";
	
	public static final String TICTRACKER_API_EMAIL_BLAST_CUSTOMERS = "CleanedClient.json";
	//public static final String TICTRACKER_API_GET_EMAIL_BLAST_CUSTOMERS = "GetCleanedClient.json";
	public static final String TICTRACKER_API_SEND_MAIL_TO_CUSTOMERS = "SendMailToCustomers.json";
	public static final String TICTRACKER_API_UPDATE_EMAIL_TEMPLATE = "UpdateEmailTemplate.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_ARTIST = "AutoCompleteArtist.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_VENUE = "AutoCompleteVenues.json";
	public static final String TICTRACKER_API_LOAD_STATES = "GetStates.json";
	
	public static final String TICTRACKER_API_AUTO_COMPLETE_ARTIST_VENUE = "AutoCompleteArtistAndVenue.json";
	public static final String TICTRACKER_API_GET_EVENTS_BY_ARTIST_VENUE = "GetEventsByVenueOrArtist.json";
	public static final String TICTRACKER_API_OPEN_ORDERS = "OpenOrders.json";
	//public static final String TICTRACKER_API_GET_OPEN_ORDERS = "GetOpenOrders.json";
	public static final String TICTRACKER_API_UPDATE_OPEN_ORDER = "UpdateOpenOrder.json";
	public static final String TICTRACKER_API_REFRESH_OPEN_ORDER = "RefreshOpenOrder.json";
	public static final String TICTRACKER_API_GET_ORDER_NOTE = "GetOrderNote.json";
	public static final String TICTRACKER_API_UPDATE_ORDER_NOTE = "UpdateOrderNote.json";
	public static final String TICTRACKER_API_PENDING_SHIPMENT_ORDERS = "PendingShipmentOrders.json";
	//public static final String TICTRACKER_API_GET_PENDING_SHIPMENT_ORDERS = "GetPendingShipmentOrders.json";
	public static final String TICTRACKER_API_PENDING_RECEIPT_ORDERS = "PendingRecieptOrders.json";
	public static final String TICTRACKER_API_DISPUTED_ORDERS = "DisputedOrders.json";
	//public static final String TICTRACKER_API_GET_PENDING_RECEIPT_ORDERS = "GetPendingRecieptOrders.json";
	public static final String TICTRACKER_API_CLOSED_ORDERS = "ClosedOrders.json";
	//public static final String TICTRACKER_API_GET_CLOSED_ORDERS = "GetClosedOrders.json";
	public static final String TICTRACKER_API_PAST_ORDERS = "PastOrders.json";
	//public static final String TICTRACKER_API_GET_PAST_ORDERS = "GetPastOrders.json";
	public static final String TICTRACKER_API_GET_RELATED_PO_INVOICES = "GetRelatedPOInvoices.json";
	
	public static final String TICTRACKER_API_MANUAL_FEDEX = "ManualFedex.json";
	//public static final String TICTRACKER_API_GET_MANUAL_FEDEX = "GetManualFedex.json";
	public static final String TICTRACKER_API_CREATE_MANUAL_FEDEX_LABEL = "CreateManualFedexLabel.json";
	public static final String TICTRACKER_API_REMOVE_FEDEX_LABEL = "RemoveFedexLabel.json";
	public static final String TICTRACKER_API_CHECK_FEDEX_LABEL_GENERATED = "CheckFedexLabelGenerated.json";
	
	public static final String TICTRACKER_API_MANAGE_PURCHASE_ORDER = "ManagePO.json";
	//public static final String TICTRACKER_API_GET_MANAGE_PURCHASE_ORDER = "GetPurchaseOrders.json";
	public static final String TICTRACKER_API_GET_UPDATE_PO_NOTE = "GetPONote.json";
	public static final String TICTRACKER_API_GET_UPDATE_PO_CSR = "GetPOCsr.json";
	public static final String TICTRACKER_API_GET_PO_PAYMENT_HISTORY = "GetPOPaymentHistory.json";
	public static final String TICTRACKER_API_GET_UPDATE_PO_TICKET_GROUP_ON_HAND = "UpdatePOTicketGroupOnHandStatus.json";
	public static final String TICTRACKER_API_GET_SHIPPING_ADDRESS_FOR_FEDEX = "getShippingAddressForFedex.json";
	public static final String TICTRACKER_API_CREATE_FEDEX_LABEL_FOR_PO = "CreateFedexLabelForPO.json";
	public static final String TICTRACKER_API_PO_UPLOAD_ETICKET = "UploadEticket.json";
	public static final String TICTRACKER_API_GET_CREATE_PO = "CreatePurchaseOrder.json";
	public static final String TICTRACKER_API_CREATE_PO = "SavePO.json";
	public static final String TICTRACKER_API_EDIT_UPDATE_PO = "EditPurchaseOrder.json";
	
	public static final String TICTRACKER_API_MANAGE_INVOICES = "Invoices.json";
	//public static final String TICTRACKER_API_GET_MANAGE_INVOICES = "GetInvoices.json";
	public static final String TICTRACKER_API_EDIT_INVOICE = "AddRealTicket.json";
	public static final String TICTRACKER_API_UPDATE_INVOICE = "SaveOpenOrders.json";
	public static final String TICTRACKER_API_GET_LONG_INVENTORY_FOR_INVOICE = "GetLongInventoryForInvoice.json";
	public static final String TICTRACKER_API_INVOICE_REAL_TICKET_CHECK_FILE_NAME = "checkFileName.json";
	public static final String TICTRACKER_API_DELETE_REAL_TICKET_ATTACHMENT = "DeleteRealTixAttachment.json";
	public static final String TICTRACKER_API_CREATE_FEDEX_LABEL_FOR_INVOICE = "CreateFedexLabel.json";	
	public static final String TICTRACKER_API_GET_INVOICE_NOTE = "GetInvoiceNote.json";
	public static final String TICTRACKER_API_UPDATE_INVOICE_NOTE = "UpdateInvoiceNote.json";
	public static final String TICTRACKER_API_GET_UPDATE_INVOICE_CSR = "GetInvoiceCsr.json";
	public static final String TICTRACKER_API_GET_INVOICE_PAYMENT_HISTORY = "GetPaymentHistory.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_ORDER_SHIPPING_ADDRESS = "GetCustomerOrderShippingAddress.json";
	public static final String TICTRACKER_API_UPDATE_CUSTOMER_ORDER_SHIPPING_ADDRESS = "UpdateShippingAddress.json";
	public static final String TICTRACKER_API_GET_CUSTOMER_ORDER_BILLING_ADDRESS = "GetCustomerOrderBillingAddress.json";
	public static final String TICTRACKER_API_GET_INVOICE_TICKETS_DOWNLOAD_DETAILS = "GetInvoiceDownloadTicket.json";
	public static final String TICTRACKER_API_CREATE_INVOICE_REFUND_DETAILS = "CreateInvoiceRefundOrCredit.json";
	public static final String TICTRACKER_API_GET_INVOICE_REFUND_DETAILS = "GetInvoiceRefund.json";
	public static final String TICTRACKER_API_GET_INVOICE_CUSTOMER_ORDER_DETAILS = "GetCustomerOrderDetails.json";
	public static final String TICTRACKER_API_GENERATE_INVOICE_PDF_AND_SEND_MAIL = "GenerateInvoicePdfAndSendMail.json";
	public static final String TICTRACKER_API_CREATE_PAY_INVOICE = "CreatePayInvoice.json";
	public static final String TICTRACKER_API_CHANGE_INVOICE_STATUS = "ChangeInvoiceStatus.json";
	
	public static final String TICTRACKER_API_MANAGE_ACCOUNT_RECEIVABLES = "AccountReceivables.json";
	public static final String TICTRACKER_API_GET_CONTEST_INVOICES = "getContestInvoices.json";
	
	public static final String TICTRACKER_API_LOTTERIES = "Lotteries.json";
	//public static final String TICTRACKER_API_GET_LOTTERIES = "GetLotteries.json";
	public static final String TICTRACKER_API_GET_LOTTERIES_PARTICIPANTS = "GetParticipants.json";
	public static final String TICTRACKER_API_EDIT_UPDATE_DELETE_LOTTERY = "UpdateLottery.json";
	public static final String TICTRACKER_API_GET_LOTTERY_WINNER = "GetLotteryWinner.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_ARTIST_CATEGORY = "AutoCompleteArtistAndChildAndGrandChildCategory.json";
	
	public static final String TICTRACKER_API_CREATE_INVOICE_POPUP = "Invoice/CreateInvoice.json";
	public static final String TICTRACKER_API_CREATE_INVOICE_REAL_TICKET_POPUP = "Invoice/CreateInvoiceRealTicket.json";
	public static final String TICTRACKER_API_GET_SHIPPING_ADDRESS = "Invoice/GetShippingAddress.json";
	public static final String TICTRACKER_API_GET_SHIPPING_ADDRESS_DETAILS = "GetShippingAddressDetails.json";
	public static final String TICTRACKER_API_GET_SAVED_CARDS_REWARDS_LOYALFAN = "GetSavedCardsRewardPointsAndLoyaFan.json";
	public static final String TICTRACKER_API_VALIDATE_REFERRAL_CODE = "ValidateReferralCode.json"; 
	public static final String TICTRACKER_API_SAVE_INVOICE = "Invoice/SaveInvoice.json";
	public static final String TICTRACKER_API_SAVE_INVOICE_REAL_TICKET = "Invoice/SaveInvoiceRealTicket.json";
	public static final String TICTRACKER_API_UNHOLD_TICKETS = "Invoice/UnHoldTicket.json";
	public static final String TICTRACKER_API_CREATE_INVOICE_HOLD_TICKET_POPUP = "Invoice/CreateInvoiceHoldTicket.json";
	public static final String TICTRACKER_API_SAVE_INVOICE_HOLD_TICKET = "Invoice/SaveInvoiceHoldTicket.json";
	
	public static final String TICTRACKER_API_AUTO_COMPLETE_BROKERS = "AutoCompleteBrokers.json";
	public static final String TICTRACKER_API_GET_FANTASY_TICKET_VALIDATION = "GetFantasyTicketValidation.json";
	public static final String TICTRACKER_API_GET_FANTASY_GRANDCHILD_CATEGORIES = "GetFantasyGrandChildCategories.json";
	public static final String TICTRACKER_API_AUTO_COMPLETE_FANTASY_CATEGORY_TEAMS = "AutoCompleteFantasyCategoryTeams.json";
	public static final String TICTRACKER_API_UPDATE_TICKET_GROUP_NOTES = "UpdateTicketGroupNotes.json";
	public static final String TICTRACKER_API_SAVE_EVENT_NOTES = "saveEventNotes.json";
	public static final String TICTRACKER_API_SET_GLOBAL_BROKER = "SetGlobalBroker.json";
	public static final String TICTRACKER_API_GET_FANTASY_CHILD_CATEGORY = "GetFantasyChildCategory.json";
	public static final String TICTRACKER_API_GET_FANTASY_GRANDCHILD_CATEGORY = "GetFantasyGrandChildCategory.json";
	public static final String TICTRACKER_API_GET_FANTASY_CATEGORY_TEAM = "getFantasyCategoryTeam.json";
	public static final String TICTRACKER_API_UPDATE_CARDS = "updateCards.json";
	
	public static final String TICTRACKER_API_MANAGE_SEATGEEK_ORDERS = "SgOpenOrders.json";
	public static final String TICTRACKER_API_UPDATE_SEATGEEK_ORDERS = "UpdateSgOrders.json";
	
	public static final String TICTRACKER_API_EVENTS_STATISTICS_REPORT = "Reports/EventsStatisticsReport.json";
	public static final String TICTRACKER_API_CHECK_USERNAME = "CheckUserName.json";
	public static final String TICTRACKER_API_GET_USER_PREFERENCES = "GettingUserPreference.json";
	public static final String TICTRACKER_API_GET_BROKERS = "GetTrackerBroker.json";
	public static final String TICTRACKER_API_GET_USER = "GetTrackerUser.json";
	public static final String TICTRACKER_API_UPDATE_USER_ACTION = "UpdateUserAction.json";
	public static final String TICTRACKER_API_GET_CONTEST_PROMOCODE = "ContestPromocodes.json";
	public static final String TICTRACKER_API_UPDATE_CONTEST_PROMOCODE_STATUS = "UpdateContestPromocodeStatus.json";
	public static final String TICTRACKER_API_UPDATE_EARNLIVES_PROMOCODE_STATUS = "UpdateContestEarnLivesOfferStatus.json";
	public static final String TICTRACKER_API_UPDATE_CONTEST_PROMOCODE = "GenerateContestPromocode.json";
	public static final String TICTRACKER_API_UPDATE_EARNLIVE_PROMOCODE = "GenerateContestEarnLivePromo.json";
	
	//Ends
	
	
	public static final String X_SIGN_HEADER = "b80ABrBWAGpwtsAC/3sLa0qwFdQBS9SIJOkTf8qNt/Q=";
	public static final Integer BROKERID = 1001;
	public static final String X_TOKEN_HEADER = "DESKTOPRTFRTW";
	public static final String X_PLATFORM_HEADER = "DESKTOP_SITE";
	public static final String CONFIGID = "DESKTOPRTFRTW";
	public static final String PRODUCTTYPE = "REWARDTHEFAN";
	public static final String PLATFORM = "TICK_TRACKER";
	public static final String GET_CUSTOMER_STATS = "GetCustomerStatsByUserId.json";
	public static final String GET_BOTS_REPORT = "GetBotsReport.json";
	public static final String GET_EVENT_DETAILS = "GetEventDetails.json"; 
	public static final String CARD_JSP_API = "GetHomeCards";
	public static final String GRAND_CHILD_CATEGORY_API = "GetGrandChildCategory.json";
	public static final String CHILD_CATEGORY_API = "GetChildCategory.json";
	public static final String CUSTOMER_LOGIN = "CustomerLogin.json";
	public static final String CUSTOMER_REGISTRATION = "CustomerRegistration.json";
	public static final String ORDER_PAYMENT_BY_REWARD = "OrderPaymentByReward.json";
	public static final String GET_COUNTRY_AND_STATE = "GetStateAndCountry.json";
	public static final String AUTO_CATS_LOCK_TICKETGROUP = "AutoCatsLockTicketGroup.json";
	public static final String VALIDATE_PROMOCODE = "ValidatePromoCodeTicTracker.json";
	public static final String VALIDATE_PROMOCODE_REAL_TICKET = "ValidatePromoCodeTicTrackerLongOrder.json";
	public static final String ADD_CUSTOMER_ADDRESS = "AddCustomerAddress.json";
	public static final String CREATE_ORDER = "TickTrackerCreateOrder.json";
	public static final String CREATE_ORDER_INITIATE = "InitiateTicTrackerOrder.json";
	public static final String CREATE_ORDER_CONFIRM = "ConfirmTicTrackerOrder.json";
	public static final String CREATE_ORDER_REAL_TICKET = "ConfirmTicTrackerLongOrder.json";
	public static final String INITIATE_ORDER_REAL_TICKET = "InitiateTicTrackerLongOrderNew.json";
	public static final String FAVOURITE_EVENTS = "FavouriteEvents.json";
	public static final String ADD_FAVOURITE_SUPER_ARTIST = "AddFavoriteOrSuperArtist.json";
	public static final String GET_REFFERERCODE = "GetReffererCode.json";
	public static final String GET_SPORTS_CROWN_JEWEL_TICKETS = "GetSportsCrownJewelTickets.json";
	public static final String GET_CUSTOMER_PROFILE_PICTURE = "GetCustomerProfilePic.json";
	public static final String GET_CUSTOMER_ORDERS = "GetCustomerOrders.json";
	//public static final String VIEW_ORDERS = "ViewOrder.json";
	public static final String EVENT_SEARCH_BY_EXPLORE = "EventSearchByExplore.json";
	public static final String GET_ARTIST_BT_GRAND_CHILD = "GetArtistByGrandChildCateoryId.json";
	public static final String GENERALIZED_SEARCH = "GeneralizedSearch.json";
	public static final String GET_CITIES = "GetCities.json";
	public static final String SEARCH_ARTIST = "SearchArtist.json";
	public static final String FAVOURITE_ARTIST_EVENTS_BY_CUSTOMER = "FavoriteArtistEventInfoByCustomer.json";
	public static final String GET_CUSTOMER_REWARDS = "GetCustomerLoyaltyRewards.json";
	public static final String GET_ARTIST_INFO = "GetArtistInfo.json";
	public static final String GET_CUSTOMER_INFO = "GetCustomerInfo.json";
	public static final String UPDATE_CUSTOMER_PHONE_NUMBER = "UpdateCustomerPhoneNumber.json";
	public static final String GET_POPULAR_ARTIST_SUPERFAN = "GetPopularArtistForSuperFan.json";
	public static final String GET_LOYALFAN_DETAILS = "GetLoyalFanDetails.json";
	public static final String ADD_ARTIST_DETAILS = "AddFavoriteOrSuperArtist.json";
	public static final String EMAIL_CUSTOMER_DETAILS = "EmailCustomerRequestInfo.json";
	public static final String GET_CUSTOMER_CARDS = "GetCustomerCardsList.json";
	public static final String REMOVE_CUSTOMER_CARDS = "RemoveSavedCustomerCard.json";
	public static final String GET_CUSTOMER_REWARDS_BREAKUP = "GetCustomerRewardsBreakup.json";
	public static final String GET_PAYPAL_API_CREDENTIALS = "GetPayPalCredentials.json";
	public static final String REMOVE_CUSTOMER_ADDRESS = "RemoveCustomerAddress.json";
	public static final String GET_POPULAR_ARTIST_FAVORITE = "GetPopularArtistForFavorite.json";
	public static final String UNLOCK_TICKET_GROUP = "UnLockTicketGroup.json";
	public static final String CUSTOMER_FEDDBACK_SUPPORT = "CustomerFeedBackAndSupport.json";
	public static final String GET_STRIPE_CREDENTIALS = "GetStripeCredentials.json";
	public static final String GET_INVOICE_REFUNDS_LIST = "GetInvoiceRefundList.json";
	public static final String GET_INVOICE_DISPUTE_LIST = "GetStripeDisputes.json";
	public static final String INVOICE_REFUND = "InvoiceRefund.json";
	public static final String PAYPAL_REFUND_TRANSACTION_LIST = "PaypalRefundTranList.json";
	public static final String STRIPE_TRANSACTION = "StripeTransaction.json";
	public static final String STRIPE_TRANSACTION_REAL_TICKET = "StripeTransactionLongOrder.json";
	public static final String CUSTOMER_WALLET_TRANSACTION = "CustomerWalletTransaction.json";
	public static final String AUTOCOMPLETE_ARTIST = "SearchArtistOrTeamByKey.json";
	public static final String AUTOCOMPLETE_LOYALFAN = "LoyalFanStateAutoSearch.json";
	public static final String GET_LOYALFAN_STATUS = "LoyalFanPageStatus.json";
	public static final String UPDATE_LOYALFAN = "UpdateLoyalFan.json";
	public static final String VALIDATE_LOYALFAN = "TickTrackerLoyalFanValidator.json";
	public static final String GET_FANTASY_PRODUCT_CATEGORY = "GetFantasyProductCategories.json";
	public static final String GET_FANTASY_EVENTS = "ViewFantasyEvent.json";
	public static final String VALIDATE_FST_ORDER = "ValidateFSTPurchase.json";
	public static final String CREATE_FST_ORDER = "CreateFSTOrder.json";
	public static final String GET_TOTAL_USERCOUNT = "GetQuizContestCustomersCount.json";
	public static final String GET_TOTAL_USERCOUNT_CASS = "GetContCustCount.json";
	
	//Crown Jewel API'S
	public static final String GET_CROWN_JEWEL_LEAGUE = "GetSuperFanLeagues.json";
	public static final String GET_CROWN_JEWEL_TEAMS = "GetSuperFanTeams.json";
	public static final String GET_CROWN_JEWEL_EVENTS = "CrownJewelEvents.json";
	public static final String GET_CROWN_JEWEL_TICKETS = "GetOthersCrownJewelTickets.json"; //Except Sports
	public static final String GET_CROWN_JEWEL_ORDER_SUMMARY = "CrownJewelOrderSummary.json";
	public static final String GET_CROWN_JEWEL_FUTURE_ORDER_CONFIRMATION = "CrownJewelFutureOrderConfirmation.json";
	public static final String VIEW_CROWN_JEWEL_ORDER = "ViewOrder.json";
	public static final String CREATE_FANTASY_ORDER_POPUP = "CreateFantasyOrder.json";
	public static final String CROWN_JEWEL_ORDER_UPDATE_REASON = "UpdateCrownJewelOrder.json";
	public static final String GET_FANTASY_EVENTS_BY_GRANDCHILD = "GetFantasyEventsByGrandChild.json";
	public static final String CREATE_FANTASY_ORDER = "FantasyOrderCreation.json";
	//public static final String GET_CROWN_JEWEL_ORDERS = "GetCrownJewelOrders.json";
	public static final String GET_RESET_PASSWORD_REQUEST = "ResetPasswordRequest.json";
	public static final String GET_RESET_PASSWORD = "ResetPassword.json";
	public static final String GET_REFERRER_CODE = "GetReffererCode.json";
	public static final String VALIDATE_CUSTOMER_LOYAL_FAN = "ValidateCustomerLoyalFan.json";
	public static final String SEND_CUSTOMER_REFERRAL_CODE = "SendReferralCodeToOthers.json";
	public static final String VALIDATE_REFERRALCODE = "ValidateReferralCode.json";
	public static final String CANCEL_ORDER = "CancelOrder.json";
	public static final String QUIZ_GET_QUESTION_ANSWER = "GetQuestionAnswers.json";
	public static final String QUIZ_GET_QUESTION_ANSWER_CASS = "GetQuestAnsCount.json";
	
	public static final String QUIZ_GET_WINNERS = "GetQuizGrandWinners.json";
	public static final String QUIZ_GET_WINNERS_CASS = "GetGrandWinners.json";
	public static final String QUIZ_GET_SUMMARY = "GetQuizContestSummary.json";
	public static final String QUIZ_GET_SUMMARY_CASS = "GetContestSummary.json";
	public static final String QUIZ_GET_MINI_JACKPOT_WINNERS = "GetMiniJackpotWinners.json";
	public static final String QUIZ_GET_MEGA_JACKPOT_WINNERS = "GetMegaJackpotWinners.json";
	public static final String QUIZ_UPDATE_USER_STATISTICS = "UpdateCustomerContestStats.json";
	public static final String QUIZ_UPDATE_USER_STATISTICS_CASS = "UpdateCustContStats.json";
	public static final String CONTINUE_CONTEST = "ContinueContest.json";
	public static final String SEND_CONTEST_MANUAL_NOTIFICATIONS = "SendQuizManualNotification.json";
	public static final String GET_ONESIGNAL_NOTIFICATION = "GetOneSignalCustomers.json";
	public static final String GET_CONTEST_SERVER_NODES = "getContestServerNodeUrls.json";
	public static final String UPDATE_ONESIGNAL_NOTIFICATION = "UpdateOneSignalCustomers.json";
	public static final String BLOCK_CUSTOMER_COMMENT = "BlockCustomerComment.json";
	public static final String UNBLOCK_CUSTOMER = "UpdateCustomerUnBlock.json";
	
	
	public static final String TICTRACKER_API_GET_GIFT_CARDS = "GetGiftCards.json";
	public static final String TICTRACKER_API_UPDATE_GIFT_CARDS = "UpdateGiftCard.json";
	public static final String TICTRACKER_API_GET_GIFTCARD_ORDERS = "GetGiftCardOrders.json";
	public static final String TICTRACKER_API_EDIT_GIFTCARD_ORDERS = "EditGiftCardOrder.json";
	public static final String TICTRACKER_API_SAVE_GIFTCARD_ORDERS = "SaveGiftCardOrder.json";
	public static final String TICTRACKER_API_VOIDE_GIFTCARD_ORDERS = "VoidGiftCardOrders.json";
	public static final String TICTRACKER_API_DELETE_GIFTCARD_ORDER_ATTACHMENT = "DeleteGiftCardOrderAttachment.json";
	public static final String TICTRACKER_API_GET_GIFT_CARD_BRANDS = "GetGiftCardBrands.json";
	public static final String TICTRACKER_API_UPDATE_GIFT_CARD_BRANDS = "UpdateGiftcardBrand.json";
	
	
	public static final String TICTRACKER_API_GET_CUSTOMER_PROFILE_QUESTIONS= "GetCustomerProfileQuestions.json";
	public static final String TICTRACKER_API_CHANGE_CUSTOMER_PROFILE_QUESTIONS_POSITION= "ChangeCustomerProfileQuestionPosition.json";
	public static final String TICTRACKER_API_UPDATE_CUSTOMER_PROFILE_QUESTIONS= "UpdateCustomerProfileQuestion.json";
	public static final String TICTRACKER_API_GET_REWARD_CONFIG_SETTINGS= "GetRewardConfigDetails.json";
	public static final String TICTRACKER_API_UPDATE_REWARD_CONFIG_SETTINGS= "UpdateRewardConfigInfo.json";
	public static final String TICTRACKER_API_GET_POINT_CONVERSION_SETTINGS= "GetRtfPointsConvSetting.json";
	public static final String TICTRACKER_API_UPDATE_POINT_CONVERSION_SETTINGS= "UpdateRtfPointsConvSetting.json";
	public static final String TICTRACKER_API_GET_ABUSED_MEDIA= "GetAbusedMedias.json";
	public static final String TICTRACKER_API_UPDATE_ABUSED_MEDIA= "UpdateAbuseMedia.json";
	public static final String TICTRACKER_API_GET_ABUSED_COMMENT= "GetAbusedComments.json";
	public static final String TICTRACKER_API_UPDATE_ABUSED_COMMENT= "UpdateAbuseComment.json";
	public static final String TICTRACKER_API_GET_SUPERFAN_LEVEL_CONFIG= "GetSuperFanLevelConf.json";
	public static final String TICTRACKER_API_UPDATE_SUPERFAN_LEVEL_CONFIG= "UpdateSuperFanLevelConf.json";
	
	
	public static final String TFF_API_GET_FANCLUBS= "GetFanClubs.json";
	public static final String TFF_API_GET_FANCLUB_POSTS= "GetFanClubPosts.json";
	public static final String TFF_API_GET_FANCLUB_EVENTS= "GetFanClubEvents.json";
	public static final String TFF_API_GET_FANCLUB_VIDEOS= "GetFanClubVideos.json";
	public static final String TFF_API_GET_FANCLUB_MEMBERS= "GetFanClubMembers.json";
	
	public static final String TFF_API_UPDATE_FANCLUBS= "UpdateFanClubs.json";
	public static final String TFF_API_UPDATE_FANCLUB_POSTS= "UpdateFanClubPosts.json";
	public static final String TFF_API_UPDATE_FANCLUB_EVENTS= "UpdateFanClubEvents.json";
	public static final String TFF_API_UPDATE_FANCLUB_VIDEOS= "UpdateFanClubVideos.json";
	public static final String TFF_API_REMOVE_FANCLUB_MEMBER= "RemoveFanClubMember.json";
	public static final String TFF_API_GET_FANCLUB_POST_COMMENTS= "GetFanClubPostComments.json";
	public static final String TFF_API_UPDATE_FANCLUB_POST_COMMENTS= "UpdateFanClubPostComments.json";
	public static final String TFF_API_GET_FANCLUB_ABUSE = "GetFanClubAbuseData.json";
	public static final String TFF_API_UPDATE_FANCLUB_ABUSE_DATA= "UpdateFanClubAbuseData.json";
	
	public static final String TFF_API_GET_OFFLINE_TRIVIA= "GetOfflineTrivia.json";
	public static final String TFF_API_GET_OFFLINE_TRIVIA_QUESTION_BANK= "GetOfflineTriviaQuestionBank.json";
	
	
	
	
	
	
	
	
	// Polling Contest 
		public static final String TICTRACKER_API_GET_POLLING_SPONSORS = "GetPollingSponsors.json";
		public static final String TICTRACKER_API_UPDATE_POLLINGSPONSOR = "UpdatePollingSponsor.json";
		public static final String TICTRACKER_API_GET_POLLING_CATEGORY = "GetPollingCategory.json";
		public static final String TICTRACKER_API_GET_POLLING_CONTEST = "GetPollingContest.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_CONTEST = "UpdatePollingContest.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_SPONSORS = "UpdatePollingSponsors.json";
		public static final String TICTRACKER_API_GET_POLLING_PRIZE = "GetPollingRewards.json";
		public static final String TICTRACKER_API_GET_POLLING_CATEGORY_BY_CONTID = "GetPollingCategoryByContId.json";
		public static final String TICTRACKER_API_GET_POLLING_CATEGORY_SELECTION = "GetPollingCategorySelection.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_CATEGORY_MAPPER = "UpdatePollingCategoryMapper.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_PRIZE = "UpdatePollingPrizes.json";
		public static final String TICTRACKER_API_REMOVE_POLLING_CATEGORY_MAPPER = "RemovePollingCategoryMapper.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_CATEGORY = "UpdatePollingCategory.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_CATEGORY_QUESTIONS = "UpdatePollingCategoryQuestion.json";
		public static final String TICTRACKER_API_GET_POLLING_CATEGORY_QUESTIONS = "PollingCategoryQuestion.json";
		public static final String TICTRACKER_API_AUTO_COMPLETE_POLLING_SPONSOR = "AutoCompletePollingSponsor.json";
		public static final String TICTRACKER_API_GET_POLLING_VIDEOS = "GetAllPollingVideos.json";
		public static final String TICTRACKER_API_GET_POLLING_VIDEO_CATEGORY = "GetAllPollingVideoCategory.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_VIDEO_CATEGORY = "UpdatePollingVideoCategory.json";
		public static final String TICTRACKER_API_GET_CUSTOMER_MEDIA_URL = "GetCustomerMediaUrl.json";
		
		public static final String TICTRACKER_API_UPDATE_VIDEO_CATEGORY_SEQUENCE = "UpdateSequencePosition.json";
		public static final String TICTRACKER_API_UPDATE_POLLING_VIDEOS = "UpdatePollingVideo.json";		
		public static final String TICTRACKER_API_GET_POLLING_CUST_VIDEOS = "GetPollingCustomerVideos.json";
		public static final String TICTRACKER_API_MANAGE_POLLING_CUST_VIDEOS = "ManagePollingCustomerVideo.json";
		public static final String TICTRACKER_API_PUBLISH_CUSTOMER_MEDIA= "PublishCustomerMediaToRTFMedia.json";
		public static final String TICTRACKER_API_DOWNLOAD_POLLING_CUST_VIDEOS = "DownloadPollingCustomerVideo.json";
		public static final String TICTRACKER_API_POLLING_REPORT = "GetPollingStatsReport.json";
		public static final String TICTRACKER_API_SUPERFAN_STARS_REPORT = "GetSuperFanStarsReport.json";
		public static final String TICTRACKER_API_TICKET_COST_REPORT = "GetTicketCostReport.json";
		public static final String TICTRACKER_API_BOTS_CUST_REPORT = "GetBotsCustomerReport.json";
		public static final String TICTRACKER_API_CONTEST_PARTICIPANT_REPORT = "GetContestParticipantReport.json";
		public static final String TICTRACKER_API_QUE_WISE_USER_ANS_REPORT = "GetQueWiseUserAnswerCountReport.json";
		public static final String TICTRACKER_API_GET_POPULAR_CHANNELS = "GetFanClubDashBoardInfo.json";
		
		// Polling Contest
		
		
		//ECOMM =========================================================
		
		public static final String GET_SELLERS = "GetRtfSellerDtls.json"; 
		public static final String UPDATE_SELLERS = "UpdateRtfSetllerDtl.json";
		public static final String GET_PRODUCTS = "getpditemaster.json";
		public static final String UPDATE_PRODUCT = "updatepditemaster.json";
		public static final String UPDATE_PROD_OPTION = "addprodttmvaroptns.json";
		
		public static final String TICTRACKER_API_GET_COUPON_CODES = "GetRtfCouponCodes.json";
		public static final String TICTRACKER_API_MANAGE_COUPON_CODES = "updateditcoupcodemaster.json";
		public static final String TICTRACKER_API_UPLOAD_RTFINVENTORY = "UploadRTFInventory.json";
		public static final String TICTRACKER_API_GETCOUPONCODE_MAPPING = "GetRtfCouponCodeProductsMap.json";
		public static final String TICTRACKER_API_SAVECOUPONCODE_MAPPING = "SaveRtfCouponCodeProductsMap.json";
		public static final String TICTRACKER_API_REMOVECOUPONCODE_MAPPING = "RemoveRtfCouponCodeProductsMap.json";
		
		public static final String RTF_ORDER_PROD_REFUND = "OrderProdRefund.json";
		//ECOMM =========================================================
		
		public static final String GET_MANAGE_ORDERS = "GetProdOrderManage.json";
		public static final String GET_ORDER_PRODUCTS_LIST = "GetCustOrderProducts.json";
		public static final String GET_ORDER_PRODUCT = "GetOrderProduct.json";
		public static final String UPDATE_ORDER_PRODUCT_SHIPPING = "UpdateOrderProductShpipping.json";
		public static final String VOID_ORDER_PRODUCT = "VoidOrderProduct.json";
		public static final String UPDATE_ORDER_PRODUCT_STATUS = "UpdateOrderProductStatus.json";
		public static final String UPDATE_ORDER_PRODUCT_TO_DELIVERED = "UpdateOrderProductToDelivered.json";
		
		public static final String GET_PROD_ITM_VARIANT_OPTIONS = "getproditmvaroptns.json";
		public static final String GENERATE_PROD_ITM_INVENTORY = "genProdInventory.json";
		public static final String GET_PROD_ITM_INVENTORIES = "getProdInventories.json";
		public static final String UPDATE_PROD_ITM_INVENTORIES = "updateProdInventories.json";
		public static final String ADD_PROD_ITM_VARIANT_OPTIONS = "addproditmvaroptns.json";
	
	
	public static final String TICKET_DOWNLOAD_NOTIFICATION = "TicketDownloadNotification.json";
	//generalized error message
	public static final String GENERALIZED_ERROR_MSG = "whoops! Something Went Wrong. Please try again later.";
	
	//Fedex property
	public static final String FEDEX_LABEL_DIRECTORY = "fedex.label.filepath";
	public static final String FEDEX_LABEL_MANUAL_DIRECTORY = "fedex.label.manual.filepath";
	public static final String FEDEX_ACCOUNT_NO = "fedex.accountnumber";
	public static final String FEDEX_METER_NO = "fedex.meternumber";
	public static final String FEDEX_KEY = "fedex.key";
	public static final String FEDEX_PASSWORD = "fedex.password";
	public static final String FEDEX_SHIPPING_URL = "fedex.shiping.url";
	public static final String FEDEX_TRACKING_URL = "fedex.tracking.url";
	
	
}
