package com.rtw.tracker.utils;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;

public class GridHeaderFilters {

	private String artistName;
	private String grandChildCategoryName;
	private String childCategoryName;
	private String parentCategoryName;
	private Integer visibleOnSearch;
	private String promoCode;
	private String promoType;
	private Integer maxOrders;
	private Integer noOfOrders;
	private String startDate;
	private String endDate;
	private Integer eventCount;
	
		
	private String customerType;
	private String retailCustomer;
	private String customerName;
	private String lastName;
	private String Email;
	private String emailSent;
	private String productType;
	private String signupType;
	private String customerStatus;
	private String companyName;
	private String addressLine1;
	private String addressLine2; //Invoice Creation Customer Grid
	private String city;
	private String state;
	private String country;
	private String zipCode;
	private String phone;
	private Integer isClient;
	private Integer isBroker;
	private String referrerCode;
	private String purchaseOrderType;
	private String purchaseOrderDateStr;
	private Double eventTicketCost;
	
	private Integer purchaseOrderId; //purchaseOrderNo
	private Date createdDate; //createdDate
	private Double poTotal;
	private Integer ticketQty; //ticketCount
	private String shippingType;
	private String status;
	private String csr;
	private Date lastUpdatedDate;
	private Integer poAged;
	private String createdBy;
	private Integer isEmailed;
	private String consignmentPoNo;
	private String transactionOffice;
	private String trackingNo;
	private String shippingNotes;
	private String externalNotes;
	private String internalNotes;	
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private Time eventTime;
	private String eventDateStr;
	private String eventTimeStr;
	private String createdDateStr;
	private String lastUpdatedDateStr;
	
	private Integer invoiceId;
	private Integer customerId;
	private String username;	
	private Double invoiceTotal;
	private Integer invoiceAged;
	private Integer customerOrderId;	
	private String shippingMethod;
	private String secondaryOrderType;
	private String secondaryOrderId;	
	private String lastUpdatedBy;
	private Date voidedDate;
	private String voidedDateStr;
	private String viewFedexLabel;
	private Integer externalNo; // Invoice Grid - Customer Screen
	
	private String seatgeekOrderId;
	private Date orderDate;
	private String orderDateStr;
	private String venueName;
	private String section;
	private String row;
	private Integer quantity;
	private Double totalSaleAmt;
	private Double totalPaymentAmt;
	
	private Date invoiceDate;
	private String invoiceDateStr;
	private Double actualSoldPrice; 
	private Double marketPrice;
	private Double lastUpdatedPrice;
	private Integer sectionTixQty;
	private Integer eventTixQty;
	private Double totalActualSoldPrice; 
	private Double totalMarketPrice;
	private Double profitAndLoss;;
	private Integer priceUpdateCount;
	private Double price;
	private Double discountCouponPrice;
	private Double mobileDiscount;
	private String url;
	
	private String dayOfWeek;
	private Integer venueId;
	private Integer noOfTixCount;
	private Integer noOfTixSoldCount;
	
	private String seatLow;
	private String seatHigh;
	private String nearTermDisplayOption;
	private Double facePrice;
	private Double cost;
	private Double wholeSalePrice;
	private Double retailPrice;
	private Integer broadcast;
	private String marketPlaceNotes;
	private Double taxAmount;
	private String ticketType;
	private String sectionRange;
	private String rowRange;
	
	Integer zoneTixQty;
	Double zoneCheapestPrice;
	Integer zoneTixGroupCount;
	Double zoneTotalPrice;
	Double zoneProfitAndLoss;
	Double netTotalSoldPrice;
	Double zoneMargin;
	Double sectionMargin;
	Double netSoldPrice;
	
	private String customerIpAddress;
	private String webConfigId;
	private String actionType;
	private String authenticatedHit;
	private String actionResult;
	private Date hittingDate;
	private String hittingDateStr;
	
	private String zone;
	private Double orderTotal;
	private String orderType;
	private String platform;
	private String paymentId;
	private String paypalTransactionId;
	private Integer transactionId;
	private Date transactionDate;
	private String transactionDateStr;
	
	private String userRole;
	
	private Integer childCategoryId;
	private Integer grandChildCategoryId;
	
	private Double rewardPoints;
	
	private Integer id;
	private String leagueName;
	private String teamName;
	private String leagueCity;
	private Integer isPackageSelected;
	private Integer ticketGroupId;
	private String ticketSelection;
	private Double ticketPoints;
	private Double requirePoints;
	private Double packagePoints;
	private Double remainingPoints;
	private String posPOId;
	
	private Double salePrice;
	private Date expirationDate;
	private String expirationDateStr;
	private Date holdDate;
	private String holdDateStr;
	private Integer expirationMinutes;
	private Integer shippingMethodId;
	private String ticketIdsStr;
	
	private Integer categoryTicketGroupId;
	private Integer userId;
	private Integer brokerId;
	private Double cashCredited;
	private Double activeCash;
	private Double pendingCash;
	private Double lastCreditedCash;
	private Double lastDebitedCash;
	private Double totalCreditedCash;
	private Double totalDebitedCash;
	
	private String primaryPaymentMethod;
	private String secondaryPaymentMethod;
	private String thirdPaymentMethod;
	private Double packageCost;
	
	private Integer ticketId;
	private Integer teamId;
	private Integer teamZoneId;
	
	private String shCustomerName;
	private String shCompanyName;
	private String shAddressLine1;
	private String shAddressLine2;
	private String shCity;
	private String shState;
	private String shCountry;
	private String shZipCode;
	private String shPhone;
	private String serviceType;
	
	private String contestName;
	private Double minimumPurchaseAmount;
	private Integer contestId;
	private Integer purchaseCustomerId;
	private String purchaseCustomerName;
	private String purchaseCustomerEmail;
	
	private String userAction;
	private String clientIPAddress;
	private String message;
	
	public String getShCustomerName() {
		return shCustomerName;
	}

	public void setShCustomerName(String shCustomerName) {
		this.shCustomerName = shCustomerName;
	}

	public String getShCompanyName() {
		return shCompanyName;
	}

	public void setShCompanyName(String shCompanyName) {
		this.shCompanyName = shCompanyName;
	}

	public String getShAddressLine1() {
		return shAddressLine1;
	}

	public void setShAddressLine1(String shAddressLine1) {
		this.shAddressLine1 = shAddressLine1;
	}

	public String getShAddressLine2() {
		return shAddressLine2;
	}

	public void setShAddressLine2(String shAddressLine2) {
		this.shAddressLine2 = shAddressLine2;
	}

	public String getShCity() {
		return shCity;
	}

	public void setShCity(String shCity) {
		this.shCity = shCity;
	}

	public String getShState() {
		return shState;
	}

	public void setShState(String shState) {
		this.shState = shState;
	}

	public String getShCountry() {
		return shCountry;
	}

	public void setShCountry(String shCountry) {
		this.shCountry = shCountry;
	}

	public String getShZipCode() {
		return shZipCode;
	}

	public void setShZipCode(String shZipCode) {
		this.shZipCode = shZipCode;
	}

	public String getShPhone() {
		return shPhone;
	}

	public void setShPhone(String shPhone) {
		this.shPhone = shPhone;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getLeagueCity() {
		return leagueCity;
	}

	public void setLeagueCity(String leagueCity) {
		this.leagueCity = leagueCity;
	}

	public Integer getIsPackageSelected() {
		return isPackageSelected;
	}

	public void setIsPackageSelected(Integer isPackageSelected) {
		this.isPackageSelected = isPackageSelected;
	}

	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	public String getTicketSelection() {
		return ticketSelection;
	}

	public void setTicketSelection(String ticketSelection) {
		this.ticketSelection = ticketSelection;
	}

	public Double getTicketPoints() {
		return ticketPoints;
	}

	public void setTicketPoints(Double ticketPoints) {
		this.ticketPoints = ticketPoints;
	}

	public Double getRequirePoints() {
		return requirePoints;
	}

	public void setRequirePoints(Double requirePoints) {
		this.requirePoints = requirePoints;
	}

	public Double getPackagePoints() {
		return packagePoints;
	}

	public void setPackagePoints(Double packagePoints) {
		this.packagePoints = packagePoints;
	}

	public Double getRemainingPoints() {
		return remainingPoints;
	}

	public void setRemainingPoints(Double remainingPoints) {
		this.remainingPoints = remainingPoints;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}

	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}

	public Integer getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}

	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}

	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}

	public String getParentCategoryName() {
		return parentCategoryName;
	}

	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}

	public Integer getVisibleOnSearch() {
		return visibleOnSearch;
	}

	public void setVisibleOnSearch(Integer visibleOnSearch) {
		this.visibleOnSearch = visibleOnSearch;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSignupType() {
		return signupType;
	}

	public void setSignupType(String signupType) {
		this.signupType = signupType;
	}

	public String getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getIsClient() {
		return isClient;
	}

	public void setIsClient(Integer isClient) {
		this.isClient = isClient;
	}

	public Integer getIsBroker() {
		return isBroker;
	}

	public void setIsBroker(Integer isBroker) {
		this.isBroker = isBroker;
	}
	
	

	public String getPurchaseOrderDateStr() {
		return purchaseOrderDateStr;
	}

	public void setPurchaseOrderDateStr(String purchaseOrderDateStr) {
		this.purchaseOrderDateStr = purchaseOrderDateStr;
	}

	public String getReferrerCode() {
		return referrerCode;
	}

	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Double getPoTotal() {
		return poTotal;
	}

	public void setPoTotal(Double poTotal) {
		this.poTotal = poTotal;
	}

	public Integer getTicketQty() {
		return ticketQty;
	}

	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}

	public String getShippingType() {
		return shippingType;
	}

	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Integer getPoAged() {
		return poAged;
	}

	public void setPoAged(Integer poAged) {
		this.poAged = poAged;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getIsEmailed() {
		return isEmailed;
	}

	public void setIsEmailed(Integer isEmailed) {
		this.isEmailed = isEmailed;
	}

	public String getConsignmentPoNo() {
		return consignmentPoNo;
	}

	public void setConsignmentPoNo(String consignmentPoNo) {
		this.consignmentPoNo = consignmentPoNo;
	}

	public String getTransactionOffice() {
		return transactionOffice;
	}

	public void setTransactionOffice(String transactionOffice) {
		this.transactionOffice = transactionOffice;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getShippingNotes() {
		return shippingNotes;
	}

	public void setShippingNotes(String shippingNotes) {
		this.shippingNotes = shippingNotes;
	}

	public String getExternalNotes() {
		return externalNotes;
	}

	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventDateStr() {
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	public String getEventTimeStr() {
		return eventTimeStr;
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getLastUpdatedDateStr() {
		return lastUpdatedDateStr;
	}

	public void setLastUpdatedDateStr(String lastUpdatedDateStr) {
		this.lastUpdatedDateStr = lastUpdatedDateStr;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Double getInvoiceTotal() {
		return invoiceTotal;
	}

	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}

	public Integer getInvoiceAged() {
		return invoiceAged;
	}

	public void setInvoiceAged(Integer invoiceAged) {
		this.invoiceAged = invoiceAged;
	}

	public Integer getCustomerOrderId() {
		return customerOrderId;
	}

	public void setCustomerOrderId(Integer customerOrderId) {
		this.customerOrderId = customerOrderId;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getSecondaryOrderType() {
		return secondaryOrderType;
	}

	public void setSecondaryOrderType(String secondaryOrderType) {
		this.secondaryOrderType = secondaryOrderType;
	}

	public String getSecondaryOrderId() {
		return secondaryOrderId;
	}

	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getVoidedDate() {
		return voidedDate;
	}

	public void setVoidedDate(Date voidedDate) {
		this.voidedDate = voidedDate;
	}

	public String getVoidedDateStr() {
		return voidedDateStr;
	}

	public void setVoidedDateStr(String voidedDateStr) {
		this.voidedDateStr = voidedDateStr;
	}

	public String getViewFedexLabel() {
		return viewFedexLabel;
	}

	public void setViewFedexLabel(String viewFedexLabel) {
		this.viewFedexLabel = viewFedexLabel;
	}

	public String getSeatgeekOrderId() {
		return seatgeekOrderId;
	}

	public void setSeatgeekOrderId(String seatgeekOrderId) {
		this.seatgeekOrderId = seatgeekOrderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderDateStr() {
		return orderDateStr;
	}

	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getTotalSaleAmt() {
		return totalSaleAmt;
	}

	public void setTotalSaleAmt(Double totalSaleAmt) {
		this.totalSaleAmt = totalSaleAmt;
	}

	public Double getTotalPaymentAmt() {
		return totalPaymentAmt;
	}

	public void setTotalPaymentAmt(Double totalPaymentAmt) {
		this.totalPaymentAmt = totalPaymentAmt;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceDateStr() {
		return invoiceDateStr;
	}

	public void setInvoiceDateStr(String invoiceDateStr) {
		this.invoiceDateStr = invoiceDateStr;
	}

	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}

	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}

	public Double getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}

	public Double getLastUpdatedPrice() {
		return lastUpdatedPrice;
	}

	public void setLastUpdatedPrice(Double lastUpdatedPrice) {
		this.lastUpdatedPrice = lastUpdatedPrice;
	}

	public Integer getSectionTixQty() {
		return sectionTixQty;
	}

	public void setSectionTixQty(Integer sectionTixQty) {
		this.sectionTixQty = sectionTixQty;
	}

	public Integer getEventTixQty() {
		return eventTixQty;
	}

	public void setEventTixQty(Integer eventTixQty) {
		this.eventTixQty = eventTixQty;
	}

	public Double getTotalActualSoldPrice() {
		return totalActualSoldPrice;
	}

	public void setTotalActualSoldPrice(Double totalActualSoldPrice) {
		this.totalActualSoldPrice = totalActualSoldPrice;
	}

	public Double getTotalMarketPrice() {
		return totalMarketPrice;
	}

	public void setTotalMarketPrice(Double totalMarketPrice) {
		this.totalMarketPrice = totalMarketPrice;
	}

	public Double getProfitAndLoss() {
		return profitAndLoss;
	}

	public void setProfitAndLoss(Double profitAndLoss) {
		this.profitAndLoss = profitAndLoss;
	}

	public Integer getPriceUpdateCount() {
		return priceUpdateCount;
	}

	public void setPriceUpdateCount(Integer priceUpdateCount) {
		this.priceUpdateCount = priceUpdateCount;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscountCouponPrice() {
		return discountCouponPrice;
	}

	public void setDiscountCouponPrice(Double discountCouponPrice) {
		this.discountCouponPrice = discountCouponPrice;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public Integer getNoOfTixCount() {
		return noOfTixCount;
	}

	public void setNoOfTixCount(Integer noOfTixCount) {
		this.noOfTixCount = noOfTixCount;
	}

	public Integer getNoOfTixSoldCount() {
		return noOfTixSoldCount;
	}

	public void setNoOfTixSoldCount(Integer noOfTixSoldCount) {
		this.noOfTixSoldCount = noOfTixSoldCount;
	}

	public String getSeatLow() {
		return seatLow;
	}

	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}

	public String getSeatHigh() {
		return seatHigh;
	}

	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}

	public String getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}

	public void setNearTermDisplayOption(String nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}

	public Double getFacePrice() {
		return facePrice;
	}

	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getWholeSalePrice() {
		return wholeSalePrice;
	}

	public void setWholeSalePrice(Double wholeSalePrice) {
		this.wholeSalePrice = wholeSalePrice;
	}

	public Double getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}

	public Integer getBroadcast() {
		return broadcast;
	}

	public void setBroadcast(Integer broadcast) {
		this.broadcast = broadcast;
	}

	public String getMarketPlaceNotes() {
		return marketPlaceNotes;
	}

	public void setMarketPlaceNotes(String marketPlaceNotes) {
		this.marketPlaceNotes = marketPlaceNotes;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getSectionRange() {
		return sectionRange;
	}

	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}

	public String getRowRange() {
		return rowRange;
	}

	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}

	public String getCustomerIpAddress() {
		return customerIpAddress;
	}

	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}

	public String getWebConfigId() {
		return webConfigId;
	}

	public void setWebConfigId(String webConfigId) {
		this.webConfigId = webConfigId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getAuthenticatedHit() {
		return authenticatedHit;
	}

	public void setAuthenticatedHit(String authenticatedHit) {
		this.authenticatedHit = authenticatedHit;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public Date getHittingDate() {
		return hittingDate;
	}

	public void setHittingDate(Date hittingDate) {
		this.hittingDate = hittingDate;
	}

	public String getHittingDateStr() {
		return hittingDateStr;
	}

	public void setHittingDateStr(String hittingDateStr) {
		this.hittingDateStr = hittingDateStr;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaypalTransactionId() {
		return paypalTransactionId;
	}

	public void setPaypalTransactionId(String paypalTransactionId) {
		this.paypalTransactionId = paypalTransactionId;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionDateStr() {
		return transactionDateStr;
	}

	public void setTransactionDateStr(String transactionDateStr) {
		this.transactionDateStr = transactionDateStr;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getPurchaseOrderType() {
		return purchaseOrderType;
	}

	public void setPurchaseOrderType(String purchaseOrderType) {
		this.purchaseOrderType = purchaseOrderType;
	}

	public Integer getExternalNo() {
		return externalNo;
	}

	public void setExternalNo(Integer externalNo) {
		this.externalNo = externalNo;
	}

	public Integer getZoneTixQty() {
		return zoneTixQty;
	}

	public void setZoneTixQty(Integer zoneTixQty) {
		this.zoneTixQty = zoneTixQty;
	}

	public Double getZoneCheapestPrice() {
		return zoneCheapestPrice;
	}

	public void setZoneCheapestPrice(Double zoneCheapestPrice) {
		this.zoneCheapestPrice = zoneCheapestPrice;
	}

	public Integer getZoneTixGroupCount() {
		return zoneTixGroupCount;
	}

	public void setZoneTixGroupCount(Integer zoneTixGroupCount) {
		this.zoneTixGroupCount = zoneTixGroupCount;
	}

	public Double getZoneTotalPrice() {
		return zoneTotalPrice;
	}

	public void setZoneTotalPrice(Double zoneTotalPrice) {
		this.zoneTotalPrice = zoneTotalPrice;
	}

	public Double getZoneProfitAndLoss() {
		return zoneProfitAndLoss;
	}

	public void setZoneProfitAndLoss(Double zoneProfitAndLoss) {
		this.zoneProfitAndLoss = zoneProfitAndLoss;
	}

	public Double getNetTotalSoldPrice() {
		return netTotalSoldPrice;
	}

	public void setNetTotalSoldPrice(Double netTotalSoldPrice) {
		this.netTotalSoldPrice = netTotalSoldPrice;
	}

	public Double getZoneMargin() {
		return zoneMargin;
	}

	public void setZoneMargin(Double zoneMargin) {
		this.zoneMargin = zoneMargin;
	}

	public Double getSectionMargin() {
		return sectionMargin;
	}

	public void setSectionMargin(Double sectionMargin) {
		this.sectionMargin = sectionMargin;
	}

	public Double getNetSoldPrice() {
		return netSoldPrice;
	}

	public void setNetSoldPrice(Double netSoldPrice) {
		this.netSoldPrice = netSoldPrice;
	}


	public Double getEventTicketCost() {
		return eventTicketCost;
	}

	public void setEventTicketCost(Double eventTicketCost) {
		this.eventTicketCost = eventTicketCost;
	}

	public String getPosPOId() {
		return posPOId;
	}

	public void setPosPOId(String posPOId) {
		this.posPOId = posPOId;
	}

	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getExpirationDateStr() {
		return expirationDateStr;
	}

	public void setExpirationDateStr(String expirationDateStr) {
		this.expirationDateStr = expirationDateStr;
	}

	public Date getHoldDate() {
		return holdDate;
	}

	public void setHoldDate(Date holdDate) {
		this.holdDate = holdDate;
	}

	public String getHoldDateStr() {
		return holdDateStr;
	}

	public void setHoldDateStr(String holdDateStr) {
		this.holdDateStr = holdDateStr;
	}

	public Integer getExpirationMinutes() {
		return expirationMinutes;
	}

	public void setExpirationMinutes(Integer expirationMinutes) {
		this.expirationMinutes = expirationMinutes;
	}

	public Integer getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	public String getTicketIdsStr() {
		return ticketIdsStr;
	}

	public void setTicketIdsStr(String ticketIdsStr) {
		this.ticketIdsStr = ticketIdsStr;
	}

	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}

	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public Double getCashCredited() {
		return cashCredited;
	}

	public void setCashCredited(Double cashCredited) {
		this.cashCredited = cashCredited;
	}

	public Double getActiveCash() {
		return activeCash;
	}

	public void setActiveCash(Double activeCash) {
		this.activeCash = activeCash;
	}

	public Double getPendingCash() {
		return pendingCash;
	}

	public void setPendingCash(Double pendingCash) {
		this.pendingCash = pendingCash;
	}

	public Double getLastCreditedCash() {
		return lastCreditedCash;
	}

	public void setLastCreditedCash(Double lastCreditedCash) {
		this.lastCreditedCash = lastCreditedCash;
	}

	public Double getLastDebitedCash() {
		return lastDebitedCash;
	}

	public void setLastDebitedCash(Double lastDebitedCash) {
		this.lastDebitedCash = lastDebitedCash;
	}

	public Double getTotalCreditedCash() {
		return totalCreditedCash;
	}

	public void setTotalCreditedCash(Double totalCreditedCash) {
		this.totalCreditedCash = totalCreditedCash;
	}

	public Double getTotalDebitedCash() {
		return totalDebitedCash;
	}

	public void setTotalDebitedCash(Double totalDebitedCash) {
		this.totalDebitedCash = totalDebitedCash;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoType() {
		return promoType;
	}

	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}

	public Integer getMaxOrders() {
		return maxOrders;
	}

	public void setMaxOrders(Integer maxOrders) {
		this.maxOrders = maxOrders;
	}

	public Integer getNoOfOrders() {
		return noOfOrders;
	}

	public void setNoOfOrders(Integer noOfOrders) {
		this.noOfOrders = noOfOrders;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}

	public void setPrimaryPaymentMethod(String primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}

	public String getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}

	public void setSecondaryPaymentMethod(String secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}

	public String getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}

	public void setThirdPaymentMethod(String thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}

	public Double getPackageCost() {
		return packageCost;
	}

	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	
	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public Integer getTeamZoneId() {
		return teamZoneId;
	}

	public void setTeamZoneId(Integer teamZoneId) {
		this.teamZoneId = teamZoneId;
	}

	public Integer getEventCount() {
		return eventCount;
	}

	public void setEventCount(Integer eventCount) {
		this.eventCount = eventCount;
	}

	public Double getMobileDiscount() {
		return mobileDiscount;
	}

	public void setMobileDiscount(Double mobileDiscount) {
		this.mobileDiscount = mobileDiscount;
	}

	public String getRetailCustomer() {
		return retailCustomer;
	}

	public void setRetailCustomer(String retailCustomer) {
		this.retailCustomer = retailCustomer;
	}

	public String getEmailSent() {
		return emailSent;
	}

	public void setEmailSent(String emailSent) {
		this.emailSent = emailSent;
	}

	public String getContestName() {
		return contestName;
	}

	public void setContestName(String contestName) {
		this.contestName = contestName;
	}

	public Double getMinimumPurchaseAmount() {
		return minimumPurchaseAmount;
	}

	public void setMinimumPurchaseAmount(Double minimumPurchaseAmount) {
		this.minimumPurchaseAmount = minimumPurchaseAmount;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getPurchaseCustomerId() {
		return purchaseCustomerId;
	}

	public void setPurchaseCustomerId(Integer purchaseCustomerId) {
		this.purchaseCustomerId = purchaseCustomerId;
	}

	public String getPurchaseCustomerName() {
		return purchaseCustomerName;
	}

	public void setPurchaseCustomerName(String purchaseCustomerName) {
		this.purchaseCustomerName = purchaseCustomerName;
	}

	public String getPurchaseCustomerEmail() {
		return purchaseCustomerEmail;
	}

	public void setPurchaseCustomerEmail(String purchaseCustomerEmail) {
		this.purchaseCustomerEmail = purchaseCustomerEmail;
	}

	public String getUserAction() {
		return userAction;
	}

	public void setUserAction(String userAction) {
		this.userAction = userAction;
	}

	public String getClientIPAddress() {
		return clientIPAddress;
	}

	public void setClientIPAddress(String clientIPAddress) {
		this.clientIPAddress = clientIPAddress;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
