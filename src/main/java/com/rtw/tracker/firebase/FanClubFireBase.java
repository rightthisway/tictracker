package com.rtw.tracker.firebase;

public class FanClubFireBase {
	
	private Integer NOM;
	private Integer id;
	private String imageUrl;
	private String name ;
	private String shareText="My user id = USERID";
	
	public Integer getNOM() {
		return NOM;
	}
	public void setNOM(Integer NOM) {
		this.NOM = NOM;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShareText() {
		return shareText;
	}
	public void setShareText(String shareText) {
		this.shareText = shareText;
	}
	
	

}
