package com.rtw.tracker.firebase;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.Model;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.pojos.AnswerCountInfo;
import com.rtw.tracker.pojos.CassContestWinners;
import com.rtw.tracker.pojos.ContestsDTO;
import com.rtw.tracker.pojos.FanClubDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

public class FirestoreUtil implements InitializingBean {

	public static Boolean isContestStarted = false;
	//private static DatabaseReference firebase = null;

	private static CollectionReference rtfCollection = null;
	private static CollectionReference hostCollection = null;
	private static DocumentReference questionRef = null;
	private static DocumentReference contestDetails = null;
	private static DocumentReference answerCount = null;
	private static DocumentReference totalUserCount = null;
	private static DocumentReference summaryWinners = null;
	private static DocumentReference summaryWinnerCount = null;
	private static DocumentReference grandWinners = null;
	private static DocumentReference contestInfo = null;
	//private static DocumentReference contestInfoWeb = null;
	private static DocumentReference updateDashbord = null;
	//private static DocumentReference CorrectOpt = null;
	private static DocumentReference jackpotWinner = null;
	private static DocumentReference polling = null;
	private static DocumentReference appSettings = null;

	private static Logger fireStoreLog = LoggerFactory.getLogger(FirebaseUtil.class);

	public SharedProperty sharedProperty;

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		initialiseFirebase();

	}

	/*
	 * public static void setAppProperty(String key,Model model){ try { appProperty
	 * = firebase.document("appProperties"); Map<String,Object> map = new
	 * HashMap<String, Object>(); map.put("uMsg",key);
	 * appProperty.update(map); model.addAttribute("status",1);
	 * fireStoreLog.info("FIREBASE SECRET KEY UDAPTED."); } catch (Exception e) {
	 * e.printStackTrace(); model.addAttribute("status",0);
	 * model.addAttribute("msg",
	 * "ERROR OCCURED WHILE UPDATING SECRET KEY ON FIREBASE");
	 * fireStoreLog.error("ERROR OCCURED WHILE UPDATING SECRET KEY ON FIREBASE"); } }
	 */

	public static void updateNextQuestion(ContestQuestions q, Model model) {
		Date timeStamp = new Date();
		try {
			questionRef = rtfCollection.document("Question");
			//CorrectOpt = firebase.document("Tpoc");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", q.getId());
			qMap.put("qNo", q.getSerialNo());
			qMap.put("text", q.getQuestion());
			qMap.put("optionA", q.getOptionA());
			qMap.put("optionB", q.getOptionB());
			qMap.put("optionC", q.getOptionC());
			qMap.put("answer", q.getAnswer());
			qMap.put("timeStamp", timeStamp.getTime());
			qMap.put("eraseOption", q.getWrongAnswer());

			questionRef.set(qMap);
			/*if (q.getCorrectAnswer() != null && !q.getCorrectAnswer().isEmpty()) {
				Map<String, Object> aMap = new HashMap<String, Object>();
				// aMap.put("uChar", RtwEncoder.encodePassword(q.getCorrectAnswer()));
				aMap.put("uChar", q.getCorrectAnswer());
				aMap.put("rewards", q.getQuestionReward());
				CorrectOpt.set(aMap);
			}*/

			model.addAttribute("status", 1);
			fireStoreLog.info("CONTEST " + q.getAnswer() + ":  --qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo());
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo() + " -- " + q.getAnswer());
			e.printStackTrace();
		}
	}

	public static void updateTotalUserCount(Integer count) {
		try {
			totalUserCount = rtfCollection.document("TotalUserCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("count", count);
			totalUserCount.set(aMap);
			updateTotalUserCountHost(count);
			// fireStoreLog.info("TOTAL COUNT UPDATED: "+count);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING TOTAL USER COUNT.");
		}
	}

	public static void updateLiveStreamUrl(String url) {
		try {
			contestInfo = rtfCollection.document("dashboardInfoNew");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("videoUrl", url);
			contestInfo.update(vMap);
			fireStoreLog.info("LIVE STREAM URL UPDATED: " + url);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING LIVE STREAM URL.");
		}
	}

	public static void setContestStarted(Boolean isStarted) {
		try {
			contestInfo = rtfCollection.document("dashboardInfoNew");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("isContestStarted", isStarted);
			isContestStarted = isStarted;
			contestInfo.update(vMap);
			// setHoulryGameEnabled(false);
			fireStoreLog.info("CONTEST STARTED/STOPPED: " + isStarted);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING CONTEST STARTED FLAG.");
		}
	}

	public static void setContestDetails(Contests c, Model model) {
		try {
			contestDetails = rtfCollection.document("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", 0);
			cMap.put("contestName", c.getContestName());
			cMap.put("extendedName", c.getExtendedName());
			cMap.put("contestFreeTickets", c.getFreeTicketPerWinner());
			cMap.put("questionSize", c.getQuestionSize());
			cMap.put("rewardPrize", c.getRewardPoints());
			cMap.put("id", c.getId());
			contestDetails.set(cMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("CONTEST DETAILS UPDATED: -- coid: " + c.getId());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE SETTING CONTEST DETAILS.");
			fireStoreLog.error("ERROR OCCURED WHILE SETTING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}

	public static void updateContestDetailsQNo(ContestQuestions q, Model model) {
		try {
			contestDetails = rtfCollection.document("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", q.getSerialNo());
			contestDetails.update(cMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("UPDATED CONTEST DETAILS: -- qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE UPDATING CONTEST DETAILS QUESTION NO.");
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING CONTEST DETAILS QUESTION NO.");
			e.printStackTrace();
		}
	}

	public static void setAnswerCount(AnswerCountInfo a, Model model) {
		Date timeStamp = new Date();
		try {
			answerCount = rtfCollection.document("UserCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("optionACount", a.getaCount());
			aMap.put("optionBCount", a.getbCount());
			aMap.put("optionCCount", a.getcCount());
			aMap.put("rewardDollar", a.getQuRwds());
			aMap.put("timeStamp", timeStamp.getTime());
			answerCount.set(aMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("UPDATED ANSWER COUNT.");
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING ANSWER COUNT.");
			fireStoreLog.error("ERROR OCCURED UPDATING ANSWER COUNT.");
			e.printStackTrace();
		}
	}

	public static void setSummaryWinners(List<CassContestWinners> summaryWinner, Integer count, Model model) {
		try {
			summaryWinners = rtfCollection.document("SummaryWinners");
			summaryWinnerCount = rtfCollection.document("SummaryWinnerCount");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				Map<String, Object> aMap = new HashMap<String, Object>();
				aMap.put("count", count);
				List<Map<String, Object>> summaryList = new ArrayList<Map<String,Object>>();
				Map<String, Object> sMap = null;
				
				for(CassContestWinners w : summaryWinner){
					sMap = new HashMap<String, Object>();
					sMap.put("coId",w.getCoId());
					sMap.put("cuId",w.getCuId());
					sMap.put("rTix", w.getrTix());
					sMap.put("rPoints", w.getrPoints());
					sMap.put("imgU", w.getImgU());
					sMap.put("uId", w.getuId());
					sMap.put("rPointsSt", w.getrPointsSt());
					sMap.put("jCreditSt", w.getjCreditSt());
					summaryList.add(sMap);
				}
				Map<String , Object> map = new HashMap<String, Object>();
				map.put("winner", summaryList);
				summaryWinnerCount.set(aMap);
				summaryWinners.set(map);
				model.addAttribute("summaryStatus", 1);
				fireStoreLog.info("SUMMARY WINNERS UPDATED.");
			} else {
				model.addAttribute("summaryStatus", 0);
				model.addAttribute("msg", "No Winners in Summary.");
				fireStoreLog.info("NO WINNERS IN SUMMARY.");
				Map<String, Object> aMap = new HashMap<String, Object>();
				aMap.put("count", count);
				summaryWinnerCount.set(aMap);
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING SUMMARY WINNERS.");
			fireStoreLog.error("ERROR OCCURED UPDATING SUMMARY WINNERS.");
			e.printStackTrace();
		}
	}

	public static void setGrandWinners(List<CassContestWinners> summaryWinner, Model model) {
		try {
			grandWinners = rtfCollection.document("GrandWinners");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				List<Map<String, Object>> winList = new ArrayList<Map<String,Object>>();
				Map<String, Object> sMap = null;
				for(CassContestWinners w : summaryWinner){
					sMap = new HashMap<String, Object>();
					sMap.put("coId",w.getCoId());
					sMap.put("cuId",w.getCuId());
					sMap.put("rTix", w.getrTix());
					sMap.put("rPoints", w.getrPoints());
					sMap.put("imgU", w.getImgU());
					sMap.put("uId", w.getuId());
					sMap.put("rPointsSt", w.getrPointsSt());
					sMap.put("jCreditSt", w.getjCreditSt());
					winList.add(sMap);
				}
				Map<String , Object> map = new HashMap<String, Object>();
				map.put("winner", winList);
				grandWinners.set(map);
				model.addAttribute("winnerStatus", 1);
				fireStoreLog.info("GRAND WINNERS UPDATED.");
			} else {
				model.addAttribute("winnerStatus", 0);
				model.addAttribute("msg", "No Users In Grand Winner.");
				fireStoreLog.info("NO GRAND WINNERS FOUND.");
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING GRAND WINNERS.");
			fireStoreLog.error("ERROR OCCURED UPDATING GRAND WINNERS.");
			e.printStackTrace();
		}
	}

	public static void setJackpotWinners(List<CassContestWinners> jackpotWinners, Model model) {
		try {
			jackpotWinner = rtfCollection.document("JackPotWinners");
			if (jackpotWinners != null && !jackpotWinners.isEmpty()) {
				List<Map<String, Object>> jackWinList = new ArrayList<Map<String,Object>>();
				Map<String, Object> sMap = null;
				for(CassContestWinners w : jackpotWinners){
					sMap = new HashMap<String, Object>();
					sMap.put("coId",w.getCoId());
					sMap.put("cuId",w.getCuId());
					sMap.put("rTix", w.getrTix());
					sMap.put("rPoints", w.getrPoints());
					sMap.put("imgU", w.getImgU());
					sMap.put("uId", w.getuId());
					sMap.put("rPointsSt", w.getrPointsSt());
					sMap.put("jCreditSt", w.getjCreditSt());
					jackWinList.add(sMap);
				}
				Map<String , Object> map = new HashMap<String, Object>();
				map.put("winner", jackWinList);
				jackpotWinner.set(map);
				model.addAttribute("jackpotStatus", 1);
				fireStoreLog.info("JACKPOT WINNERS UPDATED.");
			} else {
				model.addAttribute("jackpotStatus", 0);
				model.addAttribute("msg", "No Users In Grand Winner.");
				fireStoreLog.info("NO JACKPOT WINNERS FOUND.");
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING JACKPOT WINNERS.");
			fireStoreLog.error("ERROR OCCURED UPDATING JACKPOT WINNERS.");
			e.printStackTrace();
		}
	}

	public static void endContest(Model model) {
		Date timeStamp = new Date();
		try {
			questionRef = rtfCollection.document("Question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", 0);
			qMap.put("qNo", 0);
			qMap.put("text", "");
			qMap.put("optionA", "");
			qMap.put("optionB", "");
			qMap.put("optionC", "");
			qMap.put("answer", "END");
			qMap.put("timeStamp", timeStamp.getTime());
			qMap.put("eraseOption", "");

			answerCount = rtfCollection.document("UserCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("optionACount", 0);
			aMap.put("optionBCount", 0);
			aMap.put("optionCCount", 0);
			aMap.put("rewardDollar", 0.0);
			aMap.put("timeStamp", timeStamp.getTime());

			contestDetails = rtfCollection.document("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", "");
			cMap.put("contestName", "");
			cMap.put("extendedName", "");
			cMap.put("contestFreeTickets", "");
			cMap.put("questionSize", "");
			cMap.put("rewardPrize", "");
			cMap.put("id", "");
			
			Map<String, Object> wMap = new HashMap<String, Object>();
			wMap.put("winner", new ArrayList<Map<String, Object>>());

			summaryWinnerCount = rtfCollection.document("SummaryWinnerCount");
			summaryWinners = rtfCollection.document("SummaryWinners");
			jackpotWinner = rtfCollection.document("JackPotWinners");
			answerCount = rtfCollection.document("UserCount");
			grandWinners = rtfCollection.document("GrandWinners");
			jackpotWinner = rtfCollection.document("JackPotWinners");
			
			questionRef.set(qMap);
			answerCount.set(aMap);
			contestDetails.set(cMap);
			summaryWinners.set(wMap);
			grandWinners.set(wMap);
			jackpotWinner.set(wMap);
			
			Map<String, Object> sMap = new HashMap<String, Object>();
			sMap.put("count", 0);
			summaryWinnerCount.set(sMap);
			setPollingStartedStaus(true);

			model.addAttribute("status", 1);
			model.addAttribute("msg", "Contest ended sucessfully.");
			updateChatting(false);
			cleanupHostNode();
			fireStoreLog.info("CONTEST ENDED.");
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE ENDING CONTEST.");
			fireStoreLog.error("ERROR OCCURED WHILE ENDING CONTEST.");
			e.printStackTrace();
		}
	}

	public static void updateUpcomingContestDetails() {
		try {
			contestInfo = rtfCollection.document("dashboardInfoNew");
			//contestInfoWeb = rtfCollection.document("dashboardInfo");
			updateDashbord = rtfCollection.document("UpdateDashBoard");

			Map<String, String> map = Util.getParameterMap(null);
			map.put("type", "MOBILE");
			String data = Util.getObject(map,
					Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_UPCOMING_CONTEST);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject) jsonObject.get("contestsDTO")), ContestsDTO.class);
			//List<ContestFirebase> contestList = new ArrayList<ContestFirebase>();
			List<Map<String, Object>> contestMapList = new ArrayList<Map<String,Object>>();
			if (contestsDTO.getError() == null && contestsDTO.getStatus() == 1) {
				if (contestsDTO.getContestsList() != null && !contestsDTO.getContestsList().isEmpty()) {
					Contests c = contestsDTO.getContestsList().get(0);
					String nextGameTime = Util.getNextContestStartTimeMsg(c.getStartDateTime());
					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("grandWinnerExpiryText", Util.getGrandWinnerExpiryText(c.getPromoExpiryDate()));
					cMap.put("joinButtonPopupMsg", Util.getJoinButtonMsg(nextGameTime));
					cMap.put("isContestStarted", contestsDTO.getContestStarted());
					cMap.put("joinButtonLabel", "PLAY GAME");
					cMap.put("nextContestId", c.getId());
					cMap.put("isPassRequired", false);
					if(c.getContestMode().equalsIgnoreCase("PASSWORD")){
						cMap.put("isPassRequired", true);
					}

					Contests superFanGame = null;
					for (Contests contest : contestsDTO.getContestsList()) {
						if (contest.getContestCategory().equalsIgnoreCase("SUPERFAN")) {
							superFanGame = contest;
							break;
						}
					}
					if(superFanGame!=null){
						cMap.put("superFanText","SUPERFAN GAME :  "+Util.getNextContestStartTimeMsg(superFanGame.getStartDateTime()));
					}else{
						cMap.put("superFanText","");
					}

					
					
					for (Contests contest : contestsDTO.getContestsList()) {
						if (contestMapList.size() < 4) {
							/*ContestFirebase cf = new ContestFirebase();
							cf.setContestId(contest.getId());
							cf.setContestName(contest.getContestName());
							cf.setType(contest.getContestCategory());
							cf.setImageUrl("");
							cf.setNextGameTime(Util.getNextContestStartTimeMsg(contest.getStartDateTime()));
							cf.setShareText(Util.getShareText(contest.getContestName()));
							contestList.add(cf);*/
							
							Map<String, Object> contestMap = new HashMap<String, Object>();
							contestMap.put("contestId", contest.getId());
							contestMap.put("contestName", contest.getContestName());
							contestMap.put("type", contest.getContestCategory());
							contestMap.put("imageUrl", "");
							contestMap.put("nextGameTime", Util.getNextContestStartTimeMsg(contest.getStartDateTime()));
							contestMap.put("shareText",Util.getShareText(contest.getContestName()));
							contestMapList.add(contestMap);
						}
					}

					if (contestMapList.isEmpty()) {
						/*ContestFirebase cf = new ContestFirebase();
						cf.setContestId(-1);
						cf.setContestName("Next Contest will be Announced Shortly.");
						cf.setNextGameTime("TBD");
						cf.setType("NONE");
						cf.setImageUrl("");
						cf.setShareText("");
						contestList.add(cf);*/
						
						Map<String, Object> contestMap = new HashMap<String, Object>();
						contestMap.put("contestId", -1);
						contestMap.put("contestName", "Next Contest will be Announced Shortly.");
						contestMap.put("type", "NONE");
						contestMap.put("imageUrl", "");
						contestMap.put("nextGameTime", "TBD");
						contestMap.put("shareText","");
						contestMapList.add(contestMap);
					}

					//cMap.put("contestArray", contestList);
					cMap.put("contestArray", contestMapList);
					
					Map<String, Object> dMap = new HashMap<String, Object>();
					dMap.put("timeStamp", new Date().getTime());
					
					contestInfo.update(cMap);
					updateDashbord.update(dMap);
					fireStoreLog.info("UPCOMING CONTEST UPDATED:  -- " + contestMapList.size() + " -- special : "+ cMap.get("specialContestId"));
				} else {

					/*ContestFirebase cf = new ContestFirebase();
					cf.setContestId(-1);
					cf.setContestName("Next Contest will be Announced Shortly.");
					cf.setNextGameTime("TBD");
					cf.setType("NONE");
					cf.setImageUrl("");
					cf.setShareText("");
					contestList.add(cf);*/
					
					Map<String, Object> contestMap = new HashMap<String, Object>();
					contestMap.put("contestId", -1);
					contestMap.put("contestName", "Next Contest will be Announced Shortly.");
					contestMap.put("type", "NONE");
					contestMap.put("imageUrl", "");
					contestMap.put("nextGameTime", "TBD");
					contestMap.put("shareText","");
					contestMapList.add(contestMap);

					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("grandWinnerExpiryText", "");
					cMap.put("joinButtonPopupMsg", "Next Contest will be Announced Shortly.");
					cMap.put("isContestStarted", contestsDTO.getContestStarted());
					cMap.put("joinButtonLabel", "PLAY GAME");
					cMap.put("nextContestId", 0);
					cMap.put("superFanText","");
					cMap.put("isPassRequired", false);

					Map<String, Object> webMap = new HashMap<String, Object>();
					webMap.put("nextGameTime", "TBD");
					webMap.put("contestName", "Next Contest will be Announced Shortly.");
					webMap.put("contestId", 0);

					cMap.put("contestArray", contestMapList);
					
					Map<String, Object> dMap = new HashMap<String, Object>();
					dMap.put("timeStamp", new Date().getTime());
					
					contestInfo.update(cMap);
					updateDashbord.set(dMap);
					fireStoreLog.info("UPCOMING CONTEST UPDATED:  -No Upcoming contest");
				}

			} else {
				fireStoreLog.error("TACKERAPI ERROR: ERROR OCCURED WHILE GETTING UPCOMING CONTEST DETAILS: "+ contestsDTO.getError().getDescription());
			}
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING UPCOMING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}

	public void initialiseFirebase() {
		try {
			FileInputStream serviceAccount = new FileInputStream("C:/REWARDTHEFAN/FireBase/google-services.json");
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();
			FirebaseApp mobileFireStore = FirebaseApp.initializeApp(options,"mobileFireStore");
			Firestore firestore = FirestoreClient.getFirestore(mobileFireStore);
			rtfCollection = firestore.collection("rewardthefan");
			hostCollection = firestore.collection("hostNode");
			fireStoreLog.info("FIRESTORE CONFIRGURATION INITIALISED.");
		} catch (Exception e) {
			fireStoreLog.error("ERROR OCCURED WHILE INITIALISING FIRESTORE.");
			e.printStackTrace();
		}
	}

	public static void setPollingStartedStaus(Boolean isStarted) {
		try {
			polling = rtfCollection.document("Polling");
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("isPollingStarted", isStarted);
			polling.update(pMap);
			fireStoreLog.info("POLLING STARTED/STOPPED: " + isStarted);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING POLLING STARTED FLAG.");
		}
	}

	public static void setPollingInterval(Integer interval) {
		try {
			polling = rtfCollection.document("Polling");
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("OnlyPollingInterval", interval);
			polling.update(pMap);
			fireStoreLog.info("POLLING INTERVAL UPDATED: " + interval);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING POLLING INTERVAL.");
		}
	}

	public static void setHoulryGameEnabled(Boolean isEnabled) {
		try {
			/*
			 * polling = firebase.document("Polling"); Map<String,Object> pMap = new
			 * HashMap<String, Object>(); pMap.put("isHoulryGameEnabled",isEnabled);
			 * polling.update(pMap);
			 * fireStoreLog.info("HOURLY GAME STARTED/STOPPED: "+isEnabled);
			 */
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING HOURLY GAME FLAG.");
		}
	}

	public static void updateChatting(boolean isChat) {
		try {
			appSettings = rtfCollection.document("appSettings");
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("isChat", isChat);
			appSettings.update(pMap);
			fireStoreLog.info("CHATTING FLAG UPDATED : " + isChat);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("ERROR OCCURED WHILE UPDATING CHATTING FLAG.");
		}
	}

	public static void updatePopularChannelDetails() {
		try {
			contestInfo = rtfCollection.document("dashboardInfoNew");
			//contestInfoWeb = rtfCollection.document("dashboardInfo");
			//updateDashbord = rtfCollection.document("UpdateDashBoard").document("timeStamp");

			Map<String, String> map = Util.getParameterMap(null);
			map.put("type", "MOBILE");
			String data = Util.getObject(map,
					Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_POPULAR_CHANNELS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FanClubDTO fanClubDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")), FanClubDTO.class);

			List<Map<String, Object>> fanclubMapList = new ArrayList<Map<String,Object>>();
			List<Map<String, Object>> channelMapList = new ArrayList<Map<String,Object>>();
			Map<String, Object> fanclubMap = null;
			Map<String, Object> channelMap = null;
			
			if (fanClubDTO.getStatus() == 1) {
				if (fanClubDTO.getClubs() != null && !fanClubDTO.getClubs().isEmpty()) {
					//List<FanClubFireBase> clubList = new ArrayList<FanClubFireBase>();
					for (FanClub fanClub : fanClubDTO.getClubs()) {
						/*FanClubFireBase fc = new FanClubFireBase();
						fc.setId(fanClub.getId());
						fc.setImageUrl(fanClub.getImageUrl());
						fc.setName(fanClub.getTitle());
						fc.setNOM(fanClub.getMemberCount());
						clubList.add(fc);*/
						fanclubMap = new HashMap<String, Object>();
						fanclubMap.put("id", fanClub.getId());
						fanclubMap.put("imageUrl", fanClub.getImageUrl());
						fanclubMap.put("name", fanClub.getTitle());
						fanclubMap.put("nom",fanClub.getMemberCount() );
						fanclubMap.put("shareText","");
						fanclubMapList.add(fanclubMap);
					}
					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("fanClubArray", fanclubMapList);
					contestInfo.update(cMap);
					
					//fireStoreLog.info("DashBoard Channels Updated  -- " + cMap.size());
				}

				if (fanClubDTO.getCategories() != null && !fanClubDTO.getCategories().isEmpty()) {
					List<MediaChannelFireBase> channelList = new ArrayList<MediaChannelFireBase>();
					for (PollingVideoCategory channels : fanClubDTO.getCategories()) {
						/*MediaChannelFireBase fc = new MediaChannelFireBase();
						fc.setId(channels.getId());
						fc.setImageUrl(channels.getImageUrl());
						fc.setName(channels.getCategoryName());
						channelList.add(fc);*/
						channelMap = new HashMap<String, Object>();
						channelMap.put("id", channels.getId());
						channelMap.put("imageUrl",channels.getImageUrl() );
						channelMap.put("name", channels.getCategoryName());
						channelMapList.add(channelMap);
					}
					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("tvChannelsArray", channelMapList);
					contestInfo.update(cMap);
					
					//fireStoreLog.info("DashBoard Channels Updated  -- " + cMap.size());
				}
				//updateDashbord.set(new Date().getTime());

			} else {
				fireStoreLog.error("TACKERAPI ERROR: ERROR OCCURED WHILE UPDATING DASHBOARD CHANNEL INFORMATION ");
			}
		} catch (

		Exception e) {
			fireStoreLog.error("TACKERAPI ERROR: ERROR OCCURED WHILE UPDATING DASHBOARD CHANNEL INFORMATION");
			e.printStackTrace();
		}
	}
	
	
	
	
	//HOST SCREEN METHODS
	private static DocumentReference tracker= null;
	private static DocumentReference hostQue = null;
	private static DocumentReference hostAnswerCount = null;
	private static DocumentReference hostSummary = null;
	private static DocumentReference hostSummaryCount = null;
	private static DocumentReference hostTtotalUserCount = null;
	private static DocumentReference hostJackpot = null;
	private static DocumentReference hostGrand= null;
	
	
	public static void updateHostTracker(String state){
		try {
			tracker = hostCollection.document("tracker");
			Map<String, Object> tMap = new HashMap<String, Object>();
			tMap.put("state", state);
			
			tracker.set(tMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void updateNextQuestionHost(ContestQuestions q, Model model) {
		Date timeStamp = new Date();
		try {
			if(q==null){
				return;
			}
			updateHostTracker("QUESTION");
			hostQue = hostCollection.document("que");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", q.getId());
			qMap.put("qNo", q.getSerialNo());
			qMap.put("txt", q.getQuestion());
			qMap.put("optA", q.getOptionA());
			qMap.put("optB", q.getOptionB());
			qMap.put("optC", q.getOptionC());
			qMap.put("ans", !q.getAnswer().equalsIgnoreCase("Z")?q.getAnswer():q.getCorrectAnswer());
			qMap.put("timeStamp", timeStamp.getTime());

			hostQue.set(qMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("HOST CONTEST " + q.getAnswer() + ":  --qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "HOST : ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo());
			fireStoreLog.error("HOST : ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo() + " -- " + q.getAnswer());
			e.printStackTrace();
		}
	}
	
	
	public static void setAnswerCountHost(AnswerCountInfo a, Model model) {
		Date timeStamp = new Date();
		try {
			updateHostTracker("COUNT");
			hostAnswerCount = hostCollection.document("answerCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("countA", a.getaCount());
			aMap.put("countB", a.getbCount());
			aMap.put("countC", a.getcCount());
			aMap.put("rewardPrize", a.getQuRwds());
			aMap.put("timeStamp", timeStamp.getTime());
			hostAnswerCount.set(aMap);
			model.addAttribute("status", 1);
			fireStoreLog.info("HOST : UPDATED ANSWER COUNT.");
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "HOST : ERROR OCCURED UPDATING ANSWER COUNT.");
			fireStoreLog.error("HOST : ERROR OCCURED UPDATING ANSWER COUNT.");
			e.printStackTrace();
		}
	}
	
	
	public static void updateTotalUserCountHost(Integer count) {
		try {
			hostTtotalUserCount = hostCollection.document("totalUserCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("count", count);
			hostTtotalUserCount.set(aMap);
			// fireStoreLog.info("TOTAL COUNT UPDATED: "+count);
		} catch (Exception e) {
			e.printStackTrace();
			fireStoreLog.error("HOST : ERROR OCCURED WHILE UPDATING TOTAL USER COUNT.");
		}
	}
	
	
	
	public static void setJackpotWinnersHost(List<CassContestWinners> jackpotWinners, Model model) {
		try {
			updateHostTracker("MINIJACKPOT");
			hostJackpot = hostCollection.document("jackpot");
			if (jackpotWinners != null && !jackpotWinners.isEmpty()) {
				List<Map<String, Object>> jackWinList = new ArrayList<Map<String,Object>>();
				Map<String, Object> sMap = null;
				for(CassContestWinners w : jackpotWinners){
					sMap = new HashMap<String, Object>();
					sMap.put("coId",w.getCoId());
					sMap.put("tix", w.getrTix());
					sMap.put("uId", w.getuId());
					sMap.put("prize", w.getrPointsSt());
					jackWinList.add(sMap);
				}
				Map<String , Object> map = new HashMap<String, Object>();
				map.put("winner", jackWinList);
				hostJackpot.set(map);
				model.addAttribute("jackpotStatus", 1);
				fireStoreLog.info("HOST : JACKPOT WINNERS UPDATED.");
			} else {
				model.addAttribute("jackpotStatus", 0);
				model.addAttribute("msg", "No Users In Grand Winner.");
				fireStoreLog.info("HOST : NO JACKPOT WINNERS FOUND.");
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "HOST : ERROR OCCURED UPDATING JACKPOT WINNERS.");
			fireStoreLog.error("HOST : ERROR OCCURED UPDATING JACKPOT WINNERS.");
			e.printStackTrace();
		}
	}
	
	
	public static void setSummaryWinnersHost(List<CassContestWinners> summaryWinner, Integer count, Model model) {
		try {
			updateHostTracker("SUMMARY");
			hostSummary = hostCollection.document("summary");
			hostSummaryCount = hostCollection.document("summaryCount");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				Map<String, Object> aMap = new HashMap<String, Object>();
				aMap.put("count", count);
				List<Map<String, Object>> summaryList = new ArrayList<Map<String,Object>>();
				Map<String, Object> sMap = null;
				
				for(CassContestWinners w : summaryWinner){
					sMap = new HashMap<String, Object>();
					sMap.put("coId",w.getCoId());
					sMap.put("tix", w.getrTix());
					sMap.put("uId", w.getuId());
					sMap.put("prize", w.getrPointsSt());
					summaryList.add(sMap);
				}
				Map<String , Object> map = new HashMap<String, Object>();
				map.put("winner", summaryList);
				hostSummaryCount.set(aMap);
				hostSummary.set(map);
				model.addAttribute("summaryStatus", 1);
				fireStoreLog.info("HOST : SUMMARY WINNERS UPDATED.");
			} else {
				model.addAttribute("summaryStatus", 0);
				model.addAttribute("msg", "No Winners in Summary.");
				fireStoreLog.info("HOST : NO WINNERS IN SUMMARY.");
				Map<String, Object> aMap = new HashMap<String, Object>();
				aMap.put("count", count);
				hostSummaryCount.set(aMap);
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "HOST : ERROR OCCURED UPDATING SUMMARY WINNERS.");
			fireStoreLog.error("HOST : ERROR OCCURED UPDATING SUMMARY WINNERS.");
			e.printStackTrace();
		}
	}
	
	
	public static void setGrandWinnersHost(List<CassContestWinners> summaryWinner, Model model) {
		try {
			updateHostTracker("WINNER");
			hostGrand = hostCollection.document("grand");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				List<Map<String, Object>> winList = new ArrayList<Map<String,Object>>();
				Map<String, Object> sMap = null;
				for(CassContestWinners w : summaryWinner){
					sMap = new HashMap<String, Object>();
					sMap.put("coId",w.getCoId());
					sMap.put("tix", w.getrTix());
					sMap.put("uId", w.getuId());
					sMap.put("prize", w.getrPointsSt());
					winList.add(sMap);
				}
				Map<String , Object> map = new HashMap<String, Object>();
				map.put("winner", winList);
				hostGrand.set(map);
				model.addAttribute("winnerStatus", 1);
				fireStoreLog.info("HOST : GRAND WINNERS UPDATED.");
			} else {
				model.addAttribute("winnerStatus", 0);
				model.addAttribute("msg", "No Users In Grand Winner.");
				fireStoreLog.info("HOST : NO GRAND WINNERS FOUND.");
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "HOST : ERROR OCCURED UPDATING GRAND WINNERS.");
			fireStoreLog.error("HOST : ERROR OCCURED UPDATING GRAND WINNERS.");
			e.printStackTrace();
		}
	}
	
	
	private static void cleanupHostNode(){
		hostQue = hostCollection.document("que");
		ContestQuestions q = new ContestQuestions();
		q.setAnswer("");
		Map<String, Object> qMap = new HashMap<String, Object>();
		qMap.put("qId", q.getId());
		qMap.put("qNo", q.getSerialNo());
		qMap.put("txt", q.getQuestion());
		qMap.put("optA", q.getOptionA());
		qMap.put("optB", q.getOptionB());
		qMap.put("optC", q.getOptionC());
		qMap.put("ans", !q.getAnswer().equalsIgnoreCase("Z")?q.getAnswer():q.getCorrectAnswer());
		qMap.put("timeStamp", new Date().getTime());
		hostQue.set(qMap);
		
		AnswerCountInfo  a = new AnswerCountInfo(); 
		hostAnswerCount = hostCollection.document("answerCount");
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("countA", a.getaCount());
		aMap.put("countB", a.getbCount());
		aMap.put("countC", a.getcCount());
		aMap.put("rewardPrize", a.getQuRwds());
		aMap.put("timeStamp", new Date().getTime());
		hostAnswerCount.set(aMap);
		
		hostGrand = hostCollection.document("grand");
		hostSummary = hostCollection.document("summary");
		hostSummaryCount = hostCollection.document("summaryCount");
		hostJackpot = hostCollection.document("jackpot");
		
		Map<String, Object> wMap = new HashMap<String, Object>();
		wMap.put("winner", new ArrayList<Map<String, Object>>());
		
		Map<String, Object> sMap = new HashMap<String, Object>();
		sMap.put("count",0);
		
		hostGrand.set(wMap);
		hostSummary.set(wMap);
		hostJackpot.set(wMap);
		hostSummaryCount.set(sMap);
		updateHostTracker("END");
		
	}
	

}
