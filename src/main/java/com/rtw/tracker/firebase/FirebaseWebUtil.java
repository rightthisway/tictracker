package com.rtw.tracker.firebase;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.Model;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.FanClub;
import com.rtw.tracker.datas.PollingVideoCategory;
import com.rtw.tracker.pojos.AnswerCountInfo;
import com.rtw.tracker.pojos.CassContestWinners;
import com.rtw.tracker.pojos.ContestsDTO;
import com.rtw.tracker.pojos.FanClubDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.URLUtil;
import com.rtw.tracker.utils.Util;

public class FirebaseWebUtil implements InitializingBean  {

	private static DatabaseReference firebase = null;

	private static DatabaseReference questionRef = null;
	private static DatabaseReference contestDetails = null;
	private static DatabaseReference answerCount = null;
	private static DatabaseReference totalUserCount = null;
	private static DatabaseReference summaryWinners = null;
	private static DatabaseReference summaryWinnerCount = null;
	private static DatabaseReference grandWinners = null;
	private static DatabaseReference contestInfo = null;
	private static DatabaseReference contestInfoWeb = null;
	private static DatabaseReference updateDashbord = null;
	private static DatabaseReference CorrectOpt = null;
	private static DatabaseReference jackpotWinner = null;
	private static DatabaseReference polling = null;
	private static DatabaseReference appSettings = null;

	private static Logger firebaseLog = LoggerFactory.getLogger(FirebaseWebUtil.class);

	public SharedProperty sharedProperty;

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		initialiseFirebase();

	}

	/*
	 * public static void setAppProperty(String key,Model model){ try { appProperty
	 * = firebase.child("appProperties"); Map<String,Object> map = new
	 * HashMap<String, Object>(); map.put("uMsg",key);
	 * appProperty.updateChildrenAsync(map); model.addAttribute("status",1);
	 * firebaseLog.info("FIREBASE SECRET KEY UDAPTED."); } catch (Exception e) {
	 * e.printStackTrace(); model.addAttribute("status",0);
	 * model.addAttribute("msg",
	 * "ERROR OCCURED WHILE UPDATING SECRET KEY ON FIREBASE");
	 * firebaseLog.error("ERROR OCCURED WHILE UPDATING SECRET KEY ON FIREBASE"); } }
	 */

	public static void updateNextQuestion(ContestQuestions q, Model model) {
		Date timeStamp = new Date();
		try {
			questionRef = firebase.child("Question");
			CorrectOpt = firebase.child("Tpoc");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", q.getId());
			qMap.put("qNo", q.getSerialNo());
			qMap.put("text", q.getQuestion());
			qMap.put("optionA", q.getOptionA());
			qMap.put("optionB", q.getOptionB());
			qMap.put("optionC", q.getOptionC());
			qMap.put("answer", q.getAnswer());
			qMap.put("timeStamp", timeStamp.getTime());
			qMap.put("eraseOption", q.getWrongAnswer());

			questionRef.setValueAsync(qMap);
			if (q.getCorrectAnswer() != null && !q.getCorrectAnswer().isEmpty()) {
				Map<String, Object> aMap = new HashMap<String, Object>();
				// aMap.put("uChar", RtwEncoder.encodePassword(q.getCorrectAnswer()));
				aMap.put("uChar", q.getCorrectAnswer());
				aMap.put("rewards", q.getQuestionReward());
				CorrectOpt.setValueAsync(aMap);
			}

			model.addAttribute("status", 1);
			firebaseLog.info("CONTEST " + q.getAnswer() + ":  --qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo());
			firebaseLog.error("ERROR OCCURED WHILE UPDATING QUESTION NO:" + q.getSerialNo() + " -- " + q.getAnswer());
			e.printStackTrace();
		}
	}

	public static void updateTotalUserCount(Integer count) {
		try {
			totalUserCount = firebase.child("TotalUserCount");
			Object ob = count;
			totalUserCount.setValueAsync(ob);
			// firebaseLog.info("TOTAL COUNT UPDATED: "+count);
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING TOTAL USER COUNT.");
		}
	}

	public static void updateLiveStreamUrl(String url) {
		try {
			contestInfo = firebase.child("dashboardInfoNew");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("videoUrl", url);
			contestInfo.updateChildrenAsync(vMap);
			firebaseLog.info("LIVE STREAM URL UPDATED: " + url);
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING LIVE STREAM URL.");
		}
	}

	public static void setContestStarted(Boolean isStarted) {
		try {
			contestInfo = firebase.child("dashboardInfoNew");
			Map<String, Object> vMap = new HashMap<String, Object>();
			vMap.put("isContestStarted", isStarted);
			contestInfo.updateChildrenAsync(vMap);
			// setHoulryGameEnabled(false);
			firebaseLog.info("CONTEST STARTED/STOPPED: " + isStarted);
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING CONTEST STARTED FLAG.");
		}
	}

	public static void setContestDetails(Contests c, Model model) {
		try {
			contestDetails = firebase.child("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", 0);
			cMap.put("contestName", c.getContestName());
			cMap.put("extendedName", c.getExtendedName());
			cMap.put("contestFreeTickets", c.getFreeTicketPerWinner());
			cMap.put("questionSize", c.getQuestionSize());
			cMap.put("rewardPrize", c.getRewardPoints());
			cMap.put("id", c.getId());
			contestDetails.setValueAsync(cMap);
			model.addAttribute("status", 1);
			firebaseLog.info("CONTEST DETAILS UPDATED: -- coid: " + c.getId());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE SETTING CONTEST DETAILS.");
			firebaseLog.error("ERROR OCCURED WHILE SETTING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}

	public static void updateContestDetailsQNo(ContestQuestions q, Model model) {
		try {
			contestDetails = firebase.child("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", q.getSerialNo());
			contestDetails.updateChildrenAsync(cMap);
			model.addAttribute("status", 1);
			firebaseLog.info("UPDATED CONTEST DETAILS: -- qno: " + q.getSerialNo());
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE UPDATING CONTEST DETAILS QUESTION NO.");
			firebaseLog.error("ERROR OCCURED WHILE UPDATING CONTEST DETAILS QUESTION NO.");
			e.printStackTrace();
		}
	}

	public static void setAnswerCount(AnswerCountInfo a, Model model) {
		Date timeStamp = new Date();
		try {
			answerCount = firebase.child("UserCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("optionACount", a.getaCount());
			aMap.put("optionBCount", a.getbCount());
			aMap.put("optionCCount", a.getcCount());
			aMap.put("rewardDollar", a.getQuRwds());
			aMap.put("timeStamp", timeStamp.getTime());
			answerCount.setValueAsync(aMap);
			model.addAttribute("status", 1);
			firebaseLog.info("UPDATED ANSWER COUNT.");
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING ANSWER COUNT.");
			firebaseLog.error("ERROR OCCURED UPDATING ANSWER COUNT.");
			e.printStackTrace();
		}
	}

	public static void setSummaryWinners(List<CassContestWinners> summaryWinner, Integer count, Model model) {
		try {
			summaryWinners = firebase.child("SummaryWinners");
			summaryWinnerCount = firebase.child("SummaryWinnerCount");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				summaryWinnerCount.setValueAsync(count);
				summaryWinners.setValueAsync(summaryWinner);
				model.addAttribute("summaryStatus", 1);
				firebaseLog.info("SUMMARY WINNERS UPDATED.");
			} else {
				model.addAttribute("summaryStatus", 0);
				model.addAttribute("msg", "No Winners in Summary.");
				firebaseLog.info("NO WINNERS IN SUMMARY.");
				summaryWinnerCount.setValueAsync(0);
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING SUMMARY WINNERS.");
			firebaseLog.error("ERROR OCCURED UPDATING SUMMARY WINNERS.");
			e.printStackTrace();
		}
	}

	public static void setGrandWinners(List<CassContestWinners> summaryWinner, Model model) {
		try {
			grandWinners = firebase.child("GrandWinners");
			if (summaryWinner != null && !summaryWinner.isEmpty()) {
				grandWinners.setValueAsync(summaryWinner);
				model.addAttribute("winnerStatus", 1);
				firebaseLog.info("GRAND WINNERS UPDATED.");
			} else {
				model.addAttribute("winnerStatus", 0);
				model.addAttribute("msg", "No Users In Grand Winner.");
				firebaseLog.info("NO GRAND WINNERS FOUND.");
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING GRAND WINNERS.");
			firebaseLog.error("ERROR OCCURED UPDATING GRAND WINNERS.");
			e.printStackTrace();
		}
	}

	public static void setJackpotWinners(List<CassContestWinners> jackpotWinners, Model model) {
		try {
			jackpotWinner = firebase.child("JackPotWinners");
			if (jackpotWinners != null && !jackpotWinners.isEmpty()) {
				jackpotWinner.setValueAsync(jackpotWinners);
				model.addAttribute("jackpotStatus", 1);
				firebaseLog.info("JACKPOT WINNERS UPDATED.");
			} else {
				model.addAttribute("jackpotStatus", 0);
				model.addAttribute("msg", "No Users In Grand Winner.");
				firebaseLog.info("NO JACKPOT WINNERS FOUND.");
			}
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED UPDATING JACKPOT WINNERS.");
			firebaseLog.error("ERROR OCCURED UPDATING JACKPOT WINNERS.");
			e.printStackTrace();
		}
	}

	public static void endContest(Model model) {
		Date timeStamp = new Date();
		try {
			questionRef = firebase.child("Question");
			Map<String, Object> qMap = new HashMap<String, Object>();
			qMap.put("qId", 0);
			qMap.put("qNo", 0);
			qMap.put("text", "");
			qMap.put("optionA", "");
			qMap.put("optionB", "");
			qMap.put("optionC", "");
			qMap.put("answer", "END");
			qMap.put("timeStamp", timeStamp.getTime());
			qMap.put("eraseOption", "");

			answerCount = firebase.child("UserCount");
			Map<String, Object> aMap = new HashMap<String, Object>();
			aMap.put("optionACount", 0);
			aMap.put("optionBCount", 0);
			aMap.put("optionCCount", 0);
			aMap.put("rewardDollar", 0.0);
			aMap.put("timeStamp", timeStamp.getTime());

			contestDetails = firebase.child("contestDetails");
			Map<String, Object> cMap = new HashMap<String, Object>();
			cMap.put("qNo", "");
			cMap.put("contestName", "");
			cMap.put("extendedName", "");
			cMap.put("contestFreeTickets", "");
			cMap.put("questionSize", "");
			cMap.put("rewardPrize", "");
			cMap.put("id", "");

			summaryWinnerCount = firebase.child("SummaryWinnerCount");

			questionRef.setValueAsync(qMap);
			answerCount.setValueAsync(aMap);
			contestDetails.setValueAsync(cMap);
			summaryWinnerCount.setValueAsync(0);
			setPollingStartedStaus(true);

			model.addAttribute("status", 1);
			model.addAttribute("msg", "Contest ended sucessfully.");
			firebaseLog.info("CONTEST ENDED.");
		} catch (Exception e) {
			model.addAttribute("status", 0);
			model.addAttribute("msg", "ERROR OCCURED WHILE ENDING CONTEST.");
			firebaseLog.error("ERROR OCCURED WHILE ENDING CONTEST.");
			e.printStackTrace();
		}
	}

	public static void updateUpcomingContestDetails() {
		try {
			contestInfo = firebase.child("dashboardInfoNew");
			contestInfoWeb = firebase.child("dashboardInfo");
			updateDashbord = firebase.child("UpdateDashBoard").child("timeStamp");

			Map<String, String> map = Util.getParameterMap(null);
			map.put("type", "WEB");
			String data = Util.getObject(map,
					Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_UPCOMING_CONTEST);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject) jsonObject.get("contestsDTO")), ContestsDTO.class);
			List<ContestFirebase> contestList = new ArrayList<ContestFirebase>();
			if (contestsDTO.getError() == null && contestsDTO.getStatus() == 1) {
				if (contestsDTO.getContestsList() != null && !contestsDTO.getContestsList().isEmpty()) {
					Contests c = contestsDTO.getContestsList().get(0);
					String nextGameTime = Util.getNextContestStartTimeMsg(c.getStartDateTime());
					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("grandWinnerExpiryText", Util.getGrandWinnerExpiryText(c.getPromoExpiryDate()));
					cMap.put("joinButtonPopupMsg", Util.getJoinButtonMsg(nextGameTime));
					cMap.put("isContestStarted", contestsDTO.getContestStarted());
					cMap.put("joinButtonLabel", "PLAY GAME");
					cMap.put("nextContestId", c.getId());

					Map<String, Object> webMap = new HashMap<String, Object>();
					webMap.put("nextGameTime", Util.getNextContestStartTimeMsg(c.getStartDateTime()));
					webMap.put("contestName", c.getContestName());
					webMap.put("contestId", c.getId());

					// Contests specialContest = null;
					Contests superFanGame = null;
					for (Contests contest : contestsDTO.getContestsList()) {
						/*if (contest.getContestCategory().equalsIgnoreCase("SUPERFAN")) {
							ContestFirebase cf = new ContestFirebase();
							cf.setContestId(contest.getId());
							cf.setContestName(contest.getContestName());
							cf.setType(contest.getContestCategory());
							cf.setImageUrl("");
							cf.setNextGameTime(Util.getNextContestStartTime(contest.getStartDateTime()));
							cf.setShareText(Util.getShareText(contest.getContestName()));
							contestList.add(cf);
							break;
						}*/
						if (contest.getContestCategory().equalsIgnoreCase("SUPERFAN")) {
							superFanGame = contest;
							break;
						}
					}
					if(superFanGame!=null){
						cMap.put("superFanText","SUPERFAN GAME : "+Util.getNextContestStartTimeMsg(superFanGame.getStartDateTime()));
					}else{
						cMap.put("superFanText","");
					}

					for (Contests contest : contestsDTO.getContestsList()) {
						if (contestList.size() < 4 && !contest.getContestCategory().equalsIgnoreCase("SUPERFAN")) {
							ContestFirebase cf = new ContestFirebase();
							cf.setContestId(contest.getId());
							cf.setContestName(contest.getContestName());
							cf.setType(contest.getContestCategory());
							cf.setImageUrl("");
							cf.setNextGameTime(Util.getNextContestStartTimeMsg(contest.getStartDateTime()));
							cf.setShareText(Util.getShareText(contest.getContestName()));
							contestList.add(cf);
						}
					}

					/*
					 * if(specialContest!=null){ cMap.put("isSpecialContest",true);
					 * cMap.put("specialContestId",specialContest.getId());
					 * cMap.put("specialContestName",specialContest.getContestName());
					 * cMap.put("specialContestTime",Util.getNextContestStartTime(specialContest.
					 * getStartDateTime())); }else{ cMap.put("isSpecialContest",false);
					 * cMap.put("specialContestId",0); cMap.put("specialContestName","");
					 * cMap.put("specialContestTime",""); }
					 */

					if (contestList.isEmpty()) {
						ContestFirebase cf = new ContestFirebase();
						cf.setContestId(-1);
						cf.setContestName("Next Contest will be Announced Shortly.");
						cf.setNextGameTime("TBD");
						cf.setType("NONE");
						cf.setImageUrl("");
						cf.setShareText("");
						contestList.add(cf);
					}

					cMap.put("contestArray", contestList);
					contestInfo.updateChildrenAsync(cMap);
					contestInfoWeb.updateChildrenAsync(webMap);
					updateDashbord.setValueAsync(new Date().getTime());
					firebaseLog.info("UPCOMING CONTEST UPDATED:  -- " + contestList.size() + " -- special : "
							+ cMap.get("specialContestId"));
				} else {

					ContestFirebase cf = new ContestFirebase();
					cf.setContestId(-1);
					cf.setContestName("Next Contest will be Announced Shortly.");
					cf.setNextGameTime("TBD");
					cf.setType("NONE");
					cf.setImageUrl("");
					cf.setShareText("");
					contestList.add(cf);

					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("grandWinnerExpiryText", "");
					cMap.put("joinButtonPopupMsg", "Next Contest will be Announced Shortly.");
					cMap.put("isContestStarted", contestsDTO.getContestStarted());
					cMap.put("joinButtonLabel", "PLAY GAME");
					cMap.put("nextContestId", 0);
					cMap.put("superFanText","");

					Map<String, Object> webMap = new HashMap<String, Object>();
					webMap.put("nextGameTime", "TBD");
					webMap.put("contestName", "Next Contest will be Announced Shortly.");
					webMap.put("contestId", 0);

					cMap.put("contestArray", contestList);
					contestInfo.updateChildrenAsync(cMap);
					contestInfoWeb.updateChildrenAsync(webMap);
					updateDashbord.setValueAsync(new Date().getTime());
					firebaseLog.info("UPCOMING CONTEST UPDATED:  -No Upcoming contest");
				}

			} else {
				firebaseLog.error("TACKERAPI ERROR: ERROR OCCURED WHILE GETTING UPCOMING CONTEST DETAILS: "
						+ contestsDTO.getError().getDescription());
			}
		} catch (Exception e) {
			firebaseLog.error("ERROR OCCURED WHILE UPDATING UPCOMING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}

	public void initialiseFirebase() {
		try {
			FileInputStream serviceAccount = new FileInputStream(URLUtil.FIREBASE_WEB_CONFIG_FILE);
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://rewardthefanweb.firebaseio.com").build();
			FirebaseApp web = FirebaseApp.initializeApp(options,"WEB");
			firebase = FirebaseDatabase.getInstance(web).getReference();
			firebaseLog.info("FIREBASE CONFIRGURATION INITIALISED.");
		} catch (Exception e) {
			firebaseLog.error("ERROR OCCURED WHILE INITIALISING FIREBASE.");
			e.printStackTrace();
		}
	}

	public static void setPollingStartedStaus(Boolean isStarted) {
		try {
			polling = firebase.child("Polling");
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("isPollingStarted", isStarted);
			polling.updateChildrenAsync(pMap);
			firebaseLog.info("POLLING STARTED/STOPPED: " + isStarted);
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING POLLING STARTED FLAG.");
		}
	}

	public static void setPollingInterval(Integer interval) {
		try {
			polling = firebase.child("Polling");
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("OnlyPollingInterval", interval);
			polling.updateChildrenAsync(pMap);
			firebaseLog.info("POLLING INTERVAL UPDATED: " + interval);
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING POLLING INTERVAL.");
		}
	}

	public static void setHoulryGameEnabled(Boolean isEnabled) {
		try {
			/*
			 * polling = firebase.child("Polling"); Map<String,Object> pMap = new
			 * HashMap<String, Object>(); pMap.put("isHoulryGameEnabled",isEnabled);
			 * polling.updateChildrenAsync(pMap);
			 * firebaseLog.info("HOURLY GAME STARTED/STOPPED: "+isEnabled);
			 */
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING HOURLY GAME FLAG.");
		}
	}

	public static void updateChatting(boolean isChat) {
		try {
			appSettings = firebase.child("appSettings");
			Map<String, Object> pMap = new HashMap<String, Object>();
			pMap.put("isChat", isChat);
			appSettings.updateChildrenAsync(pMap);
			firebaseLog.info("CHATTING FLAG UPDATED : " + isChat);
		} catch (Exception e) {
			e.printStackTrace();
			firebaseLog.error("ERROR OCCURED WHILE UPDATING CHATTING FLAG.");
		}
	}

	public static void updatePopularChannelDetails() {
		try {
			contestInfo = firebase.child("dashboardInfoNew");
			contestInfoWeb = firebase.child("dashboardInfo");
			updateDashbord = firebase.child("UpdateDashBoard").child("timeStamp");

			Map<String, String> map = Util.getParameterMap(null);
			map.put("type", "MOBILE");
			String data = Util.getObject(map,
					Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_POPULAR_CHANNELS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FanClubDTO fanClubDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")), FanClubDTO.class);

			if (fanClubDTO.getError() == null && fanClubDTO.getStatus() == 1) {
				if (fanClubDTO.getClubs() != null && !fanClubDTO.getClubs().isEmpty()) {
					List<FanClubFireBase> clubList = new ArrayList<FanClubFireBase>();
					for (FanClub fanClub : fanClubDTO.getClubs()) {
						FanClubFireBase fc = new FanClubFireBase();
						fc.setId(fanClub.getId());
						fc.setImageUrl(fanClub.getImageUrl());
						fc.setName(fanClub.getTitle());
						fc.setNOM(fanClub.getMemberCount());
						clubList.add(fc);
					}
					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("fanClubArray", clubList);
					contestInfo.updateChildrenAsync(cMap);
					updateDashbord.setValueAsync(new Date().getTime());
					firebaseLog.info("DashBoard Channels Updated  -- " + cMap.size());
				}

				if (fanClubDTO.getCategories() != null && !fanClubDTO.getCategories().isEmpty()) {
					List<MediaChannelFireBase> channelList = new ArrayList<MediaChannelFireBase>();
					for (PollingVideoCategory channels : fanClubDTO.getCategories()) {
						MediaChannelFireBase fc = new MediaChannelFireBase();
						fc.setId(channels.getId());
						fc.setImageUrl(channels.getImageUrl());
						fc.setName(channels.getCategoryName());
						channelList.add(fc);
					}
					Map<String, Object> cMap = new HashMap<String, Object>();
					cMap.put("tvChannelsArray", channelList);
					contestInfo.updateChildrenAsync(cMap);
					updateDashbord.setValueAsync(new Date().getTime());
					firebaseLog.info("DashBoard Channels Updated  -- " + cMap.size());
				}

			} else {
				firebaseLog.error("TACKERAPI ERROR: ERROR OCCURED WHILE UPDATING DASHBOARD CHANNEL INFORMATION ");
			}
		} catch (

		Exception e) {
			firebaseLog.error("ERROR OCCURED WHILE UPDATING UPCOMING CONTEST DETAILS.");
			e.printStackTrace();
		}
	}

}
