package com.rtw.tracker.firebase;

public class ContestFirebase {

	private Integer contestId;
	private String contestName;
	private String nextGameTime;
	private String nextGameDate;
	private String type;
	private String imageUrl;
	private String shareText;
	
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public String getNextGameTime() {
		return nextGameTime;
	}
	public void setNextGameTime(String nextGameTime) {
		this.nextGameTime = nextGameTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getShareText() {
		return shareText;
	}
	public void setShareText(String shareText) {
		this.shareText = shareText;
	}
	public String getNextGameDate() {
		return nextGameDate;
	}
	public void setNextGameDate(String nextGameDate) {
		this.nextGameDate = nextGameDate;
	}
	
	
	
}
