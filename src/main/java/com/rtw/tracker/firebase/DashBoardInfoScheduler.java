package com.rtw.tracker.firebase;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class DashBoardInfoScheduler extends QuartzJobBean implements StatefulJob{
	

		@Override
		protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
			try {
				if(FirestoreUtil.isContestStarted){
					System.out.println("RETURN : DashBoardInfoScheduler");
					return;
				}
				System.out.println("UPDATING FANCLUB DASHBOARD  : "+new Date());
				FirestoreUtil.updatePopularChannelDetails();
				System.out.println("FANCLUB DASHBOARD UPDATED : "+new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}


}
