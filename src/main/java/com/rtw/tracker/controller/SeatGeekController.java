package com.rtw.tracker.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.SeatGeekOrderDetailsDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

@Controller
@RequestMapping(value="/SeatGeek")
public class SeatGeekController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SeatGeekController.class);
	
	/**
	 * Manage invoice screen
	 * @param map
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/SgOpenOrders")
	public String loadOrdersPage(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{	
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String selectedTab = request.getParameter("selectedTab");
			String orderNo = request.getParameter("orderNo");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("selectedTab", selectedTab);
			map.put("orderNo", orderNo);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_SEATGEEK_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SeatGeekOrderDetailsDTO seatGeekOrderDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("seatGeekOrderDetailsDTO")), SeatGeekOrderDetailsDTO.class);
			
			if(seatGeekOrderDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", seatGeekOrderDetailsDTO.getMessage());
				model.addAttribute("status", seatGeekOrderDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", seatGeekOrderDetailsDTO.getError().getDescription());
				model.addAttribute("status", seatGeekOrderDetailsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", seatGeekOrderDetailsDTO.getSeatGeekPaginationDTO());
				model.addAttribute("seatGeekOrders", seatGeekOrderDetailsDTO.getSeatGeekOrdersDTO());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(seatGeekOrderDetailsDTO.getSeatGeekPaginationDTO()));
				model.addAttribute("seatGeekOrders", gson.toJson(seatGeekOrderDetailsDTO.getSeatGeekOrdersDTO()));
			}
			model.addAttribute("fromDate", seatGeekOrderDetailsDTO.getFromDate());
			model.addAttribute("toDate", seatGeekOrderDetailsDTO.getToDate());
			model.addAttribute("selectedTab", seatGeekOrderDetailsDTO.getSelectedTab());
			model.addAttribute("orderNo", seatGeekOrderDetailsDTO.getOrderNo()!= null? seatGeekOrderDetailsDTO.getOrderNo(): "");
			
			/*String fromDateFinal = "";
			String toDateFinal = "";
			Integer count = 0;
			List<SeatGeekOrders> sgOrders = null;
			String statusStr = null;
			if(seletedTab != null && !seletedTab.isEmpty()){
			if(seletedTab.equals("NewOrders")) {
				statusStr = "'submitted','failed'";			
			} else if(seletedTab.equals("Confirmed")) {
				statusStr = "'confirmed'";			
			} else if(seletedTab.equals("FullFilled")) {
				statusStr = "'fulfilled'";			
			} else if(seletedTab.equals("Rejected")) {
				statusStr = "'denied'";			
			}}else{
				seletedTab = "NewOrders";
				statusStr = "'submitted','failed'";
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersList(fromDateFinal, toDateFinal, statusStr, filter, null);
				count = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListCount(fromDateFinal, toDateFinal, statusStr, filter);
			}else {
				sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersList(null, null, statusStr, filter, null);
				count = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListCount(null, null, statusStr, filter);
			}
			
			map.put("seatGeekOrders", JsonWrapperUtil.getSeatGeekOrderArray(sgOrders));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			map.put("selectedTab", seletedTab);
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "page-seat-geek-orders";
	}
	
	
	/*@RequestMapping(value = "/GetSgOpenOrders")
	public void loadOrders(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			JSONObject returnObject = new JSONObject();
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String orderNo = request.getParameter("orderNo");
			String action = request.getParameter("action");
			String seletedTab = request.getParameter("selectedTab");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String fromDateFinal = "";
			String toDateFinal = "";
			List<SeatGeekOrders> sgOrders = null;
			Integer count = 0;
			String statusStr = null;
			if(seletedTab == null) {
				seletedTab = "NewOrders";
			}
			if(seletedTab.equals("NewOrders")) {
				statusStr = "'submitted','failed'";			
			} else if(seletedTab.equals("Confirmed")) {
				statusStr = "'confirmed'";			
			} else if(seletedTab.equals("FullFilled")) {
				statusStr = "'fulfilled'";			
			} else if(seletedTab.equals("Rejected")) {
				statusStr = "'denied'";			
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getSeatGeekSearchHeaderFilters(headerFilter);
			if(action!=null && action.equalsIgnoreCase("search")) {
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersList(fromDateFinal, toDateFinal, statusStr, filter, pageNo);
					count = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListCount(fromDateFinal, toDateFinal, statusStr, filter);
				}else {
					sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersList(null, null, statusStr,filter,pageNo);
					count = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListCount(null, null, statusStr, filter);
				}
			}
			else if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty())){
//				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//				Calendar cal = Calendar.getInstance();
//				Date toDate = new Date();
//				cal.add(Calendar.MONTH, -3);
//				Date fromDate = cal.getTime();
//				fromDateStr = dateFormat.format(fromDate);
//				toDateStr = dateFormat.format(toDate);
//				fromDateFinal = fromDateStr + " 00:00:00";
//				toDateFinal = toDateStr + " 23:59:59";
				sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersList(fromDateFinal, toDateFinal, statusStr, filter, pageNo);
				count = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListCount(fromDateFinal, toDateFinal, statusStr, filter);
			}else{
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
					sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersList(fromDateFinal, toDateFinal, statusStr, filter, pageNo);
					count = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListCount(fromDateFinal, toDateFinal, statusStr, filter);
				}
			}
			
			returnObject.put("seatGeekOrders", JsonWrapperUtil.getSeatGeekOrderArray(sgOrders));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			returnObject.put("selectedTab", seletedTab);
			returnObject.put("orderNo", orderNo);
			returnObject.put("fromDate", fromDateStr);
			returnObject.put("toDate", toDateStr);
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
			//returnObject.put("errorMessage", "There is something wrong...Please try again");
		}		
	}*/
	
	@RequestMapping(value = "/UpdateSgOrders")
	public String loadSoldTicketOfEvent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException{
		
		try {			
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userName", username);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_SEATGEEK_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String []requestParams = request.getParameter("updateOrderrecords").split("_");
			String action = request.getParameter("action");
			String status = request.getParameter("status");
			String msg= "";
			Integer id = 0;
			String seatGeekOrderIdStr = "";
			
			if(!SeatGeekWSUtil.isSeatGeekAPIEnabled()) {
				msg =  "SeatGeek API access is in disabled mode.";
			} else {
				for (String keyString : requestParams) {
					 String []record = keyString.split(",");
					 if(record.length == 2 && !record[0].isEmpty() && !record[1].isEmpty()){
						 id=Integer.parseInt(record[0]);
						 seatGeekOrderIdStr += id+",";
						 String orderId=record[1];
						
						 try {
							 SeatGeekOrders sgOrder = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrderByOrderId(orderId);
							 if (sgOrder != null) {
								 if(!sgOrder.getStatus().equals(SeatGeekOrderStatus.failed) &&
										 !sgOrder.getStatus().equals(SeatGeekOrderStatus.submitted)) {
									 continue;
								 }
								 SeatGeekOrderStatus sgStatus = null;
								 if(action.equals("confirmed")) {
									 sgStatus = SeatGeekOrderStatus.confirmed;
								 } else if(action.equals("denied")) {
									 sgStatus = SeatGeekOrderStatus.denied;
								 }
								 String message = SeatGeekWSUtil.updatingOrderStatus(orderId, sgStatus);
								 if(!message.equals("200")) {
	
									 SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
									 sgWsTrackingOne.setAction("Updating Order status");
									 sgWsTrackingOne.setOrderId(orderId);
									 sgWsTrackingOne.setCreatedDate(new Date());
									 sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
									 sgWsTrackingOne.setMessage("Error While "+action+" order");
									 sgWsTrackingOne.setError(message);
									 TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
								
									continue;
								}
								SeatGeekOrders sgOrdersTemp = new SeatGeekOrders();
								message = SeatGeekWSUtil.getOrderByOrderId(orderId, sgOrdersTemp);
								if(message == null || !message.equals("200")) {
									SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
									sgWsTrackingOne.setAction("Getting Orders by Order Id");
									sgWsTrackingOne.setOrderId(orderId);
									sgWsTrackingOne.setCreatedDate(new Date());
									sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
									sgWsTrackingOne.setMessage("Error While Getting Orders by Order Id in updaating order status to "+action);
									sgWsTrackingOne.setError(message);
									TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
								
									continue;
								}
								if(!sgOrdersTemp.getStatus().equals(sgStatus)) {
									SeatGeekWSTracking sgWsTrackingOne = new SeatGeekWSTracking(); 
									sgWsTrackingOne.setAction("Updating Order status");
									sgWsTrackingOne.setOrderId(orderId);
									sgWsTrackingOne.setCreatedDate(new Date());
									sgWsTrackingOne.setStatus(SeatGeekWSTrackingStatus.FAILED);
									sgWsTrackingOne.setMessage("Order status not updated in seatgeek While updating order to "+action);
									sgWsTrackingOne.setError(message);
									TMATDAORegistry.getSeatGeekWSTrackingDAO().save(sgWsTrackingOne);
									
//									//If order is mismatched with API then update in DB
//									if(!sgOrder.getStatus().equals(sgOrdersTemp.getStatus())) {
//										sgOrder.setStatus(sgOrdersTemp.getStatus());
//										DAORegistry.getSeatGeekOrdersDAO().update(sgOrder);
//									}
									continue;
								}
								
								if(action.equals("confirmed")) {
									SeatGeekWSUtil.createCustomerOrder(sgOrder);
									sgOrder.setAcceptedBy(username);
								} else {
									sgOrder.setRejectedBy(username);
								}
								sgOrder.setStatus(sgStatus);
								sgOrder.setLastUpdatedDate(new Date());
									 
								DAORegistry.getSeatGeekOrdersDAO().update(sgOrder);
							}
						 } catch(Exception e) {
							 e.printStackTrace();
						 }
					 }
				}
				msg= "Selected record updated.";
				
				//Tracking User Action
				seatGeekOrderIdStr = seatGeekOrderIdStr.substring(0, seatGeekOrderIdStr.length()-1);
				String userActionMsg = "";
				if(action != null && action.equalsIgnoreCase("confirmed")) {
					userActionMsg = "SeatGeekOrder(s) are Confirmed. SeatGeekOrder Id's - "+seatGeekOrderIdStr;
				}else if(action != null && action.equalsIgnoreCase("denied")){
					userActionMsg = "SeatGeekOrder(s) are Rejected. SeatGeekOrder Id's - "+seatGeekOrderIdStr;
				}
				Util.userActionAudit(request, null, userActionMsg);
				
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");			
		}
		return "";
	}
	
	/*@RequestMapping(value = "/ExportToExcel")
	public void exportToExcel(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String orderNo = request.getParameter("orderNo");
		String seletedTab = request.getParameter("selectedTab");
		String headerFilter = request.getParameter("headerFilter");
		String fromDateFinal = null;
		String toDateFinal = null;
		List<SeatGeekOrders> sgOrders = null;
		String statusStr = null;
		if(seletedTab == null) {
			seletedTab = "NewOrders";
		}
		if(seletedTab.equals("NewOrders")) {
			statusStr = "'submitted','failed'";			
		} else if(seletedTab.equals("Confirmed")) {
			statusStr = "'confirmed'";			
		} else if(seletedTab.equals("FullFilled")) {
			statusStr = "'fulfilled'";			
		} else if(seletedTab.equals("Rejected")) {
			statusStr = "'denied'";			
		}
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getSeatGeekSearchHeaderFilters(headerFilter);
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListToExport(orderNo, fromDateFinal, toDateFinal, statusStr, filter);
			}else{
				sgOrders = DAORegistry.getSeatGeekOrdersDAO().getSeatGeekOrdersListToExport(orderNo, fromDateFinal, toDateFinal, statusStr, filter);
			}
		
			if(sgOrders!=null && !sgOrders.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("seatgeek_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Order Id");
				header.createCell(1).setCellValue("Order Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("Section");
				header.createCell(7).setCellValue("Row");
				header.createCell(8).setCellValue("Quantity");
				header.createCell(9).setCellValue("Total Sale");
				header.createCell(10).setCellValue("Total Payment");
				header.createCell(11).setCellValue("Status");
				header.createCell(12).setCellValue("Last Updated");
				header.createCell(13).setCellValue("TicTracker Order");
				header.createCell(14).setCellValue("TicTracker Invoice");
				int i=1;
				for(SeatGeekOrders sgOrder : sgOrders){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(sgOrder.getOrderId());
					row.createCell(1).setCellValue(sgOrder.getOrderDateStr());
					row.createCell(2).setCellValue(sgOrder.getEventName());
					row.createCell(3).setCellValue(sgOrder.getEventDateStr());
					row.createCell(4).setCellValue(sgOrder.getEventTimeStr());
					row.createCell(5).setCellValue(sgOrder.getVenueName());
					row.createCell(6).setCellValue(sgOrder.getSection());
					row.createCell(7).setCellValue(sgOrder.getRow());
					row.createCell(8).setCellValue(sgOrder.getQuantity().toString());
					row.createCell(9).setCellValue(sgOrder.getTotalSaleAmt().toString());
					row.createCell(10).setCellValue(sgOrder.getTotalPaymentAmt().toString());
					row.createCell(11).setCellValue(sgOrder.getStatus().toString());
					row.createCell(12).setCellValue(sgOrder.getLastUpdatedDateStr());
					row.createCell(13).setCellValue(sgOrder.getTicTrackerOrderId()!=null?sgOrder.getTicTrackerOrderId():0);
					row.createCell(14).setCellValue(sgOrder.getTicTrackerInvoiceId()!=null?sgOrder.getTicTrackerInvoiceId():0);
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=seatgeek_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
}
