package com.rtw.tracker.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.ecomerce.pojo.CouponCodeMstDTO;
import com.rtf.ecomerce.pojo.CouponCodeProductsMapDTO;
import com.rtf.ecomerce.pojo.OrderResp;
import com.rtf.ecomerce.pojo.RTFSellerlDTO;
import com.rtf.ecomerce.pojo.SellerProdItmlDTO;
import com.rtf.ecomerce.pojo.SellerProductUploadTO;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
@RequestMapping({"/ecomm"})
public class EcommController {

	
	SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	
	
	
	@RequestMapping({"/ManageSellers"})
	public String loadSellersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_SELLERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RTFSellerlDTO sellerDTO = gson.fromJson(((JsonObject)jsonObject.get("RTFSellerlDTO")), RTFSellerlDTO.class);
			
			if(sellerDTO.getStatus() == 1){				
				model.addAttribute("msg",sellerDTO.getMessage());
				model.addAttribute("status", status);
			}else{
				model.addAttribute("msg", sellerDTO.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("sellerList", sellerDTO.getRtfSellers());
				model.addAttribute("pagingInfo", sellerDTO.getPagination());
			}else{
				model.addAttribute("sellerList", gson.toJson(sellerDTO.getRtfSellers()));
				model.addAttribute("pagingInfo", gson.toJson(sellerDTO.getPagination()));
			}
			model.addAttribute("sts", sellerDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-admin-manage-sellers";
	}
	
	
	
	
	@RequestMapping({"/UpdateSeller"})
	public String addSellerPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
						
		try{
			//String returnMessage = "";
			String action = request.getParameter("action");
			String fName = request.getParameter("fName");
			String lName = request.getParameter("lName");
			String compName = request.getParameter("compName");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String addLine1 = request.getParameter("addLine1");
			String addLine2 = request.getParameter("addLine2");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String country = request.getParameter("country");
			String pincode = request.getParameter("pincode");
			String status = request.getParameter("status");
			String sellerId = request.getParameter("sellerId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Gson gson = GsonCustomConfig.getGsonBuilder();
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("action", action);
			paramMap.put("userName", userName);
			paramMap.put("fName", fName);
			paramMap.put("lName", lName);
			paramMap.put("compName", compName);
			paramMap.put("email", email);
			paramMap.put("phone", phone);
			paramMap.put("addLine1", addLine1);
			paramMap.put("addLine2", addLine2);
			paramMap.put("city", city);
			paramMap.put("state", state);
			paramMap.put("country", country);
			paramMap.put("pincode", pincode);
			paramMap.put("status", status);
			paramMap.put("sellerId", sellerId);
			
			
			String data = Util.getObject(paramMap, Constants.TICTRACKER_API_BASE_URL+Constants.UPDATE_SELLERS);
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RTFSellerlDTO sellerDTO = gson.fromJson(((JsonObject)jsonObject.get("RTFSellerlDTO")), RTFSellerlDTO.class);
			
			if(sellerDTO.getStatus() == 1){
				model.addAttribute("sellerList", sellerDTO.getRtfSellers());
				model.addAttribute("seller", sellerDTO.getRtfSeller());
				model.addAttribute("msg", sellerDTO.getMessage());
				model.addAttribute("pagingInfo", sellerDTO.getPagination());
			}else{
				model.addAttribute("msg", sellerDTO.getError().getDescription());
			}
			model.addAttribute("sts", sellerDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong..Please Try Again.");
		}				
		return "";
	}
	
	@RequestMapping({"/ManageProdOrders"})
	public String loadManageOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			//String userId = SecurityContextHolder.getContext().getAuthentication().getName();
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_MANAGE_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			/*OrderResp orderResp = new OrderResp();
			orderResp.setStatus(1);
			
			List<CustomerOrder> list = new ArrayList<CustomerOrder>();
			CustomerOrder order = new CustomerOrder();
			order.setId(1);
			order.setCrDate(new Date());
			order.setNetTot(100.0);
			order.setNoOfItems(5);
			order.setpPayType("CREDITCARD");
			order.setpPayAmt(100.0);
			order.setCustFirstName("Test");
			order.setCustLastName("Last");
			order.setCustEmail("test@gmail.com");
			order.setCustPhone("1234567890");
			list.add(order);
			list.add(order);
			list.add(order);
			orderResp.setOrders(list);
			PaginationDTO paginationDTO = new PaginationDTO();
			try {
				paginationDTO.setPageSize(0);
				paginationDTO.setPageNum(String.valueOf(0));
				paginationDTO.setTotalRows(0);
				paginationDTO.setTotalPages(1D);
			} catch (Exception e) {
				e.printStackTrace();
			}
			orderResp.setPagination(paginationDTO);*/
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
				model.addAttribute("status", status);
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("orderList", orderResp.getOrders());
				model.addAttribute("pagingInfo", orderResp.getPagination());
			}else{
				model.addAttribute("orderList", gson.toJson(orderResp.getOrders()));
				model.addAttribute("pagingInfo", gson.toJson(orderResp.getPagination()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-admin-manage-prod-orders";
	}
	
	@RequestMapping({"/GetCustOrderProducts"})
	public String getCustOrderProducts(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String status = request.getParameter("status");
			String orderId = request.getParameter("orderId");
			String sellerId = request.getParameter("sellerId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("pageNo", pageNo);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			map.put("orderId", orderId);
			map.put("sellerId", sellerId);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_ORDER_PRODUCTS_LIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
				model.addAttribute("status", status);
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("prodsList", orderResp.getProds());
				model.addAttribute("pagingInfo", orderResp.getPagination());
			}else{
				model.addAttribute("prodsList", gson.toJson(orderResp.getProds()));
				model.addAttribute("pagingInfo", gson.toJson(orderResp.getPagination()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	@RequestMapping({"/GetOrderProduct"})
	public String getOrderProduct(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String status = request.getParameter("status");
			String orderId = request.getParameter("orderProdId");
			String sellerId = request.getParameter("sellerId");
			
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			map.put("status", status);
			map.put("orderProdId", orderId);
			map.put("sellerId", sellerId);
			
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_ORDER_PRODUCT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
				model.addAttribute("status", status);
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("product", orderResp.getOrderProduct());
				model.addAttribute("order", orderResp.getOrder());
				model.addAttribute("inventory", orderResp.getInventory());
			}else{
				model.addAttribute("product", gson.toJson(orderResp.getOrderProduct()));
				model.addAttribute("order", gson.toJson(orderResp.getOrder()));
				model.addAttribute("inventory", gson.toJson(orderResp.getInventory()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping({"/UpdateOrderProductShpipping"})
	public String updateOrderProductShpipping(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String orderProdIdStr = request.getParameter("orderProdId");
			String delNote = request.getParameter("delNote");
			String shMethod = request.getParameter("shMethod");
			String shClass = request.getParameter("shClass");
			String shTrackId = request.getParameter("shTrackId");
			String expDelDateStr = request.getParameter("expDelDate");
			String orderId = request.getParameter("orderId");
			String status = request.getParameter("status");
			String prodDelivered = request.getParameter("prodDelivered");
			
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderProdId", orderProdIdStr);
			map.put("delNote", delNote);
			map.put("shMethod", shMethod);
			map.put("shClass", shClass);
			map.put("shTrackId", shTrackId);
			map.put("expDelDate", expDelDateStr);
			map.put("orderId", orderId);
			map.put("status", status);
			map.put("prodDelivered", prodDelivered);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.UPDATE_ORDER_PRODUCT_SHIPPING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("prodsList", orderResp.getProds());
				model.addAttribute("pagingInfo", orderResp.getPagination());
			}else{
				model.addAttribute("prodsList", gson.toJson(orderResp.getProds()));
				model.addAttribute("pagingInfo", gson.toJson(orderResp.getPagination()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	@RequestMapping({"/UpdateOrderProductToDelivered"})
	public String UpdateOrderProductToDelivered(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String orderProdIdStr = request.getParameter("orderProdId");
			String orderId = request.getParameter("orderId");
			String status = request.getParameter("status");
			String action = request.getParameter("action");
			String delDate = request.getParameter("delDate");
			
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderProdId", orderProdIdStr);
			map.put("orderId", orderId);
			map.put("delDate", delDate);
			map.put("status", status);
			map.put("action", action);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.UPDATE_ORDER_PRODUCT_TO_DELIVERED);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("prodsList", orderResp.getProds());
				model.addAttribute("pagingInfo", orderResp.getPagination());
			}else{
				model.addAttribute("prodsList", gson.toJson(orderResp.getProds()));
				model.addAttribute("pagingInfo", gson.toJson(orderResp.getPagination()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	@RequestMapping({"/UpdateOrderProductStatus"})
	public String updateOrderProductStatus(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String orderProdIdStr = request.getParameter("orderProdId");
			String orderId = request.getParameter("orderId");
			String status = request.getParameter("status");
			String action = request.getParameter("action");
			
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderProdId", orderProdIdStr);
			map.put("orderId", orderId);
			map.put("status", status);
			map.put("action", action);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.UPDATE_ORDER_PRODUCT_STATUS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("prodsList", orderResp.getProds());
				model.addAttribute("pagingInfo", orderResp.getPagination());
			}else{
				model.addAttribute("prodsList", gson.toJson(orderResp.getProds()));
				model.addAttribute("pagingInfo", gson.toJson(orderResp.getPagination()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	@RequestMapping({"/VoidOrderProduct"})
	public String voidOrderPRoduct(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String orderProdIdStr = request.getParameter("orderProdId");
			String orderId = request.getParameter("orderId");
			String status = request.getParameter("status");
			String refundAmt = request.getParameter("refundAmt");
			
			Boolean isSellelr = (Boolean)session.getAttribute("isSeller");
			Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderProdId", orderProdIdStr);
			map.put("refundAmt", refundAmt);
			map.put("orderId", orderId);
			map.put("status", status);
			map.put("isSeller", String.valueOf(isSellelr));
			map.put("userId",  String.valueOf(userId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.VOID_ORDER_PRODUCT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("orderResp")), OrderResp.class);
			
			if(orderResp.getStatus() == 1){				
				model.addAttribute("msg",orderResp.getMessage());
			}else{
				model.addAttribute("msg", orderResp.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("prodsList", orderResp.getProds());
				model.addAttribute("pagingInfo", orderResp.getPagination());
			}else{
				model.addAttribute("prodsList", gson.toJson(orderResp.getProds()));
				model.addAttribute("pagingInfo", gson.toJson(orderResp.getPagination()));
			}
			model.addAttribute("sts", orderResp.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping({"/addproditmvaroptns"})
	public String addproditmvaroptns(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String slrpitmsId = request.getParameter("slrpitmsId");
			String slerId = request.getParameter("slerId");
			String optVal1 = request.getParameter("optVal1");
			String optVal2 = request.getParameter("optVal2");
			String optVal3 = request.getParameter("optVal3");
			String optVal4 = request.getParameter("optVal4");
			
			String userName = request.getParameter("userName");
			//Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", slrpitmsId);
			map.put("slerId", slerId);
			map.put("userName", userName);
			map.put("optVal1", optVal1);
			map.put("optVal2", optVal2);
			map.put("optVal3", optVal3);
			map.put("optVal4", optVal4);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.ADD_PROD_ITM_VARIANT_OPTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO prodDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			if(prodDTO.getStatus() == 1){				
				model.addAttribute("msg",prodDTO.getMessage());
			}else{
				model.addAttribute("msg", prodDTO.getError().getDescription());
			}
			
			/*if(request.getRequestURI().contains(".json")){
				//model.addAttribute("prodsList", prodDTO.getProds());
				//model.addAttribute("pagingInfo", prodDTO.getPagination());
			}else{
				//model.addAttribute("prodsList", gson.toJson(prodDTO.getProds()));
				//model.addAttribute("pagingInfo", gson.toJson(prodDTO.getPagination()));
			}*/
			model.addAttribute("sts", prodDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping({"/getproditmvaroptns"})
	public String getproditmvaroptns(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			
			//Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", sellerprodidStr);
			map.put("slerId", sellerIdStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_PROD_ITM_VARIANT_OPTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO prodDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			if(prodDTO.getStatus() == 1){				
				model.addAttribute("msg",prodDTO.getMessage());
			}else{
				model.addAttribute("msg", prodDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("variantsList", prodDTO.getVariantsList());
				model.addAttribute("hasInventory", prodDTO.getHasInventory());
				//model.addAttribute("pagingInfo", prodDTO.getPagination());
			}else{
				model.addAttribute("variantsList", gson.toJson(prodDTO.getVariantsList()));
				model.addAttribute("hasInventory", gson.toJson(prodDTO.getHasInventory()));
				//model.addAttribute("pagingInfo", gson.toJson(prodDTO.getPagination()));
			}
			model.addAttribute("sts", prodDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping({"/genProdInventory"})
	public String genProdInventory(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String userName = request.getParameter("userName");
			String headerFilter = request.getParameter("headerFilter");
			
			//Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", sellerprodidStr);
			map.put("slerId", sellerIdStr);
			map.put("userName", userName);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GENERATE_PROD_ITM_INVENTORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO prodDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			if(prodDTO.getStatus() == 1){				
				model.addAttribute("msg",prodDTO.getMessage());
			}else{
				model.addAttribute("msg", prodDTO.getError().getDescription());
			}
			
			/*if(request.getRequestURI().contains(".json")){
				model.addAttribute("variantsList", prodDTO.getVariantsList());
				//model.addAttribute("pagingInfo", prodDTO.getPagination());
			}else{
				model.addAttribute("variantsList", gson.toJson(prodDTO.getVariantsList()));
				//model.addAttribute("pagingInfo", gson.toJson(prodDTO.getPagination()));
			}*/
			model.addAttribute("sts", prodDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	@RequestMapping({"/getProdInventories"})
	public String getProdInventories(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String userName = request.getParameter("userName");
			
			//Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", sellerprodidStr);
			map.put("slerId", sellerIdStr);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_PROD_ITM_INVENTORIES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO prodDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			if(prodDTO.getStatus() == 1){				
				model.addAttribute("msg",prodDTO.getMessage());
			}else{
				model.addAttribute("msg", prodDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("inventoryList", prodDTO.getInventoryList());
				model.addAttribute("pagingInfo", prodDTO.getPagination());
			}else{
				model.addAttribute("inventoryList", gson.toJson(prodDTO.getInventoryList()));
				model.addAttribute("pagingInfo", gson.toJson(prodDTO.getPagination()));
			}
			model.addAttribute("sts", prodDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	@RequestMapping({"/updateProdInventories"})
	public String updateProdInventories(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String sellerIdStr = request.getParameter("slerId");
			String userName = request.getParameter("userName");
			String invIdsStr = request.getParameter("invIds");
			
			//Integer userId = (Integer) session.getAttribute("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", sellerprodidStr);
			map.put("slerId", sellerIdStr);
			map.put("userName", userName);
			map.put("invIds", invIdsStr);
			
			/*if(invIdsStr != null) {
				String[] invIdsArr = invIdsStr.split(",");
				for(int i=0;i<invIdsArr.length;i++) {
					String idStr = invIdsArr[i];
					map.put("regPrice_"+idStr, request.getParameter("regPrice_"+idStr));
					map.put("selPrice_"+idStr, request.getParameter("selPrice_"+idStr));
					map.put("avlQty_"+idStr, request.getParameter("avlQty_"+idStr));
					map.put("reqLPoints_"+idStr, request.getParameter("reqLPoints_"+idStr));
					map.put("priceAftLPnts_"+idStr, request.getParameter("priceAftLPnts_"+idStr));
				}
			}*/
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.UPDATE_PROD_ITM_INVENTORIES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO prodDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			if(prodDTO.getStatus() == 1){				
				model.addAttribute("msg",prodDTO.getMessage());
			}else{
				model.addAttribute("msg", prodDTO.getError().getDescription());
			}
			
			/*if(request.getRequestURI().contains(".json")){
				model.addAttribute("inventoryList", prodDTO.getInventoryList());
				model.addAttribute("pagingInfo", prodDTO.getPagination());
			}else{
				model.addAttribute("inventoryList", gson.toJson(prodDTO.getInventoryList()));
				model.addAttribute("pagingInfo", gson.toJson(prodDTO.getPagination()));
			}*/
			model.addAttribute("sts", prodDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping({"/ManageProducts"})
	public String loadProductsPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_PRODUCTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO prodDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			
			Map<String, String> map1 = Util.getParameterMap(request);
			map1.put("status", "ACTIVE");
			map1.put("pageNo", pageNo);
			map1.put("headerFilter", headerFilter);
			
			String data1 = Util.getObject(map1, Constants.TICTRACKER_API_BASE_URL+Constants.GET_SELLERS);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			RTFSellerlDTO sellerDTO = gson1.fromJson(((JsonObject)jsonObject1.get("RTFSellerlDTO")), RTFSellerlDTO.class);
			
			if(prodDTO.getStatus() == 1){				
				model.addAttribute("msg",prodDTO.getMessage());
				model.addAttribute("status", status);
				model.addAttribute("sellerList", sellerDTO.getRtfSellers());
			}else{
				model.addAttribute("msg", prodDTO.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("prodList", prodDTO.getList());
				model.addAttribute("pagingInfo", prodDTO.getPagination());
			}else{
				model.addAttribute("prodList", gson.toJson(prodDTO.getList()));
				model.addAttribute("pagingInfo", gson.toJson(prodDTO.getPagination()));
			}
			model.addAttribute("sts", prodDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-product-management";
	}
	
	
	
	
	@RequestMapping({"/UpdateProduct"})
	public String updateProduct(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
						
		try{
			/*String headerFilter  = request.getParameter("headerFilter");
			String sellerprodidStr = request.getParameter("slrpitmsId");
			String action = request.getParameter("action");
			String sellerIdStr = request.getParameter("slerId");
			String pbrand = request.getParameter("pbrand");
			String pname = request.getParameter("pname");
			String pdesc = request.getParameter("pdesc");
			String plongdesc = request.getParameter("plongdesc");
			String pimg = request.getParameter("pimg");
			String psku = request.getParameter("psku");	
			String pstatus = request.getParameter("pstatus");		
			String cau = request.getParameter("cau");	
			String isvariantstr = request.getParameter("isvariant");
			String pselMinPrcstr = request.getParameter("pselMinPrc");
			String pregMinPrcstr = request.getParameter("pregMinPrc");	
			String maxcustqtystr = request.getParameter("maxcustqty");
			String dispstartdatestr=request.getParameter("dispstartdate");
			String pexpdatestr = request.getParameter("pexpdate");
			String isimgupd =  request.getParameter("isimgupd");*/
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Gson gson = GsonCustomConfig.getGsonBuilder();
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("userName", userName);
			/*paramMap.put("action", action);
			paramMap.put("userName", userName);
			paramMap.put("slrpitmsId", sellerprodidStr);
			paramMap.put("slerId", sellerIdStr);
			paramMap.put("pbrand", pbrand);
			paramMap.put("pname", pname);
			paramMap.put("pdesc", pdesc);
			paramMap.put("plongdesc", plongdesc);
			paramMap.put("pimg", pimg);
			paramMap.put("psku", psku);
			paramMap.put("pstatus", pstatus);
			paramMap.put("cau", cau);
			paramMap.put("isvariant", isvariantstr);
			paramMap.put("pselMinPrc", pselMinPrcstr);
			paramMap.put("pregMinPrc", pregMinPrcstr);
			paramMap.put("maxcustqty", maxcustqtystr);
			paramMap.put("dispstartdate", dispstartdatestr);
			paramMap.put("pexpdate", pexpdatestr);
			paramMap.put("isimgupd", isimgupd);*/
			
			
			String data = Util.getObjectFromApi(paramMap,request, Constants.TICTRACKER_API_BASE_URL+Constants.UPDATE_PRODUCT);
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProdItmlDTO sellerDTO = gson.fromJson(((JsonObject)jsonObject.get("sellerProdItmlDTO")), SellerProdItmlDTO.class);
			
			if(sellerDTO.getStatus() == 1){
				model.addAttribute("prodList", sellerDTO.getList());
				model.addAttribute("prod", sellerDTO.getSellerProductsItem());
				model.addAttribute("msg", sellerDTO.getMessage());
				model.addAttribute("pagingInfo", sellerDTO.getPagination());
			}else{
				model.addAttribute("msg", sellerDTO.getError().getDescription());
			}
			model.addAttribute("sts", sellerDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong..Please Try Again.");
		}				
		return "";
	}

	public static void main(String[] args) throws Exception {
		
		Map<String, String> map = new HashMap<String, String>(); //Util.getParameterMap(request);
		map.put("status", "ACTIVE");
		map.put("userId", "1");
		
		String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_MANAGE_ORDERS);
		Gson gson = GsonCustomConfig.getGsonBuilder();		
		JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
		OrderResp orderResp = gson.fromJson(((JsonObject)jsonObject.get("OrderResp")), OrderResp.class);
		System.out.println("1111"+data);
	}
	

	

	@RequestMapping({"/CouponCodes"})
	public String getCouponCodes(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String ccStatus = request.getParameter("ccStatus");
			//String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("ccStatus", ccStatus);
			//map.put("status", status);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_COUPON_CODES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			
			CouponCodeMstDTO couponCodeMstDTO = gson.fromJson(((JsonObject)jsonObject.get("couponCodeMstDTO")), CouponCodeMstDTO.class);
			
			if(couponCodeMstDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("couponcodelst", couponCodeMstDTO.getCoupCodeMstrList());
					model.addAttribute("pagingInfo", couponCodeMstDTO.getPagination());
				}else{
					model.addAttribute("couponcodelst", gson.toJson(couponCodeMstDTO.getCoupCodeMstrList()));
					model.addAttribute("pagingInfo",gson.toJson(couponCodeMstDTO.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", couponCodeMstDTO.getError().getDescription());
			}
			model.addAttribute("ccStatus",ccStatus);
			model.addAttribute("status",couponCodeMstDTO.getStatus());
			
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-couponcode-management";
	}
	
	@RequestMapping({"/UploadInventory"})
	public String uploadRTFInventory(HttpServletRequest request,Model model){		
	try {	
		String action = request.getParameter("action");
		System.out.println(" IN ECOMM CONTROLLER ");
		if("UPLOAD".equals(action)) {
			System.out.println(" IN ECOMM CONTROLLER FOR UPLOAD ACTION ");
			Map<String, String> map = Util.getParameterMap(request);			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPLOAD_RTFINVENTORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SellerProductUploadTO dto = gson.fromJson(((JsonObject)jsonObject.get("sellerProductUploadTO")), SellerProductUploadTO.class);

			if(dto.getStatus() == 1){
				model.addAttribute("msg", "Inventory Hs Been Uploaded Successfully");
				model.addAttribute("status",1);
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
				model.addAttribute("status",0);
			}			
		}		
		
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "page-product-uploadinventory";	
	}
	@RequestMapping(value = "/UpdateCouponCode")
	public String updateCouponCode(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			
			String action = request.getParameter("action");
			String ccId = request.getParameter("ccId");	
			String coucde = request.getParameter("coucde");
			String couname = request.getParameter("couname");
			String coudesc = request.getParameter("coudesc");
			String coudiscount = request.getParameter("coudiscount");
			String coustatus = request.getParameter("coustatus");
			String discounttype = request.getParameter("discounttype");	
			String coutype = request.getParameter("coutype");
			String exprydateStr = request.getParameter("exprydate");
			String ccStatus = request.getParameter("ccStatus");			
		
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);	
			map.put("ccId", ccId);
			map.put("coucde", coucde);
			map.put("couname", couname);
			map.put("coudesc",coudesc);			
			map.put("coudiscount", coudiscount);			
			map.put("discounttype", discounttype);
			map.put("coutype", coutype);
			map.put("cau", userName);
			map.put("exprydateStr", exprydateStr);			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_COUPON_CODES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			CouponCodeMstDTO couponCodeMstDTO = gson.fromJson(((JsonObject)jsonObject.get("couponCodeMstDTO")), CouponCodeMstDTO.class);
			
			if(couponCodeMstDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("couponcodelst", couponCodeMstDTO.getCoupCodeMstrList());
					model.addAttribute("couponCodeMstr", couponCodeMstDTO.getCoupCodeMstr());
					model.addAttribute("pagingInfo", couponCodeMstDTO.getPagination());
				}else{
					model.addAttribute("couponcodelst", gson.toJson(couponCodeMstDTO.getCoupCodeMstrList()));
					model.addAttribute("couponCodeMstr", gson.toJson(couponCodeMstDTO.getCoupCodeMstr()));
					model.addAttribute("pagingInfo",gson.toJson(couponCodeMstDTO.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", couponCodeMstDTO.getError().getDescription());
			}
			model.addAttribute("ccStatus",ccStatus);
			model.addAttribute("status",couponCodeMstDTO.getStatus());
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping({"/CouponCodeProdMapping"})
	public String getCouponCodeProdMapping(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String ccStatus = request.getParameter("ccStatus");			
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String ccId = request.getParameter("ccId");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("ccStatus", ccStatus);
			map.put("ccId", ccId);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GETCOUPONCODE_MAPPING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			
			CouponCodeProductsMapDTO dto = gson.fromJson(((JsonObject)jsonObject.get("couponCodeProductsMapDTO")), CouponCodeProductsMapDTO.class);
			
			if(dto.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("prodcoupmaplst", dto.getCoupCodeProdMapList());
					model.addAttribute("couprodmappagingInfo", dto.getPagination());
					//model.addAttribute("prodcoupmapexistlst", dto.getCoupCodeProdMapList());
				}else{
					model.addAttribute("prodcoupmaplst", gson.toJson(dto.getCoupCodeProdMapList()));
					model.addAttribute("couprodmappagingInfo",gson.toJson(dto.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
			}
			//model.addAttribute("ccStatus",ccStatus);
			//model.addAttribute("status",dto.getStatus());
						
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateCouponCodeProdMap")
	public String updateCouponCodeProductMapping(HttpServletRequest request, HttpServletResponse response,Model model){
		try {		
		
			String ccId = request.getParameter("ccId");	
			String prodIds = request.getParameter("slrpitmsIds");				
		
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", prodIds);	
			map.put("ccId", ccId);
					
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVECOUPONCODE_MAPPING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			CouponCodeProductsMapDTO dto = gson.fromJson(((JsonObject)jsonObject.get("couponCodeProductsMapDTO")), CouponCodeProductsMapDTO.class);
			
			if(dto.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("prodcoupmaplst", dto.getCoupCodeProdMapList());
					model.addAttribute("couprodmappagingInfo", dto.getPagination());					
				}else{
					model.addAttribute("prodcoupmaplst", gson.toJson(dto.getCoupCodeProdMapList()));
					model.addAttribute("couprodmappagingInfo",gson.toJson(dto.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
				
			}
			
			System.out.println ( " [ECOM CONTROLLER ]  " + dto );
			model.addAttribute("status",dto.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/RemoveCouponCodeProdMap")
	public String removeCouponCodeProductMapping(HttpServletRequest request, HttpServletResponse response,Model model){
		try {		
		
			String ccId = request.getParameter("ccId");	
			String prodIds = request.getParameter("slrpitmsIds");				
		
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("slrpitmsId", prodIds);	
			map.put("ccId", ccId);
					
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_REMOVECOUPONCODE_MAPPING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			CouponCodeProductsMapDTO dto = gson.fromJson(((JsonObject)jsonObject.get("couponCodeProductsMapDTO")), CouponCodeProductsMapDTO.class);
			
			if(dto.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("prodcoupmaplst", dto.getCoupCodeProdMapList());
					model.addAttribute("couprodmappagingInfo", dto.getPagination());					
				}else{
					model.addAttribute("prodcoupmaplst", gson.toJson(dto.getCoupCodeProdMapList()));
					model.addAttribute("couprodmappagingInfo",gson.toJson(dto.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
				
			}
			
			System.out.println ( " [ECOM CONTROLLER ]  " + dto );
			model.addAttribute("status",dto.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	
}
