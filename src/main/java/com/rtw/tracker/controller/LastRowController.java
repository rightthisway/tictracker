package com.rtw.tracker.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.EventDetails;
import com.rtw.tracker.datas.GrandChildCategory;
import com.rtw.tracker.datas.PopularArtist;
import com.rtw.tracker.datas.PopularEvents;
import com.rtw.tracker.datas.PopularGrandChildCategory;
import com.rtw.tracker.datas.PopularVenue;
import com.rtw.tracker.datas.Product;
import com.rtw.tracker.datas.ProductType;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.pojos.ZoneEvent;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GridHeaderFilters;

@Controller
@RequestMapping({"/LastRowTickets"})
public class LastRowController {@RequestMapping(value="/PopularEvents")
	public String populerEvents(HttpServletRequest request, HttpServletResponse response, ModelMap map){
	
	try {
		Product product = DAORegistry.getProductDAO().getProductByName(Constants.Last_Ten_Row_Tickets_Product);
		
		String searchType = request.getParameter("searchType");
		String searchValue = request.getParameter("searchValue");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		Collection<EventDetails> events = null;
		GridHeaderFilters filter = new GridHeaderFilters();
		if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty()) && (searchValue == null || searchValue.isEmpty())){
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date fromDate = new Date();
			Date toDate = new Date();
			toDate.setMonth(toDate.getMonth()+1);
			fromDateStr = dateFormat.format(fromDate);
			toDateStr = dateFormat.format(toDate);
			String fromDateFinal = fromDateStr + " 00:00:00";
			String toDateFinal = toDateStr + " 23:59:59";
			//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(null, null, fromDate, toDate,ProductType.REWARDTHEFAN);
			events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null, null, fromDateFinal, toDateFinal, ProductType.LASTROWMINICATS,filter,null);
		}else{
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:59";
				//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(searchType, searchValue, dateTimeFormat.parse(fromDateFinal), dateTimeFormat.parse(toDateFinal),ProductType.REWARDTHEFAN);
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType, searchValue, fromDateFinal, toDateFinal,ProductType.LASTROWMINICATS,filter,null);
			}else{
				//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(searchType, searchValue, null, null,ProductType.REWARDTHEFAN);
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType, searchValue, null, null,ProductType.LASTROWMINICATS,filter,null);
			}
		}
		Collection<PopularEvents> popEvents = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(product.getId());
		Integer totalCount = DAORegistry.getQueryManagerDAO().getEventCount();
		
		map.put("totalCount", totalCount);
		map.put("popEvents", popEvents);
		map.put("eventsList", events);
		map.put("fromDate", fromDateStr);
		map.put("toDate", toDateStr);
		map.put("product", product);
		
	} catch(Exception e){
		e.printStackTrace();
	}
	map.put("productName", "LastRow Tickets");
	map.put("url", "LastRowTickets");
	return "page-popular-events";
}
	
@RequestMapping(value="/PopularArtist")
public String popularArtist(HttpServletRequest request, HttpServletResponse response, ModelMap map){
	
	try {
		GridHeaderFilters filter = new GridHeaderFilters();
		Product product = DAORegistry.getProductDAO().getProductByName(Constants.Last_Ten_Row_Tickets_Product);
		Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(product.getId(),filter,null);
		Collection<EventDetails> artistList = DAORegistry.getQueryManagerDAO().getArtistDetails(null);
		//Collection<Artist> artistList = DAORegistry.getArtistDAO().getAll();
	
		map.put("popArtists", popArtists);
		map.put("artistList", artistList);
		map.put("product", product);
		
	} catch(Exception e){
		e.printStackTrace();
	}
	map.put("productName", "LastRow Tickets");
	map.put("url", "LastRowTickets");
	return "page-popular-artist";
}

@RequestMapping(value="/PopularGrandChildCategory")
public String popularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, ModelMap map){
	
	try {
		Product product = DAORegistry.getProductDAO().getProductByName(Constants.Last_Ten_Row_Tickets_Product);
		Collection<PopularGrandChildCategory> popGrandChilds = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(product.getId());
		Collection<GrandChildCategory> grandChilds = DAORegistry.getGrandChildCategoryDAO().getAll();
	
		map.put("popGrandChilds", popGrandChilds);
		map.put("grandChildCategoryList", grandChilds);
		map.put("product", product);
		
	} catch(Exception e){
		e.printStackTrace();
	}
	map.put("productName", "LastRow Tickets");
	map.put("url", "LastRowTickets");
	return "page-popular-grand-child-category";
}

@RequestMapping(value="/PopularVenue")
public String popularVenue(HttpServletRequest request, HttpServletResponse response, ModelMap map){
	
	try {
		Product product = DAORegistry.getProductDAO().getProductByName(Constants.Last_Ten_Row_Tickets_Product);
		Collection<PopularVenue> popVenues = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(product.getId());
		Collection<EventDetails> venueList = DAORegistry.getQueryManagerDAO().getVenueDetails();
		//Collection<Venue> venueList = DAORegistry.getVenueDAO().getAll();
	
		map.put("popVenues", popVenues);
		map.put("venueList", venueList);
		map.put("product", product);
		
	} catch(Exception e){
		e.printStackTrace();
	}
	map.put("productName", "LastRow Tickets");
	map.put("url", "LastRowTickets");
	return "page-popular-venue";
}
	
	@RequestMapping(value="/Cards")
	public String cards(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		try{
			
		}catch(Exception e){
			e.printStackTrace();
		}
		map.put("productName", "LastRow Tickets");
		map.put("url", "LastRowTickets");
		return "page-cards";
	}
}
