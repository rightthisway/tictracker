package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.pojos.POCreateDTO;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Constants;

@Controller
@RequestMapping(value="/PurchaseOrder")
public class CreatePOController {

	private static Logger LOGGER = LoggerFactory.getLogger(CreatePOController.class);
	
	/**
	 * Create purchase order screen
	 * @param map
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */

	@RequestMapping({"/CreatePurchaseOrder"})
	public String createPurchaseOrder(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CREATE_PO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POCreateDTO poCreateDTO = gson.fromJson(((JsonObject)jsonObject.get("POCreateDTO")), POCreateDTO.class);
			
			if(poCreateDTO.getStatus() == 1){
				model.addAttribute("msg", poCreateDTO.getMessage());
				model.addAttribute("status", poCreateDTO.getStatus());
				model.addAttribute("shippingMethods", poCreateDTO.getShippingMethodDTO());
			}else{
				model.addAttribute("msg", poCreateDTO.getError().getDescription());
				model.addAttribute("status", poCreateDTO.getStatus());
			}
			/*Collection<ShippingMethod> shippingMethods = null;
			JSONObject jObj = new JSONObject();
			shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			if(shippingMethods != null){
				JSONArray shippingMethodArray = new JSONArray();
				JSONObject shippingMethodObject = null;
				for(ShippingMethod shippingMethod : shippingMethods){
					shippingMethodObject = new JSONObject();
					shippingMethodObject.put("shippingMethodId", shippingMethod.getId());
					shippingMethodObject.put("shippingMethodName", shippingMethod.getName());
					shippingMethodArray.put(shippingMethodObject);
				}
				jObj.put("shippingMethods", shippingMethodArray);
			}
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	/*@RequestMapping(value = "/CreatePO")
	public String createPOForCustomer(ModelMap map, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		LOGGER.info("Create PO processing....");
		String returnMessage = "";
		String customerId = "";
//		Collection<EventDetails> events = null;
		try{
			customerId = request.getParameter("custId");
			Customer customer = null;
			CustomerAddress customerAddress = null;
			Collection<ShippingMethod> shippingMethods = null;
			if(returnMessage.isEmpty()){
				customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId.trim()));
				customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(Integer.parseInt(customerId.trim()));
				shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			}
			map.put("customer", customer);
			map.put("shippingMethods", shippingMethods);
			map.put("customerAddress", customerAddress);
			
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong...Please try again");
		}
		return "page-create-po";
	}*/
	
	/**
	 * Save purchase order for customer
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value= "/SavePO")
	public String savePurchaseOrder(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Save purchase order request processing....");
		
		try{
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_PO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POCreateDTO poCreateDTO = gson.fromJson(((JsonObject)jsonObject.get("POCreateDTO")), POCreateDTO.class);
			
			if(poCreateDTO.getStatus() == 1){
				model.addAttribute("msg", poCreateDTO.getMessage());
				model.addAttribute("status", poCreateDTO.getStatus());
				model.addAttribute("purchaseOrderId", poCreateDTO.getPoId());
			}else{
				model.addAttribute("msg", poCreateDTO.getError().getDescription());
				model.addAttribute("status", poCreateDTO.getStatus());
			}
			/*String action = "";
			String customerId = "";
			String shippingMethodName = null;
			PurchaseOrder purchaseOrder = null;
			List<TicketGroup> ticketGroups = null;
			PurchaseOrderPaymentDetails	paymentDetails = null;
			CustomerAddress customerAddress = null;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);			
			action = request.getParameter("action");
			customerId = request.getParameter("customerId");
			System.out.println("CustomerID :: " +customerId);
			
			if(customerId == null || customerId.isEmpty()){
				jObj.put("msg", "Customer Id Not Found.");
			}
			if(action != null && action.equalsIgnoreCase("savePO")){
				Map<String, String[]> requestParams = request.getParameterMap();
				String paymentType = request.getParameter("paymentType");
				
				if(requestParams != null){
					DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
					purchaseOrder = new PurchaseOrder();
					
					String shippingMethod = request.getParameter("shippingMethod");
					Integer shippingMethodId = -1;
					if(shippingMethod!=null && !shippingMethod.isEmpty()){
						shippingMethodId = Integer.parseInt(shippingMethod);
					}
					customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(Integer.parseInt(customerId));
					purchaseOrder.setCustomerId(Integer.parseInt(customerId));
					purchaseOrder.setStatus(POStatus.ACTIVE);
					String totalPrice = request.getParameter("totalPrice");
					String poType = request.getParameter("poType");
					purchaseOrder.setPoTotal(Double.parseDouble(totalPrice));
					purchaseOrder.setTicketCount(Integer.parseInt(request.getParameter("totalQty")));
					purchaseOrder.setExternalPONo(request.getParameter("externalPONo"));
					
					purchaseOrder.setTrackingNo(request.getParameter("trackingNo"));
					purchaseOrder.setTransactionOffice(request.getParameter("transactionOffice"));
					if(shippingMethodId>=0){
						purchaseOrder.setShippingMethodId(shippingMethodId);
						ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(shippingMethodId);
						if(shipMethod!=null){
							shippingMethodName = shipMethod.getName();
						}
					}
					purchaseOrder.setPurchaseOrderType("REGULAR");
					if(poType!=null && !poType.isEmpty()){
						purchaseOrder.setPurchaseOrderType(poType);
						if(poType.equalsIgnoreCase("CONSIGNMENT")){
							purchaseOrder.setConsignmentPoNo(request.getParameter("consignmentPo"));
						}else{
							purchaseOrder.setConsignmentPoNo(null);
						}
					}
					purchaseOrder.setCreatedTime(new Date());
					purchaseOrder.setCreatedBy(userName);
					purchaseOrder.setCsr(userName);
					purchaseOrder.setLastUpdated(new Date());
					purchaseOrder.setBrokerId(brokerId); //Setting Broker ID
					
					//persist purchase order info for customer into db
					LOGGER.info("Persisting purchase order details for customer " +customerId);
					DAORegistry.getPurchaseOrderDAO().save(purchaseOrder);
					
					paymentDetails = new PurchaseOrderPaymentDetails();
					paymentDetails.setName(request.getParameter("name"));
					String paymentDateStr = request.getParameter("paymentDate");
					if(paymentDateStr!=null && !paymentDateStr.isEmpty()){
						paymentDateStr += " 00:00:00";
						paymentDetails.setPaymentDate(formatDateTime.parse(paymentDateStr));
					}
					paymentDetails.setPaymentStatus(request.getParameter("paymentStatus"));
					paymentDetails.setPaymentNote(request.getParameter("paymentNote"));
					paymentDetails.setAmount(Double.parseDouble(totalPrice));
					paymentDetails.setPurchaseOrderId(purchaseOrder.getId());
					paymentDetails.setPaymentType(paymentType);
					
					System.out.println("Payment Type: "+paymentType);
					
					if(paymentType != null && paymentType.equalsIgnoreCase("card")){
						paymentDetails.setCardLastFourDigit(request.getParameter("cardNo"));
						paymentDetails.setCardType(request.getParameter("cardType"));
						paymentDetails.setAccountLastFourDigit(null);
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(null);
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("account")){
						paymentDetails.setName(request.getParameter("accountName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("cheque")){
						paymentDetails.setName(request.getParameter("chequeName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountChequeNo"));
						paymentDetails.setChequeNo(request.getParameter("checkNo"));
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingChequeNo"));
					}
					if(paymentType != null)
						DAORegistry.getPurchaseOrderPaymentDetailsDAO().saveOrUpdate(paymentDetails);
					
					ticketGroups = new ArrayList<TicketGroup>();
					EventDetails event = null;
					Integer rowNo = 0;
					for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
						TicketGroup ticketGroup= new TicketGroup();
						String key = entry.getKey();
						
						if(key.contains("rowId_")){
							rowNo = Integer.parseInt(key.replace("rowId_", ""));
						}
						if(key.contains("eventId_"+rowNo+"_")){	
							Integer id= Integer.parseInt(key.replace("eventId_"+rowNo+"_", ""));
							ticketGroup.setStatus(OrderStatus.ACTIVE.toString());
							ticketGroup.setSection(request.getParameter("section_"+rowNo+"_"+id));
							ticketGroup.setRow(request.getParameter("row_"+rowNo+"_"+id));
							ticketGroup.setEventId(id);
							ticketGroup.setQuantity(Integer.parseInt(request.getParameter("qty_"+rowNo+"_"+id)));
							ticketGroup.setSeatLow(request.getParameter("seatLow_"+rowNo+"_"+id));
							ticketGroup.setSeatHigh(request.getParameter("seatHigh_"+rowNo+"_"+id));
							ticketGroup.setPrice(Double.parseDouble(request.getParameter("price_"+rowNo+"_"+id)));
							if(shippingMethodId>=0){
								ticketGroup.setShippingMethodId(shippingMethodId);
								if(shippingMethodName != null && !shippingMethodName.isEmpty()){
									ticketGroup.setShippingMethod(shippingMethodName);
								}
							}
							ticketGroup.setCreatedDate(new Date());
							ticketGroup.setInvoiceId(0);
							ticketGroup.setCreatedBy(userName);
							ticketGroup.setNotes(null);
							ticketGroup.setBroadcast(true);
							
							event = DAORegistry.getEventDetailsDAO().get(id);
							ticketGroup.setEventName(event.getEventName());
							ticketGroup.setEventDate(event.getEventDate());
							ticketGroup.setEventTime(event.getEventTime());
							ticketGroup.setPurchaseOrderId(purchaseOrder.getId());
							ticketGroup.setBrokerId(brokerId); //Setting Broker ID
							if(ticketGroup.getQuantity()>0 && ticketGroup.getSeatLow()!=null && !ticketGroup.getSeatLow().isEmpty()){
								DAORegistry.getTicketGroupDAO().save(ticketGroup);
								ticketGroups.add(ticketGroup);
								Ticket ticket = null;
								int seatNo = Integer.parseInt(ticketGroup.getSeatLow());
								for(int i=0;i<ticketGroup.getQuantity();i++){
									ticket = new Ticket(ticketGroup);
									ticket.setSeatNo(String.valueOf(seatNo+i));
									ticket.setSeatOrder(i+1);
									ticket.setUserName(userName);
									ticket.setTicketStatus(TicketStatus.ACTIVE);
									DAORegistry.getTicketDAO().save(ticket);
								}
							}
								
						}
					}
					LOGGER.debug("Persisted purchase order details in database for customer " +customerId);
				}
				jObj.put("status", 1);				
			}
			//jObj.put("purchaseOrder", purchaseOrder);
			//jObj.put("paymentDetails", paymentDetails);
			//jObj.put("ticketGroups", ticketGroups);
			//jObj.put("customerInfo", customerAddress);
			//jObj.put("shippingMethod", shippingMethodName);
			jObj.put("purchaseOrderId", purchaseOrder.getId());
			jObj.put("msg", "Purchase order created successfully.");
			
			//Tracking User Action
			String userActionMsg = "Purchase Order Created Successfully.";
			Util.userActionAudit(request, purchaseOrder.getId(), userActionMsg);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

}
