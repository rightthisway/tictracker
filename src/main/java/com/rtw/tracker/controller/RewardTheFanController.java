package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.pojos.CardsDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.PopularArtistScreenDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

@Controller
@RequestMapping("/RewardTheFan")
public class RewardTheFanController {
	
	@Autowired
//	FileValidator fileValidator;
//
//	public FileValidator getFileValidator() {
//		return fileValidator;
//	}
//	public void setFileValidator(FileValidator fileValidator) {
//		this.fileValidator = fileValidator;
//	}
	
	/*@RequestMapping("/AutoCompleteGrandChildAndArtistAndVenue")
	public void getAutoCompleteGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");
//		String brokerId = request.getParameter("brokerId");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
//		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
//				param);
//		Collection<GrandChildCategory> grandChildCategories = DAORegistry
//				.getGrandChildCategoryDAO().getGrandChildCategoriesByName(
//						param);
//		Collection<ChildCategory> childCategories = DAORegistry
//				.getChildCategoryDAO().getChildTourCategoriesByName(param);

//		if (artists == null && venues == null && grandChildCategories == null) {
		if (artists == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
//		if (venues != null) {
//			for (Venue venue : venues) {
//				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
//			}
//		}
//		if (grandChildCategories != null) {
//			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
//				strResponse += ("GRANDCHILD" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
//			}
//		}
//
//		if (childCategories != null) {
//			for (ChildCategory childCategory : childCategories) {
//				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
//			}
//		}
		
		writer.write(strResponse);
	}*/
		
	/*
	@RequestMapping(value="/PopularEvents")
	public String populerEvents(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			Integer totalCount = 0;
			DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			
			Collection<EventDetails> events = null;
			GridHeaderFilters filter = new GridHeaderFilters();
			if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty()) && (searchValue == null || searchValue.isEmpty())){
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance();
				Date fromDate = new Date();
				cal.add(Calendar.MONTH, 3);
				Date toDate = cal.getTime();
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:59";
				//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(null, null, fromDate, toDate,ProductType.REWARDTHEFAN);
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null, null, fromDateFinal, toDateFinal, ProductType.REWARDTHEFAN,filter,null);
				totalCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null, null, fromDateFinal, toDateFinal, ProductType.REWARDTHEFAN,filter);
			}else{
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					String fromDateFinal = fromDateStr + " 00:00:00";
					String toDateFinal = toDateStr + " 23:59:59";
					//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(searchType, searchValue, dateTimeFormat.parse(fromDateFinal), dateTimeFormat.parse(toDateFinal),ProductType.REWARDTHEFAN);
					events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType, searchValue, fromDateFinal, toDateFinal,ProductType.REWARDTHEFAN,filter,null);
					totalCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(searchType, searchValue, fromDateFinal, toDateFinal,ProductType.REWARDTHEFAN,filter);
				}else{
					//events = DAORegistry.getEventDetailsDAO().getAllActiveEventsByFilter(searchType, searchValue, null, null,ProductType.REWARDTHEFAN);
					events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(searchType, searchValue, null, null,ProductType.REWARDTHEFAN,filter,null);
					totalCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(searchType, searchValue, null, null,ProductType.REWARDTHEFAN,filter);
				}
			}
			
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);			
			Collection<PopularEvents> popEvents = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(product.getId());
			
			map.put("popEvents", popEvents);
			map.put("eventsList", events);
			map.put("totalCount", totalCount);
			map.put("searchType", searchType);
			map.put("searchValue", searchValue);
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-events";
	}
	*/
		
	@RequestMapping(value="/PopularArtist")
	public String popularArtistPage(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_POPULAR_ARTIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PopularArtistScreenDTO popularArtistScreenDTO = gson.fromJson(((JsonObject)jsonObject.get("popularArtistScreenDTO")), PopularArtistScreenDTO.class);
						
			if(popularArtistScreenDTO.getStatus() == 1){
				model.addAttribute("msg", popularArtistScreenDTO.getMessage());
				model.addAttribute("status", popularArtistScreenDTO.getStatus());
				model.addAttribute("popArtists", gson.toJson(popularArtistScreenDTO.getPopularArtistList()));
				model.addAttribute("popArtistPagingInfo", gson.toJson(popularArtistScreenDTO.getPopularArtistPaginationDTO()));
				model.addAttribute("artistList", gson.toJson(popularArtistScreenDTO.getArtistList()));
				model.addAttribute("pagingInfo", gson.toJson(popularArtistScreenDTO.getPaginationDTO()));
				model.addAttribute("product", popularArtistScreenDTO.getProduct());
				model.addAttribute("productName", popularArtistScreenDTO.getProductName());				
			}else{
				model.addAttribute("msg", popularArtistScreenDTO.getError().getDescription());
				model.addAttribute("status", popularArtistScreenDTO.getStatus());
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}
		return "page-popular-artist";
	}
	
	/*@RequestMapping(value="/GetPopularArtist")
	public String popularArtist(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(product.getId());
			Collection<EventDetails> artistList = DAORegistry.getQueryManagerDAO().getArtistDetails();
			//Collection<Artist> artistList = DAORegistry.getArtistDAO().getAll();
			
			Collection<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}			
			map.put("popArtists", popularArtists);
			map.put("artistList", artistList);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-artist";
	}*/
	
	@RequestMapping(value="/ManageArtistForSearch")
	public String getArtistForManageSearch(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_ARTIST_FOR_SEARCH);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return "page-manage-artist-search";
	}
	
	/*@RequestMapping(value="/PopularGrandChildCategory")
	public String popularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularGrandChildCategory> popGrandChilds = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(product.getId());
			List<EventDetails> grandChilds = DAORegistry.getQueryManagerDAO().getGrandChildDetails();
		
			map.put("popGrandChilds", popGrandChilds);
			map.put("grandChildCategoryList", grandChilds);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-grand-child-category";
	}*/
	
	/*@RequestMapping(value="/PopularVenue")
	public String popularVenue(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		
		try {
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularVenue> popVenues = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(product.getId());
			//Collection<EventDetails> venueList = DAORegistry.getQueryManagerDAO().getVenueDetails();
			Collection<Venue> venueList = DAORegistry.getQueryManagerDAO().getAllVenueDetails(null,null);
		
			map.put("popVenues", popVenues);
			map.put("venueList", venueList);
			map.put("product", product);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "RewardTheFan");
		map.put("productName", "Reward The Fan");
		return "page-popular-venue";
	}*/
	
	@RequestMapping(value="/Cards")
	public String cards(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			 
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CARDS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CardsDTO cardsDTO = gson.fromJson(((JsonObject)jsonObject.get("cardsDTO")), CardsDTO.class);
						
			if(cardsDTO.getStatus() == 1){
				model.addAttribute("msg", cardsDTO.getMessage());
				model.addAttribute("status", cardsDTO.getStatus());
				model.addAttribute("parentCategoryList", cardsDTO.getParentCategoryList());
				model.addAttribute("childCategoryList", cardsDTO.getChildCategoryList());
				model.addAttribute("grandChildCategoryLsit", cardsDTO.getGrandChildCategoryLsit());
				model.addAttribute("cardsList", cardsDTO.getCardsList());
			}else{
				model.addAttribute("msg", cardsDTO.getError().getDescription());
				model.addAttribute("status", cardsDTO.getStatus());
			}
			
			model.addAttribute("product", cardsDTO.getProduct());
			model.addAttribute("info", cardsDTO.getInfo());
			model.addAttribute("productName", cardsDTO.getProductName());
	        
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-cards";
	}
	
	@RequestMapping(value="/CardsUpdatePopup")
	public String cardsUpdatePopup(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			Map<String, String> map = Util.getParameterMap(request);
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CARDS_UPDATE_POPUP);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CardsDTO cardsDTO = gson.fromJson(((JsonObject)jsonObject.get("cardsDTO")), CardsDTO.class);
						
			if(cardsDTO.getStatus() == 1){
				model.addAttribute("msg", cardsDTO.getMessage());
				model.addAttribute("status", cardsDTO.getStatus());
				model.addAttribute("parentCategoryList", cardsDTO.getParentCategoryList());
				model.addAttribute("childCategoryList", cardsDTO.getChildCategoryList());
				model.addAttribute("grandChildCategoryLsit", cardsDTO.getGrandChildCategoryLsit());
				model.addAttribute("card", cardsDTO.getCard());
			}else{
				model.addAttribute("msg", cardsDTO.getError().getDescription());
				model.addAttribute("status", cardsDTO.getStatus());
			}
			model.addAttribute("product", cardsDTO.getProduct());
	        
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-cards-update-popup";
	}
	
	@RequestMapping(value = "/updateCards")
	public String updateCards(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		try{
			String action = request.getParameter("action");
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("userName", username);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CARDS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("info", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("info", genericResponseDTO.getError().getDescription());
			}
						
		/*if(action != null) {
			if(action.equals("update")) {
				MultipartFile file = cards.getFile();
				MultipartFile file1 = cards.getMobileFile();
				String fileRequired = request.getParameter("fileRequired");
				String mobileFileRequired = request.getParameter("mobileFileRequired");
				String categoryTypes = request.getParameter("categoryType");
				String showEvents = request.getParameter("eventInfo");
				String noOfEvents = request.getParameter("noOfEvents");
				String userActionMsg = "";
				
				if(fileRequired != null && fileRequired.equals("Y")) {
					fileValidator.validateCards(cards, result);
					if (result.hasErrors()) {
						map.put("info", "Please select valid file.");
						return "page-cards-update-popup";
					}	
				}
				if(mobileFileRequired != null && mobileFileRequired.equals("Y")) {
					fileValidator.validateMobileImageCards(cards, result);
					if (result.hasErrors()) {
						map.put("info", "Please select valid Mobile File.");
						return "page-cards-update-popup";
					}	
				}
				
				if(cards.getCardType().equals(CardType.PROMOTION)){
					if(showEvents == null || showEvents.isEmpty()){
						map.put("info", "Please select Show Event Data.");
						return "page-cards-update-popup";
					}
					if(showEvents != null && showEvents.equalsIgnoreCase("Yes")){
						if(noOfEvents == null || noOfEvents.isEmpty()){
							map.put("info", "Please Enter No.Of Events.");
							return "page-cards-update-popup";
						}
						try{
							Integer noOfEvent = Integer.parseInt(noOfEvents);
						}catch(Exception e){
							e.printStackTrace();
							map.put("info", "Please Enter No.Of Events in Integer value.");
							return "page-cards-update-popup";
						}
					}
				}
				
				Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
//				Cards cardDb = DAORegistry.getCardsDAO().getCardsByCardPosition(product.getId(),cards.getDesktopPosition(),cards.getMobilePosition());
//				if(cardDb != null) {
//					DAORegistry.getCardsDAO().delete(cardDb);
//				}
				if(categoryTypes != null && !categoryTypes.isEmpty()){
					String categoryType[] = categoryTypes.split("_");
					if(categoryType[0].equalsIgnoreCase("pc")){
						cards.setParentCategoryId(Integer.parseInt(categoryType[1]));
					}
					if(categoryType[0].equalsIgnoreCase("cc")){
						cards.setChildCategoryId(Integer.parseInt(categoryType[1]));
					}
					if(categoryType[0].equalsIgnoreCase("gc")){
						cards.setGrandChildCategoryId(Integer.parseInt(categoryType[1]));
					}
				}
				if(cards.getId()!= null) {
					userActionMsg = "Card Updated Successfully.";
					map.put("info", "Card updated successfully.");
				} else {
					userActionMsg = "Card Added Successfully.";
					map.put("info", "Card added successfully.");
				}
				
				cards.setLastUpdated(new Date());
				cards.setLastUpdatedBy(username);
				cards.setProductId(product.getId());
				
				if(cards.getCardType().equals(CardType.PROMOTION) || cards.getCardType().equals(CardType.YESNO) || cards.getCardType().equals(CardType.MAINPROMOTION)) {
					
					File newFile1 = null;
					if(mobileFileRequired != null && mobileFileRequired.equals("Y")) {
						if(cards.getCardType().equals(CardType.MAINPROMOTION)){
							if(cards.getMobileImageFileUrl() != null && !(cards.getMobileImageFileUrl()).isEmpty()) {
								String fullFileName = Constants.RTW_cards_Path+cards.getMobileImageFileUrl();
								newFile1 = new File(fullFileName);
								if (newFile1.exists()) {
									newFile1.delete();
								}
							}							
						}
						DAORegistry.getCardsDAO().saveOrUpdate(cards);
						
						if(cards.getCardType().equals(CardType.MAINPROMOTION)){
							String ext1 = FilenameUtils.getExtension(file1.getOriginalFilename());
							String fileName1 = "rtwCard_MobileImage_" +cards.getId()+"."+ext1;
							String fullFileName1 = Constants.RTW_cards_Path+fileName1;
							newFile1 = new File(fullFileName1);
							if (!newFile1.exists()) {
								newFile1.createNewFile();
							}
							System.out.println("file path...."+newFile1.getPath());
							BufferedOutputStream stream1 = new BufferedOutputStream(
									new FileOutputStream(newFile1));
					        FileCopyUtils.copy(file1.getInputStream(), stream1);
							stream1.close();	
							
							cards.setMobileImageFileUrl(fileName1);							
						}
						DAORegistry.getCardsDAO().updateCardImageUrl(cards);
					}
					if(fileRequired != null && fileRequired.equals("Y")){
						File newFile = null;
						if(cards.getImageFileUrl() != null && !(cards.getImageFileUrl()).isEmpty()) {
							String fullFileName = Constants.RTW_cards_Path+cards.getImageFileUrl();
							newFile = new File(fullFileName);
							if (newFile.exists()) {
								newFile.delete();
							}
						}
												
						if(cards.getCardType().equals(CardType.PROMOTION)){
							if(showEvents != null && showEvents.equalsIgnoreCase("Yes")){
								cards.setShowEventData(true);
								cards.setNoOfEvents(Integer.parseInt(noOfEvents));
							}else if(showEvents != null && showEvents.equalsIgnoreCase("No")){
								cards.setShowEventData(false);
							}							
						}
						
						DAORegistry.getCardsDAO().saveOrUpdate(cards);
						
						String ext = FilenameUtils.getExtension(file.getOriginalFilename());
						String fileName = "rtwCard_" +cards.getId()+"."+ext;
						String fullFileName = Constants.RTW_cards_Path+fileName;
						newFile = new File(fullFileName);
						if (!newFile.exists()) {
							newFile.createNewFile();
						}
						System.out.println("file path...."+newFile.getPath());
						BufferedOutputStream stream = new BufferedOutputStream(
								new FileOutputStream(newFile));
				        FileCopyUtils.copy(file.getInputStream(), stream);
						stream.close();	
						
						cards.setImageFileUrl(fileName);
												
						DAORegistry.getCardsDAO().updateCardImageUrl(cards);
					} 
					if((fileRequired == null || !fileRequired.equals("Y")) && (mobileFileRequired == null || !mobileFileRequired.equals("Y"))) {
						if(cards.getImageFileUrl() != null) {
							String fullFileName = Constants.RTW_cards_Path+cards.getImageFileUrl();
							File newFile = new File(fullFileName);
							if (!newFile.exists()) {
								map.put("info", "Please select valid file.");
								return "page-cards-update-popup";
							}
						}
						
						if(cards.getCardType().equals(CardType.MAINPROMOTION)){
							if(cards.getMobileImageFileUrl() != null) {
								String fullFileName1 = Constants.RTW_cards_Path+cards.getMobileImageFileUrl();
								File newFil1 = new File(fullFileName1);
								if (!newFil1.exists()) {
									map.put("info", "Please select valid Mobile File.");
									return "page-cards-update-popup";
								}
							}
						}
						
						if(cards.getCardType().equals(CardType.PROMOTION)){
							if(showEvents != null && showEvents.equalsIgnoreCase("Yes")){
								cards.setShowEventData(true);
								cards.setNoOfEvents(Integer.parseInt(noOfEvents));
							}else if(showEvents != null && showEvents.equalsIgnoreCase("No")){
								cards.setShowEventData(false);
							}							
						}
						DAORegistry.getCardsDAO().saveOrUpdate(cards);
					}
				} else {
					if(cards.getImageFileUrl() != null) {
						String fullFileName = Constants.RTW_cards_Path+cards.getImageFileUrl();
						File newFile = new File(fullFileName);
						if (newFile.exists()) {
							newFile.delete();
						}
						cards.setImageFileUrl(null);
					}
					
					if(cards.getMobileImageFileUrl() != null) {
						String fullFileName1 = Constants.RTW_cards_Path+cards.getMobileImageFileUrl();
						File newFile1 = new File(fullFileName1);
						if (newFile1.exists()) {
							newFile1.delete();
						}
						cards.setMobileImageFileUrl(null);
					}
					DAORegistry.getCardsDAO().saveOrUpdate(cards);
				}
				
				map.put("card", cards);
				
				//Tracking User Action				
				Util.userActionAudit(request, cards.getId(), userActionMsg);
				
			} else if(action.equals("delete")) {
				Cards cardDb = DAORegistry.getCardsDAO().get(cards.getId());
				if(cardDb != null) {
					if(cards.getImageFileUrl() != null) {
						String fullFileName = Constants.RTW_cards_Path+cards.getImageFileUrl();
						File newFile = new File(fullFileName);
						if (newFile.exists()) {
							newFile.delete();
						}
						//cards.setImageFileUrl(null);
					}
					DAORegistry.getCardsDAO().delete(cardDb);
				}
				map.put("info", "Card deleted successfully.");
				
				//Tracking User Action
				String userActionMsg = "Card Deleted Successfully.";
				Util.userActionAudit(request, cards.getId(), userActionMsg);
			}
		}*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-cards-update-popup";
	}
	
    /*@RequestMapping(value = "/GetImageFile")
    public void getFileUrl(HttpServletRequest request,HttpServletResponse response)
	throws IOException, ServletException {

		try { 
			
			String fileName = request.getParameter("filePath");
			String type = request.getParameter("type");
			String fullFileName=null;
			
			if(type.equals("rtwCards")) {
				fullFileName = Constants.RTW_cards_Path+fileName;
			} else if(type.equals("artistImage")) {
				fullFileName = Constants.Artist_Image_Path+fileName;
			} else if(type.equals("grandChildCategoryImage")) {
				fullFileName = Constants.Grand_Child_Category_Image_Path+fileName;
			} else if(type.equals("fantasyGrandChildCategoryImage")) {
				fullFileName = Constants.Fantasy_Grand_Child_Category_Image_Path+fileName;
			} else if(type.equals("fantasyGrandChildCategoryMobileImage")) {
				fullFileName = Constants.Fantasy_Grand_Child_Category_Image_Path+fileName;
			} else if(type.equals("childCategoryImage")) {
				fullFileName = Constants.Child_Category__Image_Path+fileName;
			} else if(type.equals("parentCategoryImage")) {
				fullFileName = Constants.Parent_category_Image_Path+fileName;
			} else if(type.equals("loyalFanParentCategoryImage")) {
				fullFileName = Constants.LoyalFan_Parent_Category_Image_Path+fileName;
			}else {
				fullFileName=fileName;
			}
			
			String ext = FilenameUtils.getExtension(fileName);
			if(ext.equals("jpeg")|| ext.equals("jpg") || ext.equals("jpe")) {
				response.setContentType("image/jpeg");	
			}else if(ext.equals("gif")) {
				response.setContentType("image/gif");
			} else if(ext.equals("png")) {
				response.setContentType("image/png");
			} else {
				response.setContentType("image/"+ext);
			}
			
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
		
			File _RealFile = new File(fullFileName);
			
			if(_RealFile.exists()) {
				//_RealFile = new File("C:/cards/REWARDTHEFAN/" + cardPosition+".jpg");
				BufferedInputStream _Stream = new BufferedInputStream(new FileInputStream(_RealFile)) ;
				
				//byte[] imageBytes = IOUtils.toByteArray(_Stream);

				//response.setContentType("image/jpeg");
				//response.setContentLength(imageBytes.length);
				
				int value = 0;
				while( (value = _Stream.read()) != -1) {
					response.getOutputStream().write(value);
				}
				_Stream.close();
			}
			
			response.getOutputStream().flush();
			response.getOutputStream().close();

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}

	}*/
    
}