package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.utils.Constants;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.pojos.CreateFantasyOrderDTO;
import com.rtw.tracker.pojos.CrownJewelLeaguesDTO;
import com.rtw.tracker.pojos.CrownJewelOrderSummaryDTO;
import com.rtw.tracker.pojos.CrownJewelOrderUpdateReasonDTO;
import com.rtw.tracker.pojos.CrownJewelOrdersDTO;
import com.rtw.tracker.pojos.CrownJewelTeamZonesDTO;
import com.rtw.tracker.pojos.CrownJewelTeamsDTO;
import com.rtw.tracker.pojos.FantasyOrderCreationDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.JsonWrapperUtil;

@Controller
@RequestMapping("/CrownJewelEvents")
public class CrownJewelEventsController {
	
//	MailManager mailManager;
	SharedProperty sharedProperty;
//	public MailManager getMailManager() {
//		return mailManager;
//	}
//
//	public void setMailManager(MailManager mailManager) {
//		this.mailManager = mailManager;
//	}
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	@RequestMapping(value="/CrownJewelEvents")
	public String loadCrownJewelLeagues(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelLeaguesDTO crownJewelLeaguesDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelLeaguesDTO")), CrownJewelLeaguesDTO.class);
						
			if(crownJewelLeaguesDTO.getStatus() == 1){
				model.addAttribute("info", crownJewelLeaguesDTO.getMessage());
				model.addAttribute("status", crownJewelLeaguesDTO.getStatus());
				model.addAttribute("childId", crownJewelLeaguesDTO.getChildId());
				model.addAttribute("grandChildId", crownJewelLeaguesDTO.getGrandChildId());
				model.addAttribute("childCategories", crownJewelLeaguesDTO.getChildCategories());
				model.addAttribute("grandChildCategories", crownJewelLeaguesDTO.getGrandChildCategories());
				model.addAttribute("leaguesList", crownJewelLeaguesDTO.getLeaguesList());
			}else{
				model.addAttribute("info", crownJewelLeaguesDTO.getError().getDescription());
				model.addAttribute("status", crownJewelLeaguesDTO.getStatus());
			}
			
		} catch(Exception e){
			e.printStackTrace();
			model.addAttribute("info", "There is something wrong. Please try again.");
		}
		//map.put("productName", "Crown Jewel Events");
		return "page-crown-jewel-events";
	}
	
	@RequestMapping(value="/CrownJewelTeams")
	public String crownJewelTeams(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_TEAMS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
						
			if(crownJewelTeamsDTO.getStatus() == 1){
				model.addAttribute("info", crownJewelTeamsDTO.getMessage());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
				model.addAttribute("childId", crownJewelTeamsDTO.getChildId());
				model.addAttribute("grandChildId", crownJewelTeamsDTO.getGrandChildId());
				model.addAttribute("childCategories", crownJewelTeamsDTO.getChildCategories());
				model.addAttribute("grandChildCategories", crownJewelTeamsDTO.getGrandChildCategories());
				model.addAttribute("parentCategoryList", crownJewelTeamsDTO.getParentCategoryList());
				model.addAttribute("leaguesList", crownJewelTeamsDTO.getLeaguesList());
				model.addAttribute("teamsList", crownJewelTeamsDTO.getTeamsList());
				model.addAttribute("leagueId", crownJewelTeamsDTO.getLeagueId());
				model.addAttribute("copyFromLeagueId", crownJewelTeamsDTO.getCopyFromLeagueId());
			}else{
				model.addAttribute("info", crownJewelTeamsDTO.getError().getDescription());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		return "page-crown-jewel-teams";
	}
	
	
	
	
	@RequestMapping(value="/CrownJewelCategoryTeams")
	public String crownJewelCategoryTeams(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_CATEGORY_TEAMS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
			
			if(crownJewelTeamsDTO.getStatus() == 1){
				model.addAttribute("info", crownJewelTeamsDTO.getMessage());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
				model.addAttribute("childId", crownJewelTeamsDTO.getChildId());
				model.addAttribute("grandChildId", crownJewelTeamsDTO.getGrandChildId());
				model.addAttribute("childCategories", crownJewelTeamsDTO.getChildCategories());
				model.addAttribute("grandChildCategories", crownJewelTeamsDTO.getGrandChildCategories());
				model.addAttribute("leaguesList", crownJewelTeamsDTO.getLeaguesList());
				model.addAttribute("teamsList", crownJewelTeamsDTO.getCrownJewelCategoryTeams());
			}else{
				model.addAttribute("info", crownJewelTeamsDTO.getError().getDescription());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
			}		
		
		} catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-crown-jewel-category-teams";
	}
	
	/*
	@RequestMapping(value="/TeamsExportToExcel")
	public void getCrownJewelTeamsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String leagueIdStr = request.getParameter("leagueId");
			Integer leagueId = null;
			if(leagueIdStr != null && leagueIdStr.trim().length() > 0) {
				leagueId = Integer.parseInt(leagueIdStr);
			}
			List<CrownJewelLeagues> cjLeaguesList = null;
			
			List<CrownJewelTeams> cjTeamsList = null;
			if(leagueId != null){
				cjTeamsList = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(leagueId);
			}
		
			if(cjTeamsList!=null && !cjTeamsList.isEmpty()){
				HSSFWorkbook book = new HSSFWorkbook();
				HSSFSheet sheet = book.createSheet("fantasy_ticket_teams");
				HSSFRow header = sheet.createRow(0);
				header.createCell(0).setCellValue("Fantasy Ticket Team Id");
				header.createCell(1).setCellValue("League Id");
				header.createCell(2).setCellValue("Team Name");
				header.createCell(3).setCellValue("Odds");
				header.createCell(4).setCellValue("Cut off Date");
				int i=1; String packageApplicable = "";
				for(CrownJewelTeams team : cjTeamsList){
					HSSFRow row = sheet.createRow(i);					
					row.createCell(0).setCellValue(team.getId()!=null?team.getId():0);
					row.createCell(1).setCellValue(team.getLeagueId()!=null?team.getLeagueId():0);
					row.createCell(2).setCellValue(team.getName()!=null?team.getName():"");
					row.createCell(3).setCellValue(team.getOdds()!=null?team.getOdds():0);
					row.createCell(4).setCellValue(team.getCutOffDateStr()!=null?team.getCutOffDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=fantasy_ticket_teams.xls");
				book.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	@RequestMapping(value="/CrownJewelTeamZones")
	public String getCrownJewelTeamZones(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_TEAM_ZONES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamZonesDTO crownJewelTeamZonesDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamZonesDTO")), CrownJewelTeamZonesDTO.class);
						
			if(crownJewelTeamZonesDTO.getStatus() == 1){
				model.addAttribute("cjTeamZones", crownJewelTeamZonesDTO.getCjTeamZone());
				model.addAttribute("savedZoneStr", crownJewelTeamZonesDTO.getSavedZoneStr());
				model.addAttribute("teamsList", crownJewelTeamZonesDTO.getTeamsList());
				model.addAttribute("childId", crownJewelTeamZonesDTO.getChildId());
				model.addAttribute("quantityTypes",crownJewelTeamZonesDTO.getQuantityTypes());
				model.addAttribute("grandChildId", crownJewelTeamZonesDTO.getGrandChildId());
				model.addAttribute("childCategories", crownJewelTeamZonesDTO.getChildCategories());
				model.addAttribute("grandChildCategories", crownJewelTeamZonesDTO.getGrandChildCategories());
				model.addAttribute("leaguesList", crownJewelTeamZonesDTO.getLeaguesList());
				model.addAttribute("leagueId", crownJewelTeamZonesDTO.getLeagueId());
				model.addAttribute("teamId", crownJewelTeamZonesDTO.getTeamId());
				model.addAttribute("copyFromLeagueIdStr", crownJewelTeamZonesDTO.getCopyFromLeagueId());
			}else{
				model.addAttribute("info", crownJewelTeamZonesDTO.getError().getDescription());
				model.addAttribute("status", crownJewelTeamZonesDTO.getStatus());
			}

		} catch(Exception e){
			e.printStackTrace();
			model.addAttribute("info", "There is something wrong. Please try again..");
		}
		return "page-crown-jewel-team-zones";
	}
	
	@RequestMapping("/CrownJewelOrders")
	public String getCrownJewelOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelOrdersDTO crownJewelOrdersDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelOrdersDTO")), CrownJewelOrdersDTO.class);
			
			if(crownJewelOrdersDTO.getStatus() == 1){
				model.addAttribute("info", crownJewelOrdersDTO.getMessage());
				model.addAttribute("status", crownJewelOrdersDTO.getCjStatus());
			}else{
				model.addAttribute("info", crownJewelOrdersDTO.getError().getDescription());
				model.addAttribute("status", crownJewelOrdersDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("fromDate", crownJewelOrdersDTO.getFromDate());
				model.addAttribute("toDate", crownJewelOrdersDTO.getToDate());
				model.addAttribute("teamName", crownJewelOrdersDTO.getTeamName());
				model.addAttribute("leagueName", crownJewelOrdersDTO.getLeagueName());
				model.addAttribute("grandChildCategry", crownJewelOrdersDTO.getGrandChildCategory());
				model.addAttribute("orders", crownJewelOrdersDTO.getOrders());
				model.addAttribute("pagingInfo",  crownJewelOrdersDTO.getPaginationDTO());
			}else{
				model.addAttribute("fromDate", crownJewelOrdersDTO.getFromDate());
				model.addAttribute("toDate", crownJewelOrdersDTO.getToDate());
				model.addAttribute("teamName", crownJewelOrdersDTO.getTeamName());
				model.addAttribute("leagueName", crownJewelOrdersDTO.getLeagueName());
				model.addAttribute("parntCategory", crownJewelOrdersDTO.getParentCategory());
				model.addAttribute("grandChildCategry", crownJewelOrdersDTO.getGrandChildCategory());
				model.addAttribute("orders", gson.toJson(crownJewelOrdersDTO.getOrders()));
				model.addAttribute("pagingInfo",  gson.toJson(crownJewelOrdersDTO.getPaginationDTO()));
				model.addAttribute("parentCategories", crownJewelOrdersDTO.getParentCategories());
				model.addAttribute("grandChildCategories", crownJewelOrdersDTO.getGrandChildCategories());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("info", "There is something wrong. Please try again..");
		}		
		return "page-crown-jewel-orders";
	}	
	
	@RequestMapping("/FantasyOrderCreation")
	public void fantasyOrderCreation(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.CREATE_FANTASY_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyOrderCreationDTO fantasyOrderCreationDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyOrderCreationDTO")), FantasyOrderCreationDTO.class);
						
			if(fantasyOrderCreationDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyOrderCreationDTO.getMessage());
				model.addAttribute("status", fantasyOrderCreationDTO.getStatus());
				model.addAttribute("orderNo", fantasyOrderCreationDTO.getOrderNo());
			}else{
				model.addAttribute("msg", fantasyOrderCreationDTO.getError().getDescription());
				model.addAttribute("status", fantasyOrderCreationDTO.getStatus());
			}
			
			/*String customerIdStr = request.getParameter("customerId");
			String fantasyCategoryIdStr = request.getParameter("grandChildCategoryId");
			String fantasyEventIdStr = request.getParameter("fantasyEventId");
			String fantasyTeamIdStr = request.getParameter("fantasyTeamId");
			String fantasyTicketIdStr = request.getParameter("fantasyTicketId");
			String fantasyTeamZoneIdStr = request.getParameter("fantasyTeamZoneId");
			String isRealTicketStr = request.getParameter("isRealTicket");
			String zoneStr = request.getParameter("zone");
			String quantityStr = request.getParameter("quantity");
			String requiredPointsStr = request.getParameter("requiredPoints");
						
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId",customerIdStr);
			map.put("fCategoryId",fantasyCategoryIdStr);
			map.put("fEventId",fantasyEventIdStr);
			map.put("fTeamId",fantasyTeamIdStr);
			map.put("fTicketId",fantasyTicketIdStr);
			map.put("fTeamZoneId",fantasyTeamZoneIdStr);
			map.put("isRealTicket",isRealTicketStr);
			map.put("zone",zoneStr);
			map.put("quantity",quantityStr);
			map.put("requiredPoints",requiredPointsStr);
			
			JSONObject jsonObject =  new JSONObject();
			String data = Util.getObject(map, sharedProperty.getApiUrl()+Constants.CREATE_FST_ORDER);
			Gson gson = new Gson();		
			JsonObject jsonObject1 = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProduct = gson.fromJson(((JsonObject)jsonObject1.get("fantasySportsProduct")), FantasySportsProduct.class);
			if(fantasySportsProduct.getStatus() == 1){
				jsonObject.put("msg", fantasySportsProduct.getMessage());
				jsonObject.put("orderNo", fantasySportsProduct.getFantasyOrderNo());
			}else{
				jsonObject.put("msg", fantasySportsProduct.getError().getDescription());
			}
			
			IOUtils.write(jsonObject.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}
	}
	
	@RequestMapping("/CreateFantasyOrder")
	public String createFantasyOrder(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.CREATE_FANTASY_ORDER_POPUP);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CreateFantasyOrderDTO createFantasyOrderDTO = gson.fromJson(((JsonObject)jsonObject.get("createFantasyOrderDTO")), CreateFantasyOrderDTO.class);
						
			if(createFantasyOrderDTO.getStatus() == 1){
				model.addAttribute("msg", createFantasyOrderDTO.getMessage());
				model.addAttribute("status", createFantasyOrderDTO.getStatus());
				model.addAttribute("parentCategoryList", createFantasyOrderDTO.getFantasyParentCategoryDTOs());
			}else{
				model.addAttribute("msg", createFantasyOrderDTO.getError().getDescription());
				model.addAttribute("status", createFantasyOrderDTO.getStatus());
			}
			
			/*String action = request.getParameter("action");
			String msg="";
			SuperFanOrderSummary superFanOrderSummary = null;
			boolean isValid = true;
			boolean packageOption=false;
			if(action!=null && action.equalsIgnoreCase("createOrder")){
				String teamIdStr = request.getParameter("fantasyTeam");
				String leagueIdStr = request.getParameter("fantasyEvent");
				String qtyStr = request.getParameter("qty");
				String ticketIdStr = request.getParameter("ticketId");
				String isRealTicketStr = request.getParameter("isRealTicket");
				String packageOptionStr = request.getParameter("packageOption");
				String customerIdStr = request.getParameter("customerId");
				String parentType = request.getParameter("parentType");
				parentType="SPORTS";
				Integer eventId=null;
				
				if(parentType!=null && parentType.equalsIgnoreCase(ParentType.SPORTS.toString())){
					if(!Util.isEmptyOrNull(leagueIdStr)){
						CrownJewelLeagues league = DAORegistry.getCrownJewelLeaguesDAO().get(Integer.parseInt(leagueIdStr));
						eventId = league.getEventId();
					}else{
						isValid = false;
						msg="leagueId found null.";
					}
				}else{
					if(!Util.isEmptyOrNull(teamIdStr)){
						CrownJewelTeams team = DAORegistry.getCrownJewelTeamsDAO().get(Integer.parseInt(teamIdStr));
						eventId = team.getEventId()!=null?team.getEventId():0;
					}else{
						isValid = false;
						msg="teamId found Null.";
					}
					
				}
				
				if(Util.isEmptyOrNull(qtyStr)){
					isValid = false;
					msg="quantity found null.";
				}
				if(eventId==null){
					isValid = false;
					msg="Event is not found for League.";
				}
				if(Util.isEmptyOrNull(ticketIdStr)){
					isValid = false;
					msg="ticketId found null.";
				}
				if(Util.isEmptyOrNull(isRealTicketStr)){
					isValid = false;
					msg="isRealTicket found null.";
				}
				if(Util.isEmptyOrNull(packageOptionStr)){
					isValid = false;
					msg="packageOption found null.";
				}else{
					if(packageOptionStr.equalsIgnoreCase("Yes")){
						packageOption = true;
					}
				}
				if(Util.isEmptyOrNull(customerIdStr)){
					isValid = false;
					msg="customerId found null.";
				}
				if(!msg.isEmpty()){
					map.put("errorMessage",msg);
				}
				
				if(eventId!=null && eventId>0 && isValid){
					Map<String, String> paramterMap =  Util.getParameterMap(request);
					paramterMap.put("teamId",teamIdStr);
					paramterMap.put("leagueId",leagueIdStr);
					paramterMap.put("qty",qtyStr);
					paramterMap.put("ticketId",ticketIdStr);
					paramterMap.put("isRealTicket",isRealTicketStr);
					paramterMap.put("packageOption",String.valueOf(packageOption));
					paramterMap.put("customerId",customerIdStr);
					paramterMap.put("eventId",String.valueOf(eventId));
					String data = Util.getObject(paramterMap, sharedProperty.getApiUrl()+Constants.GET_CROWN_JEWEL_ORDER_SUMMARY);
					Gson gson = new Gson();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					superFanOrderSummary = gson.fromJson(((JsonObject)jsonObject.get("superFanOrderSummary")),SuperFanOrderSummary.class);
				}
				if(superFanOrderSummary!=null && superFanOrderSummary.getFutureOrderConfirmation()!=null){
					Map<String, String> futureOrderParamMap = Util.getParameterMap(request);
					futureOrderParamMap.put("crownJewelOrderNo",String.valueOf(superFanOrderSummary.getFutureOrderConfirmation().getId()));
					futureOrderParamMap.put("authorized","true");
					String data = Util.getObject(futureOrderParamMap, Constants.BASE_URL+Constants.GET_CROWN_JEWEL_FUTURE_ORDER_CONFIRMATION);
					Gson gson = new Gson();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CrownJewelOrderConfirmation orderConfirmation = gson.fromJson(((JsonObject)jsonObject.get("crownJewelOrderConfirmation")),CrownJewelOrderConfirmation.class);
					if(orderConfirmation.getStatus() == 1){
						msg = "Fantasy Order is created with Id ";
						return "redirect:ViewOrder?orderId="+orderConfirmation.getOrderNo()+"&msg="+msg;
					}else{
						map.put("errorMessage",orderConfirmation.getError().getDescription());
					}
				}else if(superFanOrderSummary.getError()!=null){
					map.put("errorMessage",superFanOrderSummary.getError().getDescription());
				}else{
					map.put("errorMessage","Failed to create fantasy order.");
				}
				
			}*/
//			map.put("parentCategoryList", DAORegistry.getFantasyParentCategoryDAO().getAll());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}		
		return "page-create-fantasy-order";
	}
	
	
	
	/*@RequestMapping("/GetCrownJewelOrders")
	public void getCrownJewelOrders(HttpServletRequest request, HttpServletResponse response){//, ModelMap map){
		JSONObject returnObject = new JSONObject();
		String fromDateStr = request.getParameter("fromDate");
		String teamName = request.getParameter("teamName");
		String leagueName = request.getParameter("leagueName");
		String toDateStr = request.getParameter("toDate");		
		String status = request.getParameter("status");
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		String parentCategory = request.getParameter("parentCategory");
		String grandChildCategory = request.getParameter("grandChildCategory");
		String fromDateFinal=null;
		String toDateFinal = null;
		List<CrownJewelCustomerOrder> CJEOrders = null;
		Integer count = 0;
		try {
			if(status.equalsIgnoreCase("All")){
				status = "";
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCrownJewelOrderSearchHeaderFilters(headerFilter);
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00"; 
				toDateFinal = toDateStr + " 23:59:59";
				CJEOrders = DAORegistry.getCrownJewelCustomerOrderDAO().getCronJewelOrdersByFilter(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,pageNo,filter);
				count = DAORegistry.getQueryManagerDAO().getCronJewelOrdersByFilterCount(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
			}
			else{
			CJEOrders = DAORegistry.getCrownJewelCustomerOrderDAO().getCronJewelOrdersByFilter(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,pageNo,filter);
			count = DAORegistry.getQueryManagerDAO().getCronJewelOrdersByFilterCount(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
			}
		
			returnObject.put("fromDate", fromDateStr);
			returnObject.put("status", status);		
			returnObject.put("toDate", toDateStr);
			returnObject.put("teamName", teamName);
			returnObject.put("leagueName", leagueName);
			returnObject.put("grandChildCategory", grandChildCategory);
			returnObject.put("orders", JsonWrapperUtil.getCrownJewelOrderArray(CJEOrders));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	

	/*@RequestMapping({"/CrownJewelOrdersExportToExcel"})
	public void getCrownJewelOrdersToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String teamName = request.getParameter("teamName");
		String leagueName = request.getParameter("leagueName");
		String parentCategory = request.getParameter("parentCategory");	
		String grandChildCategory = request.getParameter("grandChildCategory");
		String status = request.getParameter("status");
		String headerFilter = request.getParameter("headerFilter");
		String fromDateFinal=null;
		String toDateFinal = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCrownJewelOrderSearchHeaderFilters(headerFilter);
			List<CrownJewelCustomerOrder> CJEOrdersList = null;
			if(status.equalsIgnoreCase("All")){
				status = "";
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00"; 
				toDateFinal = toDateStr + " 23:59:59";
				CJEOrdersList = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByFilterToExport(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);
			}
			else{
				CJEOrdersList = DAORegistry.getCrownJewelCustomerOrderDAO().getCrownJewelOrdersByFilterToExport(null,fromDateFinal,toDateFinal,parentCategory,grandChildCategory,teamName,leagueName,status,filter);			
			}
						
			if(CJEOrdersList!=null && !CJEOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Order");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Fantasy Sports Tickets Order Id");
				header.createCell(1).setCellValue("Order Id");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Team Name");
				header.createCell(4).setCellValue("Venue");
				header.createCell(5).setCellValue("City");
				header.createCell(6).setCellValue("State");
				header.createCell(7).setCellValue("Country");
				header.createCell(8).setCellValue("Package Selected");
				header.createCell(9).setCellValue("Package Cost");
				//header.createCell(10).setCellValue("Package Notes");
				header.createCell(10).setCellValue("Event Id");
				header.createCell(11).setCellValue("Ticket Id");
				header.createCell(12).setCellValue("Zone");
				header.createCell(13).setCellValue("Ticket Qty");
				header.createCell(14).setCellValue("Ticket Price");
				header.createCell(15).setCellValue("Require Points");
				header.createCell(16).setCellValue("Customer Name");
				header.createCell(17).setCellValue("Grand Child Name");
				header.createCell(18).setCellValue("Platform");
				header.createCell(19).setCellValue("Status");
				header.createCell(20).setCellValue("Created Date");
				header.createCell(21).setCellValue("Last Updated Date");
				Integer i=1;
				for(CrownJewelCustomerOrder CJEOrder : CJEOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(CJEOrder.getId());
					row.createCell(1).setCellValue(CJEOrder.getRegularOrderId()!=null?CJEOrder.getRegularOrderId().toString():"");
					row.createCell(2).setCellValue(CJEOrder.getFantasyEventName()!=null?CJEOrder.getFantasyEventName():"");
					row.createCell(3).setCellValue(CJEOrder.getTeamName()!=null?CJEOrder.getTeamName():"");
					row.createCell(4).setCellValue(CJEOrder.getVenueName()!=null?CJEOrder.getVenueName():"");
					row.createCell(5).setCellValue(CJEOrder.getVenueCity()!=null?CJEOrder.getVenueCity():"");
					row.createCell(6).setCellValue(CJEOrder.getVenueState()!=null?CJEOrder.getVenueState():"");
					row.createCell(7).setCellValue(CJEOrder.getVenueCountry()!=null?CJEOrder.getVenueCountry():"");
					if(CJEOrder.getIsPackageSelected() != null && CJEOrder.getIsPackageSelected()){
						row.createCell(8).setCellValue("Yes");
					}else{
						row.createCell(8).setCellValue("No");
					}
					row.createCell(9).setCellValue(CJEOrder.getPackageCost()!=null?CJEOrder.getPackageCost():0.00);
					//row.createCell(10).setCellValue(CJEOrder.getPackageNote()!=null?CJEOrder.getPackageNote():"");
					row.createCell(10).setCellValue(CJEOrder.getEventId()!=null?CJEOrder.getEventId().toString():"");
					row.createCell(11).setCellValue(CJEOrder.getTicketId()!=null?CJEOrder.getTicketId().toString():"");
					row.createCell(12).setCellValue(CJEOrder.getTicketSelection()!=null?CJEOrder.getTicketSelection():"");
					row.createCell(13).setCellValue(CJEOrder.getTicketQty()!=null?CJEOrder.getTicketQty():0);
					row.createCell(14).setCellValue(CJEOrder.getTicketPrice()!=null?CJEOrder.getTicketPrice():0.00);
					//row.createCell(13).setCellValue(CJEOrder.getTicketPoints()!=null?CJEOrder.getTicketPoints().toString():"");
					row.createCell(15).setCellValue(CJEOrder.getRequiredPoints()!=null?CJEOrder.getRequiredPoints():0.00);
					//row.createCell(15).setCellValue(CJEOrder.getPackagePoints()!=null?CJEOrder.getPackagePoints().toString():"");
					row.createCell(16).setCellValue(CJEOrder.getCustomerName()!=null?CJEOrder.getCustomerName():"");
					//row.createCell(17).setCellValue(CJEOrder.getRemainingPoints()!=null?CJEOrder.getRemainingPoints().toString():"");
					row.createCell(17).setCellValue(CJEOrder.getGrandChildName()!=null?CJEOrder.getGrandChildName():"");
					row.createCell(18).setCellValue(CJEOrder.getPlatform()!=null?CJEOrder.getPlatform():"");
					row.createCell(19).setCellValue(CJEOrder.getStatus()!=null?CJEOrder.getStatus():"");
					row.createCell(20).setCellValue(CJEOrder.getCreatedDateStr()!=null?CJEOrder.getCreatedDateStr():"");
					row.createCell(21).setCellValue(CJEOrder.getLastUpdatedStr()!=null?CJEOrder.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Order.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping("/ViewOrder")
	public String getCrownJewelOrderById(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String orderIdStr = request.getParameter("orderId");
		String msg = request.getParameter("msg");
		CrownJewelCustomerOrder CJEOrder = null;
		try {
			if(orderIdStr==null || orderIdStr.isEmpty()){
				map.put("errorMessage", "There is something wrong...Please try again");
				return "page-view-crown-jewel-order-summary";
			}
			Integer orderId = Integer.parseInt(orderIdStr);			
			CJEOrder = DAORegistry.getCrownJewelCustomerOrderDAO().get(orderId);
			Integer regularOrderId = CJEOrder.getRegularOrderId();
			if(regularOrderId != null && regularOrderId > 0){
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(CJEOrder.getRegularOrderId());
				CategoryTicketGroup catTicketGroup = DAORegistry.getCategoryTicketGroupDAO().get(customerOrder.getCategoryTicketGroupId());
				//Invoice invoice = DAORegistry.getInvoiceDAO().get(CJEOrder.getRegularOrderId());
				///map.put("invoice", invoice);
				//map.put("order", customerOrder);
				map.put("ticket", catTicketGroup);
			}else{
				CrownJewelCategoryTicket crownJewelCategoryTicket = DAORegistry.getCrownJewelCategoryTicketDAO().get(CJEOrder.getTicketGroupId());
				map.put("ticket", crownJewelCategoryTicket);
			}
			Customer customer = DAORegistry.getCustomerDAO().get(CJEOrder.getCustomerId());
			Event event = DAORegistry.getEventDAO().get(CJEOrder.getEventId());
			CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(CJEOrder.getRegularOrderId());
			
			map.put("cjeOrder", CJEOrder);
			map.put("invoiceId", regularOrderId);
			map.put("customer", customer);			
			map.put("event", event);
			map.put("customerLoyalty",customerLoyaltyHistory);
			if(msg!=null && !msg.isEmpty()){
				map.put("successMessage", msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong...Please try again");
		}		
		return "page-view-crown-jewel-order-summary";
	}*/
	
	@RequestMapping("/ViewOrder")
	public String getCrownJewelOrderById(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String orderId = request.getParameter("orderId");
			String msg = request.getParameter("msg");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderId", orderId);
			map.put("msg", msg);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.VIEW_CROWN_JEWEL_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelOrderSummaryDTO crownJewelOrderSummaryDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelOrderSummaryDTO")), CrownJewelOrderSummaryDTO.class);
						
			if(crownJewelOrderSummaryDTO.getStatus() == 1){
				model.addAttribute("msg", crownJewelOrderSummaryDTO.getMessage());
				model.addAttribute("status", crownJewelOrderSummaryDTO.getStatus());
				model.addAttribute("ticket", crownJewelOrderSummaryDTO.getCategoryTicketGroupDTO());
				model.addAttribute("cjeOrder", crownJewelOrderSummaryDTO.getCrownJewelCustomerOrderDTO());
				model.addAttribute("customer", crownJewelOrderSummaryDTO.getCustomersCustomDTO());
				model.addAttribute("event", crownJewelOrderSummaryDTO.getEventDetailsDTO());
				model.addAttribute("customerLoyalty", crownJewelOrderSummaryDTO.getCustomerLoyaltyDTO());
				model.addAttribute("invoiceId", crownJewelOrderSummaryDTO.getInvoiceId());
			}else{
				model.addAttribute("msg", crownJewelOrderSummaryDTO.getError().getDescription());
				model.addAttribute("status", crownJewelOrderSummaryDTO.getStatus());
			}
			
			/*CrownJewelCustomerOrder CJEOrder = null;
			JSONObject jObj = new JSONObject();
			if(orderIdStr==null || orderIdStr.isEmpty()){
				jObj.put("errorMessage", "There is something wrong...Please try again");
			}
			Integer orderId = Integer.parseInt(orderIdStr);			
			CJEOrder = DAORegistry.getCrownJewelCustomerOrderDAO().get(orderId);
			Integer regularOrderId = CJEOrder.getRegularOrderId();
			if(regularOrderId != null && regularOrderId > 0){
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(CJEOrder.getRegularOrderId());
				CategoryTicketGroup catTicketGroup = DAORegistry.getCategoryTicketGroupDAO().get(customerOrder.getCategoryTicketGroupId());
				//Invoice invoice = DAORegistry.getInvoiceDAO().get(CJEOrder.getRegularOrderId());
				///map.put("invoice", invoice);
				//map.put("order", customerOrder);
				if(catTicketGroup != null){
					JSONObject catTicketGroupObject = new JSONObject();
					catTicketGroupObject.put("section", catTicketGroup.getSection());
					catTicketGroupObject.put("row", catTicketGroup.getRow());
					catTicketGroupObject.put("quantity", catTicketGroup.getQuantity());
					catTicketGroupObject.put("price", catTicketGroup.getPrice());
					jObj.put("ticket", catTicketGroupObject);
				}				
			}else{
				if(CJEOrder.getIsRealTicket() != null && CJEOrder.getIsRealTicket()){
					CrownJewelCategoryTicket crownJewelCategoryTicket = DAORegistry.getCrownJewelCategoryTicketDAO().get(CJEOrder.getTicketId());
					if(crownJewelCategoryTicket != null){
						JSONObject catTicketObject = new JSONObject();
						catTicketObject.put("section", crownJewelCategoryTicket.getSection());
						catTicketObject.put("row", "");
						catTicketObject.put("quantity", crownJewelCategoryTicket.getQuantity());
						catTicketObject.put("price", crownJewelCategoryTicket.getZonePrice());
						jObj.put("ticket", catTicketObject);
					}
				}else{
					CrownJewelTeamZones crownJewelTeamZones = DAORegistry.getCrownJewelTeamZonesDAO().get(CJEOrder.getTicketId());
					if(crownJewelTeamZones != null){
						JSONObject teamZonesObject = new JSONObject();
						teamZonesObject.put("section", crownJewelTeamZones.getZone());
						teamZonesObject.put("row", "");
						teamZonesObject.put("quantity", crownJewelTeamZones.getTicketsCount());
						teamZonesObject.put("price", crownJewelTeamZones.getPrice());
						jObj.put("ticket", teamZonesObject);
					}
				}
			}
			Customer customer = DAORegistry.getCustomerDAO().get(CJEOrder.getCustomerId());
			Event event = DAORegistry.getEventDAO().get(CJEOrder.getEventId());
			CustomerLoyaltyHistory customerLoyaltyHistory = null;
			if(CJEOrder.getRegularOrderId() != null){
				customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(CJEOrder.getRegularOrderId());
			}
			if(CJEOrder != null){
				JSONObject orderObject = new JSONObject();
				orderObject.put("id", CJEOrder.getId());
				orderObject.put("regularOrderId", CJEOrder.getRegularOrderId());
				orderObject.put("createdDate", CJEOrder.getCreatedDateStr());
				orderObject.put("status", CJEOrder.getStatus());
				orderObject.put("leagueName", CJEOrder.getFantasyEventName());
				orderObject.put("teamName", CJEOrder.getTeamName());
				orderObject.put("isPackageSelected", CJEOrder.getIsPackageSelected());
				jObj.put("cjeOrder", orderObject);
			}
			if(customer != null){
				JSONObject customerObject = new JSONObject();
				customerObject.put("customerName", customer.getCustomerName());
				customerObject.put("lastName", customer.getLastName());
				customerObject.put("email", customer.getEmail());
				customerObject.put("phone", customer.getPhone());
				jObj.put("customer", customerObject);
			}
			if(event != null){
				JSONObject eventObject = new JSONObject();
				eventObject.put("eventName", event.getEventName());
				eventObject.put("eventDate", event.getEventDateStr());
				eventObject.put("eventTime", event.getEventTimeStr());
				jObj.put("event", eventObject);
			}
			if(customerLoyaltyHistory != null){
				JSONObject loyaltyObject = new JSONObject();
				loyaltyObject.put("pointsSpent", customerLoyaltyHistory.getPointsSpent());
				jObj.put("customerLoyalty", loyaltyObject);
			}
			jObj.put("invoiceId", regularOrderId);
			if(msg!=null && !msg.isEmpty()){
				jObj.put("successMessage", msg);
			}
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");			
		}
		return "";
	}
	
	@RequestMapping("/UpdateCrownJewelOrder")
	public String updateCrownJewelOrder(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String orderIds = request.getParameter("orderIds");
			String reason = request.getParameter("reason");
			String status = request.getParameter("status");
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderIds", orderIds);
			map.put("reason", reason);
			map.put("status", status);
			map.put("userName", username);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.CROWN_JEWEL_ORDER_UPDATE_REASON);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelOrderUpdateReasonDTO crownJewelOrderUpdateReasonDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelOrderUpdateReasonDTO")), CrownJewelOrderUpdateReasonDTO.class);
						
			if(crownJewelOrderUpdateReasonDTO.getStatus() == 1){
				model.addAttribute("msg", crownJewelOrderUpdateReasonDTO.getMessage());
				model.addAttribute("status", crownJewelOrderUpdateReasonDTO.getStatus());
				model.addAttribute("orders", crownJewelOrderUpdateReasonDTO.getCrownJewelCustomerOrderDTOs());
			}else{
				model.addAttribute("msg", crownJewelOrderUpdateReasonDTO.getError().getDescription());
				model.addAttribute("status", crownJewelOrderUpdateReasonDTO.getStatus());
			}
			
			/*JSONObject jsonObject = new JSONObject();
			String msg="";
			JSONArray jsonArray = new JSONArray();
			if(orderIds != null && !orderIds.isEmpty()){
				String arr[] = orderIds.split(",");
				if(reason!=null  && !reason.isEmpty() && status!=null && !status.isEmpty()){
					JSONObject object=null;
					
					for(String orderId:arr){
						CrownJewelCustomerOrder order = DAORegistry.getCrownJewelCustomerOrderDAO().get(Integer.parseInt(orderId));
						CrownJewelOrderStatus orderStatus = CrownJewelOrderStatus.valueOf(status);
						order.setStatus(orderStatus.toString());
						order.setAcceptRejectReson(reason);
						object = new JSONObject();
						object.put("id", order.getId());
						jsonArray.put(object);
						CustomerOrder customerOrder = null;
						if(orderStatus.equals(CrownJewelOrderStatus.APPROVED)){
							if(order.getRegularOrderId()==null){
								CrownJewelCategoryTicket catTicketGroup = null;
								CrownJewelTeamZones crownJewelTeamZones = null;
								EventDetails event = DAORegistry.getEventDetailsDAO().get(order.getEventId());
								if(order.getIsRealTicket() != null && order.getIsRealTicket()){
									catTicketGroup = DAORegistry.getCrownJewelCategoryTicketDAO().get(order.getTicketId());
									if(catTicketGroup==null){
										msg="Ticket group is not found for one of selected order.";
										break;
									}
								}else{
									crownJewelTeamZones = DAORegistry.getCrownJewelTeamZonesDAO().get(order.getTicketId());
									if(crownJewelTeamZones==null){
										msg="Ticket Information is not found for selected team's order.";
										break;
									}
								}
								CustomerAddress blAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(order.getCustomerId());
								List<CustomerAddress> shippingAddresses = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(order.getCustomerId());
								
								if(event==null){
									msg="Event not found in system for one of selected order.";
									break;
								}
								if(blAddress==null){
									msg="Customer billing address is not found for one of selected order.";
									break;
								}
								if(shippingAddresses==null || shippingAddresses.isEmpty()){
									msg="Customer shipping address is not found for one of selected order.";
									break;
								}
								CustomerAddress shippingAddress = shippingAddresses.get(0);
								customerOrder = createNewCustomerOrderAndInvoice(order,event,catTicketGroup,crownJewelTeamZones,blAddress,shippingAddress,username);
								order.setRegularOrderId(customerOrder.getId());
								msg="selected orders are updated successfully.";
							}
						}else if(orderStatus.equals(CrownJewelOrderStatus.REJECTED)){
							if(order.getRegularOrderId()!=null){
								customerOrder = DAORegistry.getCustomerOrderDAO().get(order.getRegularOrderId());
								Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(order.getRegularOrderId());
								customerOrder.setStatus(OrderStatus.VOIDED.toString());
								customerOrder.setLastUpdated(new Date());
								invoice.setStatus(InvoiceStatus.Voided);
								invoice.setLastUpdated(new Date());
								invoice.setLastUpdatedBy(username);
								DAORegistry.getInvoiceDAO().update(invoice);
								DAORegistry.getCustomerOrderDAO().update(customerOrder);
							}
							msg="selected orders are updated successfully.";
						}
						DAORegistry.getCrownJewelCustomerOrderDAO().update(order);
					}
				}else{
					msg="Reason is found empty, Please add reason";
				}
			}else{
				msg="Selected order ids are not found, please tray again.";
			}
			jsonObject.put("msg", msg);
			jsonObject.put("orders",jsonArray);
			IOUtils.write(jsonObject.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	
	@RequestMapping("/CrownJewelFinalTeam")
	public String GetCrownJewelFinalTeam(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_FINAL_TEAM);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
						
			if(crownJewelTeamsDTO.getStatus() == 1){
				model.addAttribute("info", crownJewelTeamsDTO.getMessage());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
				model.addAttribute("childId", crownJewelTeamsDTO.getChildId());
				model.addAttribute("grandChildId", crownJewelTeamsDTO.getGrandChildId());
				model.addAttribute("childCategories", crownJewelTeamsDTO.getChildCategories());
				model.addAttribute("grandChildCategories", crownJewelTeamsDTO.getGrandChildCategories());
				model.addAttribute("leaguesList", crownJewelTeamsDTO.getLeaguesList());
				model.addAttribute("teamsList", crownJewelTeamsDTO.getTeamsList());
				model.addAttribute("leagueId", crownJewelTeamsDTO.getLeagueId());
				model.addAttribute("statusId", crownJewelTeamsDTO.getStatusId());
			}else{
				model.addAttribute("info", crownJewelTeamsDTO.getError().getDescription());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
			}

		} catch(Exception e){
			e.printStackTrace();
			model.addAttribute("info", "There is something wrong. Please try again..");
		}	
		return "page-crown-jewel-final-team";
	}
	
	
	
	/*private CustomerOrder createNewCustomerOrderAndInvoice(CrownJewelCustomerOrder order,EventDetails event,
			CrownJewelCategoryTicket catTicketGroup,CrownJewelTeamZones crownJewelTeamZones,CustomerAddress blAddress,CustomerAddress shippingAddress,String username){
		try {
			Date now = new Date();
			
			CategoryTicketGroup categoryTicketGroup = new CategoryTicketGroup();
			categoryTicketGroup.setBroadcast(true);
			categoryTicketGroup.setCost(0.0);
			categoryTicketGroup.setTaxAmount(0.0);
			categoryTicketGroup.setCreatedBy(username);
			categoryTicketGroup.setCreatedDate(now);
			categoryTicketGroup.setEventId(event.getEventId());
			categoryTicketGroup.setExternalNotes(null);
			categoryTicketGroup.setFacePrice(0.0);
			categoryTicketGroup.setInternalNotes("Manual");
			categoryTicketGroup.setLastUpdatedDate(now);
			categoryTicketGroup.setMarketPlaceNotes(null);
			categoryTicketGroup.setMaxShowing(null);
			categoryTicketGroup.setNearTermDisplayOption(null);
			//categoryTicketGroup.setPrice(catTicketGroup.getActualPrice());
			categoryTicketGroup.setProducttype(ProductType.REWARDTHEFAN); //confirm
			if(catTicketGroup != null){
				categoryTicketGroup.setQuantity(catTicketGroup.getQuantity());
				//categoryTicketGroup.setRetailPrice(catTicketGroup.getActualPrice());
				categoryTicketGroup.setRow(null);
				categoryTicketGroup.setRowRange(catTicketGroup.getRowRange());
				categoryTicketGroup.setSeatHigh(null);
				categoryTicketGroup.setSeatLow(null);
				categoryTicketGroup.setSection(catTicketGroup.getSection());
				categoryTicketGroup.setSectionRange(catTicketGroup.getSectionRange());
				categoryTicketGroup.setShippingMethod(catTicketGroup.getShippingMethod());
				if(catTicketGroup.getShippingMethod()!=null){
					ShippingMethod sMethod = DAORegistry.getShippingMethodDAO().getShippingMethodByName(catTicketGroup.getShippingMethod());
					if(sMethod!=null){
						categoryTicketGroup.setShippingMethodId(sMethod.getId());
					}
				}
				
				//categoryTicketGroup.setSoldPrice(catTicketGroup.getActualPrice()); //check with order price and ticket price
				categoryTicketGroup.setSoldQuantity(catTicketGroup.getQuantity());
			}else{
				if(crownJewelTeamZones != null){
					//CrownJewelCustomerOrder - Quantity
					categoryTicketGroup.setQuantity(order.getTicketQty());//crownJewelTeamZones.getTicketsCount()
					categoryTicketGroup.setRow(null);
					categoryTicketGroup.setRowRange(null);
					categoryTicketGroup.setSeatHigh(null);
					categoryTicketGroup.setSeatLow(null);
					categoryTicketGroup.setSection(crownJewelTeamZones.getZone());
					categoryTicketGroup.setSectionRange(null);
					categoryTicketGroup.setShippingMethod(null);
//					if(crownJewelTeamZones.getShippingMethod()!=null){
//						ShippingMethod sMethod = DAORegistry.getShippingMethodDAO().getShippingMethodByName(crownJewelTeamZones.getShippingMethod());
//						if(sMethod!=null){
//							categoryTicketGroup.setShippingMethodId(sMethod.getId());
//						}
//					}
					categoryTicketGroup.setSoldQuantity(order.getTicketQty());
				}
			}
			categoryTicketGroup.setStatus(TicketStatus.SOLD);
			categoryTicketGroup.setTicketOnhandStatus(null);
			//categoryTicketGroup.setWholeSalePrice(catTicketGroup.getActualPrice());
			
			CustomerOrder customerOrder = new CustomerOrder();
			customerOrder.setAcceptedBy(username);
			customerOrder.setAcceptedDate(now);
			customerOrder.setCreatedDate(now);
			customerOrder.setLastUpdated(now);
			customerOrder.setStatus(OrderStatus.ACTIVE.toString());
			customerOrder.setCustomerId(order.getCustomerId());
			customerOrder.setQty(order.getTicketQty());
			
			customerOrder.setRow(categoryTicketGroup.getRow());
			customerOrder.setSeatHigh(categoryTicketGroup.getSeatHigh());
			customerOrder.setSeatLow(categoryTicketGroup.getSeatLow());
			customerOrder.setSection(categoryTicketGroup.getSection());
			
			customerOrder.setEventDate(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(event.getEventTime());
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getBuilding());
			customerOrder.setVenueState(event.getState());
			
			customerOrder.setTicketPrice(order.getTicketPrice());
			customerOrder.setOriginalOrderTotal(categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity()); //need to confirm with ulaga
			customerOrder.setTixGroupNotes(null);
			customerOrder.setShippingDate(event.getEventDate());
			customerOrder.setShippingMethodId(categoryTicketGroup.getShippingMethodId());
			customerOrder.setOrderTotal(categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity()); 
			customerOrder.setProductType(ProductType.REWARDTHEFAN.toString());
			customerOrder.setPlatform(ApplicationPlatform.TICK_TRACKER); //confirm with ulaga
			customerOrder.setPrimaryPaymentMethod(PaymentMethod.FULL_REWARDS);
			customerOrder.setSecondaryPayAmt(0.0);
			customerOrder.setDiscountAmt(0.0);
			customerOrder.setOrderType(OrderType.REGULAR);
			customerOrder.setSecondaryOrdertype(SecondaryOrderType.CROWNJEWEL);
			customerOrder.setSecondaryOrderId(order.getId().toString());
			Double orderTotal = categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity(); //Confirm With Amit
			if(order.getIsPackageSelected()){
				customerOrder.setCjePackageEnabled(true);
				customerOrder.setCjePackageNote(order.getPackageNote());
				customerOrder.setCjePackagePrice(order.getPackageCost());
				//customerOrder.setCjePackagePoints(order.getPackagePoints());
				orderTotal = (categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity()) + order.getPackageCost();
				//orderTotal = (categoryTicketGroup.getPrice()+ order.getPackageCost())* categoryTicketGroup.getQuantity();
				//Confirm With Amit
			}else{
				customerOrder.setCjePackageEnabled(false);
				customerOrder.setCjePackagePrice(0.00);
				customerOrder.setCjePackagePoints(0.00);
			}
			
			customerOrder.setTicketPrice(order.getTicketPrice());
			customerOrder.setSoldPrice(order.getTicketPrice());
			customerOrder.setTaxes(0.0);
			customerOrder.setOriginalOrderTotal(orderTotal);
			customerOrder.setOrderTotal(orderTotal); 
			customerOrder.setPrimaryPayAmt(order.getRequiredPoints().doubleValue());
			customerOrder.setCjeTicketPrice(order.getTicketPrice());
			//customerOrder.setCjeTicketPoints(order.getTicketPoints());
			customerOrder.setCjeRequirePoints(order.getRequiredPoints());
			
			Property property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
			Long primaryTrxId =  Long.valueOf(property.getValue());
			primaryTrxId++;
			String primaryTransactionId = "FULLREWARDS_TRX_0000"+primaryTrxId;
			property.setValue(String.valueOf(primaryTrxId));
			DAORegistry.getPropertyDAO().update(property);
			
			customerOrder.setPrimaryTransactionId(primaryTransactionId);
			
			CustomerOrderDetails orderDetails = new CustomerOrderDetails();
			orderDetails.setBlAddress1(blAddress.getAddressLine1());
			orderDetails.setBlAddress2(blAddress.getAddressLine2());
			orderDetails.setBlCity(blAddress.getCity());
			orderDetails.setBlCountry(blAddress.getCountry().getId());
			orderDetails.setBlCountryName(blAddress.getCountryName());
			orderDetails.setBlEmail(blAddress.getEmail());
			orderDetails.setBlFirstName(blAddress.getFirstName());
			orderDetails.setBlLastName(blAddress.getLastName());
			orderDetails.setBlPhone1(blAddress.getPhone1());
			orderDetails.setBlPhone2(blAddress.getPhone2());
			orderDetails.setBlState(blAddress.getState().getId());
			orderDetails.setBlStateName(blAddress.getStateName());
			orderDetails.setBlZipCode(blAddress.getZipCode());
			orderDetails.setShAddress1(shippingAddress.getAddressLine1());
			orderDetails.setShAddress2(shippingAddress.getAddressLine2());
			orderDetails.setShCity(shippingAddress.getCity());
			orderDetails.setShCountry(shippingAddress.getCountry().getId());
			orderDetails.setShCountryName(shippingAddress.getCountryName());
			orderDetails.setShEmail(shippingAddress.getEmail());
			orderDetails.setShFirstName(shippingAddress.getFirstName());
			orderDetails.setShLastName(shippingAddress.getLastName());
			orderDetails.setShPhone1(shippingAddress.getPhone1());
			orderDetails.setShPhone2(shippingAddress.getPhone2());
			orderDetails.setShState(shippingAddress.getState().getId());
			orderDetails.setShStateName(shippingAddress.getStateName());
			orderDetails.setShZipCode(shippingAddress.getZipCode());
			
			
			Invoice invoice = new Invoice();
			invoice.setCustomerId(order.getCustomerId());
			invoice.setCreatedBy(username);
			invoice.setCreatedDate(now);
			invoice.setInvoiceType("INVOICE");
			invoice.setLastUpdated(now);
			invoice.setLastUpdatedBy(username);
			invoice.setStatus(InvoiceStatus.Outstanding);
			invoice.setTicketCount(order.getTicketQty()); //check while creating invoice in tic tracker
			invoice.setInvoiceTotal(categoryTicketGroup.getPrice() * categoryTicketGroup.getQuantity());
			invoice.setShippingMethodId(categoryTicketGroup.getShippingMethodId());
			
			DAORegistry.getCategoryTicketGroupDAO().save(categoryTicketGroup);
			customerOrder.setCategoryTicketGroupId(categoryTicketGroup.getId());
			DAORegistry.getCustomerOrderDAO().save(customerOrder);
			invoice.setCustomerOrderId(customerOrder.getId());
			orderDetails.setOrderId(customerOrder.getId());
			DAORegistry.getCustomerOrderDetailsDAO().save(orderDetails);
			DAORegistry.getInvoiceDAO().save(invoice);
			categoryTicketGroup.setInvoiceId(invoice.getId());
			DAORegistry.getCategoryTicketGroupDAO().update(categoryTicketGroup);
			
			List<CategoryTicket> catTicketList = new ArrayList<CategoryTicket>();
			if(categoryTicketGroup.getQuantity()>0){
				CategoryTicket catTicket = null;
				for(int i=0;i<categoryTicketGroup.getQuantity();i++){
					catTicket = new CategoryTicket();
					catTicket.setActualPrice(categoryTicketGroup.getPrice());
					catTicket.setCategoryTicketGroupId(categoryTicketGroup.getId());
					catTicket.setExchangeRequestId(null);
					catTicket.setFillDate(now);
					catTicket.setInvoiceId(invoice.getId());
					catTicket.setIsProcessed(true);
					catTicket.setTicketId(null);
					catTicket.setUserName(username);
					catTicket.setSoldPrice(categoryTicketGroup.getSoldPrice());
					catTicketList.add(catTicket);
				}
				DAORegistry.getCategoryTicketDAO().saveAll(catTicketList);
			}
			return customerOrder;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/
	
	@RequestMapping(value = "/CrownJewelChild")
	public String getChildCategoryDetails(HttpServletRequest request, HttpServletResponse response, Model model){	
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_CHILD);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
						
			if(crownJewelTeamsDTO.getStatus() == 1){
				model.addAttribute("parentCategoryList", crownJewelTeamsDTO.getParentCategoryList());
				model.addAttribute("childCategoryList", crownJewelTeamsDTO.getChildCategories());
			}else{
				model.addAttribute("errorMessage", crownJewelTeamsDTO.getError().getDescription());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", crownJewelTeamsDTO.getPaginationDTO());
				model.addAttribute("childCategory", crownJewelTeamsDTO.getChildCategory());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(crownJewelTeamsDTO.getPaginationDTO()));
				model.addAttribute("childCategory", gson.toJson(crownJewelTeamsDTO.getChildCategory()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-crown-jewel-child";
	}
	
	/*
	@RequestMapping(value = "/getChildCategory")
	public void getChildCategory(HttpServletRequest request, HttpServletResponse response){		
		JSONObject returnObject = new JSONObject();
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		Integer count =0;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getParentCategoriesByName("SPORTS");
			List<FantasyChildCategory> childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getAll();
			List<FantasyChildCategory> childCategory = DAORegistry.getQueryManagerDAO().getChildCategoryDetails(filter, pageNo); 		
			if(childCategory != null){
				count = childCategory.size();
			}
			
			returnObject.put("parentCategoryList", parentCategoryList);
			returnObject.put("childCategoryList", childCategoryList);
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			returnObject.put("childCategory", JsonWrapperUtil.getFantasyChildCategoryArray(childCategory));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	@RequestMapping(value = "/getChildCategoryValue")
	public void getChildCategoryValue(HttpServletRequest request, HttpServletResponse response, Model model){		
		JSONObject returnObject = new JSONObject();
		String childId = request.getParameter("childId");		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("childId", childId);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CHILD_CATEGORY_VALUE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
						
			if(crownJewelTeamsDTO.getStatus() == 1){
				com.rtw.tracker.datas.FantasyChildCategory childCat = crownJewelTeamsDTO.getFantasyChildCategory();
				com.rtw.tracker.datas.FantasyParentCategory parentCat = crownJewelTeamsDTO.getFantasyParentCategory();
				returnObject.put("childCat", JsonWrapperUtil.getFantasyChildCategoryIdArray(childCat, parentCat));
				returnObject.put("msg", crownJewelTeamsDTO.getMessage());
			}else{
				returnObject.put("msg", crownJewelTeamsDTO.getError().getDescription());				
			}
			returnObject.put("status", crownJewelTeamsDTO.getStatus());
			
			/*FantasyChildCategory childCat = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryById(Integer.parseInt(childId));
			FantasyParentCategory parentCat = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryById(childCat.getParentId());
			
			returnObject.put("childCat", JsonWrapperUtil.getFantasyChildCategoryIdArray(childCat, parentCat));*/
			
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/updateChildCategory",method=RequestMethod.POST)
	public String updateChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){		
		try{			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CHILD_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());				
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}
		return "";
	}
		
	/*
	@RequestMapping({"/ChildCategoryExportToExcel"})
	public void childCategoryToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyChildCategory> childCategoryList = DAORegistry.getQueryManagerDAO().getChildCategoryDetailsToExport(filter);
			
			if(childCategoryList!=null && !childCategoryList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Child");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Child Category Id");
				header.createCell(1).setCellValue("Child Category Name");
				header.createCell(2).setCellValue("Parent Category Name");
				header.createCell(3).setCellValue("Last Updated By");
				header.createCell(4).setCellValue("Last Updated Date");
				Integer i=1;
				for(FantasyChildCategory fantasyChild : childCategoryList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(fantasyChild.getId());
					row.createCell(1).setCellValue(fantasyChild.getName());
					row.createCell(2).setCellValue(fantasyChild.getParentCategoryName()!=null?fantasyChild.getParentCategoryName():"");
					row.createCell(3).setCellValue(fantasyChild.getLastUpdatedBy()!=null?fantasyChild.getLastUpdatedBy():"");
					row.createCell(4).setCellValue(fantasyChild.getLastUpdatedStr()!=null?fantasyChild.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Child.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	@RequestMapping(value = "/CrownJewelGrandChild")
	public String getGrandChildCategoryDetails(HttpServletRequest request, HttpServletResponse response, Model model){		
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CROWN_JEWEL_GRAND_CHILD);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
						
			if(crownJewelTeamsDTO.getStatus() == 1){
				model.addAttribute("parentCategoryList", crownJewelTeamsDTO.getParentCategoryList());
				model.addAttribute("childCategoryList", crownJewelTeamsDTO.getChildCategories());
				model.addAttribute("grandChildCategoryList", crownJewelTeamsDTO.getGrandChildCategories());
			}else{
				model.addAttribute("errorMessage", crownJewelTeamsDTO.getError().getDescription());
				model.addAttribute("status", crownJewelTeamsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", crownJewelTeamsDTO.getPaginationDTO());
				model.addAttribute("grandChildCategory", crownJewelTeamsDTO.getGrandChildCategory());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(crownJewelTeamsDTO.getPaginationDTO()));
				model.addAttribute("grandChildCategory", gson.toJson(crownJewelTeamsDTO.getGrandChildCategory()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-crown-jewel-grandchild";
	}
	
	/*
	@RequestMapping(value = "/getGrandChildCategory")
	public void getGrandChildCategory(HttpServletRequest request, HttpServletResponse response){		
		JSONObject returnObject = new JSONObject();
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		Integer count =0;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyGrandChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyParentCategory> parentCategoryList = DAORegistry.getFantasyParentCategoryDAO().getParentCategoriesByName("SPORTS");
			List<FantasyChildCategory> childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getAll();
			List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getAll();
			List<FantasyGrandChildCategory> grandChildCategory = DAORegistry.getQueryManagerDAO().getGrandChildCategoryDetails(filter, pageNo); 		
			if(grandChildCategory != null){
				count = grandChildCategory.size();
			}
			
			returnObject.put("parentCategoryList", parentCategoryList);
			returnObject.put("childCategoryList", childCategoryList);
			returnObject.put("grandChildCategoryList", grandChildCategoryList);
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			returnObject.put("grandChildCategory", JsonWrapperUtil.getFantasyGrandChildCategoryArray(grandChildCategory));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	@RequestMapping(value = "/getGrandChildCategoryValue")
	public void getGrandChildCategoryValue(HttpServletRequest request, HttpServletResponse response, Model model){		
		JSONObject returnObject = new JSONObject();
		String grandChildId = request.getParameter("grandChildId");		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("grandChildId", grandChildId);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_GRAND_CHILD_CATEGORY_VALUE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			CrownJewelTeamsDTO crownJewelTeamsDTO = gson.fromJson(((JsonObject)jsonObject.get("crownJewelTeamsDTO")), CrownJewelTeamsDTO.class);
						
			if(crownJewelTeamsDTO.getStatus() == 1){
				com.rtw.tracker.datas.FantasyGrandChildCategory grandChildCat = crownJewelTeamsDTO.getFantasyGrandChildCategory();
				com.rtw.tracker.datas.FantasyChildCategory childCat = crownJewelTeamsDTO.getFantasyChildCategory();
				com.rtw.tracker.datas.FantasyParentCategory parentCat = crownJewelTeamsDTO.getFantasyParentCategory();
				returnObject.put("grandChildCat", JsonWrapperUtil.getFantasyGrandChildCategoryIdArray(grandChildCat, childCat, parentCat));
				returnObject.put("msg", crownJewelTeamsDTO.getMessage());
			}else{
				returnObject.put("msg", crownJewelTeamsDTO.getError().getDescription());				
			}
			returnObject.put("status", crownJewelTeamsDTO.getStatus());
			
			/*FantasyGrandChildCategory grandChildCat = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildCategoryById(Integer.parseInt(grandChildId));
			FantasyChildCategory childCat = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryById(grandChildCat.getChildId());
			FantasyParentCategory parentCat = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryById(childCat.getParentId());
			
			returnObject.put("grandChildCat", JsonWrapperUtil.getFantasyGrandChildCategoryIdArray(grandChildCat, childCat, parentCat));*/
			
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/updateGrandChildCategory", method=RequestMethod.POST)	
	public String updateGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){		

		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_GRAND_CHILD_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());				
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
						
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}
		return "";
	}
	
	/*	
	@RequestMapping({"/GrandChildCategoryExportToExcel"})
	public void grandChildCategoryToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getFantasyGrandChildCategorySearchHeaderFilters(headerFilter);
			List<FantasyGrandChildCategory> grandChildCategoryList = DAORegistry.getQueryManagerDAO().getGrandChildCategoryDetailsToExport(filter);
			
			if(grandChildCategoryList!=null && !grandChildCategoryList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_GrandChild");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Grand Child Category Id");
				header.createCell(1).setCellValue("Grand Child Category Name");
				header.createCell(2).setCellValue("Parent Category Name");
				header.createCell(3).setCellValue("Child Category Name");
				header.createCell(4).setCellValue("Package Cost");
				header.createCell(5).setCellValue("Last Updated By");
				header.createCell(6).setCellValue("Last Updated Date");
				Integer i=1;
				for(FantasyGrandChildCategory fantasyGrandChild : grandChildCategoryList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(fantasyGrandChild.getId());
					row.createCell(1).setCellValue(fantasyGrandChild.getName());
					row.createCell(2).setCellValue(fantasyGrandChild.getParentCategoryName()!=null?fantasyGrandChild.getParentCategoryName():"");
					row.createCell(3).setCellValue(fantasyGrandChild.getChildCategoryName()!=null?fantasyGrandChild.getChildCategoryName():"");
					row.createCell(4).setCellValue(fantasyGrandChild.getPackageCost()!=null?fantasyGrandChild.getPackageCost():0.00);
					row.createCell(5).setCellValue(fantasyGrandChild.getLastUpdatedBy()!=null?fantasyGrandChild.getLastUpdatedBy():"");
					row.createCell(6).setCellValue(fantasyGrandChild.getLastUpdatedStr()!=null?fantasyGrandChild.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_GrandChild.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	/*private boolean updateOrdersAndRevertRewardPoints(List<CrownJewelCustomerOrder> orders,String username){
		try {
			Date now = new Date(); 
			List<CrownJewelCustomerOrder> orderList = new ArrayList<CrownJewelCustomerOrder>();
			List<CustomerOrder> regularOrderList = new ArrayList<CustomerOrder>();
			List<Invoice> invoiceList = new ArrayList<Invoice>();
			List<Integer> regularOrderIds = new ArrayList<Integer>();
			List<CustomerLoyalty> loyaltyList = new ArrayList<CustomerLoyalty>();
			List<CustomerLoyaltyHistory> historyUpdateList = new ArrayList<CustomerLoyaltyHistory>();
			List<CustomerLoyaltyHistory> newHistoryList = new ArrayList<CustomerLoyaltyHistory>();
			CustomerLoyalty loyalty = null;
			for (CrownJewelCustomerOrder order : orders) {
				order.setStatus(CrownJewelOrderStatus.REJECTED.toString());
				order.setLastUpdatedDate(now);
				order.setAcceptRejectReson("Team was Excluded.");
				
				loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
				CustomerLoyaltyHistory loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getActiveCustomerLoyaltyHistoryOfCrownJewelOrder(order.getId(),order.getCustomerId());
				if(loyaltyHistory!=null){
					loyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
					loyaltyHistory.setUpdatedDate(now);
					historyUpdateList.add(loyaltyHistory);
				}
				
				CustomerLoyaltyHistory newHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getVoidRefundCustomerLoyaltyHistoryOfCrownJewelOrder(order.getId(),order.getCustomerId());
				if(newHistory==null){
					newHistory = new CustomerLoyaltyHistory();
					newHistory.setBalanceRewardAmount(loyalty.getActivePoints() + order.getRequiredPoints());
					newHistory.setBalanceRewardPoints(loyalty.getActivePoints() + order.getRequiredPoints());
					newHistory.setCreateDate(now);
					newHistory.setCustomerId(order.getCustomerId());
					newHistory.setDollarConversionRate(1.00);
					newHistory.setInitialRewardAmount(loyalty.getActivePoints());
					newHistory.setInitialRewardPoints(loyalty.getActivePoints());
					newHistory.setOrderId(order.getId());
					newHistory.setOrderTotal(order.getTicketPrice() * order.getTicketQty());
					newHistory.setOrderType(OrderType.CROWNJEWEL_REFUND);
					newHistory.setPointsEarned(order.getRequiredPoints());
					newHistory.setPointsSpent(0.00);
					newHistory.setRewardConversionRate(0.100);
					newHistory.setRewardEarnAmount(order.getRequiredPoints());
					newHistory.setRewardSpentAmount(0.00);
					newHistory.setRewardStatus(RewardStatus.ACTIVE);
					newHistory.setUpdatedDate(now);
					newHistoryList.add(newHistory);
				}else{
					newHistory.setRewardStatus(RewardStatus.ACTIVE);
					newHistory.setUpdatedDate(now);
					historyUpdateList.add(newHistory);
				}
				
				
				loyalty.setActivePoints(loyalty.getActivePoints() + order.getRequiredPoints());
				loyalty.setRevertedSpentPoints(loyalty.getRevertedSpentPoints() + order.getRequiredPoints());
				loyaltyList.add(loyalty);
				DAORegistry.getCustomerLoyaltyDAO().update(loyalty);
				
				if(order.getRegularOrderId()!=null){
					regularOrderIds.add(order.getRegularOrderId());
				}
				orderList.add(order);
			}
			if(!regularOrderIds.isEmpty()){
				List<CustomerOrder> regularOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIds(regularOrderIds);
				for(CustomerOrder order:regularOrders){
					order.setStatus(OrderStatus.VOIDED.toString());
					order.setLastUpdated(now);
					regularOrderList.add(order);
				}
				
				List<Invoice> invoices = DAORegistry.getInvoiceDAO().getInvoiceByOrderIds(regularOrderIds);
				for(Invoice invoice:invoices){
					invoice.setStatus(InvoiceStatus.Voided);
					invoice.setLastUpdated(now);
					invoice.setLastUpdatedBy(username);
					invoiceList.add(invoice);
				}
			}
			DAORegistry.getCrownJewelCustomerOrderDAO().updateAll(orderList);
			DAORegistry.getCustomerOrderDAO().updateAll(regularOrderList);
			DAORegistry.getInvoiceDAO().updateAll(invoiceList);
			//DAORegistry.getCustomerLoyaltyDAO().updateAll(loyaltyList);
			DAORegistry.getCustomerLoyaltyHistoryDAO().updateAll(historyUpdateList);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveAll(newHistoryList);
			for(CrownJewelCustomerOrder order : orderList){
				Map<String, String> requestMap = Util.getParameterMap(null);
				requestMap.put("customerId",String.valueOf(order.getCustomerId()));
				requestMap.put("orderType",OrderType.FANTASYTICKETS.toString());
				requestMap.put("orderNo",String.valueOf(order.getId()));
				String data = Util.getObject(requestMap,Constants.BASE_URL + Constants.CANCEL_ORDER);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}*/
	
	/*@RequestMapping(value="/TicketsExportToExcel")
	public void getFantasySportsTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String grandChildId = request.getParameter("grandChildType");
			FantasyGrandChildCategory grandChildCat = null;
				
			List<CrownJewelLeagues> cjLeaguesList = null;
			if(grandChildId != null && !grandChildId.isEmpty()){
				cjLeaguesList = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildId));
			}
			if(cjLeaguesList!=null && !cjLeaguesList.isEmpty()){
				for(CrownJewelLeagues league : cjLeaguesList){
					if(league.getEventId()!=null){
						Event event = DAORegistry.getEventDAO().get(league.getEventId());
						Venue venue = DAORegistry.getVenueDAO().get(event.getVenueId());
						league.setVenueString(event.getEventName()+" "+event.getEventDateStr()+" "+event.getEventTimeStr()+" "+venue.getLocation());
					}
					
				}
			}
			
			if(cjLeaguesList!=null && !cjLeaguesList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Sports_Tickets");
				Row header = sheet.createRow((int)0);				
				header.createCell(0).setCellValue("Fantasy Ticket Id");
				header.createCell(1).setCellValue("Event Name");
				header.createCell(2).setCellValue("Final TMAT Event");
				//header.createCell(3).setCellValue("Child Category");
				header.createCell(3).setCellValue("GrandChild Category");
				header.createCell(4).setCellValue("Last Updated By");
				header.createCell(5).setCellValue("Last Updated Date");
				int i=1;
				for(CrownJewelLeagues league : cjLeaguesList){
					Row row = sheet.createRow(i);					
					row.createCell(0).setCellValue(league.getId()!=null?league.getId():0);
					row.createCell(1).setCellValue(league.getName()!=null?league.getName():"");
					row.createCell(2).setCellValue(league.getVenueString()!=null?league.getVenueString():"");
					
					//childCat = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryById(league.getChildId());
					//row.createCell(3).setCellValue(childCat!=null?childCat.getName():"");
					grandChildCat = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildCategoryById(league.getGrandChildId());					
					row.createCell(3).setCellValue(grandChildCat!=null?grandChildCat.getName():"");
										
					row.createCell(4).setCellValue(league.getLastUpdateddBy()!=null?league.getLastUpdateddBy():"");
					row.createCell(5).setCellValue(league.getLastUpdatedStr()!=null?league.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Sports_Tickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping(value="/CategoryTeamsExportToExcel")
	public void getFantasyTeamsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String parentType = request.getParameter("parentType");
			String childId = request.getParameter("childType");
			String grandChildId = request.getParameter("grandChildType");
				
			List<CrownJewelCategoryTeams> cjCategoryTeams = null;
			if(grandChildId != null && !grandChildId.isEmpty()){
				cjCategoryTeams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByGrandChild(Integer.parseInt(grandChildId));
			}
			
			if(cjCategoryTeams!=null && !cjCategoryTeams.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Fantasy_Teams");
				Row header = sheet.createRow((int)0);				
				header.createCell(0).setCellValue("Fantasy Team Id");
				header.createCell(1).setCellValue("Team Name");
				header.createCell(2).setCellValue("Markup");
				header.createCell(3).setCellValue("Fractional Odds");
				header.createCell(4).setCellValue("Odds");
				header.createCell(5).setCellValue("Cutoff Date");
				header.createCell(6).setCellValue("Last Updated By");
				header.createCell(7).setCellValue("Last Updated Date");
				int i=1;
				for(CrownJewelCategoryTeams team : cjCategoryTeams){
					Row row = sheet.createRow(i);					
					row.createCell(0).setCellValue(team.getId()!=null?team.getId():0);
					row.createCell(1).setCellValue(team.getName()!=null?team.getName():"");
					row.createCell(2).setCellValue(team.getMarkup()!=null?team.getMarkup():0);
					row.createCell(3).setCellValue(team.getFractionalOdd()!=null?team.getFractionalOdd():"");
					row.createCell(4).setCellValue(team.getOdds()!=null?team.getOdds():0);
					row.createCell(5).setCellValue(team.getCutOffDateStr()!=null?team.getCutOffDateStr():"");
					row.createCell(6).setCellValue(team.getLastUpdatedBy()!=null?team.getLastUpdatedBy():"");
					row.createCell(7).setCellValue(team.getLastUpdatedStr()!=null?team.getLastUpdatedStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Fantasy_Teams.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
}