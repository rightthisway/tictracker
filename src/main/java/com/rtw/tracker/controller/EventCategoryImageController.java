package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.pojos.ChildCategoryImagesDTO;
import com.rtw.tracker.pojos.FantasyGrandChildCategoryImagesDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GrandChildCategoryImagesDTO;
import com.rtw.tracker.pojos.LoyalFanParentCategoryImagesDTO;
import com.rtw.tracker.pojos.ParentCategoryImagesDTO;
import com.rtw.tracker.pojos.PopularArtistDTO;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.pojos.ArtistImageDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

@Controller
public class EventCategoryImageController {
	

	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	@RequestMapping({"/ArtistImages"})
	public String artistImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			String msg = request.getParameter("msg");
			String sortingString = request.getParameter("sortingString");
			if(msg!=null && !msg.isEmpty()){
				model.addAttribute("successMessage", msg);
			}
						
			Map<String, String> map = Util.getParameterMap(request);
			map.put("sortingString", sortingString);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ARTIST_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PopularArtistDTO popularArtistDTO = gson.fromJson(((JsonObject)jsonObject.get("popularArtistDTO")), PopularArtistDTO.class);
			
			if(popularArtistDTO.getStatus() == 1){
				model.addAttribute("msg", popularArtistDTO.getMessage());
				model.addAttribute("status", popularArtistDTO.getStatus());				
			}else{
				model.addAttribute("msg", popularArtistDTO.getError().getDescription());
				model.addAttribute("status", popularArtistDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("artistList", popularArtistDTO.getPopularArtistCustomDTO());
				model.addAttribute("artistPagingInfo", popularArtistDTO.getPaginationDTO());
			}else{
				model.addAttribute("artistList", gson.toJson(popularArtistDTO.getPopularArtistCustomDTO()));
				model.addAttribute("pagingInfo", gson.toJson(popularArtistDTO.getPaginationDTO()));				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		model.addAttribute("productName", "Reward The Fan");
		
		return "page-artist-image";
	}
	
	/*@RequestMapping({"/GetArtistImages"})
	public String getArtistImages(HttpServletRequest request, HttpServletResponse response, Model model){
		//JSONObject returnObject = new JSONObject();
		
		try{			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_ARTIST_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PopularArtistDTO popularArtistDTO = gson.fromJson(((JsonObject)jsonObject.get("popularArtistDTO")), PopularArtistDTO.class);
						
			if(popularArtistDTO.getStatus() == 1){
				model.addAttribute("msg", popularArtistDTO.getMessage());
				model.addAttribute("status", popularArtistDTO.getStatus());
				model.addAttribute("artistList", popularArtistDTO.getPopularArtistCustomDTO());
				model.addAttribute("artistPagingInfo", popularArtistDTO.getPaginationDTO());				
			}else{
				model.addAttribute("msg", popularArtistDTO.getError().getDescription());
				model.addAttribute("status", popularArtistDTO.getStatus());
			}
			
//			System.out.println("headerFilters   :   "+headerFilter);
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
//			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
//			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
//			
//			returnObject.put("artistList", JsonWrapperUtil.getAllArtistArray(artistList));
//			returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}*/
	
	/*@RequestMapping({"/ArtistImagesExportToExcel"})
	public void artistImagesToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtistToExport(filter);
			
			if(artistList!=null && !artistList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Artist_Images");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Image File URL");
				header.createCell(6).setCellValue("Image Uploaded");
				Integer i=1;
				for(PopularArtist artist : artistList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentType()!=null?artist.getParentType():"");
					row.createCell(5).setCellValue(artist.getImageFileUrl()!=null?artist.getImageFileUrl():"");
					if(artist.getImageFileUrl() != null && artist.getImageFileUrl() != ""){
						row.createCell(6).setCellValue("Yes");
					}else{
						row.createCell(6).setCellValue("No");
					}
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Artist_Images.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/ArtistExportToExcel"})
	public void artistToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtistToExport(filter);
			
			if(artistList!=null && !artistList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Artist");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				Integer i=1;
				for(PopularArtist artist : artistList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentType()!=null?artist.getParentType():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Artist.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/PopularArtistExportToExcel"})
	public void popularArtistToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPopularArtistSearchHeaderFilters(headerFilter);
			Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductIdToExport(product.getId(),filter);
			Collection<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}
			
			if(popularArtists!=null && !popularArtists.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Popular_Artist");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Created By");
				header.createCell(6).setCellValue("Created Date");
				Integer i=1;
				for(PopularArtist artist : popularArtists){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentType()!=null?artist.getParentType():"");
					row.createCell(5).setCellValue(artist.getCreatedBy()!=null?artist.getCreatedBy():"");
					row.createCell(6).setCellValue(artist.getCreatedDateStr()!=null?artist.getCreatedDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Popular_Artist.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/GetArtistsForCustomer"})
	public void getArtistsForCustomer(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		try{
			JSONObject returnObject = new JSONObject();
			Integer count = 0;
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String customerIdStr = request.getParameter("customerId");
			System.out.println("headerFilters   :   "+headerFilter);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
						  
			if(customerIdStr != null && !customerIdStr.isEmpty()){
				Integer customerId = Integer.parseInt(customerIdStr);

//				Map<String, String> map = Util.getParameterMap(request);
//			    map.put("customerId", customerIdStr);
//			    map.put("productType", Constants.PRODUCTTYPE);
//			    map.put("platForm", Constants.PLATFORM);
//			    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.GET_LOYALFAN_STATUS);
//			    Gson gson = new Gson();  
//				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
//				LoyalFanStatus loyalFans = gson.fromJson(((JsonObject)jsonObject.get("loyalFanStatus")), LoyalFanStatus.class);
				
				CustomerLoyalFan loyalFans = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
				JSONObject loyalFanObj = new JSONObject();				
				if(loyalFans != null){
					if(loyalFans.getLoyalFanType() != null && loyalFans.getLoyalFanType().equalsIgnoreCase("SPORTS")){
						loyalFanObj.put("artistName", loyalFans.getLoyalName());
					}else{
						loyalFanObj.put("cityName", loyalFans.getLoyalName()!=null?loyalFans.getLoyalName()+", "+loyalFans.getCountry():"");
					}
					loyalFanObj.put("categoryName", loyalFans.getLoyalFanType()!=null?loyalFans.getLoyalFanType():"");
					if(loyalFans.getTicketPurchased() != null && loyalFans.getTicketPurchased()){
						loyalFanObj.put("ticketsPurchased", "Yes");
					}else{
						loyalFanObj.put("ticketsPurchased", "No");
					}
					loyalFanObj.put("startDate", loyalFans.getStartDateStr()!=null?loyalFans.getStartDateStr():"");
					loyalFanObj.put("endDate", loyalFans.getEndDateStr()!=null?loyalFans.getEndDateStr():"");
				}
				returnObject.put("loyalFanInfo", loyalFanObj);
			}
			
			returnObject.put("artistList", JsonWrapperUtil.getAllArtistArray(artistList));
			returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());;
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/updateArtistImages"})
	public String updateArtistImages(HttpServletRequest request, HttpServletResponse response,Model model){
		String returnMessage = "";
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_ARTIST_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				returnMessage = genericResponseDTO.getMessage();		
			}else{
				returnMessage = genericResponseDTO.getError().getDescription();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "redirect:/ArtistImages?msg="+returnMessage;
	}

	
	@RequestMapping({"/GetArtistImagesForEdit"})
	public void getArtistImagesForEdit(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		try{
			String artistId = request.getParameter("artistId");
			
			JSONObject returnObject = new JSONObject();
			JSONObject artistObj = new JSONObject();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("artistId", artistId);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_ARTIST_IMAGES_FOR_EDIT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ArtistImageDTO artistImageDTO = gson.fromJson(((JsonObject)jsonObject.get("artistImageDTO")), ArtistImageDTO.class);
						
			if(artistImageDTO.getStatus() == 1){
				returnObject.put("msg", artistImageDTO.getMessage());
				returnObject.put("status", artistImageDTO.getStatus());
				if(artistImageDTO.getArtistImage() != null){
					artistObj.put("id", artistImageDTO.getArtistImage().getId());
					artistObj.put("imageFileUrl", artistImageDTO.getArtistImage().getImageFileUrl());
					//artistObj.put("file", artistImageDTO.getArtistImage().getFile());			
				}
				returnObject.put("artistImage", artistObj);				
			}else{
				returnObject.put("msg", artistImageDTO.getError().getDescription());
				returnObject.put("status", artistImageDTO.getStatus());
			}
		
			/*ArtistImage artistImage = DAORegistry.getArtistImageDAO().getArtistImageByArtistId(Integer.parseInt(artistId));
			
			returnObject.put("artistImage", artistObj);*/
			
			response.setHeader("Content-type", "application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/GrandChildCategoryImages"})
	public String getGrandChildCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GRAND_CHILD_CATEGORY_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GrandChildCategoryImagesDTO grandChildCategoryImagesDTO = gson.fromJson(((JsonObject)jsonObject.get("grandChildCategoryImagesDTO")), GrandChildCategoryImagesDTO.class);
						
			if(grandChildCategoryImagesDTO.getStatus() == 1){
				model.addAttribute("info", grandChildCategoryImagesDTO.getMessage());
				model.addAttribute("status", grandChildCategoryImagesDTO.getStatus());
			}else{
				model.addAttribute("info", grandChildCategoryImagesDTO.getError().getDescription());
				model.addAttribute("status", grandChildCategoryImagesDTO.getStatus());
			}
			
			model.addAttribute("grandChilds", grandChildCategoryImagesDTO.getGrandChilds());
			model.addAttribute("grandChildsList", grandChildCategoryImagesDTO.getGrandChildsList());
			model.addAttribute("grandChildImageList", grandChildCategoryImagesDTO.getGrandChildImageList());
			model.addAttribute("grandChildStr", grandChildCategoryImagesDTO.getGrandChildStr());
			model.addAttribute("productName", grandChildCategoryImagesDTO.getProductName());
			model.addAttribute("grandChildsCheckAll", grandChildCategoryImagesDTO.isGrandChildsCheckAll());
			model.addAttribute("grandChildSearch", request.getParameter("grandChildSearch"));
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-grand-child-category-image";
	}
	
	
	@RequestMapping({"/FantasyGrandChildCategoryImages"})
	public String getFantasyGrandChildCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_FANTASY_GRAND_CHILD_CATEGORY_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FantasyGrandChildCategoryImagesDTO fantasyGrandChildCategoryImagesDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyGrandChildCategoryImagesDTO")), FantasyGrandChildCategoryImagesDTO.class);
									
			if(fantasyGrandChildCategoryImagesDTO.getStatus() == 1){
				model.addAttribute("info", fantasyGrandChildCategoryImagesDTO.getMessage());
				model.addAttribute("status", fantasyGrandChildCategoryImagesDTO.getStatus());
			}else{
				model.addAttribute("info", fantasyGrandChildCategoryImagesDTO.getError().getDescription());
				model.addAttribute("status", fantasyGrandChildCategoryImagesDTO.getStatus());
			}
			
			model.addAttribute("grandChilds", fantasyGrandChildCategoryImagesDTO.getGrandChilds());
			model.addAttribute("grandChildsList", fantasyGrandChildCategoryImagesDTO.getGrandChildsList());
			model.addAttribute("grandChildImageList", fantasyGrandChildCategoryImagesDTO.getGrandChildImageList());
			model.addAttribute("grandChildStr", fantasyGrandChildCategoryImagesDTO.getGrandChildStr());
			model.addAttribute("productName", fantasyGrandChildCategoryImagesDTO.getProductName());
			model.addAttribute("grandChildsCheckAll", fantasyGrandChildCategoryImagesDTO.isGrandChildsCheckAll());
			model.addAttribute("grandChildSearch", request.getParameter("grandChildSearch"));
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-fantasy-grand-child-category-image";
	}
	
	
	
	@RequestMapping({"/LoyalFanParentCategoryImages"})
	public String getLoyalFanParentCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_LOYAL_FAN_PARENT_CATEGORY_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			LoyalFanParentCategoryImagesDTO loyalFanParentCategoryImagesDTO = gson.fromJson(((JsonObject)jsonObject.get("loyalFanParentCategoryImagesDTO")), LoyalFanParentCategoryImagesDTO.class);
									
			if(loyalFanParentCategoryImagesDTO.getStatus() == 1){
				model.addAttribute("info", loyalFanParentCategoryImagesDTO.getMessage());
				model.addAttribute("status", loyalFanParentCategoryImagesDTO.getStatus());
			}else{
				model.addAttribute("info", loyalFanParentCategoryImagesDTO.getError().getDescription());
				model.addAttribute("status", loyalFanParentCategoryImagesDTO.getStatus());
			}
			
			model.addAttribute("parents", loyalFanParentCategoryImagesDTO.getParents());
			model.addAttribute("parentList", loyalFanParentCategoryImagesDTO.getParentList());
			model.addAttribute("parentCheckAll", loyalFanParentCategoryImagesDTO.isParentCheckAll());
			model.addAttribute("parentStr", loyalFanParentCategoryImagesDTO.getParentStr());
			model.addAttribute("parentImageList", loyalFanParentCategoryImagesDTO.getParentImageList());
			model.addAttribute("productName", loyalFanParentCategoryImagesDTO.getProductName());
			model.addAttribute("parentSearch", request.getParameter("parentSearch"));
			
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-loyalfan-parent-category-image";
	}
	
	
	@RequestMapping({"/ChildCategoryImages"})
	public String getChildCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHILD_CATEGORY_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ChildCategoryImagesDTO childCategoryImagesDTO = gson.fromJson(((JsonObject)jsonObject.get("childCategoryImagesDTO")), ChildCategoryImagesDTO.class);
									
			if(childCategoryImagesDTO.getStatus() == 1){
				model.addAttribute("info", childCategoryImagesDTO.getMessage());
				model.addAttribute("status", childCategoryImagesDTO.getStatus());
			}else{
				model.addAttribute("info", childCategoryImagesDTO.getError().getDescription());
				model.addAttribute("status", childCategoryImagesDTO.getStatus());
			}
			
			model.addAttribute("childImageList", childCategoryImagesDTO.getChildImageList());
			model.addAttribute("childs", childCategoryImagesDTO.getChilds());
			model.addAttribute("childsList", childCategoryImagesDTO.getChildsList());
			model.addAttribute("childStr", childCategoryImagesDTO.getChildStr());
			model.addAttribute("productName", childCategoryImagesDTO.getProductName());
			model.addAttribute("childsCheckAll", childCategoryImagesDTO.isChildsCheckAll());
			model.addAttribute("childSearch", request.getParameter("childSearch"));
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-child-category-image";
	}
	
	@RequestMapping({"/ParentCategoryImages"})
	public String getParentCategoryImages(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PARENT_CATEGORY_IMAGES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ParentCategoryImagesDTO parentCategoryImagesDTO = gson.fromJson(((JsonObject)jsonObject.get("parentCategoryImagesDTO")), ParentCategoryImagesDTO.class);
									
			if(parentCategoryImagesDTO.getStatus() == 1){
				model.addAttribute("info", parentCategoryImagesDTO.getMessage());
				model.addAttribute("status", parentCategoryImagesDTO.getStatus());
			}else{
				model.addAttribute("info", parentCategoryImagesDTO.getError().getDescription());
				model.addAttribute("status", parentCategoryImagesDTO.getStatus());
			}
			
			model.addAttribute("parentImageList", parentCategoryImagesDTO.getParentImageList());
			model.addAttribute("productName", parentCategoryImagesDTO.getProductName());
			model.addAttribute("grandChildSearch", request.getParameter("grandChildSearch"));
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-parent-category-image";
	}

}
