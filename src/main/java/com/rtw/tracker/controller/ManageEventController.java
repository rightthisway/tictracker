package com.rtw.tracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.Util;
import com.rtw.tmat.dao.implementation.TMATDAORegistry;
import com.rtw.tmat.utils.Error;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Artist;
import com.rtw.tracker.datas.Venue;
import com.rtw.tracker.pojos.*;
import com.rtw.tracker.utils.GridHeaderFilters;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.JsonWrapperUtil;
import com.rtw.tracker.utils.PaginationUtil;

@Controller
@RequestMapping({"/"})
public class ManageEventController {
	

	@RequestMapping({"/Events"})
	public String loadDashboardPage(HttpServletRequest request, HttpServletResponse response, ModelMap map,HttpSession session){
		try{
//			Integer eventCount = 0;
//			Collection<EventDetails> events = null;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			Date fromDate = new Date();
			cal.add(Calendar.MONTH, 3);
			Date toDate = cal.getTime();
			String fromDateStr = dateFormat.format(fromDate);
			String toDateStr = dateFormat.format(toDate);
			//Integer brokerId= Util.getBrokerId(session);
			
			String searchType = "";
			String searchValue = "";
			String pageNo = "0";
			GridHeaderFilters filter = new GridHeaderFilters();
						
			String brokerId= Util.getBrokerId(session) != null ? String.valueOf(Util.getBrokerId(session)) : "";
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("searchType", searchType);
			paramMap.put("searchValue", searchValue);
			paramMap.put("fromDate", fromDateStr);
			paramMap.put("toDate", toDateStr);
			paramMap.put("pageNo", pageNo);
			paramMap.put("headerFilter", filter.toString());
			paramMap.put("brokerId", brokerId);
			
			String data = Util.getObject(paramMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EventListDTO eventListDTO = gson.fromJson(((JsonObject)jsonObject.get("eventListDTO")), EventListDTO.class);
			
			if(eventListDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					map.put("events", eventListDTO.getEvents());
					map.put("pagingInfo", eventListDTO.getPaginationDTO());
					map.put("msg", eventListDTO.getMessage());
					map.put("status", eventListDTO.getStatus());
				}else{
					map.put("status", "Available");
					map.put("events", gson.toJson(eventListDTO.getEvents()));
					map.put("pagingInfo", gson.toJson(eventListDTO.getPaginationDTO()));
					map.put("ticketPagingInfo", gson.toJson(eventListDTO.getTicketPagingInfo()));
					map.put("fromDate", eventListDTO.getFromDate());
					map.put("toDate", eventListDTO.getToDate());
					map.put("shippingMethods", eventListDTO.getShippingMethods());
					map.put("currentYear", eventListDTO.getCurrentYear());
					map.put("brokerId", brokerId);
				}
			}else{
				map.put("msg", eventListDTO.getError().getDescription());
				map.put("status", eventListDTO.getStatus());
			}
			
			/*if(brokerId > 0){
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByBroker(null,null,fromDateStr,toDateStr,null,brokerId,filter,null,true);
				eventCount = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsCountByBroker(null,null,fromDateStr,toDateStr,null,brokerId,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null,null,fromDateStr,toDateStr,null,filter,null);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null,null,fromDateStr,toDateStr,null,filter);
			}
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			map.put("status", "Available");
			map.put("events", JsonWrapperUtil.getEventArray(events));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(eventCount, null));
			map.put("ticketPagingInfo", PaginationUtil.getDummyPaginationParameter(0));
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("shippingMethods", shippingMethods);
			map.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));*/
			
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		return "page-manage-events";
	}
	
	
	@RequestMapping({"/GetEvents"})
	public String getEvents(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		try{
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			String brokerId= Util.getBrokerId(session) != null ? String.valueOf(Util.getBrokerId(session)) : "";
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("searchType", searchType);
			map.put("searchValue", searchValue);
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			map.put("brokerId", brokerId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EventListDTO eventListDTO = gson.fromJson(((JsonObject)jsonObject.get("eventListDTO")), EventListDTO.class);
			
			if(eventListDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("events", eventListDTO.getEvents());
					model.addAttribute("pagingInfo", eventListDTO.getPaginationDTO());
					model.addAttribute("msg", eventListDTO.getMessage());
					model.addAttribute("status", eventListDTO.getStatus());
				}else{
					model.addAttribute("status", "Available");
					model.addAttribute("events", gson.toJson(eventListDTO.getEvents()));
					model.addAttribute("pagingInfo", gson.toJson(eventListDTO.getPaginationDTO()));
					model.addAttribute("ticketPagingInfo", gson.toJson(eventListDTO.getTicketPagingInfo()));
					model.addAttribute("fromDate", eventListDTO.getFromDate());
					model.addAttribute("toDate", eventListDTO.getToDate());
					model.addAttribute("shippingMethods", eventListDTO.getShippingMethods());
					model.addAttribute("currentYear", eventListDTO.getCurrentYear());
				}
			}else{
				model.addAttribute("msg", eventListDTO.getError().getDescription());
				model.addAttribute("status", eventListDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-manage-events";
	}
	
	@RequestMapping({"/GetZoneMapTickets"})
	public String getZoneMapWithTickets(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		try{
			String eventId = request.getParameter("eventId");
			
			if(eventId!=null && !eventId.isEmpty()){
				Map<String, String> requestMap = Util.getParameterMap(request);
				requestMap.put("eventId", eventId);
				
				String data = Util.getObject(requestMap,Constants.BASE_URL+Constants.GET_EVENT_DETAILS);
				Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy HH:mm:ss").create();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				TicketList ticketList = gson.fromJson(((JsonObject)jsonObject.get("ticketList")), TicketList.class);
				
				int count=0;
				if(ticketList!=null){
					for(TicketGroupQty qty : ticketList.getTicketGroupQtyList()){
						count += qty.getCategoryTicketGroups().size();
					}
				}
				map.put("ticketListJson", jsonObject);
				map.put("ticketList", ticketList);
				map.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "page-event-zone-map-tickets";
	}
	
	/*@RequestMapping({"/GetEventsForCustomer"})
	public void getEventsForCustomer(HttpServletRequest request, HttpServletResponse response){
		try{
			JSONObject returnObject = new JSONObject();
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			Integer eventCount = 0;
			Collection<EventDetails> events = null;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			Date fromDate = new Date();
			cal.add(Calendar.MONTH, 3);
			Date toDate = cal.getTime();
			String fromDateStr = dateFormat.format(fromDate);
			String toDateStr = dateFormat.format(toDate);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:23";
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null,null,fromDateFinal,toDateFinal,null,filter,pageNo);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null,null,fromDateFinal,toDateFinal,null,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilter(null,null,null,null,null,filter,pageNo);
				eventCount = DAORegistry.getQueryManagerDAO().getEventDetailsByFilterCount(null,null,null,null,null,filter);
			}
			returnObject.put("events", JsonWrapperUtil.getEventArray(events));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(eventCount, pageNo));
			IOUtils.write(returnObject.toString(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/EventsExportToExcel"})
	public void getEventsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String headerFilter = request.getParameter("headerFilter");
			Collection<EventDetails> events = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:23";
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilterToExport(searchType,searchValue,fromDateFinal,toDateFinal,null,filter);
			}else{
				events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsByFilterToExport(searchType,searchValue,null,null,null,filter);
			}
			
			if(events!=null && !events.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("events");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Event Id");
				header.createCell(1).setCellValue("Event Name");
				header.createCell(2).setCellValue("Event Date");
				header.createCell(3).setCellValue("Event Time");
				header.createCell(4).setCellValue("Day of Week");
				header.createCell(5).setCellValue("Building");
				header.createCell(6).setCellValue("Venue Id");
				header.createCell(7).setCellValue("No. of Ticket Count");
				header.createCell(8).setCellValue("No. of Ticket Sold Count");
				header.createCell(9).setCellValue("City");
				header.createCell(10).setCellValue("State");
				header.createCell(11).setCellValue("Country");
				header.createCell(12).setCellValue("Grand Child Category Name");
				header.createCell(13).setCellValue("Child Category Name");
				header.createCell(14).setCellValue("Parent Category Name");
				header.createCell(15).setCellValue("Event Creation");
				header.createCell(16).setCellValue("Event Updated");
				header.createCell(17).setCellValue("Notes");
				Integer i=1;
				for(EventDetails event : events){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(event.getEventId());
					row.createCell(1).setCellValue(event.getEventName());
					row.createCell(2).setCellValue(event.getEventDateStr()!=null?event.getEventDateStr():"");
					row.createCell(3).setCellValue(event.getEventTimeStr()!=null?event.getEventTimeStr():"");
					row.createCell(4).setCellValue(event.getDayOfWeek());
					row.createCell(5).setCellValue(event.getBuilding()!=null?event.getBuilding():"");
					row.createCell(6).setCellValue(event.getVenueId()!=null?event.getVenueId():0);
					row.createCell(7).setCellValue(event.getNoOfTixCount()!=null?event.getNoOfTixCount():0);
					row.createCell(8).setCellValue(event.getNoOfTixSoldCount()!=null?event.getNoOfTixSoldCount():0);
					row.createCell(9).setCellValue(event.getCity()!=null?event.getCity():"");
					row.createCell(10).setCellValue(event.getState()!=null?event.getState():"");
					row.createCell(11).setCellValue(event.getCountry()!=null?event.getCountry():"");
					row.createCell(12).setCellValue(event.getGrandChildCategoryName()!=null?event.getGrandChildCategoryName():"");
					row.createCell(13).setCellValue(event.getChildCategoryName()!=null?event.getChildCategoryName():"");
					row.createCell(14).setCellValue(event.getParentCategoryName()!=null?event.getParentCategoryName():"");
					row.createCell(15).setCellValue(event.getEventCreationStr()!=null?event.getEventCreationStr():"");
					row.createCell(16).setCellValue(event.getEventUpdatedStr()!=null?event.getEventUpdatedStr():"");
					row.createCell(17).setCellValue(event.getNotes()!=null?event.getNotes():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=events.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/GetHoldTicketsForEvent"})
	public String getHoldTicketsForEvent(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("eventId", eventId);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", String.valueOf(brokerId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_HOLD_TICKETS_FOR_EVENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			HoldInventoryDTO holdInventoryDTO = gson.fromJson(((JsonObject)jsonObject.get("holdInventoryDTO")), HoldInventoryDTO.class);
			
			if(holdInventoryDTO.getStatus() == 1){
				model.addAttribute("holdTicketsList", holdInventoryDTO.getInventoryHoldList());
				model.addAttribute("holdTicketsPagingInfo", holdInventoryDTO.getPaginationDTO());
				model.addAttribute("msg", holdInventoryDTO.getMessage());
				model.addAttribute("status", holdInventoryDTO.getStatus());
			}else{
				model.addAttribute("msg", holdInventoryDTO.getError().getDescription());
				model.addAttribute("status", holdInventoryDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*@RequestMapping({"/HoldTicketsExportToExcel"})
	public void getHoldTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			List<InventoryHold> inventoryHoldList = null;
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}

			GridHeaderFilters filter = GridHeaderFiltersUtil.getHoldTicketsSearchHeaderFilters(headerFilter);
			inventoryHoldList = DAORegistry.getInventoryHoldDAO().getAllHoldTickets(brokerId, Integer.parseInt(eventId), filter);
			
			if(inventoryHoldList!=null && !inventoryHoldList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("holdTickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Customer Name");
				header.createCell(1).setCellValue("Sale Price");
				header.createCell(2).setCellValue("Created By");
				header.createCell(3).setCellValue("Expiration Date");
				header.createCell(4).setCellValue("Expiration Minutes");
				header.createCell(5).setCellValue("Shipping Method");
				header.createCell(6).setCellValue("Hold Date");
				header.createCell(7).setCellValue("Internal Notes");
				header.createCell(8).setCellValue("External Notes");
				header.createCell(9).setCellValue("Ticket Id's");
				header.createCell(10).setCellValue("Ticket Group Id");
				header.createCell(11).setCellValue("Section");
				header.createCell(12).setCellValue("Row");
				header.createCell(13).setCellValue("Seat Low");
				header.createCell(14).setCellValue("Seat High");
				header.createCell(15).setCellValue("External PO");
				header.createCell(16).setCellValue("Status");
				header.createCell(17).setCellValue("Broker Id");
				Integer i=1;
				for(InventoryHold inventoryHold : inventoryHoldList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(inventoryHold.getCustomerName());
					row.createCell(1).setCellValue(inventoryHold.getSalePrice());
					row.createCell(2).setCellValue(inventoryHold.getcSR());
					row.createCell(3).setCellValue(inventoryHold.getExpirationDateStr()!=null?inventoryHold.getExpirationDateStr():"");
					row.createCell(4).setCellValue(inventoryHold.getExpirationMinutes()!=null?inventoryHold.getExpirationMinutes().toString():"");
					row.createCell(5).setCellValue(inventoryHold.getShippingMethod()!=null?inventoryHold.getShippingMethod():"");
					row.createCell(6).setCellValue(inventoryHold.getHoldDateTimeStr()!=null?inventoryHold.getHoldDateTimeStr():"");
					row.createCell(7).setCellValue(inventoryHold.getInternalNote()!=null?inventoryHold.getInternalNote():"");
					row.createCell(8).setCellValue(inventoryHold.getExternalNote()!=null?inventoryHold.getExternalNote():"");
					row.createCell(9).setCellValue(inventoryHold.getTicketIds()!=null?inventoryHold.getTicketIds():"");
					row.createCell(10).setCellValue(inventoryHold.getTicketGroupId()!=null?inventoryHold.getTicketGroupId():0);
					row.createCell(11).setCellValue(inventoryHold.getSection()!=null?inventoryHold.getSection():"");
					row.createCell(12).setCellValue(inventoryHold.getRow()!=null?inventoryHold.getRow():"");
					row.createCell(13).setCellValue(inventoryHold.getSeatLow()!=null?inventoryHold.getSeatLow():"");
					row.createCell(14).setCellValue(inventoryHold.getSeatHigh()!=null?inventoryHold.getSeatHigh():"");
					row.createCell(15).setCellValue(inventoryHold.getExternalPo()!=null?inventoryHold.getExternalPo():"");
					row.createCell(16).setCellValue(inventoryHold.getStatus()!=null?inventoryHold.getStatus():"");
					row.createCell(17).setCellValue(inventoryHold.getBrokerId()!=null?inventoryHold.getBrokerId():0);
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=holdTickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/GetLockedTicketsForEvent"})
	public String getLockedTicketsForEvent(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("eventId", eventId);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", String.valueOf(brokerId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LOCKED_TICKETS_FOR_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CategoryTicketListDTO categoryTicketListDTO = gson.fromJson(((JsonObject)jsonObject.get("categoryTicketListDTO")), CategoryTicketListDTO.class);
			
			if(categoryTicketListDTO.getStatus() == 1){
				model.addAttribute("lockedTicketsList", categoryTicketListDTO.getCategoryTickets());
				model.addAttribute("lockedTicketsPagingInfo", categoryTicketListDTO.getPaginationDTO());
				model.addAttribute("msg", categoryTicketListDTO.getMessage());
				model.addAttribute("status", categoryTicketListDTO.getStatus());
			}else{
				model.addAttribute("msg", categoryTicketListDTO.getError().getDescription());
				model.addAttribute("status", categoryTicketListDTO.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*@RequestMapping({"/LockedTicketsExportToExcel"})
	public void getLockedTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			List<CategoryTicketGroup> lockedTicketsList = null;
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getLockedTicketsSearchHeaderFilters(headerFilter);
			lockedTicketsList = DAORegistry.getQueryManagerDAO().getLockedTicketDetails(Integer.parseInt(eventId), filter);
			EventDetails eventDtls = DAORegistry.getEventDetailsDAO().getEventById(Integer.parseInt(eventId));
			
			if(lockedTicketsList!=null && !lockedTicketsList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("lockedTickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Section");
				header.createCell(1).setCellValue("Row");
				header.createCell(2).setCellValue("Quantity");
				header.createCell(3).setCellValue("Price");
				header.createCell(4).setCellValue("Tax Amount Percentage");
				header.createCell(5).setCellValue("Section Range");
				header.createCell(6).setCellValue("Row Range");
				header.createCell(7).setCellValue("Shipping Method");
				header.createCell(8).setCellValue("Product Type");
				header.createCell(9).setCellValue("Retail Price");
				header.createCell(10).setCellValue("Wholesale Price");
				header.createCell(11).setCellValue("Face Price");
				header.createCell(12).setCellValue("Cost");
				header.createCell(13).setCellValue("Broadcast");
				header.createCell(14).setCellValue("Internal Notes");
				header.createCell(15).setCellValue("External Notes");
				header.createCell(16).setCellValue("Marketplace Notes");
				header.createCell(17).setCellValue("Max Showing");
				header.createCell(18).setCellValue("Seat Low");
				header.createCell(19).setCellValue("Seat High");
				header.createCell(20).setCellValue("Near Term Display Option");
				header.createCell(21).setCellValue("Event Name");
				header.createCell(22).setCellValue("Event Date");
				header.createCell(23).setCellValue("Event Time");
				header.createCell(24).setCellValue("Venue");
				header.createCell(25).setCellValue("Ticket Type");
				header.createCell(26).setCellValue("Category Ticket Group Id");
				header.createCell(27).setCellValue("IP Address");
				header.createCell(28).setCellValue("Platform");
				header.createCell(29).setCellValue("Creation Date");
				header.createCell(30).setCellValue("Lock Status");				
				
				Integer i=1;
				for(CategoryTicketGroup lockedTicket : lockedTicketsList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(lockedTicket.getSection()!=null?lockedTicket.getSection():"");
					row.createCell(1).setCellValue(lockedTicket.getRow()!=null?lockedTicket.getRow():"");
					row.createCell(2).setCellValue(lockedTicket.getQuantity()!=null?lockedTicket.getQuantity():0);
					row.createCell(3).setCellValue(lockedTicket.getPrice()!=null?lockedTicket.getPrice():0);
					row.createCell(4).setCellValue(lockedTicket.getTaxAmount()!=null?lockedTicket.getTaxAmount():0);
					row.createCell(5).setCellValue(lockedTicket.getSectionRange()!=null?lockedTicket.getSectionRange():"");
					row.createCell(6).setCellValue(lockedTicket.getRowRange()!=null?lockedTicket.getRowRange():"");
					row.createCell(7).setCellValue(lockedTicket.getShippingMethod()!=null?lockedTicket.getShippingMethod():"");
					row.createCell(8).setCellValue(lockedTicket.getProducttype()!=null?lockedTicket.getProducttype().toString():"");
					row.createCell(9).setCellValue(lockedTicket.getPrice()!=null?lockedTicket.getPrice():0);
					row.createCell(10).setCellValue(lockedTicket.getPrice()!=null?lockedTicket.getPrice():0);
					row.createCell(11).setCellValue(0.00);
					row.createCell(12).setCellValue(0.00);
					row.createCell(13).setCellValue(1);
					row.createCell(14).setCellValue("ZTP");
					row.createCell(15).setCellValue(lockedTicket.getExternalNotes()!=null?lockedTicket.getExternalNotes():"");
					row.createCell(16).setCellValue(lockedTicket.getMarketPlaceNotes()!=null?lockedTicket.getMarketPlaceNotes():"");
					row.createCell(17).setCellValue(lockedTicket.getMaxShowing()!=null?lockedTicket.getMaxShowing():0);
					row.createCell(18).setCellValue(lockedTicket.getSeatLow()!=null?lockedTicket.getSeatLow():"");
					row.createCell(19).setCellValue(lockedTicket.getSeatHigh()!=null?lockedTicket.getSeatHigh():"");
					row.createCell(20).setCellValue(lockedTicket.getNearTermDisplayOption()!=null?lockedTicket.getNearTermDisplayOption():"");
					row.createCell(21).setCellValue((eventDtls!=null && eventDtls.getEventName()!=null)?eventDtls.getEventName():"");
					row.createCell(22).setCellValue((eventDtls!=null && eventDtls.getEventDateStr()!=null)?eventDtls.getEventDateStr():"");
					row.createCell(23).setCellValue((eventDtls!=null && eventDtls.getEventTimeStr()!=null)?eventDtls.getEventTimeStr():"");
					row.createCell(24).setCellValue((eventDtls!=null && eventDtls.getBuilding()!=null)?eventDtls.getBuilding():"");
					row.createCell(25).setCellValue("Category");
					row.createCell(26).setCellValue(lockedTicket.getCatgeoryTicketGroupId()!=null?lockedTicket.getCatgeoryTicketGroupId():0);
					row.createCell(27).setCellValue(lockedTicket.getIpAddress()!=null?lockedTicket.getIpAddress():"");
					row.createCell(28).setCellValue(lockedTicket.getPlatform()!=null?lockedTicket.getPlatform():"");
					row.createCell(29).setCellValue(lockedTicket.getCreationDate()!=null?lockedTicket.getCreationDate().toString():"");
					row.createCell(30).setCellValue(lockedTicket.getLockStatus()!=null?lockedTicket.getLockStatus().toString():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=lockedTickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/

	/**
	 * Get Sold Tickets for given event
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/GetSoldTicketsForEvent")
	public String getSoldTicketsForEvent(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String eventId = request.getParameter("eventId");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("eventId", eventId);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", String.valueOf(brokerId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_SOLD_TICKETS_FOR_EVENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CategoryTicketListDTO categoryTicketListDTO = gson.fromJson(((JsonObject)jsonObject.get("categoryTicketListDTO")), CategoryTicketListDTO.class);
			
			if(categoryTicketListDTO.getStatus() == 1){
				model.addAttribute("soldTicketsList", categoryTicketListDTO.getCategoryTickets());
				model.addAttribute("soldTicketsPagingInfo", categoryTicketListDTO.getPaginationDTO());
				model.addAttribute("msg", categoryTicketListDTO.getMessage());
				model.addAttribute("status", categoryTicketListDTO.getStatus());
			}else{
				model.addAttribute("msg", categoryTicketListDTO.getError().getDescription());
				model.addAttribute("status", categoryTicketListDTO.getStatus());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/*@RequestMapping({"/SoldTicketsExportToExcel"})
	public void getSoldTicketsToExport(HttpServletRequest request, HttpServletResponse response){
		try{
			String headerFilter = request.getParameter("headerFilter");
			String eventId = request.getParameter("eventId");
			Collection<CategoryTicketGroup> soldTickets = null;
			List<CategoryTicket> categoryTickets = null;
			JSONObject object = null;
			JSONArray ObjectArray = new JSONArray();
			Ticket ticket = null;
			TicketGroup ticketGroup = null;
			List<TicketGroup> ticketGroupList = new ArrayList<TicketGroup>();
			Set<Integer> ticketGroupIDSet = new HashSet<Integer>();
			Set<Integer> catTicketGroupIDSet = new HashSet<Integer>();
			
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}

			GridHeaderFilters filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			soldTickets = DAORegistry.getCategoryTicketGroupDAO().getSoldTicketsSearchFilterByEventId(Integer.parseInt(eventId), brokerId, filter);

			if(soldTickets!=null && !soldTickets.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("soldTickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Section");
				header.createCell(2).setCellValue("Row");
				header.createCell(3).setCellValue("Seat Low");
				header.createCell(4).setCellValue("Seat High");
				header.createCell(5).setCellValue("Quantity");
				header.createCell(6).setCellValue("Retail Price");
				header.createCell(7).setCellValue("Wholesale Price");
				header.createCell(8).setCellValue("Face Price");
				header.createCell(9).setCellValue("Price");
				header.createCell(10).setCellValue("Cost");
				header.createCell(11).setCellValue("Shipping Method");
				header.createCell(12).setCellValue("Near Term Display Option");
				header.createCell(13).setCellValue("Broadcast");
				header.createCell(14).setCellValue("Section Range");
				header.createCell(15).setCellValue("Row Range");
				header.createCell(16).setCellValue("Internal Notes");
				header.createCell(17).setCellValue("External Notes");
				header.createCell(18).setCellValue("Marketplace Notes");
				Integer i=1;
				for(CategoryTicketGroup catTicketGroup: soldTickets){

					Row row = sheet.createRow(i);
					
					categoryTickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(catTicketGroup.getId());
					for(CategoryTicket catTicket: categoryTickets){
						
						if(catTicket.getTicketId() != null && catTicket.getTicketId() > 0){
							ticket = DAORegistry.getTicketDAO().get(catTicket.getTicketId());
							ticketGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							if(ticketGroup != null){
								Integer ticGroupId = ticketGroup.getId();
								if(ticGroupId != null){
									if(ticketGroupIDSet.add(ticGroupId)){								
										ticketGroupList.add(ticketGroup);
										
										row.createCell(0).setCellValue(catTicketGroup.getInvoiceId()!=null?catTicketGroup.getInvoiceId():0);
										if(ticketGroup!=null){
											row.createCell(1).setCellValue(ticketGroup.getSection()!=null?ticketGroup.getSection():"");
											row.createCell(2).setCellValue(ticketGroup.getRow()!=null?ticketGroup.getRow():"");
											row.createCell(3).setCellValue(ticketGroup.getSeatLow()!=null?ticketGroup.getSeatLow():"");
											row.createCell(4).setCellValue(ticketGroup.getSeatHigh()!=null?ticketGroup.getSeatHigh():"");							
										}
										row.createCell(5).setCellValue(catTicketGroup.getQuantity()!=null?catTicketGroup.getQuantity():0);
										row.createCell(6).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
										row.createCell(7).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
										row.createCell(8).setCellValue(0.00);
										row.createCell(9).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
										row.createCell(10).setCellValue(0.00);
										row.createCell(11).setCellValue(catTicketGroup.getShippingMethod()!=null?catTicketGroup.getShippingMethod():"");
										row.createCell(12).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
										row.createCell(13).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
										row.createCell(14).setCellValue(catTicketGroup.getSectionRange()!=null?catTicketGroup.getSectionRange():"");
										row.createCell(15).setCellValue(catTicketGroup.getRowRange()!=null?catTicketGroup.getRowRange():"");
										row.createCell(16).setCellValue(catTicketGroup.getInternalNotes()!=null?catTicketGroup.getInternalNotes():"");
										row.createCell(17).setCellValue(catTicketGroup.getExternalNotes()!=null?catTicketGroup.getExternalNotes():"");
										row.createCell(18).setCellValue(catTicketGroup.getMarketPlaceNotes()!=null?catTicketGroup.getMarketPlaceNotes():"");
										//object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, ticketGroup);
										//ObjectArray.put(object);
									}
								}
							}
						}else{
							Integer catTicGroupId = catTicketGroup.getId();
							if(catTicGroupId != null){
								if(catTicketGroupIDSet.add(catTicGroupId)){
									
									row.createCell(0).setCellValue(catTicketGroup.getInvoiceId()!=null?catTicketGroup.getInvoiceId():0);
									row.createCell(1).setCellValue(catTicketGroup.getSection()!=null?catTicketGroup.getSection():"");
									row.createCell(2).setCellValue(catTicketGroup.getRow()!=null?catTicketGroup.getRow():"");
									row.createCell(3).setCellValue(catTicketGroup.getSeatLow()!=null?catTicketGroup.getSeatLow():"");
									row.createCell(4).setCellValue(catTicketGroup.getSeatHigh()!=null?catTicketGroup.getSeatHigh():"");
									row.createCell(5).setCellValue(catTicketGroup.getQuantity()!=null?catTicketGroup.getQuantity():0);
									row.createCell(6).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
									row.createCell(7).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
									row.createCell(8).setCellValue(0.00);
									row.createCell(9).setCellValue(catTicketGroup.getPrice()!=null?catTicketGroup.getPrice():0);
									row.createCell(10).setCellValue(0.00);
									row.createCell(11).setCellValue(catTicketGroup.getShippingMethod()!=null?catTicketGroup.getShippingMethod():"");
									row.createCell(12).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
									row.createCell(13).setCellValue(catTicketGroup.getNearTermDisplayOption()!=null?catTicketGroup.getNearTermDisplayOption():"");
									row.createCell(14).setCellValue(catTicketGroup.getSectionRange()!=null?catTicketGroup.getSectionRange():"");
									row.createCell(15).setCellValue(catTicketGroup.getRowRange()!=null?catTicketGroup.getRowRange():"");
									row.createCell(16).setCellValue(catTicketGroup.getInternalNotes()!=null?catTicketGroup.getInternalNotes():"");
									row.createCell(17).setCellValue(catTicketGroup.getExternalNotes()!=null?catTicketGroup.getExternalNotes():"");
									row.createCell(18).setCellValue(catTicketGroup.getMarketPlaceNotes()!=null?catTicketGroup.getMarketPlaceNotes():"");
									//object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, null);
									//ObjectArray.put(object);
								}
							}
						}
					}
					
					i++;			
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=soldTickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/GetEventDetails")
	public String getEventDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){

		try{
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("eventSearchValue");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("headerFilter", headerFilter);
			map.put("pageNo", pageNo);
			map.put("searchType", searchType);
			map.put("eventSearchValue", searchValue);
			map.put("brokerId", String.valueOf(brokerId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_EVENT_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EventListDTO eventListDTO = gson.fromJson(((JsonObject)jsonObject.get("eventListDTO")), EventListDTO.class);
			
			if(eventListDTO.getStatus() == 1){
				model.addAttribute("events", eventListDTO.getEvents());
				model.addAttribute("eventPagingInfo", eventListDTO.getPaginationDTO());
				model.addAttribute("msg", eventListDTO.getMessage());
				model.addAttribute("status", eventListDTO.getStatus());
			}else{
				model.addAttribute("msg", eventListDTO.getError().getDescription());
				model.addAttribute("status", eventListDTO.getStatus());
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	
	/**
	 * Get Sold Tickets for given event
	 * @param request
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/GetSoldTickets")
	public String getSoldTickets(HttpServletRequest request, HttpServletResponse response,ModelMap map){
		Collection<CategoryTicketGroup> soldTickets = null;
		List<CategoryTicket> categoryTickets = null;
		JSONObject object = null;
		JSONArray ObjectArray = new JSONArray();
		Ticket ticket = null;
		TicketGroup ticketGroup = null;
		List<TicketGroup> ticketGroupList = new ArrayList<TicketGroup>();
		EventDetails eventDetails =null;
		Set<Integer> ticketGroupIDSet = new HashSet<Integer>();
		Set<Integer> catTicketGroupIDSet = new HashSet<Integer>();
		try {
			String eventId = request.getParameter("eventId");
			if(eventId != null && !eventId.isEmpty()){
				eventDetails = DAORegistry.getEventDetailsDAO().get(Integer.parseInt(eventId));
				soldTickets = DAORegistry.getCategoryTicketGroupDAO().getSoldTicketsByEventId(Integer.parseInt(eventId));
				
				for(CategoryTicketGroup catTicketGroup: soldTickets){
					categoryTickets = DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(catTicketGroup.getId());
					for(CategoryTicket catTicket: categoryTickets){
						if(catTicket.getTicketId() != null && catTicket.getTicketId() > 0){
							ticket = DAORegistry.getTicketDAO().get(catTicket.getTicketId());
							ticketGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							if(ticketGroup != null){
								Integer ticGroupId = ticketGroup.getId();
								if(ticGroupId != null){
									if(ticketGroupIDSet.add(ticGroupId)){								
										ticketGroupList.add(ticketGroup);
										object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, ticketGroup);
										ObjectArray.put(object);
									}
								}
							}
						}else{
							Integer catTicGroupId = catTicketGroup.getId();
							if(catTicGroupId != null){
								if(catTicketGroupIDSet.add(catTicGroupId)){
									object = JsonWrapperUtil.getSoldTicketDetailsArray(catTicketGroup, null);
									ObjectArray.put(object);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("soldTickets", ObjectArray);
		map.put("eventDetails", eventDetails);
		map.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(soldTickets!=null?soldTickets.size():0));
		return "page-event-sold-tickets";
	}*/
	
	
	 @RequestMapping({"/EditHoldTickets"})
	 public String editHoldTickets(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try{
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("brokerId", String.valueOf(brokerId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_EDIT_HOLD_TICKETS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			HoldInventoryDTO holdInventoryDTO = gson.fromJson(((JsonObject)jsonObject.get("holdInventoryDTO")), HoldInventoryDTO.class);
			
			if(holdInventoryDTO.getStatus() == 1){
				model.addAttribute("holdTicketsList", holdInventoryDTO.getInventoryHoldList());
				model.addAttribute("pagingInfo", holdInventoryDTO.getPaginationDTO());
				model.addAttribute("msg", holdInventoryDTO.getMessage());
				model.addAttribute("status", holdInventoryDTO.getStatus());
			}else{
				model.addAttribute("msg", holdInventoryDTO.getError().getDescription());
				model.addAttribute("status", holdInventoryDTO.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	 
	@RequestMapping({"/GetHoldTickets"})
	public String getHoldTickets(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try{
			String headerFilter = request.getParameter("headerFilter");
			String pageNo = request.getParameter("pageNo");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("brokerId", String.valueOf(brokerId));
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_HOLD_TICKETS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			HoldInventoryDTO holdInventoryDTO = gson.fromJson(((JsonObject)jsonObject.get("holdInventoryDTO")), HoldInventoryDTO.class);
			
			if(holdInventoryDTO.getStatus() == 1){
				model.addAttribute("holdTicketsList", holdInventoryDTO.getInventoryHoldList());
				model.addAttribute("pagingInfo", holdInventoryDTO.getPaginationDTO());
				model.addAttribute("msg", holdInventoryDTO.getMessage());
				model.addAttribute("status", holdInventoryDTO.getStatus());
			}else{
				model.addAttribute("msg", holdInventoryDTO.getError().getDescription());
				model.addAttribute("status", holdInventoryDTO.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
 
	/**
	 * Get Artist Details
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/GetArtist")
	public void getArtist(HttpServletRequest request, HttpServletResponse response){		
		JSONObject returnObject = new JSONObject();
		try{
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);						
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("searchType", searchType);
			map.put("searchValue", searchValue);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_ARTIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ArtistDTO artistDTO = gson.fromJson(((JsonObject)jsonObject.get("artistDTO")), ArtistDTO.class);
			
			if(artistDTO.getStatus() == 1){				
				returnObject.put("artistList", JsonWrapperUtil.getArtistArray(artistDTO.getArtists()));
				returnObject.put("artistPagingInfo", gson.toJson(artistDTO.getPaginationDTO()));
				//returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(artistDTO.getPaginationDTO().getTotalRows(), pageNo));
				returnObject.put("msg", artistDTO.getMessage());
				returnObject.put("status", artistDTO.getStatus());				
			}else{
				returnObject.put("msg", artistDTO.getError().getDescription());
				returnObject.put("status", artistDTO.getStatus());
			}
			
			/*GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			Collection<EventDetails> artists = DAORegistry.getEventDetailsDAO().getAllActiveArtistsByFilter(filter,pageNo);
			Integer count = DAORegistry.getEventDetailsDAO().getAllActiveArtistsTotalCount(filter);
			returnObject.put("artistList", JsonWrapperUtil.getArtistArray(artists));
			returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));*/
			
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*@RequestMapping({"/ManageArtistExportToExcel"})
	public void manageArtistToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			Collection<EventDetails> artistList = DAORegistry.getEventDetailsDAO().getAllActiveArtistsByFilterToExport(filter);
			
			if(artistList!=null && !artistList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Manage_Artist");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Artist Id");
				header.createCell(1).setCellValue("Artist Name");
				header.createCell(2).setCellValue("Grand Child Category");
				header.createCell(3).setCellValue("Child Category");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Visible/In Visible");
				Integer i=1;
				for(EventDetails artist : artistList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(artist.getArtistId());
					row.createCell(1).setCellValue(artist.getArtistName());
					row.createCell(2).setCellValue(artist.getGrandChildCategoryName()!=null?artist.getGrandChildCategoryName():"");
					row.createCell(3).setCellValue(artist.getChildCategoryName()!=null?artist.getChildCategoryName():"");
					row.createCell(4).setCellValue(artist.getParentCategoryName()!=null?artist.getParentCategoryName():"");
					row.createCell(5).setCellValue((artist.getDisplayOnSearch()!=null&&artist.getDisplayOnSearch()==true)?"Yes":"No");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Manage_Artist.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/**
	 * Get Artist Details
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetGrandChildCategory")
	public void getGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			
			if(searchType != null && searchType.trim().length() > 0 &&
					searchValue != null && searchValue.trim().length() > 0) {
				Collection<EventDetails> events = DAORegistry.getEventDetailsDAO().getAllActiveGrandChildCategoryByFilter(searchType,searchValue);
				System.out.println("=====" +events.size());
				JSONObject jObj = null;
				
				for (EventDetails eventDtls : events) {
					jObj = new JSONObject();
					jObj.put("grandChildCategoryId", eventDtls.getGrandChildCategoryId());
					jObj.put("grandChildCategory", eventDtls.getGrandChildCategoryName());
					jObj.put("childCategory", eventDtls.getChildCategoryName());
					jObj.put("parentCategory", eventDtls.getParentCategoryName());
					jsonArr.put(jObj);
				}
			}
			//return jsonArr;
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());;
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Artist Details
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetVenueDetails")
	public void getVenueDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			
			if(searchType != null && searchType.trim().length() > 0 &&
					searchValue != null && searchValue.trim().length() > 0) {
				//Collection<EventDetails> events = DAORegistry.getEventDetailsDAO().getAllActiveVenueByFilter(searchType,searchValue);
				Collection<Venue> venueList = DAORegistry.getQueryManagerDAO().getAllVenueDetails(searchType,searchValue);
				JSONObject jObj = null;
				
				for (Venue venue : venueList) {
					jObj = new JSONObject();
					jObj.put("id", venue.getId());
					jObj.put("building", venue.getBuilding());
					jObj.put("city", venue.getCity());
					jObj.put("state", venue.getState());
					jObj.put("country", venue.getCountry());
					jObj.put("postalCode", venue.getPostalCode());
					jObj.put("notes", venue.getNotes());
					jObj.put("catTicketCount", venue.getCatTicketCount());
					jsonArr.put(jObj);
				}
			}
			response.setHeader("Content-type","application/json");
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());;
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/getCategoryTicketGroups")
	public String getCategoryTicketGroups(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
//		Collection<CategoryTicketGroup> categoryTicketGroups = null;
//		Collection<TicketGroup> ticketGroups = null;
//		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
//		JSONObject returnObject = new JSONObject();
		try{
			String eventId = request.getParameter("eventId");
			String status = request.getParameter("status");
			String sectionSearch = request.getParameter("sectionSearch");
			String rowSearch = request.getParameter("rowSearch");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String seatSearch = request.getParameter("seatSearch");
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("brokerId", String.valueOf(brokerId));
			if(pageNo == null || pageNo.isEmpty()){
				pageNo = "0";
			}
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("eventId", eventId);
			map.put("status", status);
			map.put("sectionSearch", sectionSearch);
			map.put("rowSearch", rowSearch);
			map.put("seatSearch", seatSearch);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CATEGORY_TICKETS_GROUPS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EventTicketDetailsDTO eventTicketDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("eventTicketDetailsDTO")), EventTicketDetailsDTO.class);
			
			if(eventTicketDetailsDTO.getStatus() == 1){
				model.addAttribute("tickets", eventTicketDetailsDTO.getCategoryAndLongTickets());
				model.addAttribute("pagingInfo", eventTicketDetailsDTO.getPaginationDTO());
				model.addAttribute("msg", eventTicketDetailsDTO.getMessage());
				model.addAttribute("status", eventTicketDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", eventTicketDetailsDTO.getError().getDescription());
				model.addAttribute("status", eventTicketDetailsDTO.getStatus());
			}			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	
	/*@RequestMapping(value = "/TicketsExportToExcel")
	public void getCategoryTicketGroupsToExport(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		String section = request.getParameter("sectionSearch");
		String rows = request.getParameter("rowSearch");
		String seat = request.getParameter("seatSearch");
		Collection<CategoryTicketGroup> categoryTicketGroups = null;
		Collection<TicketGroup> ticketGroups = null;
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		try{
			Integer eventId = Integer.parseInt(request.getParameter("eventId"));
			String headerFilter = request.getParameter("headerFilter");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getTicketSearchHeaderFilters(headerFilter);
			
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			EventDetails eventDtls = DAORegistry.getEventDetailsDAO().getEventById(eventId);
			categoryTicketGroups = DAORegistry.getCategoryTicketGroupDAO().getAllActiveCategoryTicketsbyEventId(brokerId,eventId,filter);
			ticketGroups = DAORegistry.getQueryManagerDAO().getUnmappedTicketGroups(brokerId,eventId,filter);
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			
			if((categoryTicketGroups!=null && !categoryTicketGroups.isEmpty()) || (ticketGroups != null && !ticketGroups.isEmpty())){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("tickets");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Id");
				header.createCell(1).setCellValue("Event Id");
				header.createCell(2).setCellValue("Section");
				header.createCell(3).setCellValue("Row");
				header.createCell(4).setCellValue("Quantity");
				header.createCell(5).setCellValue("Price");
				header.createCell(6).setCellValue("Tax Amount Percentage");
				header.createCell(7).setCellValue("Section Range");
				header.createCell(8).setCellValue("Row Range");
				header.createCell(9).setCellValue("Shipping Method");
				header.createCell(10).setCellValue("Product Type");
				header.createCell(11).setCellValue("Retail Price");
				header.createCell(12).setCellValue("Wholesale Price");
				header.createCell(13).setCellValue("Face Price");
				header.createCell(14).setCellValue("Cost");
				header.createCell(15).setCellValue("Broadcast");
				header.createCell(16).setCellValue("Internal Notes");
				header.createCell(17).setCellValue("External Notes");
				header.createCell(18).setCellValue("Market Place Notes");
				header.createCell(19).setCellValue("Max Showing");
				header.createCell(20).setCellValue("Seat Low");
				header.createCell(21).setCellValue("Seat High");
				header.createCell(22).setCellValue("Near Term Display Option");
				header.createCell(23).setCellValue("Event Name");
				header.createCell(24).setCellValue("Event Date");
				header.createCell(25).setCellValue("Event Time");
				header.createCell(26).setCellValue("Venue");
				header.createCell(27).setCellValue("Ticket Type");
				header.createCell(28).setCellValue("Broker Id");
				Integer i=1;
				if(ticketGroups != null && !ticketGroups.isEmpty()){
					for(TicketGroup tx : ticketGroups){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(tx.getId());
						row.createCell(1).setCellValue(tx.getEventId());
						row.createCell(2).setCellValue(tx.getSection()!=null?tx.getSection():"");
						row.createCell(3).setCellValue(tx.getRow()!=null?tx.getRow():"");
						row.createCell(4).setCellValue(tx.getQuantity());
						row.createCell(5).setCellValue(tx.getPrice());
						row.createCell(6).setCellValue("0.0");
						row.createCell(7).setCellValue("");
						row.createCell(8).setCellValue("");
						row.createCell(9).setCellValue(shippingMap.get(tx.getShippingMethodId())!=null?shippingMap.get(tx.getShippingMethodId()):"");
						row.createCell(10).setCellValue("REWARDTHEFAN");
						row.createCell(11).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(12).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(13).setCellValue(0.0);
						row.createCell(14).setCellValue(0.0);
						if(tx.getBroadcast()!=null && tx.getBroadcast()){
							row.createCell(15).setCellValue(1);
						}else{
							row.createCell(15).setCellValue(0);
						}
						row.createCell(16).setCellValue("ZTP");
						row.createCell(17).setCellValue("");
						row.createCell(18).setCellValue("");
						row.createCell(19).setCellValue("");
						row.createCell(20).setCellValue(tx.getSeatLow()!=null?tx.getSeatLow():"");
						row.createCell(21).setCellValue(tx.getSeatHigh()!=null?tx.getSeatHigh():"");
						row.createCell(22).setCellValue("");
						row.createCell(23).setCellValue(eventDtls.getEventName()!=null?eventDtls.getEventName():"");
						row.createCell(24).setCellValue(eventDtls.getEventDateStr()!=null?eventDtls.getEventDateStr():"");
						row.createCell(25).setCellValue(eventDtls.getEventTimeStr()!=null?eventDtls.getEventTimeStr():"");
						row.createCell(26).setCellValue(eventDtls.getBuilding()!=null?eventDtls.getBuilding():"");
						row.createCell(27).setCellValue("Real");
						row.createCell(28).setCellValue(tx.getBrokerId());
						i++;
					}
				}
				if(categoryTicketGroups != null && !categoryTicketGroups.isEmpty()){
					for(CategoryTicketGroup tx : categoryTicketGroups){
						Row row = sheet.createRow(i);
						row.createCell(0).setCellValue(tx.getId());
						row.createCell(1).setCellValue(tx.getEventId());
						row.createCell(2).setCellValue(tx.getSection()!=null?tx.getSection():"");
						row.createCell(3).setCellValue(tx.getRow()!=null?tx.getRow():"");
						row.createCell(4).setCellValue(tx.getQuantity());
						row.createCell(5).setCellValue(tx.getPrice());
						row.createCell(6).setCellValue(tx.getTaxAmount()!=null?tx.getTaxAmount():0);
						row.createCell(7).setCellValue(tx.getSectionRange()!=null?tx.getSectionRange():"");
						row.createCell(8).setCellValue(tx.getRowRange()!=null?tx.getRowRange():"");
						row.createCell(9).setCellValue(tx.getShippingMethod()!=null?tx.getShippingMethod():"");
						row.createCell(10).setCellValue(tx.getProducttype().toString());
						row.createCell(11).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(12).setCellValue(tx.getPrice()!=null?tx.getPrice():0);
						row.createCell(13).setCellValue(0.0);
						row.createCell(14).setCellValue(0.0);
						if(tx.getBroadcast()!=null && tx.getBroadcast()){
							row.createCell(15).setCellValue(1);
						}else{
							row.createCell(15).setCellValue(0);
						}
						row.createCell(16).setCellValue("ZTP");
						row.createCell(17).setCellValue(tx.getExternalNotes()!=null?tx.getExternalNotes():"");
						row.createCell(18).setCellValue(tx.getMarketPlaceNotes()!=null?tx.getMarketPlaceNotes():"");
						row.createCell(19).setCellValue(tx.getMaxShowing()!=null?tx.getMaxShowing():0);
						row.createCell(20).setCellValue(tx.getSeatLow()!=null?tx.getSeatLow():"");
						row.createCell(21).setCellValue(tx.getSeatHigh()!=null?tx.getSeatHigh():"");
						row.createCell(22).setCellValue(tx.getNearTermDisplayOption()!=null?tx.getNearTermDisplayOption():"");
						row.createCell(23).setCellValue(eventDtls.getEventName()!=null?eventDtls.getEventName():"");
						row.createCell(24).setCellValue(eventDtls.getEventDateStr()!=null?eventDtls.getEventDateStr():"");
						row.createCell(25).setCellValue(eventDtls.getEventTimeStr()!=null?eventDtls.getEventTimeStr():"");
						row.createCell(26).setCellValue(eventDtls.getBuilding()!=null?eventDtls.getBuilding():"");
						row.createCell(27).setCellValue("Category");
						row.createCell(28).setCellValue(tx.getBrokerId());
						i++;
					}
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=tickets.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getCategoryTicketGroupById")
	public void getCategoryTicketGroup(HttpServletRequest request, HttpServletResponse response){
		JSONObject returnObject = new JSONObject();
		try{			
			String ticketId = request.getParameter("ticketId");
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("ticketId", ticketId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CATEGORY_TICKET_GROUP_ID);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CategoryTicketGroupDTO categoryTicketGroupDTO = gson.fromJson(((JsonObject)jsonObject.get("categoryTicketGroupDTO")), CategoryTicketGroupDTO.class);
			
			if(categoryTicketGroupDTO.getStatus() == 1){
				returnObject.put("categoryTicket", JsonWrapperUtil.getCategoryTicketGroupJsonObject(categoryTicketGroupDTO.getCategoryTicketGroup()));				
				returnObject.put("status", categoryTicketGroupDTO.getStatus());				
			}else{
				returnObject.put("msg", categoryTicketGroupDTO.getError().getDescription());
				returnObject.put("status", categoryTicketGroupDTO.getStatus());
			}
							
			//return jsonArr;
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Save shipping address
	 * @param addShippingAddress
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/AddCategoryTickets",method=RequestMethod.POST)
	@ResponseBody
	public String saveShippingAddress(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		String returnMessage = "";
		try{
			String action = request.getParameter("action");
			String id = request.getParameter("id");
			String username = SecurityContextHolder.getContext().getAuthentication().getName();				
			String eventId = request.getParameter("eventId");
			String section = request.getParameter("section");
			String row = request.getParameter("row");
			String sectionRange = request.getParameter("sectionRange");
			String rowRange = request.getParameter("rowRange");
			String seatLow = request.getParameter("seatLow");
			String seatHigh = request.getParameter("seatHigh");
			String quantity = request.getParameter("quantity");
			String price = request.getParameter("price");
			String shippingMethod = request.getParameter("shippingMethod");
			String nearTermDisplayOption = request.getParameter("nearTermDisplayOption");
			String internalNotes = request.getParameter("internalNotes");
			String externalNotes = request.getParameter("externalNotes");
			String marketPlaceNotes = request.getParameter("marketPlaceNotes");
			String broadcast = request.getParameter("broadcast");
			String taxAmount = request.getParameter("taxAmount");
			Integer brokerId = Util.getBrokerId(session);			
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("id", id);
			map.put("action", action);
			map.put("userName", username);
			map.put("brokerId", String.valueOf(brokerId));
			map.put("taxAmount", taxAmount);
			map.put("eventId", eventId);
			map.put("section", section);
			map.put("row", row);
			map.put("sectionRange", sectionRange);
			map.put("rowRange", rowRange);
			map.put("seatLow", seatLow);
			map.put("seatHigh", seatHigh);
			map.put("quantity", quantity);
			map.put("price", price);
			map.put("shippingMethod", shippingMethod);
			map.put("nearTermDisplayOption", nearTermDisplayOption);
			map.put("internalNotes", internalNotes);
			map.put("externalNotes", externalNotes);
			map.put("marketPlaceNotes", marketPlaceNotes);
			map.put("broadcast", broadcast);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ADD_CATEGORY_TICKETS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return returnMessage;
	}
	
	
	/*@RequestMapping(value = "/UploadTickets")
	public void uploadTickets(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		String msg = "";
		try {
			String eventIdStr = request.getParameter("eventId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			File f = null;
			DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
			File ticketFile = new File(com.rtw.tracker.utils.Constants.Real_ticket_Path.toString());
	        fileItemFactory.setRepository(ticketFile);
	        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
	        List items = uploadHandler.parseRequest(request);
	        Iterator iterator = items.iterator();
	        List<CategoryTicketGroup> ticketGroups = new ArrayList<CategoryTicketGroup>();
	        String ext ="";
	        boolean isQtyMissing = false;
	        boolean isSectionMissing = false;
	        boolean isCostMissing = false;
	        boolean isIDMissing = false;
	        ShippingMethod sm = DAORegistry.getShippingMethodDAO().getShippingMethodByName("E-Ticket");
	        Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				msg = "Broker is not identified.";
			}
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			
			if(msg.isEmpty()){
				MultipartFile file = multipartRequest.getFile("ticketFile");
				if(file!=null){
					ext = FilenameUtils.getExtension(file.getOriginalFilename());
					f = new File(com.rtw.tracker.utils.Constants.Real_ticket_Path+eventIdStr+"_"+brokerId+"."+ext);
					f.createNewFile();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
			        FileCopyUtils.copy(file.getInputStream(), stream);
					stream.close();
				}else{
					msg="File not found";
				}
				Iterator iterator1 = items.iterator();
				while(iterator1.hasNext()){
		        	FileItem item = (FileItem)iterator1.next();
		        	if(!item.isFormField()){
		        		ext = FilenameUtils.getExtension(file.getOriginalFilename());
		        		f = new File(com.rtw.tracker.utils.Constants.Real_ticket_Path+eventIdStr+"_"+brokerId+"."+ext);
		        		item.write(f);
		        	}
		        }
				
			}
	       
			CategoryTicketGroup tg = null;
			if(msg.isEmpty() && f!=null && f.length() > 0){
				if(ext.equalsIgnoreCase("csv")){
					Scanner scanner = new Scanner(f);
			        while (scanner.hasNext()) {
			            String line = scanner.nextLine();
			            String arr[] = null;
			            System.out.println("====================================");
			            if(line!=null && !line.isEmpty()){
			            	arr = line.split(",");
			            	
			            }
			            if(arr[0]==null || !arr[0].equalsIgnoreCase("Y")){
			            	continue;
			            }
			            if(arr.length> 0){
			            	 String eventName = null;
		            		 String eventDate = null;
		            		 String eventTime = null;
		            		 String venueName = null;
			            	 tg = new CategoryTicketGroup();
			            	 for(int i=0;i<arr.length;i++){
		            			switch(i){
			            		case 0:
			                		System.out.println(arr[i]);
			                		break;
			                	case 1:
			                		eventName = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 2:
			                		venueName = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 3:
			                		eventDate = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 4:
			                		eventTime = arr[i];
			                		System.out.println(arr[i]);
			                		break;
			                	case 5:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			try {
			                				tg.setQuantity(Integer.parseInt(arr[i]));
										} catch (Exception e) {
											e.printStackTrace();
											isQtyMissing =true;
										}
			                		}else{
			                			isQtyMissing =true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 6:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			tg.setSection(arr[i]);
			                		}else{
			                			isSectionMissing = true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 7:
			                		tg.setRow(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 8:
			                		tg.setInternalNotes(arr[i]);
			                		System.out.println(arr[i]);
			                		break;
			                	case 9:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			try {
			                				tg.setPrice(Double.parseDouble(arr[i]));
			                				tg.setCost(Double.parseDouble(arr[i]));
			                				tg.setRetailPrice(Double.parseDouble(arr[i]));
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isCostMissing =true;
										}
			                		}else{
			                			isCostMissing =true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 10:
			                		cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isIDMissing =true;
										}
			                		}else{
			                			isIDMissing =true;
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 11:
			                		if(arr[i]!=null && !arr[i].isEmpty()){
			                			tg.setTicketOnhandStatus("Yes");
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 12:
			                		if(arr[i]!=null && arr[i].equalsIgnoreCase("Y")){
			                			tg.setShippingMethodId(sm.getId());
			                		}
			                		System.out.println(arr[i]);
			                		break;
			                	case 13:
			                		System.out.println(arr[i]);
			                		break;
			                	
		            			}
			            	}
			            	 if(eventName == null || eventName.isEmpty()){
		            				continue;
		            			}
		            			if(venueName == null || venueName.isEmpty()){
		            				continue;
		            			}
		            			if(eventDate == null || eventDate.isEmpty()){
		            				continue;
		            			}
		            			if(eventTime==null || eventTime.isEmpty()){
		            				continue;
		            			}
		            			eventTime = "01/01/1970 "+eventTime;
		            			Integer eventId = DAORegistry.getEventDetailsDAO().getActiveEventIdByEventDateTimeVenue(eventName,venueName,eventDate,eventTime);
		            			if(eventId==null){
		            				continue;
		            			}
		            			List<String> list=TMATDAORegistry.getCategoryMappingDAO().getCategoryByEventIdAndZone(eventId,tg.getSection());
		            			if(list.isEmpty() && list.size() < 4){
		            				continue;
		            			}
		            			tg.setSectionRange(list.get(2));
		            			tg.setRowRange(list.get(3));
		            			tg.setBroadcast(true);
				                tg.setBrokerId(brokerId);
				                tg.setPlatform(ApplicationPlatform.TICK_TRACKER.toString());
		        				tg.setCreatedBy(userName);
		        				tg.setCreatedDate(new Date());
		        				tg.setEventId(eventId);
		        				tg.setLastUpdatedDate(new Date());
		        				tg.setProducttype(ProductType.REWARDTHEFAN);
		        				tg.setStatus(TicketStatus.ACTIVE);
		        				tg.setTaxAmount(0.000);
		        				ticketGroups.add(tg);
			            }
			            
			        }
			        scanner.close();
				}else if(ext.equalsIgnoreCase("xls") || ext.equalsIgnoreCase("xlsx")){
					FileInputStream stream = new FileInputStream(f);
					Workbook wb = new XSSFWorkbook(stream);
					Sheet firstSheet = wb.getSheetAt(0);
			        Iterator<Row> it = firstSheet.iterator();
			        
			        while (it.hasNext()) {
			            Row nextRow = it.next();
			            if(nextRow.getRowNum()==0){
			            	continue;
			            }
			            System.out.println("=============================================");
			            tg = new CategoryTicketGroup();
			            Iterator<Cell> cellIterator = nextRow.cellIterator();
			            while (cellIterator.hasNext()) {
			                Cell cell = cellIterator.next();
			                switch (cell.getColumnIndex()){
			                	case 0:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 1:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 2:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 3:
			                		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			                			System.out.println(cell.getDateCellValue());
			                		}
			                		break;
			                	case 4:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 5:
			                		cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				tg.setQuantity(Integer.parseInt(cell.getStringCellValue()));
										} catch (Exception e) {
											e.printStackTrace();
											isQtyMissing =true;
										}
			                		}else{
			                			isQtyMissing =true;
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 6:
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			tg.setSection(cell.getStringCellValue());
			                		}else{
			                			isSectionMissing = true;
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 7:
			                		tg.setRow(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 8:
			                		tg.setSeatLow(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 9:
			                		tg.setSeatHigh(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 8:
			                		tg.setInternalNotes(cell.getStringCellValue());
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 9:
			                		cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				tg.setPrice(Double.parseDouble(cell.getStringCellValue()));
			                				tg.setCost(Double.parseDouble(cell.getStringCellValue()));
			                				tg.setRetailPrice(Double.parseDouble(cell.getStringCellValue()));
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isCostMissing =true;
										}
			                		}else{
			                			isCostMissing =true;
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 10:
			                		cell.setCellType(Cell.CELL_TYPE_STRING);
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().isEmpty()){
			                			try {
			                				
										} catch (Exception e) {
											e.printStackTrace();
											isIDMissing =true;
										}
			                		}else{
			                			isIDMissing =true;
			                		}
			                		System.out.println(cell.getNumericCellValue());
			                		break;
			                	case 11:
			                		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC && cell.getDateCellValue()!=null){
			                			tg.setTicketOnhandStatus("Yes");
			                		}
			                		System.out.println(cell.getDateCellValue());
			                		break;
			                	case 12:
			                		if(cell.getStringCellValue()!=null && !cell.getStringCellValue().equalsIgnoreCase("Y")){
			                			tg.setShippingMethodId(sm.getId());
			                		}
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	case 13:
			                		System.out.println(cell.getStringCellValue());
			                		break;
			                	
			                }
			                tg.setBroadcast(true);
			                tg.setBrokerId(brokerId);
			                tg.setPlatform(ApplicationPlatform.TICK_TRACKER.toString());
            				tg.setCreatedBy(userName);
            				tg.setCreatedDate(new Date());
            				tg.setEventId(Integer.parseInt(eventIdStr));
            				tg.setLastUpdatedDate(new Date());
            				tg.setProducttype(ProductType.REWARDTHEFAN);
            				tg.setTaxAmount(0.000);
            				tg.setStatus(TicketStatus.ACTIVE);
            				ticketGroups.add(tg);
			            }
			        }
				}
				
				if(!ticketGroups.isEmpty()){
		        	if(isCostMissing){
		        		 msg = "Cost is missing in some of the tickets.";
		        	}else if(isQtyMissing){
		        		 msg = "Quantity is missing in some of the tickets.";
		        	}else if(isSectionMissing){
		        		 msg = "Section is missing in some of the tickets.";
		        	}else{
		        		List<CategoryTicketGroup> finalInsertList = new ArrayList<CategoryTicketGroup>();
		        		List<CategoryTicketGroup> finalUpdateList = new ArrayList<CategoryTicketGroup>();
		        		try {
		        			for(CategoryTicketGroup ticGrp : ticketGroups){
		        				CategoryTicketGroup dbTg = DAORegistry.getCategoryTicketGroupDAO().getDuplicateCategoryTicketGroupForBroker(ticGrp.getSection(),ticGrp.getQuantity(),ticGrp.getEventId(),brokerId);
								if(dbTg == null){
									finalInsertList.add(ticGrp);
								}else{
									dbTg.setPrice(ticGrp.getPrice());
									dbTg.setCost(ticGrp.getCost());
									dbTg.setRetailPrice(ticGrp.getRetailPrice());
									dbTg.setLastUpdatedDate(new Date());
									finalUpdateList.add(dbTg);
								}
		        			}
							DAORegistry.getCategoryTicketGroupDAO().saveAll(finalInsertList);
							DAORegistry.getCategoryTicketGroupDAO().updateAll(finalUpdateList);
						} catch (Exception e) {
							msg = "Something went wrong, Please try again.";
							e.printStackTrace();
						}
						if(msg.isEmpty()){
							List<CategoryTicket> insertList = new ArrayList<CategoryTicket>();
							List<CategoryTicket> updateList = new ArrayList<CategoryTicket>();
							CategoryTicket tic = null;
							for(CategoryTicketGroup ticGrp : finalInsertList){
								for(int i=1;i<=ticGrp.getQuantity();i++){
									tic = new CategoryTicket();
									tic.setCategoryTicketGroupId(ticGrp.getId());
									tic.setActualPrice(tg.getPrice());
									tic.setIsProcessed(true);
									tic.setPosition(i);
									tic.setUserName(tg.getCreatedBy());
									insertList.add(tic);
								}
							}
							for(CategoryTicketGroup tGroup : finalUpdateList){
								List<CategoryTicket> tics =DAORegistry.getCategoryTicketDAO().getCategoryTicketByCategoryTicketGroupId(tGroup.getId());
								for(CategoryTicket t : tics){
									t.setActualPrice(tGroup.getPrice());
								}
								updateList.addAll(tics);
							}
							DAORegistry.getCategoryTicketDAO().saveAll(insertList);
							DAORegistry.getCategoryTicketDAO().updateAll(updateList);
							msg = finalInsertList.size()+" Tickets are Created and "+finalUpdateList.size()+" are Updated successfully.";
						}
						
		        	}
		        }
			}else{
				msg = "No Data Found in File.";
			}
			IOUtils.write(msg.getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}*/
	
	@RequestMapping(value = "/UpdateTicketDetails")
	public String updateTicketDetails(HttpServletRequest request, HttpServletResponse response,Model model){

		try{
			String ticketIds = request.getParameter("ticketIds");
			String broadcast = request.getParameter("broadcast");
			String eventId = request.getParameter("eventId");
			HttpSession session = request.getSession();
			String brokerId = Util.getBrokerId(session) != null ? String.valueOf(Util.getBrokerId(session)) : "";
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("ticketIds", ticketIds);
			map.put("broadcast", broadcast);
			map.put("eventId", eventId);
			map.put("brokerId", brokerId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPLOAD_TICKET_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				if(StringUtils.isNotEmpty(ticketIds)){
					model.addAttribute("ticketMessage", genericResponseDTO.getMessage());
				}else{
					model.addAttribute("eventMessage", genericResponseDTO.getMessage());
				}
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				if(StringUtils.isNotEmpty(ticketIds)){
					model.addAttribute("ticketMessage", genericResponseDTO.getError().getDescription());
				}else{
					model.addAttribute("eventMessage", genericResponseDTO.getError().getDescription());
				}
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/CheckLockedTickets")
	public String checkLockedTickets(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		
		try{
			String ticketId = request.getParameter("ticketId");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("ticketId", ticketId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_LOCKED_TICKETS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping({"/RtfCatsEvents"})
	public String loadRtfCatsEventsPage(HttpServletRequest request, HttpServletResponse response, ModelMap map,HttpSession session){
		try{
//			Integer eventCount = 0;
//				Collection<EventDetails> events = null;
			map.put("msg", "");
			map.put("status", 1);
			
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		return "page-rtf-cats-events";
	}
	
	@RequestMapping("/AutoCompleteArtistAndVenueGChildTMAT")
	public void getAutoCompleteArtistAndVenueGChildTMAT(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_ARTIST_VENUE_GCHILD_TMAT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistVenueDTO autoCompleteArtistVenueDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistVenueDTO")), AutoCompleteArtistVenueDTO.class);
						
			if(autoCompleteArtistVenueDTO.getStatus() == 1){
				writer.write(autoCompleteArtistVenueDTO.getArtistVenue());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
