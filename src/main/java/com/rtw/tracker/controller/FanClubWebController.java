package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.firebase.FirestoreUtil;
import com.rtw.tracker.pojos.CommonRespInfo;
import com.rtw.tracker.pojos.FanClubDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
public class FanClubWebController {

	
	
	@RequestMapping(value = "/ManageFanClubs")
	public String manageFanClubs(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUBS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("clubs", respDTO.getClubs());
					model.addAttribute("categories", respDTO.getCategories());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("memberPagingInfo", respDTO.getDummyPagination());
				}else{
					model.addAttribute("clubs", gson.toJson(respDTO.getClubs()));
					model.addAttribute("categories", respDTO.getCategories());
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("memberPagingInfo",gson.toJson(respDTO.getDummyPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("clubs", respDTO.getClubs());
				model.addAttribute("categories", respDTO.getCategories());
				model.addAttribute("pagingInfo", respDTO.getPagination());
				model.addAttribute("memberPagingInfo", respDTO.getDummyPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-fan-club";
	}
	
	
	
	@RequestMapping(value = "/ManageFanClubPosts")
	public String manageFanClubPosts(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String fanClubIdStr = request.getParameter("fanClubId");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("fanClubId", fanClubIdStr);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUB_POSTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("posts", respDTO.getPosts());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("posts", gson.toJson(respDTO.getPosts()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("posts", respDTO.getPosts());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		//return "page-fan-club-posts";
		return "";
	}
	
	
	@RequestMapping(value = "/ManageFanClubEvents")
	public String manageFanClubEvents(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String fanClubIdStr = request.getParameter("fanClubId");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("fanClubId", fanClubIdStr);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUB_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("events", respDTO.getEvents());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("events", gson.toJson(respDTO.getEvents()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("events", respDTO.getEvents());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		//return "page-fan-club-events";
		return "";
	}
	
	@RequestMapping(value = "/ManageFanClubVideos")
	public String manageFanClubVideos(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String fanClubIdStr = request.getParameter("fanClubId");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("fanClubId", fanClubIdStr);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUB_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", respDTO.getVideos());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("videos", gson.toJson(respDTO.getVideos()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("videos", respDTO.getVideos());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		//return "page-fan-club-videos";
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/GetFanClubMembers")
	public String getFanClubMembers(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String fanClubId = request.getParameter("fanClubId");
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("fanClubId", fanClubId);
			map.put("status", status);
			map.put("headerFilter", headerFilter);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUB_MEMBERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("members", respDTO.getMembers());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("members", gson.toJson(respDTO.getMembers()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("members", respDTO.getMembers());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/UpdateFanClubs")
	public String updateFanClubs(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String fanClubId = request.getParameter("fanClubId");
			String title = request.getParameter("title");
			String description = request.getParameter("description");
			String category = request.getParameter("category");
			String imageFile = request.getParameter("imageFile");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("title", title);
			map.put("description", description);
			map.put("category", category);
			map.put("imageFile", imageFile);
			map.put("userName", userName);
			map.put("fanClubId", fanClubId);

			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_UPDATE_FANCLUBS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(!action.equalsIgnoreCase("EDIT")){
					FirestoreUtil.updatePopularChannelDetails();
				}
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("clubs", respDTO.getClubs());
					model.addAttribute("club", respDTO.getClub());
					model.addAttribute("msg", respDTO.getMessage());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("clubs", gson.toJson(respDTO.getClubs()));
					model.addAttribute("club", gson.toJson(respDTO.getClub()));
					model.addAttribute("msg",  gson.toJson(respDTO.getMessage()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("clubs", respDTO.getClubs());
				model.addAttribute("club", respDTO.getClub());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/UpdateFanClubPosts")
	public String updateFanClubPosts(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String postId = request.getParameter("postId");
			String fanClubIdStr = request.getParameter("fanClubId");
			String postText = request.getParameter("postText");
			String imageFile = request.getParameter("imageFile");
			String videoFile = request.getParameter("videoFile");
			String thumbnailImage = request.getParameter("thumbnailImage");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("fanClubId", fanClubIdStr);
			map.put("postId", postId);
			map.put("postText", postText);
			map.put("imageFile", imageFile);
			map.put("videoFile", videoFile);
			map.put("thumbnailImage", thumbnailImage);
			map.put("userName", userName);
			

			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_UPDATE_FANCLUB_POSTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("posts", respDTO.getPosts());
					model.addAttribute("post", respDTO.getPost());
					model.addAttribute("msg", respDTO.getMessage());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("posts", gson.toJson(respDTO.getPosts()));
					model.addAttribute("post", gson.toJson(respDTO.getPost()));
					model.addAttribute("msg",  gson.toJson(respDTO.getMessage()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("posts", respDTO.getPosts());
				model.addAttribute("post", respDTO.getPost());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/GetFanClubPostComments")
	public String getFanClubPostComments(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String action  = request.getParameter("action");
			String status = request.getParameter("status");
			String commentTypeId = request.getParameter("commentTypeId");
			String commentType = request.getParameter("commentType");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("action", action);
			map.put("commentTypeId", commentTypeId);
			map.put("commentType", commentType);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUB_POST_COMMENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("postComments", respDTO.getPostComments());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("postComments", gson.toJson(respDTO.getPostComments()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("postComments", respDTO.getPostComments());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateFanClubPostComments")
	public String updateFanClubPostComments(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String commentIdStr = request.getParameter("commentId");
			String commentTypeId = request.getParameter("commentTypeId");
			String commentType = request.getParameter("commentType");
			/*String postText = request.getParameter("postText");
			String imageFile = request.getParameter("imageFile");
			String videoFile = request.getParameter("videoFile");
			String thumbnailImage = request.getParameter("thumbnailImage");*/
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("commentId", commentIdStr);
			map.put("commentTypeId", commentTypeId);
			map.put("commentType", commentType);
			/*map.put("postText", postText);
			map.put("imageFile", imageFile);
			map.put("videoFile", videoFile);
			map.put("thumbnailImage", thumbnailImage);*/
			map.put("userName", userName);
			

			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_UPDATE_FANCLUB_POST_COMMENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("postComments", respDTO.getPostComments());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("msg", respDTO.getMessage());
				}else{
					model.addAttribute("postComments", gson.toJson(respDTO.getPostComments()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("msg", respDTO.getMessage());
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("postComments", respDTO.getPostComments());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/BlockCustomerComment")
	public String BlockCustomerComment(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String customerIdStr = request.getParameter("customerId");
			String blockSource = request.getParameter("source");
			String sourceIdStr = request.getParameter("sourceId");
			
			/*String postText = request.getParameter("postText");
			String imageFile = request.getParameter("imageFile");
			String videoFile = request.getParameter("videoFile");
			String thumbnailImage = request.getParameter("thumbnailImage");*/
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("source", blockSource);
			map.put("sourceId", sourceIdStr);
			/*map.put("postText", postText);
			map.put("imageFile", imageFile);
			map.put("videoFile", videoFile);
			map.put("thumbnailImage", thumbnailImage);*/
			map.put("userName", userName);
			

			String data = Util.getObjectFromApi(map, request, Constants.BASE_URL + Constants.BLOCK_CUSTOMER_COMMENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			CommonRespInfo respDTO = gson.fromJson(((JsonObject) jsonObject.get("commonRespInfo")),CommonRespInfo.class);
			
			if(respDTO.getSts() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("msg", respDTO.getMsg());
				}else{
					model.addAttribute("msg", respDTO.getMsg());
				}
			}else{
				model.addAttribute("msg", respDTO.getErr().getDesc());
			}
			model.addAttribute("status",respDTO.getSts());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateFanClubEvents")
	public String updateFanClubEvents(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String fanClubIdStr = request.getParameter("fanClubId");
			String eventId = request.getParameter("eventId");
			String eventName = request.getParameter("eventName");
			String venue = request.getParameter("venue");
			String eventDate = request.getParameter("eventDate");
			String eventHour = request.getParameter("eventHour");
			String eventMinute = request.getParameter("eventMinute");
			String imageFile = request.getParameter("imageFile");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("fanClubId", fanClubIdStr);
			map.put("eventId", eventId);
			map.put("eventName", eventName);
			map.put("venue", venue);
			map.put("eventDate", eventDate);
			map.put("eventHour", eventHour);
			map.put("eventMinute", eventMinute);
			map.put("imageFile", imageFile);
			map.put("userName", userName);
			

			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_UPDATE_FANCLUB_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("events", respDTO.getEvents());
					model.addAttribute("event", respDTO.getEvent());
					model.addAttribute("msg", respDTO.getMessage());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("events", gson.toJson(respDTO.getEvents()));
					model.addAttribute("event", gson.toJson(respDTO.getEvent()));
					model.addAttribute("msg",  gson.toJson(respDTO.getMessage()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("events", respDTO.getEvents());
				model.addAttribute("event", respDTO.getEvent());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/UpdateFanClubVideos")
	public String updateFanClubVideos(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String fanClubIdStr = request.getParameter("fanClubId");
			String videoId = request.getParameter("videoId");
			String title = request.getParameter("title");
			String description = request.getParameter("description");
			String category = request.getParameter("category");
			String videoFile = request.getParameter("videoFile");
			String thumbnailImage = request.getParameter("thumbnailImage");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("fanClubId", fanClubIdStr);
			map.put("videoId", videoId);
			map.put("title", title);
			map.put("description", description);
			map.put("category", category);
			map.put("videoFile", videoFile);
			map.put("thumbnailImage", thumbnailImage);
			map.put("userName", userName);

			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_UPDATE_FANCLUB_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", respDTO.getVideos());
					model.addAttribute("video", respDTO.getVideo());
					model.addAttribute("msg", respDTO.getMessage());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("videos", gson.toJson(respDTO.getVideos()));
					model.addAttribute("video", gson.toJson(respDTO.getVideo()));
					model.addAttribute("msg",  gson.toJson(respDTO.getMessage()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("videos", respDTO.getVideos());
				model.addAttribute("video", respDTO.getVideo());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/RemoveFanClubMember")
	public String removeFanClubMember(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String fanClubIdStr = request.getParameter("fanClubId");
			String memberIds = request.getParameter("memberIds");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("fanClubId", fanClubIdStr);
			map.put("memberIds", memberIds);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_REMOVE_FANCLUB_MEMBER);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("members", respDTO.getMembers());
					model.addAttribute("msg", respDTO.getMessage());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("members", gson.toJson(respDTO.getMembers()));
					model.addAttribute("msg",  gson.toJson(respDTO.getMessage()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("members", respDTO.getMembers());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/ManageFanClubAbuse")
	public String manageFanClubAbuse(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String type = request.getParameter("type");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("type", type);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_FANCLUB_ABUSE);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("abuseData", respDTO.getAbuseData());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("abuseData", gson.toJson(respDTO.getAbuseData()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("abuseData", respDTO.getAbuseData());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
			model.addAttribute("type",type);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-fanclub-abuse";
	}
	
	
	
	@RequestMapping(value = "/UpdateFanClubAbuseData")
	public String updateFanClubAbuseData(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String type  = request.getParameter("type");
			String action  = request.getParameter("action");
			String dataId  = request.getParameter("dataId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("type", type);
			map.put("dataId", dataId);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_UPDATE_FANCLUB_ABUSE_DATA);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(type.equalsIgnoreCase("FANCLUB")){
					FirestoreUtil.updatePopularChannelDetails();
				}
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("abuseData", respDTO.getAbuseData());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("msg", respDTO.getMessage());
				}else{
					model.addAttribute("abuseData", gson.toJson(respDTO.getAbuseData()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("msg",gson.toJson(respDTO.getMessage()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("abuseData", respDTO.getAbuseData());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
}
