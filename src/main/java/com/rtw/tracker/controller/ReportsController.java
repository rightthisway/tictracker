package com.rtw.tracker.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.pojos.BotDetail;
import com.rtw.tracker.pojos.CustomerStatistics;
import com.rtw.tracker.pojos.CustomerStats;
import com.rtw.tracker.pojos.EventStatisticsReportDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.ExcelUtil;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
@RequestMapping({"/Reports"})
public class ReportsController {

	@RequestMapping({"/Home"})
	public String loadReportsHomePage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			Calendar cal = Calendar.getInstance();
			String todaysDate = Util.formatDateToMonthDateYear(cal.getTime());
			String fromDate = "08/10/2018";
			
			map.addAttribute("fromDate",fromDate);
			map.addAttribute("toDate",todaysDate);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-reports-home";
	}
	
	@RequestMapping({"/EventsStatisticsReport"})
	public String downloadEventsStatisticsReport(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			Map<String, String> map = Util.getParameterMap(request);
						
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EVENTS_STATISTICS_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EventStatisticsReportDTO eventStatisticsReportDTO = gson.fromJson(((JsonObject)jsonObject.get("eventStatisticsReportDTO")), EventStatisticsReportDTO.class);
			
			if(eventStatisticsReportDTO.getStatus() == 1){				
				model.addAttribute("msg", eventStatisticsReportDTO.getMessage() != null ? eventStatisticsReportDTO.getMessage() : "");
				model.addAttribute("status", eventStatisticsReportDTO.getStatus());
				model.addAttribute("eventsStatisticsCsv", eventStatisticsReportDTO.getEventsStatisticsDTO());
				model.addAttribute("eventsStatisticsReportHeader", eventStatisticsReportDTO.getEventsStatisticsHeaderDTO());
			}else{
				model.addAttribute("msg", eventStatisticsReportDTO.getError() != null ? eventStatisticsReportDTO.getError().getDescription() : "");
				model.addAttribute("status", eventStatisticsReportDTO.getStatus());
			}
			
			/*List<String> eventsStatisticsReportHeader = new ArrayList<String>();
			eventsStatisticsReportHeader.add("SPORTS");
			eventsStatisticsReportHeader.add("CONCERTS");
			eventsStatisticsReportHeader.add("THEATER");
			eventsStatisticsReportHeader.add("OTHER");
			eventsStatisticsReportHeader.add("TOTAL");
			List<Object[]> eventsStatisticsReportTemp = DAORegistry.getEventDAO().getEventsStatistics();
			List<Integer> eventsStatisticsReport = new ArrayList<Integer>();
			int totalCounts = 0;
			for(Object[] eventCounts : eventsStatisticsReportTemp){
				if(eventCounts[1] != null){
					eventsStatisticsReport.add(Integer.parseInt(String.valueOf(eventCounts[0])));
					totalCounts += Integer.parseInt(String.valueOf(eventCounts[0])); 
				}
			}
			eventsStatisticsReport.add(totalCounts);
			map.addAttribute("eventsStatisticsCsv", eventsStatisticsReport);
			map.addAttribute("eventsStatisticsReportHeader", eventsStatisticsReportHeader);*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "page-reports-events-statistics";
	}
	
	/*@RequestMapping({"/DownloadTNDEventSalesReport"})
	public void downloadTNDEventSalesEventDetailReport(HttpServletRequest request, HttpServletResponse response){
		try{
			
			
			SXSSFWorkbook workbook = TNDEventsSalesReportScheduler.getTNDEventSalesReportExcelFile();
			String fileName = "EventLast3DaysSales.xlsx";
			System.out.println(fileName);
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename="+fileName);
			
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/*@RequestMapping({"/DownloadPointsSummeryReport"})
	public void downloadPointsSummaryReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Pending Points");
			headerList.add("Active Points");
			headerList.add("Total Points");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getQueryManagerDAO(), ssSheet);
			List<Object[]> dataList = DAORegistry.getQueryManagerDAO().getRewardPointSummary();
			Row rowhead =   ssSheet.createRow(1);
			Double totalPoints = 0.00;
			for(Object[] dataObj : dataList){
				int i=0;
				for(Object data : dataObj){
					if(data != null){
						totalPoints += Double.parseDouble(data.toString());
						rowhead.createCell(i).setCellValue(Double.parseDouble(data.toString()));
					}else{
						rowhead.createCell(i).setCellValue("");
					}
					i++;
				}
				rowhead.createCell(2).setCellValue(totalPoints);
			}
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.POINTS_SUMMARY_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadEventsNotInRTFReport"})
	public void downloadEventsNotInRTFReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Event Id");
			headerList.add("Event Updated DateAndTime");
			headerList.add("Artist Name");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");			
			headerList.add("Venue Name");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Parent Category");
			headerList.add("Child Category");
			headerList.add("GrandChild Category");
			headerList.add("Venue Category Group Name");
			headerList.add("Venue Map");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			CreationHelper createHelper = workbook.getCreationHelper();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getEventsNotInRTF(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getEventsNotInRTF();
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				CellStyle cellStyle = workbook.createCellStyle();				
				CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
				cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
				cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					Boolean venueMap = Boolean.parseBoolean(dataObj[14].toString());
					if(venueMap != null && venueMap){
						rowhead.createCell((int) j).setCellValue("Yes");
					}else{
						rowhead.createCell((int) j).setCellValue("No");
					}					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.EVENTS_IN_TMAT_BUT_NOT_IN_RTF+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}*/
	
	/*@RequestMapping({"/DownloadRTFUnsoldInventory"})
	public void downloadRTFUnsoldInventory(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			StringBuilder eventsCsv = new StringBuilder();
			eventsCsv.append("EventID,EventName,EventDate,EventTime,VenueName,VenueCity,Section,Row,Seat,Original_Ticket_Count,Remaining_Ticket_Count,Retail_Price,Face_Price,Cost,WholeSale_Price,InternalNotes,OnHand,InstandDownload,StockType,PurchaseOrderId,ClientBrokerCompanyName\n");
			eventsCsv.append(Util.getCommaSeperatedStringFromCollection(DAORegistry.getEventDAO().getRTFUnsoldInventory()));
			response.setHeader("Content-Type", "text/csv");
			response.setHeader("Content-disposition", "attachment; filename="+Constants.RTF_UNSOLD_INVENTORY_REPORT+"."+Constants.CSV_EXTENSION);
			response.getOutputStream().write(eventsCsv.toString().getBytes());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFUnFilledReport"})
	public void downloadRTFUnFilledReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Id");
			headerList.add("Invoice Created Date");		
			headerList.add("Event Category");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Section Range");
			headerList.add("Row Range");			
			headerList.add("Invoice Ticket Qty");
			headerList.add("Sold Ticket Qty");					
			headerList.add("Invoice Total");			
			headerList.add("Ticket Price");
			headerList.add("Sold Price");			
			headerList.add("Actual Sold Price");			
			headerList.add("Total Sold Price");
			headerList.add("Fees");
			headerList.add("Net Sold Price");	
			headerList.add("Client Name");
			headerList.add("Client Email");
			headerList.add("Client Phone");
			headerList.add("Client BillingAddress");
			headerList.add("Client ShippingAddress");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			CreationHelper createHelper = workbook.getCreationHelper();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getRTFUnFilledReport(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFUnFilledReport();

			Double sumInvoiceTktQuantity = 0.0, sumSoldTktQuantity = 0.0, sumInvoiceTotal = 0.0, sumTicketPrice = 0.0;
			Double sumSoldPrice = 0.0, sumActualSoldPrice = 0.0;
			Double sumTotalSoldPrice = 0.0, sumFees = 0.0, sumNetSoldPrice =0.0;
			Double invoiceTktQuantity = 0.0, soldTktQuantity = 0.0, invoiceTotal = 0.0, ticketPrice = 0.0;			
			Double soldPrice = 0.0, actualSoldPrice = 0.0;
			Double totalSoldPrice = 0.0, fees = 0.0, netSoldPrice =0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				CellStyle cellStyle = workbook.createCellStyle();				
				CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
				cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
				cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[21].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[22].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[23].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[24].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=" + Constants.RTF_UNFILLED_REPORT + "."+Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFFilledReport"})
	public void downloadRTFFilledReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Id");
			headerList.add("Invoice Created Date");
			headerList.add("Customer Order Id");
			headerList.add("Customer Order Created Date");
			headerList.add("PO Id");
			headerList.add("PO Created Date");			
			headerList.add("Event Category");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Section Range");
			headerList.add("Row Range");			
			headerList.add("Invoice Ticket Qty");
			headerList.add("Sold Ticket Qty");					
			headerList.add("Invoice Total");			
			headerList.add("Ticket Price");
			headerList.add("Sold Price");			
			headerList.add("Actual Sold Price");
			headerList.add("Ticket Cost");
			headerList.add("Total Ticket Cost");			
			headerList.add("Total Sold Price");
			headerList.add("Fees");
			headerList.add("Net Sold Price");
			headerList.add("ActualProfitOrLoss");
			headerList.add("Gross Margin");			
			headerList.add("Client Name");
			headerList.add("Client Email");
			headerList.add("Client Phone");
			headerList.add("Client BillingAddress");
			headerList.add("Client ShippingAddress");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getRTFFilledReport(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFFilledReport();
			
			Double sumInvoiceTktQuantity = 0.0, sumSoldTktQuantity = 0.0, sumInvoiceTotal = 0.0, sumTicketPrice = 0.0;
			Double sumSoldPrice = 0.0, sumActualSoldPrice = 0.0, sumTicketCost = 0.0, sumTotalTicketCost = 0.0;
			Double sumTotalSoldPrice = 0.0, sumFees = 0.0, sumNetSoldPrice =0.0, sumProfitOrLoss = 0.0, sumGrossMargin = 0.0;
			Double invoiceTktQuantity = 0.0, soldTktQuantity = 0.0, invoiceTotal = 0.0, ticketPrice = 0.0;			
			Double soldPrice = 0.0, actualSoldPrice = 0.0, ticketCost = 0.0, totalTicketCost = 0.0;
			Double totalSoldPrice = 0.0, fees = 0.0, netSoldPrice =0.0, profitOrLoss = 0.0, grossMargin = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				CellStyle cellStyle = workbook.createCellStyle();				
				CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
				cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
				cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[5].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_FILLED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFZonesNotHavingColorsReport"})
	public String downloadRTFZonesNotHavingColorsReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session,
			@RequestParam(name = "startDate",required = false) String startDate,
			@RequestParam(name = "endDate",required = false) String endDate) throws IOException{
		
			if(!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
				SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy"); 
				DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_ZONES_NOT_HAVING_COLORS+"." + today
							+ ".xlsx");
				
				try{
					Map<Integer,List<VenueCategory>> venueCategoriesMap = new HashMap<Integer, List<VenueCategory>>();
					
					 Collection<com.rtw.tmat.data.Venue> venues = TMATDAORegistry.getVenueDAO().getAllVenuesForUSandCA(dateFormatter.parse(startDate),dateFormatter.parse(endDate));
					 Set<VenueCategory> venueCategoriesList = TMATDAORegistry.getVenueCategoryDAO().getAllVenueCategoriesForUSandCA(venues);
					 
					 for (VenueCategory venueCategory : venueCategoriesList) {
						 Integer venueId = venueCategory.getVenue().getId();
						 List<VenueCategory> tempVenueCats = venueCategoriesMap.get(venueId);
						 if(tempVenueCats == null) {
							 tempVenueCats = new ArrayList<VenueCategory>();
						 }
						 tempVenueCats.add(venueCategory);
						 venueCategoriesMap.put(venueId, tempVenueCats);
					}
					 
					 FileReader fReader = null;
					 BufferedReader reader = null;
					 String svgText = "",str="";
					 List<SVGFileMissingZoneData> missingSvgZonesList = new ArrayList<SVGFileMissingZoneData>();
					 List<SVGFileMissingZoneData> missingCSVZonesList = new ArrayList<SVGFileMissingZoneData>();
					 List<SVGFileMissingZoneData> invalidFormatSVGFiles = new ArrayList<SVGFileMissingZoneData>();
					 
					 int totVenues=venues.size(),processedVenues=0;
					 for (com.rtw.tmat.data.Venue venue : venues) {
						 Integer venueId = venue.getId();
						 
						 processedVenues++;
						 if(processedVenues%100 == 0) {
							 System.out.println(processedVenues+" / "+totVenues+" : SVG and CSV Missing Venue : "+venueId);
						 }
						 
						 List<VenueCategory> venueCategories = venueCategoriesMap.get(venueId);
						 if(venueCategories == null) {
							 continue;
						 }
						 //List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueIdOrderByCategory(venueId);
					
						 for (VenueCategory venueCatObj : venueCategories) {
							 try {
								 String fileName = venueId+"_"+venueCatObj.getCategoryGroup();
								 List<String> missingCsvZones = new ArrayList<String>();
								 List<String> missingSvgZones = new ArrayList<String>();
								
								 
								 Map<String, Boolean> svgZoneMap = new HashMap<String, Boolean>();
								 Map<String, Boolean> csvZoneMap = new HashMap<String, Boolean>();
								 
								 List<String> zoneList = TMATDAORegistry.getQueryManagerDAO().getAllZoneByVenueCategory(venueId);
								 if(zoneList != null) {
									 for (String zone : zoneList) {
										 csvZoneMap.put(zone.toUpperCase(), true);
									}
								 }
								 
								 //str ="";
								 //File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//"+fileName+".txt");
								 
								 Map<String, String> httpParams = new HashMap<String, String>();
								 httpParams.put("configId", "RewardTheFan");
								 httpParams.put("venueId", String.valueOf(venue.getId()));
								 httpParams.put("categoryGroupName", venueCatObj.getCategoryGroup());
								 
								 String httpResponse = HttpUtil.executeHttpGetRequest("api.rewardthefan.com/", "GetSvgTextInformation.json", new HashMap<String, String>(), httpParams);
								 String svgTxtData = null;
								 
								 if(httpResponse != null && !httpResponse.isEmpty()){
									 ObjectMapper mapper = new ObjectMapper();
									 JsonNode actualObj = mapper.readTree(httpResponse);
									 JsonNode jsonNode = actualObj.get("TMATSvgMapDetails");
									 svgTxtData = jsonNode.get("svgText").asText();
									 System.out.println(svgTxtData);
								 }
								 
								 if(svgTxtData != null && !svgTxtData.isEmpty()) {
									fReader = new FileReader(svgTxtFile);
									reader = new BufferedReader(fReader);
									while((str = reader.readLine()) != null) {
										svgText += str;
									}
									try {
										svgZoneMap = getAllZonesFromText(svgTxtData);
									} catch(Exception e) {
										 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
										 zoneData.setVenueCategoryId(venueCatObj.getId());
										 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
										 invalidFormatSVGFiles.add(zoneData);
										 continue;
									}
								 }
								 
								 for (String key : svgZoneMap.keySet()) {
									 Boolean isZonethere = csvZoneMap.remove(key);
									 if(null == isZonethere || !isZonethere){
										 missingCsvZones.add(key);
									 }
								 }
								 for (String key : csvZoneMap.keySet()) {
									 missingSvgZones.add(key);
								 }
								 
								 if(!missingSvgZones.isEmpty()) {
									 for (String zone : missingSvgZones) {
										 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
										 zoneData.setVenueCategoryId(venueCatObj.getId());
										 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
										 zoneData.setZone(zone);
										 missingSvgZonesList.add(zoneData);
										 
									}
								 }
								 if(!missingCsvZones.isEmpty()) {
									 for (String zone : missingCsvZones) {
										 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
										 zoneData.setVenueCategoryId(venueCatObj.getId());
										 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
										 zoneData.setZone(zone);
										 missingCSVZonesList.add(zoneData);
										 
									}
								 }
							 } catch (Exception e) {
								 e.printStackTrace();
							}
						}
					 }
					 SXSSFWorkbook workbook = new SXSSFWorkbook();
					generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),missingCSVZonesList,true);
					generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),missingSvgZonesList,true);
					generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
					 
					workbook.write(out);
				 }catch (Exception e){
					 e.printStackTrace();
				 }
			}else{
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance(); 
				map.addAttribute("startDate", dateFormat.format(cal.getTime()));
				map.addAttribute("endDate", dateFormat.format(cal.getTime()));
			}
		return "page-map-not-having-zones-colors-report";
	}*/
	
	/*@RequestMapping({"/DownloadRTFZonesNotHavingColorsReport"})
	public void downloadRTFZonesNotHavingColorsReport(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat dbDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
				
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_ZONES_NOT_HAVING_COLORS+"." + today
							+ ".xlsx");
		
		String startDateJsp = request.getParameter("startDate");
		String endDateJsp = request.getParameter("endDate");
		String artistIdStr = request.getParameter("artistId");
		
		String startDateStr = null,endDateStr = null;
		String artistName = null;
		Artist artist = null;
		try {
			if(startDateJsp != null && !startDateJsp.equals("")) {
				startDateStr = dbDateFormatter.format(dateFormatter.parse(startDateJsp))+" 00:00:00";
			}
			if(endDateJsp != null && !endDateJsp.equals("")) {
				endDateStr = dbDateFormatter.format(dateFormatter.parse(endDateJsp))+" 23:59:59";
			}
		} catch (Exception e) {
		}
		if(artistIdStr != null && !artistIdStr.equals("")) {
			artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistIdStr));
		}
		if(artist != null) {
			artistName = artist.getName();
		}
				
		try{
			 
			List<Object[]> list = TMATDAORegistry.getQueryManagerDAO().getAllVenueMapZonesWihoutListingsinRTF(artistName,startDateStr,endDateStr);
			 
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet();
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			//CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			//cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
				Row rowhead =   ssSheet.createRow((int)0);
			    rowhead.createCell((int) j).setCellValue("Event Id");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Time");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Artist Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("State");
		 	    j++;
		 	    rowhead.createCell((int) j).setCellValue("Country");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Zone");
		    	j++;
			    
			    
			    if(list != null) {
			    	 int rowvariable = 1;
					 for (Object[] objArr : list) {
						 
							 
						 int column = 0;
						Row row = ssSheet.createRow(rowvariable);
						if(objArr[0]!= null) {//Event Id
							row.createCell((int) column).setCellValue(objArr[0].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[1]!= null) {//Event Name
							row.createCell((int) column).setCellValue(objArr[1].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[2]!= null) {//Event Date
							Cell cell = row.createCell((int) column);
							cell.setCellValue(new Date(objArr[2].toString()));
							cell.setCellStyle(cellStyle);
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[3]!= null) {//Event Time
							row.createCell((int) column).setCellValue(objArr[3].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[4]!= null) {//Artist
							row.createCell((int) column).setCellValue(objArr[4].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[5]!= null) {//venue
							row.createCell((int) column).setCellValue(objArr[5].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[6]!= null) {//City
							row.createCell((int) column).setCellValue(objArr[6].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[7]!= null) {//state
							row.createCell((int) column).setCellValue(objArr[7].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[8]!= null) {//Country
							row.createCell((int) column).setCellValue(objArr[8].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[9]!= null) {//zone
							row.createCell((int) column).setCellValue(objArr[9].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						
						
				        rowvariable++;
					}
			    }
			    
			workbook.write(out);
		 }catch (Exception e){
			e.printStackTrace();
		}
		
		//return "page-map-not-having-zones-colors-report";
	}*/
	
	@RequestMapping({"/LoadRTFZonesNotHavingColorsReport"})
	public String loadRTFZonesNotHavingColorsReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session,
			@RequestParam(name = "startDate",required = false) String startDate,
			@RequestParam(name = "endDate",required = false) String endDate) throws IOException{
		
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance(); 
				//map.addAttribute("startDate", dateFormat.format(cal.getTime()));
				//map.addAttribute("endDate", dateFormat.format(cal.getTime()));
				return "page-map-not-having-zones-colors-report";
	}
	
	/*@RequestMapping({"/DownloadMissingSVGandCSVZones"})
	public void downloadMissingSVGandCSVZones(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
			//if(!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
				SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy"); 
				DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-disposition",
					"attachment; filename=SVG_and_CSV_Missing_Zones." + today
							+ ".xlsx");
				
				try{
					List<SVGFileMissingZoneData> svgZonesnotinCsvList = TMATDAORegistry.getQueryManagerDAO().getAllVenueMapZonesNotInCSV();
					List<SVGFileMissingZoneData> csvZonesnotinSvgList = TMATDAORegistry.getQueryManagerDAO().getAllCsvZonesNotInVenueMap();
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),svgZonesnotinCsvList,true);
					generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),csvZonesnotinSvgList,true);
					//generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
					 
					workbook.write(out);
				 }catch (Exception e){
					 e.printStackTrace();
				 }
//			}else{
//				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//				Calendar cal = Calendar.getInstance(); 
//				map.addAttribute("startDate", dateFormat.format(cal.getTime()));
//				map.addAttribute("endDate", dateFormat.format(cal.getTime()));
//			}
//		return "page-map-not-having-zones-colors-report";
	}*/
	
	/*@RequestMapping({"/DownloadRTFNext15DaysUnsentReport"})
	public void downloadRTFNext15DaysUnsentReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Event Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Qty");
			headerList.add("Invoice Id");
			headerList.add("Invoice Created By");
			headerList.add("Invoice CreatedDate");
			headerList.add("PO Id");
			headerList.add("PO CreatedDate");
			headerList.add("Shipping Type");
			headerList.add("Vendor Company");
			headerList.add("Vendor Email");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getNext15DaysUnsentOrders();
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumQuantity = 0.0, quantity = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(dataObj[2] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[2].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				

				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[10] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[13] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[13].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[16].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[19].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;

				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_NEXT_15_DAYS_UNSENT_ORDERS+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFUnFilledShortsWithPossibleLongInventoryReport"})
	public void downloadRTFUnFilledShortsWithPossibleLongInventoryReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			//headerList.add("Event Id");			
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Artist Name");
			headerList.add("Venue Name");
			headerList.add("");
			headerList.add("Short Invoice Id");
			headerList.add("Short Section");
			headerList.add("Short Row");
			headerList.add("Short Qty");
			headerList.add("Short Sold Price");
			headerList.add("");
			headerList.add("Long PO Id");
			headerList.add("Long Section");
			headerList.add("Long Row");
			headerList.add("Long Qty");
			headerList.add("Long Retail Price");
			headerList.add("Long Wholesale Price");
			headerList.add("Long Face Price");
			headerList.add("Long Cost");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> list = DAORegistry.getEventDAO().getUnFilledShortsWithPossibleLongInventory();
			List<Object[]> finalList = new ArrayList<Object[]>();
			
			Integer eventId = new Integer(0);
			Integer categoryTicketGroupId = new Integer(0);
			Integer ticketGroupId = new Integer(0);
			Object[] finalObj = null;
			for(Object[] obj : list){			
				finalObj = new Object[20];
				if(!eventId.equals((Integer)obj[0])){
					finalObj[0] = obj[1];
					finalObj[1] = obj[2];
					finalObj[2] = obj[3];
					finalObj[3] = obj[4];
					finalObj[4] = obj[5];
					finalObj[5] = "";
				}else{
					finalObj[0] = "";
					finalObj[1] = "";
					finalObj[2] = "";
					finalObj[3] = "";
					finalObj[4] = "";
					finalObj[5] = "";
				}
				if(!categoryTicketGroupId.equals((Integer)obj[6])){
					finalObj[6] = obj[7];
					finalObj[7] = obj[8];
					finalObj[8] = obj[9];
					finalObj[9] = obj[10];
					finalObj[10] = obj[11];
					finalObj[11] = "";
				}else{
					finalObj[6] = "";
					finalObj[7] = "";
					finalObj[8] = "";
					finalObj[9] = "";
					finalObj[10] = "";
					finalObj[11] = "";
				}
				if(!ticketGroupId.equals((Integer)obj[12])){
					finalObj[12] = obj[13];
					finalObj[13] = obj[14];
					finalObj[14] = obj[15];
					finalObj[15] = obj[16];
					finalObj[16] = obj[17];
					finalObj[17] = obj[18];
					finalObj[18] = obj[19];
					finalObj[19] = obj[20];
				}else{					
					finalObj[12] = "";
					finalObj[13] = "";
					finalObj[14] = "";
					finalObj[15] = "";
					finalObj[16] = "";
					finalObj[17] = "";
					finalObj[18] = "";
					finalObj[19] = "";
				}
				eventId = (Integer) obj[0];
				categoryTicketGroupId = (Integer) obj[6];
				ticketGroupId = (Integer) obj[12];
				finalList.add(finalObj);
			}			
			
			//ExcelUtil.generateExcelData(finalList, ssSheet);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			//CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			//cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumShortQty = 0.0, sumShortSoldPrice = 0.0, sumLongQty = 0.0, sumLongRetailPrice = 0.0;
			Double sumLongWholesalePrice = 0.0, sumLongFacePrice = 0.0, sumLongCost = 0.0;
			Double shortQty = 0.0, shortSoldPrice = 0.0, longQty = 0.0, longRetailPrice = 0.0;
			Double longWholesalePrice = 0.0, longFacePrice =0.0, longCost = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : finalList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null && !dataObj[1].toString().isEmpty()){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());					
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null && !dataObj[9].toString().isEmpty()){
					shortQty = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumShortQty = sumShortQty + shortQty;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(shortQty));
				}else{
					sumShortQty = sumShortQty + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[10] != null && !dataObj[10].toString().isEmpty()){
					shortSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumShortSoldPrice = sumShortSoldPrice + shortSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(shortSoldPrice));
				}else{
					sumShortSoldPrice = sumShortSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null && !dataObj[15].toString().isEmpty()){
					longQty = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumLongQty = sumLongQty + longQty;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longQty));
				}else{
					sumLongQty = sumLongQty + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null && !dataObj[16].toString().isEmpty()){
					longRetailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumLongRetailPrice = sumLongRetailPrice + longRetailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longRetailPrice));
				}else{
					sumLongRetailPrice = sumLongRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null && !dataObj[17].toString().isEmpty()){
					longWholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumLongWholesalePrice = sumLongWholesalePrice + longWholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longWholesalePrice));
				}else{
					sumLongWholesalePrice = sumLongWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null && !dataObj[18].toString().isEmpty()){
					longFacePrice = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumLongFacePrice = sumLongFacePrice + longFacePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longFacePrice));
				}else{
					sumLongFacePrice = sumLongFacePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null && !dataObj[19].toString().isEmpty()){
					longCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumLongCost = sumLongCost + longCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(longCost));
				}else{
					sumLongCost = sumLongCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(finalList != null && finalList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumShortQty));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumShortSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongQty));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongFacePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLongCost));
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_UNFILLED_SHORTS_WITH_POSSIBLE_LONG_INVENTORY+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFProfitAndLossReport"})
	public String downloadRTFProfitAndLossReport(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(name = "startDate",required = false) String startDate,
			@RequestParam(name = "endDate",required = false) String endDate){
		try{
			if(!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)){
				List<String> headerList = new ArrayList<String>();
				headerList.add("Event Name");
				headerList.add("Event Date");
				headerList.add("Event Time");
				headerList.add("Venue Name");
				headerList.add("Venue City");
				headerList.add("Venue State");
				headerList.add("Venue Country");
				headerList.add("Section");
				headerList.add("Row");
				headerList.add("Qty");
				headerList.add("On Hand");
				headerList.add("Stock Type");
				headerList.add("Invoice Id");
				headerList.add("Sales Person");
				headerList.add("Sales Date");
				headerList.add("PurchaseOrder Id");
				headerList.add("PO CreatedDate");
				headerList.add("Vendor Company");
				headerList.add("Vendor Email");
				
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				Sheet longTicketsSheet =  workbook.createSheet("RTF Long");
				ExcelUtil.generateExcelHeaderRow(headerList, longTicketsSheet);
				ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), longTicketsSheet);
				
				Sheet categoryTicketsSheet =  workbook.createSheet("RTF Cats");
				ExcelUtil.generateExcelHeaderRow(headerList, categoryTicketsSheet);
				ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), categoryTicketsSheet);
				
				Sheet summarySheet =  workbook.createSheet("Summary");
				ExcelUtil.generateExcelHeaderRow(headerList, summarySheet);
				ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getNext15DaysUnsentOrders(), summarySheet);
				
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename="+Constants.RTF_PROFIT_AND_LOSS_REPORT+"." +Constants.EXCEL_EXTENSION);
				
				workbook.write(response.getOutputStream());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-profit-loss-report";
	}*/
		
	/*public static Map<String, Boolean> getAllZonesFromText(String svgText) throws Exception {
		
		Map<String, Boolean> allZonesMap = new HashMap<String, Boolean>();
		try {			
		 	Pattern pattern = Pattern.compile("<g id=\"zone_letters\" display=\"none\">(.*?)</g>");
			Matcher matcher = pattern.matcher(svgText);
						
			String value = new String();
			
			if(matcher.find()){
				//System.out.println("ZONE :" + dayMatcher.group(1));
				value = matcher.group(1);
			}
			
			String[] strings = value.split("transform=");
			//System.out.println(value);
			for (String text : strings) {
				
				Pattern dayPattern = Pattern.compile("<text id=\"text_(.*?)\" font-weight");
				Matcher dayMatcher = dayPattern.matcher(text);
				
				if(dayMatcher.find()){
					//System.out.println("ZONE :" + dayMatcher.group(1));
					String zone = dayMatcher.group(1);
					allZonesMap.put(zone.toUpperCase(), true);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return allZonesMap;
		 
	 }*/

	/*public void generateMissingSVGandCSVzonesFile(Sheet ssSheet,List<SVGFileMissingZoneData> zoneDataList,boolean showZoneColumn) throws Exception {
	
	try {
		int j = 0;
		Row rowhead =   ssSheet.createRow((int)0);
	    rowhead.createCell((int) j).setCellValue("Venue Id");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Building");
	    j++;
    	rowhead.createCell((int) j).setCellValue("City");
    	j++;
    	rowhead.createCell((int) j).setCellValue("State");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("Country");
    	j++;
    	rowhead.createCell((int) j).setCellValue("Venue Category Id");
    	j++;
	    rowhead.createCell((int) j).setCellValue("Venue Category Group");
	    j++;
	    if(showZoneColumn) {
	    	rowhead.createCell((int) j).setCellValue("Zone");
	    	j++;
	    }
	    
	    if(zoneDataList != null) {
	    	 int rowvariable = 1;
			 for (SVGFileMissingZoneData zoneData : zoneDataList) {
				 	 
				int column = 0;
				Row row = ssSheet.createRow(rowvariable);
				row.createCell((int) column).setCellValue(zoneData.getVenueId().toString());
				column++;
				row.createCell((int) column).setCellValue(zoneData.getBuilding().toString());
				column++;
				
				if(zoneData.getCity() != null) {
					row.createCell((int) column).setCellValue(zoneData.getCity().toString());
			        column++;
				} else {
					row.createCell((int) column).setCellValue("");
			        column++;
				}
				if(zoneData.getState() != null) {
					row.createCell((int) column).setCellValue(zoneData.getState().toString());
			        column++;
				} else {
					row.createCell((int) column).setCellValue("");
			        column++;
				}
				if(zoneData.getCountry() != null) {
					row.createCell((int) column).setCellValue(zoneData.getCountry().toString());
			        column++;
				} else {
					row.createCell((int) column).setCellValue("");
			        column++;
				}
				if(zoneData.getVenueCategoryId() != null) {
					row.createCell((int) column).setCellValue(zoneData.getVenueCategoryId().toString());
			        column++;
				} else {
					row.createCell((int) column).setCellValue("");
			        column++;
				}
				if(zoneData.getCategoryGroupName() != null) {
					row.createCell((int) column).setCellValue(zoneData.getCategoryGroupName().toString());
			        column++;
				} else {
					row.createCell((int) column).setCellValue("");
			        column++;
				}
				if(showZoneColumn) {
					if(zoneData.getZone() != null) {
						row.createCell((int) column).setCellValue(zoneData.getZone().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
				}
		        rowvariable++;
			}
	    }
	    
	} catch( Exception e) {
		e.printStackTrace();
	}
		 
	}*/

	/*@RequestMapping({"/DownloadRTFUnsoldInventoryReport"})
	public void downloadRTFUnsoldInventoryReport(HttpServletRequest request, HttpServletResponse response,
												@RequestParam(name = "startDate",required = false) String startDate,
												@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("PO Id");
			headerList.add("PO Created Date");
			headerList.add("Event Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");			
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");			
			headerList.add("Original Ticket Count");
			headerList.add("Remaining Ticket Count");
			headerList.add("Retail Price");
			headerList.add("Wholesale Price");
			headerList.add("Face Price");
			headerList.add("Cost");
			headerList.add("Total Cost");
			headerList.add("Internal Notes");
			headerList.add("Shipping Method");
			headerList.add("Client Broker Company Name");
			headerList.add("Client Broker Name");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getUnsoldInventoryReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getUnsoldInventoryReport(startDate,endDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumOriginalTicketCnt = 0.0, sumRemainingTicketCnt = 0.0, sumRetailPrice = 0.0, sumWholesalePrice = 0.0;
			Double sumFacePrice = 0.0, sumCost = 0.0, sumTotalCost = 0.0;
			Double originalTicketCnt = 0.0, remainingTicketCnt = 0.0, retailPrice = 0.0, wholesalePrice = 0.0;
			Double facePrice = 0.0, cost = 0.0, totalCost = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					originalTicketCnt = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumOriginalTicketCnt = sumOriginalTicketCnt + originalTicketCnt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(originalTicketCnt));
				}else{
					sumOriginalTicketCnt = sumOriginalTicketCnt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[14] != null){
					remainingTicketCnt = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumRemainingTicketCnt = sumRemainingTicketCnt + remainingTicketCnt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(remainingTicketCnt));
				}else{
					sumRemainingTicketCnt = sumRemainingTicketCnt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					retailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumRetailPrice = sumRetailPrice + retailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(retailPrice));
				}else{
					sumRetailPrice = sumRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					wholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumWholesalePrice = sumWholesalePrice + wholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(wholesalePrice));
				}else{
					sumWholesalePrice = sumWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					facePrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumFacePrice = sumFacePrice + facePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(facePrice));
				}else{
					sumFacePrice = sumFacePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[18] != null){
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumCost = sumCost + cost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(cost));
				}else{
					sumCost = sumCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					totalCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTotalCost = sumTotalCost + totalCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalCost));
				}else{
					sumTotalCost = sumTotalCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[21].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[22].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[23].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOriginalTicketCnt));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRemainingTicketCnt));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFacePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_UNSOLD_INVENTORY_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}*/
	
	/*@RequestMapping({"/DownloadRTFUnsoldCategoryTicketsReport"})
	public void downloadRTFUnsoldCategoryTicketsReport(HttpServletRequest request, HttpServletResponse response,
														@RequestParam(name = "startDate",required = false) String startDate,
														@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Event Id");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Qty");
			headerList.add("Retail Price");
			headerList.add("Wholesale Price");
			headerList.add("Total Retail Price");
			headerList.add("Total Wholesale Price");
			headerList.add("broadcast");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getUnsoldCategoryTicketsReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getUnsoldCategoryTicketsReport(startDate,endDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			//CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			//cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumQuantity = 0.0, sumRetailPrice = 0.0, sumWholesalePrice = 0.0, sumTotalRetailPrice = 0.0;
			Double sumTotalWholesalePrice = 0.0;
			Double quantity = 0.0, retailPrice = 0.0, wholesalePrice = 0.0, totalRetailPrice = 0.0;
			Double totalWholesalePrice = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[2].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[11] != null){
					retailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumRetailPrice = sumRetailPrice + retailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(retailPrice));
				}else{
					sumRetailPrice = sumRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					wholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumWholesalePrice = sumWholesalePrice + wholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(wholesalePrice));
				}else{
					sumWholesalePrice = sumWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					totalRetailPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumTotalRetailPrice = sumTotalRetailPrice + totalRetailPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalRetailPrice));
				}else{
					sumTotalRetailPrice = sumTotalRetailPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[14] != null){
					totalWholesalePrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumTotalWholesalePrice = sumTotalWholesalePrice + totalWholesalePrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalWholesalePrice));
				}else{
					sumTotalWholesalePrice = sumTotalWholesalePrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Boolean broadCast = Boolean.parseBoolean(dataObj[15].toString());
					if(broadCast != null && broadCast){
						rowhead.createCell((int) j).setCellValue("Yes");
					}else{
						rowhead.createCell((int) j).setCellValue("No");
					}
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalRetailPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalWholesalePrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_UNSOLD_CATEGORY_TICKET_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}		
	}*/
	
	/*@RequestMapping({"/DownloadRTFInvoiceReport"})
	public void downloadRTFInvoiceReport(HttpServletRequest request, HttpServletResponse response,
										@RequestParam(name = "startDate",required = false) String startDate,
										@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice Id");
			headerList.add("Created Date");
			headerList.add("Created By");
			headerList.add("Updated Date");
			headerList.add("Updated By");
			headerList.add("Invoice Type");
			headerList.add("Delivery Method");
			headerList.add("Status");
			headerList.add("Notes");
			headerList.add("Customer Name");
			headerList.add("Order Id");
			headerList.add("Invoice Total");
			headerList.add("Qty");
			headerList.add("Real Tix Mapped");
			headerList.add("Tix Uploaded");
			headerList.add("Tix Delivered");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getInvoiceReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getInvoiceReport(startDate,endDate);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			//CellStyle cellStyle = workbook.createCellStyle();
			//cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumInvoiceTotal = 0.0, sumQuantity = 0.0;			
			Double invoiceTotal = 0.0, quantity = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());					
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[11] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[15].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_INVOICE_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}*/
	
	/*@RequestMapping({"/DownloadRTFPurchaseOrderReport"})
	public void downloadRTFPurchaseOrderReport(HttpServletRequest request, HttpServletResponse response,
												@RequestParam(name = "startDate",required = false) String startDate,
												@RequestParam(name = "endDate",required = false) String endDate){		
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("PO Id");
			headerList.add("Created Date");
			headerList.add("Created By");
			headerList.add("PO Type");
			headerList.add("Delivery Method");
			headerList.add("Notes");
			headerList.add("Customer Name");
			headerList.add("PO Total");
			headerList.add("Qty");
			headerList.add("External PO No");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getPurchaseOrderReport(startDate,endDate), ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getPurchaseOrderReport(startDate,endDate);
				
			CreationHelper createHelper = workbook.getCreationHelper();
			//CellStyle cellStyle = workbook.createCellStyle();
			//cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double sumPOTotal = 0.0, sumQuantity = 0.0;			
			Double poTotal = 0.0, quantity = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					poTotal = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumPOTotal = sumPOTotal + poTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(poTotal));
				}else{
					sumPOTotal = sumPOTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					quantity = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumQuantity = sumQuantity + quantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(quantity));
				}else{
					sumQuantity = sumQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumPOTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_PURCHASE_ORDER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}*/
	
	@RequestMapping({"/UnsoldTickets"})
	public String unsoldTickets(HttpServletRequest request,Model model){
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance(); 
		model.addAttribute("startDate", dateFormat.format(cal.getTime()));
		cal.add(Calendar.MONTH, 1);
		model.addAttribute("endDate", dateFormat.format(cal.getTime()));
		return "page-unsold-tickets";	
	}
	
	@RequestMapping({"/PoInvoiceReport"})
	public String poInvoiceReport(HttpServletRequest request,Model model){	
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance(); 
		model.addAttribute("endDate", dateFormat.format(cal.getTime()));
		cal.add(Calendar.MONTH, -3);
		model.addAttribute("startDate", dateFormat.format(cal.getTime()));
		return "page-po-invoice-report";		
	}

	/*@RequestMapping({"/DownloadRTFSalesReport"})
	public void downloadRTFSalesReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//RTF SALES
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice No");
			headerList.add("Invoice Created Date");
			headerList.add("Invoice Status");
			headerList.add("Order No");
			headerList.add("Order Created Date");
			headerList.add("PO No.");
			headerList.add("PO Created Date");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Zone");
			headerList.add("Quantity");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Listing Price");
			headerList.add("Promocode Discount Amt");
			headerList.add("Loyalfan Discount Amt");
			headerList.add("Order Total");
			headerList.add("Cost");
			headerList.add("Net Total");
			headerList.add("Profit And Loss");
			headerList.add("Order Type");
			headerList.add("Platform");
			headerList.add("Payment Methods");
			headerList.add("Promotional Code Used");
			headerList.add("Promotional Code");
			headerList.add("Promotional Discount");
			headerList.add("LoyalFan Discount");
			headerList.add("Referral Code");
			headerList.add("Referrer Customer Email");
						
			Sheet ssSheet =  workbook.createSheet("RTFSalesOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFSalesReport();
			//ExcelUtil.generateExcelData(DAORegistry.getEventDAO().getRTFSalesReportNotInTickTracker(), ssSheet);
			
			List<Integer> invoiceIds = new ArrayList<Integer>();
			Double sumOrderTotal = 0.0,sumNetTotal = 0.0,sumCost = 0.0,sumProfitAndLoss = 0.0;
			Double orderTotal = 0.0,netTotal = 0.0,cost = 0.0,profitAndLoss = 0.0;
			int j = 0;
			int rowCount = 1;
			boolean flag=false;
			for(Object[] dataObj : dataList){
				if(invoiceIds.contains((Integer)dataObj[0])){
					String poIds = "";
					Double poCost = 0.00;
					poIds = ssSheet.getRow(rowCount-1).getCell(5).getStringCellValue();
					poIds += ","+dataObj[5].toString();
					ssSheet.getRow(rowCount-1).getCell(5).setCellValue(poIds);
					
					poCost = ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					poCost = poCost + cost;
					ssSheet.getRow(rowCount-1).getCell(23).setCellValue(poCost);
					flag = true;
					continue;
				}
				if(flag){
					Double c =  ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					Double n =  ssSheet.getRow(rowCount-1).getCell(24).getNumericCellValue();
					ssSheet.getRow(rowCount-1).getCell(25).setCellValue(n-c);
				}
				flag = false;
				Row rowhead = ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				invoiceIds.add((Integer)dataObj[0]);
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[6].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[15].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[16].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				Double doubleVariable = 0.00;
				if(dataObj[19] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[20] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[21] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumCost = sumCost + cost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(cost));
				}else{
					sumCost = sumCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					netTotal = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumNetTotal = sumNetTotal + netTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netTotal));
				}else{
					sumNetTotal = sumNetTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					profitAndLoss = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					sumProfitAndLoss = sumProfitAndLoss + profitAndLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitAndLoss));
				}else{
					sumProfitAndLoss = sumProfitAndLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}	
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[31] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[31].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[32] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[32].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[33] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[33].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[34] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[34].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitAndLoss));
				j++;
				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//Filled Report By Invoice
			List<String> headerFilledInvoice = new ArrayList<String>();
			headerFilledInvoice.add("Invoice Id");
			headerFilledInvoice.add("Invoice Created Date");
			headerFilledInvoice.add("Customer Order Id");
			headerFilledInvoice.add("Customer Order Created Date");
			headerFilledInvoice.add("PO Id");
			headerFilledInvoice.add("PO Created Date");			
			headerFilledInvoice.add("Event Category");
			headerFilledInvoice.add("Event Name");
			headerFilledInvoice.add("Event Date");
			headerFilledInvoice.add("Event Time");
			headerFilledInvoice.add("Venue Name");
			headerFilledInvoice.add("Section Range");		
			headerFilledInvoice.add("Invoice Ticket Qty");
			headerFilledInvoice.add("Sold Ticket Qty");					
			headerFilledInvoice.add("Invoice Total");			
			headerFilledInvoice.add("Ticket Price");
			headerFilledInvoice.add("Sold Price");			
			headerFilledInvoice.add("Actual Sold Price");
			headerFilledInvoice.add("Ticket Cost");
			headerFilledInvoice.add("Total Ticket Cost");			
			headerFilledInvoice.add("Total Sold Price");
			headerFilledInvoice.add("Fees");
			headerFilledInvoice.add("Net Sold Price");
			headerFilledInvoice.add("ActualProfitOrLoss");
			headerFilledInvoice.add("Gross Margin");			
			headerFilledInvoice.add("Client Name");
			headerFilledInvoice.add("Client Email");
			headerFilledInvoice.add("Client Phone");
			headerFilledInvoice.add("Client BillingAddress");
			headerFilledInvoice.add("Client ShippingAddress");
			
			Sheet ssSheet1 =  workbook.createSheet("FilledReportByInvoice");
			ExcelUtil.generateExcelHeaderRow(headerFilledInvoice, ssSheet1);			
			List<Object[]> dataFilledByInvoice = DAORegistry.getEventDAO().getRTFFilledReportByInvoice();
			
			Double sumInvoiceTktQuantity = 0.0, sumSoldTktQuantity = 0.0, sumInvoiceTotal = 0.0, sumTicketPrice = 0.0;
			Double sumSoldPrice = 0.0, sumActualSoldPrice = 0.0, sumTicketCost = 0.0, sumTotalTicketCost = 0.0;
			Double sumTotalSoldPrice = 0.0, sumFees = 0.0, sumNetSoldPrice =0.0, sumProfitOrLoss = 0.0, sumGrossMargin = 0.0;
			Double invoiceTktQuantity = 0.0, soldTktQuantity = 0.0, invoiceTotal = 0.0, ticketPrice = 0.0;			
			Double soldPrice = 0.0, actualSoldPrice = 0.0, ticketCost = 0.0, totalTicketCost = 0.0;
			Double totalSoldPrice = 0.0, fees = 0.0, netSoldPrice =0.0, profitOrLoss = 0.0, grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataFilledByInvoice){
				
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[5].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[12] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[25].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataFilledByInvoice != null && dataFilledByInvoice.size() > 0){
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
								
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//Filled Report By PO
			List<String> headerFilledPO = new ArrayList<String>();
			headerFilledPO.add("PO Id");
			headerFilledPO.add("PO Created Date");
			headerFilledPO.add("Invoice Id");
			headerFilledPO.add("Invoice Created Date");
			headerFilledPO.add("Customer Order Id");
			headerFilledPO.add("Customer Order Created Date");						
			headerFilledPO.add("Event Category");
			headerFilledPO.add("Event Name");
			headerFilledPO.add("Event Date");
			headerFilledPO.add("Event Time");
			headerFilledPO.add("Venue Name");
			headerFilledPO.add("Section Range");
			headerFilledPO.add("Row Range");			
			headerFilledPO.add("Invoice Ticket Qty");
			headerFilledPO.add("Sold Ticket Qty");					
			headerFilledPO.add("Invoice Total");			
			headerFilledPO.add("Ticket Price");
			headerFilledPO.add("Sold Price");			
			headerFilledPO.add("Actual Sold Price");
			headerFilledPO.add("Ticket Cost");
			headerFilledPO.add("Total Ticket Cost");			
			headerFilledPO.add("Total Sold Price");
			headerFilledPO.add("Fees");
			headerFilledPO.add("Net Sold Price");
			headerFilledPO.add("ActualProfitOrLoss");
			headerFilledPO.add("Gross Margin");			
			headerFilledPO.add("Client Name");
			headerFilledPO.add("Client Email");
			headerFilledPO.add("Client Phone");
			headerFilledPO.add("Client BillingAddress");
			headerFilledPO.add("Client ShippingAddress");
			
			Sheet ssSheet3 =  workbook.createSheet("FilledReportByPO");
			ExcelUtil.generateExcelHeaderRow(headerFilledPO, ssSheet3);			
			List<Object[]> dataFilledByPO = DAORegistry.getEventDAO().getRTFFilledReportByPO();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumFees = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; fees = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataFilledByPO){
				
				Row rowhead =   ssSheet3.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[5].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataFilledByPO != null && dataFilledByPO.size() > 0){
				Row rowhead =   ssSheet3.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//UnFilled Report By Invoice
			List<String> headerUnFilledInvoice = new ArrayList<String>();
			headerUnFilledInvoice.add("Invoice Id");
			headerUnFilledInvoice.add("Invoice Created Date");		
			headerUnFilledInvoice.add("Event Category");
			headerUnFilledInvoice.add("Event Name");
			headerUnFilledInvoice.add("Event Date");
			headerUnFilledInvoice.add("Event Time");
			headerUnFilledInvoice.add("Venue Name");
			headerUnFilledInvoice.add("Section");
			headerUnFilledInvoice.add("Row");
			headerUnFilledInvoice.add("Section Range");
			headerUnFilledInvoice.add("Row Range");			
			headerUnFilledInvoice.add("Invoice Ticket Qty");
			headerUnFilledInvoice.add("Sold Ticket Qty");					
			headerUnFilledInvoice.add("Invoice Total");			
			headerUnFilledInvoice.add("Ticket Price");
			headerUnFilledInvoice.add("Sold Price");			
			headerUnFilledInvoice.add("Actual Sold Price");			
			headerUnFilledInvoice.add("Total Sold Price");
			headerUnFilledInvoice.add("Fees");
			headerUnFilledInvoice.add("Net Sold Price");	
			headerUnFilledInvoice.add("Client Name");
			headerUnFilledInvoice.add("Client Email");
			headerUnFilledInvoice.add("Client Phone");
			headerUnFilledInvoice.add("Client BillingAddress");
			headerUnFilledInvoice.add("Client ShippingAddress");			
			
			Sheet ssSheet2 =  workbook.createSheet("UnFilledReportByInvoice");
			ExcelUtil.generateExcelHeaderRow(headerUnFilledInvoice, ssSheet2);
			List<Object[]> dataUnFilledInvoice = DAORegistry.getEventDAO().getRTFUnFilledReportByInvoice();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumFees = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; fees = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataUnFilledInvoice){
				
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[17].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					fees = Util.getRoundedValue(Double.parseDouble(dataObj[18].toString()));
					sumFees = sumFees + fees;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(fees));
				}else{
					sumFees = sumFees + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[21].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[22].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[23].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[24].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataUnFilledInvoice != null && dataUnFilledInvoice.size() > 0){
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumFees));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			
			//Monthly Filled Report By Invoice
			List<String> hdrMonthlyFilledInvoice = new ArrayList<String>();
			hdrMonthlyFilledInvoice.add("Invoice Year");
			hdrMonthlyFilledInvoice.add("Invoice Month");
			hdrMonthlyFilledInvoice.add("Invoice Id");
			hdrMonthlyFilledInvoice.add("Customer Order Id");						
			hdrMonthlyFilledInvoice.add("PO Id");			
			hdrMonthlyFilledInvoice.add("Invoice Ticket Qty");
			hdrMonthlyFilledInvoice.add("Sold Ticket Qty");					
			hdrMonthlyFilledInvoice.add("Invoice Total");			
			hdrMonthlyFilledInvoice.add("Ticket Price");
			hdrMonthlyFilledInvoice.add("Sold Price");			
			hdrMonthlyFilledInvoice.add("Actual Sold Price");
			hdrMonthlyFilledInvoice.add("Ticket Cost");
			hdrMonthlyFilledInvoice.add("Total Ticket Cost");
			hdrMonthlyFilledInvoice.add("Total Sold Price");
			hdrMonthlyFilledInvoice.add("Net Sold Price");
			hdrMonthlyFilledInvoice.add("ActualProfitOrLoss");
			hdrMonthlyFilledInvoice.add("Gross Margin");
			
			Sheet ssSheet4 =  workbook.createSheet("MonthlyFilledReportByInvoice");
			ExcelUtil.generateExcelHeaderRow(hdrMonthlyFilledInvoice, ssSheet4);			
			List<Object[]> dataMonthlyFilledByInvoice = DAORegistry.getEventDAO().getRTFMonthlyFilledReportByInvoice();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataMonthlyFilledByInvoice){
				
				Row rowhead =   ssSheet4.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataMonthlyFilledByInvoice != null && dataMonthlyFilledByInvoice.size() > 0){
				Row rowhead =   ssSheet4.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
			}
			
			//Monthly Filled Report By PO
			List<String> hdrMonthlyFilledPO = new ArrayList<String>();
			hdrMonthlyFilledPO.add("PO Year");
			hdrMonthlyFilledPO.add("PO Month");
			hdrMonthlyFilledPO.add("PO Id");
			hdrMonthlyFilledPO.add("Invoice Id");
			hdrMonthlyFilledPO.add("Customer Order Id");
			hdrMonthlyFilledPO.add("Invoice Ticket Qty");
			hdrMonthlyFilledPO.add("Sold Ticket Qty");					
			hdrMonthlyFilledPO.add("Invoice Total");			
			hdrMonthlyFilledPO.add("Ticket Price");
			hdrMonthlyFilledPO.add("Sold Price");			
			hdrMonthlyFilledPO.add("Actual Sold Price");
			hdrMonthlyFilledPO.add("Ticket Cost");
			hdrMonthlyFilledPO.add("Total Ticket Cost");
			hdrMonthlyFilledPO.add("Total Sold Price");
			hdrMonthlyFilledPO.add("Net Sold Price");
			hdrMonthlyFilledPO.add("ActualProfitOrLoss");
			hdrMonthlyFilledPO.add("Gross Margin");
			
			Sheet ssSheet5 =  workbook.createSheet("MonthlyFilledReportByPO");
			ExcelUtil.generateExcelHeaderRow(hdrMonthlyFilledPO, ssSheet5);			
			List<Object[]> dataMonthlyFilledByPO = DAORegistry.getEventDAO().getRTFMonthlyFilledReportByPO();
			
			sumInvoiceTktQuantity = 0.0; sumSoldTktQuantity = 0.0; sumInvoiceTotal = 0.0; sumTicketPrice = 0.0;
			sumSoldPrice = 0.0; sumActualSoldPrice = 0.0; sumTicketCost = 0.0; sumTotalTicketCost = 0.0;
			sumTotalSoldPrice = 0.0; sumNetSoldPrice =0.0; sumProfitOrLoss = 0.0; sumGrossMargin = 0.0;
			invoiceTktQuantity = 0.0; soldTktQuantity = 0.0; invoiceTotal = 0.0; ticketPrice = 0.0;			
			soldPrice = 0.0; actualSoldPrice = 0.0; ticketCost = 0.0; totalTicketCost = 0.0;
			totalSoldPrice = 0.0; netSoldPrice =0.0; profitOrLoss = 0.0; grossMargin = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataMonthlyFilledByPO){
				
				Row rowhead =   ssSheet5.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					soldTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumSoldTktQuantity = sumSoldTktQuantity + soldTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldTktQuantity));
				}else{
					sumSoldTktQuantity = sumSoldTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					actualSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumActualSoldPrice = sumActualSoldPrice + actualSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(actualSoldPrice));
				}else{
					sumActualSoldPrice = sumActualSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					ticketCost = Util.getRoundedValue(Double.parseDouble(dataObj[11].toString()));
					sumTicketCost = sumTicketCost + ticketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketCost));
				}else{
					sumTicketCost = sumTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					totalTicketCost = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumTotalTicketCost = sumTotalTicketCost + totalTicketCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketCost));
				}else{
					sumTotalTicketCost = sumTotalTicketCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					totalSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					sumTotalSoldPrice = sumTotalSoldPrice + totalSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalSoldPrice));
				}else{
					sumTotalSoldPrice = sumTotalSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					netSoldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumNetSoldPrice = sumNetSoldPrice + netSoldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netSoldPrice));
				}else{
					sumNetSoldPrice = sumNetSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					profitOrLoss = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumProfitOrLoss = sumProfitOrLoss + profitOrLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitOrLoss));
				}else{
					sumProfitOrLoss = sumProfitOrLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					grossMargin = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					//sumGrossMargin = sumGrossMargin + grossMargin;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(grossMargin));
				}else{
					//sumGrossMargin = sumGrossMargin + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataMonthlyFilledByPO != null && dataMonthlyFilledByPO.size() > 0){
				Row rowhead =   ssSheet5.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumActualSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitOrLoss));
				j++;
				
				sumGrossMargin = (sumProfitOrLoss*100)/sumTotalSoldPrice;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumGrossMargin));
				j++;
			}

			//Voided Invoice
			List<String> headerVoidedInvoice = new ArrayList<String>();
			headerVoidedInvoice.add("Invoice Id");
			headerVoidedInvoice.add("Invoice Created Date");
			headerVoidedInvoice.add("Customer Order Id");
			headerVoidedInvoice.add("Customer Order Created Date");
			headerVoidedInvoice.add("Customer Name");
			headerVoidedInvoice.add("Customer Email");
			headerVoidedInvoice.add("Event Name");
			headerVoidedInvoice.add("Event Date");
			headerVoidedInvoice.add("Event Time");
			headerVoidedInvoice.add("Venue Name");
			headerVoidedInvoice.add("Venue City");
			headerVoidedInvoice.add("Zone");
			headerVoidedInvoice.add("Order Tiket Qty");
			headerVoidedInvoice.add("Listing Price");					
			headerVoidedInvoice.add("Order Total");			
			headerVoidedInvoice.add("Ticket Price");
			headerVoidedInvoice.add("Sold Price");			
			headerVoidedInvoice.add("Order Type");
			headerVoidedInvoice.add("Order Platform");
			headerVoidedInvoice.add("Invoice Voided Date");			
			headerVoidedInvoice.add("Invoice Total");
			headerVoidedInvoice.add("Invoice Ticket Qty");
			headerVoidedInvoice.add("Invoice Refund Amount");
			headerVoidedInvoice.add("Order Refund Amount");
			
			Sheet ssSheet6 =  workbook.createSheet("VoidedInvoice");
			ExcelUtil.generateExcelHeaderRow(headerVoidedInvoice, ssSheet6);			
			List<Object[]> dataVoidedInvoice = DAORegistry.getEventDAO().getRTFVoidedInvoiceReport();
			
			Double sumOrderTktQuantity = 0.0, orderTktQuantity = 0.0; 
			sumOrderTotal = 0.0; sumTicketPrice = 0.0; sumSoldPrice = 0.0; sumInvoiceTotal = 0.0; sumInvoiceTktQuantity = 0.0; 			
			orderTotal = 0.0; ticketPrice = 0.0;	soldPrice = 0.0; invoiceTotal = 0.0; invoiceTktQuantity = 0.0; 
			Double sumInvoiceRefundAmt = 0.0, sumOrderRefundAmt = 0.0, invoiceRefundAmt = 0.0, orderRefundAmt = 0.0;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataVoidedInvoice){
				
				Row rowhead =   ssSheet6.createRow((int)rowCount);
				
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[7].toString()));
					cell.setCellStyle(cellStyle);					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[12] != null){
					orderTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumOrderTktQuantity = sumOrderTktQuantity + orderTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTktQuantity));
				}else{
					sumOrderTktQuantity = sumOrderTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				double listingPrice = 0.0;
				if(dataObj[13] != null){
					listingPrice = Util.getRoundedValue(Double.parseDouble(dataObj[13].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(listingPrice));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[14].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					ticketPrice = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumTicketPrice = sumTicketPrice + ticketPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(ticketPrice));
				}else{
					sumTicketPrice = sumTicketPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					soldPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumSoldPrice = sumSoldPrice + soldPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(soldPrice));
				}else{
					sumSoldPrice = sumSoldPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[19].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					invoiceTotal = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					sumInvoiceTotal = sumInvoiceTotal + invoiceTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTotal));
				}else{
					sumInvoiceTotal = sumInvoiceTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[21] != null){
					invoiceTktQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + invoiceTktQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceTktQuantity));
				}else{
					sumInvoiceTktQuantity = sumInvoiceTktQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					invoiceRefundAmt = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumInvoiceRefundAmt = sumInvoiceRefundAmt + invoiceRefundAmt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(invoiceRefundAmt));
				}else{
					sumInvoiceRefundAmt = sumInvoiceRefundAmt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					orderRefundAmt = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumOrderRefundAmt = sumOrderRefundAmt + orderRefundAmt;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderRefundAmt));
				}else{
					sumOrderRefundAmt = sumOrderRefundAmt + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataVoidedInvoice != null && dataVoidedInvoice.size() > 0){
				Row rowhead =   ssSheet6.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTicketPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSoldPrice));
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceTktQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumInvoiceRefundAmt));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderRefundAmt));
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_SALES_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFCustomerOrderDetailsReport"})
	public void downloadRTFCustomerOrderDetailsReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			//headerList.add("Customer ID");
			headerList.add("Customer Name");
			headerList.add("Email");
			headerList.add("Total No. Of Orders");
			headerList.add("No. Of Loyal Fan Orders");
			headerList.add("No. Of Regular Orders");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("WebOrders");
			Sheet ssSheet1 =  workbook.createSheet("PhoneOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet1);
			List<Object[]> webOrderList = DAORegistry.getEventDAO().getRTFCustomerOrderForWebOrdersReport();
			List<Object[]> phoneOrderList = DAORegistry.getEventDAO().getRTFCustomerOrderForPhoneOrdersReport();
			
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : webOrderList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
//				if(dataObj[0] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null && !dataObj[3].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[3].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[4] != null && !dataObj[4].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[4].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[5] != null && !dataObj[5].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[5].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
			}
			
			rowCount = 1;
			for(Object[] dataObj : phoneOrderList){
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				rowCount++;
				j = 0;
//				if(dataObj[0] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null && !dataObj[3].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[3].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[4] != null && !dataObj[4].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[4].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
				
				if(dataObj[5] != null && !dataObj[5].toString().isEmpty()){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[5].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue(0);
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_CUSTOMER_ORDER_DETAILS_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/*@RequestMapping({"/DownloadRTFCustomerOrdersDuplicateReport"})
	public void downloadRTFRepeatedCustomerReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("No. Of Users");
			headerList.add("No. Of Orders");
			headerList.add("Percentage(%) Of Users");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("RepeatedOrders");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> repeatedCustomersList = DAORegistry.getEventDAO().getRTFCustomerOrdersDuplicationsReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : repeatedCustomersList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[0].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[1].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(Double.parseDouble(String.format("%.2f", dataObj[2])));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;								
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_REPEATED_ORDERS_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFChannelWiseCustomerOrdersReport"})
	public void downloadRTFChannelWiseCustomerOrderReport(HttpServletRequest request, HttpServletResponse response){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("No. Of Users");
			headerList.add("No. Of Orders");
			headerList.add("Platform");
			headerList.add("Percentage(%) Of Users");

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("ChannelWiseCustomerOrder");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> repeatedCustomersList = DAORegistry.getEventDAO().getRTFChannelWiseCustomerOrdersReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : repeatedCustomersList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[0].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(Integer.parseInt(dataObj[1].toString()));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(Double.parseDouble(String.format("%.2f", dataObj[3])));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;								
			}
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment; filename="+Constants.RTF_CHANNELWISE_ORDERS_REPORT+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadClientMasterReport"})
	public void downloadClientMasterReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("ClientMaster");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Customers> clientMastersList = DAORegistry.getEventDAO().getClientMasterReport();
			
			int j = 0;
			int rowCount = 1;
			
			Map<String, Customers> clientMap  = new HashMap<String, Customers>();
			for(Customers cust : clientMastersList){
				clientMap.put(cust.getCustomerEmail().toUpperCase().trim(), cust);
			}
			
			for(Customers dataObj : clientMap.values()){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj.getCustomerId() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerId().toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getFirstName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getFirstName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj.getLastName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getLastName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCustomerEmail() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCompanyName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCompanyName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getProductType() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getProductType());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine1() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine1());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine2() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine2());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCity() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCity());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getState() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getState());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCountry() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCountry());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getZipCode() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getZipCode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.CLIENT_MASTER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadUnsubscribedClientMasterReport"})
	public void downloadUnsubscribedClientMasterReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("UnsubscribedClientMaster");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Customers> unsubscribedClientMastersList = DAORegistry.getEventDAO().getUnsubscribedClientMasterReport();
			
			int j = 0;
			int rowCount = 1;
			
			Map<String, Customers> clientMap  = new HashMap<String, Customers>();
			for(Customers cust : unsubscribedClientMastersList){
				clientMap.put(cust.getCustomerEmail().toUpperCase().trim(), cust);
			}
			
			for(Customers dataObj : clientMap.values()){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj.getCustomerId() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerId().toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getFirstName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getFirstName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj.getLastName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getLastName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCustomerEmail() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCustomerEmail());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCompanyName() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCompanyName());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getProductType() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getProductType());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine1() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine1());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getAddressLine2() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getAddressLine2());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCity() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCity());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getState() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getState());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getCountry() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getCountry());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getZipCode() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getZipCode());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj.getPhone() != null){
					rowhead.createCell((int) j).setCellValue(dataObj.getPhone());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.UNSUBSCRIBED_CLIENT_MASTER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadPurchasedMasterReport"})
	public void downloadPurchasedMasterReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("PurchasedMaster");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> puchasedMastersList = DAORegistry.getEventDAO().getPurchasedMasterReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : puchasedMastersList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.PURCHASED_MASTER_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/*@RequestMapping({"/DownloadRTFRegisteredButnotPurchasedReport"})
	public void downloadRTFRegisteredButnotPurchasedReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("RegisteredButnotPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> registeredButnotPurchasedList = DAORegistry.getEventDAO().getRTFRegisteredButnotPurchasedReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : registeredButnotPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_REGISTERED_BUT_NOT_PURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/*@RequestMapping({"/DownloadRTFCombinedPurchasedNonPurchasedReport"})
	public void downloadRTFCombinedPurchasedNonPurchasedReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("CombinedPurchasedNonPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> purchasedNonPurchasedList = DAORegistry.getEventDAO().getRTFCombinedPurchasedNonPurchasedReport();
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : purchasedNonPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_COMBINED_PURCHASED_NONPURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	

	/*@RequestMapping({"/DownloadRTFLoyalFanReport"})
	public void downloadRTFLoyalFanReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			headerList.add("Loyal Fan Type");
			headerList.add("Loyal Fan Name");
			headerList.add("Loyal Fan Start Date");
			headerList.add("Loyal Fan End Date");
			headerList.add("Loyal Fan Ticket Purchased");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("LoyalFan");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> loyalFanList = DAORegistry.getEventDAO().getRTFLoyalFanReport();
			
			CreationHelper createHelper = workbook.getCreationHelper();	
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : loyalFanList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[16].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					Boolean ticketPurchased = Boolean.parseBoolean(dataObj[17].toString());
					if(ticketPurchased != null && ticketPurchased){
						rowhead.createCell((int) j).setCellValue("Yes");
					}else{
						rowhead.createCell((int) j).setCellValue("No");
					}
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_LOYAL_FAN_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFLoyalFanPurchasedReport"})
	public void downloadRTFLoyalFanPurchasedReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			headerList.add("Loyal Fan Type");
			headerList.add("Loyal Fan Name");
			headerList.add("Loyal Fan Start Date");
			headerList.add("Loyal Fan End Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("LoyalFanPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> loyalFanPurchasedList = DAORegistry.getEventDAO().getRTFLoyalFanPurchasedReport();
			
			CreationHelper createHelper = workbook.getCreationHelper();	
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : loyalFanPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[16].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_LOYAL_FAN_PURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFLoyalFanNonPurchasedReport"})
	public void downloadRTFLoyalFanNonPurchasedReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer Id");
			headerList.add("First Name");
			headerList.add("Last Name");
			headerList.add("Email");
			headerList.add("Company Name");
			headerList.add("Product Type");
			headerList.add("Address Line1");
			headerList.add("Address Line2");
			headerList.add("City");
			headerList.add("State");
			headerList.add("Country");
			headerList.add("Zip Code");
			headerList.add("Phone");
			headerList.add("Loyal Fan Type");
			headerList.add("Loyal Fan Name");
			headerList.add("Loyal Fan Start Date");
			headerList.add("Loyal Fan End Date");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("LoyalFanNonPurchased");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> loyalFanNonPurchasedList = DAORegistry.getEventDAO().getRTFLoyalFanNonPurchasedReport();
			
			CreationHelper createHelper = workbook.getCreationHelper();	
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			int j = 0;
			int rowCount = 1;
			for(Object[] dataObj : loyalFanNonPurchasedList){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[4].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[15].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[16].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_LOYAL_FAN_NON_PURCHASED_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/DownloadRTFSalesReportByReferral"})
	public void downloadRTFSalesReportByReferral(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//RTF Sales Report By Referral
			List<String> headerList = new ArrayList<String>();
			headerList.add("Invoice No");
			headerList.add("Invoice Created Date");
			headerList.add("Invoice Status");
			headerList.add("Order No");
			headerList.add("Order Created Date");
			headerList.add("PO No.");
			headerList.add("PO Created Date");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Zone");
			headerList.add("Quantity");
			headerList.add("Section");
			headerList.add("Row");
			headerList.add("Seat");
			headerList.add("Customer Name");
			headerList.add("Customer Email");
			headerList.add("Listing Price");
			headerList.add("Promocode Discount Amt");
			headerList.add("Loyalfan Discount Amt");
			headerList.add("Order Total");
			headerList.add("Cost");
			headerList.add("Net Total");
			headerList.add("Profit And Loss");
			headerList.add("Order Type");
			headerList.add("Platform");
			headerList.add("Payment Methods");
			headerList.add("Promotional Code Used");
			headerList.add("Promotional Code");
			headerList.add("Promotional Discount");
			headerList.add("LoyalFan Discount");
			headerList.add("Referral Code");
			headerList.add("Referrer Customer Email");
						
			Sheet ssSheet =  workbook.createSheet("RTFSalesOrdersByReferral");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFSalesReportByReferral();
			
			List<Integer> invoiceIds = new ArrayList<Integer>();
			Double sumOrderTotal = 0.0,sumNetTotal = 0.0,sumCost = 0.0,sumProfitAndLoss = 0.0;
			Double orderTotal = 0.0,netTotal = 0.0,cost = 0.0,profitAndLoss = 0.0;
			int j = 0;
			int rowCount = 1;
			boolean flag=false;
			for(Object[] dataObj : dataList){
				if(invoiceIds.contains((Integer)dataObj[0])){
					String poIds = "";
					Double poCost = 0.00;
					poIds = ssSheet.getRow(rowCount-1).getCell(5).getStringCellValue();
					poIds += ","+dataObj[5].toString();
					ssSheet.getRow(rowCount-1).getCell(5).setCellValue(poIds);
					
					poCost = ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					poCost = poCost + cost;
					ssSheet.getRow(rowCount-1).getCell(23).setCellValue(poCost);
					flag = true;
					continue;
				}
				if(flag){
					Double c =  ssSheet.getRow(rowCount-1).getCell(23).getNumericCellValue();
					Double n =  ssSheet.getRow(rowCount-1).getCell(24).getNumericCellValue();
					ssSheet.getRow(rowCount-1).getCell(25).setCellValue(n-c);
				}
				flag = false;
				Row rowhead = ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				invoiceIds.add((Integer)dataObj[0]);
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[6].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[8].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[10].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[15].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[16].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[18].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				Double doubleVariable = 0.00;
				if(dataObj[19] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[19].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[20] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[20].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				doubleVariable = 0.00;
				if(dataObj[21] != null){
					doubleVariable = Util.getRoundedValue(Double.parseDouble(dataObj[21].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(doubleVariable));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[22] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[22].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[23] != null){
					cost = Util.getRoundedValue(Double.parseDouble(dataObj[23].toString()));
					sumCost = sumCost + cost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(cost));
				}else{
					sumCost = sumCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[24] != null){
					netTotal = Util.getRoundedValue(Double.parseDouble(dataObj[24].toString()));
					sumNetTotal = sumNetTotal + netTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(netTotal));
				}else{
					sumNetTotal = sumNetTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[25] != null){
					profitAndLoss = Util.getRoundedValue(Double.parseDouble(dataObj[25].toString()));
					sumProfitAndLoss = sumProfitAndLoss + profitAndLoss;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(profitAndLoss));
				}else{
					sumProfitAndLoss = sumProfitAndLoss + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				
				if(dataObj[26] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[26].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}	
				j++;
				
				if(dataObj[27] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[27].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[28] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[28].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[29] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[29].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[30] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[30].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[31] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[31].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[32] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[32].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[33] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[33].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[34] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[34].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumCost));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumNetTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumProfitAndLoss));
				j++;
				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
			}
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_SALES_REPORT_BY_REFERRAL+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/LoadRTFCustomerSpentReport"})
	public String loadRTFCustomerSpentReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException{
				
		return "page-customer-spent-report";
	}
	
	@RequestMapping({"/ReferralChainReport"})
	public String referralChainReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException{
				
		return "page-referral-chain-report";
	}
	
	/*@RequestMapping({"/DownloadRTFCustomerSpentReport"})
	public void downloadRTFCustomerSpentReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			String selectedYears = request.getParameter("selectedYears");
			//String selectedOrders = request.getParameter("selectedOrders");
			String selectedOrders = request.getParameter("selectedOrders");
			String yearStr = "";
			Boolean isAll = false;
			Boolean isPhone = false;
			Boolean isWeb = false;
			Boolean isApp = false;
			
			if(selectedYears!=null && !selectedYears.isEmpty()){
				if(selectedYears.contains("ALL")){
					yearStr = "ALL";
				}else if(selectedYears.contains(",")){
					String arr[] = selectedYears.split(",");
					for(String str : arr){						
						yearStr = yearStr + "'"+str+"'" +",";						
					}
					yearStr = yearStr.substring(0, yearStr.length()-1);					
				}else{
					if(selectedYears.equalsIgnoreCase("ALL")){
						yearStr = "ALL";
					}else{
						yearStr = yearStr + "'"+selectedYears+"'";
					}
				}
			}
			
			if(selectedOrders!=null && !selectedOrders.isEmpty()){
				if(selectedOrders.contains(",")){
					String arr[] = selectedOrders.split(",");
					for(String str : arr){
						if(str.equalsIgnoreCase("ALL")){
							isAll=true;
						}
						if(str.equalsIgnoreCase("PHONE_ORDERS")){
							isPhone=true;
						}
						if(str.equalsIgnoreCase("WEB/APP_ORDERS")){
							isWeb=true;
							isApp=true;
						}
					}
				}else{
					if(selectedOrders.equalsIgnoreCase("ALL")){
						isAll=true;
					}
					if(selectedOrders.equalsIgnoreCase("PHONE_ORDERS")){
						isPhone=true;
					}
					if(selectedOrders.equalsIgnoreCase("WEB/APP_ORDERS")){
						isWeb=true;
						isApp=true;
					}
				}
			}
			
			String selPlatform = "";
			if(isAll){
				selPlatform="ALL";
			}else if(isPhone){
				selPlatform="'TICK_TRACKER'";
			}else if(isWeb && isApp){
				selPlatform="'DESKTOP_SITE','IOS','ANDROID'";
			}
			
//			if(isAll){
//				selPlatform="ALL";
//			}else if(isPhone && isWeb && isApp){
//				selPlatform="'TICK_TRACKER','DESKTOP_SITE','IOS','ANDROID'";
//			}else if(isPhone && isWeb){
//				selPlatform="'TICK_TRACKER','DESKTOP_SITE'";
//			}else if(isPhone && isApp){
//				selPlatform="'TICK_TRACKER','IOS','ANDROID'";
//			}else if(isWeb && isApp){
//				selPlatform="'DESKTOP_SITE','IOS','ANDROID'";
//			}else if(isPhone){
//				selPlatform="'TICK_TRACKER'";
//			}else if(isWeb){
//				selPlatform="'DESKTOP_SITE'";
//			}else if(isApp){
//				selPlatform="'IOS','ANDROID'";
//			}
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			//RTF Customer Year(Platform Wise) Spent Report
			List<String> headerList = new ArrayList<String>();
//			headerList.add("Order Year");
			headerList.add("CustomerName");
			headerList.add("No Of Orders");
			headerList.add("Total Ticket Quantity");
			headerList.add("Total Order Value");
			headerList.add("Earned Points");
			headerList.add("Redeemed Points");
			headerList.add("LoyalFan Discount");
			headerList.add("App Discount");
			headerList.add("Promotional Discounts");
			headerList.add("SpentOn Customer");
			headerList.add("Average SpentOn Customer");
			
//			if(selPlatform != null && !selPlatform.isEmpty()){
//				headerList.add("Platform");
//			}
			
			Sheet ssSheet = null;
			List<Object[]> dataList = null;
//			if(selPlatform != null && !selPlatform.isEmpty()){
				ssSheet = workbook.createSheet("YearlyCustomerWise");
				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
				dataList = DAORegistry.getEventDAO().getRTFCustomerPlatformWiseReport(yearStr, selPlatform);
//			}else{
//				ssSheet = workbook.createSheet("YearlyCustomerWise");
//				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);			
//				dataList = DAORegistry.getEventDAO().getRTFCustomerYearlySpentReport(yearStr);
//			}
			
			Double sumOrders = 0.0, sumTotalTicketQuantity = 0.0, sumTotalOrderValue = 0.0;
			Double orders = 0.0, totalTicketQuantity = 0.0, totalOrderValue = 0.0;
			Double sumEarnedPoints = 0.0, sumRedeemedPoints = 0.0;
			Double earnedPoints = 0.0, redeemedPoints = 0.0;
			Double sumLoyalFanDiscount = 0.0, sumAppDiscount = 0.0, sumPromotionalDiscounts = 0.0;
			Double loyalFanDiscount = 0.0, appDiscount = 0.0, promotionalDiscounts = 0.0;
			Double sumSpentOnCustomer = 0.0, sumAverageSpentOnCustomer = 0.0;
			Double spentOnCustomer = 0.0, averageSpentOnCustomer = 0.0;
			
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){				
				Row rowhead = ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					orders = Util.getRoundedValue(Double.parseDouble(dataObj[1].toString()));
					sumOrders = sumOrders + orders;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orders));
				}else{
					sumOrders = sumOrders + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					totalTicketQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
					sumTotalTicketQuantity = sumTotalTicketQuantity + totalTicketQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalTicketQuantity));
				}else{
					sumTotalTicketQuantity = sumTotalTicketQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					totalOrderValue = Util.getRoundedValue(Double.parseDouble(dataObj[3].toString()));
					sumTotalOrderValue = sumTotalOrderValue + totalOrderValue;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalOrderValue));
				}else{
					sumTotalOrderValue = sumTotalOrderValue + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					earnedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[4].toString()));
					sumEarnedPoints = sumEarnedPoints + earnedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(earnedPoints));
				}else{
					sumEarnedPoints = sumEarnedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					redeemedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumRedeemedPoints = sumRedeemedPoints + redeemedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(redeemedPoints));
				}else{
					sumRedeemedPoints = sumRedeemedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					loyalFanDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumLoyalFanDiscount = sumLoyalFanDiscount + loyalFanDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(loyalFanDiscount));
				}else{
					sumLoyalFanDiscount = sumLoyalFanDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					appDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumAppDiscount = sumAppDiscount + appDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(appDiscount));
				}else{
					sumAppDiscount = sumAppDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					promotionalDiscounts = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumPromotionalDiscounts = sumPromotionalDiscounts + promotionalDiscounts;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(promotionalDiscounts));
				}else{
					sumPromotionalDiscounts = sumPromotionalDiscounts + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					spentOnCustomer = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumSpentOnCustomer = sumSpentOnCustomer + spentOnCustomer;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(spentOnCustomer));
				}else{
					sumSpentOnCustomer = sumSpentOnCustomer + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					averageSpentOnCustomer = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumAverageSpentOnCustomer = sumAverageSpentOnCustomer + averageSpentOnCustomer;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(averageSpentOnCustomer));
				}else{
					sumAverageSpentOnCustomer = sumAverageSpentOnCustomer + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
//				if(selPlatform != null && !selPlatform.isEmpty()){
//					if(dataObj[12] != null){
//						rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
//					}else{
//						rowhead.createCell((int) j).setCellValue("");
//					}
//					j++;
//				}
			}
			
			if(dataList != null && dataList.size() > 0){
				Row rowhead =   ssSheet.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrders));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalTicketQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalOrderValue));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumEarnedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRedeemedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLoyalFanDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAppDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumPromotionalDiscounts));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumSpentOnCustomer));
				j++;
				
				sumAverageSpentOnCustomer = sumSpentOnCustomer/sumOrders;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAverageSpentOnCustomer));
				j++;
				
//				if(selPlatform != null && !selPlatform.isEmpty()){
//					rowhead.createCell((int) j).setCellValue("");
//					j++;
//				}
			}
			
			//RTF Customer Order Wise Report
			List<String> headerList1 = new ArrayList<String>();
			headerList1.add("Order Year");
			headerList1.add("CustomerName");
			headerList1.add("Order No");
			headerList1.add("Order Date");
			headerList1.add("Order Quantity");
			headerList1.add("Order Total");
			headerList1.add("Earned Points");
			headerList1.add("Redeemed Points");
			headerList1.add("LoyalFan Discount");
			headerList1.add("App Discount");
			headerList1.add("Promotional Discounts");
			headerList1.add("SpentOn Customer");
			headerList1.add("Average SpentOn Customer");
			headerList1.add("Promotional Type");
			headerList1.add("Platform");
			
			Sheet ssSheet1 =  workbook.createSheet("CustomerOrderWise");
			ExcelUtil.generateExcelHeaderRow(headerList1, ssSheet1);			
			List<Object[]> dataList1 = DAORegistry.getEventDAO().getRTFCustomerOrderwiseReport(yearStr, selPlatform);
						
			Double sumOrderQuantity = 0.0, sumOrderTotal = 0.0;
			Double orderQuantity = 0.0, orderTotal = 0.0;
			sumEarnedPoints = 0.0; sumRedeemedPoints = 0.0;
			earnedPoints = 0.0; redeemedPoints = 0.0;
			sumLoyalFanDiscount = 0.0; sumAppDiscount = 0.0; sumPromotionalDiscounts = 0.0;
			loyalFanDiscount = 0.0; appDiscount = 0.0; promotionalDiscounts = 0.0;
						
			j = 0;
			rowCount = 1;
			Double redeemedPoint = 0.00, loyalFanDisc = 0.00, appDisc = 0.00, promotionalDisc = 0.00; 
			Double spentOnCustomers = 0.00, totalSpentOnCustomers = 0.00, averageSpentOnCustomers = 0.00;
			Double sumTotalSpentOnCustomers = 0.00, sumAverageSpentOnCustomers = 0.00;
			Integer sumNoOfOrders = 0;
			int noOfOrders = 0;
			
			//Map<String, String> map2 = new HashMap<String, String>();
			Map<Integer, String> map2 = new HashMap<Integer, String>();
			
			for(Object[] dataObj : dataList1){
				
				totalSpentOnCustomers= 0.00;
				noOfOrders = 0;
				//String key = String.valueOf(dataObj[1]).replaceAll("\\s", "").toUpperCase();
				Integer key = Integer.valueOf(dataObj[13].toString());
				
				String value = map2.get(key); 
				//totalOrderSpent + ":"+noOfOrders;
				
				if(value == null || value == ""){
					//spentOnCustomers = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString())); //Order Total
					if(dataObj[7] != null){
						redeemedPoint = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString())); //RedeemedPoints
					}
					if(dataObj[8] != null){
						loyalFanDisc = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString())); //LoyalFanDiscount
					}
					if(dataObj[9] != null){
						appDisc = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString())); //AppDiscount
					}
					if(dataObj[10] != null){
						promotionalDisc = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString())); //PromotionalDiscount
					}
					spentOnCustomers = redeemedPoint + loyalFanDisc + appDisc + promotionalDisc; //Sum of (RedeemedPoints, LoyalFanDiscount, AppDiscount, PromotionalDiscount)
					value=spentOnCustomers+":"+1;
				}else{
					
					noOfOrders = Integer.parseInt(value.split(":")[1]);
					totalSpentOnCustomers = Double.valueOf(value.split(":")[0]);					
					//spentOnCustomers = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString())); //Order Total
					if(dataObj[7] != null){
						redeemedPoint = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString())); //RedeemedPoints
					}
					if(dataObj[8] != null){
						loyalFanDisc = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString())); //LoyalFanDiscount
					}
					if(dataObj[9] != null){
						appDisc = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString())); //AppDiscount
					}
					if(dataObj[10] != null){
						promotionalDisc = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString())); //PromotionalDiscount
					}
					spentOnCustomers = redeemedPoint + loyalFanDisc + appDisc + promotionalDisc; //Sum of (RedeemedPoints, LoyalFanDiscount, AppDiscount, PromotionalDiscount)
					totalSpentOnCustomers = totalSpentOnCustomers + spentOnCustomers;
					noOfOrders++;
					value=totalSpentOnCustomers+":"+noOfOrders;
				}
				
				map2.put(key, value);
				
			}
			
			//Set<String> custNameSet = new HashSet<String>();
			Set<Integer> custNoSet = new HashSet<Integer>();
			
			for(Object[] dataObj : dataList1){
				Row rowhead = ssSheet1.createRow((int)rowCount);
				rowCount++;
				//String key = String.valueOf(dataObj[1]).replaceAll("\\s", "").toUpperCase();
				Integer key = Integer.valueOf(dataObj[13].toString());
				
				//if(custNameSet.add(key)){
				if(custNoSet.add(key)){
					
					String value = map2.get(key);
					
					int noOfOrder = Integer.parseInt(value.split(":")[1]);
					totalSpentOnCustomers = Double.valueOf(value.split(":")[0]);
					averageSpentOnCustomers = totalSpentOnCustomers / noOfOrder;
					
					sumTotalSpentOnCustomers = sumTotalSpentOnCustomers + totalSpentOnCustomers;
					sumNoOfOrders = sumNoOfOrders + noOfOrder;
					
					rowhead.createCell(1).setCellValue(dataObj[1].toString());
					rowhead.createCell(11).setCellValue(Util.getRoundedValue(totalSpentOnCustomers));
					rowhead.createCell(12).setCellValue(Util.getRoundedValue(averageSpentOnCustomers));
				}else{
					rowhead.createCell(1).setCellValue("");
					rowhead.createCell(11).setCellValue("");
					rowhead.createCell(12).setCellValue("");
				}
				
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
//				if(dataObj[1] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}				
				j++;
				
				if(dataObj[2] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[3].toString()));
					cell.setCellStyle(cellStyle);
				}else{					
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					orderQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[4].toString()));
					sumOrderQuantity = sumOrderQuantity + orderQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderQuantity));
				}else{
					sumOrderQuantity = sumOrderQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[5].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					earnedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[6].toString()));
					sumEarnedPoints = sumEarnedPoints + earnedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(earnedPoints));
				}else{
					sumEarnedPoints = sumEarnedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					redeemedPoints = Util.getRoundedValue(Double.parseDouble(dataObj[7].toString()));
					sumRedeemedPoints = sumRedeemedPoints + redeemedPoints;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(redeemedPoints));
				}else{
					sumRedeemedPoints = sumRedeemedPoints + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					loyalFanDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[8].toString()));
					sumLoyalFanDiscount = sumLoyalFanDiscount + loyalFanDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(loyalFanDiscount));
				}else{
					sumLoyalFanDiscount = sumLoyalFanDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[9] != null){
					appDiscount = Util.getRoundedValue(Double.parseDouble(dataObj[9].toString()));
					sumAppDiscount = sumAppDiscount + appDiscount;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(appDiscount));
				}else{
					sumAppDiscount = sumAppDiscount + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[10] != null){
					promotionalDiscounts = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumPromotionalDiscounts = sumPromotionalDiscounts + promotionalDiscounts;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(promotionalDiscounts));
				}else{
					sumPromotionalDiscounts = sumPromotionalDiscounts + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
//				if(dataObj[11] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
//				
//				if(dataObj[12] != null){
//					rowhead.createCell((int) j).setCellValue(dataObj[12].toString());
//				}else{
//					rowhead.createCell((int) j).setCellValue("");
//				}
//				j++;
			}
			
			if(dataList1 != null && dataList1.size() > 0){
				Row rowhead =   ssSheet1.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;				
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderQuantity));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumEarnedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRedeemedPoints));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumLoyalFanDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAppDiscount));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumPromotionalDiscounts));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalSpentOnCustomers));
				j++;
				
				sumAverageSpentOnCustomers = sumTotalSpentOnCustomers / sumNoOfOrders;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAverageSpentOnCustomers));
				j++;
			}
			
			//RTF Customer Acquisition Cost Report
			List<String> headerList2 = new ArrayList<String>();
			headerList2.add("CustomerName");
			headerList2.add("No Of Orders");
			headerList2.add("Total Order Value");
			headerList2.add("Customer Acquisition Cost");
			headerList2.add("Average Cost Per Order");
		
			Sheet ssSheet2 = workbook.createSheet("AcquisitionCost");
			List<Object[]> dataList2 = DAORegistry.getEventDAO().getRTFCustomerAcquisitionReport(yearStr, selPlatform);			
			ExcelUtil.generateExcelHeaderRow(headerList2, ssSheet2);			
						
			orders = 0.00; sumOrders = 0.00; totalOrderValue = 0.00; sumTotalOrderValue = 0.00;
			Double acquisCost = 0.00, sumAcquisCost = 0.00, avgCostPerOrder = 0.00, sumAvgCostPerOrder = 0.00;
			j = 0;
			rowCount = 1;
			
			for(Object[] dataObj : dataList2){				
				Row rowhead = ssSheet2.createRow((int)rowCount);
				rowCount++;
				j = 0;
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[1] != null){
					orders = Util.getRoundedValue(Double.parseDouble(dataObj[1].toString()));
					sumOrders = sumOrders + orders;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orders));
				}else{
					sumOrders = sumOrders + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					totalOrderValue = Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
					sumTotalOrderValue = sumTotalOrderValue + totalOrderValue;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(totalOrderValue));
				}else{
					sumTotalOrderValue = sumTotalOrderValue + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[3] != null){
					acquisCost = Util.getRoundedValue(Double.parseDouble(dataObj[3].toString()));
					sumAcquisCost = sumAcquisCost + acquisCost;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(acquisCost));
				}else{
					sumAcquisCost = sumAcquisCost + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[4] != null){
					avgCostPerOrder = Util.getRoundedValue(Double.parseDouble(dataObj[4].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(avgCostPerOrder));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			if(dataList2 != null && dataList2.size() > 0){
				Row rowhead =   ssSheet2.createRow((int)rowCount);
				j = 0;
				
				rowhead.createCell((int) j).setCellValue("");
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrders));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTotalOrderValue));
				j++;
				
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAcquisCost));
				j++;
				
				sumAvgCostPerOrder = sumAcquisCost/sumOrders;
				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumAvgCostPerOrder));
				j++;				
			}
			
			List<Object[]> dataList3 = DAORegistry.getEventDAO().getRTFCustomerAcquisitionSummaryReport(yearStr, selPlatform);
			Double avgCostOrder = 0.0;
			rowCount++;
			rowCount++;
			
			for(Object[] dataObj : dataList3){				
				Row rowhead = ssSheet2.createRow((int)rowCount);
				rowCount++; 
				j = 0;
				
				if(dataObj[1] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[1].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[2] != null){
					avgCostOrder = Util.getRoundedValue(Double.parseDouble(dataObj[2].toString()));
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(avgCostOrder));
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_CUSTOMER_SPENT_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/LoadRTFTMATSeatAllocationReport"})
	public String loadRTFTMATSeatAllocationReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session) throws IOException{
				
		return "page-tmat-seat-allocation-report";
	}
	
	/*@RequestMapping({"/DownloadRTFTMATSeatAllocationReport"})
	public void downloadRTFTMATSeatAllocationReport(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			String artistIdStr = request.getParameter("artistId");
			
			String artistName = null;
			Artist artist = null;
			if(artistIdStr != null && !artistIdStr.equals("")) {
				artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistIdStr));
			}
			if(artist != null){
				artistName = artist.getName();
			}
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Order Id");
			headerList.add("Order Date");
			headerList.add("Order Type");
			headerList.add("Event Name");
			headerList.add("Event Date");
			headerList.add("Event Time");
			headerList.add("Venue Name");
			headerList.add("Venue City");
			headerList.add("Venue State");
			headerList.add("Venue Country");
			headerList.add("Order Total");
			headerList.add("Zone");
			headerList.add("RTF Quantity");
			headerList.add("TMAT Section");
			headerList.add("TMAT Row");
			headerList.add("TMAT Quantity");
			headerList.add("TMAT Price");
			headerList.add("Site Id");
			headerList.add("TMAT Ticket Created Date");
			headerList.add("TMAT Ticket Last Updated");
			headerList.add("TMAT Ticket Status");
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet = workbook.createSheet("RTFTMATSeatAllocation");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet);
			List<Object[]> dataList = DAORegistry.getEventDAO().getRTFTMATSeatAllocationReport(artistName);
			
			CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle cellStyle = workbook.createCellStyle();				
			CellStyle cellStyleWithHourMinute = workbook.createCellStyle();
			cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy"));
			cellStyleWithHourMinute.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
			
			Double orderTotal = 0.0, sumOrderTotal = 0.0, RTFQuantity = 0.0, sumRTFQuantity = 0.0;
			Double TMATQuantity = 0.0, sumTMATQuantity = 0.0, TMATPrice = 0.0, sumTMATPrice = 0.0;
			int j = 0;
			int rowCount = 1;
			
			for(Object[] dataObj : dataList){
				
				Row rowhead =   ssSheet.createRow((int)rowCount);				
				rowCount++;
				j = 0;
				
				if(dataObj[0] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[0].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[1] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[1].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
								
				if(dataObj[2] != null){					
					rowhead.createCell((int) j).setCellValue(dataObj[2].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}				
				j++;
				
				if(dataObj[3] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[3].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				
				
				if(dataObj[4] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[4].toString()));
					cell.setCellStyle(cellStyle);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[5] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[5].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[6] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[6].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[7] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[7].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[8] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[8].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;				

				if(dataObj[9] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[9].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
								
				if(dataObj[10] != null){
					orderTotal = Util.getRoundedValue(Double.parseDouble(dataObj[10].toString()));
					sumOrderTotal = sumOrderTotal + orderTotal;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(orderTotal));
				}else{
					sumOrderTotal = sumOrderTotal + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[11] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[11].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[12] != null){					
					RTFQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[12].toString()));
					sumRTFQuantity = sumRTFQuantity + RTFQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(RTFQuantity));
				}else{
					sumRTFQuantity = sumRTFQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
						
				if(dataObj[13] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[13].toString());
					
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[14] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[14].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[15] != null){
					TMATQuantity = Util.getRoundedValue(Double.parseDouble(dataObj[15].toString()));
					sumTMATQuantity = sumTMATQuantity + TMATQuantity;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(TMATQuantity));
				}else{
					sumTMATQuantity = sumTMATQuantity + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[16] != null){
					TMATPrice = Util.getRoundedValue(Double.parseDouble(dataObj[16].toString()));
					sumTMATPrice = sumTMATPrice + TMATPrice;
					rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(TMATPrice));
				}else{
					sumTMATPrice = sumTMATPrice + 0;
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[17] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[17].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[18] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[18].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[19] != null){
					Cell cell = rowhead.createCell((int) j);
					cell.setCellValue(new Date(dataObj[19].toString()));
					cell.setCellStyle(cellStyleWithHourMinute);
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
				
				if(dataObj[20] != null){
					rowhead.createCell((int) j).setCellValue(dataObj[20].toString());
				}else{
					rowhead.createCell((int) j).setCellValue("");
				}
				j++;
			}
			
//			if(dataList != null && dataList.size() > 0){
//				Row rowhead =   ssSheet.createRow((int)rowCount);
//				j = 0;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;				
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;				
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumOrderTotal));
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumRTFQuantity));
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTMATQuantity));
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue(Util.getRoundedValue(sumTMATPrice));
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//				
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//
//				rowhead.createCell((int) j).setCellValue("");
//				j++;
//			}
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+Constants.RTF_TMAT_SEAT_ALLOCATION_REPORT+"." +Constants.EXCEL_EXTENSION);
			
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	@RequestMapping({"/DownloadCustomerStatsReport"})
	public void downloadCustomerStatsReport(HttpServletRequest request, HttpServletResponse response){
		try{
			
			String userId = request.getParameter("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("userId", userId);
			
			String data = Util.getObject(map, Constants.BASE_URL+Constants.GET_CUSTOMER_STATS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerStats customerStats = gson.fromJson(((JsonObject)jsonObject.get("customerStats")), CustomerStats.class);
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			
			if(null != customerStats.getError() || customerStats.getStatus() < 1) {
				List<String> headerList = new ArrayList<String>();
				headerList.add(customerStats.getError().getDescription());
				Sheet ssSheet =  workbook.createSheet();
				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
				 
			}else {
				List<String> headerList = new ArrayList<String>();
				headerList.add("Customer ID");
				headerList.add("User ID");
				headerList.add("Name");
				headerList.add("Email");
				headerList.add("SignUp Date");
				headerList.add("SignUp Device ID");
				headerList.add("Referral Code");
				headerList.add("Is OTP Verified");
				headerList.add("Is Bot");
				headerList.add("No Of Lives");
				headerList.add("Contest Played");
				headerList.add("Contest Wins");
				headerList.add("No Of Questions");
				headerList.add("No Of Questions Answered");
				headerList.add("Correct Answered Question");
				headerList.add("Active Rewards");
				headerList.add("Pending Rewards");
				headerList.add("Spent Rewards");
				headerList.add("Total Purchase Rewards");
				headerList.add("Contest Games Rewards");
				headerList.add("Contest Referral Rewards");
				headerList.add("No Of Customers Referred");
				
				Sheet ssSheet =  workbook.createSheet();
				ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
				
				int j = 0;
				int rowCount = 1;
				
				for(CustomerStatistics statistics : customerStats.getCustomerStatistics()) {
					
					Row rowhead =   ssSheet.createRow((int)rowCount);
					 
					rowCount++;
					j = 0;
					 
					rowhead.createCell((int) j).setCellValue(statistics.getCustomerId().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getUserId());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getFullName());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getEmail());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getSignupDate());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getSignUpDeviceId());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getReferrerCode());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getIsOtpVerified());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getIsBot());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getQuizCustomerLives().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getTotalContestPlayed().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getTotalContestWins().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getTotalNoOfQuestions().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getNoOfQuestionAnswered().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getCorrectAnsQuestions().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getCurrentActiveRewards().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getPendingPurchase().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getSpendRewards().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getPurchaseRewards().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getContestGameRewards().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getReferralRewards().toString());
					j++;
					
					rowhead.createCell((int) j).setCellValue(statistics.getNoOfCustomerReferred().toString());
					j++;
				}
			}
			
			
			 
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=CustomerReferralChainReport-"+userId+"." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/*@RequestMapping({"/DownloadBotReport"})
	public void downloadBotReport(HttpServletRequest request, HttpServletResponse response){
		try{
			
			
			Map<String, String> map = Util.getParameterMap(request);
			String data = Util.getObject(map, Constants.BASE_URL+Constants.GET_BOTS_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerStats customerStats = gson.fromJson(((JsonObject)jsonObject.get("customerStats")), CustomerStats.class);
			
			List<String> headerList = new ArrayList<String>();
			headerList.add("Customer ID");
			headerList.add("User ID");
			headerList.add("Email");
			headerList.add("SignUp Date");
			headerList.add("SignUp Platform");
			headerList.add("SignUp Device ID");
			headerList.add("Referral Code");
			headerList.add("Is OTP Verified");
			headerList.add("AllDevices");
			headerList.add("IPAddress"); 
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("Bot Details");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
			
			int j = 0;
			int rowCount = 1;
			
			for(BotDetail obj : customerStats.getBotList()) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				 
				rowCount++;
				j = 0;
				 
				rowhead.createCell((int) j).setCellValue(obj.getCustomerId().toString());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getUserId());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getEmail());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getSignupDate());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getSignUpPlatform());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getSignUpDeviceId());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getReferrerCode());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getOtpVerified());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getAllLoggedInDevices());
				j++;
				
				rowhead.createCell((int) j).setCellValue(obj.getAllIPAddress());
				j++;
			 
			}
			
			
			headerList = new ArrayList<String>();
			headerList.add("Emails");
			
			ssSheet =  workbook.createSheet("Unique-Emails");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
			
			j = 0;
			rowCount = 1;
			
			for(BotDetail obj : customerStats.getBotList()) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				
				rowhead.createCell((int) j).setCellValue(obj.getEmail());
				j++;
			 
			}
			
			headerList = new ArrayList<String>();
			headerList.add("ReferralCode");
			
			ssSheet =  workbook.createSheet("Unique-Referral-Code");
			ExcelUtil.generateExcelHeaderRow(headerList, ssSheet); 
			
			j = 0;
			rowCount = 1;
			
			for(String obj : customerStats.getReferrlCodeList()) {
				
				Row rowhead =   ssSheet.createRow((int)rowCount);
				rowCount++;
				j = 0;
				
				rowhead.createCell((int) j).setCellValue(obj);
				j++;
			 
			}
			 
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=RTFBotsReport." +Constants.EXCEL_EXTENSION);
			workbook.write(response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	@RequestMapping(value = "/GetSuperFanStarsReport")
	public String getSuperFanStarsReport(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			 
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+"Reports/"+Constants.TICTRACKER_API_SUPERFAN_STARS_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("report", gson.toJson(genericResponseDTO.getReportList()));
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("report", gson.toJson(genericResponseDTO.getReportList()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "body-superfan-stars-report";
	}
	
	@RequestMapping(value = "/GetPollingStatsReport")
	public String getPollingStatsReport(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			String videoIds = request.getParameter("videoIds"); 
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("videoIds", videoIds);
			map.put("fromDate", fromDate); 
			map.put("toDate", toDate); 
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+"Reports/"+Constants.TICTRACKER_API_POLLING_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("report", gson.toJson(genericResponseDTO.getReportList()));
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("report", gson.toJson(genericResponseDTO.getReportList()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "body-polling-report";
	}
	
	@RequestMapping(value = "/GetTicketCostReport")
	public String getTicketCostReport(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+"Reports/"+Constants.TICTRACKER_API_TICKET_COST_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("MonthWiseInvoiceStatus",gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseInvoiceStatus")));
				model.addAttribute("MonthWiseTicketCost", gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseTicketCost")));
				model.addAttribute("MonthWiseTicketQuantity", gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseTicketQuantity")));
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("MonthWiseInvoiceStatus", gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseInvoiceStatus")));
				model.addAttribute("MonthWiseTicketCost", gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseTicketCost")));
				model.addAttribute("MonthWiseTicketQuantity", gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseTicketQuantity")));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "body-ticket-cost-report";
	}
	
	@RequestMapping(value = "/GetBotsCustomerReport")
	public String getBotsCustomerReport(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			 
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);

			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+"Reports/"+Constants.TICTRACKER_API_BOTS_CUST_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("MonthWiseBotsCust",gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseBotsCust")));
				model.addAttribute("QuaterlyWiseBotsCust",gson.toJson(genericResponseDTO.getReportMap().get("QuaterlyWiseBotsCust")));
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("MonthWiseBotsCust", gson.toJson(genericResponseDTO.getReportMap().get("MonthWiseBotsCust")));
				model.addAttribute("QuaterlyWiseBotsCust", gson.toJson(genericResponseDTO.getReportMap().get("QuaterlyWiseBotsCust")));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "body-bots-customer-report";
	}
	
	@RequestMapping(value = "/GetContestParticipantReport")
	public String getContestParticipantReport(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			 
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+"Reports/"+Constants.TICTRACKER_API_CONTEST_PARTICIPANT_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("contestParticipant",gson.toJson(genericResponseDTO.getReportMap().get("contestParticipant")));
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("contestParticipant", gson.toJson(genericResponseDTO.getReportMap().get("contestParticipant")));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "body-contest-participant-report";
	}
	
	
	@RequestMapping(value = "/GetQueWiseUserAnswerCountReport")
	public String getQueWiseUserAnswerCountReport(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			 
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+"Reports/"+Constants.TICTRACKER_API_QUE_WISE_USER_ANS_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("answeredForEachGame",gson.toJson(genericResponseDTO.getReportMap().get("answeredForEachGame")));
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("answeredForEachGame", gson.toJson(genericResponseDTO.getReportMap().get("answeredForEachGame")));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "body-question-answer-user-report";
	}
}
