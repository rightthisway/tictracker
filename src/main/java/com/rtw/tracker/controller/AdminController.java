package com.rtw.tracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtf.ecomerce.pojo.RTFSellerlDTO;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.pojos.AddUserDTO;
import com.rtw.tracker.pojos.AuditUsersDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistDTO;
import com.rtw.tracker.pojos.AutoCompleteVenueDTO;
import com.rtw.tracker.pojos.EditUsersDTO;
import com.rtw.tracker.pojos.EmailBlastCustomersDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.ManageUsersDTO;
import com.rtw.tracker.pojos.ManualFedexDTO;
import com.rtw.tracker.pojos.PaypalTrackingDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
@RequestMapping({"/Admin"})
public class AdminController {
	
	//@Autowired
	//private SessionRegistry sessionRegistry;
	
	@RequestMapping({"/ManageUsers"})
	public String loadManageUsersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_USERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ManageUsersDTO manageUsersDTO = gson.fromJson(((JsonObject)jsonObject.get("manageUsersDTO")), ManageUsersDTO.class);
			
			if(manageUsersDTO.getStatus() == 1){				
				model.addAttribute("msg", gson.toJson(manageUsersDTO.getMessage()));
				model.addAttribute("status", status);
			}else{
				model.addAttribute("msg", manageUsersDTO.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("trackerUserList", manageUsersDTO.getTrackerUserCustomDTO());
				model.addAttribute("pagingInfo", manageUsersDTO.getPaginationDTO());
			}else{
				model.addAttribute("trackerUserList", gson.toJson(manageUsersDTO.getTrackerUserCustomDTO()));
				model.addAttribute("pagingInfo", gson.toJson(manageUsersDTO.getPaginationDTO()));
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-admin-manage-users";
	}
	
	/*
	@RequestMapping({"/GetManageUsers"})
	public void loadManageUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONObject returnObject = new JSONObject();
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter);
			Collection<TrackerUser> trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRoles("ROLE_SUPER_ADMIN", "ROLE_USER", status, filter,pageNo);
			Integer count = 0;
			
			if(trackerUserList != null && trackerUserList.size() > 0){
				count = trackerUserList.size();
			}else if(trackerUserList == null || trackerUserList.size() <= 0){
				returnObject.put("msg", "No "+status+" Users found.");
			}
			returnObject.put("trackerUserList", JsonWrapperUtil.getMangeUsersArray(trackerUserList));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}
	}
	*/
	
	/*
	@RequestMapping({"/UsersExportToExcel"})
	public void usersToExport(HttpServletRequest request, HttpServletResponse response){
		String user = request.getParameter("user");
		String status = request.getParameter("status");
		String headerFilter = request.getParameter("headerFilter");
		String roles = "";
		Map<Integer, AffiliateCashReward> cashRewardMapByUserId = new HashMap<Integer, AffiliateCashReward>();
		Collection<TrackerUser> trackerUserList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter); 
			if(user != null && user.equalsIgnoreCase("USER")){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRoles("ROLE_SUPER_ADMIN", "ROLE_USER", status, filter, null);
			}else if(user != null && user.equalsIgnoreCase("BROKER")){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_BROKER",status,filter,null);
			}else if(user != null && user.equalsIgnoreCase("AFFILIATE")){
				trackerUserList = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_AFFILIATES", status, filter, null);
				Collection<AffiliateCashReward> rewardList = DAORegistry.getAffiliateCashRewardDAO().getAll();
				
				for (AffiliateCashReward affiliateCashReward : rewardList) {
					cashRewardMapByUserId.put(affiliateCashReward.getUser().getId(), affiliateCashReward);
				}
			}
			
			if(trackerUserList!=null && !trackerUserList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(user);
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("User Id");
				header.createCell(1).setCellValue("User Name");
				header.createCell(2).setCellValue("First Name");
				header.createCell(3).setCellValue("Last Name");
				header.createCell(4).setCellValue("Email");
				header.createCell(5).setCellValue("Phone");
				header.createCell(6).setCellValue("Status");
				if(user != null && user.equalsIgnoreCase("USER")){
					header.createCell(7).setCellValue("Role");
				}else if(user != null && user.equalsIgnoreCase("BROKER")){
					header.createCell(7).setCellValue("Broker Id");
					header.createCell(8).setCellValue("Company Name");
				}else if(user != null && user.equalsIgnoreCase("AFFILIATE")){
					header.createCell(7).setCellValue("Promotional Code");
					header.createCell(8).setCellValue("Active Cash");
					header.createCell(9).setCellValue("Pending Cash");
				}
				Integer i=1;
				for(TrackerUser trackerUser : trackerUserList){
					roles = "";
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(trackerUser.getId());
					row.createCell(1).setCellValue(trackerUser.getUserName()!=null?trackerUser.getUserName():"");
					row.createCell(2).setCellValue(trackerUser.getFirstName()!=null?trackerUser.getFirstName():"");
					row.createCell(3).setCellValue(trackerUser.getLastName()!=null?trackerUser.getLastName():"");
					row.createCell(4).setCellValue(trackerUser.getEmail()!=null?trackerUser.getEmail():"");
					row.createCell(5).setCellValue(trackerUser.getPhone()!=null?trackerUser.getPhone():"");
					if(trackerUser.getStatus() != null && trackerUser.getStatus() == true){
						row.createCell(6).setCellValue("ACTIVE");
					}else{
						row.createCell(6).setCellValue("DEACTIVE");
					}
					
					for(Role role: trackerUser.getRoles()){
						roles += role.getName();
					}
					
					if(user != null && user.equalsIgnoreCase("USER")){
						row.createCell(7).setCellValue(roles);
					}else if(user != null && user.equalsIgnoreCase("BROKER")){
						row.createCell(7).setCellValue(trackerUser.getBroker()!= null?trackerUser.getBroker().getId().toString():"");
						row.createCell(8).setCellValue(trackerUser.getBroker()!= null?trackerUser.getBroker().getCompanyName():"");
					}else if(user != null && user.equalsIgnoreCase("AFFILIATE")){
						row.createCell(7).setCellValue(trackerUser.getPromotionalCode()!=null?trackerUser.getPromotionalCode():"");
						row.createCell(8).setCellValue(cashRewardMapByUserId.get(trackerUser.getId())!=null? cashRewardMapByUserId.get(trackerUser.getId()).getActiveCash(): 0.00);
						row.createCell(9).setCellValue(cashRewardMapByUserId.get(trackerUser.getId())!=null? cashRewardMapByUserId.get(trackerUser.getId()).getPendingCash(): 0.00);
					}
					
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+user+".xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	@RequestMapping({"/AddUser"})
	public String addUserPage(@ModelAttribute TrackerUser trackerUser, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
						
		try{
			//String returnMessage = "";
			String action = request.getParameter("action");
			String roleAffiliateBroker = request.getParameter("role");
			String sellerId = request.getParameter("seller");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Gson gson = GsonCustomConfig.getGsonBuilder();
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("action", action);
			paramMap.put("roleAffiliateBroker", roleAffiliateBroker);
			paramMap.put("sessionUser", userName);			
			
			if(action != null && action.equals("action")){
				String[] roles = request.getParameterValues("role");
				String rePassword = request.getParameter("repassword");
				paramMap.put("sellerId", sellerId);
				paramMap.put("userName", trackerUser.getUserName());
				paramMap.put("firstName", trackerUser.getFirstName());
				paramMap.put("lastName", trackerUser.getLastName());
				paramMap.put("email", trackerUser.getEmail());
				paramMap.put("password", trackerUser.getPassword());
				paramMap.put("rePassword", rePassword);
				paramMap.put("phone", trackerUser.getPhone());
				paramMap.put("role", gson.toJson(roles));
								
				/*String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				
				if(trackerUser.getUserName() == null || trackerUser.getUserName().isEmpty()){
					returnMessage= "Username can't be blank";
				}else if(trackerUser.getFirstName() == null || trackerUser.getFirstName().isEmpty()){
					returnMessage= "Firstname can't be blank";
				}else if(trackerUser.getLastName() == null || trackerUser.getLastName().isEmpty()){
					returnMessage= "Lastname can't be blank";
				}else if(trackerUser.getEmail() == null || trackerUser.getEmail().isEmpty()){
					returnMessage= "Email can't be blank";
				}else if(trackerUser.getPassword() == null || trackerUser.getPassword().isEmpty()){
					returnMessage= "Password can't be blank";
				}else if(rePassword == null || rePassword.isEmpty()){
					returnMessage = "Re-Password can't be blank";
				}else if(!trackerUser.getPassword().equals(rePassword)){
					returnMessage = "Password and Re-Password must match.";
				}else if(!validateEMail(trackerUser.getEmail())){
					returnMessage = "Invalid Email.";
				}else if(roles != null && roles.length == 0){
					returnMessage = "Please choose at least one Role.";
				}
				
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(companyName == null || companyName.isEmpty()){
							returnMessage = "Company can't be blank.";
						}
						if(serviceFees == null || serviceFees.isEmpty()){
							returnMessage = "Service Fees can't be blank.";
						}
					}
				}*/
								
				/*if(returnMessage.isEmpty()){
					TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(trackerUser.getUserName(), trackerUser.getEmail());
					TrackerBrokers trackerBrokersDb = null;
					String userName = SecurityContextHolder.getContext().getAuthentication().getName();
					
					if(trackerUserDb != null){
						returnMessage = "There is already an user with this Username or Email.";
					}else{
						
						// adding user to database
						if(trackerUser.getPhone() == null || trackerUser.getPhone().isEmpty()){
							trackerUser.setPhone(null);
						}else{
							trackerUser.setPhone(trackerUser.getPhone());
						}
						Set<Role> roleList = new HashSet<Role>();
						for(String role : roles){
							Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());
							
							//if(role.toUpperCase().equals("ROLE_BROKER")){
								//trackerBrokersDb = new TrackerBrokers();
								//trackerBrokersDb.setCompanyName(companyName);
								//trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
								//DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
								
								//trackerUser.setBroker(trackerBrokersDb);
							//}
							
							//if(role.toUpperCase().equals("ROLE_AFFILIATES")){
								//trackerUser.setPromotionalCode(Util.generateCustomerReferalCode());
							//}
							roleList.add(roleDb);
						}
						trackerUser.setPassword(encryptPwd(trackerUser.getPassword()));
						trackerUser.setRoles(roleList);						
						trackerUser.setStatus(true);
						trackerUser.setCreateDate(new Date());
						trackerUser.setCreatedBy(userName);
						DAORegistry.getTrackerUserDAO().save(trackerUser);
						
						returnMessage = "User added successfully.";
						map.put("successMessage", returnMessage);
						
						//Roles For Display Purpose - Newly Added
						for(String role : roles){
							if(role.toUpperCase().equals("ROLE_SUPER_ADMIN")){
								map.put("roleAdmin", role.toUpperCase());
							}
							if(role.toUpperCase().equals("ROLE_USER")){
								map.put("roleUser", role.toUpperCase());
							}
							if(role.toUpperCase().equals("ROLE_CONTEST")){
								map.put("roleContest", role.toUpperCase());
							}
						}
						
						//Tracking User Action
						String userActionMsg = "User Created Successfully.";
						Util.userActionAudit(request, trackerUser.getId(), userActionMsg);
					}
				}else{
					map.put("errorMessage", returnMessage);
				}*/
				
			}
			//map.put("roleList", DAORegistry.getRoleDAO().getAll());
			
			String data = Util.getObject(paramMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ADD_USER);
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AddUserDTO addUserDTO = gson.fromJson(((JsonObject)jsonObject.get("addUserDTO")), AddUserDTO.class);
			
			
			Map<String, String> map1 = Util.getParameterMap(request);
			map1.put("status", "ACTIVE");
			
			String data1 = Util.getObject(map1, Constants.TICTRACKER_API_BASE_URL+Constants.GET_SELLERS);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			RTFSellerlDTO sellerDTO = gson1.fromJson(((JsonObject)jsonObject1.get("RTFSellerlDTO")), RTFSellerlDTO.class);
			
			
			
			if(addUserDTO.getStatus() == 1){
				model.addAttribute("trackerUser", addUserDTO.getTrackerUser());
				model.addAttribute("roleAffiliateBroker", addUserDTO.getRoleAffiliateBroker()); 
				model.addAttribute("roleAdmin", addUserDTO.getRoleSuperAdmin());
				model.addAttribute("roleUser", addUserDTO.getRoleUser());
				model.addAttribute("roleSeller", addUserDTO.getRoleSeller());
				model.addAttribute("roleContest", addUserDTO.getRoleContest());
				model.addAttribute("successMessage", addUserDTO.getMessage());
				model.addAttribute("status", addUserDTO.getStatus());
				model.addAttribute("sellerList", sellerDTO.getRtfSellers());
			}else{
				model.addAttribute("errorMessage", addUserDTO.getError().getDescription());
				model.addAttribute("status", addUserDTO.getStatus());
			}
						
			//model.addAttribute("trackerUser", trackerUser);
			//model.addAttribute("roleAffiliateBroker", roleAffiliateBroker!=null?roleAffiliateBroker.toUpperCase():"");
			if(roleAffiliateBroker!=null && roleAffiliateBroker.toUpperCase().equals("ROLE_BROKER")){
				return "page-admin-add-broker";
			}else if(roleAffiliateBroker!=null && roleAffiliateBroker.toUpperCase().equals("ROLE_AFFILIATES")){
				return "page-admin-add-affiliates";
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong..Please Try Again.");
		}				
		//return "redirect:ManageUsers";
		return "page-admin-add-user";
	}
	
	@RequestMapping({"/EditUser"})
	public String editUserPage(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		
		try{
			String returnMessage = "";
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			
			Gson gson = GsonCustomConfig.getGsonBuilder();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userId", userIdStr);
			map.put("action", action);
			
			/*if(userIdStr == null || userIdStr.isEmpty()){
				returnMessage = "User Id not empty";
			}
			Integer userId = Integer.parseInt(userIdStr);
			Collection<Role> roleListDB = DAORegistry.getRoleDAO().getAll();
			TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
			TrackerUser trackerUser = new TrackerUser();
			String userName = trackerUserDb.getUserName();*/
			
			if(action != null && action.equals("editUserInfo")){
				String[] roles = request.getParameterValues("role");
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				
				/*String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				
				if(firstName == null || firstName.isEmpty()){
					returnMessage= "Firstname can't be blank";
				}else if(lastName == null || lastName.isEmpty()){
					returnMessage= "Lastname can't be blank";
				}else if(email == null || email.isEmpty()){
					returnMessage= "Email can't be blank";
				}else if(!validateEMail(email)){
					returnMessage = "Invalid Email.";
				}else if(roles != null && roles.length == 0){
					returnMessage = "Please choose at least one Role.";
				}
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(companyName == null || companyName.isEmpty()){
							returnMessage = "Company can't be blank.";
						}
						if(serviceFees == null || serviceFees.isEmpty()){
							returnMessage = "Service Fees can't be blank.";
						}
					}
				}
				map.put("companyName", companyName);
				map.put("serviceFees", serviceFees);*/
				
				map.put("role", gson.toJson(roles));
				map.put("firstName", firstName);
				map.put("lastName", lastName);
				map.put("email", email);
				map.put("phone", phone);				
				
			}else if(action != null && action.equals("changePassword")){
				String password = request.getParameter("password");
				String rePassword = request.getParameter("repassword");
				
				/*if(password == null || password.isEmpty()){
					returnMessage= "Password can't be blank";
				}else if(rePassword == null || rePassword.isEmpty()){
					returnMessage = "Re-Password can't be blank";
				}else if(!password.equals(rePassword)){
					returnMessage = "Password and Re-Password must match.";
				}*/

				map.put("password", password);
				map.put("repassword", rePassword);
				
			}else if(action != null && action.equals("logoutUser")){
				  /*for (Object principal : sessionRegistry.getAllPrincipals()) {
				    	if(principal.equals(userName)){
				    		//
				    	}
				  }*/
				  
				  /*List<Object> sessionInformations = sessionRegistry.getAllPrincipals();
					if (sessionInformations != null) {
						System.out.println("==="+sessionInformations.size());
						
						//System.out.println("yes");
						return null;
					} else {
						System.out.println("no");
						return null;
					}*/
					
					
				//Log out the user programatically
				/*Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

				if (auth != null) {
					new SecurityContextLogoutHandler().logout(request,
							response, auth);
				}*/
				/*
				boolean wasLoggedIn = SessionListener.invalidateSessionsForUser(userName);
				if (wasLoggedIn) {
					map.put("successMessage", "User " + userName + " has been logged out.");
				} else {
					map.put("userActionsMessage", "User " + userName + " was not logged in.");				
				}*/
			}
			
			if(StringUtils.isEmpty(returnMessage)){				
				String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_USER);
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				EditUsersDTO editUsersDTO = gson.fromJson(((JsonObject)jsonObject.get("editUsersDTO")), EditUsersDTO.class);
				
				
				Map<String, String> map1 = Util.getParameterMap(request);
				map1.put("status", "ACTIVE");
				
				String data1 = Util.getObject(map1, Constants.TICTRACKER_API_BASE_URL+Constants.GET_SELLERS);
				Gson gson1 = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
				RTFSellerlDTO sellerDTO = gson1.fromJson(((JsonObject)jsonObject1.get("RTFSellerlDTO")), RTFSellerlDTO.class);
				
				
				if(editUsersDTO.getError() == null){
					//model.addAttribute("trackerBroker", editUsersDTO.getTrackerBrokerCustomDTO());
					model.addAttribute("rolesList", editUsersDTO.getRolesList());
					model.addAttribute("trackerUser", editUsersDTO.getEditTrackerUserCustomDTO());
					model.addAttribute("userId", userIdStr);
					model.addAttribute("pagingInfo", editUsersDTO.getPaginationDTO());
					model.addAttribute("successMessage", editUsersDTO.getMessage());
					model.addAttribute("status", editUsersDTO.getEditTrackerUserCustomDTO().getStatus());
					model.addAttribute("sellerList", sellerDTO.getRtfSellers());
				}else{
					model.addAttribute("errorMessage", editUsersDTO.getError().getDescription());
					model.addAttribute("status", editUsersDTO.getEditTrackerUserCustomDTO().getStatus());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*
	@RequestMapping({"/EditUser"})
	public String editUserPage(@ModelAttribute TrackerUser trackerUser,HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String userIdStr = request.getParameter("userId");
		String action = request.getParameter("action");
		System.out.println("action:: "+action);
		String returnMessage = "";
		try{
			if(userIdStr == null || userIdStr.isEmpty()){
				return "redirect:ManageUsers";
			}
			Integer userId = Integer.parseInt(userIdStr);
			//Collection<Role> roleListDB = DAORegistry.getRoleDAO().getAll();
			TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
			String userName = trackerUserDb.getUserName();
			if(action != null && action.equals("editUserInfo")){
				String[] roles = request.getParameterValues("role");
				String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				if(trackerUser.getFirstName() == null || trackerUser.getFirstName().isEmpty()){
					returnMessage= "Firstname can't be blank";
				}else if(trackerUser.getLastName() == null || trackerUser.getLastName().isEmpty()){
					returnMessage= "Lastname can't be blank";
				}else if(trackerUser.getEmail() == null || trackerUser.getEmail().isEmpty()){
					returnMessage= "Email can't be blank";
				}else if(!validateEMail(trackerUser.getEmail())){
					returnMessage = "Invalid Email.";
				}else if(roles != null && roles.length == 0){
					returnMessage = "Please choose at least one Role.";
				}
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(companyName == null || companyName.isEmpty()){
							returnMessage = "Company can't be blank.";
						}
						if(serviceFees == null || serviceFees.isEmpty()){
							returnMessage = "Service Fees can't be blank.";
						}
					}
				}
				if(returnMessage.isEmpty()){
					trackerUser.setUserName(trackerUserDb.getUserName());
					trackerUser.setId(trackerUserDb.getId());
					trackerUser.setRoles(trackerUserDb.getRoles());
					trackerUser.setPassword(trackerUserDb.getPassword());
					trackerUser.setStatus(trackerUserDb.getStatus());	
					
					TrackerBrokers trackerBrokersDb = null;
					Boolean roleFlag = false;
					Boolean roleAffiliateFlag = false;
					
					
					AffiliatePromoCodeHistory affiliatePromoHistory = null;
					
					Set<Role> roleList = new HashSet<Role>();
					for(String role : roles){
						Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());
						
						if(role.toUpperCase().equals("ROLE_BROKER")){
							if(trackerUserDb.getBroker() == null){
								trackerBrokersDb = new TrackerBrokers();
								trackerBrokersDb.setCompanyName(companyName);
								trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
								DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
								
								trackerUser.setBroker(trackerBrokersDb);
								//trackerUser.setBrokerId(trackerBrokersDb.getId());
							}
							if(trackerUserDb.getBroker() != null){
								trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
								if(trackerBrokersDb != null){
									trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
									DAORegistry.getTrackerBrokersDAO().update(trackerBrokersDb);
								}
								
								trackerUser.setBroker(trackerUserDb.getBroker());
							}
							roleFlag = true;
						}
						
						if(role.toUpperCase().equals("ROLE_AFFILIATES")){
							if(trackerUserDb.getPromotionalCode() == null || trackerUserDb.getPromotionalCode().isEmpty()){
								trackerUser.setPromotionalCode(null);
									String promoCode = Util.generateCustomerReferalCode();
									trackerUser.setPromotionalCode(promoCode);
									
									affiliatePromoHistory = new AffiliatePromoCodeHistory();
									affiliatePromoHistory.setUserId(trackerUser.getId());
									affiliatePromoHistory.setPromoCode(promoCode);
									affiliatePromoHistory.setCreateDate(new Date());
									affiliatePromoHistory.setUpdateDate(new Date());
									affiliatePromoHistory.setStatus("ACTIVE");
							}
							if(trackerUserDb.getPromotionalCode() != null){				
								trackerUser.setPromotionalCode(trackerUserDb.getPromotionalCode());								
							}
							roleAffiliateFlag = true;
						}
						roleList.add(roleDb);
					}
					trackerUser.setRoles(roleList);
					
					//Delete if Role_Broker is not available when edit.
					if(!roleFlag){
						if(trackerUserDb.getBroker() != null){
							DAORegistry.getTrackerBrokersDAO().delete(trackerUserDb.getBroker());							
						}
					}
					if(!roleAffiliateFlag){
						if(trackerUserDb.getPromotionalCode() != null){
							
							affiliatePromoHistory = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(trackerUser.getId());
							if(null != affiliatePromoHistory ){
								affiliatePromoHistory.setStatus("DELETED");
								affiliatePromoHistory.setUpdateDate(new Date());
							}
							trackerUser.setPromotionalCode(null);
						}
					}
					DAORegistry.getTrackerUserDAO().update(trackerUser);
					
					if(null != affiliatePromoHistory){
						DAORegistry.getAffiliatePromoCodeHistoryDAO().saveOrUpdate(affiliatePromoHistory);
					}
					
					trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
					map.put("successMessage", "User's details updated successfully.");
				}else{
					map.put("errorMessage", returnMessage);
				}
				//return "redirect:EditUser?userId="+userIdStr;
			}else if(action != null && action.equals("changePassword")){
				String password = request.getParameter("password");
				String rePassword = request.getParameter("repassword");
				if(password == null || password.isEmpty()){
					returnMessage= "Password can't be blank";
				}else if(rePassword == null || rePassword.isEmpty()){
					returnMessage = "Re-Password can't be blank";
				}else if(!password.equals(rePassword)){
					returnMessage = "Password and Re-Password must match.";
				}
				if(returnMessage.isEmpty()){
					trackerUserDb.setAndEncryptPassword(password);
					DAORegistry.getTrackerUserDAO().saveOrUpdate(trackerUserDb);
					trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
					map.put("successMessage", "User's password changed successfully.");
				}else{
					map.put("errorMessage", returnMessage);
				}
				//return "redirect:EditUser?userId="+userIdStr;
			}else if(action != null && action.equals("logoutUser")){
				  for (Object principal : sessionRegistry.getAllPrincipals()) {
				    	if(principal.equals(userName)){
				    		//
				    	}
				  }
				  
				  List<Object> sessionInformations = sessionRegistry.getAllPrincipals();
					if (sessionInformations != null) {
						System.out.println("==="+sessionInformations.size());
						
						//System.out.println("yes");
						return null;
					} else {
						System.out.println("no");
						return null;
					}
					
					
				//Log out the user programatically
				Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

				if (auth != null) {
					new SecurityContextLogoutHandler().logout(request,
							response, auth);
				}
				
				boolean wasLoggedIn = SessionListener.invalidateSessionsForUser(userName);
				if (wasLoggedIn) {
					map.put("successMessage", "User " + userName + " has been logged out.");
				} else {
					map.put("userActionsMessage", "User " + userName + " was not logged in.");				
				}
			}else{
				map.put("error", "Invalid action " + action);
			}
			List<String> rolesList = new ArrayList<String>();
			if(trackerUserDb.getRoles() != null && !trackerUserDb.getRoles().isEmpty()){
				for(Role role : trackerUserDb.getRoles()){
					rolesList.add(role.getName());
				}
			}
			TrackerBrokers trackerBrokersDb = null;
			if(trackerUserDb.getBroker() != null){
				trackerBrokersDb = DAORegistry.getTrackerBrokersDAO().get(trackerUserDb.getBroker().getId());
			}
			//map.put("roleListDb", roleListDB);
			map.put("trackerBroker", trackerBrokersDb);
			map.put("rolesList", rolesList);
			map.put("trackerUser", trackerUserDb);
			map.put("status", trackerUserDb.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		map.put("userId", userIdStr);
		return "page-admin-edit-user";
	}
	*/
	
	@RequestMapping({"/AuditUser"})
	public String auditUserPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String userId = request.getParameter("userId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userId", userId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUDIT_USERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AuditUsersDTO auditUsersDTO = gson.fromJson(((JsonObject)jsonObject.get("auditUsersDTO")), AuditUsersDTO.class);
			
			if(auditUsersDTO.getStatus() == 1){				
				model.addAttribute("msg", auditUsersDTO.getMessage());
				model.addAttribute("status", auditUsersDTO.getStatus());
				model.addAttribute("userAuditPagingInfo", auditUsersDTO.getAuditPaginationDTO());
				model.addAttribute("userActionList", auditUsersDTO.getUserActionDTOs());
				model.addAttribute("userEmail", auditUsersDTO.getUserEmail());
				model.addAttribute("fromDate", auditUsersDTO.getFromDate());
				model.addAttribute("toDate", auditUsersDTO.getToDate());
			}else{
				model.addAttribute("msg", auditUsersDTO.getError().getDescription());
				model.addAttribute("status", auditUsersDTO.getStatus());
			}
			
			/*JSONObject returnObject = new JSONObject();
			Integer count = 0;
			returnObject.put("status", 0);
			String fromDateStr = "";
			String toDateStr = "";
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date fromDate = cal.getTime();
			Date toDate = new Date();
			
			Collection<UserAction> userActionList = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserAuditFilter(headerFilter);
			
			if(userIdStr == null || userIdStr.isEmpty()){
				returnObject.put("msg", "User Id is empty.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
				
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
			if(trackerUser == null){
				returnObject.put("msg", "User not found.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(action != null && action.equals("search")){
				fromDateStr = request.getParameter("fromDate");
				toDateStr = request.getParameter("toDate");
				
				if(fromDateStr == null || fromDateStr.isEmpty()){
					returnObject.put("msg", "From date cant' be blank");
					IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
					return;
				}else if(toDateStr == null || toDateStr.isEmpty()){
					returnObject.put("msg", "To date cant' be blank");
					IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
					return;
				}
			}else{
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
			}
			
			userActionList = DAORegistry.getUserActionDAO()
					.getAllActionsByUserIdAndDateRange(Integer.parseInt(userIdStr), fromDateStr+" 00:00:00", toDateStr+" 23:59:59", filter, pageNo);
			count = DAORegistry.getUserActionDAO().getAllActionsByUserIdAndDateRangeCount(Integer.parseInt(userIdStr), fromDateStr+" 00:00:00", toDateStr+" 23:59:59", filter);
			
			if(userActionList == null || userActionList.size() == 0){
				returnObject.put("msg", "No User Actions Found.");
			}else{
				returnObject.put("status", 1);
			}
			returnObject.put("userEmail", trackerUser.getEmail());
			returnObject.put("userActionList", JsonWrapperUtil.getUserActionsArray(userActionList));
			returnObject.put("userAuditPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			returnObject.put("fromDate", fromDateStr);
			returnObject.put("toDate", toDateStr);
			
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}
		return "";
	}
	
	public static boolean validateEMail(String email){
		try{
			final String emailPattern = 
					"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			Pattern pattern;
			Matcher matcher;
			
			pattern = Pattern.compile(emailPattern);
			matcher = pattern.matcher(email);
			return matcher.matches();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Method to encrypt the password
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String encryptPwd(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	
	@RequestMapping("/AutoCompleteVenues")
	public void getAutoCompleteVenues(HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_VENUE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteVenueDTO autoCompleteVenueDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteVenueDTO")), AutoCompleteVenueDTO.class);
						
			if(autoCompleteVenueDTO.getStatus() == 1){
				writer.write(autoCompleteVenueDTO.getVenue());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByName(param);
		
		if (venues == null) {
			return;
		}
		if (venues != null) {
			for (Venue venue : venues) {
				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding()+"|"+venue.getCity()+", "+venue.getState()+", "+venue.getCountry()+ "\n") ;
			}
		}
		
		writer.write(strResponse);*/

	}
	
	
	@RequestMapping("/AutoCompleteArtist")
	public void getAutoCompleteArtist(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_ARTIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistDTO autoCompleteArtistDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistDTO")), AutoCompleteArtistDTO.class);
						
			if(autoCompleteArtistDTO.getStatus() == 1){
				writer.write(autoCompleteArtistDTO.getArtist());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
		if (artists == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
		writer.write(strResponse);*/
	}
	
	
	@RequestMapping(value = "/CleanedClient")
	public String getClientReportPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
	
		try{
			String sortingString = request.getParameter("sortingString");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("sortingString", sortingString);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EMAIL_BLAST_CUSTOMERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EmailBlastCustomersDTO emailBlastCustomersDTO = gson.fromJson(((JsonObject)jsonObject.get("emailBlastCustomersDTO")), EmailBlastCustomersDTO.class);
			
			if(emailBlastCustomersDTO.getStatus() == 1){
				model.addAttribute("msg", emailBlastCustomersDTO.getMessage());
				model.addAttribute("status", emailBlastCustomersDTO.getStatus());
			}else{
				model.addAttribute("msg", emailBlastCustomersDTO.getError().getDescription());
				model.addAttribute("status", emailBlastCustomersDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", emailBlastCustomersDTO.getCustomersPaginationDTO());
				model.addAttribute("customers", emailBlastCustomersDTO.getCustomersDTO());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(emailBlastCustomersDTO.getCustomersPaginationDTO()));
				model.addAttribute("customers", gson.toJson(emailBlastCustomersDTO.getCustomersDTO()));
				model.addAttribute("countries", emailBlastCustomersDTO.getCountryDTO());
				model.addAttribute("childs", emailBlastCustomersDTO.getChildDTO());
				model.addAttribute("grandChilds", emailBlastCustomersDTO.getGrandChildDTO());
				model.addAttribute("templates", emailBlastCustomersDTO.getTemplateDTO());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		/*Integer count =0;
		String cities = request.getParameter("cities");
		String state = request.getParameter("stateIds");
		String country = request.getParameter("countryIds");
		String customerType = request.getParameter("customerType");
		String venues = request.getParameter("venue");
		String productType = request.getParameter("products");
		String grandChildNames = request.getParameter("grandChildIds");
		String childIds = request.getParameter("childIds");
		String artists = request.getParameter("artists");
		String customerCat = request.getParameter("customerCategory");
		String isMailSent = request.getParameter("isMailSent");
		String fromDateFinal = null;
		String toDateFinal = null;
		List<Country> countryList=null;
		Collection<Customers> rtfCustomerList = null;
		Collection<Customers> rtwCustomerList = null;
		List<GrandChildCategory> grandChilds = null;
		List<ChildCategory> childs = null;
		try{
			GridHeaderFilters filter = new GridHeaderFilters();
			rtfCustomerList = DAORegistry.getQueryManagerDAO().getClientReportData(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,null,true,fromDateFinal,toDateFinal);
			count = DAORegistry.getQueryManagerDAO().getClientReportDataCount(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
			rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW','RTW2','TIXCITY'",grandChildNames,childIds,artists,filter,null,true,fromDateFinal,toDateFinal);
			count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW','RTW2','TIXCITY'",grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
			rtfCustomerList.addAll(rtwCustomerList);
			countryList = DAORegistry.getCountryDAO().GetUSCANADA();
			childs = DAORegistry.getChildCategoryDAO().getAll();
			grandChilds = DAORegistry.getGrandChildCategoryDAO().getAll();
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		
		Collection<EmailTemplate> templateList = DAORegistry.getEmailTemplateDAO().getAll();
		
		map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
		map.put("customers", JsonWrapperUtil.getCustomerArray(rtfCustomerList));
		map.put("countries", countryList);
		map.put("childs", childs);
		map.put("grandChilds", grandChilds);
		map.put("templates", templateList);*/
		return "page-client-report";		
	}
	
	
	
	/*@RequestMapping(value = "/GetCleanedClient", method = RequestMethod.POST)
	public String getClientReportData(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{	
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_EMAIL_BLAST_CUSTOMERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			EmailBlastCustomersDTO emailBlastCustomersDTO = gson.fromJson(((JsonObject)jsonObject.get("emailBlastCustomersDTO")), EmailBlastCustomersDTO.class);
			
			if(emailBlastCustomersDTO.getStatus() == 1){
				model.addAttribute("msg", emailBlastCustomersDTO.getMessage());
				model.addAttribute("status", emailBlastCustomersDTO.getStatus());
				model.addAttribute("pagingInfo", emailBlastCustomersDTO.getCustomersPaginationDTO());
				model.addAttribute("customers", emailBlastCustomersDTO.getCustomersDTO());
			}else{
				model.addAttribute("msg", emailBlastCustomersDTO.getError().getDescription());
				model.addAttribute("status", emailBlastCustomersDTO.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		Integer count =0;
//		String cities = request.getParameter("cities");
//		String state = request.getParameter("stateIds");
//		String country = request.getParameter("countryIds");
//		String customerType = request.getParameter("customerType");
//		String venues = request.getParameter("venue");
//		String productType = request.getParameter("products");
//		String grandChildNames = request.getParameter("grandChildIds");
//		String pageNo = request.getParameter("pageNo");
//		String artists = request.getParameter("artists");
//		String headerFilter = request.getParameter("headerFilter");
//		String childIds = request.getParameter("childIds");
//		String customerCat = request.getParameter("customerCategory");
//		String isMailSent = request.getParameter("isMailSent");
//		String fromDateStr = request.getParameter("emailBlastFromDate");
//		String toDateStr = request.getParameter("emailBlastToDate");
//		String fromDateFinal = "";
//		String toDateFinal = "";
//		Boolean isRTW=false;
//		Boolean isRTW2=false;
//		Boolean isRTF=false;
//		Boolean isTIXCITY=false;
//		Collection<Customers> rtwCustomerList = new ArrayList<Customers>();
//		Collection<Customers> rtfCustomerList = new ArrayList<Customers>();
//		JSONObject object = new JSONObject();
//		try{
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
//			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//				fromDateFinal = fromDateStr + " 00:00:00";
//				toDateFinal = toDateStr + " 23:59:59";
//			}else{
//				fromDateFinal = null;
//				toDateFinal = null;
//			}
//			if(productType!=null && !productType.isEmpty()){
//				if(productType.contains(",")){
//					String arr[] = productType.split(",");
//					for(String str : arr){
//						if(str.equalsIgnoreCase("RTW")){
//							isRTW=true;
//						}
//						if(str.equalsIgnoreCase("RTW2")){
//							isRTW2=true;
//						}
//						if(str.equalsIgnoreCase("REWARDTHEFAN")){
//							isRTF=true;
//						}
//						if(str.equalsIgnoreCase("TIXCITY")){
//							isTIXCITY=true;
//						}
//					}
//				}else{
//					if(productType.equalsIgnoreCase("RTW")){
//						isRTW=true;
//					}
//					if(productType.equalsIgnoreCase("RTW2")){
//						isRTW2=true;
//					}
//					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
//						isRTF=true;
//					}
//					if(productType.equalsIgnoreCase("TIXCITY")){
//						isTIXCITY=true;
//					}
//				}
//			}else{
//				isRTW2=true;
//				isRTF=true;
//				isRTW=true;
//				isTIXCITY=true;
//			}
//			if(isRTF){
//				rtfCustomerList = DAORegistry.getQueryManagerDAO().getClientReportData(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,pageNo,true,fromDateFinal,toDateFinal);
//				count += DAORegistry.getQueryManagerDAO().getClientReportDataCount(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
//			}
//			String products = "";
//			if(isTIXCITY && isRTW && isRTW2){
//				products="'TIXCITY','RTW','RTW2'";
//			}else if(isTIXCITY && isRTW){
//				products="'TIXCITY','RTW'";
//			}else if(isTIXCITY && isRTW2){
//				products="'TIXCITY','RTW2'";
//			}else if(isRTW && isRTW2){
//				products="'RTW','RTW2'";
//			}else if(isTIXCITY){
//				products="'TIXCITY'";
//			}else if(isRTW){
//				products="'RTW'";
//			}else if(isRTW2){
//				products="'RTW2'";
//			}
//			if(products != null && !products.isEmpty()){
//				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,products,grandChildNames,childIds,artists,filter,pageNo,true,fromDateFinal,toDateFinal);
//				count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,products,grandChildNames,childIds,artists,filter,fromDateFinal,toDateFinal);
//			}
//			
//			rtfCustomerList.addAll(rtwCustomerList);
//			object.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			object.put("customers", JsonWrapperUtil.getCustomerArray(rtfCustomerList));
//			IOUtils.write(object.toString().getBytes(), response.getOutputStream());
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		return "";		
	}*/
	
	/*@RequestMapping(value = "/ExportClientReport", method = RequestMethod.GET)
	public void eportClientReporttoExcel(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count =0;
		String cities = request.getParameter("cities");
		String state = request.getParameter("stateIds");
		String customerType = request.getParameter("customerType");
		String country = request.getParameter("countryIds");
		String venues= request.getParameter("venue");
		String productType = request.getParameter("products");
		String grandChildNames = request.getParameter("grandChildIds");
		String pageNo = request.getParameter("pageNo");
		String artists = request.getParameter("artists");
		String headerFilter = request.getParameter("headerFilter");
		String childIds = request.getParameter("childIds");
		String customerCat = request.getParameter("customerCategory");
		String isMailSent = request.getParameter("isMailSent");
		String fromDateStr = request.getParameter("emailBlastFromDate");
		String toDateStr = request.getParameter("emailBlastToDate");
		String fromDateFinal = "";
		String toDateFinal = "";
		Boolean isRTW=false;
		Boolean isRTW2=false;
		Boolean isRTF=false;
		Boolean isTIXCITY=false;
		Collection<Customers> rtwCustomerList = new ArrayList<Customers>();
		Collection<Customers> rtfCustomerList = new ArrayList<Customers>();
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			if(productType!=null && !productType.isEmpty()){
				if(productType.contains(",")){
					String arr[] = productType.split(",");
					for(String str : arr){
						if(str.equalsIgnoreCase("RTW")){
							isRTW=true;
						}
						if(str.equalsIgnoreCase("RTW2")){
							isRTW2=true;
						}
						if(str.equalsIgnoreCase("REWARDTHEFAN")){
							isRTF=true;
						}
						if(str.equalsIgnoreCase("TIXCITY")){
							isTIXCITY=true;
						}
					}
				}else{
					if(productType.equalsIgnoreCase("RTW")){
						isRTW=true;
					}
					if(productType.equalsIgnoreCase("RTW2")){
						isRTW2=true;
					}
					if(productType.equalsIgnoreCase("REWARDTHEFAN")){
						isRTF=true;
					}
					if(productType.equalsIgnoreCase("TIXCITY")){
						isTIXCITY=true;
					}
				}
			}else{
				isRTW2=true;
				isRTF=true;
				isRTW=true;
				isTIXCITY=true;
			}
			if(isRTF){
				rtfCustomerList = DAORegistry.getQueryManagerDAO().getClientReportData(isMailSent,customerCat,customerType,cities,state,country,venues,productType,grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCount(city,state,country,venueId,productType,grandChildIds,filter);
			}
			if(isTIXCITY && isRTW && isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY','RTW','RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY','RTW','RTW2'",grandChildNames,childIds,artists,filter);
			}else if(isTIXCITY && isRTW){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY','RTW'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY','RTW'",grandChildNames,childIds,artists,filter);
			}else if(isTIXCITY && isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY','RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY','RTW2'",grandChildNames,childIds,artists,filter);
			}else if(isRTW && isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW','RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'RTW','RTW2'",grandChildNames,childIds,artists,filter);
			}else if(isTIXCITY){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'TIXCITY'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'TIXCITY'",grandChildNames,childIds,artists,filter);
			}else if(isRTW){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'RTW'",grandChildNames,childIds,artists,filter);
			}else if(isRTW2){
				rtwCustomerList = DAORegistry.getQueryManagerDAO().getClientReportDataRTWRTW2(isMailSent,customerCat,customerType,cities,state,country,venues,"'RTW2'",grandChildNames,childIds,artists,filter,pageNo,false,fromDateFinal,toDateFinal);
				//count += DAORegistry.getQueryManagerDAO().getClientReportDataCountRTWRTW2(customerType,cities,state,country,venues,"'RTW2'",grandChildNames,childIds,artists,filter);
			}
			
			rtfCustomerList.addAll(rtwCustomerList);
			Map<String, Customers> map  = new HashMap<String, Customers>();
			for(Customers cust : rtfCustomerList){
				map.put(cust.getCustomerEmail().toUpperCase().trim(), cust);
			}
			
			if(rtfCustomerList!=null && !rtfCustomerList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet clientSheet = (SXSSFSheet) workbook.createSheet("Clients");
				Row clientHeader =   clientSheet.createRow((int)0);
				clientHeader.createCell(0).setCellValue("Client ID");
				clientHeader.createCell(1).setCellValue("Email");
				clientHeader.createCell(2).setCellValue("First Name");
				clientHeader.createCell(3).setCellValue("Last Name");
				clientHeader.createCell(4).setCellValue("companyName");
				clientHeader.createCell(5).setCellValue("Product Type");
				clientHeader.createCell(6).setCellValue("Street1");
				clientHeader.createCell(7).setCellValue("Street2");
				clientHeader.createCell(8).setCellValue("City");
				clientHeader.createCell(9).setCellValue("State");
				clientHeader.createCell(10).setCellValue("Country");
				clientHeader.createCell(11).setCellValue("Zipcode");
				clientHeader.createCell(12).setCellValue("Type");
				
				SXSSFSheet brokerSheet =(SXSSFSheet) workbook.createSheet("Brokers");
				Row brokerHeader = brokerSheet.createRow((int)0);
				brokerHeader.createCell(0).setCellValue("Broker ID");
				brokerHeader.createCell(1).setCellValue("Email");
				brokerHeader.createCell(2).setCellValue("First Name");
				brokerHeader.createCell(3).setCellValue("Last Name");
				brokerHeader.createCell(4).setCellValue("companyName");
				brokerHeader.createCell(5).setCellValue("Product Type");
				brokerHeader.createCell(6).setCellValue("Street1");
				brokerHeader.createCell(7).setCellValue("Street2");
				brokerHeader.createCell(8).setCellValue("City");
				brokerHeader.createCell(9).setCellValue("State");
				brokerHeader.createCell(10).setCellValue("Country");
				brokerHeader.createCell(11).setCellValue("Zipcode");
				brokerHeader.createCell(12).setCellValue("Type");
				
				int i=1;
				int j=1;
				for(Customers cust : map.values()){
					if(cust.getIsClient()==true){
						Row row = clientSheet.createRow(i);
						row.createCell(0).setCellValue(cust.getCustomerId());
						row.createCell(1).setCellValue(cust.getCustomerEmail());
						row.createCell(2).setCellValue(cust.getCustomerName());
						row.createCell(3).setCellValue(cust.getLastName());
						row.createCell(4).setCellValue(cust.getCompanyName());
						row.createCell(5).setCellValue(cust.getProductType());
						row.createCell(6).setCellValue(cust.getAddressLine1());
						row.createCell(7).setCellValue(cust.getAddressLine2());
						row.createCell(8).setCellValue(cust.getCity());
						row.createCell(9).setCellValue(cust.getState());
						row.createCell(10).setCellValue(cust.getCountry());
						row.createCell(11).setCellValue(cust.getZipCode());
						row.createCell(12).setCellValue("Client");
						i++;
					}else if(cust.getIsBroker() == true){
						Row row = brokerSheet.createRow(j);
						row.createCell(0).setCellValue(cust.getCustomerId());
						row.createCell(1).setCellValue(cust.getCustomerEmail());
						row.createCell(2).setCellValue(cust.getCustomerName());
						row.createCell(3).setCellValue(cust.getLastName());
						row.createCell(4).setCellValue(cust.getCompanyName());
						row.createCell(5).setCellValue(cust.getProductType());
						row.createCell(6).setCellValue(cust.getAddressLine1());
						row.createCell(7).setCellValue(cust.getAddressLine2());
						row.createCell(8).setCellValue(cust.getCity());
						row.createCell(9).setCellValue(cust.getState());
						row.createCell(10).setCellValue(cust.getCountry());
						row.createCell(11).setCellValue(cust.getZipCode());
						row.createCell(12).setCellValue("Broker");
						j++;
					}
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Clients.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}*/
	
	/**
	 * Manage purchase order
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/PaypalTracking")
	public String getPaypalTrackingPage(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PAYPAL_TRACKING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PaypalTrackingDTO paypalTrackingDTO = gson.fromJson(((JsonObject)jsonObject.get("paypalTrackingDTO")), PaypalTrackingDTO.class);
			
			if(paypalTrackingDTO.getStatus() == 1){
				model.addAttribute("msg", gson.toJson(paypalTrackingDTO.getMessage()));				
				model.addAttribute("status", gson.toJson(paypalTrackingDTO.getStatus()));
			}else{
				model.addAttribute("msg", paypalTrackingDTO.getError().getDescription());				
				model.addAttribute("status", paypalTrackingDTO.getStatus());
			}

			if(request.getRequestURI().contains(".json")){
				model.addAttribute("trackingList", paypalTrackingDTO.getTrackingList());
				model.addAttribute("pagingInfo", paypalTrackingDTO.getPaginationDTO());
			}else{
				model.addAttribute("trackingList", gson.toJson(paypalTrackingDTO.getTrackingList()));
				model.addAttribute("pagingInfo", gson.toJson(paypalTrackingDTO.getPaginationDTO()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-paypal-tracking";
	}
	
	/**
	 * Manage purchase order
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*
	@RequestMapping(value = "/getPayPalTracking", method = RequestMethod.POST)
	public void getPaypalTracking(HttpServletRequest request, HttpServletResponse response){
		JSONObject returnObject = new JSONObject();
		Integer count = 0;
		String pageNo = request.getParameter("pageNo");
		String orderIdStr = request.getParameter("orderId");
		String customerName = request.getParameter("customerName");
		String transactionStatus = request.getParameter("transactionStatus");
		String headerFilter = request.getParameter("headerFilter"); 
		Collection<PayPalTracking> trackingList=null;
		Integer orderId = 0;
		try {
			if(orderIdStr!=null && !orderIdStr.isEmpty()){
				orderId = Integer.parseInt(orderIdStr);
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPaypalSearchHeaderFilters(headerFilter);
			trackingList = DAORegistry.getQueryManagerDAO().getPayPalTracking(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getPayPalTrackingCount(filter);			
			
			returnObject.put("trackingList", JsonWrapperUtil.getPaypalTrackingArray(trackingList));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	*/
	
	@RequestMapping(value = "/UserStatus")
	public String updateUserStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		
		try{
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				model.addAttribute("status", 0);
				model.addAttribute("msg", "Please Login and try again.");
				return "";
			}
			
			String userIdStr = request.getParameter("userId");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);			
			map.put("sessionUserId", String.valueOf(trackerUserSession.getId()));
			map.put("status", status);
			map.put("userId", userIdStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_USER_STATUS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	/*
	@RequestMapping({"/PayPalTrackingExportToExcel"})
	public void paypalTrackingToExport(HttpServletRequest request, HttpServletResponse response){
		
		String headerFilter = request.getParameter("headerFilter");
		Collection<PayPalTracking> paypalTrackingList=null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPaypalSearchHeaderFilters(headerFilter);
			paypalTrackingList = DAORegistry.getQueryManagerDAO().getPayPalTrackingToExport(filter);
			
			if(paypalTrackingList!=null && !paypalTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("PayPal_Tracking");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Paypal Tracking Id");
				header.createCell(1).setCellValue("Customer Id");
				header.createCell(2).setCellValue("First Name");
				header.createCell(3).setCellValue("Last Name");
				header.createCell(4).setCellValue("Email");
				header.createCell(5).setCellValue("Phone");
				header.createCell(6).setCellValue("Order Id");
				header.createCell(7).setCellValue("Order Total");
				header.createCell(8).setCellValue("Order Type");
				header.createCell(9).setCellValue("Event Id");
				header.createCell(10).setCellValue("Quantity");
				header.createCell(11).setCellValue("Zone");
				header.createCell(12).setCellValue("Paypal Transaction Id");
				header.createCell(13).setCellValue("Transaction Id");
				header.createCell(14).setCellValue("Payment Id");
				header.createCell(15).setCellValue("Platform");
				header.createCell(16).setCellValue("Status");
				header.createCell(17).setCellValue("Created Date");
				header.createCell(18).setCellValue("Last Updated");
				header.createCell(19).setCellValue("Transaction Date");
				Integer i=1;
				for(PayPalTracking paypalTracking : paypalTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(paypalTracking.getId());
					row.createCell(1).setCellValue(paypalTracking.getCustomerId()!=null?paypalTracking.getCustomerId().toString():"");
					row.createCell(2).setCellValue(paypalTracking.getPayerFirstName()!=null?paypalTracking.getPayerFirstName():"");
					row.createCell(3).setCellValue(paypalTracking.getPayerLastName()!=null?paypalTracking.getPayerLastName():"");
					row.createCell(4).setCellValue(paypalTracking.getPayerEmail()!=null?paypalTracking.getPayerEmail():"");
					row.createCell(5).setCellValue(paypalTracking.getPayerPhone()!=null?paypalTracking.getPayerPhone():"");
					row.createCell(6).setCellValue(paypalTracking.getOrderId()!=null?paypalTracking.getOrderId().toString():"");
					row.createCell(7).setCellValue(paypalTracking.getOrderTotal()!=null?paypalTracking.getOrderTotal().toString():"");
					row.createCell(8).setCellValue(paypalTracking.getOrderType()!=null?paypalTracking.getOrderType():"");
					row.createCell(9).setCellValue(paypalTracking.getEventId()!=null?paypalTracking.getEventId().toString():"");
					row.createCell(10).setCellValue(paypalTracking.getQuantity()!=null?paypalTracking.getQuantity():0);
					row.createCell(11).setCellValue(paypalTracking.getZone()!=null?paypalTracking.getZone():"");
					row.createCell(12).setCellValue(paypalTracking.getPaypalTransactionId()!=null?paypalTracking.getPaypalTransactionId():"");
					row.createCell(13).setCellValue(paypalTracking.getTransactionId()!=null?paypalTracking.getTransactionId().toString():"");
					row.createCell(14).setCellValue(paypalTracking.getPaymentId()!=null?paypalTracking.getPaymentId():"");
					row.createCell(15).setCellValue(paypalTracking.getPlatform()!=null?paypalTracking.getPlatform():"");
					row.createCell(16).setCellValue(paypalTracking.getStatus()!=null?paypalTracking.getStatus():"");
					row.createCell(17).setCellValue(paypalTracking.getCreatedDateStr()!=null?paypalTracking.getCreatedDateStr():"");
					row.createCell(18).setCellValue(paypalTracking.getLastUpdatedStr()!=null?paypalTracking.getLastUpdatedStr():"");
					row.createCell(19).setCellValue(paypalTracking.getTransactionDateStr()!=null?paypalTracking.getTransactionDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=PayPal_Tracking.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
	*/
	
	@RequestMapping({"/ManualFedex"})
	public String loadManualFedex(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANUAL_FEDEX);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ManualFedexDTO manualFedexDTO = gson.fromJson(((JsonObject)jsonObject.get("manualFedexDTO")), ManualFedexDTO.class);
			
			if(manualFedexDTO.getStatus() == 1){				
				model.addAttribute("msg", manualFedexDTO.getMessage());
				model.addAttribute("status", manualFedexDTO.getStatus());
			}else{
				model.addAttribute("msg", manualFedexDTO.getError().getDescription());
				model.addAttribute("status", manualFedexDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", manualFedexDTO.getFedexGenerationPaginationDTO());
				model.addAttribute("manualFedexList", manualFedexDTO.getFedexGenerationDTO());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(manualFedexDTO.getFedexGenerationPaginationDTO()));
				model.addAttribute("manualFedexList", gson.toJson(manualFedexDTO.getFedexGenerationDTO()));
				model.addAttribute("countries", gson.toJson(manualFedexDTO.getCountryDTO()));
			}
			
			/*GridHeaderFilters filter = new GridHeaderFilters();
			Integer count = 0;
			List<Country> countryList = null;
			List<ManualFedexGeneration> manualFedexList = DAORegistry.getQueryManagerDAO().getManualFedexGeneration(filter, null);
			count = DAORegistry.getQueryManagerDAO().getManualFedexGenerationCount(filter);
			
			if(manualFedexList == null || manualFedexList.size() <= 0){
				map.put("msg", "No Manual Fedex Label Record found.");
			}
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			if(countryList != null){
				JSONArray countryListArray = new JSONArray();
				JSONObject countryListObject = null;
				for(Country country : countryList){
					countryListObject = new JSONObject();
					countryListObject.put("id", country.getId());
					countryListObject.put("name", country.getName());
					countryListArray.put(countryListObject);
				}
				map.put("countries", countryListArray);
			}
			
			map.put("manualFedexList", JsonWrapperUtil.getManualFedexGenerationArray(manualFedexList));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-admin-fedex-label";
	}
	
	/*@RequestMapping({"/GetManualFedex"})
	public String getManualFedex(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_MANUAL_FEDEX);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ManualFedexDTO manualFedexDTO = gson.fromJson(((JsonObject)jsonObject.get("manualFedexDTO")), ManualFedexDTO.class);
			
			if(manualFedexDTO.getStatus() == 1){				
				model.addAttribute("msg", manualFedexDTO.getMessage());
				model.addAttribute("status", manualFedexDTO.getStatus());
				model.addAttribute("pagingInfo", manualFedexDTO.getFedexGenerationPaginationDTO());
				model.addAttribute("manualFedexList", manualFedexDTO.getFedexGenerationDTO());
			}else{
				model.addAttribute("msg", manualFedexDTO.getError().getDescription());
				model.addAttribute("status", manualFedexDTO.getStatus());
			}
//			Integer count = 0;
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getManualFedexGenerationFilter(headerFilter);
//			
//			List<ManualFedexGeneration> manualFedexList = DAORegistry.getQueryManagerDAO().getManualFedexGeneration(filter, pageNo);
//			count = DAORegistry.getQueryManagerDAO().getManualFedexGenerationCount(filter);
//			
//			if(manualFedexList == null || manualFedexList.size() <= 0){
//				returnObject.put("msg", "No Manual Fedex Label Record found.");
//			}
//			returnObject.put("manualFedexList", JsonWrapperUtil.getManualFedexGenerationArray(manualFedexList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}*/
	

	/*@RequestMapping({"/ManualFedexExportToExcel"})
	public void manualFedexToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		List<ManualFedexGeneration> manualFedexList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManualFedexGenerationFilter(headerFilter);
			manualFedexList = DAORegistry.getQueryManagerDAO().getManualFedexGenerationToExport(filter);
			
			if(manualFedexList!=null && !manualFedexList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Manual_Fedex_Label");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Manual Fedex Id");				
				header.createCell(1).setCellValue("Full Name");
				header.createCell(2).setCellValue("Company Name");
				header.createCell(3).setCellValue("Street1");
				header.createCell(4).setCellValue("Street2");
				header.createCell(5).setCellValue("City");
				header.createCell(6).setCellValue("State");
				header.createCell(7).setCellValue("Country");
				header.createCell(8).setCellValue("ZipCode");
				header.createCell(9).setCellValue("Phone");
				header.createCell(10).setCellValue("From Address First Name");
				header.createCell(11).setCellValue("From Address Last Name");
				header.createCell(12).setCellValue("From Address Company Name");				
				header.createCell(13).setCellValue("From Address Street1");
				header.createCell(14).setCellValue("From Address Street2");
				header.createCell(15).setCellValue("From Address City");
				header.createCell(16).setCellValue("From Address State");
				header.createCell(17).setCellValue("From Address Country");
				header.createCell(18).setCellValue("From Address ZipCode");
				header.createCell(19).setCellValue("From Address Phone");
				header.createCell(20).setCellValue("Service Type");
				header.createCell(21).setCellValue("Tracking Number");
				header.createCell(22).setCellValue("Fedex Label File");
				header.createCell(23).setCellValue("Created Date");
				header.createCell(24).setCellValue("Created By");
				Integer i=1;
				for(ManualFedexGeneration manualFedex : manualFedexList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(manualFedex.getId());
					row.createCell(1).setCellValue(manualFedex.getShCustomerName()!=null?manualFedex.getShCustomerName():"");
					row.createCell(2).setCellValue(manualFedex.getShCompanyName()!=null?manualFedex.getShCompanyName():"");
					row.createCell(3).setCellValue(manualFedex.getShAddress1()!=null?manualFedex.getShAddress1():"");
					row.createCell(4).setCellValue(manualFedex.getShAddress2()!=null?manualFedex.getShAddress2():"");
					row.createCell(5).setCellValue(manualFedex.getShCity()!=null?manualFedex.getShCity():"");
					row.createCell(6).setCellValue(manualFedex.getShStateName()!=null?manualFedex.getShStateName():"");
					row.createCell(7).setCellValue(manualFedex.getShCountryName()!=null?manualFedex.getShCountryName():"");
					row.createCell(8).setCellValue(manualFedex.getShZipCode()!=null?manualFedex.getShZipCode():"");
					row.createCell(9).setCellValue(manualFedex.getShPhone()!=null?manualFedex.getShPhone():"");
					row.createCell(10).setCellValue(manualFedex.getBlFirstName()!=null?manualFedex.getBlFirstName():"");
					row.createCell(11).setCellValue(manualFedex.getBlLastName()!=null?manualFedex.getBlLastName():"");
					row.createCell(12).setCellValue(manualFedex.getBlCompanyName()!=null?manualFedex.getBlCompanyName():"");					
					row.createCell(13).setCellValue(manualFedex.getBlAddress1()!=null?manualFedex.getBlAddress1():"");
					row.createCell(14).setCellValue(manualFedex.getBlAddress2()!=null?manualFedex.getBlAddress2():"");
					row.createCell(15).setCellValue(manualFedex.getBlCity()!=null?manualFedex.getBlCity():"");
					row.createCell(16).setCellValue(manualFedex.getBlStateName()!=null?manualFedex.getBlStateName():"");
					row.createCell(17).setCellValue(manualFedex.getBlCountryName()!=null?manualFedex.getBlCountryName():"");
					row.createCell(18).setCellValue(manualFedex.getBlZipCode()!=null?manualFedex.getBlZipCode():"");
					row.createCell(19).setCellValue(manualFedex.getBlPhone()!=null?manualFedex.getBlPhone():"");
					row.createCell(20).setCellValue(manualFedex.getServiceType()!=null?manualFedex.getServiceType():"");
					row.createCell(21).setCellValue(manualFedex.getTrackingNumber()!=null?manualFedex.getTrackingNumber():"");
					row.createCell(22).setCellValue(manualFedex.getFedexLabelPath()!=null?manualFedex.getFedexLabelPath():"");
					row.createCell(23).setCellValue(manualFedex.getCreatedDateStr()!=null?manualFedex.getCreatedDateStr():"");
					row.createCell(24).setCellValue(manualFedex.getCreatedBy()!=null?manualFedex.getCreatedBy():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Manual_Fedex.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
}
