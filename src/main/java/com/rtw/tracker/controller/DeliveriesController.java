package com.rtw.tracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.pojos.AutoCompleteArtistVenueDTO;
import com.rtw.tracker.pojos.ClosedOrderStatusDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GetEventsByVenueArtistDTO;
import com.rtw.tracker.pojos.OpenOrderStatusDTO;
import com.rtw.tracker.pojos.OrderNoteDTO;
import com.rtw.tracker.pojos.PastOrderStatusDTO;
import com.rtw.tracker.pojos.PendingReceiptOrderStatusDTO;
import com.rtw.tracker.pojos.PendingShipmentOrderStatusDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

@Controller
@RequestMapping(value="/Deliveries")
public class DeliveriesController {

	
	
	public static DecimalFormat df = new DecimalFormat("#.##");
	
	@RequestMapping("/AutoCompleteArtistAndVenue")
	public void getAutoCompleteArtistAndVenue(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_ARTIST_VENUE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistVenueDTO autoCompleteArtistVenueDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistVenueDTO")), AutoCompleteArtistVenueDTO.class);
						
			if(autoCompleteArtistVenueDTO.getStatus() == 1){
				writer.write(autoCompleteArtistVenueDTO.getArtistVenue());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse = "";
		try {
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			Collection<Venue> venues = DAORegistry.getVenueDAO().filterByName(param);
			if (venues != null) {
				for (Venue venue : venues) {
					strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getLocation() + "\n") ;
				}
			}
			writer.write(strResponse);
		}catch(Exception e){
			e.printStackTrace();
			writer.write("");
		}*/			
	}
	
	/**
	 * Returns list of event filter by artist or venue
	 * @param request
	 * @param response
	 */
	@RequestMapping("/GetEventsByVenueOrArtist")
	public String getEventsByVenueOrArtist(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try {
			String type = request.getParameter("type");
			String id = request.getParameter("id");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("type", type);
			map.put("id", id);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_EVENTS_BY_ARTIST_VENUE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GetEventsByVenueArtistDTO getEventsByVenueArtistDTO = gson.fromJson(((JsonObject)jsonObject.get("getEventsByVenueArtistDTO")), GetEventsByVenueArtistDTO.class);
			
			if(getEventsByVenueArtistDTO.getStatus() == 1){
				model.addAttribute("msg", getEventsByVenueArtistDTO.getMessage());
				model.addAttribute("status", getEventsByVenueArtistDTO.getStatus());
				model.addAttribute("events", getEventsByVenueArtistDTO.getEventDetails());
			}else{
				model.addAttribute("msg", getEventsByVenueArtistDTO.getError().getDescription());
				model.addAttribute("status", getEventsByVenueArtistDTO.getStatus());
			}
			/*JSONArray array = new JSONArray();;
			if(type!=null && !type.isEmpty() && idStr!=null && !idStr.isEmpty()){
				Collection<EventDetails> events = null;
				if(type.equalsIgnoreCase("ARTIST")){
					events = DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(idStr),null);
				}else{
					events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(idStr));
				}
				if(events!=null){
					JSONObject object  =null;
					for(EventDetails event : events){
						object  = new JSONObject();
						object.put("eventId", event.getEventId().toString());
						object.put("eventName", event.getEventName());
						object.put("eventDateStr", event.getEventDateStr());
						object.put("eventTimeStr", event.getEventTimeStr());
						object.put("venue", event.getBuilding());
						array.put(object);
					}
				}
			}
			JSONObject result = new JSONObject();
			result.put("events", array);
			response.setHeader("Content-type","application/json");
			IOUtils.write(result.toString().getBytes(),
					response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return "";	
	}
	
	
	/**
	 * Render open orders page
	 * @return
	 */
	@RequestMapping(value="/PendingShipmentOrders")
	public String getShipmentPendingOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");	
			String sortingString = request.getParameter("sortingString");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistVenueName", artistVenueName);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PENDING_SHIPMENT_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PendingShipmentOrderStatusDTO pendingShipmentOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pendingShipmentOrderStatusDTO")), PendingShipmentOrderStatusDTO.class);
			
			if(pendingShipmentOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", pendingShipmentOrderStatusDTO.getMessage());
				model.addAttribute("status", pendingShipmentOrderStatusDTO.getStatus());								
			}else{
				model.addAttribute("msg", pendingShipmentOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pendingShipmentOrderStatusDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", pendingShipmentOrderStatusDTO.getPendingShipmentOrdersPaginationDTO());
				model.addAttribute("shipmentpendingOrders", pendingShipmentOrderStatusDTO.getPendingShipmentOrders());				
			}else{
				model.addAttribute("pagingInfo", gson.toJson(pendingShipmentOrderStatusDTO.getPendingShipmentOrdersPaginationDTO()));
				model.addAttribute("shipmentpendingOrders", gson.toJson(pendingShipmentOrderStatusDTO.getPendingShipmentOrders()));
				model.addAttribute("events", pendingShipmentOrderStatusDTO.getEventDetails());
				model.addAttribute("profitLossSigns", pendingShipmentOrderStatusDTO.getProfitLossSign());
				//model.addAttribute("profitLossSigns", ProfitLossSign.values());
				model.addAttribute("selectedSign", pendingShipmentOrderStatusDTO.getSelectedProfitLossSign());
				model.addAttribute("selectedProduct", pendingShipmentOrderStatusDTO.getProductType());				
				model.addAttribute("eventId", eventSelect);
				model.addAttribute("artistId", artistId);
				model.addAttribute("venueId", venueId);
				model.addAttribute("artistVenueName", artistVenueName);
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);
				model.addAttribute("brokerId", brokerId.toString());				
			}
		
			/*String shipmentPendingPage = "page-shipment-pending-order";
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String action = request.getParameter("action");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String fromDateFinal = "";
			String toDateFinal="";
			Collection<OpenOrderStatus> shipmentpendingOrders = null;
			Collection<EventDetails> events = null;
			Integer count=0;
			//String productType = "REWARDTHEFAN";
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			//Getting Broker Info
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL")){ 
					companyProduct = "REWARDTHEFAN";
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,null,brokerId);
					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter,null);
					count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter);
				}
			}else{
				companyProduct = "REWARDTHEFAN";
				String product = (String) session.getAttribute("productType");
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
				shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,null,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
			}
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}				
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("selectedSign",ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()));
			map.put("eventId", eventIdStr);
			map.put("artistId", artistIdStr);
			map.put("venueId", venueIdStr);
			map.put("artistVenueName", artistVenueName);
			map.put("events", events);
			map.put("profitLossSigns",ProfitLossSign.values());
			map.put("selectedProduct",companyProduct);
			map.put("shipmentpendingOrders",JsonWrapperUtil.getOpenOrderArray(shipmentpendingOrders));
			map.put("pagingInfo",PaginationUtil.calculatePaginationParameters(count, null));*/			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "page-shipment-pending-order";
	}
	
	/*@RequestMapping(value="/GetPendingShipmentOrders")
	public String getShipmentPendingOrders(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String eventSelect = request.getParameter("eventSelect");			
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_PENDING_SHIPMENT_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PendingShipmentOrderStatusDTO pendingShipmentOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pendingShipmentOrderStatusDTO")), PendingShipmentOrderStatusDTO.class);
			
			if(pendingShipmentOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", pendingShipmentOrderStatusDTO.getMessage());
				model.addAttribute("status", pendingShipmentOrderStatusDTO.getStatus());
				model.addAttribute("pagingInfo", pendingShipmentOrderStatusDTO.getPendingShipmentOrdersPaginationDTO());
				model.addAttribute("shipmentpendingOrders", pendingShipmentOrderStatusDTO.getPendingShipmentOrders());								
			}else{
				model.addAttribute("msg", pendingShipmentOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pendingShipmentOrderStatusDTO.getStatus());
			}
			
//			JSONObject returnObject = new JSONObject();
//			String eventIdStr = request.getParameter("eventSelect");
//			String artistIdStr = request.getParameter("artistId");
//			String venueIdStr = request.getParameter("venueId");
//			String invoiceNo = request.getParameter("invoiceNo");
//			String fromDateStr = request.getParameter("fromDate");
//			String toDateStr = request.getParameter("toDate");
//			String pageNo = request.getParameter("pageNo");
//			String headerFilter = request.getParameter("headerFilter");
//			String action = request.getParameter("action");
//			String profitLossSign = request.getParameter("profitLoss");
//			String companyProduct = request.getParameter("productType");
//			String externalOrderId = request.getParameter("externalOrderId");
//			String orderId = request.getParameter("orderId");
//			Collection<OpenOrderStatus> shipmentpendingOrders = null;
//			Integer count=0;
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
//			//Getting Broker Info
//			HttpSession session = request.getSession(true);
//			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
//			Integer brokerId = Util.getBrokerId(session);
//			if(brokerId == -1){
//				brokerId = 0;
//			}
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
//						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
//					}else{
//						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter);
//					}
//				}else{
//					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
//						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,false,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,filter,brokerId);
//					}else{
//						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "PENDING", companyProduct, filter);
//					}
//					
//				}
//			}
//			returnObject.put("shipmentpendingOrders",JsonWrapperUtil.getOpenOrderArray(shipmentpendingOrders));
//			returnObject.put("pagingInfo",PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please Try again.");
		}
		return "";
	}*/
	
	
	/*@RequestMapping(value="/PendingShipmentExportToExcel")
	public void getShipmentPendingOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		String eventIdStr = request.getParameter("eventSelect");
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		String invoiceNo = request.getParameter("invoiceNo");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String profitLossSign = request.getParameter("profitLoss");
		String companyProduct = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String headerFilter = request.getParameter("headerFilter");
		String orderId = request.getParameter("orderId");
		Collection<OpenOrderStatus> shipmentpendingOrders = null;
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			//Getting Broker Info
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			
			String fromDateFinal = "";
			String toDateFinal = "";			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING SHIPMENT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
				}
			}
			
			if(shipmentpendingOrders!=null && !shipmentpendingOrders.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("pending_shipment_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				
				int i=1;
				for(OpenOrderStatus order : shipmentpendingOrders){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=pending_shipment_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	/**
	 * Render open orders page
	 * @return
	 */
	@RequestMapping(value="/PendingRecieptOrders")
	public String getPendingRecieptOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
				
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistVenueName", artistVenueName);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());			
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PENDING_RECEIPT_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PendingReceiptOrderStatusDTO pendingReceiptOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pendingReceiptOrderStatusDTO")), PendingReceiptOrderStatusDTO.class);
			
			if(pendingReceiptOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", pendingReceiptOrderStatusDTO.getMessage());
				model.addAttribute("status", pendingReceiptOrderStatusDTO.getStatus());								
			}else{
				model.addAttribute("msg", pendingReceiptOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pendingReceiptOrderStatusDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", pendingReceiptOrderStatusDTO.getPendingReceiptOrdersPaginationDTO());
				model.addAttribute("pendingRecieptOrders", pendingReceiptOrderStatusDTO.getPendingReceiptOrders());			
			}else{
				model.addAttribute("pagingInfo", gson.toJson(pendingReceiptOrderStatusDTO.getPendingReceiptOrdersPaginationDTO()));
				model.addAttribute("pendingRecieptOrders", gson.toJson(pendingReceiptOrderStatusDTO.getPendingReceiptOrders()));
				model.addAttribute("events", pendingReceiptOrderStatusDTO.getEventDetails());
				model.addAttribute("profitLossSigns", pendingReceiptOrderStatusDTO.getProfitLossSign());
				//model.addAttribute("profitLossSigns", ProfitLossSign.values());
				model.addAttribute("selectedSign", pendingReceiptOrderStatusDTO.getSelectedProfitLossSign());
				model.addAttribute("selectedProduct", pendingReceiptOrderStatusDTO.getProductType());				
				model.addAttribute("eventId", eventSelect);
				model.addAttribute("artistId", artistId);
				model.addAttribute("venueId", venueId);
				model.addAttribute("artistVenueName", artistVenueName);
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);
				model.addAttribute("brokerId", brokerId.toString());				
			}
			
			/*String shipmentPendingPage = "page-pending-reciept-order";
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String action = request.getParameter("action");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String fromDateFinal = "";
			String toDateFinal="";
			Collection<OpenOrderStatus> shipmentpendingOrders = null;
			Collection<EventDetails> events = null;
			Integer count=0;
			//String productType = "REWARDTHEFAN";
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			//Getting Broker Info
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL")){ 
					companyProduct = "REWARDTHEFAN";
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,null,brokerId);
					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
				}
			}else{
				companyProduct = "REWARDTHEFAN";
				String product = (String) session.getAttribute("productType");
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
				shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,null,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
			}
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("selectedSign",ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()));
			map.put("eventId", eventIdStr);
			map.put("artistId", artistIdStr);
			map.put("venueId", venueIdStr);
			map.put("artistVenueName", artistVenueName);
			map.put("events", events);
			map.put("profitLossSigns",ProfitLossSign.values());
			map.put("selectedProduct",companyProduct);
			map.put("pendingRecieptOrders",JsonWrapperUtil.getOpenOrderArray(shipmentpendingOrders));
			map.put("pagingInfo",PaginationUtil.calculatePaginationParameters(count, null));*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}		
		return "page-pending-reciept-order";
	}
	
	
	
	@RequestMapping(value="/DisputedOrders")
	public String getDisputedOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
				
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistVenueName", artistVenueName);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());			
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DISPUTED_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PendingReceiptOrderStatusDTO pendingReceiptOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pendingReceiptOrderStatusDTO")), PendingReceiptOrderStatusDTO.class);
			
			if(pendingReceiptOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", pendingReceiptOrderStatusDTO.getMessage());
				model.addAttribute("status", pendingReceiptOrderStatusDTO.getStatus());								
			}else{
				model.addAttribute("msg", pendingReceiptOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pendingReceiptOrderStatusDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", pendingReceiptOrderStatusDTO.getPendingReceiptOrdersPaginationDTO());
				model.addAttribute("disputedOrders", pendingReceiptOrderStatusDTO.getPendingReceiptOrders());			
			}else{
				model.addAttribute("pagingInfo", gson.toJson(pendingReceiptOrderStatusDTO.getPendingReceiptOrdersPaginationDTO()));
				model.addAttribute("disputedOrders", gson.toJson(pendingReceiptOrderStatusDTO.getPendingReceiptOrders()));
				model.addAttribute("events", pendingReceiptOrderStatusDTO.getEventDetails());
				model.addAttribute("profitLossSigns", pendingReceiptOrderStatusDTO.getProfitLossSign());
				//model.addAttribute("profitLossSigns", ProfitLossSign.values());
				model.addAttribute("selectedSign", pendingReceiptOrderStatusDTO.getSelectedProfitLossSign());
				model.addAttribute("selectedProduct", pendingReceiptOrderStatusDTO.getProductType());				
				model.addAttribute("eventId", eventSelect);
				model.addAttribute("artistId", artistId);
				model.addAttribute("venueId", venueId);
				model.addAttribute("artistVenueName", artistVenueName);
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);
				model.addAttribute("brokerId", brokerId.toString());				
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}		
		return "page-disputed-order";
	}
	
	
	/*@RequestMapping(value="/GetPendingRecieptOrders")
	public String getPendingRecieptOrders(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String eventSelect = request.getParameter("eventSelect");			
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_PENDING_RECEIPT_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PendingReceiptOrderStatusDTO pendingReceiptOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pendingReceiptOrderStatusDTO")), PendingReceiptOrderStatusDTO.class);
			
			if(pendingReceiptOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", pendingReceiptOrderStatusDTO.getMessage());
				model.addAttribute("status", pendingReceiptOrderStatusDTO.getStatus());
				model.addAttribute("pagingInfo", pendingReceiptOrderStatusDTO.getPendingReceiptOrdersPaginationDTO());
				model.addAttribute("pendingRecieptOrders", pendingReceiptOrderStatusDTO.getPendingReceiptOrders());								
			}else{
				model.addAttribute("msg", pendingReceiptOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pendingReceiptOrderStatusDTO.getStatus());
			}
			
//			JSONObject returnObject = new JSONObject();
//			String eventIdStr = request.getParameter("eventSelect");
//			String artistIdStr = request.getParameter("artistId");
//			String venueIdStr = request.getParameter("venueId");
//			String invoiceNo = request.getParameter("invoiceNo");
//			String fromDateStr = request.getParameter("fromDate");
//			String toDateStr = request.getParameter("toDate");
//			String pageNo = request.getParameter("pageNo");
//			String headerFilter = request.getParameter("headerFilter");
//			String action = request.getParameter("action");
//			String profitLossSign = request.getParameter("profitLoss");
//			String companyProduct = request.getParameter("productType");
//			String externalOrderId = request.getParameter("externalOrderId");
//			String orderId = request.getParameter("orderId");
//			Collection<OpenOrderStatus> shipmentpendingOrders = null;
//			Integer count=0;
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
//			//Getting Broker Info
//			HttpSession session = request.getSession(true);
//			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
//			Integer brokerId = Util.getBrokerId(session);
//			if(brokerId == -1){
//				brokerId = 0;
//			}
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
//						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
//					}
//				}else{
//					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
//						shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,false,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,filter,brokerId);
//					}
//					
//				}
//			}
//			returnObject.put("pendingRecieptOrders",JsonWrapperUtil.getOpenOrderArray(shipmentpendingOrders));
//			returnObject.put("pagingInfo",PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please Try again.");
		}
		return "";
	}*/
	
	
	/*@RequestMapping(value="/PendingRecieptExportToExcel")
	public void getPendingRecieptOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		String eventIdStr = request.getParameter("eventSelect");
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		String invoiceNo = request.getParameter("invoiceNo");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String profitLossSign = request.getParameter("profitLoss");
		String companyProduct = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String headerFilter = request.getParameter("headerFilter");
		String orderId = request.getParameter("orderId");
		Collection<OpenOrderStatus> shipmentpendingOrders = null;
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			//Getting Broker Info
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			
			String fromDateFinal = "";
			String toDateFinal = "";			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING RECIEPT",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					shipmentpendingOrders = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"PENDING",companyProduct,externalOrderId,orderId,filter);
				}
			}
			
			if(shipmentpendingOrders!=null && !shipmentpendingOrders.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("pending_shipment_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				
				int i=1;
				for(OpenOrderStatus order : shipmentpendingOrders){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=pending_reciept_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	@RequestMapping(value="/OpenOrders")
	public String getOpenOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
				
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
						
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistVenueName", artistVenueName);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_OPEN_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OpenOrderStatusDTO openOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("openOrderStatusDTO")), OpenOrderStatusDTO.class);
			
			if(openOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", openOrderStatusDTO.getMessage());
				model.addAttribute("status", openOrderStatusDTO.getStatus());							
			}else{
				model.addAttribute("msg", openOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", openOrderStatusDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", openOrderStatusDTO.getOpenOrdersPaginationDTO());
				model.addAttribute("openOrders", openOrderStatusDTO.getOpenOrders());				
				model.addAttribute("noOfOrders", openOrderStatusDTO.getNoOfOrders());
				model.addAttribute("totalSectionQty", openOrderStatusDTO.getTotalSectionQty());	
				model.addAttribute("sectionTotalNetSoldPrice", openOrderStatusDTO.getSectionTotalNetSoldPrice());
				model.addAttribute("sectionTotalActualSoldPrice", openOrderStatusDTO.getSectionTotalActualSoldPrice());
				model.addAttribute("sectionTotalMarketPrice", openOrderStatusDTO.getSectionTotalMarketPrice());
				model.addAttribute("sectionTotalProfitLoss", openOrderStatusDTO.getSectionTotalProfitLoss());
				model.addAttribute("sectionConsideredOrders", openOrderStatusDTO.getSectionConsideredOrders());
				model.addAttribute("sectionNotConsideredOrders", openOrderStatusDTO.getSectionNotConsideredOrders());
				model.addAttribute("totalZoneQty", openOrderStatusDTO.getTotalZoneQty());
				model.addAttribute("zoneTotalNetSoldPrice", openOrderStatusDTO.getZoneTotalNetSoldPrice());
				model.addAttribute("zoneTotalActualSoldPrice", openOrderStatusDTO.getZoneTotalActualSoldPrice());
				model.addAttribute("zoneTotalPrice", openOrderStatusDTO.getZoneTotalPrice());
				model.addAttribute("zoneTotalProfitLoss", openOrderStatusDTO.getZoneTotalProfitLoss());
				model.addAttribute("zoneConsideredOrders", openOrderStatusDTO.getZoneConsideredOrders());
				model.addAttribute("zoneNotConsideredOrders", openOrderStatusDTO.getZoneNotConsideredOrders());	
			}else{
				model.addAttribute("pagingInfo", gson.toJson(openOrderStatusDTO.getOpenOrdersPaginationDTO()));
				model.addAttribute("openOrders", gson.toJson(openOrderStatusDTO.getOpenOrders()));
				model.addAttribute("events", openOrderStatusDTO.getEventDetails());
				model.addAttribute("profitLossSigns", openOrderStatusDTO.getProfitLossSign());
				//model.addAttribute("profitLossSigns", ProfitLossSign.values());
				model.addAttribute("selectedSign", openOrderStatusDTO.getSelectedProfitLossSign());
				model.addAttribute("selectedProduct", openOrderStatusDTO.getProductType());
				model.addAttribute("noOfOrders", openOrderStatusDTO.getNoOfOrders());
				model.addAttribute("totalSectionQty", openOrderStatusDTO.getTotalSectionQty());	
				model.addAttribute("sectionTotalNetSoldPrice", openOrderStatusDTO.getSectionTotalNetSoldPrice());
				model.addAttribute("sectionTotalActualSoldPrice", openOrderStatusDTO.getSectionTotalActualSoldPrice());
				model.addAttribute("sectionTotalMarketPrice", openOrderStatusDTO.getSectionTotalMarketPrice());
				model.addAttribute("sectionTotalProfitLoss", openOrderStatusDTO.getSectionTotalProfitLoss());
				model.addAttribute("sectionConsideredOrders", openOrderStatusDTO.getSectionConsideredOrders());
				model.addAttribute("sectionNotConsideredOrders", openOrderStatusDTO.getSectionNotConsideredOrders());
				model.addAttribute("totalZoneQty", openOrderStatusDTO.getTotalZoneQty());
				model.addAttribute("zoneTotalNetSoldPrice", openOrderStatusDTO.getZoneTotalNetSoldPrice());
				model.addAttribute("zoneTotalActualSoldPrice", openOrderStatusDTO.getZoneTotalActualSoldPrice());
				model.addAttribute("zoneTotalPrice", openOrderStatusDTO.getZoneTotalPrice());
				model.addAttribute("zoneTotalProfitLoss", openOrderStatusDTO.getZoneTotalProfitLoss());
				model.addAttribute("zoneConsideredOrders", openOrderStatusDTO.getZoneConsideredOrders());
				model.addAttribute("zoneNotConsideredOrders", openOrderStatusDTO.getZoneNotConsideredOrders());
				model.addAttribute("eventId", eventSelect);
				model.addAttribute("artistId", artistId);
				model.addAttribute("venueId", venueId);
				model.addAttribute("artistVenueName", artistVenueName);
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);
				model.addAttribute("brokerId", brokerId.toString());
			}
			
			/*String fromDateFinal = "";
			String toDateFinal="";
			Collection<OpenOrderStatus> openOrdersList = null;
			Collection<EventDetails> events = null;
			Integer count=0;
			Integer noOfOrders =0,sectionConsideredOrders=0,sectionNotConsideredOrders=0,totalSectionQty=0,
			zoneConsideredOrders=0,zoneNotConsideredOrders=0,totalZoneQty=0;
			Double sectionTotalNetSoldPrice=0.0,sectionTotalMarketPrice=0.0,sectionTotalActualSoldPrice=0.0,sectionTotalProfitLoss=0.0,
			zoneTotalNetSoldPrice=0.0,zoneTotalPrice=0.0,zoneTotalActualSoldPrice=0.0,zoneTotalProfitLoss=0.0;
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			map.put("profitLossSigns",ProfitLossSign.values());
			GridHeaderFilters filter = new GridHeaderFilters();
			Collection<OpenOrderStatus> list = null;
			//Getting Broker Info
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			//Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
						artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,null,brokerId);				
				list = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
						artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,true,null,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
						artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
			}else{
				productType = "ALL";
				String product = (String) session.getAttribute("productType");
				if(product!=null && !product.isEmpty()){
					productType = product;
				}
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
						artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,null,brokerId);
				list = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
						artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,true,null,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
						artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
			}
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
//			count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(null, null, null, null, null, null, "OPEN", null, filter);
//			openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(null, null, null, null, null, null, "OPEN", null, filter,false,null);
//			Collection<OpenOrderStatus> list = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(null, null, null, null, null, null, "OPEN", null, filter,true,null);
			if(list!=null && list.size() > 0){
				for (OpenOrderStatus openOrderStatus : list) {
					
					noOfOrders++;
					if(openOrderStatus.getSectionTixQty() > 0) {
						totalSectionQty +=openOrderStatus.getSoldQty();
						sectionTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
						sectionTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
						sectionTotalMarketPrice += openOrderStatus.getTotalMarketPrice();
						sectionTotalProfitLoss += openOrderStatus.getProfitAndLoss();
						sectionConsideredOrders++;
					} else {
						sectionNotConsideredOrders++;
					}
					
					if(openOrderStatus.getZoneTixQty() > 0) {
						totalZoneQty +=openOrderStatus.getSoldQty();
						zoneTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
						zoneTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
						zoneTotalPrice += openOrderStatus.getZoneTotalPrice();
						zoneTotalProfitLoss += openOrderStatus.getZoneProfitAndLoss();
						zoneConsideredOrders++;
					} else {
						zoneNotConsideredOrders++;
					}
				}
			}
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("selectedSign",ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()));
			map.put("eventId", eventIdStr);
			map.put("artistId", artistIdStr);
			map.put("venueId", venueIdStr);
			map.put("artistVenueName", artistVenueName);
			map.put("events", events);
			map.put("selectedProduct",productType);
			map.put("openOrders", JsonWrapperUtil.getOpenOrderArray(openOrdersList));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			map.put("noOfOrders",noOfOrders);
			map.put("totalSectionQty",totalSectionQty);	
			map.put("sectionTotalNetSoldPrice",df.format(sectionTotalNetSoldPrice));
			map.put("sectionTotalActualSoldPrice",df.format(sectionTotalActualSoldPrice));
			map.put("sectionTotalMarketPrice",df.format(sectionTotalMarketPrice));
			map.put("sectionTotalProfitLoss",df.format(sectionTotalProfitLoss));
			map.put("sectionConsideredOrders",sectionConsideredOrders);
			map.put("sectionNotConsideredOrders",sectionNotConsideredOrders);
			map.put("totalZoneQty",totalZoneQty);
			map.put("zoneTotalNetSoldPrice",df.format(zoneTotalNetSoldPrice));
			map.put("zoneTotalActualSoldPrice",df.format(zoneTotalActualSoldPrice));
			map.put("zoneTotalPrice",df.format(zoneTotalPrice));
			map.put("zoneTotalProfitLoss",df.format(zoneTotalProfitLoss));
			map.put("zoneConsideredOrders",zoneConsideredOrders);
			map.put("zoneNotConsideredOrders",zoneNotConsideredOrders);*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}		
		return "page-open-order";
	}
	
	/**
	 * Render open orders page
	 * @return
	 */
	/*@RequestMapping(value="/GetOpenOrders")
	public String getOpenOrders(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_OPEN_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OpenOrderStatusDTO openOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("openOrderStatusDTO")), OpenOrderStatusDTO.class);
			
			if(openOrderStatusDTO.getStatus() == 1){				
				model.addAttribute("msg", openOrderStatusDTO.getMessage());
				model.addAttribute("status", openOrderStatusDTO.getStatus());
				model.addAttribute("pagingInfo", openOrderStatusDTO.getOpenOrdersPaginationDTO());
				model.addAttribute("openOrders", openOrderStatusDTO.getOpenOrders());				
				model.addAttribute("noOfOrders", openOrderStatusDTO.getNoOfOrders());
				model.addAttribute("totalSectionQty", openOrderStatusDTO.getTotalSectionQty());	
				model.addAttribute("sectionTotalNetSoldPrice", openOrderStatusDTO.getSectionTotalNetSoldPrice());
				model.addAttribute("sectionTotalActualSoldPrice", openOrderStatusDTO.getSectionTotalActualSoldPrice());
				model.addAttribute("sectionTotalMarketPrice", openOrderStatusDTO.getSectionTotalMarketPrice());
				model.addAttribute("sectionTotalProfitLoss", openOrderStatusDTO.getSectionTotalProfitLoss());
				model.addAttribute("sectionConsideredOrders", openOrderStatusDTO.getSectionConsideredOrders());
				model.addAttribute("sectionNotConsideredOrders", openOrderStatusDTO.getSectionNotConsideredOrders());
				model.addAttribute("totalZoneQty", openOrderStatusDTO.getTotalZoneQty());
				model.addAttribute("zoneTotalNetSoldPrice", openOrderStatusDTO.getZoneTotalNetSoldPrice());
				model.addAttribute("zoneTotalActualSoldPrice", openOrderStatusDTO.getZoneTotalActualSoldPrice());
				model.addAttribute("zoneTotalPrice", openOrderStatusDTO.getZoneTotalPrice());
				model.addAttribute("zoneTotalProfitLoss", openOrderStatusDTO.getZoneTotalProfitLoss());
				model.addAttribute("zoneConsideredOrders", openOrderStatusDTO.getZoneConsideredOrders());
				model.addAttribute("zoneNotConsideredOrders", openOrderStatusDTO.getZoneNotConsideredOrders());				
			}else{
				model.addAttribute("msg", openOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", openOrderStatusDTO.getStatus());
			}
			
//			Collection<OpenOrderStatus> openOrdersList = null;
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			Integer count=0;
//			Integer noOfOrders =0,sectionConsideredOrders=0,sectionNotConsideredOrders=0,totalSectionQty=0,
//			zoneConsideredOrders=0,zoneNotConsideredOrders=0,totalZoneQty=0;
//			Double sectionTotalNetSoldPrice=0.0,sectionTotalMarketPrice=0.0,sectionTotalActualSoldPrice=0.0,sectionTotalProfitLoss=0.0,
//			zoneTotalNetSoldPrice=0.0,zoneTotalPrice=0.0,zoneTotalActualSoldPrice=0.0,zoneTotalProfitLoss=0.0;
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
//			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
//			
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
//							artistIdStr,venueIdStr,fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
//					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
//							artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
//				}else{
//					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
//							artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
//					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
//							artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
//				}
//			}
//			else if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty())){
//				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
//						artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
//				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
//						artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
//			}else{
//				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, 
//							artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,false,pageNo,brokerId);
//					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, 
//							artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,filter,brokerId);
//				}
//			}
//			
//			
//			Collection<OpenOrderStatus> list = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr,
//					fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.toUpperCase()), "OPEN", productType, filter,true,null,brokerId);
//			if(list!=null && list.size() > 0){
//				for (OpenOrderStatus openOrderStatus : list) {
//					noOfOrders++;
//					if(openOrderStatus.getSectionTixQty() > 0) {
//						totalSectionQty +=openOrderStatus.getSoldQty();
//						sectionTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
//						sectionTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
//						sectionTotalMarketPrice += openOrderStatus.getTotalMarketPrice();
//						sectionTotalProfitLoss += openOrderStatus.getProfitAndLoss();
//						sectionConsideredOrders++;
//					} else {
//						sectionNotConsideredOrders++;
//					}
//					
//					if(openOrderStatus.getZoneTixQty() > 0) {
//						totalZoneQty +=openOrderStatus.getSoldQty();
//						zoneTotalNetSoldPrice += openOrderStatus.getNetTotalSoldPrice();
//						zoneTotalActualSoldPrice += openOrderStatus.getTotalActualSoldPrice();
//						zoneTotalPrice += openOrderStatus.getZoneTotalPrice();
//						zoneTotalProfitLoss += openOrderStatus.getZoneProfitAndLoss();
//						zoneConsideredOrders++;
//					} else {
//						zoneNotConsideredOrders++;
//					}
//				}
//				
//			}
//			returnObject.put("openOrders", JsonWrapperUtil.getOpenOrderArray(openOrdersList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			returnObject.put("noOfOrders",noOfOrders);
//			returnObject.put("totalSectionQty",totalSectionQty);	
//			returnObject.put("sectionTotalNetSoldPrice",df.format(sectionTotalNetSoldPrice));
//			returnObject.put("sectionTotalActualSoldPrice",df.format(sectionTotalActualSoldPrice));
//			returnObject.put("sectionTotalMarketPrice",df.format(sectionTotalMarketPrice));
//			returnObject.put("sectionTotalProfitLoss",df.format(sectionTotalProfitLoss));
//			returnObject.put("sectionConsideredOrders",sectionConsideredOrders);
//			returnObject.put("sectionNotConsideredOrders",sectionNotConsideredOrders);
//			returnObject.put("totalZoneQty",totalZoneQty);
//			returnObject.put("zoneTotalNetSoldPrice",df.format(zoneTotalNetSoldPrice));
//			returnObject.put("zoneTotalActualSoldPrice",df.format(zoneTotalActualSoldPrice));
//			returnObject.put("zoneTotalPrice",df.format(zoneTotalPrice));
//			returnObject.put("zoneTotalProfitLoss",df.format(zoneTotalProfitLoss));
//			returnObject.put("zoneConsideredOrders",zoneConsideredOrders);
//			returnObject.put("zoneNotConsideredOrders",zoneNotConsideredOrders);
//			
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please Try again.");
		}
		return "";
	}*/
	
	/*@RequestMapping(value = "/OpenOrderExportToExcel")
	public void openOrderExportToExcel(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String eventIdStr = request.getParameter("eventSelect");
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		String invoiceNo = request.getParameter("invoiceNo");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String profitLossSign = request.getParameter("profitLoss");
		String productType = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String orderId = request.getParameter("orderId");
		String headerFilter = request.getParameter("headerFilter");
		String fromDateFinal = "";
		String toDateFinal = "";
		List<OpenOrderStatus> openOrdersList = null;
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			if(fromDateStr!=null && !fromDateStr.isEmpty() && toDateStr!=null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, 
						artistIdStr,venueIdStr, fromDateFinal, toDateFinal,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,externalOrderId,orderId,filter,brokerId);
			}else{
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, 
						artistIdStr,venueIdStr, null, null,ProfitLossSign.valueOf(profitLossSign.toUpperCase()),"OPEN",productType,externalOrderId,orderId,filter,brokerId);
			}
		
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("open_orders");
				
				int j=0;
				Row header = sheet.createRow((int)0);
				header.createCell(j).setCellValue("Product Type");
				j++;
				header.createCell(j).setCellValue("Invoice Id");
				j++;
				header.createCell(j).setCellValue("Event Name");
				j++;
				header.createCell(j).setCellValue("Event Date");
				j++;
				header.createCell(j).setCellValue("Event Time");
				j++;
				header.createCell(j).setCellValue("Venue");
				j++;
				header.createCell(j).setCellValue("City");
				j++;
				header.createCell(j).setCellValue("State");
				j++;
				header.createCell(j).setCellValue("Country");
				j++;
				header.createCell(j).setCellValue("Zone");
				j++;
				header.createCell(j).setCellValue("Section");
				j++;
				header.createCell(j).setCellValue("Row");
				j++;
				header.createCell(j).setCellValue("Quantity");
				j++;
				header.createCell(j).setCellValue("Section Tix Count");
				j++;
				header.createCell(j).setCellValue("Zone Tix Count");
				j++;
				header.createCell(j).setCellValue("Event Tix Count");
				j++;
				header.createCell(j).setCellValue("Net Sold Price");
				j++;
				header.createCell(j).setCellValue("Market Price");
				j++;
				header.createCell(j).setCellValue("Net Total Sold Price");
				j++;
				header.createCell(j).setCellValue("Profit/Loss");
				j++;
				header.createCell(j).setCellValue("Section Margin (%)");
				j++;
				header.createCell(j).setCellValue("Zone Cheapest Price");
				j++;
				header.createCell(j).setCellValue("Zone Total Price");
				j++;
				header.createCell(j).setCellValue("Zone P/L Value");
				j++;
				header.createCell(j).setCellValue("Zone Margin (%)");
				j++;
				header.createCell(j).setCellValue("Actual Sold Price");
				j++;
				header.createCell(j).setCellValue("Total Market Price");
				j++;
				header.createCell(j).setCellValue("Total Actual Sold Price");
				j++;
				
				header.createCell(j).setCellValue("Zone Tix Group Count");
				j++;
				header.createCell(j).setCellValue("Invoice Date");
				j++;
				header.createCell(j).setCellValue("Internal Notes");
				j++;
				
				header.createCell(j).setCellValue("Last Updated Price");
				j++;
				header.createCell(j).setCellValue("Price Updated Count");
				j++;
				header.createCell(j).setCellValue("Price");
				j++;
				header.createCell(j).setCellValue("Discount Coupen Price");
				j++;
				header.createCell(j).setCellValue("Url");
				j++;
				header.createCell(j).setCellValue("Shipping Method");
				j++;
				header.createCell(j).setCellValue("Tracking No");
				j++;
				header.createCell(j).setCellValue("Secondary Order Id");
				j++;
				header.createCell(j).setCellValue("Secondary Order Type");
				j++;
				header.createCell(j).setCellValue("Order Id");
				j++;
				header.createCell(j).setCellValue("Last Updated");
				j++;
				header.createCell(j).setCellValue("Broker Id");
				j++;
				header.createCell(j).setCellValue("Company Name");
				j++;
				
				int i=1;
				for(OpenOrderStatus order : openOrdersList){
					Row row = sheet.createRow(i);
					j=0;
					row.createCell(j).setCellValue(order.getProductType());
					j++;
					row.createCell(j).setCellValue(order.getInvoiceNo());
					j++;
					row.createCell(j).setCellValue(order.getEventName());
					j++;
					row.createCell(j).setCellValue(order.getEventDateStr());
					j++;
					row.createCell(j).setCellValue(order.getEventTimeStr());
					j++;
					row.createCell(j).setCellValue(order.getVenueName());
					j++;
					row.createCell(j).setCellValue(order.getVenueCity());
					j++;
					row.createCell(j).setCellValue(order.getVenueState());
					j++;
					row.createCell(j).setCellValue(order.getVenueCountry());
					j++;
					row.createCell(j).setCellValue(order.getZone());
					j++;
					row.createCell(j).setCellValue(order.getSection());
					j++;
					row.createCell(j).setCellValue(order.getRow());
					j++;
					row.createCell(j).setCellValue(order.getSoldQty());
					j++;
					row.createCell(j).setCellValue(order.getSectionTixQty());
					j++;
					row.createCell(j).setCellValue(order.getZoneTixQty());
					j++;
					row.createCell(j).setCellValue(order.getEventTixQty());
					j++;
					row.createCell(j).setCellValue(order.getNetSoldPrice());
					j++;
					row.createCell(j).setCellValue(order.getMarketPrice());
					j++;
					row.createCell(j).setCellValue(order.getNetTotalSoldPrice());
					j++;
					row.createCell(j).setCellValue(order.getProfitAndLoss());
					j++;
					row.createCell(j).setCellValue(order.getSectionMargin());
					j++;
					row.createCell(j).setCellValue(order.getZoneCheapestPrice());
					j++;
					row.createCell(j).setCellValue(order.getZoneTotalPrice());
					j++;
					row.createCell(j).setCellValue(order.getZoneProfitAndLoss());
					j++;
					row.createCell(j).setCellValue(order.getZoneMargin());
					j++;
					row.createCell(j).setCellValue(order.getActualSoldPrice());
					j++;
					row.createCell(j).setCellValue(order.getTotalMarketPrice());
					j++;
					row.createCell(j).setCellValue(order.getTotalActualSoldPrice());
					j++;
					
					row.createCell(j).setCellValue(order.getZoneTixGroupCount());
					j++;
					row.createCell(j).setCellValue(order.getInvoiceDateStr());
					j++;
					row.createCell(j).setCellValue(order.getInternalNotes());
					j++;
					row.createCell(j).setCellValue(order.getLastUpdatedPrice());
					j++;
					row.createCell(j).setCellValue(order.getPriceUpdateCount());
					j++;
					row.createCell(j).setCellValue(order.getPrice()!=null?order.getPrice():0);
					j++;
					row.createCell(j).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					j++;
					row.createCell(j).setCellValue(order.getUrl()!=null?order.getUrl():"");
					j++;
					row.createCell(j).setCellValue(order.getShippingMethod());
					j++;
					row.createCell(j).setCellValue(order.getTrackingNo());
					j++;
					row.createCell(j).setCellValue(order.getSecondaryOrderId());
					j++;
					row.createCell(j).setCellValue(order.getSecondaryOrderType());
					j++;
					row.createCell(j).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					j++;
					row.createCell(j).setCellValue(order.getLastUpdateStr());
					j++;
					row.createCell(j).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					i++;
					row.createCell(j).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=open_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping(value = "/UpdateOpenOrder")
	public String loadSoldTicketOfEvent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException{
		
		try {
			String updateOrderrecords = request.getParameter("updateOrderrecords");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("updateOrderrecords", updateOrderrecords);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_OPEN_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String msg= "";
			Integer id = 0;
			Timestamp date = new Timestamp(new java.util.Date().getTime());
			for (String keyString : requestParams) {
				 String []record = keyString.split(",");
				 if(record.length == 4 && !record[0].isEmpty() && !record[1].isEmpty() && !record[2].isEmpty() && !record[3].isEmpty()){
					 id=Integer.parseInt(record[0]);
					 OpenOrderStatus openOrderStatusId = DAORegistry.getOpenOrderStatusDao().get(id);
					 if (openOrderStatusId != null) {
						openOrderStatusId.setPrice(Double.parseDouble(record[1]));
						openOrderStatusId.setDiscountCouponPrice(Double.parseDouble(record[2]));
						openOrderStatusId.setUrl(record[3]);
						Double price = Double.parseDouble(record[1]);
						if(price != null){
							openOrderStatusId.setLastUpdatedPriceDate(date);
						}else{
							openOrderStatusId.setLastUpdatedPriceDate(null);
						}
							
						DAORegistry.getOpenOrderStatusDao().update(openOrderStatusId);
					} 
				 }else if(record.length == 5 && record[0].trim().isEmpty() && !record[1].isEmpty() && !record[2].isEmpty() && !record[3].isEmpty()){
					 id=Integer.parseInt(record[1]);
					 OpenOrderStatus openOrderStatusId = DAORegistry.getOpenOrderStatusDao().get(id);
					 if (openOrderStatusId != null) {
						openOrderStatusId.setPrice(Double.parseDouble(record[2]));
						openOrderStatusId.setDiscountCouponPrice(Double.parseDouble(record[3]));
						openOrderStatusId.setUrl(record[4]);
						Double price = Double.parseDouble(record[1]);
						if(price != null){
							openOrderStatusId.setLastUpdatedPriceDate(date);
						}else{
							openOrderStatusId.setLastUpdatedPriceDate(null);
						}
							
						DAORegistry.getOpenOrderStatusDao().update(openOrderStatusId);
					} 
				 }
			}			
			msg= "Selected record updated.";*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	/*@RequestMapping(value = "/GetSoldTickets")
	public String loadSoldTicketOfEvent(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String soldTicketPage = "page-sold-tickets";
		String eventIdStr = request.getParameter("eventId");
		String crawlTimeStr = request.getParameter("crawlTime");
		Date crawlTime;
		Integer eventId = null;
		Event event = null;
		if(eventIdStr!=null && !eventIdStr.isEmpty()){
			eventId = Integer.parseInt(eventIdStr);
			event = TMATDAORegistry.getEventDAO().getEventById(eventId);
		}
		if(event==null){
			return soldTicketPage;
		}
		if(crawlTimeStr == null){
			crawlTime = new Date();
		}else{
			crawlTime = new Date(Long.parseLong(crawlTimeStr));
		}
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		String noCrawl = request.getParameter("nocrawl");
		String id = request.getParameter("id");
		map.put("eventId",event.getId());
		map.put("id",id);
		map.put("crawlTime", crawlTime.getTime());
		if(noCrawl==null){
			noCrawl = "false";
		}
		
		OpenOrderStatus openOrderStatus = DAORegistry.getOpenOrderStatusDao().get(Integer.parseInt(id));		
		if("true".equalsIgnoreCase(noCrawl)){
			
			try{
				tickets = TMATDAORegistry.getTicketDAO().getAllActiveTicketsByNormalizedSectionAndEventId(event.getId(), openOrderStatus.getSection());
				if(tickets != null && tickets.size() > 0){
					List<ManagePurchasePrice> managePurchasePrices = TMATDAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());
					Map<String,ManagePurchasePrice> managePurchasePriceMap = new HashMap<String, ManagePurchasePrice>();
					if(managePurchasePrices!=null && !managePurchasePrices.isEmpty() ){
						for(ManagePurchasePrice managePurchasePrice:managePurchasePrices){
							managePurchasePriceMap.put(managePurchasePrice.getExchange()+"-" + managePurchasePrice.getTicketType(), managePurchasePrice);
						}
					}
					Collection<DefaultPurchasePrice> defaultPurchasePrices = TMATDAORegistry.getDefaultPurchasePriceDAO().getAll();
					Map<String, DefaultPurchasePrice> defaultPurhasePriceMap = new HashMap<String, DefaultPurchasePrice>();
					for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
						defaultPurhasePriceMap.put(defaultPurchasePrice.getExchange()+ "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
					}
					
					DecimalFormat df2 = new DecimalFormat(".##");
					df2.setRoundingMode(RoundingMode.UP);
					
					for(Ticket ticket:tickets){
						String mapKey = ticket.getSiteId()+ "-" + (ticket.getTicketDeliveryType()==null?"REGULAR":ticket.getTicketDeliveryType());
						ManagePurchasePrice managePurchasePrice=managePurchasePriceMap.get(mapKey);
						double tempPrice = ticket.getCurrentPrice();
						
						if(managePurchasePrice!=null){
							double serviceFee=managePurchasePrice.getServiceFee();
							double  shippingFee=managePurchasePrice.getShipping();
							int currencyType=managePurchasePrice.getCurrencyType();
							tempPrice=Double.parseDouble(df2.format((currencyType==1?((ticket.getCurrentPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getCurrentPrice() + serviceFee + (shippingFee/ticket.getQuantity())))));
						}else{
							DefaultPurchasePrice defaultPurchasePrice =  defaultPurhasePriceMap.get(mapKey);
							if(defaultPurchasePrice!=null){
								double serviceFee=defaultPurchasePrice.getServiceFees();
								double  shippingFee=defaultPurchasePrice.getShipping();
								int currencyType=defaultPurchasePrice.getCurrencyType();
								tempPrice=Double.parseDouble(df2.format((currencyType==1?((ticket.getCurrentPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getCurrentPrice() + serviceFee + (shippingFee/ticket.getQuantity())))));
							}
						}
						ticket.setPurchasePrice(tempPrice);
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			noCrawl = "true";
			map.put("reload", new Date().getTime());
		}else{
			noCrawl = "false";
		}
		map.put("noCrawl", noCrawl);
		map.put("tickets", tickets);
		map.put("openOrderStatus", openOrderStatus);
		return soldTicketPage;
	}*/
	
	/** 
	 * Fetch list of open orders for invoice
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	/*@RequestMapping(value = "/CreateOpenOrder")
	public String createOpenOrders(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String action = "";
		String invoiceId = "";
		try {
			//Getting Broker ID
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			invoiceId = request.getParameter("invoiceId");
			Integer invId = Integer.parseInt(invoiceId);
			Collection<OpenOrders> openOrderListByInv = DAORegistry.getQueryManagerDAO().getOpenOrderListByInvoice(invId, brokerId);
			Integer customerId = 0;
			Integer eventId = 0;
			for(OpenOrders openOrders: openOrderListByInv){
				customerId = openOrders.getCustomerId();
				eventId = openOrders.getEventId();
			}
			
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().updatedShippingAddress(customerId);
			EventDetails eventDetail = null;
			if(eventId != null){
				eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
			}			
			map.put("eventDtl", eventDetail);
			map.put("openOrders", openOrderListByInv);
			map.put("shippingAddress", shippingAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-create-openorder";
	}*/
	
	/**
	 * Save open orders for invoice
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	/*@RequestMapping(value = "/SaveOpenOrders")
	public String saveOpenOrders(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String action ="";
		String invoiceId = "";
		String eventIdStr = "";
		try {
			action = request.getParameter("action");
			invoiceId = request.getParameter("invoiceId");
			eventIdStr = request.getParameter("eventId");
			if(returnMessage != null){
			 if(action != null && action.equals("openOrderSave")){
				 Integer rowCount = Integer.parseInt(request.getParameter("rowCount"));
				 TicketGroup ticketGroup = null;
				 for(int i=0; i<=rowCount; i++){
					 ticketGroup = new TicketGroup();
					 String section = request.getParameter("section_"+i);
					 if(section != null && !section.isEmpty()){
						String userName = SecurityContextHolder.getContext().getAuthentication().getName();
						String row = request.getParameter("row_"+i);
						String seatHigh = request.getParameter("seatHigh_"+i);
						String seatLow = request.getParameter("seatLow_"+i);
						Integer qty = Integer.parseInt(request.getParameter("qty_"+i));
						Double price = Double.parseDouble(request.getParameter("price_"+i));
						String shippingMethod = request.getParameter("shippingType");
						Integer invId = Integer.parseInt(invoiceId);
						Integer eventId = Integer.parseInt(eventIdStr);
						
						ticketGroup.setSection(section);
						ticketGroup.setRow(row);
						ticketGroup.setSeatHigh(seatHigh);
						ticketGroup.setSeatLow(seatLow);
						ticketGroup.setQuantity(qty);
						ticketGroup.setPrice(price);
						ticketGroup.setShippingMethod(shippingMethod);
						ticketGroup.setEventId(eventId);
						ticketGroup.setInvoiceId(invId);
						ticketGroup.setCreatedDate(new Date());
						ticketGroup.setCreatedBy(userName);
						ticketGroup.setPurchaseOrderId(0);
						ticketGroup.setNotes(null);
						ticketGroup.setLastUpdated(null);
						
						DAORegistry.getTicketGroupDAO().save(ticketGroup);
					 }
				 }
				//update the status as real ticket mapped for the invoice
				 if(ticketGroup != null){
					//DAORegistry.getInvoiceDAO().updateRealTixStatus(Integer.parseInt(invoiceId)); 
				 }
			  }
			 map.put("successMessage","Real ticket created successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong. Please try again..");
		}
		return "redirect:CreateOpenOrder?invoiceId="+invoiceId;
	}*/
	
	/**
	 * Render closed tracking page
	 * @return
	 */
	@RequestMapping(value="/ClosedOrders")
	public String getClosedOrderTrackingPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String sortingString = request.getParameter("sortingString");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistVenueName", artistVenueName);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());
			map.put("sortingString", sortingString);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CLOSED_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ClosedOrderStatusDTO closedOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("closedOrderStatusDTO")), ClosedOrderStatusDTO.class);
			
			if(closedOrderStatusDTO.getStatus() == 1){			
				model.addAttribute("msg", closedOrderStatusDTO.getMessage());
				model.addAttribute("status", closedOrderStatusDTO.getStatus());							
			}else{
				model.addAttribute("msg", closedOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", closedOrderStatusDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", closedOrderStatusDTO.getClosedOrdersPaginationDTO());
				model.addAttribute("closedOrders", closedOrderStatusDTO.getClosedOrders());					
			}else{	
				model.addAttribute("pagingInfo", gson.toJson(closedOrderStatusDTO.getClosedOrdersPaginationDTO()));
				model.addAttribute("closedOrders", gson.toJson(closedOrderStatusDTO.getClosedOrders()));
				model.addAttribute("events", closedOrderStatusDTO.getEventDetails());
				model.addAttribute("profitLossSigns", closedOrderStatusDTO.getProfitLossSign());
				//model.addAttribute("profitLossSigns", ProfitLossSign.values());
				model.addAttribute("selectedSign", closedOrderStatusDTO.getSelectedProfitLossSign());
				model.addAttribute("selectedProduct", closedOrderStatusDTO.getProductType());				
				model.addAttribute("eventId", eventSelect);
				model.addAttribute("artistId", artistId);
				model.addAttribute("venueId", venueId);
				model.addAttribute("artistVenueName", artistVenueName);
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);
				model.addAttribute("brokerId", brokerId.toString());
			}
			
			/*String closedOrderTracking = "page-closed-orders";
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String action = request.getParameter("action");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			String fromDateFinal = "";
			String toDateFinal="";
			Collection<OpenOrderStatus> closedOrderList = null;
			Collection<EventDetails> events = null;
			//String companyProduct = "REWARDTHEFAN";
			Integer count = 0;
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			//Getting Broker Info
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL")){
					companyProduct = "REWARDTHEFAN";
					closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,null,brokerId);
					count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
				}else{
					closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter,null);
					count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter);
				}
			}else{
				companyProduct = "REWARDTHEFAN";
				String product = (String) session.getAttribute("productType");
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
				closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,null,brokerId);
				count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
			}
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("selectedSign",ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()));
			map.put("eventId", eventIdStr);
			map.put("artistId", artistIdStr);
			map.put("venueId", venueIdStr);
			map.put("artistVenueName", artistVenueName);
			map.put("events", events);
			map.put("selectedProduct",companyProduct);
			map.put("profitLossSigns",ProfitLossSign.values());
			map.put("closedOrders", JsonWrapperUtil.getOpenOrderArray(closedOrderList));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "page-closed-orders";
	
	}
	
	/*@RequestMapping(value="/GetClosedOrders")
	public String getClosedOrderTracking(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String eventSelect = request.getParameter("eventSelect");			
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CLOSED_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ClosedOrderStatusDTO closedOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("closedOrderStatusDTO")), ClosedOrderStatusDTO.class);
			
			if(closedOrderStatusDTO.getStatus() == 1){			
				model.addAttribute("msg", closedOrderStatusDTO.getMessage());
				model.addAttribute("status", closedOrderStatusDTO.getStatus());
				model.addAttribute("pagingInfo", closedOrderStatusDTO.getClosedOrdersPaginationDTO());
				model.addAttribute("closedOrders", closedOrderStatusDTO.getClosedOrders());								
			}else{
				model.addAttribute("msg", closedOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", closedOrderStatusDTO.getStatus());
			}
//			JSONObject returnObject = new JSONObject();
//			String eventIdStr = request.getParameter("eventSelect");
//			String artistIdStr = request.getParameter("artistId");
//			String venueIdStr = request.getParameter("venueId");
//			String invoiceNo = request.getParameter("invoiceNo");
//			String fromDateStr = request.getParameter("fromDate");
//			String toDateStr = request.getParameter("toDate");
//			String pageNo = request.getParameter("pageNo");
//			String headerFilter = request.getParameter("headerFilter");
//			String action = request.getParameter("action");
//			String profitLossSign = request.getParameter("profitLoss");
//			String companyProduct = request.getParameter("productType");
//			String externalOrderId = request.getParameter("externalOrderId");
//			String orderId = request.getParameter("orderId");
//			Collection<OpenOrderStatus> closedOrderList = null;
//			String fromDateFinal = "";
//			String toDateFinal="";
//			Integer count=0;
//			if(null ==  profitLossSign || profitLossSign.isEmpty()){
//				profitLossSign = String.valueOf(ProfitLossSign.ALL);
//			}
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
//			//Getting Broker Info
//			HttpSession session = request.getSession(true);
//			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
//			Integer brokerId = Util.getBrokerId(session);
//			if(brokerId == -1){
//				brokerId = 0;
//			}
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
//						closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
//					}else{
//						closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter);
//					}
//					
//				}else{
//					if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
//						closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,false,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,filter,brokerId);
//					}else{
//						closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrders(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWRTW2FilledOrdersCount(eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()), "CLOSED", companyProduct, filter);
//					}
//					
//				}
//			}
//			returnObject.put("closedOrders", JsonWrapperUtil.getOpenOrderArray(closedOrderList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please Try again.");
		}
		return "";
	}*/
	
	
	/*@RequestMapping(value="/ClosedOrdersExportToExcel")
	public void getClosedOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		String eventIdStr = request.getParameter("eventSelect");
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		String invoiceNo = request.getParameter("invoiceNo");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String profitLossSign = request.getParameter("profitLoss");
		String companyProduct = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String orderId = request.getParameter("orderId");
		String headerFilter = request.getParameter("headerFilter");
		Collection<OpenOrderStatus> closedOrderList = null;
		String fromDateFinal = "";
		String toDateFinal="";
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			//Getting Broker Info
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN")){
					closedOrderList = DAORegistry.getQueryManagerDAO().getOpenOrShipmentPendingOrClosedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter,brokerId);
				}else{
					closedOrderList = DAORegistry.getQueryManagerDAO().getRTWRTW2OrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, null, null, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"CLOSED",companyProduct,externalOrderId,orderId,filter);
				}
			}
			
			if(closedOrderList!=null && !closedOrderList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("closed_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("City");
				header.createCell(7).setCellValue("State");
				header.createCell(8).setCellValue("Country");
				header.createCell(9).setCellValue("Product Type");
				header.createCell(10).setCellValue("Internal Notes");
				header.createCell(11).setCellValue("Quantity");
				header.createCell(12).setCellValue("Section");
				header.createCell(13).setCellValue("Row");
				header.createCell(14).setCellValue("Sold Price");
				header.createCell(15).setCellValue("Market Price");
				header.createCell(16).setCellValue("Last Updated Price");
				header.createCell(17).setCellValue("Section Tix Count");
				header.createCell(18).setCellValue("Event Tix Count");
				header.createCell(19).setCellValue("Total Sold Price");
				header.createCell(20).setCellValue("Total Market Price");
				header.createCell(21).setCellValue("Profit/Loss");
				header.createCell(22).setCellValue("Price Updated Count");
				header.createCell(23).setCellValue("Price");
				header.createCell(24).setCellValue("Discount Coupen Price");
				header.createCell(25).setCellValue("Url");
				header.createCell(26).setCellValue("Shipping Method");
				header.createCell(27).setCellValue("Tracking No");
				header.createCell(28).setCellValue("Secondary Order Id");
				header.createCell(29).setCellValue("Secondary Order Type");
				header.createCell(30).setCellValue("Order Id");
				header.createCell(31).setCellValue("Last Updated");
				header.createCell(32).setCellValue("Broker Id");
				header.createCell(33).setCellValue("Company Name");
				int i=1;
				for(OpenOrderStatus order : closedOrderList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getVenueCity());
					row.createCell(7).setCellValue(order.getVenueState());
					row.createCell(8).setCellValue(order.getVenueCountry());
					row.createCell(9).setCellValue(order.getProductType());
					row.createCell(10).setCellValue(order.getInternalNotes());
					row.createCell(11).setCellValue(order.getSoldQty());
					row.createCell(12).setCellValue(order.getSection());
					row.createCell(13).setCellValue(order.getRow());
					row.createCell(14).setCellValue(order.getActualSoldPrice());
					row.createCell(15).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(16).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(17).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(18).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(19).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(20).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(21).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(22).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(23).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(24).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(25).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(26).setCellValue(order.getShippingMethod());
					row.createCell(27).setCellValue(order.getTrackingNo());
					row.createCell(28).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(29).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(30).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(31).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(32).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(33).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=closed_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}*/
	
	
	/**
	 * Render passed tracking page
	 * @return
	 */
	@RequestMapping(value="/PassedOrders")
	public String getPassedOrderTrackingPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try {
			String eventSelect = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistVenueName", artistVenueName);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());			
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PAST_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PastOrderStatusDTO pastOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pastOrderStatusDTO")), PastOrderStatusDTO.class);
			
			if(pastOrderStatusDTO.getStatus() == 1){		
				model.addAttribute("msg", pastOrderStatusDTO.getMessage());
				model.addAttribute("status", pastOrderStatusDTO.getStatus());								
			}else{
				model.addAttribute("msg", pastOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pastOrderStatusDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", pastOrderStatusDTO.getPastOrdersPaginationDTO());
				model.addAttribute("closedOrders", pastOrderStatusDTO.getPastOrders());					
			}else{
				model.addAttribute("pagingInfo", gson.toJson(pastOrderStatusDTO.getPastOrdersPaginationDTO()));
				model.addAttribute("closedOrders", gson.toJson(pastOrderStatusDTO.getPastOrders()));
				model.addAttribute("events", pastOrderStatusDTO.getEventDetails());
				model.addAttribute("profitLossSigns", pastOrderStatusDTO.getProfitLossSign());
				//model.addAttribute("profitLossSigns", ProfitLossSign.values());
				model.addAttribute("selectedSign", pastOrderStatusDTO.getSelectedProfitLossSign());
				model.addAttribute("selectedProduct", pastOrderStatusDTO.getProductType());				
				model.addAttribute("eventId", eventSelect);
				model.addAttribute("artistId", artistId);
				model.addAttribute("venueId", venueId);
				model.addAttribute("artistVenueName", artistVenueName);
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);
				model.addAttribute("brokerId", brokerId.toString());
			}
			
			/*String passedOrderTracking = "page-passed-orders";
			String eventIdStr = request.getParameter("eventSelect");
			String artistVenueName = request.getParameter("artistVenueName");
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String action = request.getParameter("action");
			String profitLossSign = request.getParameter("profitLoss");
			String companyProduct = request.getParameter("productType");
			Collection<OpenOrderStatus> passedOrderList = null;
			Collection<OpenOrderStatus> pastOrderList = null;
			Collection<EventDetails> events = null;
			//String companyProduct = "REWARDTHEFAN";
			Integer count = 0;
			String fromDateFinal = "";
			String toDateFinal="";
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			//Getting Broker Info
			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(action!=null && action.equalsIgnoreCase("search")){
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}else{
					fromDateFinal = null;
					toDateFinal = null;
				}
				if(companyProduct.equalsIgnoreCase("REWARDTHEFAN") || companyProduct.equalsIgnoreCase("ALL")){
					companyProduct = "REWARDTHEFAN";
				}
				passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,false,null,brokerId);
				//count = DAORegistry.getQueryManagerDAO().getPassedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,brokerId);				
			}else{
				companyProduct = "REWARDTHEFAN";
				String product = (String) session.getAttribute("productType");
				if(product!=null && !product.isEmpty()){
					companyProduct = product;
				}
				passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,false,null,brokerId);
				//count = DAORegistry.getQueryManagerDAO().getPassedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,brokerId);
			}
			if(artistIdStr!=null && !artistIdStr.isEmpty()){
				events =  DAORegistry.getEventDetailsDAO().getEventsByArtistCity(Integer.parseInt(artistIdStr),null);
				
			}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
				events = DAORegistry.getEventDetailsDAO().getEventsByVenueId(Integer.parseInt(venueIdStr));
			}
			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus pastOrderObj : passedOrderList){
				openOrderMap.put(pastOrderObj.getInvoiceNo(), pastOrderObj);
			}
			pastOrderList = openOrderMap.values();
			if(pastOrderList != null && pastOrderList.size() > 0){
				count = pastOrderList.size();
			}
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("selectedSign",ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()));
			map.put("eventId", eventIdStr);
			map.put("artistId", artistIdStr);
			map.put("venueId", venueIdStr);
			map.put("artistVenueName", artistVenueName);
			map.put("events", events);
			map.put("selectedProduct",companyProduct);
			map.put("profitLossSigns",ProfitLossSign.values());
			map.put("closedOrders", JsonWrapperUtil.getOpenOrderArray(pastOrderList));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}		
		return "page-passed-orders";
	
	}
	
	/*@RequestMapping(value="/GetPassedOrders")
	public String getPassedOrderTracking(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try {
			String eventSelect = request.getParameter("eventSelect");			
			String artistId = request.getParameter("artistId");
			String venueId = request.getParameter("venueId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String profitLoss = request.getParameter("profitLoss");
			String productType = request.getParameter("productType");
			String invoiceNo = request.getParameter("invoiceNo");
			String externalOrderId = request.getParameter("externalOrderId");
			String orderId = request.getParameter("orderId");
			String action = request.getParameter("action");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventSelect", eventSelect);
			map.put("artistId", artistId);
			map.put("venueId", venueId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("profitLoss", profitLoss);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("externalOrderId", externalOrderId);
			map.put("orderId", orderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_PAST_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PastOrderStatusDTO pastOrderStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("pastOrderStatusDTO")), PastOrderStatusDTO.class);
			
			if(pastOrderStatusDTO.getStatus() == 1){		
				model.addAttribute("msg", pastOrderStatusDTO.getMessage());
				model.addAttribute("status", pastOrderStatusDTO.getStatus());
				model.addAttribute("pagingInfo", pastOrderStatusDTO.getPastOrdersPaginationDTO());
				model.addAttribute("closedOrders", pastOrderStatusDTO.getPastOrders());								
			}else{
				model.addAttribute("msg", pastOrderStatusDTO.getError().getDescription());
				model.addAttribute("status", pastOrderStatusDTO.getStatus());
			}
//			String eventIdStr = request.getParameter("eventSelect");
//			String artistIdStr = request.getParameter("artistId");
//			String venueIdStr = request.getParameter("venueId");
//			String invoiceNo = request.getParameter("invoiceNo");
//			String fromDateStr = request.getParameter("fromDate");
//			String toDateStr = request.getParameter("toDate");
//			String pageNo = request.getParameter("pageNo");
//			String headerFilter = request.getParameter("headerFilter");
//			String action = request.getParameter("action");
//			String profitLossSign = request.getParameter("profitLoss");
//			String companyProduct = request.getParameter("productType");
//			String externalOrderId = request.getParameter("externalOrderId");
//			String orderId = request.getParameter("orderId");
//			Collection<OpenOrderStatus> passedOrderList = null;
//			Collection<OpenOrderStatus> pastOrderList = null;
//			String fromDateFinal = "";
//			String toDateFinal="";
//			Integer count=0;
//			if(null ==  profitLossSign || profitLossSign.isEmpty()){
//				profitLossSign = String.valueOf(ProfitLossSign.ALL);
//			}
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
//			//Getting Broker Info
//			HttpSession session = request.getSession(true);
//			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
//			Integer brokerId = Util.getBrokerId(session);
//			if(brokerId == -1){
//				brokerId = 0;
//			}
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//				}else{
//					fromDateFinal = null;
//					toDateFinal = null;
//				}
//				if(companyProduct != null && !companyProduct.isEmpty()){
//					passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrders(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,false,pageNo,brokerId);
//					//count = DAORegistry.getQueryManagerDAO().getPassedOrdersCount(eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,filter,brokerId);
//				}
//				//Remove Duplicate Invoices
//				Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
//				for(OpenOrderStatus pastOrderObj : passedOrderList){
//					openOrderMap.put(pastOrderObj.getInvoiceNo(), pastOrderObj);
//				}
//				pastOrderList = openOrderMap.values();
//				if(pastOrderList != null && pastOrderList.size() > 0){
//					count = pastOrderList.size();
//				}
//			}
//			returnObject.put("closedOrders", JsonWrapperUtil.getOpenOrderArray(pastOrderList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please Try again.");
		}
		return "";
	}*/
	
	/*@RequestMapping(value="/PassedOrdersExportToExcel")
	public void getPassedOrdersToExport(HttpServletRequest request, HttpServletResponse response){
		String eventIdStr = request.getParameter("eventSelect");
		String artistIdStr = request.getParameter("artistId");
		String venueIdStr = request.getParameter("venueId");
		String invoiceNo = request.getParameter("invoiceNo");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String profitLossSign = request.getParameter("profitLoss");
		String companyProduct = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String orderId = request.getParameter("orderId");
		String headerFilter = request.getParameter("headerFilter");
		Collection<OpenOrderStatus> passedOrderList = null;
		Collection<OpenOrderStatus> pastOrderList = null;
		String fromDateFinal = "";
		String toDateFinal="";
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getOpenOrShipmentPendingOrClosedOrderSearchHeaderFilters(headerFilter);
			//Getting Broker Info
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(null ==  profitLossSign || profitLossSign.isEmpty()){
				profitLossSign = String.valueOf(ProfitLossSign.ALL);
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}else{
				fromDateFinal = null;
				toDateFinal = null;
			}
			if(companyProduct != null && !companyProduct.isEmpty()){
				passedOrderList = DAORegistry.getQueryManagerDAO().getPassedOrdersToExport(invoiceNo,eventIdStr, artistIdStr, venueIdStr, fromDateFinal, toDateFinal, ProfitLossSign.valueOf(profitLossSign.trim().toUpperCase()),"DELETED",companyProduct,externalOrderId,orderId,filter,brokerId);
			}			
			//Remove Duplicate Invoices
			Map<Integer, OpenOrderStatus> openOrderMap  = new HashMap<Integer, OpenOrderStatus>();
			for(OpenOrderStatus pastOrderObj : passedOrderList){
				openOrderMap.put(pastOrderObj.getInvoiceNo(), pastOrderObj);
			}
			pastOrderList = openOrderMap.values();
			
			if(pastOrderList!=null && !pastOrderList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("past_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Invoice Date");
				header.createCell(2).setCellValue("Event Name");
				header.createCell(3).setCellValue("Event Date");
				header.createCell(4).setCellValue("Event Time");
				header.createCell(5).setCellValue("Venue");
				header.createCell(6).setCellValue("Product Type");
				header.createCell(7).setCellValue("Internal Notes");
				header.createCell(8).setCellValue("Quantity");
				header.createCell(9).setCellValue("Section");
				header.createCell(10).setCellValue("Row");
				header.createCell(11).setCellValue("Sold Price");
				header.createCell(12).setCellValue("Market Price");
				header.createCell(13).setCellValue("Last Updated Price");
				header.createCell(14).setCellValue("Section Tix Count");
				header.createCell(15).setCellValue("Event Tix Count");
				header.createCell(16).setCellValue("Total Sold Price");
				header.createCell(17).setCellValue("Total Market Price");
				header.createCell(18).setCellValue("Profit/Loss");
				header.createCell(19).setCellValue("Price Updated Count");
				header.createCell(20).setCellValue("Price");
				header.createCell(21).setCellValue("Discount Coupen Price");
				header.createCell(22).setCellValue("Url");
				header.createCell(23).setCellValue("Shipping Method");
				header.createCell(24).setCellValue("Tracking No");
				header.createCell(25).setCellValue("Secondary Order Id");
				header.createCell(26).setCellValue("Secondary Order Type");
				header.createCell(27).setCellValue("Order Id");
				header.createCell(28).setCellValue("Last Updated");
				header.createCell(29).setCellValue("Broker Id");
				header.createCell(30).setCellValue("Customer Name");
				header.createCell(31).setCellValue("Company Name");
				int i=1;
				for(OpenOrderStatus order : pastOrderList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceNo());
					row.createCell(1).setCellValue(order.getInvoiceDateStr());
					row.createCell(2).setCellValue(order.getEventName());
					row.createCell(3).setCellValue(order.getEventDateStr());
					row.createCell(4).setCellValue(order.getEventTimeStr());
					row.createCell(5).setCellValue(order.getVenueName());
					row.createCell(6).setCellValue(order.getProductType());
					row.createCell(7).setCellValue(order.getInternalNotes());
					row.createCell(8).setCellValue(order.getSoldQty());
					row.createCell(9).setCellValue(order.getSection());
					row.createCell(10).setCellValue(order.getRow());
					row.createCell(11).setCellValue(order.getActualSoldPrice());
					row.createCell(12).setCellValue(order.getMarketPrice()!=null?String.valueOf(order.getMarketPrice()):"");
					row.createCell(13).setCellValue(order.getLastUpdatedPrice()!=null?String.valueOf(order.getLastUpdatedPrice()):"");
					row.createCell(14).setCellValue(order.getSectionTixQty()!=null?String.valueOf(order.getSectionTixQty()):"");
					row.createCell(15).setCellValue(order.getEventTixQty()!=null?String.valueOf(order.getEventTixQty()):"");
					row.createCell(16).setCellValue(order.getTotalActualSoldPrice());
					row.createCell(17).setCellValue(order.getTotalMarketPrice()!=null?String.valueOf(order.getTotalMarketPrice()):"");
					row.createCell(18).setCellValue(order.getProfitAndLoss()!=null?String.valueOf(order.getProfitAndLoss()):"");
					row.createCell(19).setCellValue(order.getPriceUpdateCount()!=null?String.valueOf(order.getPriceUpdateCount()):"");
					row.createCell(20).setCellValue(order.getPrice()!=null?order.getPrice():0);
					row.createCell(21).setCellValue(order.getDiscountCouponPrice()!=null?order.getDiscountCouponPrice():0);
					row.createCell(22).setCellValue(order.getUrl()!=null?order.getUrl():"");
					row.createCell(23).setCellValue(order.getShippingMethod());
					row.createCell(24).setCellValue(order.getTrackingNo());
					row.createCell(25).setCellValue(order.getSecondaryOrderId()!=null?String.valueOf(order.getSecondaryOrderId()):"");
					row.createCell(26).setCellValue(order.getSecondaryOrderType()!=null?String.valueOf(order.getSecondaryOrderType()):"");
					row.createCell(27).setCellValue(order.getOrderId()!=null?order.getOrderId():0);
					row.createCell(28).setCellValue(order.getLastUpdateStr()!=null?String.valueOf(order.getLastUpdateStr()):"");
					row.createCell(29).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					row.createCell(30).setCellValue(order.getCustomerName()!=null?order.getCustomerName():"");
					row.createCell(31).setCellValue(order.getCompanyName()!=null?order.getCompanyName():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=past_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}*/
		
	@RequestMapping(value = "/GetOrderNote")
	public String getOrderNote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String openOrderId = request.getParameter("openOrderId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("openOrderId", openOrderId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_ORDER_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderNoteDTO orderNoteDTO = gson.fromJson(((JsonObject)jsonObject.get("orderNoteDTO")), OrderNoteDTO.class);
			
			if(orderNoteDTO.getStatus() == 1){
				model.addAttribute("msg", orderNoteDTO.getMessage());
				model.addAttribute("status", orderNoteDTO.getStatus());
				model.addAttribute("id", orderNoteDTO.getOrderId());
				model.addAttribute("internalNote", orderNoteDTO.getInternalNote());				
			}else{
				model.addAttribute("msg", orderNoteDTO.getError().getDescription());
				model.addAttribute("status", orderNoteDTO.getStatus());
			}
			
			/*Integer openOrderId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(openOrderIdStr == null || openOrderIdStr.isEmpty()){
				 jObj.put("msg", "Order ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				openOrderId = Integer.parseInt(openOrderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			OpenOrderStatus openOrder = DAORegistry.getOpenOrderStatusDao().get(openOrderId);
			if(openOrder == null){
				jObj.put("msg", "Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
						
			jObj.put("id", openOrder.getId());
			jObj.put("internalNote", openOrder.getInternalNotes());
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateOrderNote")
	public String updateOrderNote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
				
		try{
			String modifyNotesOpenOrderId = request.getParameter("modifyNotesOpenOrderId");
			String modifyNotesOpenOrderNote = request.getParameter("modifyNotesOpenOrderNote");
			String action = request.getParameter("action");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("modifyNotesOpenOrderId", modifyNotesOpenOrderId);
			map.put("modifyNotesOpenOrderNote", modifyNotesOpenOrderNote);
			map.put("action", action);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_ORDER_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			OrderNoteDTO orderNoteDTO = gson.fromJson(((JsonObject)jsonObject.get("orderNoteDTO")), OrderNoteDTO.class);
			
			if(orderNoteDTO.getStatus() == 1){
				model.addAttribute("msg", orderNoteDTO.getMessage());
				model.addAttribute("status", orderNoteDTO.getStatus());			
			}else{
				model.addAttribute("msg", orderNoteDTO.getError().getDescription());
				model.addAttribute("status", orderNoteDTO.getStatus());
			}
			
			/*OpenOrderStatus openOrder = null;
			String returnMessage = "";
			Integer openOrderId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(openOrderIdStr == null || openOrderIdStr.isEmpty()){
				 jObj.put("msg", "Order ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				openOrderId = Integer.parseInt(openOrderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			openOrder = DAORegistry.getOpenOrderStatusDao().get(openOrderId);
			if(openOrder == null){
				jObj.put("msg", "Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
//			if(openOrderNote == null || openOrderNote.isEmpty()){
//				 jObj.put("msg", "Please provide Order Note.");
//				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
//				 return;
//			}
			
			if(action!=null && action.equalsIgnoreCase("update")){
				openOrder.setInternalNotes(openOrderNote);
				try{
				DAORegistry.getOpenOrderStatusDao().update(openOrder);
				}catch(Exception e){
					e.printStackTrace();
					jObj.put("msg", "Error occured while updating Internal Notes.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				returnMessage = "Notes updated Successfully.";				
			}				
			jObj.put("msg", returnMessage);
			jObj.put("status", 1);
			
			//Tracking User Action
			String userActionMsg = "Open Order Notes Updated Successfully.";
			Util.userActionAudit(request, openOrderId, userActionMsg);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	@RequestMapping(value="/RefreshOpenOrder")
	public String refreshOpenOrder(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_REFRESH_OPEN_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String msg="";
			Collection<SoldTicketDetail> ticketDetail =  new ArrayList<SoldTicketDetail>();
			ticketDetail = DAORegistry.getQueryManagerDAO().getAllSoldUnfilledNewTickets();
			OpenOrderUtil.addNewOpenOrders(ticketDetail);
			msg="OK";
			IOUtils.write(msg.getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	/*@RequestMapping(value="/Fedex")
	public String getFedexDetails(HttpServletRequest request, HttpServletResponse response,ModelMap map){
		String ticketTracking = "page-fedex";
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}


		return ticketTracking;
	}*/
	
	/**
	 * Render ticket tracking page
	 * @return
	 */
	/*@RequestMapping(value="/TicketTracking")
	public String getTicketTrackingPage(ModelMap map){
		String ticketTracking = "page-ticket-tracking";
		
		map.put("url", "Deliveries");
		return ticketTracking;
	}*/
}
