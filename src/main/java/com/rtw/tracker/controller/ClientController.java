package com.rtw.tracker.controller;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.pojos.AddCustomerDTO;
import com.rtw.tracker.pojos.AddUserDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistVenueAndCategoryDTO;
import com.rtw.tracker.pojos.CommonRespInfo;
import com.rtw.tracker.pojos.CustomerAddRewardPointDTO;
import com.rtw.tracker.pojos.CustomerAddWalletDTO;
import com.rtw.tracker.pojos.CustomerInfoDTO;
import com.rtw.tracker.pojos.CustomerInfoFavouriteEventDTO;
import com.rtw.tracker.pojos.CustomerInfoInvoiceDTO;
import com.rtw.tracker.pojos.CustomerInfoInvoiceTicketDTO;
import com.rtw.tracker.pojos.CustomerInfoLoyalFanDTO;
import com.rtw.tracker.pojos.CustomerInfoPODTO;
import com.rtw.tracker.pojos.CustomerInfoRewardPointsHistoryDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOfferDetailsDTO;
import com.rtw.tracker.pojos.CustomerPromotionalOrderSummaryDTO;
import com.rtw.tracker.pojos.DiscountCodeDetailDTO;
import com.rtw.tracker.pojos.EditUsersDTO;
import com.rtw.tracker.pojos.GenerateDiscountCodeDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.ManageCustomersDTO;
import com.rtw.tracker.pojos.ManageUsersDTO;
import com.rtw.tracker.pojos.SaveDiscountCodeDTO;
import com.rtw.tracker.pojos.SharingDiscountCodeDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
@RequestMapping({"/Client"})
public class ClientController {

	private static Logger LOGGER = LoggerFactory.getLogger(ClientController.class);
	
	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
//	private MailManager mailManager;
//	public MailManager getMailManager() {
//		return mailManager;
//	}
//
//	public void setMailManager(MailManager mailManager) {
//		this.mailManager = mailManager;
//	}
	
	/**
	 * Manage customer details
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping({"/ManageDetails"})
	public String clientBrokerManageDetailsPage(HttpServletRequest request, HttpServletResponse response, Model model,HttpSession session){
		
		try{
			String customerName = request.getParameter("customerName");
			String productType = request.getParameter("productType");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerName", customerName);
			map.put("productType", productType);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_CUSTOMERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ManageCustomersDTO manageCustomersDTO = gson.fromJson(((JsonObject)jsonObject.get("manageCustomersDTO")), ManageCustomersDTO.class);
			
			if(manageCustomersDTO.getStatus() == 1){
				model.addAttribute("msg", manageCustomersDTO.getMessage());
				model.addAttribute("status", manageCustomersDTO.getStatus());
			}else{
				model.addAttribute("msg", manageCustomersDTO.getError().getDescription());
				model.addAttribute("status", manageCustomersDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", manageCustomersDTO.getPaginationDTO());
				model.addAttribute("customers", manageCustomersDTO.getCustomersDTO());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(manageCustomersDTO.getPaginationDTO()));
				model.addAttribute("customers", gson.toJson(manageCustomersDTO.getCustomersDTO()));
				model.addAttribute("countries", manageCustomersDTO.getCountryDTO());
				model.addAttribute("layoutProduct", manageCustomersDTO.getProductType());
				model.addAttribute("brokerId", brokerId.toString());
			}
			
			/*Integer count =0;
			String customerName = request.getParameter("customerName");
			String productType = request.getParameter("productType");
			List<Country> countryList=null;
			Collection<Customers> customerList = null;
			try{
				GridHeaderFilters filter = new GridHeaderFilters();
				String product = (String) session.getAttribute("productType");
				if(product!=null && !product.isEmpty()){
					productType = product;
				}
				Integer brokerId = Util.getBrokerId(session);
				customerList = DAORegistry.getQueryManagerDAO().getCustomers(null,customerName,productType,brokerId,filter,null);			
				count = DAORegistry.getCustomerDAO().getTotalCustomerCount(null,customerName,productType,brokerId,filter);			
				countryList = DAORegistry.getQueryManagerDAO().getCountries();
			}catch(Exception e){
				e.printStackTrace();
				map.put("error", "Not able to load, Customer Details.");
			}
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			map.put("customers", JsonWrapperUtil.getCustomerArray(customerList));
			map.put("countries", countryList);
			map.put("layoutProduct", productType);
			return "page-client-manage-details";*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "Not able to load, Customer Details.");
		}
		return "page-client-manage-details";
	}
	
	/*@RequestMapping({"/GetCustomers"})
	public String getCustomers(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
//		JSONObject returnObject = new JSONObject();
//		String customerType = request.getParameter("customerType");
//		String pageNo = request.getParameter("pageNo");
//		String headerFilter = request.getParameter("headerFilter");
//		//String searchValue = request.getParameter("searchValue");
//		//String customerType = request.getParameter("customerTypeSearchValues");
//		Integer count =0;
//		try{
//			Integer brokerId = Util.getBrokerId(session);
//			String productType = (String) session.getAttribute("productType");
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
//			if(filter.getProductType()==null || filter.getProductType().isEmpty()){
//				filter.setProductType(productType);
//			}
//			Collection<Customers> customerList = DAORegistry.getQueryManagerDAO().getCustomers(null,null,null,brokerId,filter,pageNo);
//			count = DAORegistry.getCustomerDAO().getTotalCustomerCount(null,null,null,brokerId,filter);
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			returnObject.put("customers", JsonWrapperUtil.getCustomerArray(customerList));
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String productTypeSession = (String) session.getAttribute("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("productTypeSession", productTypeSession);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_MANAGE_CUSTOMERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ManageCustomersDTO manageCustomersDTO = gson.fromJson(((JsonObject)jsonObject.get("manageCustomersDTO")), ManageCustomersDTO.class);
			
			if(manageCustomersDTO.getStatus() == 1){
				model.addAttribute("msg", manageCustomersDTO.getMessage());
				model.addAttribute("status", manageCustomersDTO.getStatus());
				model.addAttribute("pagingInfo", manageCustomersDTO.getPaginationDTO());
				model.addAttribute("customers", manageCustomersDTO.getCustomersDTO());
			}else{
				model.addAttribute("msg", manageCustomersDTO.getError().getDescription());
				model.addAttribute("status", manageCustomersDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}*/
	
	/*@RequestMapping({"/ExportToExcel"})
	public void customersToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		String searchBy = request.getParameter("searchBy");
		String searchValue = request.getParameter("searchValue");
		String productType = request.getParameter("productType");
		String customerType = request.getParameter("customerTypeSearchValues");
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getCustomerSearchHeaderFilters(headerFilter);
			Integer brokerId = Util.getBrokerId(session);
			Collection<Customers> customerList = DAORegistry.getQueryManagerDAO().getCustomersToExport(null,searchBy,searchValue,productType,brokerId,filter,customerType);
			
			if(customerList!=null && !customerList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("customers");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Customer Id");
				header.createCell(1).setCellValue("Customer Type");
				header.createCell(2).setCellValue("Product Type");
				header.createCell(3).setCellValue("Signup Type");
				header.createCell(4).setCellValue("Customer Status");
				header.createCell(5).setCellValue("Company Name");
				header.createCell(6).setCellValue("Customer Name");
				header.createCell(7).setCellValue("Last Name");
				header.createCell(8).setCellValue("Customer Email");
				header.createCell(9).setCellValue("Address Line1");
				header.createCell(10).setCellValue("Address Line2");
				header.createCell(11).setCellValue("City");
				header.createCell(12).setCellValue("Billing Address Id");
				header.createCell(13).setCellValue("State");
				header.createCell(14).setCellValue("Country");
				header.createCell(15).setCellValue("Zipcode");
				header.createCell(16).setCellValue("Client");
				header.createCell(17).setCellValue("Broker");
				header.createCell(18).setCellValue("Phone");
				header.createCell(19).setCellValue("Referrer Code");
				header.createCell(20).setCellValue("Broker Id");
				Integer i=1;
				for(Customers customer : customerList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(customer.getCustomerId());
					row.createCell(1).setCellValue(customer.getCustomerType());
					row.createCell(2).setCellValue(customer.getProductType()!=null?customer.getProductType():"");
					row.createCell(3).setCellValue(customer.getSignupType()!=null?customer.getSignupType():"");
					row.createCell(4).setCellValue(customer.getCustomerStatus());
					row.createCell(5).setCellValue(customer.getCompanyName()!=null?customer.getCompanyName():"");
					row.createCell(6).setCellValue(customer.getCustomerName()!=null?customer.getCustomerName():"");
					row.createCell(7).setCellValue(customer.getLastName()!=null?customer.getLastName():"");
					row.createCell(8).setCellValue(customer.getCustomerEmail());
					row.createCell(9).setCellValue(customer.getAddressLine1()!=null?customer.getAddressLine1():"");
					row.createCell(10).setCellValue(customer.getAddressLine2()!=null?customer.getAddressLine2():"");
					row.createCell(11).setCellValue(customer.getCity()!=null?customer.getCity():"");
					row.createCell(12).setCellValue(customer.getBillingAddressId()!=null?customer.getBillingAddressId():0);
					row.createCell(13).setCellValue(customer.getState()!=null?customer.getState():"");
					row.createCell(14).setCellValue(customer.getCountry()!=null?customer.getCountry():"");
					row.createCell(15).setCellValue(customer.getZipCode()!=null?customer.getZipCode():"");
					row.createCell(16).setCellValue(customer.getIsClient()==true?"Yes":"No");
					row.createCell(17).setCellValue(customer.getIsBroker()==true?"Yes":"No");
					row.createCell(18).setCellValue(customer.getPhone()!=null?customer.getPhone():"");
					row.createCell(19).setCellValue(customer.getReferrerCode()!=null?customer.getReferrerCode():"");
					row.createCell(20).setCellValue(customer.getBrokerId()!=null?customer.getBrokerId().toString():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=customers.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/AddCustomerPopup"})
	public String addCustomerPopup(@ModelAttribute Customer customer, HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		//List<State> stateList = null;
		List<Country> countryList = null;
		try {
			//Integer countryId = 1;
			//stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//map.put("states", stateList);
		map.put("countries", countryList);
		map.put("isPopup", true);
		return "page-add-customer-popup";
	}*/
	
	/**
	 * Save customer details
	 * @param customer
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping({"/AddCustomer"})
	public String addCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("brokerId", brokerId.toString());
			map.put("action", action);
			
			if(action != null && action.equalsIgnoreCase("addCustomer")){
				String customerName = request.getParameter("customerName");
				String lastName = request.getParameter("lastName");				
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String extension = request.getParameter("extension");
				String otherPhone = request.getParameter("otherPhone");
				String representativeName = request.getParameter("representativeName");
				String companyName = request.getParameter("companyName");
				String customerType = request.getParameter("customerType");
				String customerLevel = request.getParameter("customerLevel");
				String signupType = request.getParameter("signupType");
				String clientBrokerType = request.getParameter("clientBrokerType");
				
				String blFirstName = request.getParameter("blFirstName");
				String blLastName = request.getParameter("blLastName");
				String blEmail = request.getParameter("blEmail");
				String addressLine1 = request.getParameter("addressLine1");
				String addressLine2 = request.getParameter("addressLine2");
				String countryName = request.getParameter("countryName");
				String stateName = request.getParameter("stateName");
				String city = request.getParameter("city");
				String zipCode = request.getParameter("zipCode");
				
				String shFirstName = request.getParameter("shFirstName");
				String shLastName = request.getParameter("shLastName");
				String shEmail = request.getParameter("shEmail");
				String shStreet1 = request.getParameter("shStreet1");
				String shStreet2 = request.getParameter("shStreet2");
				String shCountryName = request.getParameter("shCountryName");
				String shStateName = request.getParameter("shStateName");
				String shCity = request.getParameter("shCity");
				String shZipCode = request.getParameter("shZipCode");

				map.put("customerName", customerName);
				map.put("lastName", lastName);
				map.put("email", email);
				map.put("phone", phone);
				map.put("extension", extension);
				map.put("otherPhone", otherPhone);
				map.put("representativeName", representativeName);
				map.put("companyName", companyName);
				map.put("customerType", customerType);
				map.put("customerLevel", customerLevel);
				map.put("signupType", signupType);
				map.put("clientBrokerType", clientBrokerType);
				
				map.put("blFirstName", blFirstName);
				map.put("blLastName", blLastName);
				map.put("blEmail", blEmail);
				map.put("addressLine1", addressLine1);
				map.put("addressLine2", addressLine2);
				map.put("countryName", countryName);
				map.put("stateName", stateName);
				map.put("city", city);
				map.put("zipCode", zipCode);
				
				map.put("shFirstName", shFirstName);
				map.put("shLastName", shLastName);
				map.put("shEmail", shEmail);
				map.put("shStreet1", shStreet1);
				map.put("shStreet2", shStreet2);
				map.put("shCountryName", shCountryName);
				map.put("shStateName", shStateName);
				map.put("shCity", shCity);
				map.put("shZipCode", shZipCode);
			}
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ADD_CUSTOMER_INFO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AddCustomerDTO addCustomerDTO = gson.fromJson(((JsonObject)jsonObject.get("addCustomerDTO")), AddCustomerDTO.class);
			
			if(addCustomerDTO.getStatus() == 1){
				model.addAttribute("msg", addCustomerDTO.getMessage());
				model.addAttribute("status", addCustomerDTO.getStatus());
				model.addAttribute("countries", addCustomerDTO.getCountryDTO());
				model.addAttribute("states", addCustomerDTO.getStateDTO());
			}else{
				model.addAttribute("msg", addCustomerDTO.getError().getDescription());
				model.addAttribute("status", addCustomerDTO.getStatus());
			}
			
			/*String returnPage = "page-add-customer";
			List<State> stateList = null;
			List<Country> countryList = null;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			Integer countryId = 1;
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			String isPopup = request.getParameter("isPopup");
			if(isPopup!=null && isPopup.equalsIgnoreCase("true")){
				//jObj.put("", arg1);
			}
			
			String action = request.getParameter("action");			
			if(action != null && action.equals("action")){
				String customerName = request.getParameter("customerName");
				String lastName = request.getParameter("lastName");				
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String extension = request.getParameter("extension");
				String otherPhone = request.getParameter("otherPhone");
				String representativeName = request.getParameter("representativeName");
				String companyName = request.getParameter("companyName");
				String customerType = request.getParameter("customerType");
				String customerLevel = request.getParameter("customerLevel");
				String signupType = request.getParameter("signupType");
				
				String returnMessage = "";
				Customer customer = new Customer();
				//Adding customer to database
				if(customerName == null || customerName.isEmpty()){
					returnMessage = "Not able to identify First Name.";
				}else if(lastName == null || lastName.isEmpty()){
					returnMessage = "Not able to identify Last Name.";
				}else if(email == null || email.isEmpty()){
					returnMessage = "Not able to identify Email.";
				}else if(phone == null || phone.isEmpty()){
					returnMessage = "Not able to identify Phone No.";
				}
				
				//Checking Billing Address
				String blFirstName = request.getParameter("blFirstName");
				String blLastName = request.getParameter("blLastName");
				String blEmail = request.getParameter("blEmail");
				String addressLine1 = request.getParameter("addressLine1");
				String blAddressLine2 = request.getParameter("addressLine2");
				String country = request.getParameter("countryName");
				String state = request.getParameter("stateName");
				String city = request.getParameter("city");
				String zipCode = request.getParameter("zipCode");
				
				if(blFirstName == null || blFirstName.isEmpty()){
					returnMessage = "Not able to identify Billing Address - First Name.";
				}else if(blLastName == null || blLastName.isEmpty()){
					returnMessage = "Not able to identify Billing Address - Last Name.";
//				}else if(blEmail == null || blEmail.isEmpty()){
//					returnMessage = "Not able to identify Billing Address - Email.";
				}else if(addressLine1 == null || addressLine1.isEmpty()){
					returnMessage = "Not able to identify Billing Address - Street1.";
				}else if(zipCode == null || zipCode.isEmpty()){
					returnMessage = "Not able to identify Billing Address - ZipCode.";
				}else if(city == null || city.isEmpty()){
					returnMessage = "Not able to identify Billing Address - City.";
				}else if(country == null || country.isEmpty()){
					returnMessage = "Not able to identify Billing Address - Country.";
				}else if(state == null || state.isEmpty()){
					returnMessage = "Not able to identify Billing Address - State.";
				}
				
				//Checking Shipping Address
				String shFirstName = request.getParameter("shFirstName");
				String shLastName = request.getParameter("shLastName");
				String shEmail = request.getParameter("shEmail");
				String shstreet1 = request.getParameter("shStreet1");
				String shAddressLine2 = request.getParameter("shStreet2");
				String shCountry = request.getParameter("shCountryName");
				String shState = request.getParameter("shStateName");
				String shCity = request.getParameter("shCity");
				String shZipCode = request.getParameter("shZipCode");
				
				if(shFirstName == null || shFirstName.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - First Name.";
				}else if(shLastName == null || shLastName.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - Last Name.";
//				}/*else if(shEmail == null || shEmail.isEmpty()){
//					returnMessage = "Not able to identify Shipping Address - Email.";
				}else if(shstreet1 == null || shstreet1.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - Street1.";
				}else if(shZipCode == null || shZipCode.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - ZipCode.";
				}else if(shCity == null || shCity.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - City.";
				}else if(shCountry == null || shCountry.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - Country.";
				}else if(shState == null || shState.isEmpty()){
					returnMessage = "Not able to identify Shipping Address - State.";
				}
				
				if(returnMessage.isEmpty()){
					Customer isCustomerExists = DAORegistry.getCustomerDAO().checkCustomerExists(email);
					if(isCustomerExists != null){
						returnMessage = "There is already an customer with this Username or Email.";
					}else{
						customer.setCustomerName(customerName);
						customer.setLastName(lastName);
						customer.setEmail(email);
						customer.setPhone(phone);
						customer.setExtension(extension);
						customer.setOtherPhone(otherPhone);
						customer.setRepresentativeName(representativeName);
						customer.setCompanyName(companyName);
						customer.setCustomerType(CustomerType.valueOf(customerType));
						customer.setCustomerLevel(CustomerLevel.valueOf(customerLevel));
						customer.setSignupType(SignupType.valueOf(signupType));
						customer.setIsMailSent(false);
						customer.setReferrerCode(email);
						
						CustomerAddress billingAddress = null;
						if(addressLine1 != null && !addressLine1.isEmpty()){
							LOGGER.info("Saving the billing address ..");
							billingAddress = new CustomerAddress();
							
							Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
							State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
							
							billingAddress.setFirstName(blFirstName);
							billingAddress.setLastName(blLastName);
							if(blEmail != null && !blEmail.isEmpty()){
								billingAddress.setEmail(blEmail);
							}
							billingAddress.setAddressLine1(addressLine1);
							billingAddress.setAddressLine2(blAddressLine2);
							billingAddress.setCountry(countryObj);
							billingAddress.setState(stateObj);
							billingAddress.setCity(city);
							billingAddress.setZipCode(zipCode);
							billingAddress.setLastUpdated(new Date());
							billingAddress.setStatus(EventStatus.ACTIVE);
							billingAddress.setAddressType(AddressType.valueOf("BILLING_ADDRESS"));
						}
						
						CustomerAddress shippingAddress = null;
						String clientBrokerType = request.getParameter("clientBrokerType");
						if(shstreet1 != null && !shstreet1.isEmpty()){
							LOGGER.info("Saving the shipping address ..");
							shippingAddress = new CustomerAddress();
							
							Country countryObj1 = DAORegistry.getCountryDAO().get(Integer.parseInt(shCountry.trim()));
							State stateObj1 =  DAORegistry.getStateDAO().get(Integer.parseInt(shState.trim()));
							
							shippingAddress.setFirstName(shFirstName);
							shippingAddress.setLastName(shLastName);
							if(shEmail != null && !shEmail.isEmpty()){
								shippingAddress.setEmail(shEmail);
							}														
							shippingAddress.setAddressLine1(addressLine1);
							shippingAddress.setAddressLine2(shAddressLine2);
							shippingAddress.setCountry(countryObj1);
							shippingAddress.setState(stateObj1);
							shippingAddress.setCity(shCity);
							shippingAddress.setZipCode(shZipCode);
							shippingAddress.setLastUpdated(new Date());
							shippingAddress.setStatus(EventStatus.ACTIVE);
							shippingAddress.setAddressType(AddressType.valueOf("SHIPPING_ADDRESS"));

						}
						if(clientBrokerType!=null){
							if(clientBrokerType.equals("both")){
								customer.setIsBroker(true);
								customer.setIsClient(true);
							}else if(clientBrokerType.equals("client")){
								customer.setIsBroker(false);
								customer.setIsClient(true);
							}else if(clientBrokerType.equals("broker")){
								customer.setIsBroker(true);
								customer.setIsClient(false);
							}
						}
						//persist the customer info into database						
						customer.setBrokerId(brokerId);
						customer.setUserName(customer.getEmail());
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setApplicationPlatForm(ApplicationPlatform.TICK_TRACKER);
						customer.setSignupDate(new Date());
						
//						Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
//						String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
//						Long referrerNumber =  Long.valueOf(property.getValue());
//						referrerNumber++;  
//						//customer.setReferrerCode(Util.generateCustomerReferalCode());   
//						//property.setValue(String.valueOf(referrerNumber));
						DAORegistry.getCustomerDAO().save(customer);  
//						DAORegistry.getPropertyDAO().saveOrUpdate(property);
						
						CustomerLoyalty customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setCustomerId(customer.getId());
						customerLoyalty.setActivePoints(0.00);
						customerLoyalty.setLatestEarnedPoints(0.00);
						customerLoyalty.setLatestSpentPoints(0.00);
						customerLoyalty.setTotalEarnedPoints(0.00);
						customerLoyalty.setTotalSpentPoints(0.00);
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setPendingPoints(0.00);
						
						//Save Customer Loyalty Information
						DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
						
						
						if(billingAddress != null){
							billingAddress.setCustomerId(customer.getId());
							DAORegistry.getCustomerAddressDAO().save(billingAddress);
							LOGGER.info("Saved the billing address for " +customer.getId());
						}
						
						if(shippingAddress != null){
							shippingAddress.setCustomerId(customer.getId());
							DAORegistry.getCustomerAddressDAO().save(shippingAddress);
							LOGGER.info("Saved the shipping address for " +customer.getId());
						}
						returnMessage = "Customer saved successfully";
						jObj.put("msg", returnMessage);
						jObj.put("status", 1);
						
						//Tracking User Action
						String userActionMsg = "Customer Created Successfully.";
						Util.userActionAudit(request, customer.getId(), userActionMsg);
					}
			}else{
					jObj.put("msg", returnMessage);
				}
			}
			if(countryList != null){
				JSONArray countryListArray = new JSONArray();
				JSONObject countryListObject = null;
				for(Country country : countryList){
					countryListObject = new JSONObject();
					countryListObject.put("id", country.getId());
					countryListObject.put("name", country.getName());
					countryListArray.put(countryListObject);
				}
				jObj.put("countries", countryListArray);
			}
			if(stateList != null){
				JSONArray stateListArray = new JSONArray();
				JSONObject stateListObject = null;
				for(State state : stateList){
					stateListObject = new JSONObject();
					stateListObject.put("id", state.getId());
					stateListObject.put("name", state.getName());
					stateListArray.put(stateListObject);
				}
				jObj.put("states", stateListArray);
			}
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	@RequestMapping(value = "/UpdateCustomerDetails")
	public String updateCustomerDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String customerIdStr = request.getParameter("id");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("action", action);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			if(action.equalsIgnoreCase("customerInfo")){
				String clientBrokerType = request.getParameter("clientBrokerType");
				String name = request.getParameter("name");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String companyName = request.getParameter("companyName");
				String customerType = request.getParameter("customerType");
				String customerLevel = request.getParameter("customerLevel");
				String signupType = request.getParameter("signupType");
				String extension = request.getParameter("extension");
				String otherPhone = request.getParameter("otherPhone");
				String representativeName = request.getParameter("representativeName");
				
				map.put("clientBrokerType", clientBrokerType);
				map.put("name", name);
				map.put("lastName", lastName);
				map.put("email", email);
				map.put("phone", phone);
				map.put("companyName", companyName);
				map.put("customerType", customerType);
				map.put("customerLevel", customerLevel);
				map.put("signupType", signupType);
				map.put("extension", extension);
				map.put("otherPhone", otherPhone);
				map.put("representativeName", representativeName);
			}else if(action.equalsIgnoreCase("shippingInfo")){
				String shippingAddrId = request.getParameter("shippingAddrId");
				String shFirstName = request.getParameter("shFirstName");
				String shLastName = request.getParameter("shLastName");
				String shAddressLine1 = request.getParameter("shAddressLine1");
				String shAddressLine2 = request.getParameter("shAddressLine2");
				String shEmail = request.getParameter("shEmail");
				String shPhone = request.getParameter("shPhone");
				String shZipCode = request.getParameter("shZipCode");
				String shCity = request.getParameter("shCity");					
				String shCountryName = request.getParameter("shCountryName");
				String shStateName = request.getParameter("shStateName");
				
				map.put("shippingAddrId", shippingAddrId);
				map.put("shFirstName", shFirstName);
				map.put("shLastName", shLastName);
				map.put("shAddressLine1", shAddressLine1);
				map.put("shAddressLine2", shAddressLine2);
				map.put("shEmail", shEmail);
				map.put("shPhone", shPhone);
				map.put("shZipCode", shZipCode);
				map.put("shCity", shCity);
				map.put("shCountryName", shCountryName);
				map.put("shStateName", shStateName);
			}else if(action.equalsIgnoreCase("billingInfo")){
				String blFirstName = request.getParameter("blFirstName");
				String blLastName = request.getParameter("blLastName");
				String addressLine1 = request.getParameter("addressLine1");
				String addressLine2 = request.getParameter("addressLine2");
				String blEmail = request.getParameter("blEmail");
				String blPhone = request.getParameter("blPhone");
				String zipCode = request.getParameter("zipCode");
				String city = request.getParameter("city");					
				String countryName = request.getParameter("countryName");
				String stateName = request.getParameter("stateName");
				
				map.put("blFirstName", blFirstName);
				map.put("blLastName", blLastName);
				map.put("addressLine1", addressLine1);
				map.put("addressLine2", addressLine2);
				map.put("blEmail", blEmail);
				map.put("blPhone", blPhone);
				map.put("zipCode", zipCode);
				map.put("city", city);
				map.put("countryName", countryName);
				map.put("stateName", stateName);
			}else if(action.equalsIgnoreCase("notes")){				
				String notes = request.getParameter("notes");
				
				map.put("notes", notes);
			}else if(action.equalsIgnoreCase("loyalFan")){
				String artistSelection = request.getParameter("artistSelection");
				String loyalFanSelection = request.getParameter("loyalFanSelection");
				String city = request.getParameter("city");
				String state = request.getParameter("state");
				String country = request.getParameter("country");
				String zipCode = request.getParameter("zipCode");
				String categoryName = request.getParameter("categoryName");
				
				map.put("artistSelection", artistSelection);
				map.put("loyalFanSelection", loyalFanSelection);
				map.put("city", city);
				map.put("state", state);
				map.put("country", country);
				map.put("zipCode", zipCode);
				map.put("categoryName", categoryName);
			}else if(action.equalsIgnoreCase("favouriteEvent")){
				String eventIds = request.getParameter("eventIds");
				
				map.put("eventIds", eventIds);
			}
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CUSTOMER_INFO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoDTO customerInfoDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoDTO")), CustomerInfoDTO.class);
			
			if(customerInfoDTO.getStatus() == 1){
				model.addAttribute("msg", customerInfoDTO.getMessage());
				model.addAttribute("status", customerInfoDTO.getStatus());
				model.addAttribute("customerInfo", customerInfoDTO.getCustomerTopLevelInfoDTO());
				model.addAttribute("billingInfo", customerInfoDTO.getBillingAddressDTO());
				model.addAttribute("shippingInfo", customerInfoDTO.getShippingAddressDTO());
				model.addAttribute("shippingPagingInfo", customerInfoDTO.getShippingPaginationDTO());
				model.addAttribute("shippingCount", customerInfoDTO.getShippingCount());
				model.addAttribute("loyalFanInfo", customerInfoDTO.getLoyalFanDTO());
				model.addAttribute("customers", customerInfoDTO.getCustomersDTO());
			}else{
				model.addAttribute("msg", customerInfoDTO.getError().getDescription());
				model.addAttribute("status", customerInfoDTO.getStatus());
			}
			
			/*JSONObject jsonObject = new JSONObject();
			if(action!=null && !action.isEmpty() && customerIdStr!=null && !customerIdStr.isEmpty()){
				
				Integer customerId = Integer.parseInt(customerIdStr);
				Customer customer = DAORegistry.getCustomerDAO().get(customerId);
				String userActionMsg = "";
				
				if(action.equalsIgnoreCase("customerInfo")){
					String msg = "";
					String clientBroker = request.getParameter("clientBrokerType");
					String firstName = request.getParameter("name");
					String lastName = request.getParameter("lastName");
					String email = request.getParameter("email");
					String phone = request.getParameter("phone");
					String companyName = request.getParameter("companyName");
					
					if(firstName == null || firstName.isEmpty()){
						msg = "Not able to identify First Name.";
					}else if(lastName == null || lastName.isEmpty()){
						msg = "Not able to identify Last Name.";
					}else if(email == null || email.isEmpty()){
						msg = "Not able to identify Email.";
					}else if(phone == null || phone.isEmpty()){
						msg = "Not able to identify Phone.";
					}else if(!customer.getEmail().equalsIgnoreCase(email)){
						Customer isCustomerExists = DAORegistry.getCustomerDAO().checkCustomerExists(email);
						if(isCustomerExists != null){
							msg = "There is already an customer with this Username or Email.";
						}
					}
					if(msg.isEmpty()){
						if(clientBroker!=null){
							if(clientBroker.equals("both")){
								customer.setIsBroker(true);
								customer.setIsClient(true);
							}else if(clientBroker.equals("client")){
								customer.setIsBroker(false);
								customer.setIsClient(true);
							}else if(clientBroker.equals("broker")){
								customer.setIsBroker(true);
								customer.setIsClient(false);
							}
						}
						customer.setCustomerType(CustomerType.valueOf(request.getParameter("customerType")));
						customer.setCustomerLevel(CustomerLevel.valueOf(request.getParameter("customerLevel")));
						customer.setSignupType(SignupType.valueOf(request.getParameter("signupType")));
						customer.setCustomerName(firstName);
						customer.setCompanyName(companyName);
						customer.setLastName(lastName);
						customer.setEmail(email);
						customer.setReferrerCode(email);
						customer.setUserName(email);
						customer.setPhone(phone);
						customer.setExtension(request.getParameter("extension"));
						customer.setOtherPhone(request.getParameter("otherPhone"));
						customer.setRepresentativeName(request.getParameter("representativeName"));
						
						DAORegistry.getCustomerDAO().saveOrUpdate(customer);
						jsonObject.put("topLevelInfoMsg", "Customer Info Updated Successfully.");
						
						//Tracking User Action Msg
						userActionMsg = "Customer Top-Level Info Updated Successfully.";
					}else{
						jsonObject.put("topLevelInfoMsg", msg);
					}
				}else if(action.equalsIgnoreCase("shippingInfo")){
					String shippingAddressId = request.getParameter("shippingAddrId");
					CustomerAddress shippingAddress = null;
					String shMsg = "";
					jsonObject.put("shippingInfoStatus", 0);
					
					String shFirstName = request.getParameter("shFirstName");
					String shLastName = request.getParameter("shLastName");
					String shAddressLine1 = request.getParameter("shAddressLine1");
					String shEmail = request.getParameter("shEmail");
					String shPhone = request.getParameter("shPhone");
					String shZipCode = request.getParameter("shZipCode");
					String shCity = request.getParameter("shCity");					
					String country = request.getParameter("shCountryName");
					String state = request.getParameter("shStateName");
					
					if(shFirstName == null || shFirstName.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - First Name.";
					}else if(shLastName == null || shLastName.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - Last Name.";
					}else if(shAddressLine1 == null || shAddressLine1.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - Street1.";
					}else if(shEmail == null || shEmail.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - Email.";
					}else if(shPhone == null || shPhone.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - Phone.";
					}else if(shZipCode == null || shZipCode.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - ZipCode.";
					}else if(shCity == null || shCity.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - City.";
					}else if(country == null || country.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - Country.";
					}else if(state == null || state.isEmpty()){
						shMsg = "Not able to identify, Shipping Address - State.";
					}
					
					if(shMsg.isEmpty()){
						Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
						State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
						
						if(shippingAddressId!=null && !shippingAddressId.isEmpty()){
							shippingAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
						}else{
							shippingAddress = new CustomerAddress();
						}
						shippingAddress.setFirstName(shFirstName);
						shippingAddress.setLastName(shLastName);
						shippingAddress.setAddressLine1(shAddressLine1);
						shippingAddress.setAddressLine2(request.getParameter("shAddressLine2"));
						shippingAddress.setCountry(countryObj);
						shippingAddress.setState(stateObj);
						shippingAddress.setEmail(shEmail);
						shippingAddress.setPhone1(shPhone);
						shippingAddress.setCity(shCity);
						shippingAddress.setZipCode(shZipCode);
						shippingAddress.setCustomerId(customerId);
						shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
						shippingAddress.setLastUpdated(new Date());
						shippingAddress.setStatus(EventStatus.ACTIVE);
						
						DAORegistry.getCustomerAddressDAO().saveOrUpdate(shippingAddress);
						jsonObject.put("shippingInfoMsg", "Shipping Info Updated Successfully.");
						jsonObject.put("shippingInfoStatus", 1);
						
						//Tracking User Action Msg
						if(shippingAddressId!=null && !shippingAddressId.isEmpty()){
							userActionMsg = "Customer Shipping Info Updated Successfully.";
						}else{
							userActionMsg = "Customer Shipping Info Added Successfully.";
						}
					}else{
						jsonObject.put("shippingInfoMsg", shMsg);
					}
					
					List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAndOtherAddresInfo(customerId);
					JSONArray shippingArray = new JSONArray();
					int count=0;
					if(shippingAddressList!=null && !shippingAddressList.isEmpty()){
						count = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
						for(CustomerAddress address : shippingAddressList){
							JSONObject object = new JSONObject();
							object.put("id",address.getId());
							object.put("firstName",address.getFirstName());
							object.put("lastName",address.getLastName());
							object.put("email",address.getEmail());
							object.put("phone",address.getPhone1());
							object.put("street1",address.getAddressLine1());
							object.put("street2",address.getAddressLine2());
							object.put("state",address.getState().getName());
							object.put("stateId",address.getState().getId());
							object.put("country",address.getCountry().getName());
							object.put("countryId",address.getCountry().getId());
							object.put("city",address.getCity());
							object.put("zip",address.getZipCode());
							object.put("addressType",address.getAddressType().toString());
							shippingArray.put(object);
						}
					}
					jsonObject.put("shippingInfo", shippingArray);
					jsonObject.put("shippingCount", count);
					
				}else if(action.equalsIgnoreCase("billingInfo")){
					String blMsg = "";
					jsonObject.put("billingInfoStatus", 0);
					CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
					if(billingAddress==null){
						billingAddress = new CustomerAddress();
					}
					
					String blFirstName = request.getParameter("blFirstName");
					String blLastName = request.getParameter("blLastName");
					String blAddressLine1 = request.getParameter("addressLine1");
					String blEmail = request.getParameter("blEmail");
					String blPhone = request.getParameter("blPhone");
					String blZipCode = request.getParameter("zipCode");
					String blCity = request.getParameter("city");					
					String country = request.getParameter("countryName");
					String state = request.getParameter("stateName");
					
					if(blFirstName == null || blFirstName.isEmpty()){
						blMsg = "Not able to identify, Billing Address - First Name.";
					}else if(blLastName == null || blLastName.isEmpty()){
						blMsg = "Not able to identify, Billing Address - Last Name.";
					}else if(blAddressLine1 == null || blAddressLine1.isEmpty()){
						blMsg = "Not able to identify, Billing Address - Street1.";
					}else if(blEmail == null || blEmail.isEmpty()){
						blMsg = "Not able to identify, Billing Address - Email.";
					}else if(blPhone == null || blPhone.isEmpty()){
						blMsg = "Not able to identify, Billing Address - Phone.";
					}else if(blZipCode == null || blZipCode.isEmpty()){
						blMsg = "Not able to identify, Billing Address - ZipCode.";
					}else if(blCity == null || blCity.isEmpty()){
						blMsg = "Not able to identify, Billing Address - City.";
					}else if(country == null || country.isEmpty()){
						blMsg = "Not able to identify, Billing Address - Country.";
					}else if(state == null || state.isEmpty()){
						blMsg = "Not able to identify, Billing Address - State.";
					}
					
					if(blMsg.isEmpty()){
						Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
						State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
						billingAddress.setFirstName(blFirstName);
						billingAddress.setLastName(blLastName);
						billingAddress.setEmail(blEmail);
						billingAddress.setPhone1(blPhone);
						billingAddress.setAddressLine1(blAddressLine1);
						billingAddress.setAddressLine2(request.getParameter("addressLine2"));
						billingAddress.setCountry(countryObj);
						billingAddress.setState(stateObj);
						billingAddress.setCity(blCity);
						billingAddress.setZipCode(blZipCode);
						billingAddress.setAddressType(AddressType.BILLING_ADDRESS);
						billingAddress.setLastUpdated(new Date());
						billingAddress.setCustomerId(customerId);
						billingAddress.setStatus(EventStatus.ACTIVE);
						
						DAORegistry.getCustomerAddressDAO().saveOrUpdate(billingAddress);
						jsonObject.put("billingInfoMsg", "Billing Info Updated Successfully.");
						jsonObject.put("billingInfoStatus", 1);
						
						//Tracking User Action Msg
						userActionMsg = "Customer Billing Info Updated Successfully.";
					}else{
						jsonObject.put("billingInfoMsg", blMsg);
					}
					
					JSONObject object = new JSONObject();					
					object.put("id",billingAddress.getId());
					object.put("firstName",billingAddress.getFirstName());
					object.put("lastName",billingAddress.getLastName());
					object.put("email",billingAddress.getEmail());
					object.put("phone",billingAddress.getPhone1());
					object.put("street1",billingAddress.getAddressLine1());
					object.put("street2",billingAddress.getAddressLine2());
					object.put("state",billingAddress.getState().getName());
					object.put("stateId",billingAddress.getState().getId());
					object.put("country",billingAddress.getCountry().getName());
					object.put("countryId",billingAddress.getCountry().getId());
					object.put("city",billingAddress.getCity());
					object.put("zip",billingAddress.getZipCode());
					
					jsonObject.put("billingInfo", object);
					
				}else if(action.equalsIgnoreCase("notes")){
					
					String notes = request.getParameter("notes");
					if(notes == null || notes.isEmpty()){						
						jsonObject.put("notesMsg", "No data to save in Notes.");
					}
					else{
						DAORegistry.getCustomerDAO().updateNotes(notes, customerId);
						jsonObject.put("notesMsg", "Notes Updated Successfully.");
						
						//Tracking User Action Msg
						userActionMsg = "Customer Notes Updated Successfully.";
					}
				}else if(action.equalsIgnoreCase("loyalFan")){
					String artistStr = request.getParameter("artistSelection");
					String loyalFanSelectionStr = request.getParameter("loyalFanSelection");
					String cityStr = request.getParameter("city");
					String stateStr = request.getParameter("state");
					String countryStr = request.getParameter("country");
					String zipCodeStr = request.getParameter("zipCode");
					String categoryName = request.getParameter("categoryName");
					
//				    Map<String, String> reqMap = Util.getParameterMap(request);
//				    if(categoryName != null && categoryName.equalsIgnoreCase("SPORTS")){
//				    	reqMap.put("loyalFanSelection", artistStr);
//				    }else{
//					    reqMap.put("loyalFanSelection", loyalFanSelectionStr);
//					    reqMap.put("state", stateStr);
//					    reqMap.put("country", countryStr);
//					    reqMap.put("zipCode", zipCodeStr);
//				    }
//				    reqMap.put("loyalFanType", categoryName);
//				    reqMap.put("customerId", customerID);
//				    String reqdata = Util.getObject(reqMap,sharedProperty.getApiUrl()+Constants.UPDATE_LOYALFAN);
//				    Gson reqgson = new Gson();  
//					JsonObject jsoObject = reqgson.fromJson(reqdata, JsonObject.class);
//					LoyalFanStatus loyalFanStatus = reqgson.fromJson(((JsonObject)jsoObject.get("loyalFanStatus")), LoyalFanStatus.class);
//					if(loyalFanStatus.getStatus() == 1){
//						jsonObject.put("Message", loyalFanStatus.getMessage());
//					}else{
//						jsonObject.put("Message", loyalFanStatus.getMessage());
//					}
					
//					Map<String, String> map = Util.getParameterMap(request);
//				    map.put("customerId", customerID);
//				    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.GET_LOYALFAN_STATUS);
//				    Gson gson = new Gson();  
//					JsonObject jsnObject = gson.fromJson(data, JsonObject.class);
//					LoyalFanStatus loyalFans = gson.fromJson(((JsonObject)jsnObject.get("loyalFanStatus")), LoyalFanStatus.class);
					
					Date today = new Date();
					Date afterYear = new Date();
					afterYear.setYear(today.getYear()+1);
					CustomerLoyalFan loyalFan = null;
					CustomerLoyalFanAudit loyalFanAudit = null;										
					Artist artist = null;
					String msg = "";
					
					if(artistStr != null && !artistStr.isEmpty()){
						artist = DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr));
					}
					if(artist != null || (loyalFanSelectionStr != null && !loyalFanSelectionStr.isEmpty())){							
						loyalFan = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
					}									
					if(loyalFan != null){
						loyalFan.setStatus("DELETED");
						loyalFan.setUpdatedDate(today);
						DAORegistry.getCustomerLoyalFanDAO().update(loyalFan);
						
						//LoyalFanAudit
						loyalFanAudit = new CustomerLoyalFanAudit();
						loyalFanAudit.setCustomerLoyalFanId(loyalFan.getId());
						loyalFanAudit.setCustomerId(customerId);
						loyalFanAudit.setStatus("DELETED");
						loyalFanAudit.setCreatedBy(userName);
						loyalFanAudit.setCreatedDate(today);
						DAORegistry.getCustomerLoyalFanAuditDAO().save(loyalFanAudit);
					}
					
					loyalFan = new CustomerLoyalFan();
					if(categoryName.equalsIgnoreCase("SPORTS")){
						loyalFan.setArtistId(artist.getId());
						loyalFan.setLoyalName(artist.getName());
						msg = artist.getName();
					}else{
						loyalFan.setArtistId(-1);
						loyalFan.setLoyalName(loyalFanSelectionStr);
						loyalFan.setCountry(countryStr);
						loyalFan.setState(stateStr);
						loyalFan.setZipCode(zipCodeStr);
						msg = loyalFanSelectionStr+", "+countryStr;
					}
					loyalFan.setLoyalFanType(categoryName);
					loyalFan.setTicketPurchased(false);
					loyalFan.setStartDate(today);
					loyalFan.setEndDate(afterYear);
					loyalFan.setCreatedDate(today);
					loyalFan.setUpdatedDate(today);
					loyalFan.setCustomerId(customerId);
					loyalFan.setStatus("ACTIVE");
					DAORegistry.getCustomerLoyalFanDAO().save(loyalFan);
					
					//LoyalFanAudit
					loyalFanAudit = new CustomerLoyalFanAudit();
					loyalFanAudit.setCustomerLoyalFanId(loyalFan.getId());
					loyalFanAudit.setCustomerId(customerId);
					loyalFanAudit.setStatus("ACTIVE");
					loyalFanAudit.setCreatedBy(userName);
					loyalFanAudit.setCreatedDate(today);
					DAORegistry.getCustomerLoyalFanAuditDAO().save(loyalFanAudit);
					jsonObject.put("Message", msg+" saved as Loyal Fan.");
					
					//Tracking User Action Msg
					userActionMsg = msg+" Saved as Loyal Fan.";
					
					CustomerLoyalFan loyalFans = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
					JSONObject loyalFanObj = new JSONObject();				
					if(loyalFans != null){
						if(loyalFans.getLoyalFanType() != null && loyalFans.getLoyalFanType().equalsIgnoreCase("SPORTS")){
							loyalFanObj.put("artistName", loyalFans.getLoyalName());
						}else{
							loyalFanObj.put("cityName", loyalFans.getLoyalName()!=null?loyalFans.getLoyalName()+", "+loyalFans.getCountry():"");
						}
						loyalFanObj.put("categoryName", loyalFans.getLoyalFanType()!=null?loyalFans.getLoyalFanType():"");
						if(loyalFans.getTicketPurchased() != null && loyalFans.getTicketPurchased()){
							loyalFanObj.put("ticketsPurchased", "Yes");
						}else{
							loyalFanObj.put("ticketsPurchased", "No");
						}
						loyalFanObj.put("startDate", loyalFans.getStartDateStr()!=null?loyalFans.getStartDateStr():"");
						loyalFanObj.put("endDate", loyalFans.getEndDateStr()!=null?loyalFans.getEndDateStr():"");
					}
										
					jsonObject.put("loyalFanInfo", loyalFanObj);
					
				}else if(action.equalsIgnoreCase("favouriteEvent")){
					String eventIdStr = request.getParameter("eventIds");
					FavouriteEvents favEvents = new FavouriteEvents();
					Date today = new Date();
//					String[] eventIdsArr = null;
//					if(eventIdStr != null) {
//						eventIdsArr = eventIdStr.split(",");
//					}
//					Set<Integer> eventIdSet = new HashSet<Integer>();
//					
//					for (String eventIds : eventIdsArr) {
//						if(eventIds != null && !eventIds.isEmpty()){
//							Integer eventId = Integer.parseInt(eventIds);
//							if(eventIdSet.add(eventId)){
//								favEvents.setEventId(eventId);
//								favEvents.setCustomerId(customerId);
//								favEvents.setInsertDate(today);
//								favEvents.setUpdateDate(today);
//								favEvents.setStatus("ACTIVE");
//								DAORegistry.getFavouriteEventsDAO().save(favEvents);
//							}
//						}
//					}
					if(eventIdStr != null && !eventIdStr.isEmpty()){
						Integer eventId = Integer.parseInt(eventIdStr);
						favEvents.setEventId(eventId);
						favEvents.setCustomerId(customerId);
						favEvents.setInsertDate(today);
						favEvents.setUpdateDate(today);
						favEvents.setStatus("ACTIVE");
						DAORegistry.getFavouriteEventsDAO().save(favEvents);
						jsonObject.put("FavEventMessage", "Favouite Events Added.");
						
						//Tracking User Action Msg
						userActionMsg = "Favouite Event(s) Added. Event Id - "+eventIdStr;
					}else{
						jsonObject.put("FavEventMessage", "Event Info Not Found.");
					}					
				}
				
				GridHeaderFilters filter = new GridHeaderFilters();				
				Collection<Customers> customerList = DAORegistry.getQueryManagerDAO().getCustomers(customerId,null,null,brokerId,filter,null);
				if(customerList!=null && !customerList.isEmpty()){
					for(Customers cust :customerList){
						jsonObject.put("id", cust.getCustomerId());
						jsonObject.put("customerType", cust.getCustomerType());
						jsonObject.put("productType", cust.getProductType());
						jsonObject.put("signupType", cust.getSignupType());
						jsonObject.put("customerStatus", cust.getCustomerStatus());
						jsonObject.put("companyName", cust.getCompanyName());
						jsonObject.put("customerName", cust.getFirstName());
						jsonObject.put("lastName", cust.getLastName());
						jsonObject.put("email", cust.getCustomerEmail());
						jsonObject.put("street1", cust.getAddressLine1());
						jsonObject.put("street2", cust.getAddressLine2());
						jsonObject.put("city", cust.getCity());
						jsonObject.put("state", cust.getState());
						jsonObject.put("country", cust.getCountry());
						jsonObject.put("zip", cust.getZipCode());
						jsonObject.put("client", cust.getIsClient()==true?"Yes":"No");
						jsonObject.put("broker", cust.getIsBroker()==true?"Yes":"No");
						jsonObject.put("phone", cust.getPhone());
					}
				}
				
				//Tracking User Action
				Util.userActionAudit(request, customerId, userActionMsg);
			}
			response.setHeader("Content-type","application/json");
			IOUtils.write(jsonObject.toString().getBytes(),response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-client-manage-details";
	} 
	
	/*@RequestMapping(value = "/ViewCustomer")
	public String viewInvoiceCustomer(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String customerID= request.getParameter("custId");
		//List<State> stateList = null;
		List<Country> countryList = null;
		try {
			Integer countryId = 1;
			//stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//map.put("states", stateList);
		map.put("countries", countryList);
		map.put("customerId", customerID);
		return "page-view-customer";
	}*/
	
	/**
	 * Update customer and billing address info
	 * @param customer
	 * @param request
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/UpdateCustomer")
	public String updateCustomer(@ModelAttribute Customer customer, HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String customerID= request.getParameter("custId");
		String billingAddressId = request.getParameter("billingAddressId");
		String action = request.getParameter("action");
		List<State> stateList = null;
		List<Country> countryList = null;
		int shippingAddressCount = 0;
		try{
			Integer countryId = 1;
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			Integer customerId = Integer.parseInt(customerID);
			CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
			if(action != null && action.equals("saveCustomerInfo")){
				if(customer.getCustomerName() == null || customer.getCustomerName().isEmpty()){
					returnMessage = "Customer name can't be blank";
				}else if(customer.getEmail() == null || customer.getEmail().isEmpty()){
					returnMessage = "Email can't be blank";
				}
				else if(customer.getPhone() == null || customer.getPhone().isEmpty()){
					returnMessage = "Phone no can't be blank";
				}
				if(returnMessage.isEmpty()){
				
						Customer customers = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerID));
						customer.setId(customers.getId());
						customer.setUserName(customers.getEmail());
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setApplicationPlatForm(ApplicationPlatform.TICK_TRACKER);
						customer.setSignupDate(new Date());
						DAORegistry.getCustomerDAO().update(customer);
				}
			}else if(action != null && action.equals("billingAddressInfo")){
				Integer billingId = Integer.parseInt(billingAddressId.trim());
				CustomerAddress billingAddresss = DAORegistry.getCustomerAddressDAO().get(billingId);
			
				String addressLine1 = request.getParameter("addressLine1");
				//CustomerAddress billingAddressInfo = null;
				if(addressLine1 != null && !addressLine1.isEmpty()){
					LOGGER.info("Saving the billing address ..");
					
					//billingAddressInfo = new CustomerAddress();
					String firstName = request.getParameter("firstName");
					System.out.println("firstname ::"+firstName);
					String lastName = request.getParameter("lastName");
					String addressLine2 = request.getParameter("addressLine2");
					String country = request.getParameter("countryName");
					String state = request.getParameter("stateName");
					String city = request.getParameter("city");
					String zipCode = request.getParameter("zipCode");
					
					Country countryObj = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
					State stateObj =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));

					billingAddress.setFirstName(firstName);
					billingAddress.setId(billingAddresss.getId());
					billingAddress.setLastName(lastName);
					billingAddress.setAddressLine1(addressLine1);
					billingAddress.setAddressLine2(addressLine2);
					billingAddress.setCountry(countryObj);
					billingAddress.setState(stateObj);
					billingAddress.setCity(city);
					billingAddress.setZipCode(zipCode);
					billingAddress.setAddressType(AddressType.valueOf("BILLING_ADDRESS"));
				}
				if(billingAddress != null){
					billingAddress.setCustomerId(customerId);
					DAORegistry.getCustomerAddressDAO().update(billingAddress);
					LOGGER.info("Saved the billing address for " +customerId);
				}
			}else if(action != null && action.equals("saveNotes")){
				String notes=request.getParameter("notes");
				DAORegistry.getCustomerDAO().updateNotes(notes,customerId);
				
			}
			
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
			Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getInvoicesForCustomer(customerId);
			Collection<PurchaseOrders> custPOList = DAORegistry.getQueryManagerDAO().getPOForCustomer(customerId);
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			map.put("customer", customerInfo);
			map.put("billingAddress", billingAddress);
			map.put("shippingAddressList", shippingAddressList);
			map.put("customerInvoiceList", custInvoiceList);
			map.put("custPOList", custPOList);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("customerId", customerID);
		map.put("count", shippingAddressCount);
		map.put("states", stateList);
		map.put("countries", countryList);
		return "page-view-customer";
	}*/
	
	/**
	 * Update customer shipping address info
	 * @param customer
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/EditCustomer")
	public String editCustomer(@ModelAttribute Customer customer, HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String customerID= request.getParameter("custId");
		String action = request.getParameter("action");
		String returnMessage = "";
		List<State> stateList = null;
		List<Country> countryList = null;
		try{
			Integer countryId = 1;
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			if(customerID == null || customerID.isEmpty()){
				return "redirect:ManageDetails";
			}
			
			Integer customerId = Integer.parseInt(customerID);
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			
			if(action != null && action.equals("action")){
				if(customer.getCustomerName() == null || customer.getCustomerName().isEmpty()){
					returnMessage = "Customer name can't be blank";
				}else if(customer.getEmail() == null || customer.getEmail().isEmpty()){
					returnMessage = "Email can't be blank";
				}
				else if(customer.getPhone() == null || customer.getPhone().isEmpty()){
					returnMessage = "Phone no can't be blank";
				}
				if(returnMessage.isEmpty()){
					LOGGER.info("Edit customer info "+ customerID);
					customer.setCustomerType(customerInfo.getCustomerType());
					customer.setCustomerLevel(customerInfo.getCustomerLevel());
					customer.setSignupType(customerInfo.getSignupType());
					customer.setCustomerName(customerInfo.getCustomerName());
					customer.setEmail(customerInfo.getEmail());
					customer.setPhone(customerInfo.getPhone());
					customer.setExtension(customerInfo.getExtension());
					customer.setOtherPhone(customerInfo.getOtherPhone());
					customer.setRepresentativeName(customerInfo.getRepresentativeName());
					
					customer.setProductType(ProductType.REWARDTHEFAN);
					customer.setApplicationPlatForm(ApplicationPlatform.TICK_TRACKER);
					customer.setSignupDate(new Date());
					DAORegistry.getCustomerDAO().update(customer);
					
					map.put("successMessage", "Customer details updated successfully.");
				}else{
					map.put("errorMessage", returnMessage);
				}
				
			}
			map.put("customer", customerInfo);
			map.put("billingAddress", billingAddress);
			map.put("shippingAddressList", shippingAddressList);
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
		
		map.put("customerId", customerID);
		map.put("states", stateList);
		map.put("countries", countryList);
		return "page-edit-customer";
	}*/
	
	/**
	 * Save shipping address
	 * @param addShippingAddress
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/AddShippingAddress", method=RequestMethod.POST)
	public String saveShippingAddress(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		LOGGER.info("Save shipping address..");
		String returnMessage = "";
		String customerId = "";
		String shippingAddressId = "";
		try{
			customerId = request.getParameter("custId");
			shippingAddressId = request.getParameter("shippingAddrId");
			String firstName = request.getParameter("shFirstName");
			String lastName = request.getParameter("shLastName");
			String street1 = request.getParameter("shAddressLine1");
			String street2 = request.getParameter("shAddressLine2");
			String country = request.getParameter("shCountryName");
			String state = request.getParameter("shStateName");
			String city = request.getParameter("shCity");
			String zipCode = request.getParameter("shZipCode");
			String action = request.getParameter("action");
			if(action != null && action.equals("addShippingAddress")){
				if(addShippingAddress.getFirstName() == null || addShippingAddress.getFirstName().isEmpty()){
					returnMessage = "Firstname can't be blank";
				}
				CustomerAddress shippingAddress = new CustomerAddress();
				if(returnMessage.isEmpty()){

					Country countryObj1 = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
					State stateObj1 =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
					
					shippingAddress.setFirstName(firstName);
					shippingAddress.setLastName(lastName);
					shippingAddress.setAddressLine1(street1);
					shippingAddress.setAddressLine2(street2);
					shippingAddress.setCountry(countryObj1);
					shippingAddress.setState(stateObj1);
					shippingAddress.setCity(city);
					shippingAddress.setZipCode(zipCode);
					shippingAddress.setCustomerId(Integer.parseInt(customerId));
					shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
					
					CustomerAddress updateAddress = null;
					if(shippingAddressId != null && !shippingAddressId.isEmpty()){
						updateAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
					}
					
					if(updateAddress != null){
						shippingAddress.setId(updateAddress.getId());
						shippingAddress.setFirstName(firstName);
						shippingAddress.setLastName(lastName);
						shippingAddress.setAddressLine1(street1);
						shippingAddress.setAddressLine2(street2);
						shippingAddress.setCountry(countryObj1);
						shippingAddress.setState(stateObj1);
						shippingAddress.setCity(city);
						shippingAddress.setZipCode(zipCode);
						shippingAddress.setCustomerId(Integer.parseInt(customerId));
						shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
						DAORegistry.getCustomerAddressDAO().update(shippingAddress);
					}else{						
						DAORegistry.getCustomerAddressDAO().save(shippingAddress);
					}
					returnMessage = "Shipping address added successfully";
					map.put("successMessage", returnMessage);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
		return "redirect:EditCustomer?custId="+customerId;
	}*/
	
	
	@RequestMapping({"/GetCustomerInfo"})
	public String getCustomerInfo(HttpServletRequest request, HttpServletResponse response, Model model){
		//JSONObject returnObject = new JSONObject();
		try{
			String customerIdStr = request.getParameter("custId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoDTO customerInfoDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoDTO")), CustomerInfoDTO.class);
			
			if(customerInfoDTO.getStatus() == 1){				
				model.addAttribute("customerInfo", customerInfoDTO.getCustomerTopLevelInfoDTO());
				model.addAttribute("countries", customerInfoDTO.getCountryDTO());
				model.addAttribute("states", customerInfoDTO.getStateDTO());
				model.addAttribute("billingInfo", customerInfoDTO.getBillingAddressDTO());
				model.addAttribute("shippingInfo", customerInfoDTO.getShippingAddressDTO());
				model.addAttribute("shippingPagingInfo", customerInfoDTO.getShippingPaginationDTO());
				model.addAttribute("shippingCount", customerInfoDTO.getShippingCount());
				model.addAttribute("cardInfo", customerInfoDTO.getCreditCardDTO());
				model.addAttribute("cardPagingInfo", customerInfoDTO.getCreditCardPaginationDTO());
				model.addAttribute("creditInfo", customerInfoDTO.getCustomerWalletDTO());
				model.addAttribute("rewardPointsInfo", customerInfoDTO.getRewardPointsDTO());
			}else{
				model.addAttribute("msg", customerInfoDTO.getError().getDescription());
				model.addAttribute("status", customerInfoDTO.getStatus());
			}
			
				/*Integer customerId = Integer.parseInt(customerIdStr);
				Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
				CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
				List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAndOtherAddresInfo(customerId);
				String phoneStr = "";
				String email = "";
				
				phoneStr = customerInfo.getPhone();
				if(phoneStr == null || phoneStr.isEmpty()){
					if(billingAddress != null && billingAddress.getPhone1() != null && !billingAddress.getPhone1().isEmpty()){
						phoneStr = billingAddress.getPhone1();
					}else{
						if(shippingAddressList != null && shippingAddressList.size() > 0){
							for(CustomerAddress shipAddress : shippingAddressList){
								if(shipAddress.getPhone1() != null && !shipAddress.getPhone1().isEmpty()){
									phoneStr = shipAddress.getPhone1();
									break;
								}
							}
						}
					}
				}
				email = customerInfo.getEmail();
				if(email == null || email.isEmpty()){
					if(billingAddress.getEmail() != null && !billingAddress.getEmail().isEmpty()){
						email = billingAddress.getEmail();
					}else{
						if(shippingAddressList != null && shippingAddressList.size() > 0){
							for(CustomerAddress shipAddress : shippingAddressList){
								if(shipAddress.getEmail() != null && !shipAddress.getEmail().isEmpty()){
									email = shipAddress.getEmail();
									break;
								}
							}
						}
					}
				}
				JSONObject custInfoObj = new JSONObject();
				custInfoObj.put("id",customerInfo.getId());
				custInfoObj.put("type",customerInfo.getCustomerType());
				custInfoObj.put("level",customerInfo.getCustomerLevel());
				custInfoObj.put("signupType",customerInfo.getSignupType());
				custInfoObj.put("name",customerInfo.getCustomerName());
				custInfoObj.put("lastName",customerInfo.getLastName());
				custInfoObj.put("email",email);
				custInfoObj.put("phone",phoneStr);
				custInfoObj.put("ext",customerInfo.getExtension());
				custInfoObj.put("otherPhone",customerInfo.getOtherPhone());
				custInfoObj.put("productType",customerInfo.getProductType().toString());
				custInfoObj.put("representative",customerInfo.getRepresentativeName());
				custInfoObj.put("notes",customerInfo.getNotes());
				custInfoObj.put("companyName",customerInfo.getCompanyName());
				if(customerInfo.getIsClient() && customerInfo.getIsBroker()){
					custInfoObj.put("clientBrokerType","both");
				}else if(customerInfo.getIsClient()){
					custInfoObj.put("clientBrokerType","client");
				}else if(customerInfo.getIsBroker()){
					custInfoObj.put("clientBrokerType","broker");
				}
				returnObject.put("customerInfo", custInfoObj);
				
				JSONObject countryObj = null;
				JSONArray countryArr = new JSONArray();
				List<Country> countryList = DAORegistry.getCountryDAO().getAllCountry();
				for(Country country : countryList){
					countryObj = new JSONObject();
					countryObj.put("id", country.getId());
					countryObj.put("name", country.getName());
					countryArr.put(countryObj);
				}
				returnObject.put("countries", countryArr);
				
				//CustomerAddress billingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customerId);
				JSONObject billingInfoObj = new JSONObject();
				if(billingAddress!=null){
					billingInfoObj.put("id",billingAddress.getId());
					billingInfoObj.put("firstName",billingAddress.getFirstName());
					billingInfoObj.put("lastName",billingAddress.getLastName());
					billingInfoObj.put("email",billingAddress.getEmail());
					billingInfoObj.put("phone",billingAddress.getPhone1());
					billingInfoObj.put("street1",billingAddress.getAddressLine1());
					billingInfoObj.put("street2",billingAddress.getAddressLine2());
					billingInfoObj.put("state", billingAddress.getState().getName());
					billingInfoObj.put("stateId", billingAddress.getState().getId());
					billingInfoObj.put("country",billingAddress.getCountry().getName());
					billingInfoObj.put("countryId", billingAddress.getCountry().getId());
					billingInfoObj.put("city",billingAddress.getCity());
					billingInfoObj.put("zip",billingAddress.getZipCode());
					if(billingAddress.getCountry()!=null){
						JSONArray stateArr = new JSONArray();
						JSONObject stateObj = null;
						List<State> stateList = DAORegistry.getStateDAO().getAllStateByCountryId(billingAddress.getCountry().getId());
						for(State state : stateList){
							stateObj = new JSONObject();
							stateObj.put("id", state.getId());
							stateObj.put("name", state.getName());
							stateArr.put(stateObj);
						}
						returnObject.put("states", stateArr);
					}
				}
				returnObject.put("billingInfo", billingInfoObj);
					
				//List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAndOtherAddresInfo(customerId);
				JSONArray shippingArray = new JSONArray();
				int count=0;
				if(shippingAddressList!=null && !shippingAddressList.isEmpty()){
					count = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
					for(CustomerAddress address : shippingAddressList){
						JSONObject object = new JSONObject();
						object.put("id",address.getId());
						object.put("firstName",address.getFirstName());
						object.put("lastName",address.getLastName());
						object.put("email",address.getEmail());
						object.put("phone",address.getPhone1());
						object.put("street1",address.getAddressLine1());
						object.put("street2",address.getAddressLine2());
						object.put("state",address.getState().getName());
						object.put("stateId",address.getState().getId());
						object.put("country",address.getCountry().getName());
						object.put("countryId",address.getCountry().getId());
						object.put("city",address.getCity());
						object.put("zip",address.getZipCode());
						object.put("addressType",address.getAddressType().toString());
						shippingArray.put(object);
					}
				}
				returnObject.put("shippingInfo", shippingArray);
				returnObject.put("shippingPagingInfo", PaginationUtil.getDummyPaginationParameter(count));
				returnObject.put("shippingCount", count);*/
				
				/*
				Collection<PurchaseOrders> custPOList=null;
				if(customerInfo.getProductType().equals(ProductType.REWARDTHEFAN)){
					Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getInvoicesForCustomer(customerId);
					custPOList = DAORegistry.getQueryManagerDAO().getPOForCustomer(customerId);
					JSONArray invoiceArray = new JSONArray();
					if(custInvoiceList!=null && !custInvoiceList.isEmpty()){
						for(CustomerInvoices invoice : custInvoiceList){
							JSONObject object = new JSONObject();
							object.put("invoiceId",invoice.getInvoiceId());
							object.put("invoiceTotal",invoice.getInvoiceTotal());
							object.put("createdDate",invoice.getCreatedDate());
							object.put("createdBy",invoice.getCreatedBy());
							object.put("ticketCount",invoice.getTicketCount());
							object.put("ext",invoice.getExtNo());
							invoiceArray.put(object);
						}
					}
					returnObject.put("invoiceInfo", invoiceArray);
				}else if(customerInfo.getProductType().equals(ProductType.RTW) || customerInfo.getProductType().equals(ProductType.RTW2)){
					custPOList = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(null,null,null,null,null,null,customerId);
					Collection<OpenOrders> openOrders = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(null,null,null,null,null,null,null,null,null,customerId);
					JSONArray invoiceArray = new JSONArray();
					if(openOrders!=null && !openOrders.isEmpty()){
						for(OpenOrders invoice : openOrders){
							JSONObject object = new JSONObject();
							object.put("invoiceId",invoice.getInvoiceId());
							object.put("invoiceTotal",invoice.getInvoiceTotal());
							object.put("createdDate",invoice.getCreatedDate());
							object.put("createdBy",invoice.getCreatedBy());
							object.put("ticketCount",invoice.getTicketCount());
							object.put("ext","");
							invoiceArray.put(object);
						}
					}
					returnObject.put("invoiceInfo", invoiceArray);
				}*/
				
				/*
				JSONArray poArray = new JSONArray();
				if(custPOList!=null && !custPOList.isEmpty()){
					for(PurchaseOrders po : custPOList){
						JSONObject object = new JSONObject();
						object.put("poId",po.getId());
						object.put("customerName",po.getCustomerName());
						object.put("customerType",po.getCustomerType());
						object.put("total",po.getPoTotal());
						object.put("ticketQty",po.getTicketQty());
						object.put("created",po.getCreated());
						object.put("shippingType",po.getShippingType());
						object.put("status",po.getStatus());
						poArray.put(object);
					}
				}	
				returnObject.put("poInfo", poArray)	;
				*/
				
				/*JSONArray cardArray = new JSONArray();
				List<CustomerStripeCreditCard> cards = DAORegistry.getCustomerStripeCreditCardDAO().getCustomerStripCardDetailsByCustomerId(customerId);
				if(cards!=null && !cards.isEmpty()){
					for(CustomerStripeCreditCard card : cards){
						JSONObject object = new JSONObject();
						object.put("id",card.getId());
						object.put("stripeCustomerId",card.getStripeCustomerId());
						object.put("stripeCardId",card.getStripeCardId());
						object.put("cardType",card.getCardType());
						object.put("cardLastFourDigit",card.getCardLastFourDigit());
						object.put("expiryMonth",card.getExpiryMonth());
						object.put("expiryYear",card.getExpiryYear());
						object.put("status",card.getStatus());
						cardArray.put(object);
					}
				}
				returnObject.put("cardInfo", cardArray);
				returnObject.put("cardPagingInfo", PaginationUtil.getDummyPaginationParameter(cardArray.length()));
				
				CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
				JSONObject creditObj = new JSONObject();
				if(wallet!=null){
					creditObj.put("activeCredit", String.format("%.2f", wallet.getActiveCredit()));
					creditObj.put("totalEarnedCredit", String.format("%.2f", wallet.getTotalCredit()));
					creditObj.put("totalUsedCredit", String.format("%.2f", wallet.getTotalUsedCredit()));
				}else{
					creditObj.put("activeCredit", String.format("%.2f", 0.00));
					creditObj.put("totalEarnedCredit", String.format("%.2f", 0.00));
					creditObj.put("totalUsedCredit", String.format("%.2f", 0.00));				
				}
				returnObject.put("creditInfo", creditObj);*/
				/*
				CustomerSuperFan loyalFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
				JSONObject loyalFanObj = new JSONObject();
				if(loyalFan != null){
					loyalFanObj.put("artistName", loyalFan.getArtistName());
					loyalFanObj.put("categoryName", loyalFan.getCategoryName());
					if(loyalFan.getTicketPurchased() != null && loyalFan.getTicketPurchased()){
						loyalFanObj.put("ticketsPurchased", "Yes");
					}else{
						loyalFanObj.put("ticketsPurchased", "No");
					}
					loyalFanObj.put("startDate", loyalFan.getStartDateStr());
					loyalFanObj.put("endDate", loyalFan.getEndDateStr());
				}
				returnObject.put("loyalFanInfo", loyalFanObj);
				*/
				/*JSONObject rewardPointsObj = new JSONObject();				
				CustomerLoyalty rewardPoints = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
				if(rewardPoints!=null){
					rewardPointsObj.put("id",rewardPoints.getId());
					rewardPointsObj.put("rewardCustomerId",rewardPoints.getCustomerId());
					rewardPointsObj.put("activePoints", String.format("%.2f", rewardPoints.getActivePoints()));
					rewardPointsObj.put("pendingPoints", String.format("%.2f", rewardPoints.getPendingPoints()));
					rewardPointsObj.put("totalEarnedPoints", String.format("%.2f", rewardPoints.getTotalEarnedPoints()));
					rewardPointsObj.put("totalSpentPoints", String.format("%.2f", rewardPoints.getTotalSpentPoints()));
					rewardPointsObj.put("latestEarnedPoints", String.format("%.2f", rewardPoints.getLatestEarnedPoints()));
					rewardPointsObj.put("latestSpentPoints", String.format("%.2f", rewardPoints.getLatestSpentPoints()));						
					rewardPointsObj.put("voidedPoints", String.format("%.2f", rewardPoints.getVoidedRewardPoints()));
				}else{
					rewardPointsObj.put("activePoints",0.00);
					rewardPointsObj.put("pendingPoints",0.00);
					rewardPointsObj.put("totalEarnedPoints",0.00);
					rewardPointsObj.put("totalSpentPoints",0.00);
					rewardPointsObj.put("latestEarnedPoints",0.00);
					rewardPointsObj.put("latestSpentPoints",0.00);						
					rewardPointsObj.put("voidedPoints",0.00);
				}
				returnObject.put("rewardPointsInfo", rewardPointsObj);
			
				response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	@RequestMapping({"/GetCustomerInfoForInvoice"})
	public String getCustomerInfoForInvoice(HttpServletRequest request, HttpServletResponse response, Model model){
		//JSONObject returnObject = new JSONObject();
		try{
			String customerIdStr = request.getParameter("custId");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO_FOR_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoInvoiceDTO customerInfoInvoiceDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoInvoiceDTO")), CustomerInfoInvoiceDTO.class);
			
			if(customerInfoInvoiceDTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoInvoiceDTO.getMessage());
				model.addAttribute("status", customerInfoInvoiceDTO.getStatus());
				model.addAttribute("invoiceInfo", customerInfoInvoiceDTO.getCustomerInvoiceDTO());
				model.addAttribute("invoicePagingInfo", customerInfoInvoiceDTO.getInvoicePaginationDTO());
			}else{
				model.addAttribute("msg", customerInfoInvoiceDTO.getError().getDescription());
				model.addAttribute("status", customerInfoInvoiceDTO.getStatus());
			}
			/*Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			JSONArray invoiceArray = new JSONArray();
			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			
			if(customerInfo.getProductType().equals(ProductType.REWARDTHEFAN)){
				String productType = "";
				Collection<CustomerInvoices> custRTFInvoiceList = null;
				if(filter.getOrderType()==null || filter.getOrderType().isEmpty()){
					if(customerInfo.getRtwCustomerId() != null && customerInfo.getRtwCustomerId() > 0){
						productType = "RTW"; 
						custRTFInvoiceList = DAORegistry.getQueryManagerDAO().getRTFInvoicesForCustomer(customerId, productType, filter);
					}
					if(customerInfo.getRtw2CustomerId() != null && customerInfo.getRtw2CustomerId() > 0){
						productType = "RTW2"; 
						custRTFInvoiceList = DAORegistry.getQueryManagerDAO().getRTFInvoicesForCustomer(customerId, productType, filter);
					}
				}
				Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getInvoicesForCustomer(customerId, filter);
				
				invoiceArray = new JSONArray();
				if(custInvoiceList!=null && !custInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custInvoiceList){
						JSONObject object = new JSONObject();
						object.put("invoiceId",invoice.getInvoiceId());
						object.put("invoiceTotal", String.format("%.2f", invoice.getInvoiceTotal()));
						object.put("createdDate",invoice.getCreatedDateStr());
						object.put("createdBy",invoice.getCreatedBy());
						object.put("ticketCount",invoice.getTicketCount());
						//object.put("ext",invoice.getExtNo());
						object.put("orderType",invoice.getOrderType());
						object.put("productType", invoice.getProductType());
						if(invoice.getStatus().equalsIgnoreCase("completed")){
							object.put("status", "Yes");
						}else{
							object.put("status", "No");
						}
						invoiceArray.put(object);
					}
				}
				if(custRTFInvoiceList!=null && !custRTFInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custRTFInvoiceList){
						JSONObject object = new JSONObject();
						object.put("invoiceId",invoice.getInvoiceId());
						object.put("invoiceTotal", String.format("%.2f", invoice.getInvoiceTotal()));
						object.put("createdDate",invoice.getCreatedDateStr());
						object.put("createdBy",invoice.getCreatedBy());
						object.put("ticketCount",invoice.getTicketCount());
						//object.put("ext",invoice.getExtNo());
						object.put("orderType",invoice.getOrderType());
						object.put("productType", invoice.getProductType());
						if(invoice.getStatus().equalsIgnoreCase("completed")){
							object.put("status", "Yes");
						}else{
							object.put("status", "No");
						}
						object.put("status", "No");
						invoiceArray.put(object);
					}
				}
				returnObject.put("invoiceInfo", invoiceArray);
				returnObject.put("invoicePagingInfo", PaginationUtil.getDummyPaginationParameter(invoiceArray.length()));
			}else if(customerInfo.getProductType().equals(ProductType.RTW) || customerInfo.getProductType().equals(ProductType.RTW2)){
				//custPOList = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(null,null,null,null,null,null,customerId);
				
				Collection<CustomerInvoices> custInvoiceList = DAORegistry.getQueryManagerDAO().getRTWInvoicesForCustomer(customerId,filter);
				invoiceArray = new JSONArray();
				if(custInvoiceList != null && !custInvoiceList.isEmpty()){
					for(CustomerInvoices invoice : custInvoiceList){
						JSONObject object = new JSONObject();
						object.put("invoiceId",invoice.getInvoiceId());
						object.put("invoiceTotal", String.format("%.2f", invoice.getInvoiceTotal()));
						object.put("createdDate",invoice.getCreatedDateStr());
						object.put("createdBy",invoice.getCreatedBy());
						object.put("ticketCount",invoice.getTicketCount());
						//object.put("ext",invoice.getExtNo());
						object.put("orderType",invoice.getOrderType());
						object.put("productType", invoice.getProductType());
						if(invoice.getStatus().equalsIgnoreCase("completed")){
							object.put("status", "Yes");
						}else{
							object.put("status", "No");
						}
						object.put("status", "No");
						invoiceArray.put(object);
					}
				}				
				returnObject.put("invoiceInfo", invoiceArray);
				returnObject.put("invoicePagingInfo", PaginationUtil.getDummyPaginationParameter(invoiceArray.length()));
			}
			if(invoiceArray.length() <= 0){
				returnObject.put("msg", "No Invoice data found for Selected Customer.");
			}
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	

	@RequestMapping({"/GetCustomerInfoForInvoiceTickets"})
	public String getCustomerInfoForInvoiceTickets(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			String invoiceIdStr = request.getParameter("invoiceId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceIdStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO_FOR_INVOICE_TICKETS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoInvoiceTicketDTO customerInfoInvoiceTicketDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoInvoiceTicketDTO")), CustomerInfoInvoiceTicketDTO.class);
			
			if(customerInfoInvoiceTicketDTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoInvoiceTicketDTO.getMessage());
				model.addAttribute("status", customerInfoInvoiceTicketDTO.getStatus());
				model.addAttribute("invoiceTicketList", customerInfoInvoiceTicketDTO.getCustomerInvoiceTicketAttachmentDTO());
				model.addAttribute("pagingInfo", customerInfoInvoiceTicketDTO.getInvoiceTicketPaginationDTO());
				model.addAttribute("invoiceId", invoiceIdStr);
			}else{
				model.addAttribute("msg", customerInfoInvoiceTicketDTO.getError().getDescription());
				model.addAttribute("status", customerInfoInvoiceTicketDTO.getStatus());
			}
			
			/*JSONObject jObj = new JSONObject();
			
			Integer invoiceId = Integer.parseInt(invoiceIdStr);
			Integer count = 0;
			List<InvoiceTicketAttachment> ticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoiceId);
			
			if(ticketAttachments != null && ticketAttachments.size() > 0){
				count = ticketAttachments.size();
				jObj.put("status", 1);
			}else{
				jObj.put("msg", "There is no ticket to download for Selected Customer.");
			}
			
			jObj.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			jObj.put("invoiceTicketList", JsonWrapperUtil.getInvoiceTicketAttachmentsArray(ticketAttachments));
			jObj.put("invoiceId", invoiceIdStr);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	@RequestMapping({"/GetCustomerInfoForPO"})
	public String getCustomerInfoForPO(HttpServletRequest request, HttpServletResponse response, Model model){
		//JSONObject returnObject = new JSONObject();
		try{
			String customerIdStr = request.getParameter("custId");			
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO_FOR_PO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoPODTO customerInfoPODTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoPODTO")), CustomerInfoPODTO.class);
			
			if(customerInfoPODTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoPODTO.getMessage());
				model.addAttribute("status", customerInfoPODTO.getStatus());
				model.addAttribute("poInfo", customerInfoPODTO.getCustomerPurchaseOrderDTO());
				model.addAttribute("poPagingInfo", customerInfoPODTO.getPoPaginationDTO());
			}else{
				model.addAttribute("msg", customerInfoPODTO.getError().getDescription());
				model.addAttribute("status", customerInfoPODTO.getStatus());
			}
			
			/*Integer customerId = Integer.parseInt(customerIdStr);
			Customer customerInfo = DAORegistry.getCustomerDAO().get(customerId);
			Collection<PurchaseOrders> custPOList=null;
			Collection<PurchaseOrders> custRTFPOList = null;
			JSONArray poArray = new JSONArray();
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
			
			if(customerInfo.getProductType().equals(ProductType.REWARDTHEFAN)){
				String productType = "";
				
				if(customerInfo.getRtwCustomerId() != null && customerInfo.getRtwCustomerId() > 0){
					productType = "RTW"; 
					custRTFPOList = DAORegistry.getQueryManagerDAO().getRTFPOForCustomer(customerId, productType, filter);
				}
				if(customerInfo.getRtw2CustomerId() != null && customerInfo.getRtw2CustomerId() > 0){
					productType = "RTW2"; 
					custRTFPOList = DAORegistry.getQueryManagerDAO().getRTFPOForCustomer(customerId, productType, filter);
				}
				custPOList = DAORegistry.getQueryManagerDAO().getPOForCustomer(customerId, filter);
				
			}else if(customerInfo.getProductType().equals(ProductType.RTW) || customerInfo.getProductType().equals(ProductType.RTW2)){
				custPOList = DAORegistry.getQueryManagerDAO().getRTWPOForCustomer(customerId,filter);
			}
			if(custPOList!=null && !custPOList.isEmpty()){
				for(PurchaseOrders po : custPOList){
					JSONObject object = new JSONObject();
					object.put("poId",po.getId());
					object.put("customerName",po.getCustomerName());
					object.put("customerType",po.getCustomerType());
					object.put("total", String.format("%.2f", po.getPoTotal()));
					object.put("ticketQty",po.getTicketQty());
					object.put("created",po.getCreateDateStr());
					object.put("shippingType",po.getShippingType());
					object.put("status",po.getStatus());
					object.put("productType",po.getProductType());
					poArray.put(object);
				}
			}
			if(custRTFPOList!=null && !custRTFPOList.isEmpty()){
				for(PurchaseOrders po : custRTFPOList){
					JSONObject object = new JSONObject();
					object.put("poId",po.getId());
					object.put("customerName",po.getCustomerName());
					object.put("customerType",po.getCustomerType());
					object.put("total", String.format("%.2f", po.getPoTotal()));
					object.put("ticketQty",po.getTicketQty());
					object.put("created",po.getCreateDateStr());
					object.put("shippingType",po.getShippingType());
					object.put("status",po.getStatus());
					object.put("productType",po.getProductType());
					poArray.put(object);
				}
			}
			if(poArray.length() <= 0){
				returnObject.put("msg", "No Purchase Order data found for Selected Customer.");
			}
			returnObject.put("poInfo", poArray)	;
			returnObject.put("poPagingInfo", PaginationUtil.getDummyPaginationParameter(poArray.length()));
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	/*@RequestMapping({"/AddClientBroker"})
	public String addClientBroker(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//map.put("url", "Client");
		return "page-add-client-broker";
	}*/
	
	
	@RequestMapping({"/AddRewardPointsForCustomer"})
	public String addRewardPointsForCustomer(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			String customerIdStr = request.getParameter("customerId");
			String rewardPoints = request.getParameter("points");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("rewardPoints", rewardPoints);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ADD_REWARD_POINTS_FOR_CUSTOMER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerAddRewardPointDTO customerAddRewardPointDTO = gson.fromJson(((JsonObject)jsonObject.get("customerAddRewardPointDTO")), CustomerAddRewardPointDTO.class);
			
			if(customerAddRewardPointDTO.getStatus() == 1){				
				model.addAttribute("msg", customerAddRewardPointDTO.getMessage());
				model.addAttribute("status", customerAddRewardPointDTO.getStatus());
				model.addAttribute("rewardPointsInfo", customerAddRewardPointDTO.getRewardPointsDTO());
			}else{
				model.addAttribute("msg", customerAddRewardPointDTO.getError().getDescription());
				model.addAttribute("status", customerAddRewardPointDTO.getStatus());
			}
			/*String msg = "";
			JSONObject returnObject = new JSONObject();
			returnObject.put("status", 0);
			
			if(customerId == null || customerId.isEmpty()){
				msg = "Not able to identify Customer Id.";
			}
			if(rewardPoints == null || rewardPoints.isEmpty()){
				msg = "Not able to identify Reward Points.";
			}else{			
				try{
					Double rewardPointDouble = Double.parseDouble(rewardPoints);
				}catch(Exception e){
					e.printStackTrace();
					msg = "Bad value found for Reward Points, It should be valid Double value.";
				}
			}
			
			Customer customer  = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
			if(customer == null){
				msg = "Customer information not found.";
			}
			
			CustomerLoyalty loyalty = null;
			CustomerLoyaltyTracking loyaltyTracking = null;
			if(customerId!=null && !customerId.isEmpty() && rewardPoints!=null && !rewardPoints.isEmpty() && msg.isEmpty()){
				Double points = Double.parseDouble(rewardPoints);
				loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
				if(loyalty == null){
					loyalty = new CustomerLoyalty();
					loyalty.setActivePoints(0.00);
					loyalty.setActiveRewardDollers(0.00);
					loyalty.setCustomerId(customer.getId());
					loyalty.setLastUpdate(new Date());
					loyalty.setLatestEarnedPoints(0.00);
					loyalty.setLatestSpentPoints(0.00);
					loyalty.setPendingPoints(0.00);
					loyalty.setRevertedSpentPoints(0.00);
					loyalty.setTotalEarnedPoints(0.00);
					loyalty.setTotalSpentPoints(0.00);
					loyalty.setVoidedRewardPoints(0.00);
					//msg = "Customer reward point information not found.";
				}
					loyalty.setActivePoints(loyalty.getActivePoints() + Util.getRoundedValue(points));
					loyalty.setTotalEarnedPoints(loyalty.getTotalEarnedPoints() + Util.getRoundedValue(points));
					loyalty.setLatestEarnedPoints(Util.getRoundedValue(points));
					loyalty.setLastUpdate(new Date());
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(loyalty);
					
					//Customer Loyalty Tracking
					loyaltyTracking = new CustomerLoyaltyTracking();
					loyaltyTracking.setCustomerId(customer.getId());
					loyaltyTracking.setRewardPoints(Util.getRoundedValue(points));
					loyaltyTracking.setCreatedBy(userName);
					loyaltyTracking.setCreatedDate(new Date());
					DAORegistry.getCustomerLoyaltyTrackingDAO().save(loyaltyTracking);
				
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("customer", customer); 
					mailMap.put("points", points);
					mailMap.put("rewardPointsLink","https://www.rewardthefan.com/CustomerRewardPoints");
					
					com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[1];
					mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo.png",Util.getFilePath(request, "logo.png", "/images/"));
					try {
						mailManager.sendMailNow("text/html","sales@rewardthefan.com",customer.getEmail(), 
								null, "msanghani@rightthisway.com,AODev@rightthisway.com", 
								"Reward The Fan:Referral Order Reward Points Information",
					    		"mail-reward-points-referral.html", mailMap, "text/html", null,mailAttachment,null);
						} catch (Exception e) {
							e.printStackTrace();
						}
					msg = "Customer reward points added.";
					returnObject.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Customer Reward Points Added.";
					Util.userActionAudit(request, customer.getId(), userActionMsg);				
			}
			
			JSONObject rewardPointsObj = new JSONObject();				
			//CustomerLoyalty rewardPointsLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
			if(loyalty!=null){
				rewardPointsObj.put("id",loyalty.getId());
				rewardPointsObj.put("rewardCustomerId",loyalty.getCustomerId());
				rewardPointsObj.put("activePoints", String.format("%.2f", loyalty.getActivePoints()));
				rewardPointsObj.put("pendingPoints", String.format("%.2f", loyalty.getPendingPoints()));
				rewardPointsObj.put("totalEarnedPoints", String.format("%.2f", loyalty.getTotalEarnedPoints()));
				rewardPointsObj.put("totalSpentPoints", String.format("%.2f", loyalty.getTotalSpentPoints()));
				rewardPointsObj.put("latestEarnedPoints", String.format("%.2f", loyalty.getLatestEarnedPoints()));
				rewardPointsObj.put("latestSpentPoints", String.format("%.2f", loyalty.getLatestSpentPoints()));						
				rewardPointsObj.put("voidedPoints", String.format("%.2f", loyalty.getVoidedRewardPoints()));
			}
			returnObject.put("rewardPointsInfo", rewardPointsObj);
			
			returnObject.put("msg", msg);
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	
	@RequestMapping({"/EditCustomerInfoForRewardPoints"})
	 public String editCustomerInfoForRewardPoints(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String rewardPointBreakUp = request.getParameter("Message");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("rewardPointBreakUp", rewardPointBreakUp);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_CUSTOMER_INFO_FOR_REWARD_POINTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoRewardPointsHistoryDTO customerInfoRewardPointsHistoryDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoRewardPointsHistoryDTO")), CustomerInfoRewardPointsHistoryDTO.class);
			
			if(customerInfoRewardPointsHistoryDTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoRewardPointsHistoryDTO.getMessage());
				model.addAttribute("status", customerInfoRewardPointsHistoryDTO.getStatus());
				model.addAttribute("rewardPointsList", customerInfoRewardPointsHistoryDTO.getCustomerRewardPointsDTO());
				model.addAttribute("pagingInfo", customerInfoRewardPointsHistoryDTO.getRewardPointsPaginationDTO());
				model.addAttribute("customerId", customerIdStr);
				model.addAttribute("rewardPointBreakUp", rewardPointBreakUp);
			}else{
				model.addAttribute("msg", customerInfoRewardPointsHistoryDTO.getError().getDescription());
				model.addAttribute("status", customerInfoRewardPointsHistoryDTO.getStatus());
			}
			
			/*List<RewardDetails> rewardList = null;
			Integer count = 0;
			JSONObject jObj = new JSONObject();
			if(customerIdStr!=null && !customerIdStr.isEmpty()){
				Integer customerId = Integer.parseInt(customerIdStr);
				if(rewardPointBreakUp != null && !rewardPointBreakUp.isEmpty()){
					GridHeaderFilters filter = new GridHeaderFilters(); 
					if(rewardPointBreakUp.equalsIgnoreCase("Earned Points")){
						rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.ACTIVE,customerId, filter);
						count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatusCount(RewardStatus.ACTIVE, customerId, filter);
					}
					if(rewardPointBreakUp.equalsIgnoreCase("Redeemed Points")){
						rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerId(customerId, filter);
						count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerIdCount(customerId, filter);
					}
					if(rewardList != null && rewardList.size() > 0){
						jObj.put("status", 1);
					}else{
						jObj.put("msg", "There is no Reward Points History.");
					}
				}
			}else{
				jObj.put("errorMessage", "Customer Id not found.");
			}
			jObj.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			jObj.put("rewardPointsList", JsonWrapperUtil.getRewardPointsArray(rewardList));
			jObj.put("customerId", customerIdStr);
			jObj.put("rewardPointBreakUp", rewardPointBreakUp);
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();	
		}
		return "page-client-manage-details";
	} 
		
	 
	@RequestMapping({"/GetCustomerInfoForRewardPoints"})
	public String getCustomerInfoForRewardPoints(HttpServletRequest request, HttpServletResponse response, Model model){
		//JSONObject returnObject = new JSONObject();		
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String customerIdStr = request.getParameter("customerId");
			String rewardPointBreakUp = request.getParameter("Message");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("rewardPointBreakUp", rewardPointBreakUp);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO_FOR_REWARD_POINTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoRewardPointsHistoryDTO customerInfoRewardPointsHistoryDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoRewardPointsHistoryDTO")), CustomerInfoRewardPointsHistoryDTO.class);
			
			if(customerInfoRewardPointsHistoryDTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoRewardPointsHistoryDTO.getMessage());
				model.addAttribute("status", customerInfoRewardPointsHistoryDTO.getStatus());
				model.addAttribute("rewardPointsList", customerInfoRewardPointsHistoryDTO.getCustomerRewardPointsDTO());
				model.addAttribute("pagingInfo", customerInfoRewardPointsHistoryDTO.getRewardPointsPaginationDTO());
			}else{
				model.addAttribute("msg", customerInfoRewardPointsHistoryDTO.getError().getDescription());
				model.addAttribute("status", customerInfoRewardPointsHistoryDTO.getStatus());
			}
			
			/*List<RewardDetails> rewardList = null;
			Integer count =0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRewardPointsSearchHeaderFilters(headerFilter);
			if(rewardPointBreakUp != null && !rewardPointBreakUp.isEmpty()){
				Integer customerId = Integer.parseInt(customerIdStr);
				if(rewardPointBreakUp.equalsIgnoreCase("Earned Points")){
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.ACTIVE, customerId, filter);
					count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatusCount(RewardStatus.ACTIVE, customerId, filter);
				}
				if(rewardPointBreakUp.equalsIgnoreCase("Redeemed Points")){
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerId(customerId, filter);
					count = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerIdCount(customerId, filter);
				}
				if(rewardList != null && rewardList.size() > 0){
					returnObject.put("status", 1);
				}else{
					returnObject.put("msg", "There is no Reward Points History.");
				}
			}
			
			returnObject.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			returnObject.put("rewardPointsList", JsonWrapperUtil.getRewardPointsArray(rewardList));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*@RequestMapping({"/AddClient"})
	public String addClient(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//map.put("url", "Client");
		return "page-add-client";
	}*/
	
	
	@RequestMapping({"/ManageBrokers"})
	public String loadManageBrokersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		try{
			String status = request.getParameter("status");			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("status", status);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_BROKERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ManageUsersDTO manageUsersDTO = gson.fromJson(((JsonObject)jsonObject.get("manageUsersDTO")), ManageUsersDTO.class);
			
			if(manageUsersDTO.getStatus() == 1){
				model.addAttribute("msg", manageUsersDTO.getMessage());
				model.addAttribute("status", status);
			}else{
				model.addAttribute("msg", manageUsersDTO.getError().getDescription());
				model.addAttribute("status", status);
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("trackerBrokerList", manageUsersDTO.getTrackerUserCustomDTO());
				model.addAttribute("pagingInfo", manageUsersDTO.getPaginationDTO());
			}else{
				model.addAttribute("trackerBrokerList", gson.toJson(manageUsersDTO.getTrackerUserCustomDTO()));
				model.addAttribute("pagingInfo", gson.toJson(manageUsersDTO.getPaginationDTO()));
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-admin-manage-brokers";
	}
	
	/*
	@RequestMapping({"/GetManageBrokers"})
	public void loadManageBrokers(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONObject returnObject = new JSONObject();
		try{
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			Integer count = 0;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getUserSearchHeaderFilters(headerFilter);
			Collection<TrackerUser> trackerBrokerList= DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_BROKER",status,filter,pageNo);
			if(trackerBrokerList != null && trackerBrokerList.size() > 0){
				count = trackerBrokerList.size();
			}else if(trackerBrokerList == null || trackerBrokerList.size() <= 0){
				returnObject.put("msg", "No "+status+" Brokers found.");
			}
			
			returnObject.put("trackerBrokerList", JsonWrapperUtil.getMangeUsersArray(trackerBrokerList));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}
	}
	*/

	@RequestMapping({"/AddUsers"})
	public String addUserPage(@ModelAttribute TrackerUser trackerUser, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			//String returnMessage = "";
			String action = request.getParameter("action");			
			String roleAffiliateBroker = request.getParameter("role");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Gson gson = GsonCustomConfig.getGsonBuilder();
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("action", action);
			paramMap.put("roleAffiliateBroker", roleAffiliateBroker);
			paramMap.put("sessionUser", userName);
			
			if(action != null && action.equals("action")){
				String[] roles = request.getParameterValues("role");
				String rePassword = request.getParameter("repassword");
				
				String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				
				String fromDateStr = request.getParameter("fromDate");
				String toDateStr = request.getParameter("toDate");
				String affiliateDisc = request.getParameter("affiliateDiscount");
				String customerOrderDisc = request.getParameter("customerOrderDiscount");
				String phoneAffiliateDisc = request.getParameter("phoneAffiliateDiscount");
				String phoneCustomerOrderDisc = request.getParameter("phoneCustomerOrderDiscount");
				String canEarnRewardPoints = request.getParameter("canEarnRewardPoints");
				String status = request.getParameter("affiliateStatus");
				String repeatBusiness = request.getParameter("affiliateBusiness");
				String promoCode = request.getParameter("promoCode");
				
				paramMap.put("userName", trackerUser.getUserName());
				paramMap.put("firstName", trackerUser.getFirstName());
				paramMap.put("lastName", trackerUser.getLastName());
				paramMap.put("email", trackerUser.getEmail());
				paramMap.put("password", trackerUser.getPassword());
				paramMap.put("rePassword", rePassword);
				paramMap.put("phone", trackerUser.getPhone());
				paramMap.put("role", gson.toJson(roles));
				
				//For Role Broker
				paramMap.put("companyName", companyName);
				paramMap.put("serviceFees", serviceFees);
				
				//For Role Affiliates
				paramMap.put("fromDateStr", fromDateStr);
				paramMap.put("toDateStr", toDateStr);
				paramMap.put("affiliateDisc", affiliateDisc);
				paramMap.put("customerOrderDisc", customerOrderDisc);
				paramMap.put("status", status);
				paramMap.put("repeatBusiness", repeatBusiness);				
				paramMap.put("phoneAffiliateDiscount", phoneAffiliateDisc);
				paramMap.put("phoneCustomerOrderDiscount", phoneCustomerOrderDisc);
				paramMap.put("canEarnRewardPoints", canEarnRewardPoints);
				paramMap.put("promoCode", promoCode);
				
				/*Date fromDate = null;
				Date toDate = null;
				
				if(trackerUser.getUserName() == null || trackerUser.getUserName().isEmpty()){
					returnMessage= "Username can't be blank";
				}else if(trackerUser.getFirstName() == null || trackerUser.getFirstName().isEmpty()){
					returnMessage= "Firstname can't be blank";
				}else if(trackerUser.getLastName() == null || trackerUser.getLastName().isEmpty()){
					returnMessage= "Lastname can't be blank";
				}else if(trackerUser.getEmail() == null || trackerUser.getEmail().isEmpty()){
					returnMessage= "Email can't be blank";
				}else if(trackerUser.getPassword() == null || trackerUser.getPassword().isEmpty()){
					returnMessage= "Password can't be blank";
				}else if(rePassword == null || rePassword.isEmpty()){
					returnMessage = "Re-Password can't be blank";
				}else if(!trackerUser.getPassword().equals(rePassword)){
					returnMessage = "Password and Re-Password must match.";
				}else if(!AdminController.validateEMail(trackerUser.getEmail())){
					returnMessage = "Invalid Email.";
				}else if(roles != null && roles.length == 0){
					returnMessage = "Please choose at least one Role.";
				}
				
				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(companyName == null || companyName.isEmpty()){
							returnMessage = "Company can't be blank.";
						}
						if(serviceFees == null || serviceFees.isEmpty()){
							returnMessage = "Service Fees can't be blank.";
						}
					}
					
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(customerOrderDisc == null || customerOrderDisc.isEmpty()){
							returnMessage = "Please add value for Order Discount.";
						}
						if(phoneCustomerOrderDisc == null || phoneCustomerOrderDisc.isEmpty()){
							returnMessage = "Please add value for Customer discount got phone orders.";
						}
						if(affiliateDisc == null || affiliateDisc.isEmpty()){
							returnMessage = "Please add valid value in Affiliates Discount for Phone Orders.";
						}
						if(affiliateDisc == null || affiliateDisc.isEmpty()){
							returnMessage = "Please add valid value in Affiliates Discount for RTF Orders.";
						}
						if(fromDateStr == null || fromDateStr.isEmpty()){
							returnMessage = "Please add valid value in Effective from date.";
						}
						if(toDateStr == null || toDateStr.isEmpty()){
							returnMessage = "Please add valid value in Effective to date.";
						}
						try {
							fromDateStr = fromDateStr + " 00:00:00";
							toDateStr = toDateStr + " 23:59:59";
							fromDate = Util.getDateWithTwentyFourHourFormat1(fromDateStr);
							toDate = Util.getDateWithTwentyFourHourFormat1(toDateStr);
						} catch (Exception e) {
							e.printStackTrace();
							returnMessage = "Invalid Effective from or to date format.";
						}
						
					}
				}
				if(returnMessage.isEmpty()){
					TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(trackerUser.getUserName(), trackerUser.getEmail());
					TrackerBrokers trackerBrokersDb = null;
					
					if(trackerUserDb != null){
						returnMessage = "There is already an user with this Username or Email.";
					}else{
						// adding user to database
						if(trackerUser.getPhone() == null || trackerUser.getPhone().isEmpty()){
							trackerUser.setPhone(null);
						}else{
							trackerUser.setPhone(trackerUser.getPhone());
						}
						Set<Role> roleList = new HashSet<Role>();
						AffiliatePromoCodeHistory promoHistory = null;
						AffiliateSetting setting = null;
						for(String role : roles){
							Role roleDb = DAORegistry.getRoleDAO().getRoleByName(role.toUpperCase());
							
							if(role.toUpperCase().equals("ROLE_BROKER")){
								trackerBrokersDb = new TrackerBrokers();
								trackerBrokersDb.setCompanyName(companyName);
								trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
								DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
								
								trackerUser.setBroker(trackerBrokersDb);
							}
							
							if(role.toUpperCase().equals("ROLE_AFFILIATES")){
								//Newly Added
								if(promoCode != null && !promoCode.isEmpty()){
									trackerUser.setPromotionalCode(promoCode);
								}else{
									trackerUser.setPromotionalCode(Util.generateCustomerReferalCode());
								}
								//
								promoHistory = new AffiliatePromoCodeHistory();
								promoHistory.setPromoCode(trackerUser.getPromotionalCode());
								promoHistory.setEffectiveFromDate(fromDate);
								promoHistory.setEffectiveToDate(toDate);
								promoHistory.setStatus("ACTIVE");
								promoHistory.setUpdateDate(new Date());
								promoHistory.setCreateDate(new Date());
								
								setting = new AffiliateSetting();
								setting.setCashDiscount((Double.parseDouble(affiliateDisc)/100));
								//setting.setPhoneCashDiscount((Double.parseDouble(phoneAffiliateDisc)/100));
								setting.setCustomerDiscount((Double.parseDouble(customerOrderDisc)/100));
								//setting.setPhoneCustomerDiscount((Double.parseDouble(phoneCustomerOrderDisc)/100));
								if(repeatBusiness==null || repeatBusiness.equalsIgnoreCase("NO")){
									setting.setIsRepeatBusiness(false);
								}else{
									setting.setIsRepeatBusiness(true);
								}
								//Newly Added
								setting.setIsEarnRewardPoints(true);
								if(canEarnRewardPoints==null || canEarnRewardPoints.equalsIgnoreCase("NO")){
									setting.setIsEarnRewardPoints(false);
								}
								//
								setting.setStatus(status);
								setting.setLastUpdated(new Date());
								setting.setUpdatedBy(username);
							}
							roleList.add(roleDb);
						}
						trackerUser.setPassword(AdminController.encryptPwd(trackerUser.getPassword()));
						trackerUser.setRoles(roleList);						
						trackerUser.setStatus(true);
						trackerUser.setCreateDate(new Date());
						trackerUser.setCreatedBy(username);
						DAORegistry.getTrackerUserDAO().save(trackerUser);
						if(promoHistory!=null){
							promoHistory.setUserId(trackerUser.getId());
							setting.setUserId(trackerUser.getId());
							DAORegistry.getAffiliatePromoCodeHistoryDAO().save(promoHistory);
							DAORegistry.getAffiliateSettingDAO().save(setting);
						}
						
						returnMessage = "User added successfully.";
						map.put("successMessage", returnMessage);
						
						//Display Purpose - Newly Added						
						map.put("affiliateInfo", JsonWrapperUtil.getAffiliateInfoObject(promoHistory, setting));
						map.put("brokerInfo", trackerBrokersDb);
												
						//Tracking User Action
						String userActionMsg = "User Created Successfully.";
						Util.userActionAudit(request, trackerUser.getId(), userActionMsg);
					}
				}else{
					map.put("errorMessage", returnMessage);
				}*/
				
			}
			//map.put("roleList", DAORegistry.getRoleDAO().getAll());
			
			String data = Util.getObject(paramMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ADD_USERS);
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AddUserDTO addUserDTO = gson.fromJson(((JsonObject)jsonObject.get("addUserDTO")), AddUserDTO.class);
			
			if(addUserDTO.getStatus() == 1){
				model.addAttribute("trackerUser", addUserDTO.getTrackerUser());
				model.addAttribute("roleAffiliateBroker", addUserDTO.getRoleAffiliateBroker()); 
				model.addAttribute("affiliateInfo", addUserDTO.getSetting());
				model.addAttribute("brokerInfo", addUserDTO.getTrackerBrokers());
				model.addAttribute("successMessage", addUserDTO.getMessage());
				model.addAttribute("status", addUserDTO.getStatus());
			}else{
				model.addAttribute("errorMessage", addUserDTO.getError().getDescription());
				model.addAttribute("status", addUserDTO.getStatus());
			}
			
			//map.put("trackerUser", trackerUser);
			//map.put("roleAffiliateBroker", roleAffiliateBroker!=null?roleAffiliateBroker.toUpperCase():"");
			if(roleAffiliateBroker!=null && roleAffiliateBroker.toUpperCase().equals("ROLE_BROKER")){
				return "page-admin-add-broker";
			}else if(roleAffiliateBroker!=null && roleAffiliateBroker.toUpperCase().equals("ROLE_AFFILIATES")){
				return "page-admin-add-affiliates";
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong..Please Try Again.");
		}				
		//return "redirect:ManageUsers";
		return "page-admin-add-user";
	}
	
	@RequestMapping({"/EditUsers"})
	public String editUserPage(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
			
		try{
			Map<String, String> map = Util.getParameterMap(request);
			Gson gson = GsonCustomConfig.getGsonBuilder();	
			
			String returnMessage = "";
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			
			map.put("userId", userIdStr);
			map.put("action", action);	
			
			/*if(userIdStr == null || userIdStr.isEmpty()){
				returnMessage = "User Id not empty";
			}
			Integer userId = Integer.parseInt(userIdStr);
			Collection<Role> roleListDB = DAORegistry.getRoleDAO().getAll();
			TrackerUser trackerUserDb = DAORegistry.getTrackerUserDAO().get(userId);
			TrackerUser trackerUser = new TrackerUser();
			String userName = trackerUserDb.getUserName();*/
			
			if(action != null && action.equals("editUserInfo")){
				String[] roles = request.getParameterValues("role");
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String email = request.getParameter("email");
				String phone = request.getParameter("phone");
				String companyName = request.getParameter("companyName");
				String serviceFees = request.getParameter("serviceFees");
				String affiliateDisc = request.getParameter("affiliateDiscount");
				String customerOrderDisc = request.getParameter("customerOrderDiscount");
				String status = request.getParameter("affiliateStatus");
				String repeatBusiness = request.getParameter("affiliateBusiness");
				String phoneAffiliateDisc = request.getParameter("phoneAffiliateDiscount");
				String phoneCustomerOrderDisc = request.getParameter("phoneCustomerOrderDiscount");
				String canEarnRewardPoints = request.getParameter("canEarnRewardPoints");
				String sessionUser = SecurityContextHolder.getContext().getAuthentication().getName();
				
				/*if(firstName == null || firstName.isEmpty()){
					returnMessage= "Firstname can't be blank";
				}else if(lastName == null || lastName.isEmpty()){
					returnMessage= "Lastname can't be blank";
				}else if(email == null || email.isEmpty()){
					returnMessage= "Email can't be blank";
				}else if(!AdminController.validateEMail(email)){
					returnMessage = "Invalid Email.";
				}else if(roles != null && roles.length == 0){
					returnMessage = "Please choose at least one Role.";
				}

				for(String role : roles){
					if(role.toUpperCase().equals("ROLE_BROKER")){
						if(companyName == null || companyName.isEmpty()){
							returnMessage = "Company can't be blank.";
						}
						if(serviceFees == null || serviceFees.isEmpty()){
							returnMessage = "Service Fees can't be blank.";
						}
					}
					if(role.toUpperCase().equals("ROLE_AFFILIATES")){
						if(customerOrderDisc == null || customerOrderDisc.isEmpty()){
							returnMessage = "Please add value for Order Discount.";
						}
						if(affiliateDisc == null || affiliateDisc.isEmpty()){
							returnMessage = "Please add valid value in Affiliates Discount.";
						}
						if(phoneCustomerOrderDisc == null || phoneCustomerOrderDisc.isEmpty()){
							returnMessage = "Please add value for Customer discount(Phone Orders).";
						}
						if(phoneAffiliateDisc == null || phoneAffiliateDisc.isEmpty()){
							returnMessage = "Please add valid value in Affiliates Discount(Phone Orders)";
						}
					}
				}*/

				map.put("role", gson.toJson(roles));
				map.put("firstName", firstName);
				map.put("lastName", lastName);
				map.put("email", email);
				map.put("phone", phone);
				map.put("companyName", companyName);
				map.put("serviceFees", serviceFees);
				map.put("affiliateDisc", affiliateDisc);
				map.put("customerOrderDisc", customerOrderDisc);
				map.put("affiliateStatus", status);
				map.put("repeatBusiness", repeatBusiness);
				map.put("sessionUser", sessionUser);
				map.put("phoneCustomerOrderDiscount", phoneCustomerOrderDisc);
				map.put("phoneAffiliateDiscount", phoneAffiliateDisc);
				map.put("canEarnRewardPoints", canEarnRewardPoints);
				
			}else if(action != null && action.equals("changePassword")){
				String password = request.getParameter("password");
				String rePassword = request.getParameter("repassword");
				
				/*if(password == null || password.isEmpty()){
					returnMessage= "Password can't be blank";
				}else if(rePassword == null || rePassword.isEmpty()){
					returnMessage = "Re-Password can't be blank";
				}else if(!password.equals(rePassword)){
					returnMessage = "Password and Re-Password must match.";
				}*/
				
				map.put("password", password);
				map.put("repassword", rePassword);
				
			}else if(action != null && action.equals("logoutUser")){
				  /*for (Object principal : sessionRegistry.getAllPrincipals()) {
				    	if(principal.equals(userName)){
				    		//
				    	}
				  }*/
				  
				  /*List<Object> sessionInformations = sessionRegistry.getAllPrincipals();
					if (sessionInformations != null) {
						System.out.println("==="+sessionInformations.size());
						
						//System.out.println("yes");
						return null;
					} else {
						System.out.println("no");
						return null;
					}*/
					
					
				//Log out the user programatically
				/*Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

				if (auth != null) {
					new SecurityContextLogoutHandler().logout(request,
							response, auth);
				}*/
				/*
				boolean wasLoggedIn = SessionListener.invalidateSessionsForUser(userName);
				if (wasLoggedIn) {
					map.put("successMessage", "User " + userName + " has been logged out.");
				} else {
					map.put("userActionsMessage", "User " + userName + " was not logged in.");				
				}*/
			}
			
			if(StringUtils.isEmpty(returnMessage)){
				String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_USERS);
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				EditUsersDTO editUsersDTO = gson.fromJson(((JsonObject)jsonObject.get("editUsersDTO")), EditUsersDTO.class);
				
				if(editUsersDTO.getError() == null){
					model.addAttribute("trackerBroker", editUsersDTO.getTrackerBrokerCustomDTO());
					model.addAttribute("rolesList", editUsersDTO.getRolesList());
					model.addAttribute("trackerUser", editUsersDTO.getEditTrackerUserCustomDTO());
					model.addAttribute("userId", userIdStr);
					model.addAttribute("pagingInfo", editUsersDTO.getPaginationDTO());
					model.addAttribute("msg", editUsersDTO.getMessage());
					model.addAttribute("status", editUsersDTO.getStatus());
					model.addAttribute("affiliateSetting", editUsersDTO.getAffiliateSettingCustomDTO());
				}else{
					model.addAttribute("msg", editUsersDTO.getError().getDescription());
					model.addAttribute("status", editUsersDTO.getStatus());
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	
	/*@RequestMapping({"/AuditUsers"})
	public String auditUserPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try{
			String userIdStr = request.getParameter("userId");
			String action = request.getParameter("action");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");

			if(userIdStr == null || userIdStr.isEmpty()){
				return "redirect:/Admin/ManageUsers";
			}
			
			//String returnMessage = "";
			//SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			
			if(action != null && action.equals("action")){
				model.addAttribute("fromDate", fromDate);
				model.addAttribute("toDate", toDate);				
			}else{
				model.addAttribute("fromDate", dateFormat.format(new Date()));
				model.addAttribute("toDate", dateFormat.format(new Date()));
			}
			
			//if(StringUtils.isEmpty(returnMessage)){
				Map<String, String> map = Util.getParameterMap(request);				
				map.put("fromDate", fromDate);
				map.put("toDate", toDate);
				map.put("action", action);
				map.put("userId", userIdStr);
				
				String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUDIT_USERS);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				AuditUsersDTO auditUsersDTO = gson.fromJson(((JsonObject)jsonObject.get("auditUsersDTO")), AuditUsersDTO.class);
				
				if(auditUsersDTO.getStatus() == 1){
					model.addAttribute("allTrackerUserList", auditUsersDTO.getTrackerUserList());
					model.addAttribute("userActionList", auditUsersDTO.getUserActionList());
					model.addAttribute("msg", auditUsersDTO.getMessage());
					model.addAttribute("status", auditUsersDTO.getStatus());
				}else{
					model.addAttribute("msg", auditUsersDTO.getError().getDescription());
					model.addAttribute("status", auditUsersDTO.getStatus());
				}
			}else{
				model.addAttribute("errorMessage", returnMessage);
			}
			model.addAttribute("userId", userIdStr);
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong. Please try again");
		}
		return "page-admin-audit-user";
	}*/
	
	@RequestMapping(value = "/UserStatusChange")
	public String updateUserStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		
		try{
			/*TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				return "Please Login.";
			}*/
			
			String userIdStr = request.getParameter("userId");
			String status = request.getParameter("status");
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr = request.getParameter("toDate");
			String promoCode = request.getParameter("promoCode");
			
			/*TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
			if(trackerUser == null){
				return "There is no user to update.";
			}
			if(trackerUserSession.getId().equals(trackerUser.getId())){
				return "You can't update your own account.";
			}*/
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userId", userIdStr);
			map.put("status", status);
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("promoCode", promoCode);
			map.put("sessionUser", session.getAttribute("trackerUser").toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_USER_STATUS_CHANGE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GenerateDiscountCode")
	public String generateDiscountCode(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GENERATE_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenerateDiscountCodeDTO generateDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("generateDiscountCodeDTO")), GenerateDiscountCodeDTO.class);
			
			if(generateDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", generateDiscountCodeDTO.getMessage());
				model.addAttribute("status", generateDiscountCodeDTO.getStatus());
			}else{
				model.addAttribute("msg", generateDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", generateDiscountCodeDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("discountCodePagingInfo", generateDiscountCodeDTO.getDiscountCodePaginationDTO());
				model.addAttribute("discountCodeList", generateDiscountCodeDTO.getPromoOfferDTO());
			}else{
				model.addAttribute("discountCodePagingInfo", gson.toJson(generateDiscountCodeDTO.getDiscountCodePaginationDTO()));
				model.addAttribute("promoOfferDtlPagingInfo", gson.toJson(generateDiscountCodeDTO.getPromoOfferDetailPaginationDTO()));
				model.addAttribute("discountCodeList", gson.toJson(generateDiscountCodeDTO.getPromoOfferDTO()));
			}
			
			/*GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(null);
			List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDicountCodes(null,filter);
			map.put("discountCodePagingInfo", PaginationUtil.getDummyPaginationParameter(list!=null?list.size():0));
			map.put("promoOfferDtlPagingInfo", PaginationUtil.getDummyPaginationParameter(0));
			map.put("discountCodeList", JsonWrapperUtil.getRTFPromotionalCodeArray(list));*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-generate-discount-code";
	}
	
	/*@RequestMapping(value = "/GetGenerateDiscountCode")
	public String getGenerateDiscountCode(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_GENERATE_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenerateDiscountCodeDTO generateDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("generateDiscountCodeDTO")), GenerateDiscountCodeDTO.class);
			
			if(generateDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", generateDiscountCodeDTO.getMessage());
				model.addAttribute("status", generateDiscountCodeDTO.getStatus());
				model.addAttribute("discountCodePagingInfo", generateDiscountCodeDTO.getDiscountCodePaginationDTO());
				model.addAttribute("discountCodeList", generateDiscountCodeDTO.getPromoOfferDTO());
			}else{
				model.addAttribute("msg", generateDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", generateDiscountCodeDTO.getStatus());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}*/
	
	@RequestMapping(value = "/UpdateDiscountCodeStatus")
	public String updateDiscountCodeStatus(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String ids = request.getParameter("ids");
			String status = request.getParameter("status");			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("ids", ids);
			map.put("status", status);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_DISCOUNT_CODE_STATUS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			/*JSONObject object = new JSONObject();
			object.put("status", 0);
			
			String userActionMsg = "";
			if(idStr!=null && !idStr.isEmpty()){
				if(status!= null && status.equalsIgnoreCase("ENABLED")){
					if(idStr.contains(",")){
						String str[] = idStr.split(",");
						for(String s : str){
							if(s.trim().isEmpty()){
								continue;
							}
							RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(s));
							offer.setStatus("ENABLED");
							offer.setModifiedBy(userName);
							offer.setModifiedDate(new Date());
							DAORegistry.getRtfPromoOffersDAO().update(offer);
						}
					}else{
						RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(idStr));
						offer.setStatus("ENABLED");
						offer.setModifiedBy(userName);
						offer.setModifiedDate(new Date());
						DAORegistry.getRtfPromoOffersDAO().update(offer);
					}
					object.put("status",1);
					object.put("msg","Discount Code offer Enabled successfully.");
					
					//Tracking User Action
					if(idStr.contains(",")){
						userActionMsg = "Discount Code Offer Enabled Successfully. Offer Id's - "+idStr;
						Util.userActionAudit(request, null, userActionMsg);
					}else{
						userActionMsg = "Discount Code Offer Enabled Successfully.";
						Util.userActionAudit(request, Integer.parseInt(idStr), userActionMsg);
					}
				}else if(status!= null && status.equalsIgnoreCase("DISABLED")){
					if(idStr.contains(",")){
						String str[] = idStr.split(",");
						for(String s : str){
							if(s.trim().isEmpty()){
								continue;
							}
							RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(s));
							offer.setStatus("DISABLED");
							offer.setModifiedBy(userName);
							offer.setModifiedDate(new Date());
							DAORegistry.getRtfPromoOffersDAO().update(offer);
						}
					}else{
						RTFPromoOffers offer = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(idStr));
						offer.setStatus("DISABLED");
						offer.setModifiedBy(userName);
						offer.setModifiedDate(new Date());
						DAORegistry.getRtfPromoOffersDAO().update(offer);
					}
					object.put("status",1);
					object.put("msg","Discount Code offer Disabled successfully.");
					
					//Tracking User Action
					if(idStr.contains(",")){
						userActionMsg = "Discount Code Offer Disabled Successfully. Offer Id's - "+idStr;
						Util.userActionAudit(request, null, userActionMsg);
					}else{
						userActionMsg = "Discount Code Offer Disabled Successfully.";
						Util.userActionAudit(request, Integer.parseInt(idStr), userActionMsg);
					}
				}
				
			}
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter); 
			List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDicountCodes(null,filter);
			object.put("discountCodePagingInfo", PaginationUtil.getDummyPaginationParameter(list!=null?list.size():0));
			object.put("discountCodeList", JsonWrapperUtil.getRTFPromotionalCodeArray(list));
			object.put("status",1);
			
			//return jsonarr
			IOUtils.write(object.toString().getBytes(),response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
		
	@RequestMapping(value = "/SaveDiscountCode")
	public String saveDiscountCode(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();

			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SaveDiscountCodeDTO saveDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("saveDiscountCodeDTO")), SaveDiscountCodeDTO.class);
			
			if(saveDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", saveDiscountCodeDTO.getMessage());
				model.addAttribute("status", saveDiscountCodeDTO.getStatus());
				model.addAttribute("discountCodePagingInfo", saveDiscountCodeDTO.getDiscountCodePaginationDTO());
				model.addAttribute("discountCodeList", saveDiscountCodeDTO.getPromoOfferDTO());
			}else{
				model.addAttribute("msg", saveDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", saveDiscountCodeDTO.getStatus());
			}
			
			/*String promoAutoGenerate = request.getParameter("promoAutoGenerate");
			String promoCode = request.getParameter("promoCode");
			String promoFlatDiscount = request.getParameter("promoFlatDiscount");
			String promoDiscountPerc = request.getParameter("promoDiscountPerc");
			String promoMobileDiscountPerc = request.getParameter("promoMobileDiscountPerc");
			String promoOrderThreshold = request.getParameter("promoOrderThreshold");
			String promoFromDate = request.getParameter("promoFromDate");
			String promoToDate = request.getParameter("promoToDate");
			String promoMaxOrders = request.getParameter("promoMaxOrders");
			String count = request.getParameter("count");
			String parentId = "";
			String childId = "";
			String grandChildId = "";
			String artistId = "";
			String venueId = "";		
			String promoTypeId = "";
			String selectedString = "";
			JSONObject object = new JSONObject();
			object.put("status", 0);
			
			if(autoGenerate == null || autoGenerate.isEmpty()){
				object.put("msg","Please select Auto Generate Option.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			if(autoGenerate.equalsIgnoreCase("No")){
				if(promotionalCode == null || promotionalCode.isEmpty()){
					object.put("msg","Promotional Code is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}else{
					RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(promotionalCode);
					if(promoOffer != null){
						object.put("msg","Promotional Code is already exist.");
						IOUtils.write(object.toString().getBytes(),response.getOutputStream());
						return;
					}
				}
			}
			
			int count = 0;
			for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
				parentId = request.getParameter("parentId_"+i);
				childId = request.getParameter("childId_"+i);
				grandChildId = request.getParameter("grandChildId_"+i);
				artistId = request.getParameter("artistId_"+i);
				venueId = request.getParameter("venueId_"+i);		
				promoTypeId = request.getParameter("promoTypeId_"+i);
				if((promoTypeId == null || promoTypeId.isEmpty()) && (parentId==null || parentId.isEmpty()) && (childId==null || childId.isEmpty())
						&& (grandChildId==null || grandChildId.isEmpty()) && (artistId==null || artistId.isEmpty())
						&& (venueId==null || venueId.isEmpty())){
					count++;
				}
			}
			if(count == Integer.parseInt(countPromoTypes)){
				object.put("msg","Id not found for one of following Artist,venue,parent,child,grand child.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			
			if(flatDiscount == null || flatDiscount.isEmpty()){
				object.put("msg","Please select Flat Discount Option.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			if(flatDiscount.equalsIgnoreCase("Yes")){
				if(orderThreshold == null || orderThreshold.isEmpty()){
					object.put("msg","Order Threshold is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
			}
			
			if(discountPerc==null || discountPerc.isEmpty()){
				object.put("msg","Discount percentage is not found.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			
			if(mobileDiscountPerc==null || mobileDiscountPerc.isEmpty()){
				object.put("msg","Mobile discount percentage is not found.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			
			if(fromDate==null || fromDate.isEmpty() || toDate == null || toDate.isEmpty()){
				object.put("msg","Missing from Date or To Date.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			
			if(maxOrders==null || maxOrders.isEmpty()){
				object.put("msg","Max. Orders is not found.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			
			fromDate = fromDate + " 00:00:00";
			toDate = toDate + " 23:59:59";
			Date fDate = null;
			Date tDate = null;
			try {
				fDate = Util.getDateWithTwentyFourHourFormat1(fromDate);
				tDate = Util.getDateWithTwentyFourHourFormat1(toDate);
			} catch (Exception e) {
				e.printStackTrace();
				object.put("msg","Start Date or End Date with bad values.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			
			for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
				selectedString = request.getParameter("artistVenueName_"+i);
				if(selectedString != null && !selectedString.isEmpty()){
					break;
				}
			}
			Date today = new Date();
			
			RTFPromoOffers offer  = new RTFPromoOffers();
			offer.setStartDate(fDate);
			offer.setEndDate(tDate);
			offer.setMaxOrders(Integer.parseInt(maxOrders));
			offer.setCreatedBy(userName);
			offer.setCreatedDate(today);
			offer.setDiscountPer(Double.parseDouble(discountPerc));
			offer.setMobileDiscountPerc(Double.parseDouble(mobileDiscountPerc));
			offer.setModifiedBy(userName);
			offer.setModifiedDate(today);
			offer.setNoOfOrders(0);
			if(autoGenerate.equalsIgnoreCase("Yes")){
				offer.setPromoCode(Util.generateEncryptedKey(selectedString));
			}else if(autoGenerate.equalsIgnoreCase("No")){
				offer.setPromoCode(promotionalCode);
			}
			if(flatDiscount.equalsIgnoreCase("Yes")){
				offer.setFlatOfferOrderThreshold(Double.parseDouble(orderThreshold));	//Order Threshold
			}
			offer.setStatus("ENABLED");
			if(flatDiscount.equalsIgnoreCase("Yes")){
				offer.setIsFlatDiscount(true);
			}else if(flatDiscount.equalsIgnoreCase("No")){
				offer.setIsFlatDiscount(false);
			}
			
			DAORegistry.getRtfPromoOffersDAO().save(offer);
			
			RTFPromoType promoTypeOffer = null;
			for(int i=0; i< Integer.parseInt(countPromoTypes); i++){
				promoTypeOffer = new RTFPromoType();
				parentId = request.getParameter("parentId_"+i);
				childId = request.getParameter("childId_"+i);
				grandChildId = request.getParameter("grandChildId_"+i);
				artistId = request.getParameter("artistId_"+i);
				venueId = request.getParameter("venueId_"+i);		
				promoTypeId = request.getParameter("promoTypeId_"+i);
				
				if((promoTypeId != null && !promoTypeId.isEmpty()) || (artistId!=null && !artistId.isEmpty()) || 
					(venueId!=null && !venueId.isEmpty()) || (grandChildId!=null && !grandChildId.isEmpty()) || 
					(childId!=null && !childId.isEmpty()) || (parentId!=null && !parentId.isEmpty())){
					promoTypeOffer.setPromoOfferId(offer.getId());
					if(promoTypeId != null && !promoTypeId.isEmpty()){
						promoTypeOffer.setPromoType(PromotionalType.ALL.toString());
					}else if(artistId!=null && !artistId.isEmpty()){
						promoTypeOffer.setArtistId(Integer.parseInt(artistId));
						promoTypeOffer.setPromoType(PromotionalType.ARTIST.toString());
					}else if(venueId!=null && !venueId.isEmpty()){
						promoTypeOffer.setVenueId(Integer.parseInt(venueId));
						promoTypeOffer.setPromoType(PromotionalType.VENUE.toString());
					}else if(grandChildId!=null && !grandChildId.isEmpty()){
						promoTypeOffer.setGrandChildId(Integer.parseInt(grandChildId));
						promoTypeOffer.setPromoType(PromotionalType.GRANDCHILD.toString());
					}else if(childId!=null && !childId.isEmpty()){
						promoTypeOffer.setChildId(Integer.parseInt(childId));
						promoTypeOffer.setPromoType(PromotionalType.CHILD.toString());
					}else if(parentId!=null && !parentId.isEmpty()){
						promoTypeOffer.setParentId(Integer.parseInt(parentId));
						promoTypeOffer.setPromoType(PromotionalType.PARENT.toString());
					}
					
					DAORegistry.getRtfPromoTypeDAO().save(promoTypeOffer);
				}
			}
			
			
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(null);
			List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDicountCodes(null,filter);
			object.put("discountCodePagingInfo", PaginationUtil.getDummyPaginationParameter(list!=null?list.size():0));
			object.put("discountCodeList", JsonWrapperUtil.getRTFPromotionalCodeArray(list));
			object.put("msg","Discount Code generated successfully.");
			object.put("status",1);
			
			//Tracking User Action
			String userActionMsg = "Discount Code Generated Successfully.";
			Util.userActionAudit(request, offer.getId(), userActionMsg);
			
			//return jsonarr
			IOUtils.write(object.toString().getBytes(),response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/EditDiscountCode")
	public String editDiscountCode(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			String action = request.getParameter("action");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("action", action);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SaveDiscountCodeDTO saveDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("saveDiscountCodeDTO")), SaveDiscountCodeDTO.class);
			
			if(saveDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", saveDiscountCodeDTO.getMessage());
				model.addAttribute("status", saveDiscountCodeDTO.getStatus());
				if(action != null && action.equalsIgnoreCase("edit")){
					model.addAttribute("promoCodeList", saveDiscountCodeDTO.getPromoOfferDTO());
				}
				if(action != null && action.equalsIgnoreCase("update")){
					model.addAttribute("discountCodePagingInfo", saveDiscountCodeDTO.getDiscountCodePaginationDTO());
					model.addAttribute("discountCodeList", saveDiscountCodeDTO.getPromoOfferDTO());
				}
			}else{
				model.addAttribute("msg", saveDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", saveDiscountCodeDTO.getStatus());
			}
			/*String promoAutoGenerate = request.getParameter("promoAutoGenerate");
			String promoCode = request.getParameter("promoCode");
			String promoFlatDiscount = request.getParameter("promoFlatDiscount");
			String promoDiscountPerc = request.getParameter("promoDiscountPerc");
			String promoMobileDiscountPerc = request.getParameter("promoMobileDiscountPerc");
			String promoOrderThreshold = request.getParameter("promoOrderThreshold");
			String promoFromDate = request.getParameter("promoFromDate");
			String promoToDate = request.getParameter("promoToDate");
			String promoMaxOrders = request.getParameter("promoMaxOrders");
			String count = request.getParameter("count");
			String parentId = "";
			String childId = "";
			String grandChildId = "";
			String artistId = "";
			String venueId = "";		
			String promoTypeId = "";
			String selectedString = "";
			JSONObject object = new JSONObject();
			object.put("status",0);
			
			
			if(action != null && action.equalsIgnoreCase("edit")){
				String id = request.getParameter("promoId");
				if(id == null || id.isEmpty()){
					object.put("msg","Please select Any Promotional Code.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(null);
				List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDicountCodes(Integer.parseInt(id),filter);				
				if(list != null && list.size() > 0){
					object.put("promoCodeList", JsonWrapperUtil.getRTFPromotionalCodeArray(list));
				}
				object.put("status",1);
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}else if(action != null && action.equalsIgnoreCase("update")){
				String id = request.getParameter("promoId");
				if(autoGenerate == null || autoGenerate.isEmpty()){
					object.put("msg","Please select Auto Generate Option.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				if(autoGenerate.equalsIgnoreCase("No")){
					if(promotionalCode == null || promotionalCode.isEmpty()){
//						object.put("msg","Promotional Code is not found.");
//						IOUtils.write(object.toString().getBytes(),response.getOutputStream());
//						return;
					}else{
//						RTFPromoOffers promoOffer = DAORegistry.getRtfPromoOffersDAO().getPromoCode(promotionalCode);
//						if(promoOffer != null){
//							object.put("msg","Promotional Code is already exist.");
//							IOUtils.write(object.toString().getBytes(),response.getOutputStream());
//							return;
//						}
					}
				}
				
				int count = 0;
				for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
					parentId = request.getParameter("parentId_"+i);
					childId = request.getParameter("childId_"+i);
					grandChildId = request.getParameter("grandChildId_"+i);
					artistId = request.getParameter("artistId_"+i);
					venueId = request.getParameter("venueId_"+i);		
					promoTypeId = request.getParameter("promoTypeId_"+i);
					if((promoTypeId == null || promoTypeId.isEmpty()) && (parentId==null || parentId.isEmpty()) && (childId==null || childId.isEmpty())
							&& (grandChildId==null || grandChildId.isEmpty()) && (artistId==null || artistId.isEmpty())
							&& (venueId==null || venueId.isEmpty())){
						count++;
					}
				}
				if(count == Integer.parseInt(countPromoTypes)){
					object.put("msg","Id not found for one of following Artist,venue,parent,child,grand child.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				if(flatDiscount == null || flatDiscount.isEmpty()){
					object.put("msg","Please select Flat Discount Option.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				if(flatDiscount.equalsIgnoreCase("Yes")){
					if(orderThreshold == null || orderThreshold.isEmpty()){
						object.put("msg","Order Threshold is not found.");
						IOUtils.write(object.toString().getBytes(),response.getOutputStream());
						return;
					}
				}
				
				if(discountPerc==null || discountPerc.isEmpty()){
					object.put("msg","Discount percentage is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				if(mobileDiscountPerc==null || mobileDiscountPerc.isEmpty()){
					object.put("msg","Mobile discount percentage is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				if(fromDate==null || fromDate.isEmpty() || toDate == null || toDate.isEmpty()){
					object.put("msg","Missing from Date or To Date.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				if(maxOrders==null || maxOrders.isEmpty()){
					object.put("msg","Max. Orders is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				fromDate = fromDate + " 00:00:00";
				toDate = toDate + " 23:59:59";
				Date fDate = null;
				Date tDate = null;
				try {
					fDate = Util.getDateWithTwentyFourHourFormat1(fromDate);
					tDate = Util.getDateWithTwentyFourHourFormat1(toDate);
				} catch (Exception e) {
					e.printStackTrace();
					object.put("msg","Start Date or End Date with bad values.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				for(int i=0; i < Integer.parseInt(countPromoTypes); i++){
					selectedString = request.getParameter("artistVenueName_"+i);
					if(selectedString != null && !selectedString.isEmpty()){
						break;
					}
				}
				Date today = new Date();
				
				RTFPromoOffers offer  = DAORegistry.getRtfPromoOffersDAO().get(Integer.parseInt(id));
				offer.setStartDate(fDate);
				offer.setEndDate(tDate);
				offer.setMaxOrders(Integer.parseInt(maxOrders));
				offer.setCreatedBy(userName);
//				offer.setCreatedDate(today);
				offer.setDiscountPer(Double.parseDouble(discountPerc));
				offer.setMobileDiscountPerc(Double.parseDouble(mobileDiscountPerc));
				offer.setModifiedBy(userName);
				offer.setModifiedDate(today);
				offer.setNoOfOrders(0);
//				if(autoGenerate.equalsIgnoreCase("Yes")){
//					offer.setPromoCode(Util.generateEncryptedKey(selectedString));
//				}else if(autoGenerate.equalsIgnoreCase("No")){
//					offer.setPromoCode(promotionalCode);
//				}
				
				offer.setStatus("ENABLED");
				if(flatDiscount.equalsIgnoreCase("Yes")){
					offer.setIsFlatDiscount(true);
					offer.setFlatOfferOrderThreshold(Double.parseDouble(orderThreshold));	//Order Threshold
				}else if(flatDiscount.equalsIgnoreCase("No")){
					offer.setIsFlatDiscount(false);
					offer.setFlatOfferOrderThreshold(0.00);	//Order Threshold
				}
				
				DAORegistry.getRtfPromoOffersDAO().update(offer);
				
				List<RTFPromoType> promoTypeOffers =  DAORegistry.getRtfPromoTypeDAO().getPromoTypeFromOfferId(Integer.parseInt(id));
				if(promoTypeOffers != null && promoTypeOffers.size() > 0){
					DAORegistry.getRtfPromoTypeDAO().deleteAll(promoTypeOffers);
				}
				RTFPromoType promoTypeOffer = null;				
				for(int i=0; i< Integer.parseInt(countPromoTypes); i++){
					
					parentId = request.getParameter("parentId_"+i);
					childId = request.getParameter("childId_"+i);
					grandChildId = request.getParameter("grandChildId_"+i);
					artistId = request.getParameter("artistId_"+i);
					venueId = request.getParameter("venueId_"+i);		
					promoTypeId = request.getParameter("promoTypeId_"+i);
					
					if((promoTypeId != null && !promoTypeId.isEmpty()) || (artistId!=null && !artistId.isEmpty()) || 
						(venueId!=null && !venueId.isEmpty()) || (grandChildId!=null && !grandChildId.isEmpty()) || 
						(childId!=null && !childId.isEmpty()) || (parentId!=null && !parentId.isEmpty())){
						
//						if(j < promoTypeOffers.size() && promoTypeOffers.get(j) != null){
//							if(promoTypeOffers.get(j).getPromoOfferId().intValue() == offer.getId().intValue()){
//								promoTypeOffer = promoTypeOffers.get(j);
//								j++;
//							}
//						}else{
//							promoTypeOffer = new RTFPromoType();
//						}
						promoTypeOffer = new RTFPromoType();
						
						promoTypeOffer.setPromoOfferId(offer.getId());
						if(promoTypeId != null && !promoTypeId.isEmpty()){
							promoTypeOffer.setPromoType(PromotionalType.ALL.toString());
						}else if(artistId!=null && !artistId.isEmpty()){
							promoTypeOffer.setArtistId(Integer.parseInt(artistId));
							promoTypeOffer.setPromoType(PromotionalType.ARTIST.toString());
						}else if(venueId!=null && !venueId.isEmpty()){
							promoTypeOffer.setVenueId(Integer.parseInt(venueId));
							promoTypeOffer.setPromoType(PromotionalType.VENUE.toString());
						}else if(grandChildId!=null && !grandChildId.isEmpty()){
							promoTypeOffer.setGrandChildId(Integer.parseInt(grandChildId));
							promoTypeOffer.setPromoType(PromotionalType.GRANDCHILD.toString());
						}else if(childId!=null && !childId.isEmpty()){
							promoTypeOffer.setChildId(Integer.parseInt(childId));
							promoTypeOffer.setPromoType(PromotionalType.CHILD.toString());
						}else if(parentId!=null && !parentId.isEmpty()){
							promoTypeOffer.setParentId(Integer.parseInt(parentId));
							promoTypeOffer.setPromoType(PromotionalType.PARENT.toString());
						}
						
						DAORegistry.getRtfPromoTypeDAO().save(promoTypeOffer);
					}
				}
				
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(null);
				List<RTFPromoOffers> list = DAORegistry.getQueryManagerDAO().getDicountCodes(null,filter);
				object.put("discountCodePagingInfo", PaginationUtil.getDummyPaginationParameter(list!=null?list.size():0));
				object.put("discountCodeList", JsonWrapperUtil.getRTFPromotionalCodeArray(list));
				object.put("msg","Discount Code updated successfully.");
				object.put("status",1);
								
				//Tracking User Action
				String userActionMsg = "Discount Code Updated Successfully.";
				Util.userActionAudit(request, offer.getId(), userActionMsg);
				
				//return jsonarr
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GetDiscountCodeDetail")
	public String getDiscountCodeDetail(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String promoOfferId = request.getParameter("promoOfferId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("promoOfferId", promoOfferId);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_DISCOUNT_CODE_DETAIL);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			DiscountCodeDetailDTO discountCodeDetailDTO = gson.fromJson(((JsonObject)jsonObject.get("discountCodeDetailDTO")), DiscountCodeDetailDTO.class);
			
			if(discountCodeDetailDTO.getStatus() == 1){
				model.addAttribute("msg", discountCodeDetailDTO.getMessage());
				model.addAttribute("status", discountCodeDetailDTO.getStatus());
				model.addAttribute("promoOfferDtlPagingInfo", discountCodeDetailDTO.getPromoOfferDetailPaginationDTO());
				model.addAttribute("promoOfferDtlList", discountCodeDetailDTO.getPromoOfferDTO());
			}else{
				model.addAttribute("msg", discountCodeDetailDTO.getError().getDescription());
				model.addAttribute("status", discountCodeDetailDTO.getStatus());
			}
			/*List<RTFPromoOffers> promoOfferList = null;
			JSONObject object = new JSONObject();
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter);
			
			promoOfferList = DAORegistry.getQueryManagerDAO().getPromotionalOfferDetails(Integer.parseInt(promoOfferId), filter);
			
			object.put("promoOfferDtlPagingInfo", PaginationUtil.getDummyPaginationParameter(promoOfferList!=null?promoOfferList.size():0));
			object.put("promoOfferDtlList", JsonWrapperUtil.getRTFPromotionalCodeArray(promoOfferList));
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*@RequestMapping({"/PromoOfferDtlExportToExcel"})
	public void PromoOfferDetailToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		String promoOfferId = request.getParameter("promoOfferId");
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter); 
			List<RTFPromoOffers> promoOffersList = DAORegistry.getQueryManagerDAO().getPromotionalOfferDetails(Integer.parseInt(promoOfferId), filter);
			
			if(promoOffersList!=null && !promoOffersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Promotional_Offer_Detail");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Offer Detail Id");				
				header.createCell(1).setCellValue("Promo Type");
				header.createCell(2).setCellValue("Artist");
				header.createCell(3).setCellValue("Venue");
				header.createCell(4).setCellValue("Parent Category");
				header.createCell(5).setCellValue("Child Category");
				header.createCell(6).setCellValue("Grand Child Category");
				Integer i=1;
				for(RTFPromoOffers promoOffer : promoOffersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getPromoType()!=null?promoOffer.getPromoType():"");
					row.createCell(2).setCellValue(promoOffer.getArtistName()!=null?promoOffer.getArtistName():"");
					row.createCell(3).setCellValue(promoOffer.getVenueName()!=null?promoOffer.getVenueName():"");
					row.createCell(4).setCellValue(promoOffer.getParentCategory()!=null?promoOffer.getParentCategory():"");
					row.createCell(5).setCellValue(promoOffer.getChildCategory()!=null?promoOffer.getChildCategory():"");
					row.createCell(6).setCellValue(promoOffer.getGrandChildCategory()!=null?promoOffer.getGrandChildCategory():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Promotional_Offer_Detail.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping("/AutoCompleteArtistAndEventAndVenueAndCategory")
	public void getAutoCompleteArtistAndEventAndVenueAndCategory(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_ARTIST_EVENT_VENUE_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistVenueAndCategoryDTO autoCompleteArtistVenueAndCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistVenueAndCategoryDTO")), AutoCompleteArtistVenueAndCategoryDTO.class);
						
			if(autoCompleteArtistVenueAndCategoryDTO.getStatus() == 1){
				writer.write(autoCompleteArtistVenueAndCategoryDTO.getArtistVenueAndCategory());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<ParentCategory> parentCat = DAORegistry.getParentCategoryDAO().getParentCategoriesByName(param);
		Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
		Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByName(param);
		strResponse += ("ALL"+ "|" + "0" + "|" + "ALL" + "\n");
		if (parentCat != null) {
			for (ParentCategory parent : parentCat) {
				strResponse += ("PARENT" + "|" + parent.getId() + "|" + parent.getName() + "\n");
			}
		}
		
		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}
		
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
				
		if (venues != null) {
			for (Venue venue : venues) {
				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
			}
		}
		
		writer.write(strResponse);*/
	}
	
	
	public static boolean isEmptyOrNull(String value){
			
		if(null == value || value.isEmpty()){
			return true;
		}
		return false;
	}
	

	/*@RequestMapping({"/DiscountCodeExportToExcel"})
	public void discountCodeToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferFilter(headerFilter); 
			List<RTFPromoOffers> promoOffersList = DAORegistry.getQueryManagerDAO().getDicountCodesToExport(filter);
			
			if(promoOffersList!=null && !promoOffersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Discount_Code");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Offer Id");
				header.createCell(1).setCellValue("Promotional Code");
				header.createCell(2).setCellValue("Desktop Discount");
				header.createCell(3).setCellValue("APP Discount");
				header.createCell(4).setCellValue("Start Date");
				header.createCell(5).setCellValue("End Date");
				header.createCell(6).setCellValue("Max. Order");
				header.createCell(7).setCellValue("Orders");
				header.createCell(8).setCellValue("Status");
				header.createCell(9).setCellValue("Created By");
				header.createCell(10).setCellValue("Created Date");
				header.createCell(11).setCellValue("Modified By");
				Integer i=1;
				for(RTFPromoOffers promoOffer : promoOffersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getPromoCode());
					row.createCell(2).setCellValue(promoOffer.getDiscountPer()!=null?promoOffer.getDiscountPer():0);
					row.createCell(3).setCellValue(promoOffer.getMobileDiscountPerc()!=null?promoOffer.getMobileDiscountPerc():0);
					row.createCell(4).setCellValue(promoOffer.getStartDateStr()!=null?promoOffer.getStartDateStr():"");
					row.createCell(5).setCellValue(promoOffer.getEndDateStr()!=null?promoOffer.getEndDateStr():"");
					row.createCell(6).setCellValue(promoOffer.getMaxOrders()!=null?promoOffer.getMaxOrders():0);
					row.createCell(7).setCellValue(promoOffer.getNoOfOrders()!=null?promoOffer.getNoOfOrders():0);
					row.createCell(8).setCellValue(promoOffer.getStatus()!=null?promoOffer.getStatus():"");
					row.createCell(9).setCellValue(promoOffer.getCreatedBy()!=null?promoOffer.getCreatedBy():"");
					row.createCell(10).setCellValue(promoOffer.getCreatedDateStr()!=null?promoOffer.getCreatedDateStr():"");
					row.createCell(11).setCellValue(promoOffer.getModifiedBy()!=null?promoOffer.getModifiedBy():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Discount_Code.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping(value = "/CustomerPromotionalOffer")
	public String customerPromotionalCode(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CUSTOMER_PROMOTIONAL_OFFER_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("customerPromotionalOfferDetailsDTO")), CustomerPromotionalOfferDetailsDTO.class);
			
			if(customerPromotionalOfferDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", customerPromotionalOfferDetailsDTO.getMessage());
				model.addAttribute("status", customerPromotionalOfferDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", customerPromotionalOfferDetailsDTO.getError().getDescription());
				model.addAttribute("status", customerPromotionalOfferDetailsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("promoOfferPagingInfo", customerPromotionalOfferDetailsDTO.getCusPromoOfferPaginationDTO());
				model.addAttribute("promoOfferList", customerPromotionalOfferDetailsDTO.getCusPromoOfferDTO());
			}else{
				model.addAttribute("promoOfferPagingInfo", gson.toJson(customerPromotionalOfferDetailsDTO.getCusPromoOfferPaginationDTO()));
				model.addAttribute("promoOfferList", gson.toJson(customerPromotionalOfferDetailsDTO.getCusPromoOfferDTO()));
				model.addAttribute("promoOfferTrackingPagingInfo", gson.toJson(customerPromotionalOfferDetailsDTO.getCusPromoOfferTrackingPaginationDTO()));
				model.addAttribute("promoOfferTrackingList", gson.toJson(customerPromotionalOfferDetailsDTO.getCusPromoOfferTrackingDTO()));
			}
			
			/*GridHeaderFilters filter = new GridHeaderFilters();
			List<RTFCustomerPromotionalOffer> promoOfferList = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOffer(filter,null);
			List<RTFPromotionalOfferTracking> promoOfferTrackingList = DAORegistry.getQueryManagerDAO().getPromotionalOfferTracking(filter, null);
			map.put("promoOfferTrackingPagingInfo", PaginationUtil.calculatePaginationParameters((promoOfferTrackingList!=null?promoOfferTrackingList.size():0), null));
			map.put("promoOfferTrackingList", JsonWrapperUtil.getRTFPromotionalOfferTrackingArray(promoOfferTrackingList));
			map.put("promoOfferPagingInfo", PaginationUtil.calculatePaginationParameters((promoOfferList!=null?promoOfferList.size():0), null));
			map.put("promoOfferList", JsonWrapperUtil.getRTFCustomerPromotionalOfferArray(promoOfferList));*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-customer-promotional-offer";
	}
	
	/*@RequestMapping(value = "/GetCustomerPromotionalOffer")
	public String getCustomerPromotionalOffer(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_PROMOTIONAL_OFFER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("customerPromotionalOfferDetailsDTO")), CustomerPromotionalOfferDetailsDTO.class);
			
			if(customerPromotionalOfferDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", customerPromotionalOfferDetailsDTO.getMessage());
				model.addAttribute("status", customerPromotionalOfferDetailsDTO.getStatus());
				model.addAttribute("promoOfferPagingInfo", customerPromotionalOfferDetailsDTO.getCusPromoOfferPaginationDTO());
				model.addAttribute("promoOfferList", customerPromotionalOfferDetailsDTO.getCusPromoOfferDTO());
			}else{
				model.addAttribute("msg", customerPromotionalOfferDetailsDTO.getError().getDescription());
				model.addAttribute("status", customerPromotionalOfferDetailsDTO.getStatus());
			}
			
//			JSONObject object = new JSONObject();
//			object.put("status", 0);
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFCustomerPromotionalOfferFilter(headerFilter); 
//			List<RTFCustomerPromotionalOffer> promoOfferList = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOffer(filter,pageNo);
//			object.put("promoOfferPagingInfo", PaginationUtil.calculatePaginationParameters((promoOfferList!=null?promoOfferList.size():0), pageNo));
//			object.put("promoOfferList", JsonWrapperUtil.getRTFCustomerPromotionalOfferArray(promoOfferList));
//			object.put("status", 1);
//			IOUtils.write(object.toString().getBytes(),response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}*/
	
	@RequestMapping(value = "/GetCustomerPromotionalOfferTracking")
	public String getCustomerPromotionalOfferTracking(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_PROMOTIONAL_OFFER_TRACKING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerPromotionalOfferDetailsDTO customerPromotionalOfferDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("customerPromotionalOfferDetailsDTO")), CustomerPromotionalOfferDetailsDTO.class);
			
			if(customerPromotionalOfferDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", customerPromotionalOfferDetailsDTO.getMessage());
				model.addAttribute("status", customerPromotionalOfferDetailsDTO.getStatus());
				model.addAttribute("promoOfferTrackingPagingInfo", customerPromotionalOfferDetailsDTO.getCusPromoOfferTrackingPaginationDTO());
				model.addAttribute("promoOfferTrackingList", customerPromotionalOfferDetailsDTO.getCusPromoOfferTrackingDTO());
			}else{
				model.addAttribute("msg", customerPromotionalOfferDetailsDTO.getError().getDescription());
				model.addAttribute("status", customerPromotionalOfferDetailsDTO.getStatus());
			}
			/*JSONObject object = new JSONObject();
			object.put("status", 0);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferTrackingFilter(headerFilter); 
			List<RTFPromotionalOfferTracking> promoOfferTrackingList = DAORegistry.getQueryManagerDAO().getPromotionalOfferTracking(filter, pageNo);
			object.put("promoOfferTrackingPagingInfo", PaginationUtil.calculatePaginationParameters((promoOfferTrackingList!=null?promoOfferTrackingList.size():0), pageNo));
			object.put("promoOfferTrackingList", JsonWrapperUtil.getRTFPromotionalOfferTrackingArray(promoOfferTrackingList));
			object.put("status", 1);
			IOUtils.write(object.toString().getBytes(),response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping({"/GetCustomerPromotionalOrderSummary"})
	public String getCustomerPromotionalOrderSummary(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String promoOfferOrderId = request.getParameter("promoOfferOrderId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("promoOfferOrderId", promoOfferOrderId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_PROMOTIONAL_ORDER_SUMMARY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerPromotionalOrderSummaryDTO customerPromotionalOrderSummaryDTO = gson.fromJson(((JsonObject)jsonObject.get("customerPromotionalOrderSummaryDTO")), CustomerPromotionalOrderSummaryDTO.class);
			
			if(customerPromotionalOrderSummaryDTO.getStatus() == 1){
				model.addAttribute("msg", customerPromotionalOrderSummaryDTO.getMessage());
				model.addAttribute("status", customerPromotionalOrderSummaryDTO.getStatus());
				model.addAttribute("promotionalOfferHistory", customerPromotionalOrderSummaryDTO.getCusPromoOfferTrackingDetailsDTO());
			}else{
				model.addAttribute("msg", customerPromotionalOrderSummaryDTO.getError().getDescription());
				model.addAttribute("status", customerPromotionalOrderSummaryDTO.getStatus());
			}			
			/*//RTFCustomerPromotionalOffer customerPromOffer = null;
			RTFPromotionalOfferTracking promoOfferTracking = null;
			Customer customer = null;
			CustomerOrder customerOrder = null;
			State state = null;
			Integer countryId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			//String promoOfferId = request.getParameter("promoOfferId");
			String promoOfferOrderId = request.getParameter("promoOfferOrderId");
			
			if(promoOfferOrderId == null || promoOfferOrderId.isEmpty()){
				jObj.put("msg", "Promotional Offer Order ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
//			customerPromOffer = DAORegistry.getRtfCustomerPromotionalOfferDAO().get(Integer.parseInt(promoOfferId));
//			if(customerPromOffer != null){
//				promoOfferTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getCompletedPromoTracking(Integer.parseInt(promoOfferId));
//				customer = DAORegistry.getCustomerDAO().get(customerPromOffer.getCustomerId());
//				customerOrder = DAORegistry.getCustomerOrderDAO().get(promoOfferTracking.getOrderId());
//			}
			promoOfferTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getCompletedPromoTrackingByOrderId(Integer.parseInt(promoOfferOrderId));
			customer = DAORegistry.getCustomerDAO().get(promoOfferTracking.getCustomerId());
			customerOrder = DAORegistry.getCustomerOrderDAO().get(promoOfferTracking.getOrderId());
			
			if(promoOfferTracking != null){
		    	JSONObject historyObject = new JSONObject();
		    	historyObject.put("orderId", promoOfferTracking.getOrderId());
		    	historyObject.put("promoCode", promoOfferTracking.getPromoCode());
		    	historyObject.put("platform", promoOfferTracking.getPlatForm());
		    	historyObject.put("orderTotal", customerOrder!=null?String.format("%.2f", customerOrder.getOrderTotal()):0.00);
		    	historyObject.put("orderQuantity", customerOrder!=null?customerOrder.getQty():0);
		    	historyObject.put("eventName", customerOrder!=null?customerOrder.getEventName():"");
		    	historyObject.put("eventDate", customerOrder!=null?customerOrder.getEventDateStr():"");
		    	historyObject.put("eventTime", customerOrder!=null?customerOrder.getEventTimeStr():"");
		    	historyObject.put("venue", customerOrder!=null?customerOrder.getVenueName():"");
		    	historyObject.put("venueCity", customerOrder!=null?customerOrder.getVenueCity():"");
		    	if(customerOrder != null && customerOrder.getVenueState() != null){
		    		countryId = DAORegistry.getQueryManagerDAO().getCountryByShortDesc(customerOrder.getVenueCountry());
		    		state = DAORegistry.getStateDAO().getStateByShortDesc(customerOrder.getVenueState(), countryId);
		    		historyObject.put("venueState", state.getName());
		    	}else{
		    		historyObject.put("venueState", "");
		    	}
		    	
		    	historyObject.put("firstName", customer!=null?customer.getCustomerName():"");
		    	historyObject.put("lastName", customer!=null?customer.getLastName():"");
		    	historyObject.put("email", customer!=null?customer.getEmail():"");
		    	historyObject.put("referrerCode", customer!=null?customer.getReferrerCode():"");
		    	historyObject.put("signUpDate", customer!=null?customer.getSignupDateStr():"");
		    	jObj.put("promotionalOfferHistory", historyObject);
		    }
			
		    jObj.put("status", 1);
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	

	/*@RequestMapping({"/CustomerPromoOfferTrackingExportToExcel"})
	public void customerPromoOfferToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFCustomerPromotionalOfferFilter(headerFilter); 
			List<RTFCustomerPromotionalOffer> promoOfferList = DAORegistry.getQueryManagerDAO().getCustomerPromotionalOfferToExport(filter);
			
			if(promoOfferList!=null && !promoOfferList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Customer_Promotional_Offer");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Offer Id");
				header.createCell(1).setCellValue("Customer First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Email");
				header.createCell(4).setCellValue("Promotional Code");
				header.createCell(5).setCellValue("Discount");
				header.createCell(6).setCellValue("Start Date");
				header.createCell(7).setCellValue("End Date");
				header.createCell(8).setCellValue("Status");
				header.createCell(9).setCellValue("Created Date");
				header.createCell(10).setCellValue("Last Modified Date");
				Integer i=1;
				for(RTFCustomerPromotionalOffer promoOffer : promoOfferList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getFirstName());
					row.createCell(2).setCellValue(promoOffer.getLastName()!=null?promoOffer.getLastName():"");
					row.createCell(3).setCellValue(promoOffer.getEmail()!=null?promoOffer.getEmail():"");
					row.createCell(4).setCellValue(promoOffer.getPromoCode()!=null?promoOffer.getPromoCode():"");
					row.createCell(5).setCellValue(promoOffer.getDiscountStr()!=null?promoOffer.getDiscountStr():"");
					row.createCell(6).setCellValue(promoOffer.getStartDateStr()!=null?promoOffer.getStartDateStr():"");
					row.createCell(7).setCellValue(promoOffer.getEndDateStr()!=null?promoOffer.getEndDateStr():"");
					row.createCell(8).setCellValue(promoOffer.getStatus()!=null?promoOffer.getStatus():"");
					row.createCell(9).setCellValue(promoOffer.getCreatedDateStr()!=null?promoOffer.getCreatedDateStr():"");
					row.createCell(10).setCellValue(promoOffer.getModifiedDateStr()!=null?promoOffer.getModifiedDateStr():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Customer_Promotional_Offer.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/PromoOfferTrackingExportToExcel"})
	public void promoOfferTrackingToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		String headerFilter = request.getParameter("headerFilter");
		Double sumOrderTotal = 0.00;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getRTFPromotionalOfferTrackingFilter(headerFilter); 
			List<RTFPromotionalOfferTracking> promoOfferTrackingList = DAORegistry.getQueryManagerDAO().getPromotionalOfferTrackingToExport(filter);
			
			if(promoOfferTrackingList!=null && !promoOfferTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Promotional_Offer_Tracking");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Promotional Tracking Id");
				header.createCell(1).setCellValue("Customer First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Email");
				header.createCell(4).setCellValue("Promotional Code");
				header.createCell(5).setCellValue("Discount");
				header.createCell(6).setCellValue("Order Id");
				header.createCell(7).setCellValue("Order Total");
				header.createCell(8).setCellValue("Quantity");
				header.createCell(9).setCellValue("Customer IP Address");
				header.createCell(10).setCellValue("Primary Payment Method");
				header.createCell(11).setCellValue("Secondary Payment Method");
				header.createCell(12).setCellValue("Third Payment Method");
				header.createCell(13).setCellValue("Platform");
				header.createCell(14).setCellValue("Order Creation Date");
				Integer i=1;
				for(RTFPromotionalOfferTracking promoOffer : promoOfferTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(promoOffer.getId());
					row.createCell(1).setCellValue(promoOffer.getFirstName());
					row.createCell(2).setCellValue(promoOffer.getLastName()!=null?promoOffer.getLastName():"");
					row.createCell(3).setCellValue(promoOffer.getEmail()!=null?promoOffer.getEmail():"");
					row.createCell(4).setCellValue(promoOffer.getPromoCode()!=null?promoOffer.getPromoCode():"");
					row.createCell(5).setCellValue(promoOffer.getDiscount()!=null?promoOffer.getDiscount():"");
					row.createCell(6).setCellValue(promoOffer.getOrderId()!=null?promoOffer.getOrderId():0);
					if(promoOffer.getOrderTotal()!=null){
						sumOrderTotal += promoOffer.getOrderTotal();
						row.createCell(7).setCellValue(promoOffer.getOrderTotal());
					}else{
						sumOrderTotal += 0.00;
						row.createCell(7).setCellValue(0.00);
					}					
					row.createCell(8).setCellValue(promoOffer.getQuantity()!=null?promoOffer.getQuantity():0);
					row.createCell(9).setCellValue(promoOffer.getIpAddress()!=null?promoOffer.getIpAddress():"");
					if(promoOffer.getPrimaryPaymentMethod() == null || promoOffer.getPrimaryPaymentMethod().equalsIgnoreCase("NULL")){
						row.createCell(10).setCellValue("");
					}else{
						row.createCell(10).setCellValue(promoOffer.getPrimaryPaymentMethod());
					}
					if(promoOffer.getSecondaryPaymentMethod() == null || promoOffer.getSecondaryPaymentMethod().equalsIgnoreCase("NULL")){
						row.createCell(11).setCellValue("");
					}else{
						row.createCell(11).setCellValue(promoOffer.getSecondaryPaymentMethod());
					}
					if(promoOffer.getThirdPaymentMethod() == null || promoOffer.getThirdPaymentMethod().equalsIgnoreCase("NULL")){
						row.createCell(12).setCellValue("");
					}else{
						row.createCell(12).setCellValue(promoOffer.getThirdPaymentMethod());
					}
					row.createCell(13).setCellValue(promoOffer.getPlatForm()!=null?promoOffer.getPlatForm().toString():"");
					row.createCell(14).setCellValue(promoOffer.getCreatedDateStr()!=null?promoOffer.getCreatedDateStr():"");
					i++;
				}
				if(promoOfferTrackingList != null && promoOfferTrackingList.size() > 0){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue("");
					row.createCell(1).setCellValue("");
					row.createCell(2).setCellValue("");
					row.createCell(3).setCellValue("");
					row.createCell(4).setCellValue("");
					row.createCell(5).setCellValue("");
					row.createCell(6).setCellValue("");
					row.createCell(7).setCellValue(sumOrderTotal);										
					row.createCell(8).setCellValue("");
					row.createCell(9).setCellValue("");
					row.createCell(10).setCellValue("");
					row.createCell(11).setCellValue("");
					row.createCell(12).setCellValue("");
					row.createCell(13).setCellValue("");
					row.createCell(14).setCellValue("");
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Promotional_Offer_Tracking.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	@RequestMapping({"/GetFavouriteEventsForCustomer"})
	public String getFavouriteEventsForCustomer(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			//JSONObject returnObject = new JSONObject();
			String customerIdStr = request.getParameter("customerId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO_FOR_FAVOURITE_EVENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoFavouriteEventDTO customerInfoFavEventDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoFavouriteEventDTO")), CustomerInfoFavouriteEventDTO.class);
			
			if(customerInfoFavEventDTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoFavEventDTO.getMessage());
				model.addAttribute("status", customerInfoFavEventDTO.getStatus());
				model.addAttribute("events", customerInfoFavEventDTO.getCustomerFavouriteEventDTO());
				model.addAttribute("pagingInfo", customerInfoFavEventDTO.getFavouriteEventPaginationDTO());
			}else{
				model.addAttribute("msg", customerInfoFavEventDTO.getError().getDescription());
				model.addAttribute("status", customerInfoFavEventDTO.getStatus());
			}
			
			/*Integer eventCount = 0;
			Collection<EventDetails> events = null;	
			returnObject.put("status", 0);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getEventSearchHeaderFilters(headerFilter);
			
			events = DAORegistry.getQueryManagerDAO().getAllActiveFavouriteEventDetailsByFilter(Integer.parseInt(customerIdStr),filter,pageNo);
			if(events != null && events.size() > 0){
				eventCount = events.size();
				returnObject.put("status", 1);
			}else{
				returnObject.put("msg", "No Favourite Events found for Selected Customer.");
			}
			
			returnObject.put("events", JsonWrapperUtil.getEventArray(events));
			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(eventCount, pageNo));
			IOUtils.write(returnObject.toString(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	@RequestMapping({"/AddWalletForCustomer"})
	public String addWalletForCustomer(HttpServletRequest request, HttpServletResponse response, Model model){
		try{
			String customerIdStr = request.getParameter("customerId");
			String customerWalletAmount = request.getParameter("customerWalletAmount");
			String transactionType = request.getParameter("transactionType");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("customerWalletAmount", customerWalletAmount);
			map.put("transactionType", transactionType);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_ADD_WALLET_FOR_CUSTOMER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerAddWalletDTO customerAddWalletDTO = gson.fromJson(((JsonObject)jsonObject.get("customerAddWalletDTO")), CustomerAddWalletDTO.class);
			
			if(customerAddWalletDTO.getStatus() == 1){				
				model.addAttribute("msg", customerAddWalletDTO.getMessage());
				model.addAttribute("status", customerAddWalletDTO.getStatus());
				model.addAttribute("creditInfo", customerAddWalletDTO.getCustomerWalletDTO());
			}else{
				model.addAttribute("msg", customerAddWalletDTO.getError().getDescription());
				model.addAttribute("status", customerAddWalletDTO.getStatus());
			}
			
			/*JSONObject returnObject = new JSONObject();
			CustomerWallet customerWallet = null;
			Double customerWalletAmountDouble = 0.0;
			Double activeCreditAmount = 0.0;
			Double totalCreditAmount = 0.0;
			Double activeCredit = 0.0;
			Double totalCredit = 0.0;
			String msg = "";
			returnObject.put("status", 0);
			
			if(customerId == null || customerId.isEmpty()){
				msg = "Not able to identify Customer Id.";
			}
			
			if(customerWalletAmount == null || customerWalletAmount.isEmpty()){
				msg = "Not able to identify Wallet Amount.";
			}else{			
				try{
					customerWalletAmountDouble = Double.parseDouble(customerWalletAmount);
				}catch(Exception e){
					e.printStackTrace();
					msg = "Bad value found for Wallet Amount, It should be valid Double value.";
				}
			}
			
			if(transactionType == null || transactionType.isEmpty()){
				msg = "Not able to identify Transaction Type.";
			}
						
			if(customerId!=null && !customerId.isEmpty() && customerWalletAmount!=null && !customerWalletAmount.isEmpty()){
				customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(Integer.parseInt(customerId));
				if(customerWallet != null){
					activeCredit = customerWallet.getActiveCredit();
					totalCredit = customerWallet.getTotalCredit();
				}else{
					customerWallet = new CustomerWallet();
					customerWallet.setPendingDebit(0.00);
					customerWallet.setTotalUsedCredit(0.00);
				}
				
				Customer customer  = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
				if(customer == null){
					msg = "Customer information not found.";
				}
				if(transactionType.equalsIgnoreCase("DEBIT")){
					if(activeCredit <= 0){
						msg = "Could not update Customer Wallet, Active Credit is 0.";
					}
					if(activeCredit > 0 && activeCredit < customerWalletAmountDouble){
						msg = "Could not update Customer Wallet, Active Credit less than Specified amount.";
					}
					if(totalCredit > 0 && totalCredit < customerWalletAmountDouble){
						msg = "Could not update Customer Wallet, Total Credit less than Specified amount.";
					}
				}
				if(msg.isEmpty()){
					if(transactionType.equalsIgnoreCase("CREDIT")){
						activeCreditAmount = activeCredit + customerWalletAmountDouble;
						totalCreditAmount = totalCredit + customerWalletAmountDouble;
						customerWallet.setActiveCredit(activeCreditAmount);
						customerWallet.setTotalCredit(totalCreditAmount);
					}
					if(transactionType.equalsIgnoreCase("DEBIT")){
						if(activeCredit > 0){
							activeCreditAmount = activeCredit - customerWalletAmountDouble;
						}
						if(totalCredit > 0){
							totalCreditAmount = totalCredit - customerWalletAmountDouble;
						}
						customerWallet.setActiveCredit(activeCreditAmount);
						customerWallet.setTotalCredit(totalCreditAmount);
					}
										
					customerWallet.setCustomerId(customer.getId());
					customerWallet.setLastUpdated(new Date());
					DAORegistry.getCustomerWalletDAO().saveOrUpdate(customerWallet);
					
					CustomerWalletTracking walletTracking = new CustomerWalletTracking();
					walletTracking.setCustomerId(customer.getId());
					walletTracking.setTransactionType(transactionType);
					walletTracking.setTransactionAmount(customerWalletAmountDouble);
					walletTracking.setCreatedDate(new Date());
					walletTracking.setCreatedBy(userName);
					DAORegistry.getCustomerWalletTrackingDAO().save(walletTracking);
					
					msg = "Customer Wallet Amount updated.";
					returnObject.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Customer Wallet Amount "+transactionType+"ED.";
					Util.userActionAudit(request, customer.getId(), userActionMsg);
				}
			}
			
			JSONObject creditObj = new JSONObject();
			CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(Integer.parseInt(customerId));			
			if(wallet!=null){
				creditObj.put("activeCredit", String.format("%.2f", wallet.getActiveCredit()));
				creditObj.put("totalEarnedCredit", String.format("%.2f", wallet.getTotalCredit()));
				creditObj.put("totalUsedCredit", String.format("%.2f", wallet.getTotalUsedCredit()));
			}else{
				creditObj.put("activeCredit", String.format("%.2f", 0.00));
				creditObj.put("totalEarnedCredit", String.format("%.2f", 0.00));
				creditObj.put("totalUsedCredit", String.format("%.2f", 0.00));				
			}
			returnObject.put("creditInfo", creditObj);
			
			returnObject.put("msg", msg);
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	
	@RequestMapping({"/TrackSharingDiscountCode"})
	public String loadTrackSharingDiscountCode(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String fromTime = request.getParameter("fromTime");
			String toTime = request.getParameter("toTime");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("fromTime", fromTime);
			map.put("toTime", toTime);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_TRACK_SHARING_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SharingDiscountCodeDTO sharingDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("sharingDiscountCodeDTO")), SharingDiscountCodeDTO.class);
			
			if(sharingDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", sharingDiscountCodeDTO.getMessage());
				model.addAttribute("status", sharingDiscountCodeDTO.getStatus());
			}else{
				model.addAttribute("msg", sharingDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", sharingDiscountCodeDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", sharingDiscountCodeDTO.getShDiscountCodePaginationDTO());
				model.addAttribute("discountCodeTrackingList", sharingDiscountCodeDTO.getDiscountCodeTrackingDTO());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(sharingDiscountCodeDTO.getShDiscountCodePaginationDTO()));
				model.addAttribute("discountCodeTrackingList", gson.toJson(sharingDiscountCodeDTO.getDiscountCodeTrackingDTO()));
			}
			
			/*GridHeaderFilters filter = new GridHeaderFilters();
			Integer count = 0;			
			List<DiscountCodeTracking> discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTracking(null, null, filter, null);
			count = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingCount(null, null, filter);
			
			if(discountCodeTrackingList == null || discountCodeTrackingList.size() <= 0){
				map.put("msg", "No Sharing Discount Code - Tracking Record found.");
			}			
			
			map.put("discountCodeTrackingList", JsonWrapperUtil.getDiscountCodeTrackingArray(discountCodeTrackingList));
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-track-sharing-discount-code";
	}
	
	/*@RequestMapping({"/GetTrackSharingDiscountCode"})
	public String getTrackSharingDiscountCode(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String fromTime = request.getParameter("fromTime");
			String toTime = request.getParameter("toTime");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("fromTime", fromTime);
			map.put("toTime", toTime);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_TRACK_SHARING_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SharingDiscountCodeDTO sharingDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("sharingDiscountCodeDTO")), SharingDiscountCodeDTO.class);
			
			if(sharingDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", sharingDiscountCodeDTO.getMessage());
				model.addAttribute("status", sharingDiscountCodeDTO.getStatus());
				model.addAttribute("pagingInfo", sharingDiscountCodeDTO.getShDiscountCodePaginationDTO());
				model.addAttribute("discountCodeTrackingList", sharingDiscountCodeDTO.getDiscountCodeTrackingDTO());
			}else{
				model.addAttribute("msg", sharingDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", sharingDiscountCodeDTO.getStatus());
			}
			
//			JSONObject returnObject = new JSONObject();			
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			Integer count = 0;
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getDiscountCodeTrackingFilter(headerFilter);
//			List<DiscountCodeTracking> discountCodeTrackingList = null;
//			
//			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//				fromDateFinal = fromDateStr + " "+fromTimeStr+":00:00";
//				toDateFinal = toDateStr + " "+toTimeStr+":59:59";
//				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTracking(fromDateFinal, toDateFinal, filter, null);
//				count = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingCount(fromDateFinal, toDateFinal, filter);
//			}else{
//				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTracking(null, null, filter, null);
//				count = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingCount(null, null, filter);
//			}
//						
//			if(discountCodeTrackingList == null || discountCodeTrackingList.size() <= 0){
//				returnObject.put("msg", "No Sharing Discount Code - Tracking Record found.");
//			}
//			returnObject.put("discountCodeTrackingList", JsonWrapperUtil.getDiscountCodeTrackingArray(discountCodeTrackingList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}
		return "";
	}*/
	

	/*@RequestMapping({"/TrackSharingDiscountCodeExportToExcel"})
	public void manualFedexToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String fromTimeStr = request.getParameter("fromTime");
		String toTimeStr = request.getParameter("toTime");
		String headerFilter = request.getParameter("headerFilter");
		List<DiscountCodeTracking> discountCodeTrackingList = null;
		try{
			String fromDateFinal = "";
			String toDateFinal = "";
			GridHeaderFilters filter = GridHeaderFiltersUtil.getDiscountCodeTrackingFilter(headerFilter);
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " "+fromTimeStr+":00:00";
				toDateFinal = toDateStr + " "+toTimeStr+":59:59";
				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingToExport(fromDateFinal, toDateFinal, filter);
			}else{
				discountCodeTrackingList = DAORegistry.getQueryManagerDAO().getDiscountCodeTrackingToExport(null, null, filter);
			}
			
			if(discountCodeTrackingList!=null && !discountCodeTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Track_Discount_Code_Sharing");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Customer Id");				
				header.createCell(1).setCellValue("First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Email");
				header.createCell(4).setCellValue("Referrer Code");
				header.createCell(5).setCellValue("Facebook");
				header.createCell(6).setCellValue("Twitter");
				header.createCell(7).setCellValue("LinkedIn");
				header.createCell(8).setCellValue("WhatsApp");
				header.createCell(9).setCellValue("Android");
				header.createCell(10).setCellValue("IMessage");
				header.createCell(11).setCellValue("Google");
				header.createCell(12).setCellValue("Outlook");
				Integer i=1;
				for(DiscountCodeTracking discountCodeTrack : discountCodeTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(discountCodeTrack.getCustomerId());
					row.createCell(1).setCellValue(discountCodeTrack.getFirstName()!=null?discountCodeTrack.getFirstName():"");
					row.createCell(2).setCellValue(discountCodeTrack.getLastName()!=null?discountCodeTrack.getLastName():"");
					row.createCell(3).setCellValue(discountCodeTrack.getEmail()!=null?discountCodeTrack.getEmail():"");
					row.createCell(4).setCellValue(discountCodeTrack.getReferrerCode()!=null?discountCodeTrack.getReferrerCode():"");
					row.createCell(5).setCellValue(discountCodeTrack.getFaceBookCnt()!=null?discountCodeTrack.getFaceBookCnt():0);
					row.createCell(6).setCellValue(discountCodeTrack.getTwitterCnt()!=null?discountCodeTrack.getTwitterCnt():0);
					row.createCell(7).setCellValue(discountCodeTrack.getLinkedinCnt()!=null?discountCodeTrack.getLinkedinCnt():0);
					row.createCell(8).setCellValue(discountCodeTrack.getWhatsappCnt()!=null?discountCodeTrack.getWhatsappCnt():0);
					row.createCell(9).setCellValue(discountCodeTrack.getAndroidCnt()!=null?discountCodeTrack.getAndroidCnt():0);
					row.createCell(10).setCellValue(discountCodeTrack.getImessageCnt()!=null?discountCodeTrack.getImessageCnt():0);
					row.createCell(11).setCellValue(discountCodeTrack.getGoogleCnt()!=null?discountCodeTrack.getGoogleCnt():0);
					row.createCell(12).setCellValue(discountCodeTrack.getOutlookCnt()!=null?discountCodeTrack.getOutlookCnt():0);
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Track_Discount_Code_Sharing.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/GetArtistsForCustomer"})
	public String getArtistsForCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerIdStr);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_INFO_FOR_LOYAL_FAN);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerInfoLoyalFanDTO customerInfoLoyalFanDTO = gson.fromJson(((JsonObject)jsonObject.get("customerInfoLoyalFanDTO")), CustomerInfoLoyalFanDTO.class);
			
			if(customerInfoLoyalFanDTO.getStatus() == 1){				
				model.addAttribute("msg", customerInfoLoyalFanDTO.getMessage());
				model.addAttribute("status", customerInfoLoyalFanDTO.getStatus());
				model.addAttribute("loyalFanInfo", customerInfoLoyalFanDTO.getLoyalFanDTO());
				model.addAttribute("artistList", customerInfoLoyalFanDTO.getPopularArtistCustomDTO());
				model.addAttribute("artistPagingInfo", customerInfoLoyalFanDTO.getPopularArtistPaginationDTO());
			}else{
				model.addAttribute("msg", customerInfoLoyalFanDTO.getError().getDescription());
				model.addAttribute("status", customerInfoLoyalFanDTO.getStatus());
			}
			
			/*Integer count = 0;
			JSONObject returnObject = new JSONObject();
			System.out.println("headerFilters   :   "+headerFilter);
			GridHeaderFilters filter = GridHeaderFiltersUtil.getManageArtistSearchHeaderFilters(headerFilter);
			List<PopularArtist> artistList = DAORegistry.getQueryManagerDAO().getAllArtist(filter, pageNo);
			count = DAORegistry.getQueryManagerDAO().getAllArtistCount(filter);
						  
			if(customerIdStr != null && !customerIdStr.isEmpty()){
				Integer customerId = Integer.parseInt(customerIdStr);

				Map<String, String> map = Util.getParameterMap(request);
			    map.put("customerId", customerIdStr);
			    map.put("productType", Constants.PRODUCTTYPE);
			    map.put("platForm", Constants.PLATFORM);
			    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.GET_LOYALFAN_STATUS);
			    Gson gson = new Gson();  
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				LoyalFanStatus loyalFans = gson.fromJson(((JsonObject)jsonObject.get("loyalFanStatus")), LoyalFanStatus.class);
				
				CustomerLoyalFan loyalFans = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
				JSONObject loyalFanObj = new JSONObject();				
				if(loyalFans != null){
					if(loyalFans.getLoyalFanType() != null && loyalFans.getLoyalFanType().equalsIgnoreCase("SPORTS")){
						loyalFanObj.put("artistName", loyalFans.getLoyalName());
					}else{
						loyalFanObj.put("cityName", loyalFans.getLoyalName()!=null?loyalFans.getLoyalName()+", "+loyalFans.getCountry():"");
					}
					loyalFanObj.put("categoryName", loyalFans.getLoyalFanType()!=null?loyalFans.getLoyalFanType():"");
					if(loyalFans.getTicketPurchased() != null && loyalFans.getTicketPurchased()){
						loyalFanObj.put("ticketsPurchased", "Yes");
					}else{
						loyalFanObj.put("ticketsPurchased", "No");
					}
					loyalFanObj.put("startDate", loyalFans.getStartDateStr()!=null?loyalFans.getStartDateStr():"");
					loyalFanObj.put("endDate", loyalFans.getEndDateStr()!=null?loyalFans.getEndDateStr():"");
				}
				returnObject.put("loyalFanInfo", loyalFanObj);
			}
			
			returnObject.put("artistList", JsonWrapperUtil.getAllArtistArray(artistList));
			returnObject.put("artistPagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
			response.setHeader("Content-type","application/json");
			IOUtils.write(returnObject.toString().getBytes(),response.getOutputStream());*/
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "page-client-manage-details";
	}
	
	
	
	
	@RequestMapping({"/BlockUnblockCustomer"})
	public String blockUnblockCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String customerIdStr = request.getParameter("customerId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			if(customerIdStr == null || customerIdStr.isEmpty()){
				model.addAttribute("msg", "Customer id not found.");
				return "";
			}
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("cuId", customerIdStr);
			map.put("userName", userName);
			String data1 = Util.getObject(map,Constants.BASE_URL+Constants.UNBLOCK_CUSTOMER);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			CommonRespInfo resp = gson1.fromJson(((JsonObject)jsonObject1.get("commonRespInfo")), CommonRespInfo.class);
			
			if(resp.getSts() == 1){				
				model.addAttribute("msg", resp.getMsg());
			}else{
				model.addAttribute("msg", resp.getErr().getDesc());
			}
			model.addAttribute("status", resp.getSts());
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
}