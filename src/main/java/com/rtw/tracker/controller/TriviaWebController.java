package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.pojos.FanClubDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
public class TriviaWebController {

	
	
	@RequestMapping(value = "/ManageOfflineTrivia")
	public String manageOfflineTrivia(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_OFFLINE_TRIVIA);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("clubs", respDTO.getClubs());
					model.addAttribute("categories", respDTO.getCategories());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("memberPagingInfo", respDTO.getDummyPagination());
				}else{
					model.addAttribute("clubs", gson.toJson(respDTO.getClubs()));
					model.addAttribute("categories", respDTO.getCategories());
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("memberPagingInfo",gson.toJson(respDTO.getDummyPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("clubs", respDTO.getClubs());
				model.addAttribute("categories", respDTO.getCategories());
				model.addAttribute("pagingInfo", respDTO.getPagination());
				model.addAttribute("memberPagingInfo", respDTO.getDummyPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-offline-trivia";
	}
	
	
	
	@RequestMapping(value = "/ManageOfflineTriviaQuestions")
	public String manageOfflineTriviaQuestions(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TFF_API_GET_OFFLINE_TRIVIA_QUESTION_BANK);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			FanClubDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("fanClubDTO")),FanClubDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("clubs", respDTO.getClubs());
					model.addAttribute("categories", respDTO.getCategories());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("memberPagingInfo", respDTO.getDummyPagination());
				}else{
					model.addAttribute("clubs", gson.toJson(respDTO.getClubs()));
					model.addAttribute("categories", respDTO.getCategories());
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("memberPagingInfo",gson.toJson(respDTO.getDummyPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("clubs", respDTO.getClubs());
				model.addAttribute("categories", respDTO.getCategories());
				model.addAttribute("pagingInfo", respDTO.getPagination());
				model.addAttribute("memberPagingInfo", respDTO.getDummyPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-offline-trivia-question-bank";
	}
	
	
	
	
}
