package com.rtw.tracker.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.datas.ContestQuestions;
import com.rtw.tracker.datas.Contests;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.firebase.FirebaseWebUtil;
import com.rtw.tracker.firebase.FirestoreUtil;
import com.rtw.tracker.pojos.AffiliateCashRewardHistoryDTO;
import com.rtw.tracker.pojos.AffiliateOrderNoteDTO;
import com.rtw.tracker.pojos.AffiliateOrderSummaryDTO;
import com.rtw.tracker.pojos.AffiliatePromoCodeDTO;
import com.rtw.tracker.pojos.AffiliatePromoCodeHistoryDTO;
import com.rtw.tracker.pojos.AffiliateReportDTO;
import com.rtw.tracker.pojos.AffiliatesDTO;
import com.rtw.tracker.pojos.AnswerCountInfo;
import com.rtw.tracker.pojos.AutoCompleteArtistAndCategoryDTO;
import com.rtw.tracker.pojos.AutoCompleteDTO;
import com.rtw.tracker.pojos.AutoCompleteEventsDTO;
import com.rtw.tracker.pojos.CassContestWinners;
import com.rtw.tracker.pojos.CassJoinContestInfo;
import com.rtw.tracker.pojos.CityStateCountryAutoSearchDTO;
import com.rtw.tracker.pojos.ContSummaryInfo;
import com.rtw.tracker.pojos.ContestAffiliateEarningDTO;
import com.rtw.tracker.pojos.ContestAffiliatesDTO;
import com.rtw.tracker.pojos.ContestChecklistDTO;
import com.rtw.tracker.pojos.ContestChecklistTrackingDTO;
import com.rtw.tracker.pojos.ContestConfigSettingsDTO;
import com.rtw.tracker.pojos.ContestEventsDTO;
import com.rtw.tracker.pojos.ContestPromocodeDTO;
import com.rtw.tracker.pojos.ContestQuestionDTO;
import com.rtw.tracker.pojos.ContestWinnerDTO;
import com.rtw.tracker.pojos.ContestsDTO;
import com.rtw.tracker.pojos.CustomerProfileQuestionDTO;
import com.rtw.tracker.pojos.CustomerQuestionDTO;
import com.rtw.tracker.pojos.CustomerStats;
import com.rtw.tracker.pojos.FantasyCategoryTeamDTO;
import com.rtw.tracker.pojos.FantasyChildCategoryDTO;
import com.rtw.tracker.pojos.FantasyEventsFromGrandChildDTO;
import com.rtw.tracker.pojos.FantasyGrandChildCategoryDTO;
import com.rtw.tracker.pojos.FantasyTicketValidationDTO;
import com.rtw.tracker.pojos.GenerateDiscountCodeDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.GiftCardOrdersDTO;
import com.rtw.tracker.pojos.GiftCardOrdersEditDTO;
import com.rtw.tracker.pojos.GlobalBrokerDTO;
import com.rtw.tracker.pojos.LoadStateDTO;
import com.rtw.tracker.pojos.PopularArtistDTO;
import com.rtw.tracker.pojos.PopularArtistsCountDTO;
import com.rtw.tracker.pojos.QuestionBankDTO;
import com.rtw.tracker.pojos.QuizContestSummaryInfo;
import com.rtw.tracker.pojos.ReferralContestDTO;
import com.rtw.tracker.pojos.ReferralContestParticipantsDTO;
import com.rtw.tracker.pojos.ReferralContestWinnerDTO;
import com.rtw.tracker.pojos.RtfCatsEventsDTO;
import com.rtw.tracker.pojos.RtfGiftCardsDTO;
import com.rtw.tracker.pojos.SavedCardsRewardPointsAndLoyalFanDTO;
import com.rtw.tracker.pojos.ShippingAddressDTO;
import com.rtw.tracker.pojos.UserPreferenceDTO;
import com.rtw.tracker.utils.ComputeGrandWinnerThread;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
public class AjaxController {	
	/**
	 * Testing SVN Mitul
	 */
	
	private static Logger contestLog = LoggerFactory.getLogger(AjaxController.class);
	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
//	private MailManager mailManager;
//	public MailManager getMailManager() {
//		return mailManager;
//	}
//
//	public void setMailManager(MailManager mailManager) {
//		this.mailManager = mailManager;
//	}
	
	/*@RequestMapping("/AutoCompleteArtist")
	public void getAutoCompleteGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");
//		String brokerId = request.getParameter("brokerId");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
//		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
//				param);
//		Collection<GrandChildCategory> grandChildCategories = DAORegistry
//				.getGrandChildCategoryDAO().getGrandChildCategoriesByName(
//						param);
//		Collection<ChildCategory> childCategories = DAORegistry
//				.getChildCategoryDAO().getChildTourCategoriesByName(param);

//		if (artists == null && venues == null && grandChildCategories == null) {
		if (artists == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}
//		if (venues != null) {
//			for (Venue venue : venues) {
//				strResponse += ("VENUE" + "|" + venue.getId() + "|" + venue.getBuilding() + "\n");
//			}
//		}
//		if (grandChildCategories != null) {
//			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
//				strResponse += ("GRANDCHILD" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
//			}
//		}
//
//		if (childCategories != null) {
//			for (ChildCategory childCategory : childCategories) {
//				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
//			}
//		}
		
		writer.write(strResponse);
	}*/
	
	@RequestMapping("/AutoCompleteEvents")
	public void getAutoCompleteEvents(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteEventsDTO autoCompleteEventsDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteEventsDTO")), AutoCompleteEventsDTO.class);
						
			if(autoCompleteEventsDTO.getStatus() == 1){
				writer.write(autoCompleteEventsDTO.getEvents());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/*@RequestMapping("/AutoCompleteChildAndGrandChild")
	public void getAutoCompleteChildAndGrandChild(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
		Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);

		if (childCategories == null && grandChildCategories == null) {
			return;
		}
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}
		
		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		
		writer.write(strResponse);
	}*/
	
	
	@RequestMapping("/AutoCompleteFantasyCategoryTeams")
	public void getAutoCompleteFantasyCategoryTeams(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		try{
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_FANTASY_CATEGORY_TEAMS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteDTO autoCompleteDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteDTO")), AutoCompleteDTO.class);
						
			if(autoCompleteDTO.getStatus() == 1){
				writer.write(autoCompleteDTO.getAutoCompleteStringResponse());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		/*String param = request.getParameter("q");
		String grandChildStr = request.getParameter("grandChildId");
		List<CrownJewelCategoryTeams> teams = null;
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		if(grandChildStr!=null && !grandChildStr.isEmpty()){
			teams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByGrandChildandName(Integer.parseInt(grandChildStr), param);
		}else{
			teams = DAORegistry.getCrownJewelCategoryTeamsDAO().getCategoryTeamsByName(param);
		}
		
		if (teams == null) {
			return;
		}
		if (teams != null) {
			for (CrownJewelCategoryTeams team : teams) {
				strResponse += ("TEAM" + "|" + team.getId() + "|" + team.getName() + "\n") ;
			}
		}
		writer.write(strResponse);*/
	}
	
	/*@RequestMapping("/GetFantasyChildCategories")
	public void getFantasyChildCategories(HttpServletRequest request, HttpServletResponse response)throws Exception {
		String parentCategoryName = request.getParameter("parentCategoryName");
		List<FantasyChildCategory> childCategories = null;
		JSONArray array = new JSONArray();
		try {
			if(parentCategoryName!=null && !parentCategoryName.isEmpty()){
				FantasyParentCategory parentCategory = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryByName(parentCategoryName);
				if(parentCategory!=null){
					childCategories = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryByParentId(parentCategory.getId());
					JSONObject object = null;
					for(FantasyChildCategory child :childCategories){
						object = new JSONObject();
						object.put("name",child.getName());
						object.put("id", child.getId());
						array.put(object);
					}
				}
			}
			IOUtils.write(array.toString().getBytes(),response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping("/GetFantasyGrandChildCategories")
	public String getFantasyGrandChildCategories(HttpServletRequest request, HttpServletResponse response, Model model)throws Exception {
		
		try {
			String customerId = request.getParameter("customerId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_FANTASY_GRANDCHILD_CATEGORIES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyGrandChildCategoryDTO fantasyGrandChildCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyGrandChildCategoryDTO")), FantasyGrandChildCategoryDTO.class);
						
 			if(fantasyGrandChildCategoryDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyGrandChildCategoryDTO.getMessage()!=null?fantasyGrandChildCategoryDTO.getMessage():"");
				model.addAttribute("status", fantasyGrandChildCategoryDTO.getStatus());
				model.addAttribute("fantasyCategoryList", fantasyGrandChildCategoryDTO.getFanasyCategoryDTO());
			}else{
				model.addAttribute("msg", fantasyGrandChildCategoryDTO.getError().getDescription());
				model.addAttribute("status", fantasyGrandChildCategoryDTO.getStatus());
			}
			
//			String childCategoryId = request.getParameter("childCategoryId");
//			List<FantasyGrandChildCategory> grandChildCategories = null;
//			if(childCategoryId!=null && !childCategoryId.isEmpty()){
//				grandChildCategories = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childCategoryId));
//				JSONObject object = null;
//				for(FantasyGrandChildCategory grandChild :grandChildCategories){
//					object = new JSONObject();
//					object.put("name",grandChild.getName());
//					object.put("id", grandChild.getId());
//					array.put(object);
//				}
//			}
			
 			/*List<FantasyCategories> fantasyCategoryList = null;
			JSONArray array = new JSONArray();
			Map<String, String> map = Util.getParameterMap(request);			
		    map.put("customerId", customerIdStr);
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.GET_FANTASY_PRODUCT_CATEGORY);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProductList = gson.fromJson(((JsonObject)jsonObject.get("fantasySportsProduct")), FantasySportsProduct.class);
			
			if(fantasySportsProductList.getStatus() == 1){
				fantasyCategoryList = fantasySportsProductList.getFantasyCategories();
				JSONObject object = null;
				for(FantasyCategories grandChild :fantasyCategoryList){
					object = new JSONObject();
					object.put("name",grandChild.getName());
					object.put("id", grandChild.getId());
					object.put("packageInfo", grandChild.getPackageInformation());
					array.put(object);
				}
			}
			
			IOUtils.write(array.toString().getBytes(),response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.. Please try again.");
		}
		return "";
	}
	
	/*@RequestMapping("/AutoCompleteParentAndChild")
	public void getAutoCompleteParentAndChild(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		
		Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByName(param);
		Collection<ParentCategory> parentCategories = DAORegistry.getParentCategoryDAO().getParentCategoriesByName(param);

		if (childCategories == null && parentCategories == null) {
			return;
		}
		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		if (parentCategories != null) {
			for (ParentCategory parentCategory : parentCategories) {
				strResponse += ("PARENT" + "|" + parentCategory.getId() + "|" + parentCategory.getName() + "\n");
			}
		}
		
		writer.write(strResponse);
	}*/
	
	/*@RequestMapping(value="/GetArtistsByChildAndGrandChild")
	public void getArtistsByChildAndGrandChild(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			String grandChildId = request.getParameter("grandChildId");
			String childId = request.getParameter("childId");

			List<Artist> artists = null;
			if (childId != null) {
				artists = DAORegistry.getQueryManagerDAO().getAllActiveByGrandchildOrChild(null,Integer.parseInt(childId));
			}
			if (grandChildId != null) {
				artists = DAORegistry.getQueryManagerDAO().getAllActiveByGrandchildOrChild(Integer.parseInt(grandChildId),null);
			}
			JSONObject jObj = null;
			
			if (artists != null) {
				for (Artist artist : artists) {
					jObj = new JSONObject();
					jObj.put("id", artist.getId());
					jObj.put("name", artist.getName());
					jsonArr.put(jObj);
				}
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping(value="/GetGrandChildByParentAndChild")
	public void getGetGrandChildByParentAndChildA(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			String parentId = request.getParameter("parentId");
			String childId = request.getParameter("childId");

			List<GrandChildCategory> grandChilds = null;
			if (childId != null) {
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByChildCategory(Integer.parseInt(childId));
			}
			if (parentId != null) {
				grandChilds = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByParentCategory(Integer.parseInt(parentId));
			}
			JSONObject jObj = null;
			
			if (grandChilds != null) {
				for (GrandChildCategory grandChild : grandChilds) {
					jObj = new JSONObject();
					jObj.put("id", grandChild.getId());
					jObj.put("name", grandChild.getName());
					jsonArr.put(jObj);
				}
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping(value="/GetChildCategoriesByParentCategory")
	public void getChildCategoriesByParentCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			String parentId = request.getParameter("parentId");

			List<ChildCategory> childs = null;
			if (parentId != null) {
				childs = DAORegistry.getChildCategoryDAO().getAllChildCategoryByParentCategory(Integer.parseInt(parentId));
			}
			JSONObject jObj = null;
			
			if (childs != null) {
				for (ChildCategory child : childs) {
					jObj = new JSONObject();
					jObj.put("id", child.getId());
					jObj.put("name", child.getName());
					jsonArr.put(jObj);
				}
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping(value="/GetEventsByArtistAndCity")
	public void getEventsByArtistAndCity(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			Integer artistId = Integer.parseInt(request.getParameter("artistId"));
			String city = request.getParameter("city");
			List<EventDetails> events = DAORegistry.getEventDetailsDAO().getEventsByArtistCity(artistId, city);
			JSONObject jObj = null;
			
			for (EventDetails eventDtls : events) {
				jObj = new JSONObject();
				jObj.put("id", eventDtls.getEventId());
				jObj.put("nameWithDateandVenue", eventDtls.getNameWithDateandVenue());
				jsonArr.put(jObj);
			}
			IOUtils.write(jsonArr.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Popular Events Count
	 * @param productId
	 */
	/*@RequestMapping(value = "/GetPopularEventsCount")
	public @ResponseBody String getPopularEventsCount(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsCountByProductId(Integer.parseInt(productId));
				msg= ""+count;
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}*/
	
	/**
	 * Get Popular Artists Count
	 * @param productId
	 */
	@RequestMapping(value = "/GetPopularArtistsCount")
	public String getPopularArtistsCount(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_POPULAR_ARTISTS_COUNT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PopularArtistsCountDTO popularArtistsCountDTO = gson.fromJson(((JsonObject)jsonObject.get("popularArtistsCountDTO")), PopularArtistsCountDTO.class);
						
			if(popularArtistsCountDTO.getStatus() == 1){
				model.addAttribute("msg", popularArtistsCountDTO.getMessage());
				model.addAttribute("status", popularArtistsCountDTO.getStatus());
				model.addAttribute("count", popularArtistsCountDTO.getCount());			
			}else{
				model.addAttribute("msg", popularArtistsCountDTO.getError().getDescription());
				model.addAttribute("status", popularArtistsCountDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Get Popular Grand Childs Count
	 * @param productId
	 */
	/*@RequestMapping(value = "/GetPopularGrandChildsCount")
	public @ResponseBody String getPopularGrandChildsCount(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryCountByProductId(Integer.parseInt(productId));
				msg= ""+count;
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}*/
	
	/**
	 * Get Popular Venues Count
	 * @param productId
	 */
	/*@RequestMapping(value = "/GetPopularVenuesCount")
	public @ResponseBody String getPopularVenuesCount(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String productId = request.getParameter("productId");
				Integer count = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueCountByProductId(Integer.parseInt(productId));
				msg= ""+count;
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}*/
	
	/**
	 * Updates Event Notes
	 * @param eventId
	 */
	@RequestMapping(value = "/UpdateTicketGroupNotes")
	public String updateTicketGroupNotes(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {			
			String ticketId = request.getParameter("ticketId").trim();
			String editColumn = request.getParameter("editColumn").trim();
			String value = request.getParameter("value").trim();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("ticketId", ticketId);
			map.put("editColumn", editColumn);
			map.put("value", value);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_TICKET_GROUP_NOTES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
			/*String msg="";
			if(ticketId!=null && !ticketId.isEmpty() && editColumn!=null && !editColumn.isEmpty()){
				notes = notes.replace("\n", " ");
				notes = notes.replace("\t", " ");
				notes = notes.replace("\r", " ");
				DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicketGroupFields(Integer.parseInt(ticketId),editColumn,notes);
				msg= editColumn+"saved successfully.";
			}else{
				msg="No Data found to save, Please try again.";
			}*/
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}
		return "";
		
	}
	
	/*@RequestMapping(value="/UpdatePopularEvents")
	public void addPopulerEvents(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			//Product product = DAORegistry.getProductDAO().getProductByName(Constants.Reward_The_Fan_Product);
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String eventIdStr = request.getParameter("eventIds");
				String[] eventIdsArr = null;
				if(eventIdStr!=null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Collection<PopularEvents> popEventsDB = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(productId);
				Map<Integer,PopularEvents> popEventsMapDB = new HashMap<Integer, PopularEvents>();
				for (PopularEvents popularEvents : popEventsDB) {
					popEventsMapDB.put(popularEvents.getEventDetails().getEventId(), popularEvents);
				}
				if(eventIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularEvents> popularEventsList = new ArrayList<PopularEvents>();
					List<PopularEventAudit> auditList = new ArrayList<PopularEventAudit>();
					for (String eventStr : eventIdsArr) {
						Integer eventId= Integer.parseInt(eventStr);
						if(null != popEventsMapDB.get(eventId)) {
							continue;
						}
						EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
						if(eventDetail == null) {
							continue;
						}
						PopularEvents popEvent = new PopularEvents();
						popEvent.setEventId(eventId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy(username);
						popEvent.setProductId(productId);
						//popEvent.setEventDetails(eventDetail);
						popularEventsList.add(popEvent);
						
						
						PopularEventAudit eventAudit = new PopularEventAudit(eventDetail);
						eventAudit.setAction("CREATED");
						eventAudit.setProductId(productId);
						auditList.add(eventAudit);
					}
					DAORegistry.getPopularEventsDAO().saveAll(popularEventsList);
					DAORegistry.getEventAuditDAO().saveAll(auditList);
				}
				
			} else if(action != null && action.equals("delete")){
				String eventIdStr = request.getParameter("eventIds");
				String[] eventIdsArr = null;
				if(eventIdStr!=null) {
					eventIdsArr = eventIdStr.split(",");
				}
				Collection<PopularEvents> popEventsDB = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(productId);
				Map<Integer,PopularEvents> popEventsMapDB = new HashMap<Integer, PopularEvents>();
				for (PopularEvents popularEvents : popEventsDB) {
					popEventsMapDB.put(popularEvents.getEventDetails().getEventId(), popularEvents);
				}
				
				if(eventIdsArr != null) {
					List<PopularEvents> popularEventsList = new ArrayList<PopularEvents>();
					List<PopularEventAudit> auditList = new ArrayList<PopularEventAudit>();
					for (String eventStr : eventIdsArr) {
						Integer eventId= Integer.parseInt(eventStr);
						PopularEvents popularEvent = popEventsMapDB.get(eventId);
						if(null != popularEvent) {
							popularEventsList.add(popularEvent);
						}
//						EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
//						if(eventDetail == null) {
//							continue;
//						}
//						PopularEvents popEvent = new PopularEvents();
//						popEvent.setEventId(eventId);
//						popEvent.setStatus("ACTIVE");
//						popEvent.setCreatedDate(new Date());
//						popEvent.setCreatedBy("");
//						//popEvent.setEventDetails(eventDetail);
//						
//						popularEventsList.add(popEvent);
						
						PopularEventAudit eventAudit = new PopularEventAudit(popularEvent.getEventDetails());
						eventAudit.setAction("DELETED");
						eventAudit.setProductId(productId);
						auditList.add(eventAudit);
					}
					DAORegistry.getEventAuditDAO().saveAll(auditList);
					DAORegistry.getPopularEventsDAO().deleteAll(popularEventsList);
				}
				
			}
			response.setHeader("Content-type","application/json");
			JSONArray array = getPopularEvents(productId);
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}*/
		
	
	/**
	 * Get Category tickets for event
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularEventDetails")
	public void getEventDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		JSONArray jsonArr = new JSONArray();
		try{
			
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopularEvents(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	
	/**
	 * Helper method to get all popular events for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	/*private JSONArray getPopularEvents(Integer productId){
		JSONArray jsonArr = new JSONArray();
		try{
			Collection<PopularEvents> popEvents = DAORegistry.getPopularEventsDAO().getAllActivePopularEventsByProductId(productId);
			JSONObject jObj = null;
			
			for (PopularEvents popEvent : popEvents) {
				EventDetails eventDtls = popEvent.getEventDetails();
				jObj = new JSONObject();
				jObj.put("eventId", eventDtls.getEventId());
				jObj.put("eventName", eventDtls.getEventName());
				jObj.put("eventDate", eventDtls.getEventDateStr());
				jObj.put("eventTime", eventDtls.getEventTimeStr());
				jObj.put("venue", eventDtls.getBuilding());
				jObj.put("city", eventDtls.getCity());
				jObj.put("state", eventDtls.getState());
				jObj.put("country", eventDtls.getCountry());
				jObj.put("artistName", eventDtls.getArtistName());
				jObj.put("grandChildCategory", eventDtls.getGrandChildCategoryName());
				jObj.put("childCategory", eventDtls.getChildCategoryName());
				jObj.put("parentCategory", eventDtls.getParentCategoryName());
				jObj.put("createdBy", popEvent.getCreatedBy());
				jObj.put("createdDate", popEvent.getCreatedDateStr());
				jsonArr.put(jObj);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}*/
	
	
	@RequestMapping(value="/UpdatePopularArtist")
	public String addPopularArtist(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POPULAR_ARTIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PopularArtistDTO popularArtistDTO = gson.fromJson(((JsonObject)jsonObject.get("popularArtistDTO")), PopularArtistDTO.class);
						
			if(popularArtistDTO.getStatus() == 1){
				model.addAttribute("msg", popularArtistDTO.getMessage());
				model.addAttribute("status", popularArtistDTO.getStatus());
				model.addAttribute("popArtists", popularArtistDTO.getPopularArtistCustomDTO());			
			}else{
				model.addAttribute("msg", popularArtistDTO.getError().getDescription());
				model.addAttribute("status", popularArtistDTO.getStatus());
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	

	/**
	 * Get Category tickets for artist
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularArtist")
	public void getPopularArtist(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopularArtists(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Helper method to get all popular artists for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	/*private JSONArray getPopularArtists(Integer productId, GridHeaderFilters filter){	
		JSONArray jsonArr = new JSONArray();
		//JSONObject jObj = null;
		Collection<PopularArtist> popularArtists = new ArrayList<PopularArtist>();
		try {
			Collection<PopularArtist> popArtists = DAORegistry.getQueryManagerDAO().getAllActivePopularArtistByProductId(productId,filter,null);
			Set<Integer> artistIds = new HashSet<Integer>();
			
			for(PopularArtist popArtist: popArtists){				
				if(artistIds.add(popArtist.getArtistId())){
					popularArtists.add(popArtist);
				}
			}	
			
			jsonArr = JsonWrapperUtil.getPopArtistArray(popularArtists);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}*/
	
	
	/*@RequestMapping(value="/UpdatePopularGrandChildCategory")
	public void updatePopularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String grandChildIdStr = request.getParameter("grandChildIds");
				String[] grandChildIdsArr = null;
				if(grandChildIdStr!=null) {
					grandChildIdsArr = grandChildIdStr.split(",");
				}
				Collection<PopularGrandChildCategory> popGrandChildDB = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(productId);
				Map<Integer,PopularGrandChildCategory> popGrandChildMapDB = new HashMap<Integer, PopularGrandChildCategory>();
				for (PopularGrandChildCategory popularGrandChild : popGrandChildDB) {
					popGrandChildMapDB.put(popularGrandChild.getGrandChildCategoryId(), popularGrandChild);
				}
				if(grandChildIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularGrandChildCategory> popularGrandChildList = new ArrayList<PopularGrandChildCategory>();
					List<PopularGrandChildCategoryAudit> auditList = new ArrayList<PopularGrandChildCategoryAudit>();
					for (String grandChildStr : grandChildIdsArr) {
						Integer grandChildId= Integer.parseInt(grandChildStr);
						if(null != popGrandChildMapDB.get(grandChildId)) {
							continue;
						}
						GrandChildCategory grandChild = DAORegistry.getGrandChildCategoryDAO().get(grandChildId);
						if(grandChild == null) {
							continue;
						}
						PopularGrandChildCategory popgrandChild = new PopularGrandChildCategory();
						popgrandChild.setGrandChildCategoryId(grandChildId);
						popgrandChild.setStatus("ACTIVE");
						popgrandChild.setCreatedDate(new Date());
						popgrandChild.setCreatedBy(username);
						popgrandChild.setProductId(productId);
						//popArtist.setArtist(grandChildDetail);
						popularGrandChildList.add(popgrandChild);
						
						PopularGrandChildCategoryAudit audit = new PopularGrandChildCategoryAudit(grandChild);
						audit.setAction("CREATED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getPopularGrandChildCategoryDAO().saveAll(popularGrandChildList);
					DAORegistry.getGrandChildCategoryAuditDAO().saveAll(auditList);
				}
				
				//map.put("successMessage", returnMessage);
			} if(action != null && action.equals("delete")){
				String grandChildIdStr = request.getParameter("grandChildIds");
				String[] grandChildIdsArr = null;
				if(grandChildIdStr!=null) {
					grandChildIdsArr = grandChildIdStr.split(",");
				}
				Collection<PopularGrandChildCategory> popGrandChildDB = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(productId);
				Map<Integer,PopularGrandChildCategory> popGrandChildMapDB = new HashMap<Integer, PopularGrandChildCategory>();
				for (PopularGrandChildCategory popularGrandChild : popGrandChildDB) {
					popGrandChildMapDB.put(popularGrandChild.getGrandChildCategoryId(), popularGrandChild);
				}
				
				if(grandChildIdsArr != null) {
					List<PopularGrandChildCategory> popularGrandChildList = new ArrayList<PopularGrandChildCategory>();
					List<PopularGrandChildCategoryAudit> auditList = new ArrayList<PopularGrandChildCategoryAudit>();
					for (String grandChildStr : grandChildIdsArr) {
						Integer grandChildId= Integer.parseInt(grandChildStr);
						PopularGrandChildCategory popularChildCategory = popGrandChildMapDB.get(grandChildId);
						if(null != popularChildCategory) {
							popularGrandChildList.add(popularChildCategory);
						}
						EventDetails grandChildDetail = DAORegistry.getEventDetailsDAO().get(grandChildId);
						if(grandChildDetail == null) {
							continue;
						}
						PopularGrandChildCategory popEvent = new PopularGrandChildCategory();
						popEvent.setEventId(grandChildId);
						popEvent.setStatus("ACTIVE");
						popEvent.setCreatedDate(new Date());
						popEvent.setCreatedBy("");
						//popEvent.setEventDetails(grandChildDetail);
						
						popularArtistList.add(popEvent);
						
						PopularGrandChildCategoryAudit audit = new PopularGrandChildCategoryAudit(popularChildCategory.getGrandChildCategory());
						audit.setAction("DELETED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getGrandChildCategoryAuditDAO().saveAll(auditList);
					DAORegistry.getPopularGrandChildCategoryDAO().deleteAll(popularGrandChildList);
				}
				
			}
			JSONArray array = getPopolarGrandChildCategorys(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
			//returnMessage = "There is something wrong..Please Try Again.";
		}
	}*/

	/**
	 * Get Category tickets for artist
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularGrandChildCategory")
	public void getPopularGrandChildCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopolarGrandChildCategorys(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Helper method to get all popular grand child category for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	/*private JSONArray getPopolarGrandChildCategorys(Integer productId){
		JSONArray jsonArr = new JSONArray();
		try {
			Collection<PopularGrandChildCategory> popGrandChildList = DAORegistry.getPopularGrandChildCategoryDAO().getAllActivePopularGrandChildCategoryByProductId(productId);
			JSONObject jObj = null;
			
			for (PopularGrandChildCategory popGrandChild : popGrandChildList) {
				GrandChildCategory grandChild = popGrandChild.getGrandChildCategory();
				jObj = new JSONObject();
				jObj.put("grandChildCategoryId", grandChild.getId());
				jObj.put("grandChildCategory", grandChild.getName());
				jObj.put("childCategory", grandChild.getChildCategory().getName());
				jObj.put("parentCategory", grandChild.getChildCategory().getParentCategory().getName());
				jObj.put("createdBy", popGrandChild.getCreatedBy());
				jObj.put("createdDate", popGrandChild.getCreatedDateStr());
				jsonArr.put(jObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}*/
	
	
	/*@RequestMapping(value="/UpdatePopularVenue")
	public void updatePopularVenue(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			
			String action = request.getParameter("action");
			if(action != null && action.equals("create")){
				
				String venueIdStr = request.getParameter("venueIds");
				String[] venueIdsArr = null;
				if(venueIdStr!=null) {
					venueIdsArr = venueIdStr.split(",");
				}
				Collection<PopularVenue> popVenueDB = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(productId);
				Map<Integer,PopularVenue> popVenueMapDB = new HashMap<Integer, PopularVenue>();
				for (PopularVenue popularVenue : popVenueDB) {
					popVenueMapDB.put(popularVenue.getVenueId(), popularVenue);
				}
				if(venueIdsArr != null) {
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					List<PopularVenue> popularVenueList = new ArrayList<PopularVenue>();
					List<PopularVenueAudit> auditList = new ArrayList<PopularVenueAudit>();
					for (String venueStr : venueIdsArr) {
						Integer venueId= Integer.parseInt(venueStr);
						if(null != popVenueMapDB.get(venueId)) {
							continue;
						}
						Venue venue = DAORegistry.getVenueDAO().get(venueId);
						if(venue == null) {
							continue;
						}
						PopularVenue popVenue = new PopularVenue();
						popVenue.setVenueId(venueId);
						popVenue.setStatus("ACTIVE");
						popVenue.setCreatedDate(new Date());
						popVenue.setCreatedBy(username);
						popVenue.setProductId(productId);
						//popVenue.setVenueDetails(venueDetail);
						popularVenueList.add(popVenue);
						
						PopularVenueAudit audit = new PopularVenueAudit(venue);
						audit.setAction("CREATED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getPopularVenueDAO().saveAll(popularVenueList);
					DAORegistry.getVenueAuditDAO().saveAll(auditList);
				}
				
				//map.put("successMessage", returnMessage);
			} if(action != null && action.equals("delete")){
				String venueIdStr = request.getParameter("venueIds");
				String[] venueIdsArr = null;
				if(venueIdStr!=null) {
					venueIdsArr = venueIdStr.split(",");
				}
				Collection<PopularVenue> popVenueDB = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(productId);
				Map<Integer,PopularVenue> popVenueMapDB = new HashMap<Integer, PopularVenue>();
				for (PopularVenue popularVenue : popVenueDB) {
					popVenueMapDB.put(popularVenue.getVenueId(), popularVenue);
				}
				
				if(venueIdsArr != null) {
					List<PopularVenue> popularVenueList = new ArrayList<PopularVenue>();
					List<PopularVenueAudit> auditList = new ArrayList<PopularVenueAudit>();
					for (String venueStr : venueIdsArr) {
						Integer venueId= Integer.parseInt(venueStr);
						PopularVenue popularVenue = popVenueMapDB.get(venueId);
						if(null != popularVenue) {
							popularVenueList.add(popularVenue);
						}
//						VenueDetails venueDetail = DAORegistry.getVenueDetailsDAO().get(venueId);
//						if(venueDetail == null) {
//							continue;
//						}
//						PopularVenue popVenue = new PopularVenue();
//						popVenue.setVenueId(venueId);
//						popVenue.setStatus("ACTIVE");
//						popVenue.setCreatedDate(new Date());
//						popVenue.setCreatedBy("");
//						//popVenue.setVenueDetails(venueDetail);
//						
//						popularVenueList.add(popVenue);
						
						PopularVenueAudit audit = new PopularVenueAudit(popularVenue.getVenue());
						audit.setAction("DELETED");
						audit.setProductId(productId);
						auditList.add(audit);
					}
					DAORegistry.getVenueAuditDAO().saveAll(auditList);
					DAORegistry.getPopularVenueDAO().deleteAll(popularVenueList);
				}
				
			}
			JSONArray array = getPopularVenues(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch (Exception e) {
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
	}*/
	
	/**
	 * Get Popular venue
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/GetPopularVenueDetails")
	public void getPopularVenueDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		
		try{
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			JSONArray array = getPopularVenues(productId);
			response.setHeader("Content-type","application/json");
			IOUtils.write(array.toString().getBytes(),
					response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Helper method to get all popular venues for given productId
	 * @param productId Integer
	 * @return JSONArray
	 */
	/*private JSONArray getPopularVenues(Integer productId){
		JSONArray jsonArr = new JSONArray();
		try {
			Collection<PopularVenue> popVenueList = DAORegistry.getPopularVenueDAO().getAllActivePopularVenueByProductId(productId);
			JSONObject jObj = null;
			
			for (PopularVenue popVenue : popVenueList) {
				Venue venue = popVenue.getVenue();
				jObj = new JSONObject();
				jObj.put("venueId", venue.getId());
				jObj.put("venue", venue.getBuilding());
				jObj.put("city", venue.getCity());
				jObj.put("state", venue.getState());
				jObj.put("country", venue.getCountry());
				jObj.put("postalCode", venue.getPostalCode());
				jObj.put("createdBy", popVenue.getCreatedBy());
				jObj.put("createdDate", popVenue.getCreatedDateStr());
				jsonArr.put(jObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArr;
	}*/
	
	
	/**
	 * Check customer already exists
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/CheckCustomer")
	public String checkCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		try{
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			String userName = request.getParameter("userName");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("sessionUser", trackerUserSession!=null?trackerUserSession.getUserName():null);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_CUSTOMER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
			/*if(trackerUserSession == null){
				return "Please Login.";
			}
			String userName = request.getParameter("userName");
			Customer isCustomerExists = DAORegistry.getCustomerDAO().checkCustomerExists(userName);
			if(isCustomerExists != null){
				return "There is already an customer with this Username or Email.";
			}
			return "true";*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}

	@RequestMapping(value = "/CheckUser")
	public String checkUser(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		try{
			String userName = request.getParameter("userName");
			String email = request.getParameter("email");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("email", email);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_USER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/CheckUserForEdit")
	public String checkUserForEdit(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		try{
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				model.addAttribute("status", 0);
				model.addAttribute("msg", "Please Login and try again.");
				return "";
			}
			
			String userName = request.getParameter("userName");
			String email = request.getParameter("email");
			String userId = request.getParameter("userId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("email", email);
			map.put("userId", userId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_USER_FOR_AUDIT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return "There is something wrong. Please try again.";
		}
		return "";
	}
	
	/**
	 * Delete customer info
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/DeleteCustomer")
	public String deleteCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DELETE_CUSTOMER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String id="";
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				return "Pleas Login.";
			}
			
			String userIdStr = request.getParameter("userId");
			Customer deleteCustomer = DAORegistry.getCustomerDAO().get(Integer.parseInt(userIdStr));
			if(deleteCustomer == null){
				return "There is no customer to delete.";
			}
			if(trackerUserSession.getId().equals(deleteCustomer.getId())){
				return "You can't delete your own account.";
			}
			id = deleteCustomer.getId().toString();
			DAORegistry.getCustomerDAO().delete(deleteCustomer);
			return id;*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	
	/**
	 * Delete user
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/DeleteUser")
	public String deleteUser(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		 try{
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DELETE_USER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				return "Pleas Login.";
			}
			
			String userIdStr = request.getParameter("userId");
			TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().get(Integer.parseInt(userIdStr));
			if(trackerUser == null){
				return "There is no user to delete.";
			}
			if(trackerUserSession.getId().equals(trackerUser.getId())){
				return "You can't delete your own account.";
			}
			DAORegistry.getQueryManagerDAO().deleteUserActions(trackerUser.getId());
			DAORegistry.getTrackerUserDAO().delete(trackerUser);
			//trackerUser.setStatus(false);
			//DAORegistry.getTrackerUserDAO().update(trackerUser);
			
			//Tracking User Action
			String userActionMsg = "User is Deleted.";
			Util.userActionAudit(request, Integer.parseInt(userIdStr), userActionMsg);
			
			return "true";*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	@RequestMapping(value = "/DeleteShippingAddress")
	public String deleteShippingAddress(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DELETE_SHIPPING_ADDRESS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				return "Please Login.";
			}
			
			String userIdStr = request.getParameter("userId");

			CustomerAddress shippingAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(userIdStr));

			if(shippingAddress == null){
				return "There is no shipping address to delete.";
			}
			
			DAORegistry.getCustomerAddressDAO().delete(shippingAddress);
			return "true";*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	/**
	 * Get states for the country
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/getStates", method=RequestMethod.GET)
	@ResponseBody
	public List<State> getStateForCountry(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		List<State> stateListForCountry = null;
		System.out.println("get states initiated");
		try{
			
			Integer country = Integer.parseInt(request.getParameter("country"));
			stateListForCountry = DAORegistry.getQueryManagerDAO().getStatesForCountry(country);
			System.out.println("=====" +stateListForCountry);
			return stateListForCountry;
		}catch(Exception e){
			e.printStackTrace();
		}
		return stateListForCountry;
	}*/
	
	/**
	 * Get shipping address list for customer
	 * @param customerId
	 */
	@RequestMapping(value = "/getShippingAddressDetails")
	public String getCustomerShippingAddress(Integer customerId, HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String shippingId = request.getParameter("shippingId");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("customerId", customerId.toString());
			map.put("shippingId", shippingId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_SHIPPING_ADDRESS_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ShippingAddressDTO shippingAddressDTO = gson.fromJson(((JsonObject)jsonObject.get("shippingAddressDTO")), ShippingAddressDTO.class);
			
			if(shippingAddressDTO.getStatus() == 1){
				model.addAttribute("msg", shippingAddressDTO.getMessage());
				model.addAttribute("status", shippingAddressDTO.getStatus());
				model.addAttribute("shippingAddressList", shippingAddressDTO.getShippingAddressDTOs());
			}else{
				model.addAttribute("msg", shippingAddressDTO.getError().getDescription());
				model.addAttribute("status", shippingAddressDTO.getStatus());
			}
			
			/*JSONArray jsonArr = new JSONArray();
			int shippingAddressCount = 0;
			List<CustomerAddress> shippingAddressList = new ArrayList<CustomerAddress>();
			
			if(shippingAddressId!=null && !shippingAddressId.isEmpty()){
				CustomerAddress shippingAddress  = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
				shippingAddressList.add(shippingAddress);
			}else{
				shippingAddressList = DAORegistry.getCustomerAddressDAO().updatedShippingAddress(customerId);
			}
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
			JSONObject jsonObj = null;
			if(shippingAddressList != null){
				for(CustomerAddress shippingAddress : shippingAddressList){
					jsonObj = new JSONObject();
					jsonObj.put("shId", shippingAddress.getId());
					jsonObj.put("firstName", shippingAddress.getFirstName());
					jsonObj.put("lastName", shippingAddress.getLastName());
					jsonObj.put("street1", shippingAddress.getAddressLine1());
					jsonObj.put("street2", shippingAddress.getAddressLine2());
					jsonObj.put("country", shippingAddress.getCountryName());
					jsonObj.put("state", shippingAddress.getStateName());
					jsonObj.put("city", shippingAddress.getCity());
					jsonObj.put("zipCode", shippingAddress.getZipCode());
					jsonObj.put("phone", shippingAddress.getPhone1());
					jsonObj.put("email", shippingAddress.getEmail());
					jsonArr.put(jsonObj);
				}
				IOUtils.write(jsonArr.toString().getBytes(),
						response.getOutputStream());
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	
	/**
	 * Get shipping address list for customer
	 * @param customerId
	 */
	@RequestMapping(value = "/GetSavedCardsRewardPointsAndLoyaFan")
	public String getCustomerSavedCards(Integer customerId, HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String eventId = request.getParameter("eventId");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("customerId", customerId.toString());
			map.put("eventId", eventId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_SAVED_CARDS_REWARDS_LOYALFAN);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SavedCardsRewardPointsAndLoyalFanDTO savedCardsRewardPointsAndLoyalFanDTO = gson.fromJson(((JsonObject)jsonObject.get("savedCardsRewardPointsAndLoyalFanDTO")), SavedCardsRewardPointsAndLoyalFanDTO.class);
			
			if(savedCardsRewardPointsAndLoyalFanDTO.getStatus() == 1){
				model.addAttribute("msg", savedCardsRewardPointsAndLoyalFanDTO.getMessage());
				model.addAttribute("status", savedCardsRewardPointsAndLoyalFanDTO.getStatus());
				model.addAttribute("savedCards", savedCardsRewardPointsAndLoyalFanDTO.getCustomerCardInfoDTO());
				model.addAttribute("rewardPoints", savedCardsRewardPointsAndLoyalFanDTO.getRewardPoints());
				model.addAttribute("customerCredit", savedCardsRewardPointsAndLoyalFanDTO.getCustomerCredit());
				model.addAttribute("superFanArtistId", savedCardsRewardPointsAndLoyalFanDTO.getIsLoyalFanPrice());
			}else{
				model.addAttribute("msg", savedCardsRewardPointsAndLoyalFanDTO.getError().getDescription());
				model.addAttribute("status", savedCardsRewardPointsAndLoyalFanDTO.getStatus());
			}
			
			/*JSONObject returnObject = new JSONObject();
			Double rewardPoints = 0.00;
			
			Map<String, String> paramterMap = Util.getParameterMap(request);
			paramterMap.put("customerId", String.valueOf(customerId));
			String data = Util.getObject(paramterMap,sharedProperty.getApiUrl()+Constants.GET_CUSTOMER_CARDS);
		    Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerDetails cardDetails = gson.fromJson(((JsonObject)jsonObject.get("customerDetails")), CustomerDetails.class);
			JSONArray jsonArr = new JSONArray();
			if(null != cardDetails.getCardList()){
				JSONObject jsonObj = null;
				for(CustomerCardInfo customerCardInfo : cardDetails.getCardList()){
					jsonObj = new JSONObject();
					jsonObj.put("cardId", customerCardInfo.getId());
					jsonObj.put("cardType", customerCardInfo.getCardType());
					jsonObj.put("cardNo", customerCardInfo.getCardNo());
					jsonArr.put(jsonObj);
				}
			}
			returnObject.put("savedCards", jsonArr);
			
			CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(loyalty!=null){
				rewardPoints = loyalty.getActivePoints();
			}
			returnObject.put("rewardPoints", rewardPoints);
			
			
			CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			if(wallet!=null){
				returnObject.put("customerCredit", wallet.getActiveCredit());
			}else{
				returnObject.put("customerCredit",0);
			}
									
			boolean isLoyalFanPrice = false;
			
		    Map<String, String> map = Util.getParameterMap(request);
		    map.put("customerId", String.valueOf(customerId));
		    map.put("eventId", eventIdStr);
		    map.put("sessionId", request.getSession(true).getId());
		    String respdata = Util.getObject(map,sharedProperty.getApiUrl()+Constants.VALIDATE_LOYALFAN);
		    Gson respgson = new Gson();  
			JsonObject jsnObject = respgson.fromJson(respdata, JsonObject.class);
			ValidateCustomerLoyalFan validateLoyalFan = respgson.fromJson(((JsonObject)jsnObject.get("validateCustomerLoyalFan")), ValidateCustomerLoyalFan.class);
			if(validateLoyalFan.getStatus() == 1){
				if(validateLoyalFan.getIsLoyalFanForTicTracker() != null && validateLoyalFan.getIsLoyalFanForTicTracker()){
					isLoyalFanPrice = true;
				}
			}
//			CustomerLoyalFan loyalFan = DAORegistry.getCustomerLoyalFanDAO().getActiveLoyalFanByCustomerId(customerId);
//			Date today = new Date();
//			if(loyalFan!=null && loyalFan.getEndDate().after(today)){
//				if(eventIdStr !=null && !eventIdStr.isEmpty()){
//					List<Integer> artistIds = DAORegistry.getQueryManagerDAO().getAllArtistIdsByEventId(Integer.parseInt(eventIdStr));
//					for(Integer id  :artistIds){
//						if(id.equals(loyalFan.getArtistId())){
//							isLoyalFanPrice = true;
//							break;
//						}
//					}
//				}
//			}
			returnObject.put("superFanArtistId",isLoyalFanPrice);
			IOUtils.write(returnObject.toString().getBytes(),
					response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	@RequestMapping(value="/CheckCardPosition")
	public String checkCardPosition(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			Map<String, String> map = Util.getParameterMap(request);
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_CARD_POSITION);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Updates Event Notes
	 * @param tmatEventId
	 */
	@RequestMapping(value = "/saveEventNotes")
	public String saveEventNotes(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String eventId = request.getParameter("eventId");
			String notes = request.getParameter("notes");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("eventId", eventId);
			map.put("notes", notes);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_EVENT_NOTES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getMessage());
			}else{
				model.addAttribute("status", genericResponseDTO.getStatus());
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			
			/*String msg="";
			if(eventId!=null && !eventId.isEmpty() && notes!=null && !notes.isEmpty()){
				notes = notes.replace("\n", " ");
				notes = notes.replace("\t", " ");
				notes = notes.replace("\r", " ");
				DAORegistry.getQueryManagerDAO().saveEventNotes(Integer.parseInt(eventId), notes);
				msg= "Notes saved successfully.";
			}else{
				msg="No Data found to save, Please try again.";
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	/**
	 * Updates Event Notes
	 * @param tmatEventId
	 */
	/*@RequestMapping(value = "/saveVenueNotes")
	public @ResponseBody String saveVenuetNotes(HttpServletRequest request, HttpServletResponse response){
		String msg="";
		try {
			String venueId = request.getParameter("venueId");
			String notes = request.getParameter("notes");
			if(venueId!=null && !venueId.isEmpty() && notes!=null && !notes.isEmpty()){
				notes = notes.replace("\n", " ");
				notes = notes.replace("\t", " ");
				notes = notes.replace("\r", " ");
				DAORegistry.getQueryManagerDAO().saveVenueNotes(Integer.parseInt(venueId), notes);
				msg= "Notes saved successfully.";
			}else{
				msg="No Data found to save, Please try again.";
			}
		} catch (Exception e) {
			e.printStackTrace();
			msg="There is something wrong..Please Try Again.";
		}
		return msg;
	}*/
	
	
	@RequestMapping(value = "/SaveUserPreference")
	public String saveUserPreference(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String columns = request.getParameter("columns");
			String gridName = request.getParameter("gridName");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("columns", columns);
			map.put("gridName", gridName);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_USER_PREFERENCE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			UserPreferenceDTO userPreferenceDTO = gson.fromJson(((JsonObject)jsonObject.get("userPreferenceDTO")), UserPreferenceDTO.class);
						
			if(userPreferenceDTO.getStatus() == 1){
				model.addAttribute("status", userPreferenceDTO.getStatus());
				model.addAttribute("msg", userPreferenceDTO.getMessage());
				session.setAttribute(gridName, userPreferenceDTO.getColumnOrders());
			}else{
				model.addAttribute("status", userPreferenceDTO.getStatus());
				model.addAttribute("msg", userPreferenceDTO.getError().getDescription());
			}
			
			/*UserPreference preference = null;
			String msg="";
			
			if(columnOrders!=null && !columnOrders.isEmpty() && gridName!=null && !gridName.isEmpty()){
				
				preference = DAORegistry.getUserPreferenceDAO().getPreferenceByUserNameandGridName(userName,gridName);
				int index = columnOrders.lastIndexOf(",");
				if(index == (columnOrders.length()-1)){
					columnOrders = 	columnOrders.substring(0, index);
				}
				if(preference==null){
					preference = new UserPreference();
					preference.setColumnOrders(columnOrders);
					preference.setUserName(userName);
					preference.setGridName(gridName);
				}else{
					preference.setColumnOrders(columnOrders);
				}
				DAORegistry.getUserPreferenceDAO().saveOrUpdate(preference);
				session.setAttribute(gridName, preference.getColumnOrders());
				msg="OK";
			}
			IOUtils.write(msg.getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping({"/GetStates"})
	public String getStates(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String countryId = request.getParameter("countryId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("countryId", countryId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_LOAD_STATES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			LoadStateDTO loadStateDTO = gson.fromJson(((JsonObject)jsonObject.get("loadStateDTO")), LoadStateDTO.class);
			
			if(loadStateDTO.getStatus() == 1){
				model.addAttribute("msg", loadStateDTO.getMessage());
				model.addAttribute("status", loadStateDTO.getStatus());
				model.addAttribute("states", loadStateDTO.getStateDTO());
			}else{
				model.addAttribute("msg", loadStateDTO.getError().getDescription());
				model.addAttribute("status", loadStateDTO.getStatus());
			}
			/*List<State> stateList= new ArrayList<State>();
			JSONArray array = new JSONArray();
			if(countryId!=null && !countryId.isEmpty()){
				if(countryId.contains(",")){
					String arr[] = countryId.split(",");
					for(String str : arr){
						if(str!=null && !str.isEmpty()){
							if(str.equalsIgnoreCase("217")){
								stateList.addAll(DAORegistry.getStateDAO().getAllStateForUSA());
							}else{
								stateList.addAll(DAORegistry.getQueryManagerDAO().getStatesForCountry(Integer.parseInt(str)));
							}
						}
					}
				}else{
					stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(Integer.parseInt(countryId));
				}
				JSONObject object = null;
				for(State state : stateList){
					object = new JSONObject();
					object.put("id", state.getId());
					object.put("name", state.getName());
					array.put(object);
				}
				
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping({"/GetFantasyChildCategory"})
	public String getChildCategoryByParentID(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String parentIdStr = request.getParameter("parentIdStr");
			String parentType = request.getParameter("parentType");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("parentIdStr", parentIdStr);
			map.put("parentType", parentType);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_FANTASY_CHILD_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyChildCategoryDTO fantasyChildCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyChildCategoryDTO")), FantasyChildCategoryDTO.class);
						
 			if(fantasyChildCategoryDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyChildCategoryDTO.getMessage()!=null?fantasyChildCategoryDTO.getMessage():"");
				model.addAttribute("status", fantasyChildCategoryDTO.getStatus());
				model.addAttribute("fantasyChildCategoryList", fantasyChildCategoryDTO.getFanasyCategoryDTO());
			}else{
				model.addAttribute("msg", fantasyChildCategoryDTO.getError() != null ? fantasyChildCategoryDTO.getError().getDescription() : "");
				model.addAttribute("status", fantasyChildCategoryDTO.getStatus());
			}
 			
			/*List<FantasyChildCategory> childCategoryList = null;
			JSONArray array = new JSONArray();
			if(parentId!=null && !parentId.isEmpty()){
				childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryByParentId(Integer.parseInt(parentId));
			}if(parentType != null && !parentType.isEmpty()){
				FantasyParentCategory parentCategory = DAORegistry.getFantasyParentCategoryDAO().getParentCategoryByName(parentType);
				childCategoryList = DAORegistry.getFantasyChildCategoryDAO().getChildCategoryByParentId(parentCategory.getId());
			}
			JSONObject object = null;
			for(FantasyChildCategory childCategory : childCategoryList){
				object = new JSONObject();
				object.put("id", childCategory.getId());
				object.put("name", childCategory.getName());
				array.put(object);
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping({"/GetFantasyGrandChildCategory"})
	public String getGrandChildCategoryByChildID(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try{
			String childIdStr = request.getParameter("childIdStr");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("childIdStr", childIdStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_FANTASY_GRANDCHILD_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyGrandChildCategoryDTO fantasyGrandChildCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyGrandChildCategoryDTO")), FantasyGrandChildCategoryDTO.class);
						
 			if(fantasyGrandChildCategoryDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyGrandChildCategoryDTO.getMessage()!=null?fantasyGrandChildCategoryDTO.getMessage():"");
				model.addAttribute("status", fantasyGrandChildCategoryDTO.getStatus());
				model.addAttribute("fantasyGrandChildCategoryList", fantasyGrandChildCategoryDTO.getFanasyCategoryDTO());
			}else{
				model.addAttribute("msg", fantasyGrandChildCategoryDTO.getError() != null ? fantasyGrandChildCategoryDTO.getError().getDescription() : "");
				model.addAttribute("status", fantasyGrandChildCategoryDTO.getStatus());
			}
			
			/*List<FantasyGrandChildCategory> grandChildCategoryList = new ArrayList<FantasyGrandChildCategory>();
			JSONArray array = new JSONArray();
			if(childId!=null && !childId.isEmpty() && !childId.equalsIgnoreCase("undefined")){
				grandChildCategoryList = DAORegistry.getFantasyGrandChildCategoryDAO().getGrandChildsByChildCategoryId(Integer.parseInt(childId));
			}
			JSONObject object = null;
			for(FantasyGrandChildCategory grandChildCategory : grandChildCategoryList){
				object = new JSONObject();
				object.put("id", grandChildCategory.getId());
				object.put("name", grandChildCategory.getName());
				array.put(object);
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	
	@RequestMapping({"/getFantasyCategoryTeam"})
	public String getFantasyCategoryTeam(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String teamId = request.getParameter("teamId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("teamId", teamId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_FANTASY_CATEGORY_TEAM);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyCategoryTeamDTO fantasyCategoryTeamDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyCategoryTeamDTO")), FantasyCategoryTeamDTO.class);
						
 			if(fantasyCategoryTeamDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyCategoryTeamDTO.getMessage()!=null?fantasyCategoryTeamDTO.getMessage():"");
				model.addAttribute("status", fantasyCategoryTeamDTO.getStatus());
				model.addAttribute("fantasyCategoryTeam", fantasyCategoryTeamDTO.getFanasyCatTeamDTO());
			}else{
				model.addAttribute("msg", fantasyCategoryTeamDTO.getError() != null ? fantasyCategoryTeamDTO.getError().getDescription() : "");
				model.addAttribute("status", fantasyCategoryTeamDTO.getStatus());
			}
 			
			/*JSONObject object= new JSONObject();
			if(teamId!=null && !teamId.isEmpty()){
				CrownJewelCategoryTeams team = DAORegistry.getCrownJewelCategoryTeamsDAO().get(Integer.parseInt(teamId));
				object.put("id", team.getId());
				object.put("name", team.getName());
				object.put("fractionOdds", team.getFractionalOdd());
				object.put("odds", team.getOdds());
				object.put("markup", team.getMarkup());
				object.put("cutoffDate", team.getCutOffDateStr());
			}
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping({"/GetFantasyEventsByGrandChild"})
	public String getFantasyEventsByGrandChild(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String grandChildCategoryId = request.getParameter("grandChildCategoryId");
			String customerId = request.getParameter("customerId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("grandChildCategoryId", grandChildCategoryId);
			map.put("customerId", customerId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.GET_FANTASY_EVENTS_BY_GRANDCHILD);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyEventsFromGrandChildDTO fantasyEventsFromGrandChildDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyEventsFromGrandChildDTO")), FantasyEventsFromGrandChildDTO.class);
						
 			if(fantasyEventsFromGrandChildDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyEventsFromGrandChildDTO.getMessage()!=null?fantasyEventsFromGrandChildDTO.getMessage():"");
				model.addAttribute("status", fantasyEventsFromGrandChildDTO.getStatus());
				model.addAttribute("ticketPagingInfo", fantasyEventsFromGrandChildDTO.getEventTeamTicketPaginationDTO());
				model.addAttribute("eventTeamGrid", fantasyEventsFromGrandChildDTO.getFantasyEventTeamTicketDTOs());
				model.addAttribute("packageInfo", fantasyEventsFromGrandChildDTO.getPackageInfo());
			}else{
				model.addAttribute("msg", fantasyEventsFromGrandChildDTO.getError() != null ? fantasyEventsFromGrandChildDTO.getError().getDescription() : "");
				model.addAttribute("status", fantasyEventsFromGrandChildDTO.getStatus());
			}
 			
			/*FantasyCategories fantasyCategories = null;
			List<FantasyEventTeamTicket> fantasyEventTeamTicketList = null;
			JSONObject object= null;
			JSONArray array = new JSONArray();
			JSONObject arrayObject= new JSONObject();
//			if(grandChildStr!=null && !grandChildStr.isEmpty()){
//				List<CrownJewelLeagues> leagues = DAORegistry.getCrownJewelLeaguesDAO().getActiveLeaguesByGrandChildId(Integer.parseInt(grandChildStr));
//				for(CrownJewelLeagues  league : leagues){
//					if(league.getEventId()!=null){
//						object= new JSONObject();
//						object.put("id", league.getId());
//						object.put("name", league.getName());
//						array.put(object);
//					}
//				}
//			}
			
			Map<String, String> map = Util.getParameterMap(request);			
		    map.put("customerId", customerIdStr);
		    map.put("fCategoryId", grandChildStr);
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.GET_FANTASY_EVENTS);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProductList = gson.fromJson(((JsonObject)jsonObject.get("fantasySportsProduct")), FantasySportsProduct.class);
			if(fantasySportsProductList.getStatus() == 1){
				fantasyCategories = fantasySportsProductList.getFantasyEvent();
				fantasyEventTeamTicketList = fantasyCategories.getFantasyTeamTickets();
				
				arrayObject.put("packageInfo", fantasyCategories.getPackageInformation());
				for(FantasyEventTeamTicket fantasyEventTeam : fantasyEventTeamTicketList){
					object = new JSONObject();
					object.put("categoryId", fantasyEventTeam.getCategoryId());
					object.put("teamId", fantasyEventTeam.getTeamId());
					object.put("teamName",fantasyEventTeam.getTeamName());
					object.put("eventId", fantasyEventTeam.getFantasyEventId());
					object.put("teamZoneId", fantasyEventTeam.getTeamZoneId());
					object.put("zone",fantasyEventTeam.getZone());
					object.put("quantity",fantasyEventTeam.getQuantity());
					object.put("requiredPoints",fantasyEventTeam.getRequiredPoints());
					object.put("ticketId",fantasyEventTeam.getTicketId());
					object.put("isRealTicket",fantasyEventTeam.getIsRealTicket());
					object.put("tmatEventId",fantasyEventTeam.getTmatEventId());
					object.put("eventDateStr",fantasyEventTeam.getEventDateStr());
					object.put("eventTimeStr",fantasyEventTeam.getEventTimeStr());
					object.put("venueName",fantasyEventTeam.getVenueName());
					object.put("city",fantasyEventTeam.getCity());
					object.put("state",fantasyEventTeam.getState());
					object.put("country",fantasyEventTeam.getCountry());
					array.put(object);
				}
				arrayObject.put("eventTeamGrid", array);
			}
			
			arrayObject.put("ticketPagingInfo", PaginationUtil.getDummyPaginationParameter(fantasyEventTeamTicketList.size()));
			IOUtils.write(arrayObject.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	/*@RequestMapping({"/GetFantasyTeamsbyEventId"})
	public void getFantasyTeamsbyEventId(HttpServletRequest request, HttpServletResponse response){
		String leagueIdStr = request.getParameter("leagueId");
		JSONObject object= null;
		JSONArray array = new JSONArray();
		try{
			if(leagueIdStr!=null && !leagueIdStr.isEmpty()){
				List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(Integer.parseInt(leagueIdStr));
				for(CrownJewelTeams  team : teams){
					object= new JSONObject();
					object.put("id", team.getId());
					object.put("name", team.getName());
					array.put(object);
				}
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/GetFantasyTicketByTeam"})
	public void getFantasyTicketByTeam(HttpServletRequest request, HttpServletResponse response){
		String leagueIdStr = request.getParameter("leagueId");
		JSONObject object= null;
		JSONArray array = new JSONArray();
		try{
			if(leagueIdStr!=null && !leagueIdStr.isEmpty()){
				List<CrownJewelTeams> teams = DAORegistry.getCrownJewelTeamsDAO().getAllActiveTeamsByLeagueId(Integer.parseInt(leagueIdStr));
				for(CrownJewelTeams  team : teams){
					object= new JSONObject();
					object.put("id", team.getId());
					object.put("name", team.getName());
					array.put(object);
				}
			}
			IOUtils.write(array.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping({"/GetFantasyTickets"})
	public void getFantasyTickets(HttpServletRequest request, HttpServletResponse response){
		String leagueIdStr = request.getParameter("leagueId");
		String customerIdStr = request.getParameter("customerId");
		String teamIdStr = request.getParameter("teamId");
		JSONObject jsonObject =  new JSONObject();
		try{
			if(customerIdStr==null || customerIdStr.isEmpty()){
				
			}else if (teamIdStr==null || teamIdStr.isEmpty()){
				
			}else if(leagueIdStr==null || leagueIdStr.isEmpty()){
				
			}else{
				Map<String, String> map = Util.getParameterMap(request);
				map.put("leagueId",leagueIdStr);
				map.put("teamId",teamIdStr);
				map.put("customerId",customerIdStr);
				
				String data = Util.getObject(map, Constants.BASE_URL+Constants.GET_SPORTS_CROWN_JEWEL_TICKETS);
				Gson gson = new Gson();		
				JsonObject jsonObject1 = gson.fromJson(data, JsonObject.class);
				CrownJewelTicketList crownJewelTicketList = gson.fromJson(((JsonObject)jsonObject1.get("crownJewelTicketList")), CrownJewelTicketList.class);
				int count = 0;
				if(crownJewelTicketList!=null && crownJewelTicketList.getSectionTicketsMap()!=null
						&& !crownJewelTicketList.getSectionTicketsMap().isEmpty()){
					count = crownJewelTicketList.getSectionTicketsMap().size();
				}
				jsonObject.put("data", data);
				jsonObject.put("ticketPagingInfo", PaginationUtil.getDummyPaginationParameter(count));
			}
			IOUtils.write(jsonObject.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping({"/SetGlobalProduct"})
	public void setProductTypeForUser(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		try {
			String product = request.getParameter("productType");
			if(product!=null && !product.isEmpty() && !product.equals("-1")){
				session.setAttribute("productType",product);
			}else{
				session.setAttribute("productType",null);
			}
			IOUtils.write("OK".toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping({"/SetGlobalBroker"})
	public String setBrokerForUser(HttpServletRequest request, HttpServletResponse response,HttpSession session, Model model){
		
		try {
			String brokerId = request.getParameter("brokerId");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("brokerId", brokerId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SET_GLOBAL_BROKER);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GlobalBrokerDTO globalBrokerDTO = gson.fromJson(((JsonObject)jsonObject.get("globalBrokerDTO")), GlobalBrokerDTO.class);
			
			if(globalBrokerDTO.getStatus() == 1){
				model.addAttribute("msg", globalBrokerDTO.getMessage());
				model.addAttribute("status", globalBrokerDTO.getStatus());
			}else{
				model.addAttribute("msg", globalBrokerDTO.getError() != null ? globalBrokerDTO.getError().getDescription() : "");
				model.addAttribute("status", globalBrokerDTO.getStatus());
			}
			session.setAttribute("brokerId", globalBrokerDTO.getBrokerId() != null ? globalBrokerDTO.getBrokerId() : null);
			session.setAttribute("brokerCompanyName", globalBrokerDTO.getCompanyName() != null ? globalBrokerDTO.getCompanyName() : null);
			
			/*if(brokerId!=null && !brokerId.isEmpty() && !brokerId.equals("-1")){
				TrackerBrokers trackerBroker = DAORegistry.getTrackerBrokersDAO().get(Integer.parseInt(brokerId));
				if(trackerBroker != null){
					session.setAttribute("brokerCompanyName", trackerBroker.getCompanyName());
				}
				session.setAttribute("brokerId",Integer.parseInt(brokerId));
			}else{
				session.setAttribute("brokerId", null);
				session.setAttribute("brokerCompanyName", null);
			}
			IOUtils.write("OK".toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	

	/*@RequestMapping(value = "/SignUp",method=RequestMethod.POST)
	@ResponseBody
	public String addSignUp(HttpServletRequest request, HttpServletResponse response, HttpSession session){//ModelMap map){
		String returnMessage = "";
		try{
			String action = request.getParameter("action");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String phoneNo = request.getParameter("phoneNo");
			String userRole = request.getParameter("userRole");
			String companyName = request.getParameter("companyName");
			String serviceFees = request.getParameter("serviceFees");
			TrackerUser trackerUserDb = null;
			
			if(firstName == null || firstName.isEmpty()){
				returnMessage= "Firstname can't be blank";
			}else if(lastName == null || lastName.isEmpty()){
				returnMessage= "Lastname can't be blank";
			}else if(email == null || email.isEmpty()){
				returnMessage= "Email can't be blank";
			}else if(!validateEMail(email)){
				returnMessage = "Invalid Email.";
			}else if(password == null || password.isEmpty()){
				returnMessage= "Password can't be blank";
			}else if(userRole == null || userRole.isEmpty()){
				returnMessage = "Please choose at least one Role.";
			}else if(userRole.toUpperCase().equals("ROLE_BROKER")){
				if(companyName == null || companyName.isEmpty()){
					returnMessage= "Company Name can't be blank";
				}
				if(serviceFees == null || serviceFees.isEmpty()){
					returnMessage= "Service Fess can't be blank";
				}
			}
			if(returnMessage.isEmpty()){
				trackerUserDb = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(email, email);
				TrackerBrokers trackerBrokersDb = null;
				if(trackerUserDb != null){
					returnMessage = "There is already an user with this Username or Email.";
				}else{
					trackerUserDb = new TrackerUser();
					
					// adding user to database
					trackerUserDb.setFirstName(firstName);
					trackerUserDb.setLastName(lastName);
					trackerUserDb.setUserName(email);
					trackerUserDb.setEmail(email);						
					trackerUserDb.setPassword(encryptPwd(password));
					
					if(phoneNo == null || phoneNo.isEmpty()){
						trackerUserDb.setPhone(null);
					}else{
						trackerUserDb.setPhone(phoneNo);
					}
						
					if(userRole.toUpperCase().equals("ROLE_BROKER")){
						trackerBrokersDb = new TrackerBrokers();
						trackerBrokersDb.setCompanyName(companyName);
						trackerBrokersDb.setServiceFees(Double.parseDouble(serviceFees));
						DAORegistry.getTrackerBrokersDAO().save(trackerBrokersDb);
						
						trackerUserDb.setBroker(trackerBrokersDb);
					}
					
//					if(userRole.toUpperCase().equals("ROLE_AFFILIATES")){
//						trackerUserDb.setPromotionalCode(Util.generateCustomerReferalCode());
//					}
					
					Set<Role> roleList = new HashSet<Role>();
					Role roleDb = DAORegistry.getRoleDAO().getRoleByName(userRole.toUpperCase());
					roleList.add(roleDb);
							
					trackerUserDb.setRoles(roleList);	
					trackerUserDb.setStatus(true);
					trackerUserDb.setCreateDate(new Date());
					trackerUserDb.setCreatedBy("AUTO");
					
					DAORegistry.getTrackerUserDAO().save(trackerUserDb);
					returnMessage = "You are registered successfully.";
					//map.put("successMessage", returnMessage);					
				}
			}else{
				//map.put("errorMessage", returnMessage);
				return returnMessage;
			}
		}catch(Exception e){
			e.printStackTrace();
			//map.put("errorMessage", "There is something wrong..Please Try Again.");
			returnMessage = "There is something wrong..Please Try Again.";
		}		
		//return "page-login";
		return returnMessage;
	}*/
	
	
	/*
	 * Getting Affiliate Details
	 */
	@RequestMapping(value = "/Affiliates")
	public String getAffiliatesPage(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{
			String status = request.getParameter("status");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AFFILIATES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AffiliatesDTO affiliatesDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliatesDTO")), AffiliatesDTO.class);
			
			if(affiliatesDTO.getStatus() == 1){
				model.addAttribute("affiliatePagingInfo", gson.toJson(affiliatesDTO.getAffiliatesPaginationDTO()));
				model.addAttribute("msg", affiliatesDTO.getMessage());
			}else{
				model.addAttribute("msg", affiliatesDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("affiliates", affiliatesDTO.getManageAffiliatesDTOs());
				model.addAttribute("pagingInfo", affiliatesDTO.getPaginationDTO());
			}else{
				model.addAttribute("affiliates", gson.toJson(affiliatesDTO.getManageAffiliatesDTOs()));
				model.addAttribute("pagingInfo", gson.toJson(affiliatesDTO.getPaginationDTO()));
			}			
			model.addAttribute("status", status);
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-affiliate-details";		
	}
	
	
	
	@RequestMapping(value = "/ContestAffiliates")
	public String getContestAffiliatesPage(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{
			String status = request.getParameter("affStatus");
			String headerFilter = request.getParameter("headerFilter");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTEST_AFFILIATES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestAffiliatesDTO contestAffiliatesDTO = gson.fromJson(((JsonObject)jsonObject.get("contestAffiliatesDTO")), ContestAffiliatesDTO.class);
			
			if(contestAffiliatesDTO.getStatus() == 1){
				model.addAttribute("msg", contestAffiliatesDTO.getMessage());
			}else{
				model.addAttribute("msg", contestAffiliatesDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("affiliates", contestAffiliatesDTO.getContestAffiliatesList());
				model.addAttribute("pagingInfo", contestAffiliatesDTO.getPaginationDTO());
			}else{
				model.addAttribute("affiliates", gson.toJson(contestAffiliatesDTO.getContestAffiliatesList()));
				model.addAttribute("pagingInfo", gson.toJson(contestAffiliatesDTO.getPaginationDTO()));
			}
			model.addAttribute("status", contestAffiliatesDTO.getStatus());
			model.addAttribute("affStatus", status);
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-contest-affiliate-details";		
	}
	
	
	
	@RequestMapping(value = "/UpdateContestAffiliate")
	public String updateContestAffiliate(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{
			String customerId = request.getParameter("customerId");
			String rewardType = request.getParameter("rewardType");
			String action = request.getParameter("action");
			String rewardValue = request.getParameter("rewardValue");
			String liveCount = request.getParameter("livesCount");
			String affilaiteLivesCount = request.getParameter("affilaiteLivesCount");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String custRewardDollar = request.getParameter("custRewardDollar");
			String custSuperFanStars = request.getParameter("custSuperFanStars");
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerId);
			map.put("rewardType", rewardType);
			map.put("action", action);
			map.put("rewardValue", rewardValue);
			map.put("liveCount", liveCount);
			map.put("affilaiteLivesCount", affilaiteLivesCount);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("userName", userName);
			map.put("custRewardDollar", custRewardDollar);
			map.put("custSuperFanStars", custSuperFanStars);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CONTEST_AFFILIATES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestAffiliatesDTO affiliateDTO = gson.fromJson(((JsonObject)jsonObject.get("contestAffiliatesDTO")), ContestAffiliatesDTO.class);
			
			if(affiliateDTO.getStatus() == 1){
				model.addAttribute("status",affiliateDTO.getStatus());
				model.addAttribute("msg", affiliateDTO.getMessage());
				if(action.equalsIgnoreCase("EDIT")){
					model.addAttribute("affiliate", affiliateDTO.getContestAffiliates());
				}
			}else{
				model.addAttribute("status",affiliateDTO.getStatus());
				model.addAttribute("msg", affiliateDTO.getError().getDescription());
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";		
	}
	
	
	
	@RequestMapping(value = "/ChangeStatusContestAffiliate")
	public String changeStatusContestAffiliate(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{
			String customerId = request.getParameter("customerId");
			String status = request.getParameter("status");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerId);
			map.put("status",status);
			map.put("userName",userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHANGE_CONTEST_AFFILIATES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericDTO.getStatus() == 1){
				model.addAttribute("status",genericDTO.getStatus());
				model.addAttribute("msg", genericDTO.getMessage());
			}else{
				model.addAttribute("status",genericDTO.getStatus());
				model.addAttribute("msg", genericDTO.getError().getDescription());
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";		
	}
	
	
	
	@RequestMapping(value = "/ContestAffiliatesEarning")
	public String getContestAffiliateEarningDetails(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{
			String customerId = request.getParameter("customerId");
			String headerFilter = request.getParameter("headerFilter");
			String referralStatus = request.getParameter("referralStatus");
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerId);
			map.put("headerFilter",headerFilter);
			map.put("referralStatus",referralStatus);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTEST_AFFILIATES_EARNING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestAffiliateEarningDTO contestAffiliatesDTO = gson.fromJson(((JsonObject)jsonObject.get("contestAffiliateEarningDTO")), ContestAffiliateEarningDTO.class);
			
			if(contestAffiliatesDTO.getStatus() == 1){
				model.addAttribute("msg", contestAffiliatesDTO.getMessage());
				model.addAttribute("earnings", contestAffiliatesDTO.getAffiliateEarnings());
				model.addAttribute("pagingInfo",contestAffiliatesDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", contestAffiliatesDTO.getError().getDescription());
			}
			model.addAttribute("status", contestAffiliatesDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";		
	}
	
	
	
	@RequestMapping(value = "/SaveContestAffiliateSetting")
	public String saveContestAffiliateSettings(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{
			String customerId = request.getParameter("customerId");
			String headerFilter = request.getParameter("headerFilter");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId", customerId);
			map.put("headerFilter",headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTEST_AFFILIATES_EARNING);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestAffiliateEarningDTO contestAffiliatesDTO = gson.fromJson(((JsonObject)jsonObject.get("contestAffiliateEarningDTO")), ContestAffiliateEarningDTO.class);
			
			if(contestAffiliatesDTO.getStatus() == 1){
				model.addAttribute("msg", contestAffiliatesDTO.getMessage());
				model.addAttribute("earnings", contestAffiliatesDTO.getAffiliateEarnings());
				model.addAttribute("pagingInfo",contestAffiliatesDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", contestAffiliatesDTO.getError().getDescription());
			}
			model.addAttribute("status", contestAffiliatesDTO.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";		
	}
	
	/*
	@RequestMapping(value = "/GetAffiliates", method = RequestMethod.POST)
	public void getAffiliatesSearchPage(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count = 0;
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		String status = request.getParameter("status");
		Map<Integer, AffiliateCashReward> cashRewardMapByUserId = new HashMap<Integer, AffiliateCashReward>();
		Collection<TrackerUser> trackerUsers = null;
		JSONObject object = new JSONObject();
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliatesSearchHeaderFilters(headerFilter);
			trackerUsers = DAORegistry.getTrackerUserDAO().getAllTrackerUsersByRole("ROLE_AFFILIATES", status, filter, pageNo);
			Collection<AffiliateCashReward> rewardList = DAORegistry.getAffiliateCashRewardDAO().getAll();
			
			for (AffiliateCashReward affiliateCashReward : rewardList) {
				cashRewardMapByUserId.put(affiliateCashReward.getUser().getId(), affiliateCashReward);
			}
			if(trackerUsers != null && trackerUsers.size() > 0){
				count = trackerUsers.size();
			}else if(trackerUsers == null || trackerUsers.size() <= 0){
				object.put("msg", "No "+status+" Affiliates found.");
			}
			
			object.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			object.put("affiliates", JsonWrapperUtil.getManageAffiliatesArray(trackerUsers,cashRewardMapByUserId));
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();			
		}		
	}
	*/
	
	@RequestMapping(value = "/GetAffiliatesOrder")
	public String getAffiliatesOrderData(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try{
			String userId = request.getParameter("userId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userId", userId);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_AFFILIATES_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AffiliateCashRewardHistoryDTO affiliateCashRewardHistoryDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliateCashRewardHistoryDTO")), AffiliateCashRewardHistoryDTO.class);
			
			if(affiliateCashRewardHistoryDTO.getStatus() == 1){
				model.addAttribute("affiliatePagingInfo", affiliateCashRewardHistoryDTO.getPaginationDTO());
				model.addAttribute("affiliatesOrder", affiliateCashRewardHistoryDTO.getAffiliatesOrderDTO());
				model.addAttribute("msg", affiliateCashRewardHistoryDTO.getMessage());
				model.addAttribute("status", affiliateCashRewardHistoryDTO.getStatus());
			}else{
				model.addAttribute("msg", affiliateCashRewardHistoryDTO.getError().getDescription());
				model.addAttribute("status", affiliateCashRewardHistoryDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*
	 * Getting Affiliate Report
	 */
	@RequestMapping(value = "/AffiliateReport")
	public String getAffiliateReportPage(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){
		
		try{			
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userId", trackerUserSession.getId().toString());
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AFFILIATE_REPORT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AffiliateReportDTO affiliateReportDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliateReportDTO")), AffiliateReportDTO.class);
			
			if(affiliateReportDTO.getStatus() == 1){
				model.addAttribute("msg", affiliateReportDTO.getMessage());
				model.addAttribute("status", affiliateReportDTO.getStatus());
			}else{
				model.addAttribute("msg", affiliateReportDTO.getError().getDescription());
				model.addAttribute("status", affiliateReportDTO.getStatus());
			}

			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", affiliateReportDTO.getPaginationDTO());
				model.addAttribute("affiliates", affiliateReportDTO.getAffiliatesOrderDTO());
				model.addAttribute("affiliateCashReward", affiliateReportDTO.getAffiliateCashRewardDTO());
				model.addAttribute("promoCodeHistory", affiliateReportDTO.getAffiliatePromoCodeHistoryDTO());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(affiliateReportDTO.getPaginationDTO()));
				model.addAttribute("affiliates", gson.toJson(affiliateReportDTO.getAffiliatesOrderDTO()));				
				model.addAttribute("affiliateCashReward", affiliateReportDTO.getAffiliateCashRewardDTO());
				model.addAttribute("promoCodeHistory", affiliateReportDTO.getAffiliatePromoCodeHistoryDTO());
			}
			/*
			Integer count = 0;
			AffiliateCashReward affiliateCashReward = null;
			Collection<AffiliateCashRewardHistory> historyList = null;
			com.rtw.tracker.data.AffiliatePromoCodeHistory history = null;
			GridHeaderFilters filter = new GridHeaderFilters();
			
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(trackerUserSession.getId());
			historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(trackerUserSession.getId(), filter);			
			count = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryCountByUserId(trackerUserSession.getId(), filter);
			history = DAORegistry.getAffiliatePromoCodeHistoryDAO().getActiveAffiliatePromoCode(trackerUserSession.getId());
			if(affiliateCashReward == null){
				affiliateCashReward = new AffiliateCashReward();
				affiliateCashReward.setActiveCash(0.00);
				affiliateCashReward.setLastCreditedCash(0.00);
				affiliateCashReward.setLastDebitedCash(0.00);
				affiliateCashReward.setLastVoidCash(0.00);
				affiliateCashReward.setPendingCash(0.00);
				affiliateCashReward.setTotalCreditedCash(0.00);
				affiliateCashReward.setTotalDebitedCash(0.00);
				affiliateCashReward.setTotalVoidedCash(0.00);
				affiliateCashReward.setTotalPaidCash(0.00);
			}
		map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
		map.put("affiliates", JsonWrapperUtil.getAffiliatesArray(historyList));
		map.put("affiliateCashReward", affiliateCashReward);
		map.put("promoCodeHistory",history);*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}		
		return "page-affiliate-report";		
	}
	
	/*	
	@RequestMapping(value = "/GetAffiliateReport", method = RequestMethod.POST)
	public void getAffiliateReportData(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count =0;		
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		
		AffiliateCashReward affiliateCashReward = null;
		Collection<AffiliateCashRewardHistory> historyList = null;
		JSONObject object = new JSONObject();
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliateReportSearchHeaderFilters(headerFilter);
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(trackerUserSession.getId());
			historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(trackerUserSession.getId(), filter);			
			count = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryCountByUserId(trackerUserSession.getId(), filter);
			
			object.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			object.put("affiliates", JsonWrapperUtil.getAffiliatesArray(historyList));
			object.put("affiliateCashReward", affiliateCashReward);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	@RequestMapping({"/GetAffiliateOrderSummary"})
	public String getAffiliateOrderSummary(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try{
			String userId = request.getParameter("userId");
			String orderId = request.getParameter("orderId");
			
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(StringUtils.isEmpty(userId)){
				userId = String.valueOf(trackerUserSession.getId());
			}
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userId", userId);
			map.put("orderId", orderId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AFFILIATES_ORDER_SUMMARY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AffiliateOrderSummaryDTO affiliateOrderSummaryDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliateOrderSummaryDTO")), AffiliateOrderSummaryDTO.class);
			
			if(affiliateOrderSummaryDTO.getStatus() == 1){
				model.addAttribute("affiliateHistory", affiliateOrderSummaryDTO.getRewardHistory());
				model.addAttribute("activeDate", affiliateOrderSummaryDTO.getActiveDate());
				model.addAttribute("msg", affiliateOrderSummaryDTO.getMessage());
				model.addAttribute("status", affiliateOrderSummaryDTO.getStatus());
			}else{
				model.addAttribute("msg", affiliateOrderSummaryDTO.getError().getDescription());
				model.addAttribute("status", affiliateOrderSummaryDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*@RequestMapping(value = "/AffiliateOrderDetails", method = RequestMethod.GET)
	public String getAffiliateReportDetails(HttpServletRequest request, HttpServletResponse response,HttpSession session,ModelMap map){
		Integer count = 0;
		AffiliateCashReward affiliateCashReward = null;
		AffiliateCashRewardHistory rewardHistory = null;
		try{
			GridHeaderFilters filter = new GridHeaderFilters();
			String userId = request.getParameter("userId");
			String orderId = request.getParameter("orderId");
			
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(userId != null && !userId.isEmpty()){
				affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(Integer.parseInt(userId));
				rewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(Integer.parseInt(userId), Integer.parseInt(orderId));
			}else{
				affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(trackerUserSession.getId());
				rewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserIdAndOrderId(trackerUserSession.getId(), Integer.parseInt(orderId));
			}
		     String eventDateTime = rewardHistory.getOrder().getEventDateStr();
		     
		     if(rewardHistory.getOrder().getEventTimeStr().equals("TBD")){
		      eventDateTime = eventDateTime +" 12:00 AM";
		     }else{
		      eventDateTime = eventDateTime +" "+rewardHistory.getOrder().getEventTimeStr();
		     }
		     
		     SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		     Date activeDate = dateTimeFormat.parse(eventDateTime);
		     Calendar calendar = Calendar.getInstance();
		     calendar.setTime(activeDate);
		     calendar.add(Calendar.DAY_OF_MONTH, 1);
		     
		     activeDate = calendar.getTime();
		     String activeDateStr = dateTimeFormat.format(activeDate);
		     map.put("activeDate", activeDateStr);
		}catch(Exception e){
			e.printStackTrace();
			map.put("error", "There is something wrong..Please Try Again.");
		}
		map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
		map.put("affiliateHistory", rewardHistory);
		map.put("affiliateCashReward", affiliateCashReward);		
		return "page-affiliate-order-summary";		
	}*/
	
	/*@RequestMapping(value = "/ExportAffiliatesDetails", method = RequestMethod.GET)
	public void exportAffiliatesDetailsToExcel(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		Integer count =0;
		String pageNo = request.getParameter("pageNo");
		String headerFilter = request.getParameter("headerFilter");
		
		Collection<AffiliateCashReward> rewardList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliatesSearchHeaderFilters(headerFilter);
			
			rewardList = DAORegistry.getAffiliateCashRewardDAO().getAllAffiliates(filter);			
			count = DAORegistry.getAffiliateCashRewardDAO().getAllAffiliatesCount(filter);
			
			
			if(rewardList!=null && !rewardList.isEmpty()){
				HSSFWorkbook book = new HSSFWorkbook();
				HSSFSheet sheet = book.createSheet("AffiliatesDetails");
				HSSFRow header = sheet.createRow(0);
				header.createCell(0).setCellValue("First Name");
				header.createCell(1).setCellValue("Last Name");
				header.createCell(2).setCellValue("Active Cash");
				header.createCell(3).setCellValue("Pending Cash");
				header.createCell(4).setCellValue("Last Credited Cash");
				header.createCell(5).setCellValue("Last Debited Cash");
				header.createCell(6).setCellValue("Total Credited Cash");
				header.createCell(7).setCellValue("Total Debited Cash");
				header.createCell(8).setCellValue("Last Update");
				int i=1;
				for(AffiliateCashReward reward : rewardList){
					HSSFRow row = sheet.createRow(i);
					row.createCell(0).setCellValue(reward.getUser().getFirstName());
					row.createCell(1).setCellValue(reward.getUser().getLastName());
					row.createCell(2).setCellValue(reward.getActiveCash());
					row.createCell(3).setCellValue(reward.getPendingCash());
					row.createCell(4).setCellValue(reward.getLastCreditedCash());
					row.createCell(5).setCellValue(reward.getLastDebitedCash());
					row.createCell(6).setCellValue(reward.getTotalCreditedCash());
					row.createCell(7).setCellValue(reward.getTotalDebitedCash());
					row.createCell(8).setCellValue(reward.getLastUpdatedDateStr());
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=AffiliatesDetails.xls");
				book.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	/*@RequestMapping(value = "/ExportAffiliateReport")
	public void exportAffiliateReportToExcel(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		String userId = request.getParameter("userId");
		String headerFilter = request.getParameter("headerFilter");
		
		Collection<AffiliateCashRewardHistory> historyList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getAffiliateReportSearchHeaderFilters(headerFilter);
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			
			if(userId != null && !userId.isEmpty()){
				historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(Integer.parseInt(userId), filter);
			}else{
				historyList = DAORegistry.getAffiliateCashRewardHistoryDAO().getHistoryByUserId(trackerUserSession.getId(), filter);
			}
			
			if(historyList!=null && !historyList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Affiliate_Customer_Orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Order ID");
				header.createCell(1).setCellValue("First Name");
				header.createCell(2).setCellValue("Last Name");
				header.createCell(3).setCellValue("Event Name");
				header.createCell(4).setCellValue("Event Date");
				header.createCell(5).setCellValue("Event Time");
				header.createCell(6).setCellValue("Quantity");
				header.createCell(7).setCellValue("Order Total");
				header.createCell(8).setCellValue("Cash Credited");
				header.createCell(9).setCellValue("Status");
				header.createCell(10).setCellValue("Promotional Code");
				int i=1;
				for(AffiliateCashRewardHistory history : historyList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(history.getOrder().getId());
					row.createCell(1).setCellValue(history.getCustomer()!=null?history.getCustomer().getCustomerName():"");
					row.createCell(2).setCellValue((history.getCustomer()!=null&&history.getCustomer().getLastName()!=null)?history.getCustomer().getLastName():"");
					row.createCell(3).setCellValue(history.getOrder()!=null?history.getOrder().getEventName():"");
					row.createCell(4).setCellValue(history.getOrder()!=null?history.getOrder().getEventDateStr():"");
					row.createCell(5).setCellValue(history.getOrder()!=null?history.getOrder().getEventTimeStr():"");
					row.createCell(6).setCellValue(history.getOrder()!=null?history.getOrder().getQty():0);
					row.createCell(7).setCellValue(history.getOrderTotal()!=null?history.getOrderTotal():0);
					row.createCell(8).setCellValue(history.getCreditedCash()!=null?history.getCreditedCash():0);
					row.createCell(9).setCellValue(history.getRewardStatus()!=null?history.getRewardStatus().toString():"");
					row.createCell(10).setCellValue(history.getPromoCode()!=null?history.getPromoCode():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Affiliate_Customer_Orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	public static boolean validateEMail(String email){
		try{
			final String emailPattern = 
					"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			Pattern pattern;
			Matcher matcher;
			
			pattern = Pattern.compile(emailPattern);
			matcher = pattern.matcher(email);
			return matcher.matches();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Method to encrypt the password
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String encryptPwd(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	@RequestMapping("/GetAffiliatePromoHistory")
	public String getAffiliatePromoHistory(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model)throws Exception {
		
		try {
			String userId = request.getParameter("userId");
			
			if(userId!=null && !userId.isEmpty()){
				Map<String, String> map = Util.getParameterMap(request);				
				map.put("userId", userId);
				
				String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_AFFILIATES_PROMO_HISTORY);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				AffiliatePromoCodeHistoryDTO affiliatePromoCodeHistoryDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliatePromoCodeHistoryDTO")), AffiliatePromoCodeHistoryDTO.class);
				
				if(affiliatePromoCodeHistoryDTO.getStatus() == 1){
					model.addAttribute("promoCodeHistory", affiliatePromoCodeHistoryDTO.getAffiliatePromoCodeHistories());
					model.addAttribute("pagingInfo", affiliatePromoCodeHistoryDTO.getPaginationDTO());
					model.addAttribute("msg", affiliatePromoCodeHistoryDTO.getMessage());
					model.addAttribute("status", affiliatePromoCodeHistoryDTO.getStatus());
				}else{
					model.addAttribute("msg", affiliatePromoCodeHistoryDTO.getError().getDescription());
					model.addAttribute("status", affiliatePromoCodeHistoryDTO.getStatus());
				}
			}else{
				model.addAttribute("msg", "Please select user to get Affiliates Promo Code History.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/*
	@RequestMapping("/GetAffiliatePromoHistory")
	public String getAffiliatePromoHistory(HttpServletRequest request, HttpServletResponse response, ModelMap map)throws Exception {
		String userId = request.getParameter("userId");
		List<AffiliatePromoCodeHistory> affiliatePromoCodeHistories = null;
		JSONArray array = new JSONArray();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		try {
			if(userId!=null && !userId.isEmpty()){
				affiliatePromoCodeHistories = DAORegistry.getAffiliatePromoCodeHistoryDAO().getAllHistoryByUserId(Integer.parseInt(userId.trim()));
				JSONObject object = null;
				for(AffiliatePromoCodeHistory obj :affiliatePromoCodeHistories){
					object = new JSONObject();
					object.put("id", obj.getId());
					object.put("promoCode",obj.getPromoCode());
					object.put("createDate",dateFormat.format(obj.getCreateDate()));
					object.put("lastUpdated",dateFormat.format(obj.getUpdateDate()));
					object.put("status",obj.getStatus());
					object.put("fromDate",obj.getEffectiveFromDate()!= null?dateFormat.format(obj.getEffectiveFromDate()):"");
					object.put("toDate",obj.getEffectiveToDate()!=null?dateFormat.format(obj.getEffectiveToDate()):"");
					array.put(object);
				}
			}
			//IOUtils.write(array.toString().getBytes(),response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("promoCodeHistory", array);
		map.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(affiliatePromoCodeHistories!=null?affiliatePromoCodeHistories.size():0));
		return "page-affiliates-promocode-history";
	}
	*/
		
	@RequestMapping(value = "/GenerateAffiliatePromoCode")
	public String generateAffiliatePromoCode(HttpServletRequest request, HttpServletResponse response,HttpSession session, Model model){
		
		try{
			String userId = request.getParameter("userId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String promoCode = request.getParameter("promoCode");

			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userId", userId);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("promoCode", promoCode);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GENERATE_AFFILIATES_PROMO_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AffiliatePromoCodeDTO affiliatePromoCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliatePromoCodeDTO")), AffiliatePromoCodeDTO.class);
			
			if(affiliatePromoCodeDTO.getStatus() == 1){
				model.addAttribute("msg", affiliatePromoCodeDTO.getPromoCode());
			}else{
				model.addAttribute("msg", affiliatePromoCodeDTO.getError().getDescription());
			}
			model.addAttribute("status", affiliatePromoCodeDTO.getStatus());
			
		}catch(Exception e){
			e.printStackTrace();
			return "There is something wrong. Please try again.";
		}
		return "";
	}

	
	@RequestMapping(value = "/CheckAffiliatePromoCode")
	public String checkAffiliatePromoCode(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			Integer userId = trackerUserSession.getId();
			String promoCode = request.getParameter("promoCode");

			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userId", userId.toString());
			map.put("promoCode", promoCode);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_AFFILIATE_PROMOCODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			model.addAttribute("msg", genericResponseDTO.getMessage());
						
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	
	@RequestMapping("/AutoCompleteCityStateCountry")
	public void getAutoCompleteCityStateCountry(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		try{
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_CITY_STATE_COUNTRY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteDTO autoCompleteDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteDTO")), AutoCompleteDTO.class);
						
			if(autoCompleteDTO.getStatus() == 1){
				writer.write(autoCompleteDTO.getAutoCompleteStringResponse());
			}
			
			/*String param = request.getParameter("q");
			String strResponse ="";
			List<LoyalFanStateSearch> stateList = null;
			
			Map<String, String> map = Util.getParameterMap(request);			
		    map.put("searchKey", param);
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.AUTOCOMPLETE_LOYALFAN);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			LoyalFanAutoSearch autoSearchList = gson.fromJson(((JsonObject)jsonObject.get("loyalFanAutoSearch")), LoyalFanAutoSearch.class);
			if(autoSearchList.getStatus() == 1){
				stateList = autoSearchList.getSearchList();
			}
			if (stateList == null) {
				return;
			}
			if (stateList != null) {
				for (LoyalFanStateSearch state : stateList) {
					strResponse += ("STATE" + "|" + state.getLoyalFanSelectionValue() + "|" + state.getCity() + "|" + state.getState() + "|" + state.getCountry() + "|" + state.getZipCode() + "|" + state.getSearchGridValue() + "|" + state.getBoxDisplayValue() + "\n") ;
				}
			}
			
			writer.write(strResponse);*/
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
	
	
	@RequestMapping("/AutoCompleteCustomerArtist")
	public void getAutoCompleteCustomerArtist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_CUSTOMER_ARTIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteDTO autoCompleteDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteDTO")), AutoCompleteDTO.class);
						
			if(autoCompleteDTO.getStatus() == 1){
				writer.write(autoCompleteDTO.getAutoCompleteStringResponse());
			}
			
			/*String param = request.getParameter("q");
			PrintWriter writer =  response.getWriter();
			String strResponse ="";
			Collection<Artist> artists = null;
			Map<String, String> map = Util.getParameterMap(request);
		    map.put("searchKey", param);
		    map.put("parentType", "SPORTS");
		    String data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.AUTOCOMPLETE_ARTIST);
		    Gson gson = new Gson();  
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ArtistList artistList = gson.fromJson(((JsonObject)jsonObject.get("artistList")), ArtistList.class);
			if(artistList.getStatus() == 1){
				artists = artistList.getArtists();
			}
			if (artists == null) {
				return;
			}
			if (artists != null) {
				for (Artist artist : artists) {
					strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
				}
			}
			
			writer.write(strResponse);*/
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("/AutoCompleteEventDetails")
	public void getAutoCompleteEventDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_EVENT_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteEventsDTO autoCompleteEventsDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteEventsDTO")), AutoCompleteEventsDTO.class);
						
			if(autoCompleteEventsDTO.getStatus() == 1){
				writer.write(autoCompleteEventsDTO.getEvents());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<Event> events = DAORegistry.getQueryManagerDAO().getAllActiveEventDetailsWithVenue(param);
		
		if (events == null) {
			return;
		}
		if (events != null) {
			for (Event event : events) {
				strResponse += ("EVENT" + "|" + event.getEventId() + "|" + event.getEventName() + "|" + event.getNameWithDateandVenue() + "\n") ;
			}
		}		
		writer.write(strResponse);*/
	}
	
	@RequestMapping({"/GetFantasyTicketValidation"})
	public String getFantasyTicketValidation(HttpServletRequest request, HttpServletResponse response, Model model){
				
		try{			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_FANTASY_TICKET_VALIDATION);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			FantasyTicketValidationDTO fantasyTicketValidationDTO = gson.fromJson(((JsonObject)jsonObject.get("fantasyTicketValidationDTO")), FantasyTicketValidationDTO.class);
						
 			if(fantasyTicketValidationDTO.getStatus() == 1){
				model.addAttribute("msg", fantasyTicketValidationDTO.getMessage());
				model.addAttribute("status", fantasyTicketValidationDTO.getStatus());
				model.addAttribute("proceedPurchase", fantasyTicketValidationDTO.getProceedPurchase());
			}else{
				model.addAttribute("msg", fantasyTicketValidationDTO.getError().getDescription());
				model.addAttribute("status", fantasyTicketValidationDTO.getStatus());
			}
 			
			/*String customerIdStr = request.getParameter("customerId");
			String fantasyCategoryIdStr = request.getParameter("grandChildCategoryId");
			String fantasyEventIdStr = request.getParameter("fantasyEventId");
			String fantasyTeamIdStr = request.getParameter("fantasyTeamId");
			String fantasyTicketIdStr = request.getParameter("fantasyTicketId");
			String fantasyTeamZoneIdStr = request.getParameter("fantasyTeamZoneId");
			String isRealTicketStr = request.getParameter("isRealTicket");
			String zoneStr = request.getParameter("zone");
			String quantityStr = request.getParameter("quantity");
			String requiredPointsStr = request.getParameter("requiredPoints");
			
			JSONObject jsonObject =  new JSONObject();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("customerId",customerIdStr);
			map.put("fCategoryId",fantasyCategoryIdStr);
			map.put("fEventId",fantasyEventIdStr);
			map.put("fTeamId",fantasyTeamIdStr);
			map.put("fTicketId",fantasyTicketIdStr);
			map.put("fTeamZoneId",fantasyTeamZoneIdStr);
			map.put("isRealTicket",isRealTicketStr);
			map.put("zone",zoneStr);
			map.put("quantity",quantityStr);
			map.put("requiredPoints",requiredPointsStr);
			
			String data = Util.getObject(map, sharedProperty.getApiUrl()+Constants.VALIDATE_FST_ORDER);
			Gson gson = new Gson();		
			JsonObject jsonObject1 = gson.fromJson(data, JsonObject.class);
			FantasySportsProduct fantasySportsProduct = gson.fromJson(((JsonObject)jsonObject1.get("fantasySportsProduct")), FantasySportsProduct.class);
			if(fantasySportsProduct.getStatus() == 1){
				jsonObject.put("msg", fantasySportsProduct.getTicTrackerMessage());
				if(fantasySportsProduct.getProceedPurchase() != null && fantasySportsProduct.getProceedPurchase()){
					jsonObject.put("proceedPurchase", "Yes");
				}else{
					jsonObject.put("proceedPurchase", "No");
				}
			}
			
			IOUtils.write(jsonObject.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping({"/GetCityStateCountry"})
	 public String getCityStateCountry(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String zipCode = request.getParameter("zipCode");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("zipCode", zipCode);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_SEARCH_CITY_STATE_COUNTRY);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CityStateCountryAutoSearchDTO cityStateCountryAutoSearchDTO = gson.fromJson(((JsonObject)jsonObject.get("cityStateCountryAutoSearchDTO")), CityStateCountryAutoSearchDTO.class);
			
			if(cityStateCountryAutoSearchDTO.getStatus() == 1){
				model.addAttribute("msg", cityStateCountryAutoSearchDTO.getMessage());
				model.addAttribute("status", cityStateCountryAutoSearchDTO.getStatus());
				model.addAttribute("cityStateCountryDetails", cityStateCountryAutoSearchDTO.getCityStateCountryDTO());
				model.addAttribute("countryList", cityStateCountryAutoSearchDTO.getCountryDTO());
				model.addAttribute("stateList", cityStateCountryAutoSearchDTO.getStateDTO());
			}else{
				model.addAttribute("msg", cityStateCountryAutoSearchDTO.getError().getDescription());
				model.addAttribute("status", cityStateCountryAutoSearchDTO.getStatus());
			}
			/*List<Country> countryList = null;
			List<State> stateList = null;
			List<CityAutoSearch> cityStateCountryList = null;
			CityAutoSearch cityStateCountry = null;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			stateList = DAORegistry.getQueryManagerDAO().getStates();
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			cityStateCountryList = DAORegistry.getQueryManagerDAO().getCityStateCountry(zipCodeStr);
			
			if(cityStateCountryList != null && cityStateCountryList.size() > 0){
				JSONObject cityStateCountryObject = new JSONObject();
				cityStateCountry = cityStateCountryList.get(0);				
				cityStateCountryObject.put("id", cityStateCountry.getId());
				cityStateCountryObject.put("city", cityStateCountry.getCity());
				cityStateCountryObject.put("stateId", cityStateCountry.getStateId());
				cityStateCountryObject.put("state", cityStateCountry.getState());
				cityStateCountryObject.put("stateName", cityStateCountry.getStateName());
				cityStateCountryObject.put("countryId", cityStateCountry.getCountryId());
				cityStateCountryObject.put("country", cityStateCountry.getCountry());
				cityStateCountryObject.put("countryName", cityStateCountry.getCountryName());			
				jObj.put("cityStateCountryDetails", cityStateCountryObject);
			}
			if(countryList != null){
				JSONArray countryListArray = new JSONArray();
				JSONObject countryListObject = null;
				for(Country country : countryList){
					countryListObject = new JSONObject();
					countryListObject.put("id", country.getId());
					countryListObject.put("name", country.getName());
					countryListArray.put(countryListObject);
				}
				jObj.put("countryList", countryListArray);
			}
			if(stateList != null){
				JSONArray stateListArray = new JSONArray();
				JSONObject stateListObject = null;
				for(State state : stateList){
					stateListObject = new JSONObject();
					stateListObject.put("id", state.getId());
					stateListObject.put("name", state.getName());
					stateListArray.put(stateListObject);
				}
				jObj.put("stateList", stateListArray);
			}
			
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping("/AutoCompleteBrokers")
	public void getAutoCompleteBrokers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		try{
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_BROKERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteDTO autoCompleteDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteDTO")), AutoCompleteDTO.class);
						
			if(autoCompleteDTO.getStatus() == 1){
				writer.write(autoCompleteDTO.getAutoCompleteStringResponse());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse ="";
		Collection<TrackerBrokers> trackerBrokers = DAORegistry.getTrackerBrokersDAO().getBrokersByName(param);
		
		if (trackerBrokers == null) {
			return;
		}
		if (trackerBrokers != null) {
			for (TrackerBrokers trackerBroker : trackerBrokers) {
				strResponse += ("BROKER" + "|" + trackerBroker.getId() + "|" + trackerBroker.getCompanyName() + "\n") ;
			}
		}
		
		writer.write(strResponse);*/
	}
	
	
	//Menu - Lottery Section
	@RequestMapping("/AutoCompleteArtistAndChildAndGrandChildCategory")
	public void getAutoCompleteArtistAndChildAndGrandChildCategory(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		try{
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_ARTIST_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistAndCategoryDTO")), AutoCompleteArtistAndCategoryDTO.class);
						
			if(autoCompleteArtistAndCategoryDTO.getStatus() == 1){
				writer.write(autoCompleteArtistAndCategoryDTO.getArtistAndCategory());
			}
		
		/*String param = request.getParameter("q");
		PrintWriter writer =  response.getWriter();
		String strResponse = "";
		
		Collection<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getGrandChildCategoriesByNameAndDisplayOnSearch(param);
		Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getChildCategoriesByNameAndDisplayOnSearch(param);
		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);		
				
		if (childCategories != null) {
			for (ChildCategory childCategory : childCategories) {
				strResponse += ("CHILD" + "|" + childCategory.getId() + "|" + childCategory.getName() + "\n");
			}
		}
		
		if (grandChildCategories != null) {
			for (GrandChildCategory grandChildCateogry : grandChildCategories) {
				strResponse += ("GRAND" + "|" + grandChildCateogry.getId() + "|" + grandChildCateogry.getName() + "\n");
			}
		}
		
		if (artists != null) {
			for (Artist artist : artists) {
				strResponse += ("ARTIST" + "|" + artist.getId() + "|" + artist.getName() + "\n") ;
			}
		}		
		
		writer.write(strResponse);*/
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/Lotteries")
	public String loadLotteriesPage(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_LOTTERIES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ReferralContestDTO referralContestDTO = gson.fromJson(((JsonObject)jsonObject.get("referralContestDTO")), ReferralContestDTO.class);
			
			if(referralContestDTO.getStatus() == 1){				
				model.addAttribute("msg", referralContestDTO.getMessage());
				model.addAttribute("status", referralContestDTO.getStatus());
			}else{
				model.addAttribute("msg", referralContestDTO.getError().getDescription());
				model.addAttribute("status", referralContestDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("contestsList", referralContestDTO.getContestsListDTO());
				model.addAttribute("contestsPagingInfo", referralContestDTO.getPaginationDTO());
			}else{
				model.addAttribute("contestsList", gson.toJson(referralContestDTO.getContestsListDTO()));
				model.addAttribute("contestsPagingInfo", gson.toJson(referralContestDTO.getPaginationDTO()));
				model.addAttribute("participantsPagingInfo", gson.toJson(referralContestDTO.getParticipantsPaginationDTO()));
			}
			
			/*GridHeaderFilters filter = new GridHeaderFilters();
			List<Contest> contestList = DAORegistry.getContestDAO().getAllContests(null, filter);
			map.put("contestsPagingInfo", PaginationUtil.getDummyPaginationParameter(contestList!=null?contestList.size():0));
			map.put("participantsPagingInfo", PaginationUtil.getDummyPaginationParameter(0));
			map.put("contestsList", JsonWrapperUtil.getContestArray(contestList));*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-lotteries";
	}
	
	/*@RequestMapping(value = "/GetLotteries", method = RequestMethod.POST)
	public String getLotteries(HttpServletRequest request, HttpServletResponse response, Model model){		
		
		try {
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LOTTERIES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ReferralContestDTO referralContestDTO = gson.fromJson(((JsonObject)jsonObject.get("referralContestDTO")), ReferralContestDTO.class);
			
			if(referralContestDTO.getStatus() == 1){				
				model.addAttribute("msg", referralContestDTO.getMessage());
				model.addAttribute("status", referralContestDTO.getStatus());
				model.addAttribute("contestsList", referralContestDTO.getContestsListDTO());
				model.addAttribute("contestsPagingInfo", referralContestDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", referralContestDTO.getError().getDescription());
				model.addAttribute("status", referralContestDTO.getStatus());
			}
			
//			JSONObject object = new JSONObject();
//			object.put("status", 0);
//						
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getLotteriesFilter(headerFilter); 
//			List<Contest> contestList = DAORegistry.getContestDAO().getAllContests(null, filter);
//			object.put("contestsPagingInfo", PaginationUtil.getDummyPaginationParameter(contestList!=null?contestList.size():0));
//			object.put("contestsList", JsonWrapperUtil.getContestArray(contestList));
//			object.put("status",1);
//			
//			//return jsonarr
//			IOUtils.write(object.toString().getBytes(),response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}*/
	
		
	@RequestMapping(value = "/UpdateLottery", method = RequestMethod.POST)
	public String updateLottery(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String contestId = request.getParameter("contestId");
			String contestName = request.getParameter("contestName");			
			String artistId = request.getParameter("artistId");
			String artistName = request.getParameter("artistName");
			String contestMinPurchaseAmt = request.getParameter("contestMinPurchaseAmt");
			String contestFromDate = request.getParameter("contestFromDate");
			String contestToDate = request.getParameter("contestToDate");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("contestId", contestId);
			map.put("contestName", contestName);
			map.put("artistId", artistId);
			map.put("artistName", artistName);
			map.put("contestMinPurchaseAmt", contestMinPurchaseAmt);
			map.put("contestFromDate", contestFromDate);
			map.put("contestToDate", contestToDate);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_UPDATE_DELETE_LOTTERY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ReferralContestDTO referralContestDTO = gson.fromJson(((JsonObject)jsonObject.get("referralContestDTO")), ReferralContestDTO.class);
			
			if(referralContestDTO.getStatus() == 1){				
				model.addAttribute("msg", referralContestDTO.getMessage());
				model.addAttribute("status", referralContestDTO.getStatus());
				model.addAttribute("contestsList", referralContestDTO.getContestsListDTO());
				model.addAttribute("contestsPagingInfo", referralContestDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", referralContestDTO.getError().getDescription());
				model.addAttribute("status", referralContestDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();
			object.put("status", 0);
			if(action != null && action.equalsIgnoreCase("EDIT")){
				if(contestIdStr == null || contestIdStr.isEmpty()){
					object.put("msg", "Please select any Lottery.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				GridHeaderFilters filter = GridHeaderFiltersUtil.getLotteriesFilter(null); 
				List<Contest> contestList = DAORegistry.getContestDAO().getAllContests(Integer.parseInt(contestIdStr), filter);
				
				if(contestList != null && contestList.size() > 0){
					object.put("contestsList", JsonWrapperUtil.getContestArray(contestList));
				}
				object.put("status",1);
				
				//return jsonarr
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}else if(action != null && (action.equalsIgnoreCase("SAVE") || action.equalsIgnoreCase("UPDATE"))){
				String userActionMsg = "";
				
				if(contestName==null || contestName.isEmpty()){
					object.put("msg","Lottery Name is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				if(minPurchaseAmountStr==null || minPurchaseAmountStr.isEmpty()){
					object.put("msg","Minimum Purchase Amount is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				if(artistName==null || artistName.isEmpty()){
					object.put("msg","Artist Name is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				if(artistIdStr==null || artistIdStr.isEmpty()){
					object.put("msg","Artist id is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				Integer artistId = null;
				try{
					artistId = Integer.parseInt(artistIdStr);
				}catch(Exception e){
					e.printStackTrace();
					object.put("msg","Invalid artist id found, it should be valid Integer.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				try{
					Double.parseDouble(minPurchaseAmountStr);
				}catch(Exception e){
					e.printStackTrace();
					object.put("msg","Minimum Purchase Amount is not Valid Double value.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				if(startDateStr==null || startDateStr.isEmpty() || endDateStr == null || endDateStr.isEmpty()){
					object.put("msg","Missing from Date or To Date.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
							
				startDateStr = startDateStr + " 00:00:00";
				endDateStr = endDateStr + " 23:59:59";
				Date startDate = null;
				Date endDate = null;
				try {
					startDate = Util.getDateWithTwentyFourHourFormat1(startDateStr);
					endDate = Util.getDateWithTwentyFourHourFormat1(endDateStr);
				} catch (Exception e) {
					e.printStackTrace();
					object.put("msg","Start Date or End Date with bad values.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				Date today = new Date();
				Contest contest = null;
				
				if(action.equalsIgnoreCase("UPDATE")){
					if(contestIdStr == null || contestIdStr.isEmpty()){
						object.put("msg", "Please select any Lottery to Update.");
						IOUtils.write(object.toString().getBytes(),response.getOutputStream());
						return;
					}
					contest = DAORegistry.getContestDAO().get(Integer.parseInt(contestIdStr));
					if(contest == null){
						object.put("msg", "Lottery is not found.");
						IOUtils.write(object.toString().getBytes(),response.getOutputStream());
						return;
					}
				}
				if(contest == null){
					contest = new Contest();
				}
				contest.setName(contestName);
				contest.setMinimumPurchaseAmount(Double.parseDouble(minPurchaseAmountStr));
				contest.setStartDate(startDate);
				contest.setEndDate(endDate);
				contest.setArtistId(artistId);
				contest.setArtistName(artistName);
				contest.setStatus(true);
				
				if(action.equalsIgnoreCase("SAVE")){
					contest.setCreatedBy(userName);
					contest.setCreatedDate(today);
					contest.setContestStatus("ACTIVE");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					contest.setUpdatedBy(userName);
					contest.setUpdatedDate(today);
				}
				DAORegistry.getContestDAO().saveOrUpdate(contest);
				
				if(action.equalsIgnoreCase("SAVE")){
					userActionMsg = "Lottery Created Successfully.";
					object.put("msg", "Lottery Created successfully.");
				}
				if(action.equalsIgnoreCase("UPDATE")){
					userActionMsg = "Lottery Updated Successfully.";
					object.put("msg", "Lottery Updated successfully.");
				}
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getLotteriesFilter(null); 
				List<Contest> contestList = DAORegistry.getContestDAO().getAllContests(null, filter);
				object.put("contestsPagingInfo", PaginationUtil.getDummyPaginationParameter(contestList!=null?contestList.size():0));
				object.put("contestsList", JsonWrapperUtil.getContestArray(contestList));
				object.put("status", 1);
				
				//Tracking User Action
				Util.userActionAudit(request, contest.getId(), userActionMsg);
				
				//return jsonarr
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;			
			}else if(action != null && action.equalsIgnoreCase("DELETE")){
				
				Contest contest =  null;
				if(contestIdStr == null || contestIdStr.isEmpty()){
					object.put("msg", "Please select any Lottery to Delete.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				contest = DAORegistry.getContestDAO().get(Integer.parseInt(contestIdStr));
				if(contest == null){
					object.put("msg", "Lottery is not found.");
					IOUtils.write(object.toString().getBytes(),response.getOutputStream());
					return;
				}
				
				contest.setStatus(false);
				contest.setUpdatedBy(userName);
				contest.setUpdatedDate(new Date());
				DAORegistry.getContestDAO().update(contest);
				
				GridHeaderFilters filter = GridHeaderFiltersUtil.getLotteriesFilter(null); 
				List<Contest> contestList = DAORegistry.getContestDAO().getAllContests(null, filter);
				object.put("contestsPagingInfo", PaginationUtil.getDummyPaginationParameter(contestList!=null?contestList.size():0));
				object.put("contestsList", JsonWrapperUtil.getContestArray(contestList));
				object.put("msg", "Lottery Deleted successfully.");
				object.put("status", 1);
				
				//Tracking User Action
				String userActionMsg = "Lottery Deleted Successfully.";
				Util.userActionAudit(request, contest.getId(), userActionMsg);
				
				//return jsonarr
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;	
			}*/
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GetParticipants", method = RequestMethod.POST)
	public String getParticipants(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String contestId = request.getParameter("contestId");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LOTTERIES_PARTICIPANTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ReferralContestParticipantsDTO referralContestParticipantsDTO = gson.fromJson(((JsonObject)jsonObject.get("referralContestParticipantsDTO")), ReferralContestParticipantsDTO.class);
			
			if(referralContestParticipantsDTO.getStatus() == 1){				
				model.addAttribute("msg", referralContestParticipantsDTO.getMessage());
				model.addAttribute("status", referralContestParticipantsDTO.getStatus());
				model.addAttribute("participantsList", referralContestParticipantsDTO.getContestParticipantsListDTO());
				model.addAttribute("participantsPagingInfo", referralContestParticipantsDTO.getParticipantsPaginationDTO());
			}else{
				model.addAttribute("msg", referralContestParticipantsDTO.getError().getDescription());
				model.addAttribute("status", referralContestParticipantsDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();			
			object.put("status", 0);
			List<ContestParticipants> participantsList = null;
			GridHeaderFilters filter = GridHeaderFiltersUtil.getParticipantsFilter(headerFilter);			
			participantsList = DAORegistry.getContestParticipantsDAO().getParticipantsListByContestId(Integer.parseInt(contestIdStr), filter);
			if(participantsList == null || participantsList.size() <= 0){
				object.put("msg", "Participant is not found for Selected Lottery/Search Filter");
			}
			
			object.put("participantsPagingInfo", PaginationUtil.getDummyPaginationParameter(participantsList!=null?participantsList.size():0));
			object.put("participantsList", JsonWrapperUtil.getContestParticipantsArray(participantsList));
			object.put("status", 1);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/GetLotteryWinner", method = RequestMethod.POST)
	public String getLotteryWinner(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String contestId = request.getParameter("contestId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("contestId", contestId);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LOTTERY_WINNER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ReferralContestWinnerDTO referralContestWinnerDTO = gson.fromJson(((JsonObject)jsonObject.get("referralContestWinnerDTO")), ReferralContestWinnerDTO.class);
			
			if(referralContestWinnerDTO.getStatus() == 1){				
				model.addAttribute("msg", referralContestWinnerDTO.getMessage());
				model.addAttribute("status", referralContestWinnerDTO.getStatus());
				model.addAttribute("winner", referralContestWinnerDTO.getContestWinnerDTO());
			}else{
				model.addAttribute("msg", referralContestWinnerDTO.getError().getDescription());
				model.addAttribute("status", referralContestWinnerDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();			
			object.put("status",0);
			if(contestIdStr == null || contestIdStr.isEmpty()){
				object.put("msg", "Lottery is not found.");
				IOUtils.write(object.toString().getBytes(),response.getOutputStream());
				return;
			}
			Integer contestId = 0;
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				object.put("msg", "Bad value found for lotteryId.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			ContestWinner contestWinner = null;
			if(action!=null && action.equalsIgnoreCase("GENERATE")){
				contestWinner = DAORegistry.getContestWinnerDAO().getContestWinner(contestId);
				if(contestWinner != null){
					object.put("msg", "Winner is already selected for selected lottery.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				GridHeaderFilters filter = new GridHeaderFilters();
				List<ContestParticipants> participants = DAORegistry.getContestParticipantsDAO().getParticipantsListByContestId(contestId,filter);
				
				if(participants == null || participants.isEmpty()){
					object.put("msg", "No Participants found for selected lottery.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				Contest contest = DAORegistry.getContestDAO().get(contestId);
				List<String> cities = DAORegistry.getQueryManagerDAO().getArtistCities(contest.getArtistId());
				if(cities == null || cities.isEmpty()){
					object.put("msg", "No artist perfoming city list found for selected lottery artist.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				Random random = new Random();
				Integer index = random.nextInt(participants.size());
				ContestParticipants winner = participants.get(index);
				contestWinner = new ContestWinner();
				contestWinner.setContestId(winner.getContestId());
				contestWinner.setParticipantId(winner.getId());
				contestWinner.setCreatedOn(new Date());
				contestWinner.setCreatedBy(userName);
				contestWinner.setIsEmailed(false);
				DAORegistry.getContestWinnerDAO().save(contestWinner);
				
				Boolean isMailSent = false;
				
				com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[3];
				mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","top.png",Util.getFilePath(request, "top.png", "/resources/images/"));
				mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","bottom.png",Util.getFilePath(request, "bottom.png", "/resources/images/"));
				mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogo1.png",Util.getFilePath(request, "rtflogo1.png", "/resources/images/"));
				
				Map<String, Object> mailMap = new HashMap<String, Object>();
				mailMap.put("customerName",winner.getCustomerName());
				mailMap.put("contestName",contest.getName());
				mailMap.put("artistName",contest.getArtistName().toUpperCase());
				mailMap.put("cities",cities);
				mailMap.put("rtfUrl","https://www.rewardthefan.com/ReferralContest?contestId="+contestId+"&city=");
				
				
				try {
					mailManager.sendMailNow("text/html","sales@rewardthefan.com",winner.getCustomerEmail(), 
							null,"msanghani@rightthisway.com", "Congratulations you are a "+contest.getArtistName().toUpperCase()+" Lottery winner!" ,
				    		"email-contest-winner.html", mailMap, "text/html", null,mailAttachment,null);
				
				} catch (Exception e) {
					isMailSent = false;
					e.printStackTrace();
				}
				
				List<Contest> contests = DAORegistry.getContestDAO().getAllActiveContests();
				StringBuffer contestString = new StringBuffer();
				for(Contest con : contests){
					contestString.append("<p><b>"+con.getName()+"</b><br/><br/></p>");
				}
				
				for(ContestParticipants parti : participants){
					if(parti.getCustomerEmail().equalsIgnoreCase(winner.getCustomerEmail())){
						continue;
					}
					Map<String, Object> allMap = new HashMap<String, Object>();
					allMap.put("customerName",parti.getCustomerName());
					allMap.put("artistName",contest.getArtistName().toUpperCase());
					allMap.put("contestName",contest.getName());
					allMap.put("email",winner.getCustomerEmail());
					allMap.put("contestString",contestString);
					try {
						mailManager.sendMailNow("text/html","sales@rewardthefan.com",parti.getCustomerEmail(), 
								null,"msanghani@rightthisway.com",contest.getArtistName().toUpperCase()+" Lottery winner announced!",
					    		"email-contest-winner1.html", allMap, "text/html", null,mailAttachment,null);
						isMailSent = true;
					} catch (Exception e) {
						isMailSent = false;
						e.printStackTrace();
					}
				}
				
				if(isMailSent){
					contestWinner.setIsEmailed(true);
					DAORegistry.getContestWinnerDAO().update(contestWinner);
				}
				
				//Tracking User Action
				String userActionMsg = "Lottery Winner Announced. Winner Id is - "+winner.getId();
				Util.userActionAudit(request, contest.getId(), userActionMsg);
				
			}
			contestWinner = DAORegistry.getContestWinnerDAO().getContestWinnerDetails(contestId);
			if(contestWinner == null){
				object.put("msg","Winner is not declared yet, Please choose winner first.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			object.put("status",1);
			object.put("winner",JsonWrapperUtil.getContestWinner(contestWinner));
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	//Contests Menu
	@RequestMapping(value = "/Contests")
	public String loadContestsPage(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("status", status);
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("contestsList", contestsDTO.getContestsList());
				model.addAttribute("contestsPagingInfo", contestsDTO.getPaginationDTO());
			}else{
				if(status.equalsIgnoreCase("TODAY")){
					if(contestsDTO.getStatus() == 1){
						model.addAttribute("contest", contestsDTO.getContestsList().get(0));
					}
				}else{
					model.addAttribute("contestsList", gson.toJson(contestsDTO.getContestsList()));
					model.addAttribute("contestsPagingInfo", gson.toJson(contestsDTO.getPaginationDTO()));
				}
				
			}
			model.addAttribute("status",status);
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-contests";
	}
	
	
	@RequestMapping(value = "/HostContestPage")
	public String loadContestHostPage(HttpServletRequest request, HttpServletResponse response,Model model){

		try {
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", "TODAY");
			map.put("type", "MOBILE");
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
			}
			if(contestsDTO.getStatus() == 1){
				Contests contest = contestsDTO.getContestsList().get(0);
				if(contest.getStatus().equalsIgnoreCase("STARTED")){
					if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("ANSWER")){
						Map<String, String> qMap = Util.getParameterMap(request);
						qMap.put("contestId", contest.getId().toString());
						qMap.put("questionNo",contest.getLastQuestionNo().toString());
						
						String qData = Util.getObject(qMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LAST_QUESTION_DETAILs);
						Gson aGson = GsonCustomConfig.getGsonBuilder();		
						JsonObject qJsonObject = aGson.fromJson(qData, JsonObject.class);
						ContestQuestionDTO questionDTO = aGson.fromJson(((JsonObject)qJsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
						String buttoneText = null;
						if(questionDTO.getStatus()==1 &&  questionDTO.getNextQuestion()!=null){
							ContestQuestions q = questionDTO.getNextQuestion();
							buttoneText = Util.getResumeContestText(contest,q);
							model.addAttribute("buttonText",buttoneText);
							if(buttoneText.equalsIgnoreCase("NONE")){
								model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
							}
							model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
						}else{
							model.addAttribute("buttonText","NONE");
							model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
							model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
						}
					}else{
						model.addAttribute("buttonText",Util.getResumeContestText(contest,null));
						model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
					}
					
				}else{
					model.addAttribute("buttonText","Start Contest");
					model.addAttribute("lastQuestionNo",0);
				}
				model.addAttribute("contest",contest);
				model.addAttribute("status","TODAY");
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-contests-firebase-mobile-host-contest";
	
	}
	
	@RequestMapping(value = "/ResetContest")
	public String resetContest(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("contestId");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("userName",userName);
			map.put("action",action);
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_RESET_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
				model.addAttribute("status",contestsDTO.getStatus());
				if(action.equalsIgnoreCase("END")){
					FirestoreUtil.endContest(model);
				}
				FirestoreUtil.updateUpcomingContestDetails();
				contestLog.info("CONTEST "+action+": -- coid: "+contestId+" -- user: "+userName);
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
				model.addAttribute("status",contestsDTO.getStatus());
				contestLog.info("TICKTRACKERAPI ERROR: WHILE "+action+" CONTEST: -- coid: "+contestId+" -- user: "+userName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/ResetContestWeb")
	public String resetContestWeb(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("contestId");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("userName",userName);
			map.put("action",action);
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_RESET_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
				model.addAttribute("status",contestsDTO.getStatus());
				if(action.equalsIgnoreCase("END")){
					FirebaseWebUtil.endContest(model);
				}
				FirebaseWebUtil.updateUpcomingContestDetails();
				contestLog.info("CONTEST "+action+": -- coid: "+contestId+" -- user: "+userName);
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
				model.addAttribute("status",contestsDTO.getStatus());
				contestLog.info("TICKTRACKERAPI ERROR: WHILE "+action+" CONTEST: -- coid: "+contestId+" -- user: "+userName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/ContestsFirebaseMobile")
	public String loadContestsForFireBasePage(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("status", status);
			map.put("type", "MOBILE");
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				if(status.equalsIgnoreCase("ALL") || status.equalsIgnoreCase("EXPIRED")){
					model.addAttribute("contestsList", contestsDTO.getContestsList());
					model.addAttribute("contestsPagingInfo", contestsDTO.getPaginationDTO());
				}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
					model.addAttribute("questionBankList", contestsDTO.getQuestionBankList());
					model.addAttribute("questionBankPagingInfo", contestsDTO.getQuestionBankPaginationDTO());
				}else if(status.equalsIgnoreCase("REQUEST")){
					model.addAttribute("contestRequestList", contestsDTO.getContestEventRequestList());
					model.addAttribute("contestRequestPagingInfo", contestsDTO.getContestEventRequestPaginationDTO());
				}
			}else{
				if(status.equalsIgnoreCase("TODAY")){
					if(contestsDTO.getStatus() == 1){
						Contests contest = contestsDTO.getContestsList().get(0);
						if(contest.getStatus().equalsIgnoreCase("STARTED")){
							if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("ANSWER")){
								Map<String, String> qMap = Util.getParameterMap(request);
								qMap.put("contestId", contest.getId().toString());
								qMap.put("questionNo",contest.getLastQuestionNo().toString());
								
								String qData = Util.getObject(qMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LAST_QUESTION_DETAILs);
								Gson aGson = GsonCustomConfig.getGsonBuilder();		
								JsonObject qJsonObject = aGson.fromJson(qData, JsonObject.class);
								ContestQuestionDTO questionDTO = aGson.fromJson(((JsonObject)qJsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
								String buttoneText = null;
								if(questionDTO.getStatus()==1 &&  questionDTO.getNextQuestion()!=null){
									ContestQuestions q = questionDTO.getNextQuestion();
									buttoneText = Util.getResumeContestText(contest,q);
									model.addAttribute("buttonText",buttoneText);
									if(buttoneText.equalsIgnoreCase("NONE")){
										model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
									}
									model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
								}else{
									model.addAttribute("buttonText","NONE");
									model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
									model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
								}
							}else{
								model.addAttribute("buttonText",Util.getResumeContestText(contest,null));
								model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
							}
							
						}else{
							model.addAttribute("buttonText","Start Contest");
							model.addAttribute("lastQuestionNo",0);
						}
						model.addAttribute("contest",contest);
					}
				}else if(status.equalsIgnoreCase("ALL")  || status.equalsIgnoreCase("EXPIRED")){
					model.addAttribute("contestsList", gson.toJson(contestsDTO.getContestsList()));
					model.addAttribute("contestsPagingInfo", gson.toJson(contestsDTO.getPaginationDTO()));
				}else if(status.equalsIgnoreCase("ACTIVECONTEST") || status.equalsIgnoreCase("EXPIREDCONTEST")){
					model.addAttribute("contestsList", contestsDTO.getContestsList());
				}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
					model.addAttribute("questionBankList", gson.toJson(contestsDTO.getQuestionBankList()));
					model.addAttribute("questionBankPagingInfo", gson.toJson(contestsDTO.getQuestionBankPaginationDTO()));
				}else if(status.equalsIgnoreCase("REQUEST")){
					model.addAttribute("contestRequestList", gson.toJson(contestsDTO.getContestEventRequestList()));
					model.addAttribute("contestRequestPagingInfo", gson.toJson(contestsDTO.getContestEventRequestPaginationDTO()));
				}
				
			}
			model.addAttribute("status",status);
			
			if(status.equalsIgnoreCase("ALL") || status.equalsIgnoreCase("EXPIRED")){
				return "page-contests-firebase-mobile-allcontests";
			}else if(status.equalsIgnoreCase("ACTIVECONTEST") || status.equalsIgnoreCase("EXPIREDCONTEST")){
				return "page-contests-firebase-mobile-events";
			}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
				return "page-contests-firebase-mobile-questionbank";
			}else if(status.equalsIgnoreCase("TODAY")){
				return "page-contests-firebase-mobile-todaycontest";
			}else if(status.equalsIgnoreCase("REQUEST")){
				return "page-contests-firebase-mobile-eventrequest";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/UpdateQuestionPosition")
	public String updateQuestionPosition(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("contestId");
			String questionString = request.getParameter("questionString");
			String qId = request.getParameter("id");
			String position = request.getParameter("position");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("qId", qId);
			map.put("questionString", questionString);
			map.put("userName",userName);
			map.put("position",position);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_QUESTION_POSITION);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			
			if(questionDTO.getError() == null && questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
				model.addAttribute("questionList", questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo", questionDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());				
			}
			model.addAttribute("status", questionDTO.getStatus());
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/ContestsFirebaseWeb")
	public String loadContestsForFireBaseWeb(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("status");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("status", status);
			map.put("type", "WEB");
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				if(status.equalsIgnoreCase("ALL") || status.equalsIgnoreCase("EXPIRED")){
					model.addAttribute("contestsList", contestsDTO.getContestsList());
					model.addAttribute("contestsPagingInfo", contestsDTO.getPaginationDTO());
				}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
					model.addAttribute("questionBankList", contestsDTO.getQuestionBankList());
					model.addAttribute("questionBankPagingInfo", contestsDTO.getQuestionBankPaginationDTO());
				}else if(status.equalsIgnoreCase("REQUEST")){
					model.addAttribute("contestRequestList", contestsDTO.getContestEventRequestList());
					model.addAttribute("contestRequestPagingInfo", contestsDTO.getContestEventRequestPaginationDTO());
				}
			}else{
				if(status.equalsIgnoreCase("TODAY")){
					if(contestsDTO.getStatus() == 1){
						Contests contest = contestsDTO.getContestsList().get(0);
						if(contest.getStatus().equalsIgnoreCase("STARTED")){
							if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("ANSWER")){
								Map<String, String> qMap = Util.getParameterMap(request);
								qMap.put("contestId", contest.getId().toString());
								qMap.put("questionNo",contest.getLastQuestionNo().toString());
								
								String qData = Util.getObject(qMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LAST_QUESTION_DETAILs);
								Gson aGson = GsonCustomConfig.getGsonBuilder();		
								JsonObject qJsonObject = aGson.fromJson(qData, JsonObject.class);
								ContestQuestionDTO questionDTO = aGson.fromJson(((JsonObject)qJsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
								String buttoneText = null;
								if(questionDTO.getStatus()==1 &&  questionDTO.getNextQuestion()!=null){
									ContestQuestions q = questionDTO.getNextQuestion();
									buttoneText = Util.getResumeContestText(contest,q);
									model.addAttribute("buttonText",buttoneText);
									if(buttoneText.equalsIgnoreCase("NONE")){
										model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
									}
									model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
								}else{
									model.addAttribute("buttonText","NONE");
									model.addAttribute("msg","Contest last state is not found, Resume contest is not allowed.");
									model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
								}
							}else{
								model.addAttribute("buttonText",Util.getResumeContestText(contest,null));
								model.addAttribute("lastQuestionNo",contest.getLastQuestionNo());
							}
						}else{
							model.addAttribute("buttonText","Start Contest");
							model.addAttribute("lastQuestionNo",0);
						}
						model.addAttribute("contest",contest);
					}
				}else if(status.equalsIgnoreCase("ALL")  || status.equalsIgnoreCase("EXPIRED")){
					model.addAttribute("contestsList", gson.toJson(contestsDTO.getContestsList()));
					model.addAttribute("contestsPagingInfo", gson.toJson(contestsDTO.getPaginationDTO()));
				}else if(status.equalsIgnoreCase("ACTIVECONTEST") || status.equalsIgnoreCase("EXPIREDCONTEST")){
					model.addAttribute("contestsList", contestsDTO.getContestsList());
				}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
					model.addAttribute("questionBankList", gson.toJson(contestsDTO.getQuestionBankList()));
					model.addAttribute("questionBankPagingInfo", gson.toJson(contestsDTO.getQuestionBankPaginationDTO()));
				}else if(status.equalsIgnoreCase("REQUEST")){
					model.addAttribute("contestRequestList", gson.toJson(contestsDTO.getContestEventRequestList()));
					model.addAttribute("contestRequestPagingInfo", gson.toJson(contestsDTO.getContestEventRequestPaginationDTO()));
				}
				
			}
			model.addAttribute("status",status);
			
			if(status.equalsIgnoreCase("ALL") || status.equalsIgnoreCase("EXPIRED")){
				return "page-contests-firebase-web-allcontests";
			}else if(status.equalsIgnoreCase("ACTIVECONTEST") || status.equalsIgnoreCase("EXPIREDCONTEST")){
				return "page-contests-firebase-web-events";
			}else if(status.equalsIgnoreCase("ACTIVEQ") || status.equalsIgnoreCase("USEDQ")){
				return "page-contests-firebase-web-questionbank";
			}else if(status.equalsIgnoreCase("TODAY")){
				return "page-contests-firebase-web-todaycontest";
			}else if(status.equalsIgnoreCase("REQUEST")){
				return "page-contests-firebase-web-eventrequest";
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	/*@RequestMapping(value = "/ContinueContest")
	public String continueContestFireBase(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String type = request.getParameter("type");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("type",type);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.CONTINUE_CONTEST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			if(questionDTO.getStatus() == 1){
				ContestQuestions question = questionDTO.getQuestionList().get(0);
				if(question.getAnswer().equalsIgnoreCase("SUMMARY")){
					Map<String, String> reqMap1 = Util.getParameterMap(request);
					reqMap1.put("customerId","0");
					reqMap1.put("summaryType","CONTEST");
					reqMap1.put("contestId",question.getContestId().toString());
					String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_SUMMARY);
					Gson gson1 = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
					QuizContestSummaryInfo summaryInfo = gson1.fromJson(((JsonObject)jsonObject1.get("quizContestSummaryInfo")), QuizContestSummaryInfo.class);
					if(summaryInfo!=null && summaryInfo.getStatus()==1){
						model.addAttribute("summaryWinner",summaryInfo.getQuizContestWinner());
						model.addAttribute("summaryCount",summaryInfo.getContestWinnersCount());
					}else{
						model.addAttribute("msg",summaryInfo.getError().getDescription());
					}
				}else if(question.getAnswer().equalsIgnoreCase("WINNER")){
					Map<String, String> map1 = Util.getParameterMap(request);
					map1.put("contestId",question.getContestId().toString());
					
					
					String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_WINNERS);
					Gson gson1 = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
					QuizContestSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("quizContestSummaryInfo")), QuizContestSummaryInfo.class);
					System.out.println(" NRXT CONTEST QUESTION QUIZ_GET_WINNERS END HIT : "+ new Date());
					if(winnerSummary!=null && winnerSummary.getStatus()==1){
						Map<String, String> map2 = Util.getParameterMap(request);
						map2.put("contestId",question.getContestId().toString());
						System.out.println(" NRXT CONTEST QUESTION QUIZ_UPDATE_USER_STATISTICS START HIT : "+ new Date());
						String data2 = Util.getObject(map2, Constants.BASE_URL+Constants.QUIZ_UPDATE_USER_STATISTICS);
						Gson gson2 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject2 = gson2.fromJson(data2, JsonObject.class);
						QuizCustomerStatsInfo customerStat = gson2.fromJson(((JsonObject)jsonObject2.get("quizCustomerStatsInfo")), QuizCustomerStatsInfo.class);
						System.out.println(" NRXT CONTEST QUESTION QUIZ_UPDATE_USER_STATISTICS END HIT : "+ new Date());
						model.addAttribute("winners",winnerSummary.getQuizContestWinners());
						model.addAttribute("winnerCount",winnerSummary.getContestWinnersCount());
						model.addAttribute("winnerStatus",1);
						if(customerStat.getStatus() == 0){
							model.addAttribute("winnerMsg",customerStat.getError().getDescription());
							model.addAttribute("winnerStatus",0);
						}
					}else{
						model.addAttribute("winnerMsg",winnerSummary.getError().getDescription());
						model.addAttribute("winnerStatus",0);
					}
				}else if(question.getAnswer().equalsIgnoreCase("A") || question.getAnswer().equalsIgnoreCase("B")
						|| question.getAnswer().equalsIgnoreCase("C")){
					Map<String, String> reqMap1 = Util.getParameterMap(request);
					reqMap1.put("customerId","1");
					reqMap1.put("questionId",question.getId().toString());
					reqMap1.put("questionNo",question.getSerialNo().toString());
					reqMap1.put("contestId",question.getContestId().toString());
					String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_QUESTION_ANSWER);
					Gson gson1 = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
					QuizAnswerCountDetails userCount = gson1.fromJson(((JsonObject)jsonObject1.get("quizAnswerCountDetails")), QuizAnswerCountDetails.class);
					if(userCount!=null && userCount.getStatus()==1){
						model.addAttribute("userCount", userCount);
					}else{
						model.addAttribute("msg",userCount.getError().getDescription());
					}
					model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
				}
			}else{
				model.addAttribute("msg",questionDTO.getError().getDescription());
			}
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("questionList", questionDTO.getQuestionList());
			}else{
				model.addAttribute("questionList",questionDTO.getQuestionList());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-contests-firebase-mobile-todaycontest";
		
	}*/
	
	@RequestMapping(value = "/NextContestQuestionFireBase")
	public String startContestFireBase(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("runningContestId");
			String questionId = request.getParameter("runningQuestionNo");
			String type = request.getParameter("type");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("questionNo", questionId);
			map.put("type",type);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_STATRT_CONTEST_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			
			if(questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
				model.addAttribute("status",1);
				model.addAttribute("contest",questionDTO.getContest());
				List<ContestQuestions> questions = questionDTO.getQuestionList();
				
				Contests contest =  questionDTO.getContest();
				if(questions!= null && !questions.isEmpty()){
					ContestQuestions q = questions.get(0);
					model.addAttribute("question",q);
					//contestLog.info("CONTEST QUESTION: "+q.getSerialNo()+" -- TYPE: "+type+" --  USER: "+userName);
					
					if(q.getId()==null){
						q.setAnswer("PLAY");
						FirestoreUtil.setContestStarted(true);
						FirestoreUtil.updateNextQuestion(q, model);
						FirestoreUtil.setContestDetails(contest, model);
						FirestoreUtil.updateUpcomingContestDetails();
						FirestoreUtil.updateNextQuestionHost(questionDTO.getNextQuestion(), model);
						contestLog.info("CONTEST STARTED: -- coid: "+contest.getId()+" -- user: "+userName);
						model.addAttribute("contestMsg", "Contest is started, Press Show Question-1 button to start broadcasting questions.");
						return "";
					}else if(type!=null && type.equalsIgnoreCase("QUESTION")){
						if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("END")
								&& q.getAnswer().equalsIgnoreCase("END")){
							FirestoreUtil.endContest(model);
							FirestoreUtil.updateUpcomingContestDetails();
							contestLog.info("CONTEST ENDED: -- coid: "+contest.getId()+" -- user: "+userName);
						}else{
							FirestoreUtil.updateNextQuestion(q, model);
							FirestoreUtil.updateContestDetailsQNo(q, model);
							FirestoreUtil.updateHostTracker("QUEFIRED");
							contestLog.info("CONTEST QUESTION: -- coid: "+contest.getId()+" -- qno: "+q.getSerialNo()+" -- user: "+userName);
							model.addAttribute("lifeCount",questionDTO.getLifeCount());
						}
						return "";
					}else if(type!=null && type.equalsIgnoreCase("ANSWER") || type.equalsIgnoreCase("COUNT")){
						Map<String, String> reqMap1 = Util.getParameterMap(request);
						reqMap1.put("qId",q.getId().toString());
						reqMap1.put("qNo",q.getSerialNo().toString());
						reqMap1.put("coId",q.getContestId().toString());
						reqMap1.put("compData","1");
						if(type.equalsIgnoreCase("COUNT")){
							reqMap1.put("cuList","Y");
						}
						AnswerCountInfo userCount = null;
						String data1 = Util.getObject(reqMap1,Constants.BASE_URL+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
						if(userCount.getSts() == 0){
							model.addAttribute("status",0);
							model.addAttribute("msg",userCount.getErr().getDesc());
							contestLog.error("RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+Constants.BASE_URL);
							model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
							return "";
						}
						reqMap1.put("compData","0");
						/*for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
							String data1 = Util.getObject(reqMap1, node.getUrl()+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
							if(userCount.getSts() == 0){
								model.addAttribute("status",0);
								model.addAttribute("msg",userCount.getErr().getDesc());
								contestLog.error("RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+node.getUrl());
								model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
								return "";
							}
							reqMap1.put("compData","0");
						}*/
						model.addAttribute("userCount", userCount);
						model.addAttribute("userList", userCount.getCustList());
						if(type.equalsIgnoreCase("ANSWER")){
							FirestoreUtil.updateNextQuestion(q, model);
							FirestoreUtil.setAnswerCount(userCount, model);
							FirestoreUtil.updateNextQuestionHost(questionDTO.getNextQuestion(), model);
						}else{
							FirestoreUtil.setAnswerCountHost(userCount, model);
						}
						contestLog.info("CONTEST "+type+": -- coid: "+contest.getId()+" -- qno: "+q.getSerialNo()+" -- user: "+userName);
						model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
					}else if(type!=null && type.equalsIgnoreCase("SUMMARY")){
						Map<String, String> reqMap1 = Util.getParameterMapCass(request);
						reqMap1.put("summaryType","CONTEST");
						reqMap1.put("coId",q.getContestId().toString());
						String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_SUMMARY_CASS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo summaryInfo = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(summaryInfo!=null && summaryInfo.getSts()==1){
							List<CassContestWinners> summaryWinners = summaryInfo.getWinners();
							if(summaryWinners == null){
								summaryWinners = new ArrayList<CassContestWinners>();
							}
							Collections.shuffle(summaryWinners);
							model.addAttribute("summaryWinner",summaryWinners);
							model.addAttribute("summaryCount",summaryInfo.getwCount());
							FirestoreUtil.setSummaryWinners(summaryWinners,summaryInfo.getwCount(), model);
							FirestoreUtil.updateNextQuestion(q, model);
							FirestoreUtil.setSummaryWinnersHost(summaryWinners, summaryInfo.getwCount(), model);
							ComputeGrandWinnerThread computeGrandWinner = new ComputeGrandWinnerThread(q.getContestId(), request);
							Thread t = new Thread(computeGrandWinner);
							t.start();
							contestLog.info("CONTEST SUMMARY:  -- coid:"+contest.getId()+" -- Summary Winners: "+summaryWinners.size()+" -- user: "+userName);
						}else{
							model.addAttribute("msg",summaryInfo.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetContestSummary): "+summaryInfo.getErr().getDesc());
						}
						return "";
					}else if(type!=null && type.equalsIgnoreCase("LOTTERY")){
						FirestoreUtil.updateNextQuestion(q, model);
						FirestoreUtil.updateHostTracker("LOTTERY");
						contestLog.info("CONTEST LOTTERY: -- coid: "+contest.getId()+" -- user: "+userName);
						return "";
					}else if(type!=null && (type.equalsIgnoreCase("WINNER") || type.equalsIgnoreCase("WINNERS"))){
						Map<String, String> map1 = Util.getParameterMapCass(request);
						map1.put("coId",q.getContestId().toString());
						String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_WINNERS_CASS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(winnerSummary!=null && winnerSummary.getSts()==1){
							model.addAttribute("winnerStatus",1);
							model.addAttribute("winners",winnerSummary.getWinners());
							model.addAttribute("winnerCount",winnerSummary.getWinners());
							if(type.equalsIgnoreCase("WINNER")){
								FirestoreUtil.setGrandWinners(winnerSummary.getWinners(), model);
								FirestoreUtil.updateNextQuestion(q, model);
								FirestoreUtil.updateHostTracker("WINFIRED");
							}else{
								FirestoreUtil.setGrandWinnersHost(winnerSummary.getWinners(), model);
							}
							contestLog.info("CONTEST "+type+": -- coid:"+contest.getId()+" -- Grand Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
						}else{
							model.addAttribute("winnerMsg",winnerSummary.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetGrandWinners): "+winnerSummary.getErr().getDesc());
						}
						return "";
					}else if(type!=null && (type.equalsIgnoreCase("MINIJACKPOT") || type.equalsIgnoreCase("MINIJACKPOTS"))){
						Map<String, String> map1 = Util.getParameterMapCass(request);
						map1.put("coId",q.getContestId().toString());
						map1.put("qId",q.getId().toString());
						String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MINI_JACKPOT_WINNERS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(winnerSummary!=null && winnerSummary.getSts()==1){
							model.addAttribute("jackpotStatus",1);
							model.addAttribute("winners",winnerSummary.getWinners());
							model.addAttribute("winnerCount",winnerSummary.getWinners());
							if(type.equalsIgnoreCase("MINIJACKPOT")){
								FirestoreUtil.setJackpotWinners(winnerSummary.getWinners(), model);
								FirestoreUtil.updateNextQuestion(q, model);
								FirestoreUtil.setJackpotWinnersHost(winnerSummary.getWinners(), model);
							}
							if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
								model.addAttribute("jackpotMsg","No Mini Jackpot Winners are found.");
							}
							contestLog.info("CONTEST "+type+": -- coid:"+contest.getId()+" -- Mini Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
						}else{
							model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetMiniJackpotWinners): "+winnerSummary.getErr().getDesc());
						}
						return "";
					}else if(type!=null && (type.equalsIgnoreCase("MEGAJACKPOT") || type.equalsIgnoreCase("MEGAJACKPOTS"))){
						Map<String, String> map1 = Util.getParameterMapCass(request);
						map1.put("coId",q.getContestId().toString());
						map1.put("qId",q.getId().toString());
						String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MEGA_JACKPOT_WINNERS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(winnerSummary!=null && winnerSummary.getSts()==1){
							model.addAttribute("jackpotStatus",1);
							model.addAttribute("winners",winnerSummary.getWinners());
							model.addAttribute("winnerCount",winnerSummary.getWinners());
							if(type.equalsIgnoreCase("MEGAJACKPOT")){
								FirestoreUtil.setJackpotWinners(winnerSummary.getWinners(), model);
								FirestoreUtil.updateNextQuestion(q, model);
							}
							if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
								model.addAttribute("jackpotMsg","No Mega Jackpot Winners are found.");
							}
							contestLog.info("CONTEST "+type+": -- coid:"+contest.getId()+" -- Mega Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
						}else{
							model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetMegaJackpotWinners): "+winnerSummary.getErr().getDesc());
						}
						return "";
					}
					
					if(type!=null && type.equalsIgnoreCase("RESUME")){
						if(contest.getLastAction().equalsIgnoreCase("QUESTION")){
							FirestoreUtil.updateNextQuestion(q, model);
							FirestoreUtil.updateContestDetailsQNo(q, model);
							FirestoreUtil.updateHostTracker("QUEFIRED");
							model.addAttribute("lifeCount",questionDTO.getLifeCount());
							contestLog.info("RESUME CONTEST QUESTION: -- coid: "+contest.getId()+" -- qno:"+q.getSerialNo()+" -- user: "+userName);
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("ANSWER") || contest.getLastAction().equalsIgnoreCase("COUNT")){
							Map<String, String> reqMap1 = Util.getParameterMap(request);
							reqMap1.put("qId",q.getId().toString());
							reqMap1.put("qNo",q.getSerialNo().toString());
							reqMap1.put("coId",q.getContestId().toString());
							if(contest.getLastAction().equalsIgnoreCase("COUNT")){
								reqMap1.put("cuList","Y");
							}
							AnswerCountInfo userCount = null;
							
							String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
							if(userCount.getSts() == 0){
								model.addAttribute("status",0);
								model.addAttribute("msg",userCount.getErr().getDesc());
								contestLog.error("RESUME RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+Constants.BASE_URL);
								model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
								return "";
							}
							/*for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
								String data1 = Util.getObject(reqMap1, node.getUrl()+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
								Gson gson1 = GsonCustomConfig.getGsonBuilder();		
								JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
								userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
								if(userCount.getSts() == 0){
									model.addAttribute("status",0);
									model.addAttribute("msg",userCount.getErr().getDesc());
									contestLog.error("RESUME RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+node.getUrl());
									model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
									return "";
								}
							}*/
							model.addAttribute("userCount", userCount);
							model.addAttribute("userList", userCount.getCustList());
							if(contest.getLastAction().equalsIgnoreCase("ANSWER")){
								FirestoreUtil.updateNextQuestion(q, model);
								FirestoreUtil.setAnswerCount(userCount, model);
								FirestoreUtil.updateNextQuestionHost(questionDTO.getNextQuestion(), model);
							}else{
								FirestoreUtil.setAnswerCountHost(userCount, model);
							}
							contestLog.info("CONTEST "+type+": -- coid: "+contest.getId()+" -- qno: "+q.getSerialNo()+" -- user: "+userName);
							model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("SUMMARY")){
							Map<String, String> reqMap1 = Util.getParameterMapCass(request);
							reqMap1.put("summaryType","CONTEST");
							reqMap1.put("coId",q.getContestId().toString());
							String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_SUMMARY_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo summaryInfo = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(summaryInfo!=null && summaryInfo.getSts()==1){
								List<CassContestWinners> summaryWinners = summaryInfo.getWinners();
								if(summaryWinners == null){
									summaryWinners = new ArrayList<CassContestWinners>();
								}
								Collections.shuffle(summaryWinners);
								model.addAttribute("summaryWinner",summaryWinners);
								model.addAttribute("summaryCount",summaryInfo.getwCount());
								FirestoreUtil.setSummaryWinners(summaryWinners,summaryInfo.getwCount(), model);
								FirestoreUtil.updateNextQuestion(q, model);
								FirestoreUtil.setSummaryWinnersHost(summaryWinners,summaryInfo.getwCount(), model);
								ComputeGrandWinnerThread computeGrandWinner = new ComputeGrandWinnerThread(q.getContestId(), request);
								Thread t = new Thread(computeGrandWinner);
								t.start();
								contestLog.info("RESUME CONTEST SUMMARY:  -- coid:"+contest.getId()+" -- Summary Winners: "+summaryWinners.size()+" -- user: "+userName);
							}else{
								model.addAttribute("msg",summaryInfo.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RESUME RTFAPI ERROR (GetContestSummary): "+summaryInfo.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("LOTTERY")){
							FirestoreUtil.updateNextQuestion(q, model);
							FirestoreUtil.updateHostTracker("LOTTERY");
							contestLog.info("RESUME CONTEST LOTTERY: "+contest.getId()+" -- user: "+userName);
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("WINNER") || contest.getLastAction().equalsIgnoreCase("WINNERS")){
							Map<String, String> map1 = Util.getParameterMapCass(request);
							map1.put("coId",q.getContestId().toString());
							String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_WINNERS_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(winnerSummary!=null && winnerSummary.getSts()==1){
								model.addAttribute("winnerStatus",1);
								model.addAttribute("winners",winnerSummary.getWinners());
								model.addAttribute("winnerCount",winnerSummary.getWinners());
								if(contest.getLastAction().equalsIgnoreCase("WINNER")){
									FirestoreUtil.setGrandWinners(winnerSummary.getWinners(), model);
									FirestoreUtil.updateNextQuestion(q, model);
									FirestoreUtil.updateHostTracker("WINFIRED");
								}else{
									FirestoreUtil.setGrandWinnersHost(winnerSummary.getWinners(), model);
								}
								contestLog.info("RESUME CONTEST "+contest.getLastAction()+":  -- coid:"+contest.getId()+" -- Grand Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
							}else{
								model.addAttribute("winnerMsg",winnerSummary.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RTFAPI ERROR (GetGrandWinners): "+winnerSummary.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("MINIJACKPOTS") || contest.getLastAction().equalsIgnoreCase("MINIJACKPOT")){
							Map<String, String> map1 = Util.getParameterMapCass(request);
							map1.put("coId",q.getContestId().toString());
							map1.put("qId",q.getId().toString());
							String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MINI_JACKPOT_WINNERS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(winnerSummary!=null && winnerSummary.getSts()==1){
								model.addAttribute("jackpotStatus",1);
								model.addAttribute("winners",winnerSummary.getWinners());
								model.addAttribute("winnerCount",winnerSummary.getWinners());
								if(contest.getLastAction().equalsIgnoreCase("MINIJACKPOT")){
									FirestoreUtil.setJackpotWinners(winnerSummary.getWinners(), model);
									FirestoreUtil.updateNextQuestion(q, model);
									FirestoreUtil.setJackpotWinnersHost(winnerSummary.getWinners(), model);
								}
								if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
									model.addAttribute("jackpotMsg","No Mini Jackpot Winners are found.");
								}
								contestLog.info("RESUME CONTEST "+contest.getLastAction()+":  -- coid:"+contest.getId()+" -- Mini Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
							}else{
								model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RTFAPI ERROR (GetMiniJackpotWinners): "+winnerSummary.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("MEGAJACKPOTS") || contest.getLastAction().equalsIgnoreCase("MEGAJACKPOT")){
							Map<String, String> map1 = Util.getParameterMapCass(request);
							map1.put("coId",q.getContestId().toString());
							map1.put("qId",q.getId().toString());
							String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MEGA_JACKPOT_WINNERS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(winnerSummary!=null && winnerSummary.getSts()==1){
								model.addAttribute("jackpotStatus",1);
								model.addAttribute("winners",winnerSummary.getWinners());
								model.addAttribute("winnerCount",winnerSummary.getWinners());
								if(contest.getLastAction().equalsIgnoreCase("MEGAJACKPOT")){
									FirestoreUtil.setJackpotWinners(winnerSummary.getWinners(), model);
									FirestoreUtil.updateNextQuestion(q, model);
								}
								if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
									model.addAttribute("","No Mega Jackpot Winners are found.");
								}
								contestLog.info("RESUME CONTEST "+contest.getLastAction()+":  -- coid:"+contest.getId()+" -- Mega Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
							}else{
								model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RTFAPI ERROR (GetMegaJackpotWinners): "+winnerSummary.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("END")){
							FirestoreUtil.endContest(model);
							contestLog.info("RESUME CONTEST ENDED.");
						}
					}
					
				}
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());
				model.addAttribute("status",0);
				contestLog.error("TRACKERAPI ERROR: ERROR OCCURED WHILE GETTING NEXT QUESTION, TYPE:"+type);
			}
		} catch (Exception e) {
			contestLog.error("TRACKER ERROR: ERROR OCCURED WHILE GETTING NEXT QUESTION.");
			e.printStackTrace();
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/NextContestQuestionWeb")
	public String startContestWeb(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("runningContestId");
			String questionId = request.getParameter("runningQuestionNo");
			String type = request.getParameter("type");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("questionNo", questionId);
			map.put("type",type);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_STATRT_CONTEST_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			
			if(questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
				model.addAttribute("status",1);
				model.addAttribute("contest",questionDTO.getContest());
				List<ContestQuestions> questions = questionDTO.getQuestionList();
				Contests contest =  questionDTO.getContest();
				if(questions!= null && !questions.isEmpty()){
					ContestQuestions q = questions.get(0);
					model.addAttribute("question",q);
					//contestLog.info("CONTEST QUESTION: "+q.getSerialNo()+" -- TYPE: "+type+" --  USER: "+userName);
					
					if(q.getId()==null){
						q.setAnswer("PLAY");
						FirebaseWebUtil.setContestStarted(true);
						FirebaseWebUtil.updateNextQuestion(q, model);
						FirebaseWebUtil.setContestDetails(contest, model);
						FirebaseWebUtil.updateUpcomingContestDetails();
						contestLog.info("CONTEST STARTED: -- coid: "+contest.getId()+" -- user: "+userName);
						model.addAttribute("contestMsg", "Contest is started, Press Show Question-1 button to start broadcasting questions.");
						return "";
					}else if(type!=null && type.equalsIgnoreCase("QUESTION")){
						if(contest.getLastAction()!=null && contest.getLastAction().equalsIgnoreCase("END")
								&& q.getAnswer().equalsIgnoreCase("END")){
							FirebaseWebUtil.endContest(model);
							FirebaseWebUtil.updateUpcomingContestDetails();
							contestLog.info("CONTEST ENDED: -- coid: "+contest.getId()+" -- user: "+userName);
						}else{
							FirebaseWebUtil.updateNextQuestion(q, model);
							FirebaseWebUtil.updateContestDetailsQNo(q, model);
							contestLog.info("CONTEST QUESTION: -- coid: "+contest.getId()+" -- qno: "+q.getSerialNo()+" -- user: "+userName);
							model.addAttribute("lifeCount",questionDTO.getLifeCount());
						}
						return "";
					}else if(type!=null && type.equalsIgnoreCase("ANSWER") || type.equalsIgnoreCase("COUNT")){
						Map<String, String> reqMap1 = Util.getParameterMap(request);
						reqMap1.put("qId",q.getId().toString());
						reqMap1.put("qNo",q.getSerialNo().toString());
						reqMap1.put("coId",q.getContestId().toString());
						reqMap1.put("compData","1");
						if(type.equalsIgnoreCase("COUNT")){
							reqMap1.put("cuList","Y");
						}
						AnswerCountInfo userCount = null;
						String data1 = Util.getObject(reqMap1,Constants.BASE_URL+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
						if(userCount.getSts() == 0){
							model.addAttribute("status",0);
							model.addAttribute("msg",userCount.getErr().getDesc());
							contestLog.error("RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+Constants.BASE_URL);
							model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
							return "";
						}
						reqMap1.put("compData","0");
						/*for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
							String data1 = Util.getObject(reqMap1, node.getUrl()+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
							if(userCount.getSts() == 0){
								model.addAttribute("status",0);
								model.addAttribute("msg",userCount.getErr().getDesc());
								contestLog.error("RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+node.getUrl());
								model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
								return "";
							}
							reqMap1.put("compData","0");
						}*/
						model.addAttribute("userCount", userCount);
						model.addAttribute("userList", userCount.getCustList());
						if(type.equalsIgnoreCase("ANSWER")){
							FirebaseWebUtil.updateNextQuestion(q, model);
							FirebaseWebUtil.setAnswerCount(userCount, model);
						}
						contestLog.info("CONTEST "+type+": -- coid: "+contest.getId()+" -- qno: "+q.getSerialNo()+" -- user: "+userName);
						model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
					}else if(type!=null && type.equalsIgnoreCase("SUMMARY")){
						Map<String, String> reqMap1 = Util.getParameterMapCass(request);
						reqMap1.put("summaryType","CONTEST");
						reqMap1.put("coId",q.getContestId().toString());
						String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_SUMMARY_CASS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo summaryInfo = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(summaryInfo!=null && summaryInfo.getSts()==1){
							List<CassContestWinners> summaryWinners = summaryInfo.getWinners();
							if(summaryWinners == null){
								summaryWinners = new ArrayList<CassContestWinners>();
							}
							Collections.shuffle(summaryWinners);
							model.addAttribute("summaryWinner",summaryWinners);
							model.addAttribute("summaryCount",summaryInfo.getwCount());
							FirebaseWebUtil.setSummaryWinners(summaryWinners,summaryInfo.getwCount(), model);
							FirebaseWebUtil.updateNextQuestion(q, model);
							ComputeGrandWinnerThread computeGrandWinner = new ComputeGrandWinnerThread(q.getContestId(), request);
							Thread t = new Thread(computeGrandWinner);
							t.start();
							contestLog.info("CONTEST SUMMARY:  -- coid:"+contest.getId()+" -- Summary Winners: "+summaryWinners.size()+" -- user: "+userName);
						}else{
							model.addAttribute("msg",summaryInfo.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetContestSummary): "+summaryInfo.getErr().getDesc());
						}
						return "";
					}else if(type!=null && type.equalsIgnoreCase("LOTTERY")){
						FirebaseWebUtil.updateNextQuestion(q, model);
						contestLog.info("CONTEST LOTTERY: -- coid: "+contest.getId()+" -- user: "+userName);
						return "";
					}else if(type!=null && (type.equalsIgnoreCase("WINNER") || type.equalsIgnoreCase("WINNERS"))){
						Map<String, String> map1 = Util.getParameterMapCass(request);
						map1.put("coId",q.getContestId().toString());
						String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_WINNERS_CASS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(winnerSummary!=null && winnerSummary.getSts()==1){
							model.addAttribute("winnerStatus",1);
							model.addAttribute("winners",winnerSummary.getWinners());
							model.addAttribute("winnerCount",winnerSummary.getWinners());
							if(type.equalsIgnoreCase("WINNER")){
								FirebaseWebUtil.setGrandWinners(winnerSummary.getWinners(), model);
								FirebaseWebUtil.updateNextQuestion(q, model);
							}
							contestLog.info("CONTEST "+type+": -- coid:"+contest.getId()+" -- Grand Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
						}else{
							model.addAttribute("winnerMsg",winnerSummary.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetGrandWinners): "+winnerSummary.getErr().getDesc());
						}
						return "";
					}else if(type!=null && (type.equalsIgnoreCase("MINIJACKPOT") || type.equalsIgnoreCase("MINIJACKPOTS"))){
						Map<String, String> map1 = Util.getParameterMapCass(request);
						map1.put("coId",q.getContestId().toString());
						map1.put("qId",q.getId().toString());
						String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MINI_JACKPOT_WINNERS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(winnerSummary!=null && winnerSummary.getSts()==1){
							model.addAttribute("jackpotStatus",1);
							model.addAttribute("winners",winnerSummary.getWinners());
							model.addAttribute("winnerCount",winnerSummary.getWinners());
							if(type.equalsIgnoreCase("MINIJACKPOT")){
								FirebaseWebUtil.setJackpotWinners(winnerSummary.getWinners(), model);
								FirebaseWebUtil.updateNextQuestion(q, model);
							}
							if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
								model.addAttribute("jackpotMsg","No Mini Jackpot Winners are found.");
							}
							contestLog.info("CONTEST "+type+": -- coid:"+contest.getId()+" -- Mini Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
						}else{
							model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetMiniJackpotWinners): "+winnerSummary.getErr().getDesc());
						}
						return "";
					}else if(type!=null && (type.equalsIgnoreCase("MEGAJACKPOT") || type.equalsIgnoreCase("MEGAJACKPOTS"))){
						Map<String, String> map1 = Util.getParameterMapCass(request);
						map1.put("coId",q.getContestId().toString());
						map1.put("qId",q.getId().toString());
						String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MEGA_JACKPOT_WINNERS);
						Gson gson1 = GsonCustomConfig.getGsonBuilder();		
						JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
						ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
						if(winnerSummary!=null && winnerSummary.getSts()==1){
							model.addAttribute("jackpotStatus",1);
							model.addAttribute("winners",winnerSummary.getWinners());
							model.addAttribute("winnerCount",winnerSummary.getWinners());
							if(type.equalsIgnoreCase("MEGAJACKPOT")){
								FirebaseWebUtil.setJackpotWinners(winnerSummary.getWinners(), model);
								FirebaseWebUtil.updateNextQuestion(q, model);
							}
							if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
								model.addAttribute("jackpotMsg","No Mega Jackpot Winners are found.");
							}
							contestLog.info("CONTEST "+type+": -- coid:"+contest.getId()+" -- Mega Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
						}else{
							model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
							model.addAttribute("status",0);
							contestLog.error("RTFAPI ERROR (GetMegaJackpotWinners): "+winnerSummary.getErr().getDesc());
						}
						return "";
					}
					
					if(type!=null && type.equalsIgnoreCase("RESUME")){
						if(contest.getLastAction().equalsIgnoreCase("QUESTION")){
							FirebaseWebUtil.updateNextQuestion(q, model);
							FirebaseWebUtil.updateContestDetailsQNo(q, model);
							model.addAttribute("lifeCount",questionDTO.getLifeCount());
							contestLog.info("RESUME CONTEST QUESTION: -- coid: "+contest.getId()+" -- qno:"+q.getSerialNo()+" -- user: "+userName);
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("ANSWER") || contest.getLastAction().equalsIgnoreCase("COUNT")){
							Map<String, String> reqMap1 = Util.getParameterMap(request);
							reqMap1.put("qId",q.getId().toString());
							reqMap1.put("qNo",q.getSerialNo().toString());
							reqMap1.put("coId",q.getContestId().toString());
							if(contest.getLastAction().equalsIgnoreCase("COUNT")){
								reqMap1.put("cuList","Y");
							}
							AnswerCountInfo userCount = null;
							
							String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
							if(userCount.getSts() == 0){
								model.addAttribute("status",0);
								model.addAttribute("msg",userCount.getErr().getDesc());
								contestLog.error("RESUME RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+Constants.BASE_URL);
								model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
								return "";
							}
							/*for(RtfConfigContestClusterNodes node : Util.getContestServerNodeUrls()){
								String data1 = Util.getObject(reqMap1, node.getUrl()+Constants.QUIZ_GET_QUESTION_ANSWER_CASS);
								Gson gson1 = GsonCustomConfig.getGsonBuilder();		
								JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
								userCount = gson1.fromJson(((JsonObject)jsonObject1.get("answerCountInfo")), AnswerCountInfo.class);
								if(userCount.getSts() == 0){
									model.addAttribute("status",0);
									model.addAttribute("msg",userCount.getErr().getDesc());
									contestLog.error("RESUME RTFAPI ERROR (GetQuestAnsCount): "+userCount.getErr().getDesc()+" -- URL: "+node.getUrl());
									model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
									return "";
								}
							}*/
							model.addAttribute("userCount", userCount);
							model.addAttribute("userList", userCount.getCustList());
							if(contest.getLastAction().equalsIgnoreCase("ANSWER")){
								FirebaseWebUtil.updateNextQuestion(q, model);
								FirebaseWebUtil.setAnswerCount(userCount, model);
							}
							contestLog.info("CONTEST "+type+": -- coid: "+contest.getId()+" -- qno: "+q.getSerialNo()+" -- user: "+userName);
							model.addAttribute("nextQuestion", questionDTO.getNextQuestion());
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("SUMMARY")){
							Map<String, String> reqMap1 = Util.getParameterMapCass(request);
							reqMap1.put("summaryType","CONTEST");
							reqMap1.put("coId",q.getContestId().toString());
							String data1 = Util.getObject(reqMap1, Constants.BASE_URL+Constants.QUIZ_GET_SUMMARY_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo summaryInfo = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(summaryInfo!=null && summaryInfo.getSts()==1){
								List<CassContestWinners> summaryWinners = summaryInfo.getWinners();
								if(summaryWinners == null){
									summaryWinners = new ArrayList<CassContestWinners>();
								}
								Collections.shuffle(summaryWinners);
								model.addAttribute("summaryWinner",summaryWinners);
								model.addAttribute("summaryCount",summaryInfo.getwCount());
								FirebaseWebUtil.setSummaryWinners(summaryWinners,summaryInfo.getwCount(), model);
								FirebaseWebUtil.updateNextQuestion(q, model);
								ComputeGrandWinnerThread computeGrandWinner = new ComputeGrandWinnerThread(q.getContestId(), request);
								Thread t = new Thread(computeGrandWinner);
								t.start();
								contestLog.info("RESUME CONTEST SUMMARY:  -- coid:"+contest.getId()+" -- Summary Winners: "+summaryWinners.size()+" -- user: "+userName);
							}else{
								model.addAttribute("msg",summaryInfo.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RESUME RTFAPI ERROR (GetContestSummary): "+summaryInfo.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("LOTTERY")){
							FirebaseWebUtil.updateNextQuestion(q, model);
							contestLog.info("RESUME CONTEST LOTTERY: "+contest.getId()+" -- user: "+userName);
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("WINNER") || contest.getLastAction().equalsIgnoreCase("WINNERS")){
							Map<String, String> map1 = Util.getParameterMapCass(request);
							map1.put("coId",q.getContestId().toString());
							String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_WINNERS_CASS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(winnerSummary!=null && winnerSummary.getSts()==1){
								model.addAttribute("winnerStatus",1);
								model.addAttribute("winners",winnerSummary.getWinners());
								model.addAttribute("winnerCount",winnerSummary.getWinners());
								if(contest.getLastAction().equalsIgnoreCase("WINNER")){
									FirebaseWebUtil.setGrandWinners(winnerSummary.getWinners(), model);
									FirebaseWebUtil.updateNextQuestion(q, model);
								}
								contestLog.info("RESUME CONTEST "+contest.getLastAction()+":  -- coid:"+contest.getId()+" -- Grand Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
							}else{
								model.addAttribute("winnerMsg",winnerSummary.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RTFAPI ERROR (GetGrandWinners): "+winnerSummary.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("MINIJACKPOTS") || contest.getLastAction().equalsIgnoreCase("MINIJACKPOT")){
							Map<String, String> map1 = Util.getParameterMapCass(request);
							map1.put("coId",q.getContestId().toString());
							map1.put("qId",q.getId().toString());
							String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MINI_JACKPOT_WINNERS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(winnerSummary!=null && winnerSummary.getSts()==1){
								model.addAttribute("jackpotStatus",1);
								model.addAttribute("winners",winnerSummary.getWinners());
								model.addAttribute("winnerCount",winnerSummary.getWinners());
								if(contest.getLastAction().equalsIgnoreCase("MINIJACKPOT")){
									FirebaseWebUtil.setJackpotWinners(winnerSummary.getWinners(), model);
									FirebaseWebUtil.updateNextQuestion(q, model);
								}
								if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
									model.addAttribute("jackpotMsg","No Mini Jackpot Winners are found.");
								}
								contestLog.info("RESUME CONTEST "+contest.getLastAction()+":  -- coid:"+contest.getId()+" -- Mini Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
							}else{
								model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RTFAPI ERROR (GetMiniJackpotWinners): "+winnerSummary.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("MEGAJACKPOTS") || contest.getLastAction().equalsIgnoreCase("MEGAJACKPOT")){
							Map<String, String> map1 = Util.getParameterMapCass(request);
							map1.put("coId",q.getContestId().toString());
							map1.put("qId",q.getId().toString());
							String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.QUIZ_GET_MEGA_JACKPOT_WINNERS);
							Gson gson1 = GsonCustomConfig.getGsonBuilder();		
							JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
							ContSummaryInfo winnerSummary = gson1.fromJson(((JsonObject)jsonObject1.get("contSummaryInfo")), ContSummaryInfo.class);
							if(winnerSummary!=null && winnerSummary.getSts()==1){
								model.addAttribute("jackpotStatus",1);
								model.addAttribute("winners",winnerSummary.getWinners());
								model.addAttribute("winnerCount",winnerSummary.getWinners());
								if(contest.getLastAction().equalsIgnoreCase("MEGAJACKPOT")){
									FirebaseWebUtil.setJackpotWinners(winnerSummary.getWinners(), model);
									FirebaseWebUtil.updateNextQuestion(q, model);
								}
								if(winnerSummary.getWinners() == null || winnerSummary.getWinners().isEmpty()){
									model.addAttribute("","No Mega Jackpot Winners are found.");
								}
								contestLog.info("RESUME CONTEST "+contest.getLastAction()+":  -- coid:"+contest.getId()+" -- Mega Jackpot Winners: "+winnerSummary.getWinners().size()+" -- user: "+userName);
							}else{
								model.addAttribute("jackpotMsg",winnerSummary.getErr().getDesc());
								model.addAttribute("status",0);
								contestLog.error("RTFAPI ERROR (GetMegaJackpotWinners): "+winnerSummary.getErr().getDesc());
							}
							return "";
						}else if(contest.getLastAction().equalsIgnoreCase("END")){
							FirebaseWebUtil.endContest(model);
							contestLog.info("RESUME CONTEST ENDED.");
						}
					}
					
				}
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());
				model.addAttribute("status",0);
				contestLog.error("TRACKERAPI ERROR: ERROR OCCURED WHILE GETTING NEXT QUESTION, TYPE:"+type);
			}
		} catch (Exception e) {
			contestLog.error("TRACKER ERROR: ERROR OCCURED WHILE GETTING NEXT QUESTION.");
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GetTotalUserCount")
	public String getTotalUserCount(HttpServletRequest request, HttpServletResponse response,Model model){
		String contestId = request.getParameter("runningContestId");
		try {
			Map<String, String> map = Util.getParameterMapCass(request);
			map.put("coId",contestId);
			String data = Util.getObject(map,Constants.BASE_URL+Constants.GET_TOTAL_USERCOUNT_CASS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CassJoinContestInfo countInfo = gson.fromJson(((JsonObject)jsonObject.get("cassJoinContestInfo")), CassJoinContestInfo.class);
			if(countInfo!=null && countInfo.getSts()==1){
				model.addAttribute("status",1);
				FirestoreUtil.updateTotalUserCount(countInfo.gettCount());
				model.addAttribute("totalUserCount",countInfo.gettCount());
			}else{
				model.addAttribute("status",0);
				System.out.println("RTFAPI TOTAL USERCOUNT ERROR: "+countInfo.getErr().getDesc());
			}
		} catch (Exception e) {
			System.out.println("TRACKER TOTAL USERCOUNT ERROR: Error Occured while getting total user count."+ new Date());
			e.printStackTrace();
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/GetTotalUserCountWeb")
	public String getTotalUserCountWeb(HttpServletRequest request, HttpServletResponse response,Model model){
		String contestId = request.getParameter("runningContestId");
		try {
			Map<String, String> map = Util.getParameterMapCass(request);
			map.put("coId",contestId);
			String data = Util.getObject(map,Constants.BASE_URL+Constants.GET_TOTAL_USERCOUNT_CASS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CassJoinContestInfo countInfo = gson.fromJson(((JsonObject)jsonObject.get("cassJoinContestInfo")), CassJoinContestInfo.class);
			if(countInfo!=null && countInfo.getSts()==1){
				model.addAttribute("status",1);
				FirebaseWebUtil.updateTotalUserCount(countInfo.gettCount());
				model.addAttribute("totalUserCount",countInfo.gettCount());
			}else{
				model.addAttribute("status",0);
				System.out.println("RTFAPI TOTAL USERCOUNT ERROR: "+countInfo.getErr().getDesc());
			}
		} catch (Exception e) {
			System.out.println("TRACKER TOTAL USERCOUNT ERROR: Error Occured while getting total user count."+ new Date());
			e.printStackTrace();
		}
		return "";
	}
			
			
	
	

	@RequestMapping(value = "/UpdateContest")
	public String updateContest(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String contestId = request.getParameter("contestId");
			String preContestId = request.getParameter("preContestId");
			String contestName = request.getParameter("contestName");
			String startDateHour = request.getParameter("startDateHour");
			String startDateMinute = request.getParameter("startDateMinute");
			String contestFromDate = request.getParameter("contestFromDate");
			String maxTickets = request.getParameter("maxTickets");
			String contestCategory = request.getParameter("contestCategory");
			String rewardPoints = request.getParameter("rewardPoints");
			String ticketsPerWinner = request.getParameter("ticketsPerWinner");
			String contestMode = request.getParameter("contestMode");
			String gamePassword = request.getParameter("gamePassword");
			String questionSize = request.getParameter("questionSize");
			String promoOfferId = request.getParameter("promoOfferId");
			String artistId = request.getParameter("artistId");
			String eventId = request.getParameter("eventId");
			String parentId = request.getParameter("parentId");
			String childId = request.getParameter("childId");
			String grandChildId = request.getParameter("grandChildId");
			String artistEventCategoryName = request.getParameter("artistEventCategoryName");
			String artistEventCategoryType = request.getParameter("artistEventCategoryType");
			String promotionalCode = request.getParameter("promotionalCode");
			String discountPercentage = request.getParameter("discountPercentage");
			String zone = request.getParameter("zone");
			String tixPrice = request.getParameter("tixPrice");
			String type = request.getParameter("type");
			String status = request.getParameter("status");
			String participantStar = request.getParameter("participantStar");
			String referralStar = request.getParameter("referralStar");
			String participantPoints = request.getParameter("participantPoints");
			String referralPoints = request.getParameter("referralPoints");
			String referralRewards = request.getParameter("referralReward");
			String participantLives = request.getParameter("participantLives");
			String participantRewards = request.getParameter("participantRewards");
			String extContestName = request.getParameter("extContestName");
			
			String isMegaJackpotStr = request.getParameter("isMegaJackpot");
			String mjpWinnerPerQuestionStr = request.getParameter("mjpWinnerPerQuestion");
			String giftCardIdStr = request.getParameter("giftCardId");
			String giftCardsPerWinnerStr = request.getParameter("giftCardPerWinner");
			String contestJackpotTypeStr = request.getParameter("contestJackpotType");
			String questionRewardType = request.getParameter("questionRewardType");
			
			
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("contestId", contestId);
			map.put("preContestId", preContestId);
			map.put("contestName", contestName);
			map.put("contestCategory", contestCategory);
			map.put("startDateHour", startDateHour);
			map.put("startDateMinute", startDateMinute);
			map.put("maxTickets", maxTickets);
			map.put("contestFromDate", contestFromDate);
			map.put("userName", userName);
			map.put("extContestName", extContestName);
			map.put("rewardPoints", rewardPoints);
			map.put("ticketsPerWinner", ticketsPerWinner);
			map.put("contestMode",contestMode);
			map.put("gamePassword",gamePassword);
			map.put("questionSize",questionSize);
			map.put("promoOfferId", promoOfferId);
			map.put("artistId", artistId);
			map.put("eventId", eventId);
			map.put("parentId", parentId);
			map.put("childId", childId);
			map.put("grandChildId", grandChildId);
			map.put("artistEventCategoryName", artistEventCategoryName);
			map.put("artistEventCategoryType", artistEventCategoryType);
			map.put("promotionalCode", promotionalCode);
			map.put("discountPercentage", discountPercentage);
			map.put("zone", zone);
			map.put("tixPrice", tixPrice);
			map.put("type", type);
			map.put("status", status);
			map.put("participantStar",participantStar);
			map.put("referralStar",referralStar);
			map.put("referralRewards",referralRewards);
			map.put("participantLives",participantLives);
			map.put("referralPoints",referralPoints);
			map.put("participantPoints",participantPoints);
			map.put("participantRewards",participantRewards);
			map.put("contestJackpotType",contestJackpotTypeStr);
			map.put("questionRewardType",questionRewardType);
			
			
			map.put("isMegaJackpot",isMegaJackpotStr);
			map.put("mjpWinnerPerQuestion",mjpWinnerPerQuestionStr);
			map.put("giftCardId",giftCardIdStr);
			map.put("giftCardPerWinner",giftCardsPerWinnerStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CONTESTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			
			if(contestsDTO.getError() == null && contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
				model.addAttribute("contestsList", contestsDTO.getContestsList());
				model.addAttribute("preContestList", contestsDTO.getPreContestList());
				model.addAttribute("contestsPagingInfo", contestsDTO.getPaginationDTO());
				if(!action.equalsIgnoreCase("EDIT")){
					if(type.equalsIgnoreCase("MOBILE")){
						FirestoreUtil.updateUpcomingContestDetails();
					}else{
						FirebaseWebUtil.updateUpcomingContestDetails();
					}
				}
				contestLog.info(action+" CONTEST: -- coid: "+contestId+" -- user: "+userName);
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
				contestLog.error("TICTACKERAPI "+action+" CONTEST ERROR: -- coid: "+contestId+" -- user: "+userName+" = "+contestsDTO.getError().getDescription());
			}
			model.addAttribute("status", contestsDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/getUpcomingContestDetails")
	public String getUpcomingContestDetails(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			String type = request.getParameter("type"); 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("type", type);
						
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_UPCOMING_CONTEST);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			if(contestsDTO.getError() == null && contestsDTO.getStatus() == 1){
				if(contestsDTO.getContestsList()!= null && !contestsDTO.getContestsList().isEmpty()){
					Contests contest = contestsDTO.getContestsList().get(0);
					String nextGameTime = Util.getNextContestStartTimeMsg(contest.getStartDateTime());
					model.addAttribute("contestId",contest.getId());
					model.addAttribute("contestName",contest.getContestName());
					model.addAttribute("nextGameTime",nextGameTime);
					model.addAttribute("grandWinnerExpiryText",Util.getGrandWinnerExpiryText(contest.getPromoExpiryDate()));
					model.addAttribute("joinButtonPopupMsg",Util.getJoinButtonMsg(nextGameTime));
				}else{
					model.addAttribute("contestId",0);
					model.addAttribute("contestName","Next Contest will be Announced Shortly.");
					model.addAttribute("nextGameTime","TBD");
					model.addAttribute("grandWinnerExpiryText","");
					model.addAttribute("joinButtonPopupMsg","Next Contest will be Announced Shortly.");
				}
				model.addAttribute("isContestStarted",contestsDTO.getContestStarted());
				model.addAttribute("joinButtonLabel","PLAY GAME");
				//model.addAttribute("liveStreamUrl",contestsDTO.getLiveStreamUrl());
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());				
			}
			model.addAttribute("status", contestsDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "Error occured while getting upcoming contest to update on firebase.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/ContestQuestions")
	public String getContestQuestions(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String contestId = request.getParameter("contestId");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("contestId", contestId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_QUESTION);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			if(questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("questionList", questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo", questionDTO.getPaginationDTO());
			}else{
				model.addAttribute("questionList",questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo",questionDTO.getPaginationDTO());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	

	@RequestMapping(value = "/UpdateQuestion")
	public String updateContestQuestion(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String contestId = request.getParameter("qContestId");
			String questionId = request.getParameter("questionId");
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC");
			//String optionD = request.getParameter("optionD");
			String answer = request.getParameter("answer");
			String questionReward = request.getParameter("questionReward");
			String difficultyLevel = request.getParameter("difficultyLevel");
			String questionBankIds = request.getParameter("questionBankIds");
			
			String miniJackpotTypeStr = request.getParameter("miniJackpotType");
			String mjpNoOfWinnerStr = request.getParameter("mjpNoOfWinner");
			String mjpQtyPerWinnerStr = request.getParameter("mjpQtyPerWinner");
			String mjpGiftCardIdStr = request.getParameter("mjpGiftCardId");
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("contestId", contestId);
			map.put("questionId", questionId);
			map.put("questionText", questionText);
			map.put("optionA", optionA);
			map.put("optionB", optionB);
			map.put("optionC", optionC);
			map.put("difficultyLevel", difficultyLevel);
			map.put("answer", answer);
			//map.put("questionNo", questionNo);
			map.put("userName", userName);
			map.put("questionReward", questionReward);
			map.put("questionBankIds", questionBankIds);
			
			map.put("miniJackpotType", miniJackpotTypeStr);
			map.put("mjpNoOfWinner", mjpNoOfWinnerStr);
			map.put("mjpQtyPerWinner", mjpQtyPerWinnerStr);
			map.put("mjpGiftCardId", mjpGiftCardIdStr);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_QUESTION);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			
			if(questionDTO.getError() == null && questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
				model.addAttribute("questionList", questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo", questionDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());				
			}
			model.addAttribute("status", questionDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateQuestionRewards")
	public String updateContestQuestionRewards(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String contestId = request.getParameter("contestId");
			
			Map<String, String> map = Util.getParameterMap(request);
			for(int i=0;i<100;i++){
				String reward = request.getParameter("questionReward_"+i);
				if(reward == null || reward.isEmpty()){
					map.put("size",String.valueOf((i)));
					break;
				}
				map.put("questionReward_"+i,reward);
			}
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			map.put("contestId", contestId);
			map.put("userName", userName);
			
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_QUESTION_REWARD);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("contestQuestionDTO")), ContestQuestionDTO.class);
			
			if(questionDTO.getError() == null && questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
				model.addAttribute("questionList", questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo", questionDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());				
			}
			model.addAttribute("status", questionDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GetContestSettings")
	public String getQuizConfigSettings(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_QUIZ_CONFIG_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestConfigSettingsDTO contestConfigsettingsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestConfigSettingsDTO")), ContestConfigSettingsDTO.class);
			
			if(contestConfigsettingsDTO.getError() == null && contestConfigsettingsDTO.getStatus() == 1){
				model.addAttribute("msg", contestConfigsettingsDTO.getMessage());
				model.addAttribute("configSettings", contestConfigsettingsDTO.getContestConfigSettings());
			}else{
				model.addAttribute("msg", contestConfigsettingsDTO.getError().getDescription());				
			}
			model.addAttribute("status", contestConfigsettingsDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateContestSettings")
	public String updateQuizConfigSettings(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String liveUrl = request.getParameter("liveUrl");
			String liveThreshold = request.getParameter("liveThreshold");
			String notification7 = request.getParameter("notification7");
			String notification20 = request.getParameter("notification20");
			String notificationManual = request.getParameter("notificationManual");
			String nextContest = request.getParameter("nextContest");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			if(action == null || action.isEmpty()){
				return "page-contest-settings";
			}
			if(action.equalsIgnoreCase("MANUALNOTIFICATION")){
				if(notificationManual == null || notificationManual.isEmpty()){
					model.addAttribute("msg","Please add manual notification text.");
					model.addAttribute("status",0);
					return "";
				}
				Map<String, String> map1 = Util.getParameterMap(request);
				map1.put("userId","0");
				map1.put("message",notificationManual);
				contestLog.info("CONTEST MANUAL NOTIFICATION STARTED -- user: "+userName);
				String data1 = Util.getObject(map1, Constants.BASE_URL+Constants.SEND_CONTEST_MANUAL_NOTIFICATIONS);
				Gson gson1 = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
				QuizContestSummaryInfo summary = gson1.fromJson(((JsonObject)jsonObject1.get("quizContestSummaryInfo")), QuizContestSummaryInfo.class);
				if(summary.getStatus() == 1){
					model.addAttribute("msg","Manual notification sent sucessfully.");
					model.addAttribute("status",summary.getStatus());
					contestLog.info("CONTEST MANUAL NOTIFICATION SENT -- user "+userName);
				}else{
					model.addAttribute("msg",summary.getError().getDescription());
					model.addAttribute("status",summary.getStatus());
					contestLog.error("RTFAPI ERROR: CONTEST MANUAL NOTIFICATION ERROR: "+summary.getError().getDescription());
				}
				return "";
			}
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action",action);
			map.put("liveUrl",liveUrl);
			map.put("liveThreshold",liveThreshold);
			map.put("notification7",notification7);
			map.put("notification20",notification20);
			map.put("notificationManual",notificationManual);
			map.put("nextContest",nextContest);
			map.put("userName",userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_QUIZ_CONFIG_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestConfigSettingsDTO contestConfigsettingsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestConfigSettingsDTO")), ContestConfigSettingsDTO.class);
			
			if(contestConfigsettingsDTO.getError() == null && contestConfigsettingsDTO.getStatus() == 1){
				model.addAttribute("msg", contestConfigsettingsDTO.getMessage());
				model.addAttribute("configSettings", contestConfigsettingsDTO.getContestConfigSettings());
				if(action.equalsIgnoreCase("LIVEURL")){
					FirestoreUtil.updateLiveStreamUrl(liveUrl);
					contestLog.info("CONTEST LIVE STEAM URL UPDATED: "+liveUrl+" -- user "+userName);
				}else if(action.equalsIgnoreCase("SHOWNEXTCONTEST")){
					FirestoreUtil.updateUpcomingContestDetails();
					contestLog.info("SHOW NEXT CONTEST VALUE UPDATED: "+nextContest+" -- user "+userName);
				}
			}else{
				model.addAttribute("msg", contestConfigsettingsDTO.getError().getDescription());
				contestLog.error("TICTACKERAPI ERROR: WHILE UPDATING "+action+": "+contestConfigsettingsDTO.getError().getDescription());
			}
			model.addAttribute("status", contestConfigsettingsDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateReferralRewardSettings")
	public String updateReferralRewardSettings(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String contestCustReferralRewardPerc = request.getParameter("referralReward");
			String referralRewardCreditType = request.getParameter("referralRewardCreditType");
			 
			if(action == null || action.isEmpty()){
				return "page-contest-settings";
			}
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action",action);
			map.put("referralRewardCreditType",referralRewardCreditType);
			map.put("referralReward",contestCustReferralRewardPerc);
			map.put("userName",userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_REFERRAL_REWARDS_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestConfigSettingsDTO contestConfigsettingsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestConfigSettingsDTO")), ContestConfigSettingsDTO.class);
			
			if(contestConfigsettingsDTO.getError() == null && contestConfigsettingsDTO.getStatus() == 1){
				model.addAttribute("msg", contestConfigsettingsDTO.getMessage());
				model.addAttribute("referralRewardSettings", contestConfigsettingsDTO.getLoyaltySettings());
			}else{
				model.addAttribute("msg", contestConfigsettingsDTO.getError().getDescription());				
			}
			model.addAttribute("status", contestConfigsettingsDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GetReferralRewardSettings")
	public String getReferralRewardSettings(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			Map<String, String> map = Util.getParameterMap(request);
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_REFERRAL_REWARDS_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestConfigSettingsDTO contestConfigsettingsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestConfigSettingsDTO")), ContestConfigSettingsDTO.class);
			if(contestConfigsettingsDTO.getError() == null && contestConfigsettingsDTO.getStatus() == 1){
				model.addAttribute("msg", contestConfigsettingsDTO.getMessage());
				model.addAttribute("referralRewardSettings", contestConfigsettingsDTO.getLoyaltySettings());
			}else{
				model.addAttribute("msg", contestConfigsettingsDTO.getError().getDescription());				
			}
			model.addAttribute("status", contestConfigsettingsDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
		
	//Affiliate Note
	@RequestMapping(value = "/getAffiliatePaymentNote")
	public String getAffiliatePaymentNote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{			
			String userId = request.getParameter("userId");
			String orderId = request.getParameter("orderId");

			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userId", userId);
			map.put("orderId", orderId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_AFFILIATE_PAYMENT_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AffiliateOrderNoteDTO affiliateOrderNoteDTO = gson.fromJson(((JsonObject)jsonObject.get("affiliateOrderNoteDTO")), AffiliateOrderNoteDTO.class);
			
			if(affiliateOrderNoteDTO.getStatus() == 1){
				model.addAttribute("msg", affiliateOrderNoteDTO.getMessage());
				model.addAttribute("orderId", affiliateOrderNoteDTO.getOrderId());
				model.addAttribute("paymentNote", affiliateOrderNoteDTO.getPaymentNote());
			}else{
				model.addAttribute("msg", affiliateOrderNoteDTO.getError().getDescription());
			}
			model.addAttribute("status", affiliateOrderNoteDTO.getStatus());
						
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/saveAffiliatePaymentNote")
	public String saveAffiliatePaymentNote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{			
			String userId = request.getParameter("userId");
			String orderId = request.getParameter("affOrderIdModNotes");
			String paymentNote = request.getParameter("affPaymentNoteModNotes");

			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userId", userId);
			map.put("orderId", orderId);
			map.put("paymentNote", paymentNote);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_AFFILIATE_PAYMENT_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());				
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			model.addAttribute("status", genericResponseDTO.getStatus());
						
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/ContestEvents")
	public String getContestEvents(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String contestId = request.getParameter("contestId");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("contestId", contestId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTEST_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestEventsDTO contestEventsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestEventsDTO")), ContestEventsDTO.class);
			
			if(contestEventsDTO.getStatus() == 1){
				model.addAttribute("msg", contestEventsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestEventsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("contestEventList", contestEventsDTO.getAllEventList());
				model.addAttribute("contestEventPagingInfo", contestEventsDTO.getPaginationDTO());
			}else{
				model.addAttribute("contestEventList",contestEventsDTO.getAllEventList());
				model.addAttribute("contestEventPagingInfo",contestEventsDTO.getPaginationDTO());
				model.addAttribute("contestExcludeEventList", contestEventsDTO.getExcludeEventList());
				model.addAttribute("contestExcludeEventPagingInfo", contestEventsDTO.getExcludeEventPaginationDTO());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateExcludeContestEvents")
	public String updateExcludeContestEvents(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			String contestId = request.getParameter("contestId");
			String eventIdStr = request.getParameter("eventIdStr");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("contestId", contestId);
			map.put("eventIdStr", eventIdStr);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_EXCLUDE_CONTEST_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());				
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			model.addAttribute("status", genericResponseDTO.getStatus());
						
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/ExcludeContestEvents")
	public String getExcludeContestEvents(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String contestId = request.getParameter("contestId");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("contestId", contestId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EXCLUDE_CONTEST_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestEventsDTO contestEventsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestEventsDTO")), ContestEventsDTO.class);
			
			if(contestEventsDTO.getStatus() == 1){
				model.addAttribute("msg", contestEventsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestEventsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("contestExcludeEventList", contestEventsDTO.getExcludeEventList());
				model.addAttribute("contestExcludeEventPagingInfo", contestEventsDTO.getExcludeEventPaginationDTO());
			}else{
				model.addAttribute("contestExcludeEventList", contestEventsDTO.getExcludeEventList());
				model.addAttribute("contestExcludeEventPagingInfo", contestEventsDTO.getExcludeEventPaginationDTO());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping("/AutoCompleteArtistEventAndCategory")
	public void getAutoCompleteArtistEventAndCategory(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_ARTIST_EVENT_CATEGORIES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistAndCategoryDTO")), AutoCompleteArtistAndCategoryDTO.class);
						
			if(autoCompleteArtistAndCategoryDTO.getStatus() == 1){
				writer.write(autoCompleteArtistAndCategoryDTO.getArtistAndCategory());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/AutoCompleteGiftCard")
	public void getAutoCompleteGiftCard(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_GIFTCARD);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistAndCategoryDTO")), AutoCompleteArtistAndCategoryDTO.class);
						
			if(autoCompleteArtistAndCategoryDTO.getStatus() == 1){
				writer.write(autoCompleteArtistAndCategoryDTO.getArtistAndCategory());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/AutoCompleteCustomer")
	public void getAutoCompleteCustomer(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_CUSTOMER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistAndCategoryDTO customerDetails = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistAndCategoryDTO")), AutoCompleteArtistAndCategoryDTO.class);
						
			if(customerDetails.getStatus() == 1){
				writer.write(customerDetails.getArtistAndCategory());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/UpdateQuestionBank")
	public String updateQuestionBank(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");			
			String qBId = request.getParameter("qBId");
			String qBText = request.getParameter("qBText");
			String qBOptionA = request.getParameter("qBOptionA");
			String qBOptionB = request.getParameter("qBOptionB");
			String qBOptionC = request.getParameter("qBOptionC");
			String difficultyLevel = request.getParameter("difficultyLevel");
			//String qBOptionD = request.getParameter("qBOptionD");
			//String qBNo = request.getParameter("qBNo");
			String qBAnswer =request.getParameter("qBAnswer");
			String qBReward = request.getParameter("qBReward");
			String qBCategory = request.getParameter("qBCategory");
			String pageNo = request.getParameter("pageNo");
			String factChecked = request.getParameter("factChecked");

			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("qBId", qBId);
			map.put("qBText", qBText);
			map.put("qBOptionA", qBOptionA);
			map.put("qBOptionB", qBOptionB);
			map.put("qBOptionC", qBOptionC);
			map.put("difficultyLevel", difficultyLevel);
			//map.put("qBNo", qBNo);
			map.put("qBAnswer", qBAnswer);
			map.put("qBReward", qBReward);
			map.put("qBCategory", qBCategory);
			map.put("pageNo", pageNo);
			map.put("userName", userName);
			map.put("factChecked",factChecked);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_QUESTION_BANK);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			QuestionBankDTO questionBankDTO = gson.fromJson(((JsonObject)jsonObject.get("questionBankDTO")), QuestionBankDTO.class);
			
			if(questionBankDTO.getError() == null && questionBankDTO.getStatus() == 1){
				model.addAttribute("msg", questionBankDTO.getMessage());
				model.addAttribute("questionBankList", questionBankDTO.getQuestionBankList());
				model.addAttribute("questionBankPagingInfo", questionBankDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", questionBankDTO.getError().getDescription());				
			}
			model.addAttribute("status", questionBankDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping("/AutoCompleteQuestionBankCategory")
	public void getAutoCompleteQuestionBankCategory(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_QUESTION_BANK_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteDTO autoCompleteDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteDTO")), AutoCompleteDTO.class);
						
			if(autoCompleteDTO.getStatus() == 1){
				writer.write(autoCompleteDTO.getAutoCompleteStringResponse());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/ContestQuestionBank")
	public String getContestQuestionBank(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CONTEST_QUESTION_BANK);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestsDTO contestsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestsDTO")), ContestsDTO.class);
			
			if(contestsDTO.getStatus() == 1){
				model.addAttribute("msg", contestsDTO.getMessage());
			}else{
				model.addAttribute("msg", contestsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("contestQuesBankList", contestsDTO.getQuestionBankList());
				model.addAttribute("contestQuesBankPagingInfo", contestsDTO.getQuestionBankPaginationDTO());
			}else{				
				model.addAttribute("contestQuesBankList", gson.toJson(contestsDTO.getQuestionBankList()));
				model.addAttribute("contestQuesBankPagingInfo", gson.toJson(contestsDTO.getQuestionBankPaginationDTO()));				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/ViewContestWinners")
	public String viewContestWinners(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestIdStr = request.getParameter("contestId");
			String headerFilter = request.getParameter("headerFilter");
			String headerFilter1 = request.getParameter("headerFilter1");
			String type = request.getParameter("type");
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId",contestIdStr);
			map.put("headerFilter",headerFilter);
			map.put("headerFilter1",headerFilter1);
			map.put("type",type);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_VIEW_CONTEST_WINNER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestWinnerDTO winnerDTO = gson.fromJson(((JsonObject)jsonObject.get("contestWinnerDTO")), ContestWinnerDTO.class);
			if(winnerDTO.getStatus()==1){
				model.addAttribute("summaryWinners",winnerDTO.getSummaryWinners());
				model.addAttribute("summaryWinnerPagingInfo", winnerDTO.getSummaryPagination());
				model.addAttribute("grandWinners",winnerDTO.getGrandWinners());
				model.addAttribute("grandWinnerPagingInfo", winnerDTO.getGrandWinnerPagination());
			}else{
				model.addAttribute("msg",winnerDTO.getError().getDescription());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	

	@RequestMapping(value = "/UpdateExpiryDate")
	public String updateWinnerExpiryDate(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String winnerId = request.getParameter("winnerId");
			String expiryDate = request.getParameter("expiryDate");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName",userName);
			map.put("winnerId",winnerId);
			map.put("expiryDate",expiryDate);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_WINNER_EXPIRY_DATE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestWinnerDTO winnerDTO = gson.fromJson(((JsonObject)jsonObject.get("contestWinnerDTO")), ContestWinnerDTO.class);
			if(winnerDTO.getStatus()==1){
				model.addAttribute("msg",winnerDTO.getMessage());
			}else{
				model.addAttribute("msg",winnerDTO.getError().getDescription());
			}
			model.addAttribute("status",winnerDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/ContestPromocodes")
	public String getContestReferalCode(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String promocodeId = request.getParameter("promocodeId");
			String headerFilter = request.getParameter("headerFilter");
			String statusType = request.getParameter("status");
			String isReset = request.getParameter("isReset");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("promocodeId", promocodeId);
			map.put("headerFilter", headerFilter);
			map.put("status",statusType);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CONTEST_PROMOCODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestPromocodeDTO promoCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("contestPromocodeDTO")), ContestPromocodeDTO.class);
			
			if(promoCodeDTO.getStatus() == 1){
				if(statusType.equalsIgnoreCase("FREETICKET")){
					model.addAttribute("statusType",statusType);
					if(promocodeId!=null && !promocodeId.isEmpty()){
						model.addAttribute("discountCode",gson.toJson( promoCodeDTO.getContestPromocode()));
						model.addAttribute("status", promoCodeDTO.getStatus());
						return "";
					}else{
						model.addAttribute("discountCodePagingInfo", gson.toJson(promoCodeDTO.getDiscountCodePaginationDTO()));
						model.addAttribute("discountCodeList", gson.toJson(promoCodeDTO.getContestPromocodes()));
						model.addAttribute("status", promoCodeDTO.getStatus());
						if((headerFilter != null && !headerFilter.isEmpty()) || (isReset!=null && isReset.equalsIgnoreCase("true"))){
							return "";
						}
					}
				}else if(statusType.equalsIgnoreCase("FREELIVES")){
					model.addAttribute("statusType",statusType);
					if(promocodeId!=null && !promocodeId.isEmpty()){
						model.addAttribute("promoCode",gson.toJson( promoCodeDTO.getPromoOffer()));
						model.addAttribute("status", promoCodeDTO.getStatus());
						return "";
					}else{
						model.addAttribute("promoPagingInfo", gson.toJson(promoCodeDTO.getDiscountCodePaginationDTO()));
						model.addAttribute("promoCodeList", gson.toJson(promoCodeDTO.getPromoOffers()));
						model.addAttribute("status", promoCodeDTO.getStatus());
						if((headerFilter != null && !headerFilter.isEmpty()) || (isReset!=null && isReset.equalsIgnoreCase("true"))){
							return "";
						}
					}
				}
			}else{
				model.addAttribute("msg", promoCodeDTO.getError().getDescription());
				model.addAttribute("status", promoCodeDTO.getStatus());
				model.addAttribute("statusType",statusType);
			}
			
			if(statusType.equalsIgnoreCase("FREETICKET")){
				return "page-contest-global-referral-code";
				
			}else if(statusType.equalsIgnoreCase("FREELIVES")){
				return "page-contest-earn-lives-promo";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching promotional codes.");
			model.addAttribute("status",1);
		}
		return "page-contest-global-referral-code";
	}
	
	
	@RequestMapping(value = "/UpdateContestPromocodeStatus")
	public String updateContestPromocode(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String promocodeId = request.getParameter("promocodeId");
			String status = request.getParameter("status");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("promocodeId", promocodeId);
			map.put("status", status);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CONTEST_PROMOCODE_STATUS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestPromocodeDTO promoCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("contestPromocodeDTO")), ContestPromocodeDTO.class);
			
			if(promoCodeDTO.getStatus() == 1){
				model.addAttribute("discountCodePagingInfo", gson.toJson(promoCodeDTO.getDiscountCodePaginationDTO()));
				model.addAttribute("discountCodeList", gson.toJson(promoCodeDTO.getContestPromocodes()));
				model.addAttribute("status", promoCodeDTO.getStatus());
				model.addAttribute("msg", promoCodeDTO.getMessage());
			}else{
				model.addAttribute("msg", promoCodeDTO.getError().getDescription());
				model.addAttribute("status", promoCodeDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching promotional codes.");
			model.addAttribute("status",1);
		}
		return "";
		
	}
	
	
	
	@RequestMapping(value = "/GenerateContestPromocode")
	public String generateContestReferalCode(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String action = request.getParameter("action");
			String promoDisciption = request.getParameter("promoDisciption");
			String freeTickets = request.getParameter("freeTickets");
			String artistId = request.getParameter("artistId");
			String eventId = request.getParameter("eventId");
			String parentId = request.getParameter("parentId");
			String childId = request.getParameter("childId");
			String grandChildId = request.getParameter("grandChildId");
			String customerId = request.getParameter("customerId");
			String expiryDate = request.getParameter("expiryDate");
			String promoId = request.getParameter("promoId");
			String artistEventCategoryName = request.getParameter("artistEventCategoryName");
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("promoDisciption", promoDisciption);
			map.put("freeTickets", freeTickets);
			map.put("artistId", artistId);
			map.put("eventId", eventId);
			map.put("parentId", parentId);
			map.put("childId", childId);
			map.put("grandChildId", grandChildId);
			map.put("expiryDate", expiryDate);
			map.put("promoId", promoId);
			map.put("customerId",customerId);
			map.put("userName",userName);
			map.put("artistEventCategoryName",artistEventCategoryName);
			
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CONTEST_PROMOCODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestPromocodeDTO promoCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("contestPromocodeDTO")), ContestPromocodeDTO.class);
			
			if(promoCodeDTO.getStatus() == 1){
				model.addAttribute("discountCodePagingInfo", gson.toJson(promoCodeDTO.getDiscountCodePaginationDTO()));
				model.addAttribute("discountCodeList", gson.toJson(promoCodeDTO.getContestPromocodes()));
				model.addAttribute("status", promoCodeDTO.getStatus());
				model.addAttribute("msg",promoCodeDTO.getMessage());
			}else{
				model.addAttribute("msg", promoCodeDTO.getError().getDescription());
				model.addAttribute("status", promoCodeDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while updating promotional codes.");
			model.addAttribute("status",1);
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GenerateContestEarnLivePromo")
	public String generateContestEarnLivesCode(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String action = request.getParameter("action");
			String promoCode = request.getParameter("promoCode");
			String lifePerCustomer = request.getParameter("lifePerCustomer");
			String maxThreshold = request.getParameter("maxThreshold");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String promoId = request.getParameter("promoId");
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("promoCode", promoCode);
			map.put("lifePerCustomer", lifePerCustomer);
			map.put("maxThreshold", maxThreshold);
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			map.put("promoId", promoId);
			map.put("userName",userName);
			
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_EARNLIVE_PROMOCODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestPromocodeDTO promoCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("contestPromocodeDTO")), ContestPromocodeDTO.class);
			
			if(promoCodeDTO.getStatus() == 1){
				model.addAttribute("promoPagingInfo", gson.toJson(promoCodeDTO.getDiscountCodePaginationDTO()));
				model.addAttribute("promoCodeList", gson.toJson(promoCodeDTO.getPromoOffers()));
				model.addAttribute("status", promoCodeDTO.getStatus());
				model.addAttribute("msg",promoCodeDTO.getMessage());
			}else{
				model.addAttribute("msg", promoCodeDTO.getError().getDescription());
				model.addAttribute("status", promoCodeDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while updating promotional codes.");
			model.addAttribute("status",1);
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/UpdateContestEarnLivesOfferStatus")
	public String updateContestEarlLivesPromo(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String promocodeId = request.getParameter("promocodeId");
			String status = request.getParameter("status");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("promocodeId", promocodeId);
			map.put("status", status);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_EARNLIVES_PROMOCODE_STATUS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestPromocodeDTO promoCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("contestPromocodeDTO")), ContestPromocodeDTO.class);
			
			if(promoCodeDTO.getStatus() == 1){
				model.addAttribute("promoPagingInfo", gson.toJson(promoCodeDTO.getDiscountCodePaginationDTO()));
				model.addAttribute("promoCodeList", gson.toJson(promoCodeDTO.getPromoOffers()));
				model.addAttribute("status", promoCodeDTO.getStatus());
				model.addAttribute("msg", promoCodeDTO.getMessage());
			}else{
				model.addAttribute("msg", promoCodeDTO.getError().getDescription());
				model.addAttribute("status", promoCodeDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching promotional codes.");
			model.addAttribute("status",0);
		}
		return "";
		
	}
	
	
	@RequestMapping(value = "/ContestSettings")
	public String getContestSetttingPage(HttpServletRequest request, HttpServletResponse response,Model model){
		
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GENERATE_DISCOUNT_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenerateDiscountCodeDTO generateDiscountCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("generateDiscountCodeDTO")), GenerateDiscountCodeDTO.class);
			
			if(generateDiscountCodeDTO.getStatus() == 1){
				model.addAttribute("msg", generateDiscountCodeDTO.getMessage());
				model.addAttribute("status", generateDiscountCodeDTO.getStatus());
			}else{
				model.addAttribute("msg", generateDiscountCodeDTO.getError().getDescription());
				model.addAttribute("status", generateDiscountCodeDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("discountCodePagingInfo", generateDiscountCodeDTO.getDiscountCodePaginationDTO());
				model.addAttribute("discountCodeList", generateDiscountCodeDTO.getPromoOfferDTO());
			}else{
				model.addAttribute("discountCodePagingInfo", gson.toJson(generateDiscountCodeDTO.getDiscountCodePaginationDTO()));
				model.addAttribute("promoOfferDtlPagingInfo", gson.toJson(generateDiscountCodeDTO.getPromoOfferDetailPaginationDTO()));
				model.addAttribute("discountCodeList", gson.toJson(generateDiscountCodeDTO.getPromoOfferDTO()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching Contest Settings.");
			model.addAttribute("status",0);
		}
		return "page-contest-manual-notification";
	}
	
	
	@RequestMapping(value="/ContestCustomerQuestions")
	public String getCustomerQuestions(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("customerQuestionDTO")), CustomerQuestionDTO.class);
			if(questionDTO.getStatus() == 1){
				model.addAttribute("submitTriviaList", gson.toJson(questionDTO.getCustomerQuestions()));
				model.addAttribute("submitTriviaPagingInfo",gson.toJson( questionDTO.getQuestionPagination()));
				model.addAttribute("status", questionDTO.getStatus());
				if(headerFilter!=null && !headerFilter.isEmpty()){
					return "";
				}
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());
				model.addAttribute("status", questionDTO.getStatus());
			}
			model.addAttribute("statusType",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching Contest Customer Quesstions.");
			model.addAttribute("status",0);
		}
		return "page-customer-contest-questions";
	}
	
	
	@RequestMapping(value="/UpdateSubmitTrivia")
	public String updateSubmitTrivia(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("status");
			String action = request.getParameter("action");
			String ids = request.getParameter("ids");
			String headerFilter = request.getParameter("headerFilter");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("headerFilter", headerFilter);
			map.put("action", action);
			map.put("ids", ids);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CUSTOMER_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("customerQuestionDTO")), CustomerQuestionDTO.class);
			if(questionDTO.getStatus() == 1){
				model.addAttribute("submitTriviaList", gson.toJson(questionDTO.getCustomerQuestions()));
				model.addAttribute("submitTriviaPagingInfo",gson.toJson( questionDTO.getQuestionPagination()));
				model.addAttribute("status", questionDTO.getStatus());
				model.addAttribute("msg", questionDTO.getMessage());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());
				model.addAttribute("status", questionDTO.getStatus());
			}
			model.addAttribute("statusType",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while updating Contest Customer Quesstions.");
			model.addAttribute("status",0);
		}
		return "";
	}
	
	
	@RequestMapping(value="/PreContestChecklistMobile")
	public String getContestChecklistMobilePage(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("headerFilter", headerFilter);
			map.put("type","MOBILE");
			
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CONTEST_CHECKLIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestChecklistDTO checklistDTO = gson.fromJson(((JsonObject)jsonObject.get("contestChecklistDTO")), ContestChecklistDTO.class);
			if(checklistDTO.getStatus()==1){
				model.addAttribute("users",checklistDTO.getUsers());
				model.addAttribute("contest",gson.toJson(checklistDTO.getContest()));
				model.addAttribute("preContestList",gson.toJson(checklistDTO.getPreContestChecklists()));
				model.addAttribute("preContestPagingInfo",gson.toJson(checklistDTO.getPreContestPaginationDTO()));
				model.addAttribute("status", checklistDTO.getStatus());
				model.addAttribute("msg", checklistDTO.getMessage());
			}else{
				model.addAttribute("status", checklistDTO.getStatus());
				if(checklistDTO.getError()!=null){
					model.addAttribute("msg", checklistDTO.getError().getDescription());
				}else{
					model.addAttribute("msg", checklistDTO.getMessage());
				}
			}
			model.addAttribute("preContestStatus",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching Contest Checklist DataS.");
			model.addAttribute("status",0);
		}
		return "page-pre-contest-checklist-mobile";
	}
	
	
	@RequestMapping(value="/PreContestChecklistWeb")
	public String getContestChecklistWebPage(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("headerFilter", headerFilter);
			map.put("type","WEB");
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CONTEST_CHECKLIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestChecklistDTO checklistDTO = gson.fromJson(((JsonObject)jsonObject.get("contestChecklistDTO")), ContestChecklistDTO.class);
			if(checklistDTO.getStatus()==1){
				model.addAttribute("users",checklistDTO.getUsers());
				model.addAttribute("contest",gson.toJson(checklistDTO.getContest()));
				model.addAttribute("preContestList",gson.toJson(checklistDTO.getPreContestChecklists()));
				model.addAttribute("preContestPagingInfo",gson.toJson(checklistDTO.getPreContestPaginationDTO()));
				model.addAttribute("status", checklistDTO.getStatus());
				model.addAttribute("msg", checklistDTO.getMessage());
			}else{
				model.addAttribute("status", checklistDTO.getStatus());
				if(checklistDTO.getError()!=null){
					model.addAttribute("msg", checklistDTO.getError().getDescription());
				}else{
					model.addAttribute("msg", checklistDTO.getMessage());
				}
			}
			model.addAttribute("preContestStatus",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching Contest Checklist DataS.");
			model.addAttribute("status",0);
		}
		return "page-pre-contest-checklist-web";
	}
	
	
	@RequestMapping(value="/GetPreContestChecklist")
	public String getContestChecklist(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("status");
			String action = request.getParameter("action");
			String type = request.getParameter("type");
			String headerFilter = request.getParameter("headerFilter");
			
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("action", action);
			map.put("headerFilter", headerFilter);
			map.put("type",type);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CONTEST_CHECKLIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestChecklistDTO checklistDTO = gson.fromJson(((JsonObject)jsonObject.get("contestChecklistDTO")), ContestChecklistDTO.class);
			if(checklistDTO.getStatus()==1){
				model.addAttribute("preContestList",checklistDTO.getPreContestChecklists());
				model.addAttribute("preContestPagingInfo",checklistDTO.getPreContestPaginationDTO());
				model.addAttribute("status", checklistDTO.getStatus());
				model.addAttribute("msg", checklistDTO.getMessage());
			}else{
				model.addAttribute("status", checklistDTO.getStatus());
				if(checklistDTO.getError()!=null){
					model.addAttribute("msg", checklistDTO.getError().getDescription());
				}else{
					model.addAttribute("msg", checklistDTO.getMessage());
				}
			}
			model.addAttribute("preContestStatus",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching Contest Checklist Data.");
			model.addAttribute("status",0);
		}
		return "";
	}
	
	
	
	@RequestMapping(value="/GetPreContestChecklistLogs")
	public String getContestChecklistLogs(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String preContestId = request.getParameter("preContestId");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("preContestId", preContestId);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CONTEST_CHECKLIST_LOGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestChecklistTrackingDTO checklistDTO = gson.fromJson(((JsonObject)jsonObject.get("contestChecklistTrackingDTO")), ContestChecklistTrackingDTO.class);
			if(checklistDTO.getStatus()==1){
				model.addAttribute("preContestListLogs",checklistDTO.getTrackings());
				model.addAttribute("preContestLogsPagingInfo",checklistDTO.getTrackingPagination());
				model.addAttribute("status", checklistDTO.getStatus());
			}else{
				model.addAttribute("status", checklistDTO.getStatus());
				if(checklistDTO.getError()!=null){
					model.addAttribute("msg", checklistDTO.getError().getDescription());
				}else{
					model.addAttribute("msg", checklistDTO.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while fetching Contest Checklist Data.");
			model.addAttribute("status",0);
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdatePreContestChecklist")
	public String updatePreContestChecklist(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			String contestCategory = request.getParameter("contestCategory");
			String participantStar = request.getParameter("participantStar");
			String referralStar = request.getParameter("referralStar");
			String contestName = request.getParameter("contestName");
			String extendedName = request.getParameter("extendedName");
			String noOfQuestion = request.getParameter("noOfQuestion");
			String zone = request.getParameter("zone");
			String preContestId = request.getParameter("preContestId");
			String action = request.getParameter("action");
			String dateOfGame = request.getParameter("dateOfGame");
			String timeOfGame1 = request.getParameter("timeOfGame1");
			String timeOfGame2 = request.getParameter("timeOfGame2");
			String rewardDollarsPrize = request.getParameter("rewardDollarsPrize");
			String grandPrizeEvent = request.getParameter("grandPrizeEvent");
			String noOfTickets = request.getParameter("noOfTickets");
			String noOfGrandWinner = request.getParameter("noOfGrandWinner");
			String category = request.getParameter("categoryName");
			String categoryId = request.getParameter("categoryId");
			String categoryType = request.getParameter("categoryType");
			String category1 = request.getParameter("categoryName1");
			String categoryId1 = request.getParameter("categoryId1");
			String categoryType1 = request.getParameter("categoryType1");
			String pricePerTicket = request.getParameter("pricePerTicket");
			String discountCode = request.getParameter("discountCode");
			String discountPercentage = request.getParameter("discountPercentage");
			
			
			String hostName = request.getParameter("hostName");
			String producerName = request.getParameter("producerName");
			String directorName = request.getParameter("directorName");
			String techDirectorName = request.getParameter("techDirectorName");
			String opearatorName = request.getParameter("opearatorName");
			String isQuestionEnteredStr = request.getParameter("isQuestionEntered");
			String questionEnteredByStr = request.getParameter("questionEnteredBy");
			String isQuestionVerifiedStr = request.getParameter("isQuestionVerified");
			String questionVerifiedByStr = request.getParameter("questionVerifiedBy");
			String rewardDollarsPerQueStr = request.getParameter("rewardDollarsPerQue");
			String approvedbyStr = request.getParameter("approvedBy");
			String status = request.getParameter("status");
			String type = request.getParameter("type");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestName", contestName);
			map.put("extendedName", extendedName);
			map.put("noOfQuestion", noOfQuestion);
			map.put("zone", zone);
			map.put("type", type);
			map.put("action", action);
			map.put("dateOfGame", dateOfGame);
			map.put("timeOfGame1", timeOfGame1);
			map.put("timeOfGame2", timeOfGame2);
			map.put("rewardDollarsPrize", rewardDollarsPrize);
			map.put("grandPrizeEvent", grandPrizeEvent);
			map.put("noOfTickets", noOfTickets);
			map.put("noOfGrandWinner", noOfGrandWinner);
			map.put("category", category);
			map.put("categoryId", categoryId);
			map.put("categoryType", categoryType);
			map.put("category1", category1);
			map.put("categoryId1", categoryId1);
			map.put("categoryType1", categoryType1);
			map.put("pricePerTicket", pricePerTicket);
			map.put("discountCode", discountCode);
			map.put("discountPercentage", discountPercentage);
			map.put("preContestId",preContestId);
			map.put("userName", userName);
			map.put("hostName", hostName);
			map.put("producerName", producerName);
			map.put("directorName", directorName);
			map.put("techDirectorName", techDirectorName);
			map.put("opearatorName", opearatorName);
			map.put("isQuestionEntered", isQuestionEnteredStr);
			map.put("questionEnteredBy", questionEnteredByStr);
			map.put("isQuestionVerified", isQuestionVerifiedStr);
			map.put("questionVerifiedBy", questionVerifiedByStr);
			map.put("rewardDollarsPerQue", rewardDollarsPerQueStr);
			map.put("approvedBy", approvedbyStr);
			map.put("contestCategory", contestCategory);
			map.put("participantStar", participantStar);
			map.put("referralStar", referralStar);
			map.put("status", status);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CONTEST_CHECKLIST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestChecklistDTO checklistDTO = gson.fromJson(((JsonObject)jsonObject.get("contestChecklistDTO")), ContestChecklistDTO.class);
			if(checklistDTO.getStatus()==1){
				if(action.equalsIgnoreCase("EDIT")){
					model.addAttribute("preContest",gson.toJson(checklistDTO.getPreContestChecklist()));
				}else{
					model.addAttribute("preContestList",gson.toJson(checklistDTO.getPreContestChecklists()));
					model.addAttribute("preContestPagingInfo",gson.toJson(checklistDTO.getPreContestPaginationDTO()));
				}
				model.addAttribute("status", checklistDTO.getStatus());
				model.addAttribute("msg", checklistDTO.getMessage());
			}else{
				model.addAttribute("status", checklistDTO.getStatus());
				if(checklistDTO.getError()!=null){
					model.addAttribute("msg", checklistDTO.getError().getDescription());
				}else{
					model.addAttribute("msg", checklistDTO.getMessage());
				}
			}
			model.addAttribute("preContestStatus",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while Updating pre contest checklist");
			model.addAttribute("status",0);
		}
		return "";
		
	}
	
	
	@RequestMapping(value = "/CopyContestSetup")
	public String copyContestSetup(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			String contestId = request.getParameter("contestId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_COPY_CONTEST_SETUP);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			if(genericDTO.getStatus()==1){
				model.addAttribute("msg", genericDTO.getMessage());
			}else{
				model.addAttribute("msg", genericDTO.getError().getDescription());
			}
			model.addAttribute("status", genericDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while Updating pre contest checklist");
			model.addAttribute("status",0);
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/UpdateOneSignalNotifications")
	public String updateOneSignalNotifications(HttpServletRequest request, HttpServletResponse response, Model model){
		try {
			Map<String, String> map1 = Util.getParameterMap(request);
			String data1 = Util.getObject(map1,Constants.BASE_URL+Constants.GET_ONESIGNAL_NOTIFICATION);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			CustomerStats resp = gson1.fromJson(((JsonObject)jsonObject1.get("customerStats")), CustomerStats.class);
			if(resp.getStatus()==1){
				Thread.sleep(50000);
				String fileUrl = resp.getMessage();
				map1.put("fileUrl",fileUrl);
				String data2 = Util.getObject(map1,Constants.BASE_URL+Constants.UPDATE_ONESIGNAL_NOTIFICATION);
				Gson gson2 = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject2 = gson2.fromJson(data2, JsonObject.class);
				CustomerStats resp1 = gson2.fromJson(((JsonObject)jsonObject2.get("customerStats")), CustomerStats.class);
				if(resp1.getStatus()==1){
					model.addAttribute("msg", resp.getMessage());
				}else{
					model.addAttribute("msg", resp.getError().getDescription());
				}
			}else{
				model.addAttribute("msg", resp.getError().getDescription());
			}
			model.addAttribute("status", resp.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg","Error occured while Updating One Signal Notification Ids.");
			model.addAttribute("status",0);
		}
		return "";
	}
	
	
	@RequestMapping(value = "/ReferralRewardSetting")
	public String referralRewardSetting(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String liveUrl = request.getParameter("liveUrl");
			String liveThreshold = request.getParameter("liveThreshold");
			String notification7 = request.getParameter("notification7");
			String notification20 = request.getParameter("notification20");
			String notificationManual = request.getParameter("notificationManual");

			/*
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action",action);
			map.put("liveUrl",liveUrl);
			map.put("liveThreshold",liveThreshold);
			map.put("notification7",notification7);
			map.put("notification20",notification20);
			map.put("notificationManual",notificationManual);
			map.put("userName",userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_QUIZ_CONFIG_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ContestConfigSettingsDTO contestConfigsettingsDTO = gson.fromJson(((JsonObject)jsonObject.get("contestConfigSettingsDTO")), ContestConfigSettingsDTO.class);
			
			if(contestConfigsettingsDTO.getError() == null && contestConfigsettingsDTO.getStatus() == 1){
				model.addAttribute("msg", contestConfigsettingsDTO.getMessage());
				model.addAttribute("configSettings", contestConfigsettingsDTO.getContestConfigSettings());
			}else{
				model.addAttribute("msg", contestConfigsettingsDTO.getError().getDescription());				
			}
			model.addAttribute("status", contestConfigsettingsDTO.getStatus());*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-contest-settings";
	}

	@RequestMapping(value = "/RtfCatsEventsList")
	public String getRtfCatsEventsList(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String selectedId = request.getParameter("selectedId");
			String selectedType = request.getParameter("selectedType");
			String dataType = request.getParameter("dataType");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("selectedId", selectedId);
			map.put("selectedType", selectedType);
			map.put("selectedType", selectedType);
			map.put("dataType", dataType);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_RTF_CATS_EVENTS_LIST);
			System.out.println(data);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfCatsEventsDTO rtfCatsEventsDTO = gson.fromJson(((JsonObject)jsonObject.get("rtfCatsEventsDTO")), RtfCatsEventsDTO.class);
			
			if(rtfCatsEventsDTO.getStatus() == 1){
				model.addAttribute("msg", rtfCatsEventsDTO.getMessage());
			}else{
				model.addAttribute("msg", rtfCatsEventsDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("rtfCatsEventList", rtfCatsEventsDTO.getAllEventList());
				model.addAttribute("rtfCatsEventPagingInfo", rtfCatsEventsDTO.getPaginationDTO());
				
				model.addAttribute("rtfCatsExcludeEventList", rtfCatsEventsDTO.getExcludeEventList());
				model.addAttribute("rtfCatsExcludeEventPagingInfo", rtfCatsEventsDTO.getExcludeEventPaginationDTO());
			}else{
				model.addAttribute("rtfCatsEventList",rtfCatsEventsDTO.getAllEventList());
				model.addAttribute("rtfCatsEventPagingInfo",rtfCatsEventsDTO.getPaginationDTO());
				model.addAttribute("rtfCatsExcludeEventList", rtfCatsEventsDTO.getExcludeEventList());
				model.addAttribute("rtfCatsExcludeEventPagingInfo", rtfCatsEventsDTO.getExcludeEventPaginationDTO());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateRtfCatsEvents")
	public String updateRtfCatsEvents(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			String eventIdStr = request.getParameter("eventIdStr");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("eventIdStr", eventIdStr);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_RTF_CATS_EVENTS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());				
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			}
			model.addAttribute("status", genericResponseDTO.getStatus());
						
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/GiftCards")
	public String getGiftCards(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String gcStatus = request.getParameter("gcStatus");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("gcStatus", gcStatus);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_GIFT_CARDS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfGiftCardsDTO cardDTO = gson.fromJson(((JsonObject)jsonObject.get("rtfGiftCardsDTO")), RtfGiftCardsDTO.class);
			
			if(cardDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("cards", cardDTO.getGiftCards());
					model.addAttribute("brands", cardDTO.getBrands());
					model.addAttribute("cardPagingInfo", cardDTO.getPagination());
				}else{
					model.addAttribute("cards", gson.toJson(cardDTO.getGiftCards()));
					model.addAttribute("brands", gson.toJson(cardDTO.getBrands()));
					model.addAttribute("cardPagingInfo",gson.toJson(cardDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", cardDTO.getError().getDescription());
			}
			model.addAttribute("gcStatus",gcStatus);
			model.addAttribute("status",cardDTO.getStatus());
			
			/*if(request.getRequestURI().contains(".json")){
				model.addAttribute("cards", cardDTO.getGiftCards());
				model.addAttribute("cardPagingInfo", cardDTO.getPagination());
			}else{
				model.addAttribute("rtfCatsEventList",rtfCatsEventsDTO.getAllEventList());
				model.addAttribute("rtfCatsEventPagingInfo",rtfCatsEventsDTO.getPaginationDTO());
				model.addAttribute("rtfCatsExcludeEventList", rtfCatsEventsDTO.getExcludeEventList());
				model.addAttribute("rtfCatsExcludeEventPagingInfo", rtfCatsEventsDTO.getExcludeEventPaginationDTO());
			}*/
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-manage-gift-cards";
	}
	
	
	
	
	@RequestMapping(value = "/UpdateGiftCard")
	public String updateGiftCards(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String gcStatus = request.getParameter("gcStatus");
			String cardId = request.getParameter("cardId");
			String title = request.getParameter("title");
			String gcBrand = request.getParameter("gcBrand");
			String description = request.getParameter("description");
			String conversionType = request.getParameter("conversionType");
			String conversionRate = request.getParameter("conversionRate");
			String pointConversionRate = request.getParameter("pointConversionRate");
			String amount = request.getParameter("amount");
			String quantity = request.getParameter("quantity");
			String qtyThreshold = request.getParameter("qtyThreshold");
			//String imageUrl = request.getParameter("imageUrl");
			String action = request.getParameter("action");
			//String fileRequired = request.getParameter("fileRequired");
			String cardType = request.getParameter("cardType");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("cardId", cardId);
			map.put("gcStatus", gcStatus);
			map.put("title", title);
			map.put("gcBrand",gcBrand);
			map.put("description", description);
			map.put("conversionType", conversionType);
			map.put("conversionRate", conversionRate);
			map.put("amount", amount);
			map.put("quantity", quantity);
			map.put("qtyThreshold", qtyThreshold);
			//map.put("imageUrl", imageUrl);
			map.put("action", action);
			//map.put("fileRequired",fileRequired);
			map.put("cardType",cardType);
			map.put("userName",userName);
			map.put("startDate",startDate);
			map.put("endDate",endDate);
			map.put("pointConversionRate",pointConversionRate);
			
			
			
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_GIFT_CARDS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfGiftCardsDTO cardDTO = gson.fromJson(((JsonObject)jsonObject.get("rtfGiftCardsDTO")), RtfGiftCardsDTO.class);
			
			if(cardDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("cards", cardDTO.getGiftCards());
					model.addAttribute("cardPagingInfo", cardDTO.getPagination());
					model.addAttribute("card", cardDTO.getGiftCard());
					model.addAttribute("qtyList", cardDTO.getQtyList());
				}else{
					model.addAttribute("cards", gson.toJson(cardDTO.getGiftCards()));
					model.addAttribute("cardPagingInfo",gson.toJson(cardDTO.getPagination()));
					model.addAttribute("card",gson.toJson( cardDTO.getGiftCard()));
					model.addAttribute("qtyList",gson.toJson( cardDTO.getQtyList()));
				}
				model.addAttribute("msg",cardDTO.getMessage());
			}else{
				model.addAttribute("msg", cardDTO.getError().getDescription());
			}
			model.addAttribute("gcStatus",gcStatus);
			model.addAttribute("status",cardDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/GiftCardOrders")
	public String loadGiftCardListingPage(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String status = request.getParameter("gcStatus");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("status", status);
			map.put("type", "MOBILE");

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_GIFTCARD_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			GiftCardOrdersDTO giftCardOrdersDTO = gson.fromJson(((JsonObject) jsonObject.get("giftCardOrdersDTO")),GiftCardOrdersDTO.class);
			
			if(giftCardOrdersDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("orders", giftCardOrdersDTO.getGiftCardOrdersList());
					model.addAttribute("orderPagingInfo", giftCardOrdersDTO.getPaginationDTO());
				}else{
					model.addAttribute("orders", gson.toJson(giftCardOrdersDTO.getGiftCardOrdersList()));
					model.addAttribute("orderPagingInfo",gson.toJson(giftCardOrdersDTO.getPaginationDTO()));
				}
				model.addAttribute("msg",giftCardOrdersDTO.getMessage());
			}else{
				model.addAttribute("msg", giftCardOrdersDTO.getError().getDescription());
				model.addAttribute("orders", giftCardOrdersDTO.getGiftCardOrdersList());
				model.addAttribute("orderPagingInfo", giftCardOrdersDTO.getPaginationDTO());
			}
			model.addAttribute("gcStatus",status);
			model.addAttribute("status",giftCardOrdersDTO.getStatus());

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-gift-card-orders";
	}
	
	
	
	
	// To be Called on Right Click of Gift Card Order Listing   [  Edit Orders Menu ]  
	@RequestMapping(value = "/EditGiftCardOrder")
	public String editGiftCardOrder(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String orderId = request.getParameter("orderId");
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderId", orderId);
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();		
			map.put("userName", userName);
		
			//Change to new Controller Url 
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_GIFTCARD_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GiftCardOrdersEditDTO giftCardOrdersEditDTO = gson.fromJson(((JsonObject)jsonObject.get("giftCardOrdersEditDTO")), GiftCardOrdersEditDTO.class);
			
			if(giftCardOrdersEditDTO.getStatus() == 1){				
				model.addAttribute("msg", giftCardOrdersEditDTO.getMessage());
				model.addAttribute("status", giftCardOrdersEditDTO.getStatus());
				model.addAttribute("orders", giftCardOrdersEditDTO.getGiftCardOrders());				
				model.addAttribute("customer", giftCardOrdersEditDTO.getCustomer());
				model.addAttribute("shipping", giftCardOrdersEditDTO.getCustomerAddress());
				model.addAttribute("egiftcardCounts",0);
				model.addAttribute("barcodeCounts",0);
				model.addAttribute("eGiftAttachments",giftCardOrdersEditDTO.geteGiftAttachments());
				model.addAttribute("barcodeAttachments",giftCardOrdersEditDTO.getBarcodeAttachments());
				
				if(giftCardOrdersEditDTO.geteGiftAttachments()!=null){
					model.addAttribute("egiftcardCounts",giftCardOrdersEditDTO.geteGiftAttachments().size());
				}
				if(giftCardOrdersEditDTO.getBarcodeAttachments()!=null){
					model.addAttribute("barcodeCounts",giftCardOrdersEditDTO.getBarcodeAttachments().size());
				}
			}else{
				model.addAttribute("msg", giftCardOrdersEditDTO.getError().getDescription());
				model.addAttribute("status", giftCardOrdersEditDTO.getStatus());
			}			
		
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	 // To be Called on Right Click of Gift Card Order Listing   [  Edit Orders Menu ]  
		@RequestMapping(value = "/SaveGiftCardOrder")
		public String fulFillGiftCardOrders(HttpServletRequest request, HttpServletResponse response, Model model) {
			
			try {
				
				String orderId = request.getParameter("orderId");
				String status = request.getParameter("status");
				String remarks = request.getParameter("giftCardRemark");
				Map<String, String> map = Util.getParameterMap(request);
				map.put("orderId", orderId);
				String userName = SecurityContextHolder.getContext().getAuthentication().getName();		
				map.put("userName", userName);
				map.put("status", status);
				map.put("remarks", remarks);
				
				String data = Util.getObjectFromApi(map,request,Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_GIFTCARD_ORDERS);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				GiftCardOrdersDTO giftCardOrdersDTO = gson.fromJson(((JsonObject) jsonObject.get("giftCardOrdersDTO")),GiftCardOrdersDTO.class);
				
				if(giftCardOrdersDTO.getStatus() == 1){
					if(request.getRequestURI().contains(".json")){
						model.addAttribute("orders", giftCardOrdersDTO.getGiftCardOrdersList());
						model.addAttribute("orderPagingInfo", giftCardOrdersDTO.getPaginationDTO());
					}else{
						model.addAttribute("orders", gson.toJson(giftCardOrdersDTO.getGiftCardOrdersList()));
						model.addAttribute("orderPagingInfo",gson.toJson(giftCardOrdersDTO.getPaginationDTO()));
					}
					model.addAttribute("msg",giftCardOrdersDTO.getMessage());
				}else{
					model.addAttribute("msg", giftCardOrdersDTO.getError().getDescription());
					model.addAttribute("orders", giftCardOrdersDTO.getGiftCardOrdersList());
					model.addAttribute("orderPagingInfo", giftCardOrdersDTO.getPaginationDTO());
				}
				model.addAttribute("gcStatus",status);
				model.addAttribute("status",giftCardOrdersDTO.getStatus());
			
			} catch (Exception e) {
				e.printStackTrace();
				model.addAttribute("msg", "There is something wrong.Please try again..");
			}
			return "";
		}
	
	
	
	
	
	@RequestMapping(value = "/VoidGiftCardOrders")
	public String voidGiftCardOrders(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		try {			
			String orderId = request.getParameter("orderId");
			String status = request.getParameter("status");
			String action = request.getParameter("type");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderId", orderId);
			map.put("userName", userName);
			map.put("status", status);
			map.put("action", action);
		
			//Change to new Controller Url 			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_VOIDE_GIFTCARD_ORDERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GiftCardOrdersDTO giftCardOrdersDTO = gson.fromJson(((JsonObject) jsonObject.get("giftCardOrdersDTO")),GiftCardOrdersDTO.class);
			
			if(giftCardOrdersDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("orders", giftCardOrdersDTO.getGiftCardOrdersList());
					model.addAttribute("orderPagingInfo", giftCardOrdersDTO.getPaginationDTO());
				}else{
					model.addAttribute("orders", gson.toJson(giftCardOrdersDTO.getGiftCardOrdersList()));
					model.addAttribute("orderPagingInfo",gson.toJson(giftCardOrdersDTO.getPaginationDTO()));
				}
				model.addAttribute("msg",giftCardOrdersDTO.getMessage());
			}else{
				model.addAttribute("msg", giftCardOrdersDTO.getError().getDescription());
				model.addAttribute("orders", giftCardOrdersDTO.getGiftCardOrdersList());
				model.addAttribute("orderPagingInfo", giftCardOrdersDTO.getPaginationDTO());
			}
			model.addAttribute("gcStatus",status);
			model.addAttribute("status",giftCardOrdersDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/DeleteGiftCardOrderAttachment")
	public String deleteGiftCardOrderAttachment(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		try {			
			String orderId = request.getParameter("orderId");
			String position = request.getParameter("position");
			String fileType = request.getParameter("fileType");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderId", orderId);
			map.put("userName", userName);
			map.put("position",position);
			map.put("fileType", fileType);
		
			//Change to new Controller Url 			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DELETE_GIFTCARD_ORDER_ATTACHMENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResp = gson.fromJson(((JsonObject) jsonObject.get("genericResponseDTO")),GenericResponseDTO.class);
			
			if(genericResp.getStatus() == 1){
				model.addAttribute("msg",genericResp.getMessage());
			}else{
				model.addAttribute("msg",genericResp.getError().getDescription());
			}
			model.addAttribute("status",genericResp.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/UpdateChatting")
	public String updateChatting(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		try {			
			String isChatting = request.getParameter("isChatting");
			if(isChatting == null || isChatting.isEmpty()){
				model.addAttribute("msg","invalid chatting status.");
				return "";
			}
			Boolean isChat = null;
			try {
				isChat = Boolean.parseBoolean(isChatting);
			} catch (Exception e) {
				model.addAttribute("msg","invalid chatting status.");
				return "";
			}
			if(!FirestoreUtil.isContestStarted){
				FirestoreUtil.updateChatting(isChat);
			}else{
				model.addAttribute("msg","Not allowed to set chatting On,Contest is already started.");
				return "";
			}
			if(isChat){
				model.addAttribute("msg","Chatting is turned On.");
			}else{
				model.addAttribute("msg","Chatting is turned Off.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/GiftCardBrands")
	public String getGiftCardBrands(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String gcStatus = request.getParameter("gbStatus");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("gbStatus", gcStatus);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_GIFT_CARD_BRANDS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfGiftCardsDTO cardDTO = gson.fromJson(((JsonObject)jsonObject.get("rtfGiftCardsDTO")), RtfGiftCardsDTO.class);
			
			if(cardDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("brands", cardDTO.getBrands());
					model.addAttribute("brandPagingInfo", cardDTO.getPagination());
				}else{
					model.addAttribute("brands", gson.toJson(cardDTO.getBrands()));
					model.addAttribute("brandPagingInfo",gson.toJson(cardDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", cardDTO.getError().getDescription());
			}
			model.addAttribute("gbStatus",gcStatus);
			model.addAttribute("status",cardDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-manage-gift-card-brand";
	}
	
	
	@RequestMapping(value = "/UpdateGiftCardBrand")
	public String updateGiftCardBrand(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String gbStatus = request.getParameter("gbStatus");
			String brandId = request.getParameter("brandId");
			String name = request.getParameter("name");
			String description = request.getParameter("description");
			String quantity = request.getParameter("quantity");
			String fileRequired = request.getParameter("fileRequired");
			String fileRequired1 = request.getParameter("fileRequired1");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("gbStatus", gbStatus);
			map.put("name", name);
			map.put("gbId",brandId);
			map.put("description", description);
			map.put("quantity", quantity);
			map.put("fileRequired1", fileRequired1);
			map.put("action", action);
			map.put("fileRequired",fileRequired);
			map.put("userName",userName);
			
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_GIFT_CARD_BRANDS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfGiftCardsDTO cardDTO = gson.fromJson(((JsonObject)jsonObject.get("rtfGiftCardsDTO")), RtfGiftCardsDTO.class);
			
			if(cardDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("brands", cardDTO.getBrands());
					model.addAttribute("brandPagingInfo", cardDTO.getPagination());
					model.addAttribute("brand", cardDTO.getBrand());
				}else{
					model.addAttribute("brands", gson.toJson(cardDTO.getBrands()));
					model.addAttribute("brandPagingInfo",gson.toJson(cardDTO.getPagination()));
					model.addAttribute("brand",gson.toJson( cardDTO.getBrand()));
				}
				model.addAttribute("msg",cardDTO.getMessage());
			}else{
				model.addAttribute("msg", cardDTO.getError().getDescription());
			}
			model.addAttribute("gbStatus",gbStatus);
			model.addAttribute("status",cardDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/CustomerProfileQuestion")
	public String customerProfileQuestion(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_CUSTOMER_PROFILE_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			CustomerProfileQuestionDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("customerProfileQuestionDTO")),CustomerProfileQuestionDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("questions", respDTO.getQuestions());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("questions", gson.toJson(respDTO.getQuestions()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("questions", respDTO.getQuestions());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-customer-profile-questions";
	}
	
	
	
	@RequestMapping(value = "/ChangeCustomerProfileQuestionPosition")
	public String changeCustomerProfileQuestionPosition(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action  = request.getParameter("action");
			String questionIdStr = request.getParameter("questionId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("questionId", questionIdStr);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_CHANGE_CUSTOMER_PROFILE_QUESTIONS_POSITION);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			CustomerProfileQuestionDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("customerProfileQuestionDTO")),CustomerProfileQuestionDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("questions", respDTO.getQuestions());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("questions", gson.toJson(respDTO.getQuestions()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("questions", respDTO.getQuestions());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/UpdateCustomerProfileQuestion")
	public String updateCustomerProfileQuestion(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String question = request.getParameter("question");
			String questionType = request.getParameter("questionType");
			String pointsStr = request.getParameter("points");
			String profileType = request.getParameter("profileType");
			//String serialNo = request.getParameter("serialNo");
			String questionIdStr = request.getParameter("questionId");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("question", question);
			map.put("questionType", questionType);
			map.put("points", pointsStr);
			map.put("profileType", profileType);
			map.put("action", action);
			map.put("questionId", questionIdStr);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_UPDATE_CUSTOMER_PROFILE_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

			CustomerProfileQuestionDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("customerProfileQuestionDTO")),CustomerProfileQuestionDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("question", respDTO.getQuestion());
					model.addAttribute("questions", respDTO.getQuestions());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("question", respDTO.getQuestion());
					model.addAttribute("questions", gson.toJson(respDTO.getQuestions()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
				model.addAttribute("msg", respDTO.getMessage());
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("question", respDTO.getQuestion());
				model.addAttribute("questions", respDTO.getQuestions());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
}