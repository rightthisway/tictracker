package com.rtw.tracker.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.utils.Constants;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.pojos.AccountReceivableDetailsDTO;
import com.rtw.tracker.pojos.CreatePayInvoiceDTO;
import com.rtw.tracker.pojos.CustomerOrderDetailsDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.InvoiceCsrDTO;
import com.rtw.tracker.pojos.InvoiceCustomerOrderAddressDTO;
import com.rtw.tracker.pojos.InvoiceDetailsDTO;
import com.rtw.tracker.pojos.InvoiceDownloadTicketDTO;
import com.rtw.tracker.pojos.InvoiceEditDTO;
import com.rtw.tracker.pojos.InvoiceNoteDTO;
import com.rtw.tracker.pojos.InvoicePaymentDetailsDTO;
import com.rtw.tracker.pojos.InvoiceRefundDTO;
import com.rtw.tracker.pojos.InvoiceRefundDetailsDTO;
import com.rtw.tracker.pojos.InvoiceStatusChangeDTO;
import com.rtw.tracker.pojos.InvoiceTicketCheckFileNameDTO;
import com.rtw.tracker.pojos.POCsrDTO;
import com.rtw.tracker.pojos.POGetShippingAddressForFedexDTO;
import com.rtw.tracker.pojos.PONotesDTO;
import com.rtw.tracker.pojos.POPaymentDetailsDTO;
import com.rtw.tracker.pojos.POTicketGroupOnHandStatusDTO;
import com.rtw.tracker.pojos.POUpdateDTO;
import com.rtw.tracker.pojos.POUploadETicketDTO;
import com.rtw.tracker.pojos.PurchaseOrderDetailsDTO;
import com.rtw.tracker.pojos.RelatedPOInvoicesDTO;
import com.rtw.tracker.pojos.ValidateReferralCodeDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

/**
 * 
 * @author dthiyagarajan
 *
 */
@Controller
@RequestMapping({"/Accounting"})
public class AccountingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountingController.class);
	
	
	@Autowired
//	FileValidator fileValidator;
	
//	MailManager mailManager;
//	
//	public MailManager getMailManager() {
//		return mailManager;
//	}
//
//	public void setMailManager(MailManager mailManager) {
//		this.mailManager = mailManager;
//	}
	
	SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	/**
	 * Manage purchase order
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/ManagePO")
	public String managePurchaseOrder(HttpServletRequest request, HttpServletResponse response, Model model,HttpSession session){
		
		try {			
			String poNo = request.getParameter("poNo");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String productType = request.getParameter("productType");
			String productTypeSession = (String) session.getAttribute("productType");
			String csr = request.getParameter("csr");
			String customer = request.getParameter("customer");
			String sortingString = request.getParameter("sortingString");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("poNo", poNo);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("productType", productType);
			map.put("productTypeSession", productTypeSession);
			map.put("csr", csr);
			map.put("customer", customer);
			map.put("brokerId", brokerId.toString());
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_PURCHASE_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PurchaseOrderDetailsDTO purchaseOrderDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("purchaseOrderDetailsDTO")), PurchaseOrderDetailsDTO.class);
			
			if(purchaseOrderDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", purchaseOrderDetailsDTO.getMessage());
				model.addAttribute("status", purchaseOrderDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", purchaseOrderDetailsDTO.getError().getDescription());
				model.addAttribute("status", purchaseOrderDetailsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", purchaseOrderDetailsDTO.getPoPaginationDTO());
				model.addAttribute("purchaseOrders", purchaseOrderDetailsDTO.getPurchaseOrderDTO());
				model.addAttribute("purchaseOrderTotal", purchaseOrderDetailsDTO.getPoTotal());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(purchaseOrderDetailsDTO.getPoPaginationDTO()));
				model.addAttribute("purchaseOrders", gson.toJson(purchaseOrderDetailsDTO.getPurchaseOrderDTO()));
				model.addAttribute("purchaseOrderTotal", purchaseOrderDetailsDTO.getPoTotal());
				model.addAttribute("layoutProduct", purchaseOrderDetailsDTO.getLayoutProductType());
				model.addAttribute("selectedProduct", purchaseOrderDetailsDTO.getSelectedProductType());
				model.addAttribute("fromDate", purchaseOrderDetailsDTO.getFromDate());
				model.addAttribute("toDate", purchaseOrderDetailsDTO.getToDate());
				model.addAttribute("poNo", poNo);
				model.addAttribute("brokerId", brokerId.toString());				
			}
			/*String fromDateStr = "";
			String toDateStr = "";
			String fromDateFinal = "";
			String toDateFinal = "";			
			double poTotal = 0;
			double roundPOTotal = 0;
			Integer count = 0;
			Collection<PurchaseOrders> purchaseOrders=null;
			if(poNo==null || poNo.isEmpty()){
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Calendar cal = Calendar.getInstance();
				Date toDate = new Date();
				cal.add(Calendar.MONTH, -3);
				Date fromDate = cal.getTime();
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			
			if(product!=null && !product.isEmpty()){
				companyProduct = product;
			}
						
			if(companyProduct != null && !companyProduct.isEmpty()){
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(fromDateFinal,toDateFinal,companyProduct,filter,poNo,null,null);
					count = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListCount(fromDateFinal,toDateFinal,companyProduct,filter,poNo,null);
				}else{
					purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,poNo,null,brokerId);
					count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,poNo,brokerId);
				}
			}else{
				purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,poNo,null,brokerId);
				count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,poNo,brokerId);
			}
			Map<Integer,Double> poCountMap = new HashMap<Integer, Double>();
			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
				for(PurchaseOrders po:purchaseOrders){
					poCountMap.put(po.getId(), po.getPoTotal());
				}
			}			
			
			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
				for(Double poTot : poCountMap.values()){
					poTotal += poTot;	//po.getPoTotal();
				}
			}
			
			roundPOTotal = Math.round(poTotal*100)/100.00;
			map.put("purchaseOrders", JsonWrapperUtil.getPuchaseOrderArray(purchaseOrders));
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("poNo", poNo);
			map.put("purchaseOrderTotal", roundPOTotal);
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			map.put("layoutProduct", companyProduct);
			map.put("selectedProduct", companyProduct);*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-accounting-managePO";
	}
	
	
	/*@RequestMapping(value = "/GetPurchaseOrders", method = RequestMethod.POST)
	public String GetPurchaseOrders(HttpServletRequest request, HttpServletResponse response, Model model){
					
		try {
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customer = request.getParameter("customer");
			String productType = request.getParameter("productType");
			String poNo = request.getParameter("poNo");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("csr", csr);
			map.put("customer", customer);
			map.put("productType", productType);
			map.put("poNo", poNo);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_MANAGE_PURCHASE_ORDER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PurchaseOrderDetailsDTO purchaseOrderDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("purchaseOrderDetailsDTO")), PurchaseOrderDetailsDTO.class);
			
			if(purchaseOrderDetailsDTO.getStatus() == 1){				
				model.addAttribute("msg", purchaseOrderDetailsDTO.getMessage());
				model.addAttribute("status", purchaseOrderDetailsDTO.getStatus());
				model.addAttribute("pagingInfo", purchaseOrderDetailsDTO.getPoPaginationDTO());
				model.addAttribute("purchaseOrders", purchaseOrderDetailsDTO.getPurchaseOrderDTO());
				model.addAttribute("purchaseOrderTotal", purchaseOrderDetailsDTO.getPoTotal());
			}else{
				model.addAttribute("msg", purchaseOrderDetailsDTO.getError().getDescription());
				model.addAttribute("status", purchaseOrderDetailsDTO.getStatus());
			}
			
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			double poTotal = 0;
//			double roundPOTotal = 0;
//			Integer count = 0;
//			Collection<PurchaseOrders> purchaseOrders=null;
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
//			
//			if(action!=null && action.equalsIgnoreCase("search")){				
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					if(companyProduct!= null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
//						purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(fromDateFinal,toDateFinal,companyProduct,filter,null,null,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListCount(fromDateFinal,toDateFinal,companyProduct,filter,null,null);
//					}else{
//						purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,null,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,null,brokerId);
//					}
//				}else{
//					if(companyProduct!= null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
//						purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersList(null,null,companyProduct,filter,null,null,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListCount(null,null,companyProduct,filter,null,null);
//					}else{
//						purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(null,null,filter,null,pageNo,brokerId);
//						count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(null,null,filter,null,brokerId);
//					}
//				}
//			}
//			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
//				fromDateFinal = fromDateStr + " 00:00:00";
//				toDateFinal = toDateStr + " 23:59:59";
//				purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersList(fromDateFinal,toDateFinal,filter,null,pageNo,brokerId);
//				count = DAORegistry.getQueryManagerDAO().purchaseOrdersListCount(fromDateFinal,toDateFinal,filter,null,brokerId);
//			}
//			Map<Integer,Double> poCountMap = new HashMap<Integer, Double>();
//			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
//				for(PurchaseOrders po:purchaseOrders){
//					poCountMap.put(po.getId(), po.getPoTotal());
//				}
//			}			
//			
//			if(purchaseOrders != null && !purchaseOrders.isEmpty()){
//				for(Double poTot : poCountMap.values()){
//					poTotal += poTot;	//po.getPoTotal();
//				}
//			}
//			
//			roundPOTotal = Math.round(poTotal*100)/100.00;
//			returnObject.put("purchaseOrders", JsonWrapperUtil.getPuchaseOrderArray(purchaseOrders));
//			returnObject.put("purchaseOrderTotal", roundPOTotal);
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}*/
	
	/*@RequestMapping(value = "/ExportToExcel")
	public void GetPurchaseOrdersToExport(HttpServletRequest request, HttpServletResponse response){		
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String poNoStr = request.getParameter("poNo");
		String csr = request.getParameter("csr");
		String customerStr = request.getParameter("customer");
		String companyProduct = request.getParameter("productType");
		String headerFilter = request.getParameter("headerFilter");
		String fromDateFinal = "";
		String toDateFinal = "";
		Collection<PurchaseOrders> purchaseOrders=null;
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getPOSearchHeaderFilters(headerFilter);
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListToExport(poNoStr,csr,customerStr,fromDateFinal,toDateFinal,companyProduct,null,filter);
				}else{
					purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersListToExport(poNoStr,csr,customerStr,fromDateFinal,toDateFinal,filter,brokerId);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListToExport(poNoStr,csr,customerStr,null,null,companyProduct,null,filter);
				}else{
					purchaseOrders = DAORegistry.getQueryManagerDAO().purchaseOrdersListToExport(poNoStr,csr,customerStr,null,null,filter,brokerId);
				}
			}
			
			if(purchaseOrders!=null && !purchaseOrders.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("purchase_orders");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Purchase Order Id");
				header.createCell(1).setCellValue("Customer Name");
				header.createCell(2).setCellValue("Customer Id");
				header.createCell(3).setCellValue("Customer Type");
				header.createCell(4).setCellValue("PO Total");
				header.createCell(5).setCellValue("Ticket Qty");
				header.createCell(6).setCellValue("Created Date");
				header.createCell(7).setCellValue("Created By");
				header.createCell(8).setCellValue("Shipping Type");
				header.createCell(9).setCellValue("Status");
				header.createCell(10).setCellValue("Product Type");
				header.createCell(11).setCellValue("Last Updated");
				header.createCell(12).setCellValue("PO Aged");
				header.createCell(13).setCellValue("CSR");
				header.createCell(14).setCellValue("Consignment PO No");
				header.createCell(15).setCellValue("Transaction Office");
				header.createCell(16).setCellValue("Tracking No");
				header.createCell(17).setCellValue("Shipping Notes");
				header.createCell(18).setCellValue("External Notes");
				header.createCell(19).setCellValue("Internal Notes");
				header.createCell(20).setCellValue("Event Id");
				header.createCell(21).setCellValue("Event Name");
				header.createCell(22).setCellValue("Event Date");
				header.createCell(23).setCellValue("Event Time");
				header.createCell(24).setCellValue("Is Emailed");
				header.createCell(25).setCellValue("Event Ticket Qty");
				header.createCell(26).setCellValue("Event Ticket Cost");
				header.createCell(27).setCellValue("Purchase Order Type");
				header.createCell(28).setCellValue("RTW PO");
				header.createCell(29).setCellValue("Broker Id");
				
				int i=1;
				for(PurchaseOrders order : purchaseOrders){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getId());
					row.createCell(1).setCellValue(order.getCustomerName());
					row.createCell(2).setCellValue(order.getCustomerId()!=null?order.getCustomerId():0);
					row.createCell(3).setCellValue(order.getCustomerType()!=null?order.getCustomerType():"");
					row.createCell(4).setCellValue(order.getPoTotal());
					row.createCell(5).setCellValue(order.getTicketQty()!=null?order.getTicketQty():0);
					row.createCell(6).setCellValue(order.getCreateDateStr());
					row.createCell(7).setCellValue(order.getCreatedBy()!=null?order.getCreatedBy():"");
					row.createCell(8).setCellValue(order.getShippingType()!=null?order.getShippingType():"");
					row.createCell(9).setCellValue(order.getStatus()!=null?order.getStatus():"");
					row.createCell(10).setCellValue(order.getProductType()!=null?order.getProductType():"");
					row.createCell(11).setCellValue(order.getLastUpdatedStr()!=null?order.getLastUpdatedStr():"");
					row.createCell(12).setCellValue(order.getPoAged()!=null?order.getPoAged():0);
					row.createCell(13).setCellValue(order.getCsr()!=null?order.getCsr():"");
					row.createCell(14).setCellValue(order.getConsignmentPoNo()!=null?order.getConsignmentPoNo():"");
					row.createCell(15).setCellValue(order.getTransactionOffice());
					row.createCell(16).setCellValue(order.getTrackingNo()!=null?order.getTrackingNo():"");
					row.createCell(17).setCellValue(order.getShippingNotes()!=null?order.getShippingNotes():"");
					row.createCell(18).setCellValue(order.getExternalNotes()!=null?order.getExternalNotes():"");
					row.createCell(19).setCellValue(order.getInternalNotes()!=null?order.getInternalNotes():"");
					row.createCell(20).setCellValue(order.getEventId());
					row.createCell(21).setCellValue(order.getEventName());
					row.createCell(22).setCellValue(order.getEventDateStr()!=null?order.getEventDateStr():"");
					row.createCell(23).setCellValue(order.getEventTimeStr()!=null?order.getEventTimeStr():"");
					row.createCell(24).setCellValue(order.getIsEmailed()!=null?"Yes":"No");
					row.createCell(25).setCellValue(order.getEventTicketQty()!=null?String.valueOf(order.getEventTicketQty()):"-");
					row.createCell(26).setCellValue(order.getEventTicketCost()!=null?String.valueOf(order.getEventTicketCost()):"-");
					row.createCell(27).setCellValue(order.getPurchaseOrderType()!=null?order.getPurchaseOrderType():"");
					if(order.getPosPOId()!=null){
						row.createCell(28).setCellValue("Yes");
					}else{
						row.createCell(28).setCellValue("No");
					}
					row.createCell(29).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=purchase_orders.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}*/
	
	
	/**
	 * Edit Purchase Order
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/EditPurchaseOrder")
	public String updatePurchaseOrder(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Edit purchase order request processing");
		
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_UPDATE_PO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POUpdateDTO poUpdateDTO = gson.fromJson(((JsonObject)jsonObject.get("POUpdateDTO")), POUpdateDTO.class);
			
			if(poUpdateDTO.getStatus() == 1){
				model.addAttribute("msg", poUpdateDTO.getMessage());
				model.addAttribute("status", poUpdateDTO.getStatus());
				model.addAttribute("ticketGroup", poUpdateDTO.getTicketGroupDTO());
				model.addAttribute("payment", poUpdateDTO.getPaymentDetailsDTO());
				model.addAttribute("purchaseOrder", poUpdateDTO.getPurchaseOrderDTO());
				model.addAttribute("customerInfo", poUpdateDTO.getCustomerInfoDTO());
				model.addAttribute("customerAddress", poUpdateDTO.getCustomerAddressDTO());
				model.addAttribute("shippingMethods", poUpdateDTO.getShippingMethodDTO());
				model.addAttribute("shippingMethod", poUpdateDTO.getShippingMethodName());
			}else{
				model.addAttribute("msg", poUpdateDTO.getError().getDescription());
				model.addAttribute("status", poUpdateDTO.getStatus());
			}
			/*String returnMessage = "";
			String shippingMethodName = null;
			String purchaseOrderId = request.getParameter("poId");
			String customerId = request.getParameter("customerId");
			String action =  request.getParameter("action");
			String paymentType = request.getParameter("paymentType");
			JSONObject jObj = new JSONObject();			
			jObj.put("status", 0);
			
			if(purchaseOrderId == null || purchaseOrderId.isEmpty()){
				jObj.put("msg", "Not able to Identify Purchase Order.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			PurchaseOrderPaymentDetails paymentDetails = null;
			Collection<TicketGroup> ticketGroups = null;
			Customer customerPOInfo = null;
			CustomerAddress customerAddress = null;
			Collection<ShippingMethod> shippingMethods = null;
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			
			PurchaseOrder poInfo = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(purchaseOrderId), brokerId);
			if(poInfo == null){
				jObj.put("msg", "Purchase Order Details not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			//if(poInfo != null){
				paymentDetails = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				ticketGroups = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));
				customerPOInfo = DAORegistry.getCustomerDAO().get(poInfo.getCustomerId());
				customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(poInfo.getCustomerId());
				shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
				
				List<Integer> ticketGroupsIds = new ArrayList<Integer>();
				for(TicketGroup ticketGroup : ticketGroups){
					ticketGroupsIds.add(ticketGroup.getId());
				}
			
				boolean isPOMapped = false;
				boolean isPOOnHold = false;
				String invoiceids="";
				if(ticketGroupsIds!=null && !ticketGroupsIds.isEmpty()){
					List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketGroupIds(ticketGroupsIds);
					for(Ticket ticket : tickets){
						if(ticket.getTicketStatus().equals(TicketStatus.SOLD)){
							if(ticket.getInvoiceId()!=null && ticket.getInvoiceId() >0){
								isPOMapped = true;
								invoiceids += ticket.getInvoiceId();
								break;
							}
						}else if(ticket.getTicketStatus().equals(TicketStatus.ONHOLD)){
							isPOOnHold = true;
							break;
						}
					}
				}
				
				if(isPOMapped){					
					jObj.put("msg", "Not allowed to update, Selected Purchase Order is mapped with invoice No. ("+invoiceids+")");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				if(isPOOnHold){					
					jObj.put("msg", "Not Allowed to Update PO : Some tickets quantity of PO is on Hold.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
			//}
			if(action != null && action.equals("editPOInfo")){
				if(returnMessage.isEmpty()){
					LOGGER.debug("Purchase order to update :: " +purchaseOrderId);
					String customeName = request.getParameter("customerName");
					String customeType = request.getParameter("customerType");
					String shippingType = request.getParameter("shippingMethod");
					String poTotal = request.getParameter("totalPrice");
					String tixCount = request.getParameter("totalQty");
					Integer shippingMethodId = -1;
					
					//String status = request.getParameter("status");
					String extPo = request.getParameter("externalPONo");
					String poType = request.getParameter("poType");
					String consignmentPo = request.getParameter("consignmentPo");
					String trackingNo = request.getParameter("trackingNo");
					String transactionOffice = request.getParameter("transactionOffice");
					//String paymentType = request.getParameter("paymentType");
					
					PurchaseOrder purchaseOrder = poInfo;
					//purchaseOrder.setId(Integer.parseInt(purchaseOrderId));
					//purchaseOrder.setCustomerId(Integer.parseInt(customerId));
					//purchaseOrder.setCreatedTime(poCreated);
					
					if(shippingType == null || shippingType.isEmpty()){
						jObj.put("msg", "Shipping Method not found.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					try{
						shippingMethodId = Integer.parseInt(shippingType);
					}catch (Exception e) {
						e.printStackTrace();
						jObj.put("msg", "Not able to identify Shipping Method.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					try {
						Double poTotalDouble = Double.parseDouble(poTotal); 
						purchaseOrder.setPoTotal(Double.parseDouble(poTotal));
					}catch (Exception e) {
						e.printStackTrace();
						jObj.put("msg", "Bad value found in Total Price, It should be valid Double value.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					try {
						Integer tixCountInt = Integer.parseInt(tixCount); 
						purchaseOrder.setTicketCount(Integer.parseInt(tixCount));
					}catch (Exception e) {
						e.printStackTrace();
						jObj.put("msg", "Bad value found in Total Quantity, It should be valid Integer value.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					purchaseOrder.setStatus(POStatus.ACTIVE);
					purchaseOrder.setBrokerId(brokerId);
					
					purchaseOrder.setTrackingNo(trackingNo);
					purchaseOrder.setTransactionOffice(transactionOffice);
					purchaseOrder.setExternalPONo(extPo);
					purchaseOrder.setLastUpdated(new Date());
					purchaseOrder.setPurchaseOrderType("REGULAR");
					if(poType!=null && !poType.isEmpty()){
						purchaseOrder.setPurchaseOrderType(poType);
						if(poType.equalsIgnoreCase("CONSIGNMENT")){
							purchaseOrder.setConsignmentPoNo(consignmentPo);
						}else{
							purchaseOrder.setConsignmentPoNo(null);
						}
					}
					
					if(shippingMethodId>=0){
						purchaseOrder.setShippingMethodId(shippingMethodId);
						ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(shippingMethodId);
						if(shipMethod!=null){
							shippingMethodName = shipMethod.getName();
						}
					}					
					DAORegistry.getPurchaseOrderDAO().update(purchaseOrder);
					
					if(paymentDetails==null){
						paymentDetails = new PurchaseOrderPaymentDetails();
					}
					paymentDetails.setName(request.getParameter("name"));
					String paymentDateStr = request.getParameter("paymentDate");
					if(paymentDateStr!=null && !paymentDateStr.isEmpty()){
						paymentDateStr += " 00:00:00";
						paymentDetails.setPaymentDate(formatDateTime.parse(paymentDateStr));
					}
					paymentDetails.setPaymentStatus(request.getParameter("paymentStatus"));
					paymentDetails.setPaymentNote(request.getParameter("paymentNote"));
					paymentDetails.setAmount(Double.parseDouble(poTotal));
					paymentDetails.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
					paymentDetails.setPaymentType(paymentType);
					if(paymentType !=null && paymentType.equalsIgnoreCase("card")){
						paymentDetails.setCardLastFourDigit(request.getParameter("cardNo"));
						paymentDetails.setCardType(request.getParameter("cardType"));
						paymentDetails.setAccountLastFourDigit(null);
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(null);
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("account")){
						paymentDetails.setName(request.getParameter("accountName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
						paymentDetails.setChequeNo(null);
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
					}else if(paymentType !=null && paymentType.equalsIgnoreCase("cheque")){
						paymentDetails.setName(request.getParameter("chequeName"));
						paymentDetails.setCardLastFourDigit(null);
						paymentDetails.setCardType(null);
						paymentDetails.setAccountLastFourDigit(request.getParameter("accountChequeNo"));
						paymentDetails.setChequeNo(request.getParameter("checkNo"));
						paymentDetails.setRoutingLastFourDigit(request.getParameter("routingChequeNo"));
					}
					DAORegistry.getPurchaseOrderPaymentDetailsDAO().saveOrUpdate(paymentDetails);
					
					TicketGroup tixGroup = null;
					
					Map<String, String[]> requestParams = request.getParameterMap();
					if(requestParams != null){
						EventDetails event = null;
						String section = "";
						String row = "";
						String qty = "";
						String seatLow = "";
						String seatHigh = "";
						String price = "";
						Integer rowNo = 0;
						
						for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
							tixGroup = new TicketGroup();
							String key = entry.getKey();
							
							if(key.contains("rowId_")){
								rowNo = Integer.parseInt(key.replace("rowId_", ""));
							}
							if(key.contains("ticketGroup_"+rowNo+"_")){
								Integer id= Integer.parseInt(key.replace("ticketGroup_"+rowNo+"_", ""));
								
								TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().get(id);
								if(ticGroup != null){
									section = request.getParameter("section_"+rowNo+"_"+id);
									row = request.getParameter("row_"+rowNo+"_"+id);
									qty = request.getParameter("qty_"+rowNo+"_"+id);
									seatLow = request.getParameter("seatLow_"+rowNo+"_"+id);
									seatHigh = request.getParameter("seatHigh_"+rowNo+"_"+id);
									price = request.getParameter("price_"+rowNo+"_"+id);									
									if(section == null && row == null && qty == null && seatLow == null && seatHigh == null && price == null){										
										List<Ticket> tempTickets = DAORegistry.getTicketDAO().getAllTicketsByTicketGroupId(ticGroup.getId());
										DAORegistry.getTicketDAO().deleteAll(tempTickets); //Remove all tickets										
										DAORegistry.getTicketGroupDAO().delete(ticGroup); //Remove Ticket Group
									}else{
									tixGroup.setId(ticGroup.getId());
									tixGroup.setSection(section);
									tixGroup.setRow(row);
									tixGroup.setEventId(ticGroup.getEventId());
									tixGroup.setStatus(OrderStatus.ACTIVE.toString());
									tixGroup.setQuantity(Integer.parseInt(qty));
									tixGroup.setSeatLow(seatLow);
									tixGroup.setSeatHigh(seatHigh);
									tixGroup.setPrice(Double.parseDouble(price));	
									if(shippingMethodId>=0){
										tixGroup.setShippingMethodId(shippingMethodId);										
										if(shippingMethodName != null && !shippingMethodName.isEmpty()){
											tixGroup.setShippingMethod(shippingMethodName);
										}
									}
									tixGroup.setCreatedDate(new Date());
									tixGroup.setBrokerId(brokerId);
									tixGroup.setInvoiceId(0);
									tixGroup.setCreatedBy(userName);
									tixGroup.setNotes(null);
									tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
									event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
									tixGroup.setEventName(event.getEventName());
									tixGroup.setEventDate(event.getEventDate());
									tixGroup.setEventTime(event.getEventTime());
									
									DAORegistry.getTicketGroupDAO().update(tixGroup);
									}
								}else{
									tixGroup.setSection(request.getParameter("section_"+rowNo+"_"+id));
									tixGroup.setRow(request.getParameter("row_"+rowNo+"_"+id));
									tixGroup.setEventId(id);
									tixGroup.setStatus(OrderStatus.ACTIVE.toString());
									tixGroup.setQuantity(Integer.parseInt(request.getParameter("qty_"+rowNo+"_"+id)));
									tixGroup.setSeatLow(request.getParameter("seatLow_"+rowNo+"_"+id));
									tixGroup.setSeatHigh(request.getParameter("seatHigh_"+rowNo+"_"+id));
									tixGroup.setPrice(Double.parseDouble(request.getParameter("price_"+rowNo+"_"+id)));
									if(shippingMethodId>=0){
										tixGroup.setShippingMethodId(shippingMethodId);
										if(shippingMethodName != null && !shippingMethodName.isEmpty()){
											tixGroup.setShippingMethod(shippingMethodName);
										}
									}
									tixGroup.setCreatedDate(new Date());
									tixGroup.setInvoiceId(0);
									tixGroup.setCreatedBy(userName);
									tixGroup.setNotes(null);
									tixGroup.setBroadcast(true);
									tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
									event = DAORegistry.getEventDetailsDAO().get(id);
									tixGroup.setEventName(event.getEventName());
									tixGroup.setEventDate(event.getEventDate());
									tixGroup.setEventTime(event.getEventTime());
									
									DAORegistry.getTicketGroupDAO().saveOrUpdate(tixGroup);
								}
								List<Ticket> oldTickets = DAORegistry.getTicketDAO().getAllTicketsByTicketGroupId(tixGroup.getId());
								if(oldTickets != null && !oldTickets.isEmpty()){
									DAORegistry.getTicketDAO().deleteAll(oldTickets);
								}
								if(tixGroup != null){
									if(tixGroup.getQuantity() != null && tixGroup.getQuantity()>0 && tixGroup.getSeatLow()!=null && !tixGroup.getSeatLow().isEmpty()){
										Ticket ticket = null;
										int seatNo = Integer.parseInt(tixGroup.getSeatLow());
										for(int i=0;i<tixGroup.getQuantity();i++){
											ticket = new Ticket(tixGroup);
											ticket.setSeatNo(String.valueOf(seatNo+i));
											ticket.setSeatOrder(i+1);
											ticket.setUserName(userName);
											ticket.setTicketStatus(TicketStatus.ACTIVE);
											DAORegistry.getTicketDAO().save(ticket);
										}
									}
								}
							}
						}
					}
					jObj.put("msg", "Purchase Order updated successfully");
					jObj.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Purchase Order Updated Successfully.";
					Util.userActionAudit(request, Integer.parseInt(purchaseOrderId), userActionMsg);
				}
				PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
				PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));				
				jObj.put("purchaseOrder", poInfoUpdated);
				jObj.put("paymentDetails", paymentDetailsUpdated);
				jObj.put("ticketGroups", ticketGroupsUpdated);
				jObj.put("customerInfo", customerAddress);
				jObj.put("shippingMethod", shippingMethodName);
				//return "page-view-purchase-order-summery";
			}else if(action != null && action.equals("voidPO")){
				LOGGER.info("Void po request processing....");
				
				//System.out.println(customerId);
				String customeName = request.getParameter("customerName");
				String customeType = request.getParameter("customerType");
				//String createdTime = request.getParameter("createdTime");
				String consignmentPo = request.getParameter("consignmentPo");
				String trackingNo = request.getParameter("trackingNo");
				String transactionOffice = request.getParameter("transactionOffice");
				
				String poTotal = request.getParameter("totalPrice");
				String tixCount = request.getParameter("totalQty");
				String shippingType = request.getParameter("shippingMethod");
				//String paymentType = request.getParameter("paymentType");
				
				Integer shippingMethodId = -1;
				
				//String status = request.getParameter("status");
				String extPo = request.getParameter("externalPONo");
				
				PurchaseOrder purchaseOrder = poInfo;
				//purchaseOrder.setId(Integer.parseInt(purchaseOrderId));
				//purchaseOrder.setCustomerId(Integer.parseInt(customerId));
				//purchaseOrder.setCreatedTime(poCreated);
				
				if(shippingType == null || shippingType.isEmpty()){
					jObj.put("msg", "Not able to indentify Shipping Method.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				try{
					shippingMethodId = Integer.parseInt(shippingType);
				}catch (Exception e) {
					e.printStackTrace();
					jObj.put("msg", "Shipping Method should be valid value.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				try {
					Double poTotalDouble = Double.parseDouble(poTotal);
					purchaseOrder.setPoTotal(Double.parseDouble(poTotal));
				}catch (Exception e) {
					e.printStackTrace();
					jObj.put("msg", "Bad value found in Total Price, It should be valid Double value.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				try {
					Integer tixCountInt = Integer.parseInt(tixCount);
					purchaseOrder.setTicketCount(Integer.parseInt(tixCount));
				}catch (Exception e) {
					e.printStackTrace();
					jObj.put("msg", "Bad value found in Total Quantity, It should be valid Integer value.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				if(shippingMethodId>=0){
					purchaseOrder.setShippingMethodId(shippingMethodId);
					ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(shippingMethodId);
					if(shipMethod!=null){
						shippingMethodName = shipMethod.getName();
					}
				}
				purchaseOrder.setStatus(POStatus.VOID);
				purchaseOrder.setConsignmentPoNo(consignmentPo);
				purchaseOrder.setTrackingNo(trackingNo);
				purchaseOrder.setTransactionOffice(transactionOffice);
				purchaseOrder.setExternalPONo(extPo);
				purchaseOrder.setVoidDate(new Date());
				purchaseOrder.setLastUpdated(new Date());
				DAORegistry.getPurchaseOrderDAO().update(purchaseOrder);
				
				if(paymentDetails==null){
					paymentDetails = new PurchaseOrderPaymentDetails();
				}
				paymentDetails.setName(request.getParameter("name"));
				String paymentDateStr = request.getParameter("paymentDate");
				if(paymentDateStr!=null && !paymentDateStr.isEmpty()){
					paymentDateStr += " 00:00:00";
					paymentDetails.setPaymentDate(formatDateTime.parse(paymentDateStr));
				}
				paymentDetails.setPaymentStatus(request.getParameter("paymentStatus"));
				paymentDetails.setPaymentNote(request.getParameter("paymentNote"));
				paymentDetails.setAmount(Double.parseDouble(poTotal));
				paymentDetails.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
				paymentDetails.setPaymentType(paymentType);
				if(paymentType !=null && paymentType.equalsIgnoreCase("card")){
					paymentDetails.setCardLastFourDigit(request.getParameter("cardNo"));
					paymentDetails.setCardType(request.getParameter("cardType"));
					paymentDetails.setAccountLastFourDigit(null);
					paymentDetails.setChequeNo(null);
					paymentDetails.setRoutingLastFourDigit(null);
				}else if(paymentType !=null && paymentType.equalsIgnoreCase("account")){
					paymentDetails.setCardLastFourDigit(null);
					paymentDetails.setCardType(null);
					paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
					paymentDetails.setChequeNo(null);
					paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
				}else if(paymentType !=null && paymentType.equalsIgnoreCase("cheque")){
					paymentDetails.setCardLastFourDigit(null);
					paymentDetails.setCardType(null);
					paymentDetails.setAccountLastFourDigit(request.getParameter("accountNo"));
					paymentDetails.setChequeNo(request.getParameter("checkNo"));
					paymentDetails.setRoutingLastFourDigit(request.getParameter("routingNo"));
				}
				DAORegistry.getPurchaseOrderPaymentDetailsDAO().saveOrUpdate(paymentDetails);
								
				TicketGroup tixGroup = null;
				String section = "";
				String row = "";
				String qty = "";
				String seatLow = "";
				String seatHigh = "";
				String price = "";
				
				@SuppressWarnings("unchecked")
				Map<String, String[]> requestParams = request.getParameterMap();
				if(requestParams != null){
					EventDetails event = null;
					Integer rowNo = 0;
					String userName = SecurityContextHolder.getContext().getAuthentication().getName();
					for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
						tixGroup = new TicketGroup();
						String key = entry.getKey();
						if(key.contains("rowId_")){
							rowNo = Integer.parseInt(key.replace("rowId_", ""));
						}
						if(key.contains("ticketGroup_"+rowNo+"_")){
							Integer id= Integer.parseInt(key.replace("ticketGroup_"+rowNo+"_", ""));
							
							TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().get(id);
							if(ticGroup != null){
								section = request.getParameter("section_"+rowNo+"_"+id);
								row = request.getParameter("row_"+rowNo+"_"+id);
								qty = request.getParameter("qty_"+rowNo+"_"+id);
								seatLow = request.getParameter("seatLow_"+rowNo+"_"+id);
								seatHigh = request.getParameter("seatHigh_"+rowNo+"_"+id);
								price = request.getParameter("price_"+rowNo+"_"+id);
								if(section == null && row == null && qty == null && seatLow == null && seatHigh == null && price == null){
									DAORegistry.getTicketGroupDAO().delete(ticGroup);
								}else{
								tixGroup.setId(ticGroup.getId());
								tixGroup.setSection(section);
								tixGroup.setRow(row);
								tixGroup.setEventId(ticGroup.getEventId());
								tixGroup.setStatus(OrderStatus.VOIDED.toString());
								tixGroup.setQuantity(Integer.parseInt(qty));
								tixGroup.setSeatLow(seatLow);
								tixGroup.setSeatHigh(seatHigh);
								tixGroup.setPrice(Double.parseDouble(price));
								if(shippingMethodId>=0){
									tixGroup.setShippingMethodId(shippingMethodId);
									if(shippingMethodName != null && !shippingMethodName.isEmpty()){
										tixGroup.setShippingMethod(shippingMethodName);
									}
								}
								tixGroup.setCreatedDate(new Date());
								tixGroup.setInvoiceId(0);
								tixGroup.setCreatedBy(userName);
								tixGroup.setNotes(null);
								tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
								event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
								tixGroup.setEventName(event.getEventName());
								tixGroup.setEventDate(event.getEventDate());
								tixGroup.setEventTime(event.getEventTime());
								
								DAORegistry.getTicketGroupDAO().update(tixGroup);
								}
							}else{
								tixGroup.setSection(request.getParameter("section_"+rowNo+"_"+id));
								tixGroup.setRow(request.getParameter("row_"+rowNo+"_"+id));
								tixGroup.setEventId(id);
								tixGroup.setStatus(OrderStatus.VOIDED.toString());
								tixGroup.setQuantity(Integer.parseInt(request.getParameter("qty_"+rowNo+"_"+id)));
								tixGroup.setSeatLow(request.getParameter("seatLow_"+rowNo+"_"+id));
								tixGroup.setSeatHigh(request.getParameter("seatHigh_"+rowNo+"_"+id));
								tixGroup.setPrice(Double.parseDouble(request.getParameter("price_"+rowNo+"_"+id)));
								if(shippingMethodId>=0){
									tixGroup.setShippingMethodId(shippingMethodId);
									if(shippingMethodName != null && !shippingMethodName.isEmpty()){
										tixGroup.setShippingMethod(shippingMethodName);
									}
								}
								tixGroup.setCreatedDate(new Date());
								tixGroup.setInvoiceId(0);
								tixGroup.setCreatedBy(userName);
								tixGroup.setNotes(null);
								tixGroup.setPurchaseOrderId(Integer.parseInt(purchaseOrderId));
								event = DAORegistry.getEventDetailsDAO().get(id);
								tixGroup.setEventName(event.getEventName());
								tixGroup.setEventDate(event.getEventDate());
								tixGroup.setEventTime(event.getEventTime());
								
								DAORegistry.getTicketGroupDAO().saveOrUpdate(tixGroup);
							}
						}
					}
				}
				jObj.put("msg", "Purchase Order voided successfully");
				jObj.put("status", 1);
				PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
				PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));				
				jObj.put("purchaseOrder", poInfoUpdated);
				jObj.put("paymentDetails", paymentDetailsUpdated);
				jObj.put("ticketGroups", ticketGroupsUpdated);
				jObj.put("customerInfo", customerAddress);
				jObj.put("shippingMethod", shippingMethodName);
				//return "page-view-purchase-order-summery";
				
				//Tracking User Action
				String userActionMsg = "Purchase Order Voided Successfully.";
				Util.userActionAudit(request, Integer.parseInt(purchaseOrderId), userActionMsg);
			}
			PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
			PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
			Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));
			//jObj.put("ticketGroup", ticketGroupsUpdated);
			//jObj.put("payment", paymentDetailsUpdated);
			//jObj.put("purchaseOrder", poInfoUpdated);
			//jObj.put("customerAddress", customerAddress);
			//jObj.put("customerInfo", customerPOInfo);
			//jObj.put("shippingMethods", shippingMethods);
			
			if(ticketGroupsUpdated != null){
				JSONArray ticketGroupsArray = new JSONArray();
				JSONObject ticketGroupsObject = null;
				for(TicketGroup ticketGroup : ticketGroupsUpdated){
					ticketGroupsObject = new JSONObject();
					ticketGroupsObject.put("tgId", ticketGroup.getId());
					ticketGroupsObject.put("tgSection", ticketGroup.getSection());
					ticketGroupsObject.put("tgRow", ticketGroup.getRow());
					ticketGroupsObject.put("tgSeatLow", ticketGroup.getSeatLow());
					ticketGroupsObject.put("tgSeatHigh", ticketGroup.getSeatHigh());
					ticketGroupsObject.put("tgQuantity", ticketGroup.getQuantity());
					ticketGroupsObject.put("tgPrice", ticketGroup.getPriceStr());
					ticketGroupsObject.put("tgEventId", ticketGroup.getEventId());
					ticketGroupsObject.put("tgEventName", ticketGroup.getEventName());
					ticketGroupsObject.put("tgEventDate", ticketGroup.getEventDateStr());
					ticketGroupsObject.put("tgEventTime", ticketGroup.getEventTimeStr());
					ticketGroupsArray.put(ticketGroupsObject);
				}
				jObj.put("ticketGroup", ticketGroupsArray);
			}
			if(paymentDetailsUpdated != null){
				JSONObject paymentObject = new JSONObject();
				paymentObject.put("paymentType", (paymentDetailsUpdated.getPaymentType()!=null ? paymentDetailsUpdated.getPaymentType(): ""));
				paymentObject.put("paidName", paymentDetailsUpdated.getName());
				paymentObject.put("cardType", paymentDetailsUpdated.getCardType());
				paymentObject.put("cardLastFourDigit", paymentDetailsUpdated.getCardLastFourDigit());
				paymentObject.put("routingLastFourDigit", paymentDetailsUpdated.getRoutingLastFourDigit());
				paymentObject.put("accountLastFourDigit", paymentDetailsUpdated.getAccountLastFourDigit());
				paymentObject.put("chequeNo", paymentDetailsUpdated.getChequeNo());
				paymentObject.put("paymentDate", paymentDetailsUpdated.getPaymentDateStr());
				paymentObject.put("paymentStatus", paymentDetailsUpdated.getPaymentStatus());
				paymentObject.put("paymentNote", paymentDetailsUpdated.getPaymentNote());
				jObj.put("payment", paymentObject);						
			}
			if(poInfoUpdated != null){
				JSONObject poObject = new JSONObject();
				poObject.put("poId", poInfoUpdated.getId());
				poObject.put("poTicketCount", poInfoUpdated.getTicketCount());
				poObject.put("poTotal", poInfoUpdated.getPoTotal());
				poObject.put("poCustomerId", poInfoUpdated.getCustomerId());
				poObject.put("poTrackingNo", poInfoUpdated.getTrackingNo());
				poObject.put("poExternalPONo", poInfoUpdated.getExternalPONo());
				poObject.put("poConsignmentPONo", poInfoUpdated.getConsignmentPoNo());
				poObject.put("poTransactionOffice", poInfoUpdated.getTransactionOffice());
				poObject.put("poPurchaseOrderType", poInfoUpdated.getPurchaseOrderType());
				poObject.put("poShippingMethod", poInfoUpdated.getShippingMethodId());
				jObj.put("purchaseOrder", poObject);
			}
			if(customerPOInfo != null){
				JSONObject customerObject = new JSONObject();
				customerObject.put("customerName", customerPOInfo.getCustomerName());
				customerObject.put("userName", customerPOInfo.getUserName());
				customerObject.put("phone", customerPOInfo.getPhone());
				jObj.put("customerInfo", customerObject);
			}
			if(customerAddress != null){
				JSONObject addressObject = new JSONObject();
				addressObject.put("addressLine1", customerAddress.getAddressLine1());
				addressObject.put("addressLine2", customerAddress.getAddressLine2());
				addressObject.put("city", customerAddress.getCity());
				addressObject.put("state", customerAddress.getState().getName());
				addressObject.put("country", customerAddress.getCountry().getName());
				addressObject.put("zipCode", customerAddress.getZipCode());
				jObj.put("customerAddress", addressObject);
			}
			if(shippingMethods != null){
				JSONArray shippingMethodArray = new JSONArray();
				JSONObject shippingMethodObject = null;
				for(ShippingMethod shippingMethod : shippingMethods){
					shippingMethodObject = new JSONObject();
					shippingMethodObject.put("id", shippingMethod.getId());
					shippingMethodObject.put("name", shippingMethod.getName());
					shippingMethodArray.put(shippingMethodObject);
				}
				jObj.put("shippingMethods", shippingMethodArray);
			}
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
			

	@RequestMapping(value = "/UploadEticket")
	public String uploadEticket(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Upload ETicket purchase order request processing");
		
		try {			
			String action = request.getParameter("action");
			String poId = request.getParameter("poId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("poId", poId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PO_UPLOAD_ETICKET);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POUploadETicketDTO poUploadETicketDTO = gson.fromJson(((JsonObject)jsonObject.get("POUploadETicketDTO")), POUploadETicketDTO.class);
			
			if(poUploadETicketDTO.getStatus() == 1){
				model.addAttribute("msg", poUploadETicketDTO.getMessage());
				model.addAttribute("status", poUploadETicketDTO.getStatus());
				model.addAttribute("ticketGroup", poUploadETicketDTO.getTicketgroupDTO());
				model.addAttribute("purchaseOrder", poUploadETicketDTO.getPurchaseOrderDTO());
				model.addAttribute("customerInfo", poUploadETicketDTO.getCustomersDTO());
			}else{
				model.addAttribute("msg", poUploadETicketDTO.getError().getDescription());
				model.addAttribute("status", poUploadETicketDTO.getStatus());
			}
			/*String returnMessage = "";
			String shippingMethodName = "";
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			Collection<TicketGroup> ticketGroups = null;
			Customer customerPOInfo = null;			
			Collection<ShippingMethod> shippingMethods = null;
			TicketGroupTicketAttachment tktGrpTktAttachement = null;
			
			if(purchaseOrderId == null || purchaseOrderId.isEmpty()){
				jObj.put("msg", "Not able to identify Purchase Order.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			PurchaseOrder poInfo = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(purchaseOrderId), brokerId);
			if(poInfo == null){
				jObj.put("msg", "Purchase Order Details not found in System.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			ticketGroups = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));
			if(ticketGroups == null){
				jObj.put("msg", "Ticket Group Details not found System.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			customerPOInfo = DAORegistry.getCustomerDAO().get(poInfo.getCustomerId());
			shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			List<Integer> ticketGroupsIds = new ArrayList<Integer>();
			for(TicketGroup ticketGroup : ticketGroups){
				ticketGroupsIds.add(ticketGroup.getId());
				tktGrpTktAttachement = DAORegistry.getTicketGroupTicketAttachmentDAO().getTicketByTicketGroupId(ticketGroup.getId());
				ticketGroup.setTicketGroupTicketAttachment(tktGrpTktAttachement);
			}
			boolean isPOMapped = false;
			String invoiceids="";
			if(ticketGroupsIds!=null && !ticketGroupsIds.isEmpty()){
				List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketGroupIds(ticketGroupsIds);
				for(Ticket ticket : tickets){
					if(ticket.getInvoiceId()!=null && ticket.getInvoiceId() >0){
						isPOMapped = true;
						invoiceids += ticket.getInvoiceId();
						break;
					}
				}				
			}
			
			if(isPOMapped){
				jObj.put("msg", "Not allowed to update, Purchase order is mapped with invoice No. ("+invoiceids+")");
				//IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				//return;
			}
			
			if(action != null && action.equals("uploadPOInfo")){
				if(returnMessage.isEmpty()){
					LOGGER.debug("Purchase order to update :: " +purchaseOrderId);
					
					Map<String, String[]> requestParams = request.getParameterMap();
					if(requestParams != null){						
						Integer rowNo = 0;
						Set<Integer> rowIdSet = new HashSet<Integer>();
						
						String userName = SecurityContextHolder.getContext().getAuthentication().getName();
						for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
							
							String key = entry.getKey();
							
							if(key.contains("rowId_")){
								rowNo = Integer.parseInt(key.replace("rowId_", ""));
								rowIdSet.add(rowNo);
							}
						}
						for(Integer rowIds : rowIdSet){
							rowNo = rowIds;
						for(Map.Entry<String, String[]> entry : requestParams.entrySet()){
							
							String key = entry.getKey();
														
							if(key.contains("ticketGroup_"+rowNo+"_")){
								Integer id= Integer.parseInt(key.replace("ticketGroup_"+rowNo+"_", ""));
								
								TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().get(id);
								if(ticGroup != null){
									String instantDownloadStr = request.getParameter("instantdownload_"+rowNo+"_"+id);
									Boolean instantDownload = Boolean.parseBoolean(instantDownloadStr);
									
									List<TicketGroupTicketAttachment> ticketGrouptktAttachment = null;
									String appendFileName = "";
									TicketGroupTicketAttachment attachment;
									MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
									MultipartFile file = multipartRequest.getFile("file_"+rowNo+"_"+id);
									
									if(file!=null && file.getSize()>0){
									
									appendFileName = ticGroup.getId()+"_"+ticGroup.getEventName()+"_"+ticGroup.getEventDateStr()+"_"+ticGroup.getEventTimeStr()+"_"; 
									File newFile = null;
									String ext = FilenameUtils.getExtension(file.getOriginalFilename());
									String fullFileName = "";
									String sectionStr =  ticGroup.getSection();
									if(sectionStr != null && !sectionStr.isEmpty()){
										appendFileName += sectionStr+"_";
									}
									String rowStr = ticGroup.getRow();
									if(rowStr != null && !rowStr.isEmpty()){
										appendFileName += rowStr+"_";
									}
									if(ticGroup.getSeatLow() != null && !ticGroup.getSeatLow().isEmpty()){
										appendFileName += ticGroup.getSeatLow()+"_";
									}
									if(ticGroup.getSeatHigh() != null && !ticGroup.getSeatHigh().isEmpty()){
										appendFileName += ticGroup.getSeatHigh()+"_";
									}
									appendFileName = appendFileName.replace(" ", "_");
									appendFileName = appendFileName.replace("/", "_");
									appendFileName = appendFileName.replace(":", "_");
									fullFileName = "C:/REWARDTHEFAN/Tickets/"+appendFileName+"_eticket_1"+"."+ext;
									
									ticketGrouptktAttachment = DAORegistry.getTicketGroupTicketAttachmentDAO().getAttachmentByTicketGroupAndFileType(ticGroup.getId(),FileType.ETICKET);
									if(!ticketGrouptktAttachment.isEmpty()){
										for(TicketGroupTicketAttachment attach : ticketGrouptktAttachment){
											File f = new File(attach.getFilePath());
											if(f.exists()){
												f.delete();
											}
										}
										DAORegistry.getTicketGroupTicketAttachmentDAO().deleteAll(ticketGrouptktAttachment);
									}
									
									attachment = new TicketGroupTicketAttachment();
									attachment.setTicketGroupId(ticGroup.getId());
									attachment.setFilePath(fullFileName);
									attachment.setExtension(ext);
									attachment.setPosition(1);
									attachment.setType(FileType.ETICKET);
									attachment.setUploadedBy("AUTO");
									attachment.setUploadedDateTime(new Date());									
										
									try {
										newFile = new File(fullFileName);
										newFile.createNewFile();
										
										BufferedOutputStream stream = new BufferedOutputStream(
												new FileOutputStream(newFile));
								        FileCopyUtils.copy(file.getInputStream(), stream);
										stream.close();
										DAORegistry.getTicketGroupTicketAttachmentDAO().save(attachment);
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									//Update TicketGroup table for etickets
									ticGroup.setInstantDownload(instantDownload);
									ticGroup.setEticketsAttached(true);
									ticGroup.setEticketsAttachedDate(new Date());
									DAORegistry.getTicketGroupDAO().update(ticGroup);
									}
								}
							}
							}
						}
					}
					jObj.put("msg", "Purchase Order updated successfully");
					jObj.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Purchase Order Ticket Uploaded Successfully.";
					Util.userActionAudit(request, Integer.parseInt(purchaseOrderId), userActionMsg);
				}
				PurchaseOrder poInfoUpdated = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));
				//PurchaseOrderPaymentDetails paymentDetailsUpdated = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(Integer.parseInt(purchaseOrderId));
				Collection<TicketGroup> ticketGroupsUpdated = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(Integer.parseInt(purchaseOrderId));
				for(TicketGroup ticketGroup : ticketGroupsUpdated){
					tktGrpTktAttachement = DAORegistry.getTicketGroupTicketAttachmentDAO().getTicketByTicketGroupId(ticketGroup.getId());
					ticketGroup.setTicketGroupTicketAttachment(tktGrpTktAttachement);
				}
				if(ticketGroupsUpdated != null){
					JSONArray ticketGroupsArray = new JSONArray();
					JSONObject ticketGroupsObject = null;
					for(TicketGroup ticketGroup : ticketGroupsUpdated){
						ticketGroupsObject = new JSONObject();
						ticketGroupsObject.put("tgId", ticketGroup.getId());
						ticketGroupsObject.put("tgSection", ticketGroup.getSection());
						ticketGroupsObject.put("tgRow", ticketGroup.getRow());
						ticketGroupsObject.put("tgSeatLow", ticketGroup.getSeatLow());
						ticketGroupsObject.put("tgSeatHigh", ticketGroup.getSeatHigh());
						ticketGroupsObject.put("tgQuantity", ticketGroup.getQuantity());
						ticketGroupsObject.put("tgPrice", ticketGroup.getPriceStr());
						ticketGroupsObject.put("tgEventId", ticketGroup.getEventId());
						ticketGroupsObject.put("tgEventName", ticketGroup.getEventName());
						ticketGroupsObject.put("tgEventDate", ticketGroup.getEventDateStr());
						ticketGroupsObject.put("tgEventTime", ticketGroup.getEventTimeStr());
						if(ticketGroup.getInstantDownload() != null && ticketGroup.getInstantDownload()){
							ticketGroupsObject.put("tgInstantDownload", "Yes");
						}else{
							ticketGroupsObject.put("tgInstantDownload", "No");
						}
						ticketGroupsObject.put("tgTicketAttachment", ticketGroup.getTicketGroupTicketAttachment());
						if(ticketGroup.getTicketGroupTicketAttachment() != null){
							TicketGroupTicketAttachment tgTktAtt = ticketGroup.getTicketGroupTicketAttachment();
							ticketGroupsObject.put("tgrpId", tgTktAtt.getTicketGroupId());
							ticketGroupsObject.put("tgrpType", tgTktAtt.getType());
							ticketGroupsObject.put("tgrpPosition", tgTktAtt.getPosition());
							ticketGroupsObject.put("tgrpFilePath", tgTktAtt.getFilePath());
							ticketGroupsObject.put("tgrpFileName", tgTktAtt.getFileName());
						}
						ticketGroupsArray.put(ticketGroupsObject);
					}
					jObj.put("ticketGroup", ticketGroupsArray);
				}
				if(poInfoUpdated != null){
					JSONObject poObject = new JSONObject();
					poObject.put("poId", poInfoUpdated.getId());
					poObject.put("poTotal", poInfoUpdated.getPoTotal());
					poObject.put("poCsr", poInfoUpdated.getCsr());
					poObject.put("poTicketCount", poInfoUpdated.getTicketCount());
					poObject.put("poCreateTime", poInfoUpdated.getCreateTimeStr());
					poObject.put("poCustomerId", poInfoUpdated.getCustomerId());
					
					for(ShippingMethod shippingMethod : shippingMethods){
						if(poInfoUpdated.getShippingMethodId() == shippingMethod.getId()){
							poObject.put("poShippingMethod", shippingMethod.getName());
						}
					}
					jObj.put("purchaseOrder", poObject);
				}
				if(customerPOInfo != null){
					JSONObject customerObject = new JSONObject();
					customerObject.put("customerName", customerPOInfo.getCustomerName());
					jObj.put("customerInfo", customerObject);
				}
				//jObj.put("shippingMethods", shippingMethods);
				//jObj.put("ticketAttachment", tktGrpTktAttachments);
			}
			else{
				if(ticketGroups != null){
					JSONArray ticketGroupsArray = new JSONArray();
					JSONObject ticketGroupsObject = null;
					for(TicketGroup ticketGroup : ticketGroups){
						ticketGroupsObject = new JSONObject();
						ticketGroupsObject.put("tgId", ticketGroup.getId());
						ticketGroupsObject.put("tgSection", ticketGroup.getSection());
						ticketGroupsObject.put("tgRow", ticketGroup.getRow());
						ticketGroupsObject.put("tgSeatLow", ticketGroup.getSeatLow());
						ticketGroupsObject.put("tgSeatHigh", ticketGroup.getSeatHigh());
						ticketGroupsObject.put("tgQuantity", ticketGroup.getQuantity());
						ticketGroupsObject.put("tgPrice", ticketGroup.getPriceStr());
						ticketGroupsObject.put("tgEventId", ticketGroup.getEventId());
						ticketGroupsObject.put("tgEventName", ticketGroup.getEventName());
						ticketGroupsObject.put("tgEventDate", ticketGroup.getEventDateStr());
						ticketGroupsObject.put("tgEventTime", ticketGroup.getEventTimeStr());
						if(ticketGroup.getInstantDownload() != null && ticketGroup.getInstantDownload()){
							ticketGroupsObject.put("tgInstantDownload", "Yes");
						}else{
							ticketGroupsObject.put("tgInstantDownload", "No");
						}
						ticketGroupsObject.put("tgTicketAttachment", ticketGroup.getTicketGroupTicketAttachment());
						if(ticketGroup.getTicketGroupTicketAttachment() != null){
							TicketGroupTicketAttachment tgTktAtt = ticketGroup.getTicketGroupTicketAttachment();
							ticketGroupsObject.put("tgrpId", tgTktAtt.getTicketGroupId());
							ticketGroupsObject.put("tgrpType", tgTktAtt.getType());
							ticketGroupsObject.put("tgrpPosition", tgTktAtt.getPosition());
							ticketGroupsObject.put("tgrpFilePath", tgTktAtt.getFilePath());
							ticketGroupsObject.put("tgrpFileName", tgTktAtt.getFileName());
						}
						ticketGroupsArray.put(ticketGroupsObject);
					}
					jObj.put("ticketGroup", ticketGroupsArray);
				}
				if(poInfo != null){
					JSONObject poObject = new JSONObject();
					poObject.put("poId", poInfo.getId());
					poObject.put("poTotal", poInfo.getPoTotal());
					poObject.put("poCsr", poInfo.getCsr());
					poObject.put("poTicketCount", poInfo.getTicketCount());
					poObject.put("poCreateTime", poInfo.getCreateTimeStr());
					poObject.put("poCustomerId", poInfo.getCustomerId());
					
					for(ShippingMethod shippingMethod : shippingMethods){
						if(poInfo.getShippingMethodId() == shippingMethod.getId()){
							poObject.put("poShippingMethod", shippingMethod.getName());
						}
					}
					jObj.put("purchaseOrder", poObject);
				}				
				if(customerPOInfo != null){
					JSONObject customerObject = new JSONObject();
					customerObject.put("customerName", customerPOInfo.getCustomerName());
					jObj.put("customerInfo", customerObject);
				}
				jObj.put("status", 1);
				//jObj.put("shippingMethods", shippingMethods);
				//jObj.put("customerAddress", customerAddress);				
				//jObj.put("ticketAttachment", tktGrpTktAttachments);
			}
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	/**
	 * Delete PO
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/DeletePO", method=RequestMethod.POST)
	@ResponseBody
	public String deleteCustomer(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		try{
			TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
			if(trackerUserSession == null){
				return "Please Login.";
			}
			
			String poIdStr = request.getParameter("poId");
			PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
			if(purchaseOrder == null){
				return "There is no purchase order to delete.";
			}
			
			DAORegistry.getPurchaseOrderDAO().delete(purchaseOrder);
			return "true";
		}catch(Exception e){
			e.printStackTrace();
			return "There is something wrong. Please try again.";
		}
	}*/
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 *//*
	@RequestMapping({"/Invoices"})
	public String invoicesPage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date fromDate = new Date();
			map.put("fromDate", dateFormat.format(fromDate));
			map.put("toDate", dateFormat.format(fromDate));
		}catch(Exception e){
			e.printStackTrace();
		}
		map.put("url", "Accounting");
		return "page-accounting-invoices";
	}*/
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*@RequestMapping({"/PurchaseOrder"})
	public String purchaseOrderPage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date fromDate = new Date();
			map.put("fromDate", dateFormat.format(fromDate));
			map.put("toDate", dateFormat.format(fromDate));
		}catch(Exception e){
			e.printStackTrace();
		}
		//map.put("url", "Accounting");
		return "page-accounting-purchase-order";
	}*/
	
	/**
	 * Render open orders page
	 * @return
	 */
	@RequestMapping(value="/Invoices")
	public String getOpenOrdersPage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try {
			String status = request.getParameter("status");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String invoiceNo = request.getParameter("invoiceNo");
			String orderNo = request.getParameter("orderNo");
			String productType = request.getParameter("productType");
			String productTypeSession = (String) session.getAttribute("productType");
			String csr = request.getParameter("csr");
			String customer = request.getParameter("customer");
			String externalOrderId = request.getParameter("externalOrderId");
			String sortingString = request.getParameter("sortingString");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("invoiceNo", invoiceNo);
			map.put("orderNo", orderNo);			
			map.put("productType", productType);
			map.put("productTypeSession", productTypeSession);
			map.put("csr", csr);
			map.put("customer", customer);
			map.put("externalOrderId", externalOrderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_INVOICES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceDetailsDTO invoiceDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceDetailsDTO")), InvoiceDetailsDTO.class);
			
			if(invoiceDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceDetailsDTO.getMessage());
				model.addAttribute("statuss", invoiceDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", invoiceDetailsDTO.getError().getDescription());
				model.addAttribute("statuss", invoiceDetailsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", invoiceDetailsDTO.getInvoicePaginationDTO());
				model.addAttribute("openOrders", invoiceDetailsDTO.getOpenOrdersDTO());
				model.addAttribute("invoiceTotal", invoiceDetailsDTO.getInvoiceTotal());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(invoiceDetailsDTO.getInvoicePaginationDTO()));
				model.addAttribute("openOrders", gson.toJson(invoiceDetailsDTO.getOpenOrdersDTO()));
				model.addAttribute("invoiceTotal", invoiceDetailsDTO.getInvoiceTotal());
				model.addAttribute("status", invoiceDetailsDTO.getInvoiceStatus());
				model.addAttribute("layoutProduct", invoiceDetailsDTO.getLayoutProductType());
				model.addAttribute("selectedProduct", invoiceDetailsDTO.getSelectedProductType());
				model.addAttribute("fromDate", invoiceDetailsDTO.getFromDate());
				model.addAttribute("toDate", invoiceDetailsDTO.getToDate());
				model.addAttribute("currentYear", invoiceDetailsDTO.getCurrentYear());
				model.addAttribute("invoiceNo", invoiceNo);
				model.addAttribute("brokerId", brokerId.toString());
			}
			/*double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			if(invoiceNoStr==null || invoiceNoStr.isEmpty()){
				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}
				else{
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
					Calendar cal = Calendar.getInstance();
					Date toDate = new Date();
					cal.add(Calendar.MONTH, -3);
					Date fromDate = cal.getTime();
					fromDateStr = dateFormat.format(fromDate);
					toDateStr = dateFormat.format(toDate);
					fromDateFinal = fromDateStr + " 00:00:00";
					toDateFinal = toDateStr + " 23:59:59";
				}
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			
			if(product!=null && !product.isEmpty()){
				companyProduct = product;
			}
			
			if(companyProduct!=null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
				openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct,null,null);
				count = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status,companyProduct, null);
			}else{
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, null, brokerId,null);
				count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, brokerId,null);
			}
			
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
				if(openOrdersList.size()==1){
					status = openOrdersList.iterator().next().getStatus();
				}
			}
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;		
			map.put("status",(status==null || status.isEmpty())?"":InvoiceStatus.valueOf(status));
			map.put("openOrders", JsonWrapperUtil.getInvoiceArray(openOrdersList));
			map.put("invoiceTotal", roundInvoiceTotal);
			map.put("selectedProduct", companyProduct);
			map.put("layoutProduct", companyProduct);
			map.put("fromDate", fromDateStr);
			map.put("toDate", toDateStr);
			map.put("invoiceNo", invoiceNoStr);
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			map.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "page-manage-invoice";
	}
	
	
	/*@RequestMapping(value="/GetInvoices")
	public String getInvoices(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String status = request.getParameter("status");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String csr = request.getParameter("csr");
			String customer = request.getParameter("customer");
			String productType = request.getParameter("productType");			
			String invoiceNo = request.getParameter("invoiceNo");
			String orderNo = request.getParameter("orderNo");
			String externalOrderId = request.getParameter("externalOrderId");
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("csr", csr);
			map.put("customer", customer);
			map.put("productType", productType);
			map.put("invoiceNo", invoiceNo);
			map.put("orderNo", orderNo);
			map.put("externalOrderId", externalOrderId);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_MANAGE_INVOICES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceDetailsDTO invoiceDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceDetailsDTO")), InvoiceDetailsDTO.class);
			
			if(invoiceDetailsDTO.getStatus() == 1){				
				model.addAttribute("msg", invoiceDetailsDTO.getMessage());
				model.addAttribute("statuss", invoiceDetailsDTO.getStatus());
				model.addAttribute("pagingInfo", invoiceDetailsDTO.getInvoicePaginationDTO());
				model.addAttribute("openOrders", invoiceDetailsDTO.getOpenOrdersDTO());
				model.addAttribute("invoiceTotal", invoiceDetailsDTO.getInvoiceTotal());
			}else{
				model.addAttribute("msg", invoiceDetailsDTO.getError().getDescription());
				model.addAttribute("statuss", invoiceDetailsDTO.getStatus());
			}

//			/*JSONObject returnObject = new JSONObject();
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			double invoiceTotal = 0;
//			double roundInvoiceTotal = 0;
//			Integer count = 0;
//			Collection<OpenOrders> openOrdersList=null;
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
//			
//			//TrackerUser trackerUserSession = (TrackerUser)session.getAttribute("trackerUser");
//			
//			if(brokerId == -1){
//				brokerId = 0;
//			}
//			
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					if(companyProduct!=null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
//						openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(fromDateFinal, toDateFinal, filter, null, null, status, companyProduct,null,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListCount(fromDateFinal, toDateFinal, filter, null, null, status, companyProduct, null);
//					}else{
//						openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct,pageNo, brokerId,null);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct, brokerId,null);
//					}
//				}else{
//					if(companyProduct!=null && (companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2"))){
//						openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderList(null, null, filter, null, null, status,companyProduct,null,pageNo);
//						count = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListCount(null, null, filter, null, null, status, companyProduct, null);
//					}else{
//						openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(null,null,filter, null, null,status,companyProduct,pageNo,brokerId,null);
//						count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(null,null,filter, null, null,status,companyProduct,brokerId,null);
//					}
//				}
//			}
//			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
//				fromDateFinal = fromDateStr + " 00:00:00";
//				toDateFinal = toDateStr + " 23:59:59";
//				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,pageNo,brokerId,null);
//				count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,brokerId,null);
//			}
//			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
//			if(openOrdersList!=null && !openOrdersList.isEmpty()){
//				for(OpenOrders order : openOrdersList){
//					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
//				}
//			}			
//			
//			if(openOrdersList!=null && !openOrdersList.isEmpty()){
//				for(Double invoiceTot : invoiceCountMap.values()){
//					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
//				}
//			}		
//			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;		
//			returnObject.put("openOrders", JsonWrapperUtil.getInvoiceArray(openOrdersList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			returnObject.put("invoiceTotal", roundInvoiceTotal);
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream()); //Commented Upto this..
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}*/
	
	
	@RequestMapping(value="/AccountReceivables")
	public String getAccountReceivblePage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try {
			String status = request.getParameter("status");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String invoiceNo = request.getParameter("invoiceNo");
			String orderNo = request.getParameter("orderNo");			
			String productType = request.getParameter("productType");
			String sortingString = request.getParameter("sortingString");			
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("invoiceNo", invoiceNo);
			map.put("orderNo", orderNo);			
			map.put("productType", productType);
			map.put("sortingString", sortingString);
			map.put("brokerId", brokerId.toString());
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_ACCOUNT_RECEIVABLES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AccountReceivableDetailsDTO accountReceivableDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("accountReceivableDetailsDTO")), AccountReceivableDetailsDTO.class);
			
			if(accountReceivableDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", accountReceivableDetailsDTO.getMessage());
				model.addAttribute("statuss", accountReceivableDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", accountReceivableDetailsDTO.getError().getDescription());
				model.addAttribute("statuss", accountReceivableDetailsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", accountReceivableDetailsDTO.getAccReceivablePaginationDTO());
				model.addAttribute("openOrders", accountReceivableDetailsDTO.getOpenOrdersDTO());
				model.addAttribute("invoiceTotal", accountReceivableDetailsDTO.getInvoiceTotal());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(accountReceivableDetailsDTO.getAccReceivablePaginationDTO()));
				model.addAttribute("openOrders", gson.toJson(accountReceivableDetailsDTO.getOpenOrdersDTO()));
				model.addAttribute("invoiceTotal", accountReceivableDetailsDTO.getInvoiceTotal());
				model.addAttribute("status", accountReceivableDetailsDTO.getInvoiceStatus());
//				model.addAttribute("layoutProduct", accountReceivableDetailsDTO.getLayoutProductType());
//				model.addAttribute("selectedProduct", accountReceivableDetailsDTO.getSelectedProductType());
//				model.addAttribute("fromDate", accountReceivableDetailsDTO.getFromDate());
//				model.addAttribute("toDate", accountReceivableDetailsDTO.getToDate());
				model.addAttribute("currentYear", accountReceivableDetailsDTO.getCurrentYear());
				model.addAttribute("invoiceNo", invoiceNo);
				model.addAttribute("brokerId", brokerId.toString());				
			}
			
			/*double invoiceTotal = 0;
			double roundInvoiceTotal = 0;
			Integer count = 0;
			Collection<OpenOrders> openOrdersList=null;
			String fromDateFinal = "";
			String toDateFinal = "";
			if(invoiceNoStr==null || invoiceNoStr.isEmpty()){
//				if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//				}
//				else{
//					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//					Calendar cal = Calendar.getInstance();
//					Date toDate = new Date();
//					cal.add(Calendar.MONTH, -3);
//					Date fromDate = cal.getTime();
//					fromDateStr = dateFormat.format(fromDate);
//					toDateStr = dateFormat.format(toDate);
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//				}
			}
			GridHeaderFilters filter = new GridHeaderFilters();
			
			if(brokerId == -1){
				brokerId = 0;
			}
			
			openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, null, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal, toDateFinal, filter, orderNoStr, invoiceNoStr, status, companyProduct, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			
			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				for(OpenOrders order : openOrdersList){
					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
				}
				for(Double invoiceTot : invoiceCountMap.values()){
					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
				}
			}
			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;		
			map.put("status",(status==null || status.isEmpty())?"":InvoiceStatus.valueOf(status));
			map.put("openOrders", JsonWrapperUtil.getInvoiceArray(openOrdersList));
			map.put("invoiceTotal", roundInvoiceTotal);
			//map.put("selectedProduct", companyProduct);
			//map.put("layoutProduct", companyProduct);
			//map.put("fromDate", fromDateStr);
			//map.put("toDate", toDateStr);
			map.put("invoiceNo", invoiceNoStr);
			map.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, null));
			map.put("currentYear", Calendar.getInstance().get(Calendar.YEAR));*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "page-manage-account-receivable";
	}
	
	@RequestMapping(value="/ContestInvoice")
	public String getContestInvoicePage(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
		
		try {
			String status = request.getParameter("status");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String invoiceNo = request.getParameter("invoiceNo");
			String orderNo = request.getParameter("orderNo");			
			String productType = request.getParameter("productType");
			String sortingString = request.getParameter("sortingString");
			Integer brokerId = Util.getBrokerId(session);
			
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("invoiceNo", invoiceNo);
			map.put("orderNo", orderNo);			
			map.put("productType", productType);
			map.put("brokerId", brokerId.toString());
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CONTEST_INVOICES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AccountReceivableDetailsDTO accountReceivableDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("accountReceivableDetailsDTO")), AccountReceivableDetailsDTO.class);
			
			if(accountReceivableDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", accountReceivableDetailsDTO.getMessage());
				model.addAttribute("statuss", accountReceivableDetailsDTO.getStatus());
			}else{
				model.addAttribute("msg", accountReceivableDetailsDTO.getError().getDescription());
				model.addAttribute("statuss", accountReceivableDetailsDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("pagingInfo", accountReceivableDetailsDTO.getAccReceivablePaginationDTO());
				model.addAttribute("openOrders", accountReceivableDetailsDTO.getOpenOrdersDTO());
				model.addAttribute("invoiceTotal", accountReceivableDetailsDTO.getInvoiceTotal());
			}else{
				model.addAttribute("pagingInfo", gson.toJson(accountReceivableDetailsDTO.getAccReceivablePaginationDTO()));
				model.addAttribute("openOrders", gson.toJson(accountReceivableDetailsDTO.getOpenOrdersDTO()));
				model.addAttribute("invoiceTotal", accountReceivableDetailsDTO.getInvoiceTotal());
				model.addAttribute("status", accountReceivableDetailsDTO.getInvoiceStatus());
//				model.addAttribute("layoutProduct", accountReceivableDetailsDTO.getLayoutProductType());
//				model.addAttribute("selectedProduct", accountReceivableDetailsDTO.getSelectedProductType());
//				model.addAttribute("fromDate", accountReceivableDetailsDTO.getFromDate());
//				model.addAttribute("toDate", accountReceivableDetailsDTO.getToDate());
				model.addAttribute("currentYear", accountReceivableDetailsDTO.getCurrentYear());
				model.addAttribute("invoiceNo", invoiceNo);
				model.addAttribute("brokerId", brokerId.toString());				
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "page-manage-contest-invoice";
	}
	
	
	/*@RequestMapping(value="/GetAccountReceivables")
	public String GetAccountReceivables(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String status = request.getParameter("status");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String productType = request.getParameter("productType");
			String action = request.getParameter("action");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");			
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("productType", productType);
			map.put("action", action);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_MANAGE_ACCOUNT_RECEIVABLES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AccountReceivableDetailsDTO accountReceivableDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("accountReceivableDetailsDTO")), AccountReceivableDetailsDTO.class);
			
			if(accountReceivableDetailsDTO.getStatus() == 1){				
				model.addAttribute("msg", accountReceivableDetailsDTO.getMessage());
				model.addAttribute("statuss", accountReceivableDetailsDTO.getStatus());
				model.addAttribute("pagingInfo", accountReceivableDetailsDTO.getAccReceivablePaginationDTO());
				model.addAttribute("openOrders", accountReceivableDetailsDTO.getOpenOrdersDTO());
				model.addAttribute("invoiceTotal", accountReceivableDetailsDTO.getInvoiceTotal());
			}else{
				model.addAttribute("msg", accountReceivableDetailsDTO.getError().getDescription());
				model.addAttribute("statuss", accountReceivableDetailsDTO.getStatus());
			}
			
//			JSONObject returnObject = new JSONObject();			
//			String fromDateFinal = "";
//			String toDateFinal = "";
//			double invoiceTotal = 0;
//			double roundInvoiceTotal = 0;
//			Integer count = 0;
//			Collection<OpenOrders> openOrdersList=null;
//			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
//						
//			if(action!=null && action.equalsIgnoreCase("search")){
//				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
//					fromDateFinal = fromDateStr + " 00:00:00";
//					toDateFinal = toDateStr + " 23:59:59";
//					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct,pageNo, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
//					count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null, null,status,companyProduct, brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
//				}else{
//					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(null,null,filter, null, null,status,companyProduct,pageNo,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
//					count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(null,null,filter, null, null,status,companyProduct,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
//				}
//			}
//			else if((fromDateStr != null && !fromDateStr.isEmpty()) && (toDateStr != null && !toDateStr.isEmpty())){
//				fromDateFinal = fromDateStr + " 00:00:00";
//				toDateFinal = toDateStr + " 23:59:59";
//				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,pageNo,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
//				count = DAORegistry.getQueryManagerDAO().getOpenOrderListCount(fromDateFinal,toDateFinal,filter,null,null,status,companyProduct,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
//			}
//			Map<Integer,Double> invoiceCountMap = new HashMap<Integer, Double>();
//			if(openOrdersList!=null && !openOrdersList.isEmpty()){
//				for(OpenOrders order : openOrdersList){
//					invoiceCountMap.put(order.getInvoiceId(), order.getInvoiceTotal());
//				}
//			}			
//			
//			if(openOrdersList!=null && !openOrdersList.isEmpty()){
//				for(Double invoiceTot : invoiceCountMap.values()){
//					invoiceTotal += invoiceTot;//order.getInvoiceTotal();
//				}
//			}		
//			roundInvoiceTotal = Math.round(invoiceTotal*100)/100.00;		
//			returnObject.put("openOrders", JsonWrapperUtil.getInvoiceArray(openOrdersList));
//			returnObject.put("pagingInfo", PaginationUtil.calculatePaginationParameters(count, pageNo));
//			returnObject.put("invoiceTotal", roundInvoiceTotal);
//			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}*/
	
	
	/*@RequestMapping(value="/InvoiceExportToExcel")
	public void getInvoicesToExport(HttpServletRequest request, HttpServletResponse response){
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String invoiceNoStr = request.getParameter("invoiceNo");
		String csr = request.getParameter("csr");
		String orderNo = request.getParameter("orderNo");
		String customerStr = request.getParameter("customer");
		String status = request.getParameter("status");
		String companyProduct = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String headerFilter = request.getParameter("headerFilter");
		String fromDateFinal = "";
		String toDateFinal = "";
		Collection<OpenOrders> openOrdersList=null;
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListToExport(invoiceNoStr, orderNo, csr, customerStr, fromDateFinal, toDateFinal, status, companyProduct, externalOrderId,null,filter);
				}else{
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,fromDateFinal,toDateFinal,status,companyProduct,externalOrderId,filter,brokerId,null);
				}
			}else{
				if(companyProduct.equalsIgnoreCase("RTW") || companyProduct.equalsIgnoreCase("RTW2")){
					openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListToExport(invoiceNoStr, orderNo, csr, customerStr, null, null, status, companyProduct, externalOrderId,null,filter);
				}else{
					openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,null,null,status,companyProduct,externalOrderId,filter,brokerId,null);
				}
			}	

			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("invoice");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Product Type");
				header.createCell(2).setCellValue("Customer Id");
				header.createCell(3).setCellValue("Customer Name");
				header.createCell(4).setCellValue("Username");
				header.createCell(5).setCellValue("Customer Type");
				header.createCell(6).setCellValue("Customer Company Name");
				header.createCell(7).setCellValue("Ticket Count");
				header.createCell(8).setCellValue("Invoice Total");
				header.createCell(9).setCellValue("Address");
				header.createCell(10).setCellValue("Country");
				header.createCell(11).setCellValue("State");
				header.createCell(12).setCellValue("City");
				header.createCell(13).setCellValue("Zipcode");
				header.createCell(14).setCellValue("Phone");
				header.createCell(15).setCellValue("Invoice Aged");
				header.createCell(16).setCellValue("Created Date");
				header.createCell(17).setCellValue("Created By");
				header.createCell(18).setCellValue("CSR");
				header.createCell(19).setCellValue("Secondary Order Type");
				header.createCell(20).setCellValue("Secondary Order Id");
				header.createCell(21).setCellValue("Voided Date");
				header.createCell(22).setCellValue("Is Invoice Email Sent");
				header.createCell(23).setCellValue("Purchase Order No");
				header.createCell(24).setCellValue("Shipping Method");
				header.createCell(25).setCellValue("Tracking No");
				header.createCell(26).setCellValue("Customer Order Id");
				header.createCell(27).setCellValue("Last Updated Date");
				header.createCell(28).setCellValue("Last Updated By");
				header.createCell(29).setCellValue("Status");
				header.createCell(30).setCellValue("Event Id");
				header.createCell(31).setCellValue("Event Name");
				header.createCell(32).setCellValue("Event Date");
				header.createCell(33).setCellValue("Event Time");
				header.createCell(34).setCellValue("Broker Id");
				header.createCell(35).setCellValue("Loyal Fan Order");
				header.createCell(36).setCellValue("Primary Payment Method");
				header.createCell(37).setCellValue("Secondary Payment Method");
				header.createCell(38).setCellValue("Third Payment Method");
				int i=1;
				for(OpenOrders order : openOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceId());
					row.createCell(1).setCellValue(order.getProductType());
					row.createCell(2).setCellValue(order.getCustomerId()!=null?order.getCustomerId():0);
					row.createCell(3).setCellValue(order.getCustomerName()!=null?order.getCustomerType():"");
					row.createCell(4).setCellValue(order.getUsername());
					row.createCell(5).setCellValue(order.getCustomerType()!=null?order.getCustomerType():"");
					row.createCell(6).setCellValue(order.getCustCompanyName()!=null?order.getCustCompanyName():"");
					row.createCell(7).setCellValue(order.getTicketCount()!=null?order.getTicketCount():0);
					row.createCell(8).setCellValue(order.getInvoiceTotal());
					row.createCell(9).setCellValue(order.getAddressLine1()!=null?order.getAddressLine1():"");
					row.createCell(10).setCellValue(order.getCountry()!=null?order.getCountry():"");
					row.createCell(11).setCellValue(order.getState()!=null?order.getState():"");
					row.createCell(12).setCellValue(order.getCity()!=null?order.getCity():"");
					row.createCell(13).setCellValue(order.getZipCode()!=null?order.getZipCode():"");
					row.createCell(14).setCellValue(order.getPhone()!=null?order.getPhone():"");
					row.createCell(15).setCellValue(order.getInvoiceAged()!=null?order.getInvoiceAged():0);
					row.createCell(16).setCellValue(order.getCreatedDateStr()!=null?order.getCreatedDateStr():"");
					row.createCell(17).setCellValue(order.getCreatedBy()!=null?order.getCreatedBy():"");
					row.createCell(18).setCellValue(order.getCsr()!=null?order.getCsr():"");
					row.createCell(19).setCellValue(order.getSecondaryOrderType()!=null?order.getSecondaryOrderType():"");
					row.createCell(20).setCellValue(order.getSecondaryOrderId()!=null?order.getSecondaryOrderId():"");
					row.createCell(21).setCellValue(order.getVoidedDateStr()!=null?order.getVoidedDateStr():"");
					row.createCell(22).setCellValue(order.getIsInvoiceEmailSent());
					row.createCell(23).setCellValue(order.getPurchaseOrderNo()!=null?order.getPurchaseOrderNo():"");
					row.createCell(24).setCellValue(order.getShippingMethod()!=null?order.getShippingMethod():"");
					row.createCell(25).setCellValue(order.getTrackingNo()!=null?order.getTrackingNo():"");
					row.createCell(26).setCellValue(order.getCustomerOrderId()!=null?order.getCustomerOrderId():0);
					row.createCell(27).setCellValue(order.getLastUpdatedDateStr()!=null?order.getLastUpdatedDateStr():"");
					row.createCell(28).setCellValue(order.getLastUpdatedBy()!=null?order.getLastUpdatedBy():"");
					row.createCell(29).setCellValue(order.getStatus()!=null?order.getStatus():"");
					row.createCell(30).setCellValue(order.getEventId()!=null?order.getEventId():0);
					row.createCell(31).setCellValue(order.getEventName()!=null?order.getEventName():"");
					row.createCell(32).setCellValue(order.getEventDateStr()!=null?order.getEventDateStr():"");
					row.createCell(33).setCellValue(order.getEventTimeStr()!=null?order.getEventTimeStr():"");
					row.createCell(34).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					if(order.getOrderType()!= null && order.getOrderType().equalsIgnoreCase("LOYALFAN")){
						row.createCell(35).setCellValue("Yes");
					}else{
						row.createCell(35).setCellValue("No");
					}
					row.createCell(36).setCellValue(order.getPrimaryPaymentMethod()!=null?order.getPrimaryPaymentMethod():"");
					row.createCell(37).setCellValue(order.getSecondaryPaymentMethod()!=null?order.getSecondaryPaymentMethod():"");
					row.createCell(38).setCellValue(order.getThirdPaymentMethod()!=null?order.getThirdPaymentMethod():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=invoice.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}*/
	
	
	/*@RequestMapping(value="/AccountReceivableExportToExcel")
	public void getAccountReceivableToExport(HttpServletRequest request, HttpServletResponse response){
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String invoiceNoStr = request.getParameter("invoiceNo");
		String csr = request.getParameter("csr");
		String orderNo = request.getParameter("orderNo");
		String customerStr = request.getParameter("customer");
		String status = request.getParameter("status");
		String companyProduct = request.getParameter("productType");
		String externalOrderId = request.getParameter("externalOrderId");
		String headerFilter = request.getParameter("headerFilter");
		String fromDateFinal = "";
		String toDateFinal = "";
		Collection<OpenOrders> openOrdersList=null;
		try {
			GridHeaderFilters filter = GridHeaderFiltersUtil.getInvoiceSearchHeaderFilters(headerFilter);
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			if(brokerId == -1){
				brokerId = 0;
			}
			if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){
				fromDateFinal = fromDateStr + " 00:00:00";
				toDateFinal = toDateStr + " 23:59:59";
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,fromDateFinal,toDateFinal,status,companyProduct,externalOrderId,filter,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			}else{
				openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderListToExport(invoiceNoStr,orderNo,csr,customerStr,null,null,status,companyProduct,externalOrderId,filter,brokerId,PaymentMethod.ACCOUNT_RECIVABLE.toString());
			}	

			if(openOrdersList!=null && !openOrdersList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("invoice");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Invoice Id");
				header.createCell(1).setCellValue("Product Type");
				header.createCell(2).setCellValue("Customer Id");
				header.createCell(3).setCellValue("Customer Name");
				header.createCell(4).setCellValue("Username");
				header.createCell(5).setCellValue("Customer Type");
				header.createCell(6).setCellValue("Customer Company Name");
				header.createCell(7).setCellValue("Ticket Count");
				header.createCell(8).setCellValue("Invoice Total");
				header.createCell(9).setCellValue("Address");
				header.createCell(10).setCellValue("Country");
				header.createCell(11).setCellValue("State");
				header.createCell(12).setCellValue("City");
				header.createCell(13).setCellValue("Zipcode");
				header.createCell(14).setCellValue("Phone");
				header.createCell(15).setCellValue("Invoice Aged");
				header.createCell(16).setCellValue("Created Date");
				header.createCell(17).setCellValue("Created By");
				header.createCell(18).setCellValue("CSR");
				header.createCell(19).setCellValue("Secondary Order Type");
				header.createCell(20).setCellValue("Secondary Order Id");
				header.createCell(21).setCellValue("Voided Date");
				header.createCell(22).setCellValue("Is Invoice Email Sent");
				header.createCell(23).setCellValue("Purchase Order No");
				header.createCell(24).setCellValue("Shipping Method");
				header.createCell(25).setCellValue("Tracking No");
				header.createCell(26).setCellValue("Customer Order Id");
				header.createCell(27).setCellValue("Last Updated Date");
				header.createCell(28).setCellValue("Last Updated By");
				header.createCell(29).setCellValue("Status");
				header.createCell(30).setCellValue("Event Id");
				header.createCell(31).setCellValue("Event Name");
				header.createCell(32).setCellValue("Event Date");
				header.createCell(33).setCellValue("Event Time");
				header.createCell(34).setCellValue("Broker Id");
				header.createCell(35).setCellValue("Loyal Fan Order");
				header.createCell(36).setCellValue("Primary Payment Method");
				header.createCell(37).setCellValue("Secondary Payment Method");
				header.createCell(38).setCellValue("Third Payment Method");
				int i=1;
				for(OpenOrders order : openOrdersList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(order.getInvoiceId());
					row.createCell(1).setCellValue(order.getProductType());
					row.createCell(2).setCellValue(order.getCustomerId()!=null?order.getCustomerId():0);
					row.createCell(3).setCellValue(order.getCustomerName()!=null?order.getCustomerType():"");
					row.createCell(4).setCellValue(order.getUsername());
					row.createCell(5).setCellValue(order.getCustomerType()!=null?order.getCustomerType():"");
					row.createCell(6).setCellValue(order.getCustCompanyName()!=null?order.getCustCompanyName():"");
					row.createCell(7).setCellValue(order.getTicketCount()!=null?order.getTicketCount():0);
					row.createCell(8).setCellValue(order.getInvoiceTotal());
					row.createCell(9).setCellValue(order.getAddressLine1()!=null?order.getAddressLine1():"");
					row.createCell(10).setCellValue(order.getCountry()!=null?order.getCountry():"");
					row.createCell(11).setCellValue(order.getState()!=null?order.getState():"");
					row.createCell(12).setCellValue(order.getCity()!=null?order.getCity():"");
					row.createCell(13).setCellValue(order.getZipCode()!=null?order.getZipCode():"");
					row.createCell(14).setCellValue(order.getPhone()!=null?order.getPhone():"");
					row.createCell(15).setCellValue(order.getInvoiceAged()!=null?order.getInvoiceAged():0);
					row.createCell(16).setCellValue(order.getCreatedDateStr()!=null?order.getCreatedDateStr():"");
					row.createCell(17).setCellValue(order.getCreatedBy()!=null?order.getCreatedBy():"");
					row.createCell(18).setCellValue(order.getCsr()!=null?order.getCsr():"");
					row.createCell(19).setCellValue(order.getSecondaryOrderType()!=null?order.getSecondaryOrderType():"");
					row.createCell(20).setCellValue(order.getSecondaryOrderId()!=null?order.getSecondaryOrderId():"");
					row.createCell(21).setCellValue(order.getVoidedDateStr()!=null?order.getVoidedDateStr():"");
					row.createCell(22).setCellValue(order.getIsInvoiceEmailSent());
					row.createCell(23).setCellValue(order.getPurchaseOrderNo()!=null?order.getPurchaseOrderNo():"");
					row.createCell(24).setCellValue(order.getShippingMethod()!=null?order.getShippingMethod():"");
					row.createCell(25).setCellValue(order.getTrackingNo()!=null?order.getTrackingNo():"");
					row.createCell(26).setCellValue(order.getCustomerOrderId()!=null?order.getCustomerOrderId():0);
					row.createCell(27).setCellValue(order.getLastUpdatedDateStr()!=null?order.getLastUpdatedDateStr():"");
					row.createCell(28).setCellValue(order.getLastUpdatedBy()!=null?order.getLastUpdatedBy():"");
					row.createCell(29).setCellValue(order.getStatus()!=null?order.getStatus():"");
					row.createCell(30).setCellValue(order.getEventId()!=null?order.getEventId():0);
					row.createCell(31).setCellValue(order.getEventName()!=null?order.getEventName():"");
					row.createCell(32).setCellValue(order.getEventDateStr()!=null?order.getEventDateStr():"");
					row.createCell(33).setCellValue(order.getEventTimeStr()!=null?order.getEventTimeStr():"");
					row.createCell(34).setCellValue(order.getBrokerId()!=null?order.getBrokerId().toString():"");
					if(order.getOrderType()!= null && order.getOrderType().equalsIgnoreCase("LOYALFAN")){
						row.createCell(35).setCellValue("Yes");
					}else{
						row.createCell(35).setCellValue("No");
					}
					row.createCell(36).setCellValue(order.getPrimaryPaymentMethod()!=null?order.getPrimaryPaymentMethod():"");
					row.createCell(37).setCellValue(order.getSecondaryPaymentMethod()!=null?order.getSecondaryPaymentMethod():"");
					row.createCell(38).setCellValue(order.getThirdPaymentMethod()!=null?order.getThirdPaymentMethod():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=invoice.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}*/
	
	/** 
	 * Fetch list of open orders for invoice
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/AddRealTicket")
	public String addRealTicket(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		try {
			String invoiceId = request.getParameter("invoiceId");
			String orderId = request.getParameter("orderId");
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			//String msg = request.getParameter("msg");
			//String statusStr = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("orderId", orderId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_EDIT_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceEditDTO invoiceEditDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceEditDTO")), InvoiceEditDTO.class);
			
			if(invoiceEditDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceEditDTO.getMessage());
				model.addAttribute("status", invoiceEditDTO.getStatus());
				model.addAttribute("invoice", invoiceEditDTO.getInvoiceDTO());
				model.addAttribute("expDeliveryDate", invoiceEditDTO.getInvoiceExpDeliveryDateDTO());
				model.addAttribute("customerOrder", invoiceEditDTO.getCustomerOrderDTO());				
				model.addAttribute("openOrder", invoiceEditDTO.getOpenOrdersDTO());
				model.addAttribute("shippingAddress", invoiceEditDTO.getCustomerShippingAddressDTO());
				model.addAttribute("ticketGroups", invoiceEditDTO.getTicketGroupDTO());
				model.addAttribute("eticketAttachments", invoiceEditDTO.getEticketAttachmentDTO());
				model.addAttribute("eticketCount", invoiceEditDTO.getEticketCount());
				model.addAttribute("qrcodeAttachments", invoiceEditDTO.getQrcodeAttachmentDTO());
				model.addAttribute("qrcodeCount", invoiceEditDTO.getQrcodeCount());
				model.addAttribute("barcodeAttachments", invoiceEditDTO.getBarcodeAttachmentDTO());
				model.addAttribute("barcodeCount", invoiceEditDTO.getBarcodeCount());
				model.addAttribute("shippingMethods", invoiceEditDTO.getShippingMethodDTO());
			}else{
				model.addAttribute("msg", invoiceEditDTO.getError().getDescription());
				model.addAttribute("status", invoiceEditDTO.getStatus());
			}
			
			/*Integer invId = 0;
			Integer customerId = 0;
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			//Getting Broker ID
									
			if(msg!=null && !msg.isEmpty()){
				jObj.put("msg",msg);
				if(statusStr != null && !statusStr.isEmpty()){
					jObj.put("status", Boolean.parseBoolean(statusStr));
				}
			}
			
			if(invoiceId == null){
				jObj.put("msg", "Not able to identify Invoice.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}			
			try{
				invId = Integer.parseInt(invoiceId);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Bad value found for Invoice ID, It should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			List<OpenOrders> openOrderListByInv = DAORegistry.getQueryManagerDAO().getOpenOrderListByInvoice(invId, brokerId);
			if(openOrderListByInv == null || openOrderListByInv.size() == 0){
				jObj.put("msg", "Invoice Details not found in system.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			for (OpenOrders openOrders : openOrderListByInv) {
				customerId = openOrders.getCustomerId();
			}
			
			CustomerOrderDetails orderShippingAddress = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(Integer.parseInt(orderId));
			List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceId));
			List<Integer> list = new ArrayList<Integer>();
			Map<Integer,List<Ticket>> ticketGroupListMap = new HashMap<Integer, List<Ticket>>(); 
			List<Ticket> ticketList = new ArrayList<Ticket>();
			if(tickets!=null && !tickets.isEmpty()){
				for(Ticket tic : tickets){
					if(!list.contains(tic.getTicketGroupId())){
						list.add(tic.getTicketGroupId());
					}
					if(ticketGroupListMap.get(tic.getTicketGroupId())!=null){
						ticketList = ticketGroupListMap.get(tic.getTicketGroupId());
						ticketList.add(tic);
					}else{
						ticketList = new ArrayList<Ticket>();
						ticketList.add(tic);
					}
					ticketGroupListMap.put(tic.getTicketGroupId(),ticketList);
				}
				for(Integer ticketGroupId : list){
					TicketGroup ticketGroup = DAORegistry.getTicketGroupDAO().get(ticketGroupId);
					ticketList = ticketGroupListMap.get(ticketGroupId);
					if(ticketList!=null && !ticketList.isEmpty()){
						ticketGroup.setMappedSeatLow(ticketList.get(0).getSeatNo());
						ticketGroup.setMappedSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
						ticketGroup.setMappedQty(ticketList.size());
					}
					ticketGroups.add(ticketGroup);
				}
			}
			
			OpenOrders operOrder = null;
			if(openOrderListByInv != null && !openOrderListByInv.isEmpty()) {
				operOrder = openOrderListByInv.get(0);
			}
			Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceId));
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			List<InvoiceTicketAttachment> eticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.ETICKET);			
			for(InvoiceTicketAttachment eTicket: eticketAttachments){
				eTicket.setFileName(Util.getFileNameFromPath(eTicket.getFilePath()));
			}
			List<InvoiceTicketAttachment> qrcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.QRCODE);
			for(InvoiceTicketAttachment qrCode: qrcodeAttachments){
				qrCode.setFileName(Util.getFileNameFromPath(qrCode.getFilePath()));
			}
			List<InvoiceTicketAttachment> barcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.BARCODE);
			for(InvoiceTicketAttachment barCode: barcodeAttachments){
				barCode.setFileName(Util.getFileNameFromPath(barCode.getFilePath()));
			}
			if(invoice!=null){
				JSONObject invoiceObject = new JSONObject();
				invoiceObject.put("invoiceId", invoice.getId());
				invoiceObject.put("invoiceTrackingNo", invoice.getTrackingNo());
				invoiceObject.put("invoiceStatus", invoice.getStatus());
				invoiceObject.put("invoiceRealTixMap", invoice.getRealTixMap());				
				jObj.put("invoice", invoiceObject);
				
				if(invoice.getExpDeliveryDate()!=null){
					DateFormat format = new SimpleDateFormat("mm/DD/yyyy");
					jObj.put("expDeliveryDate", format.format(invoice.getExpDeliveryDate()));
				}
			}
			if(customerOrder != null){
				JSONObject cusOrderObject = new JSONObject();
				cusOrderObject.put("eventId", customerOrder.getEventId());
				cusOrderObject.put("eventName", customerOrder.getEventName());
				cusOrderObject.put("eventDateStr", customerOrder.getEventDateStr());
				cusOrderObject.put("eventTimeStr", customerOrder.getEventTimeStr());
				cusOrderObject.put("venueName", customerOrder.getVenueName());
				cusOrderObject.put("primaryPaymentMethod", customerOrder.getPrimaryPaymentMethod());
				cusOrderObject.put("primaryAvailableAmt", customerOrder.getPrimaryAvailableAmt());				
				cusOrderObject.put("secondaryPaymentMethod", customerOrder.getSecondaryPaymentMethod());				
				cusOrderObject.put("secondaryAvailableAmt", customerOrder.getSecondaryAvailableAmt());				
				cusOrderObject.put("thirdPaymentMethod", customerOrder.getThirdPaymentMethod());
				cusOrderObject.put("thirdAvailableAmt", customerOrder.getThirdAvailableAmt());				
				jObj.put("customerOrder", cusOrderObject);
			}
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			if(shippingMethods != null){
				JSONArray shippingMethodArray = new JSONArray();
				JSONObject shippingMethodObject = null;
				for(ShippingMethod shippingMethod : shippingMethods){
					shippingMethodObject = new JSONObject();
					shippingMethodObject.put("shippingMethodId", shippingMethod.getId());
					shippingMethodObject.put("shippingMethodName", shippingMethod.getName());
					shippingMethodArray.put(shippingMethodObject);
				}
				jObj.put("shippingMethods", shippingMethodArray);
			}			
			if(operOrder != null){
				JSONObject operOrderObject = new JSONObject();
				operOrderObject.put("openOrderInvoiceId", operOrder.getInvoiceId());
				operOrderObject.put("openOrderProductType", operOrder.getProductType());
				operOrderObject.put("openOrderSection", operOrder.getSection());
				operOrderObject.put("openOrderRow", operOrder.getRow());
				operOrderObject.put("openOrderQuantity", operOrder.getQty());
				operOrderObject.put("openOrderPrice", operOrder.getSoldPrice());
				operOrderObject.put("openOrderShippingMethodId", operOrder.getShippingMethodId());
				operOrderObject.put("openOrderShippingMethod", operOrder.getShippingMethod());
				operOrderObject.put("openOrderCustomerName", operOrder.getCustomerName()+" "+operOrder.getLastName());
				operOrderObject.put("openOrderUserName", operOrder.getUsername()); //Email
				operOrderObject.put("openOrderPhone", operOrder.getPhone());
				operOrderObject.put("openOrderAddressLine1", operOrder.getAddressLine1());
				operOrderObject.put("openOrderCountry", operOrder.getCountry());
				operOrderObject.put("openOrderState", operOrder.getState());
				operOrderObject.put("openOrderCity", operOrder.getCity());
				operOrderObject.put("openOrderZipcode", operOrder.getZipCode());
				operOrderObject.put("openOrderRealTixDelivered", operOrder.getRealTixDelivered());
				operOrderObject.put("longSale","NO");
				if(customerOrder.getIsLongSale()){
					operOrderObject.put("longSale","YES");
					operOrderObject.put("openOrderQuantity",customerOrder.getQty());
					operOrderObject.put("openOrderShippingMethod",customerOrder.getShippingMethod());
					operOrderObject.put("openOrderShippingMethodId",customerOrder.getShippingMethodId());
				}
				
				jObj.put("openOrder", operOrderObject);
				
			}			
			if(orderShippingAddress != null){
				JSONObject orderShippingAddressObject = new JSONObject();
				orderShippingAddressObject.put("shipAddressAddressLine1", orderShippingAddress.getShAddress1());
				orderShippingAddressObject.put("shipAddressAddressLine2", orderShippingAddress.getShAddress2());
				orderShippingAddressObject.put("shipAddressPhone1", orderShippingAddress.getShPhone1());	
				orderShippingAddressObject.put("shipAddressCountry", orderShippingAddress.getShCountryName());
				orderShippingAddressObject.put("shipAddressState", orderShippingAddress.getShStateName());
				orderShippingAddressObject.put("shipAddressCity", orderShippingAddress.getShCity());
				orderShippingAddressObject.put("shipAddressZipCode", orderShippingAddress.getShZipCode());			
				jObj.put("shippingAddress", orderShippingAddressObject);
			}
			if(ticketGroups != null){
				JSONArray ticketGroupsArray = new JSONArray();
				JSONObject ticketGroupsObject = null;
				for(TicketGroup ticketGroup : ticketGroups){
					ticketGroupsObject = new JSONObject();
					ticketGroupsObject.put("ticketGroupId", ticketGroup.getId());
					ticketGroupsObject.put("ticketGroupSection", ticketGroup.getSection());
					ticketGroupsObject.put("ticketGroupRow", ticketGroup.getRow());
					ticketGroupsObject.put("ticketGroupMappedSeatLow", ticketGroup.getMappedSeatLow());
					ticketGroupsObject.put("ticketGroupMappedSeatHigh", ticketGroup.getMappedSeatHigh());
					ticketGroupsObject.put("ticketGroupMappedQty", ticketGroup.getMappedQty());
					ticketGroupsObject.put("ticketGroupPrice", ticketGroup.getPriceStr());
					ticketGroupsArray.put(ticketGroupsObject);
				}
				jObj.put("ticketGroups", ticketGroupsArray);
			}
			if(eticketAttachments != null){
				JSONArray eticketAttachmentsArray = new JSONArray();
				JSONObject eticketAttachmentsObject = null;
				for(InvoiceTicketAttachment eticketAttachment : eticketAttachments){
					eticketAttachmentsObject = new JSONObject();
					eticketAttachmentsObject.put("ticketAttachmentInvoiceId", eticketAttachment.getInvoiceId());
					eticketAttachmentsObject.put("ticketAttachmentType", eticketAttachment.getType());
					eticketAttachmentsObject.put("ticketAttachmentPosition", eticketAttachment.getPosition());
					eticketAttachmentsObject.put("ticketAttachmentFileName", eticketAttachment.getFileName());
					eticketAttachmentsArray.put(eticketAttachmentsObject);
				}
				jObj.put("eticketAttachments", eticketAttachmentsArray);
			}
			jObj.put("eticketCount", eticketAttachments.size());
			if(qrcodeAttachments != null){
				JSONArray qrcodeAttachmentsArray = new JSONArray();
				JSONObject qrcodeAttachmentsObject = null;
				for(InvoiceTicketAttachment qrcodeAttachment : qrcodeAttachments){
					qrcodeAttachmentsObject = new JSONObject();
					qrcodeAttachmentsObject.put("qrcodeAttachmentInvoiceId", qrcodeAttachment.getInvoiceId());
					qrcodeAttachmentsObject.put("qrcodeAttachmentType", qrcodeAttachment.getType());
					qrcodeAttachmentsObject.put("qrcodeAttachmentPosition", qrcodeAttachment.getPosition());
					qrcodeAttachmentsObject.put("qrcodeAttachmentFileName", qrcodeAttachment.getFileName());
					qrcodeAttachmentsArray.put(qrcodeAttachmentsObject);
				}
				jObj.put("qrcodeAttachments", qrcodeAttachmentsArray);
			}
			jObj.put("qrcodeCount", qrcodeAttachments.size());
			if(barcodeAttachments != null){
				JSONArray barcodeAttachmentsArray = new JSONArray();
				JSONObject barcodeAttachmentsObject = null;
				for(InvoiceTicketAttachment barcodeAttachment : barcodeAttachments){
					barcodeAttachmentsObject = new JSONObject();
					barcodeAttachmentsObject.put("barcodeAttachmentInvoiceId", barcodeAttachment.getInvoiceId());
					barcodeAttachmentsObject.put("barcodeAttachmentType", barcodeAttachment.getType());
					barcodeAttachmentsObject.put("barcodeAttachmentPosition", barcodeAttachment.getPosition());
					barcodeAttachmentsObject.put("barcodeAttachmentFileName", barcodeAttachment.getFileName());
					barcodeAttachmentsArray.put(barcodeAttachmentsObject);
				}
				jObj.put("barcodeAttachments", barcodeAttachmentsArray);
			}
			jObj.put("barcodeCount", barcodeAttachments.size());
			jObj.put("openOrders", openOrderListByInv);			
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}	
	
	/*@RequestMapping(value = "/CreateRealTix")
	public String createOpenOrders(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) {
		String invoiceId = "";
		List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
		try {
			invoiceId = request.getParameter("invoiceId");
			String msg = request.getParameter("msg");			
			if(msg!=null && !msg.isEmpty()){
				map.put("successMessage",msg);
			}
			//Getting Broker ID
			HttpSession session = request.getSession(true);
			Integer brokerId = Util.getBrokerId(session);
			Integer invId = Integer.parseInt(invoiceId);
			List<OpenOrders> openOrderListByInv = DAORegistry
					.getQueryManagerDAO().getOpenOrderListByInvoice(invId, brokerId);
			Integer customerId = 0;
			for (OpenOrders openOrders : openOrderListByInv) {
				customerId = openOrders.getCustomerId();
			}
			
//			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			 
			List<CustomerAddress> shippingAddressList = DAORegistry
					.getCustomerAddressDAO().updatedShippingAddress(customerId);
			List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceId));
			List<Integer> list = new ArrayList<Integer>();
			Map<Integer,List<Ticket>> ticketGroupListMap = new HashMap<Integer, List<Ticket>>(); 
			List<Ticket> ticketList = new ArrayList<Ticket>();
			if(tickets!=null && !tickets.isEmpty()){
				for(Ticket tic : tickets){
					if(!list.contains(tic.getTicketGroupId())){
						list.add(tic.getTicketGroupId());
					}
					if(ticketGroupListMap.get(tic.getTicketGroupId())!=null){
						ticketList = ticketGroupListMap.get(tic.getTicketGroupId());
						ticketList.add(tic);
					}else{
						ticketList = new ArrayList<Ticket>();
						ticketList.add(tic);
					}
					ticketGroupListMap.put(tic.getTicketGroupId(),ticketList);
				}
				for(Integer ticketGroupId : list){
					TicketGroup ticketGroup = DAORegistry.getTicketGroupDAO().get(ticketGroupId);
					ticketList = ticketGroupListMap.get(ticketGroupId);
					if(ticketList!=null && !ticketList.isEmpty()){
						ticketGroup.setMappedSeatLow(ticketList.get(0).getSeatNo());
						ticketGroup.setMappedSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
						ticketGroup.setMappedQty(ticketList.size());
					}
					ticketGroups.add(ticketGroup);
				}
			}
			
			OpenOrders operOrder = null;
			if(openOrderListByInv != null && !openOrderListByInv.isEmpty()) {
				operOrder = openOrderListByInv.get(0);
			}
			Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceId));
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			List<InvoiceTicketAttachment> eticketAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.ETICKET);			
			for(InvoiceTicketAttachment eTicket: eticketAttachments){
				eTicket.setFileName(Util.getFileNameFromPath(eTicket.getFilePath()));
			}
			List<InvoiceTicketAttachment> qrcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.QRCODE);
			for(InvoiceTicketAttachment qrCode: qrcodeAttachments){
				qrCode.setFileName(Util.getFileNameFromPath(qrCode.getFilePath()));
			}
			List<InvoiceTicketAttachment> barcodeAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceId),FileType.BARCODE);
			for(InvoiceTicketAttachment barCode: barcodeAttachments){
				barCode.setFileName(Util.getFileNameFromPath(barCode.getFilePath()));
			}
			if(invoice!=null){
				map.put("invoice", invoice);
				if(invoice.getExpDeliveryDate()!=null){
					DateFormat format = new SimpleDateFormat("mm/DD/yyyy");
					map.put("expDeliveryDate", format.format(invoice.getExpDeliveryDate()));
				}
			}
			Collection<ShippingMethod> shippingMethods = DAORegistry.getShippingMethodDAO().getAll();
			map.put("shippingMethods", shippingMethods);
			map.put("eticketAttachments", eticketAttachments);
			map.put("qrcodeAttachments", qrcodeAttachments);
			map.put("barcodeAttachments", barcodeAttachments);
			map.put("eticketCount", eticketAttachments.size());
			map.put("qrcodeCount", qrcodeAttachments.size());
			map.put("barcodeCount", barcodeAttachments.size());
			map.put("customerOrder", customerOrder);
			map.put("openOrders", openOrderListByInv);
			map.put("shippingAddress", shippingAddressList);
			map.put("ticketGroups", ticketGroups);
			map.put("openOrder", operOrder);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage",
					"There is something wrong. Please try again..");
		}
		return "page-create-invoice-real-tix";
	}*/
	
	/** 
	 * Fetch list of open orders for invoice
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	/*@RequestMapping(value = "/CreateOpenOrder")
	public String createOpenOrders(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String returnMessage = "";
		String action = "";
		String invoiceId = "";
		try {
			invoiceId = request.getParameter("invoiceId");
			Integer invId = Integer.parseInt(invoiceId);
			Collection<OpenOrders> openOrderListByInv = DAORegistry.getQueryManagerDAO().getOpenOrderListByInvoice(invId);
			Integer customerId = 0;
			Integer eventId = 0;
			for(OpenOrders openOrders: openOrderListByInv){
				customerId = openOrders.getCustomerId();
				eventId = openOrders.getEventId();
			}
			
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().updatedShippingAddress(customerId);
			EventDetails eventDetail = null;
			if(eventId != null){
				eventDetail = DAORegistry.getEventDetailsDAO().get(eventId);
			}			
			map.put("eventDtl", eventDetail);
			map.put("openOrders", openOrderListByInv);
			map.put("shippingAddress", shippingAddressList);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong. Please try again..");
		}
		return "page-create-openorder";
	}*/
	
	/**
	 * Save open orders for invoice
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/SaveOpenOrders")
	public String saveOpenOrders(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userName", userName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String returnMessage = "";
			String action ="";
			String invoiceId = "";
			String eventIdStr = "";
			String trackingNo = null;
			String expDeliveryDate=null;
			Invoice invoice = null;  
			boolean status = false;
			DateFormat format = new SimpleDateFormat("mm/DD/yyyy");
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			action = request.getParameter("action");
			invoiceId = request.getParameter("invoiceId");
			eventIdStr = request.getParameter("eventId");
			expDeliveryDate = request.getParameter("expDeliveryDate");
			trackingNo = request.getParameter("trackingNo");
			
			if(invoiceId == null){
				jObj.put("msg", "not able to identify Invoice details.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(returnMessage != null){
			 if(action != null && action.equals("openOrderSave")){
				 Integer invId = 0;
				 Integer rowCount = Integer.parseInt(request.getParameter("rowCount"));
				 String shippingMethod = request.getParameter("shippingMethod");
				 Integer shippingMethodId = -1;				 
				 
				 String isTicketDelivered = request.getParameter("realTixDelivered");
				 String sentTicketToCust = request.getParameter("sendTicketToCustomer");
				 
				 if(shippingMethod == null || shippingMethod.isEmpty()){
					jObj.put("msg", "Not able to idenfity Shipping Method.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				 }
				 try{
					 shippingMethodId = Integer.parseInt(shippingMethod);
				 }catch(Exception e){
					 e.printStackTrace();
					 jObj.put("msg", "Shipping Method should be valid value.");
					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					 return;
				 }
				 
				 Boolean isRealTixDelivered = false;
				 if(isTicketDelivered != null && isTicketDelivered.equals("1")) {
					 isRealTixDelivered = true;
				 }
				
				 try{
					 invId = Integer.parseInt(invoiceId);
				 }catch(Exception e){
					 e.printStackTrace();
					 jObj.put("msg", "Bad vaue found for Invoice ID, It should be valid Integer value.");
					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					 return;
				 }
				 invoice =  DAORegistry.getInvoiceDAO().get(invId);
				 
				 if(invoice == null){
					 jObj.put("msg", "Invoice Details not found in system.");
					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					 return;
				 }
				 CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				 if(order == null){
					 jObj.put("msg", "Customer order not found in system.");
					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					 return;
				 }
				 ShippingMethod shipMethod = DAORegistry.getShippingMethodDAO().get(shippingMethodId);
				 if(shipMethod == null){
					 jObj.put("msg", "Shipping method is not found.");
					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					 return;
				 }
				 invoice.setTrackingNo(trackingNo);
				 invoice.setLastUpdated(new Date());
				 invoice.setLastUpdatedBy(userName);
				 if(shippingMethodId>=0){
					 invoice.setShippingMethodId(shippingMethodId);
					 invoice.setRealTixShippingMethod(shipMethod.getName());
				 }
				 invoice.setRealTixMap("Yes");
				 
				 invoice.setRealTixDelivered(isRealTixDelivered);
				 invoice.setUploadToExchnge(false);
				 if(sentTicketToCust!=null && sentTicketToCust.equalsIgnoreCase("Yes")){
					 invoice.setUploadToExchnge(true);
				 }
				 
				 if(expDeliveryDate!=null && !expDeliveryDate.isEmpty()){
					 invoice.setExpDeliveryDate(format.parse(expDeliveryDate));
				 }
				 order.setShippingMethod(shipMethod.getName());
				 order.setShippingMethodId(shippingMethodId);
				 
				 List<Ticket> ticketList  = new ArrayList<Ticket>();
				 List<CategoryTicket>  categoryTicketList = new ArrayList<CategoryTicket>();
				 if(!order.getIsLongSale()){
					 List<CategoryTicket> oldMappedCategoryTicket = DAORegistry.getCategoryTicketDAO().getMappedCategoryTicketByInvoiceId(invId);
					 List<Ticket> oldMappedTicket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invId);
					 if(oldMappedCategoryTicket!=null){
						 for(CategoryTicket catTicket:oldMappedCategoryTicket) {
							catTicket.setTicketId(null);
						}
						 DAORegistry.getCategoryTicketDAO().updateAll(oldMappedCategoryTicket);
					 }
					 if(oldMappedTicket!=null){
						 for(Ticket ticket:oldMappedTicket) {
							 ticket.setInvoiceId(null);
							 ticket.setTicketStatus(TicketStatus.ACTIVE);
						}
						 DAORegistry.getTicketDAO().updateAll(oldMappedTicket);
					 }
					 int totalQty = 0;
					 List<Integer> tgIds = new ArrayList<Integer>();
					 for(int i=1; i<=rowCount; i++){
						 int counter = 0;
						String ticketGroupId = request.getParameter("ticketGroup_" + i);
						String seatHighStr = request.getParameter("seatHigh_"+i);
						String seatLowStr = request.getParameter("seatLow_"+i);
						int seatLow = Integer.parseInt(seatLowStr);
						//int seatHigh = Integer.parseInt(seatHighStr);
						Integer qty = Integer.parseInt(request.getParameter("qty_"+i));
						totalQty += qty;
						 if(ticketGroupId!=null && !ticketGroupId.isEmpty() && qty > 0){
							 if(!tgIds.contains(Integer.parseInt(ticketGroupId))){
								 tgIds.add(Integer.parseInt(ticketGroupId));
							 }
							 List<Ticket> tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(Integer.parseInt(ticketGroupId));
							 if(tickets!=null && !tickets.isEmpty()){
								 List<CategoryTicket> categoryTickets = DAORegistry.getCategoryTicketDAO().getUnmappedCategoryTicketByInvoiceId(invId);
								 for(int j=seatLow;j<(seatLow+100);j++){
									 if(counter == qty){
										 break;
									 }
									 for(Ticket tic:tickets){
										 if(tic.getSeatNo().equalsIgnoreCase(String.valueOf(j)) && tic.getTicketStatus().equals(TicketStatus.ACTIVE)
												 && (tic.getInvoiceId()==null || tic.getInvoiceId()==0)){
											 for(Ticket t : ticketList){
												 if(t.getId() == tic.getId() || t.getId().equals(tic.getId())){
													 jObj.put("msg","SeatNo : "+tic.getSeatNo()+" Added two time in mapping, please fill unique seat no range in each ticket group.");
													 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
													 return;
												 }
											 }
											 tic.setInvoiceId(invId);
											 tic.setTicketStatus(TicketStatus.SOLD);
											 ticketList.add(tic);
											 if(order.getIsLongSale()){
												 counter++;
											 }
											 if(categoryTickets!=null && !categoryTickets.isEmpty()){
												 for(CategoryTicket catTicket:categoryTickets){
													 if(catTicket.getTicketId()==null || catTicket.getInvoiceId()==invId){
														 catTicket.setTicketId(tic.getId());
														 categoryTicketList.add(catTicket);
														 counter++;
														 break;
													 }
												 }
										 	}
									 	} 
									 }
								 }
								 if(order.getIsLongSale() && ticketList.size() != totalQty){
									  returnMessage = "SeatLow:"+seatLowStr +", SeatHigh:"+seatHighStr+" is already mapped with another invoice, Please select Other avilable seats";
									  jObj.put("msg",returnMessage);
									  IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
									  return;
								 }
								 if(!order.getIsLongSale() && (ticketList.size() != totalQty || categoryTicketList.size()!=totalQty)){
									 returnMessage = "SeatLow:"+seatLowStr +", SeatHigh:"+seatHighStr+" is already mapped with another invoice, Please select Other avilable seats";
									 jObj.put("msg",returnMessage);
									 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
									 return;
								 }
								 categoryTickets.clear(); 
							 }
						 }
					 }
					 boolean isMapped  = false;
					 if(order.getIsLongSale() && ticketList.size() == totalQty){
						 DAORegistry.getTicketDAO().updateAll(ticketList);
						 List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
						 Util.setActualTicketDetails(realTicketList, order);
						 categoryTicketList.clear();
						 ticketList.clear();
						 isMapped=true;
					 }
					 if(!order.getIsLongSale() && ticketList.size() == totalQty && categoryTicketList.size()==totalQty){
						 DAORegistry.getCategoryTicketDAO().updateAll(categoryTicketList);
						 DAORegistry.getTicketDAO().updateAll(ticketList);
						 List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
						 Util.setActualTicketDetails(realTicketList, order);
						 categoryTicketList.clear();
						 ticketList.clear();
						 isMapped=true;
					 }
					 if(isMapped){
						List<OrderTicketGroupDetails> orderTickets = new ArrayList<OrderTicketGroupDetails>();
						List<OrderTicketGroupDetails> oldOrderTicketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(order.getId());
						for(Integer id : tgIds){
							TicketGroup tg = DAORegistry.getTicketGroupDAO().get(id);
							List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceIdandTicketGroupId(invoice.getId(),tg.getId());
							OrderTicketGroupDetails orderTicket = new OrderTicketGroupDetails();
//							String zone = Util.computZoneForTickets(tg.getEventId(),tg.getSection());
//							if(zone==null || zone.isEmpty()){
//								zone=order.getSection();
//							}
							orderTicket.setZone(order.getSection());
							orderTicket.setQuantity(tickets.size());
							orderTicket.setSection(tg.getSection());
							orderTicket.setRow(tg.getRow());
							orderTicket.setTicketGroupId(tg.getId());
							orderTicket.setSoldPrice(order.getSoldPrice());
							orderTicket.setActualPrice(tg.getPrice());
							orderTicket.setOrderId(order.getId());
							
							
							if(tickets.size() == 1){
								orderTicket.setSeat(tickets.get(0).getSeatNo());
							}else{
								orderTicket.setSeat(tickets.get(0).getSeatNo()+"-"+tickets.get(tickets.size()-1).getSeatNo());
							}
							orderTickets.add(orderTicket);
						}
						
						DAORegistry.getOrderTicketGroupDetailsDAO().saveAll(orderTickets);
						DAORegistry.getOrderTicketGroupDetailsDAO().deleteAll(oldOrderTicketGroupDetails);
						 
						String poNoStr = "";
						 for(Ticket tic:ticketList){
							 if(!poNoStr.contains(String.valueOf(tic.getPoId()))){
								 poNoStr = poNoStr + tic.getPoId()+",";
							 }
						 }
						 returnMessage = "Real Ticket mapped successfully.";
						 
						
						 List<InvoiceAudit> list = DAORegistry.getInvoiceAuditDAO().getInvoiceAuditByInvoiceIdAndAction(invoice.getId(), InvoiceAuditAction.REAL_TICKET_MAPPED);
						 for(InvoiceAudit au : list){
							 if(au.getNote().equalsIgnoreCase("Real Ticket is Mapped, PO. No:"+poNoStr)){
								 isMapped =true;
								 break;
							 }
						 }
						 if(!isMapped){
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_MAPPED);
							 audit.setNote("Real Ticket is Mapped, PO. No:"+poNoStr);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
						 }
					 }else{
						 jObj.put("msg","Not able to map real ticket selected seat is already mapped, Please select soem other seat.");
						 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						 return;
					 }
				 }
				 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 DAORegistry.getCustomerOrderDAO().update(order);
				 if(returnMessage == null || returnMessage.isEmpty()){
					 returnMessage = "Invoice Updated successfully.";
				 }
				 
				 Map<String,Boolean> fileUploadMap = new HashMap<String, Boolean>();
				 if(uploadRealTicket(request,fileUploadMap)){
					 invoice.setIsRealTixUploaded("PROCESSING");
					 DAORegistry.getInvoiceDAO().update(invoice);
					 Map<String,String> requestMap = Util.getParameterMap(request);
					 requestMap.put("orderId",String.valueOf(invoice.getCustomerOrderId()));
					 
					 List<InvoiceTicketAttachment> eTicketAttachment = new ArrayList<InvoiceTicketAttachment>();
					 List<InvoiceTicketAttachment> qrCodeAttachment = new ArrayList<InvoiceTicketAttachment>();
					 List<InvoiceTicketAttachment> barcodeAttachment = new ArrayList<InvoiceTicketAttachment>();
					 
					 List<InvoiceTicketAttachment> attachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
					 for(InvoiceTicketAttachment attachment :attachments){
						 if(attachment.getIsEmailSent()== null || !attachment.getIsEmailSent()){
							 if(attachment.getType().equals(FileType.ETICKET)){
								 eTicketAttachment.add(attachment);
							 }else if(attachment.getType().equals(FileType.QRCODE)){
								 qrCodeAttachment.add(attachment);
							 }else if(attachment.getType().equals(FileType.BARCODE)){
								 barcodeAttachment.add(attachment);
							 }
						 }
					 }
					 
					 List<String> eticketFiles = new ArrayList<String>();
					 List<String> qrCodeFiles = new ArrayList<String>();
					 List<String> barcodeFile = new ArrayList<String>();
					 
					 if(invoice.getUploadToExchnge()){
						 for(InvoiceTicketAttachment a :eTicketAttachment){
							 if(a.getIsSent() == null || !a.getIsSent()){
								 eticketFiles.add(a.getFilePath());
							 }
						 }
						 for(InvoiceTicketAttachment a :qrCodeAttachment){
							 if(a.getIsSent() == null || !a.getIsSent()){
								 qrCodeFiles.add(a.getFilePath());
							 }
						 }
						 for(InvoiceTicketAttachment a :barcodeAttachment){
							 if(a.getIsSent() == null || !a.getIsSent()){
								 barcodeFile.add(a.getFilePath());
							 }
						 }
						 requestMap.put("attachTicket","true");
					 }else{
						 requestMap.put("attachTicket","false");
					 }
					 
					 boolean isEticket = fileUploadMap.get(FileType.ETICKET.toString());
					 boolean isqrCode = fileUploadMap.get(FileType.QRCODE.toString());
					 boolean isBarcode = fileUploadMap.get(FileType.BARCODE.toString());
					 
					 if(isEticket){
						 requestMap.put("fileType",FileType.ETICKET.toString());
						 String data = Util.getObjectWithFile(requestMap, Constants.BASE_URL + Constants.TICKET_DOWNLOAD_NOTIFICATION,eticketFiles);
						 Gson gson = new Gson();		
						 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
						 if(ticketDownload!=null && ticketDownload.getStatus()==1){
							 returnMessage = "Real Ticket mapped/uploaded, email sent to customer";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 for(InvoiceTicketAttachment a :eTicketAttachment){
								a.setIsEmailSent(true);
								if(invoice.getUploadToExchnge()){
									a.setIsSent(true);
								}
							 }
							 DAORegistry.getInvoiceTicketAttachmentDAO().updateAll(eTicketAttachment);
							
						 }else{
							 returnMessage = "Real Ticket mapped/uploaded, Error while sending email to customer.";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
						 }
					 }
					 if(isqrCode){
						 requestMap.put("fileType",FileType.QRCODE.toString());
						 String data = Util.getObjectWithFile(requestMap, Constants.BASE_URL + Constants.TICKET_DOWNLOAD_NOTIFICATION,qrCodeFiles);
						 Gson gson = new Gson();		
						 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
						 if(ticketDownload!=null && ticketDownload.getStatus()==1){
							 returnMessage = "Real Ticket mapped/uploaded, email sent to customer";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 for(InvoiceTicketAttachment a :qrCodeAttachment){
								a.setIsEmailSent(true);
								if(invoice.getUploadToExchnge()){
									a.setIsSent(true);
								}
							 }
							 DAORegistry.getInvoiceTicketAttachmentDAO().updateAll(qrCodeAttachment);
						 }else{
							 returnMessage = "Real Ticket mapped/uploaded, Error while sending email to customer.";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
						 }
					 }
					 if(isBarcode){
						 requestMap.put("fileType",FileType.BARCODE.toString());
						 String data = Util.getObjectWithFile(requestMap, Constants.BASE_URL + Constants.TICKET_DOWNLOAD_NOTIFICATION,barcodeFile);
						 Gson gson = new Gson();		
						 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
						 if(ticketDownload!=null && ticketDownload.getStatus()==1){
							 returnMessage = "Real Ticket mapped/uploaded, email sent to customer";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
							 for(InvoiceTicketAttachment a :barcodeAttachment){
								a.setIsEmailSent(true);
								if(invoice.getUploadToExchnge()){
									a.setIsSent(true);
								}
							 }
							 DAORegistry.getInvoiceTicketAttachmentDAO().updateAll(barcodeAttachment);
						 }else{
							 returnMessage = "Real Ticket mapped/uploaded, Error while sending email to customer.";
							 InvoiceAudit audit = new InvoiceAudit(invoice);
							 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
							 audit.setNote(returnMessage);
							 audit.setCreatedDate(new Date());
							 audit.setCreateBy(userName);
							 DAORegistry.getInvoiceAuditDAO().save(audit);
						 }
					 }
					 invoice.setIsRealTixUploaded("Yes");
					 invoice.setStatus(InvoiceStatus.Completed);
					 if(invoice.getInvoiceType().equals(PaymentMethod.ACCOUNT_RECIVABLE.toString())
							 && order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
						 invoice.setStatus(InvoiceStatus.Outstanding);
					 }
					 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 }else if(shipMethod!= null && (shipMethod.getId().equals(13) || shipMethod.getId() == 13)){
					 Map<String,String> requestMap = Util.getParameterMap(request);
					 requestMap.put("orderId",String.valueOf(invoice.getCustomerOrderId()));
					 requestMap.put("fileType",FileType.OTHER.toString());
					 requestMap.put("attachTicket","false");
					 String data = Util.getObject(requestMap, Constants.BASE_URL + Constants.TICKET_DOWNLOAD_NOTIFICATION);
					 Gson gson = new Gson();		
					 JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					 TicketDownloadNotification ticketDownload = gson.fromJson(((JsonObject)jsonObject.get("ticketDownloadNotification")), TicketDownloadNotification.class);
					 if(ticketDownload!=null && ticketDownload.getStatus()==1){
						 returnMessage = "Real Ticket mapped and Flash Seat Details email sent to customer";
						 InvoiceAudit audit = new InvoiceAudit(invoice);
						 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
						 audit.setNote(returnMessage);
						 audit.setCreatedDate(new Date());
						 audit.setCreateBy(userName);
						 DAORegistry.getInvoiceAuditDAO().save(audit);
					 }else{
						 returnMessage = "Real Ticket mapped, Error while sending Flash Seat Details emai to customer.";
						 InvoiceAudit audit = new InvoiceAudit(invoice);
						 audit.setAction(InvoiceAuditAction.REAL_TICKET_UPLOADED);
						 audit.setNote(returnMessage);
						 audit.setCreatedDate(new Date());
						 audit.setCreateBy(userName);
						 DAORegistry.getInvoiceAuditDAO().save(audit);
					 }
					 
					 invoice.setIsRealTixUploaded("Yes");
					 if(!order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
						 invoice.setStatus(InvoiceStatus.Completed);
					 }
					 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 }
				 status = true;
				 jObj.put("msg", returnMessage);
				 jObj.put("status", 1);
				 
				//Tracking User Action
				String userActionMsg = returnMessage;
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
				
			  }else if(action != null && action.equals("clearMapping")){
				  Integer invId = 0;
				  String poNoStr = "";
				  String fileNamesStr = "";
				  try{
					  invId = Integer.parseInt(invoiceId);
				  }catch(Exception e){
					  e.printStackTrace();
					  jObj.put("msg", "bad value found for Invoice ID, It should be valid Integer value.");
					  IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					  return;
				  }
				  invoice =  DAORegistry.getInvoiceDAO().get(invId);
				  if(invoice == null){
					  jObj.put("msg", "Invoice Details not found in system.");
					  IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					  return;
				  }
				  
				  List<InvoiceTicketAttachment> invoiceAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
				  if(invoiceAttachments!=null && !invoiceAttachments.isEmpty()){
					 File file = null;
					 for(InvoiceTicketAttachment attachment:invoiceAttachments){
						 file = new File(attachment.getFilePath());
						 if(file.exists()){
							 fileNamesStr = fileNamesStr + attachment.getFilePath()+",";
							 file.delete();
						 }
					 }
					 DAORegistry.getInvoiceTicketAttachmentDAO().deleteAll(invoiceAttachments);
				  }
				
				 invoice.setLastUpdated(new Date());
				 if(expDeliveryDate!=null && !expDeliveryDate.isEmpty()){
					 invoice.setExpDeliveryDate(format.parse(expDeliveryDate));
				 }
				 invoice.setStatus(InvoiceStatus.Outstanding);
				 invoice.setIsRealTixUploaded("No");
				 invoice.setRealTixMap("No");
				 invoice.setRealTixDelivered(false);
				 
				 CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				 order.setActualSection(null);
				 order.setActualSeat(null);
				 order.setRow(null);
				 
				 List<CategoryTicket> oldMappedCategoryTicket = DAORegistry.getCategoryTicketDAO().getMappedCategoryTicketByInvoiceId(invId);
				 List<Ticket> oldMappedTicket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invId);
				 List<OrderTicketGroupDetails> oldOrderTicketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(order.getId());
				 
				 if(oldMappedCategoryTicket!=null){
					 for(CategoryTicket catTicket:oldMappedCategoryTicket) {
						catTicket.setTicketId(null);
					}
					 DAORegistry.getCategoryTicketDAO().updateAll(oldMappedCategoryTicket);
				 }
				 if(oldMappedTicket!=null){
					 for(Ticket ticket:oldMappedTicket) {
						 ticket.setInvoiceId(null);
						 ticket.setTicketStatus(TicketStatus.ACTIVE);
						 if(!poNoStr.contains(String.valueOf(ticket.getPoId()))){
							 poNoStr = poNoStr + ticket.getPoId()+",";
						 }
					}
					 DAORegistry.getTicketDAO().updateAll(oldMappedTicket);
				 }
				 
				 DAORegistry.getCustomerOrderDAO().update(order);
				 DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
				 DAORegistry.getOrderTicketGroupDetailsDAO().deleteAll(oldOrderTicketGroupDetails);
				 
				 if(poNoStr!=null && !poNoStr.isEmpty()){
					 InvoiceAudit audit = new InvoiceAudit(invoice);
					 audit.setAction(InvoiceAuditAction.REAL_TICKET_MAPPING_REMOVED);
					 audit.setNote("Real Tickets Mappings removed, PO No.:"+poNoStr);
					 audit.setCreatedDate(new Date());
					 audit.setCreateBy(userName);
					 DAORegistry.getInvoiceAuditDAO().save(audit);
				 }
				 if(!invoiceAttachments.isEmpty()){
					 InvoiceAudit uploadAudit = new InvoiceAudit(invoice);
					 uploadAudit.setAction(InvoiceAuditAction.REAL_TICKET_REMOVED);
					 uploadAudit.setNote("Real tickets files are removed, Files are :"+fileNamesStr);
					 uploadAudit.setCreatedDate(new Date());
					 uploadAudit.setCreateBy(userName);
					 DAORegistry.getInvoiceAuditDAO().save(uploadAudit);
				 }
				 returnMessage = "Real Ticket UnMapped and all uploaded files removed successfully.";
				 status = true;
				 jObj.put("msg", returnMessage);
				 jObj.put("status", 1);
				 
				//Tracking User Action
				String userActionMsg = returnMessage;
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
				
			  }else if(action != null && action.equals("voidOpenOrder")){
					
					  try{
						  Integer invId = Integer.parseInt(invoiceId);
					  }catch(Exception e){
						  e.printStackTrace();
						  jObj.put("msg", "Bad value found for Invoice ID, It should be valid Integer value.");
						  IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						  return;
					  }
					  invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceId));
					  if(invoice == null){
						  jObj.put("msg", "Invoice Details not found in system.");
						  IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						  return;
					  }
					
					if (!invoice.getStatus().equals(InvoiceStatus.Voided)) {
						String refundTypes = request.getParameter("refundType");
						String refundAmountStr = request.getParameter("refundAmount");
						String walletCCAmtStr = request.getParameter("walletCCAmount");
						String walletAmtStr = request.getParameter("walletAmount");
						String rewardPointsStr = request.getParameter("rewardPoint");
						Double refndAmount=0.00;
						Double creditWalletAmount=0.00;
						Double creditAmount=0.00;
						Double rewardPoints=0.00;
						Double primaryAmountToDeduct=0.0;
						Double secondaryAmoutToDeduct=0.0;
						Double thirdAmountToDeduct=0.0;
						
						if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
							refndAmount = Double.parseDouble(refundAmountStr);
						}
						if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
							creditWalletAmount = Double.parseDouble(walletCCAmtStr);
						}
						if(walletAmtStr!=null && !walletAmtStr.isEmpty()){
							creditAmount = Double.parseDouble(walletAmtStr);
						}
						if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
							rewardPoints = Double.parseDouble(rewardPointsStr);
						}
						CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
						if(order == null){
							 jObj.put("msg", "Customer order details not found in system.");
							 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
							 return;
						}
						Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
						CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());						
						CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(order.getId());
						InvoiceRefund invoiceRefund=null;
						if(refundTypes.equalsIgnoreCase("NOREFUND")){
							Util.voidInvoice(invoice, order, null);
							returnMessage = "Invoice Voided Successfully.";
							//status = true;
							jObj.put("msg", returnMessage);
							jObj.put("status", 1);
							
							//Tracking User Action
							String userActionMsg = returnMessage;
							Util.userActionAudit(request, invoice.getId(), userActionMsg);
						}else if(refundTypes.equalsIgnoreCase("PARTIALREFUND")){
							if(order != null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)
									|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) || order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY))){
								if((refndAmount+creditWalletAmount) <= invoice.getInvoiceTotal() && 
										(refndAmount+creditWalletAmount) > 0){
									
									if(refndAmount > 0){
										String refundType = "";
										if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) 
												|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)){
											refundType = PaymentMethod.CREDITCARD.toString();
										}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
											refundType = PaymentMethod.PAYPAL.toString();
										}

										Map<String, String> paramMap = Util.getParameterMap(request);
										paramMap.put("orderId", String.valueOf(order.getId()));
										paramMap.put("refundType", refundType);
										paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
										
										String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
										Gson gson = Util.getGsonBuilder().create();	
										JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
										InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
										if(invoiceRefundResponse == null){
											jObj.put("msg", "Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
											IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
											return;
										}else if(invoiceRefundResponse.getStatus()==1){
											invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
											if(invoiceRefund != null){
												primaryAmountToDeduct = primaryAmountToDeduct + refndAmount;
											}
										}else{
											returnMessage = invoiceRefundResponse.getError().getDescription();
										}
									}
									if(creditWalletAmount > 0){
										Map<String, String> paramMap = Util.getParameterMap(request);
										paramMap.put("orderId", String.valueOf(order.getId()));
										paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
										paramMap.put("customerId",String.valueOf(customer.getId()));
										paramMap.put("transactionType","CREDIT");
										paramMap.put("userName",userName);
										
										String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
										Gson gson = Util.getGsonBuilder().create();	
										JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
										WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
										if(walletTransaction == null){
											jObj.put("msg", "Wallet Transaction API not available,  it happens mostly when rewardthefan API is down.");
											IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
											return;
										}else if(walletTransaction.getStatus() == 1){
											primaryAmountToDeduct = primaryAmountToDeduct + creditWalletAmount;
										}else{
											returnMessage = walletTransaction.getError().getDescription();
										}
									}
								}							
							}
							else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)){
								if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
									if((refndAmount+creditWalletAmount+creditAmount) <= invoice.getInvoiceTotal() && 
											(refndAmount+creditWalletAmount+creditAmount) > 0){
										
										if(refndAmount > 0){
											String refundType = "";
											if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
												refundType = PaymentMethod.CREDITCARD.toString();
											}

											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("refundType", refundType);
											paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
											
											String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
											if(invoiceRefundResponse == null){
												jObj.put("msg", "Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(invoiceRefundResponse.getStatus()==1){
												invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
												if(invoiceRefund != null){
													secondaryAmoutToDeduct = secondaryAmoutToDeduct + refndAmount;
												}
											}else{
												returnMessage = invoiceRefundResponse.getError().getDescription();
											}
										}
										if(creditWalletAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											if(walletTransaction == null){
												jObj.put("msg", "Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
										if(creditAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											if(walletTransaction == null){
												jObj.put("msg", "Wallet Transaction API not available,  it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(walletTransaction.getStatus() == 1){
												primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
								else{
									if(creditAmount > 0){
										Map<String, String> paramMap = Util.getParameterMap(request);
										paramMap.put("orderId", String.valueOf(order.getId()));
										paramMap.put("transactionAmount",String.valueOf(creditAmount));
										paramMap.put("customerId",String.valueOf(customer.getId()));
										paramMap.put("transactionType","CREDIT");
										paramMap.put("userName",userName);
										
										String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
										Gson gson = Util.getGsonBuilder().create();	
										JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
										WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
										if(walletTransaction == null){
											jObj.put("msg", "Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
											IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
											return;
										}else if(walletTransaction.getStatus() == 1){
											primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
										}else{
											returnMessage = walletTransaction.getError().getDescription();
										}
									}
								}
							}
							else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
								if(rewardPoints <= invoice.getInvoiceTotal() && rewardPoints > 0){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
//									if(customerLoyaltyHistory!=null){
//										customerLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
//										DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
//									}
									DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
							}
							else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS)){
								if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
									if((refndAmount+creditWalletAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
											(refndAmount+creditWalletAmount+rewardPoints) > 0){

										if(rewardPoints <= invoice.getInvoiceTotal()){
											customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
											customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
											if(invoiceRefund!=null){
												invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
											}
											primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
										}
										if(refndAmount > 0){
											String refundType = "";
											if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
												refundType = PaymentMethod.CREDITCARD.toString();
											}
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("refundType", refundType);
											paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
											
											String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
											if(invoiceRefundResponse == null){
												jObj.put("msg", "Invoice Refund API not available,  it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(invoiceRefundResponse.getStatus()==1){
												invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
												if(invoiceRefund != null){
													secondaryAmoutToDeduct = secondaryAmoutToDeduct + refndAmount;
												}
											}else{
												returnMessage = invoiceRefundResponse.getError().getDescription();
											}
										}
										if(creditWalletAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											if(walletTransaction == null){
												jObj.put("msg", "Wallet Transaction API not available,  it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
								else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET) && 
										(order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
												|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
												|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY))){
									if((refndAmount+creditWalletAmount+creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
											(refndAmount+creditWalletAmount+creditAmount+rewardPoints) > 0){

										if(rewardPoints <= invoice.getInvoiceTotal()){
											customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
											customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
											if(invoiceRefund!=null){
												invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
											}
											primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
										}
										if(creditAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											if(walletTransaction == null){
												jObj.put("msg", "Wallet Transaction API not available,  it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
										if(refndAmount > 0){
											String refundType = "";
											if(order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
													|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
												refundType = PaymentMethod.CREDITCARD.toString();
											}

											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("refundType", refundType);
											paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refndAmount)));
											
											String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
											if(invoiceRefundResponse == null){
												jObj.put("msg", "Invoice Refund API not available,  it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(invoiceRefundResponse.getStatus()==1){
												invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
												if(invoiceRefund != null){
													thirdAmountToDeduct = thirdAmountToDeduct + refndAmount;
												}
											}else{
												returnMessage = invoiceRefundResponse.getError().getDescription();
											}
										}
										if(creditWalletAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											if(walletTransaction == null){
												jObj.put("msg", "Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(walletTransaction.getStatus() == 1){
												thirdAmountToDeduct = thirdAmountToDeduct + creditWalletAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
								else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
									if((creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
											(creditAmount+rewardPoints) > 0){
										
										if(rewardPoints <= invoice.getInvoiceTotal()){
											customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
											customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
											if(invoiceRefund!=null){
												invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
											}
											primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
										}
										if(creditAmount > 0){
											Map<String, String> paramMap = Util.getParameterMap(request);
											paramMap.put("orderId", String.valueOf(order.getId()));
											paramMap.put("transactionAmount",String.valueOf(creditAmount));
											paramMap.put("customerId",String.valueOf(customer.getId()));
											paramMap.put("transactionType","CREDIT");
											paramMap.put("userName",userName);
											
											String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
											Gson gson = Util.getGsonBuilder().create();	
											JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
											WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
											if(walletTransaction == null){
												jObj.put("msg", "Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
												IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
												return;
											}else if(walletTransaction.getStatus() == 1){
												secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
											}else{
												returnMessage = walletTransaction.getError().getDescription();
											}
										}
									}
								}
							}
							if(returnMessage==null || returnMessage.isEmpty()){
								Date today = new Date();
								order.setPrimaryRefundAmount(order.getPrimaryRefundAmount()!=null?order.getPrimaryRefundAmount()+primaryAmountToDeduct:primaryAmountToDeduct);
								order.setSecondaryRefundAmount(order.getSecondaryRefundAmount()!=null?order.getSecondaryRefundAmount()+secondaryAmoutToDeduct:secondaryAmoutToDeduct);
								order.setThirdRefundAmount(order.getThirdRefundAmount()!=null?order.getThirdRefundAmount()+thirdAmountToDeduct:thirdAmountToDeduct);
//								order.setOrderTotal(order.getOrderTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));							
//								
//								if(order.getEventDate().compareTo(today) >= 0){
//									customerLoyalty.setPendingPoints((customerLoyalty.getPendingPoints()-((primaryAmountToDeduct+secondaryAmoutToDeduct+thirdAmountToDeduct)%10)));
//								}else{
//									customerLoyalty.setActivePoints((customerLoyalty.getActivePoints()-((primaryAmountToDeduct+secondaryAmoutToDeduct+thirdAmountToDeduct)%10)));
//								}
							
								invoice.setInvoiceTotal(invoice.getInvoiceTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
								invoice.setLastUpdated(today);
								invoice.setLastUpdatedBy(userName);
								invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct)
										:(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
//								if(invoiceRefund!=null){
//									invoiceRefund.setOrderId(order.getId());
//									DAORegistry.getInvoiceRefundDAO().save(invoiceRefund);
//								}
								DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
								DAORegistry.getCustomerOrderDAO().update(order);
								DAORegistry.getInvoiceDAO().update(invoice);
								
								Util.voidInvoice(invoice, order, invoiceRefund);
								returnMessage = "Invoice Voided Successfully.";
								//status = true;
								jObj.put("msg", returnMessage);
								jObj.put("status", 1);
								
								//Tracking User Action
								String userActionMsg = returnMessage;
								Util.userActionAudit(request, invoice.getId(), userActionMsg);
							}else{
								jObj.put("msg", returnMessage);
								IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
								return;
							}
//							String logMsg= "";							
//							if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
//								logMsg =logMsg +"Refunded($"+refundAmountStr+"),";
//							}
//							if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
//								logMsg =logMsg +"Credited($"+walletCCAmtStr+"),";
//							}
//							if(walletAmtStr!=null && !walletAmtStr.isEmpty()){
//								logMsg =logMsg +"Credited($"+walletAmtStr+"),";	
//							}						
//							if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
//								logMsg =logMsg +"Reverted Reward Points($"+rewardPointsStr+")";
//							}
//							
//							InvoiceAudit audit = new InvoiceAudit(invoice);
//							audit.setAction(InvoiceAuditAction.INVOICE_REFUNDED);
//							audit.setNote("Invoice is "+logMsg);
//							audit.setCreatedDate(new Date());
//							audit.setCreateBy(userName);
//							DAORegistry.getInvoiceAuditDAO().save(audit);
							
						}
						else if(refundTypes.equalsIgnoreCase("FULLREFUND")){
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY)
									|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
									|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)
									|| (order.getThirdPaymentMethod() != null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											||order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))))){
								Double refundAmount = 0.0;
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY) 
										|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY)){
									primaryAmountToDeduct = order.getPrimaryAvailableAmt();
									refundAmount = order.getPrimaryAvailableAmt();
								}else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY) 
										|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)){
									secondaryAmoutToDeduct = order.getSecondaryAvailableAmt();
									refundAmount = order.getSecondaryAvailableAmt();
								}else if(order.getThirdPaymentMethod() != null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY))){
									thirdAmountToDeduct = order.getThirdAvailableAmt();
									refundAmount = order.getThirdAvailableAmt();
								}
								if(refundAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", PaymentMethod.CREDITCARD.toString());
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl() + Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject) jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									if(invoiceRefundResponse == null){
										jObj.put("msg", "Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if (invoiceRefundResponse.getStatus() == 1) {
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
									} else {
										returnMessage = invoiceRefundResponse.getError().getDescription();
									}
								}
								
							}
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) 
									|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
									|| (order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL)))){
								Double refundAmount = 0.0;
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
									primaryAmountToDeduct = order.getPrimaryAvailableAmt();
									refundAmount = order.getPrimaryAvailableAmt();
								}else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
									secondaryAmoutToDeduct = order.getSecondaryAvailableAmt();
									refundAmount = order.getSecondaryAvailableAmt();
								}else if(order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
									thirdAmountToDeduct = order.getThirdAvailableAmt();
									refundAmount = order.getThirdAvailableAmt();
								}
								if(refundAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", PaymentMethod.PAYPAL.toString());
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl() + Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject) jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									if(invoiceRefundResponse == null){
										jObj.put("msg", "Invoice Refund API not available, it happens mostly when rewardthefan API is down.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if (invoiceRefundResponse.getStatus() == 1) {
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
									} else {
										returnMessage = invoiceRefundResponse.getError().getDescription();
									}
								}
								
							}
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET) 
									|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)
									|| (order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)))){
								Double refundAmount = 0.00;
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)){
									primaryAmountToDeduct = order.getPrimaryAvailableAmt();
									refundAmount = order.getPrimaryAvailableAmt();
								}else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
									secondaryAmoutToDeduct = order.getSecondaryAvailableAmt();
									refundAmount = order.getSecondaryAvailableAmt();
								}else if(order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
									thirdAmountToDeduct = order.getThirdAvailableAmt();
									refundAmount = order.getThirdAvailableAmt();
								}
								if(refundAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(refundAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available, it happens mostly when rewardthefan API is down.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										//secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
									}else{
										returnMessage = walletTransaction.getError().getDescription();
									}
								}
								
							}
							if(order!=null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS) ||
									order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
								primaryAmountToDeduct = order.getPrimaryAvailableAmt();
								customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+primaryAmountToDeduct);
								customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + primaryAmountToDeduct);
								if(customerLoyaltyHistory!=null){
									customerLoyaltyHistory.setRewardStatus(RewardStatus.VOIDED);
									DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
								}
								DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
								
							}
							if(returnMessage==null || returnMessage.isEmpty()){
								Date today = new Date();
								order.setPrimaryRefundAmount(order.getPrimaryRefundAmount() + primaryAmountToDeduct);
								order.setSecondaryRefundAmount(order.getSecondaryRefundAmount() + secondaryAmoutToDeduct);
								order.setThirdRefundAmount(order.getThirdRefundAmount() + thirdAmountToDeduct);							
								
								invoice.setInvoiceTotal(invoice.getInvoiceTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
								invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct)
										:(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
								invoice.setLastUpdated(today);
								invoice.setLastUpdatedBy(userName);
								
								DAORegistry.getCustomerOrderDAO().update(order);
								DAORegistry.getInvoiceDAO().update(invoice);
								Util.voidInvoice(invoice, order, invoiceRefund);
								returnMessage = "Invoice Voided Successfully.";
								//status = true;
								jObj.put("msg", returnMessage);
								jObj.put("status", 1);
								
								//Tracking User Action
								String userActionMsg = returnMessage;
								Util.userActionAudit(request, invoice.getId(), userActionMsg);
								
							}else{
								jObj.put("msg", returnMessage);
								IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
								return;
							}
						}
					} else {
						returnMessage = "Invoice already voided.";
					}	
					jObj.put("msg", returnMessage);
			  }
			}
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
		//return "redirect:CreateRealTix?invoiceId="+invoiceId+"&msg="+returnMessage;
		//return "redirect:AddRealTicket?invoiceId="+invoiceId+"&orderId="+invoice.getCustomerOrderId()+"&msg="+returnMessage+"&status="+status;
	}
	

	/**
	 * Upload ticket
	 * @param uploadedFile
	 * @param result
	 * @return
	 * @throws Exception 
	 */
	/*@RequestMapping(value = "/ticketUpload")
	public String ticketUpload(@ModelAttribute("uploadedFile")UploadedFile uploadedFile, 
			BindingResult result, ModelMap map, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		String action = "";
		String invoiceId = request.getParameter("invoiceId");
		String barCode = "";
		String returnMessage = "";
		
		InputStream inputStream = null;
		OutputStream outputStream = null;
		//File upload starts
		

		try {
			Collection<OpenOrders> openOrdersList = DAORegistry.getQueryManagerDAO().getOpenOrderList(null,null,null,null,null,null,null);
			if(openOrdersList.size() > 0){
				map.put("openOrders", openOrdersList);
			}else{
				returnMessage = "No Open Orders found";
				map.put("message",returnMessage);
			}
			action = request.getParameter("action");
			if(action != null && action.equals("uploadTicket")){
				MultipartFile file = uploadedFile.getFile();
				fileValidator.validate(uploadedFile, result);

				String fileName = file.getOriginalFilename();

				if (result.hasErrors()) {
					return "page-open-order";
				}
				
				inputStream = file.getInputStream();
				File newFile = new File("C:/Dhivakar/Tomcat7-DevRTW/tixUploads/" + fileName);
	
				if (!newFile.exists()) {
					newFile.createNewFile();
				}
				outputStream = new FileOutputStream(newFile);
				int read = 0;
				byte[] bytes = new byte[1024];
	
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
				
				Integer invId = Integer.parseInt(invoiceId);
				DAORegistry.getInvoiceDAO().updateTixUploaded(invId);
				
				map.put("message", fileName);
			}else if(action != null && action.equals("saveBarCode")){
				barCode = request.getParameter("barCode");
				String id = request.getParameter("invoiceId");
				Integer invId = Integer.parseInt(id);
				DAORegistry.getInvoiceDAO().updateBarCode(barCode, invId);
				
				map.put("message", "Barcode ");
			}
			map.put("openOrders", openOrdersList);
		} catch (IOException e) {
			e.printStackTrace();
			map.put("error", "There is something wrong.Please try again..");
		}
		return "page-open-order";
	}*/
	
	
	/**
	 * Delete Fedex Label for given invoice.
	 * @param request
	 * @param response
	 * @return
	 */
	
	@RequestMapping(value = "/RemoveFedexLabel")
	public String removeFedexLabel(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String invoiceId = request.getParameter("invoiceId");
			String poId = request.getParameter("poId");
			String fedexId = request.getParameter("fedexId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("poId", poId);
			map.put("fedexId", fedexId);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_REMOVE_FEDEX_LABEL);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			/*String fileWithPath=null;
			String msg="";
			JSONObject object = new JSONObject();
			object.put("status", 0);
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				CreateFedexLabelUtil util = new CreateFedexLabelUtil();
				Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceIdStr));
				if(invoice == null){
					object.put("msg", "Invoice Details not found in system.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				else if(invoice.getTrackingNo() == null || invoice.getTrackingNo().isEmpty()){
					object.put("msg", "Invoice - Fedex Tracking No. not found.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}				
				else{
					msg = util.deleteShipment(invoice);
					object.put("msg", msg) ;
					object.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Fedex Label is Removed from Invoice.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
				}
			}
			if(poIdStr != null && !poIdStr.isEmpty()){
				CreateFedexLabelUtil util = new CreateFedexLabelUtil();
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
				if(purchaseOrder == null){
					object.put("msg", "Purchase Order Details not found in system.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				else if(purchaseOrder.getTrackingNo() == null || purchaseOrder.getTrackingNo().isEmpty()){
					object.put("msg", "Purchase Order - Fedex Tracking No not found.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				else{
					msg = util.deletePOShipment(purchaseOrder);
					object.put("msg", msg) ;
					object.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Fedex Label is Removed from PO.";
					Util.userActionAudit(request, purchaseOrder.getId(), userActionMsg);
				}
			}
			if(manualFedexIdStr != null && !manualFedexIdStr.isEmpty()){
				CreateFedexLabelUtil util = new CreateFedexLabelUtil();
				ManualFedexGeneration manualFedex = DAORegistry.getManualFedexGenerationDAO().get(Integer.parseInt(manualFedexIdStr));
				if(manualFedex == null){
					object.put("msg", "Manual Fedex Details not found in system.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				else if(manualFedex.getTrackingNumber() == null || manualFedex.getTrackingNumber().isEmpty()){
					object.put("msg", "Manual - Fedex Tracking No not found.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				else{
					msg = util.deleteManualFedexShipment(manualFedex);
					object.put("msg", msg) ;
					object.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Fedex Label is Removed from Manual Fedex.";
					Util.userActionAudit(request, Integer.parseInt(manualFedexIdStr), userActionMsg);
				}
			}
			//return jsonArr
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	/**
	 * Create Fedex Label for given invoice.
	 * @param request
	 * @param response
	 * @return
	 */
	
	@RequestMapping(value = "/CreateFedexLabel")
	public String createFedexLabel(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String invoiceId = request.getParameter("invoiceId");
			String serviceType = request.getParameter("serviceType");
			String signatureType = request.getParameter("signatureType");
			String companyName = request.getParameter("companyName");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("serviceType", serviceType);
			map.put("signatureType", signatureType);
			map.put("companyName", companyName);
			map.put("userName", userName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_FEDEX_LABEL_FOR_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String trackingNo="";
			String msg="";
			SignatureOptionType signatureOptionType = null;
			Integer invoiceId = 0;
			JSONObject object = new JSONObject();
			object.put("status", 0);
			
			CreateFedexLabelUtil util = new CreateFedexLabelUtil();
			
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				object.put("msg", "Invoice ID not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				object.put("msg", "Invoice ID should be Intege value.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(serviceType == null || serviceType.isEmpty() || serviceType.equalsIgnoreCase("select")){
				object.put("msg", "Service Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(signatureType == null || signatureType.isEmpty()){
				object.put("msg", "Signature Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			signatureOptionType = SignatureOptionType.fromString(signatureType);
						
			List<Recipient> recipients=DAORegistry.getInvoiceDAO().getRecipientListByInvoice(invoiceId,true);
			if(recipients == null || recipients.isEmpty()){
				object.put("msg", "Could not create Fedex label, No recipient found for selected invoice.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			if(invoice == null){
				object.put("msg", "Invoice Details not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			 for(Recipient recipient : recipients){
				if(companyName != null && !companyName.isEmpty()){
					recipient.setCompanyName(companyName);
				}
				if(recipient.getCountryCode()==null){
					msg = "Could not create Fedex label, No country found for selected invoice.";
					object.put("msg", msg);
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}else if(recipient.getCountryCode().equalsIgnoreCase("US")){													
					ServiceType service =  ServiceType.fromValue(serviceType);
					msg = util.createFedExShipLabel(recipient,service,signatureOptionType);// package type = your package					
		    	}else{
		    		ServiceType service =  ServiceType.fromValue(serviceType);
		    		msg = util.createFedExShipLabel(recipient,service,signatureOptionType);
		    	}
			 }
			 
			 invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			 trackingNo = invoice.getTrackingNo();
			 if(msg==null){
				Customer cust = DAORegistry.getCustomerDAO().get(invoice.getCustomerId());
				CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				String eventDetails = order.getEventName().toUpperCase();
				String dateString = order.getEventDateStr()+" "+order.getEventTimeStr();
				String venueString = order.getVenueName()+", "+order.getVenueCity()+", "+order.getVenueState()+", "+order.getVenueCountry();
				com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
				mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",Util.getFilePath(request, "share.png", "/resources/images/"));
				mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","top.png",Util.getFilePath(request, "top.png", "/resources/images/"));
				mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","bottom.png",Util.getFilePath(request, "bottom.png", "/resources/images/"));
				mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",Util.getFilePath(request, "blue.png", "/resources/images/"));
				mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",Util.getFilePath(request, "fb1.png", "/resources/images/"));
				mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",Util.getFilePath(request, "ig1.png", "/resources/images/"));
				mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",Util.getFilePath(request, "li1.png", "/resources/images/"));
				mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogo1.png",Util.getFilePath(request, "rtflogo1.png", "/resources/images/"));
				mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",Util.getFilePath(request, "p1.png", "/resources/images/"));
				mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",Util.getFilePath(request, "tw1.png", "/resources/images/"));
				mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
				
				Map<String, Object> mailMap = new HashMap<String, Object>();
				mailMap.put("customerName",cust.getCustomerName()+" "+cust.getLastName());
				mailMap.put("trackingNo",trackingNo);
				mailMap.put("eventDetails", eventDetails);
				mailMap.put("dateString", dateString);
				mailMap.put("venueString", venueString);
				mailMap.put("orderId",order.getId());
				Boolean isMailSent = false;
				try {
					mailManager.sendMailNow("text/html","sales@rewardthefan.com",cust.getEmail(), 
							null,"msanghani@rightthisway.com","Reward The Fan: Ticket delivery information",
				    		"email-fedex-info.html", mailMap, "text/html", null,mailAttachment,null);
					isMailSent = true;
				} catch (Exception e) {
					isMailSent = false;
					e.printStackTrace();
				}
				
				
				 InvoiceAudit audit = new InvoiceAudit(invoice);
				 audit.setAction(InvoiceAuditAction.FEDEX_LABEL);
				 if(isMailSent){
					 msg = "Fedex Label is created with tracking no :"+trackingNo+", Notification mail sent to customer.";
				 }else{
					 msg = "Fedex Label is created with tracking no :"+trackingNo+", Error while sending Notification mail to customer.";
				 }
				 audit.setNote(msg);
				 audit.setCreatedDate(new Date());
				 audit.setCreateBy(userName);
				 DAORegistry.getInvoiceAuditDAO().save(audit);
				 object.put("status", 1);
				 
				//Tracking User Action
				String userActionMsg = "Fedex Label is Created for Invoice. Tracking no - "+trackingNo;
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			 }
			
			object.put("msg", msg) ;
			
			//return jsonArr
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			System.out.println("FedEx: "+e);
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	/**
	 * Create Fedex Label for given PO.
	 * @param request
	 * @param response
	 * @return
	 */	
	@RequestMapping(value = "/CreateFedexLabelForPO")
	public String createFedexLabelForPO(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String poId = request.getParameter("poId");
			String shippingId = request.getParameter("shippingId");
			String serviceType = request.getParameter("serviceType");
			String signatureType = request.getParameter("signatureType");
			String companyName = request.getParameter("companyName");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("poId", poId);
			map.put("shippingId", shippingId);
			map.put("serviceType", serviceType);
			map.put("signatureType", signatureType);
			map.put("companyName", companyName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_FEDEX_LABEL_FOR_PO);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			/*String trackingNo="";
			String msg="";
			Integer poId = 0;
			SignatureOptionType signatureOptionType = null;
			JSONObject object = new JSONObject();
			object.put("status", 0);
			CreateFedexLabelUtil util = new CreateFedexLabelUtil();
			
			if(poIdStr == null || poIdStr.isEmpty()){
				object.put("msg", "Purchase Order ID not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				object.put("msg", "Invoice ID should be Intege value.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(serviceType == null || serviceType.isEmpty() || serviceType.equalsIgnoreCase("select")){
				object.put("msg", "Service Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(signatureType == null || signatureType.isEmpty()){
				object.put("msg", "Signature Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			signatureOptionType = SignatureOptionType.fromValue(signatureType);
			
			List<Recipient> recipients=DAORegistry.getQueryManagerDAO().getRecipientListByPO(Integer.parseInt(shippingId), Integer.parseInt(poIdStr));
			if(recipients == null || recipients.isEmpty()){
				object.put("msg", "Could not create Fedex label, No recipient found for selected invoice.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			
			 for(Recipient recipient:recipients){
				if(companyName != null && !companyName.isEmpty()){
					recipient.setCompanyName(companyName);
				}
				if(recipient.getCountryCode()==null){
					msg = "Could not create Fedex label, No country found for selected PO.";
					object.put("msg", msg);
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}else if(recipient.getCountryCode().equalsIgnoreCase("US")){
					ServiceType service =  ServiceType.fromValue(serviceType);
					msg = util.createFedExShipLabel(recipient,service,signatureOptionType);// package type = your package						
		    	}else{
		    		ServiceType service =  ServiceType.fromValue(serviceType);
		    		msg = util.createFedExShipLabel(recipient,service,signatureOptionType);
		    	}
			 }
			 trackingNo = DAORegistry.getPurchaseOrderDAO().get(poId).getTrackingNo();
			 if(msg == null){
				 msg = "Fedex Label Created for selected PO with tracking no : "+trackingNo;
				 object.put("status", 1);
				 
				//Tracking User Action
				String userActionMsg = "Fedex Label is Created for PO. Tracking no - "+trackingNo;
				Util.userActionAudit(request, poId, userActionMsg);
			 }
			
			object.put("msg", msg);
			
			//return jsonArr
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			System.out.println("FedEx: "+e);
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	@RequestMapping(value = "/CheckFedexLabelGenerated")
	public String checkFedexLabelGenerated(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String invoiceId = request.getParameter("invoiceId");
			String poId = request.getParameter("poId");
			String id = request.getParameter("id");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("poId", poId);
			map.put("id", id);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_FEDEX_LABEL_GENERATED);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			/*String fileWithPath=null;
			String msg="";
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceIdStr));
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				if(invoice.getTrackingNo()!=null && !invoice.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+invoice.getId()+"_"+invoice.getTrackingNo();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							msg="OK";
						}
					}
				}
			}
			if(poIdStr!=null && !poIdStr.isEmpty()){
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				if(purchaseOrder.getTrackingNo()!=null && !purchaseOrder.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+purchaseOrder.getId()+"_"+purchaseOrder.getTrackingNo();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							msg="OK";
						}
					}
				}
			}
			if(manualFedexIdStr!=null && !manualFedexIdStr.isEmpty()){
				ManualFedexGeneration manualFedex = DAORegistry.getManualFedexGenerationDAO().get(Integer.parseInt(manualFedexIdStr));
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
				if(manualFedex.getTrackingNumber()!=null && !manualFedex.getTrackingNumber().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						//fileWithPath = property.getValue()+File.separator+manualFedex.getId()+"_"+manualFedex.getTrackingNumber();
						fileWithPath = property.getValue()+File.separator+"1_"+manualFedex.getTrackingNumber();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							msg="OK";
						}
					}
				}
			}
			IOUtils.write(msg.getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	/**
	 * Get Fedex Label Pdf for given invoice/PO.
	 * @param request
	 * @param response
	 * @return
	 */	
	/*@RequestMapping(value = "/GetFedexLabelPdf")
	public void getFedexLabelPdf(HttpServletRequest request, HttpServletResponse response){
		String invoiceIdStr = request.getParameter("invoiceId");
		String poIdStr = request.getParameter("poId");
		String manualFedexIdStr = request.getParameter("id");
		String fileWithPath=null;
		try {
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				Invoice invoice = DAORegistry.getInvoiceDAO().get(Integer.parseInt(invoiceIdStr));
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				if(invoice.getTrackingNo()!=null && !invoice.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+invoice.getId()+"_"+invoice.getTrackingNo();						
						OutputStream out = response.getOutputStream();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							FileInputStream in = new FileInputStream(file);
							response.setContentType("application/octet-stream");
							response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
							response.setHeader("Content-Length",String.valueOf(file.length()));
							byte[] buffer = new byte[4096];
							int length;
							while((length = in.read(buffer)) > 0){
							    out.write(buffer, 0, length);
							}
							in.close();
							out.flush();
						}
					}
				}
			}
			if(poIdStr!=null && !poIdStr.isEmpty()){
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(poIdStr));
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				if(purchaseOrder.getTrackingNo()!=null && !purchaseOrder.getTrackingNo().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						fileWithPath = property.getValue()+File.separator+purchaseOrder.getId()+"_"+purchaseOrder.getTrackingNo();											
						OutputStream out = response.getOutputStream();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							FileInputStream in = new FileInputStream(file);
							response.setContentType("application/octet-stream");
							response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
							
							byte[] buffer = new byte[4096];
							int length;
							while((length = in.read(buffer)) > 0){
							    out.write(buffer, 0, length);
							}
							in.close();
							out.flush();
						}
					}
				}
			}
			if(manualFedexIdStr!=null && !manualFedexIdStr.isEmpty()){
				ManualFedexGeneration manualFedex = DAORegistry.getManualFedexGenerationDAO().get(Integer.parseInt(manualFedexIdStr));
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
				if(manualFedex.getTrackingNumber()!=null && !manualFedex.getTrackingNumber().isEmpty()){
					if(property!=null && property.getValue()!=null && !property.getValue().isEmpty()){
						//fileWithPath = property.getValue()+File.separator+manualFedex.getId()+"_"+manualFedex.getTrackingNumber();
						fileWithPath = property.getValue()+File.separator+"1_"+manualFedex.getTrackingNumber();
						OutputStream out = response.getOutputStream();
						File file = new File(fileWithPath+".pdf");
						if(file.exists()){
							FileInputStream in = new FileInputStream(file);
							response.setContentType("application/octet-stream");
							response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
							response.setHeader("Content-Length",String.valueOf(file.length()));
							byte[] buffer = new byte[4096];
							int length;
							while((length = in.read(buffer)) > 0){
							    out.write(buffer, 0, length);
							}
							in.close();
							out.flush();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Pdf for given invoice.
	 * @param request
	 * @param response
	 * @return
	 */	
	/*@RequestMapping(value = "/GetInvoicePdf")
	public void getInvoicePdf(HttpServletRequest request, HttpServletResponse response){
		String invoiceIdStr = request.getParameter("invoiceId");
		String downloadView = request.getParameter("inline");
		try {
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				//Getting Broker ID
				HttpSession session = request.getSession(true);
				Integer brokerId = Util.getBrokerId(session);
				Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceIdStr), brokerId);
				if(invoice != null){
					//List<Ticket> ticket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceIdStr));
					CategoryTicketGroup categoryTicketGroup = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByInvoiceId(invoice.getId());
					CustomerOrderDetails orderDetail = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
					//EventDetails event = DAORegistry.getEventDetailsDAO().get(categoryTicketGroup.getEventId());
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
					List<OrderTicketGroupDetails> ticketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(invoice.getCustomerOrderId());
					PDFUtil.generateInvoicePdf(invoice, categoryTicketGroup, ticketGroupDetails, orderDetail, order);
					File file = new File(DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\invoice_"+invoice.getId()+".pdf");
					if(!file.exists()){
						file.createNewFile();
					}
					OutputStream out = response.getOutputStream();
					FileInputStream in = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", String.format(downloadView+"; filename=\"%s\"",file.getName()));
					
					byte[] buffer = new byte[4096];
					int length;
					while((length = in.read(buffer)) > 0){
					    out.write(buffer, 0, length);
					}
					in.close();
					out.flush();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Pdf for given RTW/RTW2 invoice.
	 * @param request
	 * @param response
	 * @return
	 */	
	/*@RequestMapping(value = "/GetRTWRTW2InvoicePdf")
	public void getRTWRTW2InvoicePdf(HttpServletRequest request, HttpServletResponse response){
		String invoiceIdStr = request.getParameter("invoiceId");
		String downloadView = request.getParameter("inline");
		String productType = request.getParameter("productType");
		Collection<OpenOrders> openOrdersList = null;
		try {
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				openOrdersList = DAORegistry.getQueryManagerDAO().getRTWOpenOrderListByInvoiceID(invoiceIdStr,productType);				
				List<POSTicket> tickets = DAORegistry.getPosTicketDAO().getMappedPOSTicketByInvoiceIdWithPdt(Integer.parseInt(invoiceIdStr), productType);
				List<POSCategoryTicket> categoryTickets = DAORegistry.getPosCategoryTicketDAO().getPOSCategoryTicketByInvoiceId(Integer.parseInt(invoiceIdStr),productType);
				Map<Integer, List<POSTicketGroup>> ticketMap = new HashMap<Integer, List<POSTicketGroup>>();
				
				if(tickets!=null && !tickets.isEmpty()){
					List<POSTicketGroup> list = null;
					List<Integer> ticketGroupIds = new ArrayList<Integer>();
					Map<Integer,Integer> ticCountMap = new HashMap<Integer, Integer>();
					Map<Integer,Double> ticPriceMap = new HashMap<Integer, Double>();
					Map<Integer,Integer> ticPoMap = new HashMap<Integer, Integer>();
					Map<Integer,String> ticOnHandMap = new HashMap<Integer, String>();
					Map<Integer,String> ticSeatMap = new HashMap<Integer, String>();
					for(POSTicket ticket: tickets){
						if(ticCountMap.get(ticket.getTicketGroupId())!=null){
							ticCountMap.put(ticket.getTicketGroupId(), ticCountMap.get(ticket.getTicketGroupId())+1);
						}else{
							ticCountMap.put(ticket.getTicketGroupId(), 1);
						}
						ticPriceMap.put(ticket.getTicketGroupId(),ticket.getActualSoldPrice());
						ticPoMap.put(ticket.getTicketGroupId(),ticket.getPurchaseOrderId());
						ticOnHandMap.put(ticket.getTicketGroupId(),((ticket.getTicketOnHandStatusId()!=null&&ticket.getTicketOnHandStatusId()==1)?"Y":"N"));
						if(ticSeatMap.get(ticket.getTicketGroupId())!=null){
							String seatNo = ticSeatMap.get(ticket.getTicketGroupId());
							if(seatNo.contains("-")){
								seatNo = seatNo.split("-")[0];
							}
							seatNo =seatNo + "-"+ticket.getSeatNumber();
							ticSeatMap.put(ticket.getTicketGroupId(), ticSeatMap.get(ticket.getTicketGroupId())+"-"+ticket.getSeatNumber());
						}else{
							ticSeatMap.put(ticket.getTicketGroupId(), ticket.getSeatNumber());
						}
					}
					for(POSTicket ticket: tickets){
						if(ticketGroupIds.contains(ticket.getTicketGroupId())){
							continue;
						}
						POSTicketGroup ticketGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupId(ticket.getTicketGroupId(),productType);
						ticketGroup.setMappedQty(ticCountMap.get(ticket.getTicketGroupId()));
						ticketGroup.setActualSoldPrice(ticPriceMap.get(ticket.getTicketGroupId()));
						ticketGroup.setPurchaseOrderId(ticPoMap.get(ticket.getTicketGroupId()));
						ticketGroup.setOnHand(ticOnHandMap.get(ticket.getTicketGroupId()));
						ticketGroup.setSeatNoString(ticSeatMap.get(ticket.getTicketGroupId()));
						if(ticketMap.get(ticketGroup.getEventId())!=null){
							ticketMap.get(ticketGroup.getEventId()).add(ticketGroup);
						}else{
							list = new ArrayList<POSTicketGroup>();
							list.add(ticketGroup);
							ticketMap.put(ticketGroup.getEventId(), list);
						}
						ticketGroupIds.add(ticketGroup.getTicketGroupId());
						
						
					}
				}
				Map<Integer, List<POSCategoryTicketGroup>> categoryTicketMap = new HashMap<Integer, List<POSCategoryTicketGroup>>();
				if(categoryTickets!=null && !categoryTickets.isEmpty()){
					List<POSCategoryTicketGroup> catTicketGroups = null;
					List<Integer> catTicketGroupIds = new ArrayList<Integer>();
					Map<Integer,Double> ticPriceMap = new HashMap<Integer, Double>();
					for(POSCategoryTicket ticket: categoryTickets){
						ticPriceMap.put(ticket.getCategoryTicketGroupId(),ticket.getActualPrice());
					}
					for(POSCategoryTicket ticket: categoryTickets){
						if(catTicketGroupIds.contains(ticket.getCategoryTicketGroupId())){
							continue;
						}
						POSCategoryTicketGroup categoryTicketGroup = DAORegistry.getPosCategoryTicketGroupDAO().getPOSCategoryTicketGroupByCategoryTicketGroupId(ticket.getCategoryTicketGroupId(), productType);
						categoryTicketGroup.setActualPrice(ticPriceMap.get(categoryTicketGroup.getCategoryTicketGroupId()));
						if(categoryTicketMap.get(categoryTicketGroup.getEventId())!=null){
							categoryTicketMap.get(categoryTicketGroup.getEventId()).add(categoryTicketGroup);
						}else{
							catTicketGroups = new ArrayList<POSCategoryTicketGroup>();
							catTicketGroups.add(categoryTicketGroup);
							categoryTicketMap.put(categoryTicketGroup.getEventId(), catTicketGroups);
						}
						catTicketGroupIds.add(categoryTicketGroup.getCategoryTicketGroupId());
					}
				}
				CustomerAddress customerBillingAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(openOrdersList.iterator().next().getCustomerId());
				List<CustomerAddress> customerShippingAddress = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(openOrdersList.iterator().next().getCustomerId());
				POSCustomerOrder order = DAORegistry.getPosCustomerOrderDAO().getPOSCustomerOrderByTicketRequestId(openOrdersList.iterator().next().getCustomerOrderId());
				
				PDFUtil.generateRTWInvoicePdf(openOrdersList, categoryTicketMap, ticketMap, customerBillingAddress, customerShippingAddress, order);
				File file = new File(DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\invoice_"+invoiceIdStr+".pdf");
				if(!file.exists()){
					file.createNewFile();
				}
				OutputStream out = response.getOutputStream();
				FileInputStream in = new FileInputStream(file);
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format(downloadView+"; filename=\"%s\"",file.getName()));
				
				byte[] buffer = new byte[4096];
				int length;
				while((length = in.read(buffer)) > 0){
				    out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Get Pdf for given PO.
	 * @param request - Event information getting from TicketGroup
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/GetPOPdf")
	public void getPOPdf(HttpServletRequest request, HttpServletResponse response){
		String poId = request.getParameter("poId");
		String download = request.getParameter("inline");
		try {
			if(poId!=null && !poId.isEmpty()){
				//Getting Broker ID
				HttpSession session = request.getSession(true);
				Integer brokerId = Util.getBrokerId(session);
				PurchaseOrder PO = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(poId), brokerId);
				if(PO != null){
					List<TicketGroup> ticketGroups = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(PO.getId());
					Customer customer = DAORegistry.getCustomerDAO().get(PO.getCustomerId());
					CustomerAddress customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(customer.getId());
					
					PDFUtil.generatePOPdf(PO,ticketGroups,customer,customerAddress);
					File file = new File(DAORegistry.getPropertyDAO().get("tictracker.po.pdffile.path").getValue()+"\\PurchaseOrder_"+PO.getId()+".pdf");
					if(!file.exists()){
						file.createNewFile();
					}
					OutputStream out = response.getOutputStream();
					FileInputStream in = new FileInputStream(file);
					response.setContentType("application/pdf");
					response.setHeader("Content-Disposition", String.format(download+"; filename=\"%s\"",file.getName()));
					byte[] buffer = new byte[4096];
					int length;
					while((length = in.read(buffer)) > 0){
					    out.write(buffer, 0, length);
					}
					in.close();
					out.flush();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
		
	
	/**
	 * Get Pdf for given PO.
	 * @param request
	 * @param response
	 * @return
	 */
	/*@RequestMapping(value = "/GetRTWRTW2POPdf")
	public void getRTWRTW2POPdf(HttpServletRequest request, HttpServletResponse response){
		String poId = request.getParameter("poId");
		String productType = request.getParameter("productType");
		String download = request.getParameter("inline");
		POSTicketGroup ticketGroups = null;
		try {
			if(poId!=null && !poId.isEmpty()){
				Collection<PurchaseOrders> purchaseOrders = DAORegistry.getQueryManagerDAO().getRTWPurchaseOrdersListByPoID(poId);			
				List<POSTicket> ticket = DAORegistry.getPosTicketDAO().getPOSTicketByPurchaseOrderId(Integer.parseInt(poId), productType);
				Map<Integer, List<POSTicketGroup>> ticketMap = new HashMap<Integer, List<POSTicketGroup>>();
				if(ticket!=null && !ticket.isEmpty()){
					List<POSTicketGroup> list = null;
					List<Integer> ticketGroupIds = new ArrayList<Integer>();
					Map<Integer,Integer> ticCountMap = new HashMap<Integer, Integer>();
					Map<Integer,Double> ticPriceMap = new HashMap<Integer, Double>();
					Map<Integer,Integer> ticPoMap = new HashMap<Integer, Integer>();
					Map<Integer,String> ticOnHandMap = new HashMap<Integer, String>();
					Map<Integer,String> ticSeatMap = new HashMap<Integer, String>();
					for(POSTicket tic: ticket){
						if(ticCountMap.get(tic.getTicketGroupId())!=null){
							ticCountMap.put(tic.getTicketGroupId(), ticCountMap.get(tic.getTicketGroupId())+1);
						}else{
							ticCountMap.put(tic.getTicketGroupId(), 1);
						}
						ticPriceMap.put(tic.getTicketGroupId(),tic.getActualSoldPrice());
						ticPoMap.put(tic.getTicketGroupId(),tic.getPurchaseOrderId());
						ticOnHandMap.put(tic.getTicketGroupId(),((tic.getTicketOnHandStatusId()!=null&&tic.getTicketOnHandStatusId()==1)?"Y":"N"));
						if(ticSeatMap.get(tic.getTicketGroupId())!=null){
							String seatNo = ticSeatMap.get(tic.getTicketGroupId());
							if(seatNo.contains("-")){
								seatNo = seatNo.split("-")[0];
							}
							seatNo =seatNo + "-"+tic.getSeatNumber();
							ticSeatMap.put(tic.getTicketGroupId(), ticSeatMap.get(tic.getTicketGroupId())+"-"+tic.getSeatNumber());
						}else{
							ticSeatMap.put(tic.getTicketGroupId(), tic.getSeatNumber());
						}
					}
					for(POSTicket tic: ticket){
						if(ticketGroupIds.contains(tic.getTicketGroupId())){
							continue;
						}
						POSTicketGroup ticketGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupId(tic.getTicketGroupId(),productType);
						ticketGroup.setMappedQty(ticCountMap.get(tic.getTicketGroupId()));
						ticketGroup.setActualSoldPrice(ticPriceMap.get(tic.getTicketGroupId()));
						ticketGroup.setPurchaseOrderId(ticPoMap.get(tic.getTicketGroupId()));
						ticketGroup.setOnHand(ticOnHandMap.get(tic.getTicketGroupId()));
						ticketGroup.setSeatNoString(ticSeatMap.get(tic.getTicketGroupId()));
						if(ticketMap.get(ticketGroup.getEventId())!=null){
							ticketMap.get(ticketGroup.getEventId()).add(ticketGroup);
						}else{
							list = new ArrayList<POSTicketGroup>();
							list.add(ticketGroup);
							ticketMap.put(ticketGroup.getEventId(), list);
						}
						ticketGroupIds.add(ticketGroup.getTicketGroupId());
						
						
					}
				}
				List<PurchaseOrders> PO = new ArrayList<PurchaseOrders>();
				PO.addAll(purchaseOrders);			
				CustomerAddress customerAddress = DAORegistry.getCustomerAddressDAO().getCustomerAddressInfo(PO.get(0).getCustomerId());
				PDFUtil.generateRTWPOPdf(purchaseOrders,ticketMap,customerAddress);
				File file = new File(DAORegistry.getPropertyDAO().get("tictracker.po.pdffile.path").getValue()+"\\PurchaseOrder_"+poId+".pdf");
				if(!file.exists()){
					file.createNewFile();
				}
				OutputStream out = response.getOutputStream();
				FileInputStream in = new FileInputStream(file);
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", String.format(download+"; filename=\"%s\"",file.getName()));
				byte[] buffer = new byte[4096];
				int length;
				while((length = in.read(buffer)) > 0){
				    out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/*public boolean uploadRealTicket(HttpServletRequest request,Map<String,Boolean> uploadFileMap){
		final String invoiceIdStr = request.getParameter("invoiceId");
		String eventNameStr = request.getParameter("eventName");
		String eventDateStr = request.getParameter("eventDate");
		String eventTimeStr = request.getParameter("eventTime");
		String sectionStr = request.getParameter("section");
		String rowStr = request.getParameter("row");
		String appendFileName = "";
		String seatStr =null;
		boolean isEticketUploaded = false;
		boolean isQrCodeUploaded = false;
		boolean isBarcodeUploaded = false;
		String fileOriginalName = null;
		
		if(eventNameStr != null && !eventNameStr.isEmpty()){
			eventNameStr = eventNameStr.replaceAll(":", "_");
			eventNameStr = eventNameStr.replaceAll(" ", "_");
			eventNameStr = eventNameStr.replaceAll("-", "_");
			eventNameStr = eventNameStr.replaceAll("/", "_");
			eventNameStr = eventNameStr.replaceAll("'", "");
			eventNameStr = eventNameStr.replaceAll("&", "_");
			eventNameStr = eventNameStr.replaceAll(",", "_");
			//eventNameStr = eventNameStr.replaceAll(".", "_");
			eventNameStr = eventNameStr.replaceAll(";", "_");
			appendFileName += eventNameStr+"_";
		}
		if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
			appendFileName += invoiceIdStr+"_";
		}
		if(eventDateStr != null && !eventDateStr.isEmpty()){
			eventDateStr = eventDateStr.replaceAll("/", "_");
			eventDateStr = eventDateStr.replaceAll(" ", "_");
			appendFileName += eventDateStr+"_";
		}
		if(eventTimeStr != null && !eventTimeStr.isEmpty()){
			eventTimeStr = eventTimeStr.replaceAll(":", "_");
			eventTimeStr = eventTimeStr.replaceAll(" ", "_");
			appendFileName += eventTimeStr+"_";
		}
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		boolean flag=false;
		InvoiceTicketAttachment attachment;
		try {
			if(invoiceIdStr != null && !invoiceIdStr.isEmpty()){
				Map<String, String[]> requestParams = request.getParameterMap();
				List<InvoiceTicketAttachment> invoiceAttachment = null;
				List<Ticket> tickets = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceIdStr));
				String userName = SecurityContextHolder.getContext().getAuthentication().getName();
				Date now = new Date();
				boolean isUpdate = false;
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					fileOriginalName = null;
					if(key.contains("eticketTable_")){
						Integer rowNumber = Integer.parseInt(request.getParameter("eticketTable_count"));
						int cnt=0;
						for(int i=1;i<=rowNumber;i++){
							cnt++;
							isUpdate=false;
							MultipartFile file = multipartRequest.getFile("eticket_"+i);
							if(file==null){
								continue;
							}
							if(file != null && file.getSize() > 0) {
								File newFile = null;
								fileOriginalName = file.getOriginalFilename();
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fullFileName = "";
								if(tickets.size() > (i-1)){
									seatStr = tickets.get(i-1).getSeatNo().toString();
									TicketGroup ticGroup =  DAORegistry.getTicketGroupDAO().get(tickets.get(i-1).getTicketGroupId());
									rowStr = ticGroup.getRow();
									sectionStr =  ticGroup.getSection();
									fullFileName = Constants.Real_ticket_Path+appendFileName+sectionStr+"_"+rowStr+"_"+seatStr+"_eticket_"+cnt+"."+ext;
								}else{
									sectionStr = request.getParameter("section");
									fullFileName = Constants.Real_ticket_Path+appendFileName+sectionStr+"_eticket_"+cnt+"."+ext;
								}
								flag=true;
								invoiceAttachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceIdStr),FileType.ETICKET);
								for(InvoiceTicketAttachment attach : invoiceAttachment){
									if(attach.getPosition()!= null && attach.getPosition() == cnt){
										newFile = new File(attach.getFilePath());
										if(newFile.exists()){
											newFile.delete();
										}
										attach.setInvoiceId(Integer.parseInt(invoiceIdStr));
										attach.setFilePath(fullFileName);
										attach.setFileOriginalName(fileOriginalName);
										attach.setPosition(cnt);
										attach.setExtension(ext);
										attach.setType(FileType.ETICKET);
										attach.setUploadedBy(userName);
										attach.setUploadedDateTime(now);
										DAORegistry.getInvoiceTicketAttachmentDAO().update(attach);
										isUpdate=true;
										break;
									}
								}
								if(!isUpdate){
									attachment = new InvoiceTicketAttachment();
									attachment.setInvoiceId(Integer.parseInt(invoiceIdStr));
									attachment.setFilePath(fullFileName);
									attachment.setFileOriginalName(fileOriginalName);
									attachment.setExtension(ext);
									attachment.setPosition(cnt);
									attachment.setType(FileType.ETICKET);
									attachment.setUploadedBy(userName);
									attachment.setUploadedDateTime(now);
									DAORegistry.getInvoiceTicketAttachmentDAO().save(attachment);
									
								}
								isUpdate=false;
								newFile = new File(fullFileName);
								newFile.createNewFile();
								
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								isEticketUploaded = true;
							}
							
						}
						
					}else if(key.contains("qrcodeTable_")){
						Integer rowNumber = Integer.parseInt(request.getParameter("qrcodeTable_count"));
						int cnt=0;
						for(int i=1;i<=rowNumber;i++){
							cnt++;
							isUpdate=false;
							MultipartFile file = multipartRequest.getFile("qrcode_"+i);
							if(file==null){
								continue;
							}
							if(file != null && file.getSize() > 0) {
								File newFile = null;
								fileOriginalName = file.getOriginalFilename();
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fullFileName = "";
								if(tickets.size() > (i-1)){
									seatStr = tickets.get(i-1).getSeatNo().toString();
									TicketGroup ticGroup =  DAORegistry.getTicketGroupDAO().get(tickets.get(i-1).getTicketGroupId());
									rowStr = ticGroup.getRow();
									sectionStr =  ticGroup.getSection();
									fullFileName = Constants.Real_ticket_Path+appendFileName+sectionStr+"_"+rowStr+"_"+seatStr+"_mobileentry_"+cnt+"."+ext;
								}else{
									sectionStr = request.getParameter("section");
									fullFileName = Constants.Real_ticket_Path+appendFileName+sectionStr+"_mobileentry_"+cnt+"."+ext;
								}
								flag=true;
								invoiceAttachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceIdStr),FileType.QRCODE);
								for(InvoiceTicketAttachment attach : invoiceAttachment){
									if(attach.getPosition()!= null && attach.getPosition() == cnt){
										newFile = new File(attach.getFilePath());
										if(newFile.exists()){
											newFile.delete();
										}
										attach.setInvoiceId(Integer.parseInt(invoiceIdStr));
										attach.setFilePath(fullFileName);
										attach.setFileOriginalName(fileOriginalName);
										attach.setExtension(ext);
										attach.setPosition(cnt);
										attach.setType(FileType.QRCODE);
										attach.setUploadedBy(userName);
										attach.setUploadedDateTime(now);
										DAORegistry.getInvoiceTicketAttachmentDAO().update(attach);
										isUpdate=true;
										break;
									}
								}
								if(!isUpdate){
									attachment = new InvoiceTicketAttachment();
									attachment.setInvoiceId(Integer.parseInt(invoiceIdStr));
									attachment.setFilePath(fullFileName);
									attachment.setFileOriginalName(fileOriginalName);
									attachment.setExtension(ext);
									attachment.setPosition(cnt);
									attachment.setType(FileType.QRCODE);
									attachment.setUploadedBy(userName);
									attachment.setUploadedDateTime(now);
									DAORegistry.getInvoiceTicketAttachmentDAO().save(attachment);
									
								}
								isUpdate=false;
								newFile = new File(fullFileName);
								newFile.createNewFile();
								
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								isQrCodeUploaded = true;
							}
							
						}
						
					}else if(key.contains("barcodeTable_")){
						Integer rowNumber = Integer.parseInt(request.getParameter("barcodeTable_count"));
						int cnt=0;
						for(int i=1;i<=rowNumber;i++){
							isUpdate=false;
							cnt++;
							MultipartFile file = multipartRequest.getFile("barcode_"+i);
							if(file==null){
								continue;
							}
							if(file != null && file.getSize() > 0) {
								File newFile = null;
								fileOriginalName = file.getOriginalFilename();
								String ext = FilenameUtils.getExtension(file.getOriginalFilename());
								String fullFileName = "";
								if(tickets.size() > (i-1)){
									seatStr = tickets.get(i-1).getSeatNo().toString();
									TicketGroup ticGroup =  DAORegistry.getTicketGroupDAO().get(tickets.get(i-1).getTicketGroupId());
									rowStr = ticGroup.getRow();
									sectionStr =  ticGroup.getSection();
									fullFileName = Constants.Real_ticket_Path+appendFileName+sectionStr+"_"+rowStr+"_"+seatStr+"_electronictransfer_"+cnt+"."+ext;
								}else{
									sectionStr = request.getParameter("section");
									fullFileName = Constants.Real_ticket_Path+appendFileName+sectionStr+"_electronictransfer_"+cnt+"."+ext;
								}
								flag=true;
								invoiceAttachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceAndFileType(Integer.parseInt(invoiceIdStr),FileType.BARCODE);
								for(InvoiceTicketAttachment attach : invoiceAttachment){
									if(attach.getPosition()!= null &&  attach.getPosition() == cnt){
										newFile = new File(attach.getFilePath());
										if(newFile.exists()){
											newFile.delete();
										}
										attach.setInvoiceId(Integer.parseInt(invoiceIdStr));
										attach.setFilePath(fullFileName);
										attach.setFileOriginalName(fileOriginalName);
										attach.setExtension(ext);
										attach.setPosition(cnt);
										attach.setType(FileType.BARCODE);
										attach.setUploadedBy(userName);
										attach.setUploadedDateTime(now);
										DAORegistry.getInvoiceTicketAttachmentDAO().update(attach);
										isUpdate=true;
										break;
									}
								}
								if(!isUpdate){
									attachment = new InvoiceTicketAttachment();
									attachment.setInvoiceId(Integer.parseInt(invoiceIdStr));
									attachment.setFilePath(fullFileName);
									attachment.setFileOriginalName(fileOriginalName);
									attachment.setExtension(ext);
									attachment.setPosition(cnt);
									attachment.setType(FileType.BARCODE);
									attachment.setUploadedBy(userName);
									attachment.setUploadedDateTime(now);
									DAORegistry.getInvoiceTicketAttachmentDAO().save(attachment);
									
								}
								isUpdate=false;
								newFile = new File(fullFileName);
								newFile.createNewFile();
								
								BufferedOutputStream stream = new BufferedOutputStream(
										new FileOutputStream(newFile));
						        FileCopyUtils.copy(file.getInputStream(), stream);
								stream.close();
								isBarcodeUploaded = true;
							}
							
						}
						
					}
				}
				
			}
			uploadFileMap.put(FileType.BARCODE.toString(),isBarcodeUploaded);
			uploadFileMap.put(FileType.QRCODE.toString(),isQrCodeUploaded);
			uploadFileMap.put(FileType.ETICKET.toString(),isEticketUploaded);
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}*/
	
	
	/*@RequestMapping(value = "/DownloadRealTix")
	public void downloadRealTix(HttpServletRequest request, HttpServletResponse response){
		String invoiceIdStr = request.getParameter("invoiceId");
		String fileTypeStr = request.getParameter("fileType");
		String position = request.getParameter("position");
		try {
			if(invoiceIdStr!=null && !invoiceIdStr.isEmpty() && fileTypeStr!=null && !fileTypeStr.isEmpty()
					&& position !=null && !position.isEmpty()){
				FileType fileType = FileType.valueOf(fileTypeStr);
				InvoiceTicketAttachment attachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceFileTypeAndPosition(Integer.parseInt(invoiceIdStr), fileType,Integer.parseInt(position));
				if(attachment!=null){
					OutputStream out = response.getOutputStream();
					File file = new File(attachment.getFilePath());
					if(file.exists()){
						FileInputStream in = new FileInputStream(file);
						//response.setContentType("application/octet-stream");
						response.setContentType("application/pdf");
						response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
						byte[] buffer = new byte[4096];
						int length;
						while((length = in.read(buffer)) > 0){
						    out.write(buffer, 0, length);
						}
						in.close();
						out.flush();
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	/*@RequestMapping(value = "/DownloadETicket")
	public void downloadETicket(HttpServletRequest request, HttpServletResponse response){
		String ticketGroupIdStr = request.getParameter("ticketGroupId");
		String fileTypeStr = request.getParameter("fileType");
		String position = request.getParameter("position");
		try {
			if(ticketGroupIdStr!=null && !ticketGroupIdStr.isEmpty() && fileTypeStr!=null && !fileTypeStr.isEmpty()
					&& position !=null && !position.isEmpty()){
				FileType fileType = FileType.valueOf(fileTypeStr);
				TicketGroupTicketAttachment attachment = DAORegistry.getTicketGroupTicketAttachmentDAO().getAttachmentByTicketGroupFileTypeAndPosition(Integer.parseInt(ticketGroupIdStr), fileType,Integer.parseInt(position));
				if(attachment!=null){
					OutputStream out = response.getOutputStream();
					File file = new File(attachment.getFilePath());
					if(file.exists()){
						FileInputStream in = new FileInputStream(file);
						//response.setContentType("application/octet-stream");
						response.setContentType("application/pdf");
						response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
						byte[] buffer = new byte[4096];
						int length;
						while((length = in.read(buffer)) > 0){
						    out.write(buffer, 0, length);
						}
						in.close();
						out.flush();
					}
				}				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	@RequestMapping(value = "/DeleteRealTixAttachment")
	public String deleteRealTixAttachment(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String invoiceId = request.getParameter("invoiceId");
			String fileType = request.getParameter("fileType");
			String position = request.getParameter("position");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("fileType", fileType);
			map.put("position", position);
			map.put("userName", userName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DELETE_REAL_TICKET_ATTACHMENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*Integer invoiceId = 0;
			Integer position = 0;
			JSONObject object = new JSONObject();
			object.put("status", 0);
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				object.put("msg", "Invoice ID not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				object.put("msg", "Invoice ID should be valid Integer value.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(fileTypeStr == null || fileTypeStr.isEmpty()){
				object.put("msg", "File Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(positionStr == null || positionStr.isEmpty()){
				object.put("msg", "File Position not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				position = Integer.parseInt(positionStr);
			}catch(Exception e){
				e.printStackTrace();
				object.put("msg", "File Position should be valid Integer value.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
							
			FileType fileType = FileType.valueOf(fileTypeStr);
			InvoiceTicketAttachment attachment = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByInvoiceFileTypeAndPosition(invoiceId, fileType, position);
			if(attachment == null){
				object.put("msg", "File not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			OutputStream out = response.getOutputStream();
			File file = new File(attachment.getFilePath());
			if(file.exists()){
				file.delete();
			}
			DAORegistry.getInvoiceTicketAttachmentDAO().delete(attachment);
		
			List<InvoiceTicketAttachment> invoiceTktAttachments = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoiceId);
			if(invoiceTktAttachments == null || invoiceTktAttachments.size() <= 0){
				Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
				if(invoice != null){
					invoice.setStatus(InvoiceStatus.Outstanding);
					invoice.setLastUpdatedBy(userName);
					invoice.setLastUpdated(new Date());
					invoice.setIsRealTixUploaded("No");
					DAORegistry.getInvoiceDAO().update(invoice);
								
					InvoiceAudit audit = new InvoiceAudit(invoice);
					audit.setAction(InvoiceAuditAction.INVOICE_COMPLETED_TO_OUTSTANDING);
					audit.setCreateBy(userName);
					audit.setCreatedDate(new Date());
					audit.setNote(userName+" has removed uploaded ticket file from invoice, now this invoice does not have any real ticket file uploaded so changing status to outstanding..");
					DAORegistry.getInvoiceAuditDAO().save(audit);
				}
			}
		
			object.put("msg", "File Deleted successfully.");
			object.put("status", 1);
			
			//return jsonArr
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	@RequestMapping(value="/ChangeInvoiceStatus")
	public String changeInvoiceStatus(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String status = request.getParameter("status");
			String invoiceId = request.getParameter("invoiceId");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("status", status);
			map.put("invoiceId", invoiceId);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHANGE_INVOICE_STATUS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceStatusChangeDTO invoiceStatusChangeDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceStatusChangeDTO")), InvoiceStatusChangeDTO.class);
			
			if(invoiceStatusChangeDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceStatusChangeDTO.getMessage());
				model.addAttribute("status", invoiceStatusChangeDTO.getStatus());				
			}else{
				model.addAttribute("msg", invoiceStatusChangeDTO.getError().getDescription());
				model.addAttribute("status", invoiceStatusChangeDTO.getStatus());
			}
			model.addAttribute("invoiceId", invoiceStatusChangeDTO.getInvoiceId());
			
			/*String msg="";
			Integer invoiceId = 0;
			JSONObject object = new JSONObject();
			object.put("status", 0);
			
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				object.put("msg", "Invoice ID not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				object.put("msg", "Invoice ID should be valid Integer value.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			if(invoice == null){
				object.put("msg", "Invoice Details not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
				
			invoice.setLastUpdatedBy(userName);
			CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			if(status!=null && status.equalsIgnoreCase(InvoiceStatus.Voided.toString())){
				if(!invoice.getStatus().equals(InvoiceStatus.Voided)){
					if(order == null){
						object.put("msg", "Customer Order not found.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					else if((order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) 
							|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
							|| (order.getThirdPaymentMethod()!= null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD))
							|| order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)
							|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
							|| (order.getThirdPaymentMethod()!= null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL)))){
						
						String refundType = "";
						if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)){
							refundType = PaymentMethod.CREDITCARD.toString();
						}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
							refundType = PaymentMethod.PAYPAL.toString();
						}
						
						Map<String, String> paramMap = Util.getParameterMap(request);
						paramMap.put("orderId", String.valueOf(order.getId()));
						paramMap.put("refundType", refundType);
						paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(invoice.getInvoiceTotal())));
						
						String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
						Gson gson = Util.getGsonBuilder().create();	
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
						if(invoiceRefundResponse == null){
							object.put("msg", "Invoice Refund API not found.");
							IOUtils.write(object.toString().getBytes(), response.getOutputStream());
							return;
						}else if(invoiceRefundResponse.getStatus()==1){
							InvoiceRefund invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
							if(invoiceRefund == null){
								object.put("msg", "Invoice Refund not found.");
								IOUtils.write(object.toString().getBytes(), response.getOutputStream());
								return;
							}									
							Util.voidInvoice(invoice, order, invoiceRefund);
							msg = "Selected Invoice Voided Successfully.";
							object.put("status", 1);
						}else{
							msg = invoiceRefundResponse.getError().getDescription();
						}
					}else if(order!=null && order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
						Util.voidInvoice(invoice, order, null);
						msg = "Selected Invoice Voided Successfully.";
						object.put("status", 1);
					}else{
						msg="Payment method is other than credicard/reward points for selected Order.";
					}
				}else{
					msg="Selected invoice already Voided.";
				}
			}else if(status!=null && status.equalsIgnoreCase(InvoiceStatus.Completed.toString())){
				if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
					msg = "You Cannot mark this invoice as complete, Invoice payment is not done yet please complete invoice payment first.";
					object.put("invoiceId", invoiceIdStr);
					object.put("msg", msg);
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				invoice.setStatus(InvoiceStatus.Completed);
				invoice.setLastUpdatedBy(userName);
				invoice.setLastUpdated(new Date());
				invoice.setIsRealTixUploaded("Yes");
				invoice.setRealTixMap("Yes");
				DAORegistry.getInvoiceDAO().update(invoice);
				
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.INVOICE_MANUALLY_COMPLETED);
				audit.setCreateBy(userName);
				audit.setCreatedDate(new Date());
				audit.setNote("Invoice Manully Marked as Completed.");
				DAORegistry.getInvoiceAuditDAO().save(audit);
				msg="Selected Invoice Marked as Completed.";
				object.put("status", 1);
				
				//Tracking User Action
				String userActionMsg = "Invoice Manually Marked as Completed.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
				
			}else if(status!=null && status.equalsIgnoreCase(InvoiceStatus.Outstanding.toString())){
				invoice.setStatus(InvoiceStatus.Outstanding);
				invoice.setLastUpdatedBy(userName);
				invoice.setLastUpdated(new Date());
				invoice.setIsRealTixUploaded("No");
				//invoice.setRealTixMap("Yes");
				DAORegistry.getInvoiceDAO().update(invoice);
				
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.INVOICE_MANUALLY_OUTSTANDING);
				audit.setCreateBy(userName);
				audit.setCreatedDate(new Date());
				audit.setNote("Invoice Manully Marked as Outstanding.");
				DAORegistry.getInvoiceAuditDAO().save(audit);
				msg="Selected Invoice Marked as Outstanding.";
				object.put("status", 1);
				
				//Tracking User Action
				String userActionMsg = "Invoice Manually Marked as Outstanding.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			}
			object.put("invoiceId", invoiceIdStr);
			object.put("msg", msg);
			
			//return jsonArr
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value="/GetPaymentHistory")
	public String getPaymentHistory(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String invoiceId = request.getParameter("invoiceId");
			String productType = request.getParameter("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("productType", productType);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_INVOICE_PAYMENT_HISTORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoicePaymentDetailsDTO invoicePaymentDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("invoicePaymentDetailsDTO")), InvoicePaymentDetailsDTO.class);
			
			if(invoicePaymentDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", invoicePaymentDetailsDTO.getMessage());
				model.addAttribute("status", invoicePaymentDetailsDTO.getStatus());
				model.addAttribute("pagingInfo", invoicePaymentDetailsDTO.getInvoicePaginationDTO());
				model.addAttribute("paymentHistory", invoicePaymentDetailsDTO.getInvoiceList());
				model.addAttribute("rtwPaymentHistory", invoicePaymentDetailsDTO.getRtwInvoiceList());				
			}else{
				model.addAttribute("msg", invoicePaymentDetailsDTO.getError().getDescription());
				model.addAttribute("status", invoicePaymentDetailsDTO.getStatus());
			}
			model.addAttribute("productType", invoicePaymentDetailsDTO.getSelectedProductType());
			
			/*List<PaymentHistory> paymentHistoryList = null;
			List<PaymentHistory> rtwPaymentHistoryList = null;
			Integer invoiceId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
						
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				jObj.put("msg", "Invoice ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Invoice ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(productType == null || productType.isEmpty()){
				jObj.put("msg", "Product Type not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
				
			if(productType.equalsIgnoreCase("REWARDTHEFAN")){
				paymentHistoryList = DAORegistry.getQueryManagerDAO().getPaymentHistoryByInvoice(invoiceId, brokerId);
			}
			if(productType.equalsIgnoreCase("RTW") || productType.equalsIgnoreCase("RTW2")){
				rtwPaymentHistoryList = DAORegistry.getQueryManagerDAO().getRTWPaymentHistoryByInvoice(invoiceId, productType);
			}
			
			if(paymentHistoryList!=null && paymentHistoryList.size() > 0){
				for(PaymentHistory paymentHistory : paymentHistoryList){
					if(paymentHistory.getPrimaryPaymentMethod().equalsIgnoreCase("ACCOUNT_RECIVABLE")){
						jObj.put("msg", "Selected Invoice is Account Receivable, no payment history found.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
				}
			}
			if(paymentHistoryList!=null && paymentHistoryList.size() > 0){
				jObj.put("paymentHistory", JsonWrapperUtil.getPaymentHistoryArray(paymentHistoryList, null));
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(paymentHistoryList.size()));
				jObj.put("status", 1);
			}
			else if(rtwPaymentHistoryList!=null && rtwPaymentHistoryList.size() > 0){
				jObj.put("rtwPaymentHistory", JsonWrapperUtil.getPaymentHistoryArray(null, rtwPaymentHistoryList));
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(rtwPaymentHistoryList.size()));
				jObj.put("status", 1);
			}
			else{
				jObj.put("msg", "No Payment details found for Selected Invoice.");
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(0));
			}
			
			jObj.put("productType", productType);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value="/GetPOPaymentHistory")
	public String getPOPaymentHistory(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String poId = request.getParameter("poId");
			String productType = request.getParameter("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("poId", poId);
			map.put("productType", productType);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_PO_PAYMENT_HISTORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POPaymentDetailsDTO poPaymentDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("POPaymentDetailsDTO")), POPaymentDetailsDTO.class);
			
			if(poPaymentDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", poPaymentDetailsDTO.getMessage());
				model.addAttribute("status", poPaymentDetailsDTO.getStatus());
				model.addAttribute("pagingInfo", poPaymentDetailsDTO.getPoPaginationDTO());
				model.addAttribute("paymentDetails", poPaymentDetailsDTO.getPoList());
				model.addAttribute("rtwPaymentDetails", poPaymentDetailsDTO.getRtwPOList());				
			}else{
				model.addAttribute("msg", poPaymentDetailsDTO.getError().getDescription());
				model.addAttribute("status", poPaymentDetailsDTO.getStatus());
			}
			model.addAttribute("productType", poPaymentDetailsDTO.getSelectedProductType());
			/*List<PurchaseOrderPaymentDetails> poList = new ArrayList<PurchaseOrderPaymentDetails>();
			List<PaymentHistory> rtwPOList = null;
			Integer poId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(poIdStr == null || poIdStr.isEmpty()){
				jObj.put("msg", "Purchase Order ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Purchase Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(productType == null || productType.isEmpty()){
				jObj.put("msg", "Product Type not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(productType.equalsIgnoreCase("REWARDTHEFAN")){					
				PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
				if(purchaseOrder == null){
					jObj.put("msg", "Purchase Order Details not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}					
				PurchaseOrderPaymentDetails paymentDetails = DAORegistry.getPurchaseOrderPaymentDetailsDAO().getPaymentDetailsByPOId(poId);
				if(paymentDetails == null){
					jObj.put("msg", "Purchase Order Payment Details not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}					
				poList.add(paymentDetails);
			}
			if(productType.equalsIgnoreCase("RTW") || productType.equalsIgnoreCase("RTW2")){
				rtwPOList = DAORegistry.getQueryManagerDAO().getRTWPaymentDetailsByPOId(poId, productType);					
			}
			
			if(poList!=null && poList.size() > 0){
				jObj.put("paymentDetails", JsonWrapperUtil.getPOPaymentHistoryArray(poList));
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(poList.size()));
				jObj.put("status", 1);
			}
			else if(rtwPOList!=null && rtwPOList.size() > 0){
				jObj.put("rtwPaymentDetails", JsonWrapperUtil.getPaymentHistoryArray(null, rtwPOList));
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(rtwPOList.size()));
				jObj.put("status", 1);
			}
			else{
				jObj.put("msg", "No Payment details found for Selected PO.");
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(0));
			}							
			
			jObj.put("productType", productType);
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value="/UpdatePOTicketGroupOnHandStatus")
	public String updatePOTicketGroupOnHandStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String poId = request.getParameter("poId");
			String ticketGroupIds = request.getParameter("ticketGroupIds");
			String onHand = request.getParameter("onHand");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("poId", poId);
			map.put("ticketGroupIds", ticketGroupIds);
			map.put("onHand", onHand);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_UPDATE_PO_TICKET_GROUP_ON_HAND);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POTicketGroupOnHandStatusDTO poTicketGroupOnHandStatusDTO = gson.fromJson(((JsonObject)jsonObject.get("POTicketGroupOnHandStatusDTO")), POTicketGroupOnHandStatusDTO.class);
			
			if(poTicketGroupOnHandStatusDTO.getStatus() == 1){
				model.addAttribute("msg", poTicketGroupOnHandStatusDTO.getMessage());
				model.addAttribute("status", poTicketGroupOnHandStatusDTO.getStatus());
				model.addAttribute("pagingInfo", poTicketGroupOnHandStatusDTO.getTicketGroupPaginationDTO());
				model.addAttribute("ticketGroups", poTicketGroupOnHandStatusDTO.getTicketGroupDTO());
				model.addAttribute("poId", poTicketGroupOnHandStatusDTO.getPoId());
			}else{
				model.addAttribute("msg", poTicketGroupOnHandStatusDTO.getError().getDescription());
				model.addAttribute("status", poTicketGroupOnHandStatusDTO.getStatus());
			}
			/*List<TicketGroup> ticketGroupList = null;
			Integer poId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(poIdStr == null || poIdStr.isEmpty()){
				jObj.put("msg", "Purchase Order ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Purchase Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			PurchaseOrder purchaseOrder = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(purchaseOrder == null){
				jObj.put("msg", "Purchase Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(ticketGroupIds!=null && !ticketGroupIds.isEmpty() && onHand!=null && !onHand.isEmpty()){
				String arr[] = ticketGroupIds.split(",");
				if(arr.length>0){
					TicketGroup tg = null;
					ticketGroupList = new ArrayList<TicketGroup>();
					for(String id : arr){
						tg = DAORegistry.getTicketGroupDAO().get(Integer.parseInt(id));
						tg.setOnHand(onHand);
						ticketGroupList.add(tg);
					}
					DAORegistry.getTicketGroupDAO().updateAll(ticketGroupList);
					jObj.put("msg", "Selected Ticket Group updated successfully.");
					
					//Tracking User Action
					String userActionMsg = "Ticket Group(s) OnHand Status Updated to "+onHand.toUpperCase()+". Ticket Group Id's - "+ticketGroupIds;					
					Util.userActionAudit(request, purchaseOrder.getId(), userActionMsg);
				}
			}
			
			ticketGroupList = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByPOId(poId);
			for(TicketGroup ticketGroup : ticketGroupList){
				EventDetails event = DAORegistry.getEventDetailsDAO().get(ticketGroup.getEventId());
				ticketGroup.setEventName(event.getEventName());
				ticketGroup.setEventDateStr(event.getEventDateTimeStr());
				ticketGroup.setVenue(event.getBuilding());
			}
				
			if(ticketGroupList != null){
				JSONArray ticketGroupArray = new JSONArray();
				JSONObject ticketGroupObject = null;
				for(TicketGroup ticketGroup : ticketGroupList){
					ticketGroupObject = new JSONObject();
					ticketGroupObject.put("id", ticketGroup.getId());
					ticketGroupObject.put("eventName", ticketGroup.getEventName());
					ticketGroupObject.put("eventDate", ticketGroup.getEventDateStr());
					ticketGroupObject.put("venue", ticketGroup.getVenue());
					ticketGroupObject.put("section", ticketGroup.getSection());
					ticketGroupObject.put("row", ticketGroup.getRow());
					ticketGroupObject.put("seatLow", ticketGroup.getSeatLow());
					ticketGroupObject.put("seatHigh", ticketGroup.getSeatHigh());
					ticketGroupObject.put("quantity", ticketGroup.getQuantity());
					ticketGroupObject.put("price", ticketGroup.getPriceStr());					
					ticketGroupObject.put("onHand", ticketGroup.getOnHand());
					ticketGroupObject.put("invoiceId", ticketGroup.getInvoiceId());
					ticketGroupArray.put(ticketGroupObject);
				}
				jObj.put("ticketGroups", ticketGroupArray);
			}			
			jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(ticketGroupList!=null?ticketGroupList.size():0));
			jObj.put("poId", poIdStr);
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value="/GetInvoiceCsr")
	public String getInvoiceCsr(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			String invoiceId = request.getParameter("invoiceId");			
			String csrUser = request.getParameter("csrUser");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("invoiceId", invoiceId);
			map.put("csrUser", csrUser);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_UPDATE_INVOICE_CSR);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceCsrDTO invoiceCsrDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceCsrDTO")), InvoiceCsrDTO.class);
			
			if(invoiceCsrDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceCsrDTO.getMessage());
				model.addAttribute("status", invoiceCsrDTO.getStatus());
				model.addAttribute("invoiceId", invoiceCsrDTO.getInvoiceId());
				model.addAttribute("invoiceCsr", invoiceCsrDTO.getInvoiceCsr());
				model.addAttribute("users", invoiceCsrDTO.getTrackerUserDTO());
			}else{
				model.addAttribute("msg", invoiceCsrDTO.getError().getDescription());
				model.addAttribute("status", invoiceCsrDTO.getStatus());
			}
			
			/*Collection<TrackerUser> users = null;
			Integer invoiceId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);			
			
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				jObj.put("msg", "Invoice ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Invoice ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
											
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(invoiceId, brokerId);
			if(invoice == null){
				jObj.put("msg", "Invoice Details not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
				
			if(action!=null && action.equalsIgnoreCase("changeCSR")){
				if(csrUser == null || csrUser.isEmpty()){
					jObj.put("msg", "CSR not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				String userName = SecurityContextHolder.getContext().getAuthentication().getName();
				if(invoice!=null){
					invoice.setCsr(csrUser);
					invoice.setLastUpdated(new Date());
					invoice.setLastUpdatedBy(userName);
					DAORegistry.getInvoiceDAO().update(invoice);
					jObj.put("msg", "Invoice CSR changed Successfully.");
					
					//Tracking User Action
					String userActionMsg = "Invoice CSR changed Successfully.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
				}
			}else{
				csrUser = invoice.getCsr();
			}				
			
			if(brokerId != 1001){
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserByBrokerId(brokerId);
			}else{
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserExceptBroker();
			}
			if(users != null && users.size() > 0){
				JSONArray userArray = new JSONArray();
				JSONObject userObject = null;
				for(TrackerUser user : users){
					userObject = new JSONObject();
					userObject.put("name", user.getUserName());
					userArray.put(userObject);
				}
				jObj.put("users", userArray);
			}			
			jObj.put("invoiceCsr", csrUser);
			jObj.put("invoiceId", invoiceIdStr);
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	 
	 @RequestMapping(value="/GetPOCsr")
	 public String getPOCsr(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			String poId = request.getParameter("poId");			
			String csrUser = request.getParameter("csrUser");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("poId", poId);
			map.put("csrUser", csrUser);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_UPDATE_PO_CSR);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POCsrDTO poCsrDTO = gson.fromJson(((JsonObject)jsonObject.get("POCsrDTO")), POCsrDTO.class);
			
			if(poCsrDTO.getStatus() == 1){
				model.addAttribute("msg", poCsrDTO.getMessage());
				model.addAttribute("status", poCsrDTO.getStatus());
				model.addAttribute("poId", poCsrDTO.getPoId());
				model.addAttribute("poCsr", poCsrDTO.getPoCsr());
				model.addAttribute("users", poCsrDTO.getTrackerUserDTO());
			}else{
				model.addAttribute("msg", poCsrDTO.getError().getDescription());
				model.addAttribute("status", poCsrDTO.getStatus());
			}

			/*Collection<TrackerUser> users = null;
			Integer poId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
						
			if(poIdStr == null || poIdStr.isEmpty()){
				jObj.put("msg", "Purchase Order ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Purchase Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			PurchaseOrder po = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(po == null){
				jObj.put("msg", "Purchase Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}				
				
			if(action!=null && action.equalsIgnoreCase("changeCSR")){
				if(csrUser == null || csrUser.isEmpty()){
					jObj.put("msg", "CSR not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				if(po!=null){
					po.setCsr(csrUser);
					po.setLastUpdated(new Date());
					DAORegistry.getPurchaseOrderDAO().update(po);
					jObj.put("msg", "PO CSR changed Successfully.");
					
					//Tracking User Action
					String userActionMsg = "PO CSR changed Successfully.";
					Util.userActionAudit(request, po.getId(), userActionMsg);
				}
			}else{
				csrUser = po.getCsr();
			}
			
			if(brokerId != 1001){
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserByBrokerId(brokerId);
			}else{
				users =  DAORegistry.getTrackerUserDAO().getAllTrackerUserExceptBroker();
			}
			if(users != null && users.size() > 0){
				JSONArray userArray = new JSONArray();
				JSONObject userObject = null;
				for(TrackerUser user : users){
					userObject = new JSONObject();
					userObject.put("name", user.getUserName());
					userArray.put(userObject);
				}
				jObj.put("users", userArray);
			}			
			jObj.put("poCsr", csrUser);
			jObj.put("poId", poIdStr);
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}	
		return "";
	}
	 
	
	 @RequestMapping({"/UpdateShippingAddress"})
	 public String updateShippingAddress(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String orderId = request.getParameter("orderId");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String street1 = request.getParameter("street1");
			String street2 = request.getParameter("street2");
			String city = request.getParameter("city");
			String zipcode = request.getParameter("zipcode");
			String phone = request.getParameter("phone");
			String email = request.getParameter("email");
			String state = request.getParameter("state");
			String country = request.getParameter("country");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderId", orderId);
			map.put("firstName", firstName);
			map.put("lastName", lastName);
			map.put("street1", street1);
			map.put("street2", street2);
			map.put("city", city);
			map.put("zipcode", zipcode);
			map.put("phone", phone);
			map.put("email", email);
			map.put("state", state);
			map.put("country", country);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_CUSTOMER_ORDER_SHIPPING_ADDRESS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*List<Country> countryList = null;
			List<State> stateList = null;
			CustomerOrderDetails orderDetails = null;
			String msg ="";
			Integer orderId = 0;
			int countryId = 0;
			int stateId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			 
			if(orderIdStr == null || orderIdStr.isEmpty()){
				 jObj.put("msg", "Customer Order ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Customer Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
						
			
			if(country == null || country.isEmpty()){
				jObj.put("msg", "Country not found.");
			 	IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
			 	return;
			}
			try{
				countryId = Integer.parseInt(country);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Country should be valid value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			
			if(stateIdStr == null || stateIdStr.isEmpty()){
				jObj.put("msg", "State not found.");
			 	IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
			 	return;
			}			
			try{
				stateId = Integer.parseInt(stateIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "State should be valid value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}			

			orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			if(orderDetails == null){
				jObj.put("msg", "Order Details not found.");
			 	IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
			 	return;
			}
			//stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(orderDetails.getShCountry());				
			orderDetails.setShFirstName(request.getParameter("firstName"));
			orderDetails.setShLastName(request.getParameter("lastName"));
			orderDetails.setShAddress1(request.getParameter("street1"));
			orderDetails.setShAddress2(request.getParameter("street2"));
			orderDetails.setShCity(request.getParameter("city"));
			orderDetails.setShZipCode(request.getParameter("zipcode"));
			orderDetails.setShPhone1(request.getParameter("phone"));
			orderDetails.setShEmail(request.getParameter("email"));
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
						
			for(State state : stateList){
				if(stateId==state.getId()){
					orderDetails.setShState(stateId);
					orderDetails.setShStateName(state.getName());
					break;
				}
			}
			
			for(Country count : countryList){
				if(countryId==count.getId()){
					orderDetails.setShCountry(countryId);
					orderDetails.setShCountryName(count.getName());
					break;
				}
			}
			DAORegistry.getCustomerOrderDetailsDAO().update(orderDetails);
			msg = "Address Updated successfully.";			
			jObj.put("msg", msg);
			jObj.put("status", 1);
			
			//Tracking User Action
			String userActionMsg = "Customer Order - Shipping Address Updated Successfully.";
			Util.userActionAudit(request, orderDetails.getId(), userActionMsg);
			
			//return JSONArray
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	 }
	 
	 @RequestMapping({"/GetCustomerOrderShippingAddress"})
	 public String getCustomerOrderShippingAddress(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
			
			try{
				String orderId = request.getParameter("orderId");
				String invoiceId = request.getParameter("invoiceId");
				Integer brokerId = Util.getBrokerId(session);
				
				Map<String, String> map = Util.getParameterMap(request);
				map.put("orderId", orderId);
				map.put("invoiceId", invoiceId);
				map.put("brokerId", brokerId.toString());
				
				String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_ORDER_SHIPPING_ADDRESS);
				Gson gson = GsonCustomConfig.getGsonBuilder();		
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				InvoiceCustomerOrderAddressDTO invoiceCustomerOrderAddressDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceCustomerOrderAddressDTO")), InvoiceCustomerOrderAddressDTO.class);
				
				if(invoiceCustomerOrderAddressDTO.getStatus() == 1){
					model.addAttribute("msg", invoiceCustomerOrderAddressDTO.getMessage());
					model.addAttribute("status", invoiceCustomerOrderAddressDTO.getStatus());
					model.addAttribute("customer", invoiceCustomerOrderAddressDTO.getCustomerDTO());
					model.addAttribute("order", invoiceCustomerOrderAddressDTO.getCustomerOrderAddressDTO());
					model.addAttribute("countryList", invoiceCustomerOrderAddressDTO.getCountryDTO());
					model.addAttribute("stateList", invoiceCustomerOrderAddressDTO.getStateDTO());
				}else{
					model.addAttribute("msg", invoiceCustomerOrderAddressDTO.getError().getDescription());
					model.addAttribute("status", invoiceCustomerOrderAddressDTO.getStatus());
				}
				model.addAttribute("invoiceId", invoiceCustomerOrderAddressDTO.getInvoiceId());
				
				/*List<Country> countryList = null;
				List<State> stateList = null;
				CustomerOrderDetails orderDetails = null;
				Integer orderId = 0;
				JSONObject jObj = new JSONObject();
				jObj.put("status", 0);
				
//				if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
//					 jObj.put("msg", "Invoice ID not found.");
//					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
//					 return;
//				}
				
				if(orderIdStr == null || orderIdStr.isEmpty()){
					 jObj.put("msg", "Customer Order ID not found.");
					 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					 return;
				}
				try{
					orderId = Integer.parseInt(orderIdStr);
				}catch(Exception e){
					e.printStackTrace();
					jObj.put("msg", "Customer Order ID should be valid Integer value.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);
				if(customerOrder == null){
					jObj.put("msg", "Customer Order not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
				if(orderDetails == null){
					jObj.put("msg", "Customer Order Details not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				Customer customer = DAORegistry.getCustomerDAO().get(customerOrder.getCustomerId()); 
				stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(orderDetails.getShCountry());
				countryList = DAORegistry.getQueryManagerDAO().getCountries();
				
				if(customer != null){
					JSONObject customerObject = new JSONObject();
					customerObject.put("companyName", customer.getCompanyName());
					jObj.put("customer", customerObject);
				}
				if(orderDetails != null){
					JSONObject cusOrderShipAddrObject = new JSONObject();
					cusOrderShipAddrObject.put("shipAddrOrderId", orderDetails.getOrderId());
					cusOrderShipAddrObject.put("shipAddrFirstName", orderDetails.getShFirstName());
					cusOrderShipAddrObject.put("shipAddrLastName", orderDetails.getShLastName());
					cusOrderShipAddrObject.put("shipAddrAddressLine1", orderDetails.getShAddress1());
					cusOrderShipAddrObject.put("shipAddrAddressLine2", orderDetails.getShAddress2());
					cusOrderShipAddrObject.put("shipAddrEmail", orderDetails.getShEmail());	
					cusOrderShipAddrObject.put("shipAddrPhone1", orderDetails.getShPhone1());
					cusOrderShipAddrObject.put("shipAddrCountry", orderDetails.getShCountry());
					cusOrderShipAddrObject.put("shipAddrCountryName", orderDetails.getShCountryName());
					cusOrderShipAddrObject.put("shipAddrState", orderDetails.getShState());
					cusOrderShipAddrObject.put("shipAddrStateName", orderDetails.getShStateName());
					cusOrderShipAddrObject.put("shipAddrCity", orderDetails.getShCity());
					cusOrderShipAddrObject.put("shipAddrZipCode", orderDetails.getShZipCode());			
					jObj.put("order", cusOrderShipAddrObject);
				}
				if(countryList != null){
					JSONArray countryListArray = new JSONArray();
					JSONObject countryListObject = null;
					for(Country country : countryList){
						countryListObject = new JSONObject();
						countryListObject.put("id", country.getId());
						countryListObject.put("name", country.getName());
						countryListArray.put(countryListObject);
					}
					jObj.put("countryList", countryListArray);
				}
				if(stateList != null){
					JSONArray stateListArray = new JSONArray();
					JSONObject stateListObject = null;
					for(State state : stateList){
						stateListObject = new JSONObject();
						stateListObject.put("id", state.getId());
						stateListObject.put("name", state.getName());
						stateListArray.put(stateListObject);
					}
					jObj.put("stateList", stateListArray);
				}
				jObj.put("invoiceId", invoiceIdStr);
				jObj.put("status", 1);
				
				//return jsonArr;
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
			}catch(Exception e){
				e.printStackTrace();
				model.addAttribute("msg", "There is something wrong.Please try again..");
			}
			return "";
		}
	 
	 
	@RequestMapping({"/GetCustomerOrderBillingAddress"})
	public String getCustomerOrderBillingAddress(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
				
		try{
			String action = request.getParameter("action");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String street1 = request.getParameter("street1");
			String street2 = request.getParameter("street2");
			String city = request.getParameter("city");
			String zipcode = request.getParameter("zipcode");
			String phone = request.getParameter("phone");
			String email = request.getParameter("email");
			String state = request.getParameter("state");
			String country = request.getParameter("country");
			String orderId = request.getParameter("orderId");			
			String invoiceId = request.getParameter("invoiceId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("firstName", firstName);
			map.put("lastName", lastName);
			map.put("street1", street1);
			map.put("street2", street2);
			map.put("city", city);
			map.put("zipcode", zipcode);
			map.put("phone", phone);
			map.put("email", email);
			map.put("state", state);
			map.put("country", country);
			map.put("orderId", orderId);
			map.put("invoiceId", invoiceId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_ORDER_BILLING_ADDRESS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceCustomerOrderAddressDTO invoiceCustomerOrderAddressDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceCustomerOrderAddressDTO")), InvoiceCustomerOrderAddressDTO.class);
			
			if(invoiceCustomerOrderAddressDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceCustomerOrderAddressDTO.getMessage());
				model.addAttribute("status", invoiceCustomerOrderAddressDTO.getStatus());
				model.addAttribute("order", invoiceCustomerOrderAddressDTO.getCustomerOrderAddressDTO());
				model.addAttribute("countryList", invoiceCustomerOrderAddressDTO.getCountryDTO());
				model.addAttribute("stateList", invoiceCustomerOrderAddressDTO.getStateDTO());
			}else{
				model.addAttribute("msg", invoiceCustomerOrderAddressDTO.getError().getDescription());
				model.addAttribute("status", invoiceCustomerOrderAddressDTO.getStatus());
			}
			model.addAttribute("invoiceId", invoiceCustomerOrderAddressDTO.getInvoiceId());
			
			/*List<Country> countryList = null;
			List<State> stateList = null;
			CustomerOrderDetails orderDetails = null;		
			String msg = "";
			Integer orderId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
//			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
//				 jObj.put("msg", "Invoice ID not found.");
//				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
//				 return;
//			}
			
			if(orderIdStr == null || orderIdStr.isEmpty()){
				 jObj.put("msg", "Customer Order ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				orderId = Integer.parseInt(orderIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Customer Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}		
						
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(orderId, brokerId);
			if(customerOrder == null){
				jObj.put("msg", "Customer Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			if(orderDetails == null){
				jObj.put("msg", "Customer Order Details not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			countryList = DAORegistry.getQueryManagerDAO().getCountries();
			stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(orderDetails.getShCountry());
			if(action!=null && action.equalsIgnoreCase("update")){						
				orderDetails.setBlFirstName(request.getParameter("firstName"));
				orderDetails.setBlLastName(request.getParameter("lastName"));
				orderDetails.setBlAddress1(request.getParameter("street1"));
				orderDetails.setBlAddress2(request.getParameter("street2"));
				orderDetails.setBlCity(request.getParameter("city"));
				orderDetails.setBlZipCode(request.getParameter("zipcode"));
				orderDetails.setBlPhone1(request.getParameter("phone"));
				orderDetails.setBlEmail(request.getParameter("email"));
				Integer countryId = Integer.parseInt(request.getParameter("country"));
				
				stateList = DAORegistry.getQueryManagerDAO().getStatesForCountry(countryId);
				String stateId = request.getParameter("state");
				if(stateId!=null && !stateId.isEmpty()){
					int id= Integer.parseInt(stateId);
					for(State state : stateList){
						if(id==state.getId()){
							orderDetails.setBlState(id);
							orderDetails.setBlStateName(state.getName());
							break;
						}
					}
				}
				String country = request.getParameter("country");
				if(country!=null && !country.isEmpty()){
					int id= Integer.parseInt(country);
					for(Country count : countryList){
						if(id==count.getId()){
							orderDetails.setBlCountry(id);
							orderDetails.setBlCountryName(count.getName());
							break;
						}
					}
				}
				DAORegistry.getCustomerOrderDetailsDAO().update(orderDetails);
				//jObj.put("successMessage", "Billing address updated successfully.");
				msg = "Billing address updated successfully.";
				jObj.put("status", 1);
				
				//Tracking User Action
				String userActionMsg = "Customer Order - Billing Address Updated Successfully.";
				Util.userActionAudit(request, orderDetails.getId(), userActionMsg);
			}	
			
			if(orderDetails != null){
				JSONObject cusOrderBillAddrObject = new JSONObject();
				cusOrderBillAddrObject.put("billAddrOrderId", orderDetails.getOrderId());
				cusOrderBillAddrObject.put("billAddrFirstName", orderDetails.getBlFirstName());
				cusOrderBillAddrObject.put("billAddrLastName", orderDetails.getBlLastName());
				cusOrderBillAddrObject.put("billAddrAddressLine1", orderDetails.getBlAddress1());
				cusOrderBillAddrObject.put("billAddrAddressLine2", orderDetails.getBlAddress2());
				cusOrderBillAddrObject.put("billAddrEmail", orderDetails.getBlEmail());	
				cusOrderBillAddrObject.put("billAddrPhone1", orderDetails.getBlPhone1());
				cusOrderBillAddrObject.put("billAddrCountry", orderDetails.getBlCountry());
				cusOrderBillAddrObject.put("billAddrCountryName", orderDetails.getBlCountryName());
				cusOrderBillAddrObject.put("billAddrState", orderDetails.getBlState());
				cusOrderBillAddrObject.put("billAddrStateName", orderDetails.getBlStateName());
				cusOrderBillAddrObject.put("billAddrCity", orderDetails.getBlCity());
				cusOrderBillAddrObject.put("billAddrZipCode", orderDetails.getBlZipCode());			
				jObj.put("order", cusOrderBillAddrObject);
			}
			if(countryList != null){
				JSONArray countryListArray = new JSONArray();
				JSONObject countryListObject = null;
				for(Country country : countryList){
					countryListObject = new JSONObject();
					countryListObject.put("id", country.getId());
					countryListObject.put("name", country.getName());
					countryListArray.put(countryListObject);
				}
				jObj.put("countryList", countryListArray);
			}
			if(stateList != null){
				JSONArray stateListArray = new JSONArray();
				JSONObject stateListObject = null;
				for(State state : stateList){
					stateListObject = new JSONObject();
					stateListObject.put("id", state.getId());
					stateListObject.put("name", state.getName());
					stateListArray.put(stateListObject);
				}
				jObj.put("stateList", stateListArray);
			};
			jObj.put("invoiceId", invoiceIdStr);
			jObj.put("msg", msg);
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value="/GetRelatedPOInvoices")
	public String getRelatedPOInvoices(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String poId = request.getParameter("poId");
			String invoiceId = request.getParameter("invoiceId");
			String productType = request.getParameter("productType");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("poId", poId);
			map.put("invoiceId", invoiceId);
			map.put("productType", productType);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_RELATED_PO_INVOICES);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RelatedPOInvoicesDTO relatedPOInvoicesDTO = gson.fromJson(((JsonObject)jsonObject.get("relatedPOInvoicesDTO")), RelatedPOInvoicesDTO.class);
			
			if(relatedPOInvoicesDTO.getStatus() == 1){
				model.addAttribute("msg", relatedPOInvoicesDTO.getMessage());
				model.addAttribute("status", relatedPOInvoicesDTO.getStatus());
				model.addAttribute("poPagingInfo", relatedPOInvoicesDTO.getPoPaginationDTO());
				model.addAttribute("poList", relatedPOInvoicesDTO.getRelatedPODTO());
				model.addAttribute("pagingInfo", relatedPOInvoicesDTO.getInvoicePaginationDTO());
				model.addAttribute("ticketList", relatedPOInvoicesDTO.getRelatedInvoiceDTO());
			}else{
				model.addAttribute("msg", relatedPOInvoicesDTO.getError().getDescription());
				model.addAttribute("status", relatedPOInvoicesDTO.getStatus());
			}
			model.addAttribute("productType", relatedPOInvoicesDTO.getSelectedProductType());
			
			/*String poId = request.getParameter("poId");
			String invoiceId = request.getParameter("invoiceId");
			String productType = request.getParameter("productType");
			List<PurchaseOrder> poList = new ArrayList<PurchaseOrder>();
			List<POSPurchaseOrder> posList = new ArrayList<POSPurchaseOrder>();
			List<Ticket> ticketList = null;
			List<POSTicket> posTicketList = null;
			Map<Integer, EventDetails> ticketGroupEventMap = new HashMap<Integer, EventDetails>();
			Map<Integer, EventDetails> posTicketGroupEventMap = new HashMap<Integer, EventDetails>();
			TicketGroup ticGroup=null;
			POSTicketGroup posTicGroup = null;
			EventDetails event = null;
			JSONObject jObj = new JSONObject();
			if(productType.equalsIgnoreCase("REWARDTHEFAN") || productType.equalsIgnoreCase("SEATGEEK")){
			if(poId!=null && !poId.isEmpty()){
				PurchaseOrder po = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(Integer.parseInt(poId), brokerId);
				if(po!=null){
					ticketList = DAORegistry.getTicketDAO().getTIcketsByPOId(po.getId());
					po.setTotalQuantity(ticketList.size());
					ticketList = DAORegistry.getTicketDAO().getMappedTicketByPOId(po.getId());
					po.setUsedQunatity(ticketList.size());
					Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
					po.setCustomerName(customer.getCustomerName());
					if(ticketList!=null && !ticketList.isEmpty()){
						for(Ticket ticket:ticketList){
							ticGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							ticket.setSection(ticGroup.getSection());
							ticket.setRow(ticGroup.getRow());
							if(ticketGroupEventMap.get(ticket.getTicketGroupId())!=null){
								event = ticketGroupEventMap.get(ticket.getTicketGroupId());
							}else{
								event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
								ticketGroupEventMap.put(ticket.getTicketGroupId(),event);
							}
							if(event!=null){
								ticket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
							}
						}
					}
					poList.add(po);
				}else{
					jObj.put("successMessage", "No Invoices found for selected PO.");
				}
			}else if(invoiceId!=null && !invoiceId.isEmpty()){
				Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceId), brokerId);
				if(invoice != null){
					ticketList = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceId));
					if(ticketList!=null && !ticketList.isEmpty()){
						List<Integer> poIds = new ArrayList<Integer>();
						for(Ticket ticket:ticketList){
							 ticGroup = DAORegistry.getTicketGroupDAO().get(ticket.getTicketGroupId());
							 ticket.setSection(ticGroup.getSection());
							 ticket.setRow(ticGroup.getRow());
							if(ticketGroupEventMap.get(ticket.getTicketGroupId())!=null){
								event = ticketGroupEventMap.get(ticket.getTicketGroupId());
							}else{
								event = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
								ticketGroupEventMap.put(ticket.getTicketGroupId(),event);
							}
							if(event!=null){
								ticket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
							}
							if(!poIds.contains(ticket.getPoId())){
								poIds.add(ticket.getPoId());
							}
						}
						List<Ticket> tickets = null;
						for(Integer id : poIds){
							PurchaseOrder po = DAORegistry.getPurchaseOrderDAO().get(id);
							tickets = DAORegistry.getTicketDAO().getTIcketsByPOId(id);
							po.setTotalQuantity(tickets.size());
							tickets = DAORegistry.getTicketDAO().getMappedTicketByPOId(id);
							po.setUsedQunatity(tickets.size());
							Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
							if(customer != null){
								po.setCustomerName(customer.getCustomerName());
							}
							poList.add(po);
						}
					}else{
						jObj.put("successMessage", "No PO(s) found for selected invoice.");
					}
				}else{
					jObj.put("errorMessage", "Require Invoice not found.");
				}
			}else{
				jObj.put("errorMessage", "Require POId/InvoiceId not found.");
			}
			}
			if(productType.equalsIgnoreCase("RTW") || productType.equalsIgnoreCase("RTW2")){
				if(poId!=null && !poId.isEmpty()){ 
					POSPurchaseOrder pos = DAORegistry.getPosPurchaseOrderDAO().getPOSPurchaseOrderDAOByPurchaseOrderId(Integer.parseInt(poId), productType);
					if(pos!=null){
						posTicketList = DAORegistry.getPosTicketDAO().getPOSTicketByPurchaseOrderId(pos.getPurchaseOrderId(), productType);
						pos.setTotalQuantity(posTicketList.size());
						posTicketList = DAORegistry.getPosTicketDAO().getMappedPOSTicketByPurchaseOrderId(pos.getPurchaseOrderId(), productType);
						pos.setUsedQunatity(posTicketList.size());
						//Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
						//po.setCustomerName(customer.getCustomerName());
						if(posTicketList!=null && !posTicketList.isEmpty()){
							for(POSTicket posTicket:posTicketList){
								posTicGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupIdWithPdt(posTicket.getTicketGroupId(), productType);
								posTicket.setSection(posTicGroup.getSection());
								posTicket.setRow(posTicGroup.getRow());
								if(posTicketGroupEventMap.get(posTicket.getTicketGroupId())!=null){
									event = posTicketGroupEventMap.get(posTicket.getTicketGroupId());
								}else{
									event = DAORegistry.getEventDetailsDAO().get(posTicGroup.getEventId());
									posTicketGroupEventMap.put(posTicket.getTicketGroupId(),event);
								}
								if(event!=null){
									//posTicket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
								}
							}
						}
						posList.add(pos);
					}else{
						jObj.put("successMessage", "No Invoices found for selected PO.");
					}
				}else if(invoiceId!=null && !invoiceId.isEmpty()){ 
					posTicketList = DAORegistry.getPosTicketDAO().getMappedPOSTicketByInvoiceId(Integer.parseInt(invoiceId));
					if(posTicketList!=null && !posTicketList.isEmpty()){
						List<Integer> poIds = new ArrayList<Integer>();						
						for(POSTicket posTicket:posTicketList){
							posTicGroup = DAORegistry.getPosTicketGroupDAO().getPOSTicketGroupByTicketGroupIdWithPdt(posTicket.getTicketGroupId(),productType);
							posTicket.setSection(posTicGroup.getSection());
							posTicket.setRow(posTicGroup.getRow());
							if(posTicketGroupEventMap.get(posTicket.getTicketGroupId())!=null){
								event = posTicketGroupEventMap.get(posTicket.getTicketGroupId());
							}else{
								event = DAORegistry.getEventDetailsDAO().get(posTicGroup.getEventId());
								posTicketGroupEventMap.put(posTicket.getTicketGroupId(),event);
							}
							if(event!=null){
								//posTicket.setEventNameString(event.getEventName()+" "+event.getEventDateTimeStr());
							}
							if(!poIds.contains(posTicket.getPurchaseOrderId())){
								poIds.add(posTicket.getPurchaseOrderId());
							}
						}
						List<POSTicket> posTickets = null;
						for(Integer id : poIds){
							POSPurchaseOrder pos = DAORegistry.getPosPurchaseOrderDAO().getPOSPurchaseOrderDAOByPurchaseOrderId(id, productType);
							if(pos != null){
							posTickets = DAORegistry.getPosTicketDAO().getPOSTicketByPurchaseOrderId(id, productType);							
							pos.setTotalQuantity(posTickets.size());
							posTickets = DAORegistry.getPosTicketDAO().getMappedPOSTicketByPurchaseOrderId(id, productType);
							pos.setUsedQunatity(posTickets.size());
							//Customer customer = DAORegistry.getCustomerDAO().get(po.getCustomerId());
							//po.setCustomerName(customer.getCustomerName());
							posList.add(pos);
							}else{
								jObj.put("successMessage", "No PO(s) found for selected invoice.");
							}
						}
					}else{
						jObj.put("successMessage", "No PO(s) found for selected invoice.");
					}
				}else{
					jObj.put("errorMessage", "Require POId/InvoiceId not found.");
				}
			}
			jObj.put("productType", productType);
			if(ticketList!= null){
				jObj.put("poList", JsonWrapperUtil.getRelatedPOArray(poList, null));
				jObj.put("poPagingInfo",PaginationUtil.getDummyPaginationParameter(poList!=null?poList.size():0));
				jObj.put("ticketList", JsonWrapperUtil.getRelatedInvoicesArray(ticketList, null));
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(ticketList!=null?ticketList.size():0));
			}
			if(posTicketList!= null){
				jObj.put("poList", JsonWrapperUtil.getRelatedPOArray(null, posList));
				jObj.put("poPagingInfo",PaginationUtil.getDummyPaginationParameter(posList!=null?posList.size():0));
				jObj.put("ticketList", JsonWrapperUtil.getRelatedInvoicesArray(null, posTicketList));
				jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(posTicketList!=null?posTicketList.size():0));
			}
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}		
	
	@RequestMapping(value = "/GetInvoiceNote")
	public String getInvoiceNote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String invoiceId = request.getParameter("invoiceId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_INVOICE_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceNoteDTO invoiceNoteDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceNoteDTO")), InvoiceNoteDTO.class);
			
			if(invoiceNoteDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceNoteDTO.getMessage());
				model.addAttribute("status", invoiceNoteDTO.getStatus());
				model.addAttribute("id", invoiceNoteDTO.getId());
				model.addAttribute("internalNote", invoiceNoteDTO.getInternalNotes());
			}else{
				model.addAttribute("msg", invoiceNoteDTO.getError().getDescription());
				model.addAttribute("status", invoiceNoteDTO.getStatus());
			}
			
			/*Integer invoiceId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				 jObj.put("msg", "Invoice ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Invoice ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(invoiceId, brokerId);
			if(invoice == null){
				jObj.put("msg", "Invoice not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
						
			jObj.put("id", invoice.getId());
			jObj.put("internalNote", invoice.getInternalNote());
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateInvoiceNote")
	public String updateInvoiceNote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			String modifyNotesInvoiceId = request.getParameter("modifyNotesInvoiceId");
			String modifyNotesInvoiceNote = request.getParameter("modifyNotesInvoiceNote");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("modifyNotesInvoiceId", modifyNotesInvoiceId);
			map.put("modifyNotesInvoiceNote", modifyNotesInvoiceNote);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_INVOICE_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceNoteDTO invoiceNoteDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceNoteDTO")), InvoiceNoteDTO.class);
			
			if(invoiceNoteDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceNoteDTO.getMessage());
				model.addAttribute("status", invoiceNoteDTO.getStatus());
			}else{
				model.addAttribute("msg", invoiceNoteDTO.getError().getDescription());
				model.addAttribute("status", invoiceNoteDTO.getStatus());
			}
			
			/*Invoice invoice = null;
			String returnMessage = "";
			Integer invoiceId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(invoiceIdStr == null || invoiceIdStr.isEmpty()){
				 jObj.put("msg", "Invoice ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				invoiceId = Integer.parseInt(invoiceIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Invoice ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(invoiceId, brokerId);
			if(invoice == null){
				jObj.put("msg", "Invoice not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
				
			if(action!=null && action.equalsIgnoreCase("update")){
				invoice.setInternalNote(request.getParameter("modifyNotesInvoiceNote"));
				DAORegistry.getInvoiceDAO().update(invoice);
				returnMessage = "Invoice Note updated Successfully.";
			}				
			jObj.put("msg", returnMessage);
			jObj.put("status", 1);
			
			//Tracking User Action
			String userActionMsg = returnMessage;
			Util.userActionAudit(request, invoice.getId(), userActionMsg);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value="/GetPONote")
	public String getPONote(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String action = request.getParameter("action");
			String poId = request.getParameter("poId");
			String internalNote = request.getParameter("internalNote");
			String externalNote = request.getParameter("externalNote");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("poId", poId);
			map.put("internalNote", internalNote);
			map.put("externalNote", externalNote);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_UPDATE_PO_NOTE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PONotesDTO poNotesDTO = gson.fromJson(((JsonObject)jsonObject.get("PONotesDTO")), PONotesDTO.class);
			
			if(poNotesDTO.getStatus() == 1){
				model.addAttribute("msg", poNotesDTO.getMessage());
				model.addAttribute("status", poNotesDTO.getStatus());
				model.addAttribute("poId", poNotesDTO.getPoId());
				model.addAttribute("internalNote", poNotesDTO.getInternalNote());
				model.addAttribute("externalNote", poNotesDTO.getExternalNote());
			}else{
				model.addAttribute("msg", poNotesDTO.getError().getDescription());
				model.addAttribute("status", poNotesDTO.getStatus());
			}
			/*PurchaseOrder po=null;
			Integer poId = 0;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);			
			
			if(poIdStr == null || poIdStr.isEmpty()){
				 jObj.put("msg", "Purchase Order ID not found.");
				 IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				 return;
			}
			try{
				poId = Integer.parseInt(poIdStr);
			}catch(Exception e){
				e.printStackTrace();
				jObj.put("msg", "Purchase Order ID should be valid Integer value.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}			
			
			po = DAORegistry.getPurchaseOrderDAO().getPurchaseOrderByIdAndBrokerId(poId, brokerId);
			if(po == null){
				jObj.put("msg", "Purchase Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
					
			if(action!=null && action.equalsIgnoreCase("update")){
				po.setInternalNotes(request.getParameter("internalNote"));
				po.setExternalNotes(request.getParameter("externalNote"));
				DAORegistry.getPurchaseOrderDAO().update(po);
				jObj.put("msg", "PO Notes updated Successfully.");
				
				//Tracking User Action
				String userActionMsg = "PO Note(s) Updated Successfully.";
				Util.userActionAudit(request, poId, userActionMsg);
			}
			jObj.put("internalNote",po.getInternalNotes());
			jObj.put("externalNote",po.getExternalNotes());
			jObj.put("poId", poIdStr);
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value = "/GetInvoiceDownloadTicket")
	public String getInvoiceDownloadTicket(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Download Ticket request processing....");
		
		try{
			String invoiceId = request.getParameter("invoiceId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_INVOICE_TICKETS_DOWNLOAD_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceDownloadTicketDTO invoiceDownloadTicketDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceDownloadTicketDTO")), InvoiceDownloadTicketDTO.class);
			
			if(invoiceDownloadTicketDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceDownloadTicketDTO.getMessage());
				model.addAttribute("status", invoiceDownloadTicketDTO.getStatus());
				model.addAttribute("pagingInfo", invoiceDownloadTicketDTO.getTicketDownloadPaginationDTO());
				model.addAttribute("ticketDownload", invoiceDownloadTicketDTO.getTicketsDownloadDTO());								
			}else{
				model.addAttribute("msg", invoiceDownloadTicketDTO.getError().getDescription());
				model.addAttribute("status", invoiceDownloadTicketDTO.getStatus());
			}
			model.addAttribute("invoiceId", invoiceDownloadTicketDTO.getInvoiceId());
			
			/*List<CustomerTicketDownloads> customerTicketDownloadList = null;			
			JSONObject jObj = new JSONObject();
			
			invoiceId = request.getParameter("invoiceId");
			if(invoiceId == null){
				jObj.put("errorMessage", "InvoiceId Not Found.");
			}else{
				customerTicketDownloadList = DAORegistry.getCustomerTicketDownloadsDAO().getCustomerTicketDownloadsByInvoiceId(Integer.parseInt(invoiceId));
				if(customerTicketDownloadList == null || customerTicketDownloadList.isEmpty()){
					jObj.put("successMessage", "There is No Tickets Downloaded for Selected Invoice.");
				}
				if(customerTicketDownloadList != null && customerTicketDownloadList.size() > 0){
					for(CustomerTicketDownloads customerTicketDownload : customerTicketDownloadList){
						Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(customerTicketDownload.getInvoiceId(), brokerId);
						if(invoice != null){
							CustomerOrderDetails customerOrderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
							if(customerOrderDetails != null && customerOrderDetails.getShEmail() != null){
								customerTicketDownload.setEmail(customerOrderDetails.getShEmail());
							}else{
								Customer customer = DAORegistry.getCustomerDAO().get(customerTicketDownload.getCustomerId());
								customerTicketDownload.setEmail(customer.getEmail());								
							}
						}else{
							jObj.put("errorMessage", "Invoice Not Found.");
						}
					}
				}
			}
			if(customerTicketDownloadList != null){
				JSONArray cusTicDownloadArray = new JSONArray();
				JSONObject cusTicDownloadObject = null;
				for(CustomerTicketDownloads customerTicketDownload : customerTicketDownloadList){
					cusTicDownloadObject = new JSONObject();
					cusTicDownloadObject.put("id", customerTicketDownload.getId());
					cusTicDownloadObject.put("invoiceId", customerTicketDownload.getInvoiceId());
					cusTicDownloadObject.put("customerId", customerTicketDownload.getCustomerId());
					cusTicDownloadObject.put("ticketType", customerTicketDownload.getTicketType());
					cusTicDownloadObject.put("ticketName", customerTicketDownload.getTicketName());
					cusTicDownloadObject.put("email", customerTicketDownload.getEmail());
					cusTicDownloadObject.put("ipAddress", customerTicketDownload.getIpAddress());
					cusTicDownloadObject.put("downloadPlatform", customerTicketDownload.getDownloadPlatform());
					cusTicDownloadObject.put("downloadDateTime", customerTicketDownload.getDownloadDateTimeStr());
					cusTicDownloadArray.put(cusTicDownloadObject);
				}
				jObj.put("ticketDownload", cusTicDownloadArray);
			}			
			jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(customerTicketDownloadList!=null?customerTicketDownloadList.size():0));
			jObj.put("invoiceId",invoiceId);
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value = "/GetInvoiceRefund")
	public String getInvoiceRefund(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){		
		
		try{
			String orderId = request.getParameter("orderId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("orderId", orderId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_INVOICE_REFUND_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceRefundDTO invoiceRefundDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundDTO")), InvoiceRefundDTO.class);
			
			if(invoiceRefundDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceRefundDTO.getMessage());
				model.addAttribute("status", invoiceRefundDTO.getStatus());
				model.addAttribute("pagingInfo", invoiceRefundDTO.getInvoiceRefundPaginationDTO());
				model.addAttribute("invoiceRefund", invoiceRefundDTO.getInvoiceRefundListDTO());								
			}else{
				model.addAttribute("msg", invoiceRefundDTO.getError().getDescription());
				model.addAttribute("status", invoiceRefundDTO.getStatus());
			}
			model.addAttribute("orderId", invoiceRefundDTO.getOrderId());
			
			/*List<InvoiceRefund> invoiceRefundList = null;
			JSONObject jObj = new JSONObject();
			
			if(orderId == null){
				jObj.put("errorMessage", "Order ID Not Found.");
			}else{
				
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(Integer.parseInt(orderId), brokerId);				
				if(customerOrder != null){
					invoiceRefundList = DAORegistry.getInvoiceRefundDAO().getInvoiceRefundByOrderId(Integer.parseInt(orderId));
					if(invoiceRefundList == null || invoiceRefundList.isEmpty()){
						jObj.put("successMessage", "There is No Refund are available for selected Invoice.");
					}
				}else{
					jObj.put("errorMessage", "Order not found for selected invoice.");
				}
			}
			if(invoiceRefundList != null){
				JSONArray invoiceRefundArray = new JSONArray();
				JSONObject invocieRefundObject = null;
				for(InvoiceRefund invoiceRefund : invoiceRefundList){
					invocieRefundObject = new JSONObject();
					invocieRefundObject.put("invoiceRefundId", invoiceRefund.getId());
					invocieRefundObject.put("refundType", invoiceRefund.getRefundType());
					invocieRefundObject.put("refundOrderId", invoiceRefund.getOrderId());
					invocieRefundObject.put("refundTransactionId", invoiceRefund.getTransactionId());
					invocieRefundObject.put("refundId", invoiceRefund.getRefundId());
					invocieRefundObject.put("refundAmount", invoiceRefund.getAmount());
					invocieRefundObject.put("refundStatus", invoiceRefund.getStatus());
					invocieRefundObject.put("refundReason", invoiceRefund.getReason());
					invocieRefundObject.put("revertedUsedRewardPoints", invoiceRefund.getRevertedUsedRewardPoints());
					invocieRefundObject.put("revertedEarnedRewardPoints", invoiceRefund.getRevertedEarnedRewardPoints());
					invocieRefundObject.put("refundCreatedTime", invoiceRefund.getCreatedTimeStr());
					invocieRefundObject.put("refundedBy", invoiceRefund.getRefundedBy());
					invoiceRefundArray.put(invocieRefundObject);
				}
				jObj.put("invoiceRefund", invoiceRefundArray);
			}
			jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(invoiceRefundList!=null?invoiceRefundList.size():0));			
			jObj.put("orderId",orderId);
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value="/ValidateReferralCode")
	 public String validateReferalCode(HttpServletRequest request,HttpServletResponse response, Model model){
		
		try {
			String isLongSale = request.getParameter("isLongSale");
			String orderTotal = request.getParameter("orderTotal");
			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_VALIDATE_REFERRAL_CODE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ValidateReferralCodeDTO validateReferralCodeDTO = gson.fromJson(((JsonObject)jsonObject.get("validateReferralCodeDTO")), ValidateReferralCodeDTO.class);
			
			if(validateReferralCodeDTO.getStatus() == 1){
				model.addAttribute("msg", validateReferralCodeDTO.getMessage());
				model.addAttribute("status", validateReferralCodeDTO.getStatus());
				model.addAttribute("rewardPoints", validateReferralCodeDTO.getRewardPoints());
				if(isLongSale.equalsIgnoreCase("true")){
					model.addAttribute("price", validateReferralCodeDTO.getLongSalePriceList());
					model.addAttribute("promoTrackingId", validateReferralCodeDTO.getPromoTrackingId());
					model.addAttribute("affPromoTrackingId", validateReferralCodeDTO.getAffPromoTrackingId());
				}else{
					model.addAttribute("price", validateReferralCodeDTO.getPrice());
				}
				model.addAttribute("orderTotal", orderTotal);
			}else{
				model.addAttribute("msg", validateReferralCodeDTO.getError().getDescription());
				model.addAttribute("status", validateReferralCodeDTO.getStatus());
			}
			/*String referalCode = request.getParameter("referralCode");
			String customerIdStr = request.getParameter("customerId");
			String eventIdStr = request.getParameter("eventId");
			String ticketGroupIdStr = request.getParameter("ticketGroupId");
			String isFanPrice = request.getParameter("isFanPrice");
			String clientIpAddress = request.getRemoteAddr();
			//System.out.println(referalCode+" , "+customerId);
			String msg = "";
			JSONObject object = new JSONObject();
			object.put("status",0);
			
			if(referalCode==null || referalCode.isEmpty()){
				IOUtils.write("Please enter referalCode.".getBytes(), response.getOutputStream());
				return;
			}
			if(customerIdStr==null || customerIdStr.isEmpty()){
				IOUtils.write("Customer Id is not found.".getBytes(), response.getOutputStream());
				return;
			}
			if(eventIdStr==null || eventIdStr.isEmpty()){
				IOUtils.write("Event Id is not found.".getBytes(), response.getOutputStream());
				return;
			}
			if(ticketGroupIdStr==null || ticketGroupIdStr.isEmpty()){
				IOUtils.write("Ticket Group Id is not found.".getBytes(), response.getOutputStream());
				return;
			}
			
			if(orderTotal==null || orderTotal.isEmpty()){
				IOUtils.write("Order Total is not found.".getBytes(), response.getOutputStream());
				return;
			}
			boolean isValidOrderTotal = true;
			try {
				orderTotal = Util.formatDoubleToTwoDecimalPoint(Double.parseDouble(orderTotal));
			} catch (Exception e) {
				isValidOrderTotal = false;
				e.printStackTrace();
			}
			
			if(!isValidOrderTotal){
				IOUtils.write("Invalid Order Total.".getBytes(), response.getOutputStream());
				return;
			}
			HttpSession session = request.getSession();
			Map<String, String> map = Util.getParameterMap(request);
		    map.put("referralCode", referalCode);
		    map.put("customerId",customerIdStr);
		    map.put("eventId", eventIdStr);
		    map.put("tgId", ticketGroupIdStr);
		    map.put("clientIPAddress", clientIpAddress);
		    map.put("isLongSale", isLongSale);
		    map.put("isFanPrice", isFanPrice);
		    map.put("orderTotal", Util.formatDoubleToTwoDecimalPoint(Double.parseDouble(orderTotal)));
		    map.put("sessionId", session.getId());
		   
		    String data = null;
		    if(isLongSale.equalsIgnoreCase("true")){
		    	String totalQuantity = request.getParameter("totalQty");
		    	String ticketSize = request.getParameter("ticketSize");
		    	if(totalQuantity==null || totalQuantity.isEmpty()){
					IOUtils.write("Order Quantity is not found.".getBytes(), response.getOutputStream());
					return;
				}
		    	if(ticketSize==null || ticketSize.isEmpty()){
					IOUtils.write("Ticket group size is not found.".getBytes(), response.getOutputStream());
					return;
				}
				Integer totalQty = 0;
				try {
					totalQty = Integer.parseInt(totalQuantity);
				} catch (Exception e) {
					e.printStackTrace();
					IOUtils.write("Invalid Order Quantity.".getBytes(), response.getOutputStream());
					return;
				}
				Integer ticSize = 0;
				try {
					ticSize = Integer.parseInt(ticketSize);
				} catch (Exception e) {
					e.printStackTrace();
					IOUtils.write("Invalid Ticket group size.".getBytes(), response.getOutputStream());
					return;
				}
				
				map.put("orderQty",totalQuantity);
		    	data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.VALIDATE_PROMOCODE_REAL_TICKET);
		    	Gson gson = new Gson();  
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
				if(referralCodeValidation.getStatus()==1){
					msg = referralCodeValidation.getMessage();
					object.put("rewardPoints",referralCodeValidation.getRewardPointsAsDouble());
					JSONArray jArray = new JSONArray();
					for(int i=0;i<ticSize;i++){
						//String quantity = request.getParameter("quantity_"+i);
						String priceStr = request.getParameter("price_"+i);
						if(priceStr==null || priceStr.isEmpty()){
							IOUtils.write("Sold Price is not found.".getBytes(), response.getOutputStream());
							return;
						}
						Double tPrice = 0.00;
						try {
							tPrice = Double.parseDouble(priceStr);
						} catch (Exception e) {
							e.printStackTrace();
							IOUtils.write("Sold price is not valid Numeric value.".getBytes(), response.getOutputStream());
							return;
						}
						if(referralCodeValidation.getCodeType().equals(CodeType.PROMOTIONAL)){
							if(referralCodeValidation.getIsFlatDiscount()!= null && referralCodeValidation.getIsFlatDiscount()){
								tPrice  =  (tPrice - referralCodeValidation.getDiscountConv());
								jArray.put(Util.formatDoubleToTwoDecimalPoint(tPrice));
							}else{
								tPrice  = tPrice - (tPrice * referralCodeValidation.getDiscountConv());
								jArray.put(Util.formatDoubleToTwoDecimalPoint(tPrice));
							}
						}else{
							jArray.put(Util.formatDoubleToTwoDecimalPoint(tPrice));
						}
					}
					object.put("promoTrackingId", referralCodeValidation.getPromoTrackingId());
					object.put("affPromoTrackingId",referralCodeValidation.getAffPromoTrackingId());
					object.put("price",jArray);
					object.put("status",1);
				}else{
					msg = referralCodeValidation.getError().getDescription();
				}
		    }else{
		    	String quantity = request.getParameter("quantity");
				String priceStr = request.getParameter("price");
				if(quantity==null || quantity.isEmpty()){
					IOUtils.write("Quantity is not found.".getBytes(), response.getOutputStream());
					return;
				}
				if(priceStr==null || priceStr.isEmpty()){
					IOUtils.write("Sold Price is not found.".getBytes(), response.getOutputStream());
					return;
				}
				map.put("orderQty",quantity);
		    	data = Util.getObject(map,sharedProperty.getApiUrl()+Constants.VALIDATE_PROMOCODE);
		    	Gson gson = new Gson();  
				JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
				ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
				if(referralCodeValidation.getStatus()==1){
					object.put("status",1);
					msg = referralCodeValidation.getMessage();
					object.put("rewardPoints",referralCodeValidation.getRewardPointsAsDouble());
					Double tPrice = Double.parseDouble(priceStr);
					if(referralCodeValidation.getCodeType().equals(CodeType.PROMOTIONAL)){
						if(referralCodeValidation.getIsFlatDiscount()!= null && referralCodeValidation.getIsFlatDiscount()){
							tPrice  =  (tPrice - referralCodeValidation.getDiscountConv());
							object.put("price",Util.formatDoubleToTwoDecimalPoint(tPrice));
						}else{
							tPrice  = tPrice - (tPrice * referralCodeValidation.getDiscountConv());
							object.put("price",Util.formatDoubleToTwoDecimalPoint(tPrice));
						}
					}else{
						object.put("price",Util.formatDoubleToTwoDecimalPoint(tPrice));
					}
				}else{
					msg = referralCodeValidation.getError().getDescription();
				}
		    }
			object.put("msg",msg);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value="/CreateInvoiceRefundOrCredit")
	public String createInvoiceRefundOrCredit(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String action = request.getParameter("action");
			String orderId = request.getParameter("orderId");
			String refundAmount = request.getParameter("refundAmount");
			String creditWalletAmount = request.getParameter("creditWalletAmount");
			String creditAmount = request.getParameter("creditAmount");
			String rewardPoints = request.getParameter("rewardPoints");
			String note = request.getParameter("refundNote");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("orderId", orderId);
			map.put("refundAmount", refundAmount);
			map.put("creditWalletAmount", creditWalletAmount);
			map.put("creditAmount", creditAmount);
			map.put("rewardPoints", rewardPoints);
			map.put("note", note);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_INVOICE_REFUND_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceRefundDetailsDTO invoiceRefundDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundDetailsDTO")), InvoiceRefundDetailsDTO.class);
			
			if(invoiceRefundDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceRefundDetailsDTO.getMessage());
				model.addAttribute("status", invoiceRefundDetailsDTO.getStatus());
				model.addAttribute("order", invoiceRefundDetailsDTO.getCustomerOrderDTO());
				model.addAttribute("invoice", invoiceRefundDetailsDTO.getInvoiceDTO());
				model.addAttribute("customer", invoiceRefundDetailsDTO.getCustomerDTO());
			}else{
				model.addAttribute("msg", invoiceRefundDetailsDTO.getError().getDescription());
				model.addAttribute("status", invoiceRefundDetailsDTO.getStatus());
			}
			model.addAttribute("isValid", invoiceRefundDetailsDTO.getIsValid());
			
			/*Double primaryAmountToDeduct=0.0;
			Double secondaryAmoutToDeduct=0.0;
			Double thirdAmountToDeduct=0.0;
			CustomerOrder order = null;
			Invoice invoice = null;
			Customer customer = null;
			InvoiceRefund invoiceRefund=null;
			String msg = "";
			String errorMsg = "";
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(orderId == null || orderId.isEmpty()){
				jObj.put("msg", "Customer Order ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}	
				
			order = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(Integer.parseInt(orderId), brokerId);
			if(order == null){
				jObj.put("msg", "Customer Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(Integer.parseInt(orderId));
			if(invoice == null){
				jObj.put("msg", "Invoice not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			if(customer == null){
				jObj.put("msg", "Customer not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(action!=null && !action.isEmpty()){
				
				Double refundAmount=0.0;
				Double creditWalletAmount=0.0;
				Double creditAmount=0.00;
				Double rewardPoints=0.00;
				if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
					refundAmount = Double.parseDouble(refundAmountStr);
				}
				if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
					creditWalletAmount = Double.parseDouble(walletCCAmtStr);
				}
				if(creditAmountStr!=null && !creditAmountStr.isEmpty()){
					creditAmount = Double.parseDouble(creditAmountStr);
				}
				if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
					rewardPoints = Double.parseDouble(rewardPointsStr);
				}
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
				Date today = new Date();
				
				
//				if(action.equals(PaymentMethod.CREDITCARD.toString()) || action.equals(PaymentMethod.PAYPAL.toString())){
//					if((refundAmount+creditAmount) <= invoice.getInvoiceTotal() && 
//							(refundAmount+creditAmount) > 0){
//						if(refundAmount>0){
//							String refundType = "";
//							if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)){
//								refundType = PaymentMethod.CREDITCARD.toString();
//							}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
//								refundType = PaymentMethod.PAYPAL.toString();
//							}
//
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("refundType", refundType);
//							paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
//							String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
//							Gson gson = Util.getGsonBuilder().create();	
//							JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
//							InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
//							if(invoiceRefundResponse!=null && invoiceRefundResponse.getStatus()==1){
//								if(invoiceRefundResponse.getInvoiceRefund() != null){
//									primaryAmountToDeduct = primaryAmountToDeduct+refundAmount;
//								}
//							}else{
//								msg = invoiceRefundResponse.getError().getDescription();
//							}
//						}
//						if(creditAmount > 0){
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("transactionAmount",String.valueOf(creditAmount));
//							paramMap.put("customerId",String.valueOf(customer.getId()));
//							paramMap.put("transactionType","CREDIT");
//							paramMap.put("userName",userName);
//							String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
//							
//							Gson gson = new Gson();
//							JsonObject cardJsonObject = gson.fromJson(data, JsonObject.class);
//							WalletTransaction walletTransaction = gson.fromJson(((JsonObject)cardJsonObject.get("walletTransaction")), WalletTransaction.class);
//							CustomerWallet wallet = walletTransaction.getWallet();
//							CustomerWalletHistory walletHistory = walletTransaction.getWalletHistory();
//							CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customer.getId());
//							CustomerWalletHistory walletHistory = new CustomerWalletHistory();
//							walletHistory.setCreatedOn(today);
//							walletHistory.setTransactionAmount(creditAmount);
//							walletHistory.setCustomerId(customer.getId());
//							walletHistory.setTransactionType("CREDIT");
//							walletHistory.setUserName(userName);
//							walletHistory.setOrderId(order.getId());
//							DAORegistry.getCustomerWalletHistoryDAO().save(walletHistory);
//							if(wallet==null){
//								wallet = new CustomerWallet();
//								wallet.setCustomerId(customer.getId());
//								wallet.setActiveCredit(creditAmount);
//								wallet.setTotalCredit(creditAmount);
//								wallet.setTotalUsedCredit(0.00);
//								wallet.setLastUpdated(today);
//								DAORegistry.getCustomerWalletDAO().save(wallet);
//							}else{
//								wallet.setActiveCredit(wallet.getActiveCredit() + creditAmount);
//								wallet.setLastUpdated(today);
//								wallet.setTotalCredit(wallet.getTotalCredit() + creditAmount);
//								DAORegistry.getCustomerWalletDAO().update(wallet);
//							}
//							primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
//						}
//					}
//				}else if(action.equals(PaymentMethod.PARTIAL_REWARDS.toString())){
//					
//					
//					if((refundAmount+creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
//							(refundAmount+creditAmount+rewardPoints) > 0){
//						if(refundAmount>0){
//							String refundType = "";
//							if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)){
//								refundType = PaymentMethod.CREDITCARD.toString();
//							}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
//								refundType = PaymentMethod.PAYPAL.toString();
//							}
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("refundType", refundType);
//							paramMap.put("refundAmount",String.valueOf(refundAmount));
//							String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
//							Gson gson = Util.getGsonBuilder().create();	
//							JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
//							InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
//							if(invoiceRefundResponse!=null && invoiceRefundResponse.getStatus()==1){
//								if(invoiceRefundResponse.getInvoiceRefund() != null){
//									secondaryAmoutToDeduct = secondaryAmoutToDeduct+refundAmount;
//								}
//							}else{
//								msg = invoiceRefundResponse.getError().getDescription();
//							}
//						}
//						if(rewardPoints <= invoice.getInvoiceTotal()){
//							customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
//							customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
//							//DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
//							if(invoiceRefund!=null){
//								invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
//							}
//							primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
//						}
//						if(creditAmount > 0){
//							Map<String, String> paramMap = Util.getParameterMap(request);
//							paramMap.put("orderId", String.valueOf(order.getId()));
//							paramMap.put("transactionAmount",String.valueOf(creditAmount));
//							paramMap.put("customerId",String.valueOf(customer.getId()));
//							paramMap.put("transactionType","CREDIT");
//							paramMap.put("userName",userName);
//							String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
//							 
//							Gson gson = new Gson();
//							JsonObject cardJsonObject = gson.fromJson(data, JsonObject.class);
//							WalletTransaction walletTransaction = gson.fromJson(((JsonObject)cardJsonObject.get("walletTransaction")), WalletTransaction.class);
//							CustomerWallet wallet = walletTransaction.getWallet();
//							CustomerWalletHistory walletHistory = walletTransaction.getWalletHistory();
//							walletHistory.setCreatedOn(today);
//							walletHistory.setTransactionAmount(creditAmount);
//							walletHistory.setCustomerId(customer.getId());
//							walletHistory.setTransactionType("CREDITED");
//							walletHistory.setUserName(userName);
//							walletHistory.setOrderId(order.getId());
//							DAORegistry.getCustomerWalletHistoryDAO().save(walletHistory);
//							if(wallet==null){
//								wallet = new CustomerWallet();
//								wallet.setCustomerId(customer.getId());
//								wallet.setActiveCredit(creditAmount);
//								wallet.setTotalCredit(creditAmount);
//								wallet.setTotalUsedCredit(0.00);
//								wallet.setLastUpdated(today);
//								DAORegistry.getCustomerWalletDAO().save(wallet);
//							}else{
//								wallet.setActiveCredit(wallet.getActiveCredit() + creditAmount);
//								wallet.setLastUpdated(today);
//								wallet.setTotalCredit(wallet.getTotalCredit() + creditAmount);
//								DAORegistry.getCustomerWalletDAO().update(wallet);
//							}
//							
//							secondaryAmoutToDeduct = secondaryAmoutToDeduct+creditAmount;
//						}
//					}
//				}else if(action.equals(PaymentMethod.FULL_REWARDS.toString())){
//					if(rewardPoints <= invoice.getInvoiceTotal()){
//						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
//						customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
//						//DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
//						primaryAmountToDeduct = primaryAmountToDeduct +  rewardPoints;
//					}
//				}
				
				if(action.equals(PaymentMethod.PARTIAL_REWARDS.toString())){
					if(order != null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)
							|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) || order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY))){
						if((refundAmount+creditWalletAmount) <= invoice.getInvoiceTotal() && 
								(refundAmount+creditWalletAmount) > 0){
							
							if(refundAmount > 0){
								String refundType = "";
								if(order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) 
										|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)){
									refundType = PaymentMethod.CREDITCARD.toString();
								}else if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
									refundType = PaymentMethod.PAYPAL.toString();
								}

								Map<String, String> paramMap = Util.getParameterMap(request);
								paramMap.put("orderId", String.valueOf(order.getId()));
								paramMap.put("refundType", refundType);
								paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
								
								String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);								
								Gson gson = Util.getGsonBuilder().create();	
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
								if(invoiceRefundResponse == null){
									jObj.put("msg", "Invoice Refund API not available.");
									IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
									return;
								}else if(invoiceRefundResponse.getStatus()==1){
									invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
									if(invoiceRefund != null){
										invoiceRefund.setRefundedBy(userName);
										primaryAmountToDeduct = primaryAmountToDeduct + refundAmount;
									}
								}else{
									errorMsg = invoiceRefundResponse.getError().getDescription();
								}
							}
							if(creditWalletAmount > 0){
								Map<String, String> paramMap = Util.getParameterMap(request);
								paramMap.put("orderId", String.valueOf(order.getId()));
								paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
								paramMap.put("customerId",String.valueOf(customer.getId()));
								paramMap.put("transactionType","CREDIT");
								paramMap.put("userName",userName);
								
								String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
								Gson gson = Util.getGsonBuilder().create();	
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
								if(walletTransaction == null){
									jObj.put("msg", "Wallet Transaction API not available.");
									IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
									return;
								}else if(walletTransaction.getStatus() == 1){
									primaryAmountToDeduct = primaryAmountToDeduct + creditWalletAmount;
								}else{
									errorMsg = walletTransaction.getError().getDescription();
								}
							}
						}							
					}
					else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)){
						if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
								|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
							
							if((refundAmount+creditWalletAmount+creditAmount) <= invoice.getInvoiceTotal() && 
									(refundAmount+creditWalletAmount+creditAmount) > 0){
								
								if(refundAmount > 0){
									String refundType = "";
									if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
										refundType = PaymentMethod.CREDITCARD.toString();
									}

									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", refundType);
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									if(invoiceRefundResponse == null){
										jObj.put("msg", "Invoice Refund API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(invoiceRefundResponse.getStatus()==1){
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
										if(invoiceRefund != null){
											invoiceRefund.setRefundedBy(userName);
											secondaryAmoutToDeduct = secondaryAmoutToDeduct + refundAmount;
										}
									}else{
										errorMsg = invoiceRefundResponse.getError().getDescription();
									}
								}
								if(creditWalletAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
								if(creditAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
						else{
							if(creditAmount > 0){
								Map<String, String> paramMap = Util.getParameterMap(request);
								paramMap.put("orderId", String.valueOf(order.getId()));
								paramMap.put("transactionAmount",String.valueOf(creditAmount));
								paramMap.put("customerId",String.valueOf(customer.getId()));
								paramMap.put("transactionType","CREDIT");
								paramMap.put("userName",userName);
								
								String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
								Gson gson = Util.getGsonBuilder().create();	
								JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
								WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
								if(walletTransaction == null){
									jObj.put("msg", "Wallet Transaction API not available.");
									IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
									return;
								}else if(walletTransaction.getStatus() == 1){
									primaryAmountToDeduct = primaryAmountToDeduct + creditAmount;
								}else{
									errorMsg = walletTransaction.getError().getDescription();
								}
							}
						}
					}
					else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
						if(rewardPoints <= invoice.getInvoiceTotal() && rewardPoints > 0){
							customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
							customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
							if(invoiceRefund!=null){
								invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
							}
							primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
						}
					}
					else if(order != null && order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS)){
						if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
								|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
							if((refundAmount+creditWalletAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
									(refundAmount+creditWalletAmount+rewardPoints) > 0){

								if(rewardPoints <= invoice.getInvoiceTotal()){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
									if(invoiceRefund!=null){
										invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
									}
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
								if(refundAmount > 0){
									String refundType = "";
									if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)){
										refundType = PaymentMethod.CREDITCARD.toString();
									}

									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", refundType);
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									if(invoiceRefundResponse == null){
										jObj.put("msg", "Invoice Refund API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(invoiceRefundResponse.getStatus()==1){
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
										if(invoiceRefund != null){
											invoiceRefund.setRefundedBy(userName);
											secondaryAmoutToDeduct = secondaryAmoutToDeduct + refundAmount;
										}
									}else{
										errorMsg = invoiceRefundResponse.getError().getDescription();
									}
								}
								if(creditWalletAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditWalletAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
						else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET) && 
								order.getThirdPaymentMethod()!= null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
										|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))){
							if((refundAmount+creditWalletAmount+creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
									(refundAmount+creditWalletAmount+creditAmount+rewardPoints) > 0){

								if(rewardPoints > 0 && rewardPoints <= invoice.getInvoiceTotal()){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
									if(invoiceRefund!=null){
										invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
									}
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
								if(creditAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
								if(refundAmount > 0){
									String refundType = "";
									if(order.getThirdPaymentMethod()!= null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
											|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))){
										refundType = PaymentMethod.CREDITCARD.toString();
									}

									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("refundType", refundType);
									paramMap.put("refundAmount",String.valueOf(Util.formatDoubleToTwoDecimalPoint(refundAmount)));
									
									String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.INVOICE_REFUND);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject  jsonObject = gson.fromJson(data, JsonObject.class);
									InvoiceRefundResponse invoiceRefundResponse = gson.fromJson(((JsonObject)jsonObject.get("invoiceRefundResponse")), InvoiceRefundResponse.class);
									if(invoiceRefundResponse == null){
										jObj.put("msg", "Invoice Refund API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(invoiceRefundResponse.getStatus()==1){
										invoiceRefund = invoiceRefundResponse.getInvoiceRefund();
										if(invoiceRefund != null){
											invoiceRefund.setRefundedBy(userName);
											thirdAmountToDeduct = thirdAmountToDeduct + refundAmount;
										}
									}else{
										errorMsg = invoiceRefundResponse.getError().getDescription();
									}
								}
								if(creditWalletAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditWalletAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										thirdAmountToDeduct = thirdAmountToDeduct + creditWalletAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
						else if(order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)){
							if((creditAmount+rewardPoints) <= invoice.getInvoiceTotal() && 
									(creditAmount+rewardPoints) > 0){
								
								if(rewardPoints > 0 && rewardPoints <= invoice.getInvoiceTotal()){
									customerLoyalty.setActivePoints(customerLoyalty.getActivePoints()+rewardPoints);
									customerLoyalty.setRevertedSpentPoints(customerLoyalty.getRevertedSpentPoints() + rewardPoints);
									if(invoiceRefund!=null){
										invoiceRefund.setRevertedUsedRewardPoints(rewardPoints);
									}
									primaryAmountToDeduct = primaryAmountToDeduct + rewardPoints ;
								}
								if(creditAmount > 0){
									Map<String, String> paramMap = Util.getParameterMap(request);
									paramMap.put("orderId", String.valueOf(order.getId()));
									paramMap.put("transactionAmount",String.valueOf(creditAmount));
									paramMap.put("customerId",String.valueOf(customer.getId()));
									paramMap.put("transactionType","CREDIT");
									paramMap.put("userName",userName);
									
									String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
									Gson gson = Util.getGsonBuilder().create();	
									JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
									WalletTransaction walletTransaction = gson.fromJson(((JsonObject)jsonObject.get("walletTransaction")), WalletTransaction.class);
									if(walletTransaction == null){
										jObj.put("msg", "Wallet Transaction API not available.");
										IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
										return;
									}else if(walletTransaction.getStatus() == 1){
										secondaryAmoutToDeduct = secondaryAmoutToDeduct + creditAmount;
									}else{
										errorMsg = walletTransaction.getError().getDescription();
									}
								}
							}
						}
					}						
				//}
				
				if(errorMsg.isEmpty()){
					order.setPrimaryRefundAmount(order.getPrimaryRefundAmount()!=null?order.getPrimaryRefundAmount()+primaryAmountToDeduct:primaryAmountToDeduct);
					order.setSecondaryRefundAmount(order.getSecondaryRefundAmount()!=null?order.getSecondaryRefundAmount()+secondaryAmoutToDeduct:secondaryAmoutToDeduct);
					order.setThirdRefundAmount(order.getThirdRefundAmount()!=null?order.getThirdRefundAmount()+thirdAmountToDeduct:thirdAmountToDeduct);
					//order.setOrderTotal(order.getOrderTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct + thirdAmountToDeduct));
					
					invoice.setInvoiceTotal(invoice.getInvoiceTotal()-(primaryAmountToDeduct + secondaryAmoutToDeduct+ thirdAmountToDeduct));
					invoice.setRefundAmount(invoice.getRefundAmount()!=null?invoice.getRefundAmount()+(primaryAmountToDeduct + secondaryAmoutToDeduct+ thirdAmountToDeduct)
							:(primaryAmountToDeduct + secondaryAmoutToDeduct+ thirdAmountToDeduct));
					invoice.setLastUpdated(today);
					invoice.setLastUpdatedBy(userName);
					if(note!=null && !note.isEmpty()){
						invoice.setInternalNote(note);
					}
					
					if(invoiceRefund!=null){
						invoiceRefund.setOrderId(order.getId());
						DAORegistry.getInvoiceRefundDAO().save(invoiceRefund);
					}
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					DAORegistry.getCustomerOrderDAO().update(order);
					DAORegistry.getInvoiceDAO().update(invoice);
					
					String logMsg= "";
					if(msg.isEmpty()){
						if(refundAmountStr!=null && !refundAmountStr.isEmpty()){
							msg =msg +"Refunded($"+refundAmountStr+"),";
							logMsg =logMsg +"Refunded($"+refundAmountStr+"),";
						}
						if(walletCCAmtStr!=null && !walletCCAmtStr.isEmpty()){
							msg =msg +"Wallet credited from Creditcard/paypal($"+walletCCAmtStr+"),";
							logMsg =logMsg +"Wallet credited from Creditcard/paypal($"+walletCCAmtStr+"),";
						}
						if(creditAmountStr!=null && !creditAmountStr.isEmpty()){
							msg =msg +"Wallet credited from Wallet($"+creditAmountStr+"),";	
							logMsg =logMsg +"Wallet credited from Wallet($"+creditAmountStr+"),";	
						}						
						if(rewardPointsStr!=null && !rewardPointsStr.isEmpty()){
							msg =msg +"Reverted Reward Points($"+rewardPointsStr+")";
							logMsg =logMsg +"Reverted Reward Points($"+rewardPointsStr+")";
						}
					}
					
					InvoiceAudit audit = new InvoiceAudit(invoice);
					audit.setAction(InvoiceAuditAction.INVOICE_REFUNDED);
					audit.setNote("Invoice is "+logMsg);
					audit.setCreatedDate(new Date());
					audit.setCreateBy(userName);
					DAORegistry.getInvoiceAuditDAO().save(audit);
					jObj.put("msg", "Action is successfully completed. Amount "+msg);
					
					//Tracking User Action
					String userActionMsg = "Invoice Refunded. "+logMsg;
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
					
					//jObj.put("order", order);
					//jObj.put("invoice", invoice);
					//jObj.put("customer", customer);
					if(order != null){
						JSONObject orderObject = new JSONObject();
						orderObject.put("orderId", order.getId());
						orderObject.put("orderTotal", order.getOrderTotal());
						orderObject.put("orderCreateDate", order.getCreatedDateStr());
						orderObject.put("primaryPaymentMethod", order.getPrimaryPaymentMethod());
						orderObject.put("primaryAvailableAmt", order.getPrimaryAvailableAmt());
						orderObject.put("primaryPayAmt", order.getPrimaryPayAmt());
						orderObject.put("secondaryPaymentMethod", order.getSecondaryPaymentMethod());
						orderObject.put("secondaryAvailableAmt", order.getSecondaryAvailableAmt());
						orderObject.put("secondaryPayAmt", order.getSecondaryPayAmt());
						orderObject.put("thirdPaymentMethod", order.getThirdPaymentMethod());
						orderObject.put("thirdAvailableAmt", order.getThirdAvailableAmt());
						orderObject.put("thirdPayAmt", order.getThirdPayAmt());
						jObj.put("order", orderObject);
					}						
					if(invoice != null){
						JSONObject invoiceObject = new JSONObject();
						invoiceObject.put("invoiceId", invoice.getId());
						invoiceObject.put("invoiceInternalNote", invoice.getInternalNote());
						jObj.put("invoice", invoiceObject);
					}						
					if(customer != null){
						JSONObject customerObject = new JSONObject();
						customerObject.put("customerName", customer.getCustomerName());
						customerObject.put("lastName", customer.getLastName());
						customerObject.put("email", customer.getEmail());
						customerObject.put("phone", customer.getPhone());
						jObj.put("customer", customerObject);
					}
					jObj.put("isValid", true);
					jObj.put("status", 1);
					//return jsonArr;
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
					//return "page-invoice-refund";
					}else{
						jObj.put("msg", errorMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
				}
			}
			if(invoice != null && invoice.getStatus().equals(InvoiceStatus.Voided)){
				jObj.put("msg", "Selected Invoice is voided, Cannot make refund on void invoice.");
				jObj.put("isValid", false);
			}
			else if(order != null && (order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY)
					|| (order.getThirdPaymentMethod()!=null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD)
					|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY)
					|| order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY)))
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) 
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS)						
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.CUSTOMER_WALLET)
					|| order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CUSTOMER_WALLET)
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
				if(invoice.getInvoiceTotal() > 0){
					if(order != null){
						JSONObject orderObject = new JSONObject();
						orderObject.put("orderId", order.getId());
						orderObject.put("orderTotal", order.getOrderTotal());
						orderObject.put("orderCreateDate", order.getCreatedDateStr());
						orderObject.put("primaryPaymentMethod", order.getPrimaryPaymentMethod());
						orderObject.put("primaryAvailableAmt", order.getPrimaryAvailableAmt());
						orderObject.put("primaryPayAmt", order.getPrimaryPayAmt());
						orderObject.put("secondaryPaymentMethod", order.getSecondaryPaymentMethod());
						orderObject.put("secondaryAvailableAmt", order.getSecondaryAvailableAmt());
						orderObject.put("secondaryPayAmt", order.getSecondaryPayAmt());
						orderObject.put("thirdPaymentMethod", order.getThirdPaymentMethod());
						orderObject.put("thirdAvailableAmt", order.getThirdAvailableAmt());
						orderObject.put("thirdPayAmt", order.getThirdPayAmt());
						jObj.put("order", orderObject);
					}						
					if(invoice != null){
						JSONObject invoiceObject = new JSONObject();
						invoiceObject.put("invoiceId", invoice.getId());
						invoiceObject.put("invoiceInternalNote", invoice.getInternalNote());
						jObj.put("invoice", invoiceObject);
					}						
					if(customer != null){
						JSONObject customerObject = new JSONObject();
						customerObject.put("customerName", customer.getCustomerName());
						customerObject.put("lastName", customer.getLastName());
						customerObject.put("email", customer.getEmail());
						customerObject.put("phone", customer.getPhone());
						jObj.put("customer", customerObject);
					}						
					jObj.put("isValid", true);
					jObj.put("status", 1);
				}else{
					jObj.put("msg", "Cannot refund as selected invoice having invoiceTotal 0.");
					jObj.put("isValid", false);
				}
			}else{
				jObj.put("msg", "Cannot refund as selected invoice payment is made using "+order.getPrimaryPaymentMethod().toString());
				jObj.put("isValid", false);
			}
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/getShippingAddressForFedex")
	public String getShippingAddressForFedex(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String poId = request.getParameter("poId");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("poId", poId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_SHIPPING_ADDRESS_FOR_FEDEX);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			POGetShippingAddressForFedexDTO poGetShippingAddressForFedexDTO = gson.fromJson(((JsonObject)jsonObject.get("POGetShippingAddressForFedexDTO")), POGetShippingAddressForFedexDTO.class);
			
			if(poGetShippingAddressForFedexDTO.getStatus() == 1){
				model.addAttribute("msg", poGetShippingAddressForFedexDTO.getMessage());
				model.addAttribute("status", poGetShippingAddressForFedexDTO.getStatus());
				model.addAttribute("pagingInfo", poGetShippingAddressForFedexDTO.getShippingAddressPaginationDTO());
				model.addAttribute("shippingAddressList", poGetShippingAddressForFedexDTO.getShippingAddressDTO());
				model.addAttribute("count", poGetShippingAddressForFedexDTO.getShippingAddressCount());
				model.addAttribute("purchaseOrderId", poGetShippingAddressForFedexDTO.getPoId());
				model.addAttribute("customerId", poGetShippingAddressForFedexDTO.getCustomerId());
				model.addAttribute("companyName", poGetShippingAddressForFedexDTO.getCompanyName());
			}else{
				model.addAttribute("msg", poGetShippingAddressForFedexDTO.getError().getDescription());
				model.addAttribute("status", poGetShippingAddressForFedexDTO.getStatus());
			}
			/*int shippingAddressCount = 0;
			PurchaseOrder PO = DAORegistry.getPurchaseOrderDAO().get(Integer.parseInt(purchaseOrderId));			
			Customer customer = DAORegistry.getCustomerDAO().get(PO.getCustomerId());	
			JSONObject jObj = new JSONObject();
			Integer customerId = PO.getCustomerId();
			if(customerId != null && customerId > 0){
				List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(customerId);
				shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(customerId);
				
				if(shippingAddressList != null){
					jObj.put("shippingAddressList", JsonWrapperUtil.getShippingAddressArray(shippingAddressList));
				}else{
					jObj.put("successMessage","No Shipping address found, Please add shipping Address.");
				}
			}else{
				jObj.put("successMessage","No Customer Id found.");
			}
			jObj.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(shippingAddressCount));
			jObj.put("count", shippingAddressCount);
			jObj.put("customerId", customerId);
			jObj.put("purchaseOrderId",purchaseOrderId);
			jObj.put("companyName", customer.getCompanyName());
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value = "/GetCustomerOrderDetails")
	public String getCustomerOrderDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String action = request.getParameter("action");
			String invoiceId = request.getParameter("invoiceId");
			String orderNo = request.getParameter("orderNo");
			String manualNote = request.getParameter("manualNote");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("invoiceId", invoiceId);
			map.put("orderNo", orderNo);
			map.put("manualNote", manualNote);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_INVOICE_CUSTOMER_ORDER_DETAILS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CustomerOrderDetailsDTO customerOrderDetailsDTO = gson.fromJson(((JsonObject)jsonObject.get("customerOrderDetailsDTO")), CustomerOrderDetailsDTO.class);
			
			if(customerOrderDetailsDTO.getStatus() == 1){
				model.addAttribute("msg", customerOrderDetailsDTO.getMessage());
				model.addAttribute("status", customerOrderDetailsDTO.getStatus());
				model.addAttribute("order", customerOrderDetailsDTO.getCustomerOrderDTO());
				model.addAttribute("orderDetails", customerOrderDetailsDTO.getCustomerOrderDetailsInfoDTO());
				model.addAttribute("invoice", customerOrderDetailsDTO.getInvoiceDTO());
				model.addAttribute("pagingInfo", customerOrderDetailsDTO.getAuditPaginationDTO());
				model.addAttribute("audits", customerOrderDetailsDTO.getInvoiceAuditDTO());
			}else{
				model.addAttribute("msg", customerOrderDetailsDTO.getError().getDescription());
				model.addAttribute("status", customerOrderDetailsDTO.getStatus());
			}
			model.addAttribute("orderNoStr", orderNo);
			model.addAttribute("invoiceIdStr", invoiceId);
			
			/*Invoice invoice = null;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
						
			if(orderNoStr!=null && !orderNoStr.isEmpty()){
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(Integer.parseInt(orderNoStr), brokerId);
				if(customerOrder == null){
					jObj.put("msg", "Customer Order not found for selected invoice.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;					
				}
				invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(Integer.parseInt(orderNoStr));
				if(invoice == null){
					jObj.put("msg", "Invoice Details not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
			}else if(invoiceIdStr!=null && !invoiceIdStr.isEmpty()){
				invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceIdStr), brokerId);
				if(invoice == null){
					jObj.put("msg", "Invoice Details not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
			}else{
				jObj.put("msg", "Customer Order Details not found for selected Invoice.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(action!=null && action.equalsIgnoreCase("addNote")){
				
				if(manualNote!=null && !manualNote.isEmpty()){
					InvoiceAudit audit = new InvoiceAudit(invoice);
					audit.setAction(InvoiceAuditAction.MANUAL_NOTE);
					audit.setCreateBy(userName);
					audit.setCreatedDate(new Date());
					audit.setNote(manualNote);
					DAORegistry.getInvoiceAuditDAO().save(audit);
					jObj.put("msg", "Note added successfully.");
					jObj.put("status", 1);
					
					//Tracking User Action
					String userActionMsg = "Invoice - Order Details, Manual Note Updated Successfully.";
					Util.userActionAudit(request, invoice.getId(), userActionMsg);
					
				}else{
					jObj.put("msg", "Please add some value in note.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
			}
			
			if(invoice!=null){
				CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				if(order == null){
					jObj.put("msg", "Customer Order not found for selected invoice.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;					
				}
				if(order.getShippingMethodId()!=null){
					ShippingMethod shipping = DAORegistry.getShippingMethodDAO().getShippingMethodById(order.getShippingMethodId());
					order.setShippingMethod(shipping.getName());
				}
				List<InvoiceAudit> auditList = DAORegistry.getInvoiceAuditDAO().getInvoiceAuditByInvoiceId(invoice.getId());
				List<CustomerTicketDownloads> customerTicketDownloads = DAORegistry.getCustomerTicketDownloadsDAO().getCustomerTicketDownloadsByInvoiceId(invoice.getId());
				CustomerOrderDetails orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(order.getId());
				
				if(order != null){
					JSONObject cusOrderObject = new JSONObject();
					cusOrderObject.put("id", order.getId());
					cusOrderObject.put("orderTotal", order.getOrderTotal());
					cusOrderObject.put("orderCreateDate", order.getCreatedDateStr());
					cusOrderObject.put("section", order.getSection());
					cusOrderObject.put("qty", order.getQty());
					cusOrderObject.put("seatLow", order.getSeatLow());
					cusOrderObject.put("seatHigh", order.getSeatHigh());
					cusOrderObject.put("ticketPrice", order.getTicketPrice());
					cusOrderObject.put("soldPrice", order.getSoldPrice());
					cusOrderObject.put("shippingMethod", order.getShippingMethod());
					cusOrderObject.put("eventId", order.getEventId());
					cusOrderObject.put("eventName", order.getEventName());
					cusOrderObject.put("eventDateStr", order.getEventDateStr());
					cusOrderObject.put("eventTimeStr", order.getEventTimeStr());
					cusOrderObject.put("venueName", order.getVenueName());
					cusOrderObject.put("primaryPaymentMethod", order.getPrimaryPaymentMethod());
					cusOrderObject.put("primaryPayAmt", order.getPrimaryPayAmt());				
					cusOrderObject.put("secondaryPaymentMethod", order.getSecondaryPaymentMethod());				
					cusOrderObject.put("secondaryPayAmt", order.getSecondaryPayAmt());				
					cusOrderObject.put("thirdPaymentMethod", order.getThirdPaymentMethod());
					cusOrderObject.put("thirdPayAmt", order.getThirdPayAmt());
					jObj.put("order",cusOrderObject);
				}
				if(orderDetails != null){
					JSONObject orderDetailsObject = new JSONObject();
					orderDetailsObject.put("billingFirstName", orderDetails.getBlFirstName());
					orderDetailsObject.put("billingLastName", orderDetails.getBlLastName());
					orderDetailsObject.put("billingEmail", orderDetails.getBlEmail());
					jObj.put("orderDetails",orderDetailsObject);
				}
				if(invoice != null){
					JSONObject invoiceObject = new JSONObject();
					invoiceObject.put("invoiceId", invoice.getId());
					jObj.put("invoice",invoiceObject);
				}
				jObj.put("audits",JsonWrapperUtil.getInvoiceAuditJsonArray(auditList, customerTicketDownloads));
				jObj.put("pagingInfo", PaginationUtil.getDummyPaginationParameter(auditList.size()+customerTicketDownloads.size()));
			}
			jObj.put("orderNoStr", orderNoStr);
			jObj.put("invoiceIdStr", invoiceIdStr);
			jObj.put("status", 1);
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	/*@RequestMapping(value = "/PaypalRefundTranList")
	public String paypalRefundTranList(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		try {
			String orderId = request.getParameter("orderId");
			String startDate = request.getParameter("startDate") != null ? request.getParameter("startDate") + "00:00:00" : null;
			String endDate = request.getParameter("endDate") != null ? request.getParameter("endDate") + "23:59:59" : null;
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			
			List<CustomerOrderPaypalRefund> customerOrderPaypalRefundList = DAORegistry.getCustomerOrderPaypalRefundDAO().getCustomerOrderPaypalRefundBySearchCriteria(orderId != null ? Integer.parseInt(orderId) : null, startDate != null ? dateFormatter.parse(startDate) : null, endDate != null ? dateFormatter.parse(endDate) : null);
			map.put("customerOrderPaypalRefundList", customerOrderPaypalRefundList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-paypal-refund-transaction-list";
	}*/
	
	
	public boolean sendCredentialMail(){
		//Customer customer  = DAORegistry.getCustomerDAO().get(1);
		try {
			//String password = Util.generatePassword(customer.getCustomerName(), "RTF");
			Map<String, Object> mailMap = new HashMap<String, Object>();
			//String password = "RTF@csmith123";//Util.generatePassword(customer.getCustomerName(), "RTF");
			/*customer.setPassword(Util.generateEncrptedPassword(password));
			System.out.println(customer.getEmail());
			System.out.println(password);
			mailMap.put("activePoints","500.53");
			mailMap.put("password",password);
			mailMap.put("customerName",customer.getCustomerName());
			mailMap.put("userName", customer.getUserName());*/
			mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
			mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
			String path = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\ROOT\\resources\\images\\";
			//com.rtw.tracker.mail.MailAttachment[] mailAttachment =Util.getRegistrationTemplateAttachment(path);
			/*com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
			mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",path+"share.png");
			mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","top.png",path+"top.png");
			mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","bottom.png",path+"bottom.png");
			mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",path+"blue.png");
			mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",path+"fb1.png");
			mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",path+"ig1.png");
			mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",path+"li1.png");
			mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogo1.png",path+"rtflogo1.png");
			mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",path+"p1.png");
			mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",path+"tw1.png");
			mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",path+"tyrtf.png");*/
			
			com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[5];
			mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app1.png",path+"app1.png");
			mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app2.png",path+"app2.png");
			mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rtflogo1.png",path+"rtflogo1.png");
			mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","dollar.png",path+"dollar.png");
			mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","rewardoptions.png",path+"rewardoptions.png");
			/*String email = customer.getEmail();
			email = "msanghani@rightthisway.com";*/
			String bcc = "";
			/*//bcc = "amit.raut@rightthisway.com,msanghani@rightthisway.com";
			mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
					null,bcc, "Reward The Fan: You have $500.53 to spend towards any future ticket purchase",
		    		"email-customer-reward-information.html", mailMap, "text/html", null,mailAttachment,null);*/
			//DAORegistry.getCustomerDAO().update(customer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	
	@RequestMapping(value="/checkFileName")
	public String checkFileName(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String fileName = request.getParameter("fileName");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("fileName", fileName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_INVOICE_REAL_TICKET_CHECK_FILE_NAME);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceTicketCheckFileNameDTO invoiceTicketCheckFileNameDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceTicketCheckFileNameDTO")), InvoiceTicketCheckFileNameDTO.class);
			
			if(invoiceTicketCheckFileNameDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceTicketCheckFileNameDTO.getMessage());
				model.addAttribute("status", invoiceTicketCheckFileNameDTO.getStatus());
				model.addAttribute("invoiceId", invoiceTicketCheckFileNameDTO.getInvoiceId());
			}else{
				model.addAttribute("msg", invoiceTicketCheckFileNameDTO.getError()!=null?invoiceTicketCheckFileNameDTO.getError().getDescription():"");
				model.addAttribute("status", invoiceTicketCheckFileNameDTO.getStatus());
			}
			
			/*List<InvoiceTicketAttachment> invoiceTktAttachmentList = null;
			String invoiceIdStr = "";
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(fileNameStr!=null && !fileNameStr.isEmpty()){
				invoiceTktAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getAttachmentByFileName(fileNameStr);
				if(invoiceTktAttachmentList != null && invoiceTktAttachmentList.size() > 0){
					for(InvoiceTicketAttachment invoiceTktAttachment : invoiceTktAttachmentList){
						if(invoiceTktAttachmentList.size() <= 1){
							invoiceIdStr += invoiceTktAttachment.getInvoiceId();
						}else{
							invoiceIdStr += invoiceTktAttachment.getInvoiceId()+",";
						}
					}
					jObj.put("invoiceId", invoiceIdStr);
					jObj.put("status", 1);
				}
			}
				
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	/**
	 * Create Fedex Label for given invoice.
	 * @param request
	 * @param response
	 * @return
	 */
	
	@RequestMapping(value = "/CreateManualFedexLabel")
	public String createManualFedexLabel(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_MANUAL_FEDEX_LABEL);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String blFirstName = request.getParameter("manualFedex_blFirstName");
			String blLastName = request.getParameter("manualFedex_blLastName");		
			String blCompanyName = request.getParameter("manualFedex_blCompanyName");
			String blStreet1 = request.getParameter("manualFedex_blStreet1");
			String blStreet2 = request.getParameter("manualFedex_blStreet2");
			String blCity = request.getParameter("manualFedex_blCity");
			String blState = request.getParameter("manualFedex_blStateName");
			String blCountry = request.getParameter("manualFedex_blCountryName");
			String blZipCode = request.getParameter("manualFedex_blZipCode");
			String blPhone = request.getParameter("manualFedex_blPhone");
			String shCustomerName = request.getParameter("manualFedex_shCustomerName");
			String shCompanyName = request.getParameter("manualFedex_shCompanyName");
			String shStreet1 = request.getParameter("manualFedex_shStreet1");
			String shStreet2 = request.getParameter("manualFedex_shStreet2");
			String shCity = request.getParameter("manualFedex_shCity");
			String shState = request.getParameter("manualFedex_shStateName");
			String shCountry = request.getParameter("manualFedex_shCountryName");
			String shZipCode = request.getParameter("manualFedex_shZipCode");
			String shPhone = request.getParameter("manualFedex_shPhone");
			String serviceType = request.getParameter("manualFedex_serviceType");
			String signatureType = request.getParameter("manualFedex_signatureType");
			String trackingNo="";
			String msg="";
			SignatureOptionType signatureOptionType = null;
			Integer invoiceId = 0;
			JSONObject object = new JSONObject();
			Country country = null;
			State state = null;
			object.put("status", 0);
			
			CreateFedexLabelUtil util = new CreateFedexLabelUtil();
			
			if(blFirstName == null || blFirstName.isEmpty()){
				object.put("msg", "Billing - First Name is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blLastName == null || blLastName.isEmpty()){
				object.put("msg", "Billing - Last Name is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blCompanyName == null || blCompanyName.isEmpty()){
				object.put("msg", "From Address - Company Name is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blStreet1 == null || blStreet1.isEmpty()){
				object.put("msg", "From Address - Street1 is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blCity == null || blCity.isEmpty()){
				object.put("msg", "From Address - City is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blState == null || blState.isEmpty()){
				object.put("msg", "From Address - State is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blCountry == null || blCountry.isEmpty()){
				object.put("msg", "From Address - Country is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blZipCode == null || blZipCode.isEmpty()){
				object.put("msg", "From Address - ZipCode is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(blPhone == null || blPhone.isEmpty()){
				object.put("msg", "From Address - Phone is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if((shCustomerName == null || shCustomerName.isEmpty()) && (shCompanyName == null || shCompanyName.isEmpty())){
				object.put("msg", "Shipping - Full Name/Company Name is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shLastName == null || shLastName.isEmpty()){
				object.put("msg", "Shipping - Last Name is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shStreet1 == null || shStreet1.isEmpty()){
				object.put("msg", "Shipping - Street1 is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shCity == null || shCity.isEmpty()){
				object.put("msg", "Shipping - City is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shState == null || shState.isEmpty()){
				object.put("msg", "Shipping - State is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shCountry == null || shCountry.isEmpty()){
				object.put("msg", "Shipping - Country is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shZipCode == null || shZipCode.isEmpty()){
				object.put("msg", "Shipping - ZipCode is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(shPhone == null || shPhone.isEmpty()){
				object.put("msg", "Shipping - Phone is mandatory.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(serviceType == null || serviceType.isEmpty() || serviceType.equalsIgnoreCase("select")){
				object.put("msg", "Service Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(signatureType == null || signatureType.isEmpty()){
				object.put("msg", "Signature Type not found.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}			
			signatureOptionType = SignatureOptionType.fromString(signatureType);
			
			country = DAORegistry.getCountryDAO().get(Integer.parseInt(shCountry));
			state = DAORegistry.getStateDAO().get(Integer.parseInt(shState));
			
			Recipient recipientObj = new Recipient();
			recipientObj.setInvoiceId(1);
			recipientObj.setRecipientName(shCustomerName);
			recipientObj.setCompanyName(shCompanyName);
			recipientObj.setAddressStreet1(shStreet1);
			recipientObj.setAddressStreet2(shStreet2);
			recipientObj.setCity(shCity);
			recipientObj.setCountryCode(country.getDesc());
			recipientObj.setStateProvinceCode(state.getShortDesc());
			recipientObj.setPostalCode(shZipCode);
			recipientObj.setPhoneNumber(shPhone);
			
			ManualFedexGeneration manualFedex = new ManualFedexGeneration();
			manualFedex.setBlFirstName(blFirstName);
			manualFedex.setBlLastName(blLastName);
			manualFedex.setBlCompanyName(blCompanyName);
			manualFedex.setBlAddress1(blStreet1);
			manualFedex.setBlAddress2(blStreet2);
			manualFedex.setBlCity(blCity);
			country = DAORegistry.getCountryDAO().get(Integer.parseInt(blCountry));
			manualFedex.setBlCountry(country.getId());
			manualFedex.setBlCountryName(country.getName());
			state = DAORegistry.getStateDAO().get(Integer.parseInt(blState));
			manualFedex.setBlState(state.getId());
			manualFedex.setBlStateName(state.getName());
			manualFedex.setBlZipCode(blZipCode);
			manualFedex.setBlPhone(blPhone);
			manualFedex.setShCustomerName(shCustomerName);
			manualFedex.setShCompanyName(shCompanyName);
			manualFedex.setShAddress1(shStreet1);
			manualFedex.setShAddress2(shStreet2);
			manualFedex.setShCity(shCity);
			country = DAORegistry.getCountryDAO().get(Integer.parseInt(shCountry));
			manualFedex.setShCountry(country.getId());
			manualFedex.setShCountryName(country.getName());
			state = DAORegistry.getStateDAO().get(Integer.parseInt(shState));
			manualFedex.setShState(state.getId());
			manualFedex.setShStateName(state.getName());
			manualFedex.setShZipCode(shZipCode);
			manualFedex.setShPhone(shPhone);
			manualFedex.setServiceType(serviceType);
			manualFedex.setSignatureType(signatureType);
			
			
			if(recipientObj.getCountryCode()==null){
				msg = "Could not create Fedex label, No country found for selected invoice.";
				object.put("msg", msg);
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}else if(recipientObj.getCountryCode().equalsIgnoreCase("US")){													
				ServiceType service =  ServiceType.fromValue(serviceType);
				msg = util.createManualFedExShipLabel(manualFedex,recipientObj,service,signatureOptionType);// package type = your package					
	    	}else{
	    		ServiceType service =  ServiceType.fromValue(serviceType);
	    		msg = CreateFedexLabelUtil.createManualFedExShipLabel(manualFedex,recipientObj,service,signatureOptionType);
	    	}			 
			 
			if(msg==null){
				msg="Fedex Label Created with tracking no : "+manualFedex.getTrackingNumber();				 
				object.put("status", 1);
				
				//Tracking User Action
				String userActionMsg = "Fedex Label is Created for Manual Fedex. Tracking no - "+manualFedex.getTrackingNumber();
				Util.userActionAudit(request, manualFedex.getId(), userActionMsg);
			}
			
			object.put("msg", msg) ;
			
			//return jsonArr
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			System.out.println("FedEx: "+e);
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value="/CreatePayInvoice")
	public String createPayInvoice(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_PAY_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CreatePayInvoiceDTO createPayInvoiceDTO = gson.fromJson(((JsonObject)jsonObject.get("createPayInvoiceDTO")), CreatePayInvoiceDTO.class);
			
			if(createPayInvoiceDTO.getStatus() == 1){
				model.addAttribute("msg", createPayInvoiceDTO.getMessage());
				model.addAttribute("status", createPayInvoiceDTO.getStatus());
				model.addAttribute("order", createPayInvoiceDTO.getCustomerOrderDTO());
				model.addAttribute("invoice", createPayInvoiceDTO.getInvoiceDTO());
				model.addAttribute("customer", createPayInvoiceDTO.getCustomerDTO());
				model.addAttribute("savedCards", createPayInvoiceDTO.getCustomerCardInfoDTO());
				model.addAttribute("sPubKey", createPayInvoiceDTO.getsPubKey());
				model.addAttribute("rewardPoints", createPayInvoiceDTO.getRewardPoints());
				model.addAttribute("customerCredit", createPayInvoiceDTO.getCustomerCredit());
			}else{
				model.addAttribute("msg", createPayInvoiceDTO.getError().getDescription());
				model.addAttribute("status", createPayInvoiceDTO.getStatus());
			}
			
			/*String orderId = request.getParameter("orderId");
			String action = request.getParameter("pinv_action");			
			CustomerOrder order = null;
			Invoice invoice = null;
			Customer customer = null;
			String msg = "";
			Double rewardPoints = 0.00;
			Double orderTotal = 0.00;
			JSONObject jObj = new JSONObject();
			jObj.put("status", 0);
			
			if(orderId == null || orderId.isEmpty()){
				jObj.put("msg", "Customer Order ID not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}	
				
			order = DAORegistry.getCustomerOrderDAO().getCustomerOrderByIdAndBrokerId(Integer.parseInt(orderId), brokerId);
			if(order == null){
				jObj.put("msg", "Customer Order not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(!order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
				jObj.put("msg", "Payment has received for Selected Invoice.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(Integer.parseInt(orderId));
			if(invoice == null){
				jObj.put("msg", "Invoice not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			if(customer == null){
				jObj.put("msg", "Customer not found.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(action!=null && !action.isEmpty() && action.equalsIgnoreCase("savePayInvoice")){
				String ticketIds = "";
				if(order.getIsLongSale()){
					List<OrderTicketGroupDetails> orderTicketDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(order.getId());
					for(OrderTicketGroupDetails otd : orderTicketDetails){
						ticketIds += otd.getTicketGroupId()+",";
					}
					ticketIds = ticketIds.substring(0,ticketIds.length()-1);
				}else{
					CategoryTicketGroup ctg = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByInvoiceId(invoice.getId());
					ticketIds = ctg.getId().toString();
				}
				
				String paymentMethod = request.getParameter("paymentMethod");
				String invoiceNote = request.getParameter("pinv_Note");
				if(TextUtil.isEmptyOrNull(paymentMethod)){
					jObj.put("msg", "Not able to identify payment method.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				String orderTotalStr = request.getParameter("pinv_orderTotal");
				if(orderTotalStr == null || orderTotalStr.isEmpty()){
					jObj.put("msg", "Order Total not found.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				try {
					orderTotal = Double.parseDouble(orderTotalStr);
				} catch (Exception e) {
					jObj.put("msg", "Invalid Order Total.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				if(orderTotal > order.getOrderTotal() || orderTotal < order.getOrderTotal()){
					jObj.put("msg", "Order Total is mismatching.");
					IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				
				PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
				Map<String, String> orderParamMap = Util.getParameterMap(request);
				Date today = new Date();
				String validationMsg = "";
				String paymentMethodCount = null;	
				Long rewardTxnId = null;
				String primaryTrxId = "";
				String secondaryTrxId = "";
				String thirdTrxId = "";
				String primaryAmount=null;
				String secondaryAmount =null;
				String thirdAmount=null;
				String rewardPointsStr = null;
				StripeTransaction transaction = null;
				WalletTransaction walletTranx = null;
				CustomerLoyaltyHistory loyaltyHistory = null;
				InvoiceController invoiceControllerObj = new InvoiceController();
				invoiceControllerObj.setSharedProperty(sharedProperty);				
				Property property = null;				   
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(order.getCustomerId());
				Double activePoints = 0.00;
				
				switch(method){
				case CREDITCARD :
					primaryAmount = orderTotalStr;
					transaction = invoiceControllerObj.doCreditCardTransaction(request,primaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					primaryTrxId = transaction.getTransactionId();
					order.setPrimaryPaymentMethod(PaymentMethod.CREDITCARD);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case WALLET :
					primaryAmount = orderTotalStr;
					walletTranx= invoiceControllerObj.doWalletTransaction(request, primaryAmount, Integer.parseInt(orderId));
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					order.setPrimaryPaymentMethod(PaymentMethod.CUSTOMER_WALLET);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case REWARDPOINTS :
					primaryAmount = orderTotalStr;
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						jObj.put("msg", "Selected customer have only "+activePoints+" active reward points.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
				    rewardTxnId =  Long.valueOf(property.getValue());
				    rewardTxnId++;
				    primaryTrxId = PaginationUtil.fullRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
				    DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() - pointSpent);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
											
				    order.setPrimaryPaymentMethod(PaymentMethod.FULL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case PARTIAL_REWARD_CC :
					rewardPointsStr =  request.getParameter("rewardPoints");
					primaryAmount = rewardPointsStr;
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward point amount not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Card amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						jObj.put("msg", "Selected customer have only "+activePoints+" active reward points.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					transaction = invoiceControllerObj.doCreditCardTransaction(request,secondaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
					rewardTxnId =  Long.valueOf(property.getValue());
					rewardTxnId++;
				    primaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						Double pointEarned = (Double.parseDouble(secondaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.PARTIAL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case PARTIAL_REWARD_WALLET :
					rewardPointsStr =  request.getParameter("rewardPoints");
					primaryAmount = rewardPointsStr;
					secondaryAmount = request.getParameter("walletAmount");					
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward point amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						jObj.put("msg", "Selected customer have only "+activePoints+" active reward points.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					walletTranx= invoiceControllerObj.doWalletTransaction(request, secondaryAmount, Integer.parseInt(orderId));
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
					rewardTxnId =  Long.valueOf(property.getValue());
					rewardTxnId++;
					primaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						Double pointEarned = (Double.parseDouble(secondaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.PARTIAL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CUSTOMER_WALLET);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case WALLET_CREDITCARD :
					primaryAmount = request.getParameter("walletAmount");
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Credit Card amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					walletTranx= invoiceControllerObj.doWalletTransaction(request, primaryAmount, Integer.parseInt(orderId));
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,secondaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = ((Double.parseDouble(primaryAmount) + Double.parseDouble(secondaryAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					secondaryTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.CUSTOMER_WALLET);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case PARTIAL_REWARD_WALLET_CC :
					rewardPointsStr =  request.getParameter("rewardPoints");
					primaryAmount = rewardPointsStr;
					secondaryAmount = request.getParameter("walletAmount");
					thirdAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward points amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(thirdAmount==null || thirdAmount.isEmpty()){
						validationMsg ="Credit card amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(primaryAmount)){
						jObj.put("msg", "Selected customer have only "+activePoints+" active reward points.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
				    
					walletTranx= invoiceControllerObj.doWalletTransaction(request, secondaryAmount, Integer.parseInt(orderId));
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,thirdAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				    rewardTxnId =  Long.valueOf(property.getValue());
				    rewardTxnId++;
					primaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(primaryAmount);
						Double pointEarned = ((Double.parseDouble(secondaryAmount) + Double.parseDouble(thirdAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					thirdTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.PARTIAL_REWARDS);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CUSTOMER_WALLET);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					order.setSecondaryTransactionId(secondaryTrxId);
					
					order.setThirdPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setThirdPayAmt(Double.parseDouble(thirdAmount));
					order.setThirdTransactionId(thirdTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = orderTotalStr;
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE_CC :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = request.getParameter("bankChequeAmt");
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Bank/Cheque amount not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Credit card amount not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					transaction = invoiceControllerObj.doCreditCardTransaction(request,secondaryAmount,ticketIds,order.getIsLongSale(),order.getQty());
					if(transaction.getStatus()==0){
						validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = ((Double.parseDouble(primaryAmount) + Double.parseDouble(secondaryAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = transaction.getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CREDITCARD);
					order.setSecondaryTransactionId(secondaryTrxId);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE_WALLET :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = request.getParameter("bankChequeAmt");
					secondaryAmount = request.getParameter("walletAmount");
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Bank/Cheque amount not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					walletTranx= invoiceControllerObj.doWalletTransaction(request, secondaryAmount, Integer.parseInt(orderId));
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointEarned = ((Double.parseDouble(primaryAmount) + Double.parseDouble(secondaryAmount)) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
					
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.CUSTOMER_WALLET);
					order.setSecondaryTransactionId(secondaryTrxId);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				case BANK_OR_CHEQUE_PARTIAL_REWARD :
					primaryTrxId = request.getParameter("bankChequeId");
					primaryAmount = request.getParameter("bankChequeAmt");
					secondaryAmount =  request.getParameter("rewardPoints");
					if(primaryTrxId==null || primaryTrxId.isEmpty()){
						validationMsg ="Transaction Id is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Bank/Cheque amount not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Reward point amount is not found.";
						jObj.put("msg", validationMsg);
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					activePoints = customerLoyalty.getActivePoints();
					if(activePoints < Double.parseDouble(secondaryAmount)){
						jObj.put("msg", "Selected customer have only "+activePoints+" active reward points.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				    rewardTxnId =  Long.valueOf(property.getValue());
				    rewardTxnId++;
				    secondaryTrxId = PaginationUtil.partialRewardPaymentPrefix+rewardTxnId;
				    property.setValue(String.valueOf(rewardTxnId));
					DAORegistry.getPropertyDAO().update(property);
					
					loyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderIdAndCustomerId(order.getId(),customer.getId());
					if(loyaltyHistory != null){
						Double pointSpent = Double.parseDouble(secondaryAmount);
						Double pointEarned = (Double.parseDouble(primaryAmount) * 0.10);
						loyaltyHistory.setPointsEarned(pointEarned);
						loyaltyHistory.setPointsSpent(pointSpent);
						loyaltyHistory.setRewardSpentAmount(pointSpent);
						loyaltyHistory.setInitialRewardAmount(customerLoyalty.getActivePoints());
						loyaltyHistory.setInitialRewardPoints(customerLoyalty.getActivePoints());
						loyaltyHistory.setBalanceRewardPoints(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setBalanceRewardAmount(customerLoyalty.getActivePoints() + pointEarned - pointSpent);
						loyaltyHistory.setUpdatedDate(today);
						DAORegistry.getCustomerLoyaltyHistoryDAO().update(loyaltyHistory);
						
						customerLoyalty.setPendingPoints(customerLoyalty.getPendingPoints() + loyaltyHistory.getPointsEarned());
						customerLoyalty.setTotalSpentPoints(customerLoyalty.getTotalSpentPoints() + pointSpent);
						customerLoyalty.setActivePoints(customerLoyalty.getActivePoints() - pointSpent);
						customerLoyalty.setLatestSpentPoints(pointSpent);
						customerLoyalty.setLastUpdate(today);
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					}
					
					order.setPrimaryPaymentMethod(PaymentMethod.BANK_OR_CHEQUE);
					order.setPrimaryPayAmt(Double.parseDouble(primaryAmount));
					order.setPrimaryTransactionId(primaryTrxId);
					
					order.setSecondaryPaymentMethod(PartialPaymentMethod.PARTIAL_REWARDS);
					order.setSecondaryTransactionId(secondaryTrxId);
					order.setSecondaryPayAmt(Double.parseDouble(secondaryAmount));
					DAORegistry.getCustomerOrderDAO().update(order);
					msg = "Payment details updated for Selected Invoice.";
					break;
				}
				jObj.put("msg", msg);
				
				//Invoice Audit
				if(invoice.getInvoiceType().equalsIgnoreCase(PaymentMethod.ACCOUNT_RECIVABLE.toString())){
					if(invoice.getRealTixMap()!=null && invoice.getRealTixMap().equalsIgnoreCase("Yes")
							&& invoice.getIsRealTixUploaded()!=null && invoice.getIsRealTixUploaded().equalsIgnoreCase("Yes")){
						invoice.setStatus(InvoiceStatus.Completed);
						invoice.setLastUpdated(new Date());
						invoice.setLastUpdatedBy(userName);
						DAORegistry.getInvoiceDAO().update(invoice);
					}
				}
				InvoiceAudit audit = new InvoiceAudit(invoice);
				audit.setAction(InvoiceAuditAction.INVOICE_PAYMENT);
				audit.setNote(invoiceNote);
				audit.setCreatedDate(new Date());
				audit.setCreateBy(userName);
				DAORegistry.getInvoiceAuditDAO().save(audit);
				
				//Tracking User Action
				String userActionMsg = "Invoice Payment is Updated.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			}
			
			if(order != null){
				if(invoice.getInvoiceTotal() > 0){
					if(order != null){
						JSONObject orderObject = new JSONObject();
						orderObject.put("orderId", order.getId());
						orderObject.put("orderTotal", order.getOrderTotal());
						orderObject.put("orderCreateDate", order.getCreatedDateStr());
						orderObject.put("primaryPaymentMethod", order.getPrimaryPaymentMethod());
						orderObject.put("primaryAvailableAmt", order.getPrimaryAvailableAmt());
						orderObject.put("primaryPayAmt", order.getPrimaryPayAmt());
						orderObject.put("eventId", order.getEventId());
//						if(order.getIsLongSale() != null && order.getIsLongSale()){
//							List<Ticket> ticketList = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(invoice.getId());
//							if(ticketList != null && ticketList.size() > 0){
//								TicketGroup ticketGroup = DAORegistry.getTicketGroupDAO().get(ticketList.get(0).getTicketGroupId());
//								orderObject.put("ticketGroupId", ticketGroup.getId());
//							}
//						}else{							
							orderObject.put("ticketGroupId", order.getCategoryTicketGroupId());
//						}
						jObj.put("order", orderObject);
					}						
					if(invoice != null){
						JSONObject invoiceObject = new JSONObject();
						invoiceObject.put("invoiceId", invoice.getId());
						invoiceObject.put("invoiceInternalNote", invoice.getInternalNote());
						jObj.put("invoice", invoiceObject);
					}						
					if(customer != null){
						JSONObject customerObject = new JSONObject();
						customerObject.put("customerId", customer.getId());
						customerObject.put("customerName", customer.getCustomerName());
						customerObject.put("lastName", customer.getLastName());
						customerObject.put("email", customer.getEmail());
						customerObject.put("phone", customer.getPhone());
						jObj.put("customer", customerObject);
					}
					
					Map<String, String> paramterMap = Util.getParameterMap(request);
					paramterMap.put("customerId", String.valueOf(customer.getId()));
					String data = Util.getObject(paramterMap,sharedProperty.getApiUrl()+Constants.GET_CUSTOMER_CARDS);
				    Gson gson = new Gson();		
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					CustomerDetails cardDetails = gson.fromJson(((JsonObject)jsonObject.get("customerDetails")), CustomerDetails.class);
					if(null != cardDetails.getCardList()){
						JSONArray jsonArr = new JSONArray();
						JSONObject jsonObj = null;
						for(CustomerCardInfo customerCardInfo : cardDetails.getCardList()){
							jsonObj = new JSONObject();
							jsonObj.put("cardId", customerCardInfo.getId());
							jsonObj.put("cardType", customerCardInfo.getCardType());
							jsonObj.put("cardNo", customerCardInfo.getCardNo());
							jsonArr.put(jsonObj);
						}
						jObj.put("savedCards", jsonArr);
					}
					
					Map<String, String> paramMap = Util.getParameterMap(request);
					String data1 = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.GET_STRIPE_CREDENTIALS);
					Gson gson1 = new Gson();		
					JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
					StripeCredentials stripeCredentials = gson1.fromJson(((JsonObject)jsonObject1.get("stripeCredentials")), StripeCredentials.class);
					if(stripeCredentials == null){
						jObj.put("msg", "Not able to retrive stripe configuration credential, it happens mostly when rewardthefan API is down.");
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}else if(stripeCredentials.getStatus()==1){
						jObj.put("sPubKey", stripeCredentials.getPublishableKey());
					}else{						
						jObj.put("msg", stripeCredentials.getError().getDescription());
						IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					if(loyalty!=null){
						rewardPoints = loyalty.getActivePoints();
					}
					jObj.put("rewardPoints", rewardPoints);
										
					CustomerWallet wallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customer.getId());
					if(wallet!=null){
						jObj.put("customerCredit", wallet.getActiveCredit());
					}else{
						jObj.put("customerCredit",0);
					}
					jObj.put("status", 1);
				}
			}
			
			//return jsonArr;
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/GenerateInvoicePdfAndSendMail")
	public String generateInvoicePdfAndSendMail(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String invoiceId = request.getParameter("invoiceId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("invoiceId", invoiceId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GENERATE_INVOICE_PDF_AND_SEND_MAIL);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*JSONObject returnObject = new JSONObject();
			 if(invoiceIdStr==null && invoiceIdStr.isEmpty()){
				returnObject.put("msg", "Invoice Id not found.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByIdAndBrokerId(Integer.parseInt(invoiceIdStr), brokerId);
			if(invoice == null){
				returnObject.put("msg", "Invoice not found.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
			//List<Ticket> ticket = DAORegistry.getTicketDAO().getMappedTicketsByInvoiceId(Integer.parseInt(invoiceIdStr));
			CategoryTicketGroup categoryTicketGroup = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByInvoiceId(invoice.getId());
			CustomerOrderDetails orderDetail = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(invoice.getCustomerOrderId());
			//EventDetails event = DAORegistry.getEventDetailsDAO().get(categoryTicketGroup.getEventId());
			CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
			List<OrderTicketGroupDetails> ticketGroupDetails = DAORegistry.getOrderTicketGroupDetailsDAO().getOrderTicketGroupDetailsByOrderId(invoice.getCustomerOrderId());
			
			PDFUtil.generateInvoicePdf(invoice, categoryTicketGroup, ticketGroupDetails, orderDetail, order);
			File file = new File(DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\invoice_"+invoice.getId()+".pdf");
			if(!file.exists()){
				returnObject.put("msg", "Error occured while generating Invoice PDF file.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
			byte[] bytes = ByteStreams.toByteArray(stream);
			
			Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
			if(customer.getEmail()==null || customer.getEmail().isEmpty()){
				returnObject.put("msg", "Customer Email not found.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
				
			Map<String, Object> mailMap = new HashMap<String, Object>();
			mailMap.put("customer", customer);  
			mailMap.put("customerOrder", order);
			mailMap.put("androidUrl", "https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
			mailMap.put("iosUrl", "https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
			
			com.rtw.tracker.mail.MailAttachment[] ticketAttachment = new com.rtw.tracker.mail.MailAttachment[1];
			ticketAttachment[0] = new com.rtw.tracker.mail.MailAttachment(bytes, "application/pdf", file.getName(), DAORegistry.getPropertyDAO().get("tictracker.invoice.pdffile.path").getValue()+"\\"+file.getName());
			
			com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
			mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",Util.getFilePath(request, "share.png", "/resources/images/"));
			mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app1.png",Util.getFilePath(request, "app1.png", "/resources/images/"));
			mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app2.png",Util.getFilePath(request, "app2.png", "/resources/images/"));
			mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",Util.getFilePath(request, "blue.png", "/resources/images/"));
			mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",Util.getFilePath(request, "fb1.png", "/resources/images/"));
			mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",Util.getFilePath(request, "ig1.png", "/resources/images/"));
			mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",Util.getFilePath(request, "li1.png", "/resources/images/"));
			mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo_white.png",Util.getFilePath(request, "logo_white.png", "/resources/images/"));
			mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",Util.getFilePath(request, "p1.png", "/resources/images/"));
			mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",Util.getFilePath(request, "tw1.png", "/resources/images/"));
			mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
			String email = customer.getEmail();
			String bcc = "";
			bcc = "amit.raut@rightthisway.com,msanghani@rightthisway.com";
			try {
				mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
						null,bcc, "Reward The Fan : Your invoice for order No : "+order.getId(),
			    		"mail-rewardfan-invoice-pdf.html", mailMap, "text/html", ticketAttachment, mailAttachment,null);
			} catch (Exception e) {
				e.printStackTrace();
				returnObject.put("msg", "Error occured while sending invoice PDF to customer.");
				IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			//Tracking User Action
			String userActionMsg = "Invoice Sent Successfully Through Customer Email.";
			Util.userActionAudit(request, invoice.getId(), userActionMsg);
			
			returnObject.put("status", 1);
			returnObject.put("msg", "Invoice Sent Successfully.");
			//return jsonArr;
			IOUtils.write(returnObject.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
}

