package com.rtw.tracker.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.firebase.FirestoreUtil;
import com.rtw.tracker.pojos.AWSResponseDTO;
import com.rtw.tracker.pojos.AbuseReportedDTO;
import com.rtw.tracker.pojos.AutoCompleteArtistAndCategoryDTO;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.PollingCategoryDTO;
import com.rtw.tracker.pojos.PollingCategoryQuestionDTO;
import com.rtw.tracker.pojos.PollingContestCategoryMapperDTO;
import com.rtw.tracker.pojos.PollingContestDTO;
import com.rtw.tracker.pojos.PollingCustomerVideoDTO;
import com.rtw.tracker.pojos.PollingRewardsDTO;
import com.rtw.tracker.pojos.PollingSponsorDTO;
import com.rtw.tracker.pojos.PollingVideoCategoryDTO;
import com.rtw.tracker.pojos.PollingVideoInventoryDTO;
import com.rtw.tracker.pojos.RTFRewardConfigDTO;
import com.rtw.tracker.pojos.RtfPointConversionSettingDTO;
import com.rtw.tracker.pojos.SuperFanLevelDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;
@Controller
public class PollingWebController {
	private static Logger contestLog = LoggerFactory.getLogger(PollingWebController.class);
	
	
	@RequestMapping(value = "/PollingSponsor")
	public String getPollingSponsor(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String pcStatus = request.getParameter("pcStatus");
			String status = request.getParameter("status");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("pcStatus", pcStatus);
			map.put("status", status);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_SPONSORS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			
			PollingSponsorDTO pollingSponsorDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingSponsorDTO")), PollingSponsorDTO.class);
			
			if(pollingSponsorDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollingSponsor", pollingSponsorDTO.getPollingSponsor());
					model.addAttribute("pollingSponsorPagingInfo", pollingSponsorDTO.getPagination());
				}else{
					model.addAttribute("pollingSponsor", gson.toJson(pollingSponsorDTO.getPollingSponsor()));
					model.addAttribute("pollingSponsorPagingInfo",gson.toJson(pollingSponsorDTO.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", pollingSponsorDTO.getError().getDescription());
			}
			model.addAttribute("pcStatus",pcStatus);
			model.addAttribute("status",pollingSponsorDTO.getStatus());
			
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-manage-polling-sponsor";
	}
	

	
	
	@RequestMapping(value = "/PollingCategory")
	public String getPollingCategory(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String gcStatus = request.getParameter("gcStatus");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("gcStatus", gcStatus);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			
			PollingCategoryDTO pollingCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingCategoryDTO")), PollingCategoryDTO.class);
			
			if(pollingCategoryDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollingCategory", pollingCategoryDTO.getPollingCategory());
					model.addAttribute("pollingCategoryPagingInfo", pollingCategoryDTO.getPagination());
				}else{
					model.addAttribute("pollingCategory", gson.toJson(pollingCategoryDTO.getPollingCategory()));
					model.addAttribute("pollingCategoryPagingInfo",gson.toJson(pollingCategoryDTO.getPagination()));
				}
				
			}else{
				model.addAttribute("msg", pollingCategoryDTO.getError().getDescription());
			}
			model.addAttribute("gcStatus",gcStatus);
			model.addAttribute("status",pollingCategoryDTO.getStatus());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "page-manage-polling-category";
	}
	
	@RequestMapping(value = "/GetPollingCategoryByContId")
	public String getPollingCategoryByContId(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try{
			String pollId = request.getParameter("pollId");
			String pageNo = request.getParameter("pageNo");
			String pcStatus = request.getParameter("pcStatus");
			String headerFilter = request.getParameter("headerFilter");			
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pollId", pollId);
			map.put("pageNo", pageNo);
			map.put("pcStatus", pcStatus);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_CATEGORY_BY_CONTID);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingContestCategoryMapperDTO pollingContestCategoryMapperDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingContestCategoryMapperDTO")), PollingContestCategoryMapperDTO.class);
			
			if(pollingContestCategoryMapperDTO.getStatus() == 1){
				model.addAttribute("pollingCategoryPagingInfo", pollingContestCategoryMapperDTO.getPagination());
				model.addAttribute("pollingCategory", pollingContestCategoryMapperDTO.getPollingContCategoryMapper());
				model.addAttribute("msg", pollingContestCategoryMapperDTO.getMessage());
				model.addAttribute("status", pollingContestCategoryMapperDTO.getStatus());
			}else{
				model.addAttribute("msg", pollingContestCategoryMapperDTO.getError().getDescription());
				model.addAttribute("status", pollingContestCategoryMapperDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/UpdatePollingCategoryMapper")
	public String updatePollingCategoryMapper(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("pcStatus");
			String id = request.getParameter("id");
			String categoryIds = request.getParameter("categoryIds");
			String pollId = request.getParameter("pollId");
			String pageNo = request.getParameter("pageNo");
			String pcStatus = request.getParameter("pcStatus");
			String action = request.getParameter("action");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("id", id);
			map.put("status", status);
			map.put("pollId", pollId);
			map.put("categoryIds", categoryIds);
			map.put("pageNo", pageNo);
			map.put("action", action);
			map.put("pcStatus", pcStatus);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_CATEGORY_MAPPER);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingContestCategoryMapperDTO pollingContestCategoryMapperDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingContestCategoryMapperDTO")), PollingContestCategoryMapperDTO.class);
			
			if(pollingContestCategoryMapperDTO.getStatus() == 1){
				model.addAttribute("pollingCategoryPagingInfo", pollingContestCategoryMapperDTO.getPagination());
				model.addAttribute("pollingCategory", pollingContestCategoryMapperDTO.getPollingContCategoryMapper());
				model.addAttribute("msg", pollingContestCategoryMapperDTO.getMessage());
				model.addAttribute("status", pollingContestCategoryMapperDTO.getStatus());
			}else{
				model.addAttribute("msg", pollingContestCategoryMapperDTO.getError().getDescription());
				model.addAttribute("status", pollingContestCategoryMapperDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/GetPollingCategorySelection")
	public String getPollingCategorySelection(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try{
			String pollId = request.getParameter("pollId");
			String pageNo = request.getParameter("pageNo");
			String pcStatus = request.getParameter("pcStatus");
			String headerFilter = request.getParameter("headerFilter");			
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pollId", pollId);
			map.put("pageNo", pageNo);
			map.put("pcStatus", pcStatus);
			map.put("headerFilter", headerFilter);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_CATEGORY_SELECTION);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingContestCategoryMapperDTO pollingContestCategoryMapperDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingContestCategoryMapperDTO")), PollingContestCategoryMapperDTO.class);
			
			if(pollingContestCategoryMapperDTO.getStatus() == 1){
				model.addAttribute("categoryPagingInfo", pollingContestCategoryMapperDTO.getPagination());
				model.addAttribute("category", pollingContestCategoryMapperDTO.getPollingContCategoryMapper());
				model.addAttribute("msg", pollingContestCategoryMapperDTO.getMessage());
				model.addAttribute("status", pollingContestCategoryMapperDTO.getStatus());
			}else{
				model.addAttribute("msg", pollingContestCategoryMapperDTO.getError().getDescription());
				model.addAttribute("status", pollingContestCategoryMapperDTO.getStatus());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}

	@RequestMapping(value = "/PollingContest")
	public String getPollingContest(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String pcStatus = request.getParameter("pcStatus");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("pcStatus", pcStatus);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_CONTEST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			PollingContestDTO pollingContestDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingContestDTO")), PollingContestDTO.class);
			
			if(pollingContestDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollingContest", pollingContestDTO.getPollingContest());
					model.addAttribute("pollingContestPagingInfo", pollingContestDTO.getPagination());
				}else{
					model.addAttribute("pollingContest", gson.toJson(pollingContestDTO.getPollingContest()));
					model.addAttribute("pollingContestPagingInfo",gson.toJson(pollingContestDTO.getPagination()));
				}				
			}else{
				model.addAttribute("msg", pollingContestDTO.getError().getDescription());
			}
			model.addAttribute("pcStatus",pcStatus);
			model.addAttribute("status",pollingContestDTO.getStatus());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "page-manage-polling-contest";
	}
	@RequestMapping(value = "/EditPollingPrizes")
	public String editPollingPrizes(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String contestId = request.getParameter("contestId");
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();		
			map.put("userName", userName);	
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_PRIZE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingRewardsDTO pollingRewardsDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingRewardsDTO")), PollingRewardsDTO.class);
			
			if(pollingRewardsDTO.getStatus() == 1){				
				model.addAttribute("msg", pollingRewardsDTO.getMessage());
				model.addAttribute("status", pollingRewardsDTO.getStatus());				
				model.addAttribute("rewards", pollingRewardsDTO.getPollingRewards());
				
				
			}else{
				model.addAttribute("msg", pollingRewardsDTO.getError().getDescription());
				model.addAttribute("status", pollingRewardsDTO.getStatus());
			}			
		
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong.Please try again..");
		}
		return "";
	}
	

	@RequestMapping(value = "/UpdatePollingSponsor")
	public String UpdatePollingSponsor(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("pcStatus");
			String id = request.getParameter("id");
			String name = request.getParameter("name");			
			String description = request.getParameter("description");
			String contactPerson = request.getParameter("contactPerson");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String altPhone = request.getParameter("altPhone");
			String address = request.getParameter("address");
			String fileRequired = request.getParameter("fileRequired");
			String imageUrl = request.getParameter("imageUrl");
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("id", id);
			map.put("status", status);
			map.put("name", name);
			map.put("description", description);
			map.put("contactPerson", contactPerson);
			map.put("email", email);
			map.put("phone", phone);
			map.put("altPhone", altPhone);
			map.put("address", address);
			map.put("imageUrl", imageUrl);
			map.put("fileRequired",fileRequired);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLINGSPONSOR);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingSponsorDTO pollingSponsorDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingSponsorDTO")), PollingSponsorDTO.class);
			
			if(pollingSponsorDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollingSponsor", pollingSponsorDTO.getPollingSponsor());
					model.addAttribute("pollSponsor",gson.toJson(pollingSponsorDTO.getPollSponsor()));
					model.addAttribute("pollingSponsorPagingInfo", pollingSponsorDTO.getPagination());
				}else{
					
					model.addAttribute("pollingSponsor",gson.toJson(pollingSponsorDTO.getPollingSponsor()));
					model.addAttribute("pollSponsor",gson.toJson(pollingSponsorDTO.getPollSponsor()));
					model.addAttribute("pollingSponsorPagingInfo",gson.toJson( pollingSponsorDTO.getPagination()));
					
				}
				model.addAttribute("msg",pollingSponsorDTO.getMessage());
			}else{
				model.addAttribute("msg", pollingSponsorDTO.getError().getDescription());
			}
			model.addAttribute("status",status);
			model.addAttribute("status",pollingSponsorDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdatePollingPrizes")
	public String updatePollingPrizes(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("pcStatus");
			String pollId = request.getParameter("pollId");
			String pollPrizeId = request.getParameter("pollPrizeId");
			String starsPerQue = request.getParameter("starsPerQue");
			String lifePerQue = request.getParameter("lifePerQue");
			String eraserPerQue = request.getParameter("eraserPerQue");
			String rtfPointsPerQue = request.getParameter("rtfPointsPerQue");
			String action = request.getParameter("action");
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pollId", pollId);
			map.put("status", status);
			map.put("action", action);
			map.put("pollPrizeId", pollPrizeId);
			map.put("starsPerQue", starsPerQue);
			map.put("lifePerQue", lifePerQue);
			map.put("eraserPerQue", eraserPerQue);
			map.put("rtfPointsPerQue", rtfPointsPerQue);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_PRIZE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());				
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong. Please try again..");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdatePollingContest")
	public String UpdatePollingContest(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String status = request.getParameter("pcStatus");
			String id = request.getParameter("id");
			String name = request.getParameter("name");			
			String description = request.getParameter("description");
			String pollingInterval = request.getParameter("pollingInterval");
			String maxQuePerCust = request.getParameter("maxQuePerCust");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String action = request.getParameter("action");
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("id", id);
			map.put("pollingId", id);
			map.put("status", status);
			map.put("name", name);
			map.put("description", description);
			map.put("pollingInterval", pollingInterval);
			map.put("maxQuePerCust", maxQuePerCust);
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			map.put("userName", SecurityContextHolder.getContext().getAuthentication().getName());
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_CONTEST);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingContestDTO pollingContestDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingContestDTO")), PollingContestDTO.class);
			
			if(pollingContestDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollContest", pollingContestDTO.getPollContest());
					model.addAttribute("pollingContest", pollingContestDTO.getPollingContest());
					model.addAttribute("pollingContCategories",gson.toJson(pollingContestDTO.getPollingContCategoriesList()));
					model.addAttribute("pollingContestPagingInfo", pollingContestDTO.getPagination());
				}else{
					model.addAttribute("pollContest",gson.toJson(pollingContestDTO.getPollContest()));
					model.addAttribute("pollingContest",gson.toJson(pollingContestDTO.getPollingContest()));
					model.addAttribute("pollingContCategories",gson.toJson(pollingContestDTO.getPollingContCategoriesList()));
					model.addAttribute("pollingContestPagingInfo",gson.toJson( pollingContestDTO.getPagination()));
					
				}
				try {
					if("START".equals(action)) {
						FirestoreUtil.setPollingStartedStaus(true);
						FirestoreUtil.setPollingInterval(Integer.valueOf(pollingInterval));
						System.out.println("[Started Firebase Polling status]");
					}
					if("STOP".equals(action)) {
						FirestoreUtil.setPollingStartedStaus(false);
						System.out.println("[Stopped Firebase Polling status]");
					}
					}catch(Exception ex) {
						ex.printStackTrace();
						model.addAttribute("status",0);
						model.addAttribute("msg", "There is something wrong while updating Firebase.Please Try Again.");
					}
				model.addAttribute("msg",pollingContestDTO.getMessage());
			}else{
				model.addAttribute("msg", pollingContestDTO.getError().getDescription());
			}
			model.addAttribute("status",status);
			model.addAttribute("status",pollingContestDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping("/AutoCompletePollingSponsor")
	public void getAutoCompletePollingSponsor(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			PrintWriter writer =  response.getWriter();			
			Map<String, String> map = Util.getParameterMap(request);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_AUTO_COMPLETE_POLLING_SPONSOR);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
 			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
 			AutoCompleteArtistAndCategoryDTO autoCompleteArtistAndCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("autoCompleteArtistAndCategoryDTO")), AutoCompleteArtistAndCategoryDTO.class);
						
			if(autoCompleteArtistAndCategoryDTO.getStatus() == 1){
				writer.write(autoCompleteArtistAndCategoryDTO.getArtistAndCategory());
			}

		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/UpdatePollingCategory")
	public String updatePollingCategory(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			
			String status = request.getParameter("status");
			String categoryIdStr = request.getParameter("categoryId");
			String title = request.getParameter("title");
			String pollingType = request.getParameter("pollingType");
			String sponsorStr = request.getParameter("sponsor"); 
			String action = request.getParameter("action"); 
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("categoryId", categoryIdStr);
			map.put("status", status);
			map.put("title", title);
			map.put("pollingType", pollingType);
			map.put("sponsorStr", sponsorStr); 
			map.put("action", action); 
			map.put("userName",userName); 
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingCategoryDTO dto = gson.fromJson(((JsonObject)jsonObject.get("pollingCategoryDTO")), PollingCategoryDTO.class);
			
			if(dto.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollingCategoryList", dto.getPollingCategory());
					model.addAttribute("pagingInfo", dto.getPagination());
					model.addAttribute("pollingCategory", dto.getPollingCategoryObj());
				}else{
					model.addAttribute("pollingCategoryList", gson.toJson(dto.getPollingCategory()));
					model.addAttribute("pagingInfo",gson.toJson(dto.getPagination()));
					model.addAttribute("pollingCategory",gson.toJson( dto.getPollingCategoryObj()));
				}
				model.addAttribute("msg",dto.getMessage());
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
			}
			model.addAttribute("gcStatus",status);
			model.addAttribute("status",dto.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdatePollingCategoryQuestion")
	public String updatePollingCategoryQuestions(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try {
			String action = request.getParameter("action");
			String categoryId = request.getParameter("qCategoryId");
			String questionId = request.getParameter("questionId");
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC"); 
			String answer = request.getParameter("answer"); 
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("categoryId", categoryId);
			map.put("questionId", questionId);
			map.put("questionText", questionText);
			map.put("optionA", optionA);
			map.put("optionB", optionB);
			map.put("optionC", optionC);
			map.put("answer", answer);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_CATEGORY_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingCategoryQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingCategoryQuestionDTO")), PollingCategoryQuestionDTO.class);
			
			if(questionDTO.getError() == null && questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
				model.addAttribute("questionList", questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo", questionDTO.getPaginationDTO());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());				
			}
			model.addAttribute("status", questionDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/PollingCategoryQuestion")
	public String getPollingCategoryQuestion(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String categoryId = request.getParameter("categoryId");
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("categoryId", categoryId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_CATEGORY_QUESTIONS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingCategoryQuestionDTO questionDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingCategoryQuestionDTO")), PollingCategoryQuestionDTO.class);
			if(questionDTO.getStatus() == 1){
				model.addAttribute("msg", questionDTO.getMessage());
			}else{
				model.addAttribute("msg", questionDTO.getError().getDescription());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("questionList", questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo", questionDTO.getPaginationDTO());
			}else{
				model.addAttribute("questionList",questionDTO.getQuestionList());
				model.addAttribute("questionPagingInfo",questionDTO.getPaginationDTO());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/PollingVideos")
	public String getAllPollingVideos(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String videoStatus = request.getParameter("videoStatus");
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			map.put("videoStatus", videoStatus);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingVideoInventoryDTO videoDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingVideoInventoryDTO")), PollingVideoInventoryDTO.class);
			
			if(videoDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", videoDTO.getVideoList());
					model.addAttribute("videoPagingInfo", videoDTO.getPagination());
					model.addAttribute("category", (videoDTO.getCategoryList()));
					model.addAttribute("sponsors", videoDTO.getSponsors());
				}else{
					model.addAttribute("videos", gson.toJson(videoDTO.getVideoList()));
					model.addAttribute("videoPagingInfo",gson.toJson(videoDTO.getPagination()));
					model.addAttribute("category", (videoDTO.getCategoryList()));
					model.addAttribute("sponsors", (videoDTO.getSponsors()));
				}
				
			}else{
				model.addAttribute("msg", videoDTO.getError().getDescription());
			}
			model.addAttribute("status",videoDTO.getStatus());
			model.addAttribute("videoStatus",videoStatus);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "page-manage-polling-videos";
	}
	
	
	
	
	
	@RequestMapping(value = "/UpdatePollingVideo")
	public String updatePollingVideo(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String title = request.getParameter("title");
			String description = request.getParameter("description");
			String videoId = request.getParameter("videoId");
			String category = request.getParameter("category");
			String videoFile = request.getParameter("videoFile");
			String action = request.getParameter("action");
			String sponsor = request.getParameter("sponsor");
			String imageFile = request.getParameter("imageFile");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("videoId", videoId);
			map.put("category", category);
			map.put("title", title);
			map.put("description", description);
			map.put("videoFile", videoFile);
			map.put("action", action);
			map.put("sponsor", sponsor);
			map.put("userName", userName);
			map.put("imageFile", imageFile);
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingVideoInventoryDTO videoDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingVideoInventoryDTO")), PollingVideoInventoryDTO.class);
			
			if(videoDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", videoDTO.getVideoList());
					model.addAttribute("videoPagingInfo", videoDTO.getPagination());
					model.addAttribute("video", videoDTO.getVideo());
				}else{
					model.addAttribute("videos", gson.toJson(videoDTO.getVideoList()));
					model.addAttribute("videoPagingInfo",gson.toJson(videoDTO.getPagination()));
					model.addAttribute("video",gson.toJson( videoDTO.getVideo()));
				}
				model.addAttribute("msg",videoDTO.getMessage());
			}else{
				model.addAttribute("msg", videoDTO.getError().getDescription());
			}
			model.addAttribute("status",videoDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/ManageCustomerMedia")
	public String loadManageCustomerMedia(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo"); 
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			String name = request.getParameter("name");
			String videoType = request.getParameter("videoType");
			String playType = request.getParameter("playType"); 
			
			Map<String, String> map = Util.getParameterMap(request);
			
			map.put("fromDate", fromDate);
			map.put("toDate", toDate);
			map.put("name", name);
			map.put("videoType", videoType);
			map.put("playType", playType); 
			map.put("pageNo", pageNo);
			map.put("gcStatus", "ACTIVE");
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_CUST_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			PollingCustomerVideoDTO dto = gson.fromJson(((JsonObject)jsonObject.get("pollingCustomerVideoDTO")), PollingCustomerVideoDTO.class);
			
			if(dto.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("pollingVideos", dto.getCustomerVideos());
					model.addAttribute("category", dto.getCategories());
					model.addAttribute("pollingVideosPagingInfo", dto.getPagination());
				}else{
					model.addAttribute("pollingVideos", gson.toJson(dto.getCustomerVideos()));
					model.addAttribute("category", dto.getCategories());
					model.addAttribute("pollingVideosPagingInfo",gson.toJson(dto.getPagination()));
				}
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "page-manage-customer-media";
	}
	
	
	@RequestMapping(value = "/ManagePollingCustomerVideo")
	public String managePollingVideos(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			String videoIds = request.getParameter("videoIds"); 
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("videoIds", videoIds);
			 
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_MANAGE_POLLING_CUST_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "page-manage-customer-media";
	}
	
	
	
	@RequestMapping(value = "/PublishCustomerMediaToRTFMedia")
	public String publishCustomerMediaToRTFMedia(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String videoIds = request.getParameter("custVideoId"); 
			String categoryId = request.getParameter("categoryId"); 
			String title = request.getParameter("title"); 
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("videoIds", videoIds);
			map.put("categoryId", categoryId);
			map.put("title", title);
			 
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_PUBLISH_CUSTOMER_MEDIA);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
						
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "";
	}
	
	@RequestMapping(value = "/DownloadCustomerVideoMedia")
	public void downloadCustomerVideoMedia(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String action = request.getParameter("action"); 
			String videoIds = request.getParameter("videoIds"); 
			 
			Map<String, String> map = Util.getParameterMap(request);
			map.put("action", action);
			map.put("videoIds", videoIds);
			 
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_DOWNLOAD_POLLING_CUST_VIDEOS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AWSResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("AWSResponseDTO")), AWSResponseDTO.class);
			String filePath = genericResponseDTO.getFilePath();	
			File file  = new File(filePath);
			
			OutputStream out = response.getOutputStream();
			if(file.exists()) {
					FileInputStream in = new FileInputStream(file);
					response.setContentType("application/octet-stream");
					response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"",file.getName()));
					response.setHeader("Content-Length",String.valueOf(file.length()));
					byte[] buffer = new byte[4096];
					int length;
					while((length = in.read(buffer)) > 0){
					    out.write(buffer, 0, length);
					}
					in.close();
					out.flush();
				
			}
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());			
			}else{
				model.addAttribute("msg", genericResponseDTO.getError()!=null?genericResponseDTO.getError().getDescription():"");
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
//		return "";
	}

	@RequestMapping(value = "/ManageVideoCategory")
	public String loadManageVideoCategory(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String pageNo = request.getParameter("pageNo"); 
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			String name = request.getParameter("name");
			String videoType = request.getParameter("videoType");
			String playType = request.getParameter("playType"); 
			Map<String, String> map = Util.getParameterMap(request);
			
			
			map.put("name", name);
			map.put("videoType", videoType);
			map.put("playType", playType); 
			map.put("pageNo", pageNo);
			map.put("gcStatus", "ACTIVE");
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_POLLING_VIDEO_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			
			PollingVideoCategoryDTO dto = gson.fromJson(((JsonObject)jsonObject.get("pollingVideoCategoryDTO")), PollingVideoCategoryDTO.class);
			
			if(dto.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", dto.getPollingVideoCategories());
					model.addAttribute("videoPagingInfo", dto.getPagination());
				}else{
					model.addAttribute("videos", gson.toJson(dto.getPollingVideoCategories()));
					model.addAttribute("videoPagingInfo",gson.toJson(dto.getPagination()));
				}
			}else{
				model.addAttribute("msg", dto.getError().getDescription());
			}
			model.addAttribute("videoStatus","ACTIVE");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		
		return "page-manage-polling--videio-category";
	}
	
	@RequestMapping(value = "/UpdatePollingVideoCategory")
	public String updatePollingVideoCategory(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String categoryName = request.getParameter("categoryName");
			String categoryDescription = request.getParameter("categoryDescription");
			String id = request.getParameter("id");
			String imageFile = request.getParameter("imageFile");
			String iconFile = request.getParameter("iconFile");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
									
			Map<String, String> map = Util.getParameterMap(request);
			map.put("categoryDescription", categoryDescription);
			map.put("id", id);
			map.put("categoryName", categoryName);
			map.put("imageFile", imageFile);
			map.put("iconFile", iconFile);
			map.put("action", action);
			map.put("userName", userName);
			
			
			String data = Util.getObjectFromApi(map,request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_POLLING_VIDEO_CATEGORY);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingVideoCategoryDTO videoCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingVideoCategoryDTO")), PollingVideoCategoryDTO.class);
			
			if(videoCategoryDTO.getStatus() == 1){
				if(!action.equalsIgnoreCase("EDIT")){
					FirestoreUtil.updatePopularChannelDetails();
				}
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", videoCategoryDTO.getPollingVideoCategories());
					model.addAttribute("video", videoCategoryDTO.getPollingVideoCategory());
					model.addAttribute("videoPagingInfo", videoCategoryDTO.getPagination());
				}else{
					model.addAttribute("videos", gson.toJson(videoCategoryDTO.getPollingVideoCategories()));
					model.addAttribute("video", videoCategoryDTO.getPollingVideoCategory());
					model.addAttribute("videoPagingInfo",gson.toJson(videoCategoryDTO.getPagination()));
					
					
				}
				model.addAttribute("msg",videoCategoryDTO.getMessage());
			}else{
				model.addAttribute("msg", videoCategoryDTO.getError().getDescription());
			}
			model.addAttribute("status",videoCategoryDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("status",0);
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateSequencePosition")
	public String updateSequencePosition(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String contestId = request.getParameter("contestId");
			String sequenceString = request.getParameter("sequenceString");
			String qId = request.getParameter("id");
			String position = request.getParameter("position");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("contestId", contestId);
			map.put("qId", qId);
			map.put("sequenceString", sequenceString);
			map.put("userName",userName);
			map.put("position",position);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_VIDEO_CATEGORY_SEQUENCE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			PollingVideoCategoryDTO videoCategoryDTO = gson.fromJson(((JsonObject)jsonObject.get("pollingVideoCategoryDTO")), PollingVideoCategoryDTO.class);
			
			if(videoCategoryDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("videos", videoCategoryDTO.getPollingVideoCategories());
					model.addAttribute("video", videoCategoryDTO.getPollingVideoCategory());
					model.addAttribute("videoPagingInfo", videoCategoryDTO.getPagination());
				}else{
					model.addAttribute("videos", gson.toJson(videoCategoryDTO.getPollingVideoCategories()));
					model.addAttribute("video", videoCategoryDTO.getPollingVideoCategory());
					model.addAttribute("videoPagingInfo",gson.toJson(videoCategoryDTO.getPagination()));
					
					
				}
				model.addAttribute("msg",videoCategoryDTO.getMessage());
			}else{
				model.addAttribute("msg", videoCategoryDTO.getError().getDescription());
			}
			model.addAttribute("status",videoCategoryDTO.getStatus());
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/GetCustomerMediaUrl")
	public String getCustomerMediaUrl(HttpServletRequest request, HttpServletResponse response,Model model){
		try {
			String id = request.getParameter("id");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("id", id);
			map.put("userName",userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_CUSTOMER_MEDIA_URL);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericDTO.getStatus() == 1){
					model.addAttribute("URL", genericDTO.getMessage());
			}else{
				model.addAttribute("msg", genericDTO.getError().getDescription());
			}
			model.addAttribute("status",genericDTO.getStatus());
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/RewardConfigSettings")
	public String rewardConfigSettings(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_REWARD_CONFIG_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RTFRewardConfigDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("RTFRewardConfigDTO")),RTFRewardConfigDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("configs", respDTO.getConfigs());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("configs", gson.toJson(respDTO.getConfigs()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("configs", respDTO.getConfigs());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-reward-config-settings";
	}
	
	
	@RequestMapping(value = "/UpdateRewardConfig")
	public String updateRewardConfig(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String title = request.getParameter("title");
			String configIdStr = request.getParameter("configId");
			String actionType = request.getParameter("actionType");
			String livesStr = request.getParameter("lives");
			String magicWandStr = request.getParameter("magicWand");
			String starsStr = request.getParameter("stars");
			String pointsStr = request.getParameter("points");
			String dollarsStr = request.getParameter("dollars");
			String maxReward = request.getParameter("maxReward");
			String noOfLikes = request.getParameter("noOfLikes");
			String minVideoTime = request.getParameter("minVideoTime");
			String rewardInterval = request.getParameter("rewardInterval");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("title", title);
			map.put("configId", configIdStr);
			map.put("actionType", actionType);
			map.put("lives", livesStr);
			map.put("action", action);
			map.put("magicWand", magicWandStr);
			map.put("userName", userName);
			map.put("stars", starsStr);
			map.put("points", pointsStr);
			map.put("dollars", dollarsStr);
			map.put("maxReward", maxReward);
			map.put("noOfLikes", noOfLikes);
			map.put("minVideoTime", minVideoTime);
			map.put("rewardInterval", rewardInterval);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_UPDATE_REWARD_CONFIG_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RTFRewardConfigDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("RTFRewardConfigDTO")),RTFRewardConfigDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("config", respDTO.getConfig());
					model.addAttribute("configs", respDTO.getConfigs());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("config", respDTO.getConfig());
					model.addAttribute("configs", gson.toJson(respDTO.getConfigs()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
				model.addAttribute("msg", respDTO.getMessage());
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("config", respDTO.getConfig());
				model.addAttribute("configs", respDTO.getConfigs());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	
	@RequestMapping(value = "/PointConversionSettings")
	public String pointConversionSettings(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_POINT_CONVERSION_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfPointConversionSettingDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("rtfPointConversionSettingDTO")),RtfPointConversionSettingDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("settings", respDTO.getSettings());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("settings", gson.toJson(respDTO.getSettings()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("settings", respDTO.getSettings());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-point-conversion-settings";
	}
	
	
	@RequestMapping(value = "/UpdateRtfPointsConvSetting")
	public String updateRtfPointsConvSetting(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String powerUpType = request.getParameter("powerUpType");
			String settingIdStr = request.getParameter("settingId");
			String qtyStr = request.getParameter("qty");
			String pointsStr = request.getParameter("points");
			String action = request.getParameter("action");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("powerUpType", powerUpType);
			map.put("settingId", settingIdStr);
			map.put("qty", qtyStr);
			map.put("points", pointsStr);
			map.put("action", action);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_UPDATE_POINT_CONVERSION_SETTINGS);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			RtfPointConversionSettingDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("rtfPointConversionSettingDTO")),RtfPointConversionSettingDTO.class);
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("setting", respDTO.getSetting());
					model.addAttribute("settings", respDTO.getSettings());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("setting", respDTO.getSetting());
					model.addAttribute("settings", gson.toJson(respDTO.getSettings()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
				model.addAttribute("msg", respDTO.getMessage());
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("setting", respDTO.getSetting());
				model.addAttribute("settings", respDTO.getSettings());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	
	@RequestMapping(value = "/AbuseReportedComment")
	public String abuseReportedComment(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_ABUSED_COMMENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AbuseReportedDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("abuseReportedDTO")),AbuseReportedDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("abuseDatas", respDTO.getReportedDatas());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("abuseDatas", gson.toJson(respDTO.getReportedDatas()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("abuseDatas", respDTO.getReportedDatas());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-abuse-comment";
	}
	
	
	@RequestMapping(value = "/UpdateAbuseComment")
	public String updateAbuseComment(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String abuseId  = request.getParameter("abuseId");
			String action = request.getParameter("action");
			String reason = request.getParameter("reason");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("abuseId", abuseId);
			map.put("action", action);
			map.put("reason", reason);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_UPDATE_ABUSED_COMMENT);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AbuseReportedDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("abuseReportedDTO")),AbuseReportedDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("abuseDatas", respDTO.getReportedDatas());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("msg", respDTO.getMessage());
				}else{
					model.addAttribute("abuseDatas", gson.toJson(respDTO.getReportedDatas()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("msg", respDTO.getMessage());
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("abuseDatas", respDTO.getReportedDatas());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status", respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/AbuseReportedMedia")
	public String abuseReportedMedia(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_ABUSED_MEDIA);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AbuseReportedDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("abuseReportedDTO")),AbuseReportedDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("abuseDatas", respDTO.getReportedDatas());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("abuseDatas", gson.toJson(respDTO.getReportedDatas()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("abuseDatas", respDTO.getReportedDatas());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-abuse-media";
	}
	
	
	@RequestMapping(value = "/UpdateAbuseMedia")
	public String updateAbuseMedia(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String abuseId  = request.getParameter("abuseId");
			String action = request.getParameter("action");
			String reason = request.getParameter("reason");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("abuseId", abuseId);
			map.put("action", action);
			map.put("reason", reason);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_UPDATE_ABUSED_MEDIA);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			AbuseReportedDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("abuseReportedDTO")),AbuseReportedDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("abuseDatas", respDTO.getReportedDatas());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("msg", respDTO.getMessage());
				}else{
					model.addAttribute("abuseDatas", gson.toJson(respDTO.getReportedDatas()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("msg", respDTO.getMessage());
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("abuseDatas", respDTO.getReportedDatas());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status", respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
	
	@RequestMapping(value = "/SuperFanLevelConfig")
	public String superFanLevelConfig(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String headerFilter  = request.getParameter("headerFilter");
			String status = request.getParameter("status");

			Map<String, String> map = Util.getParameterMap(request);
			map.put("headerFilter", headerFilter);
			map.put("status", status);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_GET_SUPERFAN_LEVEL_CONFIG);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SuperFanLevelDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("superFanLevelDTO")),SuperFanLevelDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("levels", respDTO.getLevels());
					model.addAttribute("pagingInfo", respDTO.getPagination());
				}else{
					model.addAttribute("levels", gson.toJson(respDTO.getLevels()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("levels", respDTO.getLevels());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status",status);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "page-super-fan-level-config";
	}
	
	
	@RequestMapping(value = "/UpdateSuperFanLevelConfig")
	public String updateSuperFanLevelConfig(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String action = request.getParameter("action");
			String levelId = request.getParameter("levelId");
			String starsFrom = request.getParameter("starsFrom");
			String starsTo = request.getParameter("starsTo");
			String levelNo = request.getParameter("levelNo");
			String title = request.getParameter("title");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();	

			Map<String, String> map = Util.getParameterMap(request);
			map.put("levelId", levelId);
			map.put("action", action);
			map.put("starsFrom", starsFrom);
			map.put("starsTo", starsTo);
			map.put("levelNo", levelNo);
			map.put("title", title);
			map.put("userName", userName);

			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL + Constants.TICTRACKER_API_UPDATE_SUPERFAN_LEVEL_CONFIG);
			Gson gson = GsonCustomConfig.getGsonBuilder();
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			SuperFanLevelDTO respDTO = gson.fromJson(((JsonObject) jsonObject.get("superFanLevelDTO")),SuperFanLevelDTO.class);
			
			if(respDTO.getStatus() == 1){
				if(request.getRequestURI().contains(".json")){
					model.addAttribute("levels", respDTO.getLevels());
					model.addAttribute("level", respDTO.getLevel());
					model.addAttribute("pagingInfo", respDTO.getPagination());
					model.addAttribute("msg", respDTO.getMessage());
				}else{
					model.addAttribute("levels", gson.toJson(respDTO.getLevels()));
					model.addAttribute("level", gson.toJson(respDTO.getLevel()));
					model.addAttribute("pagingInfo",gson.toJson(respDTO.getPagination()));
					model.addAttribute("msg", respDTO.getMessage());
				}
			}else{
				model.addAttribute("msg", respDTO.getError().getDescription());
				model.addAttribute("levels", respDTO.getLevels());
				model.addAttribute("pagingInfo", respDTO.getPagination());
			}
			model.addAttribute("status", respDTO.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("error", "There is something wrong..Please Try Again.");
		}
		return "";
	}
	
}
