package com.rtw.tracker.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rtw.tracker.datas.TrackerUser;

@Controller
@RequestMapping({"/","/Login","/Dashboard"})
public class HomeController {

	@RequestMapping(value = {"/Login"}, method=RequestMethod.GET)
	public String loadHomePage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		String error = request.getParameter("error");
		map.put("error", error);
		TrackerUser trackerUser = (TrackerUser)session.getAttribute("trackerUser");
		if(trackerUser != null ){
			session.setAttribute("trackerUser", null);
		}
		return "page-login";
	}
	
	@RequestMapping({"/","/Dashboard"})
	public String loadDashboardPage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){		
		Boolean isAffiliates = (Boolean) session.getAttribute("isAffiliates");
		Boolean isContest = (Boolean) session.getAttribute("isContest");
		Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");
		Boolean isSeller = (Boolean) session.getAttribute("isSeller");
		if(isAffiliates){
			return "redirect:AffiliateReport";
		}else if(isContest && !isAdmin){
			return "redirect:ContestsFirebaseMobile?status=ALL";
		}else if(isSeller && !isAdmin){
			return "redirect:ecomm/ManageProdOrders?status=ACTIVE";
		}else{
			return "redirect:Deliveries/OpenOrders";
		}
	}
}
