package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.pojos.WebServiceTrackingDTO;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.GsonCustomConfig;
import com.rtw.tracker.utils.Util;

@Controller
@RequestMapping({"/Tracking"})
public class TrackingController {
	
	@RequestMapping({"/Home"})
	public String loadTrackingHomePage(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
		
		try{		
			String fromDate = request.getParameter("fromDate");
			String startHour = request.getParameter("startHour");
			String startMinute = request.getParameter("startMinute");
			String toDate = request.getParameter("toDate");
			String endHour = request.getParameter("endHour");
			String endMinute = request.getParameter("endMinute");
			String logType = request.getParameter("logType");
			String actionType = request.getParameter("actionType");
			String customerIpAddress = request.getParameter("customerIpAddress");
			String pageNo = request.getParameter("pageNo");
			String headerFilter = request.getParameter("headerFilter");
			String sortingString = request.getParameter("sortingString");
			
			if(logType==null || logType.isEmpty()){
				logType = "OTHER";
			}
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("fromDate", fromDate);
			map.put("startHour", startHour);
			map.put("startMinute", startMinute);
			map.put("toDate", toDate);
			map.put("endHour", endHour);
			map.put("endMinute", endMinute);
			map.put("logType", logType);
			map.put("actionType", actionType);
			map.put("customerIpAddress", customerIpAddress);
			map.put("pageNo", pageNo);
			map.put("headerFilter", headerFilter);
			map.put("sortingString", sortingString);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_TRACKING_HOME);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			WebServiceTrackingDTO webServiceTrackingDTO = gson.fromJson(((JsonObject)jsonObject.get("webServiceTrackingDTO")), WebServiceTrackingDTO.class);
			
			if(webServiceTrackingDTO.getStatus() == 1){
				model.addAttribute("msg", webServiceTrackingDTO.getMessage());
				model.addAttribute("status", webServiceTrackingDTO.getStatus());
			}else{
				model.addAttribute("msg", webServiceTrackingDTO.getError().getDescription());
				model.addAttribute("status", webServiceTrackingDTO.getStatus());
			}
			
			if(request.getRequestURI().contains(".json")){
				model.addAttribute("webServiceTrackingList", webServiceTrackingDTO.getWebServiceTrackingList());
				model.addAttribute("pagingInfo", webServiceTrackingDTO.getPaginationDTO());
			}else{
				model.addAttribute("webServiceTrackingList", gson.toJson(webServiceTrackingDTO.getWebServiceTrackingList()));
				model.addAttribute("pagingInfo", gson.toJson(webServiceTrackingDTO.getPaginationDTO()));
			}

			model.addAttribute("totalCount", webServiceTrackingDTO.getWebsiteIPCount());
			model.addAttribute("fromDate", webServiceTrackingDTO.getFromDate());
			model.addAttribute("toDate", webServiceTrackingDTO.getToDate());
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong. Please try again");
		}
		return "page-tracking-home";
	}

	
	/*@RequestMapping({"/MigrateCassandraTrackingToSQL"})
	public String loadTrackingHom(HttpServletRequest request, HttpServletResponse response,Model model){
		try{
			Map<String, String> reqMap1 = Util.getParameterMapCass(request);
			String data1 = Util.getObject(reqMap1, Constants.BASE_URL+"MigrateApiTrackingToSql.json");
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			DashboardInfo dashInfo = gson1.fromJson(((JsonObject)jsonObject1.get("dashboardInfo")), DashboardInfo.class);
			if(dashInfo!=null && dashInfo.getSts() == 1){
				model.addAttribute("msg", dashInfo.getMsg());
				model.addAttribute("status",dashInfo.getSts());
			}else{
				model.addAttribute("msg", dashInfo.getErr().getDesc());
				model.addAttribute("status",dashInfo.getSts());
			}
		}catch(Exception e){
			e.printStackTrace();
			model.addAttribute("errorMessage", "There is something wrong. Please try again");
		}
		return "";
	}*/
	
	
	/*
	@RequestMapping({"/TrackByDate"})
	public String loadTrackingByDatePage(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		
		String returnMessage = "";
		String message = "";
		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String action = request.getParameter("action");
			
		try{
			if(action != null && action.equals("action")){
				String fromDate = request.getParameter("fromDate");
				String toDate = request.getParameter("toDate");
				String customerIpAddress = request.getParameter("customerIpAddress");
				String pageNo = request.getParameter("pageNo");												
				Date fromDateCon = (Date)dateFormat.parse(fromDate);
				String fromDateStr = dbDateFormat.format(fromDateCon);
				String finalFromDate=fromDateStr+ " 00:00:00.000000";
				
				Date toDateCon = (Date)dateFormat.parse(toDate);
				String toDateStr = dbDateFormat.format(toDateCon);
				String finalToDate=toDateStr+ " 23:59:00.000000";
				
				Date resultFromDate=null;
				Date resultToDate=null;
			    try {
			       	resultFromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(finalFromDate);
			       	resultToDate   = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(finalToDate);
			       	
			     	
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			    		    				
				if(finalFromDate == null || finalFromDate.isEmpty()){
					returnMessage = "From date cant' be blank";
				}else if(finalToDate == null || finalToDate.isEmpty()){
					returnMessage = "To date cant' be blank";
				}else if (customerIpAddress==null||customerIpAddress.isEmpty()){
					returnMessage = "Customer IP Address cant' be blank";
				}
				GridHeaderFilters filter = new GridHeaderFilters();
				if(returnMessage.isEmpty()){
					Collection<WebServiceTracking> webServiceTrackingList =DAORegistry.getQueryManagerDAO().getAllActionsByDateRangeAndServerIp(null,null,resultFromDate,resultToDate,customerIpAddress,filter,pageNo);
				
					if(webServiceTrackingList.size()>0){
						map.put("webServiceTrackingList", webServiceTrackingList);
						map.put("fromDate", dateFormat.format(fromDateCon));
						map.put("toDate", dateFormat.format(toDateCon));
					}
					else{
						message = "No Record found for the dates";
						map.put("recordNotFound", message);
					}
					
				}else{
					map.put("errorMessage", returnMessage);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			returnMessage = "There is something wrong. Please try again";
			map.put("errorMessage", returnMessage);
		}				
		return "page-tracking-home";
	}
	*/

	/*
	@RequestMapping({"/ExportToExcel"})
	public void customersToExport(HttpServletRequest request, HttpServletResponse response,HttpSession session){
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String fromDateStr = request.getParameter("fromDate");
		String toDateStr = request.getParameter("toDate");
		String headerFilter = request.getParameter("headerFilter");
		String productType = request.getParameter("productType");
		String actionType = request.getParameter("actionType");
		Collection<WebServiceTracking> webServiceTrackingList = null;
		try{
			GridHeaderFilters filter = GridHeaderFiltersUtil.getWebsiteTrackingSearchHeaderFilters(headerFilter);
			if((fromDateStr == null || fromDateStr.isEmpty()) && (toDateStr == null || toDateStr.isEmpty())){
				Calendar cal = Calendar.getInstance();
				Date toDate = new Date();
				cal.add(Calendar.MONTH, -3);
				Date fromDate = cal.getTime();
				fromDateStr = dateFormat.format(fromDate);
				toDateStr = dateFormat.format(toDate);
				String fromDateFinal = fromDateStr + " 00:00:00";
				String toDateFinal = toDateStr + " 23:59:23";				
				webServiceTrackingList = DAORegistry.getQueryManagerDAO().getWebsiteTrackingToExport(productType,actionType,dateTimeFormat.parse(fromDateFinal),dateTimeFormat.parse(toDateFinal),null,filter);
			}else{
				if(fromDateStr != null && !fromDateStr.isEmpty() && toDateStr != null && !toDateStr.isEmpty()){					
					String fromDateFinal = fromDateStr + " 00:00:00";
					String toDateFinal = toDateStr + " 23:59:23";
					webServiceTrackingList = DAORegistry.getQueryManagerDAO().getWebsiteTrackingToExport(productType,actionType,dateTimeFormat.parse(fromDateFinal),dateTimeFormat.parse(toDateFinal),null,filter);
				}else{
					webServiceTrackingList = DAORegistry.getQueryManagerDAO().getWebsiteTrackingToExport(productType,actionType,null,null,null,filter);
				}				
			}
			
			if(webServiceTrackingList!=null && !webServiceTrackingList.isEmpty()){
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Webservice_Tracking");
				Row header = sheet.createRow((int)0);
				header.createCell(0).setCellValue("Tracking Id");
				header.createCell(1).setCellValue("Customer IP Address");
				header.createCell(2).setCellValue("WebConfig Id");
				header.createCell(3).setCellValue("Hitting Date");
				header.createCell(4).setCellValue("Action Type");
				header.createCell(5).setCellValue("Action Result");
				header.createCell(6).setCellValue("Product Type");
				header.createCell(6).setCellValue("Authenticate Hit");
				Integer i=1;
				for(WebServiceTracking tracking : webServiceTrackingList){
					Row row = sheet.createRow(i);
					row.createCell(0).setCellValue(tracking.getId());
					row.createCell(1).setCellValue(tracking.getCustomerIpAddress()!=null?tracking.getCustomerIpAddress():"");
					row.createCell(2).setCellValue(tracking.getWebConfigId()!=null?tracking.getWebConfigId():"");
					row.createCell(3).setCellValue(tracking.getHittingDateStr()!=null?tracking.getHittingDateStr():"");
					row.createCell(4).setCellValue(tracking.getActionType()!=null?tracking.getActionType():"");
					row.createCell(5).setCellValue(tracking.getActionResult()!=null?tracking.getActionResult():"");
					row.createCell(6).setCellValue(tracking.getProductType()!=null?tracking.getProductType():"");
					row.createCell(6).setCellValue(tracking.getAuthenticatedHit()!=null?tracking.getAuthenticatedHit():"");
					i++;
				}
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=Webservice_Tracking.xls");
				workbook.write(response.getOutputStream());
				response.getOutputStream().flush();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
}
