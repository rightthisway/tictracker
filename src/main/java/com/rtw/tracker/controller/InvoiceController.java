package com.rtw.tracker.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.Util;
import com.rtw.tmat.utils.common.SharedProperty;
import com.rtw.tracker.pojos.GenericResponseDTO;
import com.rtw.tracker.pojos.InvoiceCreationDTO;
import com.rtw.tracker.pojos.InvoiceCreationForHoldTicketDTO;
import com.rtw.tracker.pojos.InvoiceCreationForRealTicketDTO;
import com.rtw.tracker.pojos.InvoiceGetLongTicketDTO;
import com.rtw.tracker.pojos.ShippingAddressDTO;
import com.rtw.tracker.pojos.TicketListDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

@Controller
@RequestMapping(value="/Invoice")
public class InvoiceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceController.class);
	private SharedProperty sharedProperty;
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
//	private MailManager mailManager;
//	public MailManager getMailManager() {
//		return mailManager;
//	}
//
//	public void setMailManager(MailManager mailManager) {
//		this.mailManager = mailManager;
//	}

	/**
	 * Manage invoice screen
	 * @param map
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/CreateInvoice")
	public String createInvoice(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Create invoice request processing....");
		
		try{
			String ticketId = request.getParameter("ticketId");
			String invoiceId = request.getParameter("invoiceId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("ticketId", ticketId);
			map.put("invoiceId", invoiceId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_INVOICE_POPUP);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceCreationDTO invoiceCreationDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceCreationDTO")), InvoiceCreationDTO.class);
			
			if(invoiceCreationDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceCreationDTO.getMessage());
				model.addAttribute("status", invoiceCreationDTO.getStatus());
				model.addAttribute("eventDtl", invoiceCreationDTO.getEventDetailsDTO());
				model.addAttribute("catTix", invoiceCreationDTO.getCategoryTicketGroupListDTOs());
				model.addAttribute("shippingMethodId", invoiceCreationDTO.getShippingMethodId());
				model.addAttribute("shippingMethod", invoiceCreationDTO.getShippingMethod());
				model.addAttribute("sPubKey", invoiceCreationDTO.getsPubKey());
				model.addAttribute("ticketId", invoiceCreationDTO.getTicketId());
				model.addAttribute("invoiceId", invoiceCreationDTO.getInvoiceId());
				model.addAttribute("type", invoiceCreationDTO.getType());
			}else{
				model.addAttribute("msg", invoiceCreationDTO.getError().getDescription());
				model.addAttribute("status", invoiceCreationDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();
			object.put("status",0);
			
			if(ticketIdStr==null || ticketIdStr.isEmpty()){
				object.put("msg", "No able to identify ticket information, Please refresh page and try again.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			Integer ticketId = Integer.parseInt(ticketIdStr);
			CategoryTicketGroup catTix = DAORegistry.getCategoryTicketGroupDAO().getCategoryTicketByIdAndBrokerId(ticketId, brokerId);
			if(catTix == null){
				object.put("msg", "Selected Ticket Information is not found for ticketId : "+ticketId);
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			Event eventDetail = DAORegistry.getEventDAO().get(catTix.getEventId());
			if(eventDetail == null){
				object.put("msg","Event details not found for selected ticket.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			if(catTix.getShippingMethodId()!=null && catTix.getShippingMethodId()>=0){
				ShippingMethod shippingMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(catTix.getShippingMethodId());
				object.put("shippingMethod", shippingMethod.getName());
				object.put("shippingMethodId", shippingMethod.getId());
			}
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			String data = Util.getObject(paramMap,Constants.BASE_URL+Constants.GET_STRIPE_CREDENTIALS);
			Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeCredentials stripeCredentials = gson.fromJson(((JsonObject)jsonObject.get("stripeCredentials")), StripeCredentials.class);
			if(stripeCredentials== null){
				object.put("msg","Not able to retrive stripe configuration credential, It happens mostly when rewardthefan API is down.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}else if(stripeCredentials.getStatus()==1){
				object.put("sPubKey", stripeCredentials.getPublishableKey());
			}else{
				object.put("msg",stripeCredentials.getError().getDescription());
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			
			List<CategoryTicketGroup> categoryTickets = new ArrayList<CategoryTicketGroup>();
			categoryTickets.add(catTix);
			
			Venue venue = DAORegistry.getVenueDAO().get(eventDetail.getVenueId());
			
			object.put("eventDtl",JsonWrapperUtil.getEventObject1(eventDetail,venue));
			object.put("catTix",JsonWrapperUtil.getCategoryTicketGroupArray(categoryTickets, shippingMap, eventDetail, null));
			object.put("ticketId", ticketId);	
			object.put("invoiceId", invoiceId);
			object.put("type", "CATEGORY");
			object.put("status",1);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	/**
	 * Create Invoice Real Ticket
	 * @param map
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/CreateInvoiceRealTicket")
	public String createInvoiceRealticket(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Create invoice real ticket request processing....");
		
		try{
			String ticketGroupId = request.getParameter("ticketGroupId");
			String invoiceId = request.getParameter("invoiceId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("ticketGroupId", ticketGroupId);
			map.put("invoiceId", invoiceId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_INVOICE_REAL_TICKET_POPUP);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceCreationForRealTicketDTO invoiceCreationForRealTicketDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceCreationForRealTicketDTO")), InvoiceCreationForRealTicketDTO.class);
			
			if(invoiceCreationForRealTicketDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceCreationForRealTicketDTO.getMessage());
				model.addAttribute("status", invoiceCreationForRealTicketDTO.getStatus());
				model.addAttribute("eventDtl", invoiceCreationForRealTicketDTO.getEventDetailsDTO());
				model.addAttribute("catTix", invoiceCreationForRealTicketDTO.getTicketGroupDTOs());
				model.addAttribute("sPubKey", invoiceCreationForRealTicketDTO.getsPubKey());
				model.addAttribute("ticketId", invoiceCreationForRealTicketDTO.getTicketId());
				model.addAttribute("invoiceId", invoiceCreationForRealTicketDTO.getInvoiceId());
				model.addAttribute("type", invoiceCreationForRealTicketDTO.getType());
			}else{
				model.addAttribute("msg", invoiceCreationForRealTicketDTO.getError().getDescription());
				model.addAttribute("status", invoiceCreationForRealTicketDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();
			object.put("status",0);
			
			if(ticketGroupIdStr==null || ticketGroupIdStr.isEmpty()){
				object.put("msg", "Not able to identify ticket information, Please refresh page and try again.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			List<Integer> ticketIds = new ArrayList<Integer>();
			if(ticketGroupIdStr.contains(",")){
				String arr[] = ticketGroupIdStr.split(",");
				for(String str : arr){
					try {
						if(str!=null && !str.trim().isEmpty()){
							ticketIds.add(Integer.parseInt(str));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else{
				ticketIds.add(Integer.parseInt(ticketGroupIdStr));
			}
			
			if(ticketIds.isEmpty()){
				object.put("msg", "Not able to identify ticket information, Please refresh page and try again.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
			Event eventDetail = null;
			for(Integer ticketGroupId : ticketIds){
				List<Ticket> ticketList = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(ticketGroupId);
				TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().getTicketGroupByIdAndBrokerId(ticketGroupId, brokerId);
				if(ticGroup == null){
					object.put("msg", "Selected Ticket Information is not found for ticketId : "+ticketGroupIdStr);
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}			
				if(!ticketList.isEmpty()){
					ticGroup.setSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
					ticGroup.setSeatLow(ticketList.get(0).getSeatNo());
				}
				ticGroup.setQuantity(ticketList.size());
				if(eventDetail == null){
					eventDetail = DAORegistry.getEventDAO().get(ticGroup.getEventId());
				}
				
				if(eventDetail==null){
					object.put("msg","Event details not found for selected ticket.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				ticketGroups.add(ticGroup);
			}
			
			Collection<ShippingMethod> methodList = DAORegistry.getShippingMethodDAO().getAll();
			Map<Integer, String> shippingMap = new HashMap<Integer, String>();
			for(ShippingMethod method : methodList){
				shippingMap.put(method.getId(),method.getName());
			}
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.GET_STRIPE_CREDENTIALS);
			Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeCredentials stripeCredentials = gson.fromJson(((JsonObject)jsonObject.get("stripeCredentials")), StripeCredentials.class);
			if(stripeCredentials== null){
				object.put("msg","Not able to retrive stripe configuration credential, it happens mostly when rewardthefan API is down.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}else if(stripeCredentials.getStatus()==1){
				object.put("sPubKey", stripeCredentials.getPublishableKey());
			}else{
				object.put("msg",stripeCredentials.getError().getDescription());
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			Venue venue = DAORegistry.getVenueDAO().get(eventDetail.getVenueId());
			
			object.put("eventDtl",JsonWrapperUtil.getEventObject1(eventDetail,venue));
			object.put("catTix",JsonWrapperUtil.getTicketGroupArray(ticketGroups, shippingMap,eventDetail));
			object.put("ticketId", ticketGroupIdStr);
			object.put("invoiceId", invoiceId);
			object.put("type", "REAL");
			object.put("status",1);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	
	/**
	 * Create Invoice Hold Ticket
	 * @param map
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/CreateInvoiceHoldTicket")
	public String createInvoiceHoldTicket(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		LOGGER.info("Create invoice hold ticket request processing....");
		
		try{
			String ticketGroupId = request.getParameter("ticketGroupId");
			String invoiceId = request.getParameter("invoiceId");
			String ticketIds = request.getParameter("ticketIds");
			String salePrice = request.getParameter("salePrice");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("ticketGroupId", ticketGroupId);
			map.put("invoiceId", invoiceId);
			map.put("ticketIds", ticketIds);
			map.put("salePrice", salePrice);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CREATE_INVOICE_HOLD_TICKET_POPUP);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceCreationForHoldTicketDTO invoiceCreationForHoldTicketDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceCreationForHoldTicketDTO")), InvoiceCreationForHoldTicketDTO.class);
			
			if(invoiceCreationForHoldTicketDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceCreationForHoldTicketDTO.getMessage());
				model.addAttribute("status", invoiceCreationForHoldTicketDTO.getStatus());
				model.addAttribute("eventDtl", invoiceCreationForHoldTicketDTO.getEventDetailsDTO());
				model.addAttribute("catTix", invoiceCreationForHoldTicketDTO.getTicketGroupDTOs());
				model.addAttribute("sPubKey", invoiceCreationForHoldTicketDTO.getsPubKey());
				model.addAttribute("ticketId", invoiceCreationForHoldTicketDTO.getTicketId());
				model.addAttribute("ticketIds", invoiceCreationForHoldTicketDTO.getTicketIds());
				model.addAttribute("invoiceId", invoiceCreationForHoldTicketDTO.getInvoiceId());
				model.addAttribute("salePrice", invoiceCreationForHoldTicketDTO.getSalePrice());
				model.addAttribute("type", invoiceCreationForHoldTicketDTO.getType());
			}else{
				model.addAttribute("msg", invoiceCreationForHoldTicketDTO.getError().getDescription());
				model.addAttribute("status", invoiceCreationForHoldTicketDTO.getStatus());
			}
			/*JSONObject object = new JSONObject();
			
			String arr[] = null;			
			if(ids !=null && !ids.isEmpty()){
				if(ids.contains(",")){
					arr = ids.split(",");
				}else{
					arr = new String[1];
					arr[0] = ids;
				}
			}
			List<Integer> list = new ArrayList<Integer>();
			for(String str : arr){
				if(!str.trim().isEmpty()){
					list.add(Integer.parseInt(str));
				}
			}
			
			if(ticketGroupIdStr==null || ticketGroupIdStr.isEmpty()){
				ticketGroupIdStr = request.getParameter("tickId");
				if(ticketGroupIdStr!=null && !ticketGroupIdStr.isEmpty()){
					object.put("msg", "Invoice created successfully");
				}else{
					object.put("msg", "There is something wrong...Please try again");
				}
			}
			//Getting Broker ID
			Integer brokerId = Util.getBrokerId(session);
			Integer ticketGroupId = Integer.parseInt(ticketGroupIdStr);
			//List<Ticket> ticketList = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(ticketGroupId);
			
			List<Ticket> ticketList = DAORegistry.getTicketDAO().getTicketsByTicketIdsAndStatus(list);
			TicketGroup ticGroup = DAORegistry.getTicketGroupDAO().getTicketGroupByIdAndBrokerId(ticketGroupId, brokerId);			
			if(ticGroup == null || ticketList == null){
				object.put("msg", "There is no ticket id found.. Please try again");
			}			
			if(!ticketList.isEmpty()){
				ticGroup.setSeatHigh(ticketList.get(ticketList.size()-1).getSeatNo());
				ticGroup.setSeatLow(ticketList.get(0).getSeatNo());
				ticGroup.setQuantity(ticketList.size());
			}
			if(ticGroup != null && ticGroup.getEventId() != null){				
				EventDetails eventDetail = DAORegistry.getEventDetailsDAO().get(ticGroup.getEventId());
				object.put("eventDtl", JsonWrapperUtil.getEventObject(eventDetail));
			}
			if(ticGroup != null && ticGroup.getShippingMethodId()!= null && ticGroup.getShippingMethodId() >= 0){
				ShippingMethod shippingMethod = DAORegistry.getShippingMethodDAO().getShippingMethodById(ticGroup.getShippingMethodId());
				object.put("shippingMethod", shippingMethod.getName());
				object.put("shippingMethodId", shippingMethod.getId());
			}
			
			Map<String, String> paramMap = Util.getParameterMap(request);
			String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.GET_STRIPE_CREDENTIALS);
			Gson gson = new Gson();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			StripeCredentials stripeCredentials = gson.fromJson(((JsonObject)jsonObject.get("stripeCredentials")), StripeCredentials.class);
			
			object.put("sPubKey", stripeCredentials.getPublishableKey());
			
			object.put("catTix",JsonWrapperUtil.getTicketGroupObject(ticGroup));
			object.put("ticketId", ticketGroupId);
			object.put("ticketIds", ids);
			object.put("invoiceId", invoiceId);
			object.put("salePrice", salePrice);
			object.put("type", "HOLD");
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
		//return "page-create-invoice-real-ticket";
	}
	
	
	/**
	 * Manage invoice screen
	 * @param map
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/ShowInvoice")
	public String ShowInvoice(ModelMap map, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		LOGGER.info("Show invoice request processing....");
		try{
			String orderIdStr = request.getParameter("orderId");
			String invoiceIdStr = request.getParameter("invoiceId");
			if(orderIdStr==null || orderIdStr.isEmpty()){
				map.put("errorMessage", "There is something wrong...Please try again");
				return "page-view-invoice-summery";
			}
			
			if(invoiceIdStr==null || invoiceIdStr.isEmpty()){
				map.put("errorMessage", "There is something wrong...Please try again");
				return "page-view-invoice-summery";
			}
			
			
			Integer orderId = Integer.parseInt(orderIdStr);
			Integer invoiceId = Integer.parseInt(invoiceIdStr);
			
			Invoice invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			CustomerOrderDetails orderDetails = DAORegistry.getCustomerOrderDetailsDAO().getCustomerOrderDetailsByOrderId(orderId);
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(orderId);
			CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyByOrderId(orderId);
			
			map.put("invoice", invoice);
			map.put("orderDetails", orderDetails);
			map.put("order", customerOrder);
			map.put("invoiceId", invoiceId);
			map.put("customerLoyalty",customerLoyaltyHistory);
			map.put("successMessage", "Invoice created successfully. Your invoice number is ");
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong...Please try again");
		}
		return "page-view-invoice-summery";
	}*/
	
	/**
	 * Save Invoice details
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @param filePath 
	 * @return
	 */
	@RequestMapping(value = "/SaveInvoice")
	public String saveInvoice(HttpServletRequest request, HttpServletResponse response, HttpSession session, String filePath, Model model){
		LOGGER.info("Save invoice request processing......");
		
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*String returnMessage = "";
			String customerId = "";
			String action = "";
			String ticketIdStr = null;
			Integer invoiceId=0,orderId=0;
			JSONObject object = new JSONObject();
			action = request.getParameter("action");
			customerId = request.getParameter("customerId");
			ticketIdStr = request.getParameter("ticketId_0");
			Double ticketSoldPrice =null;
			Double taxAmount = 0.0;
			if(returnMessage.isEmpty()){
				if(action != null && action.equals("saveInvoice")){
					object.put("status",0);
					
					String soldPrice = request.getParameter("soldPrice_0");
					String isLoyalFan = request.getParameter("isLoyalFanPrice");
					String taxAmountstr = request.getParameter("taxAmount_0");
					String ticketCountStr = request.getParameter("ticketCount_0");
					String eventIdStr = request.getParameter("eventId");
					
					String addressLine1 = request.getParameter("addressLine1");
					String addressLine2 = request.getParameter("addressLine2");
					String shAddressLine1 = request.getParameter("shAddressLine1");
					String shAddressLine2 = request.getParameter("shAddressLine2");
					String bAddressId =request.getParameter("bAddressId"); 
					String sAddressId =request.getParameter("sAddressId");
					String referalCode =request.getParameter("referralCode");
					
					String msg = "";
					ticketSoldPrice= Double.parseDouble(soldPrice);
					if(taxAmountstr!=null && !taxAmountstr.isEmpty()){
						taxAmount = Double.parseDouble(taxAmountstr);
					}
					
					if(ticketIdStr == null || ticketIdStr.trim().isEmpty()) {
						object.put("msg", "Not able to identify ticket information, Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(eventIdStr == null || eventIdStr.trim().isEmpty()) {
						object.put("msg", "Not able to identify Event information, Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(ticketCountStr == null || ticketCountStr.trim().isEmpty()) {
						object.put("msg", "Not able to identify ticket quantity, Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					Integer ticketCount = null;
					Integer eventId = null;
					try {
						ticketCount = Integer.parseInt(ticketCountStr);
					} catch (Exception e) {
						e.printStackTrace();
						object.put("msg", "Bad value found for Ticket quantity, It should be Valid Integer.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					try {
						eventId = Integer.parseInt(eventIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						object.put("msg", "No Able to identify event information, Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					try {
						ticketSoldPrice= Double.parseDouble(soldPrice);
					} catch (Exception e) {
						e.printStackTrace();
						object.put("msg", "Bad value found for Sold price, It should be valid Double value.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(bAddressId == null || bAddressId.trim().isEmpty()) {
						object.put("msg", "Builling Address Not found, Please select Billing Address.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(sAddressId == null || sAddressId.trim().isEmpty()) {
						object.put("msg", "Shipping Address Not found, Please select Shipping Address.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if((addressLine1==null || addressLine1.isEmpty()) && (addressLine2==null || addressLine2.isEmpty())){
						object.put("msg", "Selected billing address does not have either AddressLing1 or AddressLine2.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if((shAddressLine1==null || shAddressLine1.isEmpty()) && (shAddressLine2==null || shAddressLine2.isEmpty())){
						object.put("msg", "Selected Shipping address does not have either AddressLing1 or AddressLine2.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					Integer ticketId = Integer.parseInt(ticketIdStr);
					
					CategoryTicketGroup categoryTicketGroup  = DAORegistry.getCategoryTicketGroupDAO().get(ticketId);
					if(categoryTicketGroup==null){
						object.put("msg", "Selected ticket information is not found, Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					categoryTicketGroup.setSoldPrice(ticketSoldPrice);
					categoryTicketGroup.setBroadcast(true);
					categoryTicketGroup.setTaxAmount(taxAmount);
					DAORegistry.getCategoryTicketGroupDAO().update(categoryTicketGroup);
					String shippingIsSameAsBilling =request.getParameter("shippingIsSameAsBilling"); 
					
					
					Double orderTotal = (ticketSoldPrice * ticketCount) + Util.getBrokerServiceFees(brokerId, ticketSoldPrice, ticketCount, taxAmount);
					orderTotal = Util.getRoundedValue(orderTotal);
					
					CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
					if(customerLoyalty==null){
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setActivePoints(0.00);
						customerLoyalty.setActiveRewardDollers(0.00);
						customerLoyalty.setCustomerId(Integer.parseInt(customerId));
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLatestEarnedPoints(0.00);
						customerLoyalty.setLatestSpentPoints(0.00);
						customerLoyalty.setPendingPoints(0.00);
						customerLoyalty.setRevertedSpentPoints(0.00);
						customerLoyalty.setTotalEarnedPoints(0.00);
						customerLoyalty.setTotalSpentPoints(0.00);
						customerLoyalty.setVoidedRewardPoints(0.00);
						DAORegistry.getCustomerLoyaltyDAO().save(customerLoyalty);
					}
					if(referalCode!=null && !referalCode.isEmpty()){
						Map<String, String> paramMap = Util.getParameterMap(request);
						paramMap.put("referralCode", referalCode);
						paramMap.put("customerId",customerId);
						paramMap.put("eventId",eventIdStr);
						paramMap.put("tgId", ticketIdStr);
						paramMap.put("orderTotal",String.valueOf(orderTotal));
						paramMap.put("clientIPAddress", request.getRemoteAddr());
						paramMap.put("isLongSale", "false");
						if(isLoyalFan!=null && !isLoyalFan.isEmpty()){
							if(isLoyalFan.equalsIgnoreCase("Yes")){
								paramMap.put("isFanPrice","true");
							}else{
								paramMap.put("isFanPrice","false");
							}
						}
						paramMap.put("orderTotal",String.valueOf(orderTotal));
						paramMap.put("sessionId", session.getId());
						paramMap.put("orderQty",ticketCountStr);
						 
					    String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.VALIDATE_PROMOCODE);
					    Gson gson = new Gson();  
						JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
						ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
						if(referralCodeValidation == null){
							object.put("msg","Failed while validating referal code, Failed to communicate with our API server");
							IOUtils.write(object.toString().getBytes(), response.getOutputStream());
							return;
						}else if(referralCodeValidation.getStatus()==1){
							msg = "REFERRALCODEVALIDATED";
						}else if(referralCodeValidation.getStatus()==0){
							System.out.println(referralCodeValidation.getError().getDescription());
							msg = referralCodeValidation.getError().getDescription();
							object.put("msg", msg);
							IOUtils.write(object.toString().getBytes(), response.getOutputStream());
							return;
						}
					}
					String paymentMethod = request.getParameter("paymentMethod");
					if(TextUtil.isEmptyOrNull(paymentMethod)){
						object.put("msg", "Not able to identify payment method.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
					Map<String, String> orderParamMap = Util.getParameterMap(request);
					orderParamMap.put("serviceFees", String.valueOf(categoryTicketGroup.getTaxAmount()));
					String validationMsg = "";
					String paymentMethodCount = null;
					WalletTransaction walletTranx = null;
					StripeTransaction transaction = null;
					String primaryTrxId = "";
					String secondaryTrxId="";
					String thirdTrxId="";
					String primaryAmount=null;
					String secondaryAmount =null;
					String thirdAmount=null;
					String data = null;
					String rewardPoints = null;
					Gson gson = new Gson();
					JsonObject jsonObject = null;
					switch(method){
					case CREDITCARD :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CREDITCARD));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case WALLET :
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case REWARDPOINTS :
						primaryAmount = String.valueOf(orderTotal);
						rewardPoints = request.getParameter("rewardPoints");
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.FULL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					case PARTIAL_REWARD_CC :
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						secondaryAmount = request.getParameter("cardAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET :
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward point amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case WALLET_CREDITCARD :
						secondaryAmount = request.getParameter("cardAmount");
						primaryAmount= request.getParameter("walletAmount");
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							paymentMethodCount="TWO";
						}
						break;
					case PARTIAL_REWARD_WALLET_CC :
						thirdAmount = request.getParameter("cardAmount");
						secondaryAmount = request.getParameter("walletAmount");
						rewardPoints =  request.getParameter("rewardPoints");
						primaryAmount = rewardPoints;
						if(primaryAmount==null || primaryAmount.isEmpty()){
							validationMsg ="Reward points amount is not found.";
						}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
							validationMsg ="Wallet amount is not found.";
						}else if(thirdAmount==null || thirdAmount.isEmpty()){
							validationMsg ="Credit card amount is not found.";
						}else{
							orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
							orderParamMap.put("primaryPaymentAmount",primaryAmount);
							orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
							orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
							orderParamMap.put("thirdPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
							orderParamMap.put("thirdPaymentAmount",thirdAmount);
							paymentMethodCount="THREE";
						}
						break;
					case ACCOUNT_RECIVABLE:
						primaryAmount = String.valueOf(orderTotal);
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.ACCOUNT_RECIVABLE));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						paymentMethodCount="ONE";
						break;
					}
					
					
					if(!validationMsg.isEmpty()){
						object.put("msg",validationMsg);
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(isLoyalFan!=null && !isLoyalFan.isEmpty()){
						if(isLoyalFan.equalsIgnoreCase("Yes")){
							orderParamMap.put("isFanPrice","true");
						}else{
							orderParamMap.put("isFanPrice","false");
						}
					}
					
					orderParamMap.put("eventId", String.valueOf(eventId));
					orderParamMap.put("categoryTicketGroupId", String.valueOf(ticketId));
					orderParamMap.put("orderTotal",String.valueOf(orderTotal));
					orderParamMap.put("paymentMethodCount",paymentMethodCount);
					orderParamMap.put("customerId", String.valueOf(customerId));
					if(rewardPoints!=null && !rewardPoints.isEmpty()){
						Double points = Double.parseDouble(rewardPoints);
						orderParamMap.put("rewardPoints", String.valueOf(points));
					}
					orderParamMap.put("sessionId", session.getId());
					orderParamMap.put("billingAddressId", String.valueOf(bAddressId));
					orderParamMap.put("shippingAddressId", String.valueOf(sAddressId));
					orderParamMap.put("shippingSameAsBilling", shippingIsSameAsBilling);
					orderParamMap.put("referralCode", referalCode);
					orderParamMap.put("isLongSale","false");
					
					data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_INITIATE);
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					if(orderConfirmation == null){
						object.put("msg", "Invoice creation failed not able to communicate with API Server.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(orderConfirmation.getStatus() == 0){
						object.put("msg", "Could not create invoice, "+orderConfirmation.getError().getDescription());
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					orderId = orderConfirmation.getOrderId();
					boolean isPaymentSuccess = false;
					
					switch(method){
						case CREDITCARD :
							transaction = doCreditCardTransaction(request,String.valueOf(orderTotal),null,false,null);
							if(transaction.getStatus()==0){
								validationMsg = "Credit Card Transaction - "+transaction.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								primaryTrxId = transaction.getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
							}
							break;
						case WALLET :
							walletTranx= doWalletTransaction(request, String.valueOf(orderTotal),orderId);
							if(walletTranx.getStatus()==0){
								validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case REWARDPOINTS :
							isPaymentSuccess = true;
							break;
						case PARTIAL_REWARD_CC :
							transaction = doCreditCardTransaction(request,secondaryAmount,null,false,null);
							if(transaction.getStatus()==0){
								validationMsg ="Credit Card Transaction - "+transaction.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = transaction.getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								paymentMethodCount="TWO";
							}
							break;
						case PARTIAL_REWARD_WALLET :
							walletTranx= doWalletTransaction(request,secondaryAmount,orderId);
							if(walletTranx.getStatus()==0){
								validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case WALLET_CREDITCARD :
							walletTranx= doWalletTransaction(request,primaryAmount,orderId);
							transaction = doCreditCardTransaction(request,secondaryAmount,null,false,null);
							if(walletTranx.getStatus()==0){
								validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else if(transaction.getStatus()==0){
								isPaymentSuccess = false;
								validationMsg ="Credit Card Transaction - "+transaction.getError().getDescription();
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = transaction.getTransactionId();
								primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case PARTIAL_REWARD_WALLET_CC :
							walletTranx = doWalletTransaction(request,secondaryAmount,orderId);
							transaction = doCreditCardTransaction(request,thirdAmount,null,false,null);
							if(walletTranx.getStatus()==0){
								validationMsg = "Wallet Transaction - "+walletTranx.getError().getDescription();
								isPaymentSuccess = false;
							}else if(transaction.getStatus()==0){
								validationMsg ="Credit Card Transaction - "+transaction.getError().getDescription();
								isPaymentSuccess = false;
							}else{
								isPaymentSuccess = true;
								secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
								thirdTrxId = transaction.getTransactionId();
								orderParamMap.put("primaryTransactionId",primaryTrxId);
								orderParamMap.put("secondaryTransactionId",secondaryTrxId);
								orderParamMap.put("thirdTransactionId",thirdTrxId);
								orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
							}
							break;
						case ACCOUNT_RECIVABLE:
							isPaymentSuccess = true;
							orderParamMap.put("primaryTransactionId","dummytransactionid"+orderConfirmation.getOrderId());
							break;
					}
				
					orderParamMap.put("orderId", String.valueOf(orderId));
					orderParamMap.put("isPaymentSuccess",String.valueOf(isPaymentSuccess));
					data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_CONFIRM);
					gson = new Gson();		
					jsonObject = gson.fromJson(data, JsonObject.class);
					OrderConfirmation orderConfirmation1 = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
					if(!validationMsg.isEmpty()){
						object.put("msg",validationMsg);
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(orderConfirmation1 == null){
						object.put("msg", "Order confirmation failed not able to communicate with API Server.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(orderConfirmation1.getStatus() == 0){
						object.put("msg", "Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
					if(!customer.getProductType().equals(ProductType.REWARDTHEFAN) && customer.getEmail()!=null && !customer.getEmail().isEmpty()
							&& (customer.getIsRegistrationMailSent()==null || customer.getIsRegistrationMailSent() == false)){
						customer.setApplicationPlatForm(ApplicationPlatform.DESKTOP_SITE);
						customer.setProductType(ProductType.REWARDTHEFAN);
						customer.setSignupType(SignupType.REWARDTHEFAN);
						customer.setIsRegistrationMailSent(true);
						customer.setSignupDate(new Date());
						customer.setUserId(Util.generatedCustomerUserId(customer.getCustomerName(), customer.getLastName()));
						//customer.setReferrerCode(Util.generateCustomerReferalCode());
						customer.setReferrerCode(customer.getEmail());
						String password = Util.generatePassword(customer.getCustomerName(), "RTF");
						customer.setPassword(Util.generateEncrptedPassword(password));
						DAORegistry.getCustomerDAO().update(customer);
						Map<String, Object> mailMap = new HashMap<String, Object>();
						mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
						mailMap.put("password", password);
						mailMap.put("userName", customer.getUserName());
						mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
						mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
						com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
						mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",Util.getFilePath(request, "share.png", "/resources/images/"));
						mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app1.png",Util.getFilePath(request, "app1.png", "/resources/images/"));
						mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app2.png",Util.getFilePath(request, "app2.png", "/resources/images/"));
						mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",Util.getFilePath(request, "blue.png", "/resources/images/"));
						mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",Util.getFilePath(request, "fb1.png", "/resources/images/"));
						mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",Util.getFilePath(request, "ig1.png", "/resources/images/"));
						mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",Util.getFilePath(request, "li1.png", "/resources/images/"));
						mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo_white.png",Util.getFilePath(request, "logo_white.png", "/resources/images/"));
						mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",Util.getFilePath(request, "p1.png", "/resources/images/"));
						mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",Util.getFilePath(request, "tw1.png", "/resources/images/"));
						mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
						
						String email = customer.getEmail();
						//email = "msanghani@rightthisway.com";
						String bcc ="";
						bcc="amit.raut@rightthisway.com,msanghani@righthtisway.com,kulaganathan@rightthisway.com";
						try {
							mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
									null,bcc, "You are successfully registered with RewardTheFan.com.",
						    		"mail-rewardfan-customer-registration.html", mailMap, "text/html", null,mailAttachment,null);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
					}
					Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderConfirmation1.getOrderId());
					invoiceId = invoice.getId();
					orderId = orderConfirmation.getOrderId();
					CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(orderId);
					if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
						invoice.setInvoiceType(PaymentMethod.ACCOUNT_RECIVABLE.toString());
						DAORegistry.getInvoiceDAO().update(invoice);
					}
					object.put("msg", "Order created successfully, InvoiceId : "+invoiceId);
					object.put("status",1);
					
					
					//Tracking User Action
					String userActionMsg = "Invoice Created Successfully.";
					Util.userActionAudit(request, invoiceId, userActionMsg);
				}
			}
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong while Create Invoice. Please try again.");
		}
		return "";
	}
	
	
	
	/*public StripeTransaction doCreditCardTransaction(HttpServletRequest request,String transactionAmount,String ticketIdStr,Boolean isLongOrder,Integer quantity){
		StripeTransaction transaction = new StripeTransaction();
		com.rtw.tmat.utils.Error error = new Error();
		String sTempToken = request.getParameter("sTempToken");
		String sBySavedCard = request.getParameter("sBySavedCard");
		String sSavedCardId = request.getParameter("sSavedCardId");
		String customerId = request.getParameter("customerId");
		String eventId = request.getParameter("eventId");
		if(ticketIdStr==null || ticketIdStr.isEmpty()){
			ticketIdStr = request.getParameter("ticketId_0");
		}
		Map<String, String> paramterMap = null;
		Gson gson = new Gson();
		JsonObject jsonObject = null;		 
		try {
			if(TextUtil.isEmptyOrNull(sBySavedCard)){
				error.setDescription("Not able to indentify weather to use Saved card or new one.");
				transaction.setError(error);
				transaction.setStatus(0);
				return transaction;
			}
			paramterMap = Util.getParameterMap(request);
			paramterMap.put("transactionAmount",transactionAmount);
			if(sBySavedCard.equals("YES")){
				if(TextUtil.isEmptyOrNull(sSavedCardId)){
					error.setDescription("Not able to indentify saved credit card number for payment.");
					transaction.setError(error);
					transaction.setStatus(0);
					return transaction;
				}
				paramterMap.put("trxBySavedCard", "true");
				paramterMap.put("custCardId", sSavedCardId);
			}else{
				if(TextUtil.isEmptyOrNull(sTempToken)){
					error.setDescription("Not able to generate Stripe Transaction Token.");
					transaction.setError(error);
					transaction.setStatus(0);
					return transaction;
				}
				paramterMap.put("trxBySavedCard", "false");
				paramterMap.put("token", sTempToken);
				paramterMap.put("saveCustCard", "true");
			}
			paramterMap.put("paymentMethod", "CREDITCARD");
			paramterMap.put("rtfCustId", customerId);
			paramterMap.put("description", "Reward The Fan");
			
			String data = "";
			try {
				if(isLongOrder){
					paramterMap.put("tGroupRefIds",ticketIdStr);
					paramterMap.put("quantity",String.valueOf(quantity));
					paramterMap.put("eventId",eventId);
					data = Util.getObject(paramterMap, sharedProperty.getApiUrl()+Constants.STRIPE_TRANSACTION_REAL_TICKET);
				}else{
					paramterMap.put("tGroupId",ticketIdStr);
					data = Util.getObject(paramterMap, sharedProperty.getApiUrl()+Constants.STRIPE_TRANSACTION);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				transaction = new StripeTransaction();
				error.setDescription("Failed while creating stripe transaction, Failed to communicate with our API server.");
				transaction.setError(error);
				transaction.setStatus(0);
				return transaction;
			}
			
			gson = new Gson();		
			jsonObject = gson.fromJson(data, JsonObject.class);
			transaction = gson.fromJson(((JsonObject)jsonObject.get("stripeTransaction")), StripeTransaction.class);
			if(transaction==null){
				transaction = new StripeTransaction();
				error.setDescription("Failed while creating stripe transaction, Failed to communicate with our API server.");
				transaction.setError(error);
				transaction.setStatus(0);
				return transaction;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transaction;
	}*/
	
	/*public WalletTransaction doWalletTransaction(HttpServletRequest request,String amount,Integer orderId){
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		String customerId = request.getParameter("customerId");
		WalletTransaction walletTransaction = new WalletTransaction();
		Error error = new Error();
		try {
			Map<String, String> paramMap = Util.getParameterMap(request);
			paramMap.put("transactionAmount",amount);
			paramMap.put("customerId",customerId);
			paramMap.put("orderId",String.valueOf(orderId));
			paramMap.put("transactionType","DEBIT");
			paramMap.put("userName",userName);
			String data = Util.getObject(paramMap, sharedProperty.getApiUrl()+Constants.CUSTOMER_WALLET_TRANSACTION);
			Gson gson = new Gson();
			JsonObject cardJsonObject = gson.fromJson(data, JsonObject.class);
			walletTransaction = gson.fromJson(((JsonObject)cardJsonObject.get("walletTransaction")), WalletTransaction.class);
			if(walletTransaction==null){
				walletTransaction = new WalletTransaction();
				error.setDescription("Failed to create wallet transaction, Failed to communicate with API server.");
				walletTransaction.setStatus(0);
				walletTransaction.setError(error);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return walletTransaction;
	}*/
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getShippingAddress")
	public String getShippingAddress(HttpServletRequest request, HttpServletResponse response, Model model){
		
		try{
			String custId = request.getParameter("custId");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("custId", custId);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_SHIPPING_ADDRESS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			ShippingAddressDTO shippingAddressDTO = gson.fromJson(((JsonObject)jsonObject.get("shippingAddressDTO")), ShippingAddressDTO.class);
			
			if(shippingAddressDTO.getStatus() == 1){
				model.addAttribute("msg", shippingAddressDTO.getMessage());
				model.addAttribute("status", shippingAddressDTO.getStatus());
				model.addAttribute("shippingPagingInfo", shippingAddressDTO.getPaginationDTO());
				model.addAttribute("shippingAddressList", shippingAddressDTO.getShippingAddressDTOs());
				model.addAttribute("count", shippingAddressDTO.getShippingAddressCount());
				model.addAttribute("customerId", shippingAddressDTO.getCustomerId());
			}else{
				model.addAttribute("msg", shippingAddressDTO.getError().getDescription());
				model.addAttribute("status", shippingAddressDTO.getStatus());
			}
			
			/*int shippingAddressCount = 0;
			JSONObject object = new JSONObject();
			object.put("status",0);
			
			if(customerId==null || customerId.isEmpty()){
				object.put("msg","Not able to Identify selected Customer.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			Integer custId = Integer.parseInt(customerId);
			List<CustomerAddress> shippingAddressList = DAORegistry.getCustomerAddressDAO().getShippingAddressInfo(custId);
			shippingAddressCount = DAORegistry.getCustomerAddressDAO().getShippingAddressCount(custId);
			if(shippingAddressList.size() ==0){
				object.put("msg","No Shipping address found for selected Customer.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			object.put("shippingAddressList", JsonWrapperUtil.getShippingAddressArray(shippingAddressList));
			object.put("shippingPagingInfo", PaginationUtil.getDummyPaginationParameter(shippingAddressCount));
			object.put("count", shippingAddressCount);
			object.put("customerId", customerId);
			object.put("status",1);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong...Please try again");
		}
		return "";
	}
	
	/**
	 * Save shipping address
	 * @param addShippingAddress
	 * @param request
	 * @param response
	 * @param map
	 * @param session
	 * @return
	 */
	/*@RequestMapping(value = "/AddShippingAddress", method=RequestMethod.POST)
	public String saveShippingAddress(HttpServletRequest request, HttpServletResponse response, ModelMap map, HttpSession session){
		LOGGER.info("Save shipping address..");
		String returnMessage = "";
		String customerId = "";
		String shippingAddressId = "";
		try{
			customerId = request.getParameter("custId");
			shippingAddressId = request.getParameter("shippingAddrId");
			String firstName = request.getParameter("shFirstName");
			String lastName = request.getParameter("shLastName");
			String street1 = request.getParameter("shAddressLine1");
			String street2 = request.getParameter("shAddressLine2");
			String country = request.getParameter("shCountryName");
			String state = request.getParameter("shStateName");
			String city = request.getParameter("shCity");
			String zipCode = request.getParameter("shZipCode");
			String action = request.getParameter("action");
			if(action != null && action.equals("addShippingAddress")){
				if(addShippingAddress.getFirstName() == null || addShippingAddress.getFirstName().isEmpty()){
					returnMessage = "Firstname can't be blank";
				}
				CustomerAddress shippingAddress = new CustomerAddress();
				if(returnMessage.isEmpty()){

					Country countryObj1 = DAORegistry.getCountryDAO().get(Integer.parseInt(country.trim()));
					State stateObj1 =  DAORegistry.getStateDAO().get(Integer.parseInt(state.trim()));
					
					shippingAddress.setFirstName(firstName);
					shippingAddress.setLastName(lastName);
					shippingAddress.setAddressLine1(street1);
					shippingAddress.setAddressLine2(street2);
					shippingAddress.setCountry(countryObj1);
					shippingAddress.setState(stateObj1);
					shippingAddress.setCity(city);
					shippingAddress.setZipCode(zipCode);
					shippingAddress.setCustomerId(Integer.parseInt(customerId));
					shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
					
					CustomerAddress updateAddress = null;
					if(shippingAddressId != null && !shippingAddressId.isEmpty()){
						updateAddress = DAORegistry.getCustomerAddressDAO().get(Integer.parseInt(shippingAddressId));
					}
					
					if(updateAddress != null){
						shippingAddress.setId(updateAddress.getId());
						shippingAddress.setFirstName(firstName);
						shippingAddress.setLastName(lastName);
						shippingAddress.setAddressLine1(street1);
						shippingAddress.setAddressLine2(street2);
						shippingAddress.setCountry(countryObj1);
						shippingAddress.setState(stateObj1);
						shippingAddress.setCity(city);
						shippingAddress.setZipCode(zipCode);
						shippingAddress.setCustomerId(Integer.parseInt(customerId));
						shippingAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
						DAORegistry.getCustomerAddressDAO().update(shippingAddress);
					}else{						
						DAORegistry.getCustomerAddressDAO().save(shippingAddress);
					}
					returnMessage = "Shipping address added successfully";
					map.put("successMessage", returnMessage);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			map.put("errorMessage", "There is something wrong..Please Try Again.");
		}
		return "redirect:getShippingAddress?custId="+customerId;
	}*/
	

	@RequestMapping(value = "/GetLongInventoryForInvoice")
	public String getLongInventoryForInvoice(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String eventId = request.getParameter("eventId");
			String rowId = request.getParameter("rowId");
			String ticketId = request.getParameter("ticketId");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("eventId", eventId);
			map.put("rowId", rowId);
			map.put("ticketId", ticketId);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_LONG_INVENTORY_FOR_INVOICE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			InvoiceGetLongTicketDTO invoiceGetLongTicketDTO = gson.fromJson(((JsonObject)jsonObject.get("invoiceGetLongTicketDTO")), InvoiceGetLongTicketDTO.class);
			
			if(invoiceGetLongTicketDTO.getStatus() == 1){
				model.addAttribute("msg", invoiceGetLongTicketDTO.getMessage());
				model.addAttribute("status", invoiceGetLongTicketDTO.getStatus());
				model.addAttribute("pagingInfo", invoiceGetLongTicketDTO.getTicketGroupPaginationDTO());
				model.addAttribute("ticketGroups", invoiceGetLongTicketDTO.getTicketGroupDTO());
				model.addAttribute("selectedTicket", invoiceGetLongTicketDTO.getSelectedTicketId());
			}else{
				model.addAttribute("msg", invoiceGetLongTicketDTO.getError().getDescription());
				model.addAttribute("status", invoiceGetLongTicketDTO.getStatus());
			}
			model.addAttribute("rowId", rowId);
			
			/*List<TicketGroup> ticketGroupList = null;
			
			JSONObject jObj = new JSONObject();
			jObj.put("status",0);
			
			List<Ticket> tickets = null;
			if(eventId==null || eventId.isEmpty()){
				jObj.put("msg","Not able to Identify event details.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
//			if(ticketId==null || ticketId.isEmpty()){
//				jObj.put("msg","Not able to Identify Ticket details.");
//				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
//				return;
//			}
						
			ticketGroupList = DAORegistry.getTicketGroupDAO().getAllTicketGroupsByEventAndBroker(Integer.parseInt(eventId), brokerId);
			List<TicketGroup> zeroQuntityList = new ArrayList<TicketGroup>();
			if(ticketGroupList!=null && !ticketGroupList.isEmpty()){
				for(TicketGroup ticGroup : ticketGroupList){
					tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(ticGroup.getId());
					if(tickets.size() > 0 && tickets.size() <= ticGroup.getQuantity()){
						ticGroup.setAvailableSeatLow(tickets.get(0).getSeatNo());
						ticGroup.setAvailableSeatHigh(tickets.get(tickets.size()-1).getSeatNo());
						ticGroup.setAvailableQty(tickets.size());
					}else{
						zeroQuntityList.add(ticGroup);
					}
				}
				if(!zeroQuntityList.isEmpty()){
					ticketGroupList.removeAll(zeroQuntityList);
				}
			}
			if(ticketGroupList == null || ticketGroupList.isEmpty()){
				jObj.put("msg","No matching Long tickets are found for selected invoice.");
				IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			JSONArray ticketGroupArray = new JSONArray();
			JSONObject ticketGroupObject = null;
			for(TicketGroup ticketGroup : ticketGroupList){
				ticketGroupObject = new JSONObject();
				ticketGroupObject.put("id", ticketGroup.getId());
				ticketGroupObject.put("purchaseOrderId", ticketGroup.getPurchaseOrderId());
				ticketGroupObject.put("section", ticketGroup.getSection());
				ticketGroupObject.put("row", ticketGroup.getRow());
				ticketGroupObject.put("seatLow", ticketGroup.getSeatLow());
				ticketGroupObject.put("seatHigh", ticketGroup.getSeatHigh());
				ticketGroupObject.put("quantity", ticketGroup.getQuantity());
				ticketGroupObject.put("availableSeatLow", ticketGroup.getAvailableSeatLow());
				ticketGroupObject.put("availableSeatHigh", ticketGroup.getAvailableSeatHigh());
				ticketGroupObject.put("availableQty", ticketGroup.getAvailableQty());
				ticketGroupObject.put("priceStr", ticketGroup.getPriceStr());
				ticketGroupObject.put("createdBy", ticketGroup.getCreatedBy());
				ticketGroupArray.put(ticketGroupObject);
			}
			jObj.put("ticketGroups", ticketGroupArray);
			jObj.put("pagingInfo",PaginationUtil.getDummyPaginationParameter(ticketGroupList!=null?ticketGroupList.size():0));
			jObj.put("rowId", rowId);
			jObj.put("selectedTicket", ticketId);
			jObj.put("status",1);
			IOUtils.write(jObj.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong..Please Try Again.");
		}
		return "";
	}
		
	
	@RequestMapping("/HoldTicket")
	public String holdTickets(HttpServletRequest request, HttpServletResponse response,HttpSession session,Model model){

		try {
			String ticketGroupId = request.getParameter("ticketGroupId");
			String action = request.getParameter("action");
			Integer brokerId = Util.getBrokerId(session);
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			String customerId = request.getParameter("customerId");
			String ticketIds = request.getParameter("ticketIds");
			String salesPrice = request.getParameter("salesPrice");
			String expiryDate = request.getParameter("expiryDate");
			String expiryMinutes = request.getParameter("expiryMinutes");
			
			Map<String, String> map = Util.getParameterMap(request);			
			map.put("customerId", customerId);
			map.put("ticketIds", ticketIds);
			map.put("salesPrice", salesPrice);
			map.put("expiryDate", expiryDate);
			map.put("expiryMinutes", expiryMinutes);
			map.put("action", action);
			map.put("userName", userName);
			map.put("ticketGroupId", ticketGroupId);
			map.put("brokerId", String.valueOf(brokerId));
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_HOLD_TICKET);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			TicketListDTO ticketListDTO = gson.fromJson(((JsonObject)jsonObject.get("ticketListDTO")), TicketListDTO.class);
			
			if(ticketListDTO.getStatus() == 1){
				model.addAttribute("tickets", gson.toJson(ticketListDTO.getTickets()));
				model.addAttribute("holdPagingInfo", gson.toJson(ticketListDTO.getPaginationDTO()));
				model.addAttribute("msg", ticketListDTO.getMessage());
				model.addAttribute("status", ticketListDTO.getStatus());
			}else{
				model.addAttribute("msg", ticketListDTO.getError().getDescription());
				model.addAttribute("status", ticketListDTO.getStatus());
			}			
			model.addAttribute("ticketGroupId", ticketGroupId);
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "page-hold-ticket";
	}
		

	@RequestMapping("/UnHoldTicket")
	public String unHoldTickets(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try {
			String ticketGroupId = request.getParameter("ticketGroupId");			
			String action = request.getParameter("action");
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("ticketGroupId", ticketGroupId);
			map.put("action", action);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UNHOLD_TICKETS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();
			object.put("status",0);
			Integer tgId = 0;
			
			if(action!=null && ticketGroupId!=null){
				String arr[] = null;				
				String ids = request.getParameter("ticketIds");				
				if(ids !=null && !ids.isEmpty()){
					if(ids.contains(",")){
						arr = ids.split(",");
					}else{
						arr = new String[1];
						arr[0] = ids;
					}
				}
				List<Integer> list = new ArrayList<Integer>();
				for(String str : arr){
					if(!str.trim().isEmpty()){
						list.add(Integer.parseInt(str));
					}
				}
				List<Ticket> tickets = DAORegistry.getTicketDAO().getTicketsByTicketIds(list);
				if(action.equalsIgnoreCase("UNHOLD")){
					String ticketId = null;
					for(Ticket tic : tickets){
						tic.setTicketStatus(TicketStatus.ACTIVE);
						tic.setActualSoldPrice(0.00);
						tgId = tic.getTicketGroupId();
						ticketId = String.valueOf(tic.getId());
					}
					List<InventoryHold> holds = DAORegistry.getInventoryHoldDAO().getInventoryHoldByTicketGroupId(tgId);
					InventoryHold hold = null;
					if(holds.size()==1){
						hold = holds.get(0);
					}else{
						for(InventoryHold h : holds){
							if(h.getTicketIds().contains(",")){
								String id[] = h.getTicketIds().split(",");
								for(String str : id){
									if(ticketId.equalsIgnoreCase(str)){
										hold = h;
										break;
									}
								}
							}else{
								if(ticketId.equalsIgnoreCase(h.getTicketIds())){
									hold = h;
									break;
								}
							}
							if(hold!=null){
								break;
							}
						}
					}
					hold.setStatus("DELETED");
					DAORegistry.getInventoryHoldDAO().update(hold);
					DAORegistry.getTicketDAO().updateAll(tickets);
					object.put("msg", "Ticket Ids "+ids+" are successfully removed from Hold.");
					object.put("status",1);
					
					//Tracking User Action
					String userActionMsg = "Ticket(s) are Removed from Hold. Ticket Id's - "+ids;
					Util.userActionAudit(request, null, userActionMsg);
				}				
			}
			if(ticketGroupId !=null && !ticketGroupId.isEmpty()){
			}else{
				object.put("msg", "Ticket Group Id is not found.");
			}			
			IOUtils.write(object.toString().getBytes(),response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong while UnHold Ticket. Please try again.");
		}
		return "";
	}
	
	
@RequestMapping(value = "/SaveInvoiceRealTicket")
public String saveInvoiceRealTicket(HttpServletRequest request, HttpServletResponse response,HttpSession session, Model model){
	LOGGER.info("Save invoice real ticket request processing......");
	
	try {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		Integer brokerId = Util.getBrokerId(session);
		
		Map<String, String> map = Util.getParameterMap(request);
		map.put("userName", userName);
		map.put("brokerId", brokerId.toString());
		
		String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_INVOICE_REAL_TICKET);
		Gson gson = GsonCustomConfig.getGsonBuilder();		
		JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
		GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
		
		if(genericResponseDTO.getStatus() == 1){
			model.addAttribute("msg", genericResponseDTO.getMessage());
			model.addAttribute("status", genericResponseDTO.getStatus());
		}else{
			model.addAttribute("msg", genericResponseDTO.getError().getDescription());
			model.addAttribute("status", genericResponseDTO.getStatus());
		}
		
		/*JSONObject object = new JSONObject();
		String returnMessage = "";
		String customerId = "";
		String action = "";
		Integer orderId=null;
		List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
		List<TicketGroup> dummyTGList = new ArrayList<TicketGroup>();
		List<OrderTicketGroupDetails> orderTickets = new ArrayList<OrderTicketGroupDetails>();
		object.put("status",0);
		action = request.getParameter("action");
		customerId = request.getParameter("customerId");
		Double ticketSoldPrice =null;
		Double taxAmount = 0.0;
		if(returnMessage.isEmpty()){
			if(action != null && action.equals("saveInvoice")){
				String username = SecurityContextHolder.getContext().getAuthentication().getName();
				String referalCode =request.getParameter("referralCode"); 
				String eventIdStr = request.getParameter("eventId");
				String addressLine1 = request.getParameter("addressLine1");
				String addressLine2 = request.getParameter("addressLine2");
				String shAddressLine1 = request.getParameter("shAddressLine1");
				String shAddressLine2 = request.getParameter("shAddressLine2");
				String isLoyalFan = request.getParameter("isLoyalFanPrice");
				String bAddressId =request.getParameter("bAddressId"); 
				String sAddressId =request.getParameter("sAddressId");
				String invoiceType = request.getParameter("createInvoiceType");
				String ticketSizeStr = request.getParameter("ticketSize");
				String promoTrackingId = request.getParameter("promoTrackingId");
				String affPromoTrackingId = request.getParameter("affPromoTrackingId");
				
				String ticketIds = request.getParameter("ticketIdStr");
				
				if(eventIdStr == null || eventIdStr.trim().isEmpty()) {
					object.put("msg", "Not able to identify Event Information, Please refresh page and try again.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				Integer eventId = null;
				try {
					eventId = Integer.parseInt(eventIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					object.put("msg", "Invalid Event information found, Please refresh page and try again.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				if(bAddressId == null || bAddressId.trim().isEmpty()) {
					object.put("msg", "Not able to identify Billing address details, Please select Billing Address.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				if(sAddressId == null || sAddressId.trim().isEmpty()) {
					object.put("msg", "Not able to identify Shipping address details, Please select Shipping Address.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				if((addressLine1==null || addressLine1.isEmpty()) && (addressLine2==null || addressLine2.isEmpty())){
					object.put("msg", "Selected billing address does not have either AddressLing1 or AddressLine2.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				if((shAddressLine1==null || shAddressLine1.isEmpty()) && (shAddressLine2==null || shAddressLine2.isEmpty())){
					object.put("msg", "Selected Shipping address does not have either AddressLing1 or AddressLine2.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				if(ticketSizeStr==null || ticketSizeStr.isEmpty()){
					object.put("msg", "Not able to identify Total No of Tickets.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				Integer ticketSize = null;
				try {
					ticketSize = Integer.parseInt(ticketSizeStr);
				} catch (Exception e) {
					e.printStackTrace();
					object.put("msg", "Bad values found for Total No of Tickets.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				
				Integer brokerId = Util.getBrokerId(session);
				Double orderTotal = 0.0;
				Integer orderTotalQty = 0;
				Double totalTaxAmout = 0.00;
				for(int i=0;i<ticketSize;i++){
					String ticketCountStr = request.getParameter("ticketCount_"+i);
					String ticketIdStr = request.getParameter("ticketId_"+i);
					String soldPrice = request.getParameter("soldPrice_"+i);
					String taxAmountStr = request.getParameter("taxAmount_"+i);
					
					if(taxAmountStr != null && !taxAmountStr.isEmpty() ) {
						try {
							taxAmount  = Double.parseDouble(taxAmountStr);
						} catch (Exception e) {
							object.put("msg", "Invaid Fees Amount.");
							IOUtils.write(object.toString().getBytes(), response.getOutputStream());
							return;
						}
					}
					totalTaxAmout += taxAmount;
					if(ticketIdStr == null || ticketIdStr.trim().isEmpty()) {
						object.put("msg", "Not able to identify Ticket Information, Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					if(ticketCountStr == null || ticketCountStr.trim().isEmpty()) {
						object.put("msg", "Not able to identify Ticket Quantity,  Please refresh page and try again.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					if(soldPrice == null || soldPrice.trim().isEmpty()) {
						object.put("msg", "Sold Price cannot be empty, Please add valid sold price.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					Integer tgId = null;
					try {
						tgId = Integer.parseInt(ticketIdStr);
					} catch (Exception e) {
						e.printStackTrace();
						object.put("msg", "Bad value found for Ticket Id, It should be valid integer value.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					Integer ticketCount = null;
					try {
						ticketCount = Integer.parseInt(ticketCountStr);
					} catch (Exception e) {
						e.printStackTrace();
						object.put("msg", "Bad value found for Ticket Quantity, It should be valid integer value.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					try {
						ticketSoldPrice= Double.parseDouble(soldPrice);
					} catch (Exception e) {
						e.printStackTrace();
						object.put("msg", "Bad value for Sold Price found, It should be valid Double value.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					
					TicketGroup tg = new TicketGroup();
					tg.setId(tgId);
					tg.setQuantity(ticketCount);
					tg.setPrice(ticketSoldPrice);
					dummyTGList.add(tg);
					Double total = (tg.getPrice() * tg.getQuantity()) + Util.getBrokerServiceFees(brokerId,tg.getPrice(),tg.getQuantity(), taxAmount);
					orderTotal += Util.getRoundedValue(total);
					orderTotalQty += tg.getQuantity();
					
				}
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
				if(customerLoyalty==null){
					customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setActivePoints(0.00);
					customerLoyalty.setActiveRewardDollers(0.00);
					customerLoyalty.setCustomerId(Integer.parseInt(customerId));
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setLatestEarnedPoints(0.00);
					customerLoyalty.setLatestSpentPoints(0.00);
					customerLoyalty.setPendingPoints(0.00);
					customerLoyalty.setRevertedSpentPoints(0.00);
					customerLoyalty.setTotalEarnedPoints(0.00);
					customerLoyalty.setTotalSpentPoints(0.00);
					customerLoyalty.setVoidedRewardPoints(0.00);
					DAORegistry.getCustomerLoyaltyDAO().save(customerLoyalty);
				}
				System.out.println("Applied promotional/referral code : "+referalCode);
				String msg = "";
				if(referalCode!=null && !referalCode.isEmpty()){
					Map<String, String> paramMap = Util.getParameterMap(request);
					paramMap.put("referralCode", referalCode);
					paramMap.put("customerId",customerId);
					paramMap.put("eventId",String.valueOf(eventId));
					paramMap.put("tgId",ticketIds);
					paramMap.put("clientIPAddress", request.getRemoteAddr());
					paramMap.put("isLongSale", "true");
					paramMap.put("orderTotal",String.valueOf(orderTotal));
					if(isLoyalFan.equalsIgnoreCase("Yes")){
						paramMap.put("isFanPrice","true");
					}else{
						paramMap.put("isFanPrice","false");
					}
					paramMap.put("sessionId", session.getId());
					paramMap.put("orderQty",String.valueOf(orderTotalQty));
					
				    String data = Util.getObject(paramMap,sharedProperty.getApiUrl()+Constants.VALIDATE_PROMOCODE_REAL_TICKET);
				    Gson gson = new Gson();  
					JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
					ReferralCodeValidation referralCodeValidation = gson.fromJson(((JsonObject)jsonObject.get("referralCodeValidation")), ReferralCodeValidation.class);
					if(referralCodeValidation == null){
						object.put("msg","Failed while validating referal code, Failed to communicate with our API server");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(referralCodeValidation.getStatus()==1){
						msg = "REFERRALCODEVALIDATED";
					}else if(referralCodeValidation.getStatus()==0){
						System.out.println("Referral Code Validation: "+referralCodeValidation.getError().getDescription());
						msg = referralCodeValidation.getError().getDescription();
						object.put("msg", msg);
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
				}
				for(TicketGroup tg : dummyTGList){
					TicketGroup ticGroup = null;
					ticGroup = DAORegistry.getTicketGroupDAO().getAllTicketGroupById(tg.getId());
					ticketGroups.add(ticGroup);
					if(ticGroup.getShippingMethodId()== null || ticGroup.getShippingMethodId()<0){
						ticGroup.setShippingMethod("E-Ticket");
						ticGroup.setShippingMethodId(1);
					}
					String zone = Util.computZoneForTickets(eventId, ticGroup.getSection());
					if(zone==null || zone.isEmpty()){
						zone = "N/A";
					}
					OrderTicketGroupDetails orderTicket = new OrderTicketGroupDetails();
					orderTicket.setZone(zone);
					orderTicket.setQuantity(tg.getQuantity());
					orderTicket.setSection(ticGroup.getSection());
					orderTicket.setRow(ticGroup.getRow());
					orderTicket.setTicketGroupId(ticGroup.getId());
					orderTicket.setSoldPrice(tg.getPrice());
					orderTicket.setActualPrice(ticGroup.getPrice());
					orderTicket.setOrderId(-1);
					List<Ticket> tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(tg.getId());
					if(tickets.size() < tg.getQuantity()){
						object.put("msg", "Given ticket group quantity is greater than available quantity, Please enter valid quantity.");
						IOUtils.write(object.toString().getBytes(), response.getOutputStream());
						return;
					}
					if(tg.getQuantity() == 1){
						orderTicket.setSeat(tickets.get(0).getSeatNo());
					}else{
						orderTicket.setSeat(tickets.get(0).getSeatNo()+"-"+tickets.get(tg.getQuantity()-1).getSeatNo());
					}
					orderTickets.add(orderTicket);
					//orderParamMap.put(String.valueOf(orderTicket.getId()),String.valueOf(orderTicket.getQuantity())+"_"+String.valueOf(tg.getPrice()));
				}
				
				String shippingIsSameAsBilling =request.getParameter("shippingIsSameAsBilling");
				String paymentMethod = request.getParameter("paymentMethod");
				
				if(TextUtil.isEmptyOrNull(paymentMethod)){
					object.put("msg", "Not able to identify payment method, please choose valid payment method.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				PaymentMethods method = PaymentMethods.valueOf(paymentMethod);
				Map<String, String> orderParamMap = Util.getParameterMap(request);
				orderParamMap.put("serviceFees", String.valueOf(totalTaxAmout));
				orderParamMap.put("promoTrackingId",promoTrackingId);
				orderParamMap.put("affPromoTrackingId",affPromoTrackingId);
				String validationMsg = "";
				String paymentMethodCount = null;
				WalletTransaction walletTranx = null;
				StripeTransaction stripeTranx = null;
				String primaryTrxId = "";
				String secondaryTrxId="";
				String thirdTrxId="";
				String primaryAmount=null;
				String secondaryAmount =null;
				String thirdAmount=null;
				String data = null;
				String rewardPoints = null;
				Gson gson = new Gson();
				JsonObject jsonObject = null;
				
				switch(method){
				case CREDITCARD :
					primaryAmount = String.valueOf(orderTotal);
					orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CREDITCARD));
					orderParamMap.put("primaryPaymentAmount",primaryAmount);
					paymentMethodCount="ONE";
					break;
				case WALLET :
					primaryAmount = String.valueOf(orderTotal);
					orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
					orderParamMap.put("primaryPaymentAmount",primaryAmount);
					paymentMethodCount="ONE";
					break;
				case REWARDPOINTS :
					rewardPoints =  request.getParameter("rewardPoints");
					primaryAmount = String.valueOf(orderTotal);
					orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.FULL_REWARDS));
					orderParamMap.put("primaryPaymentAmount",primaryAmount);
					paymentMethodCount="ONE";
					break;
				case PARTIAL_REWARD_CC :
					rewardPoints =  request.getParameter("rewardPoints");
					primaryAmount = rewardPoints;
					secondaryAmount = request.getParameter("cardAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward point amount is not found.";
					}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Card amount is not found.";
					}else{
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
						orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
						paymentMethodCount="TWO";
					}
					break;
				case PARTIAL_REWARD_WALLET :
					secondaryAmount = request.getParameter("walletAmount");
					rewardPoints =  request.getParameter("rewardPoints");
					primaryAmount = rewardPoints;
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward point amount is not found.";
					}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
					}else{
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
						paymentMethodCount="TWO";
					}
					break;
				case WALLET_CREDITCARD :
					secondaryAmount = request.getParameter("cardAmount");
					primaryAmount = request.getParameter("walletAmount");
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
					}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Credit card amount is not found.";
					}else{
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
						orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
						paymentMethodCount="TWO";
					}
					break;
				case PARTIAL_REWARD_WALLET_CC :
					thirdAmount = request.getParameter("cardAmount");
					secondaryAmount = request.getParameter("walletAmount");
					rewardPoints =  request.getParameter("rewardPoints");
					primaryAmount = rewardPoints;
					if(primaryAmount==null || primaryAmount.isEmpty()){
						validationMsg ="Reward points amount is not found.";
					}else if(secondaryAmount==null || secondaryAmount.isEmpty()){
						validationMsg ="Wallet amount is not found.";
					}else if(thirdAmount==null || thirdAmount.isEmpty()){
						validationMsg ="Credit card amount is not found.";
					}else{
						orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.PARTIAL_REWARDS));
						orderParamMap.put("primaryPaymentAmount",primaryAmount);
						orderParamMap.put("secondaryPaymentMethod",String.valueOf(PartialPaymentMethod.CUSTOMER_WALLET));
						orderParamMap.put("secondaryPaymentAmount",secondaryAmount);
						orderParamMap.put("thirdPaymentMethod",String.valueOf(PartialPaymentMethod.CREDITCARD));
						orderParamMap.put("thirdPaymentAmount",thirdAmount);
						paymentMethodCount="THREE";
					}
					break;
				case ACCOUNT_RECIVABLE:
					primaryAmount = String.valueOf(orderTotal);
					orderParamMap.put("primaryPaymentMethod",String.valueOf(PaymentMethod.ACCOUNT_RECIVABLE));
					orderParamMap.put("primaryPaymentAmount",primaryAmount);
					paymentMethodCount="ONE";
					break;
				}
				
				if(!validationMsg.isEmpty()){
					object.put("msg",validationMsg);
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				if(isLoyalFan.equalsIgnoreCase("Yes")){
					orderParamMap.put("isFanPrice","true");
				}else{
					orderParamMap.put("isFanPrice","false");
				}
				
				DAORegistry.getOrderTicketGroupDetailsDAO().saveAll(orderTickets);
				String orderTicketDetailsIds ="";
				for(OrderTicketGroupDetails otd : orderTickets){
					orderTicketDetailsIds += otd.getId()+",";
				}
				orderTicketDetailsIds = orderTicketDetailsIds.substring(0,orderTicketDetailsIds.length()-1);
				orderParamMap.put("eventId", String.valueOf(eventId));
				//orderParamMap.put("realTicketGroupIds", String.valueOf(ticketIds));
				orderParamMap.put("ticketGroupRefIds",orderTicketDetailsIds);
				orderParamMap.put("orderTotal",String.valueOf(orderTotal));
				orderParamMap.put("paymentMethodCount",paymentMethodCount);
				orderParamMap.put("customerId", String.valueOf(customerId));
				if(rewardPoints!=null && !rewardPoints.isEmpty()){
					Double points = Double.parseDouble(rewardPoints);
					orderParamMap.put("rewardPoints", String.valueOf(points));
				}
				orderParamMap.put("sessionId", session.getId());
				orderParamMap.put("billingAddressId", String.valueOf(bAddressId));
				orderParamMap.put("shippingAddressId", String.valueOf(sAddressId));
				orderParamMap.put("shippingSameAsBilling", shippingIsSameAsBilling);
				orderParamMap.put("referralCode", referalCode);
				orderParamMap.put("isLongSale","true");
				
				try {
					data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.INITIATE_ORDER_REAL_TICKET);
				} catch (Exception e) {
					e.printStackTrace();
					object.put("msg", "Invoice creation failed not able to communicate with API Server.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				gson = new Gson();		
				jsonObject = gson.fromJson(data, JsonObject.class);
				OrderConfirmation orderConfirmation = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
				if(orderConfirmation == null){
					object.put("msg", "Invoice creation failed not able to communicate with API Server.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				if(orderConfirmation.getStatus() == 0){
					object.put("msg",orderConfirmation.getError().getDescription());
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				orderId = orderConfirmation.getOrderId();
				boolean isPaymentSuccess = false;
				
				switch(method){
				case CREDITCARD :
					stripeTranx = doCreditCardTransaction(request,String.valueOf(orderTotal),String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
					if(stripeTranx.getStatus()==0){
						validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else{
						isPaymentSuccess = true;
						primaryTrxId = stripeTranx.getTransactionId();
						orderParamMap.put("primaryTransactionId",primaryTrxId);
					}
					break;
				case WALLET :
					walletTranx = doWalletTransaction(request, String.valueOf(orderTotal),orderId);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else{
						isPaymentSuccess = true;
						primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
						orderParamMap.put("primaryTransactionId",primaryTrxId);
						orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
					}
					break;
				case REWARDPOINTS :
					isPaymentSuccess = true;
					break;
				case PARTIAL_REWARD_CC :
					stripeTranx = doCreditCardTransaction(request,secondaryAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
					if(stripeTranx.getStatus()==0){
						validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else{
						isPaymentSuccess = true;
						secondaryTrxId = stripeTranx.getTransactionId();
						orderParamMap.put("primaryTransactionId",primaryTrxId);
						orderParamMap.put("secondaryTransactionId",secondaryTrxId);
					}
					break;
				case PARTIAL_REWARD_WALLET :
					walletTranx= doWalletTransaction(request,secondaryAmount,orderId);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else{
						isPaymentSuccess = true;
						secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
						orderParamMap.put("primaryTransactionId",primaryTrxId);
						orderParamMap.put("secondaryTransactionId",secondaryTrxId);
						orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
					}
					break;
				case WALLET_CREDITCARD :
					walletTranx= doWalletTransaction(request,primaryAmount,orderId);
					stripeTranx = doCreditCardTransaction(request,secondaryAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else if(stripeTranx.getStatus()==0){
						validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else{
						isPaymentSuccess = true;
						primaryTrxId = walletTranx.getWalletHistory().getTransactionId();
						secondaryTrxId = stripeTranx.getTransactionId();
						orderParamMap.put("primaryTransactionId",primaryTrxId);
						orderParamMap.put("secondaryTransactionId",secondaryTrxId);
						orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
					}
					break;
				case PARTIAL_REWARD_WALLET_CC :
					walletTranx = doWalletTransaction(request,secondaryAmount,orderId);
					stripeTranx = doCreditCardTransaction(request,thirdAmount,String.valueOf(orderTicketDetailsIds),true,orderTotalQty);
					if(walletTranx.getStatus()==0){
						validationMsg ="Wallet Transaction - "+walletTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else if(stripeTranx.getStatus()==0){
						validationMsg ="Credit card transaction - "+stripeTranx.getError().getDescription();
						isPaymentSuccess = false;
					}else{
						isPaymentSuccess = true;
						secondaryTrxId = walletTranx.getWalletHistory().getTransactionId();
						thirdTrxId = stripeTranx.getTransactionId();
						orderParamMap.put("primaryTransactionId",primaryTrxId);
						orderParamMap.put("secondaryTransactionId",secondaryTrxId);
						orderParamMap.put("walletTransactionId",String.valueOf(walletTranx.getWalletHistory().getId()));
						orderParamMap.put("thirdTransactionId",thirdTrxId);
					}
					break;
				case ACCOUNT_RECIVABLE:
					isPaymentSuccess = true;
					orderParamMap.put("primaryTransactionId","dummytransactionid"+orderConfirmation.getOrderId());
					break;
				}
				
				orderParamMap.put("orderId", String.valueOf(orderId));
				orderParamMap.put("isPaymentSuccess",String.valueOf(isPaymentSuccess));
				try {
					data = Util.getObject(orderParamMap, sharedProperty.getApiUrl()+Constants.CREATE_ORDER_REAL_TICKET);
				} catch (Exception e) {
					e.printStackTrace();
					object.put("msg", "Order confirmation failed not able to communicate with API Server.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				gson = new Gson();		
				jsonObject = gson.fromJson(data, JsonObject.class);
				OrderConfirmation orderConfirmation1 = gson.fromJson(((JsonObject)jsonObject.get("orderConfirmation")), OrderConfirmation.class);	
				if(!validationMsg.isEmpty()){
					object.put("msg",validationMsg);
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				if(orderConfirmation1 == null){
					object.put("msg", "Order confirmation failed not able to communicate with API Server.");
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				if(orderConfirmation1.getStatus() == 0){
					object.put("msg", "Order Confirmation Failed, "+orderConfirmation1.getError().getDescription());
					IOUtils.write(object.toString().getBytes(), response.getOutputStream());
					return;
				}
				
				Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderConfirmation.getOrderId());
				CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(invoice.getCustomerOrderId());
				invoice.setRealTixMap("Yes");
				if(order.getPrimaryPaymentMethod().equals(PaymentMethod.ACCOUNT_RECIVABLE)){
					invoice.setInvoiceType(PaymentMethod.ACCOUNT_RECIVABLE.toString());
				}
				DAORegistry.getInvoiceDAO().update(invoice);
				for(TicketGroup tic : ticketGroups){
					List<Ticket> tickets = DAORegistry.getTicketDAO().getAvailableTicketsByTicketGroupId(tic.getId());
					Integer qty = 0;
					for(TicketGroup dTic : dummyTGList){
						if(tic.getId() == dTic.getId() || tic.getId().equals(dTic.getId())){
							qty = dTic.getQuantity();
							break;
						}
					}
					for(int i=0;i<qty;i++){
						tickets.get(i).setInvoiceId(invoice.getId());
						tickets.get(i).setTicketStatus(TicketStatus.SOLD);
					}
					DAORegistry.getTicketDAO().updateAll(tickets);
				}
				
				List<RealTicketSectionDetails> realTicketList = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
				Util.setActualTicketDetails(realTicketList, order);
				DAORegistry.getCustomerOrderDAO().update(order);
				
				object.put("msg", "Order created successfully, InvoiceId : "+invoice.getId());
				Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
				if(!customer.getProductType().equals(ProductType.REWARDTHEFAN) && customer.getEmail()!=null && !customer.getEmail().isEmpty()
						&& (customer.getIsRegistrationMailSent()==null || customer.getIsRegistrationMailSent() == false)){
					customer.setApplicationPlatForm(ApplicationPlatform.DESKTOP_SITE);
					customer.setProductType(ProductType.REWARDTHEFAN);
					customer.setSignupType(SignupType.REWARDTHEFAN);
					customer.setIsRegistrationMailSent(true);
					customer.setSignupDate(new Date());
					customer.setUserId(Util.generatedCustomerUserId(customer.getCustomerName(), customer.getLastName()));
					//customer.setReferrerCode(Util.generateCustomerReferalCode());
					customer.setReferrerCode(customer.getEmail());
					String password = Util.generatePassword(customer.getCustomerName(), "RTF");
					customer.setPassword(Util.generateEncrptedPassword(password));
					DAORegistry.getCustomerDAO().update(customer);
					Map<String, Object> mailMap = new HashMap<String, Object>();
					mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
					mailMap.put("password", password);
					mailMap.put("userName", customer.getUserName());
					mailMap.put("androidUrl","https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en");
					mailMap.put("iosUrl","https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8");
					com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
					mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",Util.getFilePath(request, "share.png", "/resources/images/"));
					mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app1.png",Util.getFilePath(request, "app1.png", "/resources/images/"));
					mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app2.png",Util.getFilePath(request, "app2.png", "/resources/images/"));
					mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",Util.getFilePath(request, "blue.png", "/resources/images/"));
					mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",Util.getFilePath(request, "fb1.png", "/resources/images/"));
					mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",Util.getFilePath(request, "ig1.png", "/resources/images/"));
					mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",Util.getFilePath(request, "li1.png", "/resources/images/"));
					mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo_white.png",Util.getFilePath(request, "logo_white.png", "/resources/images/"));
					mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",Util.getFilePath(request, "p1.png", "/resources/images/"));
					mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",Util.getFilePath(request, "tw1.png", "/resources/images/"));
					mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
					String email = customer.getEmail();
					//email = "msanghani@rightthisway.com";
					String bcc = "";
					bcc = "amit.raut@rightthisway.com,msanghani@righthtisway.com";
					try {
						mailManager.sendMailNow("text/html","sales@rewardthefan.com",email, 
								null,bcc, "You are successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-registration.html", mailMap, "text/html", null,mailAttachment,null);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
				}
				
				//Tracking User Action
				String userActionMsg = "Invoice Created Successfully.";
				Util.userActionAudit(request, invoice.getId(), userActionMsg);
			}
		}
		object.put("status",1);
		IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msg", "There is something wrong while Create Invoice. Please try again.");
		}
		return "";
	}

	
	@RequestMapping(value = "/SendMailToCustomers")
	public String sendMailToCustomers(HttpServletRequest request, HttpServletResponse response,HttpSession session, Model model){
		
		try{			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			String path = request.getSession().getServletContext().getRealPath("/WEB-INF/classes/templates/");
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("path", path);
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SEND_MAIL_TO_CUSTOMERS);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			
			/*JSONObject object = new JSONObject();
			object.put("status", 0);
			String mailTo = request.getParameter("mailTo");
			String mailCc = request.getParameter("mailCc");
			String mailBcc = request.getParameter("mailBcc");
			String mailSubject = request.getParameter("mailSubject");
			String templateBody = request.getParameter("txtDefaultHtmlArea");
			String templateName = "mail-rewardfan-invite-retail-customer.html";
			String templateIdStr = request.getParameter("templateName");
			String from =  request.getParameter("from");
			Customer customer = null;
			EmailTemplate emailTemplate = null;
			
			if(mailTo == null || mailTo.isEmpty()){
				object.put("msg", "Not able to identify Mail To.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(from == null || from.isEmpty()){
				object.put("msg", "Not able to identify From Address.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(mailSubject == null || mailSubject.isEmpty()){
				object.put("msg", "Not able to identify Mail Subject.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(templateBody == null || templateBody.isEmpty()){
				object.put("msg", "Not able to identify Mail Body");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(templateName == null || templateName.isEmpty()){
				object.put("msg", "Not able to identify template, Please select template.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(templateIdStr == null || templateIdStr.isEmpty()){
				object.put("msg", "Not able to identify template, Please select template.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			emailTemplate = DAORegistry.getEmailTemplateDAO().get(Integer.parseInt(templateIdStr));
			if(emailTemplate == null){
				object.put("msg", "Not able to identify template in DB, Please select template.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			templateBody = "<html><head></head><body>"+templateBody+"</body></html>";
			templateBody = templateBody.replaceAll("../resources/images/","cid:");
			templateBody = templateBody.replaceAll("https://ticketfulfillment.com/resources/images/","cid:");
			templateBody = templateBody.replace("[","${");
			templateBody = templateBody.replace("]","}");
			String path = request.getSession().getServletContext().getRealPath("/WEB-INF/classes/templates/");
			
			File templateFile = new File(path+File.separator+templateName);
			if(!templateFile.exists()){
				templateFile.createNewFile();
			}
			
			PrintWriter writter = new PrintWriter(templateFile);
			writter.print("");
			writter.close();
			
			writter = new PrintWriter(templateFile);
			writter.print(templateBody);
			writter.close();
			
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			EmailBlastTracking emailBlastTracking = null;
			if(!mailTo.isEmpty()){
				List<String> emails = new ArrayList<String>();
				if(mailTo.indexOf(",") != -1){ 
					String mailToArr[] = mailTo.split(",");
					for(int i=0; i<mailToArr.length; i++){
						emails.add(mailToArr[i].trim());
					}
				}else{
					emails.add(mailTo.trim());
				}
				boolean isMailSent = false;
				for(String email : emails){
					List<Customer> customers = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
					if(customers!= null && !customers.isEmpty()){
						customer = customers.get(0);
					}
					
					emailBlastTracking = new EmailBlastTracking();
					emailBlastTracking.setTemplateId(Integer.parseInt(templateIdStr));
					emailBlastTracking.setSentTime(new Date());
					emailBlastTracking.setSentBy(userName);
					emailBlastTracking.setSentTo(email);
					emailBlastTracking.setStatus(false);
					DAORegistry.getEmailBlastTrackingDAO().save(emailBlastTracking);
					
					//if(customer != null && customer.getEmail()!=null && !customer.getEmail().isEmpty()){
						isMailSent = false;
						Map<String, Object> mailMap = new HashMap<String, Object>();
						//mailMap.put("customerName", customer.getCustomerName()+" "+customer.getLastName());
						com.rtw.tracker.mail.MailAttachment[] mailAttachment = new com.rtw.tracker.mail.MailAttachment[11];
						mailAttachment[0] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","share.png",Util.getFilePath(request, "share.png", "/resources/images/"));
						mailAttachment[1] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tyrtf.png",Util.getFilePath(request, "tyrtf.png", "/resources/images/"));
						mailAttachment[2] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","fb1.png",Util.getFilePath(request, "fb1.png", "/resources/images/"));
						mailAttachment[3] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","tw1.png",Util.getFilePath(request, "tw1.png", "/resources/images/"));
						mailAttachment[4] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","ig1.png",Util.getFilePath(request, "ig1.png", "/resources/images/"));
						mailAttachment[5] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","p1.png",Util.getFilePath(request, "p1.png", "/resources/images/"));
						mailAttachment[6] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","li1.png",Util.getFilePath(request, "li1.png", "/resources/images/"));
						mailAttachment[7] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","blue.png",Util.getFilePath(request, "blue.png", "/resources/images/"));
						mailAttachment[8] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","logo_white_final.png",Util.getFilePath(request, "logo_white_final.png", "/resources/images/"));
						mailAttachment[9] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app1.png",Util.getFilePath(request, "app1.png", "/resources/images/"));
						mailAttachment[10] = new com.rtw.tracker.mail.MailAttachment(null,"image/png","app2.png",Util.getFilePath(request, "app2.png", "/resources/images/"));
						
						
						String bcc ="msanghani@rightthisway.com,amit.raut@rightthisway.com";
						if(from!=null && (from.equalsIgnoreCase("leor@rewardthefan.com") ||
								from.equalsIgnoreCase("dalia@rewardthefan.com"))){
							bcc += ",leor@rewardthefan.com";
						}
						if(mailBcc != null && !mailBcc.isEmpty()){
							bcc = ","+mailBcc;
						}
						try {
							mailManager.sendMailNow("text/html",from,email, 
									mailCc,bcc, mailSubject,
									templateName, mailMap, "text/html", null,mailAttachment,null);
							isMailSent = true;
						} catch (Exception e) {
							e.printStackTrace();
							object.put("msg", "Error occured while sending mail.");
						}
						
						if(isMailSent){
							emailBlastTracking.setStatus(true);
							DAORegistry.getEmailBlastTrackingDAO().update(emailBlastTracking);
						}
						if(isMailSent && customer!=null){
							for(Customer cust : customers){
								cust.setIsMailSent(true);
							}
							DAORegistry.getQueryManagerDAO().updateEmailBlastToMailSent(email);
							DAORegistry.getCustomerDAO().updateAll(customers);
						}
					//}else{
						//object.put("msg", "Customer not found.");
					//}
				}
				if(isMailSent){
					object.put("status",1);
					object.put("msg", "Email sent to selected customers.");
				}
				
				
			}
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/UpdateEmailTemplate")
	public String updateEmailTemplate(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model){
		
		try{
			String templateId = request.getParameter("templateId");
			String templateBody = request.getParameter("templateBody");
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("templateId", templateId);
			map.put("templateBody", templateBody);
			map.put("userName", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_UPDATE_EMAIL_TEMPLATE);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
			/*JSONObject object = new JSONObject();
			object.put("status", 0);
			
			if(templateIdStr == null || templateIdStr.isEmpty()){
				object.put("msg", "Not able to identify Template.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			Integer templateId = null;
			try {
				templateId = Integer.parseInt(templateIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				object.put("msg", "Could not save, Template body found empty");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			if(templateBody == null || templateBody.isEmpty()){
				object.put("msg", "Could not save, Template body found empty");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			
			EmailTemplate template = DAORegistry.getEmailTemplateDAO().get(templateId);
			if(template == null){
				object.put("msg", "Template not found in system.");
				IOUtils.write(object.toString().getBytes(), response.getOutputStream());
				return;
			}
			template.setBody(templateBody);
			template.setLastUpdated(new Date());
			template.setUpdatedBy(userName);
			
			DAORegistry.getEmailTemplateDAO().update(template);
			object.put("msg","Template changes saved successfully.");
			object.put("status",1);
			IOUtils.write(object.toString().getBytes(), response.getOutputStream());*/
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value = "/SaveInvoiceHoldTicket")
	public String saveInvoiceHoldTicket(HttpServletRequest request, HttpServletResponse response,HttpSession session, Model model){
		LOGGER.info("Save invoice hold ticket request processing......");
		
		try {
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			Integer brokerId = Util.getBrokerId(session);
			
			Map<String, String> map = Util.getParameterMap(request);
			map.put("userName", userName);
			map.put("brokerId", brokerId.toString());
			
			String data = Util.getObjectFromApi(map, request, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_SAVE_INVOICE_HOLD_TICKET);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			GenericResponseDTO genericResponseDTO = gson.fromJson(((JsonObject)jsonObject.get("genericResponseDTO")), GenericResponseDTO.class);
			
			if(genericResponseDTO.getStatus() == 1){
				model.addAttribute("msg", genericResponseDTO.getMessage());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}else{
				model.addAttribute("msg", genericResponseDTO.getError().getDescription());
				model.addAttribute("status", genericResponseDTO.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
}