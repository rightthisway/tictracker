package com.rtw.tracker.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.utils.Util;
import com.rtw.tracker.datas.Role;
import com.rtw.tracker.datas.TrackerBrokers;
import com.rtw.tracker.datas.TrackerUser;
import com.rtw.tracker.datas.UserPreference;
import com.rtw.tracker.pojos.CheckUserNameDTO;
import com.rtw.tracker.pojos.GettingBrokerDTO;
import com.rtw.tracker.pojos.UserPreferenceDTO;
import com.rtw.tracker.utils.GsonCustomConfig;

public class UserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
//		System.out.println(userName);
//		TrackerUser trackerUser = DAORegistry.getTrackerUserDAO().getTrackerUserByUsernameOrEmail(userName, userName);
//		if(trackerUser == null){
//			return null;
//		}
//		System.out.println(trackerUser);
		try{
			Map<String, String> map = Util.getParameterMap(null);
			TrackerUser trackerUser =  null;		
			
			map.put("userName", userName);
			map.put("email", userName);
			
			String data = Util.getObject(map, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_CHECK_USERNAME);
			Gson gson = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
			CheckUserNameDTO checkUserNameDTO = gson.fromJson(((JsonObject)jsonObject.get("checkUserNameDTO")), CheckUserNameDTO.class);
			
			if(checkUserNameDTO.getStatus() == 1){
				trackerUser = (TrackerUser)checkUserNameDTO.getTrackerUser();
			}else{
				return null;
			}
			
			
			HttpSession session = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
									.getRequest().getSession();
			session.setAttribute("trackerUser", trackerUser);
			
	//		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
	//		List<UserPreference> userPreferences = DAORegistry.getUserPreferenceDAO().getPreferenceByUserName(userName);
			List<UserPreference> userPreferences = null;
			
			Map<String, String> paramMap = Util.getParameterMap(null);		
			paramMap.put("userName", userName);
			
			String data1 = Util.getObject(paramMap, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_USER_PREFERENCES);
			Gson gson1 = GsonCustomConfig.getGsonBuilder();		
			JsonObject jsonObject1 = gson1.fromJson(data1, JsonObject.class);
			UserPreferenceDTO userPreferenceDTO = gson1.fromJson(((JsonObject)jsonObject1.get("userPreferenceDTO")), UserPreferenceDTO.class);
			
			if(userPreferenceDTO.getStatus() == 1){
				userPreferences = userPreferenceDTO.getUserPreferences();
			}
			
			if(userPreferences!=null && !userPreferences.isEmpty()){
				for(UserPreference userPref : userPreferences){
					session.setAttribute(userPref.getGridName(), userPref.getColumnOrders());
				}
			}
			Boolean isAdmin =false;
			Boolean isSeller =false;
			Boolean isBroker =false;
			Boolean isUser =false;
			Boolean isAffiliates =false;
			Boolean isContest = false;
			
			
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			if(trackerUser.getRoles() != null && !trackerUser.getRoles().isEmpty()){
				for(Role role : trackerUser.getRoles()){				
					authorities.add(new SimpleGrantedAuthority(role.getName()));
				    if(role.getName().equals("ROLE_SUPER_ADMIN")){
				    	isAdmin = true;
				     //session.setAttribute("isAdmin",true);
				    }else if(role.getName().equals("ROLE_BROKER")){
				    	isBroker = true;
				    // session.setAttribute("isBroker",true);
				    }else if(role.getName().equals("ROLE_USER")){
				    	isUser = true;
				     //session.setAttribute("isUser",true);
				    }else if(role.getName().equals("ROLE_AFFILIATES")){
				    	isAffiliates = true;
				    // session.setAttribute("isAffiliates",true);
				    }else if(role.getName().equals("ROLE_CONTEST")){
				    	isContest = true;
				    }else if(role.getName().equals("ROLE_SELLER")){
				    	isSeller = true;
				     //session.setAttribute("isAdmin",true);
				    }
				}
			}else{
				authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
				isUser = true;
			}
			
			session.setAttribute("isAdmin",isAdmin);
			session.setAttribute("isBroker",isBroker);
			session.setAttribute("isUser",isUser);
			session.setAttribute("isAffiliates",isAffiliates);
			session.setAttribute("isContest",isContest);
			session.setAttribute("isSeller",isSeller);
			session.setAttribute("userName",trackerUser.getUserName());
			session.setAttribute("userId", trackerUser.getId());
			try {
				Collection<TrackerBrokers> trackerBrokers = new ArrayList<TrackerBrokers>();
				if(isBroker && !isAdmin){
					trackerBrokers.add(trackerUser.getBroker());
					session.setAttribute("brokerId",trackerUser.getBroker().getId());
					session.setAttribute("brokerCompanyName", trackerUser.getBroker().getCompanyName());
				}else if(isAdmin){
					Map<String, String> paramMap1 = Util.getParameterMap(null);		
					paramMap1.put("brokerId", Constants.BROKERID.toString());
					
					String data2 = Util.getObject(paramMap1, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_BROKERS);
					Gson gson2 = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject2 = gson2.fromJson(data2, JsonObject.class);
					GettingBrokerDTO gettingBrokerDTO = gson2.fromJson(((JsonObject)jsonObject2.get("gettingBrokerDTO")), GettingBrokerDTO.class);
					
					if(gettingBrokerDTO.getStatus() == 1){
						trackerBrokers = gettingBrokerDTO.getTrackerBrokers();
						session.setAttribute("brokerCompanyName", gettingBrokerDTO.getCompanyName());
					}
					session.setAttribute("brokerId", Constants.BROKERID);
					
	//				trackerBrokers = DAORegistry.getTrackerBrokersDAO().getAll();
	//				session.setAttribute("brokerCompanyName", DAORegistry.getTrackerBrokersDAO().get(Constants.BROKERID).getCompanyName());
				}else if(isUser){
					Map<String, String> paramMap1 = Util.getParameterMap(null);		
					paramMap1.put("brokerId", "1001");
					
					String data2 = Util.getObject(paramMap1, Constants.TICTRACKER_API_BASE_URL+Constants.TICTRACKER_API_GET_BROKERS);
					Gson gson2 = GsonCustomConfig.getGsonBuilder();		
					JsonObject jsonObject2 = gson2.fromJson(data2, JsonObject.class);
					GettingBrokerDTO gettingBrokerDTO = gson2.fromJson(((JsonObject)jsonObject2.get("gettingBrokerDTO")), GettingBrokerDTO.class);
					
					if(gettingBrokerDTO.getStatus() == 1){
						trackerBrokers.add(gettingBrokerDTO.getTrackerBroker());
					}
					
	//				trackerBrokers.add(DAORegistry.getTrackerBrokersDAO().get(1001));
				}
				session.setAttribute("brokers",trackerBrokers);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			//Tracking User Action		
			String userActionMsg = "Login Success";					
			Util.userActionAudit(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest(), null, userActionMsg);
			
			UserDetails userDetails = new org.springframework.security.core.userdetails.
					User(trackerUser.getUserName(), trackerUser.getPassword(), true, true, true, true, authorities);
			
			return userDetails;
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

}
