package com.rtw.tracker.fedex;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtw.tracker.dao.implementation.HibernateDAO;



public class FedExLabelLogDAO extends HibernateDAO<Integer,FedExLabelLog> {

	public List<FedExLabelLog> getFedExLabelLogListBasedOnDateRange(Date startDate,Date endDate,Integer invoiceId){
		String sql="FROM FedExLabelLog WHERE 1=1";
		List<Object>params = new ArrayList<Object>();
		
		
		if(startDate!=null && !"".equals(startDate)){
			sql=sql + "AND labelCreationDateTime>= ?";
			params.add(startDate);
		}
		if(endDate!=null && !"".equals(endDate)){
			sql=sql + "AND labelCreationDateTime<= ?";
			params.add(endDate);
		}
		if(invoiceId!=null && !"".equals(invoiceId)){
			sql=sql + "AND invoiceId = ?";
			params.add(invoiceId);
		}
		/*if(!voidType.equalsIgnoreCase("All"))
		if(voidType!=null && !"".equals(voidType)){
			//sql=sql + "AND voidRedo = ?";
			if(voidType.equalsIgnoreCase("Void Redo"))
				sql=sql + "AND voidRedo is not NULL";
			else
				sql=sql + "AND voidRedo is NULL";
			//params.add(voidType);
		}*/
		return find(sql,params.toArray());
	}
	
	public FedExLabelLog getFedExLabelLog(Integer invoiceId){
		String sql="FROM FedExLabelLog WHERE 1=1";
		List<Object>params = new ArrayList<Object>();
		
		if(invoiceId!=null && !"".equals(invoiceId)){
			sql=sql + " AND invoiceId = ?";
			params.add(invoiceId);
		}
		return findSingle(sql,params.toArray());
	}
}
