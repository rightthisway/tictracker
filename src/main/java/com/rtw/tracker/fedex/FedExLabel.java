package com.rtw.tracker.fedex;

import java.util.Date;

public class FedExLabel {
	
	private String startDate;
	private String endDate;
	private Integer invoiceId;
	private String selectedInput="dateRange";
	
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getSelectedInput() {
		return selectedInput;
	}
	public void setSelectedInput(String selectedInput) {
		this.selectedInput = selectedInput;
	}
	
	
	
	
	

}
