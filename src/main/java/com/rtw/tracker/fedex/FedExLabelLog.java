package com.rtw.tracker.fedex;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="fedex_label_log")
public class FedExLabelLog implements Serializable {
	
	private Integer labelId;
	private Integer invoiceId;
	private Integer purchaseOrderId;
	private String labelFileName;
	private String status;
	private Date labelCreationDateTime;
	private String lastPrinted;
	private String notificationCode;
	private String notificationMessage;
	private String logType;
	private String trackingNo;
	
	public FedExLabelLog(){
		
	}
	
	public FedExLabelLog(FedExLabelLog fedExLabelLog){
		this.labelId=fedExLabelLog.labelId;
		this.invoiceId=fedExLabelLog.invoiceId;
		this.purchaseOrderId=fedExLabelLog.purchaseOrderId;
		this.labelFileName=fedExLabelLog.labelFileName;
		this.status=fedExLabelLog.status;
		this.labelCreationDateTime=fedExLabelLog.labelCreationDateTime;
		this.notificationCode=fedExLabelLog.notificationCode;
		this.notificationMessage=fedExLabelLog.notificationMessage;
		this.logType = fedExLabelLog.logType;
		this.trackingNo = fedExLabelLog.trackingNo;
	}


	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getLabelId() {
		return labelId;
	}

	public void setLabelId(Integer labelId) {
		this.labelId = labelId;
	}

	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	@Column(name="purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	@Column(name="label_file_name")
	public String getLabelFileName() {
		return labelFileName;
	}

	public void setLabelFileName(String labelFileName) {
		this.labelFileName = labelFileName;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name="created_date")
	public Date getLabelCreationDateTime() {
		return labelCreationDateTime;
	}

	public void setLabelCreationDateTime(Date labelCreationDateTime) {
		this.labelCreationDateTime = labelCreationDateTime;
	}

	/*@Column(name="last_printed")
	public String getLastPrinted() {
		return lastPrinted;
	}

	public void setLastPrinted(String lastPrinted) {
		this.lastPrinted = lastPrinted;
	}*/
	
	@Column(name="notification_code")
	public String getNotificationCode() {
		return notificationCode;
	}

	public void setNotificationCode(String notificationCode) {
		this.notificationCode = notificationCode;
	}
	
	@Column(name="notification_message")
	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	@Column(name="log_type")
	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}	
	
	@Column(name="tracking_no")
	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	
}
