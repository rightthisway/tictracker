package com.rtw.tracker.fedex;

import java.util.Date;

/**
 * PDF Ticket File POJO
 * @author frederic
 *
 */
public class PdfTicketFile {	
	public final static String FOLDER_TYPE_ERROR_EXTRACTION = "errorExtraction"; 
	public final static String FOLDER_TYPE_ERROR_EVENT_NAME = "errorEventName"; 
	public final static String FOLDER_TYPE_ERROR_DUPLICATE = "errorDuplicate"; 
	public final static String FOLDER_TYPE_ERROR_UNSPLITABLE = "errorUnsplitable"; 
	public final static String FOLDER_TYPE_ERROR_FILE_NAME = "errorFileName"; 
	public final static String FOLDER_TYPE_INCOMING = "incoming"; 
	public final static String FOLDER_TYPE_UPLOAD = "upload"; 
	public final static String FOLDER_TYPE_FIXED_EXTRACTION = "fixedExtraction"; 
	public final static String FOLDER_TYPE_BACKUP = "backup"; 
	
	private String fileName;
	private Date lastModified;
	private String section;
	private String row;
	private String seat;
	private String barCode;
	private Integer eventId;
	private String folderType;	
	private String pdfEventName;
	private Boolean isPdfEvent;
		
	public String getFolderType() {
		return folderType;
	}

	public void setFolderType(String folderType) {
		this.folderType = folderType;
	}

	public String getFileName() {
		return fileName;
	}
	
	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}		
	
	/*MITUL
	 * public PdfEvent getEvent() {
		if (eventId == null) {
			return null;
		}
		return PdfAutomationDAORegistry.getPdfEventDAO().get(eventId);
	}*/

	public String getPdfEventName() {
		return pdfEventName;
	}

	public void setPdfEventName(String pdfEventName) {
		this.pdfEventName = pdfEventName;
	}

	public Boolean getIsPdfEvent() {
		return isPdfEvent;
	}

	public void setIsPdfEvent(Boolean isPdfEvent) {
		this.isPdfEvent = isPdfEvent;
	}

}
