package com.rtw.tracker.fedex;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.apache.axis.types.NonNegativeInteger;
import org.apache.axis.types.PositiveInteger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rtw.tracker.utils.Constants;
import com.rtw.tracker.dao.implementation.DAORegistry;
import com.rtw.tracker.datas.Country;
import com.rtw.tracker.datas.Invoice;
import com.rtw.tracker.datas.InvoiceAudit;
import com.rtw.tracker.datas.ManualFedexGeneration;
import com.rtw.tracker.datas.PurchaseOrder;
import com.rtw.tracker.datas.State;
import com.rtw.tracker.enums.InvoiceAuditAction;

public class CreateFedexLabelUtil {
	

	/*public static void recreateFedExShipLabel(Recipient recipient,ServiceType serviceType )
	{
		String labelLocation=null;
		try{
		ProcessShipmentRequest request = buildRequest(recipient,serviceType);
		ShipServiceLocator service;
		ShipPortType port;
		//
		service = new ShipServiceLocator();
		updateEndPoint(service);
		port = service.getShipServicePort();
		Invoice invoice = DAORegistry.getInvoiceDAO().get(recipient.getInvoiceId());
		
		ProcessShipmentReply reply = port.processShipment(request); // This is the call to the ship web service passing in a request object and returning a reply object
		if (isResponseOk(reply.getHighestSeverity())){
			labelLocation=writeServiceOutput(reply,recipient);
		}
		int i=0;
		printNotifications(reply.getNotifications());
		for(Notification notification:reply.getNotifications()){
			if(i==0){
				FedExLabelLog fedExLabelLog=new FedExLabelLog();
				fedExLabelLog.setInvoiceId(recipient.getInvoiceId());
				fedExLabelLog.setLabelCreationDateTime(new Date());
				if(labelLocation!=null){
					fedExLabelLog.setStatus("SUCCESS");
				    fedExLabelLog.setLabelFileName(labelLocation);
				}else{
					fedExLabelLog.setLabelFileName(null);
					fedExLabelLog.setStatus("FAILURE");
				}				
				if(invoice != null){
					fedExLabelLog.setTrackingNo(invoice.getTrackingNo());
				}
				fedExLabelLog.setNotificationCode(notification.getCode());
				fedExLabelLog.setNotificationMessage(notification.getMessage());
				fedExLabelLog.setLogType("CREATE");
				DAORegistry.getFedExLabelLogDAO().save(fedExLabelLog);
				
			}
			i++;
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
		
	
	public static String createFedExShipLabel(Recipient recipient,ServiceType serviceType,SignatureOptionType signatureType){
		String labelLocation=null;
		String returnString=null;
		try{
			ProcessShipmentRequest request = buildRequest(recipient,serviceType,signatureType);
			ShipServiceLocator service;
			ShipPortType port;
			Invoice invoice = null;
			PurchaseOrder purchaseOrder = null;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();			
		    //
			ProcessShipmentReply reply = port.processShipment(request); // This is the call to the ship web service passing in a request object and returning a reply object
			//
			if (isResponseOk(reply.getHighestSeverity())){
				labelLocation=writeServiceOutput(reply,recipient);
			}else{
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					returnString = reply.getNotifications()[0].getMessage();
				}
			}
			if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
				invoice = DAORegistry.getInvoiceDAO().get(recipient.getInvoiceId());
			}
			if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
				purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(recipient.getPurchaseOrderId());
			}
			
			int i=0;
			printNotifications(reply.getNotifications());
			for(Notification notification:reply.getNotifications()){
				if(i==0){
					/*if(notification.getCode().equalsIgnoreCase("8336"))//when Notification message gets "Service type not valid with commitment."
						recreateFedExShipLabel(recipient,ServiceType.FEDEX_2_DAY);
					}*/
					
					FedExLabelLog fedExLabelLog=new FedExLabelLog();					
					fedExLabelLog.setLabelCreationDateTime(new Date());
					if(labelLocation!=null){
						fedExLabelLog.setStatus("SUCCESS");
					    fedExLabelLog.setLabelFileName(labelLocation);
					}else{
						fedExLabelLog.setLabelFileName(null);
						fedExLabelLog.setStatus("FAILURE");
					}				
					if(invoice != null){
						fedExLabelLog.setInvoiceId(recipient.getInvoiceId());
						fedExLabelLog.setTrackingNo(invoice.getTrackingNo());
					}
					if(purchaseOrder != null){
						fedExLabelLog.setPurchaseOrderId(recipient.getPurchaseOrderId());
						fedExLabelLog.setTrackingNo(purchaseOrder.getTrackingNo());
					}
					fedExLabelLog.setNotificationCode(notification.getCode());
					fedExLabelLog.setNotificationMessage(notification.getMessage());
					fedExLabelLog.setLogType("CREATE");
					DAORegistry.getFedExLabelLogDAO().save(fedExLabelLog);
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnString;
	}
	
	public static String deleteShipment(Invoice invoice){
		String returnString = "";
		try {
			String userName =  SecurityContextHolder.getContext().getAuthentication().getName();
			DeleteShipmentRequest request = new DeleteShipmentRequest();
			request.setClientDetail(createClientDetail());
	        request.setWebAuthenticationDetail(createWebAuthenticationDetail());

	        
	        TransactionDetail transactionDetail = new TransactionDetail();
		    transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+invoice.getId()); // The client will get the same value back in the response
		    request.setTransactionDetail(transactionDetail);

	        VersionId versionId = new VersionId("ship", 13, 0, 0);
	        request.setVersion(versionId);
	        
	        TrackingId track = new TrackingId();
	        track.setTrackingIdType(TrackingIdType.EXPRESS);
	        track.setTrackingNumber(invoice.getTrackingNo());
	        request.setTrackingId(track);
	        request.setDeletionControl(DeletionControlType.DELETE_ONE_PACKAGE);
	        
	        ShipServiceLocator service;
			ShipPortType port;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();
			ShipmentReply reply = port.deleteShipment(request);
			
			FedExLabelLog fedExLabelLog=new FedExLabelLog();
			fedExLabelLog.setInvoiceId(invoice.getId());
			fedExLabelLog.setLabelCreationDateTime(new Date());
			fedExLabelLog.setLabelFileName(null);
			fedExLabelLog.setLogType("DELETE");
			fedExLabelLog.setTrackingNo(invoice.getTrackingNo());
			
			if(isResponseOk(reply.getHighestSeverity())){
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					fedExLabelLog.setNotificationCode(reply.getNotifications()[0].getCode());
					fedExLabelLog.setNotificationMessage(reply.getNotifications()[0].getMessage());
				}
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				String fileWithPath = property.getValue()+File.separator+invoice.getId()+"_"+invoice.getTrackingNo();
				File file = new File(fileWithPath+".pdf");
				if(file.exists()){
					file.delete();
				}
				
				invoice.setTrackingNo(null);
				invoice.setFedexLabelCreated(false);
				invoice.setLastUpdated(new Date());
				DAORegistry.getInvoiceDAO().update(invoice);
				fedExLabelLog.setStatus("SUCCESS");
				
				 InvoiceAudit audit = new InvoiceAudit(invoice);
				 audit.setAction(InvoiceAuditAction.FEDEX_LABEL);
				 audit.setNote("Fedex Label is deleted having tracking no :"+fedExLabelLog.getTrackingNo());
				 audit.setCreatedDate(new Date());
				 audit.setCreateBy(userName);
				 DAORegistry.getInvoiceAuditDAO().save(audit);
				returnString = "Fedex label is deleted successfully for selected invoice.";
			}else{
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					fedExLabelLog.setNotificationCode(reply.getNotifications()[0].getCode());
					fedExLabelLog.setNotificationMessage(reply.getNotifications()[0].getMessage());
					returnString = reply.getNotifications()[0].getMessage();
				}
				fedExLabelLog.setStatus("FAILURE");
			}
			DAORegistry.getFedExLabelLogDAO().save(fedExLabelLog);
		} catch (Exception e) {
			e.printStackTrace();
			returnString = "Something went wrong while deleting fedex label";
		}
		return returnString;
	}
	
	public static String deletePOShipment(PurchaseOrder purchaseOrder){
		String returnString = "";
		try {
			DeleteShipmentRequest request = new DeleteShipmentRequest();
			request.setClientDetail(createClientDetail());
	        request.setWebAuthenticationDetail(createWebAuthenticationDetail());

	        
	        TransactionDetail transactionDetail = new TransactionDetail();
		    transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+purchaseOrder.getId()); // The client will get the same value back in the response
		    request.setTransactionDetail(transactionDetail);

	        VersionId versionId = new VersionId("ship", 13, 0, 0);
	        request.setVersion(versionId);
	        
	        TrackingId track = new TrackingId();
	        track.setTrackingIdType(TrackingIdType.EXPRESS);
	        track.setTrackingNumber(purchaseOrder.getTrackingNo());
	        request.setTrackingId(track);
	        request.setDeletionControl(DeletionControlType.DELETE_ONE_PACKAGE);
	        
	        ShipServiceLocator service;
			ShipPortType port;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();
			ShipmentReply reply = port.deleteShipment(request);
			
			FedExLabelLog fedExLabelLog=new FedExLabelLog();
			fedExLabelLog.setPurchaseOrderId(purchaseOrder.getId());
			fedExLabelLog.setLabelCreationDateTime(new Date());
			fedExLabelLog.setLabelFileName(null);
			fedExLabelLog.setLogType("DELETE");
			fedExLabelLog.setTrackingNo(purchaseOrder.getTrackingNo());
			
			if(isResponseOk(reply.getHighestSeverity())){
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					fedExLabelLog.setNotificationCode(reply.getNotifications()[0].getCode());
					fedExLabelLog.setNotificationMessage(reply.getNotifications()[0].getMessage());
				}
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				String fileWithPath = property.getValue()+File.separator+purchaseOrder.getId()+"_"+purchaseOrder.getTrackingNo();
				File file = new File(fileWithPath+".pdf");
				if(file.exists()){
					file.delete();
				}
				
				purchaseOrder.setTrackingNo(null);
				purchaseOrder.setFedexLabelCreated(false);
				purchaseOrder.setLastUpdated(new Date());
				DAORegistry.getPurchaseOrderDAO().update(purchaseOrder);
				fedExLabelLog.setStatus("SUCCESS");
				returnString = "Fedex label is deleted successfully for selected PO.";
			}else{
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					fedExLabelLog.setNotificationCode(reply.getNotifications()[0].getCode());
					fedExLabelLog.setNotificationMessage(reply.getNotifications()[0].getMessage());
					returnString = reply.getNotifications()[0].getMessage();
				}
				fedExLabelLog.setStatus("FAILURE");
			}
			DAORegistry.getFedExLabelLogDAO().save(fedExLabelLog);
		} catch (Exception e) {
			e.printStackTrace();
			returnString = "Something went wrong while deleting fedex label";
		}
		return returnString;
	}
	
	
	private static ProcessShipmentRequest buildRequest(Recipient recipient,ServiceType serviceType,SignatureOptionType signatureType)
	{
		ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());

        TransactionDetail transactionDetail = new TransactionDetail();
        if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
        	transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+recipient.getInvoiceId()); // The client will get the same value back in the response
        }
        if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
        	transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+recipient.getPurchaseOrderId());
        }
	    request.setTransactionDetail(transactionDetail);

        VersionId versionId = new VersionId("ship", 13, 0, 0);
        request.setVersion(versionId);

        RequestedShipment requestedShipment = new RequestedShipment();
	    requestedShipment.setShipTimestamp(Calendar.getInstance()); // Ship date and time
	    requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP); // Dropoff Types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
	    if(serviceType.equals(ServiceType.GROUND_HOME_DELIVERY)){
	    	 requestedShipment.setPackagingType(PackagingType.YOUR_PACKAGING);
	    }else{
	    	 requestedShipment.setPackagingType(PackagingType.FEDEX_ENVELOPE);
	    }
	    // Packaging type FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	    requestedShipment.setShipper(addShipper());
	    requestedShipment.setServiceType(serviceType);
	    
	    
	    if(recipient.getCountryCode().equalsIgnoreCase("US")){
	    	/*if(recipient.getPostalCode().equalsIgnoreCase("99516"))
	    	    requestedShipment.setServiceType(ServiceType.FEDEX_2_DAY);
	    	else*/
	    		//requestedShipment.setServiceType(ServiceType.FEDEX_EXPRESS_SAVER);
	    }
	    else{
	    	
	    	//requestedShipment.setServiceType(ServiceType.INTERNATIONAL_PRIORITY);
	    	/*CustomsClearanceDetail customsClearanceDetail=new CustomsClearanceDetail();
		    customsClearanceDetail.setCustomsValue(new Money("USD",new BigDecimal(1.0)));*/
	    	requestedShipment.setCustomsClearanceDetail(addCustomsClearanceDetail());
	        requestedShipment.setEdtRequestType(EdtRequestType.ALL);
	    }
	    //reques
	    
	    //for(Recipient recipient:list)
	    requestedShipment.setRecipient(addRecipient(recipient));
	    requestedShipment.setShippingChargesPayment(addShippingChargesPayment());
	    //Example Shipment special service (Express COD).
	    //requestedShipment.setSpecialServicesRequested(addShipmentSpecialServicesRequested()); 
	    requestedShipment.setLabelSpecification(addLabelSpecification());
        //
	    RateRequestType rateRequestType[] = new RateRequestType[1];
	    rateRequestType[0] = RateRequestType.ACCOUNT; // Rate types requested LIST, MULTIWEIGHT, ...
	    requestedShipment.setRateRequestTypes(new RateRequestType[]{rateRequestType[0]});
	   
	    requestedShipment.setPackageCount(new NonNegativeInteger("1"));
	    //Adding Invoice number as Reference Number
	    requestedShipment.setRequestedPackageLineItems(new RequestedPackageLineItem[] {addRequestedPackageLineItem(recipient,signatureType)}); 
        //
	    request.setRequestedShipment(requestedShipment);
	    //
		return request;
	}
	
	private static void updateEndPoint(ShipServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setShipServicePortEndpointAddress(endPoint);
		}
	}
	
	private static boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
			return true;
		}
 		return false;
	}

	private static void printNotifications(Notification[] notifications) {
		System.out.println("Notifications:");
		if (notifications == null || notifications.length == 0) {
			System.out.println("  No notifications returned");
			
		}
		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
			System.out.print("  Notification no. " + i + ": ");
			if (n == null) {
				System.out.println("null");
				continue;
			} else {
				System.out.println("");
			}
			NotificationSeverityType nst = n.getSeverity();

			System.out.println("    Severity: " + (nst == null ? "null" : nst.getValue()));
			System.out.println("    Code: " + n.getCode());
			System.out.println("    Message: " + n.getMessage());
			System.out.println("    Source: " + n.getSource()); 
		}
	}

	private static String writeServiceOutput(ProcessShipmentReply reply,Recipient recipient) throws Exception
	{
		String labelLocation=null;
		try
		{
			System.out.println(reply.getTransactionDetail().getCustomerTransactionId());
			CompletedShipmentDetail csd = reply.getCompletedShipmentDetail(); 
			String masterTrackingNumber=printMasterTrackingNumber(csd);
			printShipmentOperationalDetails(csd.getOperationalDetail());
			printShipmentRating(csd.getShipmentRating());
			CompletedPackageDetail cpd[] = csd.getCompletedPackageDetails();
			labelLocation=printPackageDetails(cpd,recipient);
			saveShipmentDocumentsToFile(csd.getShipmentDocuments(), masterTrackingNumber,recipient);
			//  If Express COD shipment is requested, the COD return label is returned as an Associated Shipment.
			getAssociatedShipmentLabels(csd.getAssociatedShipments());
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			//
		}
		return labelLocation; 
	}
	
	private static ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");
        Property accProperty = DAORegistry.getPropertyDAO().get(Constants.FEDEX_ACCOUNT_NO);
        accountNumber = accProperty.getValue();
        //accountNumber = "510087666";  //test account number
        //accountNumber = "620808504";  //production
       
        Property meterProperty = DAORegistry.getPropertyDAO().get(Constants.FEDEX_METER_NO);
        meterNumber = meterProperty.getValue();
        //meterNumber = "100299128"; //test account meter number
        //meterNumber = "110508558";//For Production environment
        
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	private static WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key = System.getProperty("key");
        String password = System.getProperty("password");
        Property keyProperty = DAORegistry.getPropertyDAO().get(Constants.FEDEX_KEY);
        key = keyProperty.getValue();
        //key = "yKThsnMItUvla8Ff";  //test account key
        //key = "0pE6JNu1oi5Wg3Vu";  //production
        Property passProperty = DAORegistry.getPropertyDAO().get(Constants.FEDEX_PASSWORD);
        password = passProperty.getValue();
        //password = "FovvaxUGU0j1MPsJSiB9tGjZo";  //test account password
        //password = "t5qHvYRpFYUjtEWHMqbbZOIkV";  //production
        
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}
	
	private static Party addShipper(){
	    Party shipperParty = new Party(); // Sender information
	    Contact shipperContact = new Contact();
	    //shipperContact.setPersonName("Amit Raut");
	    shipperContact.setCompanyName("Reward the fan");
	    shipperContact.setPhoneNumber("800-601-6100");
	    Address shipperAddress = new Address();
	    shipperAddress.setStreetLines(new String[] {"10 Times Square","3rd Floor"});
	    shipperAddress.setCity("New York");
	    shipperAddress.setStateOrProvinceCode("NY");
	    shipperAddress.setPostalCode("10018");
	    shipperAddress.setCountryCode("US");	    
	    shipperParty.setContact(shipperContact);
	    shipperParty.setAddress(shipperAddress);
	    return shipperParty;
	}
	
	private static CustomsClearanceDetail addCustomsClearanceDetail(){
        CustomsClearanceDetail customs = new CustomsClearanceDetail (); // International details
        customs.setDutiesPayment(addDutiesPayment());
        customs.setCustomsValue(addMoney("USD", 1.00));
        customs.setDocumentContent(InternationalDocumentContentType.DOCUMENTS_ONLY); 
        
        //Following three lines commented for International shipping
        /*CustomsOptionDetail customsOptionDetail=new CustomsOptionDetail();
        customsOptionDetail.setType(CustomsOptionType.COURTESY_RETURN_LABEL);
        customs.setCustomsOptions(customsOptionDetail);*/
        
        // Set export detail - To be used for Shipments that fall under AES Compliance
        //ExportDetail exportDetail = new ExportDetail();
        //exportDetail.setExportComplianceStatement("AESX20091127123456");
        //intd.setExportDetail(exportDetail);
        //customs.setCommodities(new Commodity[] {addCommodity()});// Commodity details
        customs.setCommodities(new Commodity[] {addCommodity()});
        return customs;
	}

	private static Party addRecipient(Recipient recipient){
	    Party recipientParty = new Party(); // Recipient information
	    Contact recipientContact = new Contact();
	    //recipientContact.setPersonName("Marneen Zahavi");
	    recipientContact.setPersonName(recipient.getRecipientName());
	    recipientContact.setCompanyName(recipient.getCompanyName());
	    //recipientContact.setPhoneNumber("(213)379-6010");
	    recipientContact.setPhoneNumber(recipient.getPhoneNumber());
	    Address recipientAddress = new Address();
	    //recipientAddress.setStreetLines(new String[] {"424 south broadway","apt 705"});
	    if(recipient.getAddressStreet2()!=null)
	        recipientAddress.setStreetLines(new String[] {recipient.getAddressStreet1(),recipient.getAddressStreet2()});
	    else
	    	recipientAddress.setStreetLines(new String[] {recipient.getAddressStreet1(),""});
	    //recipientAddress.setCity("LOS ANGELES");
	    recipientAddress.setCity(recipient.getCity());
	    //recipientAddress.setStateOrProvinceCode("CA");
	    recipientAddress.setStateOrProvinceCode(recipient.getStateProvinceCode());
	    //recipientAddress.setPostalCode("90013");
	    //recipientAddress.setPostalCode(recipient.getPostalCode().replaceAll(" ", "").replaceAll("-", ""));
	    recipientAddress.setPostalCode(recipient.getPostalCode());
	    //System.out.println("Postal Code after replacing Spcl Chars from fedEx web services ======================================> "+recipientAddress.getPostalCode() );
	    //System.out.println("Postal Code after replacing Spcl Chars From Datbase ======================================> "+recipient.getPostalCode().replaceAll(" ", "").replaceAll("-", "") );
	    //recipientAddress.setCountryCode("US");
	    recipientAddress.setCountryCode(recipient.getCountryCode());
	    recipientAddress.setResidential(Boolean.valueOf(true));	    
	    recipientParty.setContact(recipientContact);
	    recipientParty.setAddress(recipientAddress);
	    return recipientParty;
	}
	
	
	private static Payment addShippingChargesPayment(){
	    Payment payment = new Payment(); // Payment information
	    payment.setPaymentType(PaymentType.SENDER);
	    Payor payor = new Payor();
	    Party responsibleParty = new Party();
	    responsibleParty.setAccountNumber(getPayorAccountNumber());
	    Address responsiblePartyAddress = new Address();
	    responsiblePartyAddress.setCountryCode("US");
	    responsibleParty.setAddress(responsiblePartyAddress);
	    responsibleParty.setContact(new Contact());
		payor.setResponsibleParty(responsibleParty);
	    payment.setPayor(payor);
	    return payment;
	}
	
	private static LabelSpecification addLabelSpecification(){
	    LabelSpecification labelSpecification = new LabelSpecification(); // Label specification	    
		labelSpecification.setImageType(ShippingDocumentImageType.PDF);// Image types PDF, PNG, DPL, ...	
	    //labelSpecification.setImageType(ShippingDocumentImageType.PNG);// Image types PDF, PNG, DPL, ...
	    labelSpecification.setLabelFormatType(LabelFormatType.COMMON2D); //LABEL_DATA_ONLY, COMMON2D
	    //labelSpecification.setLabelStockType(LabelStockType.value2); // STOCK_4X6.75_LEADING_DOC_TAB	    
	    //labelSpecification.setLabelPrintingOrientation(LabelPrintingOrientationType.TOP_EDGE_OF_TEXT_FIRST);
	    return labelSpecification;
	}
	
	private static RequestedPackageLineItem addRequestedPackageLineItem(Recipient recipient,SignatureOptionType signatureType){
		RequestedPackageLineItem requestedPackageLineItem = new RequestedPackageLineItem();
		requestedPackageLineItem.setSequenceNumber(new PositiveInteger("1"));
		requestedPackageLineItem.setGroupPackageCount(new PositiveInteger("1"));
		requestedPackageLineItem.setWeight(addPackageWeight(0.5, WeightUnits.LB));
		//Commented Dimensions for testing purpose
		//requestedPackageLineItem.setDimensions(addPackageDimensions(108, 5, 5, LinearUnits.IN));
		if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
			requestedPackageLineItem.setCustomerReferences(new CustomerReference[]{
					addCustomerReference(CustomerReferenceType.CUSTOMER_REFERENCE.getValue(), recipient.getInvoiceId().toString()),
					addCustomerReference(CustomerReferenceType.INVOICE_NUMBER.getValue(), recipient.getInvoiceId().toString()),
					addCustomerReference(CustomerReferenceType.P_O_NUMBER.getValue(), ""),
			});
		}
		if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
			requestedPackageLineItem.setCustomerReferences(new CustomerReference[]{
					addCustomerReference(CustomerReferenceType.CUSTOMER_REFERENCE.getValue(), recipient.getPurchaseOrderId().toString()),
					addCustomerReference(CustomerReferenceType.INVOICE_NUMBER.getValue(), ""),
					addCustomerReference(CustomerReferenceType.P_O_NUMBER.getValue(), recipient.getPurchaseOrderId().toString()),
			});
		}
		//Adding SignatureOptionType as "Indirect Signature required"
		SignatureOptionType signatureOptionType =signatureType;
		if(signatureOptionType==null){
			signatureOptionType = SignatureOptionType.NO_SIGNATURE_REQUIRED;
		}
		 
		SignatureOptionDetail signatureOptionDetail=new SignatureOptionDetail();
		signatureOptionDetail.setOptionType(signatureOptionType);
		PackageSpecialServicesRequested specialServicesRequested=new PackageSpecialServicesRequested();
		
		//Adding SpecialServiceTypes as "SIGNATURE_OPTION"
		PackageSpecialServiceType packageSpecialServiceType = new PackageSpecialServiceType(PackageSpecialServiceType._SIGNATURE_OPTION);
		PackageSpecialServiceType[] packageSpecialServiceTypes = new PackageSpecialServiceType[1];
		packageSpecialServiceTypes[0] = packageSpecialServiceType;
		specialServicesRequested.setSpecialServiceTypes(packageSpecialServiceTypes);
		
		
		specialServicesRequested.setSignatureOptionDetail(signatureOptionDetail);
		requestedPackageLineItem.setSpecialServicesRequested(specialServicesRequested);
		return requestedPackageLineItem;
	}
	
	private static String printMasterTrackingNumber(CompletedShipmentDetail csd){
		String trackingNumber="";
		if(null != csd.getMasterTrackingId()){
			trackingNumber = csd.getMasterTrackingId().getTrackingNumber();
			System.out.println("Master Tracking Number");
			System.out.println("  Type: "
					+ csd.getMasterTrackingId().getTrackingIdType());
			System.out.println("  Tracking Number: " 
					+ trackingNumber);
		}
		return trackingNumber;
	}
	
	private static void printShipmentOperationalDetails(ShipmentOperationalDetail shipmentOperationalDetail){
		if(shipmentOperationalDetail!=null){
			System.out.println("Routing Details");
			printString(shipmentOperationalDetail.getUrsaPrefixCode(), "URSA Prefix", "  ");
			if(shipmentOperationalDetail.getCommitDay()!=null)
				printString(shipmentOperationalDetail.getCommitDay().getValue(), "Service Commitment", "  ");
			printString(shipmentOperationalDetail.getAirportId(), "Airport Id", "  ");
			if(shipmentOperationalDetail.getDeliveryDay()!=null)
				printString(shipmentOperationalDetail.getDeliveryDay().getValue(), "Delivery Day", "  ");
			System.out.println();
		}
	}
	
	private static void printShipmentRating(ShipmentRating shipmentRating){
		if(shipmentRating!=null){
			System.out.println("Shipment Rate Details");
			ShipmentRateDetail[] srd = shipmentRating.getShipmentRateDetails();
			for(int j=0; j < srd.length; j++)
			{
				System.out.println("  Rate Type: " + srd[j].getRateType().getValue());
				printWeight(srd[j].getTotalBillingWeight(), "Shipment Billing Weight", "    ");
				printMoney(srd[j].getTotalBaseCharge(), "Shipment Base Charge", "    ");
				printMoney(srd[j].getTotalNetCharge(), "Shipment Net Charge", "    ");
				printMoney(srd[j].getTotalSurcharges(), "Shipment Total Surcharge", "    ");
				if (null != srd[j].getSurcharges())
				{
					System.out.println("    Surcharge Details");
					Surcharge[] s = srd[j].getSurcharges();
					for(int k=0; k < s.length; k++)
					{
						printMoney(s[k].getAmount(),s[k].getSurchargeType().getValue(), "      ");
					}
				}
				printFreightDetail(srd[j].getFreightRateDetail());
				System.out.println();
			}
		}
	}
	
	private static String printPackageDetails(CompletedPackageDetail[] cpd,Recipient recipient) throws Exception{
		String labelFilePath=null;
		if(cpd!=null){
			System.out.println("Package Details");
			for (int i=0; i < cpd.length; i++) { // Package details / Rating information for each package
				String trackingNumber = cpd[i].getTrackingIds()[0].getTrackingNumber();
				printTrackingNumbers(cpd[i]);
				System.out.println();
				//
				printPackageRating(cpd[i].getPackageRating());
				//	Write label buffer to file
				ShippingDocument sd = cpd[i].getLabel();
				labelFilePath=saveLabelToFile(sd, trackingNumber,recipient);
				printPackageOperationalDetails(cpd[i].getOperationalDetail());
				
				Invoice invoice = null;
				if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
					invoice = DAORegistry.getInvoiceDAO().get(recipient.getInvoiceId());
					if(invoice!=null){
						invoice.setTrackingNo(trackingNumber);
						invoice.setFedexLabelCreated(true);
						DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
					}
				}
				PurchaseOrder purchaseOrder = null;
				if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
					purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(recipient.getPurchaseOrderId());
					if(purchaseOrder != null){
						purchaseOrder.setTrackingNo(trackingNumber);
						purchaseOrder.setFedexLabelCreated(true);
						DAORegistry.getPurchaseOrderDAO().saveOrUpdate(purchaseOrder);
					}
				}
				System.out.println();
			}
		}
		return labelFilePath;
	}
	
	private static void printPackageRating(PackageRating packageRating){
		if(packageRating!=null){
			System.out.println("Package Rate Details");
			PackageRateDetail[] prd = packageRating.getPackageRateDetails();
			for(int j=0; j < prd.length; j++)
			{
				System.out.println("  Rate Type: " + prd[j].getRateType().getValue());
				printWeight(prd[j].getBillingWeight(), "Billing Weight", "    ");
				printMoney(prd[j].getBaseCharge(), "Base Charge", "    ");
				printMoney(prd[j].getNetCharge(), "Net Charge", "    ");
				printMoney(prd[j].getTotalSurcharges(), "Total Surcharge", "    ");
				if (null != prd[j].getSurcharges())
				{
					System.out.println("    Surcharge Details");
					Surcharge[] s = prd[j].getSurcharges();
					for(int k=0; k < s.length; k++)
					{
						printMoney(s[k].getAmount(),s[k].getSurchargeType().getValue(), "      ");
					}
				}
				System.out.println();
			}
		}
	}
	
	private static void printTrackingNumbers(CompletedPackageDetail completedPackageDetail){
		if(completedPackageDetail.getTrackingIds()!=null){
			TrackingId[] trackingId = completedPackageDetail.getTrackingIds();
			for(int i=0; i< trackingId.length; i++){
				String trackNumber = trackingId[i].getTrackingNumber();
				String trackType = trackingId[i].getTrackingIdType().getValue();
				String formId = trackingId[i].getFormId();
				//printString(trackNumber, trackType + " tracking number", "  ");
				//printString(formId, "Form Id", "  ");
			}
		}
	}
	
    
	private static String getPayorAccountNumber() {
		// See if payor account number is set as system property,
		// if not default it to "XXX"
		String payorAccountNumber = System.getProperty("Payor.AccountNumber");
		Property accProperty = DAORegistry.getPropertyDAO().get(Constants.FEDEX_ACCOUNT_NO);
		payorAccountNumber = accProperty.getValue();
		//payorAccountNumber ="510087305"; //testing
		//payorAccountNumber = "620808504"; //production
		return payorAccountNumber;
	}
	
	private static void printString(String value, String description, String space){
		if(value!=null){
			System.out.println(space + description + ": " + value);
		}
	}
	
	private static void saveShipmentDocumentsToFile(ShippingDocument[] shippingDocument, String trackingNumber,Recipient recipient) throws Exception{
		//String shippingDocumentLabelFileName=null;
		if(shippingDocument!= null){
			for(int i=0; i < shippingDocument.length; i++){
				ShippingDocumentPart[] sdparts = shippingDocument[i].getParts();
				for (int a=0; a < sdparts.length; a++) {
					ShippingDocumentPart sdpart = sdparts[a];
					String labelLocation = System.getProperty("file.label.location");
					if(labelLocation == null) {
						Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
						labelLocation = property.getValue();
					}
					String labelName = shippingDocument[i].getType().getValue();
					String shippingDocumentLabelFileName = null;
					//String shippingDocumentLabelFileName =  new String(labelLocation + labelName + "." + trackingNumber + "_" + a + ".pdf");	
					//String shippingDocumentLabelFileName =  new String(labelLocation + "937829" + "_" + trackingNumber + "_" + a + ".pdf");
					if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
						shippingDocumentLabelFileName =  new String(labelLocation +recipient.getInvoiceId()+ "_" + trackingNumber + ".pdf");
					}
					if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
						shippingDocumentLabelFileName =  new String(labelLocation +recipient.getPurchaseOrderId()+ "_" + trackingNumber + ".pdf");
					}
					//String shippingDocumentLabelFileName =  new String(labelLocation + labelName + "." + trackingNumber + "_" + a + ".png");
					if(shippingDocumentLabelFileName != null && !shippingDocumentLabelFileName.isEmpty()){
						File shippingDocumentLabelFile = new File(shippingDocumentLabelFileName);
						FileOutputStream fos = new FileOutputStream( shippingDocumentLabelFile );
						fos.write(sdpart.getImage());
						fos.close();
						System.out.println("\nAssociated shipment label file name " + shippingDocumentLabelFile.getAbsolutePath());
					}
					//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + shippingDocumentLabelFile.getAbsolutePath());
				}
			}
		}
		//return shippingDocumentLabelFileName;
	}
	
	private static void getAssociatedShipmentLabels(AssociatedShipmentDetail[] associatedShipmentDetail) throws Exception{
		if(associatedShipmentDetail!=null){
			for (int j=0; j < associatedShipmentDetail.length; j++){
				if(associatedShipmentDetail[j].getLabel()!=null && associatedShipmentDetail[j].getType()!=null){
					String trackingNumber = associatedShipmentDetail[j].getTrackingId().getTrackingNumber();
					String associatedShipmentType = associatedShipmentDetail[j].getType().getValue();
					ShippingDocument associatedShipmentLabel = associatedShipmentDetail[j].getLabel();
					saveAssociatedShipmentLabelToFile(associatedShipmentLabel, trackingNumber, associatedShipmentType);
				}
			}
		}
	}
	
	private static Payment addDutiesPayment(){
	    Payment payment = new Payment(); // Payment information
	    payment.setPaymentType(PaymentType.SENDER);
	    Payor payor = new Payor();
	    Party responsibleParty = new Party();
	    responsibleParty.setAccountNumber(getPayorAccountNumber());
	    Address responsiblePartyAddress = new Address();
	    responsiblePartyAddress.setCountryCode("US");
	    responsibleParty.setAddress(responsiblePartyAddress);
	    responsibleParty.setContact(new Contact());
		payor.setResponsibleParty(responsibleParty);
	    payment.setPayor(payor);
	    return payment;
	}
	private static Money addMoney(String currency, Double value){
		Money money = new Money();
		money.setCurrency(currency);
		money.setAmount(new BigDecimal(value));
		return money;
	}
	private static Commodity addCommodity(){
		Commodity commodity = new Commodity();
		commodity.setNumberOfPieces(new NonNegativeInteger("1"));
		commodity.setDescription("Accounting Documents");
		commodity.setCountryOfManufacture("US");
		commodity.setWeight(new Weight());
		commodity.getWeight().setValue(new BigDecimal(0.5));
		commodity.getWeight().setUnits(WeightUnits.LB);
		commodity.setQuantity(new NonNegativeInteger("1"));
		commodity.setQuantityUnits("EA");//Dont know the value 
		commodity.setUnitPrice(new Money());
		commodity.getUnitPrice().setAmount(new java.math.BigDecimal(1.0));
		commodity.getUnitPrice().setCurrency("USD");
		commodity.setCustomsValue(new Money());
		commodity.getCustomsValue().setAmount(new java.math.BigDecimal(1.0));
		commodity.getCustomsValue().setCurrency("USD");
		commodity.setCountryOfManufacture("US");
		//commodity.setHarmonizedCode("491199005000");//Dont know the value 
		
        return commodity;
	}
	
	private static Weight addPackageWeight(Double packageWeight, WeightUnits weightUnits){
		Weight weight = new Weight();
		weight.setUnits(weightUnits);
		weight.setValue(new BigDecimal(packageWeight));
		return weight;
	}
	
/*	private static Dimensions addPackageDimensions(Integer length, Integer height, Integer width, LinearUnits linearUnits){ 
		Dimensions dimensions = new Dimensions();
		dimensions.setLength(new NonNegativeInteger(length.toString()));
		dimensions.setHeight(new NonNegativeInteger(height.toString()));
		dimensions.setWidth(new NonNegativeInteger(width.toString()));
		dimensions.setUnits(linearUnits);
		return dimensions;
	}*/

	private static CustomerReference addCustomerReference(String customerReferenceType, String customerReferenceValue){
		CustomerReference customerReference = new CustomerReference();
		customerReference.setCustomerReferenceType(CustomerReferenceType.fromString(customerReferenceType));
		customerReference.setValue(customerReferenceValue);
		return customerReference;
	}
	
	private static void printMoney(Money money, String description, String space){
		if(money!=null){
			System.out.println(space + description + ": " + money.getAmount() + " " + money.getCurrency());
		}
	}
	private static void printWeight(Weight weight, String description, String space){
		if(weight!=null){
			System.out.println(space + description + ": " + weight.getValue() + " " + weight.getUnits());
		}
	}
	
	private static void printFreightDetail(FreightRateDetail freightRateDetail){
		if(freightRateDetail!=null){
			System.out.println("  Freight Details");
			printFreightNotations(freightRateDetail);
			printFreightBaseCharges(freightRateDetail);
			
		}
	}
	
	private static void printFreightNotations(FreightRateDetail frd){
		if(null != frd.getNotations()){
			System.out.println("    Notations");
			FreightRateNotation notations[] = frd.getNotations();
			for(int n=0; n< notations.length; n++){
				printString(notations[n].getCode(), "Code", "      ");
				printString(notations[n].getDescription(), "Notification", "      ");
			}
		}
	}
	
	private static String saveLabelToFile(ShippingDocument shippingDocument, String trackingNumber,Recipient recipient) throws Exception {
		String labelFilePath=null;
		ShippingDocumentPart[] sdparts = shippingDocument.getParts();
		for (int a=0; a < sdparts.length; a++) {
			ShippingDocumentPart sdpart = sdparts[a];
			String labelLocation = System.getProperty("file.label.location");
			if (labelLocation == null) {
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				labelLocation = property.getValue();
			}
			String shippingDocumentType = shippingDocument.getType().getValue();
			String labelFileName = null;
			//String labelFileName =  new String(labelLocation + shippingDocumentType + "." + trackingNumber + "_" + a + ".pdf");
			if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
				labelFileName =  new String(labelLocation +recipient.getInvoiceId()+ "_" + trackingNumber + ".pdf");
			}
			if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
				labelFileName =  new String(labelLocation +recipient.getPurchaseOrderId()+ "_" + trackingNumber + ".pdf");
			}
			//CustomerReferenceType.CUSTOMER_REFERENCE.getValue()
			//String labelFileName =  new String(labelLocation + shippingDocumentType + "." + trackingNumber + "_" + a + ".png");
			if(labelFileName != null && !labelFileName.isEmpty()){
				File labelFile = new File(labelFileName);
				FileOutputStream fos = new FileOutputStream( labelFile );
				fos.write(sdpart.getImage());
				fos.close();
				System.out.println("\nlabel file name " + labelFile.getAbsolutePath());
				if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
					labelFilePath = recipient.getInvoiceId()+ "_" + trackingNumber + ".pdf";
				}
				if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
					labelFilePath = recipient.getPurchaseOrderId()+ "_" + trackingNumber + ".pdf";
				}
			}
			//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + labelFile.getAbsolutePath());
		}
		return labelFilePath;
	}
	
	private static void printPackageOperationalDetails(PackageOperationalDetail packageOperationalDetail){
		if(packageOperationalDetail!=null){
			System.out.println("  Routing Details");
			printString(packageOperationalDetail.getAstraHandlingText(), "Astra", "    ");
			printString(packageOperationalDetail.getGroundServiceCode(), "Ground Service Code", "    ");
			System.out.println();
		}
	}
	
	private static void saveAssociatedShipmentLabelToFile(ShippingDocument shippingDocument, String trackingNumber, String labelName) throws Exception {
		ShippingDocumentPart[] sdparts = shippingDocument.getParts();
		for (int a=0; a < sdparts.length; a++) {
			ShippingDocumentPart sdpart = sdparts[a];
			String labelLocation = System.getProperty("file.label.location");
			if (labelLocation == null) {
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_DIRECTORY);
				labelLocation = property.getValue();
			}
			//String associatedShipmentLabelFileName =  new String(labelLocation + labelName + "." + trackingNumber + "_" + a + ".pdf");	
			//String associatedShipmentLabelFileName =  new String(labelLocation + "InvoiceId" + "_" + trackingNumber + "_" + a + ".pdf");
			String associatedShipmentLabelFileName =  new String(labelLocation + labelName + "_" + trackingNumber + "_" + a + ".pdf");
			//String shippingDocumentLabelFileName =  new String(labelLocation +recipient.getInvoiceId()+ "_" + trackingNumber + ".pdf");
			//String associatedShipmentLabelFileName =  new String(labelLocation + labelName + "." + trackingNumber + "_" + a + ".png");
			File associatedShipmentLabelFile = new File(associatedShipmentLabelFileName);
			FileOutputStream fos = new FileOutputStream( associatedShipmentLabelFile );
			fos.write(sdpart.getImage());
			fos.close();
			System.out.println("\nAssociated shipment label file name " + associatedShipmentLabelFile.getAbsolutePath());
			//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + associatedShipmentLabelFile.getAbsolutePath());
		}
	}
	
	private static void printFreightBaseCharges(FreightRateDetail frd){
		if(null != frd.getBaseCharges()){
			FreightBaseCharge baseCharges[] = frd.getBaseCharges();
			for(int i=0; i < baseCharges.length; i++){
				System.out.println("    Freight Rate Details");
				printString(baseCharges[i].getDescription(), "Description", "      ");
				printString(baseCharges[i].getFreightClass().getValue(), "Freight Class", "      ");
				printString(baseCharges[i].getRatedAsClass().getValue(), "Rated Class", "      ");
				printWeight(baseCharges[i].getWeight(), "Weight", "      ");
				printString(baseCharges[i].getChargeBasis().getValue(), "Charge Basis", "      ");
				printMoney(baseCharges[i].getChargeRate(), "Charge Rate", "      ");
				printMoney(baseCharges[i].getExtendedAmount(), "Extended Amount", "      ");
				printString(baseCharges[i].getNmfcCode(), "NMFC Code", "      ");
			}
		}
	}

	
	//Create Manual Fedex Label
	public static String createManualFedExShipLabel(ManualFedexGeneration manualFedex,Recipient recipient,ServiceType serviceType,SignatureOptionType signatureType){
		String labelLocation=null;
		String returnString=null;
		try{
			ProcessShipmentRequest request = buildManualFedexRequest(manualFedex,recipient,serviceType,signatureType);
			ShipServiceLocator service;
			ShipPortType port;
			String userName =  SecurityContextHolder.getContext().getAuthentication().getName();
			String shippingDocumentLabelFileName = null;
			String associatedShipmentLabelFileName = null;
			String trackingNumber = null;
			//Invoice invoice = null;
			//PurchaseOrder purchaseOrder = null;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();			
		    //
			ProcessShipmentReply reply = port.processShipment(request); // This is the call to the ship web service passing in a request object and returning a reply object
			//
			if (isResponseOk(reply.getHighestSeverity())){
				
				CompletedShipmentDetail csd = reply.getCompletedShipmentDetail();
				String masterTrackingNumber=printMasterTrackingNumber(csd);
				printShipmentOperationalDetails(csd.getOperationalDetail());
				printShipmentRating(csd.getShipmentRating());
				
				CompletedPackageDetail cpd[] = csd.getCompletedPackageDetails();
				if(cpd!=null){
					System.out.println("Package Details");
					for (int i=0; i < cpd.length; i++) { // Package details / Rating information for each package
						trackingNumber = cpd[i].getTrackingIds()[0].getTrackingNumber();
						printTrackingNumbers(cpd[i]);
						System.out.println();
						//
						printPackageRating(cpd[i].getPackageRating());
						//	Write label buffer to file
						ShippingDocument sd = cpd[i].getLabel();
						//labelLocation=saveLabelToFile(sd, trackingNumber,recipient);
						
						ShippingDocumentPart[] sdparts = sd.getParts();
						for (int a=0; a < sdparts.length; a++) {
							ShippingDocumentPart sdpart = sdparts[a];
							labelLocation = System.getProperty("file.label.location");
							if (labelLocation == null) {
								Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
								labelLocation = property.getValue();
							}
							String shippingDocumentType = sd.getType().getValue();
							String labelFileName = null;
							//String labelFileName =  new String(labelLocation + shippingDocumentType + "." + trackingNumber + "_" + a + ".pdf");
							if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
								labelFileName =  new String(labelLocation +recipient.getInvoiceId()+ "_" + trackingNumber + ".pdf");
							}
							/*if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
								labelFileName =  new String(labelLocation +recipient.getPurchaseOrderId()+ "_" + trackingNumber + ".pdf");
							}*/
							//CustomerReferenceType.CUSTOMER_REFERENCE.getValue()
							//String labelFileName =  new String(labelLocation + shippingDocumentType + "." + trackingNumber + "_" + a + ".png");
							if(labelFileName != null && !labelFileName.isEmpty()){
								File labelFile = new File(labelFileName);
								FileOutputStream fos = new FileOutputStream( labelFile );
								fos.write(sdpart.getImage());
								fos.close();
								System.out.println("\nlabel file name " + labelFile.getAbsolutePath());
								if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
									labelLocation = recipient.getInvoiceId()+ "_" + trackingNumber + ".pdf";
								}
								/*if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
									labelFilePath = recipient.getPurchaseOrderId()+ "_" + trackingNumber + ".pdf";
								}*/
							}
							//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + labelFile.getAbsolutePath());
						}
						
						printPackageOperationalDetails(cpd[i].getOperationalDetail());
						
						/*Invoice invoice = null;
						if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
							invoice = DAORegistry.getInvoiceDAO().get(recipient.getInvoiceId());
							if(invoice!=null){
								invoice.setTrackingNo(trackingNumber);
								invoice.setFedexLabelCreated(true);
								DAORegistry.getInvoiceDAO().saveOrUpdate(invoice);
							}
						}
						PurchaseOrder purchaseOrder = null;
						if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
							purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(recipient.getPurchaseOrderId());
							if(purchaseOrder != null){
								purchaseOrder.setTrackingNo(trackingNumber);
								purchaseOrder.setFedexLabelCreated(true);
								DAORegistry.getPurchaseOrderDAO().saveOrUpdate(purchaseOrder);
							}
						}
						System.out.println();*/
					}
				}
				
				ShippingDocument[] shippingDocument = csd.getShipmentDocuments();
				if(shippingDocument!= null){
					for(int i=0; i < shippingDocument.length; i++){
						ShippingDocumentPart[] sdparts = shippingDocument[i].getParts();
						for (int a=0; a < sdparts.length; a++) {
							ShippingDocumentPart sdpart = sdparts[a];
							labelLocation = System.getProperty("file.label.location");
							if(labelLocation == null) {
								Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
								labelLocation = property.getValue();
							}
							String labelName = shippingDocument[i].getType().getValue();
							
							if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
								shippingDocumentLabelFileName =  new String(labelLocation +recipient.getInvoiceId()+ "_" + csd.getMasterTrackingId().getTrackingNumber() + ".pdf");
							}
							if(shippingDocumentLabelFileName != null && !shippingDocumentLabelFileName.isEmpty()){
								File shippingDocumentLabelFile = new File(shippingDocumentLabelFileName);
								FileOutputStream fos = new FileOutputStream( shippingDocumentLabelFile );
								fos.write(sdpart.getImage());
								fos.close();
								System.out.println("\nAssociated shipment label file name " + shippingDocumentLabelFile.getAbsolutePath());
							}
							//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + shippingDocumentLabelFile.getAbsolutePath());
						}
					}
				}
				AssociatedShipmentDetail[] associatedShipmentDetail = csd.getAssociatedShipments();
				if(associatedShipmentDetail!=null){
					for (int j=0; j < associatedShipmentDetail.length; j++){
						if(associatedShipmentDetail[j].getLabel()!=null && associatedShipmentDetail[j].getType()!=null){
							trackingNumber = associatedShipmentDetail[j].getTrackingId().getTrackingNumber();
							String associatedShipmentType = associatedShipmentDetail[j].getType().getValue();
							ShippingDocument associatedShipmentLabel = associatedShipmentDetail[j].getLabel();
							//saveAssociatedShipmentLabelToFile(associatedShipmentLabel, trackingNumber, associatedShipmentType);
							ShippingDocumentPart[] sdparts = associatedShipmentLabel.getParts();
							for (int a=0; a < sdparts.length; a++) {
								ShippingDocumentPart sdpart = sdparts[a];
								labelLocation = System.getProperty("file.label.location");
								if (labelLocation == null) {
									Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
									labelLocation = property.getValue();
								}
								associatedShipmentLabelFileName =  new String(labelLocation + associatedShipmentType + "_" + trackingNumber + "_" + a + ".pdf");
								File associatedShipmentLabelFile = new File(associatedShipmentLabelFileName);
								FileOutputStream fos = new FileOutputStream( associatedShipmentLabelFile );
								fos.write(sdpart.getImage());
								fos.close();
								System.out.println("\nAssociated shipment label file name " + associatedShipmentLabelFile.getAbsolutePath());
								//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + associatedShipmentLabelFile.getAbsolutePath());
							}
						}
					}
				}		
								
				//labelLocation=writeServiceOutput(reply,recipient);
				//Save POJO - ManualFedexGeneration
				if(labelLocation != null && !labelLocation.isEmpty()){
					if(csd.getMasterTrackingId() != null && csd.getMasterTrackingId().getTrackingNumber() != null)
					{
						manualFedex.setTrackingNumber(csd.getMasterTrackingId().getTrackingNumber());
					}else{
						manualFedex.setTrackingNumber(trackingNumber);
					}
					manualFedex.setFedexLabelPath(labelLocation);
					manualFedex.setFedexLabelCreated(true);
					manualFedex.setCreatedDate(new Date());
					manualFedex.setCreatedBy(userName);
					DAORegistry.getManualFedexGenerationDAO().save(manualFedex);
				}
			}else{
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					returnString = reply.getNotifications()[0].getMessage();
				}
			}
			/*if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
				invoice = DAORegistry.getInvoiceDAO().get(recipient.getInvoiceId());
			}
			if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
				purchaseOrder = DAORegistry.getPurchaseOrderDAO().get(recipient.getPurchaseOrderId());
			}*/
			
			int i=0;
			printNotifications(reply.getNotifications());
			for(Notification notification:reply.getNotifications()){
				if(i==0){
					/*if(notification.getCode().equalsIgnoreCase("8336"))//when Notification message gets "Service type not valid with commitment."
						recreateFedExShipLabel(recipient,ServiceType.FEDEX_2_DAY);
					}*/
					
					FedExLabelLog fedExLabelLog=new FedExLabelLog();					
					fedExLabelLog.setLabelCreationDateTime(new Date());
					if(labelLocation!=null){
						fedExLabelLog.setStatus("SUCCESS");
					    fedExLabelLog.setLabelFileName(labelLocation);
					}else{
						fedExLabelLog.setLabelFileName(null);
						fedExLabelLog.setStatus("FAILURE");
					}				
					/*if(invoice != null){
						fedExLabelLog.setInvoiceId(recipient.getInvoiceId());
						fedExLabelLog.setTrackingNo(invoice.getTrackingNo());
					}
					if(purchaseOrder != null){
						fedExLabelLog.setPurchaseOrderId(recipient.getPurchaseOrderId());
						fedExLabelLog.setTrackingNo(purchaseOrder.getTrackingNo());
					}*/
					fedExLabelLog.setInvoiceId(recipient.getInvoiceId());
					fedExLabelLog.setTrackingNo(manualFedex.getTrackingNumber());
					fedExLabelLog.setNotificationCode(notification.getCode());
					fedExLabelLog.setNotificationMessage(notification.getMessage());
					fedExLabelLog.setLogType("CREATE");
					DAORegistry.getFedExLabelLogDAO().save(fedExLabelLog);
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnString;
	}
	
	private static ProcessShipmentRequest buildManualFedexRequest(ManualFedexGeneration manualFedex,Recipient recipient,ServiceType serviceType,SignatureOptionType signatureType)
	{
		ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());

        TransactionDetail transactionDetail = new TransactionDetail();
        if(recipient.getInvoiceId() != null && recipient.getInvoiceId() > 0){
        	transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+recipient.getInvoiceId()); // The client will get the same value back in the response
        }
        /*
        if(recipient.getShippingId() != null && recipient.getPurchaseOrderId() != null){
        	transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+recipient.getPurchaseOrderId());
        }*/
	    request.setTransactionDetail(transactionDetail);

        VersionId versionId = new VersionId("ship", 13, 0, 0);
        request.setVersion(versionId);

        RequestedShipment requestedShipment = new RequestedShipment();
	    requestedShipment.setShipTimestamp(Calendar.getInstance()); // Ship date and time
	    requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP); // Dropoff Types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
	    if(serviceType.equals(ServiceType.GROUND_HOME_DELIVERY)){
	    	 requestedShipment.setPackagingType(PackagingType.YOUR_PACKAGING);
	    }else{
	    	 requestedShipment.setPackagingType(PackagingType.FEDEX_ENVELOPE);
	    }
	    // Packaging type FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	    requestedShipment.setShipper(addManualFedexShipper(manualFedex));
	    requestedShipment.setServiceType(serviceType);
	    
	    
	    if(recipient.getCountryCode().equalsIgnoreCase("US")){
	    	/*if(recipient.getPostalCode().equalsIgnoreCase("99516"))
	    	    requestedShipment.setServiceType(ServiceType.FEDEX_2_DAY);
	    	else*/
	    		//requestedShipment.setServiceType(ServiceType.FEDEX_EXPRESS_SAVER);
	    }
	    else{
	    	
	    	//requestedShipment.setServiceType(ServiceType.INTERNATIONAL_PRIORITY);
	    	/*CustomsClearanceDetail customsClearanceDetail=new CustomsClearanceDetail();
		    customsClearanceDetail.setCustomsValue(new Money("USD",new BigDecimal(1.0)));*/
	    	requestedShipment.setCustomsClearanceDetail(addCustomsClearanceDetail());
	        requestedShipment.setEdtRequestType(EdtRequestType.ALL);
	    }
	    //reques
	    
	    //for(Recipient recipient:list)
	    requestedShipment.setRecipient(addRecipient(recipient));
	    requestedShipment.setShippingChargesPayment(addShippingChargesPayment());
	    //Example Shipment special service (Express COD).
	    //requestedShipment.setSpecialServicesRequested(addShipmentSpecialServicesRequested()); 
	    requestedShipment.setLabelSpecification(addLabelSpecification());
        //
	    RateRequestType rateRequestType[] = new RateRequestType[1];
	    rateRequestType[0] = RateRequestType.ACCOUNT; // Rate types requested LIST, MULTIWEIGHT, ...
	    requestedShipment.setRateRequestTypes(new RateRequestType[]{rateRequestType[0]});
	   
	    requestedShipment.setPackageCount(new NonNegativeInteger("1"));
	    //Adding Invoice number as Reference Number
	    requestedShipment.setRequestedPackageLineItems(new RequestedPackageLineItem[] {addRequestedPackageLineItem(recipient,signatureType)}); 
        //
	    request.setRequestedShipment(requestedShipment);
	    //
		return request;
	}	
	
	private static Party addManualFedexShipper(ManualFedexGeneration manualFedex){
	    Party shipperParty = new Party(); // Sender information
	    Contact shipperContact = new Contact();
	    shipperContact.setPersonName(manualFedex.getBlFirstName()+" "+manualFedex.getBlLastName());
	    shipperContact.setCompanyName(manualFedex.getBlCompanyName());
	    shipperContact.setPhoneNumber(manualFedex.getBlPhone());
	    Address shipperAddress = new Address();
	    shipperAddress.setStreetLines(new String[] {manualFedex.getBlAddress1(),manualFedex.getBlAddress2()});
	    shipperAddress.setCity(manualFedex.getBlCity());
	    State state = DAORegistry.getStateDAO().get(manualFedex.getBlState());
	    shipperAddress.setStateOrProvinceCode(state.getShortDesc());
	    shipperAddress.setPostalCode(manualFedex.getBlZipCode());
	    Country country = DAORegistry.getCountryDAO().get(manualFedex.getBlCountry());
	    shipperAddress.setCountryCode(country.getDesc());	    
	    shipperParty.setContact(shipperContact);
	    shipperParty.setAddress(shipperAddress);
	    return shipperParty;
	}
	
	public static String deleteManualFedexShipment(ManualFedexGeneration manualFedex){
		String returnString = "";
		try {
			String userName =  SecurityContextHolder.getContext().getAuthentication().getName();
			DeleteShipmentRequest request = new DeleteShipmentRequest();
			request.setClientDetail(createClientDetail());
	        request.setWebAuthenticationDetail(createWebAuthenticationDetail());

	        
	        TransactionDetail transactionDetail = new TransactionDetail();
		    transactionDetail.setCustomerTransactionId("Reward the fan  - Ship Request .. Referance Number is "+manualFedex.getId()); // The client will get the same value back in the response
		    request.setTransactionDetail(transactionDetail);

	        VersionId versionId = new VersionId("ship", 13, 0, 0);
	        request.setVersion(versionId);
	        
	        TrackingId track = new TrackingId();
	        track.setTrackingIdType(TrackingIdType.EXPRESS);
	        track.setTrackingNumber(manualFedex.getTrackingNumber());
	        request.setTrackingId(track);
	        request.setDeletionControl(DeletionControlType.DELETE_ONE_PACKAGE);
	        
	        ShipServiceLocator service;
			ShipPortType port;
			//
			service = new ShipServiceLocator();
			updateEndPoint(service);
			port = service.getShipServicePort();
			ShipmentReply reply = port.deleteShipment(request);
			
			FedExLabelLog fedExLabelLog=new FedExLabelLog();
			fedExLabelLog.setInvoiceId(1);
			fedExLabelLog.setLabelCreationDateTime(new Date());
			fedExLabelLog.setLabelFileName(null);
			fedExLabelLog.setLogType("DELETE");
			fedExLabelLog.setTrackingNo(manualFedex.getTrackingNumber());
			
			if(isResponseOk(reply.getHighestSeverity())){
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					fedExLabelLog.setNotificationCode(reply.getNotifications()[0].getCode());
					fedExLabelLog.setNotificationMessage(reply.getNotifications()[0].getMessage());
				}
				Property property = DAORegistry.getPropertyDAO().get(Constants.FEDEX_LABEL_MANUAL_DIRECTORY);
				String fileWithPath = property.getValue()+File.separator+"1_"+manualFedex.getTrackingNumber();
				File file = new File(fileWithPath+".pdf");
				if(file.exists()){
					file.delete();
				}
				
				//manualFedex.setTrackingNumber(null);;
				//manualFedex.setFedexLabelCreated(false);
				DAORegistry.getManualFedexGenerationDAO().delete(manualFedex);
				fedExLabelLog.setStatus("SUCCESS");
				
				returnString = "Fedex label is deleted successfully.";
			}else{
				if(reply.getNotifications()!=null && reply.getNotifications().length > 0){
					fedExLabelLog.setNotificationCode(reply.getNotifications()[0].getCode());
					fedExLabelLog.setNotificationMessage(reply.getNotifications()[0].getMessage());
					returnString = reply.getNotifications()[0].getMessage();
				}
				fedExLabelLog.setStatus("FAILURE");
			}
			DAORegistry.getFedExLabelLogDAO().save(fedExLabelLog);
		} catch (Exception e) {
			e.printStackTrace();
			returnString = "Something went wrong while deleting fedex label";
		}
		return returnString;
	}
}
