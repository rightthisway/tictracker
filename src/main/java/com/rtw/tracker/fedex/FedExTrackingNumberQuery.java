package com.rtw.tracker.fedex;

public class FedExTrackingNumberQuery {
	
	private String customerName;
	private String orderNumber;
	private String requestNumber;
	private String fedExTrackingNumber;
	private String emailId;
	private String eventName;
	
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getFedExTrackingNumber() {
		return fedExTrackingNumber;
	}
	public void setFedExTrackingNumber(String fedExTrackingNumber) {
		this.fedExTrackingNumber = fedExTrackingNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	

}
