package com.rtw.tracker.fedex;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.rtw.tracker.dao.implementation.HibernateDAO;



/**
 * PropertyDAO.
 * @author frederic.dangngoc
 *
 */
public final class PropertyDAO extends HibernateDAO<String, Property> {
	
	@Override
	protected void initDao() throws Exception {
	}
	
	public Map<String, String> getPropertyMap() {
		Collection<Property> properties = getAll();
		Map<String, String> propertyMap = new HashMap<String, String>();;
		for (Property property: properties) {
			propertyMap.put(property.getName(), property.getValue());
		}
		return propertyMap;
	}
	
	public Collection<Property> getPropertyByPrefix(String prefix) {
		return find("FROM Property WHERE name LIKE '" + prefix + "%'");
	}	
}
